package com.slicktechnologies.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface NameServiceAsync {
   public void   getClientName(String url,AsyncCallback<String>callback);
}
