package com.slicktechnologies.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface TestMassDataLoadingServiceAsync 
{
	public void LoadMassCustomerData(AsyncCallback<Void> callback);
}
