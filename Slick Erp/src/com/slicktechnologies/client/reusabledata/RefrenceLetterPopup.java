package com.slicktechnologies.client.reusabledata;

import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;

public class RefrenceLetterPopup extends AbsolutePanel{

	Button btnOk,btnCancel;
	
	TextBox refNo;
	TextBox designation;
	DateBox refDate;
	
	RadioButton forBank;
	RadioButton forAll;
	
	
	
	
public RefrenceLetterPopup(){
	DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy"); 
		btnOk=new Button("Ok");
		btnCancel=new Button("Cancel");
		
		InlineLabel displayMessage = new InlineLabel();
		add(displayMessage,30,10);
		displayMessage.getElement().getStyle().setFontSize(11, Unit.PX);
		displayMessage.getElement().getStyle().setTextAlign(TextAlign.CENTER);
		
		InlineLabel ferNo=new InlineLabel("Reference Number");
		ferNo.getElement().getStyle().setFontSize(11, Unit.PX);
		add(ferNo,25,10);
		
		refNo = new TextBox();
		add(refNo,25,25);
		
		
		InlineLabel ferdate=new InlineLabel("Reference Date");
		ferdate.getElement().getStyle().setFontSize(11, Unit.PX);
		add(ferdate,220,10);
		
		refDate = new DateBox();
		add(refDate,220,25);
		refDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		InlineLabel fdesignation=new InlineLabel("Designation");
		fdesignation.getElement().getStyle().setFontSize(11, Unit.PX);
		add(fdesignation,25,60);
		
		designation = new TextBox();
		add(designation,25,75);
		
		
		InlineLabel lable1=new InlineLabel("Bank");
		lable1.getElement().getStyle().setFontSize(11, Unit.PX);
		add(lable1,55,120);
		
		forBank = new RadioButton("");
		add(forBank,25,120);
		
		InlineLabel lable2=new InlineLabel("Offices");
		lable2.getElement().getStyle().setFontSize(11, Unit.PX);
		add(lable2,55,160);
		
		forAll = new RadioButton("");
		add(forAll,25,160);
		
		
		btnOk.getElement().getStyle().setWidth(50, Unit.PX);
		btnCancel.getElement().getStyle().setWidth(70, Unit.PX);
		
		add(btnOk,150,200);
		add(btnCancel,230,200);
		setSize("500px", "250px");
		this.getElement().setId("form");
	}

//******************getters and setters ******************


public Button getBtnOk() {
	return btnOk;
}




public void setBtnOk(Button btnOk) {
	this.btnOk = btnOk;
}




public Button getBtnCancel() {
	return btnCancel;
}




public void setBtnCancel(Button btnCancel) {
	this.btnCancel = btnCancel;
}




public TextBox getRefNo() {
	return refNo;
}




public void setRefNo(TextBox refNo) {
	this.refNo = refNo;
}




public TextBox getDesignation() {
	return designation;
}




public void setDesignation(TextBox designation) {
	this.designation = designation;
}




public DateBox getRefDate() {
	return refDate;
}




public void setRefDate(DateBox refDate) {
	this.refDate = refDate;
}

public RadioButton getForBank() {
	return forBank;
}

public void setForBank(RadioButton forBank) {
	this.forBank = forBank;
}

public RadioButton getForAll() {
	return forAll;
}

public void setForAll(RadioButton forAll) {
	this.forAll = forAll;
}



}
