package com.slicktechnologies.client.reusabledata;

import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;


public class ConfigPopup extends AbsolutePanel{

	Button btnOk,btnCancel;
	
	TextBox valueBox;
	
	
	public ConfigPopup(String string){
		
		btnOk=new Button("Ok");
		btnCancel=new Button("Cancel");
		
		InlineLabel displayMessage = new InlineLabel();
		add(displayMessage,30,10);
		displayMessage.getElement().getStyle().setFontSize(12, Unit.PX);
		displayMessage.getElement().getStyle().setTextAlign(TextAlign.CENTER);
		
		
		valueBox = new TextBox();
		add(valueBox,25,25);
		
		btnOk.getElement().getStyle().setWidth(50, Unit.PX);
		btnCancel.getElement().getStyle().setWidth(70, Unit.PX);
		
		add(btnOk,150,65);
		add(btnCancel,230,65);
		setSize("500px", "110px");
		this.getElement().setId("form");
	}

		/**************************************Getters And Setters******************************************/
	
	public Button getBtnOk() {
		return btnOk;
	}


	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}


	public Button getBtnCancel() {
		return btnCancel;
	}


	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}


	public TextBox getValueBox() {
		return valueBox;
	}


	public void setValueBox(TextBox valueBox) {
		this.valueBox = valueBox;
	}
	
	
	
	
	
}
