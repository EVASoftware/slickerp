package com.slicktechnologies.client.reusabledata;

import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;

public class PrintDialogBox extends AbsolutePanel {

	CheckBox prePrinted,withoutPrePrinted;
	
	Button btnOk,btnCancel;
	
	
	public PrintDialogBox(String showMessage,String btnOneName,String btnTwoName) {
		
		
		btnOk = new Button("Ok");
		btnCancel=  new Button("Cancel");
		
		
		prePrinted = new CheckBox("Prit on preprinted ");
		withoutPrePrinted=  new CheckBox("");
		
		
		InlineLabel displayMessage = new InlineLabel(showMessage);
		add(displayMessage,10,10);
		displayMessage.getElement().getStyle().setFontSize(12, Unit.PX);
		displayMessage.getElement().getStyle().setTextAlign(TextAlign.CENTER);
		
		btnOk.getElement().getStyle().setWidth(50, Unit.PX);
		btnCancel.getElement().getStyle().setWidth(50, Unit.PX);
		
		add(btnOk,150,65);
		add(btnCancel,230,65);
		setSize("450px", "110px");
		this.getElement().setId("form");
	}

	//********************getters and setters ***************************

	public Button getBtnOk() {
		return btnOk;
	}


	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}


	public Button getBtnCancel() {
		return btnCancel;
	}


	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}
	
	
	
	
	
	
}
