package com.slicktechnologies.client.reusabledata;

import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.slicktechnologies.client.utility.AppConstants;

public class ConditionDialogBox extends AbsolutePanel {
	
	Button btnOne,btnTwo,btnThree/** date 6.4.2018  added by komal **/;
	InlineLabel displayMessage ;
	/**
	 * nidhi 19-12-2018
	 * @param showMessage
	 * @param btnOneName
	 * @param btnTwoName
	 */
	CheckBox cbPrePrint ;
		public ConditionDialogBox(String showMessage,String btnOneName,String btnTwoName) {
			
			btnOne=new Button(AppConstants.YES);
			btnTwo=new Button(AppConstants.NO);
			
//			InlineLabel displayMessage = new InlineLabel(showMessage);
			displayMessage = new InlineLabel(showMessage);
			add(displayMessage,10,10);
			displayMessage.getElement().getStyle().setFontSize(12, Unit.PX);
			displayMessage.getElement().getStyle().setTextAlign(TextAlign.CENTER);
			
			btnOne.getElement().getStyle().setWidth(70, Unit.PX);
			btnTwo.getElement().getStyle().setWidth(50, Unit.PX);
			
			add(btnOne,150,65);
			add(btnTwo,230,65);
			setSize("450px", "110px");
			this.getElement().setId("form");
		}

		/** date 6.4.2018 added by komal for fumigation**/
		public ConditionDialogBox(String showMessage,String btnOneName,String btnTwoName,String btnThreeName) {
			
			btnOne=new Button(btnOneName);
			btnTwo=new Button(btnTwoName);
			btnThree=new Button(btnThreeName);

			displayMessage = new InlineLabel(showMessage);
			add(displayMessage,10,10);
			displayMessage.getElement().getStyle().setFontSize(12, Unit.PX);
			displayMessage.getElement().getStyle().setTextAlign(TextAlign.CENTER);
			
			//btnOne.getElement().getStyle().setWidth(50, Unit.PX);
			//btnTwo.getElement().getStyle().setWidth(50, Unit.PX);
			
			add(btnOne,50,65);
			add(btnTwo,200,65);
			add(btnThree,350,65); 
			setSize("550px", "110px");
			this.getElement().setId("form");
		}

		public ConditionDialogBox(String showMessage,String btnOneName,String btnTwoName,boolean checkBox) {
			
			btnOne=new Button(AppConstants.YES);
			btnTwo=new Button(AppConstants.NO);
			/**
			 * nidhi
			 * 19-12-2018
			 */
			
			cbPrePrint = new CheckBox("Preprinted Stationery");
			cbPrePrint.setValue(false);
			add(cbPrePrint,15,35);
//			InlineLabel displayMessage = new InlineLabel(showMessage);
			displayMessage = new InlineLabel(showMessage);
			add(displayMessage,10,10);
			displayMessage.getElement().getStyle().setFontSize(12, Unit.PX);
			displayMessage.getElement().getStyle().setTextAlign(TextAlign.CENTER);
			
			btnOne.getElement().getStyle().setWidth(50, Unit.PX);
			btnTwo.getElement().getStyle().setWidth(50, Unit.PX);
			
			add(btnOne,150,70);
			add(btnTwo,230,70);
			setSize("450px", "120px");
			this.getElement().setId("form");
		}
		
	/**************************************Getters And Setters******************************************/
		
		public Button getBtnOne() {
			return btnOne;
		}

		public void setBtnOne(Button btnOne) {
			this.btnOne = btnOne;
		}

		public Button getBtnTwo() {
			return btnTwo;
		}

		public void setBtnTwo(Button btnTwo) {
			this.btnTwo = btnTwo;
		}


		public InlineLabel getDisplayMessage() {
			return displayMessage;
		}


		public void setDisplayMessage(InlineLabel displayMessage) {
			this.displayMessage = displayMessage;
		}

		public Button getBtnThree() {
			return btnThree;
		}

		public void setBtnThree(Button btnThree) {
			this.btnThree = btnThree;
		}

		public CheckBox getCbPrePrint() {
			return cbPrePrint;
		}

		public void setCbPrePrint(CheckBox cbPrePrint) {
			this.cbPrePrint = cbPrePrint;
		}

		

		
		

}
