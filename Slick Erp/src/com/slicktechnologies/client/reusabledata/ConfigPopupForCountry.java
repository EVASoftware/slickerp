package com.slicktechnologies.client.reusabledata;

import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;

public class ConfigPopupForCountry extends AbsolutePanel{

	
	
Button btnOk,btnCancel;
TextBox countryName,language;
	
	public ConfigPopupForCountry(String showMessage1,String showMessage2){
		
		btnOk=new Button("Ok");
		btnCancel=new Button("Cancel");
		
		InlineLabel displayMessage1 = new InlineLabel(showMessage1);
		add(displayMessage1,30,10);
		displayMessage1.getElement().getStyle().setFontSize(12, Unit.PX);
		displayMessage1.getElement().getStyle().setTextAlign(TextAlign.CENTER);
		
	
		InlineLabel displayMessage2 = new InlineLabel(showMessage2);
		add(displayMessage2,255,10);
		displayMessage2.getElement().getStyle().setFontSize(12, Unit.PX);
		displayMessage2.getElement().getStyle().setTextAlign(TextAlign.CENTER);
		
		
		countryName = new TextBox();
		add(countryName,25,25);
		
		language = new TextBox();
		add(language,255,25);
		
		btnOk.getElement().getStyle().setWidth(50, Unit.PX);
		btnCancel.getElement().getStyle().setWidth(70, Unit.PX);
		
		add(btnOk,150,65);
		add(btnCancel,230,65);
		setSize("500px", "110px");
		this.getElement().setId("form");
	}

	/**************************************Getters And Setters******************************************/
	
	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public TextBox getCountryName() {
		return countryName;
	}

	public void setCountryName(TextBox countryName) {
		this.countryName = countryName;
	}

	public TextBox getLanguage() {
		return language;
	}

	public void setLanguage(TextBox language) {
		this.language = language;
	}
	
}
