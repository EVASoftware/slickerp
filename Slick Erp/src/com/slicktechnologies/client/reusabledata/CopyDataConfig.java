package com.slicktechnologies.client.reusabledata;

import java.util.ArrayList;
import java.util.List;

import com.slicktechnologies.client.config.categoryconfig.CategoryTypes;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;

public class CopyDataConfig {
	
//	public static boolean copyData(String typeOfModule)
//	{
//		ArrayList<ProcessConfiguration> processConfigArray=LoginPresenter.globalProcessConfig;
//		boolean copyStatus=false;
//		List<ProcessTypeDetails> processDetailsLis=new ArrayList<ProcessTypeDetails>();
//		if(processConfigArray.size()!=0)
//		{
//			
//			for(int d=0;d<processConfigArray.size();d++)
//			{
//				if(processConfigArray.get(d).getProcessName().trim().equalsIgnoreCase(typeOfModule.trim()))
//				{
//					processDetailsLis=processConfigArray.get(d).getProcessList();
//				}
//			}
//			
//			System.out.println("List Size For Sales"+processDetailsLis.size());
//			for(int i=0;i<processDetailsLis.size();i++)
//			{
//				if(processDetailsLis.get(i).getProcessType().trim().equals(AppConstants.COPYDATA))
//				{
//					copyStatus=processDetailsLis.get(i).isStatus();
//				}
//			}
//		}
//		return copyStatus;
//	}
//	
	
	
	public static boolean copyListBoxData(String checkValue,int internalTypeVal,String globalCheck)
	{
		
		boolean listStatus=false;
		
		if(globalCheck.trim().equals(AppConstants.GLOBALRETRIEVALCATEGORY))
		{
			ArrayList<ConfigCategory> catArray=LoginPresenter.globalCategory;
			for(int i=0;i<catArray.size();i++)
			{
				if(catArray.get(i).getCategoryName().trim().equals(checkValue.trim())&&catArray.get(i).getInternalType()==internalTypeVal)
				{
					listStatus=true;
				}
			}
		}
		
		if(globalCheck.trim().equals(AppConstants.GLOBALRETRIEVALCONFIG))
		{
			ArrayList<Config> configArray=LoginPresenter.globalConfig;
			for(int i=0;i<configArray.size();i++)
			{
				if(configArray.get(i).getName().trim().equals(checkValue.trim())&&configArray.get(i).getType()==internalTypeVal)
				{
					listStatus=true;
				}
			}
		}
		
		if(globalCheck.trim().equals(AppConstants.GLOBALRETRIEVALTYPE))
		{
			ArrayList<Type> typeArray=LoginPresenter.globalType;
			for(int i=0;i<typeArray.size();i++)
			{
				if(typeArray.get(i).getTypeName().trim().equals(checkValue.trim())&&typeArray.get(i).getInternalType()==internalTypeVal)
				{
					listStatus=true;
				}
			}
		}
		
		return listStatus;
	}
	
	
	public static boolean validateCopyData()
	{
		System.out.println("Process name");
		boolean valCopyData=false;
		
		ArrayList<ProcessName> arrayProcessName=LoginPresenter.globalProcessName;
		for(int i=0;i<arrayProcessName.size();i++)
		{
			if(arrayProcessName.get(i).getProcessName().trim().equals(AppConstants.COPYDATA)&&arrayProcessName.get(i).isStatus()==true){
				valCopyData=true;
			}
		}
		return valCopyData;
	}
	
	public static boolean validateProcessConfig(String typeOfModule)
	{
		ArrayList<ProcessConfiguration> arrConfigProcess=LoginPresenter.globalProcessConfig;
		boolean processConfigStatus=false;
		List<ProcessTypeDetails> typeDetails=new ArrayList<ProcessTypeDetails>();
		
		for(int i=0;i<arrConfigProcess.size();i++)
		{System.out.println("Print"+arrConfigProcess.get(i).getProcessName());
			if(arrConfigProcess.get(i).getProcessName().trim().equals(AppConstants.COPYDATA))
			{
				typeDetails=arrConfigProcess.get(i).getProcessList();
			}
		}
		
		System.out.println("Size PCC"+typeDetails.size());
		
		for(int d=0;d<typeDetails.size();d++)
		{
			if(typeDetails.get(d).getProcessType().trim().equalsIgnoreCase(typeOfModule.trim())&&typeDetails.get(d).isStatus()==true)
			{
				processConfigStatus=true;
			}
		}
		
		return processConfigStatus;
	}
	
	
	
	public static boolean validateDataCopying(String typeOfScreenModule)
	{
		boolean copyingFlag=false;
		boolean validatingProcessName=validateCopyData();
		System.out.println("Process name validation"+validatingProcessName);
		boolean validatingProcessConfig=false;
		
		if(validatingProcessName==true)
		{
			validatingProcessConfig=validateProcessConfig(typeOfScreenModule.trim());
			System.out.println("Inside PC Copy"+validatingProcessConfig);
		}
		
		System.out.println("PCONFIG"+validatingProcessConfig);
		
		if(validatingProcessName==true){
			
			if(validatingProcessConfig==true){
				copyingFlag=true;
			}
		}
		
		return copyingFlag;
	}
	
	
	public static boolean validateData(String type,String data,Screen screenType)
	{
		boolean status=false;
		if(type.trim().equals(AppConstants.COPYCATEGORYDATA))
		{
			status=CopyDataConfig.copyListBoxData(data.trim(),CategoryTypes.getCategoryInternalType(screenType),AppConstants.GLOBALRETRIEVALCATEGORY);
		}
		
		if(type.trim().equals(AppConstants.COPYTYPEDATA))
		{
			status=CopyDataConfig.copyListBoxData(data.trim(),CategoryTypes.getCategoryInternalType(screenType),AppConstants.GLOBALRETRIEVALTYPE);
		}
		
		if(type.trim().equals(AppConstants.COPYGROUPDATA))
		{
			status=CopyDataConfig.copyListBoxData(data.trim(),ConfigTypes.getConfigType(screenType),AppConstants.GLOBALRETRIEVALCONFIG);
		}
		
		if(type.trim().equals(AppConstants.COPYLEVELDATA))
		{
			status=CopyDataConfig.copyListBoxData(data.trim(),ConfigTypes.getConfigType(screenType),AppConstants.GLOBALRETRIEVALCONFIG);
		}
		
		if(type.trim().equals(AppConstants.COPYPRIORITYDATA))
		{
			status=CopyDataConfig.copyListBoxData(data.trim(),ConfigTypes.getConfigType(screenType),AppConstants.GLOBALRETRIEVALCONFIG);
		}
		
		
		
		return status;
	}
	
	

}
