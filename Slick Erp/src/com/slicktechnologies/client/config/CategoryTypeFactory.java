package com.slicktechnologies.client.config;

import java.util.Vector;

import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.OneManyComboBox;
import com.slicktechnologies.client.config.categoryconfig.CategoryTypes;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * A factory for providing Category Type functioality to Combo Boxes.
 */
public class CategoryTypeFactory 
{
	
	/**
	 * Initiate one many functionality.
	 *
	 * @param <T> the generic type for One Kind
	 * @param <C> the generic type for Many Kind
	 * @param one the one One object List Box
	 * @param many the many Many List Box
	 */
	public static <T extends SuperModel,C extends SuperModel> void initiateOneManyFunctionality(
			ObjectListBox<T>one,ObjectListBox<C> many)
	{
		OneManyComboBox<T,C> banana=new OneManyComboBox<T,C>(one,many);
		
		
		setOneAndManyQuery(banana);

		
		
	}
	
	/**
	 * To provide One To Many Functionality there are a series of Steps this class took
	 * those series of steps.
	 * To Do: Give inline documentation Pradnya
	 *
	 * @param cat the new one and many query
	 */
	private static void setOneAndManyQuery(OneManyComboBox cat)
	{
		   Screen screen=(Screen) AppMemory.getAppMemory().currentScreen;

		    Filter filter=new Filter();
		    filter.setQuerryString("internalType");
		    filter.setIntValue(CategoryTypes.getCategoryInternalTypeOnFormScreen(screen));
		    Filter statusfilter=new Filter();
		    statusfilter.setBooleanvalue(true);
		    statusfilter.setQuerryString("status");
		   
		    Vector<Filter> vec=new Vector<Filter>();
		    vec.add(filter);
		    vec.add(statusfilter);
		    
		    MyQuerry oneQuerry=new MyQuerry(vec, new ConfigCategory());
		    
		   
		   // create many querry
		    Filter filtervalue=new Filter();
			String querrycon="categoryKey";
			filtervalue.setQuerryString(querrycon);
			Vector<Filter> manyfilter=new Vector<Filter>();
			manyfilter.add(filtervalue);
		   MyQuerry manyquerry=new MyQuerry(manyfilter, new Type());
		 
		   cat.setOneQuerry(oneQuerry);
		   cat.setManyQuerry(manyquerry);
		   cat.initializeHashMaps();
		   
	}

}
