package com.slicktechnologies.client.config.configurations;

import com.slicktechnologies.client.utility.Screen;


/**
 * Enum for ConfigTypes.The numeric value of config repersents a specifec type of Config.
 * @author Kamala
 *
 */
public enum ConfigTypes{
	//Token to add the configTypes with integer value

	SERVICEPRODUCTCATEGORY(0),ITEMMASTERCATEGORY(1),COMPANYTYPE(2),CUSTOMERLEVEL(3),CUSTOMERTYPE(4),CUSTOMERSTATUS(5),  
	LEADSTATUS(6),LEADPRIORITY(7),CONTRACTSTATUS(8),CUSTOMERPRIORITY(9),QUOTATIONPRIORITY(10), QUOTATIONSTATUS(11),
	EMPLOYEEROLE(12),EMPLOYEEDESIGNATION(13),ACESSLEVEL(14),EXPENSECATEGORY(15),PAYMENTMETHODS(16),PAYMENTTERMS(17),
	CURRENCY(18),ACTIONTAKEN(19),SERVICESTATUS(20),COMPLAINPRIORITY(21),COMPLAINCATEGORY(22),COMPLAINSTATUS(23),
	ACTIONPRIORITY(24),FOLLOWUPCATEGORY(25),FOLLOWUPTYPE(26),ACTIONSTATUS(27),EXPENSETYPE(28),CUSTOMERGROUP(29),
	QUOTATIONGROUP(30),CONTRACTGROUP(31),LEADGROUP(32),VENDORGROUP(33),EMPLOYEETYPE(34),EMPLOYEEGROUP(35),
	SHIFTCATEGORY(36),PATTERNCATEGORY(37),COMPLAINGROUP(38) ,COMPLIANTYPE(39), EXPENSEGROUP(40),ASSETCATEGORY(41),
	CLIENTSIDEASEETCATEGORY(42),ATTENDANCECONFIG(43),LOITYPE(44),LOIGROUP(45),BILLINGGROUP(46),UNITOFMEASUREMENT(47),
	GLACCOUNTGROUP(48),SALESORDERGROUP(49), SALESQUOTATIONGROUP(50),SALESQUOTATIONPRIORITY(51),QUOTATIONTYPE(52),
    GRNTYPE(53),GRNGROUP(54),PRGROUP(55),MMNDIRECTION(56),PRODUCTTYPE(57),PRODUCTGROUP(58),PRODUCTCLASSIFICATION(59),
    CHECKLISTCATEGORY(60),DELIVERYNOTEGROUP(61),PROCESSNAME(62),COMMUNICATIONTYPE(63),COMMUNICATIONCATEGORY(64),
    PERSONTYPE(65),GROUPTYPE(66),CONTACTROLE(67),CONTACTPERSON(68),CUSTOMERCONTACT(69),INTERACTIONPRIORITY(70),
    TICKETPRIORITY(71),TICKETLEVEL(72),PROCESSTYPE(73),ARTICLETYPE(74),INSPECTIONGROUP(75),WORKORDERGROUP(76),INVOICEGROUP(77),SERVICETYPE(78),APPROVALLEVEL(79),
    FUMIGATIONDECLAREDPOE(80),DOSAGERATEOFFUMIGANT(81),DURATIONOFFUMIGATION(82),MINIMUMAIRTEMP(83),EMPLOYEEFAMILYRELATION(84),ADDRESSTYPE(85),
    PLACEOFFUMIGATION(86),COUNTRYOFLOADING(87),QUANTITYDECLAREUNIT(88),SPECIFICREASONFORRESCHEDULING(89),DOCUMENTTYPE(90),NUMBERRANGE(91),CATCHTRAPPESTNAME(92)
     ,ASSETARTICLETYPE(93),ACTIONPLANFORCOMPANY(94),ACTIONPLANFORCUSTOMER(95),TDSPERCENTAGE(96),WAREHOUSETYPE(97),SALUTATION(98),RELIGION(99),SERVICEUOM(100),CONTRACTPERIOD(101),
     /**Date: 1/2/2018 By:Manisha
      *  for frequency and TypeOfTreatment drop down at product level..!;
      */
     FREQUENCY1(102),TYPEOFTREATMENT(103),
     /*End*/  
       /** added by komal **/  WARRANTYSTATUS(104) , CALLTYPE(105), CALLBACKSTATUS(106) , VISITSTATUS(107), CALLLOGSTATUS(108),LEADUNSUCCESFULREASONS(109),FUMIGATIONPERFORMED(110),VENTILATED(111),
       TRAINING(112),/** date 31.8.2018 added by komal**/CTCCATEGORY(113),BRAND(114),DONOTRENEWREMARK(115),CANCELLATIONREMARK(116),CLASSIFICATIONOFCITY(117),CASTE(118),TAXATIONYEAR(119),TAXCATEGORY(120),
       SECTIONNAME(121),SERVICECHECKLIST(122),ASSESSMENTRISKLEVEL(123),TECHNICIANREMARK(124),COMPANYPAYMENTTYPE(125);
	/**
	 * Constructor of Enum instatniating a new Config Type
	 */
	private final int value;

	private ConfigTypes(final int newValue) {
		value = newValue;
	}

	/**
	 * Returns int value of the configtype 
	 * @return value
	 */
	public int getValue() 
	{ 
		return value; 
	}

	/**
	 *  Returns the integer value corresponding to configType Screen
	 *  There must be one to one relation beetween Config Type and Screen.
	 *   
	 * @return
	 */
	public static int getConfigType(Screen screen) {

		switch(screen)
		{

		case SERVICEPRODUCTCATEGORY:
			return SERVICEPRODUCTCATEGORY.getValue();
		case ITEMMASTERCATEGORY:
			return ITEMMASTERCATEGORY.getValue();
		case COMPANYTYPE:
			return COMPANYTYPE.getValue();
		case CUSTOMERLEVEL:
			return CUSTOMERLEVEL.getValue();

		case CUSTOMERSTATUS:
			return CUSTOMERSTATUS.getValue();
		case   LEADSTATUS:
			return   LEADSTATUS.getValue();
		case LEADPRIORITY:
			return LEADPRIORITY.getValue();
		case CONTRACTSTATUS:
			return CONTRACTSTATUS.getValue();
		case CUSTOMERPRIORITY:
			return CUSTOMERPRIORITY.getValue();
		case QUOTATIONPRIORITY:
			return QUOTATIONPRIORITY.getValue();
		case   QUOTATIONSTATUS:
			return   QUOTATIONSTATUS.getValue();
		case EMPLOYEEROLE:
			return EMPLOYEEROLE.getValue();
		case EMPLOYEEDESIGNATION:
			return EMPLOYEEDESIGNATION.getValue();
		case ACESSLEVEL:
			return ACESSLEVEL.getValue();
		case   EXPENSECATEGORY:
			return   EXPENSECATEGORY.getValue();
		case PAYMENTMETHODS:
			return PAYMENTMETHODS.getValue();
		case PAYMENTTERMS:
			return PAYMENTTERMS.getValue();
		case CURRENCY:
			return CURRENCY.getValue();
		case ACTIONTAKEN:
			return ACTIONTAKEN.getValue();
		case   SERVICESTATUS:
			return   SERVICESTATUS.getValue();
		case COMPLAINPRIORITY:
			return COMPLAINPRIORITY.getValue();
		case COMPLAINCATEGORY:
			return COMPLAINCATEGORY.getValue();
		case COMPLAINSTATUS:
			return COMPLAINSTATUS.getValue();
		case ACTIONPRIORITY:
			return ACTIONPRIORITY.getValue();
		case FOLLOWUPCATEGORY:
			return FOLLOWUPCATEGORY.getValue();
		case FOLLOWUPTYPE:
			return FOLLOWUPTYPE.getValue();
		case ACTIONSTATUS:
			return ACTIONSTATUS.getValue();
		case EXPENSETYPE:
			return EXPENSETYPE.getValue();
		case EMPLOYEETYPE:
			return EMPLOYEETYPE.getValue();
		case EMPLOYEEGROUP:
			return EMPLOYEEGROUP.getValue();
		case SHIFTCATEGORY:
			return SHIFTCATEGORY.getValue();
		case PATTERNCATEGORY:
			return PATTERNCATEGORY.getValue();
		case EXPENSEGROUP:
			return EXPENSEGROUP.getValue();
		case LEADGROUP:
			return LEADGROUP.getValue();
		case QUOTATIONGROUP:
			return QUOTATIONGROUP.getValue();
		case CONTRACTGROUP:
			return CONTRACTGROUP.getValue();
		case CUSTOMERGROUP:
			return CUSTOMERGROUP.getValue();
		case VENDORGROUP:
			return VENDORGROUP.getValue();
		case ASSETCATEGORY:
			return ASSETCATEGORY.getValue();
		case CLIENTSIDEASSETCATEGORY:
			return CLIENTSIDEASEETCATEGORY.getValue();
		case ATTENDANCECONFIG:
			return ATTENDANCECONFIG.getValue();
		case BILLINGGROUP:
			return BILLINGGROUP.getValue();
		case UNITOFMEASUREMENT:
			return UNITOFMEASUREMENT.getValue();
		case GLACCOUNTGROUP:
			return GLACCOUNTGROUP.getValue();
		case SALESORDERGROUP:
			return SALESORDERGROUP.getValue();
		case SALESQUOTATIONGROUP:
			return SALESQUOTATIONGROUP.getValue();
		case SALESQUOTATIONPRIORITY:
			return SALESQUOTATIONPRIORITY.getValue();
		case QUOTATIONTYPE:
			return QUOTATIONTYPE.getValue();		
		case PRGROUP:
			return PRGROUP.getValue();
		case GRNTYPE:
			return GRNTYPE.getValue();
		case GRNGROUP:
			return GRNGROUP.getValue();
		case MMNDIRECTION:
			return MMNDIRECTION.getValue();
		case PRODUCTTYPE:
			return PRODUCTTYPE.getValue();
		case PRODUCTGROUP:
			return PRODUCTGROUP.getValue();
		case PRODUCTCLASSIFICATION:
			return PRODUCTCLASSIFICATION.getValue();
		case CHECKLISTCATEGORY:
			return CHECKLISTCATEGORY.getValue();
		case DELIVERYNOTEGROUP:
			return DELIVERYNOTEGROUP.getValue();
		case PROCESSNAME:
			return PROCESSNAME.getValue();
		case COMMUNICATIONTYPE:
			return COMMUNICATIONTYPE.getValue();
		case COMMUNICATIONCATEGORY:
			return COMMUNICATIONCATEGORY.getValue();
		case PERSONTYPE:
			return PERSONTYPE.getValue();
		case GROUPTYPE:
			return GROUPTYPE.getValue();
		case CONTACTROLE:
			return CONTACTROLE.getValue();
		case CONTACTPERSONDETAILS:
			return CONTACTPERSON.getValue();
		case CUSTOMERCONTACT:
			return CUSTOMERCONTACT.getValue();
		case INTERACTIONPRIORITY:
			return INTERACTIONPRIORITY.getValue();
		case TICKETPRIORITY:
			return TICKETPRIORITY.getValue();
		case TICKETLEVEL:
			return TICKETLEVEL.getValue();
		case PROCESSTYPE:
			return PROCESSTYPE.getValue();
		case ARTICLETYPE:
			return ARTICLETYPE.getValue();
		case INSPECTIONGROUP:
			return INSPECTIONGROUP.getValue();
		case WORKORDERGROUP:
			return WORKORDERGROUP.getValue();
		case INVOICEGROUP:
			return INVOICEGROUP.getValue();
		case SERVICETYPE:
			return SERVICETYPE.getValue();
		case APPROVALLEVEL:
			return APPROVALLEVEL.getValue();
		case FUMIGATIONDECLAREDPOE:
			return FUMIGATIONDECLAREDPOE.getValue();
		
		case DOSAGERATEOFFUMIGANT:
			return DOSAGERATEOFFUMIGANT.getValue();
			
		case DURATIONOFFUMIGATION:
			return DURATIONOFFUMIGATION.getValue();
			
		case MINIMUMAIRTEMP:
			return MINIMUMAIRTEMP.getValue();
			
		case EMPLOYEEFAMILYRELATION:
			return EMPLOYEEFAMILYRELATION.getValue();
			
		case ADDRESSTYPE:
			return ADDRESSTYPE.getValue();
			
			
		case PLACEOFFUMIGATION:
			return PLACEOFFUMIGATION.getValue();
			
		case COUNTRYOFLOADING:
			return COUNTRYOFLOADING.getValue();
			
		case QUANTITYDECLAREUNIT:
			return QUANTITYDECLAREUNIT.getValue();	
		
		case SPECIFICREASONFORRESCHEDULING:
			return SPECIFICREASONFORRESCHEDULING.getValue();
		case DOCUMENTTYPE:
			return DOCUMENTTYPE.getValue();
			
		case NUMBERRANGE:
			return NUMBERRANGE.getValue();
			
		case CATCHTRAPPESTNAME:
			return CATCHTRAPPESTNAME.getValue();
			
		case ASSETARTICLETYPE:
			return ASSETARTICLETYPE.getValue();
			
		case ACTIONPLANFORCOMPANY:
			return ACTIONPLANFORCOMPANY.getValue();
			
		case ACTIONPLANFORCUSTOMER:
			return ACTIONPLANFORCUSTOMER.getValue();
			
		case TDSPERCENTAGE:
			return TDSPERCENTAGE.getValue();
			
		case WAREHOUSETYPE:
			return WAREHOUSETYPE.getValue();	
			
		case SALUTATION:
			return SALUTATION.getValue();
			
		case RELIGION:
			return RELIGION.getValue();	
		case SERVICEUOM :
			return SERVICEUOM.getValue();
		/**
		 * nidhi
		 * 22-12-2017
		 * for email reminder setting 
		 */
//		case CRONJOB :
//			return CRONJOB.getValue();
//			
//		case FREQUENCY :
//			return FREQUENCY.getValue();
			
		case CONTRACTPERIOD:
			return CONTRACTPERIOD.getValue();
			
			/**
			 * Manisha
			 * 1/2/2018
			 * for frequency drop down
			 */
		 case FREQUENCY1:
				return FREQUENCY1.getValue();
				
		  case TYPEOFTREATMENT:
		    	 return TYPEOFTREATMENT.getValue();
			/**END**/	
		    /** added by komal **/
			case WARRANTYSTATUS:
				return WARRANTYSTATUS.getValue();
			case CALLTYPE :
				return CALLTYPE.getValue();
			case CALLBACKSTATUS :
				return CALLBACKSTATUS.getValue();
			case VISITSTATUS :
				return VISITSTATUS.getValue();
			case CALLLOGSTATUS :
				return CALLLOGSTATUS.getValue();

				
				/*Date 16-6-2018 by jayshree**/
			case LEADUNSUCCESFULREASONS :
				return LEADUNSUCCESFULREASONS.getValue();
				
				
			case FUMIGATIONPERFORMED 	:
				return FUMIGATIONPERFORMED.getValue();
			case VENTILATED		:
				return VENTILATED.getValue();
				
			case TRAINING:
				return TRAINING.getValue();
				/** DATE 31.8.2018 added by komal**/
		  case CTCCATEGORY:
			  return CTCCATEGORY.getValue();
		  case BRAND:
			  return BRAND.getValue();
		  case DONOTRENEW:
			  return DONOTRENEWREMARK.getValue();
		  case CANCELLATIONREMARK:
			  return CANCELLATIONREMARK.getValue();
		  case CLASSIFICATIONOFCITY:
			  return CLASSIFICATIONOFCITY.getValue();
		
		/**Date 1-10-2019 by Amol**/	  
		  case CASTE:
			  return CASTE.getValue();
		  /**
		   * @author Anil , Date : 06-02-2020
		   */
		  case TAXATIONYEAR:
			  return TAXATIONYEAR.getValue();
		  case TAXCATEGORY:
			  return TAXCATEGORY.getValue();
		  case SECTIONNAME:
			  return SECTIONNAME.getValue();
		  case SERVICECHECKLIST:
			  return SERVICECHECKLIST.getValue();
		  case ASSESSMENTRISKLEVEL:
			  return ASSESSMENTRISKLEVEL.getValue();
				
		  case TECHNICIANREMARK:
			  return TECHNICIANREMARK.getValue();
			  
		  case COMPANYPAYMENTTYPE:
			  return COMPANYPAYMENTTYPE.getValue();
			  
		default:
			break;


		}

		return -1;
	}

}
