package com.slicktechnologies.client.config.configurations;


import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class ConfigTabels extends SuperTable<Config> 
{
	
	TextColumn<Config> nameColumn;
	TextColumn<Config> statusColumn;
	
	ListHandler<Config> columnSort;
	GenricServiceAsync service=GWT.create(GenricService.class);
	
	TextColumn<Config> gstApplicable;
	TextColumn<Config> wholeAmtLabel,decimalLabel; 	//Ashwini Patil Date: 14-04-2022

	public ConfigTabels()
	{
		super();
		//addNameSorting();
		
		
	}
	
	public ConfigTabels(UiScreen<Config> view)
	{
		
		super(view);
		
	}
	
	private void createColumnName()
	{
	  nameColumn =  new TextColumn<Config>()
  		  {
       @Override
       public String getValue(Config object) 
       {
          return object.getName();
       }
    };
    table.addColumn(nameColumn, "Name");
    nameColumn.setSortable(true);
   }

private void createColumnStatus()
	{
	  statusColumn = new TextColumn<Config>() {
        @Override
        public String getValue(Config object) {
           
       	
        	if(object.isStatus()==true)
           	return AppUtility.ACTIVE;
           else
           	return AppUtility.INACTIVE;
        }
     };
     
     
     table.addColumn(statusColumn, "Status");
     statusColumn.setSortable(true);
	}


	
 public void createTable()
	{
		 
		   createColumnName();
		   createColumnStatus();
		   table.setWidth("100%");
		   setFieldupdaterName();
		   
		   //Ashwini Patil Date:14-04-20222 
		   if(AppMemory.getAppMemory().currentScreen == Screen.CURRENCY){
			   createColumnAmountLabelColumns();
		   }
		   
		   setFieldupdaterstatus();
		   if(AppMemory.getAppMemory().currentScreen == Screen.NUMBERRANGE){
			   createColumnGSTApplicable();
		   }
		   
		   

		  
	}
 
 private void createColumnGSTApplicable() {

	 gstApplicable = new TextColumn<Config>() {
		
		@Override
		public String getValue(Config object) {
			if(object.isGstApplicable()){
				return "Yes";
			}
			else{
				return "No";
			}
		}
	};
	table.addColumn(gstApplicable, "GST Applicable");
	gstApplicable.setSortable(true);
}

 private void createColumnAmountLabelColumns() {

	 wholeAmtLabel = new TextColumn<Config>() {
		
		@Override
		public String getValue(Config object) {
			return object.getWholeAmtLabel();
		}
	};
	decimalLabel = new TextColumn<Config>() {
		
		@Override
		public String getValue(Config object) {
			return object.getDecimalLabel();
		}
	};
	table.addColumn(wholeAmtLabel, "Label for Whole Amount");
	table.addColumn(decimalLabel, "Label for Decimal Amount");
}
private void setFieldupdaterName()
 {
	 nameColumn.setFieldUpdater(new FieldUpdater<Config, String>() {
		
		@Override
		public void update(int index, Config object, String value) {
			object.setName(value);
			table.redrawRow(index);
			
		}
	});
	 
 }
 
 private void setFieldupdaterstatus()
 {
	 statusColumn.setFieldUpdater(new FieldUpdater<Config, String>() {
		
		@Override
		public void update(int index, Config object, String value) {
			
			
				
			table.redrawRow(index);
			
		}
	});
	 
 }


@Override
protected void initializekeyprovider() {
	keyProvider = new ProvidesKey<Config>() {
		@Override
		public Object getKey(Config item) {
			if(item==null)
				return null;
			else
				return item.getId();
		}
	};
	
 }

//Since config table live status depends upon the type hence querry Structure will be differnt



public void addColumnSorting()
{
	
	addNameSorting();
	addStatusSorting();
	addGSTApplicableSorting();
	
}

	 

public void addNameSorting()
{
	List<Config> list=getDataprovider().getList();
	columnSort=new ListHandler<Config>(list);
	columnSort.setComparator(nameColumn, new Comparator<Config>()
			{
				
				@Override
				public int compare(Config e1, Config e2) 
				{
					System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.");
					if(e1!=null && e2!=null)
					{
						if(e1.getName()!=null && e2.getName()!=null)
							return e1.getName().compareTo(e2.getName());
						return 0;
					}
					else
						return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}

public void addStatusSorting()
{
	List<Config> list=getDataprovider().getList();
	columnSort=new ListHandler<Config>(list);
	columnSort.setComparator(statusColumn, new Comparator<Config>()
			{
				
				@Override
				public int compare(Config e1, Config e2) 
				{
					if(e1!=null && e2!=null)
					{
						if(e1.isStatus()==e2.isStatus())
							
						return 0;
						else if(e1.isStatus()==true)
							return 1;
						else
							return -1;
					}
					else
						return -1;
				}
			});
	table.addColumnSortHandler(columnSort);
}

@Override
public void setEnable(boolean state) {
	
	
}

public void applyStyle() {
}

@Override
public void addFieldUpdater() {
	
}

private void addGSTApplicableSorting() {
	
	 List<Config> list=getDataprovider().getList();
		columnSort=new ListHandler<Config>(list);
		columnSort.setComparator(gstApplicable, new Comparator<Config>()
				{
					
					@Override
					public int compare(Config e1, Config e2) 
					{
						if(e1!=null && e2!=null)
						{
							if(e1.isGstApplicable()==e2.isGstApplicable())
								
							return 0;
							else if(e1.isGstApplicable()==true)
								return 1;
							else
								return -1;
						}
						else
							return -1;
					}
				});
		table.addColumnSortHandler(columnSort);

}


}
 
	
	
	

