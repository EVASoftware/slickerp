package com.slicktechnologies.client.config.configurations;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.*;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.complain.TimeFormatBox;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.shared.common.helperlayer.*;

// TODO: Auto-generated Javadoc
/**
 * Ui of Config,this UI is same for any type of Config
 */
public class ConfigForm extends FormTableScreen<Config> {
	/** The name. */
	public TextBox name;

	/** The status. */
	public CheckBox status;

	/** The presenter. */
	protected ConfigPresenter presenter;
	
	  /**
	   * @author Anil @since 01-10-2021
	   * Adding invoice title for non gst invoices and print bank details flag
	   * Raised by Rahul Tiwari and Nitin Sir
	   */
	  TextBox tbInvoiceTitle;
	  CheckBox cbPrintBankDetails;

	public ConfigForm(SuperTable<Config> table, int mode, boolean captionmode) {
		super(table, mode, captionmode);

		createGui();
	}
	
	public ConfigForm(SuperTable<Config> table, int mode, boolean captionmode,boolean popupFlag) {
		super(table, mode, captionmode,popupFlag);

		createGui();
	}


	// Form Filed Variables
	/**
	 * nidhi
	 *  *:*:*
	 */
	/** The fieldstatus. */
	private FormField fieldname, fieldstatus, fieldDesciption, fieldGSTApplicable,fieldWholeAmtLabel,fieldDecimalLabel;

	/** The buildstatus. */
	private FormFieldBuilder buildname, buildstatus, buildDescription, buildGSTApplicable, buildWholeAmtLabel,buildDecimalLabel;
	/** 
	 *  *:*:* 
	 * nidhi
	 * 7-09-2018
	 * for map pritable short name of unit
	 */
	public TextBox tbUnitPrintable;
	
	/**
	 * @author Anil
	 * @since 28-05-2020
	 * Adding TAT(Turn around time)
	 */
	TimeFormatBox tmbTat;

	public CheckBox gstApplicable;
	
	//Ashwini Patil Date: 12-04-2022 Description:As per Nitin sir's instruction adding amount labels
	public TextBox wholeAmtLabel;
	public TextBox decimalLabel;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.
	 * FormTableScreen#toggleAppHeaderBarMenu()
	 */
	@Override
	public void toggleAppHeaderBarMenu() {
		System.out.println("toggle app header bar config form");
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
//					.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
				 System.out.println("Popup menu");
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				 System.out.println("Normal flow ==");
			}
			
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {

//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
//					.getMenuLabels();
			
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
//					.getMenuLabels();
			
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Edit") || text.contains("Discard"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}
	}

	/**
	 * Initialize widgets.
	 */
	private void initializeWidgets() {
		name = new TextBox();
		status = new CheckBox();
		tbUnitPrintable = new TextBox();
		tmbTat=new TimeFormatBox();
		gstApplicable = new CheckBox();
		gstApplicable.setValue(true); //Ashwini Patil Date:26-03-2022
		
		tbInvoiceTitle=new TextBox();
		cbPrintBankDetails=new CheckBox();
		cbPrintBankDetails.setValue(false);
		
		wholeAmtLabel=new TextBox();
		decimalLabel=new TextBox();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.
	 * FormTableScreen#createScreen()
	 */
	@Override
	public void createScreen() {
		processlevelBarNames = new String[] { "New" };
		initializeWidgets();
		buildname = new FormFieldBuilder("* Name", name);
		fieldname = buildname.setMandatory(true)
				.setMandatoryMsg("Name is mandatory!").build();

		buildstatus = new FormFieldBuilder("Status", status);
		fieldstatus = buildstatus.build();

		
		buildDescription = new FormFieldBuilder("Printable Name", tbUnitPrintable);
		fieldDesciption = buildDescription.setColSpan(4).build();
		
		buildname = new FormFieldBuilder("Invoice Title", tbInvoiceTitle);
		FormField ftbInvoiceTitle = buildname.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		buildname = new FormFieldBuilder("Print bank details on invoice", cbPrintBankDetails);
		FormField fcbPrintBankDetails = buildname.setMandatory(false).setRowSpan(0).setColSpan(0).build();

//		this.fields = new FormField[][] { { fieldname, fieldstatus },{fieldDesciption} };
		if(AppMemory.getAppMemory().currentScreen == Screen.SERVICEUOM 
				|| AppMemory.getAppMemory().currentScreen == Screen.UNITOFMEASUREMENT){
			this.fields = new FormField[][] { { fieldname, fieldstatus },{fieldDesciption} };
		}else if(AppMemory.getAppMemory().currentScreen == Screen.CLASSIFICATIONOFCITY ){
			buildDescription = new FormFieldBuilder("TAT", tmbTat.getTimePanel());
			fieldDesciption = buildDescription.setColSpan(4).build();
			this.fields = new FormField[][] { { fieldname, fieldstatus },{fieldDesciption} };
		}
		else if(AppMemory.getAppMemory().currentScreen == Screen.NUMBERRANGE){
			buildGSTApplicable = new FormFieldBuilder("Tax Applicable",gstApplicable);
			fieldGSTApplicable = buildGSTApplicable.setColSpan(0).build();
			this.fields = new FormField[][] { { fieldname, fieldstatus, fieldGSTApplicable } ,
					{ftbInvoiceTitle,fcbPrintBankDetails}};
		}
		//Ashwini Patil Date: 12-04-2022
		else if(AppMemory.getAppMemory().currentScreen == Screen.CURRENCY){
			buildWholeAmtLabel = new FormFieldBuilder("Label for Whole Amount",wholeAmtLabel);
			buildDecimalLabel=new FormFieldBuilder("Label for Decimal Amount",decimalLabel);
			fieldWholeAmtLabel = buildWholeAmtLabel.setColSpan(0).build();
			fieldDecimalLabel = buildDecimalLabel.setColSpan(0).build();
			this.fields = new FormField[][] { { fieldname,fieldWholeAmtLabel,fieldDecimalLabel, fieldstatus}};			
		}
		else{
			this.fields = new FormField[][] { { fieldname, fieldstatus }};
		}

		status.setValue(true);

		name.getElement().setId("configname");

	}

	/**
	 * There is no UI element Corresponding to type attribute of Model, it
	 * automatically fills corresponding to current Screen.
	 */
	@Override
	public void updateModel(Config model) {
		model.setName(this.name.getText());
		model.setStatus(this.status.getValue());
		/**
		 * *:*:*
		 *  nidhi
		 *  9-10-2018
		 */
		System.out.println("Unit print table::"+this.tbUnitPrintable.getValue());
		model.setDescription(this.tbUnitPrintable.getValue());
		
		/**
		 * end
		 */
		if(AppMemory.getAppMemory().currentScreen == Screen.CLASSIFICATIONOFCITY ){
			model.setDescription(this.tmbTat.getValue());
		}
		if(AppMemory.getAppMemory().currentScreen == Screen.NUMBERRANGE){
			model.setGstApplicable(this.gstApplicable.getValue());
			System.out.println("GST value"+this.gstApplicable.getValue());
		}
		//Ashwini Patil Date: 12-04-2022
		if(AppMemory.getAppMemory().currentScreen == Screen.CURRENCY){
			model.setWholeAmtLabel(this.wholeAmtLabel.getValue());
			model.setDecimalLabel(this.decimalLabel.getValue());
			System.out.println("amount labels set");
		}

		Screen screen = (Screen) AppMemory.getAppMemory().currentScreen;
		model.setType(ConfigTypes.getConfigType(screen));
		
		model.setInvoiceTitle(tbInvoiceTitle.getValue());
		model.setPrintBankDetails(cbPrintBankDetails.getValue());
		
		manageConfigDropDown(model);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.
	 * FormTableScreen#updateView(java.lang.Object)
	 */
	@Override
	public void updateView(Config view) {
		name.setText(view.getName());
		status.setValue(view.isStatus());
		/**
		 *  nidhi
		 *   9-10-2018
		 *   
		 */
		tbUnitPrintable.setValue(view.getDescription());
		/**
		 * end
		 */
		
		if(AppMemory.getAppMemory().currentScreen == Screen.CLASSIFICATIONOFCITY ){
			tmbTat.setValue(view.getDescription());
		}
		
		if(AppMemory.getAppMemory().currentScreen == Screen.NUMBERRANGE){
			gstApplicable.setValue(view.isGstApplicable());
			System.out.println("View GST Value"+view.isGstApplicable());
		}
		//Ashwini Patil Date: 12-04-2022
		if(AppMemory.getAppMemory().currentScreen == Screen.CURRENCY){
			wholeAmtLabel.setValue(view.getWholeAmtLabel());
			decimalLabel.setValue(view.getDecimalLabel());
			System.out.println("Update view whole amt label="+ view.getWholeAmtLabel());
			System.out.println("Update view decimal amt label="+ view.getDecimalLabel());
		}
		
		if(view.getInvoiceTitle()!=null){
			tbInvoiceTitle.setValue(view.getInvoiceTitle());
		}
		cbPrintBankDetails.setValue(view.isPrintBankDetails());
		
		presenter.setModel(view);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.simplesoftwares.client.library.appstructure.UiScreen#getPresenter()
	 */
	public ConfigPresenter getPresenter() {
		return presenter;
	}

	/**
	 * Sets the presenter.
	 *
	 * @param presenter
	 *            the new presenter
	 */
	public void setPresenter(ConfigPresenter presenter) {
		this.presenter = presenter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.
	 * FormTableScreen#clear()
	 */
	@Override
	public void clear() {
		super.clear();
		status.setValue(true);
		/**
		 * nidhi
		 *  *:*:*
		 */
		tbUnitPrintable.setValue("");
		tmbTat.clear();
		cbPrintBankDetails.setValue(false);
	}

	@Override
	public void applyStyle() {
		super.applyStyle();
		// form.getElement().setId("configsettingsformform");
		// content.getElement().setId("configsettingsform");
	}

	public void manageConfigDropDown(Config configModel) {
		ScreeenState scrState = AppMemory.getAppMemory().currentState;

		if (scrState.equals(ScreeenState.NEW)) {
			LoginPresenter.globalConfig.add(configModel);
		}
		if (scrState.equals(ScreeenState.EDIT)) {
			for (int i = 0; i < LoginPresenter.globalConfig.size(); i++) {
				if (LoginPresenter.globalConfig.get(i).getId()
						.equals(configModel.getId())) {
					LoginPresenter.globalConfig.add(configModel);
					LoginPresenter.globalConfig.remove(i);

					/**
					 * Date : 07-02-2017 by Anil Adding break statement to
					 * terminate loop
					 */
					break;
					/**
					 * End
					 */
				}
			}
		}
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

}
