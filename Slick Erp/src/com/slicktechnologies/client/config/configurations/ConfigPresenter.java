package com.slicktechnologies.client.config.configurations;

import com.google.gwt.event.dom.client.ClickEvent;

import java.util.List;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.*;

// TODO: Auto-generated Javadoc
/**
 *Presenter of Config Form.
 */
public class ConfigPresenter extends FormTableScreenPresenter<Config>{

	/** The form. */
	ConfigForm form;
	
	/**
	 * Instantiates a new config presenter.
	 *
	 * @param view the view
	 * @param model the model
	 */
	public ConfigPresenter(FormTableScreen<Config> view,
			Config model) {
		super(view, model);
		form=(ConfigForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getConfigQuery());
		form.setPresenter(this);
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactToProcessBarEvents(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("Delete"))
		{
			
			if(this.form.validate())
			{
			List<Config>data=form.getSupertable().getDataprovider().getList();
			/*
			 * Deleting
			 */
			for(int i=0;i<data.size();i++)
			{
				if(data.get(i).getId().equals(model.getId()))
				{
					data.remove(i);
				}
				form.getSupertable().getDataprovider().refresh();
			}
			reactOnDelete();
			
			}
		}
		if(lbl.getText().contains("New"))
		{
			ConfigTabels table=new ConfigTabels();
		  	ConfigForm form=new ConfigForm(table, FormTableScreen.UPPER_MODE, true);
		  	form.setToNewState();
		  	ConfigPresenter p=new ConfigPresenter(form, new Config());
		  	AppMemory.getAppMemory().stickPnel(form);
		  
		}
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnPrint()
	 */
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnEmail()
	 */
	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#makeNewModel()
	 */
	@Override
	protected void makeNewModel() {
	
		model=new Config();
		
	}
	
	/**
	 * Gets the config query.The type attributes comes from current screen.
	 *
	 * @return the config query
	 */
	public MyQuerry getConfigQuery()
	{
		Screen scr=(Screen) AppMemory.getAppMemory().currentScreen;
		MyQuerry quer=new MyQuerry();
		Filter typeFilter=new Filter();
		typeFilter.setQuerryString("type");
		typeFilter.setIntValue(ConfigTypes.getConfigType(scr));
		quer.getFilters().add(typeFilter);
		quer.setQuerryObject(new Config());
		return quer;
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#setModel(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	public void setModel(Config entity)
	{
		model=entity;
	}

}
