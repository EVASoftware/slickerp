package com.slicktechnologies.client.config.typeconfig;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.composite.*;
import com.simplesoftwares.client.library.mywidgets.*;
import com.simplesoftwares.client.library.libservice.*;
import com.slicktechnologies.client.config.categoryconfig.CategoryTypes;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.*;
import com.slicktechnologies.client.utils.Console;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickHandler;
/**
 * Form Type
 */
public class TypeForm extends FormTableScreen<Type> implements ChangeHandler{

	//Token to add the varialble declarations
	CheckBox cbStatus;
	TextBox tbtypeName;
	TextBox tbtypecode;
	ObjectListBox<ConfigCategory> olbCategoryName;
	TextArea taDescription;
	TextBox tbcatcode;
	
	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;



	public TypeForm  (SuperTable<Type> table, int mode,
			boolean captionmode) 
	{
		super(table, mode, captionmode);
		createGui();

	}

	private void initalizeWidget()
	{
		cbStatus=new CheckBox();
		cbStatus.setValue(true);
		tbtypeName=new TextBox();
		
		tbtypecode=new TextBox();
		tbtypecode.setEnabled(false);
		
		taDescription=new TextArea();
		tbcatcode=new TextBox();
		olbCategoryName=new ObjectListBox<ConfigCategory>();
		olbCategoryName.addChangeHandler(this);
		Screen screen=(Screen) AppMemory.getAppMemory().currentScreen;
		System.out.println("Screen name"+screen);
		AppUtility.MakeLiveCategoryConfig(olbCategoryName,screen);
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder("* Type Name",tbtypeName);
		FormField ftbCategoryName= fbuilder.setMandatory(true).setMandatoryMsg("Type Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Type Code",tbtypecode);
		FormField ftbCategoryCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Description",taDescription);
		FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Category Code",tbcatcode);
		FormField ftbcatcode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		
		fbuilder = new FormFieldBuilder("* Category",olbCategoryName);
		FormField folbcategory= fbuilder.setMandatory(true).setMandatoryMsg("Category Name is mandatory!").setRowSpan(0).setColSpan(0).build();


		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {{folbcategory,ftbCategoryCode,ftbCategoryName,fcbStatus},
				{ftaDescription},
		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Type model) 
	{

		if(olbCategoryName.getValue()!=null){
			model.setCategoryName(olbCategoryName.getValue());
		}
		if(cbStatus.getValue()!=null)
			model.setStatus(cbStatus.getValue());
		if(tbtypecode.getValue()!=null)
			model.setTypeCode(this.tbtypecode.getValue());
		if(taDescription.getValue()!=null)
			model.setDescription(taDescription.getValue());
		if(tbtypeName.getValue()!=null)
			model.setTypeName(tbtypeName.getValue());
		if(tbcatcode.getValue()!=null){
			model.setCatCode(tbcatcode.getValue());
			
			System.out.println("Cat Code"+tbcatcode.getValue());
		}
		
		Screen screen = (Screen) AppMemory.getAppMemory().currentScreen;
		model.setInternalType(CategoryTypes.getCategoryInternalType(screen));
		
		
		
		manageTypeDropDown(model);
		
		presenter.setModel(model);


	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Type view) 
	{
		if(view.getCategoryName()!=null)
			olbCategoryName.setValue(view.getCategoryName());
		if(view.getStatus()!=null)
			cbStatus.setValue(view.getStatus());
		if(view.getDescription()!=null)
			taDescription.setValue(view.getDescription());
		if(view.getTypeCode()!=null)
			tbtypecode.setValue(view.getTypeCode());
		if(view.getDescription()!=null)
			taDescription.setValue(view.getDescription());
		if(view.getTypeName()!=null)
			tbtypeName.setValue(view.getTypeName());
		if(view.getCatCode()!=null)
			tbcatcode.setValue(view.getCatCode());
		
		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

	}

	
//************************Buisness Logic Part ****************************************************************//
	
	@Override
	public void clear() {
		// TODO Auto-generated method stub
		super.clear();
		cbStatus.setValue(true);
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
		tbtypecode.setEnabled(false);
	}
	
	
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

	@Override
	public void applyStyle()
	{
		super.applyStyle();
		//form.getElement().setId("configsettingsformform");
		//content.getElement().setId("configsettingsform");
	}
	
	@Override
	public void onChange(ChangeEvent event) {
		if(this.olbCategoryName.getSelectedIndex()!=0){
			ConfigCategory catEntity=this.olbCategoryName.getSelectedItem();
			tbcatcode.setValue(catEntity.getCategoryCode().trim());
		}
		if(this.olbCategoryName.getSelectedIndex()==0){
			tbcatcode.setValue(null);
		}
	}
	
	
	public void manageTypeDropDown(Type typeModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalType.add(typeModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalType.size();i++)
			{
				if(LoginPresenter.globalType.get(i).getId().equals(typeModel.getId()))
				{
					LoginPresenter.globalType.add(typeModel);
					LoginPresenter.globalType.remove(i);
				}
			}
		}
	}


	
	
	

}

