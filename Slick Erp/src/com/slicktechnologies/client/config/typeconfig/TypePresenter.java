package com.slicktechnologies.client.config.typeconfig;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.config.categoryconfig.CategoryTypes;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
/**
 *  FormTableScreen presenter template
 */
public class TypePresenter extends FormTableScreenPresenter<Type>  {

	 //Token to set the concrete form
	static TypeForm form;
	
	public TypePresenter (FormTableScreen<Type> view,Type model) {
		super(view, model);
		form=(TypeForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(TypeQuery());
		form.setPresenter(this);
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("Delete"))
		{
			
			List<Type>data=form.getSupertable().getDataprovider().getList();
			/*
			 * Deleting
			 */
			for(int i=0;i<data.size();i++)
			{
				if(data.get(i).getId().equals(model.getId()))
				{
					data.remove(i);
				}
				form.getSupertable().getDataprovider().refresh();
			}
			reactOnDelete();
			
			
		}
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
		
	}
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new Type();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry TypeQuery()
	{
		Screen scr=(Screen) AppMemory.getAppMemory().currentScreen;
		MyQuerry quer=new MyQuerry();
		Filter typeFilter=new Filter();
		typeFilter.setQuerryString("internalType");
		typeFilter.setIntValue(CategoryTypes.getCategoryInternalType(scr));
		//Apply filter for CtegoryName
		quer.getFilters().add(typeFilter);
		quer.setQuerryObject(new Type());
		return quer;
	}
	
	public void setModel(Type entity)
	{
		model=entity;
	}
	
	public static void initalize()
	{
			 TypePresenterTable gentableScreen=new TypePresenterTableProxy();
			
			form=new  TypeForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			  //// TypePresenterTable gentableSearch=GWT.create( TypePresenterTable.class);
			  ////   TypePresenterSearch.staticSuperTable=gentableSearch;
			  ////     TypePresenterSearch searchpopup=GWT.create( TypePresenterSearch.class);
			  ////         form.setSearchpopupscreen(searchpopup);
			
			 TypePresenter  presenter=new  TypePresenter  (form,new Type());
			AppMemory.getAppMemory().stickPnel(form);	 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.helperlayer.Type")
		 public static class  TypePresenterTable extends SuperTable<Type> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.helperlayer.Type")
			 public static class  TypePresenterSearch extends SearchPopUpScreen<Type>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}}


}

