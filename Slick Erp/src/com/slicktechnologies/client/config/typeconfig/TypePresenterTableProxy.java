package com.slicktechnologies.client.config.typeconfig;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import java.util.Comparator;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;
import java.util.List;
import java.util.Date;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.client.config.typeconfig.TypePresenter.TypePresenterTable;
import com.slicktechnologies.client.utility.Screen;

public class TypePresenterTableProxy extends TypePresenterTable {
  TextColumn<Type> getTypeCodeColumn;
  TextColumn<Type> getTypeNameColumn;
  TextColumn<Type> getStatusColumn;
  TextColumn<Type> getCategoryNameColumn;
  TextColumn<Type> getCountColumn;
  public Object getVarRef(String varName)
  {
  if(varName.equals("getTypeCodeColumn"))
  return this.getTypeCodeColumn;
  if(varName.equals("getTypeNameColumn"))
  return this.getTypeNameColumn;
  if(varName.equals("getStatusColumn"))
  return this.getStatusColumn;
  if(varName.equals("getCategoryNameColumn"))
  return this.getCategoryNameColumn;
  if(varName.equals("getCountColumn"))
  return this.getCountColumn;
   return null ;
  }
  public TypePresenterTableProxy()
  {
  super();
  }
  @Override public void createTable() {
  addColumngetCount();
  addColumngetTypeName();
  addColumngetTypeCode();
  addColumngetCategoryName();
  addColumngetStatus();
  }
  @Override
  protected void initializekeyprovider() {
  keyProvider= new ProvidesKey<Type>()
  {
  @Override
  public Object getKey(Type item)
  {
  if(item==null)
  {
  return null;
  }
  else
  return item.getId();
  }
  };
  }
  @Override
  public void setEnable(boolean state)
  {
  }
  @Override
  public void applyStyle()
  {
  }
   public void addColumnSorting(){
  addSortinggetCount();
  addSortinggetCategoryName();
  addSortinggetTypeCode();
  addSortinggetTypeName();
  addSortinggetStatus();
  }
  @Override public void addFieldUpdater() {
  }
  protected void addSortinggetCount()
  {
  List<Type> list=getDataprovider().getList();
  columnSort=new ListHandler<Type>(list);
  columnSort.setComparator(getCountColumn, new Comparator<Type>()
  {
  @Override
  public int compare(Type e1,Type e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCount()== e2.getCount()){
  return 0;}
  if(e1.getCount()> e2.getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCount()
  {
  getCountColumn=new TextColumn<Type>()
  {
  @Override
  public String getValue(Type object)
  {
  if( object.getCount()==-1)
  return "N.A";
  else return object.getCount()+"";
  }
  };
  table.addColumn(getCountColumn,"Id");
  getCountColumn.setSortable(true);
  }
  protected void addSortinggetCategoryName()
  {
  List<Type> list=getDataprovider().getList();
  columnSort=new ListHandler<Type>(list);
  columnSort.setComparator(getCategoryNameColumn, new Comparator<Type>()
  {
  @Override
  public int compare(Type e1,Type e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getCategoryName()!=null && e2.getCategoryName()!=null){
  return e1.getCategoryName().compareTo(e2.getCategoryName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCategoryName()
  {
  getCategoryNameColumn=new TextColumn<Type>()
  {
  @Override
  public String getValue(Type object)
  {
  return object.getCategoryName()+"";
  }
  };
  table.addColumn(getCategoryNameColumn,"Category");
  getCategoryNameColumn.setSortable(true);
  }
  protected void addSortinggetTypeCode()
  {
  List<Type> list=getDataprovider().getList();
  columnSort=new ListHandler<Type>(list);
  columnSort.setComparator(getTypeCodeColumn, new Comparator<Type>()
  {
  @Override
  public int compare(Type e1,Type e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getTypeCode()!=null && e2.getTypeCode()!=null){
  return e1.getTypeCode().compareTo(e2.getTypeCode());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetTypeCode()
  {
  getTypeCodeColumn=new TextColumn<Type>()
  {
  @Override
  public String getValue(Type object)
  {
  return object.getTypeCode()+"";
  }
  };
  table.addColumn(getTypeCodeColumn,"Type Code");
  getTypeCodeColumn.setSortable(true);
  }
  protected void addSortinggetTypeName()
  {
  List<Type> list=getDataprovider().getList();
  columnSort=new ListHandler<Type>(list);
  columnSort.setComparator(getTypeNameColumn, new Comparator<Type>()
  {
  @Override
  public int compare(Type e1,Type e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getTypeName()!=null && e2.getTypeName()!=null){
  return e1.getTypeName().compareTo(e2.getTypeName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetTypeName()
  {
  getTypeNameColumn=new TextColumn<Type>()
  {
  @Override
  public String getValue(Type object)
  {
  return object.getTypeName()+"";
  }
  };
  table.addColumn(getTypeNameColumn,"Type Name");
  getTypeNameColumn.setSortable(true);
  }
  protected void addSortinggetStatus()
  {
  List<Type> list=getDataprovider().getList();
  columnSort=new ListHandler<Type>(list);
  columnSort.setComparator(getStatusColumn, new Comparator<Type>()
  {
  @Override
  public int compare(Type e1,Type e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getStatus()== e2.getStatus()){
  return 0;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetStatus()
  {
  getStatusColumn=new TextColumn<Type>()
  {
  @Override
  public String getValue(Type object)
  {
  if( object.getStatus()==true)
  return "Active";
  else 
  return "In Active";
  }
  };
  table.addColumn(getStatusColumn,"Type Status");
  getStatusColumn.setSortable(true);
  }
}
