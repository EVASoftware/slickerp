package com.slicktechnologies.client.config.categoryconfig;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.composite.*;
import com.simplesoftwares.client.library.mywidgets.*;
import com.simplesoftwares.client.library.libservice.*;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;
import com.google.gwt.event.dom.client.ClickHandler;
// TODO: Auto-generated Javadoc
/**
 * Generic Form for all type of Category.
 * 
 */
public class CategoryForm extends FormTableScreen<ConfigCategory>{

	//Token to add the varialble declarations
	/** The cb status. */
	CheckBox cbStatus;
	
	/** The tb category name. */
	TextBox tbCategoryName;
	
	/** The tb catcode. */
	TextBox tbCatcode;
	
	/** The ta description. */
	TextArea taDescription;
	
	/** The cat id. */
	IntegerBox catId;


	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;



	/**
	 * Instantiates a new category form.
	 *
	 * @param table the table
	 * @param mode the mode
	 * @param captionmode the captionmode
	 */
	public CategoryForm  (SuperTable<ConfigCategory> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		catId.setEnabled(false);
		cbStatus.setValue(true);

	}

	/**
	 * Initalize widget.
	 */
	private void initalizeWidget()
	{

		cbStatus=new CheckBox();
		cbStatus.setValue(true);
		tbCategoryName=new TextBox();
		tbCatcode=new TextBox();
		tbCatcode.setEnabled(false);
		taDescription=new TextArea();
		catId=new IntegerBox();

	}

	/*
	 * Method template to create the formtable screen
	 */

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#createScreen()
	 */
	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder("* Category Name",tbCategoryName);

		FormField ftbCategoryName= fbuilder.setMandatory(true).setMandatoryMsg("Category Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Category Code",tbCatcode);
		FormField ftbCategoryCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Description",taDescription);
		FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {{ftbCategoryCode,ftbCategoryName,fcbStatus},
				{ftaDescription},
		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name.
	 *
	 * @param model the model
	 */
	@Override
	public void updateModel(ConfigCategory model) 
	{

		if(tbCategoryName.getValue()!=null)
			model.setCategoryName(tbCategoryName.getValue());
		
			model.setStatus(cbStatus.getValue());
		if(tbCatcode.getValue()!=null)
			model.setCategoryCode(tbCatcode.getValue());
		if(taDescription.getValue()!=null)
			model.setDescription(taDescription.getValue());
		
		Screen screen = (Screen) AppMemory.getAppMemory().currentScreen;
		model.setInternalType(CategoryTypes.getCategoryInternalType(screen));
		
		
		manageCategoryDropDown(model);
		
		presenter.setModel(model);


	}

	/**
	 * method template to update the view with token entity name.
	 *
	 * @param view the view
	 */
	@Override
	public void updateView(ConfigCategory view) 
	{


		if(view.getCategoryName()!=null)
			tbCategoryName.setValue(view.getCategoryName());
		if(view.getStatus()!=null)
			cbStatus.setValue(view.getStatus());
		if(view.getDescription()!=null)
			taDescription.setValue(view.getDescription());
		if(view.getCategoryCode()!=null)
			tbCatcode.setValue(view.getCategoryCode());
		if(view.getDescription()!=null)
			taDescription.setValue(view.getDescription());
		catId.setValue(view.getCount());
		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state.
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

	}

	/**
	 * sets the id textbox with the passed count value.
	 *
	 * @param count the new count
	 */
	@Override
	public void setCount(int count)
	{
        catId.setValue(count);
	}

	


//************************Buisness Logic Part ****************************************************************//
	
	/* (non-Javadoc)
 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#clear()
 */
@Override
	public void clear() {
		
		super.clear();
		cbStatus.setValue(true);
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#setEnable(boolean)
	 */
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		this.catId.setEnabled(false);
		tbCatcode.setEnabled(false);
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

	public void manageCategoryDropDown(ConfigCategory categoryModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalCategory.add(categoryModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalCategory.size();i++)
			{
				if(LoginPresenter.globalCategory.get(i).getId().equals(categoryModel.getId()))
				{
					LoginPresenter.globalCategory.add(categoryModel);
					LoginPresenter.globalCategory.remove(i);
				}
			}
		}
	}

	@Override
	public boolean validate() {
		boolean flag = super.validate();
	
		/**
		 * @author Vijay Chougule Date 22-07-2020
		 * Des :- Added validation to Frequency type must define with no of days for cron job Reminders Frequency Screen
		 */
		if(AppMemory.getAppMemory().currentScreen==Screen.FREQUENCY){
			if(taDescription.getValue()==null || taDescription.getValue().equals("")){
					showDialogMessage("Please enter no of days in the description");
					return false;
			}
		}
		
		return flag;
	}
	
	
	

}

