package com.slicktechnologies.client.config.categoryconfig;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
// TODO: Auto-generated Javadoc
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;

/**
 *  Presenter for Category.
 */
public class CategoryPresenter extends FormTableScreenPresenter<ConfigCategory> implements ValueChangeHandler<String>{

	//Token to set the concrete form
	/** The form. */
	public static CategoryForm form;
	final GenricServiceAsync async=GWT.create(GenricService.class);
	
	/**
	 * Instantiates a new category presenter.
	 *
	 * @param view the view
	 * @param model the model
	 */
	public CategoryPresenter (FormTableScreen<ConfigCategory> view,ConfigCategory model) {
		super(view, model);
		form=(CategoryForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(CategoryQuery());
		form.setPresenter(this);
		form.tbCatcode.addValueChangeHandler(this);
		
	}

	/**
	 * Method template to set the processBar events.
	 *
	 * @param e the e
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("Delete"))
		{
			
			if(this.form.validate())
			{
			List<ConfigCategory>data=form.getSupertable().getDataprovider().getList();
			/*
			 * Deleting
			 */
			for(int i=0;i<data.size();i++)
			{
				if(data.get(i).getId().equals(model.getId()))
				{
					data.remove(i);
				}
				form.getSupertable().getDataprovider().refresh();
			}
			reactOnDelete();
			
			}
		}
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
	}
	
	

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnPrint()
	 */
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnEmail()
	 */
	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model.
	 */
	@Override
	protected void makeNewModel() {
		
		model=new ConfigCategory();
	}
	

	/*
	 * Method template to set Myquerry object
	 */
	/**
	 * Category query.
	 *
	 * @return the my querry
	 */
	public MyQuerry CategoryQuery()
	{
		Screen scr=(Screen) AppMemory.getAppMemory().currentScreen;
		MyQuerry quer=new MyQuerry();
		Filter typeFilter=new Filter();
		typeFilter.setQuerryString("internalType");
		typeFilter.setIntValue(CategoryTypes.getCategoryInternalType(scr));
		quer.getFilters().add(typeFilter);
		quer.setQuerryObject(new ConfigCategory());
		return quer;
	}

	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#setModel(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	public void setModel(ConfigCategory entity)
	{
		model=entity;
	}
	
	/**
	 * Initalize.
	 */
	public static void initalize()
	{
//			 CategoryPresenterTable gentableScreen=GWT.create( CategoryPresenterTable.class);
			CategoryPresenterTableProxy gentableScreen=new CategoryPresenterTableProxy();
			form=new  CategoryForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			  //// ConfigCategoryPresenterTable gentableSearch=GWT.create( ConfigCategoryPresenterTable.class);
			  ////   ConfigCategoryPresenterSearch.staticSuperTable=gentableSearch;
			  ////     ConfigCategoryPresenterSearch searchpopup=GWT.create( ConfigCategoryPresenterSearch.class);
			  ////         form.setSearchpopupscreen(searchpopup);
			
	 CategoryPresenter  presenter=new  CategoryPresenter  (form,new ConfigCategory());

			AppMemory.getAppMemory().stickPnel(form);
			
			
		 
	}
	
	
		/**
		 * The Class CategoryPresenterTable.
		 */
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.helperlayer.ConfigCategory")
		 public static class  CategoryPresenterTable extends SuperTable<ConfigCategory> implements GeneratedVariableRefrence{

			/* (non-Javadoc)
			 * @see com.slicktechnologies.client.utility.GeneratedVariableRefrence#getVarRef(java.lang.String)
			 */
			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#createTable()
			 */
			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#initializekeyprovider()
			 */
			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#addFieldUpdater()
			 */
			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#setEnable(boolean)
			 */
			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.ViewContainer#applyStyle()
			 */
			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}}


		@Override
		public void onValueChange(ValueChangeEvent<String> event) {
			if(event.getSource().equals(form.tbCatcode))
			{
				if(form.tbCatcode.getValue()!=null)
				{
					validateCatCode();
				}
				
			}
		}
		
		
		protected void validateCatCode()
		{
			MyQuerry querry=new MyQuerry();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(model.getCompanyId());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("categoryCode");
			temp.setStringValue(form.tbCatcode.getValue());
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ConfigCategory());
			
			view.showWaitSymbol();
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					view.hideWaitSymbol();
					form.showDialogMessage("An Unexpected error occurred!");
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					view.hideWaitSymbol();
					if(result.size()>0){
						form.showDialogMessage("Category Code Already Exists!");
						form.tbCatcode.setValue("");
					}
					
				}
			});
			
			
			
			
		}



}

