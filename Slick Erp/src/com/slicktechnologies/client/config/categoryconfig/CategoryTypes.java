package com.slicktechnologies.client.config.categoryconfig;

import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;

/**
 * Provides a way for differentiating between differnt types of
 * {@link ConfigCategory} and Type.
 * 
 * @author Kamala
 *
 */
public enum CategoryTypes {
	// Token to add the configTypes with integer value
	/** The customercategory. */
	CUSTOMERCATEGORY(1),
	/** The leadcategory. */
	LEADCATEGORY(2),
	/** The quotationcategory. */
	QUOTATIONCATEGORY(3),
	/** The contractcategory. */
	CONTRACTCATEGORY(4),
	/** The vendorcategory. */
	VENDORCATEGORY(5),
	/** The complaincategory. */
	COMPLAINCATEGORY(6),
	/** The expensecategory. */
	EXPENSECATEGORY(7),
	/** The billing category */
	BILLINGCATEGORY(8), 
	/** The salesquotation category */
	SALESQUOTATIONCATEGORY(9),
	/** The sales order category */
	SALESORDERCATEGORY(10),
	/** The grn category */
	GRNCATEGORY(11),
	/** The Delivery Category */
	DELIVERYNOTECATEGORY(12),
	/** The Module Name */
	MODULENAME(13),
	/** The Purchase Order Category */
	PURCHASEORDERCATEGORY(14),
	/** The Loi Group */
	LOICATEGORY(15),
	/** The RFQ Category */
	RFQCATEGORY(16),
	/** The PR Category */
	PRCATEGORY(17),
	/** The Mrn Category */
	MRNCATEGORY(18),
	/** The Min Category */
	MINCATEGORY(19),
	/** The MMN Category */
	MMNCATEGORY(20),
	/** The Ticket Category */
	TICKETCATEGORY(21),
	/** The Inspection Category */
	INSPECTIONCATEGORY(22),
	/** The Work Order Category */
	WORKORDERCATEGORY(23),
	/** The Invoice Category */
	INVOICECATEGORY(24),
	
	/** The Invoice Category */
	ADDPAYMENTTERMS(25),
	
	/** The Invoice Category */
	ASSESSMENTREPORTCATEGORY(26),
	
	/** The Service PO Category */
	SERVICEPOCATEGORY(27),

	ANDROIDTIPS(28),
	
	
	 /**
     *  nidhi
     *  22-12-2017
     *  for email configration setting ;
     */
    CRONJOB(29),
    FREQUENCY(30);
    /** end
    */
	/** The value. */
	private final int value;

	/**
	 * Instantiates a new category types.
	 *
	 * @param newValue
	 *            the new value
	 */
	private CategoryTypes(final int newValue) {
		value = newValue;
	}

	/**
	 * Returns int value of the configtype .
	 *
	 * @return value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Returns the integer value corresponding to configType Screen.
	 *
	 * @param screen
	 *            the screen
	 * @return the category internal type
	 */
	public static int getCategoryInternalType(Screen screen) {
		System.out.println("Value of Screen is " + screen);
		switch (screen) {

		case CUSTOMERCATEGORY:
			return CUSTOMERCATEGORY.getValue();
		case CONTRACTCATEGORY:
			return CONTRACTCATEGORY.getValue();
		case LEADCATEGORY:
			return LEADCATEGORY.getValue();
		case QUOTATIONCATEGORY:
			return QUOTATIONCATEGORY.getValue();
		case VENDORCATEGORY:
			return VENDORCATEGORY.getValue();
		case COMPLAINCATEGORY:
			return COMPLAINCATEGORY.getValue();
		case EXPENSECATEGORY:
			return EXPENSECATEGORY.getValue();
		case BILLINGCATEGORY:
			return BILLINGCATEGORY.getValue();
		case SALESORDERCATEGORY:
			return SALESORDERCATEGORY.getValue();
		case SALESQUOTATIONCATEGORY:
			return SALESQUOTATIONCATEGORY.getValue();
		case GRNCATEGORY:
			return GRNCATEGORY.getValue();
		case DELIVERYNOTECATEGORY:
			return DELIVERYNOTECATEGORY.getValue();
		case MODULENAME:
			return MODULENAME.getValue();
		case PURCHASEORDERCATEGORY:
			return PURCHASEORDERCATEGORY.getValue();
		case LOICATEGORY:
			return LOICATEGORY.getValue();
		case RFQCATEGORY:
			return RFQCATEGORY.getValue();
		case PRCATEGORY:
			return PRCATEGORY.getValue();
		case MRNCATEGORY:
			return MRNCATEGORY.getValue();
		case MINCATEGORY:
			return MINCATEGORY.getValue();
		case MMNCATEGORY:
			return MMNCATEGORY.getValue();
		  case TICKETCATEGORY:
    		  return TICKETCATEGORY.getValue();
		  case INSPECTIONCATEGORY:
			  return INSPECTIONCATEGORY.getValue();
		  case WORKORDERCATEGORY:
				return WORKORDERCATEGORY.getValue();
		  case INVOICECATEGORY:
				return INVOICECATEGORY.getValue();
				
		  case ASSESSMENTREPORTCATEGORY:
				return ASSESSMENTREPORTCATEGORY.getValue();
				
				
				
			// ///////////For Type
			// Screens///////////////////////////////////////////////////////////////////////////////////
		case CUSTOMERTYPE:
			return CUSTOMERCATEGORY.getValue();
		case QUOTATIONTYPE:
			return QUOTATIONCATEGORY.getValue();
		case CONTRACTTYPE:
			return CONTRACTCATEGORY.getValue();
		case LEADTYPE:
			return LEADCATEGORY.getValue();
		case VENDORTYPE:
			return VENDORCATEGORY.getValue();
		case COMPLAINTYPE:
			return COMPLAINCATEGORY.getValue();
		case EXPENSETYPE:
			return EXPENSECATEGORY.getValue();
		case BILLINGTYPE:
			return BILLINGCATEGORY.getValue();
		case SALESORDERTYPE:
			return SALESORDERCATEGORY.getValue();
		case SALESQUOTATIONTYPE:
			return SALESQUOTATIONCATEGORY.getValue();
		case DELIVERYNOTETYPE:
			return DELIVERYNOTECATEGORY.getValue();
		case DOCUMENTNAME:
			return MODULENAME.getValue();
		case GRNTYPE:
			return GRNCATEGORY.getValue();
		case PURCHASEORDERTYPE:
			return PURCHASEORDERCATEGORY.getValue();
		case RFQTYPE:
			return RFQCATEGORY.getValue();
		case LOITYPE:
			return LOICATEGORY.getValue();
		case PRTYPE:
			return PRCATEGORY.getValue();
		case MRNTYPE:
			return MRNCATEGORY.getValue();
		case MINTYPE:
			return MINCATEGORY.getValue();
		case MMNTYPE:
			return MMNCATEGORY.getValue();
		 case TICKETTYPE:
   		  return TICKETCATEGORY.getValue();
		 case INSPECTIONTYPE:
			 return INSPECTIONCATEGORY.getValue();
		 case WORKORDERTYPE:
				return WORKORDERCATEGORY.getValue();
		 case INVOICECONFIGTYPE:
				return INVOICECATEGORY.getValue();
				
		 case ADDPAYMENTTERMS:
				return ADDPAYMENTTERMS.getValue();
				
		 case ASSESSMENTREPORTDEFICIENCYTYPE:
				return ASSESSMENTREPORTCATEGORY.getValue();
				
		 case SERVICEPOCATEGORY:
				return SERVICEPOCATEGORY.getValue();

		 case ANDROIDTIPS :
			 return ANDROIDTIPS.getValue();
			 
			 /**
				 * nidhi
				 * 22-12-2017
				 * for email reminder setting 
				 */
				case CRONJOB :
					return CRONJOB.getValue();
					
				case FREQUENCY :
					return FREQUENCY.getValue();	
					/**
					 *  end
					 */
		default:
			break;

		}

		return -1;
	}

	/**
	 * Gets the category internal type on form screen.
	 *
	 * @param screen
	 *            the screen
	 * @return the category internal type on form screen
	 */
	public static int getCategoryInternalTypeOnFormScreen(Screen screen) {

		switch (screen) {
		case CUSTOMER:
			return CUSTOMERCATEGORY.getValue();
		case SALESCUSTOMER:
			return CUSTOMERCATEGORY.getValue();
		case CONTRACT:
			return CONTRACTCATEGORY.getValue();
		case LEAD:
			return LEADCATEGORY.getValue();
		case SALESLEAD:
			return LEADCATEGORY.getValue();
		case QUOTATION:
			return QUOTATIONCATEGORY.getValue();
		case VENDOR:
			return VENDORCATEGORY.getValue();
		case COMPLAIN:
			return COMPLAINCATEGORY.getValue();
		case EXPENSEMANAGMENT:
			return EXPENSECATEGORY.getValue();
		case BILLINGDETAILS:
			return BILLINGCATEGORY.getValue();
		case SALESORDER:
			return SALESORDERCATEGORY.getValue();
		case SALESQUOTATION:
			return SALESQUOTATIONCATEGORY.getValue();
		case GRN:
			return GRNCATEGORY.getValue();
		case DELIVERYNOTE:
			return DELIVERYNOTECATEGORY.getValue();
		case INTERACTION:
			return MODULENAME.getValue();
		case CONTACTPERSONDETAILS:
			return MODULENAME.getValue();
		case PURCHASEREQUISITE:
			return PRCATEGORY.getValue();
		case REQUESTFORQUOTATION:
			return RFQCATEGORY.getValue();
		case LETTEROFINTENT:
			return LOICATEGORY.getValue();
		case PURCHASEORDER:
			return PURCHASEORDERCATEGORY.getValue();
		case MATERIALREQUESTNOTE:
			return MRNCATEGORY.getValue();
		case MATERIALISSUENOTE:
			return MINCATEGORY.getValue();
		case MATERIALMOVEMENTNOTE:
			return MMNCATEGORY.getValue();
		 case RAISETICKET:
   		  return TICKETCATEGORY.getValue();
		 case USERAUTHORIZE:
			 return MODULENAME.getValue();
		 case DOCUMENTHISTORY:
			 return MODULENAME.getValue();
		 case INTERACTIONLIST:
			 return MODULENAME.getValue();
		 case INTERFACETALLYLIST:
			 return MODULENAME.getValue();
		 case INSPECTION:
			 return INSPECTIONCATEGORY.getValue();
		 case WORKORDER:
			 return WORKORDERCATEGORY.getValue();
		 case INVOICE:
			 return INVOICECATEGORY.getValue();

		 case ADDPAYMENTTERMS:
				return ADDPAYMENTTERMS.getValue();
				
		 case ASSESSMENTREPORTCATEGORY:
				return ASSESSMENTREPORTCATEGORY.getValue();	 
			 
		default:
			break;

		}

		return -1;
	}

}