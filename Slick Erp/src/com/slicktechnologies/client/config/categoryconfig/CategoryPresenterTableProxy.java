package com.slicktechnologies.client.config.categoryconfig;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;

public class CategoryPresenterTableProxy extends SuperTable<ConfigCategory> {

	TextColumn<ConfigCategory> getConfigCategoryIdCol;
	TextColumn<ConfigCategory> getConfigCategoryNameCol;
	TextColumn<ConfigCategory> getConfigCategoryCodeCol;
	TextColumn<ConfigCategory> getConfigCategoryStatusCol;
	
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		getConfigCategoryIdCol();
		getConfigCategoryCodeCol();
		getConfigCategoryNameCol();
		getConfigCategoryStatusCol();
		
	}

	
	
	private void getConfigCategoryIdCol() {
		// TODO Auto-generated method stub
		getConfigCategoryIdCol=new TextColumn<ConfigCategory>() {
			@Override
			public String getValue(ConfigCategory object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getConfigCategoryIdCol,"Id");
		table.setColumnWidth(getConfigCategoryIdCol,90, Unit.PX);
		getConfigCategoryIdCol.setSortable(true);
	}

	private void getConfigCategoryNameCol() {
		// TODO Auto-generated method stub
		getConfigCategoryNameCol=new TextColumn<ConfigCategory>() {
			@Override
			public String getValue(ConfigCategory object) {
				if(object.getCategoryName()!=null){
					return object.getCategoryName();
				}
				return "";
			}
		};
		table.addColumn(getConfigCategoryNameCol,"Category Name");
		table.setColumnWidth(getConfigCategoryNameCol,110, Unit.PX);
		getConfigCategoryNameCol.setSortable(true);
		
	}



	private void getConfigCategoryCodeCol() {
		// TODO Auto-generated method stub
		getConfigCategoryCodeCol=new TextColumn<ConfigCategory>() {
			@Override
			public String getValue(ConfigCategory object) {
				if(object.getCategoryCode()!=null){
					return object.getCategoryCode();
				}
				return "";
			}
		};
		table.addColumn(getConfigCategoryCodeCol,"Category Code");
		table.setColumnWidth(getConfigCategoryCodeCol,90, Unit.PX);
		getConfigCategoryCodeCol.setSortable(true);
	}



	private void getConfigCategoryStatusCol() {
		// TODO Auto-generated method stub
		getConfigCategoryStatusCol=new TextColumn<ConfigCategory>() {
			@Override
			public String getValue(ConfigCategory object) {
				if(object.getStatus()==true){
					return "Active";
				}
				return "Inactive";
			}
		};
		table.addColumn(getConfigCategoryStatusCol,"Status");
		table.setColumnWidth(getConfigCategoryStatusCol,110, Unit.PX);
		getConfigCategoryStatusCol.setSortable(true);
	}



	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		addSortingOnConfigCategoryIdCol();
		addSortingOnConfigCategoryCodeCol();
		addSortingOnConfigCategoryNameCol();
		addSortingOnConfigCategoryStatusCol();
	}



	private void addSortingOnConfigCategoryIdCol() {
		List<ConfigCategory> list=getDataprovider().getList();
		columnSort=new ListHandler<ConfigCategory>(list);
		columnSort.setComparator(getConfigCategoryIdCol, new Comparator<ConfigCategory>()
		{
			@Override
			public int compare(ConfigCategory e1,ConfigCategory e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnConfigCategoryCodeCol() {
		List<ConfigCategory> list=getDataprovider().getList();
		columnSort=new ListHandler<ConfigCategory>(list);
		columnSort.setComparator(getConfigCategoryCodeCol, new Comparator<ConfigCategory>()
		{
			@Override
			public int compare(ConfigCategory e1,ConfigCategory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCategoryCode()!=null && e2.getCategoryCode()!=null){
						return e1.getCategoryCode().compareTo(e2.getCategoryCode());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnConfigCategoryNameCol() {
		List<ConfigCategory> list=getDataprovider().getList();
		columnSort=new ListHandler<ConfigCategory>(list);
		columnSort.setComparator(getConfigCategoryNameCol, new Comparator<ConfigCategory>()
		{
			@Override
			public int compare(ConfigCategory e1,ConfigCategory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCategoryName()!=null && e2.getCategoryName()!=null){
						return e1.getCategoryName().compareTo(e2.getCategoryName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnConfigCategoryStatusCol() {
		List<ConfigCategory> list=getDataprovider().getList();
		columnSort=new ListHandler<ConfigCategory>(list);
		columnSort.setComparator(getConfigCategoryStatusCol, new Comparator<ConfigCategory>()
		{
			@Override
			public int compare(ConfigCategory e1,ConfigCategory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
