package com.slicktechnologies.client.config.adress.state;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.mywidgets.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
// TODO: Auto-generated Javadoc
/**
 * Generic Form for all type of Category.
 * 
 */
public class StateForm extends FormTableScreen<State>{

	
	
	/** The tb category name. */
	TextBox tbStateName;
	ObjectListBox<Country>olbCountry;
	CheckBox status;
	
	/**
	 * Date : 01-07-2017 BY ANIL
	 * this field stores the code of state , applicable after GST implementation
	 */
	TextBox tbStateCode;
	
	/**
	 * End
	 */
	
	public StateForm  (SuperTable<State> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		status.setValue(true);
	}

	/**
	 * Initalize widget.
	 */
	private void initalizeWidget()
	{

		tbStateName=new TextBox();
		olbCountry=new ObjectListBox<Country>();
		status=new CheckBox();
		Country.makeOjbectListBoxLive(olbCountry);
		
		tbStateCode=new TextBox();
		

	}

	/*
	 * Method template to create the formtable screen
	 */

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#createScreen()
	 */
	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames=new String[]{"New","UpdateStateCode"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder("* State Name",tbStateName);

		FormField ftbCategoryName= fbuilder.setMandatory(true).setMandatoryMsg("State Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Country",olbCountry);
		FormField ftbCategoryCode= fbuilder.setMandatory(true).setMandatoryMsg("Country is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",status);
		FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("State Code",tbStateCode);
		FormField ftbStatusCode= fbuilder.setMandatory(false).setMandatoryMsg("State Code is mandatory!").setRowSpan(0).setColSpan(0).build();
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {{ftbCategoryCode,ftbCategoryName,ftbStatusCode,fcbStatus},
				
		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name.
	 *
	 * @param model the model
	 */
	@Override
	public void updateModel(State model) 
	{

		if(tbStateName.getValue()!=null)
			model.setStateName(tbStateName.getValue());
		
			model.setStatus(status.getValue());
		if(olbCountry.getValue()!=null)
			model.setCountry(olbCountry.getValue());
		
		if(tbStateCode.getValue()!=null){
			model.setStateCode(tbStateCode.getValue());
		}
		
		manageStateDropDown(model);
		
		presenter.setModel(model);


	}

	/**
	 * method template to update the view with token entity name.
	 *
	 * @param view the view
	 */
	@Override
	public void updateView(State view) 
	{


	    if(view.getStateName().trim()!=null)
	    	tbStateName.setValue(view.getStateName());
	    if(view.getCountry()!=null)
	    	olbCountry.setValue(view.getCountry());
	  status.setValue(view.isStatus());
	  
	  if(view.getStateCode()!=null){
		  tbStateCode.setValue(view.getStateCode());
	  }
		
		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state.
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

	}


	


//************************Buisness Logic Part ****************************************************************//
	
	/* (non-Javadoc)
 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#clear()
 */
@Override
	public void clear() {
		
		super.clear();
		status.setValue(true);
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#setEnable(boolean)
	 */
	@Override
	public void setEnable(boolean state) {
		
		super.setEnable(state);
		
	}
	
	public void manageStateDropDown(State stateModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalState.add(stateModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalState.size();i++)
			{
				if(LoginPresenter.globalState.get(i).getId().equals(stateModel.getId()))
				{
					LoginPresenter.globalState.add(stateModel);
					LoginPresenter.globalState.remove(i);
				}
			}
		}
	}
	
	

}

