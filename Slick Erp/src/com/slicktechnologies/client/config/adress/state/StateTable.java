package com.slicktechnologies.client.config.adress.state;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.client.config.categoryconfig.CategoryPresenter.CategoryPresenterTable;
import com.slicktechnologies.client.utility.Screen;

public class StateTable extends SuperTable<State> {
  TextColumn<State> stateName;
  TextColumn<State> countryName;
  TextColumn<State> status;
 
  
  public StateTable()
  {
  super();
  }
  @Override public void createTable() {

  addColumnStateName();
  addColumnCountry();
  addColumngetStatus();
  }
  @Override
  protected void initializekeyprovider() {
  keyProvider= new ProvidesKey<State>()
  {
  @Override
  public Object getKey(State item)
  {
  if(item==null)
  {
  return null;
  }
  else
  return item.getId();
  }
  };
  }
  @Override
  public void setEnable(boolean state)
  {
  }
  @Override
  public void applyStyle()
  {
  }
   public void addColumnSorting(){
 
  addSortingStateName();
  addSortingCountry();
  addSortinggetStatus();
  }
  @Override public void addFieldUpdater() {
  }
  
  protected void addSortingStateName()
  {
  List<State> list=getDataprovider().getList();
  columnSort=new ListHandler<State>(list);
  columnSort.setComparator(stateName, new Comparator<State>()
  {
  @Override
  public int compare(State e1,State e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getStateName()!=null && e2.getStateName()!=null){
  return e1.getStateName().compareTo(e2.getStateName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumnStateName()
  {
  stateName=new TextColumn<State>()
  {
  @Override
  public String getValue(State object)
  {
  return object.getStateName()+"";
  }
  };
  table.addColumn(stateName,"State");
  stateName.setSortable(true);
  }
  protected void addSortingCountry()
  {
  List<State> list=getDataprovider().getList();
  columnSort=new ListHandler<State>(list);
  columnSort.setComparator(countryName, new Comparator<State>()
  {
  @Override
  public int compare(State e1,State e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getStateName()!=null && e2.getStateName()!=null){
  return e1.getCountry().compareTo(e2.getCountry());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumnCountry()
  {
  countryName=new TextColumn<State>()
  {
  @Override
  public String getValue(State object)
  {
  return object.getCountry();
  }
  };
  table.addColumn(countryName,"Country");
  countryName.setSortable(true);
  }
  protected void addSortinggetStatus()
  {
  List<State> list=getDataprovider().getList();
  columnSort=new ListHandler<State>(list);
  columnSort.setComparator(status, new Comparator<State>()
  {
  @Override
  public int compare(State e1,State e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getStatus()== e2.getStatus()){
  return 0;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetStatus()
  {
  status=new TextColumn<State>()
  {
  @Override
  public String getValue(State object)
  {
  if( object.getStatus()==true)
  return "Active";
  else 
  return "In Active";
  }
  };
  table.addColumn(status,"Status");
  status.setSortable(true);
  }
}
