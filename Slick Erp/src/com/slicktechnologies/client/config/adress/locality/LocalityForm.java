package com.slicktechnologies.client.config.adress.locality;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.mywidgets.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
// TODO: Auto-generated Javadoc
/**
 * Generic Form for all type of Category.
 * 
 */
public class LocalityForm extends FormTableScreen<Locality>{

	
	
	/** The tb category name. */
	TextBox tbLocality;
	ObjectListBox<City>olbCity;
	CheckBox status;
	
	/**
	 * 25-09-2017
	 * sagar sore
	*/
	LongBox tbLocalityPin;
	
	public LocalityForm  (SuperTable<Locality> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		status.setValue(true);
	}
	
	public LocalityForm  (SuperTable<Locality> table, int mode,
			boolean captionmode, boolean popupFlag) {
		super(table, mode, captionmode,popupFlag);
		createGui();
		status.setValue(true);
	}

	/**
	 * Initalize widget.
	 */
	private void initalizeWidget()
	{

		tbLocality=new TextBox();
		olbCity=new ObjectListBox<City>();
		status=new CheckBox();
		City.makeOjbectListBoxLive(olbCity);
		tbLocalityPin=new LongBox();
		

	}

	/*
	 * Method template to create the formtable screen
	 */

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#createScreen()
	 */
	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder("* Locality Name",tbLocality);

		FormField ftbCategoryName= fbuilder.setMandatory(true).setMandatoryMsg("Locality  is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* City",olbCity);
		FormField ftbCategoryCode= fbuilder.setMandatory(true).setMandatoryMsg("City is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",status);
		FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/** Date 25-09-2017  added by sagar sore ***/
		fbuilder = new FormFieldBuilder("Pincode",tbLocalityPin);
		FormField ftbLocalityPin= fbuilder.setMandatory(false).setMandatoryMsg("Pincode is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {{ftbCategoryCode,ftbCategoryName,ftbLocalityPin,fcbStatus},
				
		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name.
	 *
	 * @param model the model
	 */
	@Override
	public void updateModel(Locality model) 
	{

		if(tbLocality.getValue()!=null)
			model.setLocality(tbLocality.getValue());
		
			model.setStatus(status.getValue());
		if(olbCity.getValue()!=null)
			model.setCityName(olbCity.getValue());
		
		manageLocalityDropDown(model);		
		
		/*
		 * 25-09-2017
		 * sagar sore
		*/
		if(tbLocalityPin.getValue()!=null)
			model.setPinCode(tbLocalityPin.getValue());
		/*
		 * end
		*/
		
		presenter.setModel(model);


	}

	/**
	 * method template to update the view with token entity name.
	 *
	 * @param view the view
	 */
	@Override
	public void updateView(Locality view) 
	{


	    if(view.getLocality().trim()!=null)
	    	tbLocality.setValue(view.getLocality());
	    if(view.getCityName()!=null)
	    	olbCity.setValue(view.getCityName());
	  status.setValue(view.isStatus());
	  
	  /**
		 * 25-09-2017
		 * sagar sore
		*/
		
			tbLocalityPin.setValue(view.getPinCode());
		/*
		 * end
		*/
		
		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state.
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

	}


	


//************************Buisness Logic Part ****************************************************************//
	
	/* (non-Javadoc)
 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#clear()
 */
@Override
	public void clear() {
		
		super.clear();
		status.setValue(true);
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#setEnable(boolean)
	 */
	@Override
	public void setEnable(boolean state) {
		
		super.setEnable(state);
		
	}
	
	
	public void manageLocalityDropDown(Locality localityModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalLocality.add(localityModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalLocality.size();i++)
			{
				if(LoginPresenter.globalLocality.get(i).getId().equals(localityModel.getId()))
				{
					LoginPresenter.globalLocality.add(localityModel);
					LoginPresenter.globalLocality.remove(i);
				}
			}
		}
	}
	

}

