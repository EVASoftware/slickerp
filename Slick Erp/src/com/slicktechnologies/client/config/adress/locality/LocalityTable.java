package com.slicktechnologies.client.config.adress.locality;



import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.Locality;

public class LocalityTable extends SuperTable<Locality> 
{
	
	TextColumn<Locality> locality;
	TextColumn<Locality> city;
	TextColumn<Locality> status;
	
	/**
	 * 22-09-2017
	 * sagar sore
	 */
	TextColumn<Locality> pincode;
	
	GenricServiceAsync service=GWT.create(GenricService.class);
	
	
	public LocalityTable()
	{
		super();
		//addNameSorting();
		
		
	}
	
	public LocalityTable(UiScreen<Locality> view)
	{
		
		super(view);
		
	}
	
	private void createColumnName()
	{
	  locality =  new TextColumn<Locality>()
  		  {
       @Override
       public String getValue(Locality object) 
       {
          return object.getLocality();
       }
    };
    table.addColumn(locality, "Name");
	  locality.setSortable(true);
   }

private void createColumnStatus()
	{
	  status = new TextColumn<Locality>() {
        @Override
        public String getValue(Locality object) {
           
       	
        	if(object.isStatus()==true)
           	return AppUtility.ACTIVE;
           else
           	return AppUtility.INACTIVE;
        }
     };
     
     
     table.addColumn(status, "Status");
     status.setSortable(true);
	}

private void createColumnCountry()
{
  city =  new TextColumn<Locality>()
		  {
   @Override
   public String getValue(Locality object) 
   {
      return object.getCityName();
   }
};
table.addColumn(city, "City");
  city.setSortable(true);
}


	
 public void createTable()
	{
		 
		   createColumnName();
		  
		   createColumnCountry();
		   createColumnPincode();
		   createColumnStatus();
		   table.setWidth("100%");
		  
		   
		  
	}
 

	 /**
	 * 25-09-2017
	 * sagar sore
	 */
	private void createColumnPincode()
		{
		  pincode =  new TextColumn<Locality>()
			{
		   @Override
		   public String getValue(Locality object) 
		   {
		      return object.getPinCode()+"";
		   }
		};
		table.addColumn(pincode, "Pincode");
		}

@Override
protected void initializekeyprovider() {
	keyProvider = new ProvidesKey<Locality>() {
		@Override
		public Object getKey(Locality item) {
			if(item==null)
				return null;
			else
				return item.getId();
		}
	};
	
 }

//Since config table live status depends upon the type hence querry Structure will be differnt



public void addColumnSorting()
{
	
	addNameSorting();
	addStatusSorting();
	addCitySorting();
	
}


public void addNameSorting()
{
	List<Locality> list=getDataprovider().getList();
	columnSort=new ListHandler<Locality>(list);
	columnSort.setComparator(locality, new Comparator<Locality>()
			{
				
				@Override
				public int compare(Locality e1, Locality e2) 
				{
					System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.");
					if(e1!=null && e2!=null)
					{
						if(e1.getLocality()!=null && e2.getLocality()!=null)
							return e1.getLocality().compareTo(e2.getLocality());
						return 0;
					}
					else
						return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}

public void addCitySorting()
{
	List<Locality> list=getDataprovider().getList();
	columnSort=new ListHandler<Locality>(list);
	columnSort.setComparator(city, new Comparator<Locality>()
			{
				
				@Override
				public int compare(Locality e1, Locality e2) 
				{
					System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.");
					if(e1!=null && e2!=null)
					{
						if(e1.getCityName()!=null && e2.getCityName()!=null)
							return e1.getCityName().compareTo(e2.getCityName());
						return 0;
					}
					else
						return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}

public void addStatusSorting()
{
	List<Locality> list=getDataprovider().getList();
	columnSort=new ListHandler<Locality>(list);
	columnSort.setComparator(status, new Comparator<Locality>()
			{
				
				@Override
				public int compare(Locality e1, Locality e2) 
				{
					if(e1!=null && e2!=null)
					{
						if(e1.isStatus()==e2.isStatus())
							
						return 0;
						else if(e1.isStatus()==true)
							return 1;
						else
							return -1;
					}
					else
						return -1;
				}
			});
	table.addColumnSortHandler(columnSort);
}

@Override
public void setEnable(boolean state) {
	
	
}

public void applyStyle() {
}

@Override
public void addFieldUpdater() {
	
}

}
 
	
	
	

