package com.slicktechnologies.client.config.adress.city;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.mywidgets.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.complain.TimeFormatBox;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
// TODO: Auto-generated Javadoc
/**
 * Generic Form for all type of Category.
 * 
 */
public class CityForm extends FormTableScreen<City> implements ChangeHandler{

	
	
	/** The tb category name. */
	TextBox tbCity;
	ObjectListBox<State>olbState;
	CheckBox status;
	
	/**
	 * @author Anil , Date : 22-08-2019
	 */
	ObjectListBox<Config> oblClassificationOfCity;
	
	/**
	 * @author Anil
	 * @since 28-05-2020
	 * adding tat
	 */
	TimeFormatBox tmbTat;
	
	public CityForm  (SuperTable<City> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		status.setValue(true);
	}
	
	public CityForm  (SuperTable<City> table, int mode,
			boolean captionmode, boolean popupFlag) {
		super(table, mode, captionmode,popupFlag);
		createGui();
		status.setValue(true);
	}

	/**
	 * Initalize widget.
	 */
	private void initalizeWidget()
	{

		tbCity=new TextBox();
		olbState=new ObjectListBox<State>();
		status=new CheckBox();
		State.makeOjbectListBoxLive(olbState);
		
		oblClassificationOfCity=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblClassificationOfCity,Screen.CLASSIFICATIONOFCITY);
		
		oblClassificationOfCity.addChangeHandler(this);
		tmbTat=new TimeFormatBox();
	}

	/*
	 * Method template to create the formtable screen
	 */

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#createScreen()
	 */
	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder("* City Name",tbCity);

		FormField ftbCategoryName= fbuilder.setMandatory(true).setMandatoryMsg("City  is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* State",olbState);
		FormField ftbCategoryCode= fbuilder.setMandatory(true).setMandatoryMsg("State is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",status);
		FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Classification Of City",oblClassificationOfCity);
		FormField foblClassificationOfCity= fbuilder.setMandatory(false).setMandatoryMsg("Currency is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("TAT",tmbTat.getTimePanel());
		FormField ftmbTat= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {{ftbCategoryCode,ftbCategoryName,foblClassificationOfCity,ftmbTat,fcbStatus},
				
		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name.
	 *
	 * @param model the model
	 */
	@Override
	public void updateModel(City model) 
	{

		if(tbCity.getValue()!=null)
			model.setCityName(tbCity.getValue());
		
			model.setStatus(status.getValue());
		if(olbState.getValue()!=null)
			model.setState(olbState.getValue());
		if(oblClassificationOfCity.getValue()!=null){
			model.setClassName(oblClassificationOfCity.getValue());
		}else{
			model.setClassName("");
		}
		
		if(tmbTat.getValue()!=null){
			model.setTat(tmbTat.getValue());
		}else{
			model.setTat(null);
		}
		manageCityDropDown(model);
		
		
		presenter.setModel(model);


	}

	/**
	 * method template to update the view with token entity name.
	 *
	 * @param view the view
	 */
	@Override
	public void updateView(City view) 
	{


	    if(view.getCityName().trim()!=null)
	    	tbCity.setValue(view.getCityName());
	    if(view.getStateName()!=null)
	    	olbState.setValue(view.getStateName());
	  status.setValue(view.isStatus());
	  
	  if(view.getClassName()!=null){
		  oblClassificationOfCity.setValue(view.getClassName());
	  }
	  
	  if(view.getTat()!=null){
		  tmbTat.setValue(view.getTat());
	  }
		
		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state.
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

	}


	


//************************Buisness Logic Part ****************************************************************//
	
	/* (non-Javadoc)
 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#clear()
 */
@Override
	public void clear() {
		
		super.clear();
		status.setValue(true);
		tmbTat.clear();
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#setEnable(boolean)
	 */
	@Override
	public void setEnable(boolean state) {
		
		super.setEnable(state);
		tmbTat.setEnabled(state);
	}
	
	
	public void manageCityDropDown(City cityModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalCity.add(cityModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalCity.size();i++)
			{
				if(LoginPresenter.globalCity.get(i).getId().equals(cityModel.getId()))
				{
					LoginPresenter.globalCity.add(cityModel);
					LoginPresenter.globalCity.remove(i);
				}
			}
		}
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(oblClassificationOfCity)){
			if(oblClassificationOfCity.getSelectedIndex()!=0){
				Config tier=oblClassificationOfCity.getSelectedItem();
				if(tier.getDescription()!=null){
					tmbTat.setValue(tier.getDescription());
				}
			}else{
				tmbTat.setValue(null);
			}
		}
	}
	
	

}

