package com.slicktechnologies.client.config.adress.city;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;

import java.util.List;
import java.util.Vector;

import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.slicktechnologies.shared.common.helperlayer.*;
// TODO: Auto-generated Javadoc

/**
 *  Presenter for Category.
 */
public class CityPresenter extends FormTableScreenPresenter<City>{

	//Token to set the concrete form
	/** The form. */
	public static CityForm form;
	
	/**
	 * Instantiates a new category presenter.
	 *
	 * @param view the view
	 * @param model the model
	 */
	public CityPresenter (FormTableScreen<City> view,City model) {
		super(view, model);
		form=(CityForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(CategoryQuery());
		form.setPresenter(this);
		
	}

	/**
	 * Method template to set the processBar events.
	 *
	 * @param e the e
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("Delete"))
		{
			
			if(this.form.validate())
			{
			List<City>data=form.getSupertable().getDataprovider().getList();
			/*
			 * Deleting
			 */
			for(int i=0;i<data.size();i++)
			{
				if(data.get(i).getId().equals(model.getId()))
				{
					data.remove(i);
				}
				form.getSupertable().getDataprovider().refresh();
			}
			reactOnDelete();
			
			}
		}
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
	}
	
	

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnPrint()
	 */
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnEmail()
	 */
	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model.
	 */
	@Override
	protected void makeNewModel() {
		
		model=new City();
	}
	

	/*
	 * Method template to set Myquerry object
	 */
	/**
	 * Category query.
	 *
	 * @return the my querry
	 */
	public MyQuerry CategoryQuery()
	{
		
		MyQuerry quer=new MyQuerry();
		Filter typeFilter=new Filter();
		typeFilter.setQuerryString("status");
		typeFilter.setBooleanvalue(true);
		//quer.getFilters().add(typeFilter);
		quer.setQuerryObject(new City());
		return quer;
	}

	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#setModel(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	public void setModel(City entity)
	{
		model=entity;
	}
	
	/**
	 * Initalize.
	 */
	public static void initalize()
	{
			 CityTable gentableScreen=new CityTable();
			
			form=new  CityForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			CityPresenter presenter=new CityPresenter(form,new City());
			
	

			AppMemory.getAppMemory().stickPnel(form);
			
			
		 
	}
	
	
		/**
		 * The Class CategoryPresenterTable.
		 */
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.helperlayer.State")
		 public static class  CategoryPresenterTable extends SuperTable<State> implements GeneratedVariableRefrence{

			/* (non-Javadoc)
			 * @see com.slicktechnologies.client.utility.GeneratedVariableRefrence#getVarRef(java.lang.String)
			 */
			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#createTable()
			 */
			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#initializekeyprovider()
			 */
			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#addFieldUpdater()
			 */
			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#setEnable(boolean)
			 */
			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.ViewContainer#applyStyle()
			 */
			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			

		/*	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.helperlayer.ConfigCategory")
			 public static class  ConfigCategoryPresenterSearch extends SearchPopUpScreen<ConfigCategory>{


				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}};*/

}

