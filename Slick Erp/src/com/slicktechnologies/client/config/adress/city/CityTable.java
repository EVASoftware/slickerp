package com.slicktechnologies.client.config.adress.city;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.client.config.categoryconfig.CategoryPresenter.CategoryPresenterTable;
import com.slicktechnologies.client.utility.Screen;

public class CityTable extends SuperTable<City> {
  TextColumn<City> cityName;
  TextColumn<City> statename;
  TextColumn<City> status;
 /**Date 7-8-2020 by Amol added classification 
  * column raised by Rahul Tiwari**/
  TextColumn<City> classification;
  
  public CityTable()
  {
  super();
  }
  @Override public void createTable() {

  addColumnCity();
  addColumnState();
  addColumnClassification();
  addColumngetStatus();
  }
  @Override
  protected void initializekeyprovider() {
  keyProvider= new ProvidesKey<City>()
  {
  @Override
  public Object getKey(City item)
  {
  if(item==null)
  {
  return null;
  }
  else
  return item.getId();
  }
  };
  }
  @Override
  public void setEnable(boolean state)
  {
  }
  @Override
  public void applyStyle()
  {
  }
   public void addColumnSorting(){
 
  addSortingCity();
  addSortingState();
  addSortingClassification();
  addSortinggetStatus();
  }
  private void addSortingClassification() {
	  List<City> list=getDataprovider().getList();
	  columnSort=new ListHandler<City>(list);
	  columnSort.setComparator(classification, new Comparator<City>()
	  {
	  @Override
	  public int compare(City e1,City e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getClassName()!=null && e2.getClassName()!=null){
	  return e1.getClassName().compareTo(e2.getClassName());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  }
  
  private void addColumnClassification() {
		classification = new TextColumn<City>() {
			@Override
			public String getValue(City object) {
				return object.getClassName();
			}
		};
		table.addColumn(classification, "Classification");
		classification.setSortable(true);
	}
  
  
@Override public void addFieldUpdater() {
  }
  
  protected void addSortingCity()
  {
  List<City> list=getDataprovider().getList();
  columnSort=new ListHandler<City>(list);
  columnSort.setComparator(cityName, new Comparator<City>()
  {
  @Override
  public int compare(City e1,City e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getCityName()!=null && e2.getCityName()!=null){
  return e1.getCityName().compareTo(e2.getCityName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  
  
  protected void addColumnCity()
  {
  cityName=new TextColumn<City>()
  {
  @Override
  public String getValue(City object)
  {
  return object.getCityName()+"";
  }
  };
  table.addColumn(cityName,"City");
  cityName.setSortable(true);
  }
  
  
  
  protected void addSortingState()
  {
  List<City> list=getDataprovider().getList();
  columnSort=new ListHandler<City>(list);
  columnSort.setComparator(statename, new Comparator<City>()
  {
  @Override
  public int compare(City e1,City e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getStateName()!=null && e2.getStateName()!=null){
  return e1.getCityName().compareTo(e2.getCityName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  
  protected void addColumnState()
  {
  statename=new TextColumn<City>()
  {
  @Override
  public String getValue(City object)
  {
  return object.getStateName();
  }
  };
  table.addColumn(statename,"State");
  statename.setSortable(true);
  }
  protected void addSortinggetStatus()
  {
  List<City> list=getDataprovider().getList();
  columnSort=new ListHandler<City>(list);
  columnSort.setComparator(status, new Comparator<City>()
  {
  @Override
  public int compare(City e1,City e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.isStatus()== e2.isStatus()){
  return 0;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetStatus()
  {
  status=new TextColumn<City>()
  {
  @Override
  public String getValue(City object)
  {
  if( object.isStatus()==true)
  return "Active";
  else 
  return "In Active";
  }
  };
  table.addColumn(status,"Status");
  status.setSortable(true);
  }
}
