package com.slicktechnologies.client.registration;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperTable;

import java.util.Vector;

import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.client.registration.RegistrationPresenter.CompanyPresenterSearch;
import com.simplesoftwares.client.library.composite.CommonSearchComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.client.utility.AppUtility;
import com.google.gwt.user.client.ui.CheckBox;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utility.AppConstants;

public class CompanyPresenterSearchProxy extends CompanyPresenterSearch {
  public IntegerBox noOfUsers;
  public DateComparator dateComparator;
  public ListBox lbStatus;
  public TextBox companyname;
  public Object getVarRef(String varName)
  {
  if(varName.equals("noOfUsers"))
  return this.noOfUsers;
  if(varName.equals("dateComparator"))
  return this.dateComparator;
  if(varName.equals("lbStatus"))
  return this.lbStatus;
  if(varName.equals("companyname"))
  return this.companyname;
  return null ;
  
  }

  public CompanyPresenterSearchProxy()
  {
  super();
  createGui();
  }
  public void initWidget()
  {
  noOfUsers= new IntegerBox();
  dateComparator= new DateComparator("licenseStartDate",new Company());
  lbStatus= new ListBox();
  AppUtility.setStatusListBox(lbStatus,Company.getStatusList());
  companyname= new TextBox();
  }
  public void createScreen()
  {
  initWidget();
  FormFieldBuilder builder;
  builder = new FormFieldBuilder("No. of Users",noOfUsers);
  FormField fnoOfUsers= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
  builder = new FormFieldBuilder("License Start Date From",dateComparator.getFromDate());
  FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
  builder = new FormFieldBuilder("License Start Date UpTo",dateComparator.getToDate());
  FormField ftodateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
  builder = new FormFieldBuilder("Status",lbStatus);
  FormField flbStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
  builder = new FormFieldBuilder("Company Name",companyname);
  FormField fcompanyname= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
  this.fields=new FormField[][]{
  {fdateComparator,ftodateComparator,flbStatus},
  {fcompanyname,fnoOfUsers}
  };
  }
  public MyQuerry getQuerry()
  {
  Vector<Filter> filtervec=new Vector<Filter>();
  Filter temp=null;
  if(dateComparator.getValue()!=null)
  {
  filtervec.addAll(dateComparator.getValue());
  }
  if(lbStatus.getSelectedIndex()!=0){
  temp=new Filter();temp.setStringValue(lbStatus.getItemText(lbStatus.getSelectedIndex()).trim());
  temp.setQuerryString("status");
  filtervec.add(temp);
  }
  if(companyname.getValue().trim().equals("")==false){
  temp=new Filter();
  temp.setStringValue(companyname.getValue().trim());
  filtervec.add(temp);
  temp.setQuerryString("buisnessUnitName");
  filtervec.add(temp);
  }
  if(noOfUsers.getValue()!=null){
  temp=new Filter();temp.setIntValue(noOfUsers.getValue());
  temp.setQuerryString("noOfUser");
  filtervec.add(temp);
  }
  MyQuerry querry= new MyQuerry();
  querry.setFilters(filtervec);
  querry.setQuerryObject(new Company());
  return querry;
  }
}
