package com.slicktechnologies.client.registration;
import java.util.ArrayList;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.services.CompanyRegistrationService;
import com.slicktechnologies.client.services.CompanyRegistrationServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
// TODO: Auto-generated Javadoc
/**
 * Company Registartion Presenter
 */
public class RegistrationPresenter extends FormScreenPresenter<Company>{

	//Token to set the concrete FormScreen class name
	/** The form. */
	RegistartionForm form;
	
	/** The async. */
	 CompanyRegistrationServiceAsync register=GWT.create(CompanyRegistrationService.class);
	 CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	
	/**
	 * Instantiates a new registration presenter.
	 *
	 * @param view the view
	 * @param model the model
	 */
	public RegistrationPresenter  (FormScreen<Company> view, Company model) {
		super(view, model);
		form=(RegistartionForm) view;
		
		
		}

	 

	/**
	 * Method token to make new model.
	 */
	@Override
	protected void makeNewModel() {
		model=new Company();
	}
	
	
	
	
	/**
	 * Initalize.
	 */
	public static void initalize()
	{
		RegistartionForm  form=new  RegistartionForm();
		
		
		 CompanyPresenterTableProxy gentable=new CompanyPresenterTableProxy();
		  gentable.setView(form);
		  gentable.applySelectionModle();
		CompanyPresenterSearch.staticSuperTable=gentable;
		CompanyPresenterSearch searchpopup=new CompanyPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		
		
		 RegistrationPresenter  presenter=new RegistrationPresenter(form,new Company());
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	@Override
	public void reactOnDownload() 
	{
		ArrayList<Company> custarray=new ArrayList<Company>();
		List<Company> list=(List<Company>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		custarray.addAll(list);
		
		csvservice.setCompanylist(custarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				

				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+20;
				Window.open(url, "test", "enabled");
				
			}
		});

	}
	
	/**
	 * The Class CompanyPresenterSearch.
	 */
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessunitlayer.Company")
	 public static  class CompanyPresenterSearch extends SearchPopUpScreen<Company>{

		/* (non-Javadoc)
		 * @see com.simplesoftwares.client.library.appstructure.SearchPopUpScreen#getQuerry()
		 */
		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};
		
		/**
		 * The Class CompanyPresenterTable.
		 */
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessunitlayer.Company")
		 public static class CompanyPresenterTable extends SuperTable<Company> implements GeneratedVariableRefrence{

			/* (non-Javadoc)
			 * @see com.slicktechnologies.client.utility.GeneratedVariableRefrence#getVarRef(java.lang.String)
			 */
			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#createTable()
			 */
			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#initializekeyprovider()
			 */
			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#addFieldUpdater()
			 */
			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#setEnable(boolean)
			 */
			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.ViewContainer#applyStyle()
			 */
			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
	
	
		  
		  /**
  		 * Retrive company.
  		 */
  		protected void retriveCompany()
			{
			  MyQuerry myQuer= new MyQuerry();
			  myQuer.setQuerryObject(new Company());
			  form.showWaitSymbol();
			  service.getSearchResult(myQuer,new AsyncCallback<ArrayList<SuperModel>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					model=(Company) result.get(0);
					form.updateView(model);
					form.setToViewState();
					
					
				}
			});
			}



		/* (non-Javadoc)
		 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactToProcessBarEvents(com.google.gwt.event.dom.client.ClickEvent)
		 */
		@Override
		public void reactToProcessBarEvents(ClickEvent e) {
			// TODO Auto-generated method stub
			
		}



		/* (non-Javadoc)
		 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnPrint()
		 */
		@Override
		public void reactOnPrint() {
			// TODO Auto-generated method stub
			
		}



		/* (non-Javadoc)
		 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnEmail()
		 */
		@Override
		public void reactOnEmail() {
			// TODO Auto-generated method stub
			
		}



		@Override
		public void reactOnSave() {

			if(validateAndUpdateModel())
			{
				// Async querry is going to start. We do not want user to do any thing untill it completes.
				view.showWaitSymbol();
				register.saveCompany(model,new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) {
						if(result==null)
							form.showDialogMessage("Access Url already exists!");
						//Async run sucessfully , hide the wait symbol
						view.hideWaitSymbol();
						// set the count returned from server to this model
						model.setCount(result.count);
					   //set the id from server to this model
						model.setId(result.id);
						view.setToViewState();
						//Set count on view
						view.setCount(result.count);
						view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
						System.out.println("Model ID "+model.getId());
						
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						view.hideWaitSymbol();
						view.showDialogMessage("Data Save Unsuccessful! Try again.");
						caught.printStackTrace();
						
					}
				});
				
			}
			
			
		}
		
		protected void reactOnGo()		//GO
		{
			view.showWaitSymbol();
			MyQuerry querry=view.getSearchpopupscreen().getQuerry();
			view.getSearchpopupscreen().getSupertable().connectToLocal();
			register.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				
				@SuppressWarnings("unchecked")
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					if(result.size()==0)
						view.showDialogMessage("No Result To Display !");
					
					for(SuperModel model:result)
					{
						Company comp=(Company) model;
						view.getSearchpopupscreen().getSupertable().getListDataProvider().getList().add(comp);
					    //Set the row count of table as per the recieved data length
						
						view.getSearchpopupscreen().getSupertable().getTable().setRowCount(result.size());
					
					}
					
					view.hideWaitSymbol();
		}
				
				@Override
				public void onFailure(Throwable caught) {
					view.hideWaitSymbol();
					view.showDialogMessage("Data Save Unsuccessful! Try again.");
					caught.printStackTrace();
					
				}
			});
			
		}
		
	
}
