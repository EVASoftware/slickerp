package com.slicktechnologies.client.registration;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import java.util.Comparator;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;
import java.util.List;
import java.util.Date;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.client.registration.RegistrationPresenter.CompanyPresenterTable;
import com.slicktechnologies.client.utility.Screen;

public class CompanyPresenterTableProxy extends CompanyPresenterTable {
  TextColumn<Company> getNoOfUserColumn;
  Column<Company,Date> getLicenseStartDateColumn;
  TextColumn<Company> getStatusColumn;
  TextColumn<Company> getBusinessUnitNameColumn;
 
  TextColumn<Company> getCellNumber1Column;
  TextColumn<Company> getPocNameColumn;
  TextColumn<Company> getPocCellColumn;
  TextColumn<Company> getCountColumn;
  public Object getVarRef(String varName)
  {
  if(varName.equals("getNoOfUserColumn"))
  return this.getNoOfUserColumn;
  if(varName.equals("getLicenseStartDateColumn"))
  return this.getLicenseStartDateColumn;
  if(varName.equals("getStatusColumn"))
  return this.getStatusColumn;
  if(varName.equals("getBusinessUnitNameColumn"))
  return this.getBusinessUnitNameColumn;
  if(varName.equals("getCellNumber1Column"))
  return this.getCellNumber1Column;
  if(varName.equals("getPocNameColumn"))
  return this.getPocNameColumn;
  if(varName.equals("getPocCellColumn"))
  return this.getPocCellColumn;
  if(varName.equals("getCountColumn"))
  return this.getCountColumn;
   return null ;
  }
  public CompanyPresenterTableProxy()
  {
  super();
  }
  @Override public void createTable() {
  addColumngetCount();
  addColumngetNoOfUser();
  addColumngetBusinessUnitName();
  
  addColumngetCellNumber1();
  addColumngetLicenseStartDate();
  addColumngetPocName();
  addColumngetPocCell();
  addColumngetStatus();
  }
  @Override
  protected void initializekeyprovider() {
  keyProvider= new ProvidesKey<Company>()
  {
  @Override
  public Object getKey(Company item)
  {
  if(item==null)
  {
  return null;
  }
  else
  return item.getId();
  }
  };
  }
  @Override
  public void setEnable(boolean state)
  {
  }
  @Override
  public void applyStyle()
  {
  }
   public void addColumnSorting(){
  addSortinggetBusinessUnitName();
  
  addSortinggetCellNumber1();
  addSortinggetPocName();
  addSortinggetPocCell();
  addSortinggetStatus();
  }
  @Override public void addFieldUpdater() {
  }
  protected void addColumngetCount()
  {
  getCountColumn=new TextColumn<Company>()
  {
  @Override
  public String getValue(Company object)
  {
  if( object.getCount()==-1)
  return "N.A";
  else return object.getCount()+"";
  }
  };
  table.addColumn(getCountColumn,"Id");
  }
  protected void addColumngetNoOfUser()
  {
  getNoOfUserColumn=new TextColumn<Company>()
  {
  @Override
  public String getValue(Company object)
  {
  return object.getNoOfUser()+"";
  }
  };
  table.addColumn(getNoOfUserColumn,"No. of Users");
  }
 
  protected void addColumngetBusinessUnitName()
  {
  getBusinessUnitNameColumn=new TextColumn<Company>()
  {
  @Override
  public String getValue(Company object)
  {
  return object.getBusinessUnitName()+"";
  }
  };
  table.addColumn(getBusinessUnitNameColumn,"Name");
  getBusinessUnitNameColumn.setSortable(true);
  }
  protected void addSortinggetBusinessUnitName()
  {
  List<Company> list=getDataprovider().getList();
  columnSort=new ListHandler<Company>(list);
  columnSort.setComparator(getBusinessUnitNameColumn, new Comparator<Company>()
  {
  @Override
  public int compare(Company e1,Company e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getBusinessUnitName()!=null && e2.getBusinessUnitName()!=null){
  return e1.getBusinessUnitName().compareTo(e2.getBusinessUnitName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  
  protected void addSortinggetCellNumber1()
  {
  List<Company> list=getDataprovider().getList();
  columnSort=new ListHandler<Company>(list);
  columnSort.setComparator(getCellNumber1Column, new Comparator<Company>()
  {
  @Override
  public int compare(Company e1,Company e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCellNumber1()== e2.getCellNumber1()){
  return 0;}
  if(e1.getCellNumber1()> e2.getCellNumber1()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCellNumber1()
  {
  getCellNumber1Column=new TextColumn<Company>()
  {
  @Override
  public String getValue(Company object)
  {
  return object.getCellNumber1()+"";
  }
  };
  table.addColumn(getCellNumber1Column,"Phone");
  getCellNumber1Column.setSortable(true);
  }
  protected void addColumngetLicenseStartDate()
  {
  DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
  DatePickerCell date= new DatePickerCell(fmt);
  getLicenseStartDateColumn=new Column<Company,Date>(date)
  {
  @Override
  public Date getValue(Company object)
  {
  if(object.getLicenseStartDate()!=null){
  return object.getLicenseStartDate();}
  else{
  object.setLicenseStartDate(new Date());
  return new Date();
  }
  }
  };
  table.addColumn(getLicenseStartDateColumn,"License Start Date");
  }
  protected void addFieldUpdatergetLicenseStartDate()
  {
  getLicenseStartDateColumn.setFieldUpdater(new FieldUpdater<Company, Date>()
  {
  @Override
  public void update(int index,Company object,Date value)
  {
  object.setLicenseStartDate(value);
  table.redrawRow(index);
  }
  });
  }
  protected void addSortinggetPocName()
  {
  List<Company> list=getDataprovider().getList();
  columnSort=new ListHandler<Company>(list);
  columnSort.setComparator(getPocNameColumn, new Comparator<Company>()
  {
  @Override
  public int compare(Company e1,Company e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getPocName()!=null && e2.getPocName()!=null){
  return e1.getPocName().compareTo(e2.getPocName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetPocName()
  {
  getPocNameColumn=new TextColumn<Company>()
  {
  @Override
  public String getValue(Company object)
  {
  return object.getPocName()+"";
  }
  };
  table.addColumn(getPocNameColumn,"Contact Name");
  getPocNameColumn.setSortable(true);
  }
  protected void addSortinggetPocCell()
  {
  List<Company> list=getDataprovider().getList();
  columnSort=new ListHandler<Company>(list);
  columnSort.setComparator(getPocCellColumn, new Comparator<Company>()
  {
  @Override
  public int compare(Company e1,Company e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getPocCell()== e2.getPocCell()){
  return 0;}
  if(e1.getPocCell()> e2.getPocCell()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetPocCell()
  {
  getPocCellColumn=new TextColumn<Company>()
  {
  @Override
  public String getValue(Company object)
  {
  return object.getPocCell()+"";
  }
  };
  table.addColumn(getPocCellColumn,"Contact Phone");
  getPocCellColumn.setSortable(true);
  }
  protected void addSortinggetStatus()
  {
  List<Company> list=getDataprovider().getList();
  columnSort=new ListHandler<Company>(list);
  columnSort.setComparator(getStatusColumn, new Comparator<Company>()
  {
  @Override
  public int compare(Company e1,Company e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getStatus()!=null && e2.getStatus()!=null){
  return e1.getStatus().compareTo(e2.getStatus());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetStatus()
  {
  getStatusColumn=new TextColumn<Company>()
  {
  @Override
  public String getValue(Company object)
  {
  return object.getStatus()+"";
  }
  };
  table.addColumn(getStatusColumn,"Status");
  getStatusColumn.setSortable(true);
  }
}
