package com.slicktechnologies.client.registration;
import com.google.gwt.user.client.ui.*;

import java.util.Vector;

import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formscreen.*;
import com.simplesoftwares.client.library.composite.*;
import com.simplesoftwares.client.library.mywidgets.*;
import com.simplesoftwares.client.library.libservice.*;
import com.slicktechnologies.client.utility.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickHandler;
import com.slicktechnologies.shared.common.paymentlayer.*;
import com.google.gwt.event.dom.client.*;


// TODO: Auto-generated Javadoc
/**
 *Company Registartion Form
 */
public class RegistartionForm extends FormScreen<Company>    implements ClickHandler, ChangeHandler {

//Token to add the varialble declarations
	/** The etb point of contact email. */
EmailTextBox etbEmail,etbPointOfContactEmail;
	
	/** The no of user. */
	IntegerBox noOfUser;
	
	/** The no of weeks. */
	IntegerBox noOfWeeks;
	
	/** The lisense start date. */
	DateBox lisenseStartDate,licenseEndDate;
	
	/** The status. */
	ListBox status;
	
  /** The lb company type. */
  ObjectListBox<Config> lbCompanyType;
  
  /** The address composite. */
  AddressComposite addressComposite;
  
  /** The social info composite. */
  SocialInfoComposites socialInfoComposite;
  
  /** The tb license no. */
  TextBox tbCompanyName,tbWebsite,tbPointOfContactName,tbServiceTaxNo,tbVatTaxNo,tbPANCard,tbLicenseNo,
  			tbAccessUrl;
  
  /** The upload composite. */
  UploadComposite uploadComposite;
  
  /** The pnb point of contact cell. */
  PhoneNumberBox pnbLandlinePhone,pnbCellPhoneNo1,pnbCellPhoneNo2,pnbFaxNo,pnbPointOfContactLandline,pnbPointOfContactCell;
  
  /**
   * Updated By: Viraj
   * Date: 23-03-2019
   * Description: To add franchise Information
   */
  TextBox brandUrl,masterUrl;
  ListBox lbFranchiseType;
  /** Ends **/
	
	/**
	 * Instantiates a new registartion form.
	 */
	public RegistartionForm() {
		super();
		createGui();
	}

	/**
	 * Instantiates a new registartion form.
	 *
	 * @param processlevel the processlevel
	 * @param fields the fields
	 * @param formstyle the formstyle
	 */
	public RegistartionForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		
		
	}

	
	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget()
	{
		
etbEmail=new EmailTextBox();
noOfUser=new IntegerBox();
noOfWeeks=new IntegerBox();
//lisenseStartDate=new DateBox();
lisenseStartDate = new DateBoxWithYearSelector();
//licenseEndDate=new DateBox();
licenseEndDate = new DateBoxWithYearSelector();
licenseEndDate.setEnabled(false);
status=new ListBox();
//Initalizing status
status.addItem("--Select Status--");
status.addItem(Company.ACTIVE);
status.addItem(Company.DEMO);
status.addItem(Company.EXPIRED);
status.addItem(Company.INACTIVE);

    tbAccessUrl=new TextBox();
etbPointOfContactEmail=new EmailTextBox();
    
lbCompanyType=new ObjectListBox<Config>();
    AppUtility.MakeLiveConfig(lbCompanyType,Screen.COMPANYTYPE);

addressComposite=new AddressComposite();
    
socialInfoComposite=new SocialInfoComposites();
    
tbCompanyName=new TextBox();
    
tbWebsite=new TextBox();
    
tbPointOfContactName=new TextBox();
    
tbServiceTaxNo=new TextBox();
    
tbVatTaxNo=new TextBox();
    
tbPANCard=new TextBox();
    
tbLicenseNo=new TextBox();
    
uploadComposite=new UploadComposite();
    
pnbLandlinePhone=new PhoneNumberBox();
    
pnbCellPhoneNo1=new PhoneNumberBox();
    
pnbCellPhoneNo2=new PhoneNumberBox();
    
pnbFaxNo=new PhoneNumberBox();
    
pnbPointOfContactLandline=new PhoneNumberBox();
    
pnbPointOfContactCell=new PhoneNumberBox();
/**
 * Updated By: Viraj 
 * Date: 24-03-2019
 * Description: To add franchise Information   
 */
   brandUrl = new TextBox();
   masterUrl = new TextBox();
   
   lbFranchiseType = new ListBox();
   lbFranchiseType.insertItem("--Select--", 0);
   lbFranchiseType.insertItem("BRAND", 1);
   lbFranchiseType.insertItem("MASTER FRANCHISE", 2);
   lbFranchiseType.insertItem("FRANCHISE", 3);
   
   lbFranchiseType.addChangeHandler(this);
   brandUrl.setEnabled(false);
   masterUrl.setEnabled(false);
 /** Ends **/
    
	}
	
	/**
	 * method template to create screen formfields.
	 */
	@Override
	public void createScreen() {
		
	    
		initalizeWidget();
		
		//Token to initialize the processlevel menus.
		
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
	fbuilder = new FormFieldBuilder();
	FormField fgroupingCompanyLisenseInformation=fbuilder.setlabel("Company Lisense Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	fbuilder=new FormFieldBuilder("* No Of User",noOfUser);
	FormField ftNoUser= fbuilder.setMandatory(true).setMandatoryMsg("Number of User can't be empty.").setRowSpan(0).setColSpan(0).build();
	fbuilder=new FormFieldBuilder("* No Of Weeks",noOfWeeks);
	FormField ftNoOfWeeks= fbuilder.setMandatory(true).setMandatoryMsg("Number of Weeks Cannot be empty").setRowSpan(0).setColSpan(0).build();
	fbuilder=new FormFieldBuilder("* Lisence Start Date",lisenseStartDate);
	FormField flbStartDate= fbuilder.setMandatory(true).setMandatoryMsg("Start Date Cannot be Empty").setRowSpan(0).setColSpan(0).build();
	fbuilder=new FormFieldBuilder("Lisence End Date",licenseEndDate);
	FormField flicenseEndDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
	fbuilder=new FormFieldBuilder("* Status",status);
	FormField flbstatus= fbuilder.setMandatory(true).setMandatoryMsg("Status should be selected").setRowSpan(0).setColSpan(0).build();
	
	FormField fgroupingCompanyInformation=fbuilder.setlabel("Company Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	
	fbuilder = new FormFieldBuilder("* Company Name",tbCompanyName);
	FormField ftbCompanyName= fbuilder.setMandatory(true).setMandatoryMsg("Company Name can't be empty.").setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("* Company Type",lbCompanyType);
	FormField flbCompanyType= fbuilder.setMandatory(true).setMandatoryMsg("Company Type can't be empty.").setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("Website",tbWebsite);
	FormField ftbWebsite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("* Email",etbEmail);
	FormField fetbEmail= fbuilder.setMandatory(true).setMandatoryMsg("Email can't be empty.").setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("Landline Phone",pnbLandlinePhone);
	FormField fpnbLandlinePhone= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("* Cell Phone No 1",pnbCellPhoneNo1);
	FormField fpnbCellPhoneNo1= fbuilder.setMandatory(true).setMandatoryMsg("Cell Phone No 1 can't be empty.").setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("Cell Phone No 2",pnbCellPhoneNo2);
	FormField fpnbCellPhoneNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("Fax No",pnbFaxNo);
	FormField fpnbFaxNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder();
	FormField fgroupingPointOfContactInformation=fbuilder.setlabel("Point Of Contact Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	fbuilder = new FormFieldBuilder("* Point Of Contact Name",tbPointOfContactName);
	FormField ftbPointOfContactName= fbuilder.setMandatory(true).setMandatoryMsg("Point Of Contact Name can't be empty.").setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("Point Of Contact Landline",pnbPointOfContactLandline);
	FormField fpnbPointOfContactLandline= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("* Point Of Contact Cell",pnbPointOfContactCell);
	FormField fpnbPointOfContactCell= fbuilder.setMandatory(true).setMandatoryMsg("Point Of Contact Cell can't be empty.").setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("* Point Of Contact Email",etbPointOfContactEmail);
	FormField fetbPointOfContactEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder();
	FormField fgroupingTaxInformation=fbuilder.setlabel("Tax Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	fbuilder = new FormFieldBuilder("Service Tax No",tbServiceTaxNo);
	FormField ftbServiceTaxNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("Vat Tax No",tbVatTaxNo);
	FormField ftbVatTaxNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("PAN Card",tbPANCard);
	FormField ftbPANCard= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("License No",tbLicenseNo);
	FormField ftbLicenseNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder();
	FormField fgroupingSocialInformation=fbuilder.setlabel("Social Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	fbuilder = new FormFieldBuilder("",socialInfoComposite);
	FormField fsocialInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	fbuilder = new FormFieldBuilder();
	FormField fgroupingAddressInformation=fbuilder.setlabel("Address Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	fbuilder = new FormFieldBuilder("",addressComposite);
	FormField faddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	fbuilder = new FormFieldBuilder();
	FormField fgroupingUploadLogo=fbuilder.setlabel("Upload Logo").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	fbuilder = new FormFieldBuilder("",uploadComposite);
	FormField fuploadComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
	fbuilder = new FormFieldBuilder("* Access Url",tbAccessUrl);
	FormField faccessurl= fbuilder.setMandatory(true).setMandatoryMsg("Access Url is Mandatory!").setRowSpan(0).setColSpan(0).build();

	/**
	 * Updated By: Viraj 
	 * Date: 24-03-2019
	 * Description: To add franchise Information   
	 */
	FormField flbFranchiseType = null;
	FormField fFranchiseBrandUrl = null;
	FormField fmasterUrl = null;
	FormField fgroupingFranchiseInfo = null;
	if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Company","FranchiseInformation")) {
		fgroupingFranchiseInfo = fbuilder.setlabel("Franchise Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Franchise Type",lbFranchiseType);
		flbFranchiseType = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Brand URL",brandUrl);
		fFranchiseBrandUrl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Master URL",masterUrl);
		fmasterUrl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	}
    /** Ends **/	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		
	/**
	 * Updated By: Viraj 
	 * Date: 24-03-2019
	 * Description: To add franchise Information   
	 */
	if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Company","FranchiseInformation")) {
		FormField[][] formfield = {   
				    {fgroupingCompanyLisenseInformation},
				    {faccessurl,ftNoUser,ftNoOfWeeks,flbstatus},
				    {flbStartDate,flicenseEndDate},
				    {fgroupingCompanyInformation},
					{ftbCompanyName,flbCompanyType,ftbWebsite,fetbEmail},
					{fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2,fpnbFaxNo},
					{fgroupingFranchiseInfo},
					{flbFranchiseType,fFranchiseBrandUrl,fmasterUrl},
					{fgroupingPointOfContactInformation},
					{ftbPointOfContactName,fpnbPointOfContactLandline,fpnbPointOfContactCell,fetbPointOfContactEmail},
					// {fgroupingTaxInformation},
					// {ftbServiceTaxNo,ftbVatTaxNo,ftbPANCard,ftbLicenseNo},
					//{fgroupingSocialInformation},
					//{fsocialInfoComposite},
					{fgroupingAddressInformation},
					{faddressComposite},
					//{fgroupingUploadLogo},
					// {fuploadComposite},
					};
					
							this.fields=formfield;
										   
					}
	else {
		FormField[][] formfield = {   
				   {fgroupingCompanyLisenseInformation},
				   {faccessurl,ftNoUser,ftNoOfWeeks,flbstatus},
				   {flbStartDate,flicenseEndDate},
				   {fgroupingCompanyInformation},
{ftbCompanyName,flbCompanyType,ftbWebsite,fetbEmail},
{fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2,fpnbFaxNo},
{fgroupingPointOfContactInformation},
{ftbPointOfContactName,fpnbPointOfContactLandline,fpnbPointOfContactCell,fetbPointOfContactEmail},
// {fgroupingTaxInformation},
// {ftbServiceTaxNo,ftbVatTaxNo,ftbPANCard,ftbLicenseNo},
//{fgroupingSocialInformation},
//{fsocialInfoComposite},
{fgroupingAddressInformation},
{faddressComposite},
//{fgroupingUploadLogo},
// {fuploadComposite},
};

		this.fields=formfield;
					   
}
	}

	/**
	 * method template to update the model with token entity name.
	 *
	 * @param model the model
	 */
	@Override
	public void updateModel(Company model) 
	{
		
		if(tbCompanyName.getValue()!=null)
  model.setBusinessUnitName(tbCompanyName.getValue());
if(lbCompanyType.getValue()!=null)
  model.setCompanyType(lbCompanyType.getValue());
if(tbWebsite.getValue()!=null)
  model.setWebsite(tbWebsite.getValue());
if(etbEmail.getValue()!=null)
  model.setEmail(etbEmail.getValue());
if(pnbLandlinePhone.getValue()!=null)
  model.setLandline(pnbLandlinePhone.getValue());
if(pnbCellPhoneNo1.getValue()!=null)
  model.setCellNumber1(pnbCellPhoneNo1.getValue());
if(pnbCellPhoneNo2.getValue()!=null)
  model.setCellNumber2(pnbCellPhoneNo2.getValue());
if(pnbFaxNo.getValue()!=null)
  model.setFaxNumber(pnbFaxNo.getValue());
if(tbPointOfContactName.getValue()!=null)
  model.setPocName(tbPointOfContactName.getValue());
if(pnbPointOfContactLandline.getValue()!=null)
  model.setPocLandline(pnbPointOfContactLandline.getValue());
if(pnbPointOfContactCell.getValue()!=null)
  model.setPocCell(pnbPointOfContactCell.getValue());
if(etbPointOfContactEmail.getValue()!=null)
  model.setPocEmail(etbPointOfContactEmail.getValue());
if(tbServiceTaxNo.getValue()!=null)
  model.setServiceTaxNo(tbServiceTaxNo.getValue());
if(tbVatTaxNo.getValue()!=null)
  model.setVatTaxNo(tbVatTaxNo.getValue());
if(tbPANCard.getValue()!=null)
  model.setPanCard(tbPANCard.getValue());
if(tbLicenseNo.getValue()!=null)
  model.setLicense(tbLicenseNo.getValue());
if(socialInfoComposite.getValue()!=null)
  model.setSocialInfo(socialInfoComposite.getValue());
if(addressComposite.getValue()!=null)
  model.setAddress(addressComposite.getValue());
if(uploadComposite.getValue()!=null)
  model.setLogo(uploadComposite.getValue());
if(noOfUser.getValue()!=null)
	model.setNoOfUser(noOfUser.getValue());
if(noOfWeeks.getValue()!=null)
	model.setNoOfWeeks(noOfWeeks.getValue());
if(this.lisenseStartDate.getValue()!=null)
     model.setLicenseStartDate(lisenseStartDate.getValue());
if(tbAccessUrl.getValue()!=null)
{
	String accessurl=tbAccessUrl.getValue().trim().toLowerCase();
	accessurl=accessurl.replaceAll("\\s","");
	model.setAccessUrl(accessurl);
	
}
int selecteDindex=status.getSelectedIndex();
if(selecteDindex!=0)
   model.setStatus(status.getValue(selecteDindex));

/**
 * Updated By: Viraj 
 * Date: 24-03-2019
 * Description: To add franchise Information   
 */
if(lbFranchiseType.getSelectedIndex() != 0) {
	model.setFranchiseType(lbFranchiseType.getValue(lbFranchiseType.getSelectedIndex()));
}
if(brandUrl.getValue() != null) {
	model.setBrandUrl(brandUrl.getValue());
}
if(masterUrl.getValue() != null) {
	model.setMasterUrl(masterUrl.getValue());
}
/** Ends **/

		
	}

	/**
	 * method template to update the view with token entity name.
	 *
	 * @param view the view
	 */
	@Override
	public void updateView(Company view) 
	{
		
		
		if(view.getBusinessUnitName()!=null)
  tbCompanyName.setValue(view.getBusinessUnitName());
if(view.getCompanyType()!=null)
  lbCompanyType.setValue(view.getCompanyType());
if(view.getWebsite()!=null)
  tbWebsite.setValue(view.getWebsite());
if(view.getEmail()!=null)
  etbEmail.setValue(view.getEmail());
if(view.getLandline()!=null)
  pnbLandlinePhone.setValue(view.getLandline());
if(view.getCellNumber1()!=null)
  pnbCellPhoneNo1.setValue(view.getCellNumber1());
if(view.getCellNumber2()!=null)
  pnbCellPhoneNo2.setValue(view.getCellNumber2());
if(view.getFaxNumber()!=null)
  pnbFaxNo.setValue(view.getFaxNumber());
if(view.getPocName()!=null)
  tbPointOfContactName.setValue(view.getPocName());
if(view.getPocLandline()!=null)
  pnbPointOfContactLandline.setValue(view.getPocLandline());
if(view.getPocCell()!=null)
  pnbPointOfContactCell.setValue(view.getPocCell());
if(view.getPocEmail()!=null)
  etbPointOfContactEmail.setValue(view.getPocEmail());
if(view.getServiceTaxNo()!=null)
  tbServiceTaxNo.setValue(view.getServiceTaxNo());
if(view.getVatTaxNo()!=null)
  tbVatTaxNo.setValue(view.getVatTaxNo());
if(view.getPanCard()!=null)
  tbPANCard.setValue(view.getPanCard());
if(view.getLicense()!=null)
  tbLicenseNo.setValue(view.getLicense());
if(view.getSocialInfo()!=null)
  socialInfoComposite.setValue(view.getSocialInfo());
if(view.getAddress()!=null)
  addressComposite.setValue(view.getAddress());

if(view.getAccessUrl()!=null)
	tbAccessUrl.setValue(view.getAccessUrl());

if(view.getNoOfUser()!=0)
	noOfUser.setValue(view.getNoOfUser());
if(view.getNoOfWeeks()!=0)
	noOfWeeks.setValue(view.getNoOfWeeks());
if(view.getLicenseStartDate()!=null)
	this.lisenseStartDate.setValue(view.getLicenseStartDate());
if(view.getStatus().equals("")==false)
{
	for(int i=0;i<this.status.getItemCount();i++)
	{
		String item=this.status.getItemText(i);
		if(item.equals(view.getStatus()))
		{
		  this.status.setSelectedIndex(i);
		  break;
		  
	    }
		
	}
	
}
	
/**
 * Updated By: Viraj 
 * Date: 24-03-2019
 * Description: To add franchise Information   
 */
if(view.getFranchiseType() != null) {
	int count = lbFranchiseType.getItemCount();
	String item=view.getFranchiseType().trim();
	for (int i = 0; i < count; i++) {
		if(item.trim().equals(lbFranchiseType.getItemText(i).trim())){
			lbFranchiseType.setSelectedIndex(i);
			break;
		}
	}
}

if(view.getBrandUrl() != null) {
	brandUrl.setValue(view.getBrandUrl());
}

if(view.getMasterUrl() != null) {
	masterUrl.setValue(view.getMasterUrl());
}
/** Ends **/

	}

	// Hand written code shift in presenter
	
	
	/**
	 * Toggles the app header bar menus as per screen state.
	 */
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.equals("Search"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.equals("Search"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
	}
	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		licenseEndDate.setEnabled(false);
		/**
		 * Updated By: Viraj
		 * Date: 24-03-2019
		 * Description: To add Franchise Information
		 */
		this.brandUrl.setEnabled(false);
		this.masterUrl.setEnabled(false);
		/** Ends **/
	}
	
	/**
	 * sets the id textbox with the passed count value.
	 *
	 * @param count the new count
	 */
	@Override
	public void setCount(int count)
	{
	
	}

	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
		this.tbAccessUrl.setEnabled(false);
		
		/**
		 * Updated By: Viraj 
		 * Date: 24-03-2019
		 * Description: To add franchise Information   
		 */
		brandUrl.setEnabled(false);
		masterUrl.setEnabled(false);
		if(lbFranchiseType.getSelectedIndex() == 3) {
			brandUrl.setEnabled(true);
			masterUrl.setEnabled(true);
		}
		if(lbFranchiseType.getSelectedIndex() == 1) {
			brandUrl.setEnabled(false);
			masterUrl.setEnabled(false);
		}
		if(lbFranchiseType.getSelectedIndex() == 2) {
			brandUrl.setEnabled(true);
		}
		/** Ends **/
	}

	/**
	 * Updated By: Viraj 
	 * Date: 24-03-2019
	 * Description: To add franchise Information   
	 */
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(lbFranchiseType)) {
			if(lbFranchiseType.getSelectedIndex() == 3) {
				brandUrl.setEnabled(true);
				masterUrl.setEnabled(true);
			}
			
			if(lbFranchiseType.getSelectedIndex() == 2) {
				brandUrl.setEnabled(true);
				masterUrl.setEnabled(false);
			}
			
			if(lbFranchiseType.getSelectedIndex() == 1) {
				brandUrl.setEnabled(false);
				masterUrl.setEnabled(false);
			}
		}
		
	}
	/** Ends **/
   
}
