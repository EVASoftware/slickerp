package com.slicktechnologies.client.userprofile;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;

public class UserLoginInfoPopupForm extends AbsolutePanel {
	
	public InlineLabel changePasswordLbl = new InlineLabel("Change Password");
	public InlineLabel logoutLbl = new InlineLabel("Logout");
	
	/**
	 * @author Anil
	 * @since 22-09-2020
	 */
	public InlineLabel loginPersonNameLbl = new InlineLabel(LoginPresenter.loggedInUser);
	public InlineLabel viewProfileLbl = new InlineLabel("");
	
	public UserLoginInfoPopupForm() {
		add(loginPersonNameLbl,10,5);
		
		if(UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName()!=null&&!UserConfiguration.getRole().getRoleName().equals("")){
			viewProfileLbl.setText(UserConfiguration.getRole().getRoleName());
			add(viewProfileLbl,10,30);
			add(changePasswordLbl,10,55);
			add(logoutLbl,10,80);
			
			setSize("180px", "100px");
		}else{
//			add(changePasswordLbl,10,5);
//			add(logoutLbl,10,30);
			add(changePasswordLbl,10,30);
			add(logoutLbl,10,55);
			setSize("180px", "75px");
		}
		
//		setSize("180px", "50px");
		this.getElement().setId("login-form");
		
	}
	

}
