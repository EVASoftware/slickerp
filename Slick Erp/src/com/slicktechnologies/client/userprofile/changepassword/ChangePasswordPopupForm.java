package com.slicktechnologies.client.userprofile.changepassword;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PasswordTextBox;

public class ChangePasswordPopupForm  extends AbsolutePanel {
	public PasswordTextBox ptbOldPassword;
	public PasswordTextBox ptbNewPassword;
	PasswordTextBox ptbRetypeNewPassword;
	public Button btnOk,btnCancel;
	
	public void initializeWidget(){
		ptbOldPassword=new PasswordTextBox();
		ptbNewPassword=new PasswordTextBox();
		ptbRetypeNewPassword=new PasswordTextBox();
		
		btnOk=new Button("Submit");
		btnCancel=new Button("Cancel");
	}
	
	public ChangePasswordPopupForm() {
		initializeWidget();
		InlineLabel label=new InlineLabel("CHANGE PASSWORD");
		InlineLabel oldPassLbl=new InlineLabel("Old Password");
		InlineLabel newPassLbl=new InlineLabel("New Password");
		InlineLabel retypeNewPassLbl=new InlineLabel("Retype New Password");
		
		add(label,90,10);
		
		add(oldPassLbl,10,40);
		add(ptbOldPassword,150,40);
		
		add(newPassLbl,10,70);
		add(ptbNewPassword,150,70);
		
		add(retypeNewPassLbl,10,100);
		add(ptbRetypeNewPassword,150,100);
		
		add(btnOk,10,140);
		add(btnCancel,150,140);
		
		setSize("330px", "180px");
		this.getElement().setId("login-form");
		
		
	}
	
	
	public boolean valid(){
		if(ptbOldPassword.getValue().equals("")){
			Window.alert("Plese enter old password !");
			return false;
		}
		if(ptbNewPassword.getValue().equals("")){
			Window.alert("Plese enter new password !");
			return false;
		}
		if(ptbRetypeNewPassword.getValue().equals("")){
			Window.alert("Plese retype new password !");
			return false;
		}
		
		if(!ptbNewPassword.getValue().equals(ptbRetypeNewPassword.getValue())){
			Window.alert("Password Mismatch !");
			return false;
		}
		
		return true;
	}
	
	public void clear(){
		ptbOldPassword.setValue("");
		ptbNewPassword.setValue("");
		ptbRetypeNewPassword.setValue("");
	}
	
}
