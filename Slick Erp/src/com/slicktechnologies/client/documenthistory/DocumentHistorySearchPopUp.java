package com.slicktechnologies.client.documenthistory;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.documenthistorydetails.DocumentHistory;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class DocumentHistorySearchPopUp extends SearchPopUpScreen<DocumentHistory> {
	
	public IntegerBox ibcountId;
	ObjectListBox<ConfigCategory> olbModuleName;
	ObjectListBox<Type> olbDocumentType;
	IntegerBox ibDocumentId;
	ListBox lbtransactionType;
	public ObjectListBox<Employee> olbCreatedBy;
	DateComparator dateComparator;
	
	
	public DocumentHistorySearchPopUp() {
		super();
		createGui();
	}
	
	public DocumentHistorySearchPopUp(boolean b) {
		super(b);
		createGui();
	}
	
	
	public void initWidget()
	{
		ibcountId= new IntegerBox();
		olbModuleName=new ObjectListBox<ConfigCategory>();
		olbDocumentType=new ObjectListBox<Type>();
		CategoryTypeFactory.initiateOneManyFunctionality(this.olbModuleName,this.olbDocumentType);
		AppUtility.makeTypeListBoxLive(this.olbDocumentType,Screen.MODULENAME);
		ibDocumentId=new IntegerBox();
		dateComparator=new DateComparator("transactionDate",new DocumentHistory());
		lbtransactionType=new ListBox();
		AppUtility.setStatusListBox(lbtransactionType, DocumentHistory.getStatusList());
		olbCreatedBy=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbCreatedBy);
		
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Document History ID",ibcountId);
		FormField fibcountId= builder.setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date (Transaction Date)",dateComparator.getFromDate());
		FormField fdateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date (Transaction Date)",dateComparator.getToDate());
		FormField fdateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Module Name",olbModuleName);
		FormField folbModuleName= builder.setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Document Type",olbDocumentType);
		FormField folbDocumentType= builder.setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Document ID",ibDocumentId);
		FormField fibDocumentId= builder.setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Transaction Type",lbtransactionType);
		FormField flbtransactionType=builder.setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Created By",olbCreatedBy);
		FormField folbCreatedBy= builder.setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				{fibcountId,fdateComparatorfrom,fdateComparatorto},
				{folbModuleName,folbDocumentType,fibDocumentId},
				{flbtransactionType,folbCreatedBy}
		};
	}
	
	
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(dateComparator.getValue()!=null)
			filtervec.addAll(dateComparator.getValue());
		
		if(ibcountId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibcountId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if(ibDocumentId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibDocumentId.getValue());
			temp.setQuerryString("documentId");
			filtervec.add(temp);
		}
		
		  if(olbModuleName.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbModuleName.getValue().trim());
				temp.setQuerryString("moduleName");
				filtervec.add(temp);
			}
		  
		  
		  if(olbDocumentType.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbDocumentType.getValue().trim());
				temp.setQuerryString("documentName");
				filtervec.add(temp);
			}
		  
		  if(lbtransactionType.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(lbtransactionType.getItemText(lbtransactionType.getSelectedIndex()));
				temp.setQuerryString("transactionType");
				filtervec.add(temp);
			}
		  
		  if(olbCreatedBy.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbCreatedBy.getValue().trim());
				temp.setQuerryString("createdBy");
				filtervec.add(temp);
			}
		  
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new DocumentHistory());
		return querry;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}
	
	
	
	
	
	
	
	

}
