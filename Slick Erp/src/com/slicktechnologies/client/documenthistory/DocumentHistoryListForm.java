package com.slicktechnologies.client.documenthistory;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.documenthistorydetails.DocumentHistory;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class DocumentHistoryListForm extends TableScreen<DocumentHistory> {
	
	public DocumentHistoryListForm(SuperTable<DocumentHistory> superTable) {
		super(superTable);
		createGui();
	}
	
	@SuppressWarnings("unused")
	private void initalizeWidget()
	{
		//<ReplasbleVariableInitalizerBlock>
	}
	
	@Override
	public void createScreen() 
	{
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
	}
	
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(DocumentHistory model)
	{
		//<ReplasableUpdateModelFromViewCode>
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(DocumentHistory view) 
	{
		//<ReplasableUpdateViewFromModelCode>
	}
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Download")||text.equals("Search"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		  		
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Download")||text.equals("Search"))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.DOCUMENTHISTORY,LoginPresenter.currentModule.trim());
	}
	
	
	@Override
	public void retriveTable(MyQuerry querry) {
		superTable.connectToLocal();
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for(SuperModel model:result)
				superTable.getListDataProvider().getList().add((DocumentHistory) model);
				superTable.getTable().setVisibleRange(0,result.size());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		});
	}
	


}
