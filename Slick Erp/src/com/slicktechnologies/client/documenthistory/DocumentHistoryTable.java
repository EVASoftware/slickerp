package com.slicktechnologies.client.documenthistory;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.documenthistorydetails.DocumentHistory;

public class DocumentHistoryTable extends SuperTable<DocumentHistory> {
	
	TextColumn<DocumentHistory> getCountColumn;
	TextColumn<DocumentHistory> getModuleNameColumn;
	TextColumn<DocumentHistory> getDocumentTypeColumn;
	TextColumn<DocumentHistory> getDocumentIdColumn;
	TextColumn<DocumentHistory> getTransactionTypeColumn;
	TextColumn<DocumentHistory> getCreatedByColumn;
	TextColumn<DocumentHistory> getTransactionDateColumn;
	TextColumn<DocumentHistory> getTransactionTimeColumn;
	TextColumn<DocumentHistory> getUserNameColumn;
	
	
	public DocumentHistoryTable() {
		super();
	}

	@Override
	public void createTable() {
//		addColumngetCount();
//		addColumngetModuleName();
		addColumngetDocumentType();
		addColumngetDocumentId();
		addColumngetTransactionType();
		addColumnCreatedBy();
		addColumngetUserId();
		addColumnTransactionDate();
		addColumnTransactionTime();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	
	public void addColumngetCount()
	{
		
		getCountColumn=new TextColumn<DocumentHistory>() {

			@Override
			public String getValue(DocumentHistory object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getCountColumn,"Count ID");
		table.setColumnWidth(getCountColumn,100,Unit.PX);
		getCountColumn.setSortable(true);
	}
	
	
	protected void addColumngetModuleName()
	{
		getModuleNameColumn=new TextColumn<DocumentHistory>()
				{
			@Override
			public String getValue(DocumentHistory object)
			{
				return object.getModuleName();
			}
				};
		table.addColumn(getModuleNameColumn,"Module Name");
		table.setColumnWidth(getModuleNameColumn, 110, Unit.PX);
		getModuleNameColumn.setSortable(true);
	}
	
	protected void addColumngetDocumentType()
	{
		getDocumentTypeColumn=new TextColumn<DocumentHistory>()
				{
			@Override
			public String getValue(DocumentHistory object)
			{
				return object.getDocumentName();
			}
				};
		table.addColumn(getDocumentTypeColumn,"Document Type");
		table.setColumnWidth(getDocumentTypeColumn, 110, Unit.PX);
		getDocumentTypeColumn.setSortable(true);
	}
	
	protected void addColumngetDocumentId()
	{
		getDocumentIdColumn=new TextColumn<DocumentHistory>()
				{
			@Override
			public String getValue(DocumentHistory object)
			{
				return object.getDocumentId()+"";
			}
				};
		table.addColumn(getDocumentIdColumn,"Document ID");
		table.setColumnWidth(getDocumentIdColumn, 100, Unit.PX);
		getDocumentIdColumn.setSortable(true);
	}
	
	protected void addColumngetTransactionType()
	{
		getTransactionTypeColumn=new TextColumn<DocumentHistory>()
				{
			@Override
			public String getValue(DocumentHistory object)
			{
				return object.getTransactionType();
			}
				};
		table.addColumn(getTransactionTypeColumn,"Transaction Type");
		table.setColumnWidth(getTransactionTypeColumn, 110, Unit.PX);
		getTransactionTypeColumn.setSortable(true);
	}
	
	protected void addColumnCreatedBy()
	{
		getCreatedByColumn=new TextColumn<DocumentHistory>()
				{
			@Override
			public String getValue(DocumentHistory object)
			{
				return object.getCreatedBy();
			}
				};
		table.addColumn(getCreatedByColumn,"Created By");
		table.setColumnWidth(getCreatedByColumn, 110, Unit.PX);
		getCreatedByColumn.setSortable(true);
	}
	
	protected void addColumngetUserId()
	{
		getUserNameColumn=new TextColumn<DocumentHistory>()
				{
			@Override
			public String getValue(DocumentHistory object)
			{
				return object.getUserName();
			}
				};
		table.addColumn(getUserNameColumn,"User ID");
		table.setColumnWidth(getUserNameColumn, 90, Unit.PX);
		getUserNameColumn.setSortable(true);
	}
	
	protected void addColumnTransactionDate()
	{
		getTransactionDateColumn=new TextColumn<DocumentHistory>()
				{
			@Override
			public String getValue(DocumentHistory object)
			{
				if(object.getTransactionDate()!=null){
					return AppUtility.parseDate(object.getTransactionDate());
				}
				else{
					return "";
				}
				
			}
				};
		table.addColumn(getTransactionDateColumn,"Date");
		table.setColumnWidth(getTransactionDateColumn, 110, Unit.PX);
		getTransactionDateColumn.setSortable(true);
	}
	
	protected void addColumnTransactionTime()
	{
		getTransactionTimeColumn=new TextColumn<DocumentHistory>()
				{
			@Override
			public String getValue(DocumentHistory object)
			{
				if(object.getTransactionDate()!=null){
					return object.getTransactionTime();
				}
				else{
					return "";
				}
				
			}
				};
		table.addColumn(getTransactionTimeColumn,"Time");
		table.setColumnWidth(getTransactionTimeColumn, 90, Unit.PX);
		getTransactionTimeColumn.setSortable(true);
	}
	
	
	public void addColumnSorting()
	{
		addSortinggetCount();
		addSortinggetModuleName();
		addSortinggetDocumentName();
		addSortinggetDocumentId();
		addSortinggetTransactionType();
		addSortinggetUserName();
		addSortinggetCreatedBy();
		addSortinggetTransactionDate();
		addSortingTransactionTime();
	}
	
	
	protected void addSortinggetCount()
	{
		List<DocumentHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<DocumentHistory>(list);
		columnSort.setComparator(getCountColumn, new Comparator<DocumentHistory>()
				{
			@Override
			public int compare(DocumentHistory e1,DocumentHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggetModuleName()
	{
		List<DocumentHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<DocumentHistory>(list);
		columnSort.setComparator(getModuleNameColumn, new Comparator<DocumentHistory>()
				{
			@Override
			public int compare(DocumentHistory e1,DocumentHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getModuleName()!=null && e2.getModuleName()!=null){
						return e1.getModuleName().compareTo(e2.getModuleName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetDocumentName()
	{
		List<DocumentHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<DocumentHistory>(list);
		columnSort.setComparator(getDocumentTypeColumn, new Comparator<DocumentHistory>()
				{
			@Override
			public int compare(DocumentHistory e1,DocumentHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDocumentName()!=null && e2.getDocumentName()!=null){
						return e1.getDocumentName().compareTo(e2.getDocumentName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggetDocumentId()
	{
		List<DocumentHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<DocumentHistory>(list);
		columnSort.setComparator(getDocumentIdColumn, new Comparator<DocumentHistory>()
				{
			@Override
			public int compare(DocumentHistory e1,DocumentHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getDocumentId()== e2.getDocumentId()){
						return 0;}
					if(e1.getDocumentId()> e2.getDocumentId()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetTransactionType()
	{
		List<DocumentHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<DocumentHistory>(list);
		columnSort.setComparator(getTransactionTypeColumn, new Comparator<DocumentHistory>()
				{
			@Override
			public int compare(DocumentHistory e1,DocumentHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getTransactionType()!=null && e2.getTransactionType()!=null){
						return e1.getTransactionType().compareTo(e2.getTransactionType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCreatedBy()
	{
		List<DocumentHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<DocumentHistory>(list);
		columnSort.setComparator(getCreatedByColumn, new Comparator<DocumentHistory>()
				{
			@Override
			public int compare(DocumentHistory e1,DocumentHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreatedBy()!=null && e2.getCreatedBy()!=null){
						return e1.getCreatedBy().compareTo(e2.getCreatedBy());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggetUserName()
	{
		List<DocumentHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<DocumentHistory>(list);
		columnSort.setComparator(getUserNameColumn, new Comparator<DocumentHistory>()
				{
			@Override
			public int compare(DocumentHistory e1,DocumentHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getUserName()!=null && e2.getUserName()!=null){
						return e1.getUserName().compareTo(e2.getUserName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetTransactionDate()
	{
		List<DocumentHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<DocumentHistory>(list);
		columnSort.setComparator(getTransactionDateColumn, new Comparator<DocumentHistory>()
				{
			@Override
			public int compare(DocumentHistory e1,DocumentHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getTransactionDate()!=null && e2.getTransactionDate()!=null){
						return e1.getTransactionDate().compareTo(e2.getTransactionDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortingTransactionTime()
	{
		List<DocumentHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<DocumentHistory>(list);
		columnSort.setComparator(getTransactionTimeColumn, new Comparator<DocumentHistory>()
				{
			@Override
			public int compare(DocumentHistory e1,DocumentHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getTransactionTime()!=null && e2.getTransactionTime()!=null){
						return e1.getTransactionTime().compareTo(e2.getTransactionTime());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	
	
	
	
	
	
	
	
	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<DocumentHistory>() {
			@Override
			public Object getKey(DocumentHistory item) {
				if(item==null)
					return null;
				else
					return item.getId();
			}
		};
	}

	@Override
	public void addFieldUpdater() {
	}


	@Override
	public void applyStyle() {
	}
	
	@Override
	public void setEnable(boolean state) {
	}


	
	
	

}
