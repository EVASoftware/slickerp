package com.slicktechnologies.client.documenthistory;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.documenthistorydetails.DocumentHistory;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class DocumentHistoryListPresenter extends TableScreenPresenter<DocumentHistory> {
	
	TableScreen<DocumentHistory>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	public DocumentHistoryListPresenter(TableScreen<DocumentHistory> view,
			DocumentHistory model) {
		super(view, model);
		form=view;
//		view.retriveTable(getDocumentHistoryQuery());
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.DOCUMENTHISTORY,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
	}
	
	public DocumentHistoryListPresenter(TableScreen<DocumentHistory> view,
			DocumentHistory model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.DOCUMENTHISTORY,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("")){
			reactTo();
		}
	}

	public void reactOnPrint()
	{
	
	}
	
	@Override
	public void reactOnDownload() {
		ArrayList<DocumentHistory> docarray=new ArrayList<DocumentHistory>();
		List<DocumentHistory> list=form.getSuperTable().getListDataProvider().getList();
		
		docarray.addAll(list); 
		
		csvservice.setDocumentHistoryList(docarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+79;
				Window.open(url, "test", "enabled");
			}
		});
	}

	@Override
	public void reactOnEmail() {
		
		
	}

	/*
	 * Method template to make a new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new DocumentHistory();
	}
	
	public static void initalize()
	{
		DocumentHistoryTable table=new DocumentHistoryTable();
		DocumentHistoryListForm form=new DocumentHistoryListForm(table);
		DocumentHistorySearchPopUp.staticSuperTable=table;
		
		DocumentHistorySearchPopUp searchPopUp=new DocumentHistorySearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		DocumentHistoryListPresenter presenter=new DocumentHistoryListPresenter(form, new DocumentHistory());
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	public static void initalize(MyQuerry querry)
	{
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Service List",Screen.SERVICE);
		DocumentHistoryTable table=new DocumentHistoryTable();
		DocumentHistoryListForm form=new DocumentHistoryListForm(table);
		DocumentHistorySearchPopUp.staticSuperTable=table;
		DocumentHistorySearchPopUp searchPopUp=new DocumentHistorySearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		DocumentHistoryListPresenter presenter=new DocumentHistoryListPresenter(form, new DocumentHistory(),querry);
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	/*
	 * Method template to set the query object in specific to search criteria and return the same
	 * @return MyQuerry object
	 */
	public MyQuerry getDocumentHistoryQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new DocumentHistory());
		return quer;
	}
	
	  private void reactTo()
	  {
  	  }

}
