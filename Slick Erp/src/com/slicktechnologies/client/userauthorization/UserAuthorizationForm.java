package com.slicktechnologies.client.userauthorization;

import java.util.ArrayList;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.view.client.RangeChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.role.UserRole;

public class UserAuthorizationForm extends FormTableScreen<UserRole> implements
		ClickHandler, ChangeHandler {

	final GenricServiceAsync async = GWT.create(GenricService.class);
	/** The tb role. */
	TextBox tbRole;

	/** The tadescription. */
	TextArea tadescription;

	/** The status. */
	CheckBox status;

	ObjectListBox<Type> oblDocumentName;
	ObjectListBox<ConfigCategory> oblModuleName;
	Button addScreens;

	/** The table. */
	AuthorizationTable table;
	
	UserRole userRoleObj;
	
	
	final static GeneralServiceAsync generalasync=GWT.create(GeneralService.class);
	final GWTCAlert alert = new GWTCAlert();
	public UserAuthorizationForm(SuperTable<UserRole> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		table.connectToLocal();
	}

	public UserAuthorizationForm(SuperTable<UserRole> table, int mode,
			boolean captionmode, boolean popupFlag) {
		super(table, mode, captionmode,popupFlag);
		createGui();
		table.connectToLocal();
	}
	
	private void initalizeWidget() {
		tbRole = new TextBox();
		tadescription = new TextArea();
		status = new CheckBox();
		status.setValue(true);
		status.addClickHandler(this);

		oblModuleName = new ObjectListBox<ConfigCategory>();
		oblDocumentName = new ObjectListBox<Type>();
		CategoryTypeFactory.initiateOneManyFunctionality(this.oblModuleName,this.oblDocumentName);
		AppUtility.makeTypeListBoxLive(this.oblDocumentName, Screen.MODULENAME);
		
		/**
		 * @author Anil @since 20-07-2021
		 * issue raised by rahul for akanomas
		 * new button functiality for document name
		 */
		oblModuleName.addChangeHandler(this);

		addScreens = new Button("Add");
		addScreens.addClickHandler(this);
		table = new AuthorizationTable();

	}


	/************************************* Create Screen *******************************************/
	@Override
	public void createScreen() {

		initalizeWidget();

		this.processlevelBarNames = new String[] { "New" };

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();

		FormField fgroupingRoleInfo = fbuilder.setlabel("Role Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();

		fbuilder = new FormFieldBuilder("* Role", tbRole);
		FormField ftbRole = fbuilder.setMandatory(true).setColSpan(0)
				.setMandatoryMsg("Role is Mandatory").build();

		fbuilder = new FormFieldBuilder("Description", tadescription);
		FormField ftaDescription = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(2).build();

		fbuilder = new FormFieldBuilder("Status", status);
		FormField fcheckBoxStatus = fbuilder.setRowSpan(0).setColSpan(0)
				.build();

		FormField fgroupingUserAuthorization = fbuilder
				.setlabel("Screen Authorization")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("Module Name", oblModuleName);
		FormField ftbinteractionModuleName = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Document Type", oblDocumentName);
		FormField ftbinteractionDocument = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", addScreens);
		FormField ftbuserauthorizationButton = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", table.getTable());
		FormField ftable = fbuilder.setMandatory(true).setColSpan(4)
				.setMandatoryMsg("Authorization is Mandatory").build();

		FormField[][] formfield = {
				{ fgroupingRoleInfo },
				{ ftbRole, fcheckBoxStatus },
				{ ftaDescription },
				{ fgroupingUserAuthorization },
				{ ftbinteractionModuleName, ftbinteractionDocument,
						ftbuserauthorizationButton }, { ftable }, };
		this.fields = formfield;
	}

	// *************************************Create Screen
	// End*******************************************/
	/**************************************** Update Model **********************************************/
	@Override
	public void updateModel(UserRole model) {
		if (tbRole.getValue() != null)
			model.setRoleName(tbRole.getValue());
		if (tadescription.getValue() != null)
			model.setRoleDescription(tadescription.getValue());
		if (status.getValue() != null)
			model.setRoleStatus(status.getValue());

		List<ScreenAuthorization> daysalloc = table.getDataprovider().getList();
		ArrayList<ScreenAuthorization> alloc = new ArrayList<ScreenAuthorization>();
		alloc.addAll(daysalloc);
		model.setAuthorization(alloc);

		
		userRoleObj=model;
		presenter.setModel(model);
	}

	// **************************************** Update Model End
	// **********************************************/

	/**************************************** Update View **********************************************/
	@Override
	public void updateView(UserRole view) {
		userRoleObj=view;
		if (view.getRoleName() != null)
			tbRole.setValue(view.getRoleName());
		if (view.getRoleDescription() != null)
			tadescription.setValue(view.getRoleDescription());
		if (view.getRoleStatus() != null)
			status.setValue(view.getRoleStatus());
		if (view.getAuthorization() != null)
			table.setValue(view.getAuthorization());
		presenter.setModel(view);

	}

	// **************************************** Update view end
	// **********************************************/

	
	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new UserRole();
		model=userRoleObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.USERAUTHORIZATION, userRoleObj.getCount(), null,null,null, false, model, null);
	
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(userRoleObj!=null){
			SuperModel model=new UserRole();
			model=userRoleObj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.USERAUTHORIZATION, userRoleObj.getCount(), null,null,null, false, model, null);
		
		}
	}
	
	
	
	/***************************************** Setup System Menu *****************************************/
	@Override
	public void toggleAppHeaderBarMenu() {

		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
//					.getMenuLabels();
			
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			System.out.println("isPopUpAppMenubar() "+isPopUpAppMenubar());
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")
						|| text.contains("Sillenium Test")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
//					.getMenuLabels();
			
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			System.out.println("isPopUpAppMenubar() "+isPopUpAppMenubar());
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
//					.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			System.out.println("isPopUpAppMenubar() "+isPopUpAppMenubar());
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Edit") || text.contains("Discard")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.USERAUTHORIZE,LoginPresenter.currentModule.trim());

	}

	// ***************************************** System Menu END
	// *****************************************/
	String roleName="";
	private void checkUserRoleAssignedStatus(String role) {
		Filter temp = new Filter();
		temp.setQuerryString("role.roleName");
		temp.setStringValue(role);

		MyQuerry querry = new MyQuerry();
		querry.getFilters().add(temp);
		querry.setQuerryObject(new User());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						showDialogMessage("An Unexpected Error occured !");
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for (SuperModel smodel : result) {
							User comentity = (User) smodel;
							roleName = comentity.getRole().getRoleName();
							System.out.println("Role Name is : " + roleName);
							final GWTCAlert alert = new GWTCAlert();
							alert.alert("Role Is Assigend To User Can't Be Inactive Now !");
							status.setValue(true);
						}
					}
				});
	}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource() == status) {
			boolean isStatusActive = this.status.getValue();
			String name=this.tbRole.getValue();
			System.out.println("Status  :"+isStatusActive);
			System.out.println("Role Name :"+name);
			if (isStatusActive == false) {
				checkUserRoleAssignedStatus(name);
			}
		}

		if (event.getSource() == addScreens) {
			String moduleName = this.oblModuleName.getValue();
			String docType ="";
			if(this.oblDocumentName.getSelectedIndex()==0)
				docType=null;
			else				
			    docType =oblDocumentName.getItemText(this.oblDocumentName.getSelectedIndex());

			if (moduleName == null && docType != null) {
				final GWTCAlert alert = new GWTCAlert();
				alert.alert("Please Select Module !");
			}

			if (moduleName == null && docType == null) {
				final GWTCAlert alert = new GWTCAlert();
				alert.alert("Please Select Module !");
			}
			if (moduleName != null && docType == null) {
			
				for (int i = 1; i < oblDocumentName.getItemCount(); i++) {
					ScreenAuthorization screeObj = new ScreenAuthorization();
					screeObj.setModule(oblModuleName.getValue());
					screeObj.setScreens(oblDocumentName.getValue(i));
					screeObj.setCreate(true);
					screeObj.setView(true);
					screeObj.setDownload(true);
					boolean validateScreen = checkDuplicate(oblDocumentName.getValue(i),oblModuleName.getValue());
					
					if (validateScreen == false) {
						final GWTCAlert alert = new GWTCAlert();
						alert.alert(oblDocumentName.getValue(i)
								+ " Already added!");
					} else {
						table.getDataprovider().getList().add(screeObj);
					}
				}
			}
			if (moduleName != null && docType != null) {
				
				ScreenAuthorization screeObj = new ScreenAuthorization();
				screeObj.setModule(oblModuleName.getValue());
				screeObj.setScreens(docType);//oblDocumentName.getValue()
				screeObj.setCreate(true);
				screeObj.setView(true);
				screeObj.setDownload(true);

				boolean validateScreen = checkDuplicate(docType,oblModuleName.getValue());
				if (validateScreen == false) {
					
					alert.alert(docType + " Already added!");
					oblDocumentName.setValue(null);
				} else {
					if(moduleName.equals("EVA Pedio")&& docType.equals("Audit")) {
						generalasync.checkIfLicensePresent(userRoleObj.getCompanyId(), AppConstants.LICENSETYPELIST.AUDITLICENSE, new AsyncCallback<Boolean>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								
							}

							@Override
							public void onSuccess(Boolean result) {
								// TODO Auto-generated method stub
								if(result){
									ScreenAuthorization screeObj = new ScreenAuthorization();
									screeObj.setModule(oblModuleName.getValue());
									screeObj.setScreens(oblDocumentName.getItemText(oblDocumentName.getSelectedIndex()));//oblDocumentName.getValue()
									screeObj.setCreate(true);
									screeObj.setView(true);
									screeObj.setDownload(true);
									table.getDataprovider().getList().add(screeObj);
								}									
								else
									alert.alert("Company does not have audit license! This screen cannot be added.");
							}
						});
						
					}else
						table.getDataprovider().getList().add(screeObj);
					
				}
			}

		}

	}

	public boolean checkDuplicate(String name,String moduleName) {
		List<ScreenAuthorization> listOfAuthorization = this.getTable().getDataprovider().getList();
		for (int i = 0; i < listOfAuthorization.size(); i++) {
			if (listOfAuthorization.get(i).getScreens().equals(name)&&listOfAuthorization.get(i).getModule().equals(moduleName)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean validate() {
		if (table.getDataprovider().getList().size() == 0) {
			showDialogMessage("Please Add Authorization !");
			return false;
		}

		boolean superValidate = super.validate();
		if (superValidate == false) {
			return false;
		}

		return true;
	}

	/*********************************** Getter and setter for fields ****************************************/
	public TextBox getTbRole() {
		return tbRole;
	}

	public void setTbRole(TextBox tbRole) {
		this.tbRole = tbRole;
	}

	public TextArea getTadescription() {
		return tadescription;
	}

	public void setTadescription(TextArea tadescription) {
		this.tadescription = tadescription;
	}

	public CheckBox getStatus() {
		return status;
	}

	public void setStatus(CheckBox status) {
		this.status = status;
	}

	public ObjectListBox<Type> getOblDocumentName() {
		return oblDocumentName;
	}

	public void setOblDocumentName(ObjectListBox<Type> oblDocumentName) {
		this.oblDocumentName = oblDocumentName;
	}

	public ObjectListBox<ConfigCategory> getOblModuleName() {
		return oblModuleName;
	}

	public void setOblModuleName(ObjectListBox<ConfigCategory> oblModuleName) {
		this.oblModuleName = oblModuleName;
	}

	// Authorization Table
	public AuthorizationTable getTable() {
		return table;
	}

	public void setTable(AuthorizationTable table) {
		this.table = table;
	}

	// *************************************getter setter end
	// **************************************/

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		table.setEnable(state);
	}

	@Override
	public void clear() {
		super.clear();
		table.connectToLocal();
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(oblModuleName)){
			if(oblModuleName.getSelectedIndex()!=0){
				ConfigCategory cat=oblModuleName.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(oblDocumentName, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
	}

}
