package com.slicktechnologies.client.userauthorization;

import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.shared.common.role.UserRole;

public class UserAuthorizationPresenter extends FormTableScreenPresenter<UserRole> {
	UserAuthorizationForm form;
/*****************************Constructor*******************************/
	public UserAuthorizationPresenter(FormTableScreen<UserRole> view,UserRole model) {
		super(view, model);
		form = (UserAuthorizationForm) view;
		form.getSupertable().connectToLocal();
		form.getTable().connectToLocal();
		form.retriveTable(getUserQuery());
		form.setPresenter(this);
	}

	public static UserAuthorizationForm initalize() {
		UserAuthorizeTable gentableScreen = new UserAuthorizeTable();
		UserAuthorizationForm form = new UserAuthorizationForm(gentableScreen,FormTableScreen.UPPER_MODE, true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();

		UserAuthorizationPresenter presenter = new UserAuthorizationPresenter(form, new UserRole());
		AppMemory.getAppMemory().stickPnel(form);
		return form;

	}

	public MyQuerry getUserQuery() {
		MyQuerry quer = new MyQuerry(new Vector<Filter>(), new UserRole());
		return quer;
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals("New")) {
			form.setToNewState();
			initalize();
		}
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void makeNewModel() {
		model = new UserRole();
	}
	
	public void setModel(UserRole entity) {
		model = entity;
	}

	/**
	 * The Class UserAuthorizationPresenterTable.
	 */
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.role.UserRole")
	public static class UserAuthorizationPresenterTable extends SuperTable<UserRole> implements GeneratedVariableRefrence {
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.slicktechnologies.client.utility.GeneratedVariableRefrence#getVarRef
		 * (java.lang.String)
		 */
		@Override
		public Object getVarRef(String varName) {

			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.simplesoftwares.client.library.appstructure.SuperTable#createTable
		 * ()
		 */
		@Override
		public void createTable() {

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.simplesoftwares.client.library.appstructure.SuperTable#
		 * initializekeyprovider()
		 */
		@Override
		protected void initializekeyprovider() {

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.simplesoftwares.client.library.appstructure.SuperTable#
		 * addFieldUpdater()
		 */
		@Override
		public void addFieldUpdater() {

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.simplesoftwares.client.library.appstructure.SuperTable#setEnable
		 * (boolean)
		 */
		@Override
		public void setEnable(boolean state) {

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.simplesoftwares.client.library.appstructure.ViewContainer#applyStyle
		 * ()
		 */
		@Override
		public void applyStyle() {

		}
	};

	/**
	 * The Class AuthorizationPresenterSearch.
	 */
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.role.UserRole")
	public static class UserAuthorizationPresenterSearch extends SearchPopUpScreen<UserRole> {
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.simplesoftwares.client.library.appstructure.SearchPopUpScreen
		 * #getQuerry()
		 */
		@Override
		public MyQuerry getQuerry() {

			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
	};

	/**
	 * React to.
	 */
	private void reactTo() {
	}

}
