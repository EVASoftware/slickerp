package com.slicktechnologies.client.userauthorization;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;

public class AuthorizationTable extends SuperTable<ScreenAuthorization> {

	TextColumn<ScreenAuthorization> screenColumn;
	Column<ScreenAuthorization, Boolean> download;
	TextColumn<ScreenAuthorization> view;
	Column<ScreenAuthorization, Boolean> create;
	Column<ScreenAuthorization, Boolean> edit;
	Column<ScreenAuthorization, Boolean> approve;
	TextColumn<ScreenAuthorization> moduleColumn;
	TextColumn<ScreenAuthorization> viewDownload;
	TextColumn<ScreenAuthorization> viewCreate;
	TextColumn<ScreenAuthorization> viewEdit;
	
	/**
	 * Developer : Rahul Verma
	 * Dated : 27 Nov 2017
	 * Decription : This is to provide specific screen to android screen 
	 */
	Column<ScreenAuthorization, Boolean> android;
	TextColumn<ScreenAuthorization> viewAndroid;
	/**
	 * Ends For Rahul Verma
	 */
	
	
	private Column<ScreenAuthorization, String> delete;
	
	UserAuthorizationForm form;

	@Override
	public void createTable() {
		addColumngetModule();
		addColumngetScreens();
		addColumnisView();
		addColumnisCreate();
		setFieldUpdaterOnCreate();
		addColumnDownload();
		setFieldUpdaterOnDownload();
		addColumnisAndroid();
		setFieldUpdaterOnAndroid();
		addColumnDelete();
		setFieldUpdaterOnDelete();
		
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		this.table.setHeight("500px");

	}
	
	/**
	 * Dev : Rahul Verma
	 * Dated : 27 Nov 2017
	 * Description : This is updater for android screen selections
	 */
	private void setFieldUpdaterOnAndroid() {
		android.setFieldUpdater(new FieldUpdater<ScreenAuthorization, Boolean>() {
			@Override
			public void update(int index, ScreenAuthorization object, Boolean value) {
				object.setAndroid(value);
				getDataprovider().getList();
				table.redrawRow(index);
			}
		});
	}
	/**
	 *	Ends for Rahul Verma
	 */

	/**
	 * Dev : Rahul Verma
	 * Dated : 27 Nov 2017
	 * Description : This method contains check box for availability of Android Screen
	 */
	private void addColumnisAndroid() {
		// TODO Auto-generated method stub

		CheckboxCell cb = new CheckboxCell();
		android = new Column<ScreenAuthorization, Boolean>(cb) {

			@Override
			public Boolean getValue(ScreenAuthorization object) {
				return object.isAndroid();
			}
		};
		table.addColumn(android, "Android Screen");
	
	}
	/**
	 * Ends For Rahul Verma
	 */
	private void addViewColumn()
	{
		addColumngetModule();
		addColumngetScreens();
		addColumnisView();
		addColumnisViewCreate();
		addColumnisViewDownload();
		addColumnisViewAndroid();
//		addColumnDownload();
	}
	/**
	 * Dev : Rahul Verma
	 * Dated : 27 Nov 2017
	 * Decription : This is for viewing purpose of android field
	 */
	private void addColumnisViewAndroid() {
		// TODO Auto-generated method stub

		
		viewAndroid = new TextColumn<ScreenAuthorization>() {
			@Override
			public String getValue(ScreenAuthorization object) {
				return object.isAndroid()+"";
			}
		};
		table.addColumn(viewAndroid, "Android Screen");
		
	
	}
	/**
	 * Ends for RahulVerma
	 */

	
	private void addeditColumn()
	{
		addColumngetModule();
		addColumngetScreens();
		addColumnisView();
		addColumnisCreate();
		setFieldUpdaterOnCreate();
		addColumnDownload();
		setFieldUpdaterOnDownload();
		addColumnisAndroid();
		setFieldUpdaterOnAndroid();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	}
	
	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
	}
	
	
	protected void addColumngetModule() {
		moduleColumn = new TextColumn<ScreenAuthorization>() {
			@Override
			public String getValue(ScreenAuthorization object) {
				if (object.getModule() == null)
					return "N.A";
				else
					return object.getModule().toString();
			}
		};
		table.addColumn(moduleColumn, "Module");
		table.setColumnWidth(screenColumn, 200, Unit.PX);
	}
	
	
	protected void addColumngetScreens() {
		screenColumn = new TextColumn<ScreenAuthorization>() {
			@Override
			public String getValue(ScreenAuthorization object) {
				if (object.getScreens() == null)
					return "N.A";
				else
					return object.getScreens().toString();
			}
		};
		table.addColumn(screenColumn, "Screen");
		table.setColumnWidth(screenColumn, 200, Unit.PX);
	}
	
	protected void addColumnisView() {
		
		view = new TextColumn<ScreenAuthorization>() {
			@Override
			public String getValue(ScreenAuthorization object) {
				return object.isView()+" ";
			}
		};
		table.addColumn(view, "View");
		
	}
	
	protected void addColumnisViewCreate() {
		
		viewCreate = new TextColumn<ScreenAuthorization>() {
			@Override
			public String getValue(ScreenAuthorization object) {
				return object.isCreate()+"";
			}
		};
		table.addColumn(viewCreate, "Create/Edit");
		
	}
	
	protected void addColumnisViewDownload() {
		
		viewDownload = new TextColumn<ScreenAuthorization>() {
			@Override
			public String getValue(ScreenAuthorization object) {
				return object.isDownload()+"";
			}
		};
		table.addColumn(viewDownload, "Download");
		
	}

	protected void addColumnisCreate() {
		CheckboxCell cb = new CheckboxCell();
		create = new Column<ScreenAuthorization, Boolean>(cb) {

			@Override
			public Boolean getValue(ScreenAuthorization object) {
				return object.isCreate();
			}
		};
		table.addColumn(create, "Create/Edit");
	}
	
	protected void setFieldUpdaterOnCreate()
	{
		create.setFieldUpdater(new FieldUpdater<ScreenAuthorization, Boolean>() {
			@Override
			public void update(int index, ScreenAuthorization object, Boolean value) {
				object.setCreate(value);
				getDataprovider().getList();
				table.redrawRow(index);
			}
		});
	}
	
	private void addColumnDownload() {
		CheckboxCell cb = new CheckboxCell();
		download = new Column<ScreenAuthorization, Boolean>(cb) {

			@Override
			public Boolean getValue(ScreenAuthorization object) {
				return object.isDownload();
			}
		};
		table.addColumn(download, "Download");

	}
	
	protected void setFieldUpdaterOnDownload()
	{
		download.setFieldUpdater(new FieldUpdater<ScreenAuthorization, Boolean>() {
			
			@Override
			public void update(int index, ScreenAuthorization object, Boolean value) {
				object.setDownload(value);
				getDataprovider().getList();
				table.redrawRow(index);
			}
		});
	}
	
	protected void addColumnDelete()
	{
		ButtonCell btnCell= new ButtonCell();
		delete = new Column<ScreenAuthorization, String>(btnCell) {
			@Override
			public String getValue(ScreenAuthorization object) {
				return "Delete";
			}
		};
		table.addColumn(delete,"Delete");
	}

	protected void setFieldUpdaterOnDelete()
	{
		delete.setFieldUpdater(new FieldUpdater<ScreenAuthorization, String>() {
			@Override
			public void update(int index, ScreenAuthorization object, String value) {
				getDataprovider().getList().remove(index);
				table.redrawRow(index);
			}
		});
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<ScreenAuthorization>() {
			@Override
			public Object getKey(ScreenAuthorization item) {
				if (item == null) {
					return null;
				} else
					return 10;
			}
		};

	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public boolean validate() {
		return super.validate();
	}



	@Override
	public void applyStyle() {

	}

	public Object getVarRef(String varName) {
		if (varName.equals("screenColumn"))
			return this.screenColumn;
		if (varName.equals("create"))
			return this.create;
		if (varName.equals("view"))
			return this.view;
		if (varName.equals("moduleColumn"))
			return this.moduleColumn;

		return null;
	}


}
