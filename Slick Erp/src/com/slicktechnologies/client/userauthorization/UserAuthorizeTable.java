package com.slicktechnologies.client.userauthorization;

import java.util.Date;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.role.UserRole;

public class UserAuthorizeTable extends SuperTable<UserRole> {
	
	TextColumn<UserRole> getUserRoleColumn;
	TextColumn<UserRole> getStatusColumn;
	
	public UserAuthorizeTable() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetUserRole();
		addColumngetStatus();
	}
	
	
	protected void addColumngetUserRole()
	{
		getUserRoleColumn=new TextColumn<UserRole>()
				{
			@Override
			public String getValue(UserRole object)
			{
				return object.getRoleName();
			}
				};
		table.addColumn(getUserRoleColumn,"User Role");
	}
	
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<UserRole>()
				{
			@Override
			public String getValue(UserRole object)
			{
				if(object.getRoleStatus()==true){
					return "ACTIVE";
				}
				else{
					return "INACTIVE";
				}
			}
				};
		table.addColumn(getStatusColumn,"Status");
	}
	

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<UserRole>() {
			@Override
			public Object getKey(UserRole item) {
				if(item==null)
					return null;
				else
					return new Date().getTime();
			}
		};
	
	}

	@Override
	public void addFieldUpdater() {
	}


	@Override
	public void applyStyle() {
		
	}
	
	@Override
	public void setEnable(boolean state) {
	}


	
	
	

}
