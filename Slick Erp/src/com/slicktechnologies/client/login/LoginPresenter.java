
package com.slicktechnologies.client.login;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.web.bindery.autobean.vm.Configuration;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.libservice.LoginService;
import com.simplesoftwares.client.library.libservice.LoginServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.NameService;
import com.slicktechnologies.client.NameServiceAsync;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.DropDownService;
import com.slicktechnologies.client.services.DropDownServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.IPAddressService;
import com.slicktechnologies.client.services.IPAddressServiceAsync;
import com.slicktechnologies.client.services.LogoutService;
import com.slicktechnologies.client.services.LogoutServiceAsync;
import com.slicktechnologies.client.services.SessionService;
import com.slicktechnologies.client.services.SessionServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConnector;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.complaindashboard.complainHomePresenter;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.customerDetails.CustomerDetailsPresenter;
import com.slicktechnologies.client.views.customerlist.CustomerListGridPresenter;
import com.slicktechnologies.client.views.freshdeskhome.FreshDeskHomeScreen;
import com.slicktechnologies.client.views.history.History;
import com.slicktechnologies.client.views.home.HomePresenter;
import com.slicktechnologies.client.views.humanresource.statuteryreport.StatuteryReportForm;
import com.slicktechnologies.client.views.humanresource.statuteryreport.StatuteryReportPresenter;
import com.slicktechnologies.client.views.implementation.ImplementationPresenter;
import com.slicktechnologies.client.views.interaction.interactiondashboard.InteractionDashboardPresentor;
import com.slicktechnologies.client.views.popups.LoadingPopup;
import com.slicktechnologies.client.views.popups.NewPasswordResetPopUp;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.client.views.summaryscreen.SummaryForm;
import com.slicktechnologies.client.views.summaryscreen.SummaryPresenter;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.RoleDefinition;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.IPAddressAuthorization;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.helperlayer.LoginProxy;
import com.slicktechnologies.shared.common.helperlayer.SessionManagement;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.unitconversion.UnitConversion;
import com.slicktechnologies.shared.screenmenuconfiguration.ScreenMenuConfiguration;

public class LoginPresenter implements ClickHandler,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9183041119548599118L;
	
	public static LoginForm login;
	LoginProxy model;
	public static User myUserEntity =null;
	

	/**
	 * This class hold the login details of customer when he logged in to the system.
	 * Date 20-09-2016 By Anil
	 * Release 30 Sept 2016
	 */
	public static CustomerUser myCustUserEntity =null;
	/**
	 * End
	 */
//  rohan added this
	
	public static boolean branchRestrictionFlag= false;
	
	static ConditionDialogBox conditionPopup=new ConditionDialogBox("Number of users currently loggedin exceeds the limit of number of licenses"+"\n"+"Do you want to continue?",AppConstants.YES,AppConstants.NO);
	public static PopupPanel panelforConditionPopup;
	static ConditionDialogBox conditionPopup1=new ConditionDialogBox("The same has been reported to 'EVA SOFTWARE SOLUTIONS' "+"\n"+"Do you still want to continue?",AppConstants.YES,AppConstants.NO);
	public static PopupPanel panelforConditionPopup1;
	/****************************************RPC Calls***********************************/
	
	private final static GenricServiceAsync async=GWT.create(GenricService.class);
	private final static DropDownServiceAsync dpservice=GWT.create(DropDownService.class);
	private static final LoginServiceAsync loginservice=GWT.create(LoginService.class);
	public final static IPAddressServiceAsync ipserviceAsync = com.google.gwt.core.client.GWT.create(IPAddressService.class);
	public final static EmailServiceAsync emailService=GWT.create(EmailService.class);
	private final static SessionServiceAsync sessionService=GWT.create(SessionService.class);
	private static LogoutServiceAsync logoutserviceasync=GWT.create(LogoutService.class);
	public final static GeneralServiceAsync generalservice=GWT.create(GeneralService.class);
	
	public final static CommonServiceAsync commonservice=GWT.create(CommonService.class);

	/**************************************************************************************/
	
	static GWTCGlassPanel glassPanel = new GWTCGlassPanel();
	public static String loggedInUser="";
	public static String loggedInUserCallerId=""; //Ashwini Patil Date:11-10-2024
	public static String currentModule="Home";
	public static String proxyString = "";
	public static Timer sessionTimeoutResponseTimer;
	public static int timeoutCicle;
	public static int sessionCountValue=0;
	public static String sessionIdbyServer = null;
	public static boolean loggedInCustomer=false;
	/** @Sheetal:21-02-2022
	 *   Des : Added this field for showing erp license expiry date in footer panel part**/
	public static String endDate="";
	public static String tempendDate="";
	
	
	//  rohan added this field for maintaining those many number of sessions = number of users   
	public static int NumberOfUser=0;
	
	
	/**
	 *   Arraylist for Drop Down Entities. These lists are filled when login is successful.
	 */
	
	public static ArrayList<Config> globalConfig=new ArrayList<Config>();
	public static ArrayList<ConfigCategory> globalCategory=new ArrayList<ConfigCategory>();
	public static ArrayList<Type> globalType=new ArrayList<Type>();
	public static ArrayList<Employee> globalEmployee=new ArrayList<Employee>();
	public static ArrayList<Branch> globalBranch=new ArrayList<Branch>();
	public static ArrayList<Country> globalCountry=new ArrayList<Country>();
	public static ArrayList<State> globalState=new ArrayList<State>();
	public static ArrayList<City> globalCity=new ArrayList<City>();
	public static ArrayList<Locality> globalLocality=new ArrayList<Locality>();
	public static ArrayList<ProcessConfiguration> globalProcessConfig=new ArrayList<ProcessConfiguration>();
	public static ArrayList<ProcessName> globalProcessName=new ArrayList<ProcessName>();
	public static ArrayList<MultilevelApproval> globalMultilevelApproval=new ArrayList<MultilevelApproval>();
	public static HashMap<String, String> branchWiseNumberRangeMap=new HashMap<String, String>();//Ashwini Patil Date:29-05-2024 for ultima search
	/**
	 * 	Rohan added this list for loading all branches in customer screen irrespective of branch level restriction
	 * date : 20/09/2016
	 */

	public static ArrayList<Branch> customerScreenBranchList=new ArrayList<Branch>();
	
	/**
	 * ends here 
	 */
	
	/**
	 * Date : 14-02-2017 By ANil
	 * 
	 */
	public static ArrayList<RoleDefinition> globalRoleDefinition=new ArrayList<RoleDefinition>();
	/**
	 * End
	 */
	
	/**
	 * rohan added this for new logic of session 
	 * Date : 12/11/2016
	 */
	
	static ConditionDialogBox sessionLoginConditionPopup=new ConditionDialogBox("Same user has logged in to other terminal. Do you want to still login? Other terminal session will be terminated automatically.",AppConstants.YES,AppConstants.NO);
	static PopupPanel sessionLoginPanel ;
//	static LoggedIn loginUser=null;
	static List<LoggedIn> activeUsers ;
	/**
	 * ends here 
	 */
	
	public static ArrayList<History> globalHistoryList=new ArrayList<History>();
	public static PopupPanel panel;
	public static boolean historyFlag=false;
	
	public static PopupPanel communicationLogPanel;
	
	/**
	 * defined by anil for forgot passward
	 * date :28-09-2016 
	 */
		public String companyName;
		
		
	/**
	 * Date 15 jun 2017 added by vijay for calling BHASH SMS API 	
	 */
	public static boolean bhashSMSFlag = false;	
	
	
	/**
	 * Date :10-04-2018 BY ANIL
	 * Taking static variable company
	 */
	
	public static Company company;
	
	/**
	 * End
	 */
	
	/**
	 * Date 11-05-2018 
	 * Developer :Vijay
	 * Des :- This is for loading all employee for branch level restriction for Sales Person Drop Down with Process Config
	 * Requirements : NBHC
	 */
	public static ArrayList<Employee> allEmployeeList=new ArrayList<Employee>();
	public static boolean loadAllSalesPersonflag = false;

	/**
	 * ends here
	 */
	
	/**
	 * Date : 31-05-2018 By ANIL
	 * Loading all Overtime at the time of login for time report OT Calculation
	 * For Sasha ERP
	 */
	public static ArrayList<Overtime> globalOvertimeList=new ArrayList<Overtime>();
	
	/**
	 * nidhi
	 * 8-08-2018
	 * for map serial no and model no 
	 */
	public static boolean mapModelSerialNoFlag= false;
	/**************************************Constructor*****************************************/
	/**
	 * nidhi
	 * 21-09-2018
	 * @param view
	 * @param model
	 */
	public static boolean areaWiseCalRestricFlg = false;
	/**
	 *  	/**
	 * nidhi
	 * 11-09-2018
	 */
	public static ArrayList<UnitConversion> prodUnitConverList  = new ArrayList<UnitConversion>();
	public static ArrayList<UnitConversion> areaUnitConverList  = new ArrayList<UnitConversion>();
	public static ArrayList<Config> unitForProduct = new ArrayList<Config>();
	public static ArrayList<Config> unitForService = new ArrayList<Config>();
	public static boolean billofMaterialActive = false;
	public static ArrayList<ItemProduct> itemProList = new ArrayList<ItemProduct>();
	public static ArrayList<ProductGroupDetails> proGroupList = new ArrayList<ProductGroupDetails>();
	/**************************************Constructor*****************************************/
	
	/**
	 * nidhi
	 * 11-09-2018
	 * @param view for make bill of material global
	 * @param model
	 */

	
	
	/**
	 * Date :- 01-11-2018
	 * Des :- to load Large customer data using recursive method for customer composite load
	 */
	public static boolean loadLargeCustomerDataFlag=false;
	public static long customerCount=0;
	public static LoadingPopup loadingPopup = new LoadingPopup();
	public static GWTCGlassPanel gwtGlassPanel = new GWTCGlassPanel();
	public static PopupPanel loadingPanel;

	/**
	 * ends here
	 */
	
	/**
	 * @author Vijay Chougule
	 * Date :- 11-Dec-2018
	 * Des :- For NBHC Inventory Management for Automatic Order Qty 
	 */
	public static boolean automaticPROrderQtyFlag = false;
	/**
	 * ends here
	 */
	
	/**
	 * @author Vijay Chougule
	 * Date :- 18-June-2019
	 * Des :- For NBHC Inventory Management for Lock Seal Loading All warehouses Globally 
	 */
	public static ArrayList<WareHouse> allWarehouselist=new ArrayList<WareHouse>();

	/**
	 * Date 14-08-2019 by Vijay
	 * NBHC CCPM Autoinvoice Process config flag
	 */
	public static boolean autoInvoiceFlag = false;
	
	/**
	 * @author Vijay 
	 * Date :- 19--0-2020 
	 * Des :- HR Project loading globally so it will not take much loading on screen 
	 */
	public static ArrayList<HrProject> globalHRProject=new ArrayList<HrProject>();
	/**
	 * @author Vijay Date 07-01-2021
	 * Des :- Project Allocation employee DropDown loading EmployeeInfo Table Globally.
	 */
	public static ArrayList<EmployeeInfo> globalEmployeeInfo=new ArrayList<EmployeeInfo>();
	public static  ArrayList<String> shiftList = new ArrayList<String>();;
	public static  ArrayList<Shift> globalshiftList = new ArrayList<Shift>();;

	/**
	 * @author Anil
	 * @since 18-12-2020
	 * loading screen menu configuration to show system menus
	 */
	public static ArrayList<ScreenMenuConfiguration> globalScrMenuConf=new ArrayList<ScreenMenuConfiguration>();
	/**
	 * @author Anil
	 * @since 21-12-2020
	 * Capturing current document name for initialization of screen menus
	 */
	public static String currentDocumentName="Home";
	
	public static  ArrayList<TaxDetails> globalTaxList = new ArrayList<TaxDetails>();;
	/**
	 * @author Vijay
	 * Des :- For To send Email loading Contact list 
	 */
	
	public static  ArrayList<ContactPersonIdentification> globalContactpersonlist = new ArrayList<ContactPersonIdentification>();;
	public static  ArrayList<EmailTemplate> globalEmailTemplatelist = new ArrayList<EmailTemplate>();;
	public static  ArrayList<EmailTemplateConfiguration> globalEmailTemplateConfiglist = new ArrayList<EmailTemplateConfiguration>();;
	public static  ArrayList<User> globalUserEntitylist = new ArrayList<User>();;

	public static  ArrayList<SmsTemplate> globalSMSTemplatelist = new ArrayList<SmsTemplate>();;

	public static boolean serviceScheduleNewLogicFlag = false;
	public static boolean  PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag = false;
	public static boolean InactiveUser = false;
	
	public static  ArrayList<CustomerBranchServiceLocation> globalServiceLocationList = new ArrayList<CustomerBranchServiceLocation>();

	static Date erpLicenseStartDate = null;
//	public static NewPasswordResetPopUp  newpasswordResetPopup = new NewPasswordResetPopUp();
	
	public static NewPasswordResetPopUp  newpasswordResetPopup = new NewPasswordResetPopUp();

	public static ArrayList<Calendar> globalCalendarlist = new ArrayList<Calendar>();

	public static String URL = new String();
	
	public static  ArrayList<CustomerBranchDetails> globalCustomerBranchList = new ArrayList<CustomerBranchDetails>();
	public static  ArrayList<Department> globalDepartmentList = new ArrayList<Department>();

	public LoginPresenter(LoginForm view,LoginProxy model)
	{
		login=view;
		this.model=model;
		login.setPresenter(this);
		login.applyHandler(this);
		
		
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		conditionPopup1.getBtnOne().addClickHandler(this);
		conditionPopup1.getBtnTwo().addClickHandler(this);
		
		sessionLoginConditionPopup.getBtnOne().addClickHandler(this);
		sessionLoginConditionPopup.getBtnTwo().addClickHandler(this);
		
		newpasswordResetPopup.getLblOk().addClickHandler(this);
		newpasswordResetPopup.getLblCancel().addClickHandler(this);
		
		login.forgotpasslink.addClickHandler(this);
		
		/**
		 * @author Anil ,Date : 26-04-2019
		 * initialized global bom list
		 * Bug: because of this ,error occurred while click on costing button on service quotationn button
		 * raise by Prasad/Rahul Tiwari for EVA/Ankita
		 */
		ContractPresenter.globalProdBillOfMaterial=new ArrayList<BillOfMaterial>();
	}
	
	/***********************************************************************************************/

	/********************************Getters And Setters***************************************/
	
	public LoginForm getLogin() {
		return login;
	}

	public void setLogin(LoginForm login) {
		this.login = login;
	}

	public LoginProxy getModel() {
		return model;
	}

	public void setModel(LoginProxy model) {
		this.model = model;
	}
	
	/**************************************************************************************/

	@Override
	public void onClick(ClickEvent event) {
	
		if(event.getSource().equals(login.btnSignIn)){
			if(login.btnSignIn.getText().equals("Login")){
			login.btnSignIn.setText("Loading...!"); 
				getlogin();
			}
		}
		if(event.getSource().equals(login.forgotpasslink)){
			System.out.println("Hyper Link");
			
			if(!Slick_Erp.companyname.equals("")){
				companyName=Slick_Erp.companyname;
			}else{
				companyName=Slick_Erp.loginCompanyName;
			}
			System.out.println("Company Name : "+companyName);
			
			
			NameServiceAsync service=GWT.create(NameService.class);
			service.getClientName(companyName, new AsyncCallback<String>() {
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(String result) {
					RootPanel rp=RootPanel.get();
					rp.clear();
					AppUtility.createForgotPasswordScreen(result);					
				}
			});
		}
		
		
		/**
		 * rohan added this code 
		 */
		
		if(event.getSource().equals(sessionLoginConditionPopup.getBtnOne())){
			sessionLoginPanel.hide();
			getloginAfterTerminatingPreviousSession();
		}
		
		if(event.getSource().equals(sessionLoginConditionPopup.getBtnTwo())){
			sessionLoginPanel.hide();
			login.textBoxUsername.setValue("");
			login.textBoxPassword.setValue("");
			login.btnSignIn.setText("Login"); 
		}
		if(event.getSource().equals(newpasswordResetPopup.getLblCancel())){
			newpasswordResetPopup.hidePopUp();
//			logoutserviceasync.doLogout(myUserEntity.getCompanyId(), myUserEntity.getUserName(), "log out", sessionCountValue,new AsyncCallback<User>() {
//	    		@Override
//				public void onFailure(Throwable caught) {
//					caught.printStackTrace();
//				}
//				@Override
//				public void onSuccess(User result) {
//					Window.Location.reload();
//	//				Window.alert(remark);
//				}
//	    });
			
			Window.Location.reload();

		}
		/**@author Sheetal : 09-04-2022**/
		if(event.getSource().equals(newpasswordResetPopup.getLblOk())){
			
		   String newpassword = newpasswordResetPopup.getTbNewPassword().getValue();
		   String confirmPassword = newpasswordResetPopup.getTbConfirmPassword().getValue();
		   
		   if(newpassword!=null&&!newpassword.equals("")&&confirmPassword!=null&&!confirmPassword.equals("")) {
		       if(isValidPassword(confirmPassword)){
			      myUserEntity.setPassword(confirmPassword);
			 //sheetal
				  async.save(myUserEntity, new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) {
						// TODO Auto-generated method stub
					
						
//					    logoutserviceasync.doLogout(myUserEntity.getCompanyId(), myUserEntity.getUserName(), "log out", sessionCountValue,new AsyncCallback<User>() {
//					    		@Override
//								public void onFailure(Throwable caught) {
//									caught.printStackTrace();
//								}
//								@Override
//								public void onSuccess(User result) {
//									
//								}
//					    });
						emailService.sendEmailOnPasswordReset(myUserEntity,  myUserEntity.getCompanyId(), new AsyncCallback<String>(){
							
									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										
									}

									@Override
									public void onSuccess(String result) {
										// TODO Auto-generated method stub
										
										newpasswordResetPopup.showDialogMessage("Your password has been resetted succesfully!!");
										newpasswordResetPopup.hidePopUp();

										login.textBoxUsername.setValue("");
										login.textBoxPassword.setValue("");
										login.btnSignIn.setText("Login"); 
										
									
									}
							
								});
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
		   }
		   }else {
			   newpasswordResetPopup.showDialogMessage("Please Enter Password !!");
		   }
		}
		/**
		 * ends here   
		 */
	}
	
	public static boolean isValidPassword(String password){
		
//		String password1 = newpasswordResetPopup.getTbNewPassword().getValue();
		boolean isValid = true;
		
//		if(newpasswordResetPopup.getTbNewPassword().getValue().equalsIgnoreCase(myUserEntity.getPassword())){
//			 newpasswordResetPopup.showDialogMessage("New password cannot be the same as the previous password");
//			 isValid = false;
//			 return isValid;
//		}
		 if(!password.equals(newpasswordResetPopup.getTbNewPassword().getValue())){
       	  newpasswordResetPopup.showDialogMessage("Password and Confirmed password are not equal"); 
       	  isValid = false;
			  return isValid;
         }
		 
		if(password.length() < 8){
			 newpasswordResetPopup.showDialogMessage("Password must be at least eight characters in length");
			isValid = false;
			return isValid;
		}
		
		String upperCaseChars = "(.*[A-Z].*)";
		if (!password.matches(upperCaseChars)){
			 newpasswordResetPopup.showDialogMessage("Password must have atleast one uppercase character");
			isValid = false;
			return isValid;
		}
		 String lowerCaseChars = "(.*[a-z].*)";
		  if (!password.matches(lowerCaseChars)){
			  newpasswordResetPopup.showDialogMessage("Password must have atleast one lowercase character"); 
			  isValid = false;
			  return isValid;
		  }
		  String numbers = "(.*[0-9].*)";
		  if (!password.matches(numbers)){
			  newpasswordResetPopup.showDialogMessage("Password must have atleast one number"); 
			  isValid = false;
			  return isValid;
		  }
         
		return isValid;
          
	}

	public static void showHomeScreen()
	{
		RootPanel rp=RootPanel.get();
		rp.clear();
		AppConnector.initalizes();
		AppMemory mem=AppMemory.getAppMemory();
		mem.initialingapp();
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("DashBoard/Home", Screen.HOME);
		HomePresenter.initalize();
	}
	
	
	public static void getlogin()
	{
		
		System.out.println("In side login");
		final LoginProxy proxylogin=login.updateModelFromView();
		final String uName=proxylogin.getUserId().trim();
		final String pass=proxylogin.getPassword().trim();
		
		loginservice.giveLogin(proxylogin, new AsyncCallback<User>() {

			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				
			}

			@Override
			public void onSuccess(final User result) {
				
				
				erpLicenseStartDate = null;
				//  rohan added this code for removing multiple clicks Date : 29-03-2017
				if(result==null || !result.getStatus())
				{
					login.btnSignIn.setText("Login"); 
//					String url=Window.Location.getHref();
//					String[] spliturl=url.split("\\.");
//					String companyname=spliturl[0];
//					companyname=companyname.replace("http://","");
//					getInValidCredentials(companyname);
//					
//					Window.alert("Invalid Login Credentials!");
//					login.textBoxUsername.setValue("");
//					login.textBoxPassword.setValue("");
					giveCustomerLogin(proxylogin,result);
				}
				else
				{
					//  rohan added this code for finding number of users and used AccessLevel as carrier for this dt: 09-05-2017
					NumberOfUser= Integer.parseInt(result.getAccessLevel());
					/**
					 * Date : 04-12-2017 BY ANIL
					 * Checking Company's end date
					 */
//					Date todaydate = new Date();
//					if(result.getLicenseEndDate()!=null&&todaydate.after(result.getLicenseEndDate())){
//						Window.alert("License expired please contact support@evasoftwaresolutions.com");
//						login.textBoxUsername.setValue("");
//						login.textBoxPassword.setValue("");
//						Console.log("Expiring Validation");
//						login.btnSignIn.setText("Login"); 
//						return;
//					}
//					
					Console.log("Number Of User ==="+NumberOfUser);
					ipserviceAsync.retrieveClientIP(result.getCompanyId(), new AsyncCallback<String>() {
						
						@Override
						public void onFailure(Throwable caught) {
							
						}

						@Override
						public void onSuccess(String resultRetrieved) {
							if(resultRetrieved!=null)
							{
								Console.log("Client Data"+resultRetrieved);
								String companyEmail="";
								String retrievedClientsIp="";
								String[] dataRetrieved=resultRetrieved.split("-");
								Console.log("S1"+dataRetrieved[0]);
								Console.log("S2"+dataRetrieved[1]);
								Console.log("S3"+dataRetrieved[2]);
								if(!dataRetrieved[2].trim().equals("IGNORE")){
									companyEmail=dataRetrieved[2];
								}
								if(dataRetrieved[1].equals(AppConstants.YES))
								{
									retrievedClientsIp=dataRetrieved[0];
									validateIPNow(retrievedClientsIp,result.getCompanyId(),companyEmail,result,uName,pass);
								}
								if(dataRetrieved[1].trim().equals(AppConstants.NO))
								{
								
									ipserviceAsync.retrieveUserIP(result.getCompanyId(), new AsyncCallback<String>() {
									
										
										@Override
										public void onFailure(Throwable caught) {
											System.out.println("Failed----------");
										}
										@Override
										public void onSuccess(String retrievedUserIp) {
											if(retrievedUserIp!=null){
												
												Console.log("Client Data"+retrievedUserIp);
//												String retrievedUserIp=;
//												String[] dataRetrieved=allresult.split("-");
//												Console.log("U1"+dataRetrieved[0]);
//												Console.log("U2"+dataRetrieved[1]);
												
												/**@author sheetal : 15-04-2022
												 * Des : for validating IP address**/
												UserIPValidaton(retrievedUserIp,result);
												/**end**/
												
												/**
												 * @author Anil, Date : 10-09-2019
												 */
												myUserEntity =result;
												loggedInUser=result.getEmployeeName().trim();
												
												if(result.getCallerId()!=null)
													loggedInUserCallerId=result.getCallerId();
												proxyString = result.getUserName();

												myUserEntity =result;
												validateUserIPNow(retrievedUserIp,result.getCompanyId(),result,uName);
												
//												if(dataRetrieved[1].equals(AppConstants.YES))
//												{
//													Console.log("IP Authorization No && user level IP yes");
//													
//													/**
//													 * @author Anil, Date : 10-09-2019
//													 */
//													myUserEntity =result;
//													loggedInUser=result.getEmployeeName().trim();
//													proxyString = result.getUserName();
//
//													retrievedUserIp=dataRetrieved[0];
//													validateUserIPNow(retrievedUserIp,result.getCompanyId(),result,uName);
//													
//												}
//												if(dataRetrieved[1].trim().equals(AppConstants.NO))
//												{
//													Console.log("IP Authorization No && user level IP NO");
//
//													loggedInUser=result.getEmployeeName().trim();
//													proxyString = result.getUserName();
//													
//													myUserEntity =result;
//													
////													/**
////													 *  Rohan Bhagde. 
////													 *    rohan commented this code as logic changes for session
////													 *    Date : 
////													 */
////													sessionService.getSessionArrayFromServer(myUserEntity , new AsyncCallback<Boolean>() {
////														
////														@Override
////														public void onFailure(Throwable caught) {
////															
////														}
////
////														@Override
////														public void onSuccess(Boolean result) {
////															
////															System.out.println("Rohan in side success"+result);
////															if(result)
////															{
////																validateLicenseDate(myUserEntity.getCompanyId(),myUserEntity);
////															}
////															else
////															{
////																Window.alert("User already LoggedIn ....!!");
////																login.textBoxUsername.setValue("");
////																login.textBoxPassword.setValue("");
////																Console.log("Expiring Validation");
////															}
////														}
////													});
//													
//													
////													/**
////													 * rohan added this code for new session logic 
////													 */
////													
//														getLoggedInStatusByUserEntity(myUserEntity);
////													
////													/**
////													 * ends here 
////													 */
////													
//										//***********rohan uncommented this for your to login to mainn panel **********			
////													UserConfiguration.initUserConfiguration(result,result.getCompanyId());
////													System.out.println("Sucess Full----");
////													Console.log("Success");
////													loggedInUser=result.getEmployeeName().trim();
//////													showHomeScreen();
////													RootPanel rp=RootPanel.get();
////													rp.clear();
////													AppConnector.initalizes();
////													AppMemory mem=AppMemory.getAppMemory();
////													mem.initialingapp();
////													for(int i=0;i<mem.skeleton.menuLabels.length;i++){
////														mem.skeleton.menuLabels[i].setVisible(false);
////													}
////													
////													
//////													   rohan addded this code for branch level restriction 
////													
////													loginservice.getBranchLevelRestriction(myUserEntity, new AsyncCallback<Employee>()
////															{
////
////																@Override
////																public void onFailure(Throwable caught) {
////																	
////																}
////
////																@Override
////																public void onSuccess(Employee result) {
////																	
////																	
////																	if(result!=null)
////																	{
////																		System.out.println("user role "+myUserEntity.getRole().getRoleName());
////																		if(!myUserEntity.getRole().getRoleName().equals("ADMIN"))
////																		{
////																		
////																			branchRestrictionFlag = true;
////																		
////																			System.out.println("rohan get employee in return listSize"+result.getEmpBranchList().size());	
////																			
////																			try {
////																				globalDataRetrievalWithBranchRestriction(result.getEmpBranchList(),result.getBranchName());
////																			} catch (Exception e) {
////																				e.printStackTrace();
////																			}
////																		}
////																		else
////																		{
////																			branchRestrictionFlag = false;
////																			try {
////																				globalDataRetrieval();
////																			} catch (Exception e) {
////																				e.printStackTrace();
////																			}
////																		}
////																	}
////																	else
////																	{
////																		
////																		branchRestrictionFlag = false;
////																		try {
////																			globalDataRetrieval();
////																		} catch (Exception e) {
////																			e.printStackTrace();
////																		}
////																	}
////																}
////
////															});
////													//  changes ends here 
////													
////
////													Timer timer = new Timer() {
////														
////														@Override
////														public void run() {
////														
////															InteractionDashboardPresentor.initialize();
////														}
////													};
////													 timer.schedule(4000); 
//												
//												}
												
											}
										}
									
								
										
									});
										
										
										
									
//									loggedInUser=result.getEmployeeName().trim();
//									proxyString = result.getUserName();
////									validateLicenseDate(result.getCompanyId(),result);
//						//***********rohan uncommented this for your to login to mainn panel **********			
//									UserConfiguration.initUserConfiguration(result,result.getCompanyId());
//									System.out.println("Sucess Full----");
//									Console.log("Success");
//									loggedInUser=result.getEmployeeName().trim();
////									showHomeScreen();
//									RootPanel rp=RootPanel.get();
//									rp.clear();
//									AppConnector.initalizes();
//									AppMemory mem=AppMemory.getAppMemory();
//									mem.initialingapp();
//									for(int i=0;i<mem.skeleton.menuLabels.length;i++){
//										mem.skeleton.menuLabels[i].setVisible(false);
//									}
//									
//									try {
//										globalDataRetrieval();
//									} catch (Exception e) {
//										e.printStackTrace();
//									}
//									
//									Timer timer = new Timer() {
//										
//										@Override
//										public void run() {
//										
//											complainHomePresenter.initialize();
//										}
//									};
//									 timer.schedule(4000); 
//									
//					//******************************changes ends here *************************				
								}
								
							}
						}
					});
				}
			}
		});
	}
	//sheetal
	private static void UserIPValidaton(final String retrievedUserIp, final User userEntity) {
		Console.log("name==" +userEntity.getEmployeeName());
		Console.log("ip==" +userEntity.getLoginIPAddress());
		Console.log("retrievedUserIp" +retrievedUserIp);

//			List<String> userIPAddresslist = new ArrayList<String>();
//			if(result.getIpAddress()!=null){
//					userIPAddresslist.addAll(result.getIpAddress());
//			}
//			Console.log("userIPAddresslist==" +userIPAddresslist.size());
	          boolean flag = false;
				if(userEntity.getLoginIPAddress()==null || userEntity.getLoginIPAddress().equals("")) {
//					List<String> listofIPaddress = new ArrayList<String>(Arrays.asList(retrievedUserIp));
//					Console.log("listofIPaddress==" +listofIPaddress.size());
//					result.setIpAddress(listofIPaddress);
//					Console.log("If there is no ip defined method==" +listofIPaddress.size());
					userEntity.setLoginIPAddress(retrievedUserIp);
					userEntity.getIpAddress().clear();
					
					async.save(userEntity, new AsyncCallback<ReturnFromServer>() {

						@Override
						public void onFailure(Throwable arg0) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onSuccess(ReturnFromServer arg0) {
							// TODO Auto-generated method stub
							
						}
					});
					flag=true;
					
				}
				else{
					if(userEntity.getLoginIPAddress()!=null && !userEntity.getLoginIPAddress().equals("")){
						if(userEntity.getLoginIPAddress().trim().equals(retrievedUserIp.trim())){
							 flag=true;
						}
					}
				}
			
//					for(int i=0;i<userIPAddresslist.size();i++){
//										
//						if(userIPAddresslist.get(i).equals(retrievedUserIp)){
//							 Console.log("retrieved IP =="+retrievedUserIp);
//								 Console.log("User IP =="+userIPAddresslist.get(i));
//								 flag=true;
//							 }
//						}
									
						
				
				if(!flag) {
					Console.log("If ip address is not matched=="+flag);
					Window.alert("You are trying to login from different IP address");
					/**
					 * @author Vijay Date - 29-09-2023
					 * Des :- added process configuration to disable IP address email
					 */
					
					Vector<Filter> filtrvec = new Vector<Filter>();
					Filter filtr = null;
					
					filtr = new Filter();
					filtr.setQuerryString("companyId");
					filtr.setLongValue(userEntity.getCompanyId());
					filtrvec.add(filtr);
					
					filtr = new Filter();
					filtr.setQuerryString("processName");
					filtr.setStringValue("Company");
					filtrvec.add(filtr);
					
					MyQuerry querry = new MyQuerry();
					querry.setFilters(filtrvec);
					querry.setQuerryObject(new ProcessConfiguration());
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {

							Console.log("IP Email address process coonfiguration "+result.size());
							boolean flag = false;
							if(result.size()!=0){
							for(SuperModel model : result){
								ProcessConfiguration processConfig = (ProcessConfiguration) model;
								for(ProcessTypeDetails processType : processConfig.getProcessList()){
									if(processType.getProcessName().equals("Company")
									   &&processType.getProcessType().equalsIgnoreCase(AppConstants.PC_DISABLEIPADDRESSEMAIL)
									   &&processType.isStatus()){
										flag = true;
									}
								}
							}
							}
							Console.log("IP Email address process coonfiguration flag "+flag); 
							if(!flag){
								IPAddressAlertMail(userEntity.getUserName(),retrievedUserIp,userEntity.getCompanyId());

							}

						
							userEntity.setLoginIPAddress(retrievedUserIp);
							
//							List<String> listofIPaddress = new ArrayList<String>(Arrays.asList(retrievedUserIp));
//							Console.log("listofIPaddress==" +listofIPaddress.size());
//							result.setIpAddress(listofIPaddress);
//							Console.log("If there is no ip defined method =" +listofIPaddress.size());
							async.save(userEntity, new AsyncCallback<ReturnFromServer>() {

								@Override
								public void onFailure(Throwable arg0) {
									// TODO Auto-generated method stub
									
								}

								@Override
								public void onSuccess(ReturnFromServer arg0) {
									// TODO Auto-generated method stub
									
								}
							});
							
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
					});
					
					
					/**
					 * ends here
					 */
					
				}
				
			}
			
		
//		List<String> userIPAddresslist = new ArrayList<String>();
//		Console.log("user ip address==" +myUserEntity.getIpAddress());
//		
//		userIPAddresslist.addAll(myUserEntity.getIpAddress());
//		Console.log("Inside UserIPValidaton method==" +userIPAddresslist.size());
//					
							
	

private static void validateUserIPNow(final String retrievedUserIp,Long companyId,final User userentity,String uName) {
		
//		System.out.println("NAME+++"+uName);
//		
//		MyQuerry querry = new MyQuerry();
//		Vector<Filter> filtervec = new Vector<Filter>();
//		
//		Filter filter = new Filter();
//		filter.setQuerryString("companyId");
//		filter.setLongValue(companyId);
//		filtervec.add(filter);
//		
//		filter = new Filter();
//		filter.setQuerryString("userName");
//		filter.setStringValue(uName);
//		filtervec.add(filter);
//		
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new User());
//		
//		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				
//				List<String> userIPAddresslist = new ArrayList<String>();
//
//				boolean flag = false;
//				
//				for(SuperModel model:result){
//					
//					User user = (User) model;
//					if(user.getIpAddress()!=null && user.getIpAddress().size()>0)
//					userIPAddresslist.addAll(user.getIpAddress());
//				}
//				
//				System.out.println("SIZE=="+userIPAddresslist.size());
//				for(int i=0;i<userIPAddresslist.size();i++){
//					
//					if(userIPAddresslist.get(i).equals(retrievedUserIp)){
//						Console.log("User IP =="+userIPAddresslist.get(i));
//						flag=true;
//					}
//					
//				}
//				/**
//				 * @author Vijay
//				 * Des :- As per Nitin sir if ip adderess not added then dont allow user to login 
//				 */
//				if(userIPAddresslist.size()==0){
//					flag=true;
//				}
//				
//				if(flag){
//					
//					
//					getLoggedInStatusByUserEntity(myUserEntity);
//					
//
//				} else {
//						Window.alert("You are trying an unauthorized access");
//						login.textBoxUsername.setValue("");
//						login.textBoxPassword.setValue("");
//						login.btnSignIn.setText("Login"); 
//				}
//			}
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				System.out.println("Failed");
//			}
//			
//			
//		});
	
	
			boolean flag = false;

			List<String> userIPAddresslist = new ArrayList<String>();
			if(userentity.getIpAddress()!=null && userentity.getIpAddress().size()>0){
				userIPAddresslist.addAll(userentity.getIpAddress());
			}
			
			if(userIPAddresslist.size()>0){
				for(int i=0;i<userIPAddresslist.size();i++){
					
					if(userIPAddresslist.get(i).equals(retrievedUserIp)){
						Console.log("User IP =="+userIPAddresslist.get(i));
						flag=true;
					}
					
				}
			}
			else{
				/**
				 * @author Vijay
				 * Des :- As per Nitin sir if ip adderess not added in the user entity for ip restriction
				 * then allow user to login 
				 */
					flag=true;
			}
			
			if(flag){
				
				
				getLoggedInStatusByUserEntity(myUserEntity);
				
		
			} else {
					Window.alert("You are trying to login from unauthorised IP address");//old msg "You are trying an unauthorized access"
					login.textBoxUsername.setValue("");
					login.textBoxPassword.setValue("");
					login.btnSignIn.setText("Login"); 
			}
			
		
	}
	
	
	
	/**
	 * Global Data Retrieval Method for adding data to corressponding arraylists.
	 * 
	 */
	
	public static void globalDataRetrieval() throws Exception
	{
		System.out.println("Rohan in side old data retrive method ");
		Console.log("Data Retrieval");
		
				Company c=new Company();
				
				/**
				 * @author Vijay Date 15-06-2021
				 * Des :- below method called from Appmemory after enableMenuBar prcoess config value set
				 * to resolve UI issues
				 */
//				/**
//				 * @author Anil
//				 * @since 18-12-2020
//				 */
//				String screenMenuConfg=AppConstants.SCREENMENUCONFIGURATION+"-"+c.getCompanyId();
//				loadSystemConfigurationMenu(screenMenuConfg);
				
//				/**
//				 * Date 11-05-2018 
//				 * Developer :Vijay
//				 * Requirements : NBHC
//				 */
//				String empType1=AppConstants.GLOBALRETRIEVALEMPLOYEE+"-"+c.getCompanyId();
//				loadAllEmployeeForBranchLevelRestriction(empType1);
//				/**
//				 * ends here
//				 */
				String configType=AppConstants.GLOBALRETRIEVALCONFIG+"-"+c.getCompanyId();
				globalMakeLiveConfig(configType);
		//		creatingLoopTimer();
				String categoryType=AppConstants.GLOBALRETRIEVALCATEGORY+"-"+c.getCompanyId();
				globalMakeLiveCategory(categoryType);
		//		creatingLoopTimer();
				String typeType=AppConstants.GLOBALRETRIEVALTYPE+"-"+c.getCompanyId();
				globalMakeLiveType(typeType);
		//		creatingLoopTimer();
				String empType=AppConstants.GLOBALRETRIEVALEMPLOYEE+"-"+c.getCompanyId();
				globalMakeLiveEmployee(empType);
		//		creatingLoopTimer();
				String branchType=AppConstants.GLOBALRETRIEVALBRANCH+"-"+c.getCompanyId();
				globalMakeLiveBranch(branchType);
		//		creatingLoopTimer();
				String countryType=AppConstants.GLOBALRETRIEVALCOUNTRY+"-"+c.getCompanyId();
				globalMakeLiveCountry(countryType);
		//		creatingLoopTimer();
				String stateType=AppConstants.GLOBALRETRIEVALSTATE+"-"+c.getCompanyId();
				globalMakeLiveState(stateType);
//				creatingLoopTimer();
				String cityType=AppConstants.GLOBALRETRIEVALCITY+"-"+c.getCompanyId();
				globalMakeLiveCity(cityType);
//				creatingLoopTimer();
				String localityType=AppConstants.GLOBALRETRIEVALLOCALITY+"-"+c.getCompanyId();
				globalMakeLiveLocality(localityType);
//				creatingLoopTimer();
				String processConfigType=AppConstants.GLOBALRETRIEVALPROCESSCONFIG+"-"+c.getCompanyId();
				globalMakeLiveProcessConfig(processConfigType);
				String processNameType=AppConstants.GLOBALRETRIEVALPROCESSNAME+"-"+c.getCompanyId();
				globalMakeLiveProcessName(processNameType);
				String processMultiLevel=AppConstants.GLOBALRETRIEVALMULTILEVELAPPROVAL+"-"+c.getCompanyId();
				globalMakeLiveMultiLevelApproval(processMultiLevel);
				
				String processRoleDefinition=AppConstants.GLOBALRETRIEVALROLEDEFINITION+"-"+c.getCompanyId();
				globalMakeLiveRoleDefinition(processRoleDefinition);
				
				/**
				 * @author Vijay Chougule
				 * Date :- 19-July-2020
				 * Des :- For HRProject Dropdown HRProject loading globaly
				 */
				String processHRProject=AppConstants.GLOBALHRPROJECT+"-"+c.getCompanyId();
				globalMakeLiveHRProject(processHRProject);
				
				/**
				 * Date 23 Feb 2017
				 * added by vijay
				 * when we login then globalcustomerlist clear from here. and global customerlist is initialized when we click first time in customerlist screen
				 */
				CustomerListGridPresenter.globalCustomerlist.clear();
				/**
				 * end here
				 */
				
				/**
				 * Date : 31-05-2018 By ANIL
				 * Loading all overtime
				 */
				String processOvertime=AppConstants.GLOBALRETRIEVALOVERTIME+"-"+c.getCompanyId();
				globalMakeLiveOvertime(processOvertime);
				
//				/**
//				 * @author Vijay Chougule
//				 * Date :- 19-July-2020
//				 * Des :- For HRProject Dropdown HRProject loading globaly
//				 */
//				String processHRProject=AppConstants.GLOBALHRPROJECT+"-"+c.getCompanyId();
//				globalMakeLiveHRProject(processHRProject);
				
				globalLoadTaxDetails();
				loadContactPersonDetailslist();
				loadEmailTemplatelist();
				loadEmailTemplateConfigurationlist();
				loadUserEntitylist();
				
				loadSMSTemplatelist();
				loadCalendarList();

	}

	
	

	//   This method is create by  Rohan  on 20/7/2016 
	//   used for branch level restriction 
	//  this method load employee branches to global config of branch
	
	

	private static void globalDataRetrievalWithBranchRestriction(List<EmployeeBranch> empBranchList,String empBranch) {
		/**
		 * Date : 27-02-2017 By ANIL
		 * 
		 */
		HashSet<String> hset=new HashSet<String>();
		for(EmployeeBranch eb:empBranchList){
			hset.add(eb.getBranchName());
		}
		hset.add(empBranch);
		List<String> branchList=new ArrayList<String>(hset);
		/**
		 * End
		 */
		
		Company c=new Company();
		
		/**
		 * @author Vijay Date 15-06-2021
		 * Des :- below method called from Appmemory after enableMenuBar prcoess config value set
		 * to resolve UI issues
		 */
//		/**
//		 * @author Anil
//		 * @since 18-12-2020
//		 */
//		String screenMenuConfg=AppConstants.SCREENMENUCONFIGURATION+"-"+c.getCompanyId();
//		loadSystemConfigurationMenu(screenMenuConfg);
		
//		/**
//		 * Date 11-05-2018 
//		 * Developer :Vijay
//		 * Requirements : NBHC
//		 */
//		String empType=AppConstants.GLOBALRETRIEVALEMPLOYEE+"-"+c.getCompanyId();
//		loadAllEmployeeForBranchLevelRestriction(empType);
//		/**
//		 * ends here
//		 */
		String configType=AppConstants.GLOBALRETRIEVALCONFIG+"-"+c.getCompanyId();
		globalMakeLiveConfig(configType);
//		creatingLoopTimer();
		String categoryType=AppConstants.GLOBALRETRIEVALCATEGORY+"-"+c.getCompanyId();
		globalMakeLiveCategory(categoryType);
//		creatingLoopTimer();
		String typeType=AppConstants.GLOBALRETRIEVALTYPE+"-"+c.getCompanyId();
		globalMakeLiveType(typeType);
//		creatingLoopTimer();
		/**
		 * Old Code
		 */
//		String empType=AppConstants.GLOBALRETRIEVALEMPLOYEE+"-"+c.getCompanyId();
//		globalMakeLiveEmployee(empType);
		/**
		 * End
		 */
		
		/**
		 * Date : 27-02-2017 by ANIL
		 */
		globalMakeLiveEmployee(branchList, c.getCompanyId());
		/**
		 * End
		 */
//		creatingLoopTimer();
		String branchType=AppConstants.GLOBALRETRIEVALBRANCH+"-"+c.getCompanyId();
		/**
		 * Date 02-02-2018 By Vijay
		 * below old commented by vijay because branch user getting issue unable to update Branch NBHC
		 * below new code added
		 */
//		globalMakeLiveWithBranchRestriction(empBranchList,empBranch);
//		globalMakeLiveCustomerScreenBranchDropDown(branchType);
		
		
		globalMakeLiveCustomerScreenBranchDropDown(branchType,empBranchList,empBranch);
		
		/**
		 * ends here
		 */
		
		
//		creatingLoopTimer();
		String countryType=AppConstants.GLOBALRETRIEVALCOUNTRY+"-"+c.getCompanyId();
		globalMakeLiveCountry(countryType);
//		creatingLoopTimer();
		String stateType=AppConstants.GLOBALRETRIEVALSTATE+"-"+c.getCompanyId();
		globalMakeLiveState(stateType);
//		creatingLoopTimer();
		String cityType=AppConstants.GLOBALRETRIEVALCITY+"-"+c.getCompanyId();
		globalMakeLiveCity(cityType);
//		creatingLoopTimer();
		String localityType=AppConstants.GLOBALRETRIEVALLOCALITY+"-"+c.getCompanyId();
		globalMakeLiveLocality(localityType);
//		creatingLoopTimer();
		String processConfigType=AppConstants.GLOBALRETRIEVALPROCESSCONFIG+"-"+c.getCompanyId();
		globalMakeLiveProcessConfig(processConfigType);
		String processNameType=AppConstants.GLOBALRETRIEVALPROCESSNAME+"-"+c.getCompanyId();
		globalMakeLiveProcessName(processNameType);
		String processMultiLevel=AppConstants.GLOBALRETRIEVALMULTILEVELAPPROVAL+"-"+c.getCompanyId();
		globalMakeLiveMultiLevelApproval(processMultiLevel);
		
		String processRoleDefinition=AppConstants.GLOBALRETRIEVALROLEDEFINITION+"-"+c.getCompanyId();
		globalMakeLiveRoleDefinition(processRoleDefinition);
		
		/**
		 * Date 23 Feb 2017
		 * added by vijay
		 * when we login then globalcustomerlist clear from here. and global customerlist is initialized when we click first time in customerlist screen
		 */
		CustomerListGridPresenter.globalCustomerlist.clear();
		/**
		 * end here
		 */
		
		/**
		 * Date : 31-05-2018 By ANIL
		 * Loading all overtime
		 */
		String processOvertime=AppConstants.GLOBALRETRIEVALOVERTIME+"-"+c.getCompanyId();
		globalMakeLiveOvertime(processOvertime);
		
		/**
		 * @author Vijay Chougule
		 * Date :- 19-July-2020
		 * Des :- For HRProject Dropdown HRProject loading globaly
		 */
		String processHRProject=AppConstants.GLOBALHRPROJECT+"-"+c.getCompanyId();
		globalMakeLiveHRProject(processHRProject);
		
		globalLoadTaxDetails();
		loadContactPersonDetailslist();
		loadEmailTemplatelist();
		loadEmailTemplateConfigurationlist();
		loadUserEntitylist();
		
		loadSMSTemplatelist();

		loadCalendarList();

	}
	
	
	private static void globalMakeLiveOvertime(final String processOvertime) {
		glassPanel.show();
		Timer timer = new Timer() {
			@Override
			public void run() {
				dpservice.getDropDown(processOvertime,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
						glassPanel.hide();
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						globalOvertimeList.clear();
						for (SuperModel model : result) {
							Overtime ot = (Overtime) model;
							globalOvertimeList.add(ot);
						}
						Console.log("Role Definition Size"+ globalOvertimeList.size());
						glassPanel.hide();
					}
				});
			}
		};
		timer.schedule(3000);
	}
	
	/**
	 * Date 11-05-2018 
	 * Developer :Vijay
	 * Des :- This is for loading all employee for branch level restriction for Sales Person Drop Down with Process Config
	 * Requirements : NBHC
	 */
	public static void loadAllEmployeeForBranchLevelRestriction(final String empType) {
		
//		MyQuerry querry = new MyQuerry();
//		Company c = new Company();
//
//		Vector<Filter> filtervec = new Vector<Filter>();
//		Filter filter = null;
//		filter = new Filter();
//		filter.setQuerryString("companyId");
//		filter.setLongValue(c.getCompanyId());
//		filtervec.add(filter);
//
//		filter = new Filter();
//		filter.setQuerryString("processName");
//		filter.setStringValue("Employee");
//		filtervec.add(filter);
//
//		filter = new Filter();
//		filter.setQuerryString("processList.status"); 
//		filter.setBooleanvalue(true);
//		filtervec.add(filter);
//		
//		filter = new Filter();
//		filter.setQuerryString("processList.processName"); 
//		filter.setStringValue("Employee");
//		filtervec.add(filter);
//		
//		filter = new Filter();
//		filter.setQuerryString("processList.processType"); 
//		filter.setStringValue("EnableLoadAllEmployeeWithBranchLevelRestriction");
//		filtervec.add(filter);
//
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new ProcessConfiguration());
//		
//		async.getSearchResult(querry,
//				new AsyncCallback<ArrayList<SuperModel>>() {
//
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						
//						Console.log("proccess result  Size"+ result.size());
//						System.out.println("result ==== __ " + result.size());
//						if (result.size() > 0) {
							
//							loadAllSalesPersonflag = true;
							glassPanel.show();
							Timer timer = new Timer() {
								@Override
								public void run() {
									dpservice.getDropDown(empType,new AsyncCallback<ArrayList<SuperModel>>() {
										@Override
										public void onFailure(Throwable caught) {
											caught.printStackTrace();
											glassPanel.hide();
										}

										@Override
										public void onSuccess(ArrayList<SuperModel> result) {
											allEmployeeList.clear();
											for (SuperModel model : result) {
												Employee employeeEntity = (Employee) model;
												allEmployeeList.add(employeeEntity);
											}
											Console.log("salesPersonEmployee Size"+ allEmployeeList.size());
											glassPanel.hide();
										}
									});
								}
							};
							timer.schedule(3000);

//						}else{
//							loadAllSalesPersonflag = false;
//						}
//					}

//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//						loadAllSalesPersonflag = false;
//					}
//				});
			
	   	 
	}
	/**
	 * ends here
	 */
	
	public static void globalMakeLiveWithBranchRestriction(List<EmployeeBranch> empBranchList,String empBranch) {
		globalBranch.clear();
		if(empBranchList.size()!=0){
		for(int i=0; i< empBranchList.size();i++)
		{
//			Branch branchEntity= new Branch();
//			branchEntity.setBusinessUnitName(empBranchList.get(i).getBranchName());
//			/***
//			 * Date : 12-10-2017 BY ANIL
//			 * branch status is not set
//			 * ideally we should load branches and then we have to add those branches to global branch list
//			 */
//			branchEntity.setstatus(true);
//			/**
//			 * End
//			 */
//			globalBranch.add(branchEntity);
			
			/**
			 * Date 02-02-2018 By Vijay above old commented branch user unable to update branch issue
			 * below new code updated
			 */
			
			for(int j=0;j<customerScreenBranchList.size();j++){
				if(customerScreenBranchList.get(j).getBusinessUnitName().trim().equals(empBranchList.get(i).getBranchName().trim())){
			
					Branch branchEntity= new Branch();
					branchEntity.setBusinessUnitName(customerScreenBranchList.get(j).getBusinessUnitName());
					/***
					 * Date : 12-10-2017 BY ANIL
					 * branch status is not set
					 * ideally we should load branches and then we have to add those branches to global branch list
					 */
					branchEntity.setstatus(customerScreenBranchList.get(j).getstatus());
					/**
					 * End
					 */
					// vijay for nbhc issue unable to save branch by branch user
					branchEntity.setId(customerScreenBranchList.get(j).getId());
					if(customerScreenBranchList.get(j).getEmail()!=null && !customerScreenBranchList.get(j).getEmail().equals(""))
					branchEntity.setEmail(customerScreenBranchList.get(j).getEmail());
				
					globalBranch.add(branchEntity);
				}
			}
			
			/**
			 * ends here
			 */
		}
		}
		else
		{
//			Branch branchEntity= new Branch();
//			branchEntity.setBusinessUnitName(empBranch);
//			/***
//			 * Date : 12-10-2017 BY ANIL
//			 * branch status is not set
//			 * ideally we should load branches and then we have to add those branches to global branch list
//			 */
//			branchEntity.setstatus(true);
//			/**
//			 * End
//			 */
//			globalBranch.add(branchEntity);
			
			/**
			 * Date 02-02-2018 By Vijay above old commented branch user unable to update branch issue
			 * below new code updated
			 */
			
			for(int j=0;j<customerScreenBranchList.size();j++){
				
				if(customerScreenBranchList.get(j).getBusinessUnitName().trim().equals(empBranch.trim())){
					
					Branch branchEntity= new Branch();
					branchEntity.setBusinessUnitName(customerScreenBranchList.get(j).getBusinessUnitName());
					/***
					 * Date : 12-10-2017 BY ANIL
					 * branch status is not set
					 * ideally we should load branches and then we have to add those branches to global branch list
					 */
					branchEntity.setstatus(customerScreenBranchList.get(j).getstatus());
					/**
					 * End
					 */
					// vijay for nbhc issue unable to save branch by branch user
					branchEntity.setId(customerScreenBranchList.get(j).getId());

					globalBranch.add(branchEntity);
				}
			}
			
			/**
			 * ends here
			 */
			
		}
		
		
		System.out.println("Rohan Global Branch list size ="+globalBranch.size());
	}

	/*****************************Validations And Logged In History Maintaining*******************************/
	
	private static void validateIPNow(final String ipAddressOfClient,long companyId,final String emailOfCompany,final User userEntity,final String userName,final String passwd)
	{
		final long compId = companyId;
		
		MyQuerry querry = new MyQuerry();
	  	Filter filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(companyId);
		querry.getFilters().add(filter);
		querry.setQuerryObject(new IPAddressAuthorization());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> lisResult) {
				int cnt=0;
				List<IPAddressAuthorization> ipaddressList= new ArrayList<IPAddressAuthorization>();
				Console.log("result set size in login presenter  +++++++"+lisResult.size());
				for(SuperModel model:lisResult)
				{
					IPAddressAuthorization ipAuthorization=(IPAddressAuthorization)model;
					ipaddressList.add(ipAuthorization);
				}
				Console.log("ipaddressList     "+ipaddressList.size());
				
				for(int i=0;i<ipaddressList.size();i++){
					if(ipaddressList.get(i).isStatus()==true){
						if(ipAddressOfClient.trim().equals(ipaddressList.get(i).getIpAddress().trim())){
							cnt= cnt+1;
							Console.log("client ip form list     "+ipaddressList.get(i));
						}
					}
				}
				
				if(cnt>0)
				{
					

					Console.log("IP Authoriztion Yes && User Wise Checking");

					
					/************************** vijay ****************************/
					
					ipserviceAsync.retrieveUserIP(compId, new AsyncCallback<String>() {
						
						@Override
						public void onFailure(Throwable caught) {
							System.out.println("Failed----------");
						}
						@Override
						public void onSuccess(String allresult) {
							System.out.println("Done ===================");
							if(allresult!=null){
								
								Console.log("Client Data"+allresult);
								String retrievedUserIp="";
								String[] dataRetrieved=allresult.split("-");
								Console.log("U1"+dataRetrieved[0]);
								Console.log("U2"+dataRetrieved[1]);
								
								if(dataRetrieved[1].equals(AppConstants.YES))
								{
									Console.log("IP Authoriztion Yes && User Wise Yes");

									retrievedUserIp=dataRetrieved[0];
									validateUserIPNow(retrievedUserIp,compId,userEntity,userName);
									
								}
								if(dataRetrieved[1].trim().equals(AppConstants.NO))
								{
									Console.log("IP Authoriztion Yes && User Wise No");

									
									loggedInUser=userEntity.getEmployeeName().trim();
									if(userEntity.getCallerId()!=null)
										loggedInUserCallerId=userEntity.getCallerId();
									proxyString = userEntity.getUserName();
									UserConfiguration.initUserConfiguration(userEntity,userEntity.getCompanyId());
									System.out.println("Sucess Full----");
									Console.log("Success");
									loggedInUser=userEntity.getEmployeeName().trim();
									RootPanel rp=RootPanel.get();
									rp.clear();
									AppConnector.initalizes();
									AppMemory mem=AppMemory.getAppMemory();
									mem.initialingapp();
									for(int i=0;i<mem.skeleton.menuLabels.length;i++){
										mem.skeleton.menuLabels[i].setVisible(false);
									}
									
									
									try {
										globalDataRetrieval();
									} catch (Exception e) {
										e.printStackTrace();
									}
									
									Timer timer2 = new Timer() {
										
										@Override
										public void run() {
										
											complainHomePresenter.initialize();
										}
									};
									 timer2.schedule(4000); 
									
								}
								
							}
						}
						
					});
					
					
//					
//					loggedInUser=userEntity.getEmployeeName().trim();
//					proxyString = userEntity.getUserName();
////					validateLicenseDate(userEntity.getCompanyId(),userEntity);
//					
//					
////					UserConfiguration.initUserConfiguration(userEntity,userEntity.getCompanyId());
////					System.out.println("Sucess Full----");
////					Console.log("Success");
////					loggedInUser=userEntity.getEmployeeName().trim();
//////					showHomeScreen();
////					RootPanel rp=RootPanel.get();
////					rp.clear();
////					AppConnector.initalizes();
////					AppMemory mem=AppMemory.getAppMemory();
////					mem.initialingapp();
////					for(int i=0;i<mem.skeleton.menuLabels.length;i++){
////						mem.skeleton.menuLabels[i].setVisible(false);
////					}
////					
//////					CreatedByInfo.setEmpName(result.getEmployeeName());
//////					CreatedByInfo.setUserId(result.getUserName());
////					
////					try {
////						globalDataRetrieval();
////					} catch (Exception e) {
////						e.printStackTrace();
////					}
				}
				else
				{
					Window.alert("You are trying to login from unauthorised IP address");//old msg "You are trying an unauthorized access"
					reactOnIPAddressAlertEmail(userName,passwd,ipAddressOfClient,emailOfCompany);
					login.textBoxUsername.setValue("");
					login.textBoxPassword.setValue("");
					login.btnSignIn.setText("Login"); 
				}
				
				
			}
		});
	}
	
	

	/*************************Validations And Logged In History Maintaining****************************/
	
	/**
	 *  When user enters invalid password or username log in maintained.
	 * @param companyname
	 * @param proxylogin 
	 */

	public static void getInValidCredentials(String companyname, final LoginProxy proxylogin){
		MyQuerry query = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("accessUrl");
		
		/*****************************UNCOMMENT THIS WHILE DEPLOYING************************/
		filter.setStringValue(companyname);
		/*****************************COMMENT THIS WHILE ON LOCAL************************/
//		filter.setStringValue("ganesh");
		
		query.getFilters().add(filter);
		query.setQuerryObject(new Company());
		 async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>()
		{
				@Override
				public void onFailure(Throwable caught) {
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					for(SuperModel model : result){
						
						Company company = (Company)model;
//						saveLoginData(company.getCompanyId(), AppConstants.LOGININVALID, "", login.updateModelFromView().getUserId());
						Console.log("Username == "+proxylogin.getUserId());
						saveLoginData(company.getCompanyId(), AppConstants.LOGININVALID, "", proxylogin.getUserId());

					}
				}
			});
		}
	
	
	
	/*********************************************************************************************/
	/********************************************************************************************/
	/******************************Session Management Code***************************************/
	/**********************************************************************************************/
	
/**********************************Implementing Session Management**************************************/
//****************************rohan commented this below code for removing login validations on 19/8/15 ***	
	public static void validateLicenseDate(final Long companyId,final User userEntity){
		MyQuerry query = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(companyId);
		query.getFilters().add(filter);
		query.setQuerryObject(new Company());
		 async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					for(SuperModel model:result){
						Company c = (Company)model;
						Date todaydate = new Date();
						
						
						
						/**
						 * Date : 06-04-2018 BY ANIL
						 * 
						 */
						System.out.println("LOGIN DATE : "+AppUtility.parseDate(todaydate));
						Console.log("LOGIN DATE : "+AppUtility.parseDate(todaydate));
						ArrayList<LicenseDetails> licenseDetailsList=new ArrayList<LicenseDetails>();
						if(c.getLicenseDetailsList()!=null&&c.getLicenseDetailsList().size()!=0){
							System.out.println("TOTAL EVA LICENSE LIST : "+c.getLicenseDetailsList().size());
							Console.log("TOTAL EVA LICENSE LIST : "+c.getLicenseDetailsList().size());
							for(LicenseDetails license:c.getLicenseDetailsList()){
								//Ashwini Patil Date:12-05-2022 System should accept license name as EVAERPLICENSE also.
								if(AppConstants.LICENSETYPELIST.EVA_ERP_LICENSE.equals(license.getLicenseType())&&license.getStatus()==true||AppConstants.LICENSETYPELIST.EVAERPLICENSE.equals(license.getLicenseType())&&license.getStatus()==true){
									licenseDetailsList.add(license);
								}
							}
							System.out.println("ACTIVE EVE ERP LICENSE LIST SIZE : "+licenseDetailsList.size());
							Console.log("ACTIVE EVE ERP LICENSE LIST SIZE : "+licenseDetailsList.size());
						}
						
						Comparator<LicenseDetails> licDtComp=new Comparator<LicenseDetails>() {
							@Override
							public int compare(LicenseDetails arg0, LicenseDetails arg1) {
								return arg0.getStartDate().compareTo(arg1.getStartDate());
							}
						};
						Collections.sort(licenseDetailsList, licDtComp);
						
						/**
						 * @author Anil
						 * @since 01-07-2020
						 */
						ArrayList<LicenseDetails> contractWiseActiveLicLis=new ArrayList<LicenseDetails>();
						for(LicenseDetails obj:licenseDetailsList){
							if(obj.getContractCount()!=0){
								contractWiseActiveLicLis.add(obj);
							}
						}
						
						ArrayList<LicenseDetails> licenseList=new ArrayList<LicenseDetails>();
						if(contractWiseActiveLicLis.size()!=0){
							licenseList=contractWiseActiveLicLis;
						}else{
							licenseList=licenseDetailsList;
						}
						if(licenseList!=null && licenseList.size()>0){
							erpLicenseStartDate = licenseList.get(0).getStartDate();
						}
						
						int noOfLicense=0;
						int expiredLicenseCount=0;
						int notActiveLicenseCount=0;
						boolean validLicense=false;
						if(licenseList.size()!=0){
							DateTimeFormat sdf = DateTimeFormat.getFormat("dd-MM-yyyy");
							
							for(LicenseDetails license:licenseList){
								System.out.println(license.getLicenseType()+" No. Of License : "+license.getNoOfLicense()+" LICENSE START DATE : "+AppUtility.parseDate(license.getStartDate())+" LICENSE END DATE : "+AppUtility.parseDate(license.getEndDate()));
								Console.log(license.getLicenseType()+" No. Of License : "+license.getNoOfLicense()+" LICENSE START DATE : "+AppUtility.parseDate(license.getStartDate())+" LICENSE END DATE : "+AppUtility.parseDate(license.getEndDate()));
								endDate=AppUtility.parseDate(license.getEndDate());
								Console.log("Today="+sdf.format(todaydate)+"license start day="+sdf.format(license.getStartDate()));
								
								//Ashwini Patil Date:20-12-2022 added one more condition so that license should work from the currect day
								if(todaydate.after(license.getStartDate())||sdf.format(todaydate).equals(sdf.format(license.getStartDate()))){
									/**
									 * @author Anil
									 * @since 05-10-2021
									 * Issue - system is not allowing to login on last date of contract
									 * Raised by Nitin sir for pecopp
									 */
									if(AppUtility.parseDate(todaydate).equals(AppUtility.parseDate(license.getEndDate()))||todaydate.before(license.getEndDate())){
										noOfLicense=noOfLicense+license.getNoOfLicense();
										validLicense=true;
									}else{
										expiredLicenseCount++;
//										System.out.println("License Expired. Please contact support@evasoftwaresolutions.com");
									}
								}else{
									notActiveLicenseCount++;
//									System.out.println("License is not Active. Please contact support@evasoftwaresolutions.com");
								}
							}
							
							/**
							 * for temp use
							 */
//							noOfLicense=2;
//							validLicense=true;
//							NumberOfUser=2;
							/**
							 * END
							 */
							
							
							Console.log("TOTAL NO. OF ACTIVE LICENSE : "+noOfLicense);
							System.out.println("TOTAL NO. OF ACTIVE LICENSE : "+noOfLicense);
							c.setNoOfUser(noOfLicense);
							if(validLicense){
								company=c;
								validateNumberOfUsers(c.getCompanyId(),userEntity,c.getNoOfUser());
							}else if(validLicense==false&&notActiveLicenseCount>0){
								login.textBoxUsername.setValue("");
								login.textBoxPassword.setValue("");
								login.btnSignIn.setText("Login"); 
								Window.alert("License is not Active. Please contact support@evasoftwaresolutions.com");
							}else if(validLicense==false&&expiredLicenseCount>0){
								login.textBoxUsername.setValue("");
								login.textBoxPassword.setValue("");
								login.btnSignIn.setText("Login"); 
								Window.alert("License Expired. Please contact support@evasoftwaresolutions.com");
							}
						}
						
						
						/**
						 * Date :09-04-2018 BY ANIL
						 * Commenting old code for login
						 */
						
//						if(todaydate.after(c.getLicenseEndingDate())){
//							saveLoginData(c.getCompanyId(), AppConstants.LOGINEXPIRYDATE,userEntity.getEmployeeName(),userEntity.getUserName());
//							Window.alert("License expired please contact support@evasoftwaresolutions.com");
//							login.textBoxUsername.setValue("");
//							login.textBoxPassword.setValue("");
//							Console.log("Expiring Validation");
//							login.btnSignIn.setText("Login"); 
//						}
//						else{
//							Console.log("Passed Expiry Validation");
							/**
							 * 
							validateNumberOfUsers(c.getCompanyId(),userEntity,c.getNoOfUser());
							*/
//						}
							
						/**
						 * End	
						 */
						
						boolean updateflag = false;
						if(c.getCompanyURL()==null || c.getCompanyURL().equals("")){
							c.setCompanyURL(getUrlFromwindow());
							updateflag = true;
						}
						else{
							if(c.getCompanyURL().contains("http")){
								c.setCompanyURL(getUrlFromwindow());
								updateflag = true;
							}
						}
						Console.log("updateflag "+updateflag);
						if(updateflag){
							async.save(c, new AsyncCallback<ReturnFromServer>() {
								
								@Override
								public void onSuccess(ReturnFromServer result) {
									// TODO Auto-generated method stub
									
								}
								
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									
								}
							});
						}
						
					}
				}
		 });
	}
	
	
	public static void validateNumberOfUsers(final Long companyId,final User userEntity,final int noOfUser){
		MyQuerry query = new MyQuerry();
		Vector<Filter> filterobj = new Vector<Filter>();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(companyId);
		filterobj.add(filter);
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setStringValue(AppConstants.ACTIVE);																															;
		filterobj.add(filter);
		filter = new Filter();		
		filter.setQuerryString("userName !=");
		filter.setStringValue(userEntity.getUserName().trim());																															;
		filterobj.add(filter);
		query.setFilters(filterobj);
		query.setQuerryObject(new LoggedIn());
		async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
//					if(result.size()>0){
						if(result.size()>=noOfUser){
							//   rohan commented this 
							Window.alert("Number of users currently loggedin exceeds the limit of number of licenses");
							saveLoginData(companyId, AppConstants.LOGINNOOFUSER, userEntity.getEmployeeName().trim(),userEntity.getUserName().trim());
							login.textBoxUsername.setValue("");
							login.textBoxPassword.setValue("");
							login.btnSignIn.setText("Login"); 
							
//							panelforConditionPopup=new PopupPanel(true);
//							panelforConditionPopup.add(conditionPopup);
//							panelforConditionPopup.setGlassEnabled(true);
//							panelforConditionPopup.show();
//							panelforConditionPopup.center();
							
						}
							else{
								
								
								/**@author Sheetal
								 * Des : For resetting password after login***/
								
								if(userEntity.getPassword().length()<8){
					        		newpasswordResetPopup.showPopUp();
					        	}
								else {
									validateCurrentUser(companyId, userEntity, noOfUser);
								}
								
								//   old code 
								//								//   new code 
//								saveLoginData(companyId, AppConstants.LOGINSUCCESS,userEntity.getEmployeeName(), userEntity.getUserName());
//								UserConfiguration.initUserConfiguration(userEntity,companyId);
//								loggedInUser=userEntity.getEmployeeName().trim();
//								
//								RootPanel rp=RootPanel.get();
//								rp.clear();
//								AppConnector.initalizes();
//								AppMemory mem=AppMemory.getAppMemory();
//								mem.initialingapp();
//								for(int i=0;i<mem.skeleton.menuLabels.length;i++){
//									mem.skeleton.menuLabels[i].setVisible(false);
//								}
//								
//								try {
//									
//									globalDataRetrieval();
//								} catch (Exception e) {
//									e.printStackTrace();
//								}
//								
//								/******************* vijay *********************/
//								
//								Timer timer2 = new Timer() {
//									
//									@Override
//									public void run() {
//									
//										complainHomePresenter.initialize();
//									}
//								};
//								 timer2.schedule(4000); 
//								
//							}
//					}
//					else{
//						//  old code 
//						validateCurrentUser(companyId, userEntity, noOfUser);
//						
//////					   new code 
//						saveLoginData(companyId, AppConstants.LOGINSUCCESS,userEntity.getEmployeeName(), userEntity.getUserName());
//						UserConfiguration.initUserConfiguration(userEntity,companyId);
//						loggedInUser=userEntity.getEmployeeName().trim();
//						
//						RootPanel rp=RootPanel.get();
//						rp.clear();
//						AppConnector.initalizes();
//						AppMemory mem=AppMemory.getAppMemory();
//						mem.initialingapp();
//						for(int i=0;i<mem.skeleton.menuLabels.length;i++){
//							mem.skeleton.menuLabels[i].setVisible(false);
//						}
//						
//						try {
//							
//							globalDataRetrieval();
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//						
//						/******************* vijay *********************/
//						
//						Timer timer2 = new Timer() {
//							
//							@Override
//							public void run() {
//							
//								complainHomePresenter.initialize();
//							}
//						};
//						 timer2.schedule(4000); 
					}
					}
				
		 		});
			}
//	
	public static void validateCurrentUser(final Long companyId,final User userEntity,final int noOfUser){
		
//		MyQuerry query = new MyQuerry();
//		Vector<Filter> filterobj = new Vector<Filter>();
//		Filter filter = new Filter();
//		filter.setQuerryString("companyId");
//		filter.setLongValue(companyId);
//		filterobj.add(filter);
//		filter = new Filter();
//		filter.setQuerryString("status");
//		filter.setStringValue(AppConstants.ACTIVE);
//		filterobj.add(filter);
//		filter = new Filter();
//		filter.setQuerryString("userName");
//		filter.setStringValue(userEntity.getUserName().trim());
//		filterobj.add(filter);
//		query.setFilters(filterobj);
//		query.setQuerryObject(new LoggedIn());
//		 async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
//				
//				@Override
//				public void onFailure(Throwable caught) {
//				}
//	
//				@Override
//				public void onSuccess(ArrayList<SuperModel> result) {
//					if(result.size()>0){
//						
//						Window.alert("User already LoggedIn");
//							saveLoginData(companyId, AppConstants.LOGINCURRENTUSER,userEntity.getEmployeeName().trim() ,userEntity.getUserName().trim());
//							login.textBoxUsername.setValue("");
//							login.textBoxPassword.setValue("");
//						}
//							else{
		
								
								saveLoginData(companyId, AppConstants.LOGINSUCCESS,userEntity.getEmployeeName().trim(), userEntity.getUserName().trim());
								UserConfiguration.initUserConfiguration(userEntity,companyId);
								loggedInUser=userEntity.getEmployeeName().trim();
								if(userEntity.getCallerId()!=null)
									loggedInUserCallerId=userEntity.getCallerId();
								RootPanel rp=RootPanel.get();
								rp.clear();
								AppConnector.initalizes();
								AppMemory mem=AppMemory.getAppMemory();
								mem.initialingapp();
								/**
								 * @author Anil
								 * @since 16-09-2020
								 * commented app header button logic here called it in intializeApp method above
								 */
//								for(int i=0;i<mem.skeleton.menuLabels.length;i++){
//									mem.skeleton.menuLabels[i].setVisible(false);
//								}
								
								//   rohan commented this code for branch level restriction 
//								try {
//									
//									globalDataRetrieval();
//								} catch (Exception e) {
//									e.printStackTrace();
//								}
								
								/**
								 * Date 03-11-2018 By Vijay
								 * Des :- Customer composite load with party using process config
								 */
								Company c = new Company();
								checkProceesConfigAndGetCustomerCount(c.getCompanyId());
								/**
								 * ends here
								 */
								
								/**
								 * Date 18-05-2019 by Vijay for service product loading issue so product composite called here initialize to load once login in global array
								 */
								ProductInfoComposite prodInfoComposite=AppUtility.initiateServiceProductComposite(new SuperProduct());
								/**
								 * ends here
								 */
								
								
								deleteInvalidLogin(userEntity.getUserName().trim(),c.getCompanyId());
								
								//   rohan addded this code for branch level restriction 
								loginservice.getBranchLevelRestriction(myUserEntity, new AsyncCallback<Employee>()
								{

									@Override
									public void onFailure(Throwable caught) {
										
									}

									@Override
									public void onSuccess(Employee result) {
										
										
										if(result!=null)
										{
											System.out.println("user role "+myUserEntity.getRole().getRoleName());
											if(!myUserEntity.getRole().getRoleName().equals("ADMIN"))
											{
											
												branchRestrictionFlag = true;
											
												System.out.println("rohan get employee in return listSize"+result.getEmpBranchList().size());	
												
												try {
													globalDataRetrievalWithBranchRestriction(result.getEmpBranchList(),result.getBranchName());
												} catch (Exception e) {
													e.printStackTrace();
												}
											}
											else
											{
												branchRestrictionFlag = false;
												try {
													globalDataRetrieval();
												} catch (Exception e) {
													e.printStackTrace();
												}
											}
										}
										else
										{
											branchRestrictionFlag = false;
											try {
												globalDataRetrieval();
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
										pleaseWaitDataIsLoading();
									}
									
								});
								//  changes ends here 

								Timer timer = new Timer() {
//									
									@Override
									public void run() {
									
										
//										/**@author Sheetal
//										 * Des : For resetting password after login***/
//										resetpassword(userEntity);
//										InteractionDashboardPresentor.initialize();
										/**
										 * @author Abhinav
										 * @since 17/10/2019
										 */
//										CustomerDetailsPresenter.initalize();
										
										
										/** 
										 * @author Vijay  Date 18-11-2021
										 * Des :- when new client sign up then first 30 days 
										 * home screen will show implementation screen
										 */
										Console.log("erpLicenseStartDate "+erpLicenseStartDate);
										Date licenseStartdatePlusThirtyDaysDate = erpLicenseStartDate;
										CalendarUtil.addDaysToDate(licenseStartdatePlusThirtyDaysDate, 31);
										Console.log("LicenseStart Date +30 days Date"+licenseStartdatePlusThirtyDaysDate);
										Date todayDate = new Date();
										if(AppMemory.getAppMemory().enableMenuBar && todayDate.before(licenseStartdatePlusThirtyDaysDate)){
											ImplementationPresenter.initalize();
											if(loadLargeCustomerDataFlag){ //1-12-2023
												PersonInfoComposite person=AppUtility.customerInfoComposite(new Customer());
											}
										}
										/**
										 * ends here
										 */
										else if(AppMemory.getAppMemory().globalDsReportList!=null&&AppMemory.getAppMemory().globalDsReportList.size()!=0&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
											AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Home", Screen.HOME);
											FreshDeskHomeScreen form=new FreshDeskHomeScreen();
											/**
											 * @author Anil @since 20-07-2021
											 * Issue raised by Rahul for surat pest
											 * if GDS report is active and activating large customer flag 
											 * data is not loaded and screen got hang
											 * So Loading here customer data
											 * 
											 */
											if(loadLargeCustomerDataFlag){
												PersonInfoComposite person=AppUtility.customerInfoComposite(new Customer());
											}
										}else if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
											SummaryForm.className="technicianSchedule2";
											SummaryPresenter.initalize();
										}else if (LoginPresenter.myCustUserEntity!=null&&LoginPresenter.myCustUserEntity.getCompanyType().equalsIgnoreCase("Facility Management")) {
											StatuteryReportPresenter.initalize();
											
											if(loadLargeCustomerDataFlag){
												PersonInfoComposite person=AppUtility.customerInfoComposite(new Customer());
											}
										}else{
											//Ashwini Patil Date:30-06-2023 If GDS is not configured in system then old dashboard screen was coming. Nitin sir said to display summary screen in that case
//											CustomerDetailsPresenter.initalize();
											SummaryForm.className="technicianSchedule2";
											SummaryPresenter.initalize();
										}
										
										/**
										 * @author Vijay Chougule
										 * Des :- Loading vendor data globally so here just called the method to load vendor data.
										 */
										PersonInfoComposite	personInfoVenodrComposite = new PersonInfoComposite(new MyQuerry(new Vector<Filter>(), new Vendor()),false);

									}
        //sheetal
//        private void resetpassword(User userEntity) {
//        	if(userEntity.getPassword().equalsIgnoreCase("Pass123")){
//        		newpasswordResetPopup.showPopUp();
//        	}
//	
//	
//        }
								};
								 timer.schedule(18000); 
								
								
//								Timer timer2 = new Timer() {
//									
//									@Override
//									public void run() {
//									
//										complainHomePresenter.initialize();
//									}
//								};
//								 timer2.schedule(4000); 
//							}
//						}
//		 			});
				}
	

	

	public static void pleaseWaitDataIsLoading(){
		final GWTCGlassPanel gwtGlsPanel=new GWTCGlassPanel();
		LoadingPopup loadPopup = new LoadingPopup();
		final PopupPanel loadPanel;
		gwtGlsPanel.show();
		
		loadPanel = new PopupPanel(true);
		loadPanel.add(loadPopup);
		loadPanel.center();
		loadPanel.show();
		Timer timer = new Timer() {
			@Override
			public void run() {
				gwtGlsPanel.hide();
				loadPanel.hide();;
			}
		};
		/**
		 * @author Vijay Date :- 10-02-2022
		 * Des :- if company type is FM Services then increased loading time because globally project not loading in dropdown getting issue so incresed the time for Facility Management
		 */ 
		if(company!=null && company.getCompanyType()!=null && !company.getCompanyType().equals("") && company.getCompanyType().equalsIgnoreCase("FM Services")) {
			timer.schedule(30000);
		}
		else {
			timer.schedule(20000);
		}
	}
//***********************	changes here made by rohan ends here ****** *****************************
	
	private static void initSessionTimers(int sessionCount) {
		
		
		System.out.println("Inside intiate session timers ");
		/**
		 * Developed by : Rohan Bhagde
		 *  1. userName , 2.LoggedIn entity count after successful login , 3.companyId, 4.LoggedIn user
		 */
		
		sessionService.getUserSessionTimeout(proxyString,sessionCount,UserConfiguration.getCompanyId(),loggedInUser,new AsyncCallback<String>() {
		public void onSuccess(String timeout) {
			System.out.println("OnSuccess Time Out"+timeout);
			
			String[] dataRetrieved=timeout.split("-");
			Console.log("timeout"+dataRetrieved[0]);
			Console.log("session id by server "+dataRetrieved[1]);
			
			
			RootPanel rp=RootPanel.get();
			rp.clear();
			AppConnector.initalizes();
			AppMemory mem=AppMemory.getAppMemory();
			mem.initialingapp();
			for(int i=0;i<mem.skeleton.menuLabels.length;i++){
				mem.skeleton.menuLabels[i].setVisible(false);
			}
			
			//   rohan commented this code for branch level restriction 
//			try {
//				
//				globalDataRetrieval();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
			
			
			//   rohan addded this code for branch level restriction 
			loginservice.getBranchLevelRestriction(myUserEntity, new AsyncCallback<Employee>()
					{

						@Override
						public void onFailure(Throwable caught) {
							
						}

						@Override
						public void onSuccess(Employee result) {
							
							
							if(result!=null)
							{
								System.out.println("user role "+myUserEntity.getRole().getRoleName());
								if(!myUserEntity.getRole().getRoleName().equals("ADMIN"))
								{
								
									branchRestrictionFlag = true;
								
									System.out.println("rohan get employee in return listSize"+result.getEmpBranchList().size());	
									
									try {
										globalDataRetrievalWithBranchRestriction(result.getEmpBranchList(),result.getBranchName());
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
								else
								{
									branchRestrictionFlag = false;
									try {
										globalDataRetrieval();
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}
							else
							{
								branchRestrictionFlag = false;
								try {
									globalDataRetrieval();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					});
			//  changes ends here 

			Timer timer = new Timer() {
//				
				@Override
				public void run() {
				
//					InteractionDashboardPresentor.initialize();
					/**
					 * @author Abhinav
					 * @since 17/10/2019
					 */
					CustomerDetailsPresenter.initalize();
					/**
					 * @author Vijay Chougule
					 * Des :- Loading vendor data globally so here just called the method to load vendor data.
					 */
					PersonInfoComposite	personInfoVenodrComposite = new PersonInfoComposite(new MyQuerry(new Vector<Filter>(), new Vendor()),false);

				}
			};
			 timer.schedule(4000); 
			
			
//		timeoutCicle = Integer.parseInt(dataRetrieved[0]);
//		sessionIdbyServer = dataRetrieved[1];
//		sessionTimeoutResponseTimer = new Timer() {
//		@Override
//		public void run() {
//			isSessionAlive(sessionIdbyServer);
//		}
//		};
//		sessionTimeoutResponseTimer.schedule(timeoutCicle);
		}
		 
		public void onFailure(Throwable caught) {
		}
		});
		 
		}
		 
	/**
	 * Developed by : Rohan Bhagde
	 *  1.LoggedIn entity count after successful login, 2. userName , 3.companyId
	 */
//		private static void isSessionAlive(String sessionId) {
//		sessionService.isSessionAlive(sessionId,proxyString,sessionCountValue,UserConfiguration.getCompanyId(),new AsyncCallback<Boolean>() {
//		public void onSuccess(Boolean alive) {
//		if (alive.booleanValue()){
//			sessionTimeoutResponseTimer.cancel();
//			sessionTimeoutResponseTimer.scheduleRepeating(timeoutCicle);
//		} else {
//			
//			displaySessionTimedOut("Session Timed Out On Client Side");
//		}
//		}
//		 
//		public void onFailure(Throwable caught) {
//		//throw custom errors
//		}
//		});
//		 
//		}
		 
		public static void displaySessionTimedOut(final String remark) {
			logoutserviceasync.doLogoutonBrowserClose(UserConfiguration.getCompanyId(),UserConfiguration.getUserconfig().getUser().getUserName().trim(),remark,sessionCountValue,new AsyncCallback<User>() {
				@Override
				public void onFailure(Throwable caught) {
					caught.printStackTrace();
				}
				@Override
				public void onSuccess(User result) {
					Window.Location.reload();
					Window.alert(remark);
				}
			});
		}
		 
//		private void updateSessionLastAccessedTime() {
//		sessionService.ping(proxyStringnew AsyncCallback<Object>() {
//		public void onSuccess(Object paramT) {
//		//do nothing
//		}
//		 
//		public void onFailure(Throwable caught) {
//		//throw custom errors
//		}
//		});
//		}
	
	public static void saveLoginData(final Long companyId,final String type,String employeeName,final String userName){
		
		final LoggedIn loggedInEntity = new LoggedIn();
		
		if(type.equals(AppConstants.LOGININVALID))
		{
			loggedInEntity.setCompanyId(companyId);
			loggedInEntity.setRemark("Invalid Login");
			loggedInEntity.setUserName(userName.trim());
			loggedInEntity.setStatus(AppConstants.INACTIVE);
			loggedInEntity.setLoginDate(new Date());
		}
		
		if(type.equals(AppConstants.LOGINEXPIRYDATE))
		{
			loggedInEntity.setRemark("Exceeds License Expiry Date");
//			loggedInEntity.setLoginDate(GenricServiceImpl.parseDate(new Date()));
//			String strDate=AppUtility.parseDate(new Date());
//			String saveTime=DateTimeFormat.getShortTimeFormat().format(new Date());
//			loggedInEntity.setLoginTime(saveTime);
			loggedInEntity.setCompanyId(companyId);
			loggedInEntity.setEmployeeName(employeeName.trim());
			loggedInEntity.setStatus(AppConstants.INACTIVE);
			loggedInEntity.setUserName(userName.trim());
		}
		
		if(type.equals(AppConstants.LOGINNOOFUSER))
		{
			loggedInEntity.setRemark("Exceeds Number Of Licenses");
//			loggedInEntity.setLoginDate(GenricServiceImpl.parseDate(new Date()));
//			String strDate=AppUtility.parseDate(new Date());
//			String saveTime=DateTimeFormat.getShortTimeFormat().format(new Date());
//			loggedInEntity.setLoginTime(saveTime);
			loggedInEntity.setEmployeeName(employeeName.trim());
			loggedInEntity.setStatus(AppConstants.INACTIVE);
			loggedInEntity.setCompanyId(companyId);
			loggedInEntity.setUserName(userName.trim());
		}
		
		if(type.equals(AppConstants.LOGINSUCCESS))
		{
			loggedInEntity.setRemark("Successful Login");
//			loggedInEntity.setLoginDate(GenricServiceImpl.parseDate(new Date()));
//			String strDate=AppUtility.parseDate(new Date());
//			String saveTime=DateTimeFormat.getShortTimeFormat().format(new Date());
//			loggedInEntity.setLoginTime(saveTime);
			loggedInEntity.setEmployeeName(employeeName.trim());
			loggedInEntity.setStatus(AppConstants.ACTIVE);
			loggedInEntity.setCompanyId(companyId);
			loggedInEntity.setUserName(userName.trim());
		}
		
		
		if(type.equals(AppConstants.LOGINCURRENTUSER))
		{
			loggedInEntity.setRemark("Current User trying to Login again");
//			loggedInEntity.setLoginDate(GenricServiceImpl.parseDate(new Date()));
//			String strDate=AppUtility.parseDate(new Date());
//			String saveTime=DateTimeFormat.getShortTimeFormat().format(new Date());
//			loggedInEntity.setLoginTime(saveTime);
			
			loggedInEntity.setEmployeeName(userName.trim());
			loggedInEntity.setStatus(AppConstants.INACTIVE);
			loggedInEntity.setCompanyId(companyId);
			loggedInEntity.setUserName(userName.trim());
		}
		
		
		async.save(loggedInEntity,new AsyncCallback<ReturnFromServer>(){

			@Override
			public void onFailure(Throwable caught) {
				GWTCAlert alert=new GWTCAlert();
				alert.alert("An unexpected error occurred!");
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				if(loggedInEntity.getStatus().trim().equals(AppConstants.ACTIVE))
				{
					sessionCountValue=result.count;
//					initSessionTimers(result.count);
				}
				
				if(type.equals(AppConstants.LOGININVALID)){
					
					//locked user
					MyQuerry querry = new MyQuerry();
					Vector<Filter> filtervec = new Vector<Filter>();
					Filter filter = new Filter();
					filter = new Filter();
					filter.setQuerryString("userName");
					filter.setStringValue(userName.trim());
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("remark");
					filter.setStringValue("Invalid Login");
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("status");
					filter.setStringValue(AppConstants.INACTIVE);
					filtervec.add(filter);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new LoggedIn());
					
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							Console.log("inactive user"+result.size());
							 ArrayList<LoggedIn> LoggedInlist= new  ArrayList<LoggedIn>();
					
							 String todaysdate = AppUtility.parseDate(new Date());
							 Console.log("todaysdate=="+todaysdate);
							 
								for(SuperModel model:result){
									LoggedIn loggedInUser = (LoggedIn)model;
									 Console.log("login date=="+AppUtility.parseDate(loggedInUser.getLoginDate()));
								     String logindate = AppUtility.parseDate(loggedInUser.getLoginDate());
									      if(logindate.equals(todaysdate)) {
										      LoggedInlist.add(loggedInUser);
										      Console.log("LoggedInlist=="+LoggedInlist.size());
									}
								}
								if(LoggedInlist.size()>=5) {
									
									Console.log("LoggedInlist size after invalidlogin=="+LoggedInlist.size());
//									loggedInEntity.setStatus(AppConstants.INACTIVE);
									
									if(LoggedInlist.size()==5) {
										Window.alert("User id locked due 5 incorrect login attempts!!");

										generalservice.inactiveUser(userName, companyId, new AsyncCallback<Void>() {
											@Override
											public void onFailure(Throwable arg0) {
												// TODO Auto-generated method stub
												Console.log("fail");
											}

											@Override
											public void onSuccess(Void arg0) {
												// TODO Auto-generated method stub
												Console.log("Incorrect password");

												emailService.sendEmailOnIncorrectPassword(loggedInEntity, userName, companyId, new AsyncCallback<Void>() {

													@Override
													public void onFailure(Throwable arg0) {
														// TODO Auto-generated method stub
														
													}

													@Override
													public void onSuccess(Void arg0) {
														// TODO Auto-generated method stub
														Console.log("inside email");
													}
													
												});
											}
										});
									}
									else {
										Window.alert("User id locked due 5 incorrect login attempts!! Please contact to admin");

									}
									
									
								}
//								else {
//									Window.alert("Invalid Login Credentials and After 5 incorrect login attempts user id will be locked!!");
//
//								}
								
//								else if(LoggedInlist.size()>5) {
//									
//									Window.alert("User id locked due 5 incorrect login attempts!! Please contact to admin");
//
//									MyQuerry querry = new MyQuerry();
//									Vector<Filter> filtervec = new Vector<Filter>();
//									Filter filter = new Filter();
//									filter = new Filter();
//									filter.setQuerryString("userName");
//									filter.setStringValue(userName.trim());
//									filtervec.add(filter);
//									
//									querry.setFilters(filtervec);
//									querry.setQuerryObject(new User());
//									
//									async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//										
//										@Override
//										public void onSuccess(ArrayList<SuperModel> result) {
//											// TODO Auto-generated method stub
//											
//											for(SuperModel model : result) {
//												User user = (User) model;
//												if(user.getStatus()) {
//													
//												}
//												else {
//													Window.alert("User id locked due 5 incorrect login attempts!! Please contact to admin");
//												}
//												break;
//											}
//										}
//										
//										@Override
//										public void onFailure(Throwable arg0) {
//											// TODO Auto-generated method stub
//											
//										}
//									});
//								}
							
						}
						
						public void onFailure(Throwable arg0) {
							// TODO Auto-generated method stub
							
						}
						
					});
				}
			}
			
		});
	} 
	
	
	/********************************Global Make Live For Drop Downs*******************************************/
	
	/********************************Global Make Live For Config***********************************************/
	
	public static void globalMakeLiveConfig(final String querry)
	{
		glassPanel.show();
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
				{
//			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
			dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
			{
				@Override
				public void onFailure(Throwable caught) {
					glassPanel.hide();
			    	caught.printStackTrace();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result)
			{
				globalConfig.clear();
				for(SuperModel model:result){
					Config configEntity=(Config)model;
					globalConfig.add(configEntity);
				}
				System.out.println("Config Size"+globalConfig.size());
				Console.log("Config Size"+globalConfig.size());
				glassPanel.hide();
			}
			});
		}
   	 };
   	 timer.schedule(3000); 
	}
	
	/*****************************Global Make Live For ConfigCategory***************************************/
	
	public static void globalMakeLiveCategory(final String querry)
	{
		glassPanel.show();
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
				{
//			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
			dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
			{
				@Override
				public void onFailure(Throwable caught) {
			    	caught.printStackTrace();
			    	glassPanel.hide();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result)
			{
				globalCategory.clear();
				for(SuperModel model:result){
					ConfigCategory categoryEntity=(ConfigCategory)model;
					globalCategory.add(categoryEntity);
				}
				System.out.println("Cateogry Size"+globalCategory.size());
				Console.log("Cateogry Size"+globalCategory.size());
				glassPanel.hide();
			}
			});
		}
   	 };
   	 timer.schedule(3000); 
	}
	
	/********************************Global Make Live For Type**********************************************/
	
	public static void globalMakeLiveType(final String querry)
	{
		glassPanel.show();
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
				{
//			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
			dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
			{
				@Override
				public void onFailure(Throwable caught) {
			    	caught.printStackTrace();
			    	glassPanel.hide();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result)
			{
				globalType.clear();
				for(SuperModel model:result){
					Type typeEntity=(Type)model;
					globalType.add(typeEntity);
				}
				System.out.println("Type Size"+globalType.size());
				Console.log("Type Size"+globalType.size());
				glassPanel.hide();
			}
			});
		}
   	 };
   	 timer.schedule(3000); 
	}
	
	
/********************************Global Make Live For Employee When branch is restricted********************************************/
	
	/**
	 * Date: 27-02-2017 by Anil
	 * This code is used to load only those branch employee which are assigned to login person 
	 */
	public static void globalMakeLiveEmployee(final List<String> branchList,final Long companyId) {
		glassPanel.show();
		Timer timer = new Timer() {
			@Override
			public void run() {
				dpservice.getEmployeeDropDown(branchList, companyId,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
						glassPanel.hide();
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						globalEmployee.clear();
						for (SuperModel model : result) {
							Employee employeeEntity = (Employee) model;
							globalEmployee.add(employeeEntity);
						}
						Console.log("Employee Size"+ globalEmployee.size());
						glassPanel.hide();
					}
				});
			}
		};
		timer.schedule(3000);
	}
	
	
	/********************************Global Make Live For Employee********************************************/
	
	public static void globalMakeLiveEmployee(final String querry)
	{
		glassPanel.show();
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
				{
//			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
			dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
			{
				@Override
				public void onFailure(Throwable caught) {
			    	caught.printStackTrace();
			    	glassPanel.hide();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result)
			{
				globalEmployee.clear();
				for(SuperModel model:result){
					Employee employeeEntity=(Employee)model;
					globalEmployee.add(employeeEntity);
				}
				Console.log("Employee Size"+globalEmployee.size());
				glassPanel.hide();
			}
			});
		}
   	 };
   	 timer.schedule(3000); 
	}
	
	/********************************Global Make Live For Branch***********************************************/
	
	public static void globalMakeLiveBranch(final String querry)
	{
		glassPanel.show();
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
				{
//			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
			dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
			{
				@Override
				public void onFailure(Throwable caught) {
			    	caught.printStackTrace();
			    	glassPanel.hide();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result)
			{
				globalBranch.clear();
				//   rohan added this branch load code here for saving RPC   
				customerScreenBranchList.clear();
				
				for(SuperModel model:result){
					Branch branchEntity=(Branch)model;
					globalBranch.add(branchEntity);
					customerScreenBranchList.add(branchEntity);
				}
				
				Console.log("Branch Size"+globalBranch.size());
				glassPanel.hide();
			}
			});
		}
   	 };
   	 timer.schedule(3000); 
	}
	
/********************************Global Make Live For Customer Screen Branch Load Dropdown***********************************************/
	/**
	 * Developed by : Rohan Bhagde
	 * Used for : This method is used for retrieving all the branches irrespective of branch level restriction
	 *  Date : 20-09-2016
	 * @param empBranch 
	 * @param empBranchList 
	 */
	public static void globalMakeLiveCustomerScreenBranchDropDown(final String querry, final List<EmployeeBranch> empBranchList, final String empBranch)
	{
		glassPanel.show();
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
				{
//			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
			dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
			{
				@Override
				public void onFailure(Throwable caught) {
			    	caught.printStackTrace();
			    	glassPanel.hide();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result)
			{
				customerScreenBranchList.clear();
				
				for(SuperModel model:result){
					Branch branchEntity=(Branch)model;
					customerScreenBranchList.add(branchEntity);
					if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
						branchWiseNumberRangeMap.put(branchEntity.getBusinessUnitName(), branchEntity.getNumberRange());
						Console.log("number range added to map for branch "+branchEntity.getBusinessUnitName());
					}else
						Console.log("no number range found for branch "+branchEntity.getBusinessUnitName());
				
				}
				Console.log("branchwisemap size="+LoginPresenter.branchWiseNumberRangeMap.size());
				
				Console.log("Branch Size"+customerScreenBranchList.size());
				/** Date 02-02-2018 by Vijay for branch user unble to save issue ***/
				globalMakeLiveWithBranchRestriction(empBranchList,empBranch);

				glassPanel.hide();
			}
			});
		}
   	 };
   	 timer.schedule(3000); 
	}
	
	/**
	 * Ends here 
	 */
	
	/********************************Global Make Live For Country**********************************************/
	
	public static void globalMakeLiveCountry(final String querry)
	{
		glassPanel.show();
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
			{
//			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
			dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
			{
				@Override
				public void onFailure(Throwable caught) {
			    	caught.printStackTrace();
			    	glassPanel.hide();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result)
			{
				globalCountry.clear();
				for(SuperModel model:result){
					Country countryEntity=(Country)model;
					globalCountry.add(countryEntity);
				}
				
				Console.log("Country Size"+globalCountry.size());
				glassPanel.hide();
			}
			});
		}
   	 };
   	 timer.schedule(3000); 
	}
	
	/********************************Global Make Live For State**********************************************/

	public static void globalMakeLiveState(final String querry)
	{
		glassPanel.show();
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
				{
//			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
			dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
			{
				@Override
				public void onFailure(Throwable caught) {
			    	caught.printStackTrace();
			    	glassPanel.hide();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result)
			{
				globalState.clear();
				for(SuperModel model:result){
					State stateEntity=(State)model;
					globalState.add(stateEntity);
				}
					
				Console.log("State Size"+globalState.size());
				glassPanel.hide();
			}
			});
		}
   	 };
   	 timer.schedule(3000); 
	}
	
	/********************************Global Make Live For City**********************************************/

	public static void globalMakeLiveCity(final String querry)
	{
		glassPanel.show();
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
			{
//			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
			dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
			{
				@Override
				public void onFailure(Throwable caught) {
			    	caught.printStackTrace();
			    	glassPanel.hide();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result)
			{
				globalCity.clear();
				for(SuperModel model:result){
					City cityEntity=(City)model;
					globalCity.add(cityEntity);
				}
					
				Console.log("City Size"+globalCity.size());
				glassPanel.hide();
			}
			});
		}
   	 };
   	 timer.schedule(3000); 
	}
	
	/********************************Global Make Live For Locality********************************************/

	public static void globalMakeLiveLocality(final String querry)
	{
		glassPanel.show();
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
				{
//			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
			dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
			{
				@Override
				public void onFailure(Throwable caught) {
			    	caught.printStackTrace();
			    	glassPanel.hide();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result)
			{
				globalLocality.clear();
				for(SuperModel model:result){
					Locality localityEntity=(Locality)model;
					globalLocality.add(localityEntity);
				}
				
				Console.log("Locality Size"+globalLocality.size());
				glassPanel.hide();
			}
			});
		}
   	 };
   	 timer.schedule(3000); 
	}
	
	/******************************Global Make Live For Process Config**********************************/

	public static void globalMakeLiveRoleDefinition(final String querry)
	{
		glassPanel.show();
		Timer timer = new Timer() {
			@Override
			public void run() {
				dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
						glassPanel.hide();
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						globalRoleDefinition.clear();
						for (SuperModel model : result) {
							RoleDefinition roleDefEntity = (RoleDefinition) model;
							globalRoleDefinition.add(roleDefEntity);
						}
						Console.log("Role Definition Size"+ globalRoleDefinition.size());
						glassPanel.hide();
					}
				});
			}
		};
		timer.schedule(3000);
	}
	
	public static void globalMakeLiveProcessConfig(final String querry)
	{
		glassPanel.show();
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
				{
//			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
			dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
			{
				@Override
				public void onFailure(Throwable caught) {
			    	caught.printStackTrace();
			    	glassPanel.hide();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result)
			{
				globalProcessConfig.clear();
				for(SuperModel model:result){
					ProcessConfiguration processConfigEntity=(ProcessConfiguration)model;
					globalProcessConfig.add(processConfigEntity);
				}
				
				Console.log("Process Config Size"+globalProcessConfig.size());
				glassPanel.hide();
				
				/**
				 * Date 15 jun 2017 added by vijay 
				 * this flag is used to SMS API Calling
				 */
				bhashSMSFlag =  AppUtility.checkForProcessConfigurartionIsActiveOrNot("SMS", "BhashSMSAPI");
				/**
				 * ends here
				 */
				/**
				 * nidhi
				 * 8-08-2018
				 * for map serial
				 */
				mapModelSerialNoFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("HVASC","HideOrShowModelNoAndSerialNoDetails");
				/**
				 * nidhi
				 */
				areaWiseCalRestricFlg = AppUtility.checkForProcessConfigurartionIsActiveOrNot("contract", "DisableAreaWiseBillingForAmc");
			
				/**
				 * nidhi
				 * 18-09-2018
				 */
				billofMaterialActive = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillOfMaterial","ApplyBillOfMaterial");

				/** 
				 *  *:*:*
				 * nidhi
				 * 11-09-2018
				 * for make billing group global
				 */
				if(billofMaterialActive){
					billofMaterialActive = true;
					String processBillOfMaterial=AppConstants.BILLOFMATERIAL;
					globalMakeLiveBillOfMaterial(processBillOfMaterial);
					
					globalMakeLiveUnitConversion(processBillOfMaterial);
//					globalMakeLiveUnitOfMasurment(ConfigTypes.getConfigType(Screen.UNITOFMEASUREMENT));
					globalMakeLiveUnitOfMasurment(ConfigTypes.getConfigType(Screen.SERVICEUOM));
					globalMakeLiveItemProduct();
					globalMakeLiveItemProductGroupList();
				}
				globalMakeLiveUnitOfMasurment(ConfigTypes.getConfigType(Screen.UNITOFMEASUREMENT));
				/**
				 * ends here
				 */
				
				/**
				 * @author Vijay Chougule
				 * Date :- 11-Dec-2018
				 * Des :- For NBHC Inventory Management for Automatic PR Order Qty 
				 */
				 automaticPROrderQtyFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableAutomaticPROrderQty");
				/**
				 * ends here
				 */
				 
			 	/**
				 * @author Vijay Chougule
				 * Date :- 18-June-2019
				 * Des :- For NBHC Inventory Management for Lock Seal Loading All warehouses Globally 
				 */
				 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialMovementNote", "EnableLoadAllWarehousesGlobally")){
					 Company c = new Company();
					 String warehouses = AppConstants.GLOBALRETRIEVALWAREHOUSE+"-"+c.getCompanyId();
					 globalMakeLiveWarehouses(warehouses);
				 }
				 /**
				  * Date 14-08-2019 by Vijay Chougule
				  * Des :- NBHC CCPM Auto Invoice process config
				  */
				 autoInvoiceFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableAutoInvoice");
				 Console.log("autoInvoiceFlag =="+autoInvoiceFlag);
				 
				 /**
				  * Date 01-10-2021 by Vijay Chougule
				  * Des :- Innovative service scheduling new logic with process config
				  */
				 serviceScheduleNewLogicFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CONTRACT,AppConstants.PC_SERVICESCHEDULEMINMAXROUNDUP);
				 Console.log("serviceScheduleNewLogicFlag =="+serviceScheduleNewLogicFlag);
				 
				 PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CONTRACT,AppConstants.PC_NO_ROUNDOFF_INVOICE_CONTRACT);
				 Console.log("PC_NO_ROUNDOFF_INVOICE_CONTRACT =="+PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag);
				 
			}
			});
		}
   	 };
   	 timer.schedule(3000); 
	}
	
	
	/******************************Global Make Live For Process Name**********************************/

	public static void globalMakeLiveProcessName(final String querry)
	{
		glassPanel.show();
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
				{
//			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
			dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
			{
				@Override
				public void onFailure(Throwable caught) {
			    	caught.printStackTrace();
			    	glassPanel.hide();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result)
			{
				globalProcessName.clear();
				for(SuperModel model:result){
					ProcessName processNameEntity=(ProcessName)model;
					globalProcessName.add(processNameEntity);
				}
				
				Console.log("Process Name Size"+globalProcessName.size());
				glassPanel.hide();
			}
			});
		}
   	 };
   	 timer.schedule(3000); 
	}
	
	/******************************Global Make Live For Process Name**********************************/

	public static void globalMakeLiveMultiLevelApproval(final String querry)
	{
		glassPanel.show();
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
				{
//			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
			dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
			{
				@Override
				public void onFailure(Throwable caught) {
			    	caught.printStackTrace();
			    	glassPanel.hide();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result)
			{
				globalMultilevelApproval.clear();
				for(SuperModel model:result){
					MultilevelApproval multiLevelEntity=(MultilevelApproval)model;
					globalMultilevelApproval.add(multiLevelEntity);
				}
				
				Console.log("Multi Level Size"+globalMultilevelApproval.size());
				glassPanel.hide();
			}
			});
		}
   	 };
   	 timer.schedule(3000); 
	}
	
	
	protected static void clearList()
	{
		Timer timer=new Timer() 
	   	 {
			@Override
			public void run() 
				{
					globalConfig.clear();
					globalCategory.clear();
					globalType.clear();
					globalEmployee.clear();
					globalBranch.clear();
					globalCountry.clear();
					globalState.clear();
					globalCity.clear();
					globalLocality.clear();
				}
	   	 };
	   	 timer.schedule(500); 
	}
	
	
	
	/*********************************Email If Ip is not authorized********************************/
	/**
	 *  If IP Address Authorization is active in process configuration then the clientsip address is
	 *  validated while user trys to log in.
	 *  If the user is not authorized then an email gets sent to companys email address.
	 *  So this method is processed for sending email
	 */
	
	public static void reactOnIPAddressAlertEmail(String userID,String passward,String ipaddress,String compEmail)
	{
		//Quotation Entity patch patch patch
		emailService.initiateIPAddressAuthorizationEmail(userID,passward,ipaddress,compEmail, new AsyncCallback<Void>(){

			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(Void result) {
//				Window.alert("Email Sent Sucessfully !");
			}
		});
	}

	public static void giveCustomerLogin(final LoginProxy proxylogin, final User userEntity){
		System.out.println("INSIDE CUSTOMER LOGIN.....");
		loginservice.giveCustLogin(proxylogin, new AsyncCallback<CustomerUser>() {

			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(final CustomerUser result){
				
				
				
			//  rohan added this code for removing multiple clicks Date : 29-03-2017
				login.btnSignIn.setEnabled(true);
				if(result==null)
				{
					System.out.println("CUSTOMER LOGIN FAILED ----- ");
					String url=Window.Location.getHref();
					String[] spliturl=url.split("\\.");
					String companyname=spliturl[0];
					companyname=companyname.replace("http://","");
					getInValidCredentials(companyname,proxylogin);
					
//					Window.alert("Invalid Login Credentials!");
					Console.log("userEntity"+userEntity);
					if(userEntity==null || userEntity!=null && userEntity.getStatus() ) {
						Window.alert("Invalid Login Credentials and After 5 incorrect login attempts user id will be locked!!");	
					}
					else if(!userEntity.getStatus()) {
						Window.alert("User id locked due 5 incorrect login attempts!! Please contact to admin");
					}
					
					login.textBoxUsername.setValue("");
					login.textBoxPassword.setValue("");
				}else{
					
					System.out.println("CUSTOMER LOGIN SUCCEED ----- ");
					
					loggedInUser=result.getCustomerName().trim();
					proxyString = result.getUserName();
//					validateLicenseDate(result.getCompanyId(),result);
		//***********rohan uncommented this for your to login to mainn panel **********			
					UserConfiguration.initCustomerUserConfiguration(result,result.getCompanyId());
					System.out.println("Sucess Full----");
					Console.log("Success");
					loggedInUser=result.getCustomerName().trim();
					myCustUserEntity=result;
					loggedInCustomer=true;
					
//					showHomeScreen();
					RootPanel rp=RootPanel.get();
					rp.clear();
					AppConnector.initalizes();
					AppMemory mem=AppMemory.getAppMemory();
					mem.initialingapp();
					
					for(int i=0;i<mem.skeleton.menuLabels.length;i++){
						mem.skeleton.menuLabels[i].setVisible(false);
					}
					/**Date 18-2-2020 by Amol display the statutory Report form in home page if
					 *  login user customer's Company Type is "Facitlity Management"**/
					
					Console.log("Statutory Report"+myCustUserEntity.getCompanyType());
							if (myCustUserEntity.getCompanyType()
									.equalsIgnoreCase("Facility Management")) {
								Console.log("inside Statutory Report");
								StatuteryReportPresenter.initalize();
							}else{
						Console.log("inside Customer Presenter");
					    CustomerDetailsPresenter.initalize();
					}
				}
			}
		});
	}
	
	
	/**
	 * Rohan Bhagde
	 * Date :12/11/2016
	 * This method is used to check wether User wanted to login has already Active status or not 
	 */

		private static void getLoggedInStatusByUserEntity(final User myUserEntity) {
		
			Console.log("inside logged status checking username"+myUserEntity.getUserName().trim());
			Console.log("inside logged status checking username"+myUserEntity.getEmployeeName().trim());
			
			MyQuerry querry = new MyQuerry();
			Vector<Filter> filtervec = new Vector<Filter>();
			
			Filter filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(myUserEntity.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("userName");
			filter.setStringValue(myUserEntity.getUserName().trim());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("employeeName");
			filter.setStringValue(myUserEntity.getEmployeeName().trim());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setStringValue("Active");
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new LoggedIn());
			
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
				
					if(result.size()== 0){
//						sessionService.removeUserFromSessionArray(myUserEntity.getUserName().trim(),myUserEntity.getEmployeeName().trim(),new AsyncCallback<Boolean>() {
//
//							@Override
//							public void onFailure(Throwable caught) {
//								
//							}
//
//							@Override
//							public void onSuccess(Boolean result) {
								
								validateLicenseDate(myUserEntity.getCompanyId(),myUserEntity);
//							}
//						});
						
					}
					else{
					for(SuperModel model:result){
						LoggedIn loggedInUser = (LoggedIn) model;
						activeUsers = new ArrayList<LoggedIn>();
						activeUsers.add(loggedInUser);
					}
					
					Console.log("Active user size="+activeUsers.size());
					if(activeUsers.size() != 0)
					{
						sessionLoginPanel=new PopupPanel(true);
						sessionLoginPanel.add(sessionLoginConditionPopup);
						sessionLoginPanel.setGlassEnabled(true);
						sessionLoginPanel.show();
						sessionLoginPanel.center();
					}
				}
			}

				@Override
				public void onFailure(Throwable caught) {
					
				}
			});
		}
		
		
		private void getloginAfterTerminatingPreviousSession() {
			
			Console.log("Active user size inside getloginAfterTerminatingPreviousSession="+activeUsers.size());
			
			ArrayList<LoggedIn> activeLoginsList = new ArrayList<LoggedIn>();
			activeLoginsList.addAll(activeUsers);
			
			sessionService.getUserStatusInactiveAndRemoveSessionFromServer(activeLoginsList ,myUserEntity.getCompanyId(), new AsyncCallback<Boolean>() {
				
				@Override
				public void onFailure(Throwable caught) {
					
				}

				@Override
				public void onSuccess(Boolean result) {
					
					if(result)
					{
						validateLicenseDate(myUserEntity.getCompanyId(),myUserEntity);
					}
					else
					{
						panelforConditionPopup1=new PopupPanel(true);
						panelforConditionPopup1.add(conditionPopup1);
						panelforConditionPopup1.setGlassEnabled(true);
						panelforConditionPopup1.show();
					}
					
				}
			});
		}
		
		private static void globalMakeLiveBillOfMaterial(final String processOvertime) {

			MyQuerry querry = new MyQuerry();
			Vector<Filter> filtervec = new Vector<Filter>();
			
			Filter filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(myUserEntity.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(true);
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new BillOfMaterial());
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					ContractPresenter.globalProdBillOfMaterial =	new ArrayList<BillOfMaterial>();
					ContractPresenter.globalProdBillOfMaterial.clear();
					for(SuperModel model :result){
						BillOfMaterial bill = (BillOfMaterial) model;
						ContractPresenter.globalProdBillOfMaterial.add(bill);
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		private static void globalMakeLiveUnitConversion(final String processOvertime) {

			MyQuerry querry = new MyQuerry();
			Vector<Filter> filtervec = new Vector<Filter>();
			
			Filter filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(myUserEntity.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("unitStatus");
			filter.setBooleanvalue(true);
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new UnitConversion());
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					LoginPresenter.areaUnitConverList.clear();
					LoginPresenter.prodUnitConverList.clear();
					for(SuperModel model :result){
						UnitConversion unit = (UnitConversion) model;
						if(unit.getUnitCode()==ConfigTypes.getConfigType(Screen.UNITOFMEASUREMENT)){
								LoginPresenter.prodUnitConverList.add(unit);
						}else if(unit.getUnitCode()==ConfigTypes.getConfigType(Screen.SERVICEUOM)){
							LoginPresenter.areaUnitConverList.add(unit);
						}
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		private static void globalMakeLiveUnitOfMasurment(final int screenCode) {

			MyQuerry querry = new MyQuerry();
			Vector<Filter> filtervec = new Vector<Filter>();
			
			Filter filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(myUserEntity.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("type");
			filter.setIntValue(screenCode);
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Config());
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					
					if(screenCode == ConfigTypes.getConfigType(Screen.UNITOFMEASUREMENT)){
						LoginPresenter.unitForProduct.clear();
					}else if(screenCode == ConfigTypes.getConfigType(Screen.SERVICEUOM)){
						LoginPresenter.unitForService.clear();
					}
//					ContractPresenter.globalProdBillOfMaterial.clear();
					for(SuperModel model :result){
						Config unit = (Config) model;
						if(unit.getType()==ConfigTypes.getConfigType(Screen.UNITOFMEASUREMENT)){
								LoginPresenter.unitForProduct.add(unit);
						}else if(unit.getType()==ConfigTypes.getConfigType(Screen.SERVICEUOM)){
							LoginPresenter.unitForService.add(unit);
						}
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		private static void globalMakeLiveItemProduct() {

			MyQuerry querry = new MyQuerry();
			Vector<Filter> filtervec = new Vector<Filter>();
			
			Filter filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(myUserEntity.getCompanyId());
			filtervec.add(filter);
			
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ItemProduct());
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					
					LoginPresenter.itemProList.clear();
					for(SuperModel model :result){
						ItemProduct prod = (ItemProduct) model;
						LoginPresenter.itemProList.add(prod);
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		private static void globalMakeLiveItemProductGroupList() {

			MyQuerry querry = new MyQuerry();
			Vector<Filter> filtervec = new Vector<Filter>();
			
			Filter filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(myUserEntity.getCompanyId());
			filtervec.add(filter);
			
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ProductGroupDetails());
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					
					LoginPresenter.proGroupList.clear();
					for(SuperModel model :result){
						ProductGroupDetails prod = (ProductGroupDetails) model;
						LoginPresenter.proGroupList.add(prod);
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
	
		/**
		 * Date 02-11-2018 by Vijay
		 * Des:- To load large customer Data with process config 
		 * so we need customer number range to load data partially for customer composite
		 */
		
		private static void getCustomercount(Long companyId) {

			MyQuerry query = new MyQuerry();
			Vector<Filter> filterVec = new Vector<Filter>();
			
			Filter filter =null;
			filter = new Filter();
			filter.setQuerryString("processName");
			filter.setStringValue("Customer");
			filterVec.add(filter);
			
			query.setFilters(filterVec);
			query.setQuerryObject(new NumberGeneration());
			
			async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					if(result.size()>0){
						for(SuperModel model : result){
							NumberGeneration numbGen = (NumberGeneration) model;
							customerCount = numbGen.getNumber();
							Console.log("Customer Count from number range"+customerCount);
							break;
						}
						
						if(PersonInfoComposite.globalCustomerArray.size()==0){
							gwtGlassPanel.show();
							loadingPanel = new PopupPanel(true);
							loadingPanel.add(loadingPopup);
							loadingPanel.center();
							loadingPanel.show();

						}
					}
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					
				}
			});
			
		}
		
		/**
		 * ends here
		 */

		private static void checkProceesConfigAndGetCustomerCount(final Long companyId) {

			Vector<Filter> filtrvec = new Vector<Filter>();
			Filter filtr = null;
			
			filtr = new Filter();
			filtr.setQuerryString("companyId");
			filtr.setLongValue(companyId);
			filtrvec.add(filtr);
			
			filtr = new Filter();
			filtr.setQuerryString("processName");
			filtr.setStringValue("Customer");
			filtrvec.add(filtr);
			
			MyQuerry querry = new MyQuerry();
			querry.setFilters(filtrvec);
			querry.setQuerryObject(new ProcessConfiguration());
			
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					Console.log("cuatomer load process config =="+result.size());
					boolean flag = false;
					if(result.size()!=0){
					for(SuperModel model : result){
						ProcessConfiguration processConfig = (ProcessConfiguration) model;
						for(ProcessTypeDetails processType : processConfig.getProcessList()){
							if(processType.getProcessName().equals("Customer")
							   &&processType.getProcessType().equalsIgnoreCase("EnableloadLargeCustomerData")
							   &&processType.isStatus()){
								flag = true;
								loadLargeCustomerDataFlag = true;
							}
						}
					}
					}
					Console.log("Customer load large data Process config"+flag); 
					if(flag)
					getCustomercount(companyId);

				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});

		}
		
		
		/**
		 * @author Vijay Chougule
		 * Date :- 18-June-2019
		 * Des :- For NBHC Inventory Management for Lock Seal Loading All warehouses Globally 
		 */
		
		private static void globalMakeLiveWarehouses(final String warehouses) {
			glassPanel.show();
			Timer timer = new Timer() {
				@Override
				public void run() {
					dpservice.getDropDown(warehouses,new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onFailure(Throwable caught) {
							caught.printStackTrace();
							glassPanel.hide();
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							allWarehouselist.clear();
							for (SuperModel model : result) {
								WareHouse warehouse = (WareHouse) model;
								allWarehouselist.add(warehouse);
							}
							Console.log("All Warehouse  Size"+ allWarehouselist.size());
							glassPanel.hide();
						}
					});
				}
			};
			timer.schedule(3000);
		
		}
		
		
		
		/**
		 * @author Vijay Chougule
		 * Date :- 19-July-2020
		 * Des :- For HRProject Dropdown HRProject loading globaly
		 */
		public static void globalMakeLiveHRProject(final String querry)
		{
			glassPanel.show();
			Timer timer=new Timer() 
		   	 {
				@Override
				public void run() 
				{
					dpservice.getDropDown(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							// TODO Auto-generated method stub
							globalHRProject.clear();
							for(SuperModel model:result){
								HrProject hrProjectEntity=(HrProject)model;
								globalHRProject.add(hrProjectEntity);
							}
							
							Console.log("HR Project Size"+globalHRProject.size());
							glassPanel.hide();
						}
						
						@Override
						public void onFailure(Throwable caught) {
							caught.printStackTrace();
					    	glassPanel.hide();
						}
					});
			}
	   	 };
	   	 timer.schedule(3000); 
		}

	/**
	 * @author Anil
	 * @since 18-12-2020
	 * loading screen menu configuration
	 */
	public static void loadSystemConfigurationMenu(final String querry) {
		
		if(!AppMemory.getAppMemory().enableMenuBar){
			Console.log("enableMenuBar is not active!!!!!!");
			return;
		}
		glassPanel.show();
		Timer timer = new Timer() {
			@Override
			public void run() {
				dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						globalScrMenuConf.clear();
						for (SuperModel model : result) {
							ScreenMenuConfiguration obj = (ScreenMenuConfiguration) model;
							globalScrMenuConf.add(obj);
						}
						Console.log("Screen Menu CONF Size"+ globalScrMenuConf.size());
						glassPanel.hide();
					}
					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
						glassPanel.hide();
					}
				});
			}
		};
		timer.schedule(2500);
	}
	
		public static void globalMakeLiveEmployeeInfo(final String empInfoType) {
			
			glassPanel.show();
			Timer timer=new Timer() 
		   	 {
				@Override
				public void run() 
					{
				dpservice.getDropDown(empInfoType,new AsyncCallback<ArrayList<SuperModel>>()
				{
					@Override
					public void onFailure(Throwable caught) {
				    	caught.printStackTrace();
				    	glassPanel.hide();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result)
				{
					globalEmployeeInfo.clear();
					for(SuperModel model:result){
						EmployeeInfo employeeEntity=(EmployeeInfo)model;
						globalEmployeeInfo.add(employeeEntity);
					}
					Console.log("Employee Size globalEmployeeInfo "+globalEmployeeInfo.size());
					glassPanel.hide();
				}
				});
			}
	   	 };
	   	 timer.schedule(3000); 
		
		}
		
		
		public static void GlobalLoadShift() {
			// TODO Auto-generated method stub
			final GenricServiceAsync service = GWT.create(GenricService.class);
			MyQuerry query = new MyQuerry();
			Company c=new Company();
			System.out.println("Company Id :: "+c.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			/** date 27.8.2018 added by komal to load status shiftwise**/
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(true);
			filtervec.add(filter);
			query.setFilters(filtervec);
			
			query.setQuerryObject(new Shift());
			
			service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					shiftList.clear();
					shiftList.add("--SELECT--");
					globalshiftList.clear();
					for (SuperModel model : result) {
						Shift shift=(Shift) model;
						shiftList.add(shift.getShiftName());
						globalshiftList.add(shift);
					}
					
				}
				
				@Override
				public void onFailure(Throwable arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		

		public static void globalLoadTaxDetails() {
			final GenricServiceAsync service = GWT.create(GenricService.class);
			MyQuerry query = new MyQuerry();
			Company c=new Company();
			System.out.println("Company Id :: "+c.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			filter = new Filter();
			filter.setQuerryString("taxChargeStatus");
			filter.setBooleanvalue(true);
			filtervec.add(filter);
			query.setFilters(filtervec);
			
			query.setQuerryObject(new TaxDetails());
			
			service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					globalTaxList.clear();
					for (SuperModel model : result) {
						TaxDetails taxDetails=(TaxDetails) model;
						globalTaxList.add(taxDetails);
					}
					
				}
				
				@Override
				public void onFailure(Throwable arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		
		public static void loadContactPersonDetailslist() {
			final GenricServiceAsync service = GWT.create(GenricService.class);
			MyQuerry query = new MyQuerry();
			Company c=new Company();
			System.out.println("Company Id :: "+c.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			query.setFilters(filtervec);
			query.setQuerryObject(new ContactPersonIdentification());
			
			service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					globalContactpersonlist.clear();
					Console.log("Contact person list size"+result.size());
					for (SuperModel model : result) {
						ContactPersonIdentification contactPersonDetails=(ContactPersonIdentification) model;
						globalContactpersonlist.add(contactPersonDetails);
					}
					
				}
				
				@Override
				public void onFailure(Throwable arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		public static void loadEmailTemplatelist() {
			final GenricServiceAsync service = GWT.create(GenricService.class);
			MyQuerry query = new MyQuerry();
			Company c=new Company();
			System.out.println("Company Id :: "+c.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			query.setFilters(filtervec);
			query.setQuerryObject(new EmailTemplate());
			
			service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					globalEmailTemplatelist.clear();
					for (SuperModel model : result) {
						EmailTemplate emailTemplate=(EmailTemplate) model;
						globalEmailTemplatelist.add(emailTemplate);
					}
					Console.log("globalEmailTemplatelist size"+globalEmailTemplatelist.size());

				}
				
				@Override
				public void onFailure(Throwable arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		public static void loadEmailTemplateConfigurationlist() {
			final GenricServiceAsync service = GWT.create(GenricService.class);
			MyQuerry query = new MyQuerry();
			Company c=new Company();
			System.out.println("Company Id :: "+c.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			query.setFilters(filtervec);
			query.setQuerryObject(new EmailTemplateConfiguration());
			
			service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					globalEmailTemplateConfiglist.clear();
					for (SuperModel model : result) {
						EmailTemplateConfiguration emailTemplateConfig=(EmailTemplateConfiguration) model;
						globalEmailTemplateConfiglist.add(emailTemplateConfig);
					}
					Console.log("globalEmailTemplateConfiglist size"+globalEmailTemplateConfiglist.size());

				}
				
				@Override
				public void onFailure(Throwable arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		
		public static void loadUserEntitylist() {
			final GenricServiceAsync service = GWT.create(GenricService.class);
			MyQuerry query = new MyQuerry();
			Company c=new Company();
			System.out.println("Company Id :: "+c.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			query.setFilters(filtervec);
			query.setQuerryObject(new User());
			
			service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					globalUserEntitylist.clear();
					for (SuperModel model : result) {
						User userEntity=(User) model;
						globalUserEntitylist.add(userEntity);
					}
					Console.log("globalUserEntitylist size"+globalUserEntitylist.size());

				}
				
				@Override
				public void onFailure(Throwable arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		public static void loadSMSTemplatelist() {
			final GenricServiceAsync service = GWT.create(GenricService.class);
			MyQuerry query = new MyQuerry();
			Company c=new Company();
			System.out.println("Company Id :: "+c.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			query.setFilters(filtervec);
			query.setQuerryObject(new SmsTemplate());
			
			service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					globalSMSTemplatelist.clear();
					for (SuperModel model : result) {
						SmsTemplate emailTemplate=(SmsTemplate) model;
						globalSMSTemplatelist.add(emailTemplate);
					}
					Console.log("globalSMSTemplatelist size"+globalSMSTemplatelist.size());

				}
				
				@Override
				public void onFailure(Throwable arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		}
	//sheetal
	  public static void IPAddressAlertMail(String userID,String ipaddress,long companyId)
		{
		emailService.sendEmailForIPAuthorisation(userID, ipaddress,companyId, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(Void arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		}
	  
	  private static void deleteInvalidLogin(String userName, Long companyId) {
	
		  commonservice.deleteInvalidLogin(userName, companyId, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(String arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	 }
    
		  private static void loadCalendarList() {
			final GenricServiceAsync service = GWT.create(GenricService.class);

		    MyQuerry qu= new MyQuerry();
			Filter filter=new Filter();
			filter.setQuerryString("status");
			filter.setStringValue(Calendar.ACTIVE);
			qu.getFilters().add(filter);
			qu.setQuerryObject(new Calendar());
			
			service.getSearchResult(qu, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					Date todayDate = new Date();
					globalCalendarlist.clear();
					for(SuperModel model : result){
						Calendar calendar = (Calendar) model;
						if(calendar.getCalEndDate().after(todayDate) || 
								AppUtility.parseDate(calendar.getCalEndDate()).equals(AppUtility.parseDate(todayDate))){
							globalCalendarlist.add(calendar);
						}
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		
		private static String getUrlFromwindow() {
			// TODO Auto-generated method stub
					String url=com.google.gwt.user.client.Window.Location.getHref();
					String urlWithoutHttp="";
					String urlWithoutDomain="";
					String finalurl="";
					
					String[] urlarray=url.split("://");
					System.out.println("arrlength="+urlarray.length);
					if(urlarray.length>1)
						 urlWithoutHttp=urlarray[1];
					else
						urlWithoutHttp=url;
					System.out.println("urlWithoutHttp="+urlWithoutHttp);
					urlarray=urlWithoutHttp.split(".appspot.com");
					System.out.println("arrlength="+urlarray.length);
					if(urlarray.length>0)
						urlWithoutDomain=urlarray[0];
					System.out.println("urlWithoutDomain="+urlWithoutDomain);
					finalurl=urlWithoutDomain.replace(".","-dot-");
					finalurl=finalurl+".appspot.com";
					System.out.println("finalurl="+finalurl);
					Console.log("finalurl="+finalurl);
			return finalurl;
		}

}

