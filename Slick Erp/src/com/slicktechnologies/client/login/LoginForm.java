package com.slicktechnologies.client.login;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.shared.common.helperlayer.LoginProxy;

public class LoginForm extends ViewContainer {
	@UiField
	public TextBox textBoxUsername, textBoxCompanyname;

	@UiField
	public PasswordTextBox textBoxPassword;
	public Image logo;
	LoginPresenter presenter;
	@UiField
	public Button btnSignIn;
	public HTMLPanel panel;

	@UiField
	public Hyperlink forgotpasslink;

	interface MyBinder extends UiBinder<HTMLPanel, LoginForm> {
		
		
	}

	private static MyBinder uiBinder = GWT.create(MyBinder.class);

	public LoginForm() {
		super();
		createGui();
	}

	public LoginProxy updateModelFromView() {
		LoginProxy model = this.presenter.getModel();

		/********************* UnComment line below while Deploying(Server) *************************/

//		 model.setCompanyName(Slick_Erp.loginCompanyName);
		

		/********************** comment line below while deploying (for local) ******************************/
		 
		model.setCompanyName(Slick_Erp.companyname);
		 
		model.setPassword(this.textBoxPassword.getText().trim());
		model.setUserId(this.textBoxUsername.getText().trim());
		return model;
	}

	public void setPresenter(LoginPresenter presenter) {
		this.presenter = presenter;
	}

	public void applyHandler(ClickHandler handler) {
		this.btnSignIn.addClickHandler(handler);
		// this.btnSignIn.addKeyPressHandler(new KeyPressHandler() {
	}

	public void createGui() {

		panel = (HTMLPanel) uiBinder.createAndBindUi(this);
		textBoxCompanyname.setVisible(false);

		applyStyle();
	}

	public void applyStyle() {
		// textBoxCompanyname.getElement().setClassName("input username");
		textBoxUsername.getElement().setClassName("input username");
		textBoxPassword.getElement().setClassName("input password");
		// textBoxCompanyname.getElement().setPropertyString("placeholder",
		// "Company Name");
		textBoxUsername.getElement().setPropertyString("placeholder",
				"User Name");
		textBoxPassword.getElement().setPropertyString("placeholder",
				"Password");
		btnSignIn.getElement().setClassName("button");
		forgotpasslink.getElement().setClassName("forgetlink");
	}

	private class SubmitListener implements KeyPressHandler {
		public void onKeyPress(Widget sender, char key, int mods) {

		}

		@Override
		public void onKeyPress(KeyPressEvent event) {
			// TODO Auto-generated method stub

		}
	}

}
