package com.slicktechnologies.client.login.staticresources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface ImageResources extends ClientBundle {
	@Source("logo.jpg")
	  ImageResource logo();

	 
}
