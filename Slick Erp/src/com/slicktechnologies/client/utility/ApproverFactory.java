package com.slicktechnologies.client.utility;

import java.util.Date;

import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessprocesslayer.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating Approver objects.
 */
public class ApproverFactory 
{
	public static final String CONTRACT="Contract",QUOTATION="Quotation",
			EXPENSEMANAGMENT="Expense Managment",COMPLAIN="Complaint",
			LEAVEAPPLICATION="Leave Application",ADVANCE="Advance",LETTEROFINTENT="Letter Of Intent",BILLINGDETAILS="Billing Details",INVOICEDETAILS="Invoice Details",
			REQUESTFORQUOTATION="Request For Quotation",SALESQUOTATION="Sales Quotation",SALESORDER="Sales Order",
			PURCHASEORDER="Purchase Order",PURCHASEREQUISITION="Purchase Requisition",GRN="GRN",TIMEREPORT="Time Report",TRANSFER="Transfer",DELIVERYNOTE="Delivery Note",
			MATERIALREQUESTNOTE="MRN",MATERIALISSUENOTE="MIN",MATERIALMOVEMENTNOTE="MMN",INSPECTION="Inspection",WORKORDER="Work Order",CUSTOMERTRAINING="Campaign Management",EMPLOYEE ="Employee",
		    MULTIPLEEXPENSEMANAGMENT="Expense",SERVICEPO="Service PO",CUSTOMER="Customer",CNC="CNC" , /** date 26.6.2018 added komal**/VENDORINVOICEDETAILS="Vendor Invoice Details",
		    PHYSICALINVETORYDETAIL="Physical Inventory";
	/**
	 * Create object of Approver depending upon the state of Screen.
	 */
	public ApproverFactory() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Gets the approvals.
	 *
	 * @param requesterName the Employee name (e.g sales person or purchase engineer or person responsible )
	 * @param approvarName the approvar name
	 * @param branch the branch of the approver
	 * @param count the count count of the {@link BusinessProcess}
	 * @return the approvals   returns a newley created Approval object
	 */
	public Approvals getApprovals(String emplyeeName,String approvarName,String branch,int count,String documentCreatedBy,String customerId,String customerName)
	{
		Approvals approval=new Approvals();
		approval.setApproverName(approvarName);
		approval.setBranchname(branch);
		approval.setBusinessprocessId(count);
	 /**
	  *   Date 01-05-2017 below one line commented by vijay becs no need this this storing date only 
	  *   we needed time for NBHC automatic approval cron job also so creation date code added in onsave method
	  */
//		approval.setCreationDate(new Date());
//		approval.setRequestedBy(requesterName);
		
		/*
		 * Date :- 16 sep 2016
		 * Release Date :- 31 sep 2016 release 
		 * below if condition added by vijay 
		 * for while Request for approval sending email we want to send email to CC for requester means loggedin user 
		 * so in approval entity here i am setting created by current login user name
		 * 
		 */
		if(UserConfiguration.getInfo()!=null && !UserConfiguration.getInfo().getFullName().equals("")){
			approval.setRequestedBy(UserConfiguration.getInfo().getFullName());
			System.out.println(" hi vijay login employee name ==="+UserConfiguration.getInfo().getFullName());
		}
		
		// here i am setting person responsible to send email in cc for person responsible
		approval.setPersonResponsible(emplyeeName);
		
		/**
		 * Setting Document Created By (ANIL)
		 */
		approval.setDocumentCreatedBy(documentCreatedBy);
		
		approval.setApprovalLevel(1);
		approval.setStatus(Approvals.PENDING);
		approval.setBusinessprocesstype(getBuisnessProcessType());
		/**
		 *  nidhi 7-10-2017
		 *  set customer id and name in approval table
		 *  
		 */
		approval.setBpId(customerId);
		approval.setBpName(customerName);
		
		/**
		 * Date 13-01-2019 by vijay for NBHC Inventory Management added flag
		 */
		approval.setDocumentValidation(false);
		/**
		 * ends here
		 */
		
		return approval;
		
	}
	
	
	/**
	 * 
	 *
	 * @return the buisness process type constant repersenting the {@link BusinessProcess} against
	 * which approvel is sought.
	 */
	public String getBuisnessProcessType()
	{
		Screen screen=(Screen) AppMemory.getAppMemory().currentScreen;
		switch(screen)
		{
			case CONTRACT:
				return CONTRACT;
			case QUOTATION:
				return QUOTATION;
			case EXPENSEMANAGMENT:
				return EXPENSEMANAGMENT;
			case COMPLAIN:
				return COMPLAIN;
			case LEAVEAPPLICATION:
				return LEAVEAPPLICATION;
			case ADVANCE:
				return ADVANCE;
			case LETTEROFINTENT:
				return LETTEROFINTENT;
			case BILLINGDETAILS:
				return BILLINGDETAILS;
			case INVOICEDETAILS:
				return INVOICEDETAILS;
			case REQUESTFORQUOTATION:
				return REQUESTFORQUOTATION;
			case SALESQUOTATION:
				return SALESQUOTATION;
			case SALESORDER:
				return SALESORDER;
			case PURCHASEORDER:
				return PURCHASEORDER;
			case PURCHASEREQUISITE:
				return PURCHASEREQUISITION;
			case GRN:
				return GRN;
			case TRANSFER:
				return TRANSFER;
			case TIMEREPORT:
				return TIMEREPORT;
			case DELIVERYNOTE:
				return DELIVERYNOTE;
			case MATERIALREQUESTNOTE:
				return MATERIALREQUESTNOTE;
			case MATERIALISSUENOTE:
				return MATERIALISSUENOTE;
			case MATERIALMOVEMENTNOTE:
				return MATERIALMOVEMENTNOTE;
			case INSPECTION:
				return INSPECTION;
			case WORKORDER:
				return WORKORDER;
			case CUSTOMERTRAINING:
				return CUSTOMERTRAINING;	
			case EMPLOYEE:
				return EMPLOYEE;	
				
			case MULTIPLEEXPENSEMANAGMENT:
				return MULTIPLEEXPENSEMANAGMENT;	
				
			case SERVICEPO:
				return SERVICEPO;	
			case CUSTOMER:
				return CUSTOMER;
			/** date 26.6.2018 added by komal**/
			case VENDORINVOICEDETAILS:
					return VENDORINVOICEDETAILS;
			case CNC:
					return CNC;
					/**
					 * nidhi
					 * 
					 */
			case WAREHOUSEDETAILS:
				return PHYSICALINVETORYDETAIL;
				
		}
		return "N.A";
		
	}
	/** date 14/12/2017 added by komal to approve mmn directly for hvac **/
	public Approvals getApprovalMMN(String emplyeeName,String approvarName,String branch,int count,String documentCreatedBy,String customerId,String customerName, String ProcessType)
	{
		Approvals approval=new Approvals();
		approval.setApproverName(approvarName);
		approval.setBranchname(branch);
		approval.setBusinessprocessId(count);
		approval.setCreationDate(new Date());
		if(UserConfiguration.getInfo()!=null && !UserConfiguration.getInfo().getFullName().equals("")){
			approval.setRequestedBy(UserConfiguration.getInfo().getFullName());
			System.out.println("employee name ==="+UserConfiguration.getInfo().getFullName());
		}
		approval.setPersonResponsible(emplyeeName);

		approval.setDocumentCreatedBy(documentCreatedBy);
		
		approval.setApprovalLevel(1);
		approval.setStatus(Approvals.PENDING);
		approval.setBusinessprocesstype(ProcessType);
		approval.setBpId(customerId);
		approval.setBpName(customerName);
		return approval;
		
	}

	

}
