package com.slicktechnologies.client.utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

//import net.java.balloontip.BalloonTip;
























import java.util.concurrent.TimeUnit;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.RowStyles;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.ibm.icu.text.DateFormat;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.OneToManyComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.categoryconfig.CategoryTypes;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.forgotpassword.ForgotPasswordForm;
import com.slicktechnologies.client.forgotpassword.ForgotPasswordPresenter;
import com.slicktechnologies.client.login.LoginForm;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.login.staticresources.ImageResources;
import com.slicktechnologies.client.services.DropDownService;
import com.slicktechnologies.client.services.DropDownServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.history.History;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.LoginProxy;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.TermsAndConditions;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.AssignedShift;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.ShiftPattern;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.InspectionDetails;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.Person;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;
import com.smartgwt.client.util.SC;


// TODO: Auto-generated Javadoc
/**
 * Contains Repeted methods which are needed by all Screens.In Future we need to seprate the methods to their 
 * common classes.
 */
public class AppUtility {
	
	/** The active. */
	public static String ACTIVE="Active";
	
	/** The inactive. */
	public static String INACTIVE="In Active";
	
	static DateTimeFormat format = DateTimeFormat.getFormat("c");
	
	/**
	 * Instantiates a new app utility.
	 */
	/**
	 *  nidhi
	 *  5-12-2017
	 *  for reutilize drop down list details
	 */
	final static GenricServiceAsync async=GWT.create(GenricService.class);
	private final static DropDownServiceAsync dpservice=GWT.create(DropDownService.class);
	/**
	 *  end
	 */
	
	/**
	 * Date 16-06-2018 by vijay 
	 * Des :- for contract renewal services schedule branch name. for old contracts hashmap not exist so
	 */
	public static String branchName =null;

	
	public AppUtility()
	{
	}

	/**
	 * Make object list box live.
	 *
	 * @param olb the olb
	 * @param querryObject the querry object
	 * @param filters the filters
	 */
	public static void makeObjectListBoxLive(ObjectListBox<?> olb,SuperModel querryObject,Filter... filters)
	{
		Vector<Filter>filterVec= new Vector<Filter>();
		
		for(int i=0;i<filters.length;i++)
		{
			filterVec.add(filters[i]);
		}
		
		MyQuerry qu= new MyQuerry(filterVec,querryObject);
		olb.MakeLive(qu);	
	}
	
	/**
	 * Make live config.
	 *
	 * @param olb the olb
	 * @param screen the screen
	 */
	public static void MakeLiveConfig(ObjectListBox<?> olb,Screen screen)
	{
		
		    Filter filter=new Filter();
		    filter.setQuerryString("type");
		    filter.setIntValue(ConfigTypes.getConfigType(screen));
		   
		    Filter filterStatus=new Filter();
		    filterStatus.setQuerryString("status");
		    filterStatus.setBooleanvalue(true);
		    
		    makeObjectListBoxLive(olb, new Config(), filter,filterStatus);
	}
	
	private static Filter filterOnStatus()
	{
		 Filter statusfilter=new Filter();
		 statusfilter.setQuerryString("status");
		 statusfilter.setBooleanvalue(true);	
		 return statusfilter;
	}
	/**
	 * Make live category config.
	 *
	 * @param olb the olb
	 * @param screen the screen
	 */
	public static void MakeLiveCategoryConfig(ObjectListBox<?> olb,Screen screen)
	{
		    Filter filter=new Filter();
		    filter.setQuerryString("internalType");
		    filter.setIntValue(CategoryTypes.getCategoryInternalType(screen));

//		    //   rohan added this status for loading active clients
//		    Filter filterStatus=new Filter();
//		    filterStatus.setQuerryString("status");
//		    filterStatus.setBooleanvalue(true);
		    
		    
		    makeObjectListBoxLive(olb, new com.slicktechnologies.shared.common.helperlayer.ConfigCategory(), filter,filterOnStatus());
	}
		

	/**
	 * Make sales person list box live.
	 *
	 * @param olb the olb
	 */
	public static void makeSalesPersonListBoxLive(ObjectListBox<?> olb)
	{
		MyQuerry qu= new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		qu.getFilters().add(filter);
		qu.setQuerryObject(new Employee());
		olb.MakeLive(qu);
	}
	
	/**@author Sheetal : 29-05-2022
	 * Des : To make vendor list box live**/
	public static void makeVendorListBoxLive(ObjectListBox<?> olb)
	{
		MyQuerry qu= new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("vendorStatus");
		filter.setBooleanvalue(true);
		qu.getFilters().add(filter);
		qu.setQuerryObject(new Vendor());
		olb.MakeLive(qu);
	}
	
	
	public static void makeApproverListBoxLive(ObjectListBox<?> olb,String typeOfScreen)
	{
		MyQuerry qu= new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		qu.getFilters().add(filter);
		qu.setQuerryObject(new Employee());
		olb.makeApproverLive(qu,typeOfScreen);
	}

	
	/**
	 * Make branch list box live.
	 *
	 * @param olb the olb
	 */
	public static void makeBranchListBoxLive(ObjectListBox<?> olb)
	{
		MyQuerry qu= new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		qu.setQuerryObject(new Branch());
		olb.MakeLive(qu);
	}
	
	/**
	 * Make type list box live.
	 *
	 * @param olb the olb
	 * @param screen the screen
	 */
	public static void makeTypeListBoxLive(ObjectListBox<?> olb,Screen screen)
	{	
		Vector<Filter> filterVec= new Vector<Filter>();
		Filter filter=new Filter();
	    filter.setQuerryString("internalType");
	    filter.setIntValue(CategoryTypes.getCategoryInternalType(screen));
	    filterVec.add(filter);
	    filter.setQuerryString("status");//Ashwini Patil Date:4-1-2024 as inactive categories getting populated in customet type dropdown
	    filter.setBooleanvalue(true);
	    filterVec.add(filter);
	    makeObjectListBoxLive(olb, new Type(), filterVec);
	}
	//Ashwini Patil Date:4-1-2024
	public static void makeObjectListBoxLive(ObjectListBox<?> olb,SuperModel querryObject,Vector<Filter> filterVec)
	{
		MyQuerry qu= new MyQuerry(filterVec,querryObject);
		olb.MakeLive(qu);	
	}
	
	/**
	 * Make product category list box live.
	 *
	 * @param olb the olb
	 */
	public static void makeProductCategoryListBoxLive(ObjectListBox<?> olb)
	{
		MyQuerry qu= new MyQuerry();
		qu.setQuerryObject(new Category());
		olb.MakeLive(qu);
	}


	/**
	 * Customer info composite.
	 *
	 * @param customer the customer
	 * @return the person info composite
	 */
	public static PersonInfoComposite customerInfoComposite(Person customer) {
		
		Console.log("Customer Composite Called");
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(customer);
		Console.log("Querry Created");
		PersonInfoComposite personinfo=new PersonInfoComposite(querry); 
		return personinfo;
	}

		/**
		 * Initiate one to many composite.
		 *
		 * @param otmComposite the otm composite
		 * @param handler the handler
		 */
		public static void initiateOneToManyComposite(OneToManyComposite<?,?,?> otmComposite,ClickHandler handler) 
		{  
		  // MyQuerry onequerry = new MyQuerry(new Vector<Filter>(), new Category());
			
			 MyQuerry onequerry = new MyQuerry();
			 Filter retFilter=new Filter();
			 retFilter.setQuerryString("isServiceCategory");
			 retFilter.setBooleanvalue(true);
			 onequerry.getFilters().add(retFilter);
			 onequerry.setQuerryObject(new Category());
	
		   Filter filtervalue=new Filter();
		   String querrycon="categoryKey"; 
		   filtervalue.setQuerryString(querrycon);
		   Vector<Filter> manyfilter=new Vector<Filter>();
		   manyfilter.add(filtervalue);
		   MyQuerry manyquerry=new MyQuerry(manyfilter, new SuperProduct());
		  // manyquerry.setQuerryObject(new SuperProduct());
		  // manyquerry.getFilters().add(filtervalue);
		   
		   otmComposite.setOneQuerry(onequerry);
		   otmComposite.setManyQuerry(manyquerry);
		   otmComposite.initializeHashMaps();
		   otmComposite.addClickHandler(handler);
	}
		
		/**
		 * Initiate one to many composite.
		 *
		 * @param otmComposite the otm composite
		 * @param handler the handler
		 */
		public static void initiateSalesOneToManyComposite(OneToManyComposite<?,?,?> otmComposite,ClickHandler handler) 
		{  
		   //MyQuerry onequerry = new MyQuerry(new Vector<Filter>(), new Category());
		   
		   MyQuerry onequerry = new MyQuerry();
		   Filter retFilter=new Filter();
		   retFilter.setQuerryString("isSellCategory");
		   retFilter.setBooleanvalue(true);
		   onequerry.getFilters().add(retFilter);
		   onequerry.setQuerryObject(new Category());
		   
	
		   Filter filtervalue=new Filter();
		   String querrycon="categoryKey"; 
		   filtervalue.setQuerryString(querrycon);
		   Vector<Filter> manyfilter=new Vector<Filter>();
		   manyfilter.add(filtervalue);
		   MyQuerry manyquerry=new MyQuerry(manyfilter, new SuperProduct());
		  // manyquerry.setQuerryObject(new SuperProduct());
		  // manyquerry.getFilters().add(filtervalue);
		   
		   otmComposite.setOneQuerry(onequerry);
		   otmComposite.setManyQuerry(manyquerry);
		   otmComposite.initializeHashMaps();
		   otmComposite.addClickHandler(handler);
	}
		

		/**
		 * Make live config.
		 *
		 * @param olb the olb
		 * @param category the category
		 */
		public static void MakeLiveConfig(
				
				ObjectListBox<Config> olb,  SuperModel category) {
			    Filter filter=new Filter();
			    filter.setQuerryString("deleted");
			    filter.setBooleanvalue(false);
			    
			    makeObjectListBoxLive(olb, new Category(), filter);
			
		}

		
		/**
		 * React on add for one to many composite.
		 *
		 * @param otmComposite the otm composite
		 */
		public static void ReactOnAddForOneToManyComposite(OneToManyComposite<? extends SuperModel,? extends SuperModel,SalesLineItem> otmComposite)
		{
			SuperProduct product=(SuperProduct) otmComposite.getManyComboBox().getSelectedItem();
			if(product!=null)
			{
			   SalesLineItem lineItem=new SalesLineItem();
			   lineItem.setPrduct(product);
			   otmComposite.getMany().getDataprovider().getList().add(lineItem);
				
			}
			
		}

		
		/**
		 * Sets the status list box.
		 *
		 * @param lbstatus the lbstatus
		 * @param list the list
		 */
		public static void setStatusListBox(ListBox lbstatus,List<String> list)
		{
			lbstatus.addItem("--SELECT--");
			for(String temp:list)
				lbstatus.addItem(temp);
		}
		
		/*
		 * Date:25/10/2018
		 * Developer:Ashwini
		 * des:To add invoice document type
		 */
		
		public static void setInvoiceDocumentType(ListBox lbinvoicetype ){
			
			lbinvoicetype.addItem("--SELECT--");
			lbinvoicetype.addItem(AppConstants.CREATEPROFORMAINVOICE);
			lbinvoicetype.addItem(AppConstants.CREATETAXINVOICE);
			
		}
		
		/*
		 * end by Ashwini
		 */
		
		public static void createLogin(String name)
		{
			RootPanel rootPanel = RootPanel.get();
			FlowPanel flowpanel = new FlowPanel();
			ImageResources resources = GWT.create(ImageResources.class);
			Image logo = new Image(resources.logo());
			logo.addStyleName("logo");
			Label lblWelcomeToThe = new Label("Welcome To "+name);
			
			lblWelcomeToThe.setStyleName("gwt-Label-LoginTitle");
			flowpanel.add(lblWelcomeToThe);
			
			
			
			//Patch Patch
			
			
			LoginForm login = new LoginForm();
			LoginProxy model =new LoginProxy();
			LoginPresenter presenter=new LoginPresenter(login,model);
			
			flowpanel.getElement().setId("incenter");
			login.panel.addStyleName("background");
		    flowpanel.add(login.panel);
			rootPanel.add(flowpanel);
		}
		
		public static String parseDate(Date date)
		{
			DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
			return format.format(date);
		}
		
		/**
		 * @author Anil
		 * @since 09-11-2020
		 */
		public static Date formatDate(String date){
			DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
			return format.parse(date);
		}
		
		/**
		 * Date:07-11-2017 BY ANIL
		 * used to parse service date used in customer support form
		 */
		public static String parseDate1(Date date)
		{
			DateTimeFormat format= DateTimeFormat.getFormat("dd-MM-yyyy");
			return format.format(date);
		}
		
		public static DateBox returnFormattedDate(DateBox dateBox)
		{
			dateBox = new DateBoxWithYearSelector();
			DateTimeFormat format1=DateTimeFormat.getFormat("dd-MM-yyyy");
			DateBox.DefaultFormat defformat1=new DateBox.DefaultFormat(format1);
			dateBox.setFormat(defformat1);
			
			return dateBox;
		}

		public static void makeSalesPersonListBoxDepartment(
				ObjectListBox<Department> olbReportsTo) {
			
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(new Department());
			Filter filter=new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(true);
			olbReportsTo.MakeLive(querry);
			
		}
		
		public static EmployeeInfoComposite employeeInfoComposite(EmployeeInfo employee,boolean val)
		{
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(employee);
             /*****DAte 24-5-2019 by AMOL for only Active employees*******/
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(true);
	        temp.add(filter);
			querry.setFilters(temp);
			
			EmployeeInfoComposite employeeinfo=new EmployeeInfoComposite(querry,val); 
			return employeeinfo;
		}
		
		
		public static EmployeeInfoComposite InitialiseEmployeeInfoComposite(EmployeeInfo employee,boolean val)
		{
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(employee);
			
			EmployeeInfoComposite employeeinfo=new EmployeeInfoComposite(querry,val); 
			return employeeinfo;
		}
		
		
		
		
		
		
		
		
		
		
		
		// vijay   added this for employee composite with non mandetory 
		public static EmployeeInfoComposite employeeInfoComposite(EmployeeInfo employee,boolean val, boolean mandatory)
		{
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(employee);
			
			EmployeeInfoComposite employeeinfo=new EmployeeInfoComposite(querry,val,mandatory); 
			return employeeinfo;
		}
		
//		public static EmployeeInfoComposite employeeComposite(Employee employee)
//		{
//			MyQuerry querry=new MyQuerry();
//			querry.setQuerryObject(employee);
//			
//			EmployeeInfoComposite employeeinfo=new EmployeeInfoComposite(querry); 
//			return employeeinfo;
//		}
		
		

		
		public static void makeCompanyAssetListBoxLive(ObjectListBox<?> olb)
		{
			MyQuerry qu= new MyQuerry();
			qu.setQuerryObject(new CompanyAsset());
			olb.MakeLive(qu);
		}
		
		public static void makeClientSideAssetListBoxLive(ObjectListBox<?> olb)
		{
			MyQuerry qu= new MyQuerry();
			qu.setQuerryObject(new ClientSideAsset());
			olb.MakeLive(qu);
		}
		
		public static void makeToolGroupListBoxLive(ObjectListBox<?> olb)
		{
			MyQuerry qu= new MyQuerry();
			qu.setQuerryObject(new ToolGroup());
			olb.MakeLive(qu);

		}
		public static void makeShiftPatternListBoxLive(ObjectListBox<ShiftPattern> olbpattern)
		{
			olbpattern.MakeLive(new MyQuerry(new Vector<Filter>(),new ShiftPattern()));
		}
		public static void makeAssignedPatternListBoxLive(ObjectListBox<AssignedShift> olbassignshift)
		{
			olbassignshift.MakeLive(new MyQuerry(new Vector<Filter>(),new AssignedShift()));
		}
		
		
		public static Date calculateQuotationEndDate(List<SalesLineItem>items)
		{
			
			
			ArrayList<Date> datetArray=new ArrayList<Date>();
			
			
			// number of days to add
			int i=0;
			for(SalesLineItem item:items)
			{
				if(item.getPrduct() instanceof ServiceProduct)
				{
					Date tempDate= CalendarUtil.copyDate(item.getStartDate());
					int duration=item.getDuration();
					CalendarUtil.addDaysToDate(tempDate,duration-1);
					datetArray.add(tempDate);
				}
			}
			
			if(datetArray.size()==0)
			{
				return null;
			}
			Collections.sort(datetArray);
			
            return datetArray.get(datetArray.size()-1);
		}
		
		
		public static Date calculateQuotationStartDate(List<SalesLineItem> list)
		{
			ArrayList<Date> datetArray=new ArrayList<Date>();
			
			// number of days to add
			int i=0;
			for(SalesLineItem item:list)
			{
				if(item.getPrduct() instanceof ServiceProduct)
				{
					Date tempDate= CalendarUtil.copyDate(item.getStartDate());
				    datetArray.add(tempDate);
				}
			}
			
			if(datetArray.size()==0)
			{
				return null;
				
			}
			Collections.sort(datetArray);
			
            return datetArray.get(0);
			
		}
	
		public static PersonInfoComposite vendorInfoComposite(Vendor vendor) {
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(vendor);
			
			PersonInfoComposite personinfo=new PersonInfoComposite(querry); 
			return personinfo;
		}
		
		public static ProductInfoComposite product(SuperProduct superprod,boolean bool)
		{
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(superprod);
			
			
			/**	Date 5-3-2020 Added by Amol to do not show the InActive Item Product.**/
			 Filter productstatus=new Filter();
			 productstatus.setQuerryString("status");
			 productstatus.setBooleanvalue(true);
			 querry.getFilters().add(productstatus);
			
			
			
			ProductInfoComposite productinfo=new ProductInfoComposite(querry, bool); 
			System.out.println("&&"+productinfo);
			return productinfo;
		}

		public static ProductInfoComposite Addedproduct(SuperProduct product)
		{
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(product);
			
			ProductInfoComposite productinfo=new ProductInfoComposite(querry, false); 
			return productinfo;
		}
		
		/**
		 * Initiates product info composite for sales. It means only item product will be available for adding.
		 * @param product
		 * @return
		 */
		public static ProductInfoComposite initiateSalesProductComposite(SuperProduct product)
		{
			product=new ItemProduct();
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(product);
			/**Date 22-3-2019 Added by Amol to do not show the InActive Item Product.
			 * 
			 */
			 Filter productstatus=new Filter();
			 productstatus.setQuerryString("status");
			 productstatus.setBooleanvalue(true);
			 querry.getFilters().add(productstatus);
			ProductInfoComposite productinfo=new ProductInfoComposite(querry, false); 
			return productinfo;
		}
		
		/**
		 * Initiates product info composite for service. It means only service product will be available for adding.
		 * @param product
		 * @return
		 */
		
		public static ProductInfoComposite initiateServiceProductComposite(SuperProduct product)
		{
			product=new ServiceProduct();
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(product);
			ProductInfoComposite productinfo=new ProductInfoComposite(querry, false); 
			return productinfo;
		}
		
		public static ProductInfoComposite initiateServiceProductComposite(SuperProduct product,boolean searchScreen)
		{
			product=new ServiceProduct();
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(product);
			ProductInfoComposite productinfo=null;
			if(searchScreen){
				productinfo=new ProductInfoComposite(querry, false,"Search Screen"); 
			}else{
				productinfo=new ProductInfoComposite(querry, false); 
			}
			return productinfo;
		}
		
		/**
		 * For product info composite while adding in amc
		 */
		
		public static ProductInfoComposite initiatePurchaseProductComposite(SuperProduct product)
		{
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(product);
			
		/**	Date 5-3-2020 Added by Amol to do not show the InActive Item Product.**/
			 Filter productstatus=new Filter();
			 productstatus.setQuerryString("status");
			 productstatus.setBooleanvalue(true);
			 querry.getFilters().add(productstatus);
			ProductInfoComposite productinfo=new ProductInfoComposite(querry, false); 
			return productinfo;
		}
		
		/**
		 * 
		 * @param prodInfo
		 * @param customer 
		 * @param branchEntity 
		 * @param gstApplicable 
		 * @param customerId 
		 * @return
		 * 
		 * 
		 * in this method term and condition and product image are set to null
		 * because it cause serach and approval issue .known from leathertechno.
		 * 
		 * 
		 * Date 10/08/2015 
		 * By Rohan and Anil
		 */
		
		public static SalesLineItem ReactOnAddProductComposite(SuperProduct prodInfo, Customer customer, Branch branchEntity, boolean gstApplicable)
		{
			SalesLineItem lineItem=new SalesLineItem();
			ArrayList<SalesLineItem> arr=new ArrayList<SalesLineItem>();
			if(prodInfo!=null)
			{
//			   lineItem.setPrduct(prodInfo);
//			   lineItem.setTermsAndConditions(new DocumentUpload());
//			   lineItem.setProductImage(new DocumentUpload());
//			   arr.add(lineItem);
				
				
					if(prodInfo.getVatTax()!=null){
					   lineItem.setRefproductmasterTax1(prodInfo.getVatTax());
					   System.out.println("prodInfo.getVatTax()"+prodInfo.getVatTax().getTaxPrintName());
					   System.out.println("lineItem.getPrduct().getVatTax()== "+prodInfo.getVatTax().getPercentage());
					}
					if(prodInfo.getServiceTax()!=null){
					   lineItem.setRefproductmasterTax2(prodInfo.getServiceTax());
					   System.out.println("lineItem.getPrduct().getVatTax()"+prodInfo.getServiceTax().getTaxPrintName());
					   System.out.println("lineItem.getPrduct().getVatTax()=="+prodInfo.getServiceTax().getPercentage());
					}
				   
			   
			  //    rohan revert this to old  date : 20/12/2016
			  //  we will look in to this afterwards 
					System.out.println("gstApplicable ===  "+gstApplicable);
					if(customer!=null && gstApplicable && (!prodInfo.getVatTax().getTaxConfigName().equals("") ||
										!prodInfo.getServiceTax().getTaxConfigName().equals("")) ){
						
						prodInfo = getTaxDeatilsAsPerState(customer,branchEntity,prodInfo);
						
					}
					else{
						System.out.println("GST NA");
//						if(prodInfo.getVatTax().getTaxConfigName().equals("") &&
//										prodInfo.getServiceTax().getTaxConfigName().equals("")){
							 Tax tax1 = new Tax();
							 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
							 if(tax1!=null)
							 prodInfo.setVatTax(tax1);
							 
							 Tax tax2 = new Tax();
							 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
							 if(tax2!=null)
							 prodInfo.setServiceTax(tax2);
//						}
						
					}
				
				   lineItem.setPrduct(prodInfo);
				   
//				   System.out.println("PRINTABLE NAME : "+lineItem.getServiceTax().getTaxPrintName());
				   
				   lineItem.setTermsAndConditions(new DocumentUpload());
				   lineItem.setProductImage(new DocumentUpload());
				   lineItem.getPrduct().setProductImage1(new DocumentUpload());
				   lineItem.getPrduct().setProductImage2(new DocumentUpload());
				   lineItem.getPrduct().setProductImage3(new DocumentUpload());
				   
				   //Date 17-08-2017 added by vijay for description having more than 500 chars so quotation not allowing to save so
				   lineItem.getPrduct().setComment("");
				   lineItem.getPrduct().setCommentdesc("");
				   lineItem.getPrduct().setCommentdesc1("");
				   lineItem.getPrduct().setCommentdesc2("");
				   
				   /**
				    *  nidhi
				    *   9-10-2018
				    *   *:*:*
				    */
				   lineItem.setAreaUnit(prodInfo.getUnitOfMeasurement()); /** end*/
				   
				   
				   
				   arr.add(lineItem);
				
			}
			return lineItem;
		}
		
		

		public static SalesLineItem ReactOnAddProductComposite(SuperProduct prodInfo)
		{
			SalesLineItem lineItem=new SalesLineItem();
			ArrayList<SalesLineItem> arr=new ArrayList<SalesLineItem>();
			if(prodInfo!=null)
			{
//			   lineItem.setPrduct(prodInfo);
//			   lineItem.setTermsAndConditions(new DocumentUpload());
//			   lineItem.setProductImage(new DocumentUpload());
//			   arr.add(lineItem);
				
			   
			  //    rohan revert this to old  date : 20/12/2016
			  //  we will look in to this afterwards 
			   
				   lineItem.setPrduct(prodInfo);
				   
				   System.out.println("PRINTABLE NAME : "+lineItem.getServiceTax().getTaxPrintName());
				   
				   lineItem.setTermsAndConditions(new DocumentUpload());
				   lineItem.setProductImage(new DocumentUpload());
				   lineItem.getPrduct().setProductImage1(new DocumentUpload());
				   lineItem.getPrduct().setProductImage2(new DocumentUpload());
				   lineItem.getPrduct().setProductImage3(new DocumentUpload());
				   
				   //Date 17-08-2017 added by vijay for description having more than 500 chars so quotation not allowing to save so
				   lineItem.getPrduct().setComment("");
				   lineItem.getPrduct().setCommentdesc("");
				   lineItem.getPrduct().setCommentdesc1("");
				   lineItem.getPrduct().setCommentdesc2("");
				   
				   /**
				    *  nidhi
				    *   9-10-2018
				    *   *:*:*
				    */
				   lineItem.setAreaUnit(prodInfo.getUnitOfMeasurement()); /** end*/
				   arr.add(lineItem);
				
			}
			return lineItem;
		}
		
		
	
		public static SalesLineItem ReactOnAddProductCompositeForSales(SuperProduct prodInfo)
		{
			SalesLineItem lineItem=new SalesLineItem();
			ArrayList<SalesLineItem> arr=new ArrayList<SalesLineItem>();
			if(prodInfo!=null)
			{
			   lineItem.setPrduct(prodInfo);
			   
			   System.out.println("Sales = PRINTABLE NAME : "+lineItem.getServiceTax().getTaxPrintName());
			   System.out.println("Hi vijay =="+lineItem.getPrduct().getServiceTax().getTaxPrintName());

			   lineItem.setTermsAndConditions(new DocumentUpload());
			   lineItem.setProductImage(new DocumentUpload());
			   lineItem.getPrduct().setProductImage1(new DocumentUpload());
			   lineItem.getPrduct().setProductImage2(new DocumentUpload());
			   lineItem.getPrduct().setProductImage3(new DocumentUpload());
			   arr.add(lineItem);
			}
			return lineItem;
		}
		
		//*****************************
		
		public static ProductInfoComposite initiateMrnProductComposite(SuperProduct product)
		{
			product=new ItemProduct();
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(product);
			
			/**Date 22-3-2019 Added by Amol to do not show the InActive Item Product.
			 * 
			 */
			 Filter productstatus=new Filter();
			 productstatus.setQuerryString("status");
			 productstatus.setBooleanvalue(true);
			 querry.getFilters().add(productstatus);
			
			ProductInfoComposite productinfo=new ProductInfoComposite(querry, false); 
			return productinfo;
		}
		
		public static ProductDetailsPO ReactOnAddPurchaseProductComposite(SuperProduct prodInfo)
		{
		
			SalesLineItem lineItem=new SalesLineItem();
			ProductDetailsPO poproducts=new ProductDetailsPO();
			
			if(prodInfo instanceof ServiceProduct){
				if(prodInfo!=null)
				{
					lineItem.setPrduct(prodInfo);
					lineItem.setTermsAndConditions(new DocumentUpload());
				    lineItem.setProductImage(new DocumentUpload());
				    lineItem.getPrduct().setProductImage1(new DocumentUpload());
				    lineItem.getPrduct().setProductImage2(new DocumentUpload());
				    lineItem.getPrduct().setProductImage3(new DocumentUpload());
					
					poproducts.setProductID(lineItem.getPrduct().getCount());
					poproducts.setProductCategory(lineItem.getProductCategory());
					poproducts.setProductCode(lineItem.getProductCode());
					poproducts.setProductName(lineItem.getProductName());
					poproducts.setUnitOfmeasurement(lineItem.getUnitOfMeasurement());
					poproducts.setProdPrice(lineItem.getPrice());
					poproducts.setTax(lineItem.getServiceTax().getPercentage());
					poproducts.setVat(lineItem.getVatTax().getPercentage());
					poproducts.setProductQuantity(lineItem.getQty());
					poproducts.setDiscount(lineItem.getPercentageDiscount());
				}
			}
			if(prodInfo instanceof ItemProduct){
				System.out.println("In side item product");
				
				prodInfo.setTermsAndConditions(new DocumentUpload());
				prodInfo.setProductImage(new DocumentUpload());
				prodInfo.setProductImage1(new DocumentUpload());
				prodInfo.setProductImage2(new DocumentUpload());
				prodInfo.setProductImage3(new DocumentUpload());
				lineItem.setPrduct(prodInfo);
				
				poproducts.setProductID(lineItem.getPrduct().getCount());
				poproducts.setProductCategory(lineItem.getProductCategory());
				poproducts.setProductCode(lineItem.getProductCode());
				poproducts.setProductName(lineItem.getProductName());
				poproducts.setUnitOfmeasurement(lineItem.getUnitOfMeasurement());
				
				/**
				 * Date : 10-07-2018 BY ANIL
				 */
//				poproducts.setProdPrice(lineItem.getPrice());
//				poproducts.setTax(lineItem.getServiceTax().getPercentage());
//				poproducts.setVat(lineItem.getVatTax().getPercentage());
				
				
				poproducts.setSalesPrice(prodInfo.getPrice());
				poproducts.setSalesTax1(prodInfo.getVatTax());
				poproducts.setSalesTax2(prodInfo.getServiceTax());
				
				
				
				poproducts.setProdPrice(lineItem.getPrduct().getPurchasePrice());
				poproducts.setTax(lineItem.getPrduct().getPurchaseTax2().getPercentage());
				poproducts.setVat(lineItem.getPrduct().getPurchaseTax1().getPercentage());
				
				poproducts.setProductQuantity(lineItem.getQty());
				poproducts.setDiscount(lineItem.getPercentageDiscount());
				poproducts.setPrduct(prodInfo);
				
				
				/**
				 * @author Anil @since 11-05-2021
				 * Setting Purchase Tax Value
				 */
				try{
					poproducts.setPurchaseTax1(lineItem.getPrduct().getPurchaseTax1());
					poproducts.setPurchaseTax2(lineItem.getPrduct().getPurchaseTax2());
				}catch(Exception e){
					
				}
				
				
				/**
				 * @author Anil
				 * @since 05-01-2021
				 * tax were getting mapped on purchase register
				 */
				try{
					poproducts.setVatTax(lineItem.getPrduct().getPurchaseTax1());
					poproducts.setServiceTax(lineItem.getPrduct().getPurchaseTax2());
				}catch(Exception e){
					
				}
				/**
				 * End
				 */
				
				
			}
			return poproducts;
				}
		
		
		public static ProductDetails ReactOnAddPurchaseReqProductComposite(SuperProduct prodInfo)
		{
			SalesLineItem lineItem=new SalesLineItem();
			ProductDetails poproducts=new ProductDetails();
			
			if(prodInfo instanceof ServiceProduct){
				if(prodInfo!=null)
				{
					lineItem.setPrduct(prodInfo);
					
					poproducts.setProductID(lineItem.getPrduct().getCount());
					poproducts.setProductCategory(lineItem.getProductCategory());
					poproducts.setProductCode(lineItem.getProductCode());
					poproducts.setProductName(lineItem.getProductName());
					poproducts.setUnitOfmeasurement(lineItem.getUnitOfMeasurement());
					poproducts.setProdPrice(lineItem.getPrice());
					poproducts.setServiceTax(lineItem.getServiceTax().getPercentage());
					poproducts.setVat(lineItem.getVatTax().getPercentage());
					poproducts.setProductQuantity(lineItem.getQty());
					poproducts.setPrduct(prodInfo);
//					poproducts.setDiscount(lineItem.getPercentageDiscount());
				}
			}
			if(prodInfo instanceof ItemProduct){
				prodInfo.setTermsAndConditions(new DocumentUpload());
				prodInfo.setProductImage(new DocumentUpload());
				prodInfo.setProductImage1(new DocumentUpload());
				prodInfo.setProductImage2(new DocumentUpload());
				prodInfo.setProductImage3(new DocumentUpload());
				
				lineItem.setPrduct(prodInfo);
				poproducts.setProductID(lineItem.getPrduct().getCount());
				poproducts.setProductCategory(lineItem.getProductCategory());
				poproducts.setProductCode(lineItem.getProductCode());
				poproducts.setProductName(lineItem.getProductName());
				poproducts.setUnitOfmeasurement(lineItem.getUnitOfMeasurement());
				
				/**
				 * Date : 10-07-2018 By ANIL
				 * Setting Purchase Price instead of selling Price
				 */
//				poproducts.setProdPrice(lineItem.getPrice());
//				poproducts.setServiceTax(lineItem.getServiceTax().getPercentage());
//				poproducts.setVat(lineItem.getVatTax().getPercentage());
				
				ItemProduct itemProd=(ItemProduct) prodInfo;
				
				poproducts.setProdPrice(itemProd.getPurchasePrice());
				poproducts.setServiceTax(itemProd.getPurchaseTax1().getPercentage());
				poproducts.setVat(itemProd.getPurchaseTax2().getPercentage());
				
				/**
				 * End
				 */
				
				poproducts.setProductQuantity(lineItem.getQty());
				poproducts.setPrduct(prodInfo);
//				poproducts.setDiscount(lineItem.getPercentageDiscount());
			}
			return poproducts;
		}
		
		
		public static GRNDetails ReactOnAddGRNProductComposite(SuperProduct prodInfo)
		{
			SalesLineItem lineItem=new SalesLineItem();
			GRNDetails poproducts=new GRNDetails();
			
			if(prodInfo instanceof ServiceProduct){
				if(prodInfo!=null)
				{
					lineItem.setPrduct(prodInfo);
					
					poproducts.setProductID(lineItem.getPrduct().getCount());
					poproducts.setProductCategory(lineItem.getProductCategory());
					poproducts.setProductCode(lineItem.getProductCode());
					poproducts.setProductName(lineItem.getProductName());
					poproducts.setUnitOfmeasurement(lineItem.getUnitOfMeasurement());
					poproducts.setProdPrice(lineItem.getPrice());
					poproducts.setTax(lineItem.getServiceTax().getPercentage());
					poproducts.setVat(lineItem.getVatTax().getPercentage());
					poproducts.setProductQuantity(lineItem.getQty());
					poproducts.setWarehouseLocation("");
					poproducts.setStorageLoc("");
					poproducts.setStorageBin("");
//					poproducts.setDiscount(lineItem.getPercentageDiscount());
				}
			}
			if(prodInfo instanceof ItemProduct){
				/**
				 * Date : 22-12-2018 By ANIL
				 * made image and terms condition to null
				 * AS due to data issue grn is not getting serached for sasha
				 */
				prodInfo.setTermsAndConditions(new DocumentUpload());
				prodInfo.setProductImage(new DocumentUpload());
				prodInfo.setProductImage1(new DocumentUpload());
				prodInfo.setProductImage2(new DocumentUpload());
				prodInfo.setProductImage3(new DocumentUpload());
				/**
				 * End
				 */
				lineItem.setPrduct(prodInfo);
				
				/**
				 * @author Anil , Date : 01-07-2019
				 * if we were creating GRN independently of PO then it was not getting approved
				 * issue raised by Sonu for Sasha
				 */
				poproducts.setPrduct(prodInfo);
				/**
				 * End
				 */
				
				poproducts.setProductID(lineItem.getPrduct().getCount());
				poproducts.setProductCategory(lineItem.getProductCategory());
				poproducts.setProductCode(lineItem.getProductCode());
				poproducts.setProductName(lineItem.getProductName());
				poproducts.setUnitOfmeasurement(lineItem.getUnitOfMeasurement());
				poproducts.setProdPrice(lineItem.getPrice());
				poproducts.setTax(lineItem.getServiceTax().getPercentage());
				poproducts.setVat(lineItem.getVatTax().getPercentage());
				poproducts.setProductQuantity(lineItem.getQty());
				poproducts.setWarehouseLocation("");
				poproducts.setStorageLoc("");
				poproducts.setStorageBin("");
//				poproducts.setDiscount(lineItem.getPercentageDiscount());
			}
			return poproducts;
		}
		
		public static InspectionDetails ReactOnAddInspectionProductComposite(SuperProduct prodInfo)
		{
			SalesLineItem lineItem=new SalesLineItem();
			InspectionDetails poproducts=new InspectionDetails();
			
			if(prodInfo instanceof ItemProduct){
				lineItem.setPrduct(prodInfo);
				
				poproducts.setProductID(lineItem.getPrduct().getCount());
				poproducts.setProductCategory(lineItem.getProductCategory());
				poproducts.setProductCode(lineItem.getProductCode());
				poproducts.setProductName(lineItem.getProductName());
				poproducts.setUnitOfmeasurement(lineItem.getUnitOfMeasurement());
				poproducts.setProdPrice(lineItem.getPrice());
				poproducts.setTax(lineItem.getServiceTax().getPercentage());
				poproducts.setVat(lineItem.getVatTax().getPercentage());
				poproducts.setProductQuantity(lineItem.getQty());
				poproducts.setWarehouseLocation("");
				poproducts.setStorageLoc("");
				poproducts.setStorageBin("");
				poproducts.setReasonForReject("");
			}
			return poproducts;
		}
		
		
		public static boolean applyDownloadAuthorization(List<ScreenAuthorization> scrAuth,Screen scrName,String moduleName)
		{
			boolean downloadAuth=false;
			for(int i=0;i<scrAuth.size();i++)
			{
				if(scrAuth.get(i).getModule().trim().equals(moduleName)&&scrAuth.get(i).getScreens().trim().equals(scrName))
				{
					downloadAuth=scrAuth.get(i).isDownload();
				}
			}
			
			return downloadAuth;
		}
		
		
		public static boolean applyCreateAuthorization(List<ScreenAuthorization> scrAuth,Screen scrName,String moduleName)
		{
			boolean createAuth=false;
			for(int i=0;i<scrAuth.size();i++)
			{
				if(scrAuth.get(i).getModule().trim().equals(moduleName)&&scrAuth.get(i).getScreens().trim().equals(scrName))
				{
					createAuth=scrAuth.get(i).isCreate();
				}
			}
			
			return createAuth;
		}
		
		
		
		
		public static <T> void ApplyWhiteColoronRow(SuperTable<T>table,final int index) 
		{  
			table.getTable().setRowStyles(new RowStyles<T>() {
			    @Override
			    public String getStyleNames(T object, int rowIndex) {
			             return "";
			        } 
			    });
	    }
		
		public static <T> void ApplyRedColoronRow(SuperTable<T>table,final int index) 
		{  
			table.getTable().setRowStyles(new RowStyles<T>() {
			    @Override
			    public String getStyleNames(T object, int rowIndex) {
			    
			    	if(rowIndex==index)
				    	return "redBackground";
				    	return "";
			        } 
			    });
	    }
		
		
		public static void makeLiveTypeDropDown(ObjectListBox<?> olbType,String catNameVal,String catCodeVal,int internalTypeVal)
		{
			Console.log("Apputility Type Method Called");
			int count=olbType.getItemCount()-1;
			ArrayList<Type> arrayOfType=new ArrayList<Type>();
			arrayOfType=LoginPresenter.globalType;
			for(int i=count;i>=0;i--)
			{
				olbType.removeItem(i);
			}
			
			olbType.addItem("--SELECT--");
			
			/**
			 * @author Anil @since 23-04-2021
			 */
//			if(arrayOfType.size()==0){
				olbType.categoryName=catNameVal;
//			}else{
//				olbType.categoryName=null;
//			}
			Console.log("Type :: "+catNameVal +" | "+olbType.categoryName);
			if(LoginPresenter.myUserEntity!=null&&LoginPresenter.myUserEntity.getRoleName()!=null
					&&(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") 
					|| LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator"))){
				olbType.addItem("New");
			}
			
			System.out.println(" .......array size"+arrayOfType.size());
			for(int a=0;a<arrayOfType.size();a++)
			{
				// below condition type status condtion addded by vijay for dont load inactive types
//				if(arrayOfType.get(a).getCategoryName().trim().equals(catNameVal.trim())&&arrayOfType.get(a).getCatCode().trim().equals(catCodeVal.trim())&&arrayOfType.get(a).getInternalType()==internalTypeVal && arrayOfType.get(a).getStatus())
				if(arrayOfType.get(a).getCategoryName().trim().equals(catNameVal.trim())&&arrayOfType.get(a).getInternalType()==internalTypeVal && arrayOfType.get(a).getStatus())
				{
					System.out.println("in side final condition");
					olbType.addItem(arrayOfType.get(a).getTypeName());
				}
			}
		}
		
		
/****************** Mailtain History Method **************************************/
		
		public static void addDocumentToHistoryTable(String moduleName,String documentType,Integer documentId,Integer personId,String personName,Long cell,boolean isList,SuperModel model,ArrayList<SuperModel>listModel ){
			System.out.println("1");
			
			checkDocumentIdAlreadyExist(moduleName,documentType,documentId);
			System.out.println("2");
			
			if(LoginPresenter.globalHistoryList.size()<10){
				System.out.println("3");
				History his=new History();
				
				his.setModuleName(moduleName);
				his.setDocumentType(documentType);
				his.setDocumentId(documentId);
				his.setListForm(isList);
				his.setModel(model);
				his.setListModel(listModel);
				his.setPersonId(personId);
				his.setPersonName(personName);
				if(cell!=null)
					his.setPersonCell(cell);
				
				LoginPresenter.globalHistoryList.add(his);
			}else{
				System.out.println("4");
				LoginPresenter.globalHistoryList.remove(0);
				
				History his=new History();
				
				his.setModuleName(moduleName);
				his.setDocumentType(documentType);
				his.setDocumentId(documentId);
				his.setListForm(isList);
				his.setModel(model);
				his.setListModel(listModel);
				
				LoginPresenter.globalHistoryList.add(his);
				
			}
		}

		public static void checkDocumentIdAlreadyExist(String moduleName,String documentType,Integer documentId){
			System.out.println("11");
			if(documentId!=null){
			for(int i=0;i<LoginPresenter.globalHistoryList.size();i++){
				if(LoginPresenter.globalHistoryList.get(i).getModuleName().equals(moduleName)&&
						LoginPresenter.globalHistoryList.get(i).getDocumentType().equals(documentType)&&
						LoginPresenter.globalHistoryList.get(i).getDocumentId().equals(documentId)){
					System.out.println("11111111");
					LoginPresenter.globalHistoryList.remove(i);
				}
			}
			}
			
		}
		
	
		

		/**************************LOAD APPROVER LIST ACCORDING TO AMOUNT ****************************/
		
		public static void getApproverListAsPerTotalAmount(ObjectListBox<?> olb,String documentName,String approverName,Double amount)
		{
			/**
			 * Date : 16-05-2017 by ANIL
			 * Commented this code as  the logic is changed for loading approvers.
			 * earlier if quotation value changes its approver name for that level also changes depending upon the approver limit assiged to him. 
			 *  
			 */
			
//			Console.log("1-Amount Level Approver");
//			ArrayList<ProcessName> processNme=LoginPresenter.globalProcessName;
//			boolean flagApproval=false;
//			for(int b=0;b<processNme.size();b++)
//			{
//				if(processNme.get(b).getProcessName().trim().equals(AppConstants.GLOBALRETRIEVALMULTILEVELAPPROVAL)&&processNme.get(b).isStatus()==true)
//				{
//					flagApproval=true;
//				}
//			}
//			
//			if(flagApproval==true){
//				ArrayList<MultilevelApproval> approvalList=LoginPresenter.globalMultilevelApproval;
//				for(int y=0;y<approvalList.size();y++){
//					if(approvalList.get(y).getDocumentType().trim().equalsIgnoreCase(documentName.trim())&&approvalList.get(y).isDocStatus()==true){
//						Console.log("2-MultiLevel DOC Status");
//						olb.setApproverDropDownAsPerDocumentAmount(approvalList.get(y), amount);
//						break;
//					}
//				}
//				olb.setValue(approverName);
//			}
			
			
			/**
			 * ends here
			 */
		
		}
		
		
		/**
		 * This method checks whether branch should be mandatory or not by process configuration.
		 * Date : 14-09-2016 By Anil
		 * Release : 31 Aug 2016
		 * Project : INVENTORY ERROR SOLVING(NBHC)
		 * I/P : Document Name
		 */
		
		public static boolean isBranchNonMandatory(String docType){
			if(LoginPresenter.globalProcessConfig.size()!=0){
				for(ProcessConfiguration proConf:LoginPresenter.globalProcessConfig){
					if(proConf.getProcessName().equals(docType)&&proConf.isConfigStatus()==true){
						for(ProcessTypeDetails proTypDet:proConf.getProcessList()){
							if(proTypDet.getProcessType().equalsIgnoreCase("BRANCHNONMANDATORY")&&proTypDet.isStatus()==true){
								return true;
							}
						}
					}
				}
			}
			return false;
		}

		
		/**
		 * Developed By : Rohan Bhagde
		 * Used for : used for customer branch drop down loading on customer screen 
		 * Need  : NBHC required no branch restriction in customer screen 
		 * Date : 20-09-2016
		 */
		public static void makeCustomerBranchListBoxLive(ObjectListBox<Branch> olbbBranch) {
			MyQuerry qu= new MyQuerry();
			Filter filter=new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(true);
			qu.setQuerryObject(new Branch());
			olbbBranch.MakeLiveCustomerScreen(qu);
			
		}
		
		/**
		 * Developed By : Anil Pal
		 * Used for : creats forgot password form
		 * Date : 28-09-2016
		 */
		public static void createForgotPasswordScreen(String name)
		{
			RootPanel rootPanel = RootPanel.get();
			FlowPanel flowpanel = new FlowPanel();
			
			ImageResources resources = GWT.create(ImageResources.class);
			Image logo = new Image(resources.logo());
			logo.addStyleName("logo");
//			flowpanel.add(logo);
			
			Label lblWelcomeToThe = new Label("Welcome To "+name);
			lblWelcomeToThe.setStyleName("gwt-Label-LoginTitle");
			flowpanel.add(lblWelcomeToThe);
			
			ForgotPasswordForm form = new ForgotPasswordForm();
			ForgotPasswordPresenter presenter=new ForgotPasswordPresenter(form);
			
			flowpanel.getElement().setId("incenter");
			form.panel.addStyleName("background");
		    flowpanel.add(form.panel);
			rootPanel.add(flowpanel);
		}
		
		
		
		/**
	     * Developed By : Rohan Bhagde
	     * Purpose : This is global method use to load process confog   
	     * @return 
	     */
	    public static boolean checkForProcessConfigurartionIsActiveOrNot(String processName , String processType) {
	    	
	    	List<ProcessConfiguration> processList = LoginPresenter.globalProcessConfig;
	    	int cnt =0;
	    	for(int k=0;k<processList.size();k++){	
				if(processList.get(k).getProcessName().trim().equalsIgnoreCase(processName)){
				
					for (int i = 0; i < processList.get(k).getProcessList().size(); i++) {
						if(processList.get(k).getProcessList().get(i).getProcessType().equalsIgnoreCase(processType)
								&& processList.get(k).getProcessList().get(i).isStatus()==true)
						{
							cnt = cnt +1;	
						}
					}
					
				}	
	    	}
					
				if(cnt > 0)
				{	
					return true;
				}
	    	
				else
				{
					return false; 	
				}
				
		}

	    
	    
	    /**
	     * Date : 30-12-2016 By Anil
	     * This method initialize warehouse as per branch restriction
	     */
	    
	    
		public static void initializeWarehouse(ObjectListBox<WareHouse> olbWarehouse){
			
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(new WareHouse());
			
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(true);
			querry.getFilters().add(filter);
			
			if (LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")) {
				olbWarehouse.MakeLive(querry);
			}else{
				boolean isBranchRestricted=false;
				if(LoginPresenter.globalProcessConfig.size()!=0){
					for(ProcessConfiguration proConf:LoginPresenter.globalProcessConfig){
						if(proConf.getProcessName().equals("Branch")&&proConf.isConfigStatus()==true){
							for(ProcessTypeDetails proTypDet:proConf.getProcessList()){
								if(proTypDet.getProcessType().equalsIgnoreCase("BRANCHLEVELRESTRICTION")&&proTypDet.isStatus()==true){
									isBranchRestricted= true;
									break;
								}
							}
						}
					}
				}
				if(isBranchRestricted){
					HashSet<String> branchSet=new HashSet<String>();
					for(Employee emp:LoginPresenter.globalEmployee){
//						branchSet.add(emp.getBranchName().trim());
						if(UserConfiguration.getInfo()!=null&&UserConfiguration.getInfo().getEmpCount()==emp.getCount()){
							if(emp.getEmpBranchList()!=null&&emp.getEmpBranchList().size()!=0){
								for(EmployeeBranch empBranch:emp.getEmpBranchList()){
									branchSet.add(empBranch.getBranchName().trim());
								}
							}
						}
					}
					List<String> list=new ArrayList<String>();
					list.addAll(branchSet);
					
					if(list.size()!=0){
						filter=new Filter();
						filter.setQuerryString("branchdetails.branchName IN");
						filter.setList(list);
						querry.getFilters().add(filter);
						
						filter=new Filter();
						filter.setQuerryString("companyId");
						filter.setLongValue(UserConfiguration.getCompanyId());
						querry.getFilters().add(filter);
						
						olbWarehouse.MakeLive(querry);
					}
					
				}else{
					olbWarehouse.MakeLive(querry);
				}
				
			}
			
			
		}
		
		
	     public static boolean isBranchRestricted(){
//	        	if (!LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")) {
//	                boolean isBranchRestricted=false;
	                
	        		if(LoginPresenter.globalProcessConfig.size()!=0){
	                    for(ProcessConfiguration proConf:LoginPresenter.globalProcessConfig){
	                        if(proConf.getProcessName().equals("Branch")&&proConf.isConfigStatus()==true){
	                            for(ProcessTypeDetails proTypDet:proConf.getProcessList()){
	                                if(proTypDet.getProcessType().equalsIgnoreCase("BRANCHLEVELRESTRICTION")&&proTypDet.isStatus()==true){
	                                	return true;
	                                }
	                            }
	                        }
	                    }
	                }
	                
//	                if(isBranchRestricted){
//	                    HashSet<String> branchSet=new HashSet<String>();
//	                    for(Employee emp:LoginPresenter.globalEmployee){
//	                        branchSet.add(emp.getBranchName().trim());
//	                        if(UserConfiguration.getInfo()!=null&&UserConfiguration.getInfo().getEmpCount()==emp.getCount()){
//	                            if(emp.getEmpBranchList()!=null&&emp.getEmpBranchList().size()!=0){
//	                                for(EmployeeBranch empBranch:emp.getEmpBranchList()){
//	                                    branchSet.add(empBranch.getBranchName().trim());
//	                                }
//	                            }
//	                        }
//	                    }
//	                    List<String> list=new ArrayList<String>();
//	                    list.addAll(branchSet);
//	                }
	        		
	                return false;
	        }
		
		
	     
	     
	     /**
		     * Date : 24-01-2016 By Rohan
		     * This method initialize warehouse as per branch restriction
		     */
		    
		    
			public static void initializePettyCashName(ObjectListBox<PettyCash> olbWarehouse){
				
				MyQuerry querry=new MyQuerry();
				querry.setQuerryObject(new PettyCash());
				
				Filter filter=null;
				
				filter=new Filter();
				filter.setQuerryString("status");
				filter.setBooleanvalue(true);
				querry.getFilters().add(filter);
				
				if (LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")) {
					olbWarehouse.MakeLive(querry);
				}else{
					boolean isBranchRestricted=false;
					if(LoginPresenter.globalProcessConfig.size()!=0){
						for(ProcessConfiguration proConf:LoginPresenter.globalProcessConfig){
							if(proConf.getProcessName().equals("Branch")&&proConf.isConfigStatus()==true){
								for(ProcessTypeDetails proTypDet:proConf.getProcessList()){
									if(proTypDet.getProcessType().equalsIgnoreCase("BRANCHLEVELRESTRICTION")&&proTypDet.isStatus()==true){
										isBranchRestricted= true;
										break;
									}
								}
							}
						}
					}
					if(isBranchRestricted){
						HashSet<String> branchSet=new HashSet<String>();
						for(Employee emp:LoginPresenter.globalEmployee){
//							branchSet.add(emp.getBranchName().trim());
							if(UserConfiguration.getInfo()!=null&&UserConfiguration.getInfo().getEmpCount()==emp.getCount()){
								if(emp.getEmpBranchList()!=null&&emp.getEmpBranchList().size()!=0){
									for(EmployeeBranch empBranch:emp.getEmpBranchList()){
										branchSet.add(empBranch.getBranchName().trim());
									}
								}
							}
						}
						List<String> list=new ArrayList<String>();
						list.addAll(branchSet);
						
						if(list.size()!=0){
							filter=new Filter();
							filter.setQuerryString("branchdetails.branchName IN");
							filter.setList(list);
							querry.getFilters().add(filter);
							
							filter=new Filter();
							filter.setQuerryString("companyId");
							filter.setLongValue(UserConfiguration.getCompanyId());
							querry.getFilters().add(filter);
							
							olbWarehouse.MakeLive(querry);
						}
						
					}else{
						olbWarehouse.MakeLive(querry);
					}
					
				}
				
				
			}
	     
	     
			/**
			 * This method checks whether auto code generation is true or not
			 * Date : 07-10-2016 By Anil
			 * Release : 30 Sept 2016
			 * Project : PURCHASE MODIFICATION (NBHC)
			 * I/P : Document Name
			 */
			public static boolean isAutoGenerateCode(String docType){
				if(LoginPresenter.globalProcessConfig.size()!=0){
					for(ProcessConfiguration proConf:LoginPresenter.globalProcessConfig){
						if(proConf.getProcessName().equals(docType)&&proConf.isConfigStatus()==true){
							for(ProcessTypeDetails proTypDet:proConf.getProcessList()){
								if(proTypDet.getProcessType().equalsIgnoreCase("CODEAUTOGENERATE")&&proTypDet.isStatus()==true){
									return true;
								}
							}
						}
					}
				}
				return false;
			}	
			
			
			/**
			 * This method checks whether on PR Approval Mail should send to Po Purchase Engg , 
			 * Date : 14-10-2016 By Anil
			 * Release : 30 Sept 2016
			 * Project : PURCHASE MODIFICATION (NBHC)
			 * I/P : Document Name
			 */
			public static boolean isPrAssignToPO(String docType){
				if(LoginPresenter.globalProcessConfig.size()!=0){
					for(ProcessConfiguration proConf:LoginPresenter.globalProcessConfig){
						if(proConf.getProcessName().equals(docType)&&proConf.isConfigStatus()==true){
							for(ProcessTypeDetails proTypDet:proConf.getProcessList()){
								if(proTypDet.getProcessType().equalsIgnoreCase("PRAPPROVALMAILTOPOPURENGG")&&proTypDet.isStatus()==true){
									return true;
								}
							}
						}
					}
				}
				return false;
			}
			
			
			/**
			 * This method checks whether PR HEAD should be selected as default purchase engineer
			 * Date : 23-11-2016 By Anil
			 * Project : PURCHASE MODIFICATION (NBHC)DEADSTOCK
			 * I/P : Document Name
			 */
			public static boolean isPrHeadAsDefaultPurEngg(String docType){
				if(LoginPresenter.globalProcessConfig.size()!=0){
					for(ProcessConfiguration proConf:LoginPresenter.globalProcessConfig){
						if(proConf.getProcessName().equals(docType)&&proConf.isConfigStatus()==true){
							for(ProcessTypeDetails proTypDet:proConf.getProcessList()){
								if(proTypDet.getProcessType().equalsIgnoreCase("PRHEADASDEFAULTPURENGG")&&proTypDet.isStatus()==true){
									return true;
								}
							}
						}
					}
				}
				return false;
			}
			
			
			/**
			 * This method checks whether PO HEAD should be selected as default purchase engineer for PO
			 * Date : 31-01-2017 By Anil
			 * Project : PURCHASE MODIFICATION (NBHC)DEADSTOCK
			 * I/P : Document Name
			 */
			public static boolean isPoHeadAsDefaultPurEngg(String docType){
				if(LoginPresenter.globalProcessConfig.size()!=0){
					for(ProcessConfiguration proConf:LoginPresenter.globalProcessConfig){
						if(proConf.getProcessName().equals(docType)&&proConf.isConfigStatus()==true){
							for(ProcessTypeDetails proTypDet:proConf.getProcessList()){
								if(proTypDet.getProcessType().equalsIgnoreCase("ADDPOPURENGGINPR")&&proTypDet.isStatus()==true){
									return true;
								}
							}
						}
					}
				}
				return false;
			}
			
			
			
			

			/**
			 * Date : 03-03-2017 by ANil
			 * this method checks whether selected PR is NORMAL PR/SERVICE PR
			 */
			
			public static String getPrType(ArrayList<ProductDetails> productList){
				System.out.println("1 ::: "+productList);
				if(productList.size()==0){
					return null;
				}
				String type=null;
				for(ProductDetails obj:productList){
					System.out.println("3");
					if(obj.getPrduct() instanceof ItemProduct){
						type="Normal PR";
						System.out.println("4");
						break;
					}else{
						type="Service PR";
						System.out.println("5");
						break;
					}
				}
				System.out.println("6");
				return type;
			}
			
			/**
			 * Date : 28-03-2017 By ANIL
			 * This method is used to schedule the services as per branch and/or day selected. 
			 */
			
			public static ArrayList<ServiceSchedule> getScheduleList(List<SalesLineItem> itemList){
				boolean unitFlag = false;
				DateTimeFormat format = DateTimeFormat.getFormat("c");
				ArrayList<ServiceSchedule> serSchList = new ArrayList<ServiceSchedule>();
				for (int i = 0; i < itemList.size(); i++) {

					/**
					 * Date 02-05-2018 
					 * Developer : Vijay
					 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
					 * so i have updated below code using hashmap. 
					 */
					
//					/**
//					 * Date : 03-04-2017 By ANIL
//					 */
////					ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(itemList.get(i).getBranchSchedulingInfo());
//					
//					String[] branchArray=new String[5];
//					if(itemList.get(i).getBranchSchedulingInfo()!=null
//							&&!itemList.get(i).getBranchSchedulingInfo().equals("")){
//						branchArray[0]=itemList.get(i).getBranchSchedulingInfo();
//					}
//					if(itemList.get(i).getBranchSchedulingInfo1()!=null
//							&&!itemList.get(i).getBranchSchedulingInfo1().equals("")){
//						branchArray[1]=itemList.get(i).getBranchSchedulingInfo1();
//					}
//					if(itemList.get(i).getBranchSchedulingInfo2()!=null
//							&&!itemList.get(i).getBranchSchedulingInfo2().equals("")){
//						branchArray[2]=itemList.get(i).getBranchSchedulingInfo2();
//					}
//					if(itemList.get(i).getBranchSchedulingInfo3()!=null
//							&&!itemList.get(i).getBranchSchedulingInfo3().equals("")){
//						branchArray[3]=itemList.get(i).getBranchSchedulingInfo3();
//					}
//					if(itemList.get(i).getBranchSchedulingInfo4()!=null
//							&&!itemList.get(i).getBranchSchedulingInfo4().equals("")){
//						branchArray[4]=itemList.get(i).getBranchSchedulingInfo4();
//					}
//					
//					/**
//					 * Date :12-06-2017 BY ANIL
//					 */
//					if(itemList.get(i).getBranchSchedulingInfo5()!=null
//							&&!itemList.get(i).getBranchSchedulingInfo5().equals("")){
//						branchArray[5]=itemList.get(i).getBranchSchedulingInfo5();
//					}
//					if(itemList.get(i).getBranchSchedulingInfo6()!=null
//							&&!itemList.get(i).getBranchSchedulingInfo6().equals("")){
//						branchArray[6]=itemList.get(i).getBranchSchedulingInfo6();
//					}
//					if(itemList.get(i).getBranchSchedulingInfo7()!=null
//							&&!itemList.get(i).getBranchSchedulingInfo7().equals("")){
//						branchArray[7]=itemList.get(i).getBranchSchedulingInfo7();
//					}
//					if(itemList.get(i).getBranchSchedulingInfo8()!=null
//							&&!itemList.get(i).getBranchSchedulingInfo8().equals("")){
//						branchArray[8]=itemList.get(i).getBranchSchedulingInfo8();
//					}
//					if(itemList.get(i).getBranchSchedulingInfo9()!=null
//							&&!itemList.get(i).getBranchSchedulingInfo9().equals("")){
//						branchArray[9]=itemList.get(i).getBranchSchedulingInfo9();
//					}
//					/**
//					 * End
//					 */
//					
//					ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
					
					/**
					 * Date 30-04-2018 By vijay updated code using hashmap for customer branches scheduling services
					 * above old code commented string
					 */
					boolean branchSchFlag=false;
					int qty=0;
					
					
					ArrayList<BranchWiseScheduling> branchSchedulingList=null;
					if(itemList.get(i).getCustomerBranchSchedulingInfo()!=null){
						branchSchFlag =true;
						branchSchedulingList = itemList.get(i).getCustomerBranchSchedulingInfo().get(itemList.get(i).getProductSrNo());
					}else{
						qty=(int) itemList.get(i).getQty();
					}
					
					/**
					 * ends here
					 */
					
					if(branchSchFlag){

					for (int k = 0; k < branchSchedulingList.size(); k++) {
						
						if(branchSchedulingList.get(k).isCheck()==true){

						long noServices = (long) (itemList.get(i).getNumberOfServices());
						int noOfdays = itemList.get(i).getDuration();
						int interval = (int) (noOfdays / itemList.get(i).getNumberOfServices());
						Date servicedate = itemList.get(i).getStartDate();
						Date d = new Date(servicedate.getTime());

						Date productEndDate = new Date(servicedate.getTime());
						CalendarUtil.addDaysToDate(productEndDate, noOfdays);
						Date prodenddate = new Date(productEndDate.getTime());
						productEndDate = prodenddate;

						Date tempdate1 = new Date();
						for (int j = 0; j < noServices; j++) {

							ServiceSchedule scheduleEntity = new ServiceSchedule();

							Date Stardaate = null;
							Stardaate = itemList.get(i).getStartDate();

							scheduleEntity.setSerSrNo(itemList.get(i).getProductSrNo());
							scheduleEntity.setScheduleStartDate(Stardaate);
							scheduleEntity.setScheduleProdId(itemList.get(i).getPrduct().getCount());
							scheduleEntity.setScheduleProdName(itemList.get(i).getProductName());
							scheduleEntity.setScheduleDuration(itemList.get(i).getDuration());
							scheduleEntity.setScheduleNoOfServices(itemList.get(i).getNumberOfServices());
							scheduleEntity.setScheduleServiceNo(j + 1);

							scheduleEntity.setScheduleProdStartDate(Stardaate);
							scheduleEntity.setScheduleProdEndDate(productEndDate);
							
							scheduleEntity.setTotalServicingTime(itemList.get(i).getServicingTIme());

							String calculatedServiceTime = "";
//							if (popuptablelist.size() != 0) {
//								calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
//							} else {
								calculatedServiceTime = "Flexible";
//							}
							scheduleEntity.setScheduleServiceTime(calculatedServiceTime);
								
							scheduleEntity.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
							if (branchSchedulingList.get(k).getServicingBranch()!=null) {
								scheduleEntity.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
							} 
								
							int dayIndex=getDayIndex(branchSchedulingList.get(k).getDay());
							
							if(dayIndex!=0){
								String dayOfWeek1 = format.format(d);
								int adate = Integer.parseInt(dayOfWeek1);
								int adday1 = dayIndex - 1;
								int adday = 0;
								if (adate <= adday1) {
									adday = (adday1 - adate);
								} else {
									adday = (7 - adate + adday1);
								}
								
								Date newDate = new Date(d.getTime());
								CalendarUtil.addDaysToDate(newDate, adday);
								Date day = new Date(newDate.getTime());
	
								if (productEndDate.before(day) == false) {
									scheduleEntity.setScheduleServiceDate(day);
								} else {
									scheduleEntity.setScheduleServiceDate(productEndDate);
								}
	
								CalendarUtil.addDaysToDate(d, interval);
								Date tempdate = new Date(d.getTime());
								d = tempdate;
								scheduleEntity.setScheduleServiceDay(ContractForm.serviceDay(scheduleEntity.getScheduleServiceDate()));
							}else{
								
								if (j > 0) {
									CalendarUtil.addDaysToDate(d, interval);
									tempdate1 = new Date(d.getTime());
								}
								
								if (j == 0) {
									scheduleEntity.setScheduleServiceDate(servicedate);
								}
								if (j != 0) {
									if (productEndDate.before(d) == false) {
										scheduleEntity.setScheduleServiceDate(tempdate1);
									} else {
										scheduleEntity.setScheduleServiceDate(productEndDate);
									}
								}
								/** Date 15-01-2020 by vijay for updating service Day issue  ***/
								scheduleEntity.setScheduleServiceDay(ContractForm.serviceDay(scheduleEntity.getScheduleServiceDate()));

							}
							
							scheduleEntity.setServiceRemark(itemList.get(i).getRemark());
							
							/**
							 * Date 01-04-2017 added by vijay for week number
							 */
							int weekNumber = AppUtility.getweeknumber(scheduleEntity.getScheduleServiceDate());
							scheduleEntity.setWeekNo(weekNumber);
							/**
							 * ends here
							 */
							/***
							  * nidhi *:*:*
							  * 18-09-2018
							  * for map bill product
							  * if any update please update in else conditions also
							  */
							 if(LoginPresenter.billofMaterialActive){
								 
								 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(itemList.get(i).getPrduct());
								 if(billPrDt!=null){
									 
									 
									 UnitConversionUtility unitConver = new UnitConversionUtility();

									 
									 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
										 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
										 seList.add(scheduleEntity);
										 seList = unitConver.getServiceitemProList(seList,itemList.get(i).getPrduct(),itemList.get(i),billPrDt);
										 if(seList!=null && seList.size()>0)
											 scheduleEntity = seList.get(0);
										 
									 }else{
										 unitFlag = true;
//										 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
									 }
									 
									 
									 
									 
								 }
							 }
							 /**
							  * end
							  */
							 
							 /**
							  * @author Vijay Chougule Date :- 09-09-2020
							  * Des :- added Service duration 
							  */
							 if(branchSchedulingList.get(k).getServiceDuration()!=0){
								 scheduleEntity.setServiceDuration(branchSchedulingList.get(k).getServiceDuration());
							 }
							 System.out.println("branchSchedulingList.get(k).getServiceDuration()"+branchSchedulingList.get(k).getServiceDuration());
							 /**
							  * ends here
							  */
							serSchList.add(scheduleEntity);
							Console.log("AppUtitlity.. Scheduling list update..");
							/**
							 * @author Anil
							 * @since 06-06-2020
							 * Updating tat and component details on scheduling object
							 */
							Console.log("AppUtitlity.. Complain flag.."+ContractForm.complainTatFlag);
							if(ContractForm.complainTatFlag){
								Console.log("AppUtitlity.. Complain flag..");
								List<ServiceSchedule>list=ContractForm.updateSchedulingListComponentWise(scheduleEntity, branchSchedulingList.get(k));
								if(list!=null&&list.size()!=0){
									serSchList.addAll(list);
								}
							}
						}
					}
					}
					
					
				   }else{

						for(int k=0;k < qty;k++){
							long noServices=(long) (itemList.get(i).getNumberOfServices());
							int noOfdays=itemList.get(i).getDuration();
							int interval= (int) (noOfdays/itemList.get(i).getNumberOfServices());
							Date servicedate=new Date();
							Date d=new Date(servicedate.getTime());
							Date productEndDate= new Date(servicedate.getTime());
							CalendarUtil.addDaysToDate(productEndDate, noOfdays);
							Date prodenddate=new Date(productEndDate.getTime());
							productEndDate=prodenddate;
							Date tempdate1 = new Date();
							for(int j=0;j<noServices;j++){
								Console.log("Log 8 ");

								ServiceSchedule scheduleEntity = new ServiceSchedule();

								Date Stardaate = null;
								Stardaate = itemList.get(i).getStartDate();

								scheduleEntity.setSerSrNo(itemList.get(i).getProductSrNo());
								scheduleEntity.setScheduleStartDate(Stardaate);
								scheduleEntity.setScheduleProdId(itemList.get(i).getPrduct().getCount());
								scheduleEntity.setScheduleProdName(itemList.get(i).getProductName());
								scheduleEntity.setScheduleDuration(itemList.get(i).getDuration());
								scheduleEntity.setScheduleNoOfServices(itemList.get(i).getNumberOfServices());
								scheduleEntity.setScheduleServiceNo(j + 1);

								scheduleEntity.setScheduleProdStartDate(Stardaate);
								scheduleEntity.setScheduleProdEndDate(productEndDate);
								
								scheduleEntity.setTotalServicingTime(itemList.get(i).getServicingTIme());

								String calculatedServiceTime = "";
//									calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
//								} else {
									calculatedServiceTime = "Flexible";
//								}
								scheduleEntity.setScheduleServiceTime(calculatedServiceTime);
									
								scheduleEntity.setScheduleProBranch("Service Address");
								if(branchName!=null)
								scheduleEntity.setServicingBranch(branchName);
									if (j > 0) {
										CalendarUtil.addDaysToDate(d, interval);
										tempdate1 = new Date(d.getTime());
									}
									
									if (j == 0) {
										scheduleEntity.setScheduleServiceDate(servicedate);
									}
									if (j != 0) {
										if (productEndDate.before(d) == false) {
											scheduleEntity.setScheduleServiceDate(tempdate1);
										} else {
											scheduleEntity.setScheduleServiceDate(productEndDate);
										}
									}
								
								scheduleEntity.setServiceRemark(itemList.get(i).getRemark());
								
								/**
								 * Date 01-04-2017 added by vijay for week number
								 */
								int weekNumber = AppUtility.getweeknumber(scheduleEntity.getScheduleServiceDate());
								scheduleEntity.setWeekNo(weekNumber);
								/**
								 * ends here
								 */
								Console.log("Log 9 ");
								scheduleEntity.setScheduleServiceDay(ContractForm.serviceDay(scheduleEntity.getScheduleServiceDate()));

								
								 /***
								  * nidhi *:*:*
								  * 18-09-2018
								  * for map bill product
								  * if any update please update in else conditions also
								  */
								 if(LoginPresenter.billofMaterialActive){
									 
									 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(itemList.get(i).getPrduct());
									 if(billPrDt!=null){
										 
										 
										 UnitConversionUtility unitConver = new UnitConversionUtility();

										 
										 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
											 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
											 seList.add(scheduleEntity);
											 seList = unitConver.getServiceitemProList(seList,itemList.get(i).getPrduct(),itemList.get(i),billPrDt);
											 if(seList!=null && seList.size()>0)
												 scheduleEntity = seList.get(0);
											 
										 }else{
											 unitFlag = true;
//											 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
										 }
										 
										 
										 
										 
									 }
								 }
								 /**
								  * end
								  */
								serSchList.add(scheduleEntity);
								
								Console.log("Log 10 ");

							}
							
					    }
				   }
				}
				if(unitFlag){
					final GWTCAlert alert = new GWTCAlert(); 
				     alert.alert("Unit of service are and product group detail area not mapped at branch level. Or conversion not available.");
				}
				return serSchList;
			}
			

			public static int getDayIndex(String day) {
				switch(day){
					case "Sunday":
						return 1;
					case "Monday":
						return 2;
					case "Tuesday":
						return 3;
					case "Wednesday":
						return 4;
					case "Thursday":
						return 5;
					case "Friday":
						return 6;
					case "Saturday":
						return 7;
				}
				return 0;
			}
			
		

			/**
			 * Date 02-05-2018 
			 * Developer : Vijay
			 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
			 * so i have updated below code using hashmap.
			 */
			
//			/**
//			 * Date : 03-04-2017 By ANIL
//			 * Adding customer branch in product list for scheduling servies branch wise.
//			 * 
//			 */
//			public static String[] getCustomersBranchsForSchedulingInfo(ArrayList<CustomerBranchDetails> customerBranchList,String branch) {
//				
////				System.out.println();
////				System.out.println("CUSTOMER BRANCH LIST :: "+customerBranchList.size());
////				Console.log("CUSTOMER BRANCH LIST :: "+customerBranchList.size());
//				String[] branchArray=new String[10];
//				int j=0;
//				String branchSchedulingInfo="";
//				/** date 14.04.2018 commented by komal for orion **/
//			//	branchSchedulingInfo=branchSchedulingInfo+"Service Address"+"#"+"Yes"+"#"+branch+"#"+""+"$";
//				/** date 14.04.2018 added by komal for orion **/
//				branchSchedulingInfo=branchSchedulingInfo+"Service Address"+"#"+"Yes"+"#"+branch+"#"+""+"#"+""+"$";//changed by komal
//				branchArray[j]=branchSchedulingInfo;
//				if(customerBranchList.size()!=0){
//					for(int i=0;i<customerBranchList.size();i++){
//						String branchString="";
//						if(i==customerBranchList.size()-1){
//							if(customerBranchList.get(i).getBranch()!=null){
////								branchSchedulingInfo=branchSchedulingInfo+customerBranchList.get(i).getBusinessUnitName()+"#"+"No"+"#"+customerBranchList.get(i).getBranch()+"#"+"";
//								/** date 14.04.2018 commented by komal for orion **/
//								//branchString=customerBranchList.get(i).getBusinessUnitName()+"#"+"No"+"#"+customerBranchList.get(i).getBranch()+"#"+"";
//								/** date 14.04.2018 added by komal for orion **/
//								branchString=customerBranchList.get(i).getBusinessUnitName()+"#"+"No"+"#"+customerBranchList.get(i).getBranch()+"#"+customerBranchList.get(i).getArea()+"#"+"";
//								if(validString(branchSchedulingInfo,branchString)){
//									branchSchedulingInfo=branchSchedulingInfo+branchString;
//									branchArray[j]=branchSchedulingInfo;
//								}else{
//									if(j<9){
//										branchArray[j]=branchSchedulingInfo;
//										j++;
//										branchSchedulingInfo="";
//										branchSchedulingInfo=branchSchedulingInfo+branchString;
//										branchArray[j]=branchSchedulingInfo;
//									}else{
//										branchArray[j]=branchSchedulingInfo;
//									}
//								}
//								
//							}else{
////								branchSchedulingInfo=branchSchedulingInfo+customerBranchList.get(i).getBusinessUnitName()+"#"+"No"+"#"+branch+"#"+"";
//								/** date 14.04.2018 commented by komal for orion **/
//								//branchString=customerBranchList.get(i).getBusinessUnitName()+"#"+"No"+"#"+branch+"#"+"";
//								/** date 14.04.2018 added by komal for orion **/
//								branchString=customerBranchList.get(i).getBusinessUnitName()+"#"+"No"+"#"+branch+"#"+customerBranchList.get(i).getArea()+"#"+"";
//								if(validString(branchSchedulingInfo,branchString)){
//									branchSchedulingInfo=branchSchedulingInfo+branchString;
//									branchArray[j]=branchSchedulingInfo;
//								}else{
//									if(j<9){
//										branchArray[j]=branchSchedulingInfo;
//										j++;
//										branchSchedulingInfo="";
//										branchSchedulingInfo=branchSchedulingInfo+branchString;
//										branchArray[j]=branchSchedulingInfo;
//									}else{
//										branchArray[j]=branchSchedulingInfo;
//									}
//								}
//							}
//						}else{
//							if(customerBranchList.get(i).getBranch()!=null){
////								branchSchedulingInfo=branchSchedulingInfo+customerBranchList.get(i).getBusinessUnitName()+"#"+"No"+"#"+customerBranchList.get(i).getBranch()+"#"+""+"$";
//								/** date 14.04.2018 commented by komal for orion **/
//								//branchString=customerBranchList.get(i).getBusinessUnitName()+"#"+"No"+"#"+customerBranchList.get(i).getBranch()+"#"+""+"$";
//								/** date 14.04.2018 added by komal for orion **/
//								branchString=customerBranchList.get(i).getBusinessUnitName()+"#"+"No"+"#"+customerBranchList.get(i).getBranch()+"#"+customerBranchList.get(i).getArea()+"#"+""+"#"+"$";
//								if(validString(branchSchedulingInfo,branchString)){
//									branchSchedulingInfo=branchSchedulingInfo+branchString;
//									branchArray[j]=branchSchedulingInfo;
//								}else{
//									if(j<9){
//										branchArray[j]=branchSchedulingInfo;
//										j++;
//										branchSchedulingInfo="";
//										branchSchedulingInfo=branchSchedulingInfo+branchString;
//										branchArray[j]=branchSchedulingInfo;
//									}else{
//										branchArray[j]=branchSchedulingInfo;
//									}
//								}
//							}else{
////								branchSchedulingInfo=branchSchedulingInfo+customerBranchList.get(i).getBusinessUnitName()+"#"+"No"+"#"+branch+"#"+""+"$";
//								/** date 14.04.2018 commented by komal for orion **/
//								//branchString=customerBranchList.get(i).getBusinessUnitName()+"#"+"No"+"#"+branch+"#"+""+"$";
//								/** date 14.04.2018 added by komal for orion **/
//								branchString=customerBranchList.get(i).getBusinessUnitName()+"#"+"No"+"#"+branch+"#"+customerBranchList.get(i).getArea()+"#"+""+"$";
//								if(validString(branchSchedulingInfo,branchString)){
//									branchSchedulingInfo=branchSchedulingInfo+branchString;
//									branchArray[j]=branchSchedulingInfo;
//								}else{
//									if(j<9){
//										branchArray[j]=branchSchedulingInfo;
//										j++;
//										branchSchedulingInfo="";
//										branchSchedulingInfo=branchSchedulingInfo+branchString;
//										branchArray[j]=branchSchedulingInfo;
//									}else{
//										branchArray[j]=branchSchedulingInfo;
//									}
//								}
//							}
//						}
//						
//					}
//				}
////				System.out.println();
////				System.out.println("BRANCH ARRAY LENGTH 2-"+branchArray.length);
////				Console.log("BRANCH ARRAY LENGTH 2-"+branchArray.length);
////				System.out.println("BRANCH SCHEDULING "+branchSchedulingInfo);
////				System.out.println();
////				System.out.println();
////				return branchSchedulingInfo;
//				return branchArray;
//			}
//			/**
//			 * End
//			 */
			
			/**
			 * ends here
			 */
			
			
			/**
			 * Date : 03-04-2017 By ANIL
			 * This method checks after adding two string its length is less or equal its return true
			 */
			private static boolean validString(String branchSchedulingInfo,String branchString) {
				String testString=branchSchedulingInfo+branchString;
				if(testString.length()<=500){
					return true;
				}
				return false;
			}

			/**
			 * Date : 03-04-2017 By ANIL
			 * Adding customer branch in product list for scheduling servies branch wise.
			 * 
			 */
			public static String[] getCustomersUpdatedBranchSchedulingList(List<BranchWiseScheduling> branchSchedulingList) {
				
//				System.out.println();
//				System.out.println("UPDATED SCHEDULE LIST SIZE :: "+branchSchedulingList.size());
//				Console.log("UPDATED SCHEDULE LIST SIZE :: "+branchSchedulingList.size());
				String[] branchArray=new String[10];
				int j=0;
				String branchSchedulingInfo="";
				if(branchSchedulingList.size()!=0){
					for(int i=0;i<branchSchedulingList.size();i++){
						String branchString="";
						if(i==branchSchedulingList.size()-1){
							if(branchSchedulingList.get(i).isCheck()==true){
//								branchSchedulingInfo=branchSchedulingInfo+branchSchedulingList.get(i).getBranchName()+"#"+"Yes"+"#"+branchSchedulingList.get(i).getServicingBranch()+"#"+branchSchedulingList.get(i).getDay();
								/** date 14.04.2018 commented by komal for orion (for area)**/
								//branchString=branchSchedulingList.get(i).getBranchName()+"#"+"Yes"+"#"+branchSchedulingList.get(i).getServicingBranch()+"#"+branchSchedulingList.get(i).getDay();
								/** date 14.04.2018 added by komal for orion (for area)**/
								/**
								 *  nidhi *:*:*
								 *  for map unit of masurment
								 */
								branchString=branchSchedulingList.get(i).getBranchName()+"#"+"Yes"+"#"+branchSchedulingList.get(i).getServicingBranch()+"#"+branchSchedulingList.get(i).getArea()+"#"+branchSchedulingList.get(i).getDay()+"#"+branchSchedulingList.get(i).getUnitOfMeasurement();
								if(validString(branchSchedulingInfo,branchString)){
									branchSchedulingInfo=branchSchedulingInfo+branchString;
									branchArray[j]=branchSchedulingInfo;
								}else{
									if(j<9){
										branchArray[j]=branchSchedulingInfo;
										j++;
										branchSchedulingInfo="";
										branchSchedulingInfo=branchSchedulingInfo+branchString;
										branchArray[j]=branchSchedulingInfo;
									}else{
										branchArray[j]=branchSchedulingInfo;
									}
								}
							}else{
//								branchSchedulingInfo=branchSchedulingInfo+branchSchedulingList.get(i).getBranchName()+"#"+"No"+"#"+branchSchedulingList.get(i).getServicingBranch()+"#"+branchSchedulingList.get(i).getDay();
								/** date 14.04.2018 commented by komal for orion (for area)**/
								//branchString=branchSchedulingList.get(i).getBranchName()+"#"+"No"+"#"+branchSchedulingList.get(i).getServicingBranch()+"#"+branchSchedulingList.get(i).getDay();
								/** date 14.04.2018 added by komal for orion (for area)**/
								branchString=branchSchedulingList.get(i).getBranchName()+"#"+"No"+"#"+branchSchedulingList.get(i).getServicingBranch()+"#"+branchSchedulingList.get(i).getArea()+"#"+branchSchedulingList.get(i).getDay()+"#"+branchSchedulingList.get(i).getUnitOfMeasurement();
								if(validString(branchSchedulingInfo,branchString)){
									branchSchedulingInfo=branchSchedulingInfo+branchString;
									branchArray[j]=branchSchedulingInfo;
								}else{
									if(j<9){
										branchArray[j]=branchSchedulingInfo;
										j++;
										branchSchedulingInfo="";
										branchSchedulingInfo=branchSchedulingInfo+branchString;
										branchArray[j]=branchSchedulingInfo;
									}else{
										branchArray[j]=branchSchedulingInfo;
									}
								}
							}
						}else{
							if(branchSchedulingList.get(i).isCheck()==true){
//								branchSchedulingInfo=branchSchedulingInfo+branchSchedulingList.get(i).getBranchName()+"#"+"Yes"+"#"+branchSchedulingList.get(i).getServicingBranch()+"#"+branchSchedulingList.get(i).getDay()+"$";
								/** date 14.04.2018 commented by komal for orion (for area)**/
								//branchString=branchSchedulingList.get(i).getBranchName()+"#"+"Yes"+"#"+branchSchedulingList.get(i).getServicingBranch()+"#"+branchSchedulingList.get(i).getDay()+"$";
								/** date 14.04.2018 added by komal for orion (for area)**/
								branchString=branchSchedulingList.get(i).getBranchName()+"#"+"Yes"+"#"+branchSchedulingList.get(i).getServicingBranch()+"#"+branchSchedulingList.get(i).getArea()+"#"+branchSchedulingList.get(i).getDay()+"#"+branchSchedulingList.get(i).getUnitOfMeasurement()+"$";
								if(validString(branchSchedulingInfo,branchString)){
									branchSchedulingInfo=branchSchedulingInfo+branchString;
									branchArray[j]=branchSchedulingInfo;
								}else{
									if(j<9){
										branchArray[j]=branchSchedulingInfo;
										j++;
										branchSchedulingInfo="";
										branchSchedulingInfo=branchSchedulingInfo+branchString;
										branchArray[j]=branchSchedulingInfo;
									}else{
										branchArray[j]=branchSchedulingInfo;
									}
								}
							
							}else{
//								branchSchedulingInfo=branchSchedulingInfo+branchSchedulingList.get(i).getBranchName()+"#"+"No"+"#"+branchSchedulingList.get(i).getServicingBranch()+"#"+branchSchedulingList.get(i).getDay()+"$";			
								/** date 14.04.2018 commented by komal for orion (for area)**/
								//branchString=branchSchedulingList.get(i).getBranchName()+"#"+"No"+"#"+branchSchedulingList.get(i).getServicingBranch()+"#"+branchSchedulingList.get(i).getDay()+"$";
								/** date 14.04.2018 added by komal for orion (for area)**/
								branchString=branchSchedulingList.get(i).getBranchName()+"#"+"No"+"#"+branchSchedulingList.get(i).getServicingBranch()+"#"+branchSchedulingList.get(i).getArea()+"#"+branchSchedulingList.get(i).getDay()+"#"+branchSchedulingList.get(i).getUnitOfMeasurement()+"$";
								if(validString(branchSchedulingInfo,branchString)){
									branchSchedulingInfo=branchSchedulingInfo+branchString;
									branchArray[j]=branchSchedulingInfo;
								}else{
									if(j<9){
										branchArray[j]=branchSchedulingInfo;
										j++;
										branchSchedulingInfo="";
										branchSchedulingInfo=branchSchedulingInfo+branchString;
										branchArray[j]=branchSchedulingInfo;
									}else{
										branchArray[j]=branchSchedulingInfo;
									}
								}
							
							}
						}
						
					}
				}
//				System.out.println("BRANCH ARRAY LENGHT 1 - "+branchArray.length);
//				Console.log("BRANCH ARRAY LENGHT 1 - "+branchArray.length);
//				return branchSchedulingInfo;
				return branchArray;
			}
			/**
			 * End
			 */
			
			/**
			 * Date : 03-04-2017 By ANIL
			 * Adding customer branch in product list for scheduling servies branch wise.
			 * 
			 */
			public static ArrayList<BranchWiseScheduling> getBranchSchedulingList(String[] branchArray) {
				
				ArrayList<BranchWiseScheduling> branchSchedulingList=new ArrayList<BranchWiseScheduling>();
//				String[] branchArray = null;
//				if(branchSchedulingInfo!=null&&!branchSchedulingInfo.equals("")){
				for(int j=0;j<branchArray.length;j++){
//					String[] branchSchedulingArray = branchSchedulingInfo.split("[$]");
					if(branchArray[j]!=null &&  !branchArray[j].equals("")){
					String[] branchSchedulingArray = branchArray[j].split("[$]");
//					System.out.println("BRANCH SCHEDULE ARRAY SIZE "+branchSchedulingArray.length);
					
					for(int i=0;i<branchSchedulingArray.length;i++){
						

						String[] schedulingInfo=branchSchedulingArray[i].split("#");
						
//						System.out.println(" "+branchSchedulingArray[i]);
//						System.out.println();
//						System.out.println("Scheduling array size "+schedulingInfo.length);
//						System.out.println(i+" " +schedulingInfo[0]);
//						System.out.println(i+" " +schedulingInfo[1]);
//						System.out.println(i+" " +schedulingInfo[2]);
						
						BranchWiseScheduling obj1=new BranchWiseScheduling();
						obj1.setBranchName(schedulingInfo[0]);
						if(schedulingInfo[1].equals("Yes")){
							obj1.setCheck(true);
						}else{
							obj1.setCheck(false);
						}
						
						obj1.setServicingBranch(schedulingInfo[2]);
						
						if(schedulingInfo.length>4){
//							System.out.println(" "+branchSchedulingArray.length);
//							System.out.println(i+" " +schedulingInfo[3]);
							obj1.setDay(schedulingInfo[4]);
						}
						/** date 13.04.2018 added by komal for orion (area)**/
						if(schedulingInfo.length>3){
						if(!(schedulingInfo[3].equals("")))
						 obj1.setArea(Double.parseDouble(schedulingInfo[3]));
						}
						
						/**
						 * nidhi
						 * 20-09-2018
						 * *:*:*
						 */
						if(schedulingInfo.length>4){
							if(!(schedulingInfo[4].equals(""))){
								obj1.setUnitOfMeasurement(schedulingInfo[5]);
							}
						}
						/**
						 * @author Vijay Chougule Date 09-09-2020
						 * Des :- Adding Service duration with process config for LifeLine
						 */
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CONTRACT, AppConstants.PC_ADDSERVICEDURATIONCOLUMN)) {
							try {
								if(schedulingInfo.length>3 && !schedulingInfo[3].equals("")) {
									obj1.setServiceDuration(Double.parseDouble(schedulingInfo[3]));
									obj1.setArea(0);
								}
							} catch (Exception e) {
								// TODO: handle exception
							}
							
						}
						/**
						 * ends here
						 */
						
						branchSchedulingList.add(obj1);
					}
				}
					
				}
				return branchSchedulingList;
			}
			/**
			 * End
			 */
			
			
			
			
			/**
			 * Date 01-04-2017
			 * added by vijay
			 * this method returns service date week number of month
			 */
			
			public static int getweeknumber(Date scheduleServiceDate) {
		
			 Date serviceDatewithtime = new  Date();
			 serviceDatewithtime =  CalendarUtil.copyDate(scheduleServiceDate);
			 String strDate = DateTimeFormat.getFormat("dd/MM/yyyy").format(serviceDatewithtime);
			 Date serviceDate =  DateTimeFormat.getFormat("dd/MM/yyyy").parse(strDate);
			 System.out.println("service Date"+serviceDate);
			 
			 
			  Date date  = new Date();
			  date = CalendarUtil.copyDate(scheduleServiceDate);
			  System.out.println("date ="+date);
			  
			  String dayOfWeek1 = format.format(date);
			  int dayNumber = Integer.parseInt(dayOfWeek1);
			  System.out.println("dayNumber ==="+dayNumber);
			  
			  CalendarUtil.setToFirstDayOfMonth(date);	
			  Date sdate =  getCalculatedDatewithWeek(date, dayNumber);
			  System.out.println("sdate==="+sdate);
			  System.out.println("serviceDate =="+serviceDate);
			  
//			  dayNumber=dayNumber+1;
			  if(sdate.equals(serviceDate)){
				  System.out.println("1 week");
				  return 1;
			  }
			  CalendarUtil.addDaysToDate(date, 7);
			  sdate = getCalculatedDatewithWeek(date, dayNumber);
			  System.out.println("sdate==="+sdate);
			  if(sdate.equals(serviceDate)){
				  System.out.println("2 week");
				  return 2;
			  }
			  
			  CalendarUtil.addDaysToDate(date, 7);
			  sdate = getCalculatedDatewithWeek(date, dayNumber);
			  System.out.println("sdate==="+sdate);
			  if(sdate.equals(serviceDate)){
				  System.out.println("3 week");
				  return 3;
			  }
			  CalendarUtil.addDaysToDate(date, 7);
			  sdate = getCalculatedDatewithWeek(date, dayNumber);
			  System.out.println("sdate==="+sdate);
			  if(sdate.equals(serviceDate)){
				  System.out.println("4 week");
				  return 4;
			  }
			  
			  CalendarUtil.addDaysToDate(date, 7);
			  sdate = getCalculatedDatewithWeek(date, dayNumber);
			  System.out.println("sdate==="+sdate);
			  if(sdate.equals(serviceDate)){
				  System.out.println("5 week");
				  return 5;
			  }
			return 0;
		
			}
			
			/**
			 * date :- 01-04-2017
			 * added by vijay for scheduling services with day and week
			 * this method return date with specified day
			 */
			public static Date getCalculatedDatewithWeek(Date d, int adday1value) {
				
				String dayOfWeek1 = format.format(d);
				int adate = Integer.parseInt(dayOfWeek1);
				int adday1 =adday1value;
				int adday = 1;
				if (adate <= adday1) {
					adday = (adday1 - adate);
				} else {
					adday = (7 - adate + adday1);
				}
				Date newDate = new Date(d.getTime());
				CalendarUtil.addDaysToDate(newDate, adday);
		
				Date day = new Date(newDate.getTime());
				
				return day;
			}
//			
			public static Date getCalculatedDatewithWeekNew(Date d, ArrayList<Integer> selectedSpecificDays) {
				
				String dayOfWeek1 = format.format(d);
				int adate = Integer.parseInt(dayOfWeek1);
//				int adday1 =selectedSpecificDays.get(0);
				int adday = 1;
//				if (adate <= adday1) {
//					adday = (adday1 - adate);
//				} else {
//					adday = (7 - adate + adday1);
//				}
//				Date newDate = new Date(d.getTime());
//				CalendarUtil.addDaysToDate(newDate, adday);
				
				if(selectedSpecificDays!=null&&selectedSpecificDays.size()>0) {
				while(!selectedSpecificDays.contains(adate)) {
					CalendarUtil.addDaysToDate(d, 1);
					adate=d.getDay();
				}
				}
				
				Date newDate = new Date(d.getTime());
				Date day = new Date(newDate.getTime());
				
				return day;
			}
//			
			/**
			 * Date 13 March 2017
			 * this method returns date as per selected day 
			 * if day is not selected then it returns by default monday date
			 */
			
			public static Date getCalculatedDate(Date d, int selectedIndexday) {
				boolean flag=false;
				if(selectedIndexday==1){
					flag=true;
				}
				/**
				 * if condition for if day selected and else for if day not selected then the by default day is monday
				 */
				int selectedIndex;
				if(selectedIndexday!=0){
					selectedIndex = selectedIndexday -1;
				}
				else{
					selectedIndex = 0;
				}
				/**
				 * ends here
				 */
				
				String dayOfWeek1 = format.format(d);
				int adate = Integer.parseInt(dayOfWeek1);
				int adday1;
				if(selectedIndex==0){
					if(flag){
						adday1 =selectedIndex;
					}else{
						adday1 =1;
					}
					
				}else{
					adday1 =selectedIndex;
				}
				
				int adday = 1;
				if (adate <= adday1) {
					adday = (adday1 - adate);
				} else {
					adday = (7 - adate + adday1);
				}
				Date newDate = new Date(d.getTime());
				CalendarUtil.addDaysToDate(newDate, adday);

				Date day = new Date(newDate.getTime());
				
				return day;
				
			}
			/**
			 * ends here
			 */

			
			
			/**
			 * 14 April 2017
			 * added by vijay
			 * when we click on add buton on communication log popup then this method will called
			 */
			public static InteractionType getCommunicationLog(String moduleName, String documentName, int documentId,
					  String personResponible, String remark, Date dueDate, PersonInfo personInfo, EmployeeInfo employeeInfo, String interactiongGroup, String Branch,String status) {

				InteractionType interaction = new InteractionType();
				
				Date date = new Date();
				DateTimeFormat dtf = DateTimeFormat.getFormat("HH:mm:ss");
			    String time=new String(dtf.format(date).toString());

			    interaction.setinteractionCreationDate(date);
			    interaction.setInteractionTime(time);
			    if(LoginPresenter.loggedInUser!=null)
			    interaction.setCreatedBy(LoginPresenter.loggedInUser);
			    interaction.setInteractionPurpose(remark);
			    interaction.setInteractionDueDate(dueDate);
			    interaction.setCommunicationlogFlag(true);
			    
			    interaction.setInteractionDocumentId(documentId);
			    interaction.setInteractionModuleName(moduleName);
			    interaction.setInteractionDocumentName(documentName);
			    if(interactiongGroup!=null)
			    interaction.setInteractionGroup(interactiongGroup);	
			    if(Branch!=null)
			    interaction.setInteractionBranch(Branch);
			    if(personResponible!=null)
			    interaction.setInteractionPersonResponsible(personResponible);
			    if(personInfo!=null){
			    	if(personInfo.isVendor()){
			    		System.out.println("Vendor");
				    	interaction.setInteractionPersonType("Vendor");
				    	interaction.setPersonVendorInfo(personInfo);
			    	}else{
			    		System.out.println("Customer");
				    	interaction.setInteractionPersonType("Customer");
				    	interaction.setPersonInfo(personInfo);
			    	}
			    }
			    /**by Amol, set the lead status to interaction status**/
			    if(status!=null&&!status.equals("")){
			    	interaction.setInteractionStatus(status);
			    }
			    if(employeeInfo!=null){
			    	interaction.setInteractionPersonType("Employee");
			    	interaction.setEmployeeInfo(employeeInfo);
			    }
				return interaction;
			}

			/**
			 * Date 15 April 2017
			 * added by vijay for Communication log validation 
			 * this method also reusable for communication log
			 */
			public static boolean validateCommunicationlog(String remark, Date dueDate) {
				
				GWTCAlert alert = new GWTCAlert();
				
				boolean flag = true;
				
				Date todayDate = new Date();
				todayDate.setHours(00);
				todayDate.setMinutes(0);
				todayDate.setSeconds(00);
				
				if(dueDate!=null){
					dueDate.setHours(11);
					dueDate.setMinutes(59);
					dueDate.setSeconds(50);
				}
				
				Console.log("Today Date ==="+todayDate);
				Console.log("Due Date ="+dueDate);
				
				if(remark==null || remark.equals("")){
					alert.alert("Remark can not be blank");
					flag = false;
				}
				else if(dueDate==null){
					alert.alert("Due date can not be blank");
					flag = false;
				}
				else if(remark.length()>500){
					alert.alert("Remark can not be greater than 500 character");
					flag = false;
				}
				else if(dueDate.before(todayDate)){
					alert.alert("Due date can not be less than today date");
					flag = false;
				}
				
				return flag;
			}

			/**
			 * Date 15 April 2017
			 * added by vijay this is reusable method for view all 
			 * interaction log for every document
			 */
			public static MyQuerry communicationLogQuerry(int documentId, Long companyId,
					String moduleName, String documentName) {

				MyQuerry querry=new MyQuerry();
				Vector<Filter> filtervec=new Vector<Filter>();
				
				Filter filter;
				filter=new Filter();
				filter.setQuerryString("interactionDocumentId");
				filter.setIntValue(documentId);
				filtervec.add(filter);
				
				filter=new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(companyId);
				filtervec.add(filter);
				
				filter=new Filter();
				filter.setQuerryString("interactionModuleName");
				filter.setStringValue(moduleName);
				filtervec.add(filter);
				
				filter=new Filter();
				filter.setQuerryString("interactionDocumentName");
				filter.setStringValue(documentName);
				filtervec.add(filter);
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new InteractionType());
				
				return querry;
			}

			public static boolean checkNewInteractionAdded(	ArrayList<InteractionType> interactionlist) {
				for(int i=0;i<interactionlist.size();i++){
					if(interactionlist.get(i).isCommunicationlogFlag())
						return true;
				}
				return false;
			}

			/**
			 * Ends here
			 */
			
			
			
			/**
			 * rohan added this code for validating customer Ref Number1 in customer 
			 * @param long1 
			 * @param i 
			 * @return 
			 * 
			 */
				public static MyQuerry checkRefNUmberInCustomerByCustomerId(int CustomerId, Long companyId) {

					// *************rohan changes ********************

					MyQuerry querry = new MyQuerry();
					Vector<Filter> filtervec = new Vector<Filter>();
					Filter filter = null;
					
					filter = new Filter();
					filter.setQuerryString("companyId");
					filter.setLongValue(companyId);
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("count");
					filter.setIntValue(CustomerId);
					filtervec.add(filter);

					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());
					
					return querry;
					// **************************changes ends here ********************************

				}
	
			/**
			 * ends here 
			 */

				/**
				 * Date : 11-05-2017 By ANIL
				 * This method update contract item list for branch
				 */
				public static List<SalesLineItem> getItemList(List<SalesLineItem> items,List<ServiceSchedule> serviceScheduleList,ArrayList<CustomerBranchDetails> custBranchLis,String branch){
					Console.log("5-PRODUCT ITEM LIST SIZE "+items.size());
					Console.log("SS LIST SIZE "+serviceScheduleList.size());
					Console.log("CUST BRANCH LIST SIZE "+custBranchLis.size());
					
					for(SalesLineItem obj:items){
						boolean serSrNoFlag=false;
						ArrayList<ServiceSchedule> sSchList=new ArrayList<ServiceSchedule>();
						
						for(ServiceSchedule ssobj:serviceScheduleList){
							Console.log("5A-sSchList LIST SIZE "+obj.getProductSrNo()+" & " +ssobj.getSerSrNo());
							if(obj.getProductSrNo()!=0&&ssobj.getSerSrNo()!=0){
								serSrNoFlag=true;
								if(obj.getPrduct().getCount()==ssobj.getScheduleProdId()&&obj.getProductSrNo()==ssobj.getSerSrNo()){
									sSchList.add(ssobj);
								}
							}
						}
						
						Console.log("6-sSchList LIST SIZE "+sSchList.size() +serSrNoFlag);
						if(!serSrNoFlag){
							Console.log("6A");
							for(ServiceSchedule ssobj:serviceScheduleList){
								Console.log("6B "+obj.getPrduct().getCount()+" & "+ssobj.getScheduleProdId());
								if(obj.getPrduct().getCount()==ssobj.getScheduleProdId()){
									sSchList.add(ssobj);
								}
							}
						}
						Console.log("7-sSchList LIST SIZE "+sSchList.size());
						
						HashSet<String> hset=new HashSet<String>();
						for(ServiceSchedule ssobj:sSchList){
							if(ssobj.getScheduleProBranch()!=null&&!ssobj.getScheduleProBranch().equals("")){
								hset.add(ssobj.getScheduleProBranch());
							}
						}
						Console.log("8-Hset LIST SIZE "+hset.size());
						ArrayList<String> branchList=new ArrayList<String>(hset);
						Console.log("9-branchList LIST SIZE "+branchList.size());
						
						Console.log("10-PROD LVL NO. OF BRANCHES "+obj.getQty()+" SSP BRANCHES "+branchList.size());
						if(branchList.size()!=obj.getQty()){
							Console.log("11-Returning NULL ");
							return null;
						}else{
							Console.log("12-SCH AS PER BRANCH ");
//							ArrayList<CustomerBranchDetails> customerBranchList=new ArrayList<CustomerBranchDetails>();
							ArrayList<BranchWiseScheduling> customerBranchList=new ArrayList<BranchWiseScheduling>();
							
							if(branchList.size()==0){
								BranchWiseScheduling cbObj1=new BranchWiseScheduling();
								cbObj1.setBranchName("Service Address");
								cbObj1.setServicingBranch(branch);
								cbObj1.setCheck(true);
								customerBranchList.add(cbObj1);
							}else{
								BranchWiseScheduling cbObj1=new BranchWiseScheduling();
								cbObj1.setBranchName("Service Address");
								cbObj1.setServicingBranch(branch);
								cbObj1.setCheck(false);
								customerBranchList.add(cbObj1);
							}
							
							
							for(CustomerBranchDetails ccobj:custBranchLis){
								BranchWiseScheduling cbObj=new BranchWiseScheduling();
								cbObj.setBranchName(ccobj.getBusinessUnitName());
								cbObj.setServicingBranch(ccobj.getBranch());
								cbObj.setCheck(false);
								customerBranchList.add(cbObj);
							}
							
							for(BranchWiseScheduling bwlObj:customerBranchList){
								for(String branchName:branchList){
									if(branchName.equals(bwlObj.getBranchName())){
										bwlObj.setCheck(true);
									}
								}
							}
							/**
							 * nidhi |*|
							 */
							/**
							 * @author Vijay Date :- 29-01-2021
							 * Des :- updated below code if old contract slected multiple branches then only it should be selected else it should not be
							 * bug :- it was selecting all branches and shceduling services accordingly 
							 */
							for(BranchWiseScheduling bwlObj:customerBranchList){
								for(BranchWiseScheduling branchName:obj.getCustomerBranchSchedulingInfo().get( obj.getProductSrNo())){
									if(branchName.getBranchName().equals(bwlObj.getBranchName())){
										bwlObj.setCheck(branchName.isCheck());
//										bwlObj.setCheck(true);
										bwlObj.setArea(branchName.getArea());
										bwlObj.setUnitOfMeasurement(branchName.getUnitOfMeasurement());
									}
								}
							}
							
							/**
							 * Date 02-05-2018 
							 * Developer : Vijay
							 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
							 * so i have updated below code using hashmap. 
							 */
//							String branchArray[]=AppUtility.getCustomersUpdatedBranchSchedulingList(customerBranchList);
//							if(branchArray[0]!=null){
//								obj.setBranchSchedulingInfo(branchArray[0]);
//							}
//							if(branchArray[1]!=null){
//								obj.setBranchSchedulingInfo1(branchArray[1]);
//							}
//							if(branchArray[2]!=null){
//								obj.setBranchSchedulingInfo2(branchArray[2]);
//							}
//							if(branchArray[3]!=null){
//								obj.setBranchSchedulingInfo3(branchArray[3]);
//							}
//							if(branchArray[4]!=null){
//								obj.setBranchSchedulingInfo4(branchArray[4]);
//							}
//							
//							/**
//							 * Date : 12-06-2017 By ANIL
//							 */
//							if(branchArray[5]!=null){
//								obj.setBranchSchedulingInfo5(branchArray[5]);
//							}
//							if(branchArray[6]!=null){
//								obj.setBranchSchedulingInfo6(branchArray[6]);
//							}
//							if(branchArray[7]!=null){
//								obj.setBranchSchedulingInfo7(branchArray[7]);
//							}
//							if(branchArray[8]!=null){
//								obj.setBranchSchedulingInfo8(branchArray[8]);
//							}
//							if(branchArray[9]!=null){
//								obj.setBranchSchedulingInfo9(branchArray[9]);
//							}
//							
//							/**
//							 * End
//							 */
							
							/**
							 * Date 30-04-2018 By vijay updated code using hashmap for customer branches scheduling services
							 * above old code commented
							 */
								Integer prodSrNo = obj.getProductSrNo();
								HashMap<Integer, ArrayList<BranchWiseScheduling>> hsmpcustomerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
								hsmpcustomerBranchlist.put(prodSrNo, customerBranchList);
								obj.setCustomerBranchSchedulingInfo(hsmpcustomerBranchlist);
							/**
							 * ends here		
							 */
							/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
								if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableServiceBranch")){
									ArrayList<BranchWiseScheduling> branchArrayList = new ArrayList<BranchWiseScheduling>();
									for(Branch branch1 : LoginPresenter.customerScreenBranchList){
										BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
										branchWiseScheduling.setBranchName(branch1.getBusinessUnitName());
										branchWiseScheduling.setArea(0);
										branchWiseScheduling.setCheck(false);
										branchArrayList.add(branchWiseScheduling);
									}
									if(obj.getServiceBranchesInfo() != null && obj.getServiceBranchesInfo().size() > 0){
										obj.setServiceBranchesInfo(obj.getServiceBranchesInfo());
									}else{
									HashMap<Integer, ArrayList<BranchWiseScheduling>> serviceBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
									serviceBranchlist.put(obj.getProductSrNo(), branchArrayList);
									obj.setServiceBranchesInfo(serviceBranchlist);
									}
								}
								/** end komal **/

						}
						
					}
					
					Console.log("13-Updated Item list size : "+items.size());
					
					return items;
				}
				
				/**
				 * Date : 20-05-2017 By Anil
				 * loading item product
				 */
				public static ProductInfoComposite initiateItemProductComposite(SuperProduct product)
				{
					product=new ItemProduct();
					MyQuerry querry=new MyQuerry();
					querry.setQuerryObject(product);
					ProductInfoComposite productinfo=new ProductInfoComposite(querry, false); 
					return productinfo;
				}
				/**
				 * End
				 */
				
				
				/**
				 * Date 29 jun 2017 added by vijay 	for ECO Friendly 
				 * this is for SMS if customer corespondance name must use if its blank then use full name	 
				 * @param companyId 
				 * @param i 
				 */
						
				public  static MyQuerry getcustomerName(int customerid, Long companyId){
					
					MyQuerry querry = new MyQuerry();
					Vector<Filter> filtervec = new Vector<Filter>();
					Filter filter = null;
					
					filter = new Filter();
					filter.setQuerryString("companyId");
					filter.setLongValue(companyId);
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("count");
					filter.setIntValue(customerid);
					filtervec.add(filter);

					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());
					
					return querry;
				}	
				
				/**
				  * Date 26 jun 2017 added by vijay 
				  * for getting issues in 4 services and 6 services gap  with day selection
				  */		
		public static int checkDateMonthOFEndAndAddDays(String currentServiceDate, Date day) {

			String serviceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];

			if(currentServiceDate.equals("30")){
				if(serviceMonth.equals("1") || serviceMonth.equals("3") || serviceMonth.equals("5") || serviceMonth.equals("7") || serviceMonth.equals("8") || serviceMonth.equals("10") || serviceMonth.equals("12")){
					return 2;
				}
				return 1;
			}
			else if(currentServiceDate.equals("31")){
				return 1;
			}
			else if(currentServiceDate.equals("28")){
				return 1;
			}
			else if(currentServiceDate.equals("29")){
				return 1;
			}
			return 0;
		}
		
		/**
		 * Date : 02-08-2017 By Anil
		 * this method return the the config value by passing its internal type
		 */
		
		public static ArrayList<String> getConfigValue(int internalType){
			ArrayList<String> configValue=new ArrayList<String>();
			
			for(Config obj:LoginPresenter.globalConfig){
				if(obj.getType()==internalType&&obj.isStatus()==true){
					configValue.add(obj.getName());
				}
			}
			return configValue;
		}
		
		/**
		 * End
		 */
		
		
		/*******   Date 06-09-2017 added by vijay  for getting number from String *********/
		public static double getOnlyNumbersFromString(String roundOff) {
			double number =0;
			try {
				number =  Math.abs(Double.parseDouble(roundOff.replaceAll("\\.[^0-9]", "")));
 
			} catch (Exception e) {
				GWTCAlert alert = new GWTCAlert();
				alert.alert("Round off must have number");
			}
			return number;
		}

		/*******   Date 06-09-2017 added by vijay  for Calculating Round Off Operation *********/
		public static double calculateRoundOff(String roundOff, double netPay) {

			double roundoffvalue=0;
			if(roundOff.contains("-")|| roundOff.contains("+")){
				double value =  getOnlyNumbersFromString(roundOff);
				if(value!=0){
					if(roundOff.contains("+")){
						roundoffvalue = netPay+value;
					}
					else if(roundOff.contains("-")){
						roundoffvalue = netPay-value;
					}
				}else{
					roundoffvalue=netPay;
				}
			}
			else{
				double value =  getOnlyNumbersFromString(roundOff);
				if(value!=0){
						roundoffvalue = netPay+value;
				}else{
					roundoffvalue=netPay;
				}
			}
			return roundoffvalue;
		}
		/**
		 * ends here
		 */
		
/** date 23/10/2017 added by komal for architect , PMC , contractor dropdown **/
		
		public static void makeContactPersonListBoxLive(ObjectListBox<ContactPersonIdentification> olbRole , String role, int customerId) {
			String customer = "Customer";
			MyQuerry qu = new MyQuerry();
			Vector filtervec = new Vector();
			Filter filter= null;
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(UserConfiguration.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("role");
			filter.setStringValue(role);
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("entype");
			filter.setStringValue(customer);
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("personInfo.count");
			filter.setIntValue(customerId);;
			filtervec.add(filter);
			System.out.println("customer id :" + customerId);
			
			qu.setFilters(filtervec);
			System.out.println("Query string:" + qu);
			qu.setQuerryObject(new ContactPersonIdentification());
			olbRole.MakeContactPersonLive(qu);
			
		}
				
			
////	     rohan added this code for loading config in view and edit states 
//		
//		public static List<ConfigCategory> getConfigDatainViewAndEditState(ObjectListBox<ConfigCategory> olbContractCategory, String type, Screen contractcategory, ScreeenState currentState)
//		{
//			int internalType = 0;
//			List<ConfigCategory> catlist = new ArrayList<ConfigCategory>();
//			if(type.equals("ConfigCategory"))
//			{
//			
//				internalType = ConfigTypes.getConfigType(contractcategory);
//				System.out.println("internal type");
//				for(int i=0;i<LoginPresenter.globalCategory.size();i++)
//				{
//					if(internalType==LoginPresenter.globalCategory.get(i).getInternalType())
//					{
//						if(currentState==ScreeenState.EDIT)
//						{
//							if(LoginPresenter.globalCategory.get(i).getStatus()==true)
//							{
//								ConfigCategory category=new ConfigCategory();
//								category.setCategoryCode(LoginPresenter.globalCategory.get(i).getCategoryCode());
//								category.setCategoryName(LoginPresenter.globalCategory.get(i).getCategoryName());
//								category.setCompanyId(LoginPresenter.globalCategory.get(i).getCompanyId());
//								category.setCount(LoginPresenter.globalCategory.get(i).getCount());
//								category.setDescription(LoginPresenter.globalCategory.get(i).getDescription());
//								category.setId(LoginPresenter.globalCategory.get(i).getId());
//								category.setInternalType(LoginPresenter.globalCategory.get(i).getInternalType());
//								category.setStatus(LoginPresenter.globalCategory.get(i).getStatus());
//								catlist.add(category);
//							}
//						}
//						else
//						{
//							ConfigCategory category=new ConfigCategory();
//							category.setCategoryCode(LoginPresenter.globalCategory.get(i).getCategoryCode());
//							category.setCategoryName(LoginPresenter.globalCategory.get(i).getCategoryName());
//							category.setCompanyId(LoginPresenter.globalCategory.get(i).getCompanyId());
//							category.setCount(LoginPresenter.globalCategory.get(i).getCount());
//							category.setDescription(LoginPresenter.globalCategory.get(i).getDescription());
//							category.setId(LoginPresenter.globalCategory.get(i).getId());
//							category.setInternalType(LoginPresenter.globalCategory.get(i).getInternalType());
//							category.setStatus(LoginPresenter.globalCategory.get(i).getStatus());
//							catlist.add(category);
//						}
//						
//					}
//				}
//			}
//			return catlist;
//			
//		}
		
		public static boolean checkContractTypeAvailableOrNot(String type,String category,Long companyId,String approvalName,int internalType){
	    	boolean flag =false ;
	    	try {
//				List<Type> serviceType = ofy().load().type(Type.class)
//						.filter("companyId", companyId)
//						.filter("type", internalType).list();
	    		final List<Type> serviceType = new ArrayList<Type>();
	    		MyQuerry querry=new MyQuerry();
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter filtercount=null;
				System.out.println("One Two Three");
				filtercount=new Filter();
				filtercount.setQuerryString("internalType");
				filtercount.setIntValue(internalType);
				filtervec.add(filtercount);
				
				filtercount=new Filter();
				filtercount.setQuerryString("companyId");
				filtercount.setLongValue(companyId);
				filtervec.add(filtercount);

				querry.setFilters(filtervec);
				querry.setQuerryObject(new Type());
				async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						if(result.size()>0){
							for(SuperModel superlist : result){
								serviceType.add((Type)superlist);
							}
						}
					}
					
				});
				
				for(Type conf : serviceType){
					if(conf.getTypeName().trim().equalsIgnoreCase(type) && conf.getCategoryName().equalsIgnoreCase(category)){
						flag = true;
						break;
					}
					
				}
				
				if(!flag && !category.trim().equals("") && category != null && !type.trim().equals("") && type != null){
					
					Type billingtype  = new Type();
					billingtype.setCompanyId(companyId);
					billingtype.setCreatedBy(approvalName);
					billingtype.setCategoryName(category);
					billingtype.setDescription("Saved Throught process configration.");
					billingtype.setUserId(approvalName);
					billingtype.setInternalType(internalType);
					billingtype.setTypeName(type);
					billingtype.setStatus(true);
					async.save(billingtype,new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onFailure(Throwable caught) {
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
						}
					});
//					GenricServiceImpl imp = new GenricServiceImpl();
//					imp.save(billingtype);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(" GEt error in --"+e);
			}
	    	return flag;
	    }
	    
	    public static boolean checkContractCategoryAvailableOrNot(String category,Long companyId,String approvalName,int internalType){
	    	boolean flag =false ;
	    	try {
//				List<ConfigCategory> serviceType = ofy().load().type(ConfigCategory.class)
//						.filter("companyId", companyId)
//						.filter("type", internalType).list();
	    		
				
				final List<ConfigCategory> serviceType = new ArrayList<ConfigCategory>();
	    		MyQuerry querry=new MyQuerry();
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter filtercount=null;
				System.out.println("One Two Three");
				filtercount=new Filter();
				filtercount.setQuerryString("internalType");
				filtercount.setIntValue(internalType);
				filtervec.add(filtercount);
				
				filtercount=new Filter();
				filtercount.setQuerryString("companyId");
				filtercount.setLongValue(companyId);
				filtervec.add(filtercount);

				querry.setFilters(filtervec);
				querry.setQuerryObject(new ConfigCategory());
				async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						if(result.size()>0){
							for(SuperModel superlist : result){
								serviceType.add((ConfigCategory)superlist);
							}
						}
					}
					
				});
				
				
				for(ConfigCategory conf : serviceType){
					if(conf.getCategoryName().trim().equalsIgnoreCase(category)){
						flag = true;
						break;
					}
					
				}
				
				if(!flag && !category.trim().equals("") && category != null){
					
					ConfigCategory billingtype  = new ConfigCategory();
					billingtype.setCompanyId(companyId);
					billingtype.setCreatedBy(approvalName);
					billingtype.setCategoryName(category);
					billingtype.setDescription("Saved Throught process configration.");
					billingtype.setUserId(approvalName);
					billingtype.setInternalType(internalType);
					billingtype.setStatus(true);
					async.save(billingtype,new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onFailure(Throwable caught) {
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
						}
					});
//					GenricServiceImpl imp = new GenricServiceImpl();
//					imp.save(billingtype);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(" GEt error in --"+e);
			}
	    	return flag;
	    }
	    
	    public static boolean checkServiceTypeAvailableOrNot(String category,Long companyId,String approvalName,int internalType){
	    	boolean flag =false ;
	    	try {
//				List<Config> serviceType = ofy().load().type(Config.class)
//						.filter("companyId", companyId)
//						.filter("type", internalType).list();
	    		
	    		

				final List<Config> serviceType = new ArrayList<Config>();
	    		MyQuerry querry=new MyQuerry();
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter filtercount=null;
				System.out.println("One Two Three");
				filtercount=new Filter();
				filtercount.setQuerryString("type");
				filtercount.setIntValue(internalType);
				filtervec.add(filtercount);
				
				filtercount=new Filter();
				filtercount.setQuerryString("companyId");
				filtercount.setLongValue(companyId);
				filtervec.add(filtercount);

				querry.setFilters(filtervec);
				querry.setQuerryObject(new Config());
				async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						if(result.size()>0){
							for(SuperModel superlist : result){
								serviceType.add((Config)superlist);
							}
						}
					}
					
				});
				
				for(Config conf : serviceType){
					if(conf.getName().trim().equalsIgnoreCase(category)){
						flag = true;
						break;
					}
					
				}
				
				if(!flag && !category.trim().equals("") && category != null){
					
					Config type  = new Config();
					type.setCompanyId(companyId);
					type.setCreatedBy(approvalName);
					type.setName(category);
					type.setDescription("Saved Throught process configration.");
					type.setUserId(approvalName);
					type.setType(internalType);
					type.setStatus(true);
					async.save(type,new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onFailure(Throwable caught) {
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
						}
					});
//					GenricServiceImpl imp = new GenricServiceImpl();
//					imp.save(type);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(" GEt error in --"+e);
			}
	    	return flag;
	    }
	    
	    /********************************Global Make Live For Config***********************************************/
		
	    public static int globalMakeLiveConfig(final String querry)
		{
//				async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
				dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
				{
					@Override
					public void onFailure(Throwable caught) {
				    	caught.printStackTrace();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result)
				{
					LoginPresenter.globalConfig.clear();
					for(SuperModel model:result){
						Config configEntity=(Config)model;
						LoginPresenter.globalConfig.add(configEntity);
					}
					System.out.println("Config Size"+LoginPresenter.globalConfig.size());
					Console.log("Config Size"+LoginPresenter.globalConfig.size());
				}
				});

				return 0;
		}
		
		/*****************************Global Make Live For ConfigCategory***************************************/
		
		public static int globalMakeLiveCategory(final String querry)
		{
//				async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
				dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
				{
					@Override
					public void onFailure(Throwable caught) {
				    	caught.printStackTrace();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result)
				{
					LoginPresenter.globalCategory.clear();
					for(SuperModel model:result){
						ConfigCategory categoryEntity=(ConfigCategory)model;
						LoginPresenter.globalCategory.add(categoryEntity);
					}
					System.out.println("Cateogry Size"+LoginPresenter.globalCategory.size());
					Console.log("Cateogry Size"+LoginPresenter.globalCategory.size());
				}
				});
				return 1;
		}
		
		/********************************Global Make Live For Type**********************************************/
		
		public static int globalMakeLiveType(final String querry)
		{
//				async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
				dpservice.getDropDown(querry,new AsyncCallback<ArrayList<SuperModel>>()
				{
					@Override
					public void onFailure(Throwable caught) {
				    	caught.printStackTrace();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result)
				{
					LoginPresenter.globalType.clear();
					for(SuperModel model:result){
						Type typeEntity=(Type)model;
						LoginPresenter.globalType.add(typeEntity);
					}
					System.out.println("Type Size"+LoginPresenter.globalType.size());
					Console.log("Type Size"+LoginPresenter.globalType.size());
				}
				});
				return 1;
		}
		
		
		/**
		 * Date 30 March 2018 BY ANil
		 * updated the same validation code for smartgwt alert
		 */
		public static boolean validateCommunicationlogSmartGwt(String remark, Date dueDate) {
			
//			GWTCAlert alert = new GWTCAlert();
			
			boolean flag = true;
			
			Date todayDate = new Date();
			todayDate.setHours(00);
			todayDate.setMinutes(0);
			todayDate.setSeconds(00);
			
			if(dueDate!=null){
				dueDate.setHours(11);
				dueDate.setMinutes(59);
				dueDate.setSeconds(50);
			}
			
			
			if(remark==null || remark.equals("")){
				SC.say("Remark can not be blank");
				flag = false;
			}
			else if(dueDate==null){
				SC.say("Due date can not be blank");
				flag = false;
			}
			else if(remark.length()>500){
				SC.say("Remark can not be greater than 500 character");
				flag = false;
			}
			else if(dueDate.before(todayDate)){
				SC.say("Due date can not be less than today date");
				flag = false;
			}
			
			return flag;
		}
		
		
	/*	*//**
	     * Developed By : nidhi
	     * 2-04-2018
	     * Purpose : This is global method use to get active process type   
	     * @return 
	     *//*
	    public static Date getForProcessConfigurartionIsActiveOrNot(String processName) {
	    	
	    	List<ProcessConfiguration> processList = LoginPresenter.globalProcessConfig;
	    	int cnt =0;
	    	for(int k=0;k<processList.size();k++){	
				if(processList.get(k).getProcessName().trim().equalsIgnoreCase(processName)){
				
					for (int i = 0; i < processList.get(k).getProcessList().size(); i++) {
						if(processList.get(k).getProcessList().get(i).isStatus()==true)
						{
							DateTimeFormat smpDate = DateTimeFormat.getFormat("dd/MM/yyyy");
							System.out.println("get smp Date --" +smpDate);
							Date datefmt = new Date();
							
							try {
								datefmt = smpDate.parse(processList.get(k).getProcessList().get(i).getProcessType());
								return datefmt;
							} catch (Exception e) {
								return null;
							}
						}
					}
					
				}	
	    	}
					
					return null; 	
				
		}*/
		
		/**
	     * Developed By : nidhi
	     * 2-04-2018
	     * Purpose : This is global method use to get active process type   
	     * @return 
	     */
	    public static String getForProcessConfigurartionIsActiveOrNot(String processName) {
	    	
	    	List<ProcessConfiguration> processList = LoginPresenter.globalProcessConfig;
	    	int cnt =0;
	    	for(int k=0;k<processList.size();k++){	
				if(processList.get(k).getProcessName().trim().equalsIgnoreCase(processName)){
				
					for (int i = 0; i < processList.get(k).getProcessList().size(); i++) {
						if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("AMCBillDateRestriction"))
						{
							DateTimeFormat smpDate = DateTimeFormat.getFormat("dd/MM/yyyy");
							System.out.println("get smp Date --" +smpDate);
							Date datefmt = new Date();
							
							try {
								datefmt = smpDate.parse(processList.get(k).getProcessList().get(i).getProcessType());
								return processList.get(k).getProcessList().get(i).getProcessType().trim();
							} catch (Exception e) {
								return null;
							}
						}else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("GRNDateRestriction"))
						{
							return processList.get(k).getProcessList().get(i).getProcessType().trim();
						}else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("MINDateRestriction"))
						{
							return processList.get(k).getProcessList().get(i).getProcessType().trim();
						}/**nidhi 14-12-2018*/else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("QuotationValidateDuration"))
						{
							return processList.get(k).getProcessList().get(i).getProcessType().trim();
						}
						/*** Vijay for NBHC CCPM contract without lead id and quotation not allow to save contract validation with specified Date****/
						else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("ContractLeadIDQuotationIDRestriction"))
						{
							DateTimeFormat smpDate = DateTimeFormat.getFormat("dd/MM/yyyy");
							System.out.println("get smp Date --" +smpDate);
							Date datefmt = new Date();
							
							try {
								datefmt = smpDate.parse(processList.get(k).getProcessList().get(i).getProcessType());
								return processList.get(k).getProcessList().get(i).getProcessType().trim();
							} catch (Exception e) {
								return null;
							}
						}
						/*** Vijay 28-01-2019 for NBHC Inventory management Confirm Vendor restriction for Purchase Requisition **/
						else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("EnablePRConfirmVendorRestriction")){
							return processList.get(k).getProcessList().get(i).getProcessType().trim();
						}
						/***** Date 08-03-2019 by Vijay for NBHC CCPM Invoice restriction validation *********/
						else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("EnableInvoiceRestrictionForPendingServices")){
							return processList.get(k).getProcessList().get(i).getProcessType().trim();
						}
						/***** Date 21-05-2019 by Vijay for NBHC Complaint due Date with process config ****/
						else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("EnableComplaintDueDate")){
							return processList.get(k).getProcessList().get(i).getProcessType().trim();
						}
						/***** Date 17-07-2019 by Vijay for NBHC Complaint Ticket Date Restriction with process config ****/
						else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("EnableTicketDateRestriction")){
							return processList.get(k).getProcessList().get(i).getProcessType().trim();
						}
						/*** Date 17-08-2020 by Vijay for PfMaxFixedValue to check for PF and Epf calculation ****/
						else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("PfMaxFixedValue")){
							return processList.get(k).getProcessList().get(i).getProcessType().trim();
						}
					}
					
				}	
	    	}
					
					return null; 	
				
		}
		
	    /**
		 * Date : 10-04-2018 BY ANIL
		 * this method checks the applications license status
		 */
		
		public static boolean checkLicenseStatus(String licenseType){
			if(LoginPresenter.company!=null){

				ArrayList<LicenseDetails> licList=new ArrayList<LicenseDetails>();
				if(LoginPresenter.company.getLicenseDetailsList()!=null){
					for(LicenseDetails obj:LoginPresenter.company.getLicenseDetailsList()){
						if(obj.getLicenseType().equals(licenseType)){
							licList.add(obj);
						}
					}
					Comparator<LicenseDetails> licDtComp=new Comparator<LicenseDetails>() {
						@Override
						public int compare(LicenseDetails arg0, LicenseDetails arg1) {
							return arg0.getStartDate().compareTo(arg1.getStartDate());
						}
					};
					Collections.sort(licList, licDtComp);
					
					Date todaysDate=new Date();
					
					if(licList.size()!=0){
						int noOfLicense=0;
						int expiredLicenseCount=0;
						int notActiveLicenseCount=0;
						boolean validLicense=false;
						for(LicenseDetails license:licList){
							if(todaysDate.after(license.getStartDate())){
								if(todaysDate.before(license.getEndDate())){
									noOfLicense=noOfLicense+license.getNoOfLicense();
									validLicense=true;
								}else{
									expiredLicenseCount++;
								}
							}else{
								notActiveLicenseCount++;
							}
						}
						if(validLicense==true){
							return true;
						}else if(validLicense==false&&expiredLicenseCount>0){
							GWTCAlert alert=new GWTCAlert();
							alert.alert(licenseType+ " is expired.Please contact support@evasoftwaresolutions.com");
							return false;
						}else if(validLicense==false&&notActiveLicenseCount>0){
							GWTCAlert alert=new GWTCAlert();
							alert.alert(licenseType+ " is not active.Please contact support@evasoftwaresolutions.com");
							
							return false;
						}
						
					}
				}
			
			}
			
			return true;
		}
		  /** date 05/12/2017 added by komal for company dropdown in hvac **/
				public static void makeCompanyNameListBoxLive(ObjectListBox<?> olb)
				{
					MyQuerry qu= new MyQuerry();
					Vector filtervec = new Vector();
					Filter filter= null ; 
					filter = new Filter();
					filter.setQuerryString("companyId");
					filter.setLongValue(UserConfiguration.getCompanyId());
					filtervec.add(filter);
				    filter = new Filter();
					filter.setQuerryString("description");
					filter.setStringValue("client");
					filtervec.add(filter);
					qu.setFilters(filtervec);
					qu.setQuerryObject(new Customer());
					olb.MakeLive(qu);
				}
				
				
				/**
				 * Date 02-05-2018 
				 * Developer : Vijay
				 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
				 * so i have updated below code using hashmap. So setting data 
				 */
				public static ArrayList<BranchWiseScheduling> getCustomerBranchSchedulinglistInfo(
						ArrayList<CustomerBranchDetails> customerbranchlist, String branch, String unit, double area) {
					
					ArrayList<BranchWiseScheduling> scheduleBranchlist = new ArrayList<BranchWiseScheduling>();
					
					BranchWiseScheduling custBranchSchedule = new BranchWiseScheduling();
					custBranchSchedule.setBranchName("Service Address");
					custBranchSchedule.setCheck(true);
					custBranchSchedule.setServicingBranch(branch);
					/** nidhi *:*:* */
					custBranchSchedule.setArea(area);
					custBranchSchedule.setUnitOfMeasurement(unit);
					/** end */
					/**
					 * @author Anil
					 * @since 23-06-2020
					 * Setting asset qty as 1
					 */
					if(ContractForm.complainTatFlag){
						custBranchSchedule.setAssetQty(1);
					}
					/**
					 * @author Vijay
					 * Des:- added service duration for life line services
					 */
					custBranchSchedule.setServiceDuration(0);
					/**
					 * ends here
					 */
					
					//Ashwini Patil Date:16-04-2024 for orion
					if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "RemoveServiceAddressFromBranchPopup")){
						scheduleBranchlist.add(custBranchSchedule);
					}
//					
					if(customerbranchlist!=null)
						Console.log("customerbranchlist size="+customerbranchlist.size());
					for(int i=0;i<customerbranchlist.size();i++){
						BranchWiseScheduling branchSchedule = new BranchWiseScheduling();
					
						branchSchedule.setBranchName(customerbranchlist.get(i).getBusinessUnitName());
						branchSchedule.setCheck(false);
						if(customerbranchlist.get(i).getBranch()!=null){
							branchSchedule.setServicingBranch(customerbranchlist.get(i).getBranch());
						}else{
							branchSchedule.setServicingBranch(branch);
						}
						branchSchedule.setArea(customerbranchlist.get(i).getArea());
						/**
						 * nidhi
						 * 18-09-2018 *:*:*
						 * for map unit of masurment
						 */
						branchSchedule.setUnitOfMeasurement(customerbranchlist.get(i).getUnitOfMeasurement());
						/**
						 * end
						 */
						
						/**
						 * @author Anil
						 * @since 06-06-2020
						 * Adding Tier and Tat details
						 */
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ComplainServiceWithTurnAroundTime")){
							if(customerbranchlist.get(i).getTierName()!=null&&!customerbranchlist.get(i).getTierName().equals("")){
								branchSchedule.setTierName(customerbranchlist.get(i).getTierName());
							}else{
								branchSchedule.setTierName("NA");
							}
							
							if(customerbranchlist.get(i).getTat()!=null&&!customerbranchlist.get(i).getTat().equals("")){
								branchSchedule.setTat(customerbranchlist.get(i).getTat());
							}
							
						}
						
						scheduleBranchlist.add(branchSchedule);

					}
					
					return scheduleBranchlist;
				}	
				
			/**
			 * ends here	
			 */
				/** date 13.4.2018 added by komal to get current financial year **/
				  public static String GetCurrentFinancialYear()
			        {
					  int month = Integer.parseInt(DateTimeFormat.getFormat( "M" ).format( new Date() )) + 1;

					  int year = Integer.parseInt(DateTimeFormat.getFormat( "yyyy" ).format( new Date() ));
					    String yearString = "";
					    System.out.println("Financial month : " + month);
					    if (month < 4) {
					        System.out.println("Financial Year : " + (year - 1) + "-" + year);
					        yearString = (year - 1) + "-" + year;
					    } else {
					        System.out.println("Financial Year : " + year + "-" + (year + 1));
					        yearString = year + "-" + (year + 1);
					    }
					    return yearString;
			        }	


  /**
	 * Date : 15-05-2018 By ANIl
	 * validate method which restrict user to enter less or more characters in given component
	 */
	
	public static boolean validateFieldRange(int charCurrLen,int minChar,int maxChar,String minCaseMsg,String maxCaseMsg){
		boolean status=true;
		GWTCAlert alert = new GWTCAlert();
		if (charCurrLen < minChar) {
			alert.alert(minCaseMsg);
			status=false;
		}else if(charCurrLen > maxChar){
			alert.alert(maxCaseMsg);
			status=false;
		}
		return status;
	}
				  
	/** date 15.5.2018 added by komal for date and time formatting **/
	public static String parseDateTime(Date date)
	{
		DateTimeFormat format= DateTimeFormat.getFormat("dd-MM-yyyy hh:mm:ss");
		return format.format(date);
	}		  

	public static Tax checkTaxValue(Tax tax , Tax taxDt, ArrayList<TaxDetails> vattaxlist){
		if (tax.getTaxPrintName().equals(AppConstants.IGSTTAX) || tax.getTaxPrintName().equals("SEZ")) {
			for(TaxDetails tax1 : vattaxlist){
				if(tax1.getTaxChargeName().trim().equalsIgnoreCase("NA")){
//					object.setServiceTaxEdit(tax1.getTaxChargeName());
					Tax taxdt = taxDt;
					taxdt.setTaxName(tax1.getTaxChargeName());
					taxdt.setPercentage(tax1.getTaxChargePercent());
					//Date 17-08-2017 added by vijay for GST
					taxdt.setTaxPrintName(tax1.getTaxPrintName());
					
//					taxdt.getTaxChargeName(tax1.getTaxChargeName());
//					if(vattaxlist.get(i).getTaxPrintName().equalsIgnoreCase("IGST"))
					/**
					 * Date : 03-08-2017 By ANIL
					 */
					taxdt.setTaxConfigName(tax1.getTaxChargeName());
					/**
					 * End
					 */
//					object.setServiceTax(taxdt);
					return taxdt;
				}
			}
		}else if(tax.getTaxPrintName().equals(AppConstants.CGST)){
			for(TaxDetails tax1 : vattaxlist){
				if(tax1.getTaxPrintName().trim().equalsIgnoreCase(AppConstants.SGSTTAX) && tax1.getTaxChargePercent() == tax.getPercentage() ){
//					object.setServiceTaxEdit(tax1.getTaxChargeName());
					Tax taxdt = taxDt;
					taxdt.setTaxName(tax1.getTaxChargeName());
					taxdt.setPercentage(tax1.getTaxChargePercent());
					//Date 17-08-2017 added by vijay for GST
					taxdt.setTaxPrintName(tax1.getTaxPrintName());
					
//					taxdt.getTaxChargeName(tax1.getTaxChargeName());
//					if(vattaxlist.get(i).getTaxPrintName().equalsIgnoreCase("IGST"))
					/**
					 * Date : 03-08-2017 By ANIL
					 */
					taxdt.setTaxConfigName(tax1.getTaxChargeName());
					/**
					 * End
					 */
//					object.setServiceTax(taxdt);
					return taxdt;
				}
			}
		}else if(tax.getTaxPrintName().equals(AppConstants.SGSTTAX)){
			for(TaxDetails tax1 : vattaxlist){
				if(tax1.getTaxPrintName().trim().equalsIgnoreCase(AppConstants.CGST) && tax1.getTaxChargePercent() == tax.getPercentage() ){
//					object.setServiceTaxEdit(tax1.getTaxChargeName());
					Tax taxdt = taxDt;
					taxdt.setTaxName(tax1.getTaxChargeName());
					taxdt.setPercentage(tax1.getTaxChargePercent());
					//Date 17-08-2017 added by vijay for GST
					taxdt.setTaxPrintName(tax1.getTaxPrintName());
					
//					taxdt.getTaxChargeName(tax1.getTaxChargeName());
//					if(vattaxlist.get(i).getTaxPrintName().equalsIgnoreCase("IGST"))
					/**
					 * Date : 03-08-2017 By ANIL
					 */
					taxdt.setTaxConfigName(tax1.getTaxChargeName());
					/**
					 * End
					 */
//					object.setServiceTax(taxdt);
					return taxdt;
				}
			}
		}else if(tax.getTaxConfigName().equals("NA")){
			for(TaxDetails tax1 : vattaxlist){
				if(tax1.getTaxChargeName().trim().equalsIgnoreCase("NA")){
//					object.setServiceTaxEdit(tax1.getTaxChargeName());
					Tax taxdt = taxDt;
					taxdt.setTaxName(tax1.getTaxChargeName());
					taxdt.setPercentage(tax1.getTaxChargePercent());
					//Date 17-08-2017 added by vijay for GST
					taxdt.setTaxPrintName(tax1.getTaxPrintName());
					
//					taxdt.getTaxChargeName(tax1.getTaxChargeName());
//					if(vattaxlist.get(i).getTaxPrintName().equalsIgnoreCase("IGST"))
					/**
					 * Date : 03-08-2017 By ANIL
					 */
					taxdt.setTaxConfigName(tax1.getTaxChargeName());
					/**
					 * End
					 */
//					object.setServiceTax(taxdt);
					return taxdt;
				}
			}
		}
		return null;
	}

	/**
	 * nidhi
	 *  *:*:*
	 * @param superProd
	 * @return
	 */
	public static BillOfMaterial verifyBillofMaterilProd(SuperProduct superProd){
		BillOfMaterial bill = null;
		
		for(BillOfMaterial billpro : ContractPresenter.globalProdBillOfMaterial){
			if(billpro.getCode().equals(superProd.getProductCode()) && billpro.getProduct_id() == superProd.getCount()){
				return billpro;
			}
		}
		
		return bill;
	}
/** date 20.10.2018 added by komal to compare states between 2 addresses**/
	public static boolean isAddressStateDifferent(String state1 , String state2){
		if(state1.equalsIgnoreCase(state2)){
			return false;
		}else{
			return true;
		}
		
	}
	public static SalesLineItem setByDefaultTaxAsIGST(SalesLineItem item ,boolean flag){
		if(flag){
			double per1 =0 , per2 = 0 ,total = 0;
		if(item.getServiceTax() !=null && item.getServiceTax().getPercentage()!=0) { 
			  per1 = item.getServiceTax().getPercentage();
			  Tax tax = new Tax();
			  tax.setTaxName("NA");
			  tax.setPercentage(0);
			  tax.setTaxPrintName(" ");
			  tax.setTaxConfigName("NA");
			  item.setServiceTax(tax);
		}
		if(item.getVatTax() != null && item.getVatTax().getPercentage() != 0){
			  per2 = item.getVatTax().getPercentage();
			  total = per1 + per2;
			  Tax tax1 = new Tax();
			  tax1.setTaxName("IGST@"+total);
			  tax1.setPercentage(total);
			  tax1.setTaxPrintName("IGST");
			  tax1.setTaxConfigName("IGST@"+total);
			  item.setVatTax(tax1);
		}
		}else{
			if(item.getPreviousTax1() != null && item.getPreviousTax1().getPercentage() != 0){
				  Tax tax = new Tax();
				  tax.setTaxName(item.getPreviousTax1().getTaxName());
				  tax.setPercentage(item.getPreviousTax1().getPercentage());
				  tax.setTaxPrintName(item.getPreviousTax1().getTaxPrintName());
				  tax.setTaxConfigName(item.getPreviousTax1().getTaxConfigName());
				  item.setServiceTax(tax);
			}
			if(item.getPreviousTax2() != null && item.getPreviousTax2().getPercentage() != 0){
				  Tax tax1 = new Tax();
				  tax1.setTaxName(item.getPreviousTax2().getTaxName());
				  tax1.setPercentage(item.getPreviousTax2().getPercentage());
				  tax1.setTaxPrintName(item.getPreviousTax2().getTaxPrintName());
				  tax1.setTaxConfigName(item.getPreviousTax2().getTaxConfigName());
				  item.setVatTax(tax1);
			}
		}
		return item;
	}
	
	/**
	 * @author Vijay Chougule
	 * Date :- 11-Dec-2018
	 * Des :- For NBHC Inventory Management for Automatic PR Order Qty calculations
	 */
	public static double getOrderQty(double stockInHand,double forecastDemand,
			double futherConsmptntillStockReceived,double reorderLevel,double minOrderQty,
			double safetyStock){
			double orderQty = 0;
			int finalOrderQty = 0;
			if(stockInHand<=forecastDemand){
				orderQty = (forecastDemand-(stockInHand-futherConsmptntillStockReceived))/minOrderQty;
				orderQty = getRoundUpNumber(orderQty)*minOrderQty;

			}
			else if(stockInHand<=reorderLevel){
				orderQty = (reorderLevel-(stockInHand-futherConsmptntillStockReceived))/minOrderQty;
				orderQty = getRoundUpNumber(orderQty)*minOrderQty;
			}
			else if(stockInHand<=safetyStock){
				orderQty = (safetyStock-(stockInHand-futherConsmptntillStockReceived))/minOrderQty;
				orderQty = getRoundUpNumber(orderQty)*minOrderQty;
			}
			finalOrderQty = (int) orderQty;
		return finalOrderQty;
	}
	/**
	 * ends here
	 */
	
	/**
	 * @author Vijay Chougule
	 * Date :- 11-Dec-2018
	 * Des :- For NBHC Inventory Management for Automatic PR Order Qty calculations
	 * Des :- below Method is reusable for get roundup if number contains decimal points which is
	 * greater than 1 then number will increased by 1
	 */
	public static int getRoundUpNumber(double number){
		NumberFormat numberFormat=NumberFormat.getFormat("0.00");
		String strNumber = String.valueOf(numberFormat.format(number));
		String num = strNumber.substring(strNumber.indexOf(".")).substring(1);
		int num1 = Integer.parseInt(num);
		if(num1>0){
			number = number+1;
		}
		return (int)number;
		
	}
	
	/**
	 * Date 02-04-2019 by Vijay to check date is end of the month date
	 * @param date
	 * @return
	 */
	public static boolean checkDateEndOfTheMonth(Date date){
		String serviceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format(date).split("-")[0];
		String month =  DateTimeFormat.getFormat( "d-M-yyyy" ).format(date).split("-")[1];
		if(serviceDate.equals("28")||serviceDate.equals("29")||serviceDate.equals("30")||serviceDate.equals("31")){
			CalendarUtil.addDaysToDate(date, 1);
			String newmonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format(date).split("-")[1];
			if(!month.equals(newmonth)){
				return true;	
			}
		}
		return false;
	}
	
	/***
	 * Date 22-05-2019 by Vijay 
	 * Des :- for customer support service location branch with hardcode service address
	 * @param serviceaddress 
	 */
	public static void makeCustomerBranchLive(PersonInfo personInfo,ObjectListBox<CustomerBranchDetails> olbcustomerBranch, String serviceaddress) {

		MyQuerry querry= new MyQuerry();
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter filter = null;
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filterVec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(personInfo.getCount());
		filterVec.add(filter);
		
		querry.setQuerryObject(new CustomerBranchDetails());
		querry.setFilters(filterVec);

		//Ashwini Patil Date:26-03-2022 Description: In case of rate contract CustomerBranch was not getting set with the service. For such services setting customerBranch as "Service Address"
		if(serviceaddress==null || serviceaddress.equals("") ){
			Console.log("ashwini AppUtility in if(serviceaddress==null || serviceaddress.equals() )");
			serviceaddress = "Service Address";
		}

		olbcustomerBranch.MakeLiveCustomerBranchrScreen(querry,serviceaddress,personInfo);
	}

	/**
	 * @author Vijay Chougule Date 21-09-2019
	 * Des :- NBHC CCPM complaint service assessment closed or not cheking
	 */
	public MyQuerry complaintIdQueryy(Integer ticketNumber) {
		MyQuerry querry= new MyQuerry();
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter filter = null;
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setStringValue(AssesmentReport.CLOSED);
		filterVec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("complaintId");
		filter.setIntValue(ticketNumber);
		filterVec.add(filter);
		
		querry.setQuerryObject(new AssesmentReport());
		querry.setFilters(filterVec);
		return querry;
	}

	public static void makeServiceProductListBoxLive(
			ObjectListBox<ServiceProduct> olbProductName) {
		// TODO Auto-generated method stub
		
	}

	

	public static void initializeServiceProduct(ObjectListBox<ServiceProduct> olbProductName) {
		MyQuerry querry= new MyQuerry();
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter filter = null;
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filterVec.add(filter);
		
		querry.setQuerryObject(new ServiceProduct());
		querry.setFilters(filterVec);
		
		
		olbProductName.MakeLive(querry);
	}
	
	/**
	 * Date 20-04-2020
	 * @author :- Vijay Chougule
	 * Description :- to check code executing at server side or local
	 * TO run local project properly
	 */
	public static boolean checkIsItServerSide() {
		String url=Window.Location.getHref();
		if(url.contains("http://localhost:8080/")||url.contains("https://localhost:8080/")) {
			return false;
		}else {
			return true;
		}
		
	}

	public static List<String> getbranchlist() {
		List<String> branchlist = new ArrayList<String>();
		for(Branch branchEntity : LoginPresenter.globalBranch){
			branchlist.add(branchEntity.getBusinessUnitName());
		}
		return branchlist;
	}
	
	public static void makeCustomerBranchLive(ObjectListBox<?> olb, int customerId)
	{
		MyQuerry qu= new MyQuerry();
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter filter = null;
		
		filter=new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(customerId);
		filterVec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filterVec.add(filter);
		
		qu.setFilters(filterVec);
		qu.setQuerryObject(new CustomerBranchDetails());
		olb.MakeLive(qu);
	}
	
	public static SalesLineItem ReactOnAddProductCompositeForSales(SuperProduct prodInfo, Customer customer, Branch branchEntity, boolean gstApplicable)
	{
		SalesLineItem lineItem=new SalesLineItem();
		ArrayList<SalesLineItem> arr=new ArrayList<SalesLineItem>();
		if(prodInfo!=null)
		{
			

				if(prodInfo.getVatTax()!=null){
				   lineItem.setRefproductmasterTax1(prodInfo.getVatTax());
				}
				if(prodInfo.getServiceTax()!=null){
				   lineItem.setRefproductmasterTax2(prodInfo.getServiceTax());
				}
				
			if(customer!=null && gstApplicable && (!prodInfo.getVatTax().getTaxConfigName().equals("") ||
								!prodInfo.getServiceTax().getTaxConfigName().equals("")) ){
				prodInfo = getTaxDeatilsAsPerState(customer,branchEntity,prodInfo);
				
			}
			else{
					 Tax tax1 = new Tax();
					 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
					 if(tax1!=null)
					 prodInfo.setVatTax(tax1);
					 
					 Tax tax2 = new Tax();
					 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
					 if(tax2!=null)
					 prodInfo.setServiceTax(tax2);
				
			}
			
		   lineItem.setPrduct(prodInfo);
		   

		   lineItem.setTermsAndConditions(new DocumentUpload());
		   lineItem.setProductImage(new DocumentUpload());
		   lineItem.getPrduct().setProductImage1(new DocumentUpload());
		   lineItem.getPrduct().setProductImage2(new DocumentUpload());
		   lineItem.getPrduct().setProductImage3(new DocumentUpload());
		   arr.add(lineItem);
		}
		return lineItem;
	}
	
	
	public static ProductDetailsPO ReactOnAddPurchaseProductComposite(SuperProduct prodInfo, Vendor vendor, Branch branchEntity)
	{
	
		SalesLineItem lineItem=new SalesLineItem();
		ProductDetailsPO poproducts=new ProductDetailsPO();
		
		if(vendor!=null && (!prodInfo.getVatTax().getTaxConfigName().equals("") ||
				!prodInfo.getServiceTax().getTaxConfigName().equals("")) ){
		prodInfo = getTaxDeatilsAsPerState(vendor,branchEntity,prodInfo);
		
		}
		else{
		if(prodInfo.getVatTax().getTaxConfigName().equals("") &&
						prodInfo.getServiceTax().getTaxConfigName().equals("")){
			 Tax tax1 = new Tax();
			 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
			 if(tax1!=null)
			 prodInfo.setVatTax(tax1);
			 
			 Tax tax2 = new Tax();
			 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
			 if(tax2!=null)
			 prodInfo.setServiceTax(tax2);
		}
		
		}
		
		if(prodInfo instanceof ServiceProduct){
			if(prodInfo!=null)
			{
				lineItem.setPrduct(prodInfo);
				lineItem.setTermsAndConditions(new DocumentUpload());
			    lineItem.setProductImage(new DocumentUpload());
			    lineItem.getPrduct().setProductImage1(new DocumentUpload());
			    lineItem.getPrduct().setProductImage2(new DocumentUpload());
			    lineItem.getPrduct().setProductImage3(new DocumentUpload());
				
				poproducts.setProductID(lineItem.getPrduct().getCount());
				poproducts.setProductCategory(lineItem.getProductCategory());
				poproducts.setProductCode(lineItem.getProductCode());
				poproducts.setProductName(lineItem.getProductName());
				poproducts.setUnitOfmeasurement(lineItem.getUnitOfMeasurement());
				poproducts.setProdPrice(lineItem.getPrice());
				poproducts.setTax(lineItem.getServiceTax().getPercentage());
				poproducts.setVat(lineItem.getVatTax().getPercentage());
				poproducts.setProductQuantity(lineItem.getQty());
				poproducts.setDiscount(lineItem.getPercentageDiscount());
			}
		}
		if(prodInfo instanceof ItemProduct){
			System.out.println("In side item product");
			
			prodInfo.setTermsAndConditions(new DocumentUpload());
			prodInfo.setProductImage(new DocumentUpload());
			prodInfo.setProductImage1(new DocumentUpload());
			prodInfo.setProductImage2(new DocumentUpload());
			prodInfo.setProductImage3(new DocumentUpload());
			lineItem.setPrduct(prodInfo);
			
			poproducts.setProductID(lineItem.getPrduct().getCount());
			poproducts.setProductCategory(lineItem.getProductCategory());
			poproducts.setProductCode(lineItem.getProductCode());
			poproducts.setProductName(lineItem.getProductName());
			poproducts.setUnitOfmeasurement(lineItem.getUnitOfMeasurement());
			
			/**
			 * Date : 10-07-2018 BY ANIL
			 */
//			poproducts.setProdPrice(lineItem.getPrice());
//			poproducts.setTax(lineItem.getServiceTax().getPercentage());
//			poproducts.setVat(lineItem.getVatTax().getPercentage());
			
			
			poproducts.setSalesPrice(prodInfo.getPrice());
			poproducts.setSalesTax1(prodInfo.getVatTax());
			poproducts.setSalesTax2(prodInfo.getServiceTax());
			
			poproducts.setProdPrice(lineItem.getPrduct().getPurchasePrice());
			poproducts.setTax(lineItem.getPrduct().getPurchaseTax2().getPercentage());
			poproducts.setVat(lineItem.getPrduct().getPurchaseTax1().getPercentage());
			
			
			
			poproducts.setProductQuantity(lineItem.getQty());
			poproducts.setDiscount(lineItem.getPercentageDiscount());
			poproducts.setPrduct(prodInfo);
			
			
			/**
			 * @author Anil
			 * @since 05-01-2021
			 * tax were getting mapped on purchase register
			 */
			try{
				poproducts.setVatTax(lineItem.getPrduct().getPurchaseTax1());
				poproducts.setServiceTax(lineItem.getPrduct().getPurchaseTax2());
			}catch(Exception e){
				
			}
			/**
			 * End
			 */
			
			
		}
		return poproducts;
	}
	
	
	private static SuperProduct getTaxDeatilsAsPerState(Customer customer,	Branch branchEntity, SuperProduct prodInfo) {
		try {

			System.out.println("LoginPresenter.globalTaxList"+LoginPresenter.globalTaxList.size());
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany")){
			if(customer.getAdress().getState()!=null && !customer.getAdress().getState().equals("") 
					&& branchEntity!=null && branchEntity.getAddress().getState()!=null && !branchEntity.getAddress().getState().equals("") ){
				if(!customer.getAdress().getState().trim().equals(branchEntity.getAddress().getState().trim())){
					
					prodInfo = getDifferentStateTaxDetails(prodInfo);
					
				}
				else{
					
					prodInfo = getSameStateTaxDetails(prodInfo);

				}
			}
		}
		else{
			System.out.println("else block");
			if(customer.getAdress().getState()!=null && !customer.getAdress().getState().equals("") 
					&& LoginPresenter.company!=null && LoginPresenter.company.getAddress().getState()!=null && !LoginPresenter.company.getAddress().getState().equals("") ){
				
				if(!customer.getAdress().getState().trim().equals(LoginPresenter.company.getAddress().getState().trim())){

					prodInfo = getDifferentStateTaxDetails(prodInfo);

				}
				else{
					prodInfo = getSameStateTaxDetails(prodInfo);

				}
			}
		
		}
		
	} catch (Exception e) {
		// TODO: handle exception
	}
		
		return prodInfo;
	}

	//*************************8888

	private static Tax getTaxDetails(String taxNamePrintName,ArrayList<TaxDetails> globalTaxList, double percentage) {
		
		try {
		
			Console.log("taxNamePrintName"+taxNamePrintName);
			Console.log("percentage"+percentage);
			if(taxNamePrintName.trim().equalsIgnoreCase("NA") && percentage==0) {
				Console.log("inside NA Tax");
				for(TaxDetails taxDetails : globalTaxList){
					if(taxDetails.getTaxChargeStatus() && taxDetails.getTaxChargeName().trim().equalsIgnoreCase(taxNamePrintName.trim()) && percentage==taxDetails.getTaxChargePercent()){
						Tax tax = new Tax();
						tax.setPercentage(taxDetails.getTaxChargePercent());
						tax.setTaxConfigName(taxDetails.getTaxChargeName());
						tax.setTaxName(taxDetails.getTaxChargeName());
						tax.setTaxPrintName(taxDetails.getTaxPrintName());
						return tax;
					}
				}
			}
			else {
				Console.log("Normal Taxes");
				for(TaxDetails taxDetails : globalTaxList){
					if(taxDetails.getTaxChargeStatus() && taxDetails.getTaxPrintName().trim().equalsIgnoreCase(taxNamePrintName.trim()) && percentage==taxDetails.getTaxChargePercent()){
						Tax tax = new Tax();
						tax.setPercentage(taxDetails.getTaxChargePercent());
						tax.setTaxConfigName(taxDetails.getTaxChargeName());
						tax.setTaxName(taxDetails.getTaxChargeName());
						tax.setTaxPrintName(taxDetails.getTaxPrintName());
						return tax;
					}
				}
			}
		
		
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
	}


	private static SuperProduct getTaxDeatilsAsPerState(Vendor vendor,Branch branchEntity, SuperProduct prodInfo) {
		
		
	try {
			System.out.println("LoginPresenter.globalTaxList"+LoginPresenter.globalTaxList.size());
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany")){
			if(vendor.getPrimaryAddress().getState()!=null && !vendor.getPrimaryAddress().getState().equals("") 
					&& branchEntity.getAddress().getState()!=null && !branchEntity.getAddress().getState().equals("") ){
				if(!vendor.getPrimaryAddress().getState().trim().equals(branchEntity.getAddress().getState().trim())){
					 
					prodInfo = getDifferentStateTaxDetails(prodInfo);

					
				}
				else{
					
					prodInfo = getSameStateTaxDetails(prodInfo);
					
				}
			}
		}
		else{
			System.out.println("else block");
			if(vendor.getPrimaryAddress().getState()!=null && !vendor.getPrimaryAddress().getState().equals("") 
					&& LoginPresenter.company!=null && LoginPresenter.company.getAddress().getState()!=null && !LoginPresenter.company.getAddress().getState().equals("") ){
				
				if(!vendor.getPrimaryAddress().getState().trim().equals(LoginPresenter.company.getAddress().getState().trim())){

					prodInfo = getDifferentStateTaxDetails(prodInfo);

				
				}
				else{
					System.out.println("else block =="+prodInfo.getVatTax().getPercentage());
					prodInfo = getSameStateTaxDetails(prodInfo);

				}

			}
		
		}
		
	} catch (Exception e) {
		// TODO: handle exception
	}
		
		return prodInfo;
	
	}
	
		private static SuperProduct getSameStateTaxDetails(SuperProduct prodInfo) {
			GWTCAlert gwtalert = new GWTCAlert();
			
			 if(prodInfo.getVatTax()!=null && prodInfo.getVatTax().getPercentage()!=0){
				 Tax tax1 = new Tax();
				 double tax1percentage = 0;
				 if(prodInfo.getVatTax()!=null && prodInfo.getVatTax().getPercentage()!=0){
					 if(prodInfo.getVatTax().getTaxPrintName().trim().equals("IGST")){
							 tax1percentage = prodInfo.getVatTax().getPercentage()/2;
					 }
					 else{
						 tax1percentage = prodInfo.getVatTax().getPercentage();
					 }
					 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax1percentage);
				 }
				 else{
					 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
				 }
				 if(tax1!=null){
					 if(prodInfo.getVatTax().getTaxPrintName().trim().equals("IGST") && 
							 tax1.getTaxPrintName().trim().equals("CGST")){
						 Tax tax2 = new Tax();
						 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax1.getPercentage());
						 tax2.setInclusive(prodInfo.getServiceTax().isInclusive());
						 prodInfo.setServiceTax(tax2);

					 }
					 tax1.setInclusive(prodInfo.getVatTax().isInclusive());
					 prodInfo.setVatTax(tax1);
				 }
				 else{
					 System.out.println("here alert");
					 gwtalert.alert("CGST "+tax1percentage +" does not exist in Tax Master!");
					 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
					 tax1.setInclusive(prodInfo.getVatTax().isInclusive());
					 prodInfo.setVatTax(tax1);
					 
					 Tax tax2 = new Tax();
					 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
					 tax2.setInclusive(prodInfo.getServiceTax().isInclusive());
					 prodInfo.setServiceTax(tax2);
				 }
				 
			 }
//			 else{
			  if(prodInfo.getServiceTax()!=null && prodInfo.getServiceTax().getPercentage()!=0){
				  
				 System.out.println("else tax 2 == ");
				 double tax2percentage = 0;
				 Tax tax2 = new Tax();
				 if(prodInfo.getServiceTax()!=null && prodInfo.getServiceTax().getPercentage()!=0){
					 if(prodInfo.getServiceTax().getTaxPrintName().trim().equals("IGST")){
						 if(prodInfo.getServiceTax()!=null ){
							 tax2percentage = prodInfo.getServiceTax().getPercentage()/2;

						 }
					 }
					 else{
						 tax2percentage = prodInfo.getServiceTax().getPercentage();
					 }
					 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax2percentage);
				 }
				 else{
					 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
				 }
				 if(tax2!=null){
					 if(prodInfo.getServiceTax().getTaxPrintName().trim().equals("IGST") && 
							 tax2.getTaxPrintName().trim().equals("SGST")){
						 Tax tax1 = new Tax();
						 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax2.getPercentage());
						 tax1.setInclusive(prodInfo.getVatTax().isInclusive());
						 prodInfo.setVatTax(tax1);

					 }
					 tax2.setInclusive(prodInfo.getVatTax().isInclusive());
					 prodInfo.setServiceTax(tax2);
				 }
				 else{
					 System.out.println("here alert ==");

					 gwtalert.alert("SGST "+tax2percentage +"does not exist in Tax Master!");
					 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
					 tax2.setInclusive(prodInfo.getServiceTax().isInclusive());
					 prodInfo.setServiceTax(tax2);
					 
					 Tax tax = new Tax();
					 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
					 tax.setInclusive(prodInfo.getServiceTax().isInclusive());
					 prodInfo.setServiceTax(tax);
				 }
			 }
			 
			return prodInfo;
		}

		private static SuperProduct getDifferentStateTaxDetails(SuperProduct prodInfo) {
			GWTCAlert gwtalert = new GWTCAlert();
			
			if(prodInfo.getVatTax()!=null && prodInfo.getVatTax().getPercentage()!=0){
				Tax tax1 = new Tax();
				double tax1percentage = 0;
				if(prodInfo.getVatTax()!=null && prodInfo.getVatTax().getPercentage()!=0){
					 if(prodInfo.getVatTax().getTaxPrintName().trim().equals("CGST") || prodInfo.getVatTax().getTaxPrintName().trim().equals("SGST")){
						 if(prodInfo.getServiceTax()!=null ){
							 tax1percentage = prodInfo.getVatTax().getPercentage() + prodInfo.getServiceTax().getPercentage();
						 }
						 else{
							 tax1percentage = prodInfo.getVatTax().getPercentage();
						 }
					 }
					 else{
						 tax1percentage = prodInfo.getVatTax().getPercentage();
					 }
					 System.out.println("tax1percentage "+tax1percentage);
					 tax1 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax1percentage);
				 }
				 else{
					 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
				 }
				 
				 if(tax1!=null){
					 tax1.setInclusive(prodInfo.getVatTax().isInclusive());
					 prodInfo.setVatTax(tax1);
				 }
				 else{
					 gwtalert.alert("IGST "+tax1percentage +" does not exist in Tax Master!");
					 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
					 tax1.setInclusive(prodInfo.getVatTax().isInclusive());
					 prodInfo.setVatTax(tax1);
					 System.out.println("here igst found");
				 }
				 Tax tax2 = new Tax();
				 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
				 tax2.setInclusive(prodInfo.getServiceTax().isInclusive());
				 prodInfo.setServiceTax(tax2);
			}
			else{
				
			 System.out.println("tax 2 started here");
			 double tax2percentage = 0;
			 Tax tax2 = new Tax();
			 if(prodInfo.getServiceTax()!=null && prodInfo.getServiceTax().getPercentage()!=0){
				 if(prodInfo.getVatTax().getTaxPrintName().trim().equals("CGST") || prodInfo.getServiceTax().getTaxPrintName().trim().equals("SGST")){
					 if(prodInfo.getVatTax()!=null ){
						 tax2percentage = prodInfo.getServiceTax().getPercentage() + prodInfo.getVatTax().getPercentage();
					 }
					 else{
						 tax2percentage = prodInfo.getVatTax().getPercentage();
					 }
				 }
				 else{
					 tax2percentage = prodInfo.getVatTax().getPercentage();
				 }
				 tax2 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax2percentage);
			 }
			 else{
				 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
			 }
			 if(tax2!=null){
				 tax2.setInclusive(prodInfo.getServiceTax().isInclusive());
				 prodInfo.setServiceTax(tax2);
			 }
			 else{
				 gwtalert.alert("IGST "+tax2percentage +" does not exist in Tax Master!");
				 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
				 tax2.setInclusive(prodInfo.getServiceTax().isInclusive());
				 prodInfo.setServiceTax(tax2);
			 }
			 Tax tax = new Tax();
			 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
			 tax2.setInclusive(prodInfo.getVatTax().isInclusive());
			 prodInfo.setVatTax(tax);
	  	}	
			 
			return prodInfo;
		}

		public static List<SalesLineItem> updateTaxesDetails(List<SalesLineItem> list,	Branch branchEntity, String customerBillingaddresState, boolean gstApplicable) {

		try {
			GWTCAlert gwtalert = new GWTCAlert();

			for(SalesLineItem saleslineitem : list){
				if(gstApplicable && (!saleslineitem.getVatTax().getTaxConfigName().equals("") ||
						!saleslineitem.getServiceTax().getTaxConfigName().equals("")) ){
		
					System.out.println("LoginPresenter.globalTaxList"+LoginPresenter.globalTaxList.size());
					System.out.println(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany"));
					/**
					 * @author Vijay Date :- 28-07-2021
					 * Des :- added if condition auto GST should not work if tax is VAT so added if conditions
					 */
					Console.log("First Condition =="+(saleslineitem.getVatTax().getTaxPrintName()!=null && !saleslineitem.getVatTax().getTaxPrintName().equalsIgnoreCase("") && !saleslineitem.getVatTax().getTaxPrintName().equalsIgnoreCase("NA")));
					Console.log("2nd OR Condition == "+(saleslineitem.getServiceTax().getTaxPrintName()!=null && !saleslineitem.getServiceTax().getTaxPrintName().equalsIgnoreCase("") && !saleslineitem.getServiceTax().getTaxPrintName().equalsIgnoreCase("NA")));
					if( ( (saleslineitem.getVatTax().getTaxPrintName()!=null && !saleslineitem.getVatTax().getTaxPrintName().equalsIgnoreCase("") && !saleslineitem.getVatTax().getTaxPrintName().equalsIgnoreCase("NA")) ||
							(saleslineitem.getServiceTax().getTaxPrintName()!=null && !saleslineitem.getServiceTax().getTaxPrintName().equalsIgnoreCase("") && !saleslineitem.getServiceTax().getTaxPrintName().equalsIgnoreCase("NA"))) ){
						
						Console.log("VAT Condition =="+(!saleslineitem.getVatTax().getTaxPrintName().trim().equalsIgnoreCase("VAT") && !saleslineitem.getServiceTax().getTaxPrintName().trim().equalsIgnoreCase("NA")));
						Console.log("second VAT Condition "+(!saleslineitem.getServiceTax().getTaxPrintName().trim().equalsIgnoreCase("VAT") && !saleslineitem.getVatTax().getTaxPrintName().trim().equalsIgnoreCase("NA")));
						
						if(saleslineitem.getVatTax().getTaxPrintName().trim().equalsIgnoreCase("VAT") || saleslineitem.getServiceTax().getTaxPrintName().trim().equalsIgnoreCase("VAT")) {
							
						}
						else {
						
						Console.log("Inside Auto GST logic ");
							
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany")){
						if(customerBillingaddresState!=null && !customerBillingaddresState.trim().equals("") 
								&& branchEntity.getAddress().getState()!=null && !branchEntity.getAddress().getState().equals("") ){
							if(!customerBillingaddresState.trim().equals(branchEntity.getAddress().getState().trim())){
								System.out.println("branch as company different state");
								
									if(saleslineitem.getRefproductmasterTax1()!=null && saleslineitem.getRefproductmasterTax1().getPercentage()!=0){
										saleslineitem.setVatTax(saleslineitem.getRefproductmasterTax1());
									}
									if(saleslineitem.getRefproductmasterTax2()!=null && saleslineitem.getRefproductmasterTax2().getPercentage()!=0){
										saleslineitem.setServiceTax(saleslineitem.getRefproductmasterTax2());
									}
									
									if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0 ){
										Tax tax1 = new Tax();
										double tax1percentage = 0;
										if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0){
											 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("CGST") || saleslineitem.getVatTax().getTaxPrintName().trim().equals("SGST")){
												 if(saleslineitem.getServiceTax()!=null ){
													 tax1percentage = saleslineitem.getVatTax().getPercentage() + saleslineitem.getServiceTax().getPercentage();
												 }
												 else{
													 tax1percentage = saleslineitem.getVatTax().getPercentage();
												 }
											 }
											 else{
												 tax1percentage = saleslineitem.getVatTax().getPercentage();
											 }
											 tax1 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax1percentage);
										 }
										 else{
											 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 }
										 
										 if(tax1!=null){
											 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
											 saleslineitem.setVatTax(tax1);
										 }
										 else{
											 gwtalert.alert("IGST "+tax1percentage +" does not exist in Tax Master!");
											 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
											 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
											 saleslineitem.setVatTax(tax1);
										 }
										 Tax tax2 = new Tax();
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
										 saleslineitem.setServiceTax(tax2);
									}
									if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){	
									 System.out.println("tax 2 started here");
									 double tax2percentage = 0;
									 Tax tax2 = new Tax();
									 if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){
										 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("CGST") || saleslineitem.getServiceTax().getTaxPrintName().trim().equals("SGST")){
											 if(saleslineitem.getVatTax()!=null ){
												 tax2percentage = saleslineitem.getServiceTax().getPercentage() + saleslineitem.getVatTax().getPercentage();
											 }
											 else{
												 tax2percentage = saleslineitem.getVatTax().getPercentage();
											 }
										 }
										 else{
											 tax2percentage = saleslineitem.getVatTax().getPercentage();
										 }
										 tax2 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax2percentage);
									 }
									 else{
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 }
									 if(tax2!=null){
										 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
										 saleslineitem.setServiceTax(tax2);
									 }
									 else{
										 gwtalert.alert("IGST "+tax2percentage +" does not exist in Tax Master!");
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
										 saleslineitem.setServiceTax(tax2);
									 }
									 Tax tax = new Tax();
									 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 tax.setInclusive(saleslineitem.getVatTax().isInclusive());
									 saleslineitem.setVatTax(tax);
							  	}	 
									 
							}
							else{
								System.out.println("branch as company same state");
								
								if(saleslineitem.getRefproductmasterTax1()!=null && saleslineitem.getRefproductmasterTax1().getPercentage()!=0){
									saleslineitem.setVatTax(saleslineitem.getRefproductmasterTax1());
								}
								if(saleslineitem.getRefproductmasterTax2()!=null && saleslineitem.getRefproductmasterTax2().getPercentage()!=0){
									saleslineitem.setServiceTax(saleslineitem.getRefproductmasterTax2());
								}
								
								 if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0){
									 Tax tax1 = new Tax();
									 double tax1percentage = 0;
									 if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0){
										 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("IGST")){
												 tax1percentage = saleslineitem.getVatTax().getPercentage()/2;
										 }
										 else{
											 tax1percentage = saleslineitem.getVatTax().getPercentage();
										 }
										 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax1percentage);
									 }
									 else{
										 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 }
									 if(tax1!=null){
										 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("IGST") && 
												 tax1.getTaxPrintName().trim().equals("CGST")){
											 Tax tax2 = new Tax();
											 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax1.getPercentage());
											 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
											 saleslineitem.setServiceTax(tax2);

										 }
										 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
										 saleslineitem.setVatTax(tax1);
									 }
									 else{
										 gwtalert.alert("CGST "+tax1percentage +" does not exist in Tax Master!");
										 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
										 saleslineitem.setVatTax(tax1);
										 
										 Tax tax2 = new Tax();
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
										 saleslineitem.setServiceTax(tax2);
									 }
									 
								 }
//								 else{
								 if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){
									 System.out.println("else tax 2 == ");
									 double tax2percentage = 0;
									 Tax tax2 = new Tax();
									 if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){
										 if(saleslineitem.getServiceTax().getTaxPrintName().trim().equals("IGST")){
											 if(saleslineitem.getServiceTax()!=null ){
												 tax2percentage = saleslineitem.getServiceTax().getPercentage()/2;
											 }
										 }
										 else{
											 tax2percentage = saleslineitem.getServiceTax().getPercentage();
										 }
										 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax2percentage);
									 }
									 else{
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 }
									 if(tax2!=null){
										 if(saleslineitem.getServiceTax().getTaxPrintName().trim().equals("IGST") && 
												 tax2.getTaxPrintName().trim().equals("SGST")){
											 Tax tax1 = new Tax();
											 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax2.getPercentage());
											 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
											 saleslineitem.setVatTax(tax1);

										 }
										 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
										 saleslineitem.setServiceTax(tax2);
									 }
									 else{

										 gwtalert.alert("SGST "+tax2percentage +"does not exist in Tax Master!");
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
										 saleslineitem.setServiceTax(tax2);
										 
										 Tax tax = new Tax();
										 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 tax.setInclusive(saleslineitem.getServiceTax().isInclusive());
										 saleslineitem.setServiceTax(tax);
									 }
								 }
								 
								
							}
						}
					}
					else{
						System.out.println("else block");
						if(customerBillingaddresState!=null && !customerBillingaddresState.equals("") 
								&& LoginPresenter.company!=null && LoginPresenter.company.getAddress().getState()!=null && !LoginPresenter.company.getAddress().getState().equals("") ){
							
							if(!customerBillingaddresState.trim().equals(LoginPresenter.company.getAddress().getState().trim())){
								System.out.println("else block 1");

								if(saleslineitem.getRefproductmasterTax1()!=null && saleslineitem.getRefproductmasterTax1().getPercentage()!=0){
									saleslineitem.setVatTax(saleslineitem.getRefproductmasterTax1());
								}
								if(saleslineitem.getRefproductmasterTax2()!=null && saleslineitem.getRefproductmasterTax2().getPercentage()!=0){
									saleslineitem.setServiceTax(saleslineitem.getRefproductmasterTax2());
								}
								
								if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0){
									Tax tax1 = new Tax();
									double tax1percentage = 0;
									if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0){
										 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("CGST") || saleslineitem.getVatTax().getTaxPrintName().trim().equals("SGST")){
											 if(saleslineitem.getServiceTax()!=null ){
												 tax1percentage = saleslineitem.getVatTax().getPercentage() + saleslineitem.getServiceTax().getPercentage();
											 }
											 else{
												 tax1percentage = saleslineitem.getVatTax().getPercentage();
											 }
										 }
										 else{
											 tax1percentage = saleslineitem.getVatTax().getPercentage();
										 }
										 System.out.println("tax1percentage "+tax1percentage);
										 tax1 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax1percentage);
									 }
									 else{
										 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 }
									 
									 if(tax1!=null){
										 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
										 saleslineitem.setVatTax(tax1);
									 }
									 else{
										 gwtalert.alert("IGST "+tax1percentage +" does not exist in Tax Master!");
										 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
										 saleslineitem.setVatTax(tax1);
									 }
									 Tax tax2 = new Tax();
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
									 saleslineitem.setServiceTax(tax2);
								}
								if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){
	
								 System.out.println("tax 2 started here");
								 double tax2percentage = 0;
								 Tax tax2 = new Tax();
								 if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){
									 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("CGST") || saleslineitem.getServiceTax().getTaxPrintName().trim().equals("SGST")){
										 if(saleslineitem.getVatTax()!=null ){
											 tax2percentage = saleslineitem.getServiceTax().getPercentage() + saleslineitem.getVatTax().getPercentage();
										 }
										 else{
											 tax2percentage = saleslineitem.getVatTax().getPercentage();
										 }
									 }
									 else{
										 tax2percentage = saleslineitem.getVatTax().getPercentage();
									 }
									 tax2 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax2percentage);
								 }
								 else{
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 }
								 if(tax2!=null){
									 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
									 saleslineitem.setServiceTax(tax2);
								 }
								 else{
									 System.out.println("tax 2 ");
									 gwtalert.alert("IGST "+tax2percentage +" does not exist in Tax Master!");
									 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
									 saleslineitem.setServiceTax(tax2);
								 }
								 Tax tax = new Tax();
								 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
								 tax.setInclusive(saleslineitem.getVatTax().isInclusive());
								 saleslineitem.setVatTax(tax);
						  	}	
									 
							}
							else{
								 System.out.println("else block 2 else");
								 if(saleslineitem.getRefproductmasterTax1()!=null && saleslineitem.getRefproductmasterTax1().getPercentage()!=0){
										saleslineitem.setVatTax(saleslineitem.getRefproductmasterTax1());
									}
									if(saleslineitem.getRefproductmasterTax2()!=null && saleslineitem.getRefproductmasterTax2().getPercentage()!=0){
										saleslineitem.setServiceTax(saleslineitem.getRefproductmasterTax2());
									}
								 if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0){
									 Tax tax1 = new Tax();
									 double tax1percentage = 0;
									 if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0){
										 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("IGST")){
												 tax1percentage = saleslineitem.getVatTax().getPercentage()/2;
										 }
										 else{
											 tax1percentage = saleslineitem.getVatTax().getPercentage();
										 }
										 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax1percentage);
									 }
									 else{
										 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 }
									 if(tax1!=null){
										 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("IGST") && 
												 tax1.getTaxPrintName().trim().equals("CGST")){
											 Tax tax2 = new Tax();
											 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax2.getPercentage());
											 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
											 saleslineitem.setServiceTax(tax2);

										 }
										 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
										 saleslineitem.setVatTax(tax1);
									 }
									 else{
										 gwtalert.alert("CGST "+tax1percentage +" does not exist in Tax Master!");
										 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
										 saleslineitem.setVatTax(tax1);
										 
										 Tax tax2 = new Tax();
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
										 saleslineitem.setServiceTax(tax2);
									 }
									 
								 }
								 if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){

									 double tax2percentage = 0;
									 Tax tax2 = new Tax();
									 if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){
										 if(saleslineitem.getServiceTax().getTaxPrintName().trim().equals("IGST")){
											 if(saleslineitem.getServiceTax()!=null ){
												 tax2percentage = saleslineitem.getServiceTax().getPercentage()/2;
											 }
										 }
										 else{
											 tax2percentage = saleslineitem.getServiceTax().getPercentage();
										 }
										 System.out.println("tax2percentage **&& "+tax2percentage);
										 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax2percentage);
									 }
									 else{
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
									 }
									 if(tax2!=null){
										 if(saleslineitem.getServiceTax().getTaxPrintName().trim().equals("IGST") && 
												 tax2.getTaxPrintName().trim().equals("SGST")){
											 Tax tax1 = new Tax();
											 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax2.getPercentage());
											 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
											 saleslineitem.setVatTax(tax1);

										 }
										 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
										 saleslineitem.setServiceTax(tax2);
									 }
									 else{
										 gwtalert.alert("SGST "+tax2percentage +"does not exist in Tax Master!");
										 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
										 saleslineitem.setServiceTax(tax2);
										 
										 Tax tax = new Tax();
										 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 tax.setInclusive(saleslineitem.getServiceTax().isInclusive());
										 saleslineitem.setServiceTax(tax);
									 }
								 }
								 
							}
			
							
						}
					
					}
					
				}
				}
					
			}
			else{
					System.out.println("NOn GST Applicable");
					 Tax tax1 = new Tax();
					 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
					 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
					 if(tax1!=null)
						 saleslineitem.setVatTax(tax1);
					 
					 Tax tax2 = new Tax();
					 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
					 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
					 if(tax2!=null)
						 saleslineitem.setServiceTax(tax2);
					 
			}
					
			}
					
			
	} catch (Exception e) {
		// TODO: handle exception
	}
		
		return list;
		
	
	}

		public static List<SalesOrderProductLineItem> updateBillingTaxesDetails(List<SalesOrderProductLineItem> prodlist, String customerBillingaddresState, Branch branchEntity,
				boolean gstApplicable) {


			try {
				GWTCAlert gwtalert = new GWTCAlert();

				for(SalesOrderProductLineItem saleslineitem : prodlist){
					if(gstApplicable && (!saleslineitem.getVatTax().getTaxConfigName().equals("") ||
							!saleslineitem.getServiceTax().getTaxConfigName().equals("")) ){
			
						if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0 ){
							saleslineitem.setRefproductmasterTax1(saleslineitem.getVatTax());
						}
						if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){	
							saleslineitem.setRefproductmasterTax2(saleslineitem.getServiceTax());
						}
						
						System.out.println("LoginPresenter.globalTaxList"+LoginPresenter.globalTaxList.size());
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany")){
							
							if(customerBillingaddresState!=null && !customerBillingaddresState.trim().equals("") 
									&& branchEntity.getAddress().getState()!=null && !branchEntity.getAddress().getState().equals("") ){
								if(!customerBillingaddresState.trim().equals(branchEntity.getAddress().getState().trim())){
									
										if(saleslineitem.getRefproductmasterTax1()!=null && saleslineitem.getRefproductmasterTax1().getPercentage()!=0){
											saleslineitem.setVatTax(saleslineitem.getRefproductmasterTax1());
										}
										if(saleslineitem.getRefproductmasterTax2()!=null && saleslineitem.getRefproductmasterTax2().getPercentage()!=0){
											saleslineitem.setServiceTax(saleslineitem.getRefproductmasterTax2());
										}
										
										if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0 ){
											Tax tax1 = new Tax();
											double tax1percentage = 0;
											if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0){
												 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("CGST") || saleslineitem.getVatTax().getTaxPrintName().trim().equals("SGST")){
													 if(saleslineitem.getServiceTax()!=null ){
														 tax1percentage = saleslineitem.getVatTax().getPercentage() + saleslineitem.getServiceTax().getPercentage();
													 }
													 else{
														 tax1percentage = saleslineitem.getVatTax().getPercentage();
													 }
												 }
												 else{
													 tax1percentage = saleslineitem.getVatTax().getPercentage();
												 }
												 tax1 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax1percentage);
											 }
											 else{
												 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
											 }
											 
											 if(tax1!=null){
												 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
												 saleslineitem.setVatTax(tax1);
											 }
											 else{
												 gwtalert.alert("IGST "+tax1percentage +" does not exist in Tax Master!");
												 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
												 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
												 saleslineitem.setVatTax(tax1);
											 }
											 Tax tax2 = new Tax();
											 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
											 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
											 saleslineitem.setServiceTax(tax2);
										}
										if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){	
										 double tax2percentage = 0;
										 Tax tax2 = new Tax();
										 if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){
											 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("CGST") || saleslineitem.getServiceTax().getTaxPrintName().trim().equals("SGST")){
												 if(saleslineitem.getVatTax()!=null ){
													 tax2percentage = saleslineitem.getServiceTax().getPercentage() + saleslineitem.getVatTax().getPercentage();
												 }
												 else{
													 tax2percentage = saleslineitem.getVatTax().getPercentage();
												 }
											 }
											 else{
												 tax2percentage = saleslineitem.getVatTax().getPercentage();
											 }
											 tax2 =	 getTaxDetails("IGST",LoginPresenter.globalTaxList,tax2percentage);
										 }
										 else{
											 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 }
										 if(tax2!=null){
											 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
											 saleslineitem.setServiceTax(tax2);
										 }
										 else{
											 gwtalert.alert("IGST "+tax2percentage +" does not exist in Tax Master!");
											 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
											 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
											 saleslineitem.setServiceTax(tax2);
										 }
										 Tax tax = new Tax();
										 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 tax.setInclusive(saleslineitem.getVatTax().isInclusive());
										 saleslineitem.setVatTax(tax);
								  	}	 
										 
								}
								else{
									
									if(saleslineitem.getRefproductmasterTax1()!=null && saleslineitem.getRefproductmasterTax1().getPercentage()!=0){
										saleslineitem.setVatTax(saleslineitem.getRefproductmasterTax1());
									}
									if(saleslineitem.getRefproductmasterTax2()!=null && saleslineitem.getRefproductmasterTax2().getPercentage()!=0){
										saleslineitem.setServiceTax(saleslineitem.getRefproductmasterTax2());
									}
									
									 if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0){
										 Tax tax1 = new Tax();
										 double tax1percentage = 0;
										 if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0){
											 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("IGST")){
													 tax1percentage = saleslineitem.getVatTax().getPercentage()/2;
											 }
											 else{
												 tax1percentage = saleslineitem.getVatTax().getPercentage();
											 }
											 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax1percentage);
										 }
										 else{
											 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 }
										 if(tax1!=null){
											 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("IGST") && 
													 tax1.getTaxPrintName().trim().equals("CGST")){
												 Tax tax2 = new Tax();
												 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax1.getPercentage());
												 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
												 saleslineitem.setServiceTax(tax2);

											 }
											 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
											 saleslineitem.setVatTax(tax1);
										 }
										 else{
											 gwtalert.alert("CGST "+tax1percentage +" does not exist in Tax Master!");
											 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
											 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
											 saleslineitem.setVatTax(tax1);
											 
											 Tax tax2 = new Tax();
											 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
											 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
											 saleslineitem.setServiceTax(tax2);
										 }
										 
									 }
									 if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){
										 System.out.println("else tax 2 == ");
										 double tax2percentage = 0;
										 Tax tax2 = new Tax();
										 if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){
											 if(saleslineitem.getServiceTax().getTaxPrintName().trim().equals("IGST")){
												 if(saleslineitem.getServiceTax()!=null ){
													 tax2percentage = saleslineitem.getServiceTax().getPercentage()/2;
													 System.out.println("saleslineitem.getServiceTax().getPercentage()"+saleslineitem.getServiceTax().getPercentage());

												 }
											 }
											 else{
												 tax2percentage = saleslineitem.getServiceTax().getPercentage();
											 }
											 tax2 =	 getTaxDetails("SGST",LoginPresenter.globalTaxList,tax2percentage);
										 }
										 else{
											 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
										 }
										 if(tax2!=null){
											 if(saleslineitem.getServiceTax().getTaxPrintName().trim().equals("IGST") && 
													 tax2.getTaxPrintName().trim().equals("SGST")){
												 Tax tax1 = new Tax();
												 tax1 =	 getTaxDetails("CGST",LoginPresenter.globalTaxList,tax2.getPercentage());
												 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
												 saleslineitem.setVatTax(tax1);

											 }
											 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
											 saleslineitem.setServiceTax(tax2);
										 }
										 else{

											 gwtalert.alert("SGST "+tax2percentage +"does not exist in Tax Master!");
											 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
											 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
											 saleslineitem.setServiceTax(tax2);
											 
											 Tax tax = new Tax();
											 tax =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
											 tax.setInclusive(saleslineitem.getServiceTax().isInclusive());
											 saleslineitem.setServiceTax(tax);
										 }
									 }
									 
									
								}
							}
						}
						
				}
				else{
						 Tax tax1 = new Tax();
						 tax1 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
						 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());

						 if(tax1!=null)
							 saleslineitem.setVatTax(tax1);
						 
						 Tax tax2 = new Tax();
						 tax2 =	 getTaxDetails("NA",LoginPresenter.globalTaxList,0);
						 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());

						 if(tax2!=null)
							 saleslineitem.setServiceTax(tax2);
						 
				}
						
				}
						
				
		} catch (Exception e) {
			// TODO: handle exception
		}
			
			return prodlist;
		
		}

		public static void MakeliveEmailTemplate(ObjectListBox<EmailTemplate> olbTemplateName) {

			MyQuerry qu= new MyQuerry();
			Filter filter=new Filter();
			filter.setQuerryString("templateStatus");
			filter.setBooleanvalue(true);
			qu.getFilters().add(filter);
			qu.setQuerryObject(new EmailTemplate());
			olbTemplateName.MakeLive(qu);
		}
		
		public static void MakeliveContactPerson(ObjectListBox<ContactPersonIdentification> olbTemplateName, String entitytype, int personId, String personName, String personemailId ) {

			MyQuerry qu= new MyQuerry();
			Filter filter=new Filter();
			filter.setQuerryString("templateStatus");
			filter.setBooleanvalue(true);
			qu.getFilters().add(filter);
			qu.setQuerryObject(new ContactPersonIdentification());
			olbTemplateName.MakeLive(qu);
			
			MyQuerry querry=new MyQuerry();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			if(entitytype.trim().equalsIgnoreCase("Customer") || entitytype.trim().equalsIgnoreCase("Vendor")){
				temp=new Filter();
				temp.setQuerryString("personInfo.count");
				temp.setIntValue(personId);
				filtervec.add(temp);
				
			}
			else if(entitytype.trim().equalsIgnoreCase("Employee")){
				temp=new Filter();
				temp.setQuerryString("employeeInfo.empCount");
				temp.setIntValue(personId);
				filtervec.add(temp);
			}
			
			temp=new Filter();
			temp.setQuerryString("entype");
			temp.setStringValue(entitytype);
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ContactPersonIdentification());
			olbTemplateName.makeliveContactPerson(querry,personName,personemailId);
		}


		public static String getFromEmailAddress(String branchName, String employeeName) {
			String fromEmailAddress ="";
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE)){
				if(employeeName!=null && !employeeName.equals("")){
					for(User userEntity : LoginPresenter.globalUserEntitylist){
						if(userEntity.getEmployeeName().trim().equals(employeeName.trim())){
							if(userEntity.getEmail()!=null && !userEntity.getEmail().equals("")){
								fromEmailAddress = userEntity.getEmail();
							}
						}
					}
				}
				if(fromEmailAddress.equals("") && AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany")){
					for(Branch branchEntity : LoginPresenter.globalBranch){
						if(branchEntity.getBusinessUnitName().trim().equals(branchName.trim())){
							if(branchEntity.getEmail()!=null){
								fromEmailAddress = branchEntity.getEmail();
							}
						}
					}
				}
			}
			else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany")){
				for(Branch branchEntity : LoginPresenter.globalBranch){
					if(branchEntity.getBusinessUnitName().trim().equals(branchName.trim())){
						if(branchEntity.getEmail()!=null){
							fromEmailAddress = branchEntity.getEmail();
						}
					}
				}
			}
			else{
				if(LoginPresenter.company!=null && LoginPresenter.company.getEmail()!=null)
				fromEmailAddress = LoginPresenter.company.getEmail();
			}
			if(fromEmailAddress!=null && fromEmailAddress.equals("")){
				if(LoginPresenter.company!=null && LoginPresenter.company.getEmail()!=null)
					fromEmailAddress = LoginPresenter.company.getEmail();
			}
			return fromEmailAddress;
		}

		public static void loadContactPersonlist(ObjectListBox<ContactPersonIdentification> olbContactlistEmailId, String entityType, int personId, String personName, String personemail, List<ContactPersonIdentification> vendorDetailslist) {
			List<ContactPersonIdentification> list = new ArrayList<ContactPersonIdentification>();
			List<ContactPersonIdentification> templist = new ArrayList<ContactPersonIdentification>();

			boolean multiplepersonIdsFlag=false;
			if(entityType.trim().equalsIgnoreCase("Vendor") && vendorDetailslist!=null && vendorDetailslist.size()>0){
				list.addAll(vendorDetailslist);
				multiplepersonIdsFlag = true;
				templist.addAll(vendorDetailslist);
			}
			else if(entityType.trim().equalsIgnoreCase("Employee") && vendorDetailslist!=null && vendorDetailslist.size()>0){
				list.addAll(vendorDetailslist); // employee list
				multiplepersonIdsFlag = true;
				templist.addAll(vendorDetailslist);
			}
			else if(entityType.trim().equalsIgnoreCase("Customer") && vendorDetailslist!=null && vendorDetailslist.size()>0){
				list.addAll(vendorDetailslist); // this is for service screen customer branch email ids
			}
			boolean flag = false;

			Console.log("LoginPresenter.globalContactpersonlist"+LoginPresenter.globalContactpersonlist.size());

			if(multiplepersonIdsFlag){
				
				if(LoginPresenter.globalContactpersonlist!=null && LoginPresenter.globalContactpersonlist.size()!=0){
					
					for(ContactPersonIdentification person : templist){
						
						for(ContactPersonIdentification contactperson : LoginPresenter.globalContactpersonlist){
							if(contactperson.getEntype().trim().equalsIgnoreCase(entityType.trim())){
								if((((entityType.trim().equalsIgnoreCase("Customer")) && person.getCount()==contactperson.getPersonInfo().getCount())) || (entityType.trim().equalsIgnoreCase("Vendor") && person.getCount()==contactperson.getPersonVendorInfo().getCount())
										|| ((entityType.trim().equalsIgnoreCase("Employee") && person.getCount()==contactperson.getEmployeeInfo().getEmpCount()))) {
									list.add(contactperson);
									Console.log("contact added from contact list");
									flag=true;
								}
								
							}
						}
					}
					
					
				}
				
			}
			else{
				
				if(LoginPresenter.globalContactpersonlist!=null && LoginPresenter.globalContactpersonlist.size()!=0){
					for(ContactPersonIdentification contactperson : LoginPresenter.globalContactpersonlist){
						if(contactperson.getEntype().trim().equalsIgnoreCase(entityType.trim())){
							Console.log("contactperson.getcount"+contactperson.getPersonInfo().getCount());
							Console.log("personId"+personId);
							Console.log("1st condition"+(entityType.trim().equalsIgnoreCase("Customer")  || entityType.trim().equalsIgnoreCase("Vendor"))) ;
							Console.log("2nd condition"+(((entityType.trim().equalsIgnoreCase("Customer")  || entityType.trim().equalsIgnoreCase("Vendor")) && personId==contactperson.getPersonInfo().getCount())));
							Console.log("3rd condition"+((entityType.trim().equalsIgnoreCase("Employee") && personId==contactperson.getEmployeeInfo().getEmpCount())));
							if((((entityType.trim().equalsIgnoreCase("Customer")  || entityType.trim().equalsIgnoreCase("Vendor")) && personId==contactperson.getPersonInfo().getCount())) ||
									((entityType.trim().equalsIgnoreCase("Employee") && personId==contactperson.getEmployeeInfo().getEmpCount()))) {
								list.add(contactperson);
								Console.log("contact added from contact list");
								flag=true;
							}
							
						}
					}
				}
				else{
					Console.log("else block to load contact list"+list.size());
					flag=false;
					olbContactlistEmailId.makeliveContactPerson(list,personName,personemail,false);
				}
			}
			
			
			if(flag || personId==-1){
				Console.log("another if block to load contact list"+list.size());
				olbContactlistEmailId.makeliveContactPerson(list,personName,personemail,false);
			}
			else{
				Console.log("else block no contact list");
				if(list.size()==0 && personName!=null && personemail!=null && !personName.equals("") && !personemail.equals("")){
					olbContactlistEmailId.makeliveContactPerson(list,personName,personemail,false);
				}
				else if(LoginPresenter.globalContactpersonlist.size()!=0 && personName!=null && personemail!=null && !personName.equals("") && !personemail.equals("")){
					olbContactlistEmailId.makeliveContactPerson(list,personName,personemail,false);
				}
			}
			
			
		}

		public static MyQuerry getcustomerQuerry(int count) {
			
			MyQuerry querry=new MyQuerry();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setQuerryString("count");
			temp.setIntValue(count);
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			
			return querry;
		}

		public static void makeLiveSmsTemplateConfig(ObjectListBox<EmailTemplate> oblTemplateName, String moduleName,  Screen screenName) {

			List<String> templateNamelist = new ArrayList<String>();
			String documentName = AuthorizationHelper.checkScreenName(screenName);
			Console.log("documentName "+documentName);
			Console.log("moduleName"+moduleName);
			for(EmailTemplateConfiguration emailConfig : LoginPresenter.globalEmailTemplateConfiglist){
				System.out.println("config status"+emailConfig.isStatus());
				System.out.println("emailConfig.getModuleName() "+emailConfig.getModuleName());
				System.out.println("emailConfig.getDocumentName()"+emailConfig.getDocumentName());
				
				if(emailConfig.getCommunicationChannel().trim().equals("Email") && emailConfig.isStatus() && emailConfig.getModuleName().trim().equals(moduleName.trim()) && emailConfig.getDocumentName().trim().equals(documentName.trim()) ){
					templateNamelist.add(emailConfig.getTemplateName());
				}
			}
			
			Console.log("templateNamelist"+templateNamelist.size());

			List<EmailTemplate> emailtemplatelist = new ArrayList<EmailTemplate>();
			for(String strtemplateName : templateNamelist ){
				for(EmailTemplate emailTemplate : LoginPresenter.globalEmailTemplatelist){
					System.out.println("emailTemplate.getTemplateName().trim() "+ emailTemplate.getTemplateName().trim());
					System.out.println("strtemplateName" +strtemplateName);
					System.out.println("emailTemplate.isTemplateStatus()"+emailTemplate.isTemplateStatus());
					if(emailTemplate.isTemplateStatus() && strtemplateName.trim().equals(emailTemplate.getTemplateName().trim())){
						emailtemplatelist.add(emailTemplate);
					}
				}
			}
			Console.log("emailtemplatelist"+emailtemplatelist.size());

			oblTemplateName.makeliveEmailTemplate(emailtemplatelist);
		}

		public static String getCustomerEmailid(int customerId) {
			String customerEmail ="";
			if(PersonInfoComposite.globalCustomerArray!=null && PersonInfoComposite.globalCustomerArray.size()>0){
				for(PersonInfo custEntity : PersonInfoComposite.globalCustomerArray){
					if(custEntity.getCount()==customerId){
						if(custEntity.getEmail()!=null)
							customerEmail = custEntity.getEmail();
						break;
					}
				}
			}
			return customerEmail;
		}
		
		/**
		 *     Added By : Priyanka Bhagwat
		 *     Date : 1/06/2021
		 *     Des : When Submit PO then SMS and EMAIL will be send Req by Pecopp raised By Ashwini.	
		 */
		public static String getVendorEmailid(int vendorId) {
			String customerEmail ="";
			if(PersonInfoComposite.globalVendorArray!=null && PersonInfoComposite.globalVendorArray.size()>0){
				for(PersonInfo custEntity : PersonInfoComposite.globalVendorArray){
					if(custEntity.getCount()==vendorId){
						if(custEntity.getEmail()!=null)
							customerEmail = custEntity.getEmail();
						break;
					}
				}
			}
			return customerEmail;
		}
		
		public static Long getVendorCellNo(int vendorId) {
			Long vendorCell = null ;
			if(PersonInfoComposite.globalVendorArray!=null && PersonInfoComposite.globalVendorArray.size()>0){
				for(PersonInfo custEntity : PersonInfoComposite.globalVendorArray){
					if(custEntity.getCount()==vendorId){
						if(custEntity.getCellNumber()!=null)
							vendorCell = custEntity.getCellNumber();
						break;
					}
				}
			}
			return vendorCell;
		}
		/**
		 *  End
		 */
		
		public static String getEntityName(Screen screen) {
			Console.log("screen "+screen);
			String EntityName = "";
			switch (screen) {
			
			case LEAD:
				EntityName = "Lead";
				break;
				
			case QUOTATION:
				EntityName = "Quotation";
				break;
				
			case CONTRACT:
				EntityName = "Contract";
				break;
			
			case INVOICEDETAILS:
				EntityName = "Invoice";
				break;
				
			case PAYMENTDETAILS:
				EntityName = "CustomerPayment";
				break;
			
			case SALESLEAD:
				EntityName = "Lead";
				break;
				
			case SALESQUOTATION:
				EntityName = "SalesQuotation";
				break;	
				
			case SALESORDER:
				EntityName = "SalesOrder";
				break;
				
			case COMPLAIN:
				EntityName = "Complain";
				break;	
				
			case SERVICE:
				EntityName = "Service";
				break;	
				
			case DELIVERYNOTE:
				EntityName = "DeliveryNote";
				break;	
				
			case PURCHASEREQUISITE:
				EntityName = "PurchaseRequisition";
				break;
				
			case REQUESTFORQUOTATION:
				EntityName = "RequsestForQuotation";
				break;
				
			case LETTEROFINTENT:
				EntityName = "LetterOfIntent";
				break;
			
			case PURCHASEORDER:
				EntityName = "PurchaseOrder";
				break;	
				
			case VENDORINVOICEDETAILS:
				EntityName = "VendorInvoice";
				break;	
			
			case SALARYSLIP:
				EntityName = "PaySlip";
				break;
				
			case VENDORPAYMENTDETAILS:
				EntityName = "CustomerPayment";
				break;
				
			case QUICKCONTRACT:
				EntityName = "Contract";
				break;
				
			case QUICKSALESORDER:
				EntityName = "SalesOrder";
				break;	
				
			case QUICKPURCHASEORDER:
				EntityName = "PurchaseOrder";
				break;	
				
			case CONTRACTRENEWAL:
				EntityName = "ContractRenewal";
				break;
				
			case DEVICE:
				EntityName = "Service";
				Console.log("EntityName "+EntityName);
				break;
				
			default:
				EntityName = screen.toString();
			}
			return EntityName;	
		}

		public static String getPurchaseEngineerEmailid(String employeeName) {
			for(Employee empEntity : LoginPresenter.globalEmployee){
				if(empEntity.getFullname().trim().equals(employeeName.trim()) && empEntity.getEmail()!=null && !empEntity.getEmail().equals("")){
					return empEntity.getEmail();
				}
			}
			return null;
		}

		public static List<ContactPersonIdentification> getvenodrEmailId(List<VendorDetails> vendorlist, boolean otherThanPOFlag) {
			List<ContactPersonIdentification> list = new ArrayList<ContactPersonIdentification>();
			System.out.println("globalVendorArray "+PersonInfoComposite.globalVendorArray.size());
			if(PersonInfoComposite.globalVendorArray!=null && PersonInfoComposite.globalVendorArray.size()>0){
				for(VendorDetails vendor : vendorlist){
					if(vendor.getStatus() || otherThanPOFlag){
					for(PersonInfo personInfo : PersonInfoComposite.globalVendorArray){
						System.out.println("personInfo.getcount "+personInfo.getCount());
						System.out.println("vendor.getVendorId() "+vendor.getVendorId());
						System.out.println("vendor.getVendorId()="+vendor.getCount());
						if(personInfo.getCount()==vendor.getVendorId()){
							System.out.println("condtion ok"+personInfo.getEmail());
							if(personInfo.getEmail()!=null && !personInfo.getEmail().equals("")){
								System.out.println("email address found");
								ContactPersonIdentification contactPerson = new ContactPersonIdentification();
								contactPerson.setName(vendor.getVendorName());
								contactPerson.setEmail(personInfo.getEmail());
								contactPerson.setCount(personInfo.getCount());
								contactPerson.setCell(vendor.getVendorPhone());
								list.add(contactPerson);

							}
						}
					}
					}
				}
				
			}
			return list;
		}

		public static List<ContactPersonIdentification> getvenodrEmailId(int vendorId) {
			List<ContactPersonIdentification> list = new ArrayList<ContactPersonIdentification>();
			System.out.println("globalVendorArray "+PersonInfoComposite.globalVendorArray.size());
			if(PersonInfoComposite.globalVendorArray!=null && PersonInfoComposite.globalVendorArray.size()>0){
					for(PersonInfo personInfo : PersonInfoComposite.globalVendorArray){
						System.out.println("personInfo.getcount "+personInfo.getCount());
						if(personInfo.getCount()==vendorId){
							System.out.println("condtion ok"+personInfo.getEmail());
							if(personInfo.getEmail()!=null && !personInfo.getEmail().equals("")){
								System.out.println("email address found");
								ContactPersonIdentification contactPerson = new ContactPersonIdentification();
								contactPerson.setName(personInfo.getFullName());
								contactPerson.setEmail(personInfo.getEmail());
								contactPerson.setCount(personInfo.getCount());
								list.add(contactPerson);

							}
						}
				}
				
			}
			return list;
		}

		public static List<ContactPersonIdentification> getEmployeeEmailid(int empid) {
			List<ContactPersonIdentification> list = new ArrayList<ContactPersonIdentification>();
			if(LoginPresenter.globalEmployee!=null && LoginPresenter.globalEmployee.size()>0){
				for(Employee employee : LoginPresenter.globalEmployee){
					if(employee.getCount() == empid){
						System.out.println("employee id match");
						if(employee.getEmail()!=null && !employee.getEmail().equals("")){
							System.out.println("found email id");
							ContactPersonIdentification contactPerson = new ContactPersonIdentification();
							contactPerson.setName(employee.getFullName());
							contactPerson.setEmail(employee.getEmail());
							contactPerson.setCount(employee.getCount());

							list.add(contactPerson);
						}
					}
				}
			}
			
			return list;
		}

		
		public static void loadContactPersonlist(ObjectListBox<ContactPersonIdentification> olbContactlistEmailId, String entityType, int personId, String personName, String personCellNumber, List<ContactPersonIdentification> vendorDetailslist, boolean newflag) {
			List<ContactPersonIdentification> list = new ArrayList<ContactPersonIdentification>();
			List<ContactPersonIdentification> templist = new ArrayList<ContactPersonIdentification>();

			boolean multiplepersonIdsFlag=false;
			if(entityType.trim().equalsIgnoreCase("Vendor") && vendorDetailslist!=null && vendorDetailslist.size()>0){
				list.addAll(vendorDetailslist);
				multiplepersonIdsFlag = true;
				templist.addAll(vendorDetailslist);
			}
			else if(entityType.trim().equalsIgnoreCase("Employee") && vendorDetailslist!=null && vendorDetailslist.size()>0){
				list.addAll(vendorDetailslist); // employee list
				multiplepersonIdsFlag = true;
				templist.addAll(vendorDetailslist);
			}
			else if(entityType.trim().equalsIgnoreCase("Customer") && vendorDetailslist!=null && vendorDetailslist.size()>0){
				list.addAll(vendorDetailslist); // this is for service screen customer branch email ids
			}
			boolean flag = false;

			Console.log("LoginPresenter.globalContactpersonlist"+LoginPresenter.globalContactpersonlist.size());

			if(multiplepersonIdsFlag){
				
				if(LoginPresenter.globalContactpersonlist!=null && LoginPresenter.globalContactpersonlist.size()!=0){
					
					for(ContactPersonIdentification person : templist){
						
						for(ContactPersonIdentification contactperson : LoginPresenter.globalContactpersonlist){
							if(contactperson.getEntype().trim().equalsIgnoreCase(entityType.trim())){
								if((((entityType.trim().equalsIgnoreCase("Customer")) && person.getCount()==contactperson.getPersonInfo().getCount())) || (entityType.trim().equalsIgnoreCase("Vendor") && person.getCount()==contactperson.getPersonVendorInfo().getCount())
										|| ((entityType.trim().equalsIgnoreCase("Employee") && person.getCount()==contactperson.getEmployeeInfo().getEmpCount()))) {
									list.add(contactperson);
									Console.log("contact added from contact list");
									flag=true;
								}
								
							}
						}
					}
					
					
				}
				
			}
			else{
				
				if(LoginPresenter.globalContactpersonlist!=null && LoginPresenter.globalContactpersonlist.size()!=0){
					for(ContactPersonIdentification contactperson : LoginPresenter.globalContactpersonlist){
						if(contactperson.getEntype().trim().equalsIgnoreCase(entityType.trim())){
							Console.log("contactperson.getcount"+contactperson.getPersonInfo().getCount());
							Console.log("personId"+personId);
							Console.log("1st condition"+(entityType.trim().equalsIgnoreCase("Customer")  || entityType.trim().equalsIgnoreCase("Vendor"))) ;
							Console.log("2nd condition"+(((entityType.trim().equalsIgnoreCase("Customer")  || entityType.trim().equalsIgnoreCase("Vendor")) && personId==contactperson.getPersonInfo().getCount())));
							Console.log("3rd condition"+((entityType.trim().equalsIgnoreCase("Employee") && personId==contactperson.getEmployeeInfo().getEmpCount())));
							if((((entityType.trim().equalsIgnoreCase("Customer")  || entityType.trim().equalsIgnoreCase("Vendor")) && personId==contactperson.getPersonInfo().getCount())) ||
									((entityType.trim().equalsIgnoreCase("Employee") && personId==contactperson.getEmployeeInfo().getEmpCount()))) {
								list.add(contactperson);
								Console.log("contact added from contact list");
								flag=true;
							}
							
						}
					}
				}
				else{
					Console.log("else block to load contact list = "+list.size());
					flag=false;
					olbContactlistEmailId.makeliveContactPerson(list,personName,personCellNumber,true);
				}
			}
			
			
			if(flag || personId==-1){
				Console.log("another if block to load contact list"+list.size());
				olbContactlistEmailId.makeliveContactPerson(list,personName,personCellNumber,true);
			}
			else{
				Console.log("else block no contact list");
				if(list.size()==0 && personName!=null && personCellNumber!=null && !personCellNumber.equals("") && !personCellNumber.equals("")){
					olbContactlistEmailId.makeliveContactPerson(list,personName,personCellNumber,true);
				}
				else if((LoginPresenter.globalContactpersonlist==null || LoginPresenter.globalContactpersonlist.size()==0) && personName!=null && personCellNumber!=null && !personCellNumber.equals("") && !personCellNumber.equals("")){
					Console.log("no contact list and with location master list size"+list.size());
					olbContactlistEmailId.makeliveContactPerson(list,personName,personCellNumber,true);
				}
			}
			
		}
		
		
		public static void makeLiveSMSTemplate(ObjectListBox<?> olb)
		{
			MyQuerry qu= new MyQuerry();
			Filter filter=new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(true);
			qu.getFilters().add(filter);
			qu.setQuerryObject(new SmsTemplate());
			olb.MakeLive(qu);
		}

		public static void makeLiveSmsTemplateConfig(ObjectListBox<SmsTemplate> oblsmsTemplate, String moduleName,
				Screen screenName,boolean flag,String communicationChannelName) {



			List<String> templateNamelist = new ArrayList<String>();
			String documentName = AuthorizationHelper.checkScreenName(screenName);
			Console.log("documentName "+documentName);
			Console.log("moduleName"+moduleName);
			for(EmailTemplateConfiguration emailConfig : LoginPresenter.globalEmailTemplateConfiglist){
				System.out.println("config status"+emailConfig.isStatus());
				System.out.println("emailConfig.getModuleName() "+emailConfig.getModuleName());
				System.out.println("emailConfig.getDocumentName()"+emailConfig.getDocumentName());
				
//				if(emailConfig.getCommunicationChannel().trim().equalsIgnoreCase("SMS") && emailConfig.isStatus() && emailConfig.getModuleName().trim().equals(moduleName.trim()) && emailConfig.getDocumentName().trim().equals(documentName.trim()) ){
//					templateNamelist.add(emailConfig.getTemplateName());
//				}
				if(emailConfig.getCommunicationChannel().trim().equalsIgnoreCase(communicationChannelName) && emailConfig.isStatus() && emailConfig.getModuleName().trim().equals(moduleName.trim()) && emailConfig.getDocumentName().trim().equals(documentName.trim()) ){
					templateNamelist.add(emailConfig.getTemplateName());
				}
			}
			
			Console.log("templateNamelist"+templateNamelist.size());
			List<SmsTemplate> smsTemplatelist = new ArrayList<SmsTemplate>();

			for(String strtemplateName : templateNamelist ){
				for(SmsTemplate smsTemplate : LoginPresenter.globalSMSTemplatelist){
//					System.out.println("emailTemplate.getTemplateName().trim() "+ emailTemplate.getTemplateName().trim());
//					System.out.println("strtemplateName" +strtemplateName);
//					System.out.println("emailTemplate.isTemplateStatus()"+emailTemplate.isTemplateStatus());
//					if(emailTemplate.isTemplateStatus() && strtemplateName.trim().equals(emailTemplate.getTemplateName().trim())){
//						emailtemplatelist.add(emailTemplate);
//					}
					if(smsTemplate.getStatus() && strtemplateName.trim().equals(smsTemplate.getEvent().trim())){
//						EmailTemplate template = new EmailTemplate();
//						template.setCompanyId(smsTemplate.getCompanyId());
//						template.setTemplateName(smsTemplate.getEvent());
//						template.setTemplateStatus(smsTemplate.getStatus());
//						template.setEmailBody(smsTemplate.getMessage());
//						smsTemplatelist.add(template);
						
						smsTemplatelist.add(smsTemplate);
					}
				}
			}
			Console.log("smsTemplatelist"+smsTemplatelist.size());

			oblsmsTemplate.makeliveSMSTemplate(smsTemplatelist);
		
		}

		public static boolean validateNumricValue(String strvalue) {
			
			if(strvalue.matches("[0-9]+(\\.){0,1}[0-9]*") ) {
				return true;
			}
			else {
				return false;
			}
		}
		/*
		 * Added by Sheetal : 27-10-2021
		 * Des : Document upload in Customer Report section should be mandatory before closure of service
		 */
	  public static boolean ValidateCustomerReport(DocumentUpload report1, DocumentUpload report2){
	
		if(report1==null && report2==null){
			return true;
		}
			return false;
	}
	  
	  public double getLastTwoDecimalOnly(double AmountIncludingmoredecimal) {

			String strtotalamtincludingtax = AmountIncludingmoredecimal+"";
			if(strtotalamtincludingtax.contains(".")){
				String [] includingTaxAmtString = strtotalamtincludingtax.split("\\.");
				String firstDecimal = includingTaxAmtString[0];
				String secondDecimal = includingTaxAmtString[1];
				String twoDecimal = null;
				for(int p=0;p<secondDecimal.length();p++){
					if(p==0){
						twoDecimal = secondDecimal.charAt(p)+"";
					}
					else{
						twoDecimal += secondDecimal.charAt(p)+"";
					}
					if(p==1){
						break;
					}
				}
				System.out.println("twoDecimal "+twoDecimal);
				if(twoDecimal!=null){
					firstDecimal = includingTaxAmtString[0] +"."+twoDecimal;
					return Double.parseDouble(firstDecimal);
				}
				else{
					return AmountIncludingmoredecimal;
				}
			}
			return AmountIncludingmoredecimal;

		}
	  
	  /**
	   * @author Vijay Date :- 22-07-2022
	   * Des :- to get difference days of two dates in GWT client side 
	   * reusable method
	   */
	  public static int getDifferenceDays(Date d1, Date d2) {
		  	int daysdiff = 0;
		    long diff = d2.getTime() - d1.getTime();
		    long diffDays = diff / (24 * 60 * 60 * 1000) + 1;
		    daysdiff = (int) diffDays;
		    return daysdiff;
		}
	  
	  public static String parseDate(Date date, String dateformat){
			
		  DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
			return format.format(date);
	}
	  
	  public static void makeLiveTermsAndCondition(ObjectListBox<?> olb, String documentName)
		{
			MyQuerry qu= new MyQuerry();
			Filter filter;
			filter=new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(true);
			qu.getFilters().add(filter);
			
			filter=new Filter();
			filter.setQuerryString("document");
			filter.setStringValue(documentName);
			qu.getFilters().add(filter);
			
			qu.setQuerryObject(new TermsAndConditions());
			olb.MakeLive(qu);
		}
	  
	  public static String validateProductCode(List<SalesLineItem> items) {
		  Console.log("in validateProductCode");
		  String response="";
		  ArrayList<ProductInfo> productsDB=ProductInfoComposite.globalServiceProdArray;
		  ArrayList<String> unavailableProductCodes=new ArrayList<String>();
		  for(SalesLineItem item:items) {
			  boolean foundFlag=false;
			  for(ProductInfo pinfo:productsDB) {
				  
				  if(pinfo.getProductCode().equals(item.getProductCode())&&pinfo.getProdID()==item.getPrduct().getCount()) {
					  foundFlag=true;
					  break;
				  }				  
			  }		
			  if(!foundFlag) {
				  unavailableProductCodes.add(item.getProductCode());
				  response+=" Product code "+item.getProductCode()+" missing in product master,";
			  }
			  }
		  
		  if(!response.equals("")) {
			  response+=" kindly add all the products manually.";
		  }
		  
		  return response;
	  }
	  
	  public static boolean validateServiceCompletionDate(Date serviceDate) {
		  //Console.log("in validateServiceCompletionDate serviceDate="+serviceDate);
		  if(LoginPresenter.company!=null) {
			  if(LoginPresenter.company.getServiceCompletionDeadline()!=null&&LoginPresenter.company.getServiceCompletionDeadline()>0) {
				  if(serviceDate.before(new Date())) {
					//  Console.log("Trying to mark complete old service");
					  int deadline=LoginPresenter.company.getServiceCompletionDeadline();	
					  int currentDay=new Date().getDate();
					  int currentMonth=new Date().getMonth();
					  int currentYear=new Date().getYear();
					  int serviceDay=serviceDate.getDate();
					  int serviceMonth=serviceDate.getMonth();
					  int serviceYear=serviceDate.getYear();
					  Console.log("deadline="+deadline+" currentDay="+currentDay+" currentMonth="+currentMonth+" currentYear="+currentYear);
					 // Console.log("serviceDay="+serviceDay+" serviceMonth="+serviceMonth+" serviceYear="+serviceYear);
					  if(serviceYear==currentYear) {
						  if(serviceMonth<currentMonth) {
							if((currentMonth-serviceMonth)>1)
								return false;
							else if(currentDay>deadline)
								return false;
							else 
								return true;
						  }
					  }else if(serviceYear<currentYear) {
						  if((currentYear-serviceYear)>1)
							  return false;
						  else if(currentMonth!=0) //current month is not january means more than a month old service
							  return false;
						  else if(serviceMonth!=11) //current month january but service month is not december means more than a month old service
							  return false;
						  else if(currentDay>deadline) //current month jan and service month last december
							return false;
						  else 
							  return true;
					  }
				}
			  }
		  }else {
			  Console.log("company found null");
		  }
		  
		  return true;
	  }

	  public static boolean validateInvoiceEditDate(Date invDate) {
		  Console.log("in validateServiceCompletionDate serviceDate="+invDate);
		  if(LoginPresenter.company!=null) {
			  if(LoginPresenter.company.getInvoiceEditDeadline()!=null&&LoginPresenter.company.getInvoiceEditDeadline()>0) {
				  if(invDate.before(new Date())) {
					  int deadline=LoginPresenter.company.getInvoiceEditDeadline();	
					  int currentDay=new Date().getDate();
					  int currentMonth=new Date().getMonth();
					  int currentYear=new Date().getYear();
					  int invDay=invDate.getDate();
					  int invMonth=invDate.getMonth();
					  int invYear=invDate.getYear();
					  Console.log("deadline="+deadline+" currentDay="+currentDay+" currentMonth="+currentMonth+" currentYear="+currentYear);
					  if(invYear==currentYear) {
						  if(invMonth<currentMonth) {
							if((currentMonth-invMonth)>1)
								return false;
							else if(currentDay>deadline)
								return false;
							else 
								return true;
						  }
					  }else if(invYear<currentYear) {
						  if((currentYear-invYear)>1)
							  return false;
						  else if(currentMonth!=0) //current month is not january means more than a month old service
							  return false;
						  else if(invMonth!=11) //current month january but service month is not december means more than a month old service
							  return false;
						  else if(currentDay>deadline) //current month jan and service month last december
							return false;
						  else 
							  return true;
					  }
				}
			  }
		  }else {
			  Console.log("company found null");
		  }
		  
		  return true;
	  }

}

