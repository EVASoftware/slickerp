package com.slicktechnologies.client.utility;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.UnitConverCal;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterialDetails;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.BillProductDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.unitconversion.UnitConversion;
import com.slicktechnologies.shared.common.unitconversion.UnitConversionDetailList;

public class UnitConversionUtility {

	ArrayList<ItemProduct> proGroupItemList = new ArrayList<ItemProduct>();
//	DecimalFormat df2 = new DecimalFormat( "#.00" );
	NumberFormat dft2 = NumberFormat.getFormat("#0.000");

	public ArrayList<ProductGroupList> getServiceProDetails(UnitConverCal unitCal,String unitName,Double area,String prodTitle){
		
		ArrayList<ProductGroupList> prodtList = new ArrayList<ProductGroupList>();
		
		ProductGroupDetails prGpLt = new ProductGroupDetails();
		for(ProductGroupDetails prDt  : LoginPresenter.proGroupList){
			if(prDt.getTitle().equals(prodTitle)){
				prGpLt = prDt;
				break;
			}
		}
		
		if(prGpLt!=null){
			Console.log("billPrDt1.getBillProdItems() " + prGpLt.getTitle());
			for(ProductGroupList proDt : prGpLt.getPitems()){
				ProductGroupList proGrpList = proDt.copyOfObject();
				String prUnit = proGrpList.getUnit();
				System.out.println("unit --  " + proDt.getQuantity());
				Double prQty = proGrpList.getQuantity();
				proGrpList.setProActualQty(area);

				proGrpList.setProActualUnit(unitName);
				System.out.println("proGrpList.getQuantity() "+proGrpList.getQuantity());
				
				if(unitName.equals(prGpLt.getUnit())){
					prQty = (area*proGrpList.getQuantity()) / prGpLt.getArea();
				}else{
					double unitConver  = getConversionOfUnit(prGpLt.getUnit(),unitName,prGpLt.getArea());
					prQty = (area * proGrpList.getQuantity())/ unitConver;
				}
				
				System.out.println("prQty "+prQty);
				/**
				 * @author Vijay Chougule
				 * Des :- if area is not defined the quantity will map from BOM quantity
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", AppConstants.AUTOMATICSCHEDULING)&&area==0){
					prQty = proGrpList.getQuantity();
				}
				/**
				 * ends here
				 */
				
				double price = getPriceConversion(proGrpList.getUnit(), prQty, prUnit, proGrpList.getCode());
				proGrpList.setPrice(Double.parseDouble(dft2.format(price)));
				proGrpList.setUnit(prUnit);
				proGrpList.setQuantity(Double.parseDouble(dft2.format(prQty)));
				/**
				 *  ||*||
				 *  nidhi 27-12-2018 for area and unit
				 */
				proGrpList.setProActualUnit(unitName);
				proGrpList.setProActualQty(Double.parseDouble(dft2.format(area)));
				proGrpList.setPlannedQty(prQty);
				proGrpList.setPlannedUnit(prUnit);
				/**
				 * end ||*||
				 */
				prodtList.add(proGrpList);
				
			}
		}
		
		return prodtList;
	}
	
	public boolean varifyUnitConversion(BillOfMaterial billPRod, String unit){
		
		boolean get = true;
		for(BillProductDetails prDt: billPRod.getBillProdItems()){
			get = false;
			for(ProductGroupDetails pr : LoginPresenter.proGroupList){
				if(prDt.getProdGroupTitle().equals(pr.getTitle())){
					if(pr.getUnit().equals(unit)){
						get = true;
						break;
					}else{
						for( UnitConversion unitCon : LoginPresenter.areaUnitConverList){
							if(pr.getUnit().equals(unitCon.getUnit())){
								for(UnitConversionDetailList unList :unitCon.getUnitConversionList()){
									if((unList.getMaxUnit().equals(unit) && unList.getMinUnit().equals(pr.getUnit())) ||
											(unList.getMinUnit().equals(unit) && unList.getMaxUnit().equals(pr.getUnit()))){
										get = true;
										break;
									}
								}
							}
						}
					}
				}
			}
			if(!get){
				return false;
			}
		}
		
		return get;
	}
	
	public List<ServiceSchedule>  getServiceitemProList(List<ServiceSchedule> popuplist,SuperProduct suprPrd,SalesLineItem lis1,BillOfMaterial billPrDt1){
		 HashMap<Integer, ArrayList<BranchWiseScheduling>> custBranch = lis1.getCustomerBranchSchedulingInfo();
		 
		 List<ServiceSchedule> seriveScheList = new ArrayList<ServiceSchedule>();
		 
		 Console.log("Cust brancch -- " + custBranch.size() +"\n branch key set -- " + custBranch.keySet());
		 Console.log("lisl size  number --" +lis1.getProductSrNo());
		 Console.log("billPrDt1.getBillProdItems() " + billPrDt1.getBillProdItems().size());
		 List<ServiceSchedule> branchSerlist = new ArrayList<ServiceSchedule>();
		 if(custBranch!=null && custBranch.containsKey(lis1.getProductSrNo())){
			 for(BranchWiseScheduling brch : custBranch.get(lis1.getProductSrNo())){
			 		if(brch.isCheck()){
			 			for(ServiceSchedule serSch: popuplist){
			 				if(serSch.getScheduleProBranch().equals(brch.getBranchName())){
			 					branchSerlist.add(serSch);
			 					continue;
			 				}
			 			}
			 			
			 			if(branchSerlist.size() == lis1.getNo_Of_Services()){
			 				Comparator<ServiceSchedule> serviceScheduleComparator = new Comparator<ServiceSchedule>() {
								public int compare(ServiceSchedule s1, ServiceSchedule s2) {
									if(s1!=null && s2!=null)
									{
										if(s1.getScheduleServiceNo()== s2.getScheduleServiceNo()){
											return 0;}
										if(s1.getScheduleServiceNo()> s2.getScheduleServiceNo()){
											return 1;}
										else{
											return -1;}
									}
									else{
										return 0;}
								}
								};
								Collections.sort(branchSerlist, serviceScheduleComparator);
			 			}
			 			
			 			
			 			if(billPrDt1.getBillProdItems().size()>0){

			 				Comparator<BillProductDetails> serviceScheduleComparator = new Comparator<BillProductDetails>() {
								public int compare(BillProductDetails s1, BillProductDetails s2) {
									if(s1!=null && s2!=null)
									{
										if(s1.getServiceNo() == s2.getServiceNo()){
											return 0;}
										if(s1.getServiceNo()> s2.getServiceNo()){
											return 1;}
										else{
											return -1;}
									}
									else{
										return 0;}
								}
								};
								Collections.sort(billPrDt1.getBillProdItems(), serviceScheduleComparator);
			 			
			 					boolean get = false;
								for(int i=0;i< branchSerlist.size();i++){
									get = false;
									for(BillProductDetails bill : billPrDt1.getBillProdItems()){
										int j = i;
										
										if(bill.getServiceNo() == branchSerlist.get(i).getScheduleServiceNo()){
											get = true;
											
											ArrayList<ProductGroupList> proGrop = getServiceProDetails(null,brch.getUnitOfMeasurement(),brch.getArea(),bill.getProdGroupTitle());
											HashMap<Integer,  ArrayList<ProductGroupList> > serProdList = new HashMap<Integer, ArrayList<ProductGroupList>>();
											serProdList.put(0,proGrop);
											branchSerlist.get(i).setServiceProductList(serProdList);
											seriveScheList.add(branchSerlist.get(i).copyOfObject());
											break;
										}
									}
									if(!get){
										BillProductDetails bill1 =	billPrDt1.getBillProdItems().get(billPrDt1.getBillProdItems().size()-1);
										
										ArrayList<ProductGroupList> proGrop = getServiceProDetails(null,brch.getUnitOfMeasurement(),brch.getArea(),bill1.getProdGroupTitle());
										HashMap<Integer,  ArrayList<ProductGroupList> > serProdList = new HashMap<Integer, ArrayList<ProductGroupList>>();
										serProdList.put(0,proGrop);
										branchSerlist.get(i).setServiceProductList(serProdList);
										seriveScheList.add(branchSerlist.get(i).copyOfObject());
									}
									
								}
								
			 			}
			 			
			 			
			 		}
			 	} 
		 }
		 	
		 	
		 Console.log("material list size --  "+ seriveScheList.size());
		 return seriveScheList;
	}
	
	
	public double getConversionOfUnit(String prGroupUnit, String serviceUnit,double area){
		double unitValue = 1;
		
			for(UnitConversion unit : LoginPresenter.areaUnitConverList){
				if(unit.getUnit().equals(prGroupUnit)){
					for(UnitConversionDetailList unitDt : unit.getUnitConversionList()){
						
						if((unitDt.getMaxUnit().equals(prGroupUnit) && unitDt.getMinUnit().equals(serviceUnit))
							||	(unitDt.getMinUnit().equals(prGroupUnit) && unitDt.getMaxUnit().equals(serviceUnit))){
							if(prGroupUnit.equals(unitDt.getMinUnit())){
								unitValue = (area * unitDt.getMaxValue()) / unitDt.getMinValue();
							}else{
								unitValue = (area * unitDt.getMinValue()) / unitDt.getMaxValue();
							}
						}
					}
				}
			}
		
		return unitValue;
	}
	
	
	public UnitConverCal getConversionOfQtyUnit(String prGroupUnit,double qty){
		double unitValue = qty;
		String proUnit = prGroupUnit;
			for(UnitConversion unit : LoginPresenter.prodUnitConverList){
				if(unit.getUnit().equals(prGroupUnit)){
					for(UnitConversionDetailList unitDt : unit.getUnitConversionList()){
						if(unitDt.getMinValue()<qty && unitDt.getMinUnit().equals(prGroupUnit)){
							unitValue = (unitDt.getMaxValue() * qty ) / unitDt.getMinValue();
							proUnit = unitDt.getMaxUnit();
						}else if(unitDt.getMaxValue()>qty && unitDt.getMaxUnit().equals(prGroupUnit)){
							unitValue = (unitDt.getMinValue() * qty ) / unitDt.getMaxValue();
							proUnit = unitDt.getMinUnit();
						}
						
					}
				}
			}
			UnitConverCal unitcal = new UnitConverCal();
			unitcal.setArea(unitValue);
			unitcal.setUom(proUnit);
		return unitcal;
	}
	
	public double getPriceConversion(String proGroupUnit,double qty,String convertedUnit,String proCode){
		double upPrice = 1;
		
//			if(proGroupItemList != null && proGroupItemList.size()!=0){
				boolean flag = false;
				for(ItemProduct itm : proGroupItemList){
					if(itm.getProductCode().equals(proCode)){
						flag = true;
						upPrice = convertItemProPrice(itm,proGroupUnit,qty,convertedUnit,proCode);
						break;
					}
				}
				
				if(!flag){
					ItemProduct item = getItemProductFromGlobalLit(proCode);
					upPrice = convertItemProPrice(item, proGroupUnit, qty, convertedUnit, proCode);
				}
//			}
			
		
		return upPrice;
	}
	
	public ItemProduct getItemProductFromGlobalLit(String proCode){
		if(proGroupItemList == null){
			proGroupItemList = new ArrayList<ItemProduct>();
		}
		for(ItemProduct item : LoginPresenter.itemProList){
			if(item.getProductCode().equals(proCode)){
				
				proGroupItemList.add(item);
				return item;
			}
		}
		return null;
	}
	
	
	public double convertItemProPrice(ItemProduct item,String prGroupUnit,double qty,String convertedUnit,String proCode){
		double price = 1;
			if(convertedUnit.equals(prGroupUnit)){
				price = item.getPrice() * qty;
			}else{
				for(UnitConversion unit : LoginPresenter.prodUnitConverList){
					if(unit.getUnit().equals(prGroupUnit) && item.getUnitOfMeasurement().equals(prGroupUnit)){
						for(UnitConversionDetailList unitDt : unit.getUnitConversionList()){
							if(unitDt.getMinUnit().equals(prGroupUnit) && unitDt.getMaxUnit().equals(convertedUnit) ){
								price = (qty * item.getPrice()) / unitDt.getMaxValue(); 
							}
						}
					}
				}
			}
			
		return price;
	}
}
