package com.slicktechnologies.client.utility;

// TODO: Auto-generated Javadoc
/**
 * Every Generated Class should implement this interface.It provides a way to
 * acess the Generated Variables.
 */
public interface GeneratedVariableRefrence {
	
	/**
	 * Gets the var ref.
	 *
	 * @param varName the var name
	 * @return the var ref
	 */
	Object getVarRef(String varName);

}
