package com.slicktechnologies.client.utility;

import java.util.ArrayList;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.user.client.Timer;
import com.slicktechnologies.client.utils.Console;

// TODO: Auto-generated Javadoc
/**
 * Creates Constants used by application
 */
public class AppConstants {
	
	
	public final static String COMPANYPROFILECONFIG = "Company Profile Configuration";
	
	/***
	 * Date 15-5-2018 
	 * Add by jayshree
	 * Des.add new button employeeid card print
	 */
	public final static String PRINTIDCARDINPORTRATEVIEW = "Print ID Card in Portrait View";
	public final static String PRINTIDCARDINLANDSCAPEVIEW = "Print ID Card in Landscape View ";
	
	/**
	 * Date : 10-05-2018 BY ANIL
	 * Adding constant for revising salary
	 */
	public final static String SALARYREVISIONANDARREARS="Salary Revision/Arrears";
	
	
	/**
	 *Date : 19-04-2018 BY ANIL
	 */
	public final static String VIEWAMC="View AMC";
	public final static String VIEWPO="View PO";
	public final static String CREATEPO="Create PO";
	public final static String VIEWSALESORDER="View Sales Order";
	/**
	 *End
	 */
	
	/**
	 *Date : 06-05-2018 BY ANIL
	 *These are the types of license
	 */
	public interface LICENSETYPELIST{
		public String EVA_ERP_LICENSE="EVA ERP License";
		public String EVAERPLICENSE="EVAERPLICENSE"; //Ashwini Patil Date:12-05-2022
		public String EVA_SMS_LICENSE="EVA SMS License";
		public String EVA_TALLY_LICENSE="EVA TALLY License";
		
		public String EVA_PRIORA_LICENSE="EVA PRIORA License";
		public String EVA_PEDIO_LICENSE="EVA PEDIO License";
		public String EVA_KRETO_LICENSE="EVA KRETO License";
		/** date 20.4.2019 added by komal for eva attendance **/
		public String EVA_ATTENDANCE_LICENSE="EVA ATTENDANCE License";
		
		public String EVA_URL="my.evaesserp.appspot.com";
		public String EVA_LICENSE_UPDATE_URL="/slick_erp/clientsLicenseUpdateApi";
		public String EVA_VALID_PRODUCT_CATEGORY="Licensed";
		public String UPDATE_CLIENTS_LICENSE="Update Client's License";
		
		public String ACTION_TASK_CREATE="Create";
		public String ACTION_TASK_UPDATE="Update";
		public String ACTION_TASK_CANCEL="Cancel";
		public String UPDATE_PRODUCT="Update product's category and code";
		
		public String EVA_PTSPL_LICENSE="PTSPL Services License";
		public String CUSTOMERPORTAL="CUSTOMERPORTAL";
		public String AUDITLICENSE="PCAudit";//Ashwini Patil Date:27-07-2023 to validate audit license for pedio
		public String HRLICENSE="ATTENDANCEPAYROLL";//Ashwini Patil Date:6-09-2023
		
	}
	
	/**
	 * Updated By:Viraj
	 * Date: 26-03-2019
	 * Description: For all webServices of silverLining
	 */
	public interface FRANCHISETYPELIST {
		public String BRAND_URL="/slick_erp/fetchbranddetails";
		public String CUSTOMERUPDATE_URL = "/slick_erp/createcustomerdetails";
		public String CUSTPRODINVLIST_URL = "/slick_erp/fetchcustomerprodinvlist";
		public String CUSTSALESREGLIST_URL = "/slick_erp/fetchsalesregisterlist";
		public String SALESORDERGENERATION_URL = "/slick_erp/createsalesorder";
	}
	/** Ends **/
	
	/**
	 * Date : 24 Jan 2018
	 * Description : This is for menu items in invoice
	 * BY: Rahul Verma
	 * 
	 */
	public interface ANDROIDMENULIST {
        String LEAD_SUCCESSFULL = "Successfull";
        String LEAD_UNCESSFULL = "Unsucessfull";
        String LEAD_CHANGEFOLLOWUPDATE="Follow Up";
        String LEAD_COMMUNICATIONLOG="Communication Log";
        String QUOTATION_FOLLOWDATE="Follow Up";
        String QUOTATION_COMMUNICATIONLOG="Communication Log";
        String QUOTATION_EMAIL="Email";
        String CUSTOMER_SERVICE_RESCHEDULE = "Reschedule";
        String CUSTOMER_SERVICE_ASSIGN_TECHNICIAN = "Assign Technician";
        /***Date 20-1-2020 by AMOL added  a new Constant for Schedule Service**/
        String CUSTOMER_SERVICE_SCHEDULE="Schedule";
        String CUSTOMER_SERVICE_PLANNED="Plan";
        
        
        String CONTRACT = "Contract";
        String INVOICE = "Invoice Details";
        String PAYMENT = "Payment Details";
        String SALES_ORDER = "Sales Order";
        String CUSTOMER_SERVICE = "Customer Service";
        String CONTRACT_RENEWAL = "Contract Renewal";
		String COMPLAIN_DASHBOARD_ASSIGN_TO = "Assign To";
		String COMPLAIN_STATUS_UPDATE = "Update Status";
		String QUOTATION_UPDATE_SALES_PERSON = "Assign Sales Person";
    }
	
		
	/**
	 * Date: 31 Jul 2017
	 * By: Apeksha Gunjal
	 * Separate Android Constants
	 */
	
	public interface ANDROIDSCREENLIST {
        String LEAD = "Lead";
        String QUOTATION = "Quotation";
        String CONTRACT = "Contract";
        String INVOICE = "Invoice Details";
        String PAYMENT = "Payment Details";
        String SALES_ORDER = "Sales Order";
        String CUSTOMER_SERVICE = "Customer Service";
        String CONTRACT_RENEWAL = "Contract Renewal";
		String COMPLAIN_DASHBOARD = "Complain Dashboard";
		/** date 3.5.2019 added by komal for sale quotation **/
		String SALES_QUOTATION = "Sales Quotation";
    }
	
	public interface IAndroid {
		public final static String EVA_PEDIO = "EVA Pedio";
		public final static String EVA_KRETO = "EVA Kreto";
		public final static String EVA_PRIORA = "EVA PRIORA";
		static String API_KEY = "AAAAqLvb-Sk:APA91bEwrLLYQ3p_z3Z9Chd-PD1QaSlolpe4b-KKu3i76ePWayaR1ByIJ55DaO-kWZibOalyFhmOpr-WSX3hKM7iQHiaJWHnh-LhWqc4ZRcN8AakD9Zhn0Vs2u0eJd1OOcV08wHSIKeuKM3xk9D7O3nS9hDZG7zb_w";
//				"AAAAZPGA6AE:APA91bHH0r_HsFXl6Uhht_XlSoWf2-THhqDp0VfRyT_Ni5eWs9AaeX2URd35nmnryMldb8ZRTeEypee6HgPyJD0oFUneHx1nUuwwTdhwuPsp3zVLK-bmiJF29e8q_QCkT7BEYsXzA_li";
	}
	/**
	 * Added By ANIL On Date : 15-10-2016
	 * Used In PR and PO
	 * Release : 30 Sept 2016
	 * Project : Purchase Modification (NBHC)
	 */
	public final static String  ASSIGNPRTOPOPURCHASEENGG ="Assign PR To PO Purchase Engineer";
	public final static String  SENDMAILTOASSIGNPERSON ="Send Mail To Assign Person";
	public final static String ASSIGNASSET="Assign Asset";
	public final static String RETIREASSET="Retire Asset";
	public final static String CREATEASSET="Create Asset";
	public final static String CREATEMASSASSET="Create Mass Assets";
	public final static String UPLOADASSET="Upload Asset";
	/**
	 * Added by Rahul Verma on 19 June 2017
	 * Used in GRN form
	 * Description: This will cancel whole record and generate a record in created state where we can change the required quantity.
	 */
	public final static String PARTIALCANCEL="Partial Cancel";
	
	
	public final static String  PROCESSED ="Processed";
	
	public final static String  UNPROCESSED ="Unprocessed";
	
	public final static String  RENEWED ="Renewed";
	public final static String  NEVERRENEW ="Never Renew";
	public final static String  DONOTRENEW ="Do Not Renew";
	
	 
	/** The Constant ACTIVE. */
	public final static String  ACTIVE ="Active";
	
	/** The Constant INACTIVE. */
	public final static String  INACTIVE ="Inactive";
	
	
	
	/** The Constant ACTIVE. */
	public final static String  TRUE ="true";
	
	
	/** The Constant YES. */
	public static final String YES = "YES";
	
	/** The Constant NO. */
	public static final String NO = "NO";
	
	/** The Constant YES. */
	public static final String ENGLISH = "ENGLISH";
	
	/** The Constant NO. */
	public static final String THAI = "THAI";
	
	/** The Constant NA. */
	public static final String NA ="N.A";
	
	
	
	/** The Constant CREATELEAD. */
	public static final String CREATELEAD="Create Lead";
	
	/** The Constant CREATEQUOTATION. */
	public static final String CREATEQUOTATION="Create Quotation";
	
	/** The Constant CREATECONTRACT. */
	public static final String CREATECONTRACT="Create Contract";
	
	/** The Constant CREATEQUICKCONTRACT. */
	public static final String CREATEQUICKCONTRACT="Create Quick Contract";
	
	/** The Constant VIEWSERVICES. */
	public static final String VIEWSERVICES="View Services";
	
	/** The Constant NEW. */
	public static final String NEW="New";
	
	/** The Constant RENEW. */
	public static final String RENEW="Renew";
	
	/** The Constant CANCEL. */
	public static final String CANCEL="Cancel";
	
	/** The Constant PAYMENT. */
	public static final String PAYMENT="Payment";
	
	/** The Constant Approve. */
	public static final String Approve="Approve";
	
	/** The Constant Unsucessful. */
	public static final String Unsucessful="Unsuccessful";			//Updated by: Viraj Date:13-02-2019 Description: Changed wrong spelling to correct
	
	/** The Constant Sucessful. */
	public static final String Sucessful="Sucessful";
	
	public static final String Successful="Successful";

	
	/** The Constant PROSPECT. */
	public static final String PROSPECT = "Prospect";
	
	/** The Constant CREATESALESORDER */
	public static final String CREATESALESORDER="Create Sales Order";
	
	/** This constant is used for renew contract button */
	
	public static final String CONTRACTRENEWAL="Contract Renew";
	
	public static final String WORKORDER = "Create Work Order";
	public static final String SERVICETYPE="Normal";
	public static final String SERVICETYPET="Ad Hoc";
	
	public final static String CUSTOMERINTEREST ="Do Not Renew";
	
	public final static String CUSTOMERVIEWBRANCHES ="View Branches";
	public final static String IPADDRESSPROCESSNAME="IPAddressAuthorization";
	
	/**
	 *  These constants are used for cancellation of documents;
	 */
	
	public static final String CANCELPAYMENT="Cancel Payment";
	public static final String CANCLEDELIVERYNOTE="Cancel Delivery Note";
	public static final String CANCLESALESORDER="Cancel Sales Order";
	public static final String CANCLECONTRACT="Cancel Contract";
	public static final String ORDERTYPEFORSALES="Sales Order";
	public static final String ORDERTYPEFORSERVICE="Service Order";
	
	public static final String CANCELSERVICES="Cancel Services";
	
	public static final String CANCELLATIONSUMMARY="Cancellation Summary";
	public static final String CANCELMMN="Cancel MMN";
	public static final String CANCELMIN="Cancel MIN";
	public static final String CANCELMRN="Cancel MRN";
	public static final String CANCELGRN="Cancel GRN";
	public static final String CANCELPR="Cancel PR";
	public static final String CANCELRFQ="Cancel RFQ";
	public static final String CANCELLOI="Cancel LOI";
	public static final String CANCELPURCHASEORDER="Cancel Purchase order";
	public static final String ORDERTYPEFORPURCHASE="Purchase Order";
	
	/** The Constant PRINTJOBCARD. */
	public static final String PRINTJOBCARD = "Job Card";

	public static final String ADVANCECREATED = null;

	public static final Object ADVANCESUCESSFUL = null;

	public static final String LEAVEREPORT = "Leave DownLoad";

	public static final String SUBMIT = "Submit";
	
	

	public static final String MANAGEPROJECT = "Manage Customer Project";
	
	public final static String  QUICKEMPLOYEESCREEN ="Quick Employee";
	
	public final static String  TECHNICIANSCEDULINGLIST ="Technician Scheduling List";

	
	/**
	 *  These constants are used for Approvals Table For Identifying which type of Button Is Clicked
	 */
	
	public static final String APPROVALBTN = "APPROVAL";
	public static final String REJECTBTN = "REJECTED";
	
	
	/** Customer Payment and Billing Constants */
	
	
	public static final String CREATEPROFORMAINVOICE="Proforma Invoice";
	
	public static final String CREATETAXINVOICE="Tax Invoice";
	
	public static final String PROCEEDPAYMENT="Proceed To Payment";
	
	public static final String CANCELBILLING="Cancel Billing Document";
	
	public static final String CANCELINVOICE="Cancel Invoice";

	public static final String EMAIL = "Email";
	
	public static final String EMAILCTC="Email CTC";

	public static final String CREATE_OUTBOUND_DELIVERY = "Create Outbound Delivery";

	public static final String VIEW_BUSINESS_DOCUMENTS = "View Business Documents";
	
	/**
	 * These constants are used to identify which type of order is being processed either Sales OR Service.
	 * Used in Payment Process (Billing,Invoicing,Customer Payment) to identify order type of payment.
	 */
	
	public static final String ORDERTYPESALES = "Sales Order";
	public static final String ORDERTYPEPURCHASE = "Purchase Order";
	public static final String ORDERTYPESERVICE = "Service Order";
	/**
	 * These constants are used for identifying which type of account type it is for billing document.
	 * Eg: AP, AR 
	 */
	/**
	 * Date : 05-03-2017 BY Anil
	 */
	public static final String ORDERTYPESERVICEPO = "Service PO";
	
	public static final String BILLINGACCOUNTTYPEAP = "AP";
	public static final String BILLINGACCOUNTTYPEAR = "AR";
	
	public final static String MATERIALREQUIREDREPORT = "Material Report";
	
	/**
	 * These constants are used to identify whether lead and customer screen are loaded from which node.
	 */
	
	public static final String SCREENTYPESALESLEAD="SALESLEAD";
	public static final String SCREENTYPESALESCUST="SALESCUSTOMER";
	public static final String SCREENTYPESERVICELEAD="LEAD";
	public static final String SCREENTYPESERVICECUST="CUSTOMER";
	
	
	/**
	 * These constants are used for saving type of customer in customer entity. Either from sales or service
	 */
	
	public static final String SALESTYPECUST="Sales";
	public static final String SERVICETYPECUST="Service";
	
	
	/**
	 * These constants are used for differentiate between sales and service in payment process.
	 * Used in charges balance identification. ref. Taxes And Charges Entity.
	 */
	
	public static final String BILLINGSALESFLOW="SalesFlow";
	public static final String BILLINGSERVICEFLOW="ServiceFlow";
	public static final String BILLINGPURCHASEFLOW="PurchaseFlow";
	
	/**
	 * This constant is used in Delivery Note for marking it as complete on successful delivery of products.
	 */
	
	public static final String MARKCOMPLETED="Mark Completed";
	
	
	
	public final static String  REQUESTINSPECTION="Request Inspection";
	public final static String  INSPECTIONREQUESTED="Inspection Requested";
	public final static String  INSPECTIONAPPROVED="Inspection Approved";
	
	
	/**
	 *  This constant is used for validating sales module when we switch from one screen to another
	 *   to copy data i.e category, group etc
	 */
	
	public static final String SALESCOPYDATA="Sales";
	public static final String COPYDATA="Copy Data";
	public static final String LEADCOPYDATA="LEADCOPYDATA";
	public static final String SALESQUOTATIONCOPYDATA="SALESQUOTATIONCOPYDATA";
	public static final String QUOTATIONCOPYDATA="QUOTATIONCOPYDATA";
	public static final String SALESORDERCOPYDATA="SALESORDERCOPYDATA";
	public static final String CONTRACTCOPYDATA="CONTRACTCOPYDATA";
	public static final String COPYCATEGORYDATA="Category";
	public static final String COPYTYPEDATA="Type";
	public static final String COPYLEVELDATA="Level";
	public static final String COPYPRIORITYDATA="Priority";
	public static final String COPYGROUPDATA="Group";
	
	
	public static final String CONTRACTDONOTRENEW="Contract Flagged For Do Not Renew";
	
	/**
	 * These constants are used for taxes.
	 */
	
	public static final String SERVICETAX="Service Tax";
	public static final String VAT="VAT";
	public static final String CENTRALTAX="CST";
	
	/**
	 * This constant is used in Accounting Interface for synching data.
	 */
	
	public static final String SYNC="Sync";
	
	/**
	 * This constant is used in approval table for viewing selected document.
	 */
	
	public static final String APPROVALVIEWDOCUMENT="View Document";
	public static final String APPROVALPRINTDOCUMENT="Print Document";
	
	/**
	 * This constant is used for setting product category from Company Asset Entity for creating ItemProduct
	 */
	
	public static final String ASSETPRODUCTCATEGORY="ASSET";
	
	/**
	 * This constant is used for creating MIN Screen through Customer Project Form.
	 */
	
	public static final String ISSUEMATERIAL="Issue Material";
	public static final String RETURNMATERIAL="Return Material";
	
	/**
	 *  These constants are used for identifying which type of Person Composite is being loaded
	 */
	
	public static final String PERSONCOMPOSITECUSTOMER="Customer";
	public static final String PERSONCOMPOSITEVENDOR="Vendor";
	public static final String PRODUCTCOMPOSITEITEM="ItemProduct";
	public static final String PRODUCTCOMPOSITESERVICE="ServiceProduct";
	public static final String PRODUCTCOMPOSITESUPER="SuperProduct";
	
	
	/**
	 *  These constants are used for identifying which type of Product Composite is being loaded
	 */
	
	public static final String ITEMPRODUCTCOMPOSITE="ItemProduct";
	public static final String SERVICEPRODUCTCOMPOSITE="ServiceProduct";
	public static final String SUPERPRODUCTCOMPOSITE="SuperProduct";
	
	
	
	/**
	 *  These constants are used for ticket status.
	 */
	
	public static final String TICKETFIXED="Fixed";
	public static final String TICKETCLOSE="Close";
	
	public static final String MRN="MRN";
	public static final String MIN="MIN";
	public static final String MMN="MMN";
	
	public static final String CREATEMIN="Material Issue"; // added by priyanka for simplification
	public static final String CREATEMMN="Create MMN";
	
	
	/**
	 *  These constants are used for inventory operations.
	 *  Used for updating stock , whether stock is added or subtracted.
	 */
	
	public static final String ADD="Add";
	public static final String SUBTRACT="Subtract";
	
	
	public static final String ACCOUNTTYPE="AR";
	public static final String PERSONRESPONSIBLE = "Sales Person";
	public static final String STATUS = "Created";
	public static final String DOCUMENTGL = "Purchase Order";
	public static final String LOIREFERENCETYPE = "LOI";
	public static final String PRREFERENCETYPE = "PR";
	public static final String RFQREFERENCETYPE = "RFQ";
	public static final String DOCUMENTTYPE="Purchase Order";
	
	/**
	 * These constants are used for session timeout and validation for login
	 */
	
	public static final String LOGINNOOFUSER = "UserValidate";
	public static final String LOGINEXPIRYDATE = "ExpiryDate";
	public static final String LOGININVALID= "Invalid";
	public static final String LOGINSUCCESS= "Login Success";
	public static final String LOGINTIMEOUT= "Session Timeout";
	public static final String LOGINCURRENTUSER= "User Logging In Again";
	public static final String SUCCESSFULLOGOUT= "Successful Logout";
	
	/**
	 * These constants are used for accounting interface functionality to be enabled or not.
	 * These are used as filter while accounting interface is created in the approval process of 
	 * business process.
	 */
	
	public static final String PROCESSCONFIGSO = "SalesOrder";
	public static final String PROCESSCONFIGPO = "PurchaseOrder";
	public static final String PROCESSCONFIGCO = "Contract";
	public static final String PROCESSCONFIGQUO = "Quotation";
	public static final String PROCESSCONFIGEXP = "Expense";
	public static final String PROCESSCONFIGINV = "Invoice";
	public static final String PROCESSCONFIGCP = "CustomerPayment";
	public static final String PROCESSNAMEACCINTERFACE = "AccountingInterface";
	public static final String PROCESSTYPEACCINTERFACE = "Accounting Interface";
	public static final String PROCESSTYPEQUOTATIONSERVDATA = "Quotation Service Data";
	public static final String PROCESSTYPECONTRACTSERVDATA = "Contract Service Data";
	public static final String PROCESSCONFIGGRN="GRN";
	public static final String PROCESSCONFIGMRN="MRN";
	public static final String PROCESSCONFIGMIN="MIN";
	public static final String PROCESSCONFIGMMN="MMN";
	
	public static final String REFDOCSO="SalesOrder.Ref.Doc";
	public static final String REFDOCCONTRACT="Contract";
	public static final String REFDOCPO="Purchase Order";
	public static final String REFGLACCOUNTSO="AR-Sales Order";
	public static final String REFGLACCOUNTPO="PO.Ref.Doc";
	public static final String REFDOCWO="Work Order";
	public static final String REFDOCMRN="MRN";
	public static final String REFDOCMIN="MIN";
	
	
	/**
	 * These constants are used for identifying which type of approval is selected to view the document.
	 */
	
	public static final String APPROVALCONTRACT ="Contract";
	public static final String APPROVALDELIVERYNOTE ="Delivery Note";
	public static final String APPROVALQUOTATION="Quotation";
	public static final String APPROVALEXPENSEMANAGMENT="Expense Management";
	public static final String APPROVALCOMPLAIN="Complaint";
	public static final String APPROVALLEAVEAPPLICATION="Leave Application";
	public static final String APPROVALADVANCE="Advance";
	public static final String APPROVALLETTEROFINTENT="Letter Of Intent";
	public static final String APPROVALBILLINGDETAILS="Billing Details";
	public static final String APPROVALINVOICEDETAILS="Invoice Details";
	public static final String APPROVALREQUESTFORQUOTATION="Request For Quotation";
	public static final String APPROVALSALESQUOTATION="Sales Quotation";
	public static final String APPROVALPURCHASEORDER="Purchase Order";
	public static final String APPROVALPURCHASEREQUISITION="Purchase Requisition";
	public static final String APPROVALSALESORDER="Sales Order";
	public static final String APPROVALGRN="GRN";
	public static final String APPROVALMATERIALREQUESTNOTE="MRN";
	public static final String APPROVALMATERIALISSUENOTE="MIN";
	public static final String APPROVALMATERIALMOVEMENTNOTE="MMN";
	public static final String APPROVALINSPECTION="Inspection";
	public static final String APPROVALWORKORDER="Work Order";
	public static final String APPROVALSERVICEPO="Service PO";
	
	// rohan added this for using multiple expense in approval
	public static final String APPROVALMULTIPLEEXPENSEMANAGMENT="Expense";
	/**
	 * Date : 17-06-2017 BY ANIL
	 */
	public static final String APPROVALCUSTOMER="Customer";
	/**
	 * End
	 */
	
	/**
	 * This constant is used as default value for Cform NO.
	 * Used for GLAccount of Cform.
	 */
	
	public static final String FILTERFORCST = "CST NO";
	
	public static final String ACCOUNTTYPEAP="AP";
	
	public static final String REFERENCEDOCUMENTTYPE = "Contract.Ref.Doc";
	
	
	/**
	 *  These constants are used for identifying which type of drop down is being loaded.
	 */
	
	public static final String GLOBALRETRIEVALCONFIG="Config";
	public static final String GLOBALRETRIEVALCATEGORY="Category";
	public static final String GLOBALRETRIEVALTYPE="Type";
	public static final String GLOBALRETRIEVALEMPLOYEE="Employee";
	public static final String GLOBALRETRIEVALBRANCH="Branch";
	public static final String GLOBALRETRIEVALCOUNTRY="Country";
	public static final String GLOBALRETRIEVALSTATE="State";
	public static final String GLOBALRETRIEVALCITY="City";
	public static final String GLOBALRETRIEVALLOCALITY="Locality";
	public static final String GLOBALRETRIEVALPROCESSCONFIG="ProcessConfiguration";
	public static final String GLOBALRETRIEVALPROCESSNAME="ProcessName";
	public static final String GLOBALRETRIEVALMULTILEVELAPPROVAL="MultilevelApproval";
	public static final String GLOBALRETRIEVALROLEDEFINITION="RoleDefinition";
	/** Date 31-05-2018 BY ANIL**/public static final String GLOBALRETRIEVALOVERTIME="Overtime";
	/**
	 *  These constants represents the module names
	 */
	
	public final static String  SALESMODULE ="Sales";
	public final static String  SALESCONFIG ="Sales Configuration";
	public final static String  SERVICEMODULE="Service";
	public final static String  SERVICECONFIG="Service Configuration";
	public final static String  PURCHASEMODULE="Purchase";
	public final static String  PURCHASECONFIG="Purchase Configuration";	
	public final static String  INVENTORYMODULE="Inventory";
	public final static String  INVENTORYCONFIG="Inventory Configuration";
	public final static String  HRMODULE="HR Admin";
	public final static String  HRCONFIG="HR Configuration";
	public final static String  HRSTAUTORYDEDUCTION="Statutory Deduction";
	public final static String  EMPSELFSERVICEMODULE="Employee Self Service";
	public final static String  PRODUCTMODULE="Product";
	public final static String  PRODUCTCONFIG="Product Configuration";
	public final static String  ASSETMANEGEMENTMODULE="Asset Management";
	public final static String  ASSETMANAGEMENTCONFIG="Asset Management Configuration";
	public final static String  ACCOUNTMODULE="Accounts";
	public final static String  ACCOUNTCONFIG="Account Configuration";
	public final static String  SETTINGSMODULE="Settings";
	public final static String  APPROVALMODULE="Approval";
	public final static String  SUPPORTMODULE="Support";
	public final static String  SUPPORTCONFIG="Support Configuration";
	
	public final static String VERSIONMANAGEMENTCONFIG="Version Management";
	
	public final static String  REGISTRATIONMODULE="Registration";
	/** date 03.03.2018 added by komal for nbhc new report module **/
	public final static String REPORTMODEULE="Reports";
	
	

	/***
	 * These constants represents the document name
	 */
	
	// ***********************  SERVICE 
	
	public final static String  CUSTOMER ="Customer";
	public final static String  CUSTOMERHELPDESK ="Customer Help Desk";
	public final static String  CAMPAIGNMANAGEMENT ="Campaign Management";
	public final static String  CUSTOMERBRANCH ="Customer Branch";
	
	public final static String  LEAD ="Lead";
	public final static String  QUOTATION ="Quotation";
	public final static String  CONTRACT ="Contract";
	public final static String  CUSTOMERSERVICELIST ="Customer Service List";
	public final static String  CUSTOMERSERVICE ="Customer Service";
	public final static String  CUSTOMERPROJECT ="Customer Project";
	
	/****Date :20/11/2017 
    BY :Manisha
    Description :Incase of inspection quotation there should be some schedules for visiting person,
       create single service for assessment single service.**/

     public final static String AssessmentReport="Assessment Report";
    /**ends for Manisha**/
	
	//   rohan added this for quick contract navigation 
	public final static String  QUICKCONTRACTSCREEN ="Quick Contract";
	
	public final static String  FUMIGATION ="Fumigation";
	public final static String  FUMIGATIONAUS ="Fumigation(Aus)";
	
	public final static String  LIASIONSTEPS ="Liasion Steps";
	public final static String  LIASIONGROUP ="Liasion Group";
	public final static String  LIASION ="Liasion";
	
	public final static String  CUSTOMERSUPPORT ="Customer Support";
	public final static String  CONTRACTRENEWALL ="Contract Renewal";
	public final static String  SCHEDULINGANDROUTING ="Scheduling And Routing";
	public final static String  TEAMMANAGEMENT ="Team Management";
	
	public final static String  INTERACTIONTODOLIST ="Interaction To Do List";
	public final static String  INTERACTIONTODO ="Interaction To Do";
	public final static String  CONTACTPERSONLIST ="Contact Person List";
	public final static String  CONTACTPERSONDETAILS ="Contact Person Details";
	public final static String  SECURITYDEPOSITELIST ="Security Deposite List";
	
	public final static String  SALESPERSONTARGET ="Sales Person Target";
	// ***********************  SAlES 
	
	
	public final static String  SALESORDER ="Sales Order";
	public final static String  DELIVERYNOTE ="Delivery Note";
	public final static String  WORK_ORDER ="Work Order";
	
	
	// ***********************  PURCHASE 
	
	public final static String  VENDOR ="Vendor";
	public final static String  PURCHASEREQUISION ="Purchase Requisition";
	public final static String  RFQ ="RFQ";
	public final static String  LETTEROFINTENT ="Letter Of Intent(LOI)";
	public final static String  PURCHASEORDER ="Purchase Order";
	public final static String  VENDORPRODUCTPRICELIST ="Vendor Product Pricelist";
	public final static String  VENDORPRODUCTPRICE ="Vendor Product Price";
	
	public final static String  CREATESINGLEPR ="Create Single PR";
	public final static String  SERVICEPO ="Service PO";
	
	
	
	// ***********************  INVENTORY 
	
		public final static String  INVENTORYTRANSACTIONLIST ="Inventory Transaction List";
		public final static String  PRODUCTINVENTORYVIEW ="Product Inventory View";
		public final static String  PRODUCTINVENTORYLIST ="Product Inventory List";
		public final static String  STOCKALERT ="Stock Alert";
		public final static String  GRNLIST ="GRN List";
		public final static String  GRN ="GRN";
		public final static String  INSPECTIONLIST ="Inspection List";
		public final static String  INSPECTION ="Inspection";
		public final static String  MATERIALREQUESTNOTE ="Material Request Note";
		public final static String  MATERIALISSUENOTE ="Material Issue Note";
		public final static String  MATERIALMOVEMENTNOTE ="Material Movement Note";
		public final static String  PHYSICALINVENTORY ="Physical Inventory";
		public final static String  MATERIALCONSUMPTIONREPORT ="Material Consumption Report";
		
	
		
		// ***********************  HR ADMIN 
		
		public final static String  EMPLOYEE ="Employee";
		public final static String  EMPLOYEEADDITIONALDETAILS ="Employee Additional Details";
		public final static String  LEAVEAPPLICATION ="Leave Application";
		public final static String  ADVANCEREQUEST ="Advance Request";
		public final static String  UPLOADATTENDANCE ="Upload Attendance";
		public final static String  EMPLOYEEATTENDANCE ="Employee Attendance";
		public final static String  TIMEREPORT ="Time Report";
		public final static String  CTC ="CTC";
		public final static String  LEAVEBALANCE ="Leave Balance";
		public final static String  PROCESSPAYROLL ="Process Payroll";
		public final static String  SALARYSLIP ="Salary Slip";
		public final static String  COMPANYSCONTRIBUTION ="Comapny's Contribution";
		/**
		 * Date : 30-04-2018 BY  ANIL/ Sasha ERP
		 */
		public final static String  CTCTEMPLATE ="CTC Template";
		

		// ***********************  PRODUCT MODULE 
		
		public final static String  SERVICEPRODUCT ="Service Product";
		public final static String  ITEMPRODUCT ="Item Product";
		public final static String  PRODUCTGROUPLIST ="Product Group List";
		public final static String  PRODUCTGROUPDETAILS ="Product Group Details";
		public final static String  BILLOFMATERIAL ="Bill Of Material";
		
		
		// ***********************  ASSET MANAGEMENT 
		
		public final static String  COMPANYASSET ="Company Asset";
		public final static String  CLIENTASSET ="Client Asset";
		public final static String  TOOLSET ="Tool Set";
		public final static String  ASSETARTICLETYPE="Asset Article Type";
		
		
		// ***********************  ACCOUNT
		
		public final static String  MULTIPLEEXPENSEMANAGEMET ="Expense";
		public final static String  EXPENSEMANAGEMET ="Expense Management";
		public final static String  PETTYCASHTRANSACTION ="Petty Cash Transaction";
		public final static String  BILLINGDETAILS ="Billing Details";
		public final static String  INVOICEDETAILS ="Invoice Details";
		public final static String  PAYMENTDETAILS ="Payment Details";
		/*
		 * Added by Rahul in Accounting Interface
		 */
		public final static String  INTEGRATESYNC ="Integrate Sync";
	
		
		// ***********************  SETTING  
		
		public final static String  BRANCH ="Branch";
		public final static String  USER ="User";
		public final static String  CUSTOMERUSER ="Customer User";
		public final static String  NUMBERGENERATION="Number Generation";
		public final static String  SMSTEMPLATE="SMS Template";
		public final static String  SMSCONFIGURATION="SMS Configguration";
		public final static String  MULTILEVELAPPROVAL="Multilevel Approval";
		public final static String  IPADDRESSAUTHPRIZATION="IP Address Authorization";
		public final static String  USERAUTHORIZATION="User Authorization";
		
		public final static String  ROLEDEFINITION="Role Definition";
		
		public final static String  LICENSEINFO ="License Info";
		public final static String  CURRENCY ="Currency";
		public final static String  COUNTRY ="Country";
		public final static String  STATE ="State";
		public final static String  CITY ="City";
		public final static String  LOCALITY ="Locality";
		
		
	
	/****************************************ADMIN USER*****************************************/
	
	public final static String  ADMINUSER ="ADMIN";

	public static final String APPROVALVIEWMATERIAL = "View Material";

	public static final String NAVIGATION = "Navigation";
	
	/** This constant is used for Communication log added by vijay on 13 April 2017*/
//	public static final String COMMUNICATIONLOG = "Communication Log";
	public static final String COMMUNICATIONLOG = "Follow Up";


	/** This constant is used for user wise IP Restriction button */
	public final static String USERWISEIPADDRESSPROCESSNAME="UserWiseIPAddressAuthorization";

	
	//    used for quick contract 
	public static final String QUICKCONTRACT = "Quick Contract";
	public static final String QUICKPURCHASEORDER = "Quick Purchase Order";


	public static final String  FINANCIALCALENDAR = "financial Calendar";
	public static final String CREATESERVICE = "Create Service Billing";
	
	//Added by Rahul on 15-10-2016 for NBHC(This is used to update prices of product if it has been approved).
	public static final String UPDATEPRICES = "Update Prices";
	
	/*******************URL Constants***********************************************/
	
	/**************************for UAT ********************************************************/
//	public static final String NBHCSALESORDERINTEGRATE = "http://220.227.236.217/SAPPUSH/api/v1/sap/sales";
//	public static final String NBHCINVOICEINTEGRATE = "http://220.227.236.217/SAPPUSH/api/v1/sap/invoice";
//	public static final String NBHCACKNNOWLEDGEMENTINTEGRATE = "http://220.227.236.217/SAPPUSH/api/v1/sap/GetInvoices";
	
	/**
	 * Date 13/06/2018 by Vijay
	 * Des :- as per communication with ajinkya(NBHC)  he said he changed ip address of url
	 * so bleow old code i have commented here and new updated ip address url added below
	 * 
	 */
	
//	public static final String NBHCPOINBOUNDINTEGRATE = "http://220.227.236.217/SAPPUSH/api/v1/sap/GetPOs";
//	public static final String NBHCGRNOUTBOUNDINTEGRATE = "http://220.227.236.217/SAPPUSH/api/v1/sap/grn";
//	public static final String NBHCMMNOUTBOUNDINTEGRATE = "http://220.227.236.217/SAPPUSH/api/v1/sap/MMN";
//	public static final String NBHCACKNNOWLEDGEMENTGENINTEGRATE = "http://220.227.236.217/SAPPUSH/api/v1/sap/GetAck";
	
	public static final String NBHCPOINBOUNDINTEGRATE = "http://103.69.91.120/SAPPUSH/api/v1/sap/GetPOs";
	public static final String NBHCGRNOUTBOUNDINTEGRATE = "http://103.69.91.120/SAPPUSH/api/v1/sap/grn";
	public static final String NBHCMMNOUTBOUNDINTEGRATE = "http://103.69.91.120/SAPPUSH/api/v1/sap/MMN";
	public static final String NBHCACKNNOWLEDGEMENTGENINTEGRATE = "http://103.69.91.120/SAPPUSH/api/v1/sap/GetAck";
	public static final String NBHCGRNCANCELLATION = "http://103.69.91.120/SAPPUSH/api/v1/sap/Cancel";

	/*** Date 07-06-2019 by Vijay for MIN EVA to SAP  ******/
	public static final String NBHCMINOUTBOUNDINTEGRATE = "http://103.69.91.120/SAPPUSH/api/v1/sap/MIN";
	
	/**
	 * ends here
	 */
	/*******************for production **************************************************************/
	public static final String NBHCSALESORDERINTEGRATE = "";
	public static final String NBHCINVOICEINTEGRATE = "http://123.108.47.218/SAPPUSH/api/v1/sap/invoice";
	public static final String NBHCACKNNOWLEDGEMENTINTEGRATE = "http://123.108.47.218/SAPPUSH/api/v1/sap/GetInvoices";
//	public static final String NBHCGRNCANCELLATION = "http://220.227.236.217/SAPPUSH/api/v1/sap/Cancel";
	/**********************************************Timer Constants****************************************/
	
	/**
	 * Added by Rahul Verma
	 * Constants Source System and Destination System 
	 */
	public static final String NBHC="NBHC";
	public static final String CCPM="CCPM";
	
	
	/**
	 * Date 24 july 2017 added by vijay for GENRAL POPUp
	 */
	public static final String CREATED ="Created";
	public static final String APPROVED ="Approved";
	public static final String REQUESTED ="Requested";
	public static final String CANCELLED ="Cancelled";
	public static final String CLOSED ="Closed";
	public static final String REJECTED ="Rejected";
	public static final String INPROCESSE="In Process";
	public static final String PROFORMAINVOICE = "PInvoice";
	public static final String PAYMENTCLOSED = "Closed";
	public static final String BILLINGINVOICED = "Invoiced";
	
	public static final String CREATEAMC ="Create AMC";
	public static final String VIEWINVOICEPAYMENT ="Invoice And Payment";
	//Date 14 july 2017 added by vijay 
	public final static String  QUICKSALESORDER ="Quick Sales Order";
	
	
	
	/**
	 * Date : NIDHI
	 */
	public static final String APPROVALCAMPAIGNMANNAGEMENT="Campaign Management";
	public static final String MATERIALSATUS="Available Stock Details";
	public static final String APPROVALTIMEREPORT= "Time Report";
	/**
	 * Date : 29-07-2017 By ANIL
	 * 
	 */
	public static final String VIEWBILL="View Bill";
	public static final String VIEWINVOICE="View Invoice";
	public static final String VIEWPAYMENT="View Payment";
	public static final String CREATEINVOICE="Create Invoice";
	public static final String VIEWORDER="View Order";
	
	/**Date  23-1-2019 added by amol for View GRN**/
	public static final String VIEWGRN="View GRN";
	/**
	 * End
	 */
	
	
	/**
	 * Date 22/2/2018
	 * By Jayshree
	 * Des.Add new Button View tax Invoice
	 */
	public static final String VIEWTAXINVOICE="View Tax Invoice";
	
	/**
	 * End
	 */
	/** Date 05-08-2017 added by vijay for Return Expense **/
	public static final String RETURNEXPENSE="Return Expense";
	public static final String viewReturnAmtHistory="Return Amount History";

	/** Date 12-09-2017 added by vijay for copy contract **/
	public static final String COPYCONTRACT = "Copy Contract";
	/** Date 13-09-2017 added by vijay for customer Address in contract **/
	public static final String CUSTOMERADDRESS = "Customer Address";
	/**24-10-2017 sagar sore To add sales register download in invoice list**/
	public static final String SALESREGISTER = "Sales Register";
	
	/**16-06-2021 Priyanka Bhagwat To crm tally report download in invoice **/
	public static final String CRMTALLYREPORT = "CRM Tally Report";
	/**
	 * Date : 31-10-2017 BY ANIL
	 * Adding consatnt 
	 */
	/** The Constant VIEWSERVICES. */
	public static final String VIEWSERVICE="View Service";
	/**
	 * End
	 */
	/** Date 14-10-2017 
	 * by jayshree 
	 * for Service Card in Contract
	 */
	public static final String SERVICECARD = "Service Card";
	
	/** Date : 06-11-2017 By ANIL **/
	public final static String UPLOADTERMSANDCONDITION="Upload T&C";
	
	/**
	 *  nidhi
	 * 10-11-2017
	 * for daily fumigation report
	 */
	public final static String SERVICEFUMIGATIONREPORT = "Commodity Fumigation Report";
	/**
	 * end
	 */
	
	/**
	 *  nidhi
	 * 16-11-2017
	 * for daily fumigation value report
	 */
	public final static String SERVICEFUMIGATIONVALUEREPORT = "Service Revenue Report";
	/**
	 * end
	 */
	/**
	 *  nidhi
	 *  27-10-2017 MIN Upload PRocess
	 *  set contant name for 
	 *  if any chnage here do as wel as in  savedTransactionDetails method in DataMigrationImpl
	 */
	public static final String MINUPLOAD = "MIN Upload";
	/**
	 *  end
	 */
	/**
	 *  nidhi
	 *  2-11-2017 Contract Upload  PRocess
	 *  set contant name for 
	 *  if any chnage here do as wel as in  savedTransactionDetails method in DataMigrationImpl
	 */
	public static final String CONTRACTUPLOAD = "Contract Upload";
	/**
	 *  end
	 */
	/**
	 *  nidhi
	 * 20-11-2017
	 * for NBHC send email introductory info to Customer
	 */
	public final static String LEADCUSTOMERINTRODUCTORY = "Email to customer";
	/**
	 * end
	 */
	
	/**
	 * nidhi
	 * 22-12-2017
	 * 
	 */
	public final static String CRONJOB ="Cron Job";
	public final static String CRONJOBCONFIGRATION = "Cron Job Reminder Setting";
	public final static String FREQUENCY = "Frequency";
	public final static String CRONJOBCONFIG = "Cron Job configration";
	/**
	 *  end
	 */
	
	/**
	 * Date 15-01-2018 by Vijay
	 * work order process bar button constant
	 */
	public static final String PRINTDELIVERYNOTE = "PrintDeliveryNote";
	
	
	public final static String  VERSIONDATE ="DATE:- 23-10-2023";
	public final static String  VERSION = "VERSION:- 18.00";
	
	
	/**
	 * Date 10/3/2018 
	 * By Jayshree
	 * Des.add the suspend button for customer service screen
	 */
	public static final String SUSPEND = "Suspend";
	public static final String SUSPENDSERVICES = "Suspend";  // added by priyanka for simplification
	public static final String RESUMESERVICES = "Resume";    // added by priyanka for simplification
	//End By Jayshree
	/** date 18/1/2018 added by komal for hvac **/
	public static final String MATERIALPENDING = "Material Pending";
	public static final String CREDITRECEIVED = "Credit Received";
	public static final String SENTTOCOMPANY = "Sent To Company";
	public static final String MATERIALRECEIVED = "Material Received";
	/** 
	 * end
	 */
	/** date 06.04.2018 added by komal for fumigation **/
	public final static String FUMIGATIONMB = "Fumigation MB";
	public final static String FUMIGTAIONALP = "Fumigation ALP";
	public final static String FUMIGATIONAFAS = "Fumigation AFAS";

	/**
	 * Date 04-05-2018 by vijay
	 * for service list service completion
	 */
	public static final String MARKCOMPLETE= "Complete"; // added by priyanka for simplification
	/**
	 * ends here
	 */

	/**
	 * Date 22-5-2018 by jayshree add the service voucher
	 */
	public static final String SERVICEVOUCHER= "Service Voucher";
	public static final String UPDATESERVICES = "Update Services";
	/** 28/3/2018 added by komal for new tally interface **/
	public final static String DISCOUNT = "Discount";
	
	public final static String DELIVERYCHARGES  = "Delivery Charges";
	
	public final static String INDIRECTEXPENSE = "Indirect Expenses";
	public final static String DIRECTEXPENSE = "Direct Expenses";
	public final static String TDSFEES = "TDS on Professional Fees";
	public final static String INDIRECTINCOMES = "Indirect Incomes";
	public final static String JOURNAL = "Journal";
	public final static String PAYMENT1 = "Payment";
	/** date 3/4/18 added by komal  for multiple expense management process configuration **/
	public final static  String MULTIPLEXPMANAGEMENT = "MultipleExpenseMngt";
	/**
	 * ends here
	 */
	/**
	 * nidhi
	 * 18-05-2018
	 * for 
	 */
	public static final String CONTRACTDISCOUNTINUED= "Contract Discontinue";
	public static final String IGSTTAX = "IGST" , SGSTTAX = "SGST" , CGST = "CGST";

	public static final String UPDATEQUICCONTRACTSERVICES = "Update QuickContract/Services";
	public static final String  UPDATEQUICLCONTRACTBILLING= "Update QuickContract/Billing";
	public static final String  CANCELPARTIALLYCANCELLEDSERVICES= "Cancel Partially Cancelled Services";
	
	/** date 28.07.2018 added by komal for accounting interface voucher name column **/
	public static final String SALESINVOICE = "Sales Invoice";
	public static final String RECEIPT = "Receipt";
	/**
	 * nidhi
	 * 2-08-2018
	 * for technician assign
	 */
	public static final String ASSIGNTECHNICIAN  = "Assign Technician";
	
	/**
	 * Date 07-09-2018 By Vijay
	 * Des :- for multiple services reschedule
	 */
	public static final String Reschedule = "Reschedule";
	/**
	 * ends here
	 */
	
	/** date 6.7.2018 added by komal for sasha **/
	public final static String ABSENT = "Absent";
	public final static String COMPOFF = "Comp-Off";
	public final static String HALFDAY = "Half-Day";
	public final static String WEEKLYOFF = "Weekly-Off";
	public final static String HOLIDAY = "Holiday";
	public final static String LEAVE = "Leave";
	public final static String PRESENT = "Present";
	public final static String A = "A";
	public final static String CO = "CO";
	public final static String HF = "HF";
	public final static String WO = "WO";
	public final static String H = "H";
	public final static String L = "L";
	public final static String P = "P";
	
	/** date 27.6.2018 added by komal vendor invoice and vendor payment**/
	public static final String PROCESSCONFIGINVVENDOR = "VendorInvoice";
	/**
	 * Updated By: Viraj
	 * Date: 12-02-2019
	 * Description: For vendor invoice details
	 */
	public static final String VENDORINVOICEDETAILS= "Vendor Invoice Details";
	/** Ends **/
	public static final String PROCESSCONFIGCPVENDOR = "VendorCustomerPayment";
	/** date 08.08.18 added by komal for cost sheet download **/
	public static final String COSTSHHETDOWNLOAD = "COST Sheet";
	/** date 24.7.2018 added by komal for employee full and final **/
	public static final String FULLANDFINAL = "Employee F&F";
	/** date 21.7.2018 added by komal for tds**/
	public final static String TDS = "TDS";
	public final static String EMPLOYEECHECKLISTTYPE = "Employee CheckList Type";
	public final static String DEDUCT = "DEDUCT";
	public final static String SHORTFALLAMOUNT = "Notice Period Deduction";
	public final static String ONETIMEDEDUCTION = "One Time Deduction";
	public final static String SEARCHVERSION = "Search Version";
	/** date 30.8.2018 added by komal for cnc bill**/
	public final static String GENERATEBILL = "Generate Bill";
	public final static String CTCCATEGORY = "CTC Category";
	public final static String CNC = "CNC";
	/** date 5.9.2018 added by komal**/
	public final static String STAFFINGANDRENTAL = "Staffing And Rental";
	public final static String OTHERSERVICES = "Other Services";
	public final static String CONSUMABLES = "Consumables";
	
	public final static String NATIONALHOLIDAY = "National Holiday";
	public final static String OVERTIME = "Overtime";
	
	public final static String PUBLICHOLIDAY = "Public Holiday";
	
	/*******31-1-2019 added by amol for Payroll Sheet in CNC******/
	public final static String PAYROLLSHEETDOWNLOAD="Payroll Sheet";
	public final static String GROSSPROFITSHEETDOWNLOAD="GP Sheet";
	
	/**
	 * nidhi 
	 *  *:*:*
	 * 	 7-09-2018* 
	 */
	public static final String UNITCONVERSION = "Unit Conversion";
	/**
	 * nidhi
	 * 1-10-2018
	 *  *:*:*
	 */
	public static final String BOMREPORT = "BOM Service Material Report";
	/**
	 * nidhi 30-10-2018
	 */
	public static final String SERVICEINVOICEMAPPING = "Service Invoice Mapping Report";
	
	/** date 1.11.2018 added by komal to view cnc **/
	public static final String VIEWCNC = "View CNC";
	/** Date 3.11.2018 added by komal to create accounting interface **/
	public static final String CREATEACCOUNTINGINTERFACE = "Create Accounting Interface";

	public final static String FIXEDCONSUMABLES = "Fixed Consumables";
	/**
	 * nidhi
	 * 20-2-2018
	 */
	public final static String RENEWCONTRACTREPORT = "Renew Contract Report";
	public static final void callTimerLarge()
	{
		 Timer timer=new Timer() 
    	 {
				@Override
				public void run() {
					System.out.println("Timer Started Large");
			}
		};
      timer.schedule(2500); 
	}
	
	public static final void callTimerMedium()
	{
		 Timer timer=new Timer() 
    	 {
				@Override
				public void run() {
					
					System.out.println("Timer Started Medium");
			}
		};
      timer.schedule(1600); 
	}
	
	public static final void callTimerSmall()
	{
		 Timer timer=new Timer() 
    	 {
				@Override
				public void run() {
					
					System.out.println("Timer Started Small");
			}
		};
      timer.schedule(800); 
	}
	
	public static final void callTimerMicro()
	{
		 Timer timer=new Timer() 
    	 {
				@Override
				public void run() {
					System.out.println("Timer Started Micro");
			}
		};
      timer.schedule(1000); 
	}
	

	/**
	 * Date : 12-09-2018 BY ANIL
	 */
	public static final String APPROVALEMPLOYEE="Employee";
	/**
	 * End
	 */
	 	
	/**
	 * Date : 19-10-2018 BY ANIL
	 */
	public static final String PROMOTION="Promotion";
	/**
	 * End
	 */
	
	/** date 29-10-2018  added by Anil  **/
	public static final String EMPLOYMENTCARD = "Employment Card";
	

	/** date 01-12-2018  added by Anil  **/
	public static final String FORM3A = "Form 3A";

	/** Date 28-11-2018 by Vijay ***/
	public static final String VIEWCONTRACT = "Contract";  // added by priyanka for simplification
	
	/** Date 30.11.2018 added by komal **/
	public final static String STAFFING = "Staffing";
	public final static String EQUIPMENTRENTAL = "Equipment Rental";
	public final static String CONSOLIDATEBILL = "Consolidate Bill";
	/** Date 13.12.2018 added by komal to edit accounting interface**/
	public final static String UPDATEACCOUNTINGINTERFACE = "Update Accounting Interface";

	/**** Date 06-02-2019 By Vijay for MRN View MMN Button ***********/
	public final static String VIEWMMN = "View MMN";
	
	/** Added by Viraj Date:19-03-2019 for billed unbilled(AR) csv download **/
	public static final String DOWNLOAD = "Download";

	/**** Date 10-04-2019 by Vijay for NBHC CCPM *******/ 
	public static final String SUBMITINVOICE = "Submit Invoice";

	/** date 1.2.2019 added by komal for unsuccessful stuas **/
	public final static String UNSUCCESSFUL = "Unsuccessful";


	/**** Date 20-04-2019 by Vijay for NBHC CCPM *******/ 
	public static final String CHANGESTATUSTOCREATED = "Change Status To Created";

	public static final String SCHEDULEMULTIPLESERVICES = "Schedule Services"; // added by priyanka for simplification
	/**** Date 18-06-2019 By Vijay For NBHC Inventory management *****/
	public static final String GLOBALRETRIEVALWAREHOUSE = "Warehouse";
	
	/** Date:15-06-2019 Description: added for assign multiple branches **/
	public static final String ASSIGNBRANCH = "Assign Branch";
	
	/** Date:29-06-2019 Description: added for assign multiple status **/
	public static final String ASSIGNSTATUS = "Uncancel";  // added by priyanka for simplification
	
	public static final String CREATEOVERTIME="Create Overtime";
	
	public static final String SENDSRMAIL = "Email SR";  // added by priyanka for simplification
	//	public static final String SCHEDULEMULTIPLESERVICES = "Schedule Services";
	public static final String ARREARSBILL = "Arrears Bill";
	public static final String SERVICERECORD = "Service Record";
	
	/**** Date 19-07-2020 By Vijay For HRProject Loading Globally *****/
	public static final String GLOBALHRPROJECT = "HRProject";
	
	/** Date 03-09-2020 by Vijay for Process Configuration **/
	public static final String AUTOMATICSCHEDULING = "AutoMaticScheduling"; 
	/** Date 07-09-2020 by Vijay for Payment Gateway Sign Up **/
	public static final String SIGNUP = "Sign Up"; 
	/** Date 07-09-2020 by Vijay for Payment Gateway URL **/
//	public static final String PAYMENETGATEWAYAPI = "https://us-east1-oneclickventure.cloudfunctions.net/razorPay/inbenv"; 
//	public static final String PAYMENETGATEWAYAPI = "http://139.59.16.130:4000/pgs/erp-inbound"; 
	public static final String PAYMENETGATEWAYAPI = "https://onlinepayment.evasoftwaresolutions.com/pgs/erp-inbound";


	public static final String PC_ADDSERVICEDURATIONCOLUMN = "PC_AddServiceDurationColumn"; 
	public static final String PC_BranchLevelRestrictionForCustomer = "PC_BranchLevelRestrictionForCustomer"; 
	public static final String BILLINGDOCUMENT = "BillingDocument"; 
	public static final String PC_PICKkNOOFStAFFFROMCNC = "PickNoOfStaffFromCNC"; 
	public static final String PC_FREEMATERIAL = "PC_FreeMaterial";
	public static final String UPDATEASSETDETAILS = "Update Asset Details";
	public static final String PC_HRPROJECTOVERTIMENONMANDATORY = "PC_HRProjectOvertimeNonMandatory";
	public static final String PAYMENTUPLOAD = "Payment Upload";
	public static final String PC_ServiceCertificate = "PC_ServiceCertificate";
	public static final String PC_LOADCUSTOMERWITHBRANCHLEVELRESTRICTION = "PC_LoadCustomerWithBranchLevelRestriction";
	public static final String INHOUSESERVICE = "InHouseService";
	public static final String MAKERENEWABLE = "Make Renewable";
	public static final String PC_DIFFERENTNUMBERSERIESFORSALESANDSERVICE = "PC_DifferentNumberSeriesForSalesAndSerivce";
	public static final String PC_DISABLEINVOICECATEGORY = "PC_DisableInvoiceCategory";
	/** Date 28-10-2020 by Vijay for Payment Gateway link for SMS to customer **/
//	public static final String PAYMENTGATEWAYSMSLINK = "https://us-east1-oneclickventure.cloudfunctions.net/razorPay/razorPayment/callRazorPayGateway/";
//	public static final String PAYMENTGATEWAYSMSLINK = "http://139.59.16.130:4000/pgs/cc/uid";
//	public static final String PAYMENTGATEWAYSMSLINK = "http://139.59.16.130:4000/pgs/cc/";
	public static final String PAYMENTGATEWAYSMSLINK = "https://onlinepayment.evasoftwaresolutions.com/pgs/cc/"; //http://159.65.144.223:4000/pgs/cc/

	public static final String TRENDZWEBSOLUTION = "http://trendzwebsolutions.in";

	public static final String PC_ENABLESITELOCATION = "PC_EnbaleSiteLocation";

	
	public static final String SCREENMENUCONFIGURATION = "Screen Menu Configuration";
	public static final String UPDATESERVICEADDRESS = "Update Service";
	public static final String PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE = "PC_EnableSendEmailsFromPersonResponsible";

	public static final String TALLYPAYMENTUPLOAD = "Tally Payment Upload";
	public static final String GLOBALRETRIEVALEMPLOYEEINFO="EmployeeInfo";

	public static final String CTCALOCATIONUPLOAD="CTC Allocation";

	public static final String VENDORINVOICE = "VendorInvoice";
	public static final String CREDITNOTE = "Credit Note";
	public static final String EMAILTEMPLATE = "Email Template";
	public static final String EMAILTEMPLATECONFIGURATION = "Communication Configuration";

	public static final String ENABLESENDGRIDEMAILAPI = "EnableSendGridEmailApi";
	
	public static final String CONFIG = "Config";
	public static final String CONFIGCATEGORY ="ConfigCategory";
	public static final String TYPE ="Type";
	
	public static final String PC_COMPLETEFIRSTSERVICE = "PC_CompleteFirstService";
	public static final String PAYANDCONFIRMNOW = "Pay & Confirm Now";
	public static final String CONFIRMNOWPAYLATER = "Confirm Now Pay Later";

	static String digitalPaymentlink = "Digital&nbsp;Payment&nbsp;Link";
	public static final CharSequence EMAILPAYMENTGATEWAYREQUEST =  "{Digital&nbsp;Payment&nbsp;Link}";
	public static final String EMAILPAYMENTGATEWAYREQUESTLINK2 = "{DigitalPaymentLink}";

	/** This constant is used for Send SMS added by vijay on 26-06-2021*/
	public static final String SMS = "SMS";
//	public static final String SMS = "Message";
	public static final String MESSAGE = "Message";

	public static final String CONTRACT_RENEWAL = "ContractRenewal";

	public static final String LEADPAYMENTGATEWAY = "Lead PaymentGateway";
	public static final String QUOTATIONPAYMENTGATEWAY = "Quatation PaymentGateway";
	public static final String CONTRACTPAYMENTGATEWAY = "Contract PaymentGateway";
	public static final String CONTRACTRENEWALPAYMENTGATEWAY = "ContractRenewal PaymentGateway";
	public static final String INVOICEPAYMENTGATEWAY = "Invoice PaymentGateway";
	public final static String PAYMENTGATEWAYINTRO = "Payment Gateway Introduction";
	
	// for cors
//	public static final String PAYMNETGATEWAY = "http://139.59.16.130:4000";


	

	public static final String DIGITALCONTRACT = "Digital Contract";
	public static final String DIGITALINVOICE = "Digital Invoice";
	
	public static final String LEADENQUIRY = "Lead Enquiry";
	public static final String COMPLAINCREATED = "Complaint Created";
	public static final String COMPLAINCOMPLETED = "Complaint Completed";
	public final static String  SERVICELEAD ="Service Lead";
	
	public static final String COMPANY = "Company";
	public static final String COMPANYPAYMENT = "Company Payment";
	public static final String SELFAPPROVAL = "SelfApproval";
	public static final String SPECIFICREASONFORRESCHEDULING = "Service Reschedule Specific Reasons";
	public static final String SERVICECANCELLATIONREMARK = "Service Cancellation Resons";
	
	public static final String QUOTATIONPDFLINK = "Quotation PDF Link";
	public static final String CONTRACTPDFLINK = "Contract PDF Link";
	public static final String INVOICEPDFLINK = "Invoice PDF Link";
	public static final String CONTRACTRENEWALPDFLINK = "ContractRenewal PDF Link";

	public static final String HIGHFREQUENCYSERVICES = "HighFrequencyServices";
	public static final String PC_AMOUNTINWORDSHUNDREADSTRUCTURE = "PC_AmountInWordsHundreadStructure";
	public static final String PC_SERVICESCHEDULEMINMAXROUNDUP = "PC_ServiceScheduleMinMaxRoundUp";
	
	public static final String DAY = "Day";
	public static final String EXCLUDEDAY = "ExcludeDay";
	public static final String INTERNAL = "Internal";
	public static final String WEEK = "Week";
	public static final String PC_REMOVESALUTATIONFROMCUSTOMERONPDF = "PC_RemoveSalutationFromCustomerOnPdf";

	/** Added by Sheetal on 20-10-2021 for Adding an attendance file format to download on the link**/
    public static final String ADDSINGLEANDDOUBLEOTINATTENDANCEUPLOADTEMPLATE ="Add Single and Double OT in Attendance Upload Template";
    public static final String ENABLESITELOCATION ="Enable Site Location";
    public static final String ATTENDANCE = "Attendance";
	public static final String SR_SRCOPYEMAILTEMPLATE = "SR_SRCopyEmailTemplate";
	public static final String SERVICERECORDPDFLINK = "Service Record PDF Link";
	public static final String CONTRACTRENEWALREMINDER = "Renewal_Final_Reminder";
	public static final String CONTRACTRENEWALOVERDUEREMINDER = "Renewal_Overdue_Reminder";
	public static final String CONTRACTRENEWALEMAILTOCLIENT = "PC_ContractRenewalReminderToClient";
	 /** Added by Sheetal on 23-11-2021, old renewal letter for pepcopp**/
    public static final String PC_PDF_ContractRenewal_Old_New_Price_format = "PC_PDF_ContractRenewal_Old_New_Price_format";
   

    public static final String QUICKEMPLOYEE = "QuickEmployee";
    public static final String CUSTOMERPORTAL = "CustomerPortal";
    public static final String PC_DISPLAYIMAGE = "PC_DisplayImage";
	
	public static final String PC_NO_ROUNDOFF_INVOICE_CONTRACT = "PC_NO_ROUNDOFF_INVOICE_CONTRACT";
	public static final String PC_ENABLEUJUMBESMSAPI = "PC_ENABLEUJUMBESMSAPI";
    public static final String PC_UPDATEALLSERVICEADDRESS = "PC_UPDATEALLSERVICEADDRESS";
	public static final String PC_CUSTOMQUOTATION = "PC_CUSTOMQUOTATION";

	public static final String VIEWLEAD = "View Lead";
	public static final String VIEWQUOTATION = "View Quotation";
	public static final String PC_NEWINVOICEFORMAT = "PC_NewInvoiceFormat";
	public static final String TabularGstServiceInvoice = "TabularGstServiceInvoice";
	public static final String PDFDATEFORMAT = "PDFDateFormat";
	
	public static final String UPDATESERVICEVALUE = "Update Service Value";
	public static final String RePlanServices = "Replan Services";
	
	/**@author Sheetal : 10-05-2022 , for contract renewal cron job templates **/
	public static final String ContractRenewalOverdueEmail = "Contract Renewal Overdue Reminder";
	public static final String ContractRenewalFinalRemainder = "Contract Renewal Final Reminder";
	
	
	
	public static final String WHATSAPP = "WhatsApp";
	public static final String SMS_WHATSAPP = "SMS/WhatsApp";
	public static final String SERVICE = "Service";
	public static final String Invoice = "Invoice";
	public static final String PEDIO = "Pedio";
	public static final String CUSTOMERPAYMENT = "CustomerPayment";
	public static final String PERIODIC = "Periodic";
	public static final String LOADACTIVECONTRACTSONLY = "loadActiveContractOnly";
	public static final String CONTRACTAPPROVED = "Contract Approved";

	public static final String PRIORA = "Priora";

	public static final String USERAPPREGISTRATIONOTP = "User App Registartion OTP";
	
	public static final String AMOUNTPAYABLE = "Amount Payable";
	public static final String AMOUNTRECEIVED = "Payment Received";
	public static final String SERVICECOMPLETION = "Service Completion";
	public static final String CONTRACTCANCELLATION = "Contract Cancellation";
	public static final String DELIVERYNOTEAPPROVED = "Delivery Note Approved";
	public static final String VENDORPAYMENTDETAILS = "Vendor Payment Details";

	public static final String COMPLAINSERVICECOMPLETED = "Complaint Service Completed";

	public static final String SERVICECREATIONUPLOAD = "Service Creation Upload";

	/**@author Sheetal : 29-05-2022 , process configuration for Envocare**/
	public static final String PC_EnableOracletab = "PC_EnableOracletab";
	public static final String PC_MakeEmailNonMandatoryOnVendor= "PC_MakeEmailNonMandatoryOnVendor";
	public static final String PC_MakeCellNumberNonMandatoryOnVendor= "PC_MakeCellNumberNonMandatoryOnVendor";
	
	public static final String CasualLeave = "Casual Leave";
	public static final String UPDATETECHNICIANNAME = "Update Technician Name";
	public static final String CONTRACTRESET = "Contract Reset";

	public static final String REVERSECHARGEVALUE = "No";
	
	public static final String PC_LOADALLCUSTOMER = "PC_LoadAllCustomer";

	public static final String ContractRenewalCronJobImpl = "ContractRenewalCronJobImpl";
	public static final String ServiceReminderToclientCronJobImpl = "ServiceReminderToclientCronJobImpl";
	public static final String CustomerARPaymentDueEmailToClientCronJob = "CustomerARPaymentDueEmailToClientCronJob";
	public static final String ServiceFeedbackCronJobImpl = "ServiceFeedbackCronJobImpl";
	public static final String ServiceSchedulingCronJobImpl = "ServiceSchedulingCronJobImpl";

	public static final String PC_QUOTATIONFOTMATVERSION1 = "PC_QuotationFormatVersion1";
	
	public static final String PC_ENABLEPASSWORDENCRYPTION = "PC_EnablePasswordEncryption";

	public static final int employeeProjectAllocationListNumber = 600;
	public static final String SERVICETYPECOMPLAIN = "Complain";
	public static final String APPREGISTRATIONOTP = "App Registartion OTP";

	public static final String ZONALCOORDINATOR = "Zonal Coordinator";
	public static final String OPERATOR = "Operator";
	public static final String SALES = "Sales";
	
	public static final String TECHNICIANREMARK = "Technician Remark";

	public static final String TECHNICIAN = "Technician";
//	public static final String BRANCHMANAGER = "Branch Manager";
//	public static final String WAREHOUSEMANAGER = "Warehouse Manager";
	public static final String SUPERVISOR = "Supervisor";
	public static final String STOREKEEPER = "Store Keeper";
	public static final String PC_MMNOUTAPPROVERASMMNINREQUESTEDBY = "PC_MMNOutApproverAsMMNinRequestedBy";

	public static final int payrollProcessTaskQueueLimit = 100;

	public static final String QUOTATIONAPPROVAL = "Quotation Approval";
	public static final String INVOICEAPPROVAL = "Invoice Approval";
	public static final String GRNAPPROVAL = "GRN Approval";
	
	public static final String PC_DONOTPRINTCONTRACTSERVICES = "PC_DoNotPrintContractServices";
	public static final String PC_DONOTPRINTCONTRACTDURATION = "PC_DoNotPrintContractDuration";
	public static final String PC_DONOTPRINTRATE = "PC_DoNotPrintRate";
	public static final String PC_DONOTPRINTDISCOUNT = "PC_DoNotPrintDiscount";
	
	public static final String LISENCEINFORMATION = "Lisence Information";
	public static String GETLICENSEINFO="getlicenseInfo";


	public static final String STANDARDINVOICEFORMAT = "Standard Invoice Format";
	public static final String NEWSTANDARDINVOICEFORMAT = "New Standard Invoice Format";
	public static final String CUSTOMIZEDINVOICEFORMAT = "Customized Invoice Format";

	public static final String CustomerPortalLink = "https://squid-app-ik7i3.ondigitalocean.app/login?authToken=";
	public static final String SHARECUSTOMERPORTALLINKTEMPLATE = "Customer Portal Link Communication";
	public static final String COPY = "Copy";
	
	public static final String CALENDAR = "Calendar";
	public final static String HOLIDAYS = "Holidays";
	public final static String TERMSANDCONDITIONS="Terms And Conditions";
	
	public static final String PC_DONOTPRINTQUANTITY = "PC_DoNotPrintQuantity";
	public static final String PC_DONOTPRINTCONTRACTPERIOD = "PC_DoNotPrintContractPeriod";
	public static final String DELETECLIENTLICENSE = "Delete Client License";
	public static final String MONTHLYPREVIOUSMONTHBILLINGPERIOD = "Monthly (Previous Month Billing Period)";
	public static final String MONTHLYPARTIALFIRSTMONTH = "Monthly (Partial First Month)";
	public static final String PC_DONOTPRINTUOM = "PC_DoNotPrintUOM";
	public static final String PC_DONOTPRINTTAX = "PC_DoNotPrintTax";
	public static final String PC_RENAMEASSALVALASAMOUNT = "PC_RenameAssesValAsAmount";

	public static final String PC_SRFORMATVERSION1 = "PC_SRFormatVersion1";

	public static final String PC_ENABLETAXMANDATORY = "PC_EnableTaxMandatory";

	public static final String PC_ENABLEREFERENCEDATEMANDATORY = "PC_EnableReferenceDateMandatory";
	public static final String PC_ENABLEVENDORREFERENCEIDMANDATORY = "PC_EnableVendorReferenceIdMandatory";

	public static final String PC_ENABLEADDDELETESALESORDERPRODUCTSONINVOICE = "PC_EnableAddDeleteSalesOrderProductsOnInvoice";

	public static final String MYGSTCAFE = "MyGSTCafe";//Ashwini Patil Date:29-05-2023
	public static final String TAXILLA = "Taxilla";//Ashwini Patil Date:29-05-2023

	public static final String PC_UPDATESALESORDERINVOICE="PC_UpdateSalesOrderInvoice";
	public static final String UPDATESALESORDERINVOICE="UpdateSalesOrderInvoice";
	
	public static final String REGISTEREXPENSE = "Register Expense";
	public static final String COMPLAIN = "Complaint";
	public static final String VIEWCOMPLAIN = "View Complaint";
	public static final String VIEWEXPENSES = "View Expenses";
	public final static String CNCHR = "CNC & HR";
	public final static String PC_ENABLELOADCNCHRTEMPLATE="PC_EnableLoadCNCHRTemplate";
	public static final String HR = "HR";
	public static final String COMPLAINCREATION = "ComplainCreation";//Ashwini Patil Date:13-06-2023
	public static final String PC_ENABLESHOWINVOICECONFIGTYPEFROMINVOICE = "PC_EnableShowInvoiceConfigTypeFromInvoice";
	public static final String PC_ENABLEDONOTPRINTASSESSEDBYSERIALNUMBER = "PC_EnableDoNotPrintAssessedBySerialNumber";
	public static final String PC_ENABLEDONOTPRINTASSESSEDBYCELLNUMBER = "PC_EnableDoNotPrintAssessedByCellNumber";
	public static final String PC_ENABLEDONOTPRINTACCOMPAINEDBYCELLNUMBER = "PC_EnableDoNotPrintAccompainedByCellNumber";
	public static final String PC_ENABLEDONOTPRINTEMAIL = "PC_EnableDoNotPrintEmail";
	public static final String PC_ENABLEOBESERVATIONSUMMARYONFIRSTPAGE = "PC_EnableObservationSummaryOnFirstPage";
    public static final String PC_ENABLESHOWSERVICETYPEDROPDOWN = "PC_EnableShowServiceTypeDropDown";
	public static final String CONTRACTRENEWALDUEREMINDER = "Contract Renewal Due Reminder";

	public static final String RestrictContractsDownloadWithOTP = "Restrict Contracts Download with OTP";//Ashwini Patil Date:4-07-2023
	public static final String RestrictServiceDownloadWithOTP = "Restrict Customer Service Download with OTP";//Ashwini Patil Date:4-07-2023
	public static final String  InvalidOTPDownloadNotification = "Invalid OTP Download Notification";//Ashwini Patil Date:4-07-2023
	
	public static final String PC_ENABLEPURCHASEPDFFORMATWITHOUTTOTALANDTAXINFO = "PC_EnablePurchasePdfFormatWithoutTotalAndTaxInfo";
	public static final String PC_ENABLEDONOTPRINTOBESERVATIONSUMMARY = "PC_EnableDoNotPrintObservationSummary";
	public static final String TinyURLAccount2AuthCode = "T3sHdhmCp2jhyq79zyHsjEfXzB2yCHTeKAlnrhidKBrKZ54aAvOjSCNDOKXN";
	public static final String PC_ENABLETINYURLACCOUNTNO2 = "PC_EnableTinyURLAccountNo2";
	public static final String CANCELLOCATIONWISESERVICES = "Cancel Location Wise Services";
	public static final String PC_SETPAYMENTRECEIVEDATETOPAYMENTDUEDATE = "PC_SetPaymentReceivedDateToPaymentDueDate";
	public static final String BulkeInvoiceGeneration ="Bulk eInvoice Generation";
	public static final String PC_DISABLEIPADDRESSEMAIL = "PC_DisableIPAddressEmail";
	public static final String ACTIVATEORDEACTIVATECUSTOMERBRANCH="Activate/Deactivate Branch";
	public static final String DISTRIBUTESERVICESEQUALLYONGIVENDATES="Reschedule for a period";

	public static final String ScheduleOrCompleteViaExcel="Schedule/Complete via excel";
	public static final String EVAERPUsageReport="EVAERPUsageReport";
	public static final String RESETAPPID="Reset Appid";
	public static final String downloadReports="Download Uploaded SR Copies";
	public static final String generatePayment="Generate Payment";
	public static final String uploadDoc ="Upload Document";
	public static final String syncWithZohoBooks ="Sync With Zoho Books";
	public static final String TallyExportForUltima ="Tally export";
	public static final String UpdateZohoCustomerId ="Update Zoho Customer ID";
	public static final String UpdateCustomerIdInAllDocs ="Update Customer Id";
	public static final String UpdateSalesPerson ="Update Sales Person";
	public static final String AddCustomerBranches ="Add customer branches";
	public static final String markcontractasrenewed="Mark Contract As Renewed";
	public static final String reverseServiceCompletionAndRemoveDuplicate="Reverse Service Completion In Bulk";
	public static final String generateComplainQR="Generate Complain QR";

}



