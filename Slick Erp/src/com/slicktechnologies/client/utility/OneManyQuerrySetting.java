package com.slicktechnologies.client.utility;

import java.util.Vector;

import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.OneManyComboBox;
import com.slicktechnologies.client.config.categoryconfig.CategoryTypes;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class OneManyQuerrySetting 
{


	public  OneManyComboBox setOneAndManyQuery(OneManyComboBox cat,Screen screen)
	{

		    Filter filter=new Filter();
		    filter.setQuerryString("internalType");
		    filter.setIntValue(CategoryTypes.getCategoryInternalType(screen));
		    System.out.println("Size is "+CategoryTypes.getCategoryInternalType(screen));
		   
		    Vector<Filter> vec=new Vector<Filter>();
		    vec.add(filter);
		    MyQuerry oneQuerry=new MyQuerry();
		    oneQuerry.setFilters(vec);
		    oneQuerry.setQuerryObject(new ConfigCategory());

		   
	
		   // create many querry
		   MyQuerry manyquerry=new MyQuerry();
		   Filter filtervalue=new Filter();
		   String querrycon="categoryKey";
		   
		   filtervalue.setQuerryString(querrycon);
		   manyquerry.setQuerryObject(new Type());
		   manyquerry.getFilters().add(filtervalue);
		   
		   cat.setOneQuerry(oneQuerry);
		   cat.setManyQuerry(manyquerry);
		   
		 
		   cat.initializeHashMaps();
		   return cat;
	}

}
