//<ReplasablePackageBlock>
package com.slicktechnologies.client.utility;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.MenuItems;
import com.simplesoftwares.client.library.appskeleton.NavigationBarItem;
import com.simplesoftwares.client.library.appskeleton.SideBarItems;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.complaindashboard.complainHomePresenter;
import com.slicktechnologies.client.views.customerDetails.CustomerDetailsPresenter;
import com.slicktechnologies.client.views.freshdeskhome.FreshDeskHomeScreen;
import com.slicktechnologies.client.views.home.HomePresenter;
import com.slicktechnologies.client.views.humanresource.statuteryreport.StatuteryReportPresenter;
import com.slicktechnologies.client.views.interaction.interactiondashboard.InteractionDashboardPresentor;
import com.slicktechnologies.client.views.summaryscreen.SummaryPresenter;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;

/**
 * The AppConnector template.
 * @author Kamala
 *
 */
public class AppConnector extends AppMemory 
{
	/**
	 * This static part runs at entry point i.e @ onModuleLoad().
	 * Creates appconnector object at runtime itself.
	 */
	private AppConnector()
	{

	}
	static{
		if(memory==null)
			memory=new AppConnector();
	}

	public static void initalizes()
	{

	}

	
	/**
	 *   NOTE:  While Deploying For First Time (Creating Registration/Client Environment)
	 *   
	 *   Methods To Be Updated
	 *   
	 *   createNewNavigationBar
	 *   createAndAddMenuItems
	 *   createBillingSideBar
	 *   createAdminSideBar
	 */


	/**
	 * Method template to create {@link MenuItems} to be displayed on NavigationBar.
	 * @return navigationBarItems
	 */
	@Override
	public Vector<MenuItems> createNavigationBar() 
	{
		//handle navigationbar
		Vector<MenuItems> navigationBarItems=new Vector<MenuItems>();

		//Token to add menuitems in navigationBarItems
	//	navigationBarItems.add(createAndAddMenuItems(Screen.HOME,"Dashboard","Dashboard/Home"));
//		navigationBarItems.add(createAndAddMenuItems(Screen.APPROVAL, "Approval", "Approval"));
//		if(registerationPower())
//		{
//			navigationBarItems.add(createAndAddMenuItems(Screen.REGISTRATION, "Registration", "Registration"));
//		}
		return navigationBarItems;
	}
	
	
	
	
	
	
	
	@Override
	public Vector<NavigationBarItem> createNewNavigationBar() 
	{
		
		boolean salesValidation=false;
		boolean purchaseValidation = false;
		boolean servicesValidation = false;
		boolean inventoryValidation = false;
		boolean hrAdminValidation = false;
		boolean empselfserviceValidation = false;
		boolean productValidation = false;
		boolean assetmanagementValidation = false;
		boolean accountsValidation = false;
		boolean settingValidation = false;
		boolean approvalValidation = false;
		boolean supportValidation = true;
		
		boolean customerScreenValidation=false;
		/** date 03/03/2018 added by komal for new repot module for nbhc **/
		boolean reportValidation = false;
		/***********************************Comment While Deploying For First TIme***********************************/
		
		//*****************old code for login ****************
//		List<ScreenAuthorization> moduleValidation=UserConfiguration.getRole().getAuthorization();
		
		
		/*******************new code for login by Anil************************************/
		List<ScreenAuthorization> moduleValidation = null;
		if(UserConfiguration.getRole()!=null){
			moduleValidation=UserConfiguration.getRole().getAuthorization();
		}
		/*************************************Uncomment while deploying for first time****************************************/
//		List<ScreenAuthorization> moduleValidation=ScreenAuthorization.getAllAuthorizationsForEnvironment();
//		String r=AppConstants.ADMINUSER;
		

		/******************************************For First Time Deployment*********************************************/
		/**
		 *  Uncomment first if condition below for normal deployment
		 *  And Uncomment 2nd if condition for first time deployment
		 */
		
		if(UserConfiguration.getRole()!=null){
		
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER))
//		if(r.equalsIgnoreCase(AppConstants.ADMINUSER))
		{
			 salesValidation=true;
			 purchaseValidation = true;
			 servicesValidation = true;
			 inventoryValidation = true;
			 hrAdminValidation = true;
			 empselfserviceValidation = true;
			 productValidation = true;
			 assetmanagementValidation = true;
			 accountsValidation = true;
			 settingValidation = true;
			 approvalValidation = true;
//			 supportValidation = true;
			 /** date 03/03/2018 added by komal for new repot module for nbhc **/
			 reportValidation = true;
		}
		else
		{
			for(int f=0;f<moduleValidation.size();f++)
			{
				
			if(moduleValidation.get(f).getModule().trim().equals("Sales")||moduleValidation.get(f).getModule().trim().equals("Sales Configuration"))
			{
				salesValidation=true;
			}
			if(moduleValidation.get(f).getModule().trim().equals("Service")||moduleValidation.get(f).getModule().trim().equals("Service Configuration")){
				servicesValidation=true;
			}
			if(moduleValidation.get(f).getModule().trim().equals("Purchase")||moduleValidation.get(f).getModule().trim().equals("Purchase Configuration")){
				purchaseValidation=true;
			}
			if(moduleValidation.get(f).getModule().trim().equals("Inventory")||moduleValidation.get(f).getModule().trim().equals("Inventory Configuration")){
				inventoryValidation = true;
			}
			if(moduleValidation.get(f).getModule().trim().equals("HR Admin")||moduleValidation.get(f).getModule().trim().equals("HR Configuration")){
				hrAdminValidation = true;
			}
			if(moduleValidation.get(f).getModule().trim().equals("Employee Self Service")){
				empselfserviceValidation = true;
			}
			if(moduleValidation.get(f).getModule().trim().equals("Product")||moduleValidation.get(f).getModule().trim().equals("Product Configuration")){
				productValidation = true;
			}
			if(moduleValidation.get(f).getModule().trim().equals("Asset Management")||moduleValidation.get(f).getModule().trim().equals("Asset Managemnent Configuration")){
				assetmanagementValidation = true;
			}
			if(moduleValidation.get(f).getModule().trim().equals("Accounts")||moduleValidation.get(f).getModule().trim().equals("Account Configuration")){
				accountsValidation = true;
			}
			if(moduleValidation.get(f).getModule().trim().equals("Settings")){
				settingValidation = true;
			}
			if(moduleValidation.get(f).getModule().trim().equals("Approval")){
				approvalValidation=true;
			}
//			if(moduleValidation.get(f).getModule().trim().equals("Support")){
//				supportValidation=true;
//			}
			 /** date 03/03/2018 added by komal for new repot module for nbhc **/
			if(moduleValidation.get(f).getModule().trim().equals("Reports")){
				 reportValidation = true;
			}

		}
		}
			}else{
				System.out.println("INSIDE ELSE CUSTOMER.................");
//				customerScreenValidation=true;
			}
		
		
		
		
		
		Vector<NavigationBarItem> navigationBarItems=new Vector<NavigationBarItem>();
		NavigationBarItem navItem=new NavigationBarItem();
		
		/**
		 * Updated By: Viraj
		 * Date: 21-06-2019
		 * Description: To hide HR when Process configuration is active 
		 * @author Anil ,Date : 07-12-2019
		 * checked userconfig not null condition
		 * for creating new link app initialization was getting failed
		 */
		if(UserConfiguration.getUserconfig()!=null&&UserConfiguration.getUserconfig().isHideStatus()){
			Console.log("inside process config,companyType: "+UserConfiguration.getUserconfig().getCompType().trim());
			if(UserConfiguration.getUserconfig().getCompType().trim().equalsIgnoreCase("pest control")) {
				Console.log("Inside if ");
				hrAdminValidation = false;
			}
		}
		Console.log("hrAdminValidation value: "+hrAdminValidation);
		/** Ends **/
		
		if(salesValidation==true){
			navItem.setLabel(new Label("Sales"));
			navItem.setSideBarItems(createSalesSideBar());
			items.add(navItem);
			navigationBarItems.add(navItem);
		}
		
		if(servicesValidation==true){
			navItem=new NavigationBarItem();
			navItem.setLabel(new Label("Service"));
			navItem.setSideBarItems(createServiceSideBar());
			items.add(navItem);
			navigationBarItems.add(navItem);
		}
		
		if(purchaseValidation==true){
			navItem=new NavigationBarItem();
			navItem.setLabel(new Label("Purchase"));
			navItem.setSideBarItems(createPurchaseSideBar());
			items.add(navItem);
			navigationBarItems.add(navItem);
		}
		
		if(inventoryValidation==true){
			navItem=new NavigationBarItem();
			navItem.setLabel(new Label("Inventory"));
			navItem.setSideBarItems(createInventorySideBar());
			items.add(navItem);
			navigationBarItems.add(navItem);
		}
		
		if(hrAdminValidation==true){
			navItem=new NavigationBarItem();
			navItem.setLabel(new Label("HR Admin"));
			navItem.setSideBarItems(createHumanResourceSideBar());
			items.add(navItem);
			navigationBarItems.add(navItem);
		}
		
		if(empselfserviceValidation==true){
			navItem=new NavigationBarItem();
			navItem.setLabel(new Label("Employee Self Service"));
			navItem.setSideBarItems(createEmployeeSelfServiceSideBar());
			items.add(navItem);
			navigationBarItems.add(navItem);
		}
		
		if(productValidation==true){
			navItem=new NavigationBarItem();
			navItem.setLabel(new Label("Product"));
			navItem.setSideBarItems(createProductsSideBar());
			items.add(navItem);
			navigationBarItems.add(navItem);
		}
		
		if(assetmanagementValidation==true){
			navItem=new NavigationBarItem();
			navItem.setLabel(new Label("Asset Management"));
			navItem.setSideBarItems(createAssetSideBar());
			items.add(navItem);
			navigationBarItems.add(navItem);
		}
		
		if(accountsValidation==true){
			navItem=new NavigationBarItem();
			navItem.setLabel(new Label("Accounts"));
			navItem.setSideBarItems(createBillingSideBar());
			items.add(navItem);
			navigationBarItems.add(navItem);
		}
		
		if(settingValidation==true){
			navItem=new NavigationBarItem();
			navItem.setLabel(new Label("Settings"));
			navItem.setSideBarItems(createAdminSideBar());
			items.add(navItem);
			navigationBarItems.add(navItem);
		}
		
		if(approvalValidation==true){
				navItem=new NavigationBarItem();
				navItem.setLabel(new Label("Approval"));
				navItem.setSideBarItems(createApprovalSideBar());
				items.add(navItem);
				navigationBarItems.add(navItem);
		}	
		/**
		 * @author Anil @since 02-02-2021
		 * Removing support module as we have implemented fresh desk widget to get clients complaint
		 * As per the Nitin sir's instruction removing this option
		 */
//		if(supportValidation==true){
//			navItem=new NavigationBarItem();
//			navItem.setLabel(new Label("Support"));
//			navItem.getLabel().getElement().setId("support");
//			navItem.setSideBarItems(createSupportSideBar());
//			items.add(navItem);
//			navigationBarItems.add(navItem);
//		}
		
		
	    if(customerScreenValidation==true){
    		navItem.setLabel(new Label("Menu"));
			navItem.setSideBarItems(createSideBarForCustomer());
			items.add(navItem);
			navigationBarItems.add(navItem);
		}
		
		//*********************Uncomment code below for 1st time deployment 
//	    navItem=new NavigationBarItem();
//		navItem.setLabel(new Label("Registration"));
//		navItem.setSideBarItems(createRegistrationSideBar());
//		items.add(navItem);
//		navigationBarItems.add(navItem);
		
		
		if(AppUtility.checkIsItServerSide() && registerationPower())
		{
			navItem=new NavigationBarItem();
			navItem.setLabel(new Label("Registration"));
			navItem.setSideBarItems(createRegistrationSideBar());
			items.add(navItem);
			navigationBarItems.add(navItem);
		}
		
		/** date 03/03/2018 added by komal for new reports module for nbhc **/
		if(reportValidation == true)
		{
			navItem=new NavigationBarItem();
			navItem.setLabel(new Label("Reports"));
			navItem.setSideBarItems(createReportSideBar());
			items.add(navItem);
			navigationBarItems.add(navItem);
		}
		return navigationBarItems;
	}

	
	/**
	 * Method template to create {@link MenuItems} to be displayed on SideBar.
	 * @return sideBarNames.
	 */
	@Override
	public Vector<SideBarItems> createSideBar()
	{

        Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
//		Vector<MenuItems> temp;
//		        
//		        //Token to add menuitems in SideBarItems and add this sidebaritem in sideBarNames
//		//temp=new Vector<MenuItems>();
//		//temp.add(createAndAddMenuItems(Screen.USERAUTHORIZATION,"User Authorization","User Authorization/User Authorization"));
//		//sideBarNames.add(new SideBarItems("User Authorization",temp));
//		
//		//*************Sales*******************//
//	temp=new Vector<MenuItems>();
//	temp.add(createAndAddMenuItems(Screen.SALESHOME,"Dashboard","Sales/Dashboard"));
//	temp.add(createAndAddMenuItems(Screen.SALESCUSTOMER,"Customer","Sales/Customer"));
//	temp.add(createAndAddMenuItems(Screen.SALESLEAD,"Lead","Sales/Lead"));
//	temp.add(createAndAddMenuItems(Screen.SALESQUOTATION,"Quotation","Sales/Quotation"));
//	temp.add(createAndAddMenuItems(Screen.SALESORDER,"Sales Order","Sales/Sales Order"));
//	temp.add(createAndAddMenuItems(Screen.DELIVERYNOTE,"Delivery Note","Sales/Delivery Note"));
//	
//	sideBarNames.add(new SideBarItems("Sales",temp));
//	
//	
//	//***************SAles config***************//
//	temp=new Vector<MenuItems>();
//	temp.add(createAndAddMenuItems(Screen.CUSTOMERGROUP,"Customer Group","Sales Configuration/Customer Group"));
//	temp.add(createAndAddMenuItems(Screen.CUSTOMERCATEGORY,"Customer Category","Sales Configuration/Customer Category"));
//	temp.add(createAndAddMenuItems(Screen.CUSTOMERTYPE,"Customer Type","Sales Configuration/Customer Type"));
//	temp.add(createAndAddMenuItems(Screen.CUSTOMERLEVEL,"Customer Level","Sales Configuration/Customer Level"));
//	temp.add(createAndAddMenuItems(Screen.CUSTOMERPRIORITY,"Customer Priority","Sales Configuration/Customer Priority"));
//
//	temp.add(createAndAddMenuItems(Screen.LEADSTATUS,"Lead Status","Sales Configuration/Lead Status"));
//	temp.add(createAndAddMenuItems(Screen.LEADGROUP,"Lead Group","Sales Configuration/Lead Group"));
//	temp.add(createAndAddMenuItems(Screen.LEADCATEGORY,"Lead Category","Sales Configuration/Lead Category"));
//	temp.add(createAndAddMenuItems(Screen.LEADTYPE,"Lead Type","Sales Configuration/Lead Type"));
//	temp.add(createAndAddMenuItems(Screen.LEADPRIORITY,"Lead Priority","Sales Configuration/Lead Priority"));
//
//	temp.add(createAndAddMenuItems(Screen.SALESQUOTATIONCATEGORY,"Sales Quotation Category","Sales Configuration/Sales Quotation Category"));
//	temp.add(createAndAddMenuItems(Screen.SALESQUOTATIONTYPE,"Sales Quotation Type","Sales Configuration/Sales Quotation Type"));
//	temp.add(createAndAddMenuItems(Screen.SALESQUOTATIONGROUP,"Sales Quotation Group","Sales Configuration/Sales Quotation Group"));
//	temp.add(createAndAddMenuItems(Screen.SALESQUOTATIONPRIORITY,"Sales Quotation Priority","Sales Configuration/Sales Quotation Priority"));
//	temp.add(createAndAddMenuItems(Screen.SALESORDERCATEGORY,"Sales Order Category","Sales Configuration/Sales Order Category"));
//	temp.add(createAndAddMenuItems(Screen.SALESORDERTYPE,"Sales Order Type","Sales Configuration/Sales Order Type"));
//	temp.add(createAndAddMenuItems(Screen.SALESORDERGROUP,"Sales Order Group","Sales Configuration/Sales Order Group"));
//
//	sideBarNames.add(new SideBarItems("Sales Configuration",temp));
//
//	
//	
//	//****************Service******************************//
//	temp=new Vector<MenuItems>();
//	temp.add(createAndAddMenuItems(Screen.HOME,"Dashboard","Service/Dashboard"));
//	temp.add(createAndAddMenuItems(Screen.CUSTOMER,"Customer","Service/Customer"));
//	temp.add(createAndAddMenuItems(Screen.LEAD,"Lead","Service/Lead"));
//	temp.add(createAndAddMenuItems(Screen.QUOTATION,"Quotation","Service/Quotation"));
//	temp.add(createAndAddMenuItems(Screen.CONTRACT,"Contract","Service/Contract"));
//	temp.add(createAndAddMenuItems(Screen.SERVICE, "Customer Service List", "Service/Customer Service List"));
//	temp.add(createAndAddMenuItems(Screen.DEVICE, "Customer Service", "Service/Customer Service"));
//	temp.add(createAndAddMenuItems(Screen.PROJECT, "Customer Project", "Service/Customer Project"));
//	temp.add(createAndAddMenuItems(Screen.COMPANYASSET, "Company Asset", "Service/Company Asset"));
//	temp.add(createAndAddMenuItems(Screen.CLIENTSIDEASSET, "Client Asset", "Service/Client Asset"));
//	temp.add(createAndAddMenuItems(Screen.TOOLGROUP, "Tool Set", "Service/Tool Set"));
//	temp.add(createAndAddMenuItems(Screen.LIAISONSTEPS, "Liasion Steps", "Service/Liasion Steps"));
//	temp.add(createAndAddMenuItems(Screen.LIAISONGROUP, "Liasion Group", "Service/Liasion Group"));
//	temp.add(createAndAddMenuItems(Screen.LIASION, "Liasion", "Service/Liasion"));
//	
//	sideBarNames.add(new SideBarItems("Sales (Service)", temp));
//	
//	temp=new Vector<MenuItems>();
//	temp.add(createAndAddMenuItems(Screen.ASSETCATEGORY,"Tool Category", "Service Configuration/Tool Category"));
//	temp.add(createAndAddMenuItems(Screen.CLIENTSIDEASSETCATEGORY,"Client Asset Category", "Service Configuration/Client Asset Category"));
//	temp.add(createAndAddMenuItems(Screen.CUSTOMERGROUP,"Customer Group","Service Configuration/Customer Group"));
//	temp.add(createAndAddMenuItems(Screen.CUSTOMERCATEGORY,"Customer Category","Service Configuration/Customer Category"));
//	temp.add(createAndAddMenuItems(Screen.CUSTOMERTYPE,"Customer Type","Service Configuration/Customer Type"));
//	temp.add(createAndAddMenuItems(Screen.CUSTOMERLEVEL,"Customer Level","Service Configuration/Customer Level"));
//	temp.add(createAndAddMenuItems(Screen.CUSTOMERPRIORITY,"Customer Priority","Service Configuration/Customer Priority"));
//
//	temp.add(createAndAddMenuItems(Screen.LEADSTATUS,"Lead Status","Service Configuration/Lead Status"));
//	temp.add(createAndAddMenuItems(Screen.LEADGROUP,"Lead Group","Service Configuration/Lead Group"));
//	temp.add(createAndAddMenuItems(Screen.LEADCATEGORY,"Lead Category","Service Configuration/Lead Category"));
//	temp.add(createAndAddMenuItems(Screen.LEADTYPE,"Lead Type","Service Configuration/Lead Type"));
//	temp.add(createAndAddMenuItems(Screen.LEADPRIORITY,"Lead Priority","Service Configuration/Lead Priority"));
//	
//	temp.add(createAndAddMenuItems(Screen.QUOTATIONGROUP,"Quotation Group","Service Configuration/Quotation Group"));
//	temp.add(createAndAddMenuItems(Screen.QUOTATIONCATEGORY,"Quotation Category","Service Configuration/Quotation Category"));
//	temp.add(createAndAddMenuItems(Screen.QUOTATIONTYPE,"Quotation Type","Service Configuration/Quotation Type"));
//	temp.add(createAndAddMenuItems(Screen.QUOTATIONPRIORITY,"Quotation Priority","Service Configuration/Quotation Priority"));
//	
//	temp.add(createAndAddMenuItems(Screen.CONTRACTGROUP,"Contract Group","Service Configuration/Contract Group"));
//	temp.add(createAndAddMenuItems(Screen.CONTRACTCATEGORY,"Contract Category","Service Configuration/Contract Category"));
//	temp.add(createAndAddMenuItems(Screen.CONTRACTTYPE,"Contract Type","Service Configuration/Contract Type"));
//	
//	sideBarNames.add(new SideBarItems("Sales (Service) Configuration", temp));
//	
//	
//	
//	//**************Settings********************//
//	temp=new Vector<MenuItems>();
//	
//	temp.add(createAndAddMenuItems(Screen.BRANCH,"Branch","Settings/Branch"));
//	temp.add(createAndAddMenuItems(Screen.EMPLOYEE,"Employee","Settings/Employee"));
//	temp.add(createAndAddMenuItems(Screen.USER,"User","Settings/User"));
//	temp.add(createAndAddMenuItems(Screen.USERAUTHORIZATION,"User Authorization","Settings/User Authorization"));
//	temp.add(createAndAddMenuItems(Screen.COUNTRY,"Country","Settings/Country"));
//	sideBarNames.add(new SideBarItems("Settings",temp));
//	
//	
//	
//	
//	//******************Account***********************//
//	temp=new Vector<MenuItems>();
//	temp.add(createAndAddMenuItems(Screen.ACCOUNTDASHBOARD,"Dashboard","Accounts/Dashboard"));
//	temp.add(createAndAddMenuItems(Screen.EXPENSEMANAGMENT,"Expense Management","Accounts/Expense Management"));
//	temp.add(createAndAddMenuItems(Screen.PETTYCASHTRANSACTION,"Petty Cash Transaction","Accounts/Petty Cash Transaction"));
//	temp.add(createAndAddMenuItems(Screen.BILLINGLIST,"Billing List","Accounts/Billing List"));
//	temp.add(createAndAddMenuItems(Screen.BILLINGDETAILS,"Billing Details","Accounts/Billing Details"));
//	temp.add(createAndAddMenuItems(Screen.INVOICELIST,"Invoice List","Accounts/Invoice List"));
//	temp.add(createAndAddMenuItems(Screen.INVOICEDETAILS,"Invoice Details","Accounts/Invoice Details"));
//	temp.add(createAndAddMenuItems(Screen.PAYMENTLIST,"Payment List","Accounts/Payment List"));
//	temp.add(createAndAddMenuItems(Screen.PAYMENTDETAILS,"Payment Details","Accounts/Payment Details"));
//	temp.add(createAndAddMenuItems(Screen.COMPANYPAYMENT,"Payment Mode","Accounts/Payment Mode"));
//	
//	sideBarNames.add(new SideBarItems("Accounts",temp));
//	
//	//*******Account Config*************//
//	temp=new Vector<MenuItems>();
//	temp.add(createAndAddMenuItems(Screen.EXPENSEGROUP, "Expense Group", "Account Configuration/Expense Group"));
//	temp.add(createAndAddMenuItems(Screen.EXPENSECATEGORY,"Expense Category","Account Configuration/Expense Category"));
//	temp.add(createAndAddMenuItems(Screen.EXPENSETYPE,"Expense Type","Account Configuration/Expense Type"));
//	temp.add(createAndAddMenuItems(Screen.GLACCOUNT,"GLAccount","Account Configuration/GLAccount"));
//	temp.add(createAndAddMenuItems(Screen.GLACCOUNTGROUP,"GLAccount Group","Account Configuration/GLAccount Group"));
//	temp.add(createAndAddMenuItems(Screen.PAYMENTMETHODS,"Payment Method","Account Configuration/Payment Method"));
//	temp.add(createAndAddMenuItems(Screen.PAYMENTTERMS,"Payment Terms","Account Configuration/Payment Terms"));
//	temp.add(createAndAddMenuItems(Screen.OTHERCHARGES,"Other Charges","Account Configuration/Other Charges"));
//	temp.add(createAndAddMenuItems(Screen.PETTYCASH,"Petty Cash","Account Configuration/Petty Cash"));
//	temp.add(createAndAddMenuItems(Screen.CURRENCY,"Currency","Account Configuration/Currency"));
//	temp.add(createAndAddMenuItems(Screen.COMPANY,"Company","Accounts/Company"));
//	temp.add(createAndAddMenuItems(Screen.COMPANYTYPE,"Company Type","Account Configuration/Company Type"));
//	temp.add(createAndAddMenuItems(Screen.BILLINGCATEGORY,"Billing Category","Account Configuration/Billing Catergory"));
//	temp.add(createAndAddMenuItems(Screen.BILLINGTYPE,"Billing Type","Account Configuration/Billing Type"));
//	temp.add(createAndAddMenuItems(Screen.BILLINGGROUP,"Billing Group","Account Configuration/Billing Group"));
//	temp.add(createAndAddMenuItems(Screen.TAXDETAILS,"Tax Details","Account Configuration/Tax Details"));
//	sideBarNames.add(new SideBarItems("Account Configuration", temp));
//	
//	//*****************Inventory**************************/
//		temp=new Vector<MenuItems>();
//		temp.add(createAndAddMenuItems(Screen.PRODUCTINVENTORYVIEWLIST,"Product Inventory View List","Product Inventory View List"));
//		temp.add(createAndAddMenuItems(Screen.GRN,"GRN","Inventory/GRN"));
//		temp.add(createAndAddMenuItems(Screen.PRODUCTINVENTORYVIEW,"Product Inventory View","Inventory/Product Inventory View"));
//		temp.add(createAndAddMenuItems(Screen.INVENTORYINHAND,"Inventory in Hand","Inventory/Inventory in Hand"));
//		temp.add(createAndAddMenuItems(Screen.WAREHOUSE,"Warehouse","Inventory/Warehouse"));
//		temp.add(createAndAddMenuItems(Screen.STORAGELOCATION,"Storage Location","Inventory/Storage Location"));
//		temp.add(createAndAddMenuItems(Screen.STORAGEBIN,"Storage Bin","Inventory/Storage Bin"));
//		temp.add(createAndAddMenuItems(Screen.WAREHOUSEDETAILS,"Physical Inventory","Inventory/Physical Inventory"));
//		
//		sideBarNames.add(new SideBarItems("Inventory",temp));
//
//	//*****************Inventory Config**************************/
//		temp=new Vector<MenuItems>();
//		temp.add(createAndAddMenuItems(Screen.GRNCATEGORY,"GRN Category","Inventory Configuration/GRN Category"));
//		temp.add(createAndAddMenuItems(Screen.GRNGROUP,"GRN Group","Inventory Configuration/GRN Group"));
//		temp.add(createAndAddMenuItems(Screen.GRNTYPE,"GRN Type","Inventory Configuration/GRN Type"));
//		
//		sideBarNames.add(new SideBarItems("Inventory Configuration",temp));
//	
//	//***************Purchase**********************/
//	temp=new Vector<MenuItems>();
//	temp.add(createAndAddMenuItems(Screen.PURCHASEDASHBOARD,"Dashboard","Purchase/Dashboard"));
//	temp.add(createAndAddMenuItems(Screen.VENDOR,"Vendor","Purchase/Vendor"));
//	temp.add(createAndAddMenuItems(Screen.PURCHASEREQUISITE,"Purchase Requisition","Purchase/Purchase Requisition"));
//	temp.add(createAndAddMenuItems(Screen.REQUESTFORQUOTATION,"RFQ","Purchase/RFQ"));
//	temp.add(createAndAddMenuItems(Screen.LETTEROFINTENT,"Letter Of Intent(LOI)","Purchase/Letter Of Intent(LOI)"));
//	temp.add(createAndAddMenuItems(Screen.PURCHASEORDER,"Purchase Order","Purchase/Purchase Order"));
//	temp.add(createAndAddMenuItems(Screen.VENDORPRODUCTLIST,"Vendor Product Pricelist","Purchase/Vendor Product Pricelist"));
//	temp.add(createAndAddMenuItems(Screen.VENDORPRICELIST,"Vendor Product Price","Purchase/Vendor Product Price"));
//	
//	sideBarNames.add(new SideBarItems("Purchase",temp));
//	
//	//*********Purchase Config**************/
//	temp=new Vector<MenuItems>();
//	temp.add(createAndAddMenuItems(Screen.PURCHASEORDERCATEGORY, "Purchase Category", "Purchase Configuration/Purchase Category"));
//	temp.add(createAndAddMenuItems(Screen.PURCHASEORDERTYPE, "Purchase Type", "Purchase Configuration/Purchase Type"));
//	temp.add(createAndAddMenuItems(Screen.VENDORGROUP, "Vendor Group", "Purchase Configuration/Vendor Group"));
//	temp.add(createAndAddMenuItems(Screen.VENDORCATEGORY, "Vendor Category", "Purchase Configuration/Vendor Category"));
//	temp.add(createAndAddMenuItems(Screen.VENDORTYPE, "Vendor Type", "Purchase Configuration/Vendor Type"));
//	temp.add(createAndAddMenuItems(Screen.LOICATEGORY, "LOI Category", "Purchase/LOI Category"));
//	temp.add(createAndAddMenuItems(Screen.LOITYPE, "LOI Type", "Purchase/LOI Type"));
//	temp.add(createAndAddMenuItems(Screen.RFQTYPE, "RFQ Type", "Purchase/RFQ Type"));
//	temp.add(createAndAddMenuItems(Screen.RFQCATEGORY, "RFQ Category", "Purchase/RFQ Category"));
//	temp.add(createAndAddMenuItems(Screen.PRCATEGORY, "PR Category", "Purchase/PR Category"));
//	temp.add(createAndAddMenuItems(Screen.PRTYPE, "PR Type", "Purchase/PR Type"));
//	temp.add(createAndAddMenuItems(Screen.PRGROUP, "PR Group", "Purchase/PR Group"));
//	sideBarNames.add(new SideBarItems("Purchase Configuration", temp));
//	
//	//********************Product***************//
//	temp=new Vector<MenuItems>();
//	temp.add(createAndAddMenuItems(Screen.SERVICEPRODUCT,"Service Product","Product/Service Product"));
//	temp.add(createAndAddMenuItems(Screen.ITEMPRODUCT,"Item Product","Product/Item Product"));
//	temp.add(createAndAddMenuItems(Screen.TAX, "Tax", "Product/Tax"));
//	
//	sideBarNames.add(new SideBarItems("Product",temp));
//	
//	
//	//***************Product Setting***************//
//	temp=new Vector<MenuItems>();
//	temp.add(createAndAddMenuItems(Screen.CATEGORY,"Category","Product Confiuration/Category"));
//	temp.add(createAndAddMenuItems(Screen.UNITOFMEASUREMENT,"Unit Of Measurement","Product Confiuration/Unit Of Measurement"));
//	sideBarNames.add(new SideBarItems("Product Configuration", temp));
//
//	
//	//******Complain**************//
//	temp=new Vector<MenuItems>();
//	temp.add(createAndAddMenuItems(Screen.COMPLAIN, "Complain", "Complain/Complain"));
//	sideBarNames.add(new SideBarItems("Complain", temp));
//	
//	//**********Complain Config*************//
//	temp=new Vector<MenuItems>();
//	temp.add(createAndAddMenuItems(Screen.COMPLAINCATEGORY,"Complaint Category","Complain Configuration/Complain Category"));
//	temp.add(createAndAddMenuItems(Screen.COMPLAINSTATUS,"Complaint Status","Complain Configuration/Complain Status"));
//	temp.add(createAndAddMenuItems(Screen.COMPLAINPRIORITY,"Compalint Priority","Complain Configuration/Complain Priority"));
//	sideBarNames.add(new SideBarItems("Complain Configuration", temp));
//	
//	
//	
//	
//	//************HR***************//
//	temp=new Vector<MenuItems>();
//	temp.add(createAndAddMenuItems(Screen.EMPLOYEE,"Employee","Human Resource/Employee"));
//	temp.add(createAndAddMenuItems(Screen.LEAVEAPPLICATION,"Leave Application","Human Resource/Leave Application"));
//	temp.add(createAndAddMenuItems(Screen.SHIFT,"Shift","Human Resource/Shift"));
//	temp.add(createAndAddMenuItems(Screen.SHIFTPATTERN,"Shift Pattern","Human Resource/Shift Pattern"));
//	temp.add(createAndAddMenuItems(Screen.ADVANCE,"Advance Request","Human Resource/Advance Request "));
//	temp.add(createAndAddMenuItems(Screen.UPLOADATTENDENCE,"Upload Attendance","Human Resource/Upload Attendance"));
//	temp.add(createAndAddMenuItems(Screen.EMPLOYEEATTENDENCE,"Employee Attendance","Human Resource/Employee Attendance"));
//	temp.add(createAndAddMenuItems(Screen.TIMEREPORT,"Time Report","Human Resource/Time Report"));
//	temp.add(createAndAddMenuItems(Screen.CTC,"CTC","Human Resource/CTC"));
//	temp.add(createAndAddMenuItems(Screen.LEAVEALLOCATION,"Calendar & Leave Allocation ","Human Resource/Calendar & Leave Allocation "));
//	temp.add(createAndAddMenuItems(Screen.LEAVEBALANCE,"Leave Balance","Human Resource/Leave Balance"));
//	temp.add(createAndAddMenuItems(Screen.ASSIGNEDSHIFT, "Assign Shift", "Assign Shift"));
//	temp.add(createAndAddMenuItems(Screen.PROCESSPAYROLL, "Process Payroll", "Human Resource/Process Payroll"));
//
//	
//	sideBarNames.add(new SideBarItems("Human Resource",temp));
//
//
//	//*****HR Config**********//
//	temp=new Vector<MenuItems>();
//	temp.add(createAndAddMenuItems(Screen.EMPLOYEEROLE,"Employee Role","HR Configuration/Employee Role"));
//	temp.add(createAndAddMenuItems(Screen.EMPLOYEEDESIGNATION,"Employee Designation","HR Configurations/Employee Designation"));
//	temp.add(createAndAddMenuItems(Screen.EMPLOYEETYPE,"Employee Type","HR Configuration/Employee Type"));
//	temp.add(createAndAddMenuItems(Screen.DEPARTMENT,"Department","Human Resource/Department"));
//	temp.add(createAndAddMenuItems(Screen.SHIFTCATEGORY,"Shift Category","HR Configuration/Shift Category"));
//	temp.add(createAndAddMenuItems(Screen.PATTERNCATEGORY,"Pattern Category","HR Configuration/Pattern Category"));
//	temp.add(createAndAddMenuItems(Screen.HRPROJECT,"Project","Human Resource/Project"));
//	temp.add(createAndAddMenuItems(Screen.CALENDAR,"Calendar","Human Resource/Calendar"));
//	temp.add(createAndAddMenuItems(Screen.HOLIDAYTYPE,"Holiday Type","HR Configuration/Holiday Type"));
//	temp.add(createAndAddMenuItems(Screen.ADVANCETYPE,"Advance Type","HR Configuration/Advance Type"));
//	temp.add(createAndAddMenuItems(Screen.SALARYHEAD,"CTC Component","Human Resource/CTC Component"));
//	temp.add(createAndAddMenuItems(Screen.ATTENDANCECONFIG,"Attendance Type","Human Resource/Attendance Type"));
//	
//	
//	temp.add(createAndAddMenuItems(Screen.DEDUCTION,"Deductions","HR Configuration/Deductions"));
//	temp.add(createAndAddMenuItems(Screen.LEAVETYPE,"Leave Type","Human Resource/Leave Type"));
//	temp.add(createAndAddMenuItems(Screen.LEAVEGROUP,"Leave Group","HR Configuration/Leave Group"));
//	temp.add(createAndAddMenuItems(Screen.TIMEREPORTCONFIG, "Time Reporting Period", "Time Reporting Configuration"));
//
//	
//
//	
//	sideBarNames.add(new SideBarItems("HR Configuration", temp));
//	
		return sideBarNames;
	}


	/**
	 * AppLevel menu names to be displayed on AppHeaderBar.
	 * @return menuNames
	 */
	@Override
	public String[] getApplevelMenus()
	{

		//Create App Level Menu
		/**
		 * @author Abhinav Bihade
		 * @since 18/12/2019
		 * In Weekly Meeting decision has come out by Nitin Sir, Anil Sir,Sonu Porel and other member also was in Meeting they decided to Remove 'Discard' Button from Framework
		 */
//		String[]menuNames={"Save","Discard",AppConstants.NAVIGATION,"Search","Email","Print","Edit","Download",AppConstants.COMMUNICATIONLOG};
		String[]menuNames={"Save",AppConstants.NAVIGATION,"Search","Email","Print","Edit","Download",AppConstants.COMMUNICATIONLOG,AppConstants.MESSAGE,"Rows:"};
		return menuNames;
	}

	/**
	 * Method template to redirect the screen to the Home page.
	 */
	@Override
	public void goToHome() {
//		RootPanel rp=RootPanel.get();
//		rp.clear();
		Console.log("go to home");
		AppConnector.initalizes();
		AppMemory mem=AppMemory.getAppMemory();
		mem.initialingapp();
		for(int i=0;i<mem.skeleton.menuLabels.length;i++){
			mem.skeleton.menuLabels[i].setVisible(false);
		}
		
//		if(LoginPresenter.loggedInCustomer==true)
//		{
		/**
		 * @author Abhinav
		 * @since 17/10/2019	
		 */
//		Console.log("before initialize");
//		if (LoginPresenter.myCustUserEntity!=null&&LoginPresenter.myCustUserEntity.getCompanyType().equalsIgnoreCase("Facility Management")) {
//			Console.log("inside Statutory Report1111");
//			StatuteryReportPresenter.initalize();
//		}else{
//    	    CustomerDetailsPresenter.initalize();   
//		}
		
		if(AppMemory.getAppMemory().globalDsReportList!=null&&AppMemory.getAppMemory().globalDsReportList.size()!=0&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
			FreshDeskHomeScreen form=new FreshDeskHomeScreen();
		}else if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
			SummaryPresenter.initalize();
		}else if (LoginPresenter.myCustUserEntity!=null&&LoginPresenter.myCustUserEntity.getCompanyType().equalsIgnoreCase("Facility Management")) {
			StatuteryReportPresenter.initalize();
		}else{
			CustomerDetailsPresenter.initalize();
		}
		
	}

	/**
	 * Method to create {@link MenuItems} objects.
	 * @param screen the screen
	 * @param widgettitle title of the widget
	 * @param processname title to be displayed on the AppHeaderBar.
	 * @return MenuItems
	 */
	private MenuItems  createAndAddMenuItems(String module,Screen screen,String widgettitle,String processname)
	{
		/*********************************Old Code*************************************/
		
//		Label lbltemp=new Label(widgettitle);
//		boolean isAuthorized=UserConfiguration.getRole().getAuthorization(screen);
//		MenuItems itemtemp=new MenuItems(lbltemp,screen,processname);
//		//System.out.println("Item tnp="+isAuthorized);
//		//if(isAuthorized)
//		//System.out.println("Item tnp="+isAuthorized);
//	//	if(isAuthorized)
//		{
//		    items.add(itemtemp);
//		    return itemtemp;
//		}
	//return null;
		
		
		/**************************************New Code*******************************************/
		/************************Uncomment Below Code for Normal Deployment***********************/
		
		Label lbltemp=new Label(widgettitle);
		String scren=widgettitle;//***
		boolean isAuthorized=false;
		
		//*************old code for login ********************
//		if(!UserConfiguration.getRole().getRoleName().trim().equals(AppConstants.ADMINUSER)){
//			isAuthorized=UserConfiguration.getRole().getAuthorization(scren,module);//****
//		}
		
		//********new code for login by Anil ***************** 
		if(UserConfiguration.getRole()!=null){
			
			if(!UserConfiguration.getRole().getRoleName().trim().equals(AppConstants.ADMINUSER)){
				isAuthorized=UserConfiguration.getRole().getAuthorization(scren,module);//****
		}
		
//		boolean isAuthorized=UserConfiguration.getRole().getAuthorization(scren);//****
//		boolean isRoleActive=UserConfiguration.getRole().getRoleStatus();
//		System.out.println("Role Active :"+isRoleActive);
		
		
		MenuItems itemtemp=new MenuItems(lbltemp,screen,processname);
		String roleName=UserConfiguration.getRole().getRoleName();
		if(roleName.equalsIgnoreCase(AppConstants.ADMINUSER))
		{
			items.add(itemtemp);
			return itemtemp;
		}
		else
		{
			if(isAuthorized)
			{
				items.add(itemtemp);
				return itemtemp;
			}
		}
		}
			else{
				System.out.println("CREATING MENU ITEMS......");
				MenuItems itemtemp=new MenuItems(lbltemp,screen,processname);
				items.add(itemtemp);
				return itemtemp;
			}
		
		return null;
		
		
	/*******************************Uncomment while deploying for first time***************************/
//		
//		Label lbltemp=new Label(widgettitle);
//		MenuItems itemtemp=new MenuItems(lbltemp,screen,processname);
//		{
//			items.add(itemtemp);
//			return itemtemp;
//		}
//		return null;

	}
	
	
	
	
	private boolean registerationPower()
	{
		String url=Window.Location.getHref();
		System.out.println("URL-------- "+url);
		String[] spliturl=url.split("\\.");
		String companyname=spliturl[0];
		companyname=companyname.replace("http://","");
		/**
		 * @author Anil , Date : 26-08-2019
		 * setting registration option to evaappdemo link
		 */
		String appName=spliturl[1];
		appName=appName.replace("http://","");
		if(companyname.equals("ess")||appName.equals("evaappdemo"))
			return true;
		else
			return false;
	}
	
	public boolean demoPower()
	{
		String url=Window.Location.getHref();
		System.out.println("URL-------- "+url);
		String[] spliturl=url.split("\\.");
		String companyname=spliturl[1];
		String accessUrlName=spliturl[0];
		companyname=companyname.replace("http://","");
		accessUrlName=accessUrlName.replace("http://","");
		if(accessUrlName.equals("ess")||companyname.equals("evaerpdemo")||companyname.equals("evadev001")||companyname.equals("evadev006")||companyname.equals("evaessdemo"))
			return true;
		else
			return false;
	}
	
	
	
	/**
	 *     rohan added this code for showing Expense management to Ankita and for all remaining 
	 *     Multiple Expense should be used 
	 */
	
	public static boolean cheackForExpense()
	{
		String url=Window.Location.getHref();
		System.out.println("URL-------- "+url);
		String[] spliturl=url.split("\\.");
		String companyname=spliturl[1];
		String accessUrlName=spliturl[0];
		companyname=companyname.replace("http://","");
		accessUrlName=accessUrlName.replace("http://","");
		if(companyname.equals("ankitapestcontrolerp"))
			return true;
		else
			return false;		
	}
	
	
	
	private Vector<SideBarItems> createSalesSideBar()
	{
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		
		boolean salesFlag=false;
		boolean salesConfigFlag=false;
		List<ScreenAuthorization> internalModuleValidation=UserConfiguration.getRole().getAuthorization();
		if(!UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)){
			for(int f=0;f<internalModuleValidation.size();f++)
			{
				
				if(internalModuleValidation.get(f).getModule().trim().equals("Sales")){
					salesFlag=true;
				}
					
				if(internalModuleValidation.get(f).getModule().trim().equals("Sales Configuration")){
						salesConfigFlag=true;
				}
			}
		}
		
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||salesFlag==true)
		{
			temp=new Vector<MenuItems>();
//			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.SALESHOME,"Dashboard","Sales/Dashboard")); //Ashwini Patil Commented on 06-08-2024
			/** Updated By: Viraj Date: 15-04-2019 Description: To create lead dashboard in sales **/
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.SALESLEADDASHBOARD,"Lead Dashboard","Sales/Lead Dashboard"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.SALESCUSTOMER,"Customer","Sales/Customer"));
			/** Date 06-11-2017 BY ANIL **/
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.CUSTOMERBRANCH,"Customer Branch","Sales/Customer Branch"));
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.CUSTOMERTRAINING,"Campaign Management","Sales/Campaign Management"));	
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.SALESLEAD,"Lead","Sales/Lead"));
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.SALESQUOTATION,"Quotation","Sales/Quotation"));
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.SALESORDER,"Sales Order","Sales/Sales Order"));
			
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.QUICKSALESORDER,"Quick Sales Order","Sales/Quick Sales Order"));

			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.DELIVERYNOTE,"Delivery Note","Sales/Delivery Note"));
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.WORKORDER,"Work Order","Sales/Work Order"));
			//Ashwini Patil Date: 18-07-2022 Renaming "Interaction To Do" to "Follow Up" as per Nitin sir's Instruction
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.INTERACTIONLIST,"Follow Up List","Sales/Follow Up List"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.INTERACTION,"Follow Up","Sales/Follow Up"));
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.CONTACTPERSONLIST,"Contact Person List","Sales/Contact Person List"));
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.CONTACTPERSONDETAILS,"Contact Person Details","Sales/Contact Person Details"));
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.SALESSECURITYDEPOSITLIST,"Security Deposit List","Sales/Security Deposit List"));
			/** date 25/11/2017 added by komal for customer log details screen in service **/
			temp.add(createAndAddMenuItems(AppConstants.SALESMODULE,Screen.CUSTOMERCALLLOGDETAILS,"Customer Call Log Details","Sales/Customer Call Log Details"));
			sideBarNames.add(new SideBarItems("Sales",temp));
		}
		

		//***************SAles config***************//
		if(UserConfiguration.getRole().getRoleName().trim().equals("ADMIN")||salesConfigFlag==true)
		{
			temp=new Vector<MenuItems>();
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.CUSTOMERGROUP,"Customer Group","Sales Configuration/Customer Group"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.CUSTOMERCATEGORY,"Customer Category","Sales Configuration/Customer Category"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.CUSTOMERTYPE,"Customer Type","Sales Configuration/Customer Type"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.CUSTOMERLEVEL,"Customer Level","Sales Configuration/Customer Level"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.CUSTOMERPRIORITY,"Customer Priority","Sales Configuration/Customer Priority"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.LEADSTATUS,"Lead Status","Sales Configuration/Lead Status"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.LEADGROUP,"Lead Group","Sales Configuration/Lead Group"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.LEADCATEGORY,"Lead Category","Sales Configuration/Lead Category"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.LEADTYPE,"Lead Type","Sales Configuration/Lead Type"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.LEADPRIORITY,"Lead Priority","Sales Configuration/Lead Priority"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.SALESQUOTATIONGROUP,"Sales Quotation Group","Sales Configuration/Sales Quotation Group"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.SALESQUOTATIONCATEGORY,"Sales Quotation Category","Sales Configuration/Sales Quotation Category"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.SALESQUOTATIONTYPE,"Sales Quotation Type","Sales Configuration/Sales Quotation Type"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.SALESQUOTATIONPRIORITY,"Sales Quotation Priority","Sales Configuration/Sales Quotation Priority"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.SALESORDERGROUP,"Sales Order Group","Sales Configuration/Sales Order Group"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.SALESORDERCATEGORY,"Sales Order Category","Sales Configuration/Sales Order Category"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.SALESORDERTYPE,"Sales Order Type","Sales Configuration/Sales Order Type"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.DELIVERYNOTEGROUP,"Delivery Note Group","Sales Configuration/Delivery Note Group"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.DELIVERYNOTECATEGORY,"Delivery Note Category","Sales Configuration/Delivery Note Category"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.DELIVERYNOTETYPE,"Delivery Note Type","Sales Configuration/Delivery Note Type"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.WORKORDERGROUP,"Work Order Group","Sales Configuration/Work Order Group"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.WORKORDERCATEGORY,"Work Order Category","Sales Configuration/Work Order Category"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.WORKORDERTYPE,"Work Order Type","Sales Configuration/Work Order Type"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.CHECKLISTSTEP,"Checklist Step","Sales Configuration/Checklist Step"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.CHECKLISTTYPE,"Checklist Type","Sales Configuration/Checklist Type"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.SHIPPINGMETHOD,"Shipping Method","Sales Configuration/Shipping Method"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.PACKINGMETHOD,"Packing Method","Sales Configuration/Packing Method"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.PERSONTYPE,"Entity Type","Sales Configuration/Entity Type"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.GROUPTYPE,"Interaction Group","Sales Configuration/Interaction Group"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.COMMUNICATIONCATEGORY,"Communication Category","Sales Configuration/Communication Category"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.COMMUNICATIONTYPE,"Communication Type","Sales Configuration/Communication Type"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.INTERACTIONPRIORITY,"Interaction Priority","Sales Configuration/Interaction Priority"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.CONTACTROLE,"Contact Role","Sales Configuration/Contact Role"));
			/** date 05/12/2017 added by komal for hvac (warranty and call type status **/
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.WARRANTYSTATUS,"Warranty Status","Sales Configuration/Warranty Status"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.CALLTYPE,"Call Type","Sales Configuration/Call Type"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.CALLBACKSTATUS,"Callback Status","Sales Configuration/Callback Status"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.VISITSTATUS,"Visit Status","Sales Configuration/Visit Status"));
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.CALLLOGSTATUS,"Call Log Status","Sales Configuration/Call Log Status"));

			/** Date 16-6-2018 By jayshree **/
			temp.add(createAndAddMenuItems(AppConstants.SALESCONFIG,Screen.LEADUNSUCCESFULREASONS,"Lead Unsuccesful Reasons","Sales Configuration/Lead Unsuccesful Reasons"));
			
			sideBarNames.add(new SideBarItems("Sales Configuration",temp));
		}
		return sideBarNames;
	}
	
	
	private Vector<SideBarItems> createInventorySideBar()
	{
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		
		boolean inventoryFlag=false;
		boolean inventoryConfigFlag=false;
		List<ScreenAuthorization> internalModuleValidation=UserConfiguration.getRole().getAuthorization();
		if(!UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)){
			for(int f=0;f<internalModuleValidation.size();f++)
			{
				
				if(internalModuleValidation.get(f).getModule().trim().equals("Inventory")){
					inventoryFlag=true;
				}
					
				if(internalModuleValidation.get(f).getModule().trim().equals("Inventory Configuration")){
					inventoryConfigFlag=true;
				}
			}
		}
		
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||inventoryFlag==true)
		{
			temp=new Vector<MenuItems>();
//			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.INVENTORYHOME,"Dashboard","Dashboard/Inventory"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.PRODUCTINVENTORYTRANSACTION,"Inventory Transaction List","Inventory Transaction List"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.PRODUCTINVENTORYVIEW,"Product Inventory View","Inventory/Product Inventory View"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.PRODUCTINVENTORYVIEWLIST,"Product Inventory List","Product Inventory List"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.STOCKALERT,"Stock Alert","Inventory/Stock Alert"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.GRNLIST,"GRN List","Inventory/GRN List"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.GRN,"GRN","Inventory/GRN"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.INSPECTIONLIST,"Inspection List","Inventory/Inspection List"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.INSPECTION,"Inspection","Inventory/Inspection"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.MATERIALREQUESTNOTE,"Material Request Note","Material Request Note"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.MATERIALISSUENOTE,"Material Issue Note","Material Issue Note"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.MATERIALMOVEMENTNOTE,"Material Movement Note","Material Movement Note"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.WAREHOUSEDETAILS,"Physical Inventory","Inventory/Physical Inventory"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.MATERIALCONSUMPTIONREPORT,"Contract P & L","Contract P & L"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.INTERACTIONLIST,"Follow Up List","Inventory/Follow Up List"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.INTERACTION,"Follow Up","Inventory/Follow Up"));
			/** date 25/11/2017 added by komal for hvac defecrive material list **/
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.DEFECTIVEMATERIALLIST,"Defective Material List","Inventory/Defective Material List"));
			/** date 01-04-2019 added by Viraj for inventory list for franchise **/
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.INVENTORYlIST,"Inventory List","Inventory/Inventory List"));
				/** date 7.3.2018 added by komal for to assign warehouse to technician**/
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYMODULE,Screen.TECHNICIANWAREHOUSEDETAILS,"Technician WareHouse Details","Inventory/Technician WareHouse Details"));
			sideBarNames.add(new SideBarItems("Inventory", temp));
		}
		
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||inventoryConfigFlag==true)
		{
			temp=new Vector<MenuItems>();
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.WAREHOUSE,"Warehouse","Inventory/Warehouse"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.WAREHOUSETYPE,"Warehouse Type","Inventory/Warehouse Type"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.STORAGELOCATION,"Storage Location","Inventory/Storage Location"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.STORAGEBIN,"Storage Bin","Inventory/Storage Bin"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.GRNGROUP,"GRN Group","Inventory Configuration/GRN Group"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.GRNCATEGORY,"GRN Category","Inventory Configuration/GRN Category"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.GRNTYPE,"GRN Type","Inventory Configuration/GRN Type"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.INSPECTIONGROUP,"Inspection Group","Inventory Configuration/Inspection Group"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.INSPECTIONCATEGORY,"Inspection Category","Inventory Configuration/Inspection Category"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.INSPECTIONTYPE,"Inspection Type","Inventory Configuration/Inspection Type"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.MRNCATEGORY,"MRN Category","Inventory Configuration/MRN Category"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.MRNTYPE,"MRN Type","Inventory Configuration/MRN Type"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.MINCATEGORY,"MIN Category","Inventory Configuration/MIN Category"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.MINTYPE,"MIN Type","Inventory Configuration/MIN Type"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.MMNCATEGORY,"MMN Category","Inventory Configuration/MMN Category"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.MMNTYPE,"MMN Type","Inventory Configuration/MMN Type"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.MMNDIRECTION,"MMN Direction","Inventory Configuration/MMN Direction"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.MMNTRANSACTIONTYPE,"MMN Transaction Type","MMN Transaction Type"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.CHECKLISTSTEP,"Checklist Step","Inventory Configuration/Checklist Step"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.CHECKLISTTYPE,"Checklist Type","Inventory Configuration/Checklist Type"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.INSPECTIONMETHOD,"Inspection Method","Inspection Method"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.PERSONTYPE,"Entity Type","Inventory Configuration/Entity Type"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.GROUPTYPE,"Interaction Group","Inventory Configuration/Interaction Group"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.COMMUNICATIONCATEGORY,"Communication Category","Inventory Configuration/Communication Category"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.COMMUNICATIONTYPE,"Communication Type","Inventory Configuration/Communication Type"));
			temp.add(createAndAddMenuItems(AppConstants.INVENTORYCONFIG,Screen.INTERACTIONPRIORITY,"Interaction Priority","Inventory Configuration/Interaction Priority"));
			sideBarNames.add(new SideBarItems("Inventory Configuration",temp));
		}
		
		return sideBarNames;
		
					
					
	}
	
	
	private Vector<SideBarItems> createServiceSideBar()
	{
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		//****************Service******************************//
		
		boolean serviceFlag=false;
		boolean serviceConfigFlag=false;
		List<ScreenAuthorization> internalModuleValidation=UserConfiguration.getRole().getAuthorization();
		if(!UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)){
			for(int f=0;f<internalModuleValidation.size();f++)
			{
				
				if(internalModuleValidation.get(f).getModule().trim().equals("Service")){
					serviceFlag=true;
				}
					
				if(internalModuleValidation.get(f).getModule().trim().equals("Service Configuration")){
					serviceConfigFlag=true;
				}
			}
		}
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||serviceFlag==true)
		{
			temp=new Vector<MenuItems>();
//			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.HOME,"Dashboard","Service/Dashboard"));//Ashwini Patil Commented on 06-08-2024
			
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.LEADDASHBOARD,"Lead Dashboard","Service/Lead Dashboard"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.QUOTATIONDASHBOARD,"Quotation Dashboard","Service/Quotation Dashboard"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CONTRACTDASHBOARD,"Contract Dashboard","Service/Contract Dashboard"));
			
			/**29-12-2018 contract renewal dashboard  by AMOL**/
	//		temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CONTRACTRENEWALDASHBOARD,"Contract Renewal Dashboard","Service/Contract Renewal Dashboard"));
			
			
			
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.COMPLAINDASHBOARD,"Complain Dashboard","Service/Complain Dashboard"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CUSTOMER,"Customer","Service/Customer"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CUSTOMERLIST,"Customer List","Service/Customer List"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CUSTOMERRECORD,"Customer Help Desk","Customer Help Desk"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CUSTOMERTRAINING,"Campaign Management","Service/Campaign Management"));	
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CUSTOMERBRANCH,"Customer Branch","Service/Customer Branch"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.LEAD,"Lead","Service/Lead"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.QUOTATION,"Quotation","Service/Quotation"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CONTRACT,"Contract","Service/Contract"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.QUICKCONTRACT,"Quick Contract","Service/Quick Contract"));
			/*** Date 16-08-2019 by Vijay for NBHC CCPM Contract uploading data ***/ 
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.TRANSACTIONUPLOADING,"Transactions Upload","Service/Transactions Upload"));
		
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CNC,"CNC","Service/CNC"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.SERVICE, "Customer Service List", "Service/Customer Service List"));
			
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.INHOUSESERVICE, "In-House Service List", "Service/In-House Service List"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.TECHNICIANSCHEDULNG, "Technician Scheduling list", "Service/Technician Scheduling list"));//Ashwini Patil Commented on 06-08-2024

			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.DEVICE, "Customer Service", "Service/Customer Service"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.PROJECT, "Customer Project", "Service/Customer Project"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE, Screen.TECHNICIANDASHBOARD, "Technician Dashboard", "Service/Technician Dashboard"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE, Screen.SALESPERSONDASHBOARD, "Sales Person Dashboard", "Service/Sales Person Dashboard"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE, Screen.SALESPERSONTARGET, "Sales Person Targets", "Service/Sales Person Targets"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE, Screen.SALESPERSONPERFORMANCE, "Sales Person Targets Performance", "Service/Sales Person Targets Performance"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.ASSESMENTREPORT, "Assessment Report", "Service/Assessment Report"));
		
			/**Date :19/11/2017 By:Manisha
			 * To create Assessment Scheduling List in Service Module
			 */
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE, Screen.ASSESSMENTSCHEDULING, "Assessment Scheduling List", "Service/Assessment Scheduling List"));
			/**End * */
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.FUMIGATION, "Fumigation", "Service/Fumigation"));
			/** date 06/04/18 added by komal for fumigation ALP **/
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.FUMIGATIONALP, "Fumigation(ALP)", "Service/Fumigation(ALP)"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.FUMIGATIONAUSTRALIA, "Fumigation(AFAS)", "Service/Fumigation(AFAS)"));
			/** Date 23-08-2019 by Vijay for NBH CCPM ****/
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CUSTOMERSUPPORT,"Customer Support New","Service/Customer Support"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CUSTOMERSUPPORTDASHBOARD,"Customer Support Dashboard","Service/Customer Support Dashboard"));//Ashwini Patil Commented on 06-08-2024

//			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.FUMIGATIONAUSTRALIA, "Fumigation(Aus)", "Service/Fumigation(Aus)"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.LIAISONSTEPS, "Liasion Steps", "Service/Liasion Steps"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.LIAISONGROUP, "Liasion Group", "Service/Liasion Group"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.LIASION, "Liasion", "Service/Liasion"));	
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.COMPLAIN,"Customer Support","Service/Customer Support"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CONTRACTRENEWAL,"Contract Renewal","Service/Contract Renewal"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.SCHEDULINGANDROUTING,"Scheduling And Routing","Service/Scheduling And Routing"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.TEAMMANAGEMENT,"Team Management","Service/Team Management"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.INTERACTIONLIST,"Follow Up List","Service/Follow Up List"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.INTERACTION,"Follow Up","Service/Follow Up"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CONTACTPERSONLIST,"Contact Person List","Service/Contact Person List"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CONTACTPERSONDETAILS,"Contact Person Details","Service/Contact Person Details"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.SERVICESECURITYDEPOSITLIST,"Security Deposit List","Service/Security Deposit List"));
			 /** dare 04/12/2017 added by komal for sales orders without amc list **/
//			temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CONTRACTLIST,"Contract List","Service/Contract List"));//Ashwini Patil Commented on 06-08-2024
			sideBarNames.add(new SideBarItems("Service", temp));
		}
		
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||serviceConfigFlag==true)
		{
			temp=new Vector<MenuItems>();
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.SALUTATION,"Salutation","Service Configuration/Salutation"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.CUSTOMERGROUP,"Customer Group","Service Configuration/Customer Group"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.CUSTOMERCATEGORY,"Customer Category","Service Configuration/Customer Category"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.CUSTOMERTYPE,"Customer Type","Service Configuration/Customer Type"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.CUSTOMERLEVEL,"Customer Level","Service Configuration/Customer Level"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.CUSTOMERPRIORITY,"Customer Priority","Service Configuration/Customer Priority"));
		
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.RELIGION,"Religion","Service Configuration/Religion"));
			
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.LEADSTATUS,"Lead Status","Service Configuration/Lead Status"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.LEADGROUP,"Lead Group","Service Configuration/Lead Group"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.LEADCATEGORY,"Lead Category","Service Configuration/Lead Category"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.LEADTYPE,"Lead Type","Service Configuration/Lead Type"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.LEADPRIORITY,"Lead Priority","Service Configuration/Lead Priority"));
			
			/**
			 * @author Abhinav Bihade
			 * @since 28/01/2020
			 * Move Lead unsuccessful reason (sales module) to service module after lead priority
			 */
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.LEADUNSUCCESFULREASONS,"Lead Unsuccesful Reasons","Service Configuration/Lead Unsuccesful Reasons"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.QUOTATIONGROUP,"Quotation Group","Service Configuration/Quotation Group"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.QUOTATIONCATEGORY,"Quotation Category","Service Configuration/Quotation Category"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.QUOTATIONTYPE,"Quotation Type","Service Configuration/Quotation Type"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.QUOTATIONPRIORITY,"Quotation Priority","Service Configuration/Quotation Priority"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.CONTRACTGROUP,"Contract Group","Service Configuration/Contract Group"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.CONTRACTCATEGORY,"Contract Category","Service Configuration/Contract Category"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.CONTRACTTYPE,"Contract Type","Service Configuration/Contract Type"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.SERVICETYPE,"Service Type","Service Configuration/Service Type"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.DOCUMENTTYPE,"Document Type","Service Configuration/Document Type"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.SPECIFICREASONFORRESCHEDULING,"Specific Reason","Service Configuration/Specific Reason"));
			/**Added by sheetal:20-12-2021 Renaming Specific reason to Service Reschedule Reason**/
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.SPECIFICREASONFORRESCHEDULING,"Service Reschedule Reason","Service Configuration/Service Reschedule Reason"));
			/**Date :17/02/2018 BY: Manisha 
			 * Create master for frequency and TypeOfTreatment as per sonu's requirement.!! 
			 */
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.FREQUENCY1,"Frequency","Service Configuration/Frequency"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.TYPEOFTREATMENT,"Type Of Treatment","Service Configuration/Type Of Treatment"));
			/**End**/
			/*** Date 19-03-2019 by Vijay for Do not renewal Message configuration NBHC CCPM
			 */
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG, Screen.DONOTRENEW, "Do Not Renew Remark", "Service Configuration/DO Not Renew Remark"));
			/** 
			 * Date 23-03-2019 by Vijay for cancellation remark message with configuration NBHC CCPM
			 */
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG, Screen.CANCELLATIONREMARK, "Cancellation Remark", "Service Configuration/Cancellation Remark"));
			
			/**@author Sheetal : 28-05-2022 , Des : added Tax classification code screen for envocare**/
		
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.TAXCLASSIFICATIONCODE, "Tax Classification Code","Service Configuration/Tax Classification Code"));
			
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.BILLOFMATERIAL,"Bill Of Material","Service/Bill Of Material"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.PERSONTYPE,"Entity Type","Service Configuration/Entity Type"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.GROUPTYPE,"Interaction Group","Service Configuration/Interaction Group"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.COMMUNICATIONCATEGORY,"Communication Category","Service Configuration/Communication Category"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.COMMUNICATIONTYPE,"Communication Type","Service Configuration/Communication Type"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.INTERACTIONPRIORITY,"Interaction Priority","Service Configuration/Interaction Priority"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.CONTACTROLE,"Contact Role","Service Configuration/Contact Role"));
//			
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.FUMIGATIONDECLAREDPOE,"Declare point of Entry","Service Configuration/Declare point of Entry"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.DOSAGERATEOFFUMIGANT,"Dosage rate of fumigant","Service Configuration/Dosage rate of fumigant"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.DURATIONOFFUMIGATION,"Duration of fumigation","Service Configuration/Duration of fumigation"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.MINIMUMAIRTEMP,"Minimum air temperature","Service Configuration/Minimum air temperature"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.QUANTITYDECLAREUNIT,"Quantity Declare Unit","Service Configuration/Quantity Declare Unit"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.PLACEOFFUMIGATION,"Place of Fumigation","Service Configuration/Place of Fumigation"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.COUNTRYOFLOADING,"Port and Country of Loading","Service Configuration/Port and Country of Loading"));
//			
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.DECLARATION,"Fumigation Declaration","Service Configuration/Fumigation Declaration"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.COMPLAINPRIORITY,"Complain Priority","Sales Configuration/Complain Priority"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.COMPLAINSTATUS,"Complain Status","Sales Configuration/Complain Status"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.NUMBERRANGE,"Number Range","Service Configuration/Number Range"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.CATCHTRAPPESTNAME,"Catch Pest Name","Catch Pest Name"));
//			
//			
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.ASSESSMENTREPORTCATEGORY,"Assessment Report Category","Assessment Report Category"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.ASSESSMENTREPORTDEFICIENCYTYPE,"Assessment Report Deficiency Type","Assessment Report Deficiency Type"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.ACTIONPLANFORCOMPANY,"Action Plan For Company","Action Plan For Company"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.ACTIONPLANFORCUSTOMER,"Action Plan For Customer","Action Plan For Customer"));
//			
			sideBarNames.add(new SideBarItems("Service Configuration", temp));
		}
		
		//  rohan added this code to create 
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||serviceConfigFlag==true)
		{
			temp=new Vector<MenuItems>();
		
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.BILLOFMATERIAL,"Bill Of Material","Service/Bill Of Material"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.PERSONTYPE,"Entity Type","Service Configuration/Entity Type"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.GROUPTYPE,"Interaction Group","Service Configuration/Interaction Group"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.COMMUNICATIONCATEGORY,"Communication Category","Service Configuration/Communication Category"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.COMMUNICATIONTYPE,"Communication Type","Service Configuration/Communication Type"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.INTERACTIONPRIORITY,"Interaction Priority","Service Configuration/Interaction Priority"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.CONTACTROLE,"Contact Role","Service Configuration/Contact Role"));
			
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.FUMIGATIONDECLAREDPOE,"Declare point of Entry","Service Configuration/Declare point of Entry"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.DOSAGERATEOFFUMIGANT,"Dosage rate of fumigant","Service Configuration/Dosage rate of fumigant"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.DURATIONOFFUMIGATION,"Duration of fumigation","Service Configuration/Duration of fumigation"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.MINIMUMAIRTEMP,"Minimum air temperature","Service Configuration/Minimum air temperature"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.QUANTITYDECLAREUNIT,"Quantity Declare Unit","Service Configuration/Quantity Declare Unit"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.PLACEOFFUMIGATION,"Place of Fumigation","Service Configuration/Place of Fumigation"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.COUNTRYOFLOADING,"Port and Country of Loading","Service Configuration/Port and Country of Loading"));
			
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.DECLARATION,"Fumigation Declaration","Service Configuration/Fumigation Declaration"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.COMPLAINPRIORITY,"Complain Priority","Sales Configuration/Complain Priority"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.COMPLAINSTATUS,"Complain Status","Sales Configuration/Complain Status"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.NUMBERRANGE,"Number Range","Service Configuration/Number Range"));
//			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.CATCHTRAPPESTNAME,"Catch Pest Name","Catch Pest Name"));
			/**Added by sheetal:20-12-2021 Renaming Catch Pest Name to Finding Type**/
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.CATCHTRAPPESTNAME,"Finding Type","Finding Type"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.SERVICECHECKLIST,"Service Checklist","Service Checklist"));
			
			
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.ASSESSMENTREPORTCATEGORY,"Assessment Report Category","Assessment Report Category"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.ASSESSMENTREPORTDEFICIENCYTYPE,"Assessment Report Deficiency Type","Assessment Report Deficiency Type"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.ASSESSMENTREPORTUNIT,"Assessment Report Unit","Assessment Report Unit"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.ACTIONPLANFORCOMPANY,"Action Plan For Company","Action Plan For Company"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.ACTIONPLANFORCUSTOMER,"Action Plan For Customer","Action Plan For Customer"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.ASSESSMENTRISKLEVEL,"Risk Level","Risk Level"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.SERVICEUOM,"Unit of Measurement","Unit of Measurement"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.SERVICEUNITCONVERSION,AppConstants.UNITCONVERSION,"Service Configuration/"+AppConstants.UNITCONVERSION)); /** nidhi 7-09-2018 for unit conversion *:*:* */
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.CONTRACTPERIOD,"Contract Period","Service Configuration/Contract Period"));
			/**
			 * Date 27-7-2018 by jayshree
			 */
			
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.FUMIGATIONPERFORMED,"Fumigation Performed","Service Configuration/Fumigation Performed"));
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.VENTILATED,"Ventilated","Service Configuration/Ventilated"));
			/*** Date 16-10-2018 By Vijay for Complain Ticket Category ***/
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.TICKETCATEGORY,"Ticket Category","Service Configuration/Ticket Category"));
           /**date 14-11-2020 by Amol added company profile details**/
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.COMPANYPROFILECONFIGURATION,AppConstants.COMPANYPROFILECONFIG,"Service Configuration/"+AppConstants.COMPANYPROFILECONFIG));
		
			temp.add(createAndAddMenuItems(AppConstants.SERVICECONFIG,Screen.TECHNICIANREMARK,AppConstants.TECHNICIANREMARK,"Service Configuration/"+AppConstants.TECHNICIANREMARK));

			sideBarNames.add(new SideBarItems("Service Configuration", temp));
		}
		
		return sideBarNames;
	}
	
	private Vector<SideBarItems> createEmployeeSelfServiceSideBar()
	{
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		//************HR***************//
		
		boolean empselfserviceFlag=false;
		List<ScreenAuthorization> internalModuleValidation=UserConfiguration.getRole().getAuthorization();
		if(!UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)){
			for(int f=0;f<internalModuleValidation.size();f++)
			{
				
				if(internalModuleValidation.get(f).getModule().trim().equals("Employee Self Service")){
					empselfserviceFlag=true;
				}
			}
		}
		
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||empselfserviceFlag==true)
		{
			temp=new Vector<MenuItems>();
			temp.add(createAndAddMenuItems(AppConstants.EMPSELFSERVICEMODULE,Screen.LEAVEAPPLICATION,"Leave Application","Human Resource/Leave Application"));
			temp.add(createAndAddMenuItems(AppConstants.EMPSELFSERVICEMODULE,Screen.ADVANCE,"Advance Request","Human Resource/Advance Request "));
			temp.add(createAndAddMenuItems(AppConstants.EMPSELFSERVICEMODULE,Screen.EMPLOYEEATTENDENCE,"Employee Attendance","Human Resource/Employee Attendance"));
			temp.add(createAndAddMenuItems(AppConstants.EMPSELFSERVICEMODULE,Screen.TIMEREPORT,"Time Report","Human Resource/Time Report"));
			temp.add(createAndAddMenuItems(AppConstants.EMPSELFSERVICEMODULE,Screen.CTC,"CTC","Human Resource/CTC"));
			temp.add(createAndAddMenuItems(AppConstants.EMPSELFSERVICEMODULE,Screen.LEAVEBALANCE,"Leave Balance","Human Resource/Leave Balance"));
			sideBarNames.add(new SideBarItems("Self Service",temp));
		}
        return sideBarNames;
		
	}
	
	
	public Vector<SideBarItems> createHumanResourceSideBar()
	{
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		//************HR***************//
		
		boolean hrFlag=false;
		boolean hrConfigFlag=false;
		boolean hrStatutorydeductionflag=false;
		List<ScreenAuthorization> internalModuleValidation=UserConfiguration.getRole().getAuthorization();
		if(!UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)){
			for(int f=0;f<internalModuleValidation.size();f++)
			{
				
				if(internalModuleValidation.get(f).getModule().trim().equals("HR Admin")){
					hrFlag=true;
				}
					
				if(internalModuleValidation.get(f).getModule().trim().equals("HR Configuration")){
						hrConfigFlag=true;
				}
				if(internalModuleValidation.get(f).getModule().trim().equals("Statutory Deduction")){
					hrStatutorydeductionflag=true;
				}
				
				
				}
		}
		
		if(UserConfiguration.getRole().getRoleName().trim().equals("ADMIN")||hrFlag==true)
		{
			temp=new Vector<MenuItems>();
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.EMPLOYEE,"Employee","Human Resource/Employee"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.EMPLOYEEADDITIONALDETAILS,"Employee Additional Details","Human Resource/Employee Additional Details"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.LEAVEAPPLICATION,"Leave Application","Human Resource/Leave Application"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.ADVANCE,"Advance Request","Human Resource/Advance Request "));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.UPLOADATTENDENCE,"Upload Attendance","Human Resource/Upload Attendance"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.CREATEATTENDANCE,"Create Attendance","Human Resource/Create Attendance"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.EMPLOYEEATTENDENCE,"Employee Attendance","Human Resource/Employee Attendance"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.TIMEREPORT,"Time Report","Human Resource/Time Report"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.CTC,"CTC","Human Resource/CTC"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.CTCALLOCATION,"CTC Allocation","Human Resource/CTC Allocation"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.LEAVEBALANCE,"Leave Balance","Human Resource/Leave Balance"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.EMPLOYEELEAVEHISTORY,"Employee Leave History","Human Resource/Employee Leave History"));
			
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.EMPLOYEEOVERTIMEHISTORY,"Employee Overtime History","Human Resource/Employee Overtime History"));
//			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.ASSIGNEDSHIFT, "Assign Shift", "Assign Shift"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.PROCESSPAYROLL, "Process Payroll", "Human Resource/Process Payroll"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.SALARYSLIP, "Salary Slip", "Human Resource/Salary Slip"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.COMPANYSCONTRIBUTION, "Company's Contribution", "Human Resource/Company's Contribution"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.INTERACTIONLIST,"Follow Up List","Human Resource/Follow Up List"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.INTERACTION,"Follow Up","Human Resource/Follow Up"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.CONTACTPERSONLIST,"Contact Person List","Human Resource/Contact Person List"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.CONTACTPERSONDETAILS,"Contact Person Details","Human Resource/Contact Person Details"));
			
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.PROJECTALLOCATION, "Project Allocation", "Human Resource/Project Allocation"));		
			
			/**
			 * Date 2-7-2018 by jayshree
			 * Statutory
			 */
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.STATUTERYREPORT, "Statutory Reports", "Human Resource/Statutory Reports"));	
			/** date 11.7.2018 added by komal for attendance list**/
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.ATTENDANCElIST, "Attendance Report", "Human Resource/Attendance Report"));

			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.EMPLOYEEINVESTMENT,"Employee Investment","Human Resource/Employee Investment"));
			/** end komal **/
			/** date 24.7.2018 added by komal for employee asset allocation  **/
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.EMPLOYEEASSETALLOCATION, "Employee Asset Allocation", "Human Resource/Employee Asset Allocation"));
			/** date 03.08.2018 added by komal for employee arrears management **/
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.ARREARSMANAGEMENT, "Adhoc Settlement", "Human Resource/Adhoc Settlement"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.VOLUNTARYPF,"Voluntary PF","Human Resource/Voluntary PF"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.VOLUNTARYPFHISTORY,"Voluntary PF History","Human Resource/Voluntary PF History"));
			temp.add(createAndAddMenuItems(AppConstants.HRMODULE,Screen.COMPLIANCEFORM,"Compliance Forms","Human Resource/Compliance Forms"));
			
			sideBarNames.add(new SideBarItems("Human Resource",temp));
		}

		//*****HR Config**********//
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||hrConfigFlag==true)
		{
			temp=new Vector<MenuItems>();
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.EMPLOYEEROLE,"Employee Role","HR Configuration/Employee Role"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.EMPLOYEEDESIGNATION,"Employee Designation","HR Configurations/Employee Designation"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.EMPLOYEETYPE,"Employee Type","HR Configuration/Employee Type"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.DEPARTMENT,"Department","HR Configuration/Department"));
			
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.EMPLOYEEFAMILYRELATION,"Employee Family Relation","HR Configuration/Employee Family Relation"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.ADDRESSTYPE,"Address Type","HR Configuration/Address Type"));
			
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.SHIFT,"Shift","HR Configuration/Shift"));
//			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.SHIFTPATTERN,"Shift Pattern","HR Configuration/Shift Pattern"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.SHIFTCATEGORY,"Shift Category","HR Configuration/Shift Category"));
//			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.PATTERNCATEGORY,"Pattern Category","HR Configuration/Pattern Category"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.HRPROJECT,"Project","HR Configuration/Project"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.HOLIDAYTYPE,"Holiday Type","HR Configuration/Holiday Type"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.CALENDAR,"Calendar","HR Configuration/Calendar"));
//			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.ATTENDANCECONFIG,"Attendance Type","HR Configuration/Attendance Type"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.LEAVETYPE,"Leave Type","HR Configuration/Leave Type"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.LEAVEGROUP,"Leave Group","HR Configuration/Leave Group"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.OVERTIME,"Overtime","HR Configuration/Overtime"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.LEAVEALLOCATION,"Calendar & Leave Allocation","HR Configuration/Calendar & Leave Allocation "));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.ADVANCETYPE,"Advance Type","HR Configuration/Advance Type"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.SALARYHEAD,"CTC Component","HR Configuration/CTC Component"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.DEDUCTION,"Deductions","HR Configuration/Deductions"));
			
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.CTCTEMPLATE,AppConstants.CTCTEMPLATE,"HR Configuration/"+AppConstants.CTCTEMPLATE));
			
//			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.TIMEREPORTCONFIG, "Time Reporting Period", "HR Configuration/Time Reporting Configuration"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.PERSONTYPE,"Entity Type","HR Configuration/Entity Type"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.GROUPTYPE,"Interaction Group","HR Configuration/Interaction Group"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.COMMUNICATIONCATEGORY,"Communication Category","HR Configuration/Communication Category"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.COMMUNICATIONTYPE,"Communication Type","HR Configuration/Communication Type"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.INTERACTIONPRIORITY,"Interaction Priority","HR Configuration/Interaction Priority"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.CONTACTROLE,"Contact Role","HR Configuration/Contact Role"));
			/** date 18.7.2018 added by komal**/
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.TAXATIONYEAR,"Taxation Year","HR Configuration/Taxation Year"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.TAXCATEGORY,"Tax Category","HR Configuration/Tax Category"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.SECTIONNAME,"Section Name","HR Configuration/Section Name"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.TAXSLABCONFIGURATION,"Tax Slab Configuration","HR Configuration/Tax Slab Configuration"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.INVESTENTCONFIGURATION,"Investment Configuration","HR Configuration/Investment Configuration"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.EMPLOYEECHECKLISTTYPE,AppConstants.EMPLOYEECHECKLISTTYPE,"HR Configuration/"+AppConstants.EMPLOYEECHECKLISTTYPE));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.TRAINING,"Training Type","HR Configuration/Training Type"));
//			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.PROFESSIONALTAX,"Professional Tax","HR Configuration/Professional Tax"));
			/** date 31.8.2018 added by komal**/
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.CTCCATEGORY,"CTC Category","HR Configuration/CTC Category"));
			/** date 6.10..2018 added by komal**/
//			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.LWF,"LWF","HR Configuration/LWF"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.PAIDLEAVE,"Paid Leave","HR Configuration/Paid Leave"));
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.BONUS,"Bonus","HR Configuration/Bonus"));
			/**Date 1-10-2019 added by Amol **/
			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.CASTE,"Caste","HR Configuration/Caste"));
//			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.PROVIDENTFUND,"Provident Fund","HR Configuration/Provident Fund"));
//			temp.add(createAndAddMenuItems(AppConstants.HRCONFIG,Screen.ESIC,"Esic","HR Configuration/Esic"));
			sideBarNames.add(new SideBarItems("HR Configuration", temp));
		}
		/**Date 24-8-2019 by Amol Added new Module "Statutary Deduction" ***/
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||hrStatutorydeductionflag==true){
			temp=new Vector<MenuItems>();
			
			temp.add(createAndAddMenuItems(AppConstants.HRSTAUTORYDEDUCTION,Screen.PROVIDENTFUND,"Provident Fund","Statutory Deduction/Provident Fund"));
			temp.add(createAndAddMenuItems(AppConstants.HRSTAUTORYDEDUCTION,Screen.ESIC,"Esic","Statutory Deduction/Esic"));
			temp.add(createAndAddMenuItems(AppConstants.HRSTAUTORYDEDUCTION,Screen.PROFESSIONALTAX,"Professional Tax","Statutory Deduction/Professional Tax"));
			temp.add(createAndAddMenuItems(AppConstants.HRSTAUTORYDEDUCTION,Screen.LWF,"LWF","Statutory Deduction/LWF"));
			sideBarNames.add(new SideBarItems("Statutory Deduction", temp));
		}
		return sideBarNames;
	}
	
	
	private Vector<SideBarItems> createBillingSideBar()
	{
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		//******************Account***********************//
		
		boolean accountsFlag=false;
		boolean accountsConfigFlag=false;
		List<ScreenAuthorization> internalModuleValidation=UserConfiguration.getRole().getAuthorization();
		if(!UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)){

			for(int f=0;f<internalModuleValidation.size();f++)
			{
				
				if(internalModuleValidation.get(f).getModule().trim().equals("Accounts")){
					accountsFlag=true;
				}
					
				if(internalModuleValidation.get(f).getModule().trim().equals("Account Configuration")){
					accountsConfigFlag=true;
				}
			}
		}	
		
		/*****************************************First Time Deployment**************************************/
		//accountsConfigFlag=true;
		
		
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||accountsFlag==true)
		{
			temp=new Vector<MenuItems>();
//			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.ACCOUNTDASHBOARD,"Dashboard (AR)","Accounts/Dashboard  (AR)"));//Ashwini Patil Commented on 06-08-2024
//			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.ACCOUNTSDASHBOARDAP,"Dashboard (AP)","Accounts/Dashboard (AP)"));//Ashwini Patil Commented on 06-08-2024
			/** date 02-04-2019 added by viraj for billed unbilled dashboard **/
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.BILLEDUNBILLED,"Billed & Unbilled Dashboard","Accounts/Billed & Unbilled Dashboard"));
			
			/**date 29-12-2018 added by amol for payment dashboard***/
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.PAYMENTDASHBOARD,"Payment Dashboard(AR)","Accounts/Payment Dashboard"));//Ashwini Patil Commented on 06-08-2024
			
			
			if(AppUtility.checkIsItServerSide() && cheackForExpense()){
				temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.EXPENSEMANAGMENT,"Expense Management","Accounts/Expense Management"));
			}
			else
			{
				//  rohan added this code for multiple expense mngt
				temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.MULTIPLEEXPENSEMANAGMENT,"Expense Management","Accounts/Expense Management"));
			}
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.PETTYCASHTRANSACTION,"Petty Cash Transaction","Accounts/Petty Cash Transaction"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.PETTYCASHTRANSACTIONLIST,"Petty Cash Transaction List","Accounts/Petty Cash Transaction List"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.BILLINGLIST,"Billing List","Accounts/Billing List"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.BILLINGDETAILS,"Billing Details","Accounts/Billing Details"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.INVOICELIST,"Invoice List","Accounts/Invoice List"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.INVOICEDETAILS,"Invoice Details","Accounts/Invoice Details"));
			
			/** date 25.6.2018 added by komal for vendor payment details form and list **/
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.VENDORINVOICELIST,"Vendor Invoice List","Accounts/Vendor Invoice List"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.VENDORINVOICEDETAILS,"Vendor Invoice Details","Accounts/Vendor Invoice Details"));
			/**
			 * end komal
			 */
			
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.PAYMENTLIST,"Payment List","Accounts/Payment List"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.PAYMENTDETAILS,"Payment Details","Accounts/Payment Details"));
			/** date 25.6.2018 added by komal for vendor payment details form and list **/
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.VENDORPAYMENTLIST,"Vendor Payment List","Accounts/Vendor Payment List"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.VENDORPAYMENTDETAILS,"Vendor Payment Details","Accounts/Vendor Payment Details"));
			/**
			 * end komal
			 */

			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.INTERFACETALLYLIST,"Accounting Interface","Accounts/Accounting Interface"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.FINANCIALCALENDAR,"Financial Calendar","Accounts/Financial Calendar"));
			
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTMODULE,Screen.CREDITNOTE,"Credit Note List","Accounts/Credit Note List"));

			
				
			
			
			sideBarNames.add(new SideBarItems("Accounts",temp));
		}
		//*******Account Config*************//
		if(UserConfiguration.getRole().getRoleName().trim().equals("ADMIN")||accountsConfigFlag==true)
		{
			temp=new Vector<MenuItems>();
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.EXPENSEGROUP, "Expense Group", "Account Configuration/Expense Group"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.EXPENSECATEGORY,"Expense Category","Account Configuration/Expense Category"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.EXPENSETYPE,"Expense Type","Account Configuration/Expense Type"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.EXPENSEPOLICIES, "Expense Policies","Account Configuration/Expense Policies"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.GLACCOUNTGROUP,"GLAccount Group","Account Configuration/GLAccount Group"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.GLACCOUNT,"GLAccount","Account Configuration/GLAccount"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.PAYMENTMETHODS,"Payment Method","Account Configuration/Payment Method"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.OTHERCHARGES,"Other Charges","Account Configuration/Other Charges"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.PETTYCASH,"Petty Cash","Account Configuration/Petty Cash"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.COMPANYTYPE,"Company Type","Account Configuration/Company Type"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.COMPANY,"Company","Account Configuration/Company"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.BILLINGGROUP,"Billing Group","Account Configuration/Billing Group"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.BILLINGCATEGORY,"Billing Category","Account Configuration/Billing Catergory"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.BILLINGTYPE,"Billing Type","Account Configuration/Billing Type"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.INVOICEGROUP,"Invoice Group","Account Configuration/Invoice Group"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.INVOICECATEGORY,"Invoice Category","Account Configuration/Invoice Category"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.INVOICECONFIGTYPE,"Invoice Type","Account Configuration/Invoice Type"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.TAXDETAILS,"Tax Details","Account Configuration/Tax Details"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.COMPANYPAYMENT,"Payment Mode","Accounts/Payment Mode"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.VENDORPAYMENT,"Vendor Payment Mode","Accounts/Vendor Payment Mode"));
			
//			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.TDSPERCENTAGE,"TDS Tax Details","TDS Percentage"));
			temp.add(createAndAddMenuItems(AppConstants.ACCOUNTCONFIG,Screen.ADDPAYMENTTERMS, "Payment Terms Details","Account Configuration/Payment Terms Details"));
			sideBarNames.add(new SideBarItems("Account Configuration", temp));
		}
		
		return sideBarNames;
		
	}
	
	private Vector<SideBarItems> createPurchaseSideBar()
	{
		
		//***************Purchase**********************/
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		
		boolean purchaseFlag=false;
		boolean purchaseConfigFlag=false;
		List<ScreenAuthorization> internalModuleValidation=UserConfiguration.getRole().getAuthorization();
		if(!UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)){
			for(int f=0;f<internalModuleValidation.size();f++)
			{
				
				if(internalModuleValidation.get(f).getModule().trim().equals("Purchase")){
					purchaseFlag=true;
				}
					
				if(internalModuleValidation.get(f).getModule().trim().equals("Purchase Configuration")){
						purchaseConfigFlag=true;
				}
			}
		}	
		
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||purchaseFlag==true)
		{
			temp=new Vector<MenuItems>();
//			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.PURCHASEDASHBOARD,"Dashboard","Purchase/Dashboard"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.VENDOR,"Vendor","Purchase/Vendor"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.PURCHASEREQUISITIONLIST,"Purchase Requisition List","Purchase/Purchase Requisition List"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.PURCHASEREQUISITE,"Purchase Requisition","Purchase/Purchase Requisition"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.REQUESTFORQUOTATION,"RFQ","Purchase/RFQ"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.LETTEROFINTENT,"Letter Of Intent(LOI)","Purchase/Letter Of Intent(LOI)"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.PURCHASEORDER,"Purchase Order","Purchase/Purchase Order"));
			
			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.QUICKPURCHASEORDER,"Purchase Register","Purchase/Purchase Register"));

			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.SERVICEPO,"Service PO","Purchase/Service PO"));
			
			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.VENDORPRODUCTLIST,"Vendor Product Pricelist","Purchase/Vendor Product Pricelist"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.VENDORPRICELIST,"Vendor Product Price","Purchase/Vendor Product Price"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.INTERACTIONLIST,"Follow Up List","Purchase/Follow Up List"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.INTERACTION,"Follow Up","Purchase/Follow Up"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.CONTACTPERSONLIST,"Contact Person List","Purchase/Contact Person List"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASEMODULE,Screen.CONTACTPERSONDETAILS,"Contact Person Details","Purchase/Contact Person Details"));
			sideBarNames.add(new SideBarItems("Purchase",temp));
		}
		
		//*********Purchase Config**************/
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||purchaseConfigFlag==true)
		{
			temp=new Vector<MenuItems>();
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.VENDORGROUP, "Vendor Group", "Purchase Configuration/Vendor Group"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.VENDORCATEGORY, "Vendor Category", "Purchase Configuration/Vendor Category"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.VENDORTYPE, "Vendor Type", "Purchase Configuration/Vendor Type"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.PRGROUP, "PR Group", "Purchase/PR Group"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.PRCATEGORY, "PR Category", "Purchase/PR Category"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.PRTYPE, "PR Type", "Purchase/PR Type"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.RFQCATEGORY, "RFQ Category", "Purchase/RFQ Category"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.RFQTYPE, "RFQ Type", "Purchase/RFQ Type"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.LOICATEGORY, "LOI Category", "Purchase/LOI Category"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.LOITYPE, "LOI Type", "Purchase/LOI Type"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.PURCHASEORDERCATEGORY, "PO Category", "Purchase Configuration/PO Category"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.PURCHASEORDERTYPE, "PO Type", "Purchase Configuration/PO Type"));
			
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.SERVICEPOCATEGORY, "Service PO Category", "Purchase Configuration/Service PO Category"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.SERVICEPOTYPE, "Service PO Type", "Purchase Configuration/Service PO Type"));
			
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.PERSONTYPE,"Entity Type","Purchase Configuration/Entity Type"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.GROUPTYPE,"Interaction Group","Purchase Configuration/Interaction Group"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.COMMUNICATIONCATEGORY,"Communication Category","Purchase Configuration/Communication Category"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.COMMUNICATIONTYPE,"Communication Type","Purchase Configuration/Communication Type"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.INTERACTIONPRIORITY,"Interaction Priority","Purchase Configuration/Interaction Priority"));
			temp.add(createAndAddMenuItems(AppConstants.PURCHASECONFIG,Screen.CONTACTROLE,"Contact Role","Purchase Configuration/Contact Role"));
			
			sideBarNames.add(new SideBarItems("Purchase Configuration", temp));
		}
		
		return sideBarNames;
	}
	
	private Vector<SideBarItems> createAdminSideBar()
	{
		
		//***************Purchase**********************/
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		//**************Settings********************//
		temp=new Vector<MenuItems>();
		
		boolean settingFlag=false;
		List<ScreenAuthorization> internalModuleValidation=UserConfiguration.getRole().getAuthorization();
		if(!UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)){
			for(int f=0;f<internalModuleValidation.size();f++)
			{
				if(internalModuleValidation.get(f).getModule().trim().equals("Settings")){
					settingFlag=true;
				}
			}
		}
		
		/******************************************First Time Deployment**************************/
		/**
		 *  Uncomment the below line for first time deployment
		 */
		//settingFlag=true;
		
		
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||settingFlag==true)
		{
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.COMPANY,"Company","Settings/Company"));//Ashwini Patil Date:19-07-2022			
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.BRANCH,"Branch","Settings/Branch"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.EMPLOYEE,"Employee","Settings/Employee"));
//			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.QUICKEMPLOYEESCREEN,"Quick Employee","Settings/Quick Employee"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.QUICKEMPLOYEESCREEN,"Quick User","Settings/Quick User"));//Ashwini Patil Date:12-07-2022 changing Quick employee to Quick User as per Nitin sir's instruction
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.EMPLOYEELEVEL,"Employee Level","Settings/Employee Level"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.USER,"User","Settings/User"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.CUSTOMERUSER,"Customer User","Settings/Customer User"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.LICENSEINFO,"License Info","Settings/License Info"));
	//		temp.add(createAndAddMenuItems(Screen.USERAUTHORIZATION,"User Authorization","Settings/User Authorization"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.CURRENCY,"Currency","Settings/Currency"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.COUNTRY,"Country","Settings/Country"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.STATE,"State","Settings/State"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.CLASSIFICATIONOFCITY,"Classification Of City","Settings/Classification Of City"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.CITY,"City","Settings/City"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.LOCALITY,"Locality","Settings/Locality"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.MODULENAME,"Module Name","Settings/Module Name"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.DOCUMENTNAME,"Document Name","Settings/Document Name"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.NUMBERGENERATOR,"Number Generation","Settings/Number Generation"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.NUMBERGENERATORPROCESSNAME,"Process Name","Settings/Process Name"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.PROCESSCONFIGURATION,"Process Configuration","Settings/Process Configuration"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.PROCESSTYPE,"Process Type","Settings/Process Type"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.ARTICLETYPE,"Article Type","Settings/Article Type"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.SMSHISTORY,"SMS History","Settings/SMS History"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.SMSTEMPLATE,"Communication Template","Settings/Communication Template"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.SMSCONFIG,"SMS Configuration","Settings/Sms Configuration"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.MULTILEVELAPPROVAL,"Multilevel Approval","Settings/Multilevel Approval"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.APPROVALLEVEL,"Approval Level","Settings/Approval Level"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.IPADDRESSAUTHORIZATION,"IP Address Authorization","Settings/IP Address Authorization"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.LOGGEDINHISTORY,"LoggedIn History","Settings/LoggedIn History"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.DOCUMENTHISTORY,"Document History","Settings/Document History"));
			
			if(AppUtility.checkIsItServerSide() && demoPower())
			{
				temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.DEMOCONFIGURATION,"Demo Configuration","Settings/Demo Configuration"));
			}
			
	//		temp.add(createAndAddMenuItems(Screen.LOGGEDINHISTORY,"Logged In History","Logged In History"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.USERAUTHORIZE,"User Authorization","Settings/User Authorization"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.DATAMIGRATION,"Data Migration","Settings/Data Migration"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.DATAMIGRATIONTASKQUEUE,"Data Migration Task Queue","Settings/Data Migration Task Queue"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.ROLEDEFINITION,"Role Definition","Settings/Role Definition"));
			//  rohan removed this as it is not needed for NBHC  on 16/1/2017
//			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.INVENTORYTRANSCATIONERRORUPDATE,"Inventory Transaction Error Handling","Inventory Transaction Error Handling"));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.ANDROIDTIPS,"Android Tips","Settings/Android Tips"));
			/** date 22.4.2019 added by komal to show app registration history **/
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.APPREGISTRATIONHISTORY,"App Registration History","Settings/App Registration History"));
			
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.SCREENMENUCONFIGURATION,AppConstants.SCREENMENUCONFIGURATION,"Settings/"+AppConstants.SCREENMENUCONFIGURATION));
			
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.EMAILTEMPLATE,AppConstants.EMAILTEMPLATE,"Settings/"+AppConstants.EMAILTEMPLATE));
			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.EMAILTEMPLATECONFIGURATION,AppConstants.EMAILTEMPLATECONFIGURATION,"Settings/"+AppConstants.EMAILTEMPLATECONFIGURATION));

			temp.add(createAndAddMenuItems(AppConstants.SETTINGSMODULE,Screen.IMPLEMENTATIONSCREEN,"Implementation","Settings/Implementation"));
			sideBarNames.add(new SideBarItems("Settings",temp));
			/**
			 * nidhi
			 * 23-12-2017
			 * for email reminder 
			 */
//			sideBarNames.add(new SideBarItems("Settings",temp));
			temp=new Vector<MenuItems>();
			temp.add(createAndAddMenuItems(AppConstants.CRONJOBCONFIGRATION, Screen.CRONJOB, AppConstants.CRONJOB,AppConstants.CRONJOBCONFIGRATION+"/"+ AppConstants.CRONJOB));
			temp.add(createAndAddMenuItems(AppConstants.CRONJOBCONFIGRATION, Screen.FREQUENCY,AppConstants.FREQUENCY, AppConstants.CRONJOBCONFIGRATION+"/"+AppConstants.FREQUENCY));
			temp.add(createAndAddMenuItems(AppConstants.CRONJOBCONFIGRATION, Screen.CRONJOBCONFIGRATION, AppConstants.CRONJOBCONFIGRATION,  AppConstants.CRONJOBCONFIGRATION+"/"+AppConstants.CRONJOBCONFIGRATION));
			temp.add(createAndAddMenuItems(AppConstants.TERMSANDCONDITIONS, Screen.TERMSANDCONDITIONS, AppConstants.TERMSANDCONDITIONS,  "Settings/"+AppConstants.TERMSANDCONDITIONS)); //Ashwini Patil Date:28-02-2023
			
			sideBarNames.add(new SideBarItems(AppConstants.CRONJOBCONFIGRATION,temp));
		}
		
		return sideBarNames;
	}
	
	
	private Vector<SideBarItems> createProductsSideBar()
	{
		
		//***************Purchase**********************/
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		//********************Product***************//
		
		boolean productFlag=false;
		boolean productConfigFlag=false;
		List<ScreenAuthorization> internalModuleValidation=UserConfiguration.getRole().getAuthorization();
		if(!UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)){
			for(int f=0;f<internalModuleValidation.size();f++)
			{
				
				if(internalModuleValidation.get(f).getModule().trim().equals("Product")){
					productFlag=true;
				}
					
				if(internalModuleValidation.get(f).getModule().trim().equals("Product Configuration")){
						productConfigFlag=true;
				}
			}
		}	
		
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||productFlag==true)
		{
			temp=new Vector<MenuItems>();
			temp.add(createAndAddMenuItems(AppConstants.PRODUCTMODULE,Screen.SERVICEPRODUCT,"Service Product","Product/Service Product"));
			temp.add(createAndAddMenuItems(AppConstants.PRODUCTMODULE,Screen.ITEMPRODUCT,"Item Product","Product/Item Product"));
			temp.add(createAndAddMenuItems(AppConstants.PRODUCTMODULE,Screen.PRODUCTGROUPLIST,"Product Group List","Product/Product Group List"));
			temp.add(createAndAddMenuItems(AppConstants.PRODUCTMODULE,Screen.PRODUCTGROUPDETAILS,"Product Group Details","Product/Product Group Details"));
			temp.add(createAndAddMenuItems(AppConstants.PRODUCTMODULE,Screen.BILLOFPRODUCTMATERIAL,"Bill Of Material","Product/Bill Of Material"));
			sideBarNames.add(new SideBarItems("Product",temp));
		}
		
		//***************Product Setting***************//
		if(UserConfiguration.getRole().getRoleName().trim().equals(AppConstants.ADMINUSER)||productConfigFlag==true)
		{
			temp=new Vector<MenuItems>();
			temp.add(createAndAddMenuItems(AppConstants.PRODUCTCONFIG,Screen.CATEGORY,"Category","Product Configuration/Category"));
			temp.add(createAndAddMenuItems(AppConstants.PRODUCTCONFIG,Screen.UNITOFMEASUREMENT,"Unit Of Measurement","Product Configuration/Unit Of Measurement"));
			temp.add(createAndAddMenuItems(AppConstants.PRODUCTCONFIG,Screen.PRODUCTGROUP,"Product Group","Product Configuration/Product Group"));
			temp.add(createAndAddMenuItems(AppConstants.PRODUCTCONFIG,Screen.PRODUCTTYPE,"Product Type","Product Configuration/Product Type"));
			temp.add(createAndAddMenuItems(AppConstants.PRODUCTCONFIG,Screen.PRODUCTCLASSIFICATION,"Product Classification","Product Configuration/Product Classification"));
//			temp.add(createAndAddMenuItems(AppConstants.PRODUCTCONFIG,Screen.EMPLOYEELEVEL,"Employee Level","Product Confiuration/Employee Level"));
			temp.add(createAndAddMenuItems(AppConstants.PRODUCTCONFIG,Screen.BRAND,"Brand","Product Configuration/Brand"));
			sideBarNames.add(new SideBarItems("Product Configuration", temp));
		}
		
		return sideBarNames;
	}
	
	
	private Vector<SideBarItems> createAssetSideBar()
	{
		
		//***************Purchase**********************/
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		//********************Product***************//
		
		boolean assetmanagemnetFlag=false;
		boolean assetmanagemnetConfigFlag=false;
		List<ScreenAuthorization> internalModuleValidation=UserConfiguration.getRole().getAuthorization();
		if(!UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)){
			for(int f=0;f<internalModuleValidation.size();f++)
			{
				
				if(internalModuleValidation.get(f).getModule().trim().equals("Asset Management")){
					assetmanagemnetFlag=true;
				}
					
				if(internalModuleValidation.get(f).getModule().trim().equals("Asset Management Configuration")){
					assetmanagemnetConfigFlag=true;
				}
			}
		}
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||assetmanagemnetFlag==true)
		{
			temp=new Vector<MenuItems>();
//			temp.add(createAndAddMenuItems(AppConstants.ASSETMANEGEMENTMODULE,Screen.ASSETMANAGEMENTDASHBOARD, "Dashboard", "Dashboard"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.ASSETMANEGEMENTMODULE,Screen.COMPANYASSET, "Company Asset", "Company Asset"));
			temp.add(createAndAddMenuItems(AppConstants.ASSETMANEGEMENTMODULE,Screen.COMPANYASSETLIST, "Company Asset List", "Company Asset List"));//Ashwini Patil Commented on 06-08-2024
			temp.add(createAndAddMenuItems(AppConstants.ASSETMANEGEMENTMODULE,Screen.CLIENTSIDEASSET, "Client Asset", "Client Asset"));
			temp.add(createAndAddMenuItems(AppConstants.ASSETMANEGEMENTMODULE,Screen.TOOLGROUP, "Tool Set", "Tool Set"));
			sideBarNames.add(new SideBarItems("Asset Management",temp));
		}
		
		//***************Product Setting***************//
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||assetmanagemnetConfigFlag==true)
		{
			temp=new Vector<MenuItems>();
			temp.add(createAndAddMenuItems(AppConstants.ASSETMANAGEMENTCONFIG,Screen.ASSETCATEGORY,"Asset Category", "Asset Category"));
			temp.add(createAndAddMenuItems(AppConstants.ASSETMANAGEMENTCONFIG,Screen.CLIENTSIDEASSETCATEGORY,"Client Asset Category", "Client Asset Category"));
			temp.add(createAndAddMenuItems(AppConstants.ASSETMANAGEMENTCONFIG,Screen.ASSETARTICLETYPE,AppConstants.ASSETARTICLETYPE,AppConstants.ASSETMANAGEMENTCONFIG+"/"+AppConstants.ASSETARTICLETYPE));
			sideBarNames.add(new SideBarItems("Asset Management Configuration", temp));
		}
		
		return sideBarNames;
	}
	
	
	
	private Vector<SideBarItems> createRegistrationSideBar()
	{
		//***************Purchase**********************/
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		//**************Settings********************//
		temp=new Vector<MenuItems>();
		
		temp.add(createAndAddMenuItems(AppConstants.REGISTRATIONMODULE,Screen.REGISTRATION,"Registration","Registration"));
		temp.add(createAndAddMenuItems(AppConstants.REGISTRATIONMODULE,Screen.LICENSEMANAGEMENT,"License Management","License Management"));
		sideBarNames.add(new SideBarItems("Registration",temp));
		return sideBarNames;
	}
	
	private Vector<SideBarItems> createSupportSideBar()
	{
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		temp=new Vector<MenuItems>();
		temp.add(createAndAddMenuItems(AppConstants.SUPPORTMODULE,Screen.RAISETICKET,"Raise Ticket","Raise Ticket"));
		temp.add(createAndAddMenuItems(AppConstants.SUPPORTMODULE,Screen.REFRESHSCREEN, "Refresh Screen",""));
		sideBarNames.add(new SideBarItems("Support",temp));
		
		temp=new Vector<MenuItems>();
		temp.add(createAndAddMenuItems(AppConstants.SUPPORTCONFIG,Screen.TICKETCATEGORY,"Ticket Category", "Support Configuration/Ticket Category"));
		temp.add(createAndAddMenuItems(AppConstants.SUPPORTCONFIG,Screen.TICKETTYPE, "Ticket Type","Support Configuration/Ticket Type"));
		temp.add(createAndAddMenuItems(AppConstants.SUPPORTCONFIG,Screen.TICKETPRIORITY,"Ticket Priority", "Support Configuration/Ticket Priority"));
		temp.add(createAndAddMenuItems(AppConstants.SUPPORTCONFIG,Screen.TICKETLEVEL, "Ticket Level","Support Configuration/Ticket Level"));
		sideBarNames.add(new SideBarItems("Support Configuration",temp));
		
		temp=new Vector<MenuItems>();
		temp.add(createAndAddMenuItems(AppConstants.VERSIONMANAGEMENTCONFIG,Screen.VERSIONMANAGEMENT,AppConstants.VERSIONDATE,""));
		temp.add(createAndAddMenuItems(AppConstants.VERSIONMANAGEMENTCONFIG,Screen.VERSIONMANAGEMENT,AppConstants.VERSION,""));

		sideBarNames.add(new SideBarItems("Version Management",temp));
		
		return sideBarNames;
	}
	
	private Vector<SideBarItems> createApprovalSideBar()
	{
		
		//***************Purchase**********************/
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		//**************Settings********************//
		boolean approvalFlag=false;
		List<ScreenAuthorization> internalModuleValidation=UserConfiguration.getRole().getAuthorization();
		if(!UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER))
		{
			for(int f=0;f<internalModuleValidation.size();f++)
			{
				if(internalModuleValidation.get(f).getModule().trim().equals("Approval")){
					approvalFlag=true;
				}
			}
		}
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)||approvalFlag==true)
		{
			temp=new Vector<MenuItems>();
			temp.add(createAndAddMenuItems(AppConstants.APPROVALMODULE,Screen.APPROVAL,"Approval","Approval"));
			sideBarNames.add(new SideBarItems("Approval",temp));
		}
		
		return sideBarNames;
	}
	
	
	private Vector<SideBarItems> createSideBarForCustomer() {
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		
		temp=new Vector<MenuItems>();
		temp.add(createAndAddMenuItems(AppConstants.SERVICEMODULE,Screen.CUSTOMERRECORD,"Customer Help Desk","Customer Help Desk"));
		sideBarNames.add(new SideBarItems("Service", temp));
		return sideBarNames;
	}
	
	/** date 03.03.2018 added by komal for nbhc reports **/
	private Vector<SideBarItems> createReportSideBar()
	{
		
		//***************Purchase**********************/
		Vector<SideBarItems> sideBarNames=new Vector<SideBarItems>();
		Vector<MenuItems> temp;
		//**************Settings********************//
		temp=new Vector<MenuItems>();
		
		boolean reportFlag=false;
		List<ScreenAuthorization> internalModuleValidation=UserConfiguration.getRole().getAuthorization();
		if(!UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)){
			for(int f=0;f<internalModuleValidation.size();f++)
			{
				if(internalModuleValidation.get(f).getModule().trim().equals("Reports")){
					reportFlag=true;
				}
			}
		}
		
		/******************************************First Time Deployment**************************/
		/**
		 *  Uncomment the below line for first time deployment
		 */
		//settingFlag=true;
		
		
		if((LoginPresenter.myUserEntity!=null&&LoginPresenter.myUserEntity.getCompanyType().equalsIgnoreCase("Pest Control"))&&UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)|| reportFlag==true)
		{
			Console.log("inside pest control condition");
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE,Screen.REVENUEREPORT,"Revenue Report","Reports/Revenue Report"));
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE,Screen.TECHNICIANREVENUEREPORT,"Technician Revenue Report","Reports/Technician Revenue Report"));
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE,Screen.SERVICEREVENUEREPORT,"Service Revenue Report","Reports/Service Revenue Report"));
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE,Screen.COMMODITYFUMIGATIONREPORT,"Commodity Fumigation Report","Reports/Commodity Fumigation Report"));
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE,Screen.CLOSINGSTOCKREPORT,"Closing Stock Report","Reports/Closing Stock Report"));
			/** date 14.03.2018 added by komal for nbhc reports **/
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE,Screen.ACTIVECONTRACTREPORT,"Active Contract Report","Reports/Active Contract Report"));
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE,Screen.AMCASSUREDREVENUEREPORT,"AMC Assured Revenue Report","Reports/AMC Assured Revenue Report"));
			/**
			 * nidhi
			 *  *:*:*
			 */
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE,Screen.BOMSERVICEMATRIALREPORT,AppConstants.BOMREPORT,"Reports/"+AppConstants.BOMREPORT));
			/**
			 * nidhi
			 *  *:*:*
			 */
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE,Screen.SERVICEINVOICEREPORT,AppConstants.SERVICEINVOICEMAPPING,"Reports/"+AppConstants.SERVICEINVOICEMAPPING));
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE,Screen.SALESREGISTERREPORT,"Sales Register Report","Reports/"+"Sales Register Report"));
			/**
			 * nidhi
			 * *:*:*
			 */
				temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE, Screen.RENEWALCONTRACTREPORT, AppConstants.RENEWCONTRACTREPORT,"Reports/"+AppConstants.RENEWCONTRACTREPORT )); // nidhi 20-12-2018
			/** date 5.4.2019 added by komal to add download of contract p and l report(nbhc specific)****/
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE, Screen.CONTRACTPANDLREPORT, "Contract P & L Report","Reports/Contract P & L Report"));
			/** date 5.4.2019 added by komal to add download of branch wise split invoice report(nbhc specific)****/
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE, Screen.BRANCHWISESPLITINVOICEREPORT, "Branchwise Split Invoice Report","Reports/Branchwise Split Invoice Report"));
			
			/*** Date 26-07-2019 by Vijay for NBHC CCPM ****/
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE, Screen.SERVICEREVENUELOSSREPORT, "Service Revenue Loss Report","Reports/Service Revenue Loss Report"));
			
			/***Date 11-9-2020 by Amol added Data Studio Reports Section**/
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE, Screen.REPORTS, "Reports","Reports/Reports"));
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE, Screen.REPORTAUTHORISATION, "Report Authorization","Reports/Report Authorization"));
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE, Screen.REPORTSCONFIGURATIONS, "Report Configuration","Reports/Report Configuration"));
			
			
			
			sideBarNames.add(new SideBarItems("Reports",temp));
		}else if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)|| reportFlag==true){
			Console.log("inside pest control condition");
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE, Screen.REPORTS, "Reports","Reports/Reports"));
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE, Screen.REPORTAUTHORISATION, "Report Authorization","Reports/Report Authorization"));
			temp.add(createAndAddMenuItems(AppConstants.REPORTMODEULE, Screen.REPORTSCONFIGURATIONS, "Report Configuration","Reports/Report Configuration"));
			
			
			
			
			
			sideBarNames.add(new SideBarItems("Reports",temp));
		}
		
		return sideBarNames;
	}
	
//	private boolean createSidebarMenuItems(Vector<MenuItems> menus){
//		int ctr=0;
////		System.out.println("Vector List size  :"+menus.size());
//		Object[] items;
//		items=menus.toArray();
//		for(int i=0;i<items.length;i++)
//		{	
//			if(items[i]==null){
//				ctr++;
//			}
////			System.out.println("Vector List Item   :"+items[i]);
//		}
////		System.out.println("Null Values     :"+ctr);
//		if(ctr==menus.size())
//			return true;
//		else
//			return false;
//	}
	
	
	
	
	
	
	
	
}








