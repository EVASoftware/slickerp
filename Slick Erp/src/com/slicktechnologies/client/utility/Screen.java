package com.slicktechnologies.client.utility;

//package ;
import com.simplesoftwares.client.library.appskeleton.AbstractScreen;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.client.config.adress.city.CityPresenter;
import com.slicktechnologies.client.config.adress.locality.LocalityPresenter;
import com.slicktechnologies.client.config.adress.state.StatePresenter;
import com.slicktechnologies.client.config.configurations.ConfigForm;
import com.slicktechnologies.client.config.configurations.ConfigPresenter;
import com.slicktechnologies.client.config.configurations.ConfigTabels;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.config.typeconfig.TypePresenter;
import com.slicktechnologies.client.documenthistory.DocumentHistoryListPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.numbergenerator.NumberGeneratorPresenter;
import com.slicktechnologies.client.processname.ProcessNamePresenter;
import com.slicktechnologies.client.registration.RegistrationPresenter;
import com.slicktechnologies.client.userauthorization.UserAuthorizationPresenter;
import com.slicktechnologies.client.views.account.dashboard.ArDashboardPresenter;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenter;
import com.slicktechnologies.client.views.account.glaccount.GLAccountPresenter;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.ExpensePoliciesPresenter;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentPresenter;
import com.slicktechnologies.client.views.account.paymentdashboard.PaymentdashboardPresenter;
import com.slicktechnologies.client.views.account.pettycash.PettyCashPresenter;
import com.slicktechnologies.client.views.account.pettycashtransactionlist.PettyCashTransactionListPresentor;
import com.slicktechnologies.client.views.account.pettycashtransactions.PettyCashTransactionPresenter;
import com.slicktechnologies.client.views.accountinginterface.AccountingInterfacePresenter;
import com.slicktechnologies.client.views.accountsdashboardap.AccountsAPHomePresenter;
import com.slicktechnologies.client.views.accountsdashboardar.AccountsHomePresenter;
import com.slicktechnologies.client.views.approval.ApprovalPresenter;
import com.slicktechnologies.client.views.assesmentreport.AssesmentReportPresenter;
import com.slicktechnologies.client.views.assesmentreportunit.AssessmentReportUnitPresenter;
import com.slicktechnologies.client.views.assetmanagementdashboard.AssetManagementDashboardPresenter;
import com.slicktechnologies.client.views.authorization.AuthorizationPresenter;
import com.slicktechnologies.client.views.billofproductmaterial.BillOfProductMaterialPresenter;
import com.slicktechnologies.client.views.cnc.CNCPresenter;
import com.slicktechnologies.client.views.companyassetlist.CompanyAssetListPresentor;
import com.slicktechnologies.client.views.complain.ComplainPresenter;
import com.slicktechnologies.client.views.complaindashboard.complainHomePresenter;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails.ContactPersonPresenter;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersonlist.ContactListPresenter;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contractamc.PendingContractPresenter;
import com.slicktechnologies.client.views.contractrenewal.ContractRenewalPresenter;
import com.slicktechnologies.client.views.customer.CustomerPresenter;
import com.slicktechnologies.client.views.customerDetails.CustomerDetailsPresenter;
import com.slicktechnologies.client.views.customerbranchdetails.CustomerBranchPresenter;
import com.slicktechnologies.client.views.customerlist.CustomerListGridPresenter;
import com.slicktechnologies.client.views.customerlog.CustomerLogDetailsPresenter;
import com.slicktechnologies.client.views.customerlog.DefectiveMaterialPresenter;
import com.slicktechnologies.client.views.customersupport.CustomerSupportPresenter;
import com.slicktechnologies.client.views.customersupportdashboard.CustomerSupportDashboardPresenter;
import com.slicktechnologies.client.views.customertraining.CustomerTrainingPresenter;
import com.slicktechnologies.client.views.dashboard.contractdashboard.ContractDashboardPresenter;
import com.slicktechnologies.client.views.dashboard.leaddashboard.LeadDashboardPresenter;
import com.slicktechnologies.client.views.dashboard.quotationdashboard.QuotationDashboardPresenter;
import com.slicktechnologies.client.views.dashboard.salesleaddashboard.SalesLeadDashboardPresenter;
import com.slicktechnologies.client.views.datamigration.DataMigrationPresentor;
import com.slicktechnologies.client.views.datamigrationtaskqueue.DataMigrationTaskQueuePresenter;
import com.slicktechnologies.client.views.datastudioreports.DsReportsAuthorizationPresenter;
import com.slicktechnologies.client.views.datastudioreports.DsReportsConfigurationPresenter;
import com.slicktechnologies.client.views.datastudioreports.DsReportsPresenter;
import com.slicktechnologies.client.views.datauploading.TransactionsUploadingPresenter;
import com.slicktechnologies.client.views.declaration.DeclarationPresenter;
import com.slicktechnologies.client.views.deliverynote.DeliveryNotePresenter;
import com.slicktechnologies.client.views.democonfig.DemoConfigSummaryPresenter;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.financialcalendar.FinancialCalendarPresenter;
import com.slicktechnologies.client.views.followup.FollowupPresenter;
import com.slicktechnologies.client.views.franchise.InventoryListPresenter;
import com.slicktechnologies.client.views.fumigationAustralia.FumigationAustraliaPresenter;
import com.slicktechnologies.client.views.fumigationdetails.FumigationALPPresenter;
import com.slicktechnologies.client.views.fumigationdetails.FumigationPresenter;
import com.slicktechnologies.client.views.home.HomePresenter;
import com.slicktechnologies.client.views.hrtaxStructure.TaxSlabPresenter;
import com.slicktechnologies.client.views.hrtaxStructure.employeeinvestment.EmployeeInvestmentPresenter;
import com.slicktechnologies.client.views.hrtaxStructure.investment.InvestmentPresenter;
import com.slicktechnologies.client.views.humanresource.advance.AdvancePresenter;
import com.slicktechnologies.client.views.humanresource.advancetype.AdvanceTypePresenter;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.LeaveAllocationPresenter;
import com.slicktechnologies.client.views.humanresource.allocation.payslipallocation.PaySlipPresenter;
import com.slicktechnologies.client.views.humanresource.arrearsmanagement.ArrearsManagementPresenter;
import com.slicktechnologies.client.views.humanresource.attendancelist.AttendanceListPresenter;
import com.slicktechnologies.client.views.humanresource.bonus.BonusPresenter;
import com.slicktechnologies.client.views.humanresource.calendar.CalendarPresenter;
import com.slicktechnologies.client.views.humanresource.companyscontribution.CompanyContributionListSearchProxy;
import com.slicktechnologies.client.views.humanresource.companyscontribution.CompanysContributionListPresenter;
import com.slicktechnologies.client.views.humanresource.compliance.CompliancePresenter;
import com.slicktechnologies.client.views.humanresource.country.CountryPresenter;
import com.slicktechnologies.client.views.humanresource.ctc.CTCpresenter;
import com.slicktechnologies.client.views.humanresource.ctcallocation.CTCAllocationPresenter;
import com.slicktechnologies.client.views.humanresource.ctccomponent.EarningComponentPresenter;
import com.slicktechnologies.client.views.humanresource.ctctemplate.CTCTemplatePresenter;
import com.slicktechnologies.client.views.humanresource.deduction.DeductionComponentPresenter;
import com.slicktechnologies.client.views.humanresource.deduction.lwf.LWFPresenter;
import com.slicktechnologies.client.views.humanresource.deduction.pt.PTPresenter;
import com.slicktechnologies.client.views.humanresource.department.DepartmentPresenter;
import com.slicktechnologies.client.views.humanresource.employeeadditionaldetails.EmployeeAdditionalDetailsPresenter;
import com.slicktechnologies.client.views.humanresource.employeeasset.EmployeeAssetAllocationPresenter;
import com.slicktechnologies.client.views.humanresource.employeeattendance.EmployeeAttendancePresenter;
import com.slicktechnologies.client.views.humanresource.employeechecklist.EmployeeCheckListTypePresenter;
import com.slicktechnologies.client.views.humanresource.employeegroup.EmployeeGroupPresenter;
import com.slicktechnologies.client.views.humanresource.employeeleave.EmployeeLeavePresenter;
import com.slicktechnologies.client.views.humanresource.employeeleavehistory.EmployeeLeaveHistoryListPresenter;
import com.slicktechnologies.client.views.humanresource.esic.EsicPresenter;
import com.slicktechnologies.client.views.humanresource.grade.GradePresenter;
import com.slicktechnologies.client.views.humanresource.holidaytype.HolidayTypePresenter;
import com.slicktechnologies.client.views.humanresource.leaveapplication.LeaveApplicationPresenter;
import com.slicktechnologies.client.views.humanresource.leavegroup.LeaveGroupPresenter;
import com.slicktechnologies.client.views.humanresource.leavetype.LeaveTypePresenter;
import com.slicktechnologies.client.views.humanresource.overtime.OvertimePresenter;
import com.slicktechnologies.client.views.humanresource.paidleave.PaidLeavePresenter;
import com.slicktechnologies.client.views.humanresource.pf.ProvidentFundPresenter;
import com.slicktechnologies.client.views.humanresource.project.HrProjectPresenter;
import com.slicktechnologies.client.views.humanresource.salarySlip.SalaryslipPresenter;
import com.slicktechnologies.client.views.humanresource.shift.ShiftPresenter;
import com.slicktechnologies.client.views.humanresource.shiftpattern.ShiftPatternPresenter;
import com.slicktechnologies.client.views.humanresource.statuteryreport.StatuteryReportPresenter;
import com.slicktechnologies.client.views.humanresource.timereport.TimeReportPresenter;
import com.slicktechnologies.client.views.humanresource.timereportconfig.TimeReportConfigPresenter;
import com.slicktechnologies.client.views.humanresource.uploadattendance.CreateAttendancePresenter;
import com.slicktechnologies.client.views.humanresource.uploadattendance.UploadAttendancePresenter;
import com.slicktechnologies.client.views.humanresource.voluntarypf.VoluntaryPfHistoryListPresenter;
import com.slicktechnologies.client.views.humanresource.voluntarypf.VoluntaryPfPresenter;
import com.slicktechnologies.client.views.implementation.ImplementationPresenter;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionPresenter;
import com.slicktechnologies.client.views.interaction.interactionlist.InteractionListPresenter;
import com.slicktechnologies.client.views.inventory.billofmaterial.BillOfMaterialPresenter;
import com.slicktechnologies.client.views.inventory.grnlist.GRNListPresenter;
import com.slicktechnologies.client.views.inventory.inspection.InspectionPresenter;
import com.slicktechnologies.client.views.inventory.inspectionlist.InspectionListPresenter;
import com.slicktechnologies.client.views.inventory.inventoryinhand.InventoryInHandPresenter;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNotePresenter;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNotePresenter;
import com.slicktechnologies.client.views.inventory.materialmovementtype.MaterialMovementTypePresenter;
import com.slicktechnologies.client.views.inventory.materialreuestnote.MaterialRequestNotePresenter;
import com.slicktechnologies.client.views.inventory.productinventorytransaction.InventoryProductTransactionPresenter;
import com.slicktechnologies.client.views.inventory.productinventoryview.ProductInventoryViewPresenter;
import com.slicktechnologies.client.views.inventory.productinventoryviewlist.ProductInventoryViewListPresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNPresenter;
import com.slicktechnologies.client.views.inventory.stockalert.StockAlertPresenter;
import com.slicktechnologies.client.views.inventory.storagebin.StoragebinPresenter;
import com.slicktechnologies.client.views.inventory.storagelocation.StorageLocationPresenter;
import com.slicktechnologies.client.views.inventory.warehouse.WareHousePresenter;
import com.slicktechnologies.client.views.inventory.warehousedetails.WareHouseDetailsPresenter;
import com.slicktechnologies.client.views.inventoryTranscationErrorUpdate.InventoryTransactionErrorUpdatePresenter;
import com.slicktechnologies.client.views.inventorydashboard.InventoryHomePresenter;
import com.slicktechnologies.client.views.ipaddressauthorization.IPAddressAuthorizationPresenter;
import com.slicktechnologies.client.views.lead.CompanyProfileConfigurationPresenter;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.licenseinfo.LicenseInfoPresenter;
import com.slicktechnologies.client.views.licensemanagement.LicenseManagementPresenter;
import com.slicktechnologies.client.views.loggedinhistory.LoggedInHistoryPresenter;
import com.slicktechnologies.client.views.materialconsumptionreport.MaterialConsumptionReportPresenter;
import com.slicktechnologies.client.views.multilevelapprovaldetails.MultilevelApprovalPresenter;
import com.slicktechnologies.client.views.othercharges.OtherChargesPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicelist.InvoiceListPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentlist.PaymentListPresenter;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicedetails.VendorInvoiceDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicelist.VendorInvoiceListPresenter;
import com.slicktechnologies.client.views.paymentmodes.companypayment.CompanyPaymentPresenter;
import com.slicktechnologies.client.views.paymentmodes.vendorpayment.VendorPaymentPresenter;
import com.slicktechnologies.client.views.processconfig.ProcessConfigurationPresenter;
import com.slicktechnologies.client.views.productgroupidentification.productgroupdetails.ProductGroupDetailsPresenter;
import com.slicktechnologies.client.views.productgroupidentification.productgrouplist.ProductGroupListPresenter;
import com.slicktechnologies.client.views.products.category.CategoryPresenter;
import com.slicktechnologies.client.views.products.itemproduct.ItemproductPresenter;
import com.slicktechnologies.client.views.products.serviceproduct.ServiceproductPresenter;
import com.slicktechnologies.client.views.products.unitconversion.UnitConversionPresenter;
import com.slicktechnologies.client.views.project.clientsideasset.ClientSideAssetPresenter;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.project.tool.ToolPresenter;
import com.slicktechnologies.client.views.project.toolGroup.ToolGroupPresenter;
import com.slicktechnologies.client.views.projectallocation.ProjectAllocationPresenter;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentPresenter;
import com.slicktechnologies.client.views.purchase.productrequisite.ProductRequisitePresenter;
import com.slicktechnologies.client.views.purchase.purchase.PurchasePresenter;
import com.slicktechnologies.client.views.purchase.purchaselist.PurchaseRequisitionListPresenter;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequitionPresenter;
import com.slicktechnologies.client.views.purchase.quickpurchase.QuickPurchaseOrderPreseneter;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationPresenter;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoPresenter;
import com.slicktechnologies.client.views.purchase.vendor.VendorPresenter;
import com.slicktechnologies.client.views.purchase.vendorPriceList.VendorPriceListPresenter;
import com.slicktechnologies.client.views.purchase.vendorproductlist.VendorProductListPresenter;
import com.slicktechnologies.client.views.purchasedashboard.PurchaseDashboardPresenter;
import com.slicktechnologies.client.views.quickcontract.QuickContractPresentor;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderPresenter;
import com.slicktechnologies.client.views.raiseticket.RaiseTicketPresenter;
import com.slicktechnologies.client.views.reminder.ReminderPresenter;
import com.slicktechnologies.client.views.reports.ReportPresenter;
import com.slicktechnologies.client.views.reports.SalesRegisterFormPresenter;
import com.slicktechnologies.client.views.salesdashboard.SalesHomePresenter;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.client.views.salesperformance.SalesPerformancePresentor;
import com.slicktechnologies.client.views.salespersontarget.SalesPersonTargetPresentor;
import com.slicktechnologies.client.views.salespersontargetachievement.SalesPersonTargetAchievementPresentor;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationPresenter;
import com.slicktechnologies.client.views.schedulingandrouting.SchedulingPresenter;
import com.slicktechnologies.client.views.screenmenuconfiguration.ScreenMenuConfigurationPresenter;
import com.slicktechnologies.client.views.securitydeposit.salessecuritydepositlist.SalesSecurityDepositListPresenter;
import com.slicktechnologies.client.views.securitydeposit.servicesecuritydepositlist.ServiceSecurityDepositListPresenter;
import com.slicktechnologies.client.views.service.ServicePresenter;
import com.slicktechnologies.client.views.serviceliaison.liaison.LiaisonPresenter;
import com.slicktechnologies.client.views.serviceliaison.liaisongroup.LiaisonGroupPresenter;
import com.slicktechnologies.client.views.serviceliaison.liaisonsteps.LiaisonStepsPresenter;
import com.slicktechnologies.client.views.settings.branch.BranchPresenter;
import com.slicktechnologies.client.views.settings.company.CompanyPresenter;
import com.slicktechnologies.client.views.settings.cronjobconfig.CronJobConfigrationPresenter;
import com.slicktechnologies.client.views.settings.customerslogin.CustomerUserPresenter;
import com.slicktechnologies.client.views.settings.employee.EmployeePresenter;
import com.slicktechnologies.client.views.settings.employee.QuickEmployeePresenter;
import com.slicktechnologies.client.views.settings.roledefinition.RoleDefinitionPresenter;
import com.slicktechnologies.client.views.settings.user.UserPresenter;
import com.slicktechnologies.client.views.shipping.checkliststep.CheckListStepPresenter;
import com.slicktechnologies.client.views.shipping.checklisttype.CheckListTypePresenter;
import com.slicktechnologies.client.views.shipping.inspectionmethod.InspectionMethodPresenter;
import com.slicktechnologies.client.views.shipping.packingmethod.PackingMethodPresenter;
import com.slicktechnologies.client.views.shipping.shippingmethod.ShippingMethodPresenter;
import com.slicktechnologies.client.views.smsconfiguration.SmsConfigurationPresenter;
import com.slicktechnologies.client.views.smshistorydetails.SmsHistoryListPresenter;
import com.slicktechnologies.client.views.smstemplate.SmsTemplatePresenter;
import com.slicktechnologies.client.views.taxdetails.TaxDetailsPresenter;
import com.slicktechnologies.client.views.teammanagement.TeamManagementPresenter;
import com.slicktechnologies.client.views.techniciandashboard.TechnicianDashboardPresenter;
import com.slicktechnologies.client.views.technicianschedule.TechnicianScheduleListPresenter;
import com.slicktechnologies.client.views.technicianwarehousedetails.TechnicianWareHouseDetailsPresenter;
import com.slicktechnologies.client.views.termsandconditions.TermsAndConditionsPresenter;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.client.views.quotation.coststructure.EmployeeLevelPresenter;
import com.slicktechnologies.client.views.workorder.WorkOrderPresenter;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.client.views.account.OutstandingReport.OutstandingReportPresenter;
import com.slicktechnologies.client.views.contractpandlreport.ContractPAndLReportPresenter;
import com.slicktechnologies.client.views.creditnote.CreditNoteListGridPresenter;
import com.slicktechnologies.client.views.deregisterdevice.DeregisterPresenter;
import com.slicktechnologies.client.views.emailtemplate.EmailTemplatePresenter;
import com.slicktechnologies.client.views.emailtemplateconfig.EmailTemplateConfigurationPresenter;
import com.slicktechnologies.client.views.humanresource.employeeovertimehistory.EmployeeOvertimeHistoryListPresenter;
/**
 * Enum for ConfigTypes. Abstracts the screen.
 * 
 * @author Kamala
 *
 */
public enum Screen implements AbstractScreen {
	// Token to add the project specific screens

	CUSTOMER, LEAD, QUOTATION, CONTRACT, VENDOR, PURCHASE, COMPANY, SERVICE, EXPENSEMANAGMENT, SERVICEPRODUCT, ITEMPRODUCT, BRANCH, EMPLOYEE, USER, COMPLAIN, FOLLOWUP, HOME, REMINDER, SERVICEPRODUCTCATEGORY, ITEMMASTERCATEGORY, COMPANYTYPE, CUSTOMERLEVEL, CUSTOMERTYPE, CUSTOMERSTATUS, LEADSTATUS, LEADPRIORITY, CONTRACTSTATUS, CUSTOMERPRIORITY, QUOTATIONPRIORITY, QUOTATIONSTATUS, EMPLOYEEROLE, EMPLOYEEDESIGNATION, ACESSLEVEL, EXPENSECATEGORY, PAYMENTMETHODS, PAYMENTTERMS, CURRENCY, FOLLOWUPACTIONS, SERVICESTATUS, COMPLAINPRIORITY, COMPLAINCATEGORY, COMPLAINSTATUS, ACTIONPRIORITY, FOLLOWUPCATEGORY, FOLLOWUPTYPE, FOLLOWUPSTATUS, EXPENSETYPE, EMPLOYEETYPE, EMPLOYEEGROUP, SHIFTCATEGORY, PATTERNCATEGORY, CALENDAR, LEAVEAPPLICATION, SHIFT, SHIFTPATTERN, DEPARTMENT, CATEGORY, CUSTOMERCATEGORY, CONTRACTCATEGORY, LEADCATEGORY, QUOTATIONCATEGORY, VENDORCATEGORY, CUSTOMERGROUP, LEADGROUP, QUOTATIONGROUP, CONTRACTGROUP, VENDORGROUP, SALARYHEAD, CTC, EMPLOYEELEAVE, LEAVEALLOCATION, PETTYCASH, PETTYCASHTRANSACTION, 

	REGISTRATION, LEADTYPE, QUOTATIONTYPE, CONTRACTTYPE, VENDORTYPE, EXPENSEGROUP, ADVANCE, GRADE, COUNTRY, LEAVETYPE, ACTIONTAKEN, ACTIONSTATUS, COMPLAINTYPE, COMPLAINGROUP, CONTRACTPRIORITY, ROLECATEGORY, USERAUTHORIZATION, APPROVAL, TAX, DEVICE, HOLIDAYTYPE, ADVANCETYPE, TIMEREPORT, HRPROJECT, ASSIGNEDSHIFT,

	SALESCONFIGURATION, SETTINGCONFIGURATION, ACCOUNTCONFIGURATION, PURCHASECONFIGURATION, HRCONFIGURATION, COMPANYASSET, CLIENTSIDEASSET, TOOLGROUP, PROJECT, ASSETCATEGORY, EARNINGS, DEDUCTION, LEAVEBALANCE, LEAVEGROUP, CLIENTSIDEASSETCATEGORY, CALENDARALLOCATION, TIMEREPORTCONFIG,

	UPLOADATTENDENCE, EMPLOYEEATTENDENCE, LIAISONSTEPS, LIAISONGROUP,INVOICE, GLACCOUNT, TAXDEFINATION, FINANCECUSTOMER, ATTENDANCECONFIG, UPLOADATTENDANCE, PROCESSPAYROLL, LIASION,LOIGROUP,

	VENDORPRICELIST, VENDORPRODUCTLIST, REQUESTFORQUOTATION, LETTEROFINTENT, GLACCOUNTGROUP,ASSETMANAGEMENTDASHBOARD, 
	
	BILLINGCATEGORY, BILLINGTYPE, BILLINGGROUP, BILLINGLIST, BILLINGDETAILS, INVOICELIST, INVOICEDETAILS, PAYMENTLIST, PAYMENTDETAILS, ACCOUNTDASHBOARD, OTHERCHARGES, UNITOFMEASUREMENT,DEMOCONFIGURATION,
	
	PURCHASEORDER,PRODUCTREQUISITE,PURCHASEREQUISITE,PURCHASEDASHBOARD,GRN,INVENTORYINHAND,WAREHOUSE,STORAGELOCATION, DOCUMENTNAME,MODULENAME,COMMUNICATIONTYPE,COMMUNICATIONCATEGORY,PERSONTYPE,GROUPTYPE,INTERACTIONPRIORITY,INTERACTION,INTERACTIONLIST,PRODUCTGROUPDETAILS, PRODUCTGROUPLIST, BILLOFMATERIAL,CONTACTROLE, CONTACTPERSONDETAILS, CONTACTPERSONLIST,CUSTOMERLIST,CUSTOMERCONTACT,WAREHOUSEDETAILS,TICKETCATEGORY,TICKETTYPE,RAISETICKET,TICKETPRIORITY,TICKETLEVEL,INSPECTIONMETHOD,CUSTOMERBRANCH,
	
	STORAGEBIN,PRODUCTINVENTORYVIEW,PRODUCTINVENTORYVIEWLIST,TAXDETAILS,CITY,STATE,LOCALITY,TRANSFER,PROCESSNAME,
	
	SALESLEAD,SALESCUSTOMER,SALESQUOTATION, SALESORDER, SALESQUOTATIONGROUP, SALESQUOTATIONCATEGORY, SALESQUOTATIONTYPE, SALESQUOTATIONPRIORITY, SALESQUOTATIONSTATUS, SALESORDERCATEGORY, SALESORDERTYPE, SALESORDERGROUP, SALESORDERPRIORITY, SALESORDERSTATUS, DELIVERYNOTE,SALESHOME,COMPANYPAYMENT,VENDORPAYMENT,PRODUCTTYPE,PRODUCTGROUP,PRODUCTCLASSIFICATION,DELIVERYNOTECATEGORY,DELIVERYNOTETYPE,DELIVERYNOTEGROUP,INVOICECATEGORY,INVOICECONFIGTYPE,INVOICEGROUP,
	
	PURCHASEORDERCATEGORY, PURCHASEORDERTYPE, RFQCATEGORY, RFQTYPE, PRCATEGORY, PRTYPE, LOICATEGORY, LOITYPE, GRNCATEGORY, GRNGROUP, GRNTYPE,PRGROUP,REFRESHSCREEN,LOGGEDINHISTORY,LICENSEMANAGEMENT,
	
	MRNCATEGORY,MRNTYPE,MATERIALREQUESTNOTE,MINCATEGORY,MINTYPE,MATERIALISSUENOTE,PRODUCTINVENTORYTRANSACTION,ACCOUNTSDASHBOARDAP,MATERIALCONSUMPTIONREPORT,
	
	MMNTRANSACTIONTYPE,MMNDIRECTION,MMNCATEGORY,MMNTYPE,MATERIALMOVEMENTNOTE,INVENTORYHOME,NUMBERGENERATORPROCESSNAME,DOCUMENTHISTORY,ARTICLETYPE,USERAUTHORIZE,WORKORDER,
	
	CHECKLISTCATEGORY,SHIPPINGDETAILS,CHECKLISTSTEP,CHECKLISTTYPE,SHIPPINGMETHOD,PACKINGMETHOD,NUMBERGENERATOR,INTERFACETALLYLIST,SALESSECURITYDEPOSITLIST,SERVICESECURITYDEPOSITLIST,PROCESSTYPE,PROCESSCONFIGURATION,GRNLIST,INSPECTIONGROUP,INSPECTIONCATEGORY,INSPECTIONTYPE,INSPECTION,INSPECTIONLIST,BILLOFPRODUCTMATERIAL,WORKORDERGROUP,WORKORDERCATEGORY,WORKORDERTYPE,SERVICETYPE,STOCKALERT,SMSCONFIG,SMSTEMPLATE,IPADDRESSAUTHORIZATION,APPROVALLEVEL,MULTILEVELAPPROVAL,SMSHISTORY,LICENSEINFO, FUMIGATION,FUMIGATIONAUSTRALIA,CUSTOMERTRAINING,FUMIGATIONDECLAREDPOE,
	DOSAGERATEOFFUMIGANT,DURATIONOFFUMIGATION,MINIMUMAIRTEMP,PLACEOFFUMIGATION,COUNTRYOFLOADING,QUANTITYDECLAREUNIT,
	OVERTIME,COMPANYSCONTRIBUTION,EMPLOYEEADDITIONALDETAILS,EMPLOYEEFAMILYRELATION,ADDRESSTYPE,SALARYSLIP,DATAMIGRATION,DECLARATION,CONTRACTRENEWAL, DATAMIGRATIONTASKQUEUE,CUSTOMERRECORD, SCHEDULINGANDROUTING, TEAMMANAGEMENT, CUSTOMERUSER, SPECIFICREASONFORRESCHEDULING, DOCUMENTTYPE, NUMBERRANGE, TDSPERCENTAGE, QUICKCONTRACT, PETTYCASHTRANSACTIONLIST,
	CATCHTRAPPESTNAME, SALESPERSONDASHBOARD, SALESPERSONTARGET, SALESPERSONPERFORMANCE, FINANCIALCALENDAR, ADDPAYMENTTERMS,COMPLAINDASHBOARD,ASSETARTICLETYPE, COMPANYASSETLIST, TECHNICIANDASHBOARD,ASSESMENTREPORT,ACTIONPLANFORCOMPANY,ACTIONPLANFORCUSTOMER,ASSESSMENTREPORTCATEGORY,ASSESSMENTREPORTDEFICIENCYTYPE, MULTIPLEEXPENSEMANAGMENT,SALUTATION,
	INVENTORYTRANSCATIONERRORUPDATE,WAREHOUSETYPE,SERVICEPOCATEGORY,SERVICEPOTYPE,SERVICEPO,PURCHASEREQUISITIONLIST,ROLEDEFINITION, RELIGION,EMPLOYEELEVEL,
	
	//Ajikya added this 15/06/2017 
	OUTSTANDINGREPORT,SERVICEUOM/*Date 17-06-2017 By Rahul*/,QUICKSALESORDER,QUICKPURCHASEORDER,TECHNICIANSCHEDULNG,
	/**
	 *  nidhi
	 *  22-12-2017
	 *  for email remainder screen define
	 */
	CRONJOB,FREQUENCY,CRONJOBCONFIGRATION,ANDROIDTIPS/*Date 24-01-2018*/, CONTRACTPERIOD,FREQUENCY1,TYPEOFTREATMENT ,ASSESSMENTSCHEDULING,REVENUEREPORT ,TECHNICIANREVENUEREPORT , SERVICEREVENUEREPORT , COMMODITYFUMIGATIONREPORT ,
	CLOSINGSTOCKREPORT /** date 03.03.2018 added by komal **/ ,VERSIONMANAGEMENT , ACTIVECONTRACTREPORT , AMCASSUREDREVENUEREPORT/** date 14.03.2018 added by komal **/,
	LEADDASHBOARD,QUOTATIONDASHBOARD,CONTRACTDASHBOARD,/* date25/11/2017 by komal */CUSTOMERCALLLOGDETAILS  , CONTRACTLIST, DEFECTIVEMATERIALLIST , WARRANTYSTATUS , CALLTYPE , CALLBACKSTATUS ,  VISITSTATUS, CALLLOGSTATUS,FUMIGATIONALP/** added by komal for fumigation**/
	,CTCALLOCATION,CTCTEMPLATE,/* DAte 16-6-2018 by jayshree**/LEADUNSUCCESFULREASONS,ASSESSMENTREPORTUNIT, FUMIGATIONPERFORMED,VENTILATED,/**Date 27-7-2018 by jayshree**/
	PROJECTALLOCATION,STATUTERYREPORT,/** DATE 11.7.2018 ADDED BY KOMAL **/ATTENDANCElIST,
	EMPLOYEEASSETALLOCATION /** date 24.7.2018 added by komal for employee asset allocation  **/,
	/** 18.7.2018 added by komal **/TAXSLABCONFIGURATION ,INVESTENTCONFIGURATION,/**date 19.7.2018 added  by komal for tax year and category **/TAXATIONYEAR , TAXCATEGORY , SECTIONNAME , EMPLOYEEINVESTMENT,ARREARSMANAGEMENT/** date 03.08.2018 added by komal for employee arrears management **/,
	/** date 07.08.2018 added by komal **/EMPLOYEECHECKLISTTYPE,TRAINING,PROFESSIONALTAX,CTCCATEGORY/** komal**/,
	/** date 25.6.2018 by komal**/VENDORINVOICELIST,VENDORINVOICEDETAILS,VENDORPAYMENTLIST,VENDORPAYMENTDETAILS,CNC
	,UNITCONVERSION,SERVICEUNITCONVERSION/**nidhi 7-09-2018**/,
	BOMSERVICEMATRIALREPORT, LWF,SERVICEINVOICEREPORT,VOLUNTARYPF,VOLUNTARYPFHISTORY,BRAND,COMPLIANCEFORM,SALESREGISTERREPORT,PAYMENTDASHBOARD,RENEWALCONTRACTREPORT,EMPLOYEELEAVEHISTORY,DONOTRENEW,CANCELLATIONREMARK,/** date 6.10.2018 added by komal**/
	INVENTORYlIST,TECHNICIANWAREHOUSEDETAILS, BILLEDUNBILLED, SALESLEADDASHBOARD,BRANCHWISESPLITINVOICEREPORT,APPREGISTRATIONHISTORY,CONTRACTPANDLREPORT,PAIDLEAVE,BONUS,PROVIDENTFUND,ESIC,SERVICEREVENUELOSSREPORT,TRANSACTIONUPLOADING,CUSTOMERSUPPORT,CUSTOMERSUPPORTDASHBOARD, CLASSIFICATIONOFCITY,EXPENSEPOLICIES,EMPLOYEEOVERTIMEHISTORY,LOCKSEALDEVIATIONREPORT,
	CREATEATTENDANCE,CASTE,INHOUSESERVICE,SERVICECHECKLIST,REPORTS,REPORTSCONFIGURATIONS,REPORTAUTHORISATION,COMPANYPROFILECONFIGURATION,SCREENMENUCONFIGURATION,CREDITNOTE,EMAILTEMPLATE,EMAILTEMPLATECONFIGURATION,QUICKEMPLOYEESCREEN,IMPLEMENTATIONSCREEN,DONOTRENEWREMARK,SUMMARYSCREEN,ASSESSMENTRISKLEVEL,SERVICELOCATION, //Updated By: Viraj Date: 01-04-2019 Description: For franchise inventory list and billed unbilled DATE:15-04-2019 Description: for lead dashboard in sales
	 TAXCLASSIFICATIONCODE,TECHNICIANREMARK,/**Sheetal:28-05-2022, Adding Tax Classification code screen for envocare**/
	 TERMSANDCONDITIONS, COMPANYPAYMENTTYPE;//Ashwini Patil Date:28-02-2023
	
	//end Here

	AppMemory mem;

	/*
	 * Method template to switch the screens
	 */
	@SuppressWarnings("incomplete-switch")
	@Override
	public void changeScreen() {
		Screen s = (Screen) AppMemory.getAppMemory().currentScreen;

		switch (s) {
		
		
		
		case CUSTOMERUSER:
			CustomerUserPresenter.initalize();
			break;
		
		case TEAMMANAGEMENT:
			TeamManagementPresenter.initalize();
			break;
		
		case SCHEDULINGANDROUTING:
			SchedulingPresenter.initalize();
			break;
		
		case CUSTOMERRECORD:
			CustomerDetailsPresenter.initalize();
			break;
		
		case CUSTOMER:
			CustomerPresenter.initalize();
			break;
		case TIMEREPORTCONFIG:
			TimeReportConfigPresenter.initalize();
			break;
		case LEAD:
			LeadPresenter.initalize();
			break;
		case QUOTATION:
			QuotationPresenter.initalize();
			break;
		case CONTRACT:
			ContractPresenter.initalize();
			break;
		case VENDOR:
			VendorPresenter.initalize();
			break;
		case PURCHASE:
			PurchasePresenter.initalize();
			break;
		case COMPANY:
			CompanyPresenter.initalize();
			break;
		case SERVICE:
			ServicePresenter.initalize(SERVICE);
			break;
		case EXPENSEMANAGMENT:
			ExpensemanagmentPresenter.initalize();
			break;
		case PETTYCASH:
			PettyCashPresenter.initalize();
			break;
		case GLACCOUNT:
			GLAccountPresenter.initalize();
			break;
		case PETTYCASHTRANSACTION:
			PettyCashTransactionPresenter.initalize();
			break;
		case OTHERCHARGES:
			OtherChargesPresenter.initalize();
			break;
		case BILLINGLIST:
			BillingListPresenter.initalize();
			break;
		case BILLINGDETAILS:
			BillingDetailsPresenter.initalize();
			break;
		case INVOICELIST:
			InvoiceListPresenter.initalize();
			break;
		case INVOICEDETAILS:
			InvoiceDetailsPresenter.initalize();
			break;
		case PAYMENTDETAILS:
			PaymentDetailsPresenter.initalize();
			break;
		case PAYMENTLIST:
			PaymentListPresenter.initalize();
			break;
			/**
			 * Updated By: Viraj
			 * Date: 25-01-2019
			 * Description: To navigate to new dashboard(AR)
			 */
		case ACCOUNTDASHBOARD:
			AccountsHomePresenter.initalize();
//			ArDashboardPresenter.initalize();
			break;
			/** Ends **/
		case FINANCECUSTOMER:
			break;
		case SERVICEPRODUCT:
			ServiceproductPresenter.initalize();
			break;
		case ITEMPRODUCT:
			ItemproductPresenter.initalize();
			break;
		case TAX:
			// TaxPresenter.initalize();
			break;
		case DEVICE:
			DevicePresenter.initalize();
			break;

		case COMPANYASSET:
			ToolPresenter.initalize();
			break;
		case CLIENTSIDEASSET:
			ClientSideAssetPresenter.initalize();
			break;
		case TOOLGROUP:
			ToolGroupPresenter.initalize();
			break;
		case PROJECT:
			ProjectPresenter.initalize();
			break;
		case UPLOADATTENDENCE:
			UploadAttendancePresenter.initalize();
			break;

		case UPLOADATTENDANCE:
			UploadAttendancePresenter.initalize();
			break;

		case LEAVEALLOCATION:
			LeaveAllocationPresenter.initalize();
			break;

		case LEAVEBALANCE:
			EmployeeLeavePresenter.initalize();
			break;

		case LIAISONSTEPS:
			LiaisonStepsPresenter.initalize();
			break;

		case LIAISONGROUP:
			LiaisonGroupPresenter.initalize();
			break;

		case LIASION:
			LiaisonPresenter.initalize();
			break;

		case BRANCH:
			BranchPresenter.initalize();
			break;
		case EMPLOYEE:
			EmployeePresenter.initalize();
			break;
		case USER:
			UserPresenter.initalize();
			break;
		case COMPLAIN:
			ComplainPresenter.initalize();
			break;
		case FOLLOWUP:
			FollowupPresenter.initalize();
			break;
		case TIMEREPORT:
			TimeReportPresenter.initalize();
			break;
		case HOME:
			HomePresenter.initalize();
			break;
		case EMPLOYEEATTENDENCE:
			EmployeeAttendancePresenter.initalize();
			break;
		case REMINDER:
			ReminderPresenter.initalize();
			break;
		case CALENDAR:
			CalendarPresenter.initalize();
			break;
		case USERAUTHORIZATION:
			AuthorizationPresenter.initalize();
			break;

		case SALARYHEAD:
			EarningComponentPresenter.initalize();
			break;

		case CTC:
			CTCpresenter.initalize();
			break;
		case LEAVEAPPLICATION:
			LeaveApplicationPresenter.initalize();
			break;
		case LEAVEGROUP:
			LeaveGroupPresenter.initalize();
			break;
		case SHIFT:
			ShiftPresenter.initalize();
			break;
		case SHIFTPATTERN:
			ShiftPatternPresenter.initalize();
			break;
		case DEPARTMENT:
			DepartmentPresenter.initalize();
			break;
		case CATEGORY:
			CategoryPresenter.initalize();
			break;
		case ADVANCE:
			AdvancePresenter.initalize();
			break;
		case LEAVETYPE:
			LeaveTypePresenter.initalize();
			break;
		case GRADE:
			GradePresenter.initalize();
			break;
		case COUNTRY:
			CountryPresenter.initalize();

			break;
		case REGISTRATION:
			RegistrationPresenter.initalize();
			break;

		case APPROVAL:
			ApprovalPresenter.initalize();
			break;

		case HOLIDAYTYPE:
			HolidayTypePresenter.initalize();
			break;
		case ADVANCETYPE:
			AdvanceTypePresenter.initalize();
			break;

		case HRPROJECT:
			HrProjectPresenter.initalize();
			break;
		case EARNINGS:
			EarningComponentPresenter.initalize();
			break;
		case DEDUCTION:
			DeductionComponentPresenter.initalize();
			break;

		case PROCESSPAYROLL:
			PaySlipPresenter.initalize();
			break;

		case EMPLOYEEGROUP:
			EmployeeGroupPresenter.initalize();
			break;

		case VENDORPRICELIST:
			VendorPriceListPresenter.initalize();
			break;

		case VENDORPRODUCTLIST:
			VendorProductListPresenter.initalize();
			break;

		case REQUESTFORQUOTATION:
			RequestForQuotationPresenter.initalize();
			break;

		case LETTEROFINTENT:
			LetterOfIntentPresenter.initalize();
			break;
		case SALESORDER:
			SalesOrderPresenter.initalize();
			break;
		case SALESQUOTATION:
			SalesQuotationPresenter.initalize();
			break;
		case SALESLEAD:
			LeadPresenter.initalize();
			break;
		case SALESCUSTOMER:
			CustomerPresenter.initalize();
			break;
		case DELIVERYNOTE:
			DeliveryNotePresenter.initalize();
			break;
		case SALESHOME:
			SalesHomePresenter.initalize();
			break;
		case PURCHASEORDER:
			PurchaseOrderPresenter.initalize();
			break;
		case PRODUCTREQUISITE:
			ProductRequisitePresenter.initialize();
			break;
		case PURCHASEREQUISITE:
			PurchaseRequitionPresenter.initalize();
			break;
		case PURCHASEDASHBOARD:
			PurchaseDashboardPresenter.initalize();
			break;
		case GRN:
			GRNPresenter.initalize();
			break;
		case WAREHOUSE:
			WareHousePresenter.initalize();
			break;
		case STORAGELOCATION:
			StorageLocationPresenter.initalize();
			break;
		case PRODUCTINVENTORYVIEW:
			ProductInventoryViewPresenter.initalize();
			break;
		case INVENTORYINHAND:
			InventoryInHandPresenter.initalize();
			break;
		case STORAGEBIN:
			StoragebinPresenter.initalize();
			break;
		case PRODUCTINVENTORYVIEWLIST:
			ProductInventoryViewListPresenter.initalize();
			break;
		case COMPANYPAYMENT:
			CompanyPaymentPresenter.initalize();
			break;
		case VENDORPAYMENT:
			VendorPaymentPresenter.initialize();
			break;
		case TAXDETAILS:
			TaxDetailsPresenter.initalize();
			break;
		case CITY:
			CityPresenter.initalize();
			break;
		case STATE:
			StatePresenter.initalize();
			break;
		case LOCALITY:
			LocalityPresenter.initalize();
			break;
		case CHECKLISTSTEP:
			CheckListStepPresenter.initalize();
			break;
		case CHECKLISTTYPE:
			CheckListTypePresenter.initalize();
			break;
		case SHIPPINGMETHOD:
			ShippingMethodPresenter.initalize();
			break;
		case PACKINGMETHOD:
			PackingMethodPresenter.initalize();
			break;
		case INSPECTIONMETHOD:
			InspectionMethodPresenter.initalize();
			break;
		case NUMBERGENERATOR:
			NumberGeneratorPresenter.initalize();
			break;
		case INTERFACETALLYLIST:
			AccountingInterfacePresenter.initalize();
			break;
		case PRODUCTGROUPDETAILS:
			ProductGroupDetailsPresenter.initialize();
			break;
		case PRODUCTGROUPLIST:
			ProductGroupListPresenter.initalize();
			break;
		case BILLOFMATERIAL:
			BillOfMaterialPresenter.initialize();
			break;
		case WAREHOUSEDETAILS:
			WareHouseDetailsPresenter.initalize();
			break;
		case CONTACTPERSONDETAILS:
			ContactPersonPresenter.initalize();
			break;
		case CONTACTPERSONLIST:
			ContactListPresenter.initialize();
			break;
		case MATERIALREQUESTNOTE:
			MaterialRequestNotePresenter.initialize();
			break;
		case MATERIALISSUENOTE:
			MaterialIssueNotePresenter.initialize();
			break;
		case PRODUCTINVENTORYTRANSACTION:
			InventoryProductTransactionPresenter.initalize();
			break;
		case MATERIALMOVEMENTNOTE:
			MaterialMovementNotePresenter.initialize();
			break;
		case MMNTRANSACTIONTYPE:
			MaterialMovementTypePresenter.initalize();
			break;
		case INVENTORYHOME:
			InventoryHomePresenter.initalize();
			break;
		case RAISETICKET:
			RaiseTicketPresenter.initalize();
			break;
		case NUMBERGENERATORPROCESSNAME:
			ProcessNamePresenter.initalize();
			break;
		case INTERACTION:
			InteractionPresenter.initalize();
			break;
		case INTERACTIONLIST:
			InteractionListPresenter.initalize();
			break;
		case REFRESHSCREEN:
			try {
				LoginPresenter.globalDataRetrieval();
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case SALESSECURITYDEPOSITLIST:
			SalesSecurityDepositListPresenter.initialize();
			break;
		case SERVICESECURITYDEPOSITLIST:
			ServiceSecurityDepositListPresenter.initialize();
			break;
			
		case ACCOUNTSDASHBOARDAP:
			AccountsAPHomePresenter.initalize();
			break;
			
		case PROCESSCONFIGURATION:
			ProcessConfigurationPresenter.initalize();
			break;
		case DOCUMENTHISTORY:
			DocumentHistoryListPresenter.initalize();
			break;
		case USERAUTHORIZE:
			UserAuthorizationPresenter.initalize();
			break;
		case LOGGEDINHISTORY:
			LoggedInHistoryPresenter.initalize();
			break;
		case LICENSEMANAGEMENT:
			LicenseManagementPresenter.initalize();
			break;
		case ASSETMANAGEMENTDASHBOARD:
			AssetManagementDashboardPresenter.initalize();
			break;
		case DEMOCONFIGURATION:
			DemoConfigSummaryPresenter.initalize();
			break;
		case INSPECTION:
			InspectionPresenter.initalize();
			break;
		case GRNLIST:
			GRNListPresenter.initalize();
			break;
		case INSPECTIONLIST:
			InspectionListPresenter.initalize();
			break;
		case BILLOFPRODUCTMATERIAL:
			BillOfProductMaterialPresenter.initialize();
			break;
		case WORKORDER:
			WorkOrderPresenter.initialize();
			break;
			
		case CUSTOMERBRANCH:
			CustomerBranchPresenter.initalize();
			break;
		case STOCKALERT:
			StockAlertPresenter.initalize();
			break;
		case MATERIALCONSUMPTIONREPORT:
			MaterialConsumptionReportPresenter.initialize();
			break;
		case SMSTEMPLATE:
			SmsTemplatePresenter.initalize();
			break;
		case SMSCONFIG:
			SmsConfigurationPresenter.initalize();
			break;
		case IPADDRESSAUTHORIZATION:
			IPAddressAuthorizationPresenter.initalize();
			break;
		case MULTILEVELAPPROVAL:
			MultilevelApprovalPresenter.initalize();
			break;
		case LICENSEINFO:
			LicenseInfoPresenter.initalize();
			break;
		case FUMIGATION:
			FumigationPresenter.initalize();
			break;
	//*************added by rohan ***********************888		
		case CUSTOMERTRAINING:
			CustomerTrainingPresenter.initalize();
			break;
			
		case PETTYCASHTRANSACTIONLIST:
			PettyCashTransactionListPresentor.initalize();
			break;
			
			
//			,
		case EMPLOYEEADDITIONALDETAILS:
			EmployeeAdditionalDetailsPresenter.initalize();
			break;		
			
		case OVERTIME:
			OvertimePresenter.initalize();
			break;	
			
		case SALARYSLIP:
			SalaryslipPresenter.initalize();
			break;	
			
		case COMPANYSCONTRIBUTION:
			CompanysContributionListPresenter.initalize();
			break;	
			
		case DATAMIGRATION:
			DataMigrationPresentor.initialize();
			break;	
			
		case FUMIGATIONAUSTRALIA:
			FumigationAustraliaPresenter.initalize();
			break;	
			
		case DECLARATION:
			DeclarationPresenter.initalize();
			break;	
			
		case CONTRACTRENEWAL:
			ContractRenewalPresenter.initalize();
			break;	
			
			
		case DATAMIGRATIONTASKQUEUE:
			DataMigrationTaskQueuePresenter.initialize();
			break;		
			
		case QUICKCONTRACT:
			QuickContractPresentor.initalize();
			break;		
		
			
		case SALESPERSONDASHBOARD:
			SalesPerformancePresentor.initalize();
			break;	
			
		case SALESPERSONTARGET:
			SalesPersonTargetPresentor.initalize();
			break;	
			
		case SALESPERSONPERFORMANCE:
			SalesPersonTargetAchievementPresentor.initalize();
			break;	
			
		case FINANCIALCALENDAR:
			FinancialCalendarPresenter.initalize();
			break;	
			
		case COMPLAINDASHBOARD:
			complainHomePresenter.initialize();
			break;	
		
		case COMPANYASSETLIST:
			CompanyAssetListPresentor.initalize();
			break;	
			
		case TECHNICIANDASHBOARD:
			TechnicianDashboardPresenter.initalize();
			break;	
			
		case ASSESMENTREPORT:	
			AssesmentReportPresenter.initalize();
			break;
			
		case SMSHISTORY:	
			SmsHistoryListPresenter.initalize();
			break;
			
		case MULTIPLEEXPENSEMANAGMENT:
			MultipleExpensemanagmentPresenter.initalize();
			break;
			
		case INVENTORYTRANSCATIONERRORUPDATE:
			InventoryTransactionErrorUpdatePresenter.initialize();
			break;
			
		case PURCHASEREQUISITIONLIST:
			PurchaseRequisitionListPresenter.initialize();
			break;
				
		case SERVICEPO:
			ServicePoPresenter.initialize();
			break;
			
		case ROLEDEFINITION:
			RoleDefinitionPresenter.initialize();
			break;
			
		case CUSTOMERLIST:
			CustomerListGridPresenter.initialize();
			break;
		case EMPLOYEELEVEL:
			EmployeeLevelPresenter.initialize();
			break;
			
			//  Ajinkya Added this Screen   
		case OUTSTANDINGREPORT:
			OutstandingReportPresenter.initialize();
			break;	
			
			// Date 14 july 2017 added by vijay
		case QUICKSALESORDER:
			QuickSalesOrderPresenter.initalize();
			break;
			
			//Date 16-08-2017 added by vijay
		case QUICKPURCHASEORDER:
			QuickPurchaseOrderPreseneter.initalize();
			break;
			
			//Date 10-10-2017 added by vijay
		case TECHNICIANSCHEDULNG:
			TechnicianScheduleListPresenter.initialize(false);
			break;
			
			//Date 29/3/2018 added by manisha
		case ASSESSMENTSCHEDULING:
			TechnicianScheduleListPresenter.initialize(true);
			break;	
		/** Date 03.03.2018 added by komal for nbhc reports screens **/
		case REVENUEREPORT:
			ReportPresenter.initialize(Screen.REVENUEREPORT);
			break;
		case TECHNICIANREVENUEREPORT:
			ReportPresenter.initialize(Screen.TECHNICIANREVENUEREPORT);
			break;
		case SERVICEREVENUEREPORT:
			ReportPresenter.initialize(Screen.SERVICEREVENUEREPORT);
			break;
		case COMMODITYFUMIGATIONREPORT:
			ReportPresenter.initialize(Screen.COMMODITYFUMIGATIONREPORT);
			break;
		case CLOSINGSTOCKREPORT:
			ReportPresenter.initialize(Screen.CLOSINGSTOCKREPORT);
			 break;
			 /** Date 14.03.2018 added by komal for nbhc reports screens **/
		case ACTIVECONTRACTREPORT:
			ReportPresenter.initialize(Screen.ACTIVECONTRACTREPORT);
			break;
		case AMCASSUREDREVENUEREPORT:
			ReportPresenter.initialize(Screen.AMCASSUREDREVENUEREPORT);
			break;
		 /**
		 * end komal
		 */	
			/**
			 * Date : 02/04/2018 By Anil
			 * Service cylce dashboard in smartGwt
			 */
		case LEADDASHBOARD:
			LeadDashboardPresenter.initialize();
			break;
		case QUOTATIONDASHBOARD:
			QuotationDashboardPresenter.initialize();
			break;
		case CONTRACTDASHBOARD:
			ContractDashboardPresenter.initialize();
			break;
			/**
			 * End
			 */
		/** date 29/11/2017 added by komal to open customer log form for hvac **/
		case CUSTOMERCALLLOGDETAILS:
			CustomerLogDetailsPresenter.initalize();
			break;
		/** date 05/15/2017 added by komal to see all pending contracts **/
		case CONTRACTLIST:
			PendingContractPresenter.initalize();
			break;
		/** date 26/12/2017 added by komal for hvac defective Material List **/
		case DEFECTIVEMATERIALLIST:
			DefectiveMaterialPresenter.initalize();
			break;		
			/** date 06/04/2018 added by komal for ALP fumigation**/
		case FUMIGATIONALP:
			 FumigationALPPresenter.initialize();
			break;		
			
			/**
			 * Date : 28-04-2018 By ANIL
			 */
		case CTCALLOCATION:
			CTCAllocationPresenter.initialize();
			break;
			/**
			 * Date : 30-04-2018 By ANIL
			 */
		case CTCTEMPLATE:
			CTCTemplatePresenter.initialize();
			break;

			/**
			 * Date : 13-07-2018 By VIJAY for NBHC CCPM
			 */
		case ASSESSMENTREPORTUNIT:
			AssessmentReportUnitPresenter.initalize();
			break;
			
			/**
			 * Date 4-7-2018 by jayshree
			 */
			
		case STATUTERYREPORT:
			StatuteryReportPresenter.initalize();
			break;
			//End
			/** DATE 11.7.2018 ADDED BY komal for attendance list**/
		case ATTENDANCElIST:
			AttendanceListPresenter.initialize();
			break;
			/** 18.7.2018 added by komal **/
		case TAXSLABCONFIGURATION:
			TaxSlabPresenter.initialize();
			break;
			
		case INVESTENTCONFIGURATION:
			InvestmentPresenter.initialize();
			break;
		case EMPLOYEEINVESTMENT:
			EmployeeInvestmentPresenter.initialize();
			break;
			/** date 24.7.2018 added by komal for employee asset allocation  **/
		case EMPLOYEEASSETALLOCATION :
			EmployeeAssetAllocationPresenter.initialize();
			break;
			/** date 03.08.2018 added by komal for employee arrears management **/
		case ARREARSMANAGEMENT:
			ArrearsManagementPresenter.initialize();
			break;
			/** date 07.08.2018 added by komal **/
		case EMPLOYEECHECKLISTTYPE:
			EmployeeCheckListTypePresenter.initialize();
			break;
		case PROFESSIONALTAX:
			PTPresenter.initialize();
			break;
		case PROJECTALLOCATION:
			ProjectAllocationPresenter.initalize();
			break;
			/** Date 25.6.2018 added by komal for vendor invoice and payment**/
		case VENDORINVOICELIST:
			 VendorInvoiceListPresenter.initalize();
			break;
		case VENDORINVOICEDETAILS:
			VendorInvoiceDetailsPresenter.initalize();
			break;
		case VENDORPAYMENTLIST:
			PaymentListPresenter.initalize(true);
			break;
		case VENDORPAYMENTDETAILS:
			PaymentDetailsPresenter.initalize(true);
			break;
		case CNC:
			CNCPresenter.initalize();
			break;
			/** date 6.10.2018 added by komal**/
		case LWF :
			LWFPresenter.initialize();
			break;
			/** date 22-11-2018 added by Anil**/
		case VOLUNTARYPF :
			VoluntaryPfPresenter.initialize();
			break;
		case VOLUNTARYPFHISTORY :
			VoluntaryPfHistoryListPresenter.initialize();
			break;
			/**
			 * Date : 12-12-2018 BY ANIL
			 */
		case COMPLIANCEFORM :
			CompliancePresenter.initialize();
			break;
			/**
			 * Date : 20-12-2018 BY AMOL
			 */
		case SALESREGISTERREPORT :
			SalesRegisterFormPresenter.initialize();
			break;

			/**29-12-2018 BY AMOL**/
		case PAYMENTDASHBOARD :
			PaymentdashboardPresenter.initialize();
			break;
			
	//	case CONTRACTRENEWALDASHBOARD :
	//		ContractRenewalDashboardPresenter.initialize();
	//		break;
			/** date 7.3.2018 added  by komal**/
		case TECHNICIANWAREHOUSEDETAILS:
			TechnicianWareHouseDetailsPresenter.initialize();
			break;
			
			/** Date 16-08-2019 by Vijay for branch level Uploading**/
		case TRANSACTIONUPLOADING:
			TransactionsUploadingPresenter.initialize();
			break;
			
			/** Date 23-08-2019 by Vijay for NBHC CCPM **/
		case CUSTOMERSUPPORT:
			CustomerSupportPresenter.initialize();
			break;
			
		case CUSTOMERSUPPORTDASHBOARD:
			CustomerSupportDashboardPresenter.initialize();
			break;
			
		case EXPENSEPOLICIES:
			ExpensePoliciesPresenter.initalize();
			break;
			
			/** Date 04-02-2020 by Vijay for NBHC CCPM fumigaton tracker **/
		case INHOUSESERVICE:
			ServicePresenter.initalize(INHOUSESERVICE);
			break;
			
		case SCREENMENUCONFIGURATION:
			ScreenMenuConfigurationPresenter.initialize();
			break;
			
		case QUICKEMPLOYEESCREEN:
			QuickEmployeePresenter.initalize();
			break;
			
			
			
	//**********************changes ends here *************************		
			
			
		case COMPANYTYPE:
		case CUSTOMERLEVEL:
		case CUSTOMERSTATUS:
		case LEADSTATUS:
		case LEADPRIORITY:
		case CONTRACTSTATUS:
		case SALESORDERSTATUS:
		case CUSTOMERPRIORITY:
		case QUOTATIONPRIORITY:
		case QUOTATIONSTATUS:
		case SALESQUOTATIONPRIORITY:
		case SALESQUOTATIONSTATUS:
		case EMPLOYEEROLE:
		case EMPLOYEEDESIGNATION:
		case ACESSLEVEL:
		case PAYMENTMETHODS:
		case PAYMENTTERMS:
		case CURRENCY:
		case SERVICESTATUS:
		case COMPLAINPRIORITY:
		case COMPLAINSTATUS:
		case ACTIONPRIORITY:
		case EMPLOYEETYPE:
		case UNITOFMEASUREMENT:
		case SHIFTCATEGORY:
		case PATTERNCATEGORY:
		case CUSTOMERGROUP:
		case TAXCLASSIFICATIONCODE:
		case LEADGROUP:
		case QUOTATIONGROUP:
		case CONTRACTGROUP:
		case SALESQUOTATIONGROUP:
		case SALESORDERGROUP:
		case COMPLAINGROUP:
		case VENDORGROUP:
		case BILLINGGROUP:
		case ATTENDANCECONFIG:
		case GLACCOUNTGROUP:
		case EXPENSEGROUP:
		case ACTIONTAKEN:
		case ACTIONSTATUS:
		case ASSETCATEGORY:
		case CLIENTSIDEASSETCATEGORY:
		case PRODUCTTYPE:
		case PRODUCTGROUP:
		case PRODUCTCLASSIFICATION:
		case CHECKLISTCATEGORY:
		case DELIVERYNOTEGROUP:
		case PROCESSNAME:
		case COMMUNICATIONCATEGORY:
		case COMMUNICATIONTYPE:
		case PERSONTYPE:
		case GROUPTYPE:
		case CONTACTROLE:
		case PRGROUP:
		case GRNGROUP:
		case MMNDIRECTION:
		case INTERACTIONPRIORITY:
		case TICKETPRIORITY:
		case TICKETLEVEL:
		case PROCESSTYPE:
		case ARTICLETYPE:
		case INSPECTIONGROUP:
		case WORKORDERGROUP:
		case SERVICETYPE:
		case INVOICEGROUP:
		case APPROVALLEVEL:
		case FUMIGATIONDECLAREDPOE:
		case DOSAGERATEOFFUMIGANT:
		case DURATIONOFFUMIGATION:
		case MINIMUMAIRTEMP:
			
		case PLACEOFFUMIGATION:
		case COUNTRYOFLOADING:
		case QUANTITYDECLAREUNIT:	
			
		case EMPLOYEEFAMILYRELATION:
		case ADDRESSTYPE:
			
		case SPECIFICREASONFORRESCHEDULING:
        			
		case DOCUMENTTYPE:
		case NUMBERRANGE:
		case TDSPERCENTAGE:
			
		case CATCHTRAPPESTNAME:	
		case ASSETARTICLETYPE:
			
		case ACTIONPLANFORCOMPANY:
		case ACTIONPLANFORCUSTOMER:
			
		case WAREHOUSETYPE:
		case SALUTATION:
		case RELIGION:
		case SERVICEUOM:
		case CONTRACTPERIOD:
			/**Date :17/02/2018 BY: Manisha 
			 * Create master for frequency and TypeOfTreatment as per sonu's requirement.!! 
			 */	
	    case FREQUENCY1:
		case TYPEOFTREATMENT:
           /**END**/
		/** date 05/12/2017 added by komal for hvac **/
		case WARRANTYSTATUS :
		case CALLTYPE  :	
		case CALLBACKSTATUS :
		case VISITSTATUS :
		case CALLLOGSTATUS :
			
			/*Date 16-6-2018 By jayshree**/
		case LEADUNSUCCESFULREASONS:
		case FUMIGATIONPERFORMED://add by jayshree
		case VENTILATED://Add By jayshree
//			configscreenswitchcase();
			/**date 19.7.2018 added  by komal for tax year and category **/
		case  TAXATIONYEAR :
		case  TAXCATEGORY :
		case SECTIONNAME:
		case TRAINING:
		case BRAND:
		case CASTE:	
		case DONOTRENEW:	
		case CANCELLATIONREMARK:
		case CLASSIFICATIONOFCITY:
		case SERVICECHECKLIST:
		case ASSESSMENTRISKLEVEL:
		case TECHNICIANREMARK:	
		case COMPANYPAYMENTTYPE:
			configscreenswitchcase();
			break;
		case EXPENSECATEGORY:
		case CUSTOMERCATEGORY:
		case LEADCATEGORY:
		case QUOTATIONCATEGORY:
		case CONTRACTCATEGORY:
		case VENDORCATEGORY:
		case COMPLAINCATEGORY:
		case BILLINGCATEGORY:
		case SALESQUOTATIONCATEGORY:
		case SALESORDERCATEGORY:
		case GRNCATEGORY:
		case DELIVERYNOTECATEGORY:
		case MODULENAME:
		case PURCHASEORDERCATEGORY:
		case LOICATEGORY:
		case RFQCATEGORY:
		case PRCATEGORY:
		case MRNCATEGORY:
		case MINCATEGORY:
		case MMNCATEGORY:
		case TICKETCATEGORY:
		case INSPECTIONCATEGORY:
		case WORKORDERCATEGORY:
		case INVOICECATEGORY:
		case ADDPAYMENTTERMS:		
		case ASSESSMENTREPORTCATEGORY:	
		case SERVICEPOCATEGORY:
		/**
		 *  nidhi
		 *  22-12-2017
		 *  for email reminder process
		 */
		case CRONJOB:
		case FREQUENCY:
		case ANDROIDTIPS:	
		/** 
		 * end
		 */
		/** date 31.8.2018 added by komal**/
		case CTCCATEGORY:
		com.slicktechnologies.client.config.categoryconfig.CategoryPresenter.initalize();
		break;
		
		case CUSTOMERTYPE:
		case QUOTATIONTYPE:
		case CONTRACTTYPE:
		case LEADTYPE:
		case VENDORTYPE:
		case COMPLAINTYPE:
		case EXPENSETYPE:
		case BILLINGTYPE:
		case SALESORDERTYPE:
		case SALESQUOTATIONTYPE:
		case GRNTYPE:
		case DELIVERYNOTETYPE:
		case DOCUMENTNAME:
		case PURCHASEORDERTYPE:
		case RFQTYPE:
		case PRTYPE:
		case LOITYPE:
		case MRNTYPE:
		case MINTYPE:
		case MMNTYPE:
		case TICKETTYPE:
		case INSPECTIONTYPE:
		case WORKORDERTYPE:
		case INVOICECONFIGTYPE:
		case ASSESSMENTREPORTDEFICIENCYTYPE:
		case SERVICEPOTYPE:
		
			TypePresenter.initalize();
			
			/**
			 *  nidhi
			 *  25-12-2017
			 *  cron job configration
			 */
			break;
			case CRONJOBCONFIGRATION:
				CronJobConfigrationPresenter.initalize();
				break;
				/*
				 * nidhi
				 * 7-09-2018
				 */
		case UNITCONVERSION:
			UnitConversionPresenter.initalize(ConfigTypes.getConfigType(UNITOFMEASUREMENT));
			break;
		case SERVICEUNITCONVERSION:
			UnitConversionPresenter.initalize(ConfigTypes.getConfigType(SERVICEUOM));
			break;
		/**
		 * nidhi
		 * 1-10-2018
		 *  *:*:*
		 */
		case BOMSERVICEMATRIALREPORT:
			ReportPresenter.initialize(Screen.BOMSERVICEMATRIALREPORT);
			break;
		case SERVICEINVOICEREPORT:
			ReportPresenter.initialize(Screen.SERVICEINVOICEREPORT);
			break;
			/**
			 * nidhi 20-12-2018
			 */
		case RENEWALCONTRACTREPORT:
			ReportPresenter.initialize(Screen.RENEWALCONTRACTREPORT);
			break;
			/**
			 * @author Anil , Date : 11-03-2019
			 */
		case EMPLOYEELEAVEHISTORY:
			EmployeeLeaveHistoryListPresenter.initialize();
			break;
		/**
		 * Updated By: Viraj
		 * Date: 01-04-2019
		 * Description: For inventory list of franchise,master franchise in brand and billed unbilled bashboard	
		 */
		case INVENTORYlIST:
			InventoryListPresenter.initialize();
			break;
			
		case BILLEDUNBILLED: 
			ArDashboardPresenter.initalize();
			break;
		case CONTRACTPANDLREPORT:
			ContractPAndLReportPresenter.initialize();
			break;
	/** date 5.4.2019 added by komal to add download of branch wise split invoice report(nbhc specific)****/
		case BRANCHWISESPLITINVOICEREPORT:
			ReportPresenter.initialize(Screen.BRANCHWISESPLITINVOICEREPORT);
			break;
			
		case SALESLEADDASHBOARD:
//			LeadDashboardPresenter.initialize();
			SalesLeadDashboardPresenter.initialize();
			break;
		case APPREGISTRATIONHISTORY:
			DeregisterPresenter.initialize();
			break;
			
			/**
			 * @author Anil , Date : 26-06-2019
			 */
		case PAIDLEAVE:
			PaidLeavePresenter.initalize();
			break;
		case BONUS:
			BonusPresenter.initalize();
			break;
		case PROVIDENTFUND:
			ProvidentFundPresenter.initalize();
			break;
		case ESIC:
			EsicPresenter.initialize();
			break;
			
		case SERVICEREVENUELOSSREPORT:
			ReportPresenter.initialize(SERVICEREVENUELOSSREPORT);
			break;

		case EMPLOYEEOVERTIMEHISTORY:
			EmployeeOvertimeHistoryListPresenter.initialize();
			break;		
			
		case LOCKSEALDEVIATIONREPORT:
			ReportPresenter.initialize(LOCKSEALDEVIATIONREPORT);
			break;	
		case CREATEATTENDANCE:
			CreateAttendancePresenter.initialize();
			break;	
		
		case REPORTSCONFIGURATIONS:
			DsReportsConfigurationPresenter.initalize();
			break;
		case REPORTAUTHORISATION:
			DsReportsAuthorizationPresenter.initalize();
			break;
			
		case REPORTS:
			DsReportsPresenter.initalize();
			break;
			
			
		case COMPANYPROFILECONFIGURATION:
			CompanyProfileConfigurationPresenter.initalize();
			break;
			
		case CREDITNOTE :
			CreditNoteListGridPresenter.initialize();
			break;
			
		case EMAILTEMPLATE:
			EmailTemplatePresenter.initalize();
			break;
			
		case EMAILTEMPLATECONFIGURATION:
			EmailTemplateConfigurationPresenter.initalize();
			break;
				
		case IMPLEMENTATIONSCREEN :
			ImplementationPresenter.initalize();
			break;
		case TERMSANDCONDITIONS:
			TermsAndConditionsPresenter.initalize();
			break;
			
		}
		/** Ends **/
		
		
	

	}

	@SuppressWarnings("unused")
	private void configscreenswitchcase() {
		ConfigTabels table = new ConfigTabels();
		ConfigForm form = new ConfigForm(table, FormTableScreen.UPPER_MODE,
				true);
		ConfigPresenter p = new ConfigPresenter(form, new Config());
		AppMemory.getAppMemory().stickPnel(form);

	}

}
