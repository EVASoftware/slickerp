package com.slicktechnologies.client.utility;

import java.util.Date;

import com.google.gwt.user.client.Timer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.project.concproject.ProjectForm;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

public class ProcessLevelMenu 
{
 /* public static void createLead(PersonInfo cinfo,final String branchName,final String employeeName)
  {
	  AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Leads",Screen.LEAD);
	  LeadPresenter.initalize();
	  final LeadForm form=LeadPresenter.form;
	  form.setToNewState();
	  form.showWaitSymbol();
		    // Create a new timer that calls Window.alert().
		    Timer t = new Timer() {
		      @Override
		      public void run() {
		    	  AppMemory.getAppMemory().setList(branchName,form.getOlbbBranch());
		    	  AppMemory.getAppMemory().setList(employeeName,form.getOlbeSalesPerson()  );
		    	  form.hideWaitSymbol();
		      }
		    };

		    // Schedule the timer to run once in 5 seconds.
		    t.schedule(3000);
		form.getPic().setValue(cinfo);
		form.getPic().setEnable(false);
		
  }
  
  
  public static void createQuotation(int leadid, PersonInfo cinfo,final String branchName,final String employeeName)
  {
	  AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Quotations",Screen.QUOTATION);
	  QuotationPresenter.initalize();
	  final QuotationForm form=QuotationPresenter.form;
	  form.setToNewState();
	  form.showWaitSymbol();
		Timer t = new Timer() {
		      @Override
		      public void run() {
		    	  AppMemory.getAppMemory().setList(branchName,form.getOlbbBranch());
		    	  AppMemory.getAppMemory().setList(employeeName,form.getOlbeSalesPerson()); 
		    	  form.hideWaitSymbol();
		      }
		    };

		    // Schedule the timer to run once in 5 seconds.
		  t.schedule(3000);  	
		form.getPersonInfoComposite().setValue(cinfo);
		form.getPersonInfoComposite().setEnable(false);
		if(leadid>=0)
		{
			form.getTbLeadId().setValue(leadid);
		}
		
  }
  
  public static void createContract(int leadid, PersonInfo cinfo,final String branchName,final String employeeName)
  {
	  AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Contracts",Screen.CONTRACT);
	  ContractPresenter.initalize();
	  final ContractForm form=ContractPresenter.form;
	  form.setToNewState();
	  form.showWaitSymbol();
		Timer t = new Timer() {
		      @SuppressWarnings("static-access")
			@Override
		      public void run() {
		    	  AppMemory.getAppMemory().setList(branchName,form.getOlbbBranch());
		    	  AppMemory.getAppMemory().setList(employeeName,form.getOlbeSalesPerson());
		    	  form.hideWaitSymbol();
		  		
		        
		      }
		    };
		    t.schedule(3000);
		form.getPersonInfoComposite().setValue(cinfo);
		form.getPersonInfoComposite().setEnable(false);
		if(leadid>0)
		{
			form.getTbLeadId().setValue(leadid);
		}
  }
  
  public static void createProject(int contractid,PersonInfo cinfo,Date contractStartDate,Date contractEndDate)
  {
	  AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Project", Screen.PROJECT);
	  //ProjectPresenter.initalize();
	  final ProjectForm form=ProjectPresenter.initalize(contractid);
	  form.setToNewState();
	  Timer t=new Timer()
	  {
		
		@Override
		public void run() {
			//form.hideWaitSymbol();
		}
	};
	t.schedule(3000);
	form.getPic().setValue(cinfo);
	form.getPic().setEnable(false);
	if(contractid>0)
	{
		form.getIbContractId().setValue(contractid);
		System.out.println("ppppppppppppppp");
		form.initalizeClientSideAssetComboBox();
		System.out.println("AAAAAAAAAAA");
		form.getDbContractStartDate().setValue(contractStartDate);
		form.getDbContractEndDate().setValue(contractEndDate);
		ProjectPresenter presenter=(ProjectPresenter) form.getPresenter();
		
		form.ibContractId.setEnabled(false);
		AppUtility.makeClientSideAssetListBoxLive(form.olbClientSideAsset);
	}
  }
  
  
  
 /* public static void convertQuotationToContract(CustomerInfo cinfo,final String branchName,final String employeeName
		  ,Vector<SuperProductEntity>products, double d, double e, DocumentUploadEntity documentUploadEntity)
  {
	    AppUtilites.changeCurrentScreen("Contracts",Screen.CONTRACT);
		ConcreteSearchForm<ContractEntity> customerPopup=new ConcreteSearchForm<ContractEntity>();
		ContractTable table = new ContractTable();
		ContractPopUp popup=new ContractPopUp(customerPopup, table);
		
		final ContractForm form=new ContractForm();
		ContractView view =new ContractView(popup,form);
		
		
		ContractEntity entity=new ContractEntity();
		entity.setCinfo(cinfo);
		ContractPresenter presenter = new ContractPresenter(entity,view);
		Timer t = new Timer() {
		      @Override
		      public void run() {
		    	  AppUtilites.setList(branchName,form.getSalesInfo().getBranch());
		  		  AppUtilites.setList(employeeName,form.getSalesInfo().getSalesperson());
		  		  
		        
		      }
		    };
		    t.schedule(3000);
		form.getCinfo().setValue(cinfo);
		form.getMultipleProducts().setValue(products);
		AppUtilites.stickPnel(view);
  }
  
  */
 /* public static void convertQuotationToContract(final Contract entity)
  {
	  AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Contracts",Screen.CONTRACT);
	  ContractPresenter.initalize();
	  final ContractForm form=ContractPresenter.form;
	  form.setToNewState();
		Timer t = new Timer() {
		      @Override
		      public void run() {
		    	 
		    	  form.updateView(entity);
		    	  form.getTbContractId().setText("");
		        
		      }
		    };
		    t.schedule(3000);
  }
 */
  
}
