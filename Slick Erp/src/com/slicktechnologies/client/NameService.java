package com.slicktechnologies.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
@RemoteServiceRelativePath("clientname")
public interface NameService extends RemoteService 
{
   public String getClientName(String url);
}
