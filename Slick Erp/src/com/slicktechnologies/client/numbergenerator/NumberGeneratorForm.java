package com.slicktechnologies.client.numbergenerator;

import java.util.Date;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;


public class NumberGeneratorForm extends FormTableScreen<NumberGeneration>  {

	MyLongBox ibNumber;
	TextArea taRemark;
	CheckBox cbStatus;
	ObjectListBox<ProcessName> oblProcessName; 
	Button btnCopy;
	NumberGeneration numberGenerationObj;

	public NumberGeneratorForm  (SuperTable<NumberGeneration> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();

	}
	
	private void initializeWidget(){
		
		ibNumber = new MyLongBox();
		taRemark = new TextArea();
		cbStatus = new CheckBox();
		cbStatus.setValue(true);
		oblProcessName = new ObjectListBox<ProcessName>();
		ProcessName.initializeProcessName(oblProcessName);
		btnCopy=new Button("Copy Classes");

	}
	

	@Override
	public void createScreen() {
		
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		FormField fgroupingNumberGeneratorInformation=fbuilder.setlabel("Number Generator Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("* Class Name",oblProcessName);
		FormField ftbProcessName= fbuilder.setMandatory(true).setMandatoryMsg("Process Name is mandatory").setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("* Number ",ibNumber);
		FormField ftbNumberName= fbuilder.setMandatory(true).setMandatoryMsg("Number is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField ftbCheckbox= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Remark",taRemark);
		FormField ftbRemark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("",btnCopy);
		FormField fbtnCopy= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = { 
				{fgroupingNumberGeneratorInformation},
				
				{ftbProcessName,ftbNumberName,ftbCheckbox,fbtnCopy},
				{ftbRemark}
				
		};

		this.fields=formfield;

	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(NumberGeneration model) {
		if(oblProcessName.getValue()!=null)
			model.setProcessName(oblProcessName.getValue());
		model.setNumber(ibNumber.getValue());
		if(taRemark.getValue()!=null)
			model.setRemark(taRemark.getValue());
		if(cbStatus.getValue()!=null)
			model.setStatus(cbStatus.getValue());
		model.setUpdatedBy(LoginPresenter.loggedInUser);
		model.setLastUpdatedDate(new Date());
		numberGenerationObj=model;
		presenter.setModel(model);
	}

	@Override
	public void updateView(NumberGeneration view) {
		
		numberGenerationObj=view;
		if(view.getProcessName()!=null)
			oblProcessName.setValue(view.getProcessName());
		
		
		ibNumber.setValue(view.getNumber());
		
		if(view.getRemark()!=null)
			taRemark.setValue(view.getRemark());
		
		if(view.getStatus()!=null)
			cbStatus.setValue(view.getStatus());
			
			
	presenter.setModel(view);
		
	}
	

	@Override
	public void toggleAppHeaderBarMenu() {
	
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   

				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
						menus[k].setVisible(true); 

					else
						menus[k].setVisible(false);  		   
				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
						menus[k].setVisible(true); 

					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.NUMBERGENERATOR,LoginPresenter.currentModule.trim());
		}
	
	@Override
	public boolean validate() {
		boolean superValidate=super.validate();
		boolean nrValidate=true;
		
		/*** Date 04 oct 2017 if condition added by  vijay as per rohan and nitin sir *********/
		/**
		 * Date : 18-11-2017 BY ANIL
		 * Old process type-NumberRangeNonValidation
		 * New process type-RemoveNumberRangeValidation
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberGeneration", "RemoveNumberRangeValidation")){
			return superValidate && nrValidate;
		}else{
		
			if(ibNumber.getValue() > 1000000000){
				
				showDialogMessage("Please enter number range in 9 digits");
				nrValidate = false;
				
			}
			if(ibNumber.getValue() < 99999999){
				
				showDialogMessage("Please enter number range in 9 digits");
				nrValidate = false;
				
			}
			 
			return superValidate && nrValidate;
		}
		
		
	}
	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		cbStatus.setValue(true);
		
	}
	

	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new NumberGeneration();
		model=numberGenerationObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.NUMBERGENERATION, numberGenerationObj.getCount(), null,null,null, false, model, null);
	
	}
	
	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(numberGenerationObj!=null){
			SuperModel model=new NumberGeneration();
			model=numberGenerationObj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.NUMBERGENERATION, numberGenerationObj.getCount(), null,null,null, false, model, null);
		
		}
	}
	
	
	
		
	}

	

