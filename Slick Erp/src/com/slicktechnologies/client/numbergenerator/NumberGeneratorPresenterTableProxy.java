package com.slicktechnologies.client.numbergenerator;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.numbergenerator.NumberGeneratorPresenter.NumberGeneratorPresenterTable;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;

public class NumberGeneratorPresenterTableProxy extends NumberGeneratorPresenterTable {

	TextColumn<NumberGeneration> getProcessNameColumn;
	TextColumn<NumberGeneration> getNumberColumn;
	TextColumn<NumberGeneration> getstatusColumn;

	public Object getVarRef(String varName) {
		if (varName.equals("getProcessNameColumn"))
			return this.getProcessNameColumn;
		if (varName.equals("getNumberColumn"))
			return this.getNumberColumn;
		if (varName.equals("getstatusColumn"))
			return this.getstatusColumn;

		return null;
	}

	public NumberGeneratorPresenterTableProxy() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetProcessName();
		addColumngetNumber();
		addColumngetStatus();
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<NumberGeneration>() {
			@Override
			public Object getKey(NumberGeneration item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	public void addColumnSorting() {
		addSortinggetProcessName();
		addSortinggetNumber();
		addSortinggetStatus();
	}

	@Override
	public void addFieldUpdater() {
	}

	protected void addSortinggetProcessName() {
		List<NumberGeneration> list = getDataprovider().getList();
		columnSort = new ListHandler<NumberGeneration>(list);
		columnSort.setComparator(getProcessNameColumn,
				new Comparator<NumberGeneration>() {
					@Override
					public int compare(NumberGeneration e1, NumberGeneration e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProcessName() != null
									&& e2.getProcessName() != null) {
								return e1.getProcessName().compareTo(
										e2.getProcessName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetProcessName() {
		getProcessNameColumn = new TextColumn<NumberGeneration>() {
			@Override
			public String getValue(NumberGeneration object) {
				return object.getProcessName();
			}
		};
		table.addColumn(getProcessNameColumn, "Process Name");
		getProcessNameColumn.setSortable(true);
		table.setColumnWidth(getProcessNameColumn, "110px");
	}

	protected void addSortinggetNumber() {
		List<NumberGeneration> list = getDataprovider().getList();
		columnSort = new ListHandler<NumberGeneration>(list);
		columnSort.setComparator(getNumberColumn,
				new Comparator<NumberGeneration>() {
					@Override
					public int compare(NumberGeneration e1, NumberGeneration e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCount() == e2.getCount()) {
								return 0;
							}
							if (e1.getCount() > e2.getCount()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetNumber() {
		getNumberColumn = new TextColumn<NumberGeneration>() {
			@Override
			public String getValue(NumberGeneration object) {
					return object.getNumber()+"";
			}
		};
		table.addColumn(getNumberColumn, "Number");
		getNumberColumn.setSortable(true);
		table.setColumnWidth(getNumberColumn, "110px");
	}

	protected void addSortinggetStatus() {
		List<NumberGeneration> list = getDataprovider().getList();
		columnSort = new ListHandler<NumberGeneration>(list);
		columnSort.setComparator(getstatusColumn,
				new Comparator<NumberGeneration>() {
					@Override
					public int compare(NumberGeneration e1, NumberGeneration e2) {
						if (e1 != null && e2 != null) {
							if (e1.getStatus() == e2.getStatus()) {
								return 0;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetStatus() {
		getstatusColumn = new TextColumn<NumberGeneration>() {
			@Override
			public String getValue(NumberGeneration object) {
				if (object.getStatus() == true){
					return "Active";
				}
				return "In Active";
			}
		};
		table.addColumn(getstatusColumn, "Status");
		getstatusColumn.setSortable(true);
		table.setColumnWidth(getstatusColumn, "110px");
	}

}
