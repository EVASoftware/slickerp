package com.slicktechnologies.client.numbergenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

public class NumberGeneratorPresenter extends FormTableScreenPresenter<NumberGeneration> {
	
	NumberGeneratorForm form;
	final GenricServiceAsync async=GWT.create(GenricService.class);

	public NumberGeneratorPresenter (FormTableScreen<NumberGeneration> view,
			NumberGeneration model) {
		super(view, model);
		form=(NumberGeneratorForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getNumberGeneratorQuery());
		form.setPresenter(this);
		form.btnCopy.addClickHandler(this);
	}
	
	
	
  public static NumberGeneratorForm initalize(){
	  
	  NumberGeneratorPresenterTable gentableScreen=new NumberGeneratorPresenterTableProxy();
	  NumberGeneratorForm form = new NumberGeneratorForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		NumberGeneratorPresenter presenter = new NumberGeneratorPresenter(form,new NumberGeneration());
		  AppMemory.getAppMemory().stickPnel(form);
		return form;
		  
	  
  }
  
  
  
  
  public MyQuerry getNumberGeneratorQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new NumberGeneration());
		return quer;
	}
	

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
	}
	

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.numbergenerator.NumberGeneration")
	 public static class  NumberGeneratorPresenterTable extends SuperTable<NumberGeneration> implements GeneratedVariableRefrence{

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub
			
		}} ;
		
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.numbergenerator.NumberGeneration")
		 public static class  NumberGeneratorPresenterSearch extends SearchPopUpScreen<NumberGeneration>{

			@Override
			public MyQuerry getQuerry() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean validate() {
				// TODO Auto-generated method stub
				return true;
			}
			}
		
		
		public ArrayList<String> classesList()
		{
			ArrayList<String> classessArray=new ArrayList<String>();
			
			classessArray.add("LoneType");
			classessArray.add("PaySlip");
			classessArray.add("LoneEmi");
			classessArray.add("LeaveBalance");
			classessArray.add("LoggedIn");
			
			classessArray.add("Service");
			classessArray.add("Quotation");
			classessArray.add("Approvals");
			classessArray.add("Contract");
			classessArray.add("Config");
					
			classessArray.add("ConfigCategory");
			classessArray.add("Type");
			classessArray.add("Company");
			classessArray.add("Activity");
			classessArray.add("TimeReportConfig");
			
			classessArray.add("Branch");
			classessArray.add("BranchRelation");
			classessArray.add("Employee");
			classessArray.add("EmployeeInfo");
			classessArray.add("EmployeeRelation");
			classessArray.add("User");
			classessArray.add("ItemProduct");
			classessArray.add("SuperProduct");
			classessArray.add("ServiceProduct");
			classessArray.add("Vendor");
			
			classessArray.add("Expense");
			classessArray.add("Lead");
			classessArray.add("Customer");
			classessArray.add("VendorRelation");
			classessArray.add("CustomerRelation");
			classessArray.add("Category");
			classessArray.add("Purchase");
			classessArray.add("Calendar");
			classessArray.add("GLAccount");
			classessArray.add("SalesOrder");
			classessArray.add("EmployeeLeave");
			classessArray.add("Loan");
			
			classessArray.add("Country");
			classessArray.add("Grade");
			classessArray.add("LeaveType");
			classessArray.add("LeaveApplication");
			classessArray.add("Shift");
			classessArray.add("ShiftPattern");
			classessArray.add("Department");
			classessArray.add("UserRole");
			classessArray.add("HrProject");
			classessArray.add("HolidayType");
			classessArray.add("EmployeeGroup");
			classessArray.add("LeaveGroup");
			classessArray.add("CtcComponent");
			classessArray.add("CTC");
			classessArray.add("AssignedShift");
			
			classessArray.add("CompanyAsset");
			classessArray.add("ToolGroup");
			classessArray.add("ServiceProject");
			classessArray.add("ClientSideAsset");
			classessArray.add("EmployeeAttendance");
			classessArray.add("LeaveAllocationProcess");
			classessArray.add("PaySlipAllocationProcess");
			classessArray.add("LiaisonStep");
			classessArray.add("Liaison");
			classessArray.add("LiaisonGroup");
			classessArray.add("SalesQuotation");
			classessArray.add("PriceList");
			classessArray.add("RequsestForQuotation");
			classessArray.add("LetterOfIntent");
			classessArray.add("PettyCash");
			
			classessArray.add("PettyCashDeposits");
			classessArray.add("BillingDocument");
			classessArray.add("Invoice");
			classessArray.add("CustomerPayment");
			classessArray.add("OtherTaxCharges");
			classessArray.add("TaxesAndCharges");
			classessArray.add("DeliveryNote");
			classessArray.add("PurchaseOrder");
			classessArray.add("PurchaseRequisition");
			classessArray.add("GRN");
			classessArray.add("InventoryInHand");
			classessArray.add("Transfer");
			
			classessArray.add("IncomingInventory");
			classessArray.add("LocationRelation");
			classessArray.add("InventoryEntries");
			classessArray.add("WareHouse");
			classessArray.add("OutgoingInventory");
			classessArray.add("InventoryProductDetail");
			classessArray.add("StorageLocation");
			classessArray.add("Storagebin");
			classessArray.add("ProductInventoryView");
			classessArray.add("ProductInventoryViewDetails");
			classessArray.add("CompanyPayment");
			classessArray.add("VendorPayment");
			classessArray.add("TaxDetails");
			
			classessArray.add("State");
			classessArray.add("City");
			classessArray.add("Locality");
			classessArray.add("CheckListStep");
			classessArray.add("CheckListType");
			classessArray.add("ShippingMethod");
			classessArray.add("PackingMethod");
			classessArray.add("EmployeeShift");
			classessArray.add("DateShift");
			classessArray.add("TimeReport");
			classessArray.add("PaySlipRecord");
			classessArray.add("ProductGroupList");
			classessArray.add("ProductGroupDetails");
			
			classessArray.add("BillOfMaterial");
			classessArray.add("NumberGeneration");
			classessArray.add("AccountingInterface");
			classessArray.add("ContactPersonIdentification");
			classessArray.add("PhysicalInventoryMaintaince");
			classessArray.add("RaiseTicket");
			classessArray.add("MaterialRequestNote");
			classessArray.add("MaterialIssueNote");
			classessArray.add("ProductInventoryTransaction");
			classessArray.add("MaterialMovementNote");
			classessArray.add("MaterialMovementType");
			classessArray.add("InteractionType");
			classessArray.add("ProcessConfiguration");
			classessArray.add("CronJob");
			classessArray.add("DocumentHistory");
			classessArray.add("InspectionMethod");
			classessArray.add("CancelSummary");
			classessArray.add("Inspection");
			classessArray.add("BillOfProductMaterial");
			classessArray.add("VendorPriceListDetails");
			classessArray.add("WorkOrder");
			classessArray.add("CustomerBranchDetails");
			classessArray.add("SmsConfiguration");
			classessArray.add("SmsTemplate");
			classessArray.add("IPAddressAuthorization");
			classessArray.add("MultilevelApproval");
			classessArray.add("SmsHistory");
			
			//********************rohan added these entity for number generation
			classessArray.add("Overtime");
			classessArray.add("Fumigation");
			classessArray.add("EmployeeOvertime");
			classessArray.add("CompanyPayrollRecord");
			classessArray.add("EmployeeAdditionalDetails");
			classessArray.add("ContractRenewal");
			classessArray.add("PettyCashTransaction");
			
			return classessArray;
		}
		
		
		private void reactOnCopy()
		{
			saveConfig();
			saveNumberGeneration();
		}
		
		private void deleteConfig()
		{
			MyQuerry querry=new MyQuerry();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setLongValue(model.getCompanyId());
			temp.setQuerryString("companyId");
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setIntValue(62);
			temp.setQuerryString("type");
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Config());
			
			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
						
					}
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for(SuperModel cmodel:result)
						{
							Config centity =(Config)cmodel;
							
							async.delete(model, new AsyncCallback<Void>(){

								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									
								}

								@Override
								public void onSuccess(Void result) {
									
								}
							});
					}
					}
				});
		}
		
		
		private void saveConfig()
		{
			List<String> lis=classesList();
			for(int i=0;i<lis.size();i++)
			{
				ProcessName processEntity=new ProcessName();
				processEntity.setCompanyId(model.getCompanyId());
				processEntity.setProcessName(lis.get(i));
				

				if(lis.get(i).equals("MultilevelApproval"))
				{
					processEntity.setStatus(false);
				}
				else
				{
					processEntity.setStatus(true);
				}
				async.save(processEntity,new AsyncCallback<ReturnFromServer>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
					}

					@Override
					public void onSuccess(ReturnFromServer result) {
						System.out.println("Process Name Saved Successfully!");
					}
				});
			}
	
		}
		
		private void saveNumberGeneration()
		{
			List<String> lis=classesList();
			for(int i=0;i<lis.size();i++)
			{
				NumberGeneration ngEntity=new NumberGeneration();
				ngEntity.setCompanyId(model.getCompanyId());
				ngEntity.setProcessName(lis.get(i));

				if(lis.get(i).trim().equals("ItemProduct")){
					ngEntity.setNumber(100000000);
				}
				else if(lis.get(i).trim().equals("ServiceProduct")){
					ngEntity.setNumber(500000000);
				}
				else if(lis.get(i).trim().equals("Quotation")){
					ngEntity.setNumber(500000000);
				}
				else if(lis.get(i).trim().equals("Contract")){
					ngEntity.setNumber(500000000);
				}
				else{
					ngEntity.setNumber(100000000);
				}
				
				ngEntity.setStatus(true);
				async.save(ngEntity,new AsyncCallback<ReturnFromServer>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
					}

					@Override
					public void onSuccess(ReturnFromServer result) {
						System.out.println("Number Generation Saved Successfully!");
					}
				});
			}
		}



		@Override
		public void onClick(ClickEvent event) {
			super.onClick(event);
			
			if(event.getSource().equals(form.btnCopy)){
				if(form.getSupertable().getDataprovider().getList().size()==0){
					saveConfig();
					saveNumberGeneration();
				}
				else{
					form.showDialogMessage("Classes Already Copied!");
				}
			}
		}
		
		@Override
		protected void makeNewModel() {
			model = new NumberGeneration();
		}

		public void setModel(NumberGeneration entity) {
			model = entity;
		}
		
		
		


}
