package com.slicktechnologies.client;

import java.io.IOException;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("loadservice")
public interface TestMassDataLoadingService extends RemoteService 
{
	public void LoadMassCustomerData() throws IOException;
}
