package com.slicktechnologies.client.userconfigurations;

import java.util.ArrayList;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.role.UserRole;

public class UserConfiguration 
{
	private  long companyId;
	private  User user;
	
	private static UserConfiguration userconfig;
	
	private static EmployeeInfo info;
	private CustomerUser custUser;
	/**
	 * Updated By: Viraj
	 * Date: 21-06-2019
	 * Description: To store companyType 
	 */
	private String compType;
	private boolean hideStatus;			//to store the status of process configuation
	/** Ends **/
	
	private UserConfiguration() {
		super();
		user=new User();
	}
	
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Updated By: Viraj
	 * Date: 21-06-2019
	 * Description: To store company type
	 */
	public String getCompType() {
		if(userconfig!=null)
		    return userconfig.compType;
		return null;
	}

	public void setCompType(String compType) {
		this.compType = compType;
	}
	
	
	public boolean isHideStatus() {
		return hideStatus;
	}

	public void setHideStatus(boolean hideStatus) {
		this.hideStatus = hideStatus;
	}

	/** Ends **/
	public static void initUserConfiguration(User user,Long company)
	{
		System.out.println("Kom"+userconfig);
		
		if(userconfig==null)
		{
			userconfig=new UserConfiguration();
			userconfig.setCompanyId(company);
			/**
			 * Updated By: Viraj
			 * Date: 21-06-2019
			 * Description: To store company type and status of process configuration
			 */
			userconfig.setCompType(user.getCompanyType());
			userconfig.setHideStatus(user.isHrStatus());
			/** Ends **/
			System.out.println("Ajay ");
			userconfig.setUser(user);
			initEmployeeInfo();
		}
	}
	
	
	public static UserConfiguration getUserconfig() {
		return userconfig;
	}
	public static void setUserconfig(UserConfiguration userconfig) {
		UserConfiguration.userconfig = userconfig;
	}
	
	public static Long getCompanyId()
	{
		if(userconfig!=null)
		    return userconfig.companyId;
		return null;
	}

	public static UserRole getRole()
	{
		if(userconfig==null)
			return new UserRole();
		return userconfig.user.getRole();
	}
	
	
	public static void initEmployeeInfo()
	{
		GenricServiceAsync async=GWT.create(GenricService.class);
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("fullName");
		String empName=userconfig.user.getEmployeeName();
		querry.setQuerryObject(new EmployeeInfo());
		filter.setStringValue(empName);
		querry.getFilters().add(filter);
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				info = new EmployeeInfo();
				/*below if condition added by vijay becs here out of bound exceptions rising when employee and employeeinfo changed full name to uppercase
				* Date :- 13 sep 2016
				* Release :- 30 sep 2016
				*/
				if(result.size()!=0)
				{
				info=(EmployeeInfo) result.get(0);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				
			}
		});
	}
	
	public static EmployeeInfo getInfo() {
		return info;
	}
	public static void setInfo(EmployeeInfo info) {
		UserConfiguration.info = info;
	}
	
	
	public static void initCustomerUserConfiguration(CustomerUser user,Long company)
	{
		System.out.println("Kom"+userconfig);
		
		if(userconfig==null)
		{
			userconfig=new UserConfiguration();
			userconfig.setCompanyId(company);
			userconfig.setCustUser(user);
//			initEmployeeInfo();
		}
	}

	public CustomerUser getCustUser() {
		return custUser;
	}

	public void setCustUser(CustomerUser custUser) {
		this.custUser = custUser;
	}
	
	
	
}
