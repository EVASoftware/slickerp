package com.slicktechnologies.client.processname;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;

public class ProcessNamePresenter extends FormTableScreenPresenter<ProcessName> {
	
	ProcessNameForm form;
	
	public ProcessNamePresenter(FormTableScreen<ProcessName> view,
			ProcessName model) 
	{
		super(view, model);
		form=(ProcessNameForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getProcessNameQuery());
		form.setPresenter(this);
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel lbl= (InlineLabel) e.getSource();
		/** date 16/10/2017 if condition added by komal as new button in process name was not working   **/
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
		
   }
	
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		model=new ProcessName();
	}
	
	public void setModel(ProcessName entity)
	{
		model=entity;
	}

	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getProcessNameQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new ProcessName());
		return quer;
	}
	
	public static void initalize()
	{
		ProcessNameTableProxy gentableScreen=new ProcessNameTableProxy();
		ProcessNameForm form=new ProcessNameForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		//OtherChargesPresenterSearch.staticSuperTable=gentableScreen;
		//OtherChargesSearchPopUp searchpopup=new OtherChargesSearchPopUp();
		//form.setSearchpopupscreen(searchpopup);
		ProcessNamePresenter presenter=new ProcessNamePresenter(form,new ProcessName());
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	
		 @EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.numbergenerator.ProcessName")
		 public static class ProcessNamePresenterTable extends SuperTable<ProcessName> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}};
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.numbergenerator.ProcessName")
			 public static class ProcessNamePresenterSearch extends SearchPopUpScreen<ProcessName>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}}

				

}
