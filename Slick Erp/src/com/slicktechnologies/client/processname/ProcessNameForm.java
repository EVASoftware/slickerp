package com.slicktechnologies.client.processname;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ProcessNameForm extends FormTableScreen<ProcessName> {
	
	public TextBox name;
	public CheckBox status;
	protected ProcessNamePresenter presenter;
	
	public ProcessNameForm(SuperTable<ProcessName> table, int mode,boolean captionmode) 
	{
		super(table, mode, captionmode);
		createGui();
	}
	
	private void initializeWidgets()
	{
		name=new TextBox();
		status=new CheckBox();
		status.setValue(true);	
	}
	
	
	@Override
	public void createScreen() 
	{	
		processlevelBarNames=new String[]{"New"};
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingprocessname=fbuilder.setlabel("Process Name Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("* Process Name",name);
		FormField fname=fbuilder.setMandatory(true).setMandatoryMsg("Process Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",status);
		FormField fstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingprocessname},
				{fname,fstatus},
		};


		this.fields=formfield;	
//		status.setValue(true);	
	}
	
	@Override
	public void updateModel(ProcessName model) 
	{
		if(name.getValue()!=null)
			model.setProcessName(name.getValue());
		    model.setStatus(status.getValue());
		
		manageProcessNameDropDown(model);
		

		presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(ProcessName view) 
	{
		System.out.println("status of processs Name"+view.isStatus());
		if(view.getProcessName()!=null)
			name.setValue(view.getProcessName());
		    status.setValue(view.isStatus());

		presenter.setModel(view);
	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.NUMBERGENERATORPROCESSNAME,LoginPresenter.currentModule.trim());
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
	}
	
	public void manageProcessNameDropDown(ProcessName processNameModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalProcessName.add(processNameModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			
			for(int i=0;i<LoginPresenter.globalProcessName.size();i++)
			{
				if(LoginPresenter.globalProcessName.get(i).getId().equals(processNameModel.getId()))
				{
					LoginPresenter.globalProcessName.add(processNameModel);
					LoginPresenter.globalProcessName.remove(i);
				}
			}
		}
	}
	
	public ProcessNamePresenter getPresenter() {
		return presenter;
	}

	public void setPresenter(ProcessNamePresenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void clear() 
	{	
		super.clear();
		status.setValue(true);
	}
	
	
}
