package com.slicktechnologies.client.processname;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.processname.ProcessNamePresenter.ProcessNamePresenterTable;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;

public class ProcessNameTableProxy extends ProcessNamePresenterTable {


	TextColumn<ProcessName> getCountColumn;
	TextColumn<ProcessName> getNameColumn;
	TextColumn<ProcessName> getStatusColumn;
	
	
	public ProcessNameTableProxy()
	{
		super();
	}
	
	@Override public void createTable() {
		addColumngetCount();
		addColumngetName();
		addColumngetStatus();
		
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<ProcessName>()
				{
			@Override
			public Object getKey(ProcessName item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}

	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetName();
		addSortinggetStatus();
	}
	@Override public void addFieldUpdater() {
	}
	
	
	protected void addSortinggetCount()
	{
		List<ProcessName> list=getDataprovider().getList();
		columnSort=new ListHandler<ProcessName>(list);
		columnSort.setComparator(getCountColumn, new Comparator<ProcessName>()
				{
			@Override
			public int compare(ProcessName e1,ProcessName e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<ProcessName>()
				{
			@Override
			public String getValue(ProcessName object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				getCountColumn.setSortable(true);
	}
	
	protected void addSortinggetName()
	{
		List<ProcessName> list=getDataprovider().getList();
		columnSort=new ListHandler<ProcessName>(list);
		columnSort.setComparator(getNameColumn, new Comparator<ProcessName>()
				{
			@Override
			public int compare(ProcessName e1,ProcessName e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProcessName()!=null && e2.getProcessName()!=null){
						return e1.getProcessName().compareTo(e2.getProcessName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetName()
	{
		getNameColumn=new TextColumn<ProcessName>()
				{
			@Override
			public String getValue(ProcessName object)
			{
				return object.getProcessName().trim();
			}
				};
				table.addColumn(getNameColumn,"Process Name");
				getNameColumn.setSortable(true);
	}

	protected void addSortinggetStatus()
	{
		List<ProcessName> list=getDataprovider().getList();
		columnSort=new ListHandler<ProcessName>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<ProcessName>()
				{
			@Override
			public int compare(ProcessName e1,ProcessName e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.isStatus()!=null && e2.isStatus()!=null){
						return e1.isStatus().compareTo(e2.isStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<ProcessName>()
		{
			@Override
			public String getValue(ProcessName object)
			{
				if(object.isStatus()==true){
	              	return "Active";
				}
	             else{
	              	return "In Active";
	             }
			}
		};
		table.addColumn(getStatusColumn,"Status");
		getStatusColumn.setSortable(true);
	}


	
}
