package com.slicktechnologies.client.forgotpassword;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.simplesoftwares.client.library.appstructure.ViewContainer;

public class ForgotPasswordForm extends ViewContainer {

	private static ForgotPasswordFormUiBinder uiBinder = GWT.create(ForgotPasswordFormUiBinder.class);

	interface ForgotPasswordFormUiBinder extends UiBinder<Widget, ForgotPasswordForm> {
	}
	
	@UiField
	public TextBox textBoxUsername, textBoxEmail;
	@UiField
	public Button btnSubmit;
	@UiField
	public Hyperlink loginlink;
	
	ForgotPasswordPresenter presenter;
	public HTMLPanel panel;
	

	public ForgotPasswordForm() {
		super();
		createGui();
		textBoxEmail.setVisible(false);//Ashwini Patil Date:28-04-2023
	}
	
	@Override
	protected void createGui() {
		panel = (HTMLPanel) uiBinder.createAndBindUi(this);
		applyStyle();
		
	}

	@Override
	public void applyStyle() {
		textBoxUsername.getElement().setClassName("input username");
//		textBoxEmail.getElement().setClassName("input password");
		textBoxUsername.getElement().setPropertyString("placeholder","User Name");
//		textBoxEmail.getElement().setPropertyString("placeholder","Email");
		btnSubmit.getElement().setClassName("button");
		loginlink.getElement().setClassName("forgetlink");
	}
	
	public void setPresenter(ForgotPasswordPresenter presenter) {
		this.presenter = presenter;
	}

	
	

	

}
