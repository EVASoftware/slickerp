package com.slicktechnologies.client.forgotpassword;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.simplesoftwares.client.library.libservice.LoginService;
import com.simplesoftwares.client.library.libservice.LoginServiceAsync;
import com.slicktechnologies.client.NameService;
import com.slicktechnologies.client.NameServiceAsync;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.login.LoginForm;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;

public class ForgotPasswordPresenter implements ClickHandler {

	public static ForgotPasswordForm form;
	NameServiceAsync service=GWT.create(NameService.class);
	private static final LoginServiceAsync loginservice=GWT.create(LoginService.class);
	static GWTCGlassPanel glassPanel = new GWTCGlassPanel();
	public String companyName;
	
	ConditionDialogBox conditionPopup = new ConditionDialogBox("Email id is not registered with system! Would you like to receive password on company or branch email Id?", AppConstants.YES,
			AppConstants.NO);
	PopupPanel panel;
	
	public ForgotPasswordPresenter(ForgotPasswordForm form) {
		this.form=form;
		this.form.setPresenter(this);
		this.form.btnSubmit.addClickHandler(this);
		this.form.loginlink.addClickHandler(this);
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
	}

	@Override
	public void onClick(ClickEvent event) {
		
		if(!Slick_Erp.companyname.equals("")){
			companyName=Slick_Erp.companyname;
		}else{
			companyName=Slick_Erp.loginCompanyName;
		}
		System.out.println("Company Name : "+companyName);
		
		if(event.getSource().equals(form.btnSubmit)){
			System.out.println("Submit Button Clicked!!");
			
			String username=form.textBoxUsername.getValue();
			String email=form.textBoxEmail.getValue();
			
			if(username.equals("")&&email.equals("")){
				Window.alert("Please enter user id or email id!");
				return;
			}
			if(username.equals("")){
				username=null;
			}
			
			if(email.equals("")){
				email=null;
			}
			
			loginservice.processForgotPasswordRequest(companyName, username, email, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					
				}

				@Override
				public void onSuccess(String result) {
					if(result.equals("Email id is wrong!")||result.equals("Email id is not registered with system,Please contact admin!")){			
						panel = new PopupPanel(true);
						panel.add(conditionPopup);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();						
					}else {
						Window.alert(result);
						form.textBoxUsername.setValue("");
						form.textBoxEmail.setValue("");
					}
					
				}
			});
			
			
			
		}
		if(event.getSource().equals(form.loginlink)){
			System.out.println("LOGIN LINK CLICKED");
			service.getClientName(companyName, new AsyncCallback<String>() {
				@Override
				public void onSuccess(String result) {
					RootPanel rp=RootPanel.get();
					rp.clear();
					AppUtility.createLogin(result);
				}
				
				@Override
				public void onFailure(Throwable caught) {
				}
			});
		}
		
		
		if(event.getSource() == conditionPopup.getBtnOne()){
			panel.hide();
			String username=form.textBoxUsername.getValue();
			loginservice.sendPasswordResetEmailToCompanyAndBranch(companyName, username, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					Window.alert(result);
					form.textBoxUsername.setValue("");
					form.textBoxEmail.setValue("");
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		if(event.getSource() == conditionPopup.getBtnTwo()){
			panel.hide();
		}
	}
	
	
}
