package com.slicktechnologies.client.views.ipaddressauthorization;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.helperlayer.IPAddressAuthorization;

public class IPAddressAuthorizationTable extends SuperTable<IPAddressAuthorization> {
	

	TextColumn<IPAddressAuthorization> columnID;
	TextColumn<IPAddressAuthorization> columnIpAddress;
	TextColumn<IPAddressAuthorization> columnStatus;
	
	@Override
	public void createTable() {
		addcolumnID();
		addcolumnIpAddress();
		addcolumnStatus();
	}

	private void addcolumnStatus() {
		
		columnStatus=new TextColumn<IPAddressAuthorization>() {
			
			@Override
			public String getValue(IPAddressAuthorization object) {
				if(object.isStatus()==true)
				return "Active";
				else return "Inactive";
			}
		};
		table.addColumn(columnStatus,"Status");
		table.setColumnWidth(columnStatus, 100, Unit.PX);
		columnStatus.setSortable(true);
	}

	private void addcolumnIpAddress() {
		
		columnIpAddress=new TextColumn<IPAddressAuthorization>() {
			
			@Override
			public String getValue(IPAddressAuthorization object) {
				return object.getIpAddress();
			}
		};
		table.addColumn(columnIpAddress,"Name");
		table.setColumnWidth(columnIpAddress, 110, Unit.PX);
		columnIpAddress.setSortable(true);
	}

	private void addcolumnID() {
		
		columnID=new TextColumn<IPAddressAuthorization>() {
			
			@Override
			public String getValue(IPAddressAuthorization object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(columnID,"ID");
		table.setColumnWidth(columnID, 100, Unit.PX);
		columnID.setSortable(true);
		
	}

	public void addColumnSorting() {
		addColumnIDSorting();
		addColumnIpAddressSorting();
		addColumnStatusSorting();
	}

		
	protected void addColumnStatusSorting()
	{
		List<IPAddressAuthorization> list=getDataprovider().getList();
		columnSort=new ListHandler<IPAddressAuthorization>(list);
		columnSort.setComparator(columnStatus, new Comparator<IPAddressAuthorization>()
				{
			@Override
			public int compare(IPAddressAuthorization e1,IPAddressAuthorization e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.isStatus()!=null && e2.isStatus()!=null){
						return e1.isStatus().compareTo(e2.isStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnIpAddressSorting() {
		
		List<IPAddressAuthorization> list=getDataprovider().getList();
		columnSort=new ListHandler<IPAddressAuthorization>(list);
		columnSort.setComparator(columnIpAddress, new Comparator<IPAddressAuthorization>()
				{
			@Override
			public int compare(IPAddressAuthorization e1,IPAddressAuthorization e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getIpAddress()!=null && e2.getIpAddress()!=null){
						return e1.getIpAddress().compareTo(e2.getIpAddress());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	
	}
	
	
	
	private void addColumnIDSorting() {
		
		List<IPAddressAuthorization> list=getDataprovider().getList();
		columnSort=new ListHandler<IPAddressAuthorization>(list);
		columnSort.setComparator(columnID, new Comparator<IPAddressAuthorization>()
				{
			@Override
			public int compare(IPAddressAuthorization e1,IPAddressAuthorization e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	
	}


	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}
	

}
