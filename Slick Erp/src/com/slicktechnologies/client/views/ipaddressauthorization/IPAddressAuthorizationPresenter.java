package com.slicktechnologies.client.views.ipaddressauthorization;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.common.helperlayer.IPAddressAuthorization;

public class IPAddressAuthorizationPresenter extends FormTableScreenPresenter<IPAddressAuthorization> {


	IPAddressAuthorizationForm form;
	public IPAddressAuthorizationPresenter(FormTableScreen<IPAddressAuthorization> view, IPAddressAuthorization model) {
		super(view, model);
		form=(IPAddressAuthorizationForm) view;
		form.retriveTable(getIpAddressQuery());
	}

	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
	}

	@Override
	public void reactOnPrint() {
	}

	@Override
	public void reactOnEmail() {
	}

	@Override
	protected void makeNewModel() {
		model=new IPAddressAuthorization();
		
	}
	public MyQuerry getIpAddressQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new IPAddressAuthorization());
		return quer;
	}
	
	public void setModel(IPAddressAuthorization entity)
	{
		model=entity;
	}
	
	public static IPAddressAuthorizationForm initalize()
	{
		IPAddressAuthorizationTable gentableScreen=new IPAddressAuthorizationTable();
		IPAddressAuthorizationForm  form=new  IPAddressAuthorizationForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
	    gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		IPAddressAuthorizationPresenter  presenter=new  IPAddressAuthorizationPresenter (form,new IPAddressAuthorization());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
}
