package com.slicktechnologies.client.views.ipaddressauthorization;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.IPAddressAuthorization;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class IPAddressAuthorizationForm extends FormTableScreen<IPAddressAuthorization> {
	
	TextBox ipAddressId;
	TextBox ipAddress;
	TextArea description;
	CheckBox status;
	
	IPAddressAuthorization ipAuthObj;
	
	public IPAddressAuthorizationForm(SuperTable<IPAddressAuthorization> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
	}
	
	private void initalizeWidget()
	{
		ipAddressId=new TextBox();
		ipAddressId.setEnabled(false);
		ipAddress= new TextBox();
		status=new CheckBox();
		status.setValue(true);
		description = new TextArea();
	}

	
	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();


		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder("ID",ipAddressId);
		FormField fid= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("IP Address *",ipAddress);
		FormField fipAddress= fbuilder.setMandatory(true).setMandatoryMsg("IP Address is Mandatory!").setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Status",status);
		FormField fstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Description",description);
		FormField fdescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
	

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   
				{fid,fipAddress,fstatus},
				{fdescription},
		};


		this.fields=formfield;		
	}

	
	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(IPAddressAuthorization model) 
	{
		if(ipAddress.getValue()!=null){
			model.setIpAddress(ipAddress.getValue());
		}	
		model.setStatus(status.getValue());
		if(description.getValue()!=null){
			model.setDescription(description.getValue());
		}
		ipAuthObj=model;
		presenter.setModel(model);

	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(IPAddressAuthorization view) 
	{
		ipAuthObj=view;
		if(view.getIpAddress()!=null){
			ipAddress.setValue(view.getIpAddress());
		}
		if(view.getDescription()!= null){
			description.setValue(view.getDescription());
		}
		
		ipAddressId.setValue(view.getCount()+"");
		status.setValue(view.isStatus());
		presenter.setModel(view);
	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.IPADDRESSAUTHORIZATION,LoginPresenter.currentModule.trim());
	}

	

	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new IPAddressAuthorization();
		model=ipAuthObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.IPADDRESSAUTHPRIZATION, ipAuthObj.getCount(), null,null,null, false, model, null);
	
	}
	
	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(ipAuthObj!=null){
			SuperModel model=new IPAddressAuthorization();
			model=ipAuthObj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.IPADDRESSAUTHPRIZATION, ipAuthObj.getCount(), null,null,null, false, model, null);
		
		}
	}
	/******************************************Getters And Setters*************************************/
	
	
	

	public TextBox getIpAddress() {
		return ipAddress;
	}

	public TextBox getIpAddressId() {
		return ipAddressId;
	}

	public void setIpAddressId(TextBox ipAddressId) {
		this.ipAddressId = ipAddressId;
	}

	public void setIpAddress(TextBox ipAddress) {
		this.ipAddress = ipAddress;
	}

	public CheckBox getStatus() {
		return status;
	}

	public void setStatus(CheckBox status) {
		this.status = status;
	}

	public TextArea getDescription() {
		return description;
	}

	public void setDescription(TextArea description) {
		this.description = description;
	}
	
	

}
