package com.slicktechnologies.client.views.multilevelapprovaldetails;

import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApprovalDetails;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;

public class MultilevelApprovalPresenter extends FormTableScreenPresenter<MultilevelApproval> {
	

	MultilevelApprovalForm form;
		
		
	public MultilevelApprovalPresenter(FormTableScreen<MultilevelApproval> view, MultilevelApproval model) {
		super(view, model);
		form=(MultilevelApprovalForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getMultilevelQuery());
		form.setPresenter(this);
		form.btnadd.addClickHandler(this);
	}
		
	public MyQuerry getMultilevelQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new MultilevelApproval());
		return quer;
	}
	
	public static MultilevelApprovalForm initalize() {
	
		MultilevelApprovalTable gentableScreen = new MultilevelApprovalTable();
		MultilevelApprovalForm form = new MultilevelApprovalForm(gentableScreen, FormTableScreen.UPPER_MODE, true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		MultilevelApprovalPresenter presenter = new MultilevelApprovalPresenter(form, new MultilevelApproval());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("New")){
			reactOnNew();
		}	
	}
	
	 private void reactOnNew()
	  {
		  form.setToNewState();
		  this.initalize();
	  }
	
	 
	 @Override
		public void onClick(ClickEvent event) {
		 super.onClick(event);
		 
			if(event.getSource().equals(form.btnadd)){
				List<MultilevelApprovalDetails> processLis=form.table.getDataprovider().getList();
				
				if(form.oblEmployee.getSelectedIndex()==0
						 &&(form.olbBranch.getSelectedIndex()!=0||form.olbRole.getSelectedIndex()!=0)){
					
					 if(form.lbdocname.getSelectedIndex()==0){
						 form.showDialogMessage("Please Select document name !");
						 return;
					 }
					 if(form.olblevel.getSelectedIndex()==0){
						 form.showDialogMessage("Please Select level name !");
						 return;
					 }
					 if(processLis.size()==0){
						 if(form.olblevel.getSelectedIndex()!=0){
							 if(!form.olblevel.getValue().equals("1")){
								form.showDialogMessage("First level should be 1 !");
							 	return;
							 }
						 }
					 }
					 
					 String message="";
					 for (int i = 1; i < form.oblEmployee.getItemCount(); i++) {
						 boolean duplicateEmpFlag=false;
						 if(processLis.size()!=0){
							 for(MultilevelApprovalDetails mulAppDet:processLis){
								 if(mulAppDet.getEmployeeName().equals(form.oblEmployee.getItemText(i))
										 &&mulAppDet.getLevel().equals(form.olblevel.getValue())){
									 message=message+form.oblEmployee.getItemText(i)+" with selected level already exist"+"\n";
									 duplicateEmpFlag=true;
								 }
							 }
						 }
						 
						 MultilevelApprovalDetails pim=new MultilevelApprovalDetails();
						 pim.setDocumentName(form.lbdocname.getValue(form.lbdocname.getSelectedIndex()));
						 pim.setEmployeeName(form.oblEmployee.getItemText(i));
						 if(form.dbAmoountUpto.getValue()!=null){
							pim.setAmountUpto(form.dbAmoountUpto.getValue());
						 }
						 pim.setLevel(form.olblevel.getValue());
						 pim.setRemark(form.taremark.getValue());
						 
						 if(!duplicateEmpFlag){
							 form.table.getDataprovider().getList().add(pim);
						 }
					 }
					 form.taremark.setValue(null); 
					 
					 if(!message.equals("")){
						 form.showDialogMessage(message);
					 }
					 
					 
					 
				 }else{
					if(validateMultiAppEntry()){
						addToMultilevelTable();
					}
				 }
				if(processLis.size()!=0){
					form.lbdocname.setEnabled(false);
				}
			}
		}
	
	 
	 public boolean validateMultiAppEntry(){
		 List<MultilevelApprovalDetails> processLis=form.table.getDataprovider().getList();
		 if(form.lbdocname.getSelectedIndex()==0){
			 form.showDialogMessage("Please Select document name !");
			 return false;
		 }
		 if(form.oblEmployee.getSelectedIndex()==0){
			 form.showDialogMessage("Please Select approver !");
			 return false;
		 }
		 if(form.olblevel.getSelectedIndex()==0){
			 form.showDialogMessage("Please Select level name !");
			 return false;
		 }
		 if(processLis.size()==0){
			 if(form.olblevel.getSelectedIndex()!=0){
				 if(!form.olblevel.getValue().equals("1")){
					form.showDialogMessage("First level should be 1 !");
				 	return false;
				 }
			 }
		 }
		 
		 if(processLis.size()!=0){
			 for(MultilevelApprovalDetails mulAppDet:processLis){
				 if(mulAppDet.getEmployeeName().equals(form.oblEmployee.getValue())&&mulAppDet.getLevel().equals(form.olblevel.getValue())){
					 form.showDialogMessage("Employee with selected level already exist!");
					 return false;
				 }
			 }
		 }
		 
//		 if(form.oblEmployee.getSelectedIndex()==0
//				 &&(form.olbBranch.getSelectedIndex()!=0||form.olbRole.getSelectedIndex()!=0)){
//			 
//		 }
		 
		 
		 return true;
	 }
	 
	private void addToMultilevelTable() {
	
		System.out.println("INSIDE ADD TO TBL");
		MultilevelApprovalDetails pim=new MultilevelApprovalDetails();
		
//		pim.setModuleName(form.oblModuleName.getValue());
		pim.setDocumentName(form.lbdocname.getValue(form.lbdocname.getSelectedIndex()));
//		pim.setEmployeeRole(form.olbRole.getValue());
		pim.setEmployeeName(form.oblEmployee.getValue());
		if(form.dbAmoountUpto.getValue()!=null){
			pim.setAmountUpto(form.dbAmoountUpto.getValue());
		}
		pim.setLevel(form.olblevel.getValue());
		pim.setRemark(form.taremark.getValue());
		
		form.table.getDataprovider().getList().add(pim);
		form.taremark.setValue(null);
	}
	
//		private boolean validateProcess(int level12) {
//	
//		List<MultilevelApprovalDetails> processLis=form.table.getDataprovider().getList();
//		
//		if(processLis.size()!=0){
//			int conditionflag=0;
//			
//			for(int i=0;i<processLis.size();i++)
//			{
//				if(processLis.get(i).getEmployeeRole().trim().equals(form.olbRole.getValue().trim())&&processLis.get(i).getLevel()==form.olblevel.getValue())
//				{
//					form.showDialogMessage("Role and Level already exists!");
//					return false;
//				}
//				
//				if(processLis.get(i).getEmployeeRole().trim().equals(form.olbRole.getValue().trim())&&form.olblevel.getSelectedIndex()!=1){
//					form.showDialogMessage("Employee Role already added!");
//					return false;
//				}
//				
//				if(!processLis.get(i).getEmployeeRole().trim().equals(form.olbRole.getValue().trim())&&processLis.get(i).getLevel()==form.olblevel.getValue()){
//					form.showDialogMessage("Level already added!");
//					return false;
//				}
//				
//				String level="";
//				
//				int level1=processLis.size();
//				int formlevel=Integer.parseInt(form.olblevel.getValue());
//				
//				level1=level1+1;
//				if(formlevel>level1){
//					level1=0;
//					conditionflag=conditionflag+1;
//				}
//			}
//			
//			if(conditionflag!=0){
//				
//				form.showDialogMessage("Level Should be in Sequence 1,2,3..!");
//				conditionflag=0;
//				return false;
//			}
//			
//		}
//		return true;
//	}
	
		@Override
		public void reactOnPrint() {
		}
		
		@Override
		public void reactOnEmail() {
		}
		
		@Override
		protected void makeNewModel() {
			model=new MultilevelApproval();
		}
		
		public void setModel(MultilevelApproval entity) {
			model = entity;
		}
	
	

}
