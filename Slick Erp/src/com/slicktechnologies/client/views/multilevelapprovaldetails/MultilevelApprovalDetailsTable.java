package com.slicktechnologies.client.views.multilevelapprovaldetails;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApprovalDetails;

public class MultilevelApprovalDetailsTable extends SuperTable<MultilevelApprovalDetails> {
	

	TextColumn<MultilevelApprovalDetails> documentNameColumn;
	TextColumn<MultilevelApprovalDetails> roleColumn;
	TextColumn<MultilevelApprovalDetails> levelColumn;
	TextColumn<MultilevelApprovalDetails> remarkColumn;
	Column<MultilevelApprovalDetails,String> deleteColumn;
	
	TextColumn<MultilevelApprovalDetails> getModuleNameCol;
	TextColumn<MultilevelApprovalDetails> getEmpCol;
	TextColumn<MultilevelApprovalDetails> getAmountUptoCol;
	
	
	public MultilevelApprovalDetailsTable() {
		super();
		table.setHeight("400px");
	}
	
	@Override
	public void createTable() {
//		getModuleNameCol();
		createColumnDocumentName();
		getEmpCol();
//		createColumnRole();
		createColumnLevel();
		getAmountUptoCol();
		createColumnRemark();
		
		createColumndeleteColumn();
		addFieldUpdater();
		addColumnSorting();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);	
	}
	
//	private void getModuleNameCol() {
//		getModuleNameCol=new TextColumn<MultilevelApprovalDetails>() {
//			@Override
//			public String getValue(MultilevelApprovalDetails object) {
//				return object.getModuleName();
//			}
//		};
//		table.addColumn(getModuleNameCol,"Module Name");
//		
//	}

	private void getEmpCol() {
		getEmpCol=new TextColumn<MultilevelApprovalDetails>() {
			@Override
			public String getValue(MultilevelApprovalDetails object) {
				return object.getEmployeeName();
			}
		};
		table.addColumn(getEmpCol,"Employee Name");
		getEmpCol.setSortable(true);
	}

	private void getAmountUptoCol() {
		getAmountUptoCol=new TextColumn<MultilevelApprovalDetails>() {
			@Override
			public String getValue(MultilevelApprovalDetails object) {
				if(object.getAmountUpto()!=null){
					return object.getAmountUpto()+"";
				}
				return "";
			}
		};
		table.addColumn(getAmountUptoCol,"Amount Upto");
		getAmountUptoCol.setSortable(true);
	}

	public void addEditColumn(){
//		getModuleNameCol();
		createColumnDocumentName();
		getEmpCol();
		createColumnLevel();
		getAmountUptoCol();
		createColumnRemark();
		createColumndeleteColumn();
		addFieldUpdater();
	}

	public void addViewColumn() {
//		getModuleNameCol();
		createColumnDocumentName();
		getEmpCol();
		createColumnLevel();
		getAmountUptoCol();
		createColumnRemark();
	}
	
	

	private void createColumndeleteColumn() {

		
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<MultilevelApprovalDetails,String>(btnCell)
				{
			@Override
			public String getValue(MultilevelApprovalDetails object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
		
		
	}

	private void createColumnDocumentName() {

		documentNameColumn=new TextColumn<MultilevelApprovalDetails>() {
			@Override
			public String getValue(MultilevelApprovalDetails object) {
				return object.getDocumentName();
			}
		};
		table.addColumn(documentNameColumn,"Document Name");
		
	}

	private void createColumnRole() {

		roleColumn=new TextColumn<MultilevelApprovalDetails>() {
			@Override
			public String getValue(MultilevelApprovalDetails object) {
				return object.getEmployeeRole();
			}
		};
		table.addColumn(roleColumn,"Role");
		table.setColumnWidth(roleColumn,200,Unit.PX);
	}

	private void createColumnLevel() {

		levelColumn=new TextColumn<MultilevelApprovalDetails>() {
			@Override
			public String getValue(MultilevelApprovalDetails object) {
				return object.getLevel()+"";
			}
		};
		table.addColumn(levelColumn,"Level");
		levelColumn.setSortable(true);
		
	}

	private void createColumnRemark() {

		remarkColumn=new TextColumn<MultilevelApprovalDetails>() {
			@Override
			public String getValue(MultilevelApprovalDetails object) {
				return object.getRemark();
			}
		};
		table.addColumn(remarkColumn,"Remark");
		table.setColumnWidth(remarkColumn,250,Unit.PX);
		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {

		createFieldUpdaterdeleteColumn();
	}

	
	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<MultilevelApprovalDetails,String>()
				{
			@Override
			public void update(int index,MultilevelApprovalDetails object,String value)
			{
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
				});
	}

	
	
	
	@Override
	public void setEnable(boolean state) {
		
		System.out.println("state === "+state);
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--){
			table.removeColumn(i);
		}
		if (state == true)
			addEditColumn();
		if (state == false)
			addViewColumn();
		
		
	}

	@Override
	public void applyStyle() {
	}

	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		addSortingOnEmployeeCol();
		addSortingOnLevelCol();
		addSortingOnAmountUptoCol();
	}

	private void addSortingOnEmployeeCol() {
		List<MultilevelApprovalDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<MultilevelApprovalDetails>(list);
		columnSort.setComparator(getEmpCol, new Comparator<MultilevelApprovalDetails>()
				{
			@Override
			public int compare(MultilevelApprovalDetails e1,MultilevelApprovalDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeeName()!=null && e2.getEmployeeName()!=null){
						return e1.getEmployeeName().compareTo(e2.getEmployeeName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingOnLevelCol() {
		List<MultilevelApprovalDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<MultilevelApprovalDetails>(list);
		columnSort.setComparator(levelColumn, new Comparator<MultilevelApprovalDetails>()
				{
			@Override
			public int compare(MultilevelApprovalDetails e1,MultilevelApprovalDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getLevel()!=null && e2.getLevel()!=null){
						return e1.getLevel().compareTo(e2.getLevel());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingOnAmountUptoCol() {
		List<MultilevelApprovalDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<MultilevelApprovalDetails>(list);
		columnSort.setComparator(getAmountUptoCol, new Comparator<MultilevelApprovalDetails>()
				{
			@Override
			public int compare(MultilevelApprovalDetails e1,MultilevelApprovalDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getAmountUpto()!=null && e2.getAmountUpto()!=null){
						return e1.getAmountUpto().compareTo(e2.getAmountUpto());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	

}
