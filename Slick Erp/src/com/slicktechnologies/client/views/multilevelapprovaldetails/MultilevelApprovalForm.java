package com.slicktechnologies.client.views.multilevelapprovaldetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.inventory.BranchDetails;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApprovalDetails;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.UserRole;

public class MultilevelApprovalForm extends FormTableScreen<MultilevelApproval> implements ChangeHandler {
	
	ListBox lbdocname;
	/**
	 * Date :15-02-2017 By Anil
	 * Change widget form text area to text box
	 */
	TextBox taremark;
	/**
	 * End
	 */
	Button btnadd;
	
	/**
	 * Refactor cbStatus to cbMultilevelStatus
	 * Date : 15-02-2017 By Anil
	 */
	CheckBox cbMultilevelStatus;
	/**
	 * End
	 */
	MultilevelApprovalDetailsTable table;
	ObjectListBox<Config> olblevel;
	ObjectListBox<Employee> oblEmployee;
	DoubleBox dbAmoountUpto;
	
	/**
	 * Date : 15-02-2017 By ANil
	 */
	ObjectListBox<Branch>olbBranch;
	ObjectListBox<Config> olbRole;
	CheckBox cbDocStatus;
	/**
	 * End
	 */
	
	MultilevelApproval multilevelApprovalObj;

	public MultilevelApprovalForm  (SuperTable<MultilevelApproval> table1, int mode,boolean captionmode) {
		super(table1, mode, captionmode);
		createGui();
		cbMultilevelStatus.setValue(false);
		cbDocStatus.setValue(true);
		table1.connectToLocal();
	}

	

	private void initalizeWidget(){
		lbdocname=new ListBox();
		lbdocname.addItem("--SELECT--");
		lbdocname.addItem("Quotation");
		lbdocname.addItem("Contract");
		lbdocname.addItem("Expense Management");
		lbdocname.addItem("Complaint");
		lbdocname.addItem("Leave Application");
		lbdocname.addItem("Advance");
		lbdocname.addItem("Letter Of Intent");
		lbdocname.addItem("Billing Details");
		lbdocname.addItem("Invoice Details");
		lbdocname.addItem("Request For Quotation");
		lbdocname.addItem("Sales Quotation");
		lbdocname.addItem("Purchase Order");
		lbdocname.addItem("Purchase Requisition");
		lbdocname.addItem("Sales Order");
		lbdocname.addItem("GRN");
		lbdocname.addItem("MRN");
		lbdocname.addItem("MIN");
		lbdocname.addItem("MMN");
		lbdocname.addItem("Delivery Note");
		lbdocname.addItem("Inspection");
		lbdocname.addItem("Work Order");
		lbdocname.addItem("Service PO");
		lbdocname.addItem("Customer");
		lbdocname.addItem("Physical Inventory");
		/**Date 19-3-2019 added by Amol**/
		lbdocname.addItem("Employee");
		lbdocname.addItem("CNC");

		olblevel=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olblevel, Screen.APPROVALLEVEL);
//		olblevel.setSelectedIndex(1);
		
		cbMultilevelStatus=new CheckBox();
		cbMultilevelStatus.setValue(false);
		
		taremark=new TextBox();
		btnadd=new Button("Add");
		table=new MultilevelApprovalDetailsTable();
		table.connectToLocal();
		/**
		 * Date :24-10-2017 BY ANIL
		 */
		table.addColumnSorting();
		/**
		 * End
		 */
		
		oblEmployee = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(oblEmployee);
		dbAmoountUpto=new DoubleBox();
		
		olbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		olbBranch.addChangeHandler(this);
		
		olbRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbRole,Screen.EMPLOYEEROLE);
		olbRole.addChangeHandler(this);
		
		cbDocStatus=new CheckBox();
		cbDocStatus.setValue(true);
	}
	
	
	
	@Override
	public void createScreen() {
		
		processlevelBarNames=new String[]{"New"};
		initalizeWidget();
	
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCustomerInformation=fbuilder.setlabel("Document Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Level",olblevel);
		FormField levelInformation= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Multilevel Approval",cbMultilevelStatus);
		FormField checkboxInformation= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Remark",taremark);
		FormField remarkInformation= fbuilder.setRowSpan(0).setColSpan(1).build();
		fbuilder = new FormFieldBuilder("", btnadd);
		FormField fadd = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingCustomerInformation1=fbuilder.setlabel("Multilevel Approval Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("", table.getTable());
		FormField ftable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Documnet Name",lbdocname);
		FormField foblDocumentName= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Approver",oblEmployee);
		FormField foblEmployee= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Amount Upto",dbAmoountUpto);
		FormField fdbAmoountUpto= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Employee Role",olbRole);
		FormField folbRole= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status",cbDocStatus);
		FormField fcbDocStatus= fbuilder.setRowSpan(0).setColSpan(0).build();

		FormField[][] formfield = {
				{fgroupingCustomerInformation},
				{foblDocumentName,checkboxInformation ,fcbDocStatus},
				{folbBranch,folbRole,foblEmployee},
				{levelInformation,fdbAmoountUpto,remarkInformation,fadd},
//				{remarkInformation},
				{fgroupingCustomerInformation1 }, 
				{ftable} 
				};

		this.fields = formfield;
			
	}
	
	
	@Override
	public void updateModel(MultilevelApproval model) 
	{
		List<MultilevelApprovalDetails> prodlist=this.table.getDataprovider().getList();
		ArrayList<MultilevelApprovalDetails> prodlistArr=new ArrayList<MultilevelApprovalDetails>();
		prodlistArr.addAll(prodlist);
		model.setApprovalLevelDetails(prodlistArr);

		if(lbdocname.getSelectedIndex()!=0){
			model.setDocumentType(lbdocname.getValue(lbdocname.getSelectedIndex()));
		}
		
		model.setStatus(cbMultilevelStatus.getValue());
		model.setDocStatus(cbDocStatus.getValue());
		manageMultilevelApproval(model);
		
		multilevelApprovalObj=model;
		
		presenter.setModel(model);

	}
	
	@Override
	public void updateView(MultilevelApproval view) 
	{
		
		multilevelApprovalObj=view;
		
		if(view.getDocumentType()!=null){
			for(int i=1;i<lbdocname.getItemCount();i++){
				if(lbdocname.getItemText(i).trim().equals(view.getDocumentType().trim())){
					lbdocname.setSelectedIndex(i);
					break;
				}
			}
		}
		cbMultilevelStatus.setValue(view.isStatus());
		cbDocStatus.setValue(view.isDocStatus());
		table.setValue(view.getApprovalLevelDetails());
		table.addColumnSorting();
		
		presenter.setModel(view);
	}


	@Override
	public void clear() {
		super.clear();
		table.clear();
	}

	@Override
	public boolean validate() {
		 boolean superValidate = super.validate();
		 
		 if(superValidate==false){
        	 return false;
        }
        return true;
	}

	private boolean validatetablelist() {

		List<MultilevelApprovalDetails> multiLevelTableLis=this.table.getDataprovider().getList();
		
		if(multiLevelTableLis.size()==0){
			
			return false;
		}
		
		return true;
	}

	private boolean validateMultilevel() {

		boolean flagApproval=validateMultiLevelApproval();
		
		if(flagApproval==true)
		{
			List<MultilevelApprovalDetails> multiLevelTableLis=this.table.getDataprovider().getList();
			int level=0;
			int ctr=0;
			for(int i=0;i<multiLevelTableLis.size();i++){
				level=Integer.parseInt(multiLevelTableLis.get(i).getLevel());
			}
			
		if(multiLevelTableLis.size()<level){
			ctr=1;
		}
		
		if(ctr==1){
			return false;
		}
		}
		return true;
	}

	private boolean validateMultiLevelApproval() {
			ArrayList<ProcessName> processNme=LoginPresenter.globalProcessName;
			boolean flagApproval=false;
			for(int b=0;b<processNme.size();b++)
			{
				if(processNme.get(b).getProcessName().trim().equals(AppConstants.GLOBALRETRIEVALMULTILEVELAPPROVAL)&&processNme.get(b).isStatus()==true)
				{
					flagApproval=true;
				}
			}
			return flagApproval;
	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.MULTILEVELAPPROVAL,LoginPresenter.currentModule.trim());
	}

	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		table.setEnable(state);
		table.addColumnSorting();
		
	}
	
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		this.lbdocname.setEnabled(false);
		
	}
	

	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new MultilevelApproval();
		model=multilevelApprovalObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.MULTILEVELAPPROVAL, multilevelApprovalObj.getCount(), null,null,null, false, model, null);
	
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		cbMultilevelStatus.setValue(false);
		cbDocStatus.setValue(true);
		processLevelBar.setVisibleFalse(true);
		if(multilevelApprovalObj!=null){
			SuperModel model=new MultilevelApproval();
			model=multilevelApprovalObj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.MULTILEVELAPPROVAL, multilevelApprovalObj.getCount(), null,null,null, false, model, null);
		}
	}
	
	public void manageMultilevelApproval(MultilevelApproval multiApprovalModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalMultilevelApproval.add(multiApprovalModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalMultilevelApproval.size();i++)
			{
				if(LoginPresenter.globalMultilevelApproval.get(i).getId().equals(multiApprovalModel.getId()))
				{
					LoginPresenter.globalMultilevelApproval.add(multiApprovalModel);
					LoginPresenter.globalMultilevelApproval.remove(i);
				}
			}
		}
	}



	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(olbBranch)){
			if(olbBranch.getSelectedIndex()!=0){
				if(olbRole.getSelectedIndex()!=0){
					setApproverLisDropDown(olbBranch.getValue().trim(), olbRole.getValue().trim());
				}else{
					setApproverLisDropDown(olbBranch.getValue().trim(), null);
				}
			}else{
				if(olbRole.getSelectedIndex()!=0){
					setApproverLisDropDown(null, olbRole.getValue().trim());
				}else{
					setApproverLisDropDown(null, null);
				}
			}
		}
		
		if(event.getSource().equals(olbRole)){
			if(olbRole.getSelectedIndex()!=0){
				if(olbBranch.getSelectedIndex()!=0){
					setApproverLisDropDown(olbBranch.getValue().trim(), olbRole.getValue().trim());
				}else{
					setApproverLisDropDown(null, olbRole.getValue().trim());
				}
			}else{
				if(olbBranch.getSelectedIndex()!=0){
					setApproverLisDropDown(olbBranch.getValue().trim(), null);
				}else{
					setApproverLisDropDown(null, null);
				}
			}
		}
	}

	private void setApproverLisDropDown(String branch,String employeeRole){
		System.out.println(branch+" "+employeeRole);
		ArrayList<Employee> empList=new ArrayList<Employee>();
		
		if(LoginPresenter.globalEmployee.size()!=0){
			if(branch!=null&&employeeRole==null){
				
				for(Employee emp:LoginPresenter.globalEmployee){
					boolean branchFlag=false;
					if(emp.getBranchName().trim().equals(branch)){
						branchFlag=true;
						empList.add(emp);
					}
					if(!branchFlag){
						for(EmployeeBranch empBranch:emp.getEmpBranchList()){
							if(empBranch.getBranchName().trim().equals(branch)){
								empList.add(emp);
							}
						}
					}
				}
				System.out.println("1- "+empList.size());
				oblEmployee.setListItems(empList);
			}else if(branch==null&&employeeRole!=null){
				for(Employee emp:LoginPresenter.globalEmployee){
					if(emp.getRoleName().trim().equals(employeeRole)){
						empList.add(emp);
					}
				}
				System.out.println("2- "+empList.size());
				oblEmployee.setListItems(empList);
			}
			else if(branch!=null&&employeeRole!=null){
				
				for(Employee emp:LoginPresenter.globalEmployee){
					boolean branchFlag=false;
					if(emp.getBranchName().trim().equals(branch)
							&&emp.getRoleName().trim().equals(employeeRole)){
						branchFlag=true;
						empList.add(emp);
					}
					if(!branchFlag){
						for(EmployeeBranch empBranch:emp.getEmpBranchList()){
							if(empBranch.getBranchName().trim().equals(branch)
									&&emp.getRoleName().trim().equals(employeeRole)){
								empList.add(emp);
							}
						}
					}
				}
				System.out.println("3- "+empList.size());
				oblEmployee.setListItems(empList);
				
			}else{
				for(Employee emp:LoginPresenter.globalEmployee){
					if(emp.isStatus()==true){
						empList.add(emp);
					}
				}
				System.out.println("4- "+empList.size());
				oblEmployee.setListItems(empList);
			}
		}
		
		
	}
	
	
	

}
