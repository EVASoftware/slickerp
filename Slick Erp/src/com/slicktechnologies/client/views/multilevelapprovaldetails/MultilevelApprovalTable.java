package com.slicktechnologies.client.views.multilevelapprovaldetails;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;

public class MultilevelApprovalTable extends SuperTable<MultilevelApproval> {
	
	TextColumn<MultilevelApproval> countColumn;
	TextColumn<MultilevelApproval> documentNameColumn;
	TextColumn<MultilevelApproval> statusColumn;
	
	TextColumn<MultilevelApproval> docStatusColumn;

	
	public MultilevelApprovalTable()
	{
		super();
	}
	
	@Override
	public void createTable() {
		createColumnCount();
		createColumnDocumentName();
		createColumnStatus();
		createColumnDocStatus();
	}

	
	private void createColumnDocStatus() {
		docStatusColumn = new TextColumn<MultilevelApproval>() {
			@Override
			public String getValue(MultilevelApproval object) {
				if (object.isDocStatus() == true) {
					return "Active";
				} else {
					return "In Active";
				}
			}
		};
		table.addColumn(docStatusColumn, "Status");
		docStatusColumn.setSortable(true);
		
	}

	private void createColumnCount() {

		countColumn=new TextColumn<MultilevelApproval>() {
			@Override
			public String getValue(MultilevelApproval object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(countColumn,"ID");
		countColumn.setSortable(true);
		
	}
	private void createColumnStatus() {
		
		
		statusColumn=new TextColumn<MultilevelApproval>()
		{
			@Override
			public String getValue(MultilevelApproval object)
			{
				if(object.isStatus()==true){
	              	return "Active";
				}
	             else{
	              	return "In Active";
	             }
			}
		};
		table.addColumn(statusColumn,"Multilevel");
		statusColumn.setSortable(true);
		
		
	}
	private void createColumnDocumentName() {

		documentNameColumn=new TextColumn<MultilevelApproval>() {
			@Override
			public String getValue(MultilevelApproval object) {
				return object.getDocumentType();
			}
		};
		table.addColumn(documentNameColumn,"Document Name");
		documentNameColumn.setSortable(true);
	}
	
		public void addColumnSorting(){
		
			addSortinggetCount();
			addSortinggetDocument();
			addSortinggetStatus();
			addSortinggetDocStatus();
		}
		
		
		private void addSortinggetCount() {
			
			List<MultilevelApproval> list=getDataprovider().getList();
			columnSort=new ListHandler<MultilevelApproval>(list);
			columnSort.setComparator(countColumn, new Comparator<MultilevelApproval>()
					{
				@Override
				public int compare(MultilevelApproval e1,MultilevelApproval e2)
				{
					if(e1!=null && e2!=null)
					{
						if(e1.getCount()== e2.getCount()){
							return 0;}
						if(e1.getCount()> e2.getCount()){
							return 1;}
						else{
							return -1;}
					}
					else{
						return 0;}
				}
					});
			table.addColumnSortHandler(columnSort);
		}
		
		private void addSortinggetStatus() {

			List<MultilevelApproval> list=getDataprovider().getList();
			columnSort=new ListHandler<MultilevelApproval>(list);
			columnSort.setComparator(statusColumn, new Comparator<MultilevelApproval>()
					{
				@Override
				public int compare(MultilevelApproval e1,MultilevelApproval e2)
				{
					if(e1!=null && e2!=null)
					{
						if(e1.isStatus()!=null&& e2.isStatus()!=null){
							return e1.isStatus().compareTo(e2.isStatus());
							}
					}
					else{
						return 0;}
					return 0;
				}
					});
			table.addColumnSortHandler(columnSort);
		}
		private void addSortinggetDocStatus() {
			List<MultilevelApproval> list = getDataprovider().getList();
			columnSort = new ListHandler<MultilevelApproval>(list);
			columnSort.setComparator(docStatusColumn,new Comparator<MultilevelApproval>() {
				@Override
				public int compare(MultilevelApproval e1,MultilevelApproval e2) {
					if (e1 != null && e2 != null) {
						if (e1.isDocStatus() == e2.isDocStatus()) {
							return 0;
						} else {
							return -1;
						}
					} else {
						return 0;
					}
				}
			});
			table.addColumnSortHandler(columnSort);
		}
		
		private void addSortinggetDocument() {


			List<MultilevelApproval> list=getDataprovider().getList();
			columnSort=new ListHandler<MultilevelApproval>(list);
			columnSort.setComparator(documentNameColumn, new Comparator<MultilevelApproval>()
					{
				@Override
				public int compare(MultilevelApproval e1,MultilevelApproval e2)
				{
					if(e1!=null && e2!=null)
					{
						if( e1.getDocumentType()!=null && e2.getDocumentType()!=null){
							return e1.getDocumentType().compareTo(e2.getDocumentType());}
					}
					else{
						return 0;}
					return 0;
				}
					});
			table.addColumnSortHandler(columnSort);
			
		}
		
		@Override
		protected void initializekeyprovider() {
		}
		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub
			
		}
	

}
