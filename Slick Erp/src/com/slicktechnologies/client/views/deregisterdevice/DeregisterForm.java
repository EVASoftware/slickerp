package com.slicktechnologies.client.views.deregisterdevice;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.AutoFitWidthApproach;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.util.DateUtil;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class DeregisterForm extends ViewContainer{


	ListGrid grid;
	DataSource ds;
	
	ObjectListBox<String> olbApplicationName;
	DateBox dbFromDate;
	DateBox dbToDate;
	Button btnGo;
	
	FlexForm form;
	ObjectListBox<String> olbStatus;
	HorizontalPanel horizontalpanel;
	VerticalPanel verticalpanel;
	
	public DeregisterForm() {
		super();
		createGui();
	}
	
	@Override
	protected void createGui() {
		System.out.println("CREATE GUI");
		toggleAppHeaderBarMenu();
		
		olbApplicationName =new ObjectListBox<String>();
		AppUtility.setStatusListBox(olbApplicationName, RegisterDevice.getStatusList());
		
		olbStatus =new ObjectListBox<String>();
		olbStatus.addItem("--SELECT--");
		olbStatus.addItem("ACTIVE");
		olbStatus.addItem("INACTIVE");
		
		olbApplicationName.getElement().getStyle().setHeight(26, Unit.PX);
		olbStatus.getElement().getStyle().setHeight(26, Unit.PX);
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		dbFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		dbToDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		btnGo=new Button("GO");
		
		
		horizontalpanel = new HorizontalPanel();
		
		
		VerticalPanel vpanel1=new VerticalPanel();
		VerticalPanel vpanel2=new VerticalPanel();
		VerticalPanel vpanel3=new VerticalPanel();
		VerticalPanel vpanel4=new VerticalPanel();
		VerticalPanel vpanel5=new VerticalPanel();
		
		InlineLabel lablel1=new InlineLabel("Application Name");
		InlineLabel lablel2=new InlineLabel("Status");
		InlineLabel lablel3=new InlineLabel("From Date(Registration)");
		InlineLabel lablel4=new InlineLabel("To Date(Registration");
		InlineLabel lablel5=new InlineLabel(" . ");
		
		vpanel1.add(lablel1);
		vpanel1.add(olbApplicationName);
		
		vpanel2.add(lablel2);
		vpanel2.add(olbStatus);
		
		vpanel3.add(lablel3);
		vpanel3.add(dbFromDate);
		
		vpanel4.add(lablel4);
		vpanel4.add(dbToDate);
		
		vpanel5.add(lablel5);
		vpanel5.add(btnGo);
		
		horizontalpanel.add(vpanel1);
		horizontalpanel.add(vpanel2);
		//horizontalpanel.add(vpanel3);
		//horizontalpanel.add(vpanel4);
		horizontalpanel.add(vpanel5);
		horizontalpanel.setSpacing(10);
		horizontalpanel.getElement().addClassName("technicianscheduleButtons");
		
		
		verticalpanel = new VerticalPanel();
				
		
		grid = new ListGrid();
		grid.setStyleName("GridList");
		setGridProperty(grid);
		grid.setFields(getGridFields());
		
		ds= new DataSource();
		ds.setClientOnly(true);
		setDataSourceField(ds);
		grid.setDataSource(ds);
		
		verticalpanel.getElement().addClassName("varticalGridPanel");
		grid.getElement().addClassName("technicianSchedule");
		verticalpanel.add(grid);
		
		content.add(horizontalpanel);
		content.add(verticalpanel);
		
		
	}
	
	public void setDataToListGrid(ArrayList<RegisterDevice> list){
		System.out.println("FORM SETTING DATA....");
		
		grid.setData(new ListGridRecord[] {});
		ds= new DataSource();
		ds.setClientOnly(true);
		setDataSourceField(ds);
		
		ds.setTestData(getGridData(list));
		grid.setDataSource(ds);
		grid.setFields(getGridFields());
		grid.fetchData();
		
	}

	
	private void setGridProperty(ListGrid grid) {
	   grid.setWidth("83%");
	   grid.setHeight("70%");
	   grid.setAutoFetchData(true);
	   grid.setAlternateRecordStyles(true);
	   grid.setShowAllRecords(true);
	   grid.setShowFilterEditor(true);
	   grid.setFilterOnKeypress(true);
	   
	   grid.setWrapCells(true);	
	   grid.setFixedRecordHeights(false);
	   grid.setAutoFitData(Autofit.HORIZONTAL);	
	   grid.setAutoFitWidthApproach(AutoFitWidthApproach.BOTH);	
	   grid.setCanResizeFields(true);
	}
	
	private ListGridField[] getGridFields() {
		
	  	   
	   ListGridField field1 = new ListGridField("deregisterButton","Deregister Device");
	   field1.setWrap(true);
	   field1.setAlign(Alignment.CENTER);
	   field1.setType(ListGridFieldType.IMAGE);
	   field1.setDefaultValue("deregister.png");
	   field1.setCanEdit(false);
	   field1.setImageSize(80);
	   field1.setImageHeight(30);
	   
	   ListGridField field2= new ListGridField("applicationName","Application Name");
	   field2.setWrap(true);
	   field2.setAlign(Alignment.CENTER);
	   
	   ListGridField field3 = new ListGridField("registratonDate","Registration Date");
	   field3.setWrap(true);
	   field3.setAlign(Alignment.CENTER);
	   
	   field3.setCellFormatter(new CellFormatter() {
			@Override
			public String format(Object value, ListGridRecord record, int rowNum,int colNum) {
				 if (value instanceof Date) {
		                try {
		                    return AppUtility.parseDate((Date) value);
		                } catch (Exception e) {
		                    return value.toString();
		                }
		            } else {
		                return value == null ? null : value.toString();
		            }
			}
	   });
	   
	   
	   
	   
	   
	   ListGridField field4 = new ListGridField("employeeName","User");
	   field4.setWrap(true);
	   field4.setAlign(Alignment.CENTER);
	   
	   ListGridField field5 = new ListGridField("imeiNumber","IMEI Number");
	   field5.setWrap(true);
	   field5.setAlign(Alignment.CENTER);
	   
	   ListGridField field6 = new ListGridField("status","Status");
	   field6.setWrap(true);
	   field6.setAlign(Alignment.CENTER);
	   
	     
	   
	   return new ListGridField[] { 
			   field1, field2 ,field3,field4,field5,field6
			   };
	}
	
	//data source field
	protected void setDataSourceField(DataSource dataSource) {
		DataSourceIntegerField pkField = new DataSourceIntegerField("pk");  
        pkField.setHidden(true);  
        pkField.setPrimaryKey(true);
		
	   DataSourceField field1 = new DataSourceField("deregisterButton", FieldType.IMAGE);
	   DataSourceField field2 = new DataSourceField("applicationName", FieldType.TEXT);
	   DataSourceDateField field3 = new DataSourceDateField("registratonDate");
	   DataSourceField field4 = new DataSourceField("employeeName", FieldType.TEXT);
	   DataSourceField field5 = new DataSourceField("imeiNumber", FieldType.TEXT);
	   DataSourceField field6 = new DataSourceField("status", FieldType.TEXT);
	   DataSourceField field7 = new DataSourceField("id", FieldType.TEXT); 
	   dataSource.setFields(pkField,field1, field2 ,field3,field4,field5,field6,field7);
	}
	
	
	 private ListGridRecord[] getGridData(ArrayList<RegisterDevice> deviceList) {
		 
		 ListGridRecord[] records = null; 
		 records = new ListGridRecord[deviceList.size()];
		 for(int i=0;i<deviceList.size();i++){
			 ListGridRecord record = new ListGridRecord();
			 RegisterDevice registerDevice=deviceList.get(i);
			 
			 record.setAttribute("applicationName", registerDevice.getApplicationName());
			 record.setAttribute("deregisterButton", "deregister.png");
			 if(registerDevice.getCreationDate()!=null){
				 record.setAttribute("registratonDate", registerDevice.getCreationDate());
			 }else{
				 record.setAttribute("registratonDate", "");
			 }
			
			 record.setAttribute("employeeName", registerDevice.getEmployeeName());
			 record.setAttribute("imeiNumber", registerDevice.getImeiNumber());
			 String status = "";
			 if(registerDevice.isStatus()){
				 status = "ACTIVE";
			 }else{
				 status = "INACTIVE";
			 }
			 
			 record.setAttribute("status",status);
			 record.setAttribute("id", registerDevice.getCount()+"");
			 records[i]=record;
		 }
		 System.out.println("RECORDS LENGTH : "+records.length);
		 
		 return records;
	}
	 
	 /**
		 * Toggles the app header bar menus as per screen state
		 */
		public void toggleAppHeaderBarMenu() {
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);    		  		
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  	   
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.APPREGISTRATIONHISTORY,LoginPresenter.currentModule.trim());
		}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	public void showMessage(String msg){
		Window alert=new Window();
		
		alert.setWidth("25%");
        alert.setHeight("20%"); 
        alert.setTitle("Alert");
        alert.setShowMinimizeButton(false);
        alert.setIsModal(true);
        alert.setShowModalMask(true);
        alert.centerInPage();
        alert.setMargin(4);
        
//		InlineLabel msgLbl=new InlineLabel(msg);
//		alert.addItem(msgLbl);
		
//		window.setWidth(200);
//		window.setHeight(200);
//		window.setCanDragResize(true);
//		window.setShowFooter(true);

		Label label = new Label();
		label.setContents(msg);
		label.setAlign(Alignment.CENTER);
		label.setPadding(5);
		label.setHeight100();
		
		alert.addItem(label);
		
		
		alert.show();
	}
	
	
	

	public ListGrid getGrid() {
		return grid;
	}

	public void setGrid(ListGrid grid) {
		this.grid = grid;
	}

	public DataSource getDs() {
		return ds;
	}

	public void setDs(DataSource ds) {
		this.ds = ds;
	}

	
	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}

	public Button getBtnGo() {
		return btnGo;
	}

	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}

	public FlexForm getForm() {
		return form;
	}

	public void setForm(FlexForm form) {
		this.form = form;
	}

	public HorizontalPanel getHorizontalpanel() {
		return horizontalpanel;
	}

	public void setHorizontalpanel(HorizontalPanel horizontalpanel) {
		this.horizontalpanel = horizontalpanel;
	}

	public VerticalPanel getVerticalpanel() {
		return verticalpanel;
	}

	public void setVerticalpanel(VerticalPanel verticalpanel) {
		this.verticalpanel = verticalpanel;
	}

	public ObjectListBox<String> getOlbApplicationName() {
		return olbApplicationName;
	}

	public void setOlbApplicationName(ObjectListBox<String> olbApplicationName) {
		this.olbApplicationName = olbApplicationName;
	}

	public ObjectListBox<String> getOlbStatus() {
		return olbStatus;
	}

	public void setOlbStatus(ObjectListBox<String> olbStatus) {
		this.olbStatus = olbStatus;
	}
	
	
	
	
	

}
