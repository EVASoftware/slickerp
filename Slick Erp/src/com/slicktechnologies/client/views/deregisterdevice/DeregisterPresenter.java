package com.slicktechnologies.client.views.deregisterdevice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;

public class DeregisterPresenter implements ClickHandler, CellClickHandler {

	DeregisterForm form;
	GenricServiceAsync service=GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	ArrayList<RegisterDevice> globalRegisterDeviceList=new ArrayList<RegisterDevice>();

	RegisterDevice registerDevice=null;
	ListGridRecord selectedRecord=null;
	int position;
	
	protected AppMemory mem;
		
	public DeregisterPresenter(DeregisterForm homeForm) {
		this.form=homeForm;
		
		form.grid.addCellClickHandler(this);
		mem=AppMemory.getAppMemory();
		setEventHandeling();
		//form.getBtnGo().addClickHandler(this);
		retrieveData();
		
		
	}
	
	private void setEventHandeling() {
		form.getBtnGo().addClickHandler(this);
			
		InlineLabel label[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
		for (int k = 0; k < label.length; k++) {
			if (AppMemory.getAppMemory().skeleton.registration[k] != null)
				AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
		}
		for (int k = 0; k < label.length; k++) {
			AppMemory.getAppMemory().skeleton.registration[k] = label[k].addClickHandler(this);
		}
	}

	public static void initialize(){
		System.out.println("INSIDE CHART INITIALIZATION");
		DeregisterForm homeForm = new DeregisterForm();
		AppMemory.getAppMemory().currentScreen=Screen.HOME;
		AppMemory.getAppMemory().skeleton.getProcessName().setText(AppConstants.SETTINGSMODULE+"/"+"App Registration History");
		DeregisterPresenter presenter= new DeregisterPresenter(homeForm);
		
		AppMemory.getAppMemory().stickPnel(homeForm);	
	}
	
	
	
	public void retriveTable(MyQuerry querry) {
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				Console.log("Result length : "+result.size());
				globalRegisterDeviceList = new ArrayList<RegisterDevice>();

				if (result.size() == 0) {
					SC.say("No result found.");
				}
				for (SuperModel model : result) {
					RegisterDevice rDevice = (RegisterDevice) model;
					globalRegisterDeviceList.add(rDevice);
				}
				Console.log("Register Device length : "+globalRegisterDeviceList.size());
				form.setDataToListGrid(globalRegisterDeviceList);
			}

			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		});
	}
	

	@Override
	public void onClick(ClickEvent event) {
		System.out.println("CLICK EVENT..!!!");
		if (event.getSource().equals(form.getBtnGo())) {
			searchByFromToDate();
		}
		if(event.getSource() instanceof InlineLabel){
			InlineLabel lbl= (InlineLabel) event.getSource();
			if(lbl.getText().equals("Download")){
				reactOnDownload();
			}
			
		}
			
	}
	
	private void searchByFromToDate() {
		Long compId = UserConfiguration.getCompanyId();
		Vector<Filter> filtervec = null;
		Filter filter = null;
		MyQuerry querry;

		filtervec = new Vector<Filter>();

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		if(form.getOlbApplicationName().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("applicationName");
			filter.setStringValue(form.getOlbApplicationName().getValue(form.getOlbApplicationName().getSelectedIndex()));
			filtervec.add(filter);
		} 
		if(form.getOlbStatus().getSelectedIndex() != 0){
			boolean status = false;
			if(form.getOlbStatus().getValue(form.getOlbStatus().getSelectedIndex()).equalsIgnoreCase("ACTIVE")){
				status = true;
			}
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(status);
			filtervec.add(filter);
		}
		
		querry = new MyQuerry();
		querry.setQuerryObject(new RegisterDevice());
		querry.setFilters(filtervec);
		Console.log("QUERRY :- " + querry.toString());
		this.retriveTable(querry);

	}
	
	

	@Override
	public void onCellClick(CellClickEvent event) {
		
		ListGridRecord record = event.getRecord();
		int colNum = event.getColNum();
		ListGridField field = form.grid.getField(colNum);
		String fieldName = form.grid.getFieldName(colNum);
		String fieldTitle = field.getTitle();
		position=event.getRowNum();
		System.out.println("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Lead Id :: "+ record.getAttribute("id")+"POSITION "+position);
		registerDevice = getRegisterDeviceDetails((Integer.parseInt(record.getAttribute("id").trim())));
		if (fieldName.equals("deregisterButton")) {
			selectedRecord = record;
			deregisterDevice(registerDevice);
		}
	}	
	private RegisterDevice getRegisterDeviceDetails(int id) {
		for(RegisterDevice object:globalRegisterDeviceList){
			if(object.getCount()==id){
				return object;
			}
		}
		return null;
	}
	private void deregisterDevice(final RegisterDevice entity){
		entity.setStatus(false);
		entity.setDeactivatedBy(LoginPresenter.loggedInUser);
		entity.setDeactivationDate(new Date());
		
		service.save(entity, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				// TODO Auto-generated method stub
				form.showMessage("Device deregistered successfully.");
				/***Date 28-2-2020 by Amol to refresh the Grid when Device is Deregistered**/
				if(globalRegisterDeviceList!=null && globalRegisterDeviceList.size()!=0) {
					Console.log("inside deregister device ");
					for(RegisterDevice regDevice : globalRegisterDeviceList) {
						if(regDevice.getCount() == entity.getCount()) {
							globalRegisterDeviceList.remove(regDevice);
						}
					}
					form.setDataToListGrid(globalRegisterDeviceList);
				}
			}
			
		});
	}

	private void retrieveData() {

		System.out.println("FROM AND DATE ---");
		Long compId = UserConfiguration.getCompanyId();
		Vector<Filter> filtervec = null;
		Filter filter = null;
		MyQuerry querry;

		filtervec = new Vector<Filter>();

		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);

		querry = new MyQuerry();
		querry.setQuerryObject(new RegisterDevice());
		querry.setFilters(filtervec);
		this.retriveTable(querry);

	}
	private void reactOnDownload() {
		ArrayList<RegisterDevice> registerDeviceList = new ArrayList<RegisterDevice>();
		ListGridRecord[] allRecords = form.grid.getRecords();
        if (allRecords != null) {
        	List<ListGridRecord> allRecordsList = Arrays.asList(allRecords);
	   		for(int i=0;i<allRecordsList.size();i++){
	   			 final RegisterDevice model=getRegisterDeviceDetails(Integer.parseInt(allRecordsList.get(i).getAttribute("id").trim()));
	   			 if(model!=null){
	   				registerDeviceList.add(model);
	   			 }
	   		}
        }
		
		csvservice.setRegisterDeviceList(registerDeviceList, new AsyncCallback<Void>(){
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			
			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+180;
				Window.open(url, "test", "enabled");
			}
		});
	}
}
