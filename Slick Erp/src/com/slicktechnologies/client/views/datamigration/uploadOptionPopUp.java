package com.slicktechnologies.client.views.datamigration;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class uploadOptionPopUp extends PopupScreen{
	
	
	public CheckBox serviceCreate;
	//Button custUploadOkBtn,custUploadCancleBtn;
	public CheckBox billCreate;
	
	public uploadOptionPopUp(){
		super();
		createGui();
		
	}
	
	
	
	private void initilizewidget() {
		
		serviceCreate = new CheckBox("Create Service");
		billCreate = new CheckBox("Create Billing Document");
		
		
//		custUploadOkBtn = new Button("Ok");
//		custUploadCancleBtn = new Button("Cancle");
//		custUploadOkBtn.addClickHandler(this);
//		custUploadCancleBtn.addClickHandler(this);
		
	}
	
	@Override
	public void createScreen() {
		initilizewidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingExpenseDetails=fbuilder.setlabel("Upload Option").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("",serviceCreate);
		FormField fdbserviceCreate= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",billCreate);
		FormField fdbbillCreate= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = {
				{fgroupingExpenseDetails},
				{fdbserviceCreate,fdbbillCreate}
		};


		this.fields=formfield;		

	}

	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}


}
