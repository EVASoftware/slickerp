
package com.slicktechnologies.client.views.datamigration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.CustomerNameChangeService;
import com.slicktechnologies.client.services.CustomerNameChangeServiceAsync;
import com.slicktechnologies.client.services.DataMigrationService;
import com.slicktechnologies.client.services.DataMigrationServiceAsync;
import com.slicktechnologies.client.services.DataMigrationTaskQueueService;
import com.slicktechnologies.client.services.DataMigrationTaskQueueServiceAsync;
import com.slicktechnologies.client.services.DocumentUploadService;
import com.slicktechnologies.client.services.DocumentUploadServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.complain.ServiceDateBoxPopup;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.client.views.popups.ContractDownloadPopUp;
import com.slicktechnologies.client.views.popups.CustomerInfoPopup;
import com.slicktechnologies.client.views.popups.DatePopPup;
import com.slicktechnologies.client.views.popups.InvoiceDatePopup;
import com.slicktechnologies.client.views.popups.ReportPopup;
import com.slicktechnologies.client.views.popups.SerailNumberRangePopup;
import com.slicktechnologies.server.DataMigrationImpl;
import com.slicktechnologies.shared.ContractUploadDetails;
import com.slicktechnologies.shared.CreditNote;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.MINUploadExcelDetails;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.StackMaster;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.servicerelated.AssetMovementInfo;

public class DataMigrationPresentor extends
		FormScreenPresenter<DummyEntityOnlyForScreen> {

	/*
	 * i have create an entity ie DummyEntityOnlyForScreen for only screen (ie
	 * do not save any data in it.)
	 */

	 DataMigrationServiceAsync datamigAsync = GWT
			.create(DataMigrationService.class);

	 DocumentUploadServiceAsync documentuploadAsync = GWT.create(DocumentUploadService.class);
	 
	/**
	 * date 26/10/2017 added by komal to update followup date in lead and
	 * quotation
	 **/
	final GeneralServiceAsync genasync = GWT.create(GeneralService.class);
	DataMigrationForm form;
	/**
	 * nidhi 25-09-2017
	 * 
	 * @param view
	 * @param model
	 */
	uploadOptionPopUp CustomerUploadOpt;
	
	/**
	 * Date : 27-07-2018 BY ANIL
	 */
	ServiceDateBoxPopup empIdPopup=new ServiceDateBoxPopup("Text", null);
	PopupPanel panel=new PopupPanel();
	
	/**
	 * Date : 24-12-2018 by ANIL
	 */
	DataMigrationTaskQueueServiceAsync dataTaskAsync = GWT.create(DataMigrationTaskQueueService.class);
    CustomerNameChangeServiceAsync custAsync= GWT.create(CustomerNameChangeService.class);
	/**
	 * end
	 * 
	 * @param view
	 * @param model
	 */
	GeneralServiceAsync generalAsync = GWT.create(GeneralService.class);
	final CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	/** date 10.4.2019 added by komal to update services within duration **/
	FromAndToDateBoxPopup fromAndToDateBoxPopup = new FromAndToDateBoxPopup(new PersonInfoComposite());
	
	FromAndToDateBoxPopup fromandToDatePopUp= new FromAndToDateBoxPopup();
	
	/**Date 29-11-2019 by Amol added this popup for contract updation**/
	FromAndToDateBoxPopup fromAndTodate=new FromAndToDateBoxPopup("Header");
	
	DateTimeFormat format=DateTimeFormat.getFormat("dd/MM/yyyy");
	DatePopPup datepopup = new DatePopPup();
		CustomerNameChangeServiceAsync customernamechangeasync = GWT.create(CustomerNameChangeService.class);
	SerailNumberRangePopup sernumber = new SerailNumberRangePopup();
	
	ContractDownloadPopUp contractDownload = new ContractDownloadPopUp();
	ServiceDateBoxPopup updateFrequencyPopup=new ServiceDateBoxPopup("Update Frequency");//Ashwini Patil Date:10-07-2023
	
	InvoiceDatePopup invoicedatepopup = new InvoiceDatePopup();
	
	InvoiceDatePopup paymentdatepopup = new InvoiceDatePopup(true);
	
	CustomerInfoPopup customerinfopopup = new CustomerInfoPopup();
	
	DocumentUploadServiceAsync documentupload = GWT.create(DocumentUploadService.class);

	final UpdateServiceAsync updateservice = GWT.create(UpdateService.class);
	
	public DataMigrationPresentor(FormScreen<DummyEntityOnlyForScreen> view,
			DummyEntityOnlyForScreen model) {
		super(view, model);
		form = (DataMigrationForm) view;

		form.setPresenter(this);
		form.btnMasterGo.addClickHandler(this);
		form.btnConfigurationGo.addClickHandler(this);
		form.btnTransactionGo.addClickHandler(this);

		/**
		 * nidhi
		 * 
		 */
		CustomerUploadOpt = new uploadOptionPopUp();
		CustomerUploadOpt.getLblOk().addClickHandler(this);
		CustomerUploadOpt.getLblCancel().addClickHandler(this);
		/**
		 * end
		 */
		
		empIdPopup.getBtnOne().addClickHandler(this);
		empIdPopup.getBtnTwo().addClickHandler(this);
		/** date 10.4.2019 added by komal to update services within duration **/
		fromAndToDateBoxPopup.getBtnOne().addClickHandler(this);
		fromAndToDateBoxPopup.getBtnTwo().addClickHandler(this);
		
		fromandToDatePopUp.getBtnOne().addClickHandler(this);
		fromandToDatePopUp.getBtnTwo().addClickHandler(this);
		datepopup.getLblOk().addClickHandler(this);
		datepopup.getLblCancel().addClickHandler(this);
		
		fromAndTodate.getBtnOne().addClickHandler(this);
		fromAndTodate.getBtnTwo().addClickHandler(this);
		
		contractDownload.getLblOk().addClickHandler(this);
		contractDownload.getLblCancel().addClickHandler(this);
		
		updateFrequencyPopup.getBtnOne().addClickHandler(this);
		updateFrequencyPopup.getBtnTwo().addClickHandler(this);
	}

	public static void initialize() {
		AppMemory.getAppMemory().skeleton.isMenuVisble=false;
		AppMemory.getAppMemory().currentState = ScreeenState.NEW;

		DataMigrationForm form = new DataMigrationForm();

		DataMigrationPresentor presenter = new DataMigrationPresentor(form,
				new DummyEntityOnlyForScreen());
		AppMemory.getAppMemory().stickPnel(form);

	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		super.onClick(event);
		
		if(event.getSource()==empIdPopup.getBtnOne()){
			if(empIdPopup.getAction()!=null&&empIdPopup.getIbIntegerBox().getValue()!=null){
				updateEmployee(empIdPopup.getAction(), empIdPopup.getIbIntegerBox().getValue(),empIdPopup.getPayRollPeriod(),empIdPopup.getProject().getValue(),empIdPopup.getLbBranch().getValue());
				panel.hide();
			}else{
				form.showDialogMessage("Please enter employee id.");
			}
		}
		
		if(event.getSource()==empIdPopup.getBtnTwo()){
			panel.hide();
		}

		if (event.getSource() == form.btnMasterGo) {
			System.out.println("Hi Gm");

			Company c = new Company();

			if (form.mastersUploadComposite.getValue() == null) {
				form.showDialogMessage("Please upload a file");
			} else {

				final String name = form.lstboxMasters.getValue(form.lstboxMasters.getSelectedIndex());
				
				/**
				 * Date : 24-12-2018 BY ANIL
				 * 
				 */
				if(name.equalsIgnoreCase("Update Leave Balance")){
					updateLeaveBalance();
					return;
				}else if(name.equalsIgnoreCase("Customer Branch")){
					saveCustomerBranch();
					return;
				}
				/**@author Sheetal : 02-06-2022 
				 * Des : Customer branch with service location , area Upload**/
				else if(name.equalsIgnoreCase("Customer branch with service location and area Upload")){
					saveCustomerBranchWithServiceLocation();
					return;
				}
				/**
				 * @author Vijay Chougule Date :- 16-07-2020
				 * Des :- Vendor Product Price Upload
				 */
				else if(name.equalsIgnoreCase("Vendor Product Price")){
					reactonVendorProductPrice();
					return;
				}
				
				/**
				 * @author Vijay Chougule Date :- 29-07-2020
				 * Des :- CTC Template Upload
				 */
				else if(name.equalsIgnoreCase("CTC Template")){
					reactonCTCTemplate();
					return;
				}
				
				/**
				 * @author Vijay Chougule Date :- 12-08-2021
				 * Des :- Stock Upload
				 */
				else if(name.equalsIgnoreCase("Stock Upload")){
					reactonStockUpload();
					return;
				}

				form.txtfilecount.setValue(null);
				form.txtsavedrecords.setValue(null);

				datamigAsync.savedRecordsDeatils(c.getCompanyId(), name, true,LoginPresenter.loggedInUser,
						new AsyncCallback<ArrayList<Integer>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("Failed");

							}

							@Override
							public void onSuccess(ArrayList<Integer> result) {

								if(name.equalsIgnoreCase("Update Employee")){
									if(result.contains(-1)){
										String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
										final String url = gwt + "csvservlet" + "?type=" + 186;
										Window.open(url, "test", "enabled");
										form.showDialogMessage("Please find error file!");
									}
									else if(result.contains(-2)) {
											form.showDialogMessage("Excel sheet header not matched Please get latest Excel sheet to upload! ");
									}
									else if(result.contains(0)){
										Console.log("Employee Update Upload Process Started");
										form.showDialogMessage("Employee Update Process Started");
									}
									else{
										Console.log("Failed"+result);
									}
									return;
								}
								if (name.equalsIgnoreCase("City")) {
									if (result.contains(-1)) {
										Console.log("inside update employee on success error occurs");
										String gwt = com.google.gwt.core.client.GWT
												.getModuleBaseURL();
										final String url = gwt + "csvservlet"
												+ "?type=" + 191;
										Window.open(url, "test", "enabled");
										return;
									} else {
										Console.log("City update successfully");
										form.showDialogMessage("City update successfully");
									}
								}
								if (name.equalsIgnoreCase("Locality")) {
									if (result.contains(-1)) {
										String gwt = com.google.gwt.core.client.GWT
												.getModuleBaseURL();
										final String url = gwt + "csvservlet"
												+ "?type=" + 192;
										Window.open(url, "test", "enabled");
										return;
									} else {
										Console.log("Locality update successfully");
										form.showDialogMessage("Locality update successfully");
									}
								}
								if (name.equalsIgnoreCase("Employee")) {
									if (result.contains(-1)) {
										String gwt = com.google.gwt.core.client.GWT
												.getModuleBaseURL();
										final String url = gwt + "csvservlet"
												+ "?type=" + 195;
										Window.open(url, "test", "enabled");
										form.showDialogMessage("Please find error file!");
									} else if(result.contains(-2)) {
										form.showDialogMessage("Excel sheet header not matched Please get latest Excel sheet to upload! ");
									}
									else if(result.contains(-3)) {
										form.showDialogMessage("You are trying to upload a blank file!");
									}
									else if(result.contains(0)){
										Console.log("Employee Upload Process Started");
										form.showDialogMessage("Employee Update Process Started");
									}
									else{
										Console.log("Failed"+result);
									}
									return;
								}
								if (name.equalsIgnoreCase("Service Product")) {
									if (result.contains(-1)) {
										String gwt = com.google.gwt.core.client.GWT
												.getModuleBaseURL();
										final String url = gwt + "csvservlet"
												+ "?type=" + 196;
										Window.open(url, "test", "enabled");
										form.showDialogMessage("Please find error file!");
									} else if(result.contains(-2)) {
										form.showDialogMessage("Excel sheet header not matched Please get latest Excel sheet to upload! ");
									}
									else if(result.contains(0)){
										Console.log("Service Product Upload Process Started");
										form.showDialogMessage("Service Product Upload Process Started");
									}
									else{
										Console.log("Failed"+result);
									}
										
									return;
								}
								if (name.equalsIgnoreCase("Item Product")) {
									if (result.contains(-1)) {
										String gwt = com.google.gwt.core.client.GWT
												.getModuleBaseURL();
										final String url = gwt + "csvservlet"
												+ "?type=" + 197;
										Window.open(url, "test", "enabled");
										form.showDialogMessage("Please find error file!");
									} else if(result.contains(-2)) {
										form.showDialogMessage("Excel sheet header not matched Please get latest Excel sheet to upload! ");
									}
									else if(result.contains(0)){
										Console.log("Item Product Upload Process Started");
										form.showDialogMessage("Item Product Upload Process Started");
									}
									else{
										Console.log("Failed"+result);
									}
										
									return;
								}
								
								Console.log("result from impl="+result);
								if (result.contains(-1)) {
									form.showDialogMessage("Invalid Excel File");
								} else if (result.contains(-2)) {
									form.showDialogMessage("Error: Branch Name Already Exist!!");
								} else if (result.contains(-3)) {
									form.showDialogMessage("Error: City Name Already Exist!!");
								} else if (result.contains(-4)) {
									form.showDialogMessage("Error: Country Name Already Exist!!");
								} else if (result.contains(-5)) {
									form.showDialogMessage("Error: Customer Already Exist!!");
								} else if (result.contains(-6)) {
									form.showDialogMessage("Error: Department And Parent Department Already Exist!!");
								} else if (result.contains(-7)) {
									form.showDialogMessage("Error: Employee Name Already Exist!!");
								} else if (result.contains(-8)) {
									form.showDialogMessage("Error: Product Code Already Exist!!");
								} else if (result.contains(-9)) {
									form.showDialogMessage("Error: Locality Name Already Exist!!");
								} else if (result.contains(-10)) {
									form.showDialogMessage("Error: Procode Code Already Exist!!");
								} else if (result.contains(-11)) {
									form.showDialogMessage("Error: State Name Already Exist!!");
								} else if (result.contains(-12)) {
									form.showDialogMessage("Error: Storage Bin Already Exist!!");
								} else if (result.contains(-13)) {
									form.showDialogMessage("Error: Storage Location Already Exist!!");
								} else if (result.contains(-14)) {
									form.showDialogMessage("Error: Vendor Name Already Exist!!");
								} else if (result.contains(-15)) {
									System.out
											.println("Error: WareHouse Already Exist!!");
									form.showDialogMessage("Error: WareHouse Already Exist!!");
								} else if (result.contains(-16)) {
									form.showDialogMessage("Error: Asset Name Already Exists!!");
								} else if (result.contains(-17)) {
									form.showDialogMessage("Error: Please set date format to date column!!");
								} else if (result.contains(-18)) {
									form.showDialogMessage("Error: Please upload .xls format file!!");
								} else if (result.contains(-19)) {
									form.showDialogMessage("Error: Stock is already exist!!");
								} else if (result.contains(-20)) {
									form.showDialogMessage("Error: Contract/customer id does not exist!!");
								} else if (result.contains(-21)) {
									form.showDialogMessage("Error: No contract found for updation!!");
								} else if(result.contains(-22)){
									form.showDialogMessage("Error: Customer branch is already exist!!");
								} else if(result.contains(-23)){	
									form.showDialogMessage("Error: Customer does not exist!!");
							    } else if(result.contains(-24)){	
									form.showDialogMessage("Error: Please enter values in all Mandatory Fields !!");
							    }
							    else if(result.contains(-25)){	
									form.showDialogMessage("Error: Servicing Branch does not exist or does not match Please check !!");
							    }
								else if(result.contains(-26)){	
									form.showDialogMessage("Error: City does not exist or does not match Please check !!");
							    }
								else if(result.contains(-27)){	
									form.showDialogMessage("Error: Duplicate employee name found in the excel please check !!");
							    }
								else if(result.contains(-28)){	
									form.showDialogMessage("Error: Some characters found Landline no. column in the excel please check !!");
							    }
								else if(result.contains(-29)){	
									form.showDialogMessage("Error: Some characters found Cell no. column in the excel please check !!");
							    }
								else if(result.contains(-30)){	
									form.showDialogMessage("Error: Some characters found Fax no. column in the excel please check !!");
							    }
								else if(result.contains(-31)){	
									form.showDialogMessage("Error: Check numeric column. Number is too long.");
							    }
								else if(result.contains(-32)){	
									form.showDialogMessage("Error: GSTIN number must be 15 characters!.");
							    }
							    else {
									form.txtfilecount.setValue(result.get(0)
											+ "");
									form.txtsavedrecords.setValue(result.get(1)
											+ "");
									System.out.println("Successfully");

									if (name.equalsIgnoreCase("Update Employee")||name.equalsIgnoreCase("Locality")||name.equalsIgnoreCase("City")) {
									
									} else {
										/**
										 * @author Anil @since 27-04-2021
										 */
//										form.showDialogMessage("Sucessfully");
										form.showDialogMessage("Successful");
										
									}

								}

							}

						});
			}

		}

		if (event.getSource() == form.btnConfigurationGo) {

			System.out.println("Sucessfully Clicked on Configurations Go.:)");

			Company c = new Company();

			if (form.configurationsUploadComposite.getValue() == null) {
				System.out.println("In if of config: "
						+ form.configurationsUploadComposite.getValue());
				form.showDialogMessage("Please upload file");
			} else {
				System.out.println("In else of config:  "
						+ form.configurationsUploadComposite.getValue());
				final String name = form.lstboxConfigurations
						.getValue(form.lstboxConfigurations.getSelectedIndex());
				System.out.println("name: "
						+ form.lstboxConfigurations
								.getValue(form.lstboxConfigurations
										.getSelectedIndex()));
				form.txtfilecount.setValue(null);
				form.txtsavedrecords.setValue(null);

				datamigAsync.savedConfigDetails(c.getCompanyId(), name,
						new AsyncCallback<ArrayList<Integer>>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								form.showDialogMessage("Failed");
							}

							@Override
							public void onSuccess(ArrayList<Integer> result) {
								// TODO Auto-generated method stub

								if(name.equalsIgnoreCase("Category")){
									if(result.contains(-1)){
										Console.log("inside Category on success error occurs");
										String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
										final String url = gwt + "csvservlet" + "?type=" + 188;
										Window.open(url, "test", "enabled");
										return;
									}
									else{
										Console.log("Category upload successfully");
										form.showDialogMessage("Category upload successfully.");
									}
								}
								
								if(name.equalsIgnoreCase("Type")){
									if(result.contains(-1)){
										Console.log("inside Category on success error occurs");
										String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
										final String url = gwt + "csvservlet" + "?type=" + 189;
										Window.open(url, "test", "enabled");
										return;
									}
									else{
										Console.log("Type upload successfully");
										form.showDialogMessage("Type upload successfully.");
									}
								}
								
								
								
								if (result.contains(-1)) {
									form.showDialogMessage("Invalid Excel File");
								} else {
									System.out
											.println("Inside Else of onSuccess");
									form.txtfilecount.setValue(result.get(0)
											+ "");
									form.txtsavedrecords.setValue(result.get(1)
											+ "");
									if(name.equalsIgnoreCase("Category")||name.equalsIgnoreCase("Type")){
										
									}else{
										form.showDialogMessage("Successful");
									}
									
									  /**Date :29/03/2018 By:Manisha
		                                * Description:after uploading configuration master need not to logout the system and start again						Company c=new Company();
		                                */
		                                
									Company c=new Company();
									String categoryType=AppConstants.GLOBALRETRIEVALCATEGORY+"-"+c.getCompanyId();
									LoginPresenter.globalMakeLiveCategory(categoryType);
									
									String configType=AppConstants.GLOBALRETRIEVALCONFIG+"-"+c.getCompanyId();
									LoginPresenter.globalMakeLiveConfig(configType);
							//		creatingLoopTimer();
									
							//		creatingLoopTimer();
									String typeType=AppConstants.GLOBALRETRIEVALTYPE+"-"+c.getCompanyId();
									LoginPresenter.globalMakeLiveType(typeType);
									/**End**/
								}
							}
						});

			}

		}

		// ****************code for trans on 15/12/2015*******************88

		if (event.getSource() == form.btnTransactionGo) {

			System.out.println("Here You Go for Transaction");
			final Company c = new Company();

			if (form.transactionUploadComposite.getValue() == null) {
				form.showDialogMessage("Please upload file");
			} else {
				String name = form.lstboxTransaction.getValue(form.lstboxTransaction.getSelectedIndex());
				System.out.println("GEt herer... "+ name.equals("Customer With Service Details"));

				if (name.equals("Employee Asset Allocation")) {
					form.showWaitSymbol();
					datamigAsync = GWT.create(DataMigrationService.class);
					datamigAsync.saveEmployeeAsset(UserConfiguration.getCompanyId(),new AsyncCallback<ArrayList<EmployeeAsset>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(
							ArrayList<EmployeeAsset> result) {
						// TODO Auto-generated method stub
						if (result == null) {
							form.hideWaitSymbol();
							form.showDialogMessage("Invalid file.");
							return;
						}

						datamigAsync.validateEmployeeAsset(result,new AsyncCallback<ArrayList<EmployeeAsset>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(ArrayList<EmployeeAsset> result) {

							for (EmployeeAsset obj : result) {
								if (obj.getCount() == -1) {
									form.hideWaitSymbol();
									String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url = gwt+ "csvservlet"+ "?type="+ 173;
									Window.open(url,"test","enabled");
									form.showDialogMessage("Please find error file.");
									return;
								}
							}
							
							datamigAsync.uploadEmployeeAsset(result,new AsyncCallback<ArrayList<EmployeeAsset>>() {

								@Override
								public void onFailure(Throwable caught) {
									form.hideWaitSymbol();
								}

								@Override
								public void onSuccess(ArrayList<EmployeeAsset> result) {
									form.hideWaitSymbol();
									form.showDialogMessage("Employee asset upload process started.");
									String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url = gwt+ "csvservlet"+ "?type="+ 173;
									Window.open(url,"test","enabled");
								}
							});
						}
					});
					}
				});

				} else if (name.equals("Customer With Service Details")) {
					CustomerUploadOpt.showPopUp();
					CustomerUploadOpt.serviceCreate.setValue(false);
					CustomerUploadOpt.billCreate.setValue(false);
					// CustomerUploadOpt.center();
					System.out.println("GEt herer... ");
					
				} else if (name.equals(AppConstants.MINUPLOAD)) {
					/**
					 * nidhi 27-10-2017 min upload list option selection process
					 **/
					form.showWaitSymbol();
					name = form.lstboxTransaction.getValue(form.lstboxTransaction.getSelectedIndex());
					System.out.println("name: "+ form.lstboxTransaction.getValue(form.lstboxTransaction.getSelectedIndex()));
					form.txtfilecount.setValue(null);
					form.txtsavedrecords.setValue(null);
					String loginPerson = LoginPresenter.loggedInUser;

					datamigAsync = GWT.create(DataMigrationService.class);

					datamigAsync.savedUploadTransactionDetails(c.getCompanyId(), name, false, false, loginPerson,new AsyncCallback<MINUploadExcelDetails>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Failed");
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(MINUploadExcelDetails resultOne) {
						// TODO Auto-generated method stub
						// form.showDialogMessage(resultOne.getExceldatalist().get(10));
						if (resultOne.getErrorList().size() > 0) {
							form.showDialogMessage("Invalid Excel File");
							csvservice.setMinUploadError(resultOne.getErrorList(),new AsyncCallback<Void>() {
								@Override
								public void onFailure(Throwable caught) {
									System.out.println("RPC call Failed"+ caught);
									form.hideWaitSymbol();
								}

								@Override
								public void onSuccess(Void result) {
									String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url = gwt+ "csvservlet"+ "?type="+ 130;
									Window.open(url,"test","enabled");
									form.hideWaitSymbol();
								}
							});
						} else {
							MINUploadExcelDetails mindtTwo = new MINUploadExcelDetails();
							mindtTwo = resultOne;
							datamigAsync.saveMinUploadProcess(mindtTwo,new AsyncCallback<MINUploadExcelDetails>() {

							@Override
							public void onSuccess(MINUploadExcelDetails resultTwo) {
								if (resultTwo.getErrorList().size() > 0) {
									form.showDialogMessage("Invalid Excel File");
									csvservice.setMinUploadError(resultTwo.getErrorList(),new AsyncCallback<Void>() {

										@Override
										public void onFailure(Throwable caught) {
											System.out.println("RPC call Failed"+ caught);
											form.hideWaitSymbol();
										}

										@Override
										public void onSuccess(Void result) {
											String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
											final String url = gwt+ "csvservlet"+ "?type="+ 130;
											Window.open(url,"test","enabled");
											form.hideWaitSymbol();
										}
									});
								} else {

									MINUploadExcelDetails mindtThree = new MINUploadExcelDetails();
									mindtThree = resultTwo;
									datamigAsync.checkMinUploadServiceListDetails(mindtThree,new AsyncCallback<MINUploadExcelDetails>() {
	
									@Override
									public void onSuccess(MINUploadExcelDetails resultThree) {
										if (resultThree.getErrorList().size() > 0) {

											form.showDialogMessage("Invalid Excel File");
											csvservice.setMinUploadError(resultThree.getErrorList(),new AsyncCallback<Void>() {

												@Override
												public void onFailure(Throwable caught) {
													System.out.println("RPC call Failed"+ caught);
													form.hideWaitSymbol();
												}

												@Override
												public void onSuccess(Void result) {
													String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
													final String url = gwt+ "csvservlet"+ "?type="+ 130;
													Window.open(url,"test","enabled");
													form.hideWaitSymbol();
												}
											});

										} else {
											MINUploadExcelDetails mindtFour = new MINUploadExcelDetails();
											mindtFour = resultThree;
											datamigAsync.updateProductInventoryViewList(mindtFour,new AsyncCallback<MINUploadExcelDetails>() {

											@Override
											public void onSuccess(MINUploadExcelDetails resultFour) {

												if (resultFour.getErrorList().size() > 0) {

													form.showDialogMessage("Invalid Excel File");
													csvservice.setMinUploadError(resultFour.getErrorList(),new AsyncCallback<Void>() {

														@Override
														public void onFailure(Throwable caught) {
															System.out.println("RPC call Failed"+ caught);
															form.hideWaitSymbol();
														}

														@Override
														public void onSuccess(Void result) {
															String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
															final String url = gwt+ "csvservlet"+ "?type="+ 130;
															Window.open(url,"test","enabled");
															form.hideWaitSymbol();
														}
													});

												} else {
													MINUploadExcelDetails mindtFive = new MINUploadExcelDetails();
													mindtFive = resultFour;
													datamigAsync.ServiceUploadToCompleteStatus(mindtFive,new AsyncCallback<MINUploadExcelDetails>() {

													@Override
													public void onSuccess(MINUploadExcelDetails resultFive) {
														ArrayList<String> error = resultFive.getErrorList();

														if (error.contains("1")) {
															form.hideWaitSymbol();
															form.showDialogMessage("Data Upload Process  Start..!!");
														}
														if (!error.contains("1")&& error.size() > 0) {
															csvservice.setMinUploadError(error,new AsyncCallback<Void>() {

																@Override
																public void onFailure(Throwable caught) {
																	System.out.println("RPC call Failed"+ caught);
																	form.hideWaitSymbol();
																}

																@Override
																public void onSuccess(Void result) {
																	form.hideWaitSymbol();
																	String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
																	final String url = gwt+ "csvservlet"+ "?type="+ 130;
																	Window.open(url,"test","enabled");
																}
															});
														}

													}

													@Override
													public void onFailure(Throwable caught) {
														System.out.println("RPC call Failed"+ caught);
														form.hideWaitSymbol();
													}
												});
												}
											}

											@Override
											public void onFailure(Throwable caught) {
												System.out.println("RPC call Failed"+ caught);
												form.hideWaitSymbol();
											}
										});
										}
									}

									@Override
									public void onFailure(Throwable caught) {
										System.out.println("RPC call Failed"+ caught);
										form.hideWaitSymbol();
										form.showDialogMessage("Error while Uploaded excel details");
									}
								});
								}

							}

							@Override
							public void onFailure(Throwable caught) {
								System.out.println("RPC call Failed"+ caught);
								form.hideWaitSymbol();
							}
						});
						}
					}
				});
					
				}/**
				 * end
				 **/
				/**
				 * nidhi 2-11-2017 for nbhc contract Upload process
				 */
				else if (name.equals(AppConstants.CONTRACTUPLOAD)) {/*
																	 * 
																	 * name =
																	 * form.
																	 * lstboxTransaction
																	 * .
																	 * getValue(
																	 * form.
																	 * lstboxTransaction
																	 * .
																	 * getSelectedIndex
																	 * ());
																	 * form.
																	 * showWaitSymbol
																	 * ();
																	 * System
																	 * .out
																	 * .println
																	 * ("name: "
																	 * + form.
																	 * lstboxTransaction
																	 * .
																	 * getValue(
																	 * form.
																	 * lstboxTransaction
																	 * .
																	 * getSelectedIndex
																	 * ()));
																	 * form
																	 * .txtfilecount
																	 * .
																	 * setValue(
																	 * null); //
																	 * form.
																	 * showDialogMessage
																	 * (c.
																	 * getCompanyId
																	 * ()+"");
																	 * form.
																	 * txtsavedrecords
																	 * .
																	 * setValue(
																	 * null);
																	 * String
																	 * loginPerson
																	 * =
																	 * LoginPresenter
																	 * .
																	 * loggedInUser
																	 * ;
																	 * datamigAsync
																	 * .
																	 * savedContractTransactionDetails
																	 * (c.
																	 * getCompanyId
																	 * (),
																	 * name,false
																	 * ,false,
																	 * loginPerson
																	 * , new
																	 * AsyncCallback
																	 * <
																	 * ArrayList
																	 * <
																	 * String>>(
																	 * ) {
																	 * 
																	 * @Override
																	 * public
																	 * void
																	 * onFailure
																	 * (
																	 * Throwable
																	 * caught) {
																	 * // TODO
																	 * Auto
																	 * -generated
																	 * method
																	 * stub
																	 * form.
																	 * hideWaitSymbol
																	 * (); }
																	 * 
																	 * @Override
																	 * public
																	 * void
																	 * onSuccess
																	 * (
																	 * ArrayList
																	 * <String>
																	 * result) {
																	 * // form.
																	 * showDialogMessage
																	 * (
																	 * "result set -- "
																	 * + result.
																	 * toString
																	 * ());
																	 * 
																	 * @
																	 * SuppressWarnings
																	 * (
																	 * "unchecked"
																	 * )
																	 * ArrayList
																	 * <String>
																	 * error =
																	 * result;
																	 * 
																	 * if(error.
																	 * contains
																	 * ("-1")){
																	 * form.
																	 * showDialogMessage
																	 * (
																	 * "Invalid Excel Sheet"
																	 * ); form.
																	 * hideWaitSymbol
																	 * (); }else
																	 * if(error.
																	 * contains
																	 * ("-2")){
																	 * form.
																	 * showDialogMessage
																	 * (
																	 * "Can not upload more than 15 records at a time."
																	 * ); form.
																	 * hideWaitSymbol
																	 * (); }else
																	 * if(error.
																	 * contains
																	 * ("-4")){
																	 * form.
																	 * showDialogMessage
																	 * (
																	 * "InValide Excel Formate."
																	 * ); form.
																	 * hideWaitSymbol
																	 * (); }else
																	 * if
																	 * (!error.
																	 * contains
																	 * ("-1") &&
																	 * result
																	 * .size
																	 * ()>0){
																	 * csvservice
																	 * .
																	 * setMinUploadError
																	 * (error,
																	 * new
																	 * AsyncCallback
																	 * <Void>()
																	 * {
																	 * 
																	 * @Override
																	 * public
																	 * void
																	 * onFailure
																	 * (
																	 * Throwable
																	 * caught) {
																	 * System
																	 * .out
																	 * .println(
																	 * "RPC call Failed"
																	 * +caught);
																	 * form.
																	 * hideWaitSymbol
																	 * (); }
																	 * 
																	 * @Override
																	 * public
																	 * void
																	 * onSuccess
																	 * (Void
																	 * result) {
																	 * String
																	 * gwt
																	 * =com.google
																	 * .
																	 * gwt.core.
																	 * client
																	 * .GWT.
																	 * getModuleBaseURL
																	 * (); final
																	 * String
																	 * url=gwt +
																	 * "csvservlet"
																	 * +
																	 * "?type="+
																	 * 131;
																	 * Window
																	 * .open
																	 * (url,
																	 * "test",
																	 * "enabled"
																	 * ); form.
																	 * hideWaitSymbol
																	 * (); } });
																	 * }
																	 * if(result
																	 * .
																	 * size()==0
																	 * ){ //
																	 * form.
																	 * showDialogMessage
																	 * (
																	 * " CheckUploadContractDetail method -- "
																	 * + result.
																	 * toString
																	 * ());
																	 * datamigAsync
																	 * .
																	 * CheckUploadContractDetail
																	 * (c.
																	 * getCompanyId
																	 * (), new
																	 * AsyncCallback
																	 * <
																	 * ArrayList
																	 * <
																	 * String>>(
																	 * ){
																	 * 
																	 * @Override
																	 * public
																	 * void
																	 * onFailure
																	 * (
																	 * Throwable
																	 * caught) {
																	 * form.
																	 * hideWaitSymbol
																	 * (); }
																	 * 
																	 * @Override
																	 * public
																	 * void
																	 * onSuccess
																	 * (
																	 * ArrayList
																	 * <String>
																	 * result) {
																	 * // form.
																	 * showDialogMessage
																	 * (
																	 * " CheckUploadContractDetail after method -- "
																	 * + result.
																	 * toString
																	 * ());
																	 * if(result
																	 * .
																	 * size()>0)
																	 * {
																	 * csvservice
																	 * .
																	 * setMinUploadError
																	 * (result,
																	 * new
																	 * AsyncCallback
																	 * <Void>()
																	 * {
																	 * 
																	 * @Override
																	 * public
																	 * void
																	 * onFailure
																	 * (
																	 * Throwable
																	 * caught) {
																	 * System
																	 * .out
																	 * .println(
																	 * "RPC call Failed"
																	 * +caught);
																	 * form.
																	 * hideWaitSymbol
																	 * (); }
																	 * 
																	 * @Override
																	 * public
																	 * void
																	 * onSuccess
																	 * (Void
																	 * result) {
																	 * 
																	 * String
																	 * gwt
																	 * =com.google
																	 * .
																	 * gwt.core.
																	 * client
																	 * .GWT.
																	 * getModuleBaseURL
																	 * (); final
																	 * String
																	 * url=gwt +
																	 * "csvservlet"
																	 * +
																	 * "?type="+
																	 * 131;
																	 * Window
																	 * .open
																	 * (url,
																	 * "test",
																	 * "enabled"
																	 * ); form.
																	 * hideWaitSymbol
																	 * (); } });
																	 * }else{
																	 * 
																	 * // form.
																	 * showDialogMessage
																	 * (
																	 * " UploadContractDetails after method -- "
																	 * + result.
																	 * toString
																	 * ());
																	 * datamigAsync
																	 * .
																	 * UploadContractDetails
																	 * (c.
																	 * getCompanyId
																	 * (),new
																	 * AsyncCallback
																	 * <
																	 * ArrayList
																	 * <
																	 * String>>(
																	 * ){
																	 * 
																	 * @Override
																	 * public
																	 * void
																	 * onFailure
																	 * (
																	 * Throwable
																	 * caught) {
																	 * // TODO
																	 * Auto
																	 * -generated
																	 * method
																	 * stub
																	 * form.
																	 * hideWaitSymbol
																	 * (); }
																	 * 
																	 * @Override
																	 * public
																	 * void
																	 * onSuccess
																	 * (
																	 * ArrayList
																	 * <String>
																	 * result) {
																	 * // form.
																	 * showDialogMessage
																	 * (
																	 * " UploadContractDetails after method -- "
																	 * + result.
																	 * toString
																	 * ()); //
																	 * if
																	 * (result.
																	 * contains
																	 * ("1")){
																	 * // form.
																	 * showDialogMessage
																	 * (
																	 * "Data Upload Process  Start..!!"
																	 * ); // }
																	 * //
																	 * if(!result
																	 * .
																	 * contains(
																	 * "1") &&
																	 * result
																	 * .size
																	 * ()>0){ //
																	 * csvservice
																	 * .
																	 * setMinUploadError
																	 * (result,
																	 * new
																	 * AsyncCallback
																	 * <Void>()
																	 * { // //
																	 * @Override
																	 * // public
																	 * void
																	 * onFailure
																	 * (
																	 * Throwable
																	 * caught) {
																	 * //
																	 * System.
																	 * out
																	 * .println(
																	 * "RPC call Failed"
																	 * +caught);
																	 * // // }
																	 * // //
																	 * @Override
																	 * // public
																	 * void
																	 * onSuccess
																	 * (Void
																	 * result) {
																	 * // String
																	 * gwt
																	 * =com.google
																	 * .
																	 * gwt.core.
																	 * client
																	 * .GWT.
																	 * getModuleBaseURL
																	 * (); //
																	 * final
																	 * String
																	 * url=gwt +
																	 * "csvservlet"
																	 * +
																	 * "?type="+
																	 * 131; //
																	 * Window
																	 * .open
																	 * (url,
																	 * "test",
																	 * "enabled"
																	 * ); // }
																	 * // }); //
																	 * }
																	 * 
																	 * if(result.
																	 * size
																	 * ()>0){
																	 * csvservice
																	 * .
																	 * setMinUploadError
																	 * (result,
																	 * new
																	 * AsyncCallback
																	 * <Void>()
																	 * {
																	 * 
																	 * @Override
																	 * public
																	 * void
																	 * onFailure
																	 * (
																	 * Throwable
																	 * caught) {
																	 * System
																	 * .out
																	 * .println(
																	 * "RPC call Failed"
																	 * +caught);
																	 * form.
																	 * hideWaitSymbol
																	 * ();
																	 * 
																	 * }
																	 * 
																	 * @Override
																	 * public
																	 * void
																	 * onSuccess
																	 * (Void
																	 * result) {
																	 * String
																	 * gwt
																	 * =com.google
																	 * .
																	 * gwt.core.
																	 * client
																	 * .GWT.
																	 * getModuleBaseURL
																	 * (); final
																	 * String
																	 * url=gwt +
																	 * "csvservlet"
																	 * +
																	 * "?type="+
																	 * 131;
																	 * Window
																	 * .open
																	 * (url,
																	 * "test",
																	 * "enabled"
																	 * ); form.
																	 * hideWaitSymbol
																	 * (); } });
																	 * }else{
																	 * datamigAsync
																	 * .
																	 * finallyUploadContractDetails
																	 * (c.
																	 * getCompanyId
																	 * (), new
																	 * AsyncCallback
																	 * <
																	 * ArrayList
																	 * <
																	 * String>>(
																	 * ) {
																	 * 
																	 * @Override
																	 * public
																	 * void
																	 * onSuccess
																	 * (
																	 * ArrayList
																	 * <String>
																	 * result) {
																	 * 
																	 * if(result.
																	 * contains
																	 * ("1")){
																	 * form.
																	 * showDialogMessage
																	 * (
																	 * "Contract Upload Process Started..!!"
																	 * ); form.
																	 * hideWaitSymbol
																	 * ();
																	 * }else{
																	 * form.
																	 * showDialogMessage
																	 * (
																	 * "Data upload process there some error occured in excution!!"
																	 * );
																	 * 
																	 * String
																	 * gwt
																	 * =com.google
																	 * .
																	 * gwt.core.
																	 * client
																	 * .GWT.
																	 * getModuleBaseURL
																	 * (); final
																	 * String
																	 * url=gwt +
																	 * "csvservlet"
																	 * +
																	 * "?type="+
																	 * 131;
																	 * Window
																	 * .open
																	 * (url,
																	 * "test",
																	 * "enabled"
																	 * ); form.
																	 * hideWaitSymbol
																	 * (); } }
																	 * 
																	 * @Override
																	 * public
																	 * void
																	 * onFailure
																	 * (
																	 * Throwable
																	 * caught) {
																	 * form.
																	 * showDialogMessage
																	 * (
																	 * "Data upload process there some error occured in excution!!"
																	 * ); form.
																	 * hideWaitSymbol
																	 * (); } });
																	 * }
																	 * 
																	 * }
																	 * 
																	 * }); } }
																	 * 
																	 * });
																	 * 
																	 * }
																	 * 
																	 * }
																	 * 
																	 * });
																	 */

					name = form.lstboxTransaction
							.getValue(form.lstboxTransaction.getSelectedIndex());
					form.showWaitSymbol();
					System.out.println("name: "
							+ form.lstboxTransaction
									.getValue(form.lstboxTransaction
											.getSelectedIndex()));
					form.txtfilecount.setValue(null);
					// form.showDialogMessage(c.getCompanyId()+"");
					form.txtsavedrecords.setValue(null);
					String loginPerson = LoginPresenter.loggedInUser;
					datamigAsync.savedContractUploadTransactionDetails(
							c.getCompanyId(), name, loginPerson,
							new AsyncCallback<ContractUploadDetails>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								
							}

							@Override
							public void onSuccess(ContractUploadDetails resultOne) {
								// TODO Auto-generated method stub
								
								if(resultOne.getErrorList().size()>0){

									form.showDialogMessage("Invalid Excel File");
									csvservice.setMinUploadError(resultOne.getErrorList(), new AsyncCallback<Void>() {

										@Override
										public void onFailure(Throwable caught) {
											System.out.println("RPC call Failed"+caught);
											form.hideWaitSymbol();
										}

										@Override
										public void onSuccess(Void result) {
											String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
											final String url=gwt + "csvservlet"+"?type="+131;
											Window.open(url, "test", "enabled");
											form.hideWaitSymbol();
										}
									});
								
								}else{
									datamigAsync.checkContractUploadDetails(resultOne,new AsyncCallback<ContractUploadDetails>() {

										@Override
										public void onFailure(Throwable caught) {
											// TODO Auto-generated method stub
											System.out.println("RPC call Failed"+caught);
											form.hideWaitSymbol();
										}

										@Override
										public void onSuccess(ContractUploadDetails resultTwo) {
											if(resultTwo.getErrorList().size()>0){

												form.showDialogMessage("Invalid Excel File");
												csvservice.setMinUploadError(resultTwo.getErrorList(), new AsyncCallback<Void>() {

													@Override
													public void onFailure(Throwable caught) {
														System.out.println("RPC call Failed"+caught);
														form.hideWaitSymbol();
													}

													@Override
													public void onSuccess(Void result) {
														String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
														final String url=gwt + "csvservlet"+"?type="+131;
														Window.open(url, "test", "enabled");
														form.hideWaitSymbol();
													}
												});
											}else{
												datamigAsync.CheckUploadContractDetail(resultTwo, new AsyncCallback<ContractUploadDetails>() {
													
													@Override
													public void onSuccess(ContractUploadDetails resultThree) {
														if(resultThree.getErrorList().size()>0){

															form.showDialogMessage("Invalid Excel File");
															csvservice.setMinUploadError(resultThree.getErrorList(), new AsyncCallback<Void>() {

																@Override
																public void onFailure(Throwable caught) {
																	System.out.println("RPC call Failed"+caught);
																	form.hideWaitSymbol();
																}

																@Override
																public void onSuccess(Void result) {
																	String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
																	final String url=gwt + "csvservlet"+"?type="+131;
																	Window.open(url, "test", "enabled");
																	form.hideWaitSymbol();
																}
															});
														}else{
															datamigAsync.UploadContractDetails(resultThree, new AsyncCallback<ContractUploadDetails>() {
																
																@Override
																public void onSuccess(ContractUploadDetails resultFour) {
																	// TODO Auto-generated method stub
																	if(resultFour.getErrorList().size()>0){

																		form.showDialogMessage("Invalid Excel File");
																		csvservice.setMinUploadError(resultFour.getErrorList(), new AsyncCallback<Void>() {

																			@Override
																			public void onFailure(Throwable caught) {
																				System.out.println("RPC call Failed"+caught);
																				form.hideWaitSymbol();
																			}

																			@Override
																			public void onSuccess(Void result) {
																				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
																				final String url=gwt + "csvservlet"+"?type="+131;
																				Window.open(url, "test", "enabled");
																				form.hideWaitSymbol();
																			}
																		});
																	}else{
																		datamigAsync.finallyUploadContractDetails(resultFour, new AsyncCallback<ArrayList<String>>() {
																			
																			@Override
																			public void onSuccess(ArrayList<String> result) {
																				// TODO Auto-generated method stub
																				if(result.size() >1 && !result.contains("1")){

																					form.showDialogMessage("Invalid Excel File");
																					csvservice.setMinUploadError(result, new AsyncCallback<Void>() {

																						@Override
																						public void onFailure(Throwable caught) {
																							System.out.println("RPC call Failed"+caught);
																							form.hideWaitSymbol();
																						}

																						@Override
																						public void onSuccess(Void result) {
																							String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
																							final String url=gwt + "csvservlet"+"?type="+131;
																							Window.open(url, "test", "enabled");
																							form.hideWaitSymbol();
																						}
																					});
																				}else if(result.contains("1") && result.size() == 1){
																					form.showDialogMessage("Contract data uploading process started successfully.!! ");
																					form.hideWaitSymbol();
																				}else{
																					form.showDialogMessage("There is some error Problem. Uploading process stoped.");
																					form.hideWaitSymbol();
																				}
																			}
																			
																			@Override
																			public void onFailure(Throwable caught) {
																				// TODO Auto-generated method stub
																				System.out.println("RPC call Failed"+caught);
																				form.hideWaitSymbol();
																			}
																		});
																	}
																}
																
																@Override
																public void onFailure(Throwable caught) {
																	// TODO Auto-generated method stub
																	
																}
															});
														}
														
													}
													
													@Override
													public void onFailure(Throwable caught) {
														// TODO Auto-generated method stub
														System.out.println("RPC call Failed"+caught);
														form.hideWaitSymbol();
													}
												});
											}
										}
									} );
								}
								
							}
						});
					
				
				} 
				/**
				 end **/
				else if(name.equals(AppConstants.UPDATEASSETDETAILS)){
					
					reactonUpdateAssetDetails();
					
				}
				
				else if(name.equals(AppConstants.CTCALOCATIONUPLOAD)){
					reactonCTCAllocationUpload();
				}
				else if(name.equals(AppConstants.CREDITNOTE)){
					reactonCreditNoteUpload();
				}
				else if(name.equals("Upload Inhouse Services")){
					reactonUploadInhouseServices();
				}
				//sheetal
				else if(name.equals("User Creation")){
					reactonUserCreation();
				}
				else if(name.equals(AppConstants.SERVICECREATIONUPLOAD)) {
					reactonServiceCreationUpload();
				}
				else if(name.equals("Service Cancellation Upload")){
					reactOnServiceCancellationUpload();
				}
				
				 else {
					// System.out.println("In else of config:  "+form.configurationsUploadComposite.getValue());
					name = form.lstboxTransaction
							.getValue(form.lstboxTransaction.getSelectedIndex());
					System.out.println("name: "
							+ form.lstboxTransaction
									.getValue(form.lstboxTransaction
											.getSelectedIndex()));
					form.txtfilecount.setValue(null);
					form.txtsavedrecords.setValue(null);

					datamigAsync.savedTransactionDetails(c.getCompanyId(),
							name, new AsyncCallback<ArrayList<Integer>>() {

								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									form.showDialogMessage("Failed");
								}

								@Override
								public void onSuccess(ArrayList<Integer> result) {
									// TODO Auto-generated method stub

									if (result.contains(-1)) {
										form.showDialogMessage("Invalid Excel File");
									}

									// vijay for lead
									else if (result.contains(-2)) {
										form.showDialogMessage("Customer does not exist Please create first");
									} else if (result.contains(-3)) {
										form.showDialogMessage("Product does not exist Please create first");
									}

									// } else if (result.contains(0)) {
									// form.showDialogMessage("Temp Success");
									// }
									else {
										System.out
												.println("Inside Else of onSuccess");
										form.txtfilecount.setValue(result
												.get(0) + "");
										form.txtsavedrecords.setValue(result
												.get(1) + "");
										form.showDialogMessage("Successful");
									}
								}
							});
				}
			}

		}

		if (event.getSource() == CustomerUploadOpt.getLblOk()) {

			// System.out.println("In else of config:  "+form.configurationsUploadComposite.getValue());
			System.out.println("Here You Go for Transaction");
			Company c = new Company();
			String name = form.lstboxTransaction
					.getValue(form.lstboxTransaction.getSelectedIndex());
			System.out.println("name: "
					+ form.lstboxTransaction.getValue(form.lstboxTransaction
							.getSelectedIndex()));
			boolean serviceStatus = CustomerUploadOpt.serviceCreate.getValue();
			boolean billingStatus = CustomerUploadOpt.billCreate.getValue();
			CustomerUploadOpt.hidePopUp();
			Console.log("Here You Go for Transaction");
			form.txtfilecount.setValue(null);
			form.txtsavedrecords.setValue(null);
			String loginPerson = LoginPresenter.loggedInUser;
			// form.showDialogMessage(name);
			datamigAsync.savedContractTransactionDetails(c.getCompanyId(),
					name, serviceStatus, billingStatus, loginPerson,
					new AsyncCallback<ArrayList<String>>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.showDialogMessage("Failed");
						}

						@Override
						public void onSuccess(ArrayList<String> result) {
							// TODO Auto-generated method stub
							ArrayList<String> error = result;
							
							if(error.contains("-2")) {
								String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url = gwt + "csvservlet" + "?type=" + 194;
								Window.open(url, "test", "enabled");
								form.showDialogMessage("Please find error file.");
							}
							else if (error.contains("1")) {
								System.out.println("Inside Else of onSuccess");
								form.showDialogMessage("Upload Process start Sucessfully");

							} else {
								result.remove("-1");
								if(result.size()>0){
									form.showDialogMessage("Invalid Excel File. \n "+ result.toString());
								}else{
									form.showDialogMessage("Invalid Excel File. \n ");
								}
								
							}
						}
					});

		}

		if (event.getSource() == CustomerUploadOpt.getLblCancel()) {
			CustomerUploadOpt.hidePopUp();
		}
		/**By Amol for ultarapestcontrolerp raised by rohan sir to update emailid of old invoices**/
		if(event.getSource()==fromandToDatePopUp.getBtnOne()){
			form.showWaitSymbol();
			if(fromandToDatePopUp.getFromDate()!=null&&fromandToDatePopUp.getToDate()!=null){
				generalAsync.updateInvoices(UserConfiguration.getCompanyId(),(format.format(fromandToDatePopUp.getFromDate().getValue())),(format.format(fromandToDatePopUp.getToDate().getValue())), new AsyncCallback<Void>() {
		             
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
						}
		
						@Override
						public void onSuccess(Void result) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
							form.showDialogMessage("Process Started...");
						}
					});
			}
		}
		/**Date 29-11-2019 by Amol To Update the Customer Email ID In Contracts**/
		if(event.getSource()==fromAndTodate.getBtnOne()){
			form.showWaitSymbol();
			if(fromAndTodate.getFromDate()!=null&&fromAndTodate.getToDate()!=null){
				custAsync.updateCustomerEmailIdInContract(UserConfiguration.getCompanyId(),(fromAndTodate.getFromDate().getValue()),(fromAndTodate.getToDate().getValue()), new AsyncCallback<String>() {
		             
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
					}
	
					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						panel.hide();
						form.hideWaitSymbol();
						form.showDialogMessage("Process Started...");
					}
				});
		}
		}
		
		
		
		
		/** date 10.4.2019 added by komal to update services **/
		if(event.getSource()==fromAndToDateBoxPopup.getBtnOne()){
			form.showWaitSymbol();
			if(fromAndToDateBoxPopup.getComposite() != null && fromAndToDateBoxPopup.getComposite().getValue() != null 
					&& (fromAndToDateBoxPopup.getComposite().getValue().getCount() != 0 || fromAndToDateBoxPopup.getComposite().getValue().getCount() != -1) ){
		}else{
			if(fromAndToDateBoxPopup.getFromDate().getValue() == null){
				form.showDialogMessage("Please select from date.");
				return;
			}
			if(fromAndToDateBoxPopup.getToDate().getValue() == null){
				form.showDialogMessage("Please select to date.");
				return;
			}
			if(fromAndToDateBoxPopup.getOlbBranch().getSelectedIndex() ==0){
				form.showDialogMessage("Please select branch.");
				return;
			}
		}
			genasync.updateServices(UserConfiguration.getCompanyId(), fromAndToDateBoxPopup.getFromDate().getValue(), fromAndToDateBoxPopup.getToDate().getValue(),fromAndToDateBoxPopup.getComposite().getIdValue(),
					fromAndToDateBoxPopup.getOlbBranch().getValue(fromAndToDateBoxPopup.getOlbBranch().getSelectedIndex()), new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					
					panel.hide();
					form.showDialogMessage(result);
					form.hideWaitSymbol();
				}
				
			});
			
		}
		
		if(event.getSource()==fromandToDatePopUp.getBtnTwo()){
			panel.hide();
		}
		
		
		if(event.getSource()==fromAndToDateBoxPopup.getBtnTwo()){
			panel.hide();
		}
		
		if(event.getSource()==fromAndTodate.getBtnTwo()){
			panel.hide();
		}
		
		if(event.getSource()==datepopup.getLblOk()){
			if(datepopup.validate()){
				reactonRateContractServiceValueUpdation();
			}
		}
		if(event.getSource()==datepopup.getLblCancel()){
			datepopup.hidePopUp();
		}
		
		if(event.getSource()==contractDownload.getLblOk()){
			if(validatePopUpFilters()){
				System.out.println("all ok");
				reactonDownloadContractsData();
			}
		}
		if(event.getSource()==contractDownload.getLblCancel()){
			contractDownload.hidePopUp();
		}
		
		if(event.getSource()==updateFrequencyPopup.getBtnOne()){
			if(updateFrequencyPopup.getContractStartDate().getValue()!=null){
				updateFrequency(updateFrequencyPopup.getContractStartDate().getValue());
				panel.hide();
			}else{
				form.showDialogMessage("Please enter contract start date.");
			}
		}
		
		if(event.getSource()==updateFrequencyPopup.getBtnTwo()){
			panel.hide();
		}
		
	}

	

	



	
	
	

	

	

	

	

	private boolean validatePopUpFilters() {
		if(contractDownload.getChkActiveContracts().getValue()==false){
			System.out.println("contractDownload.getChkActiveContracts().getValue()"+contractDownload.getChkActiveContracts().getValue());
			if(contractDownload.getEndDateComparator().getFromDate().getValue()==null && contractDownload.getEndDateComparator().getToDate().getValue()==null){
				form.showDialogMessage("From date and To date is mandatory");
				return false;
			}
			if(contractDownload.getEndDateComparator().getFromDate().getValue()==null){
				form.showDialogMessage("From date is mandatory");
				return false;
			}
			if(contractDownload.getEndDateComparator().getToDate().getValue()==null){
				form.showDialogMessage("To date is mandatory");
				return false;
			}
		}
		
		if(contractDownload.getEmailTable().getDataprovider().getList().size()==0){
			form.showDialogMessage("Please add email id");
			return false;
		}
		
		return true;
	}

	private void updateLeaveBalance() {
		// TODO Auto-generated method stub
		form.showWaitSymbol();
		dataTaskAsync.getEmployeeLeaveDetails(UserConfiguration.getCompanyId(), new AsyncCallback<ArrayList<EmployeeLeaveBalance>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
				System.out.println("First FAIL RPC...");
			}

			@Override
			public void onSuccess(ArrayList<EmployeeLeaveBalance> result) {
				System.out.println("FIRST RPC...");
				if(result!=null&&result.size()!=0){
					if(result.get(0).getEmpId()==-1){
						form.hideWaitSymbol();
						form.showDialogMessage("Invalid excel format.");
					}else{
						dataTaskAsync.validateEmployeeLeaveDetails(result, new AsyncCallback<ArrayList<EmployeeLeaveBalance>>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								form.hideWaitSymbol();
								System.out.println("SECOND FAIL RPC...");
							}

							@Override
							public void onSuccess(ArrayList<EmployeeLeaveBalance> result) {
								// TODO Auto-generated method stub
								System.out.println("SECOND RPC...");
								if(result.get(0).getEmpId()==-1){
									form.hideWaitSymbol();
									form.showDialogMessage("Please find excel.");
									
									String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url = gwt + "csvservlet" + "?type=" + 164;
									Window.open(url, "test", "enabled");
								}else{
									dataTaskAsync.updateLeaveBalance(result, new AsyncCallback<String>() {

										@Override
										public void onFailure(Throwable caught) {
											// TODO Auto-generated method stub
											form.hideWaitSymbol();
										}

										@Override
										public void onSuccess(String result) {
											// TODO Auto-generated method stub
											form.hideWaitSymbol();
											form.showDialogMessage("Leave balance update successful.");
										}
									});
								}
							}
						});
					}
				}
			}
		});
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub

		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		Console.log("selected option="+text);

		/**Date 18-12-2019 by Amol added a button Update the service status to open**/
		if(text.equals("Update ServicesSStatus to Open")){
			reactOnUpdateServicesStatus();
		}
		
		
		
		
		
		
		
		
		/**
		 * date 26/10/2017 added by komal to update folloeup date in lead and
		 * quotation
		 **/
		if (text.equals("Update")) {
//			reactToUpdate();
			
			/**
			 * Date : 22-09-2018 BY ANIL
			 */
			updateEmployee("Update",0,null,null,null);
		}
		
		if(text.equals("UpdateCNC")){
			genasync.updateCnc(UserConfiguration.getCompanyId(),new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(Void result) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage("process started");
				}
			});
		}
		
		
		
		/**
		 * Date 09-05-2018 
		 * Developer : Vijay
		 * Des :- For updating payment docs with follow up date
		 */
		if (text.equals("Update Payment")) {
			System.out.println("Hi vijay 1");
			reactToUpdatePaymentDocsWithFollowUpDate();
		}
		/**
		 * ends here
		 */
		
		/**
		 * Date : 27-07-2018 BY ANIL
		 */
		if (text.equals("Delete Attendance")) {
			panel=new PopupPanel(true);
			empIdPopup.setAction("Delete Attendance");
			empIdPopup.getIbIntegerBox().setValue(null);
			panel.add(empIdPopup);
			panel.center();
			panel.show();
		}
		if (text.equals("Delete Payroll")) {
			panel=new PopupPanel(true);
			empIdPopup.setAction("Delete Payroll");
			empIdPopup.getIbIntegerBox().setValue(null);
			panel.add(empIdPopup);
			panel.center();
			panel.show();
		}
		
		/**Date 29-11-2019 by Amol Added a For to Update the Customer mail ID In Contract**/
		if(text.equals("Update Customer Email")){
			panel=new PopupPanel(true);
			panel.add(fromAndTodate);
			panel.center();
			panel.show();
		}
		
		
		
		
		if(text.equals("UpdateInvoices")){
			 Console.log("INSIDE UPDATE INVOICESSSSSS");
			 
			 panel=new PopupPanel(true);			
				panel.add(fromandToDatePopUp);
				panel.center();
				panel.show();
//			generalAsync.updateInvoices(UserConfiguration.getCompanyId(), new AsyncCallback<Void>() {
//             
//				@Override
//				public void onFailure(Throwable caught) {
//					// TODO Auto-generated method stub
//					form.hideWaitSymbol();
//				}
//
//				@Override
//				public void onSuccess(Void result) {
//					// TODO Auto-generated method stub
//					form.hideWaitSymbol();
//					form.showDialogMessage("Process Started...");
//				}
//			});
		}
		
//		Update Branch In Payslip
		
		if (text.equals("Update Branch In Payslip")) {
			updateEmployee("Update Branch", 0, null,null,null);
		}
		
		if (text.equals("Update PT")) {
			updateEmployee("Update PT", 0, null,null,null);
		}
		
		if (text.equals("Update Advance")) {
			updateEmployee("Update Advance", 0, null,null,null);
			
		}
		
		if (text.equals("Update Attendance and Payroll")) {
//			updateEmployee("Update Attendance and Payroll", 0, null);
			panel=new PopupPanel(true);
			empIdPopup.setAction("Update Attendance and Payroll");
			empIdPopup.getIbIntegerBox().setValue(null);
			empIdPopup.getLbBranch().setSelectedIndex(0);
			empIdPopup.getProject().setSelectedIndex(0);
			panel.add(empIdPopup);
			panel.center();
			panel.show();
			
		}
		if (text.equals("Update Project")) {
			updateEmployee("Update Project", 0, null,null,null);
			
		}
		
//		if (text.equals("Update Aadhar")) {
//			updateEmployee("Update Aadhar", 0, null,null,null);
//			
//		}
		
		/**
		 * Date 28-11-2018 By Vijay
		 * Des :- If billing id is created as 0 then updating records with billing Id
		 */
		if(text.equals("Update Billing")){
			reactonUpdateBillingDocs();
		}
		
		if(text.equals("Update GRN")){
			reactonUpdateGRN();
		}
		
		/**
		 * Date 28-01-2019 by Vijay For NBHC Inventory Management
		 * updating warehouse master with warehouse type
		 */
		if(text.equals("Update Warehouse Master")){
			reactonUpdateWarehouseMaster();
		}
		/**
		 * Date 10-05-2019 by Vijay 
		 * Des :- Creating Storage location and Storage Bin
		 */
		if(text.equals("Update Storage Location Bin")){
			reactonCreateStorageLocationBinforExistingWarehouses();
		}
		/**
		 * Date 20-05-2019 by Vijay for Updating contract discontinue Date in Contract
		 */
		if(text.equals("Update Contract Discontinue")){
			reactonUpdateContractDisconitinue();
		}
		/** date 10.4.2019 added by komal to update service*/
		if(text.equals("Update Service")){
			panel=new PopupPanel(true);			
			panel.add(fromAndToDateBoxPopup);
			panel.center();
			panel.show();
		}
		/**
		 * Date 27-08-2019 by Vijay 
		 * Des :- NBHC CCPM Rate contracts Service value updation for Contract PNL Report 
		 */
		if(text.equals("Update RateContract Services")){
			datepopup.showPopUp();
		}
		
		
		/**
		 * @author Anil , Date : 14-06-2019
		 * updating pf,esic ,uan, adhar ,pan and bank details on payslip
		 */
		if (text.equals("Update Payslip")) {
			panel=new PopupPanel(true);
			empIdPopup.setAction("Update Payslip");
			empIdPopup.getIbIntegerBox().setValue(null);
			panel.add(empIdPopup);
			panel.center();
			panel.show();
		}
		
		/**
		 * @author Anil, Date : 15-11-2019
		 * for deleting cancelled service before 1st Apr 2018
		 * date will be picked from process configuration under process Name-Delete Service and process type will be date formatted as dd/MM/yyyy
		 */
		if(text.equals("Delete Cancelled Services")){
			updateEmployee("Delete Cancelled Services", 0, null,null,null);
		}
		
		
		if(text.equals("View Heap Memory")){
			CommonServiceAsync commonService = GWT.create(CommonService.class);
			form.showWaitSymbol();
			commonService.checkHeapMemoryStatus(new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage(result);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage(caught.getMessage());
				}
			});
		}
		
		if(text.equals("Release Heap Memory")){
			CommonServiceAsync commonService = GWT.create(CommonService.class);
			form.showWaitSymbol();
			commonService.deleteStaticDataFromHeapMemory(new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage(result);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage(caught.getMessage());
				}
			});
		}	
		if(text.equals("UpdateSerialNumber")){
			sernumber.showPopUp();
		}
		
		/**
		 * Date 13-03-2020
		 * Des :- NBHC services updating with WMS service flag
		 */
		if(text.equals("Update Services WMSFlag")){
			reactonUpdateServicesWithWMSFlag();
		}
		
		if(text.equals("Download Contracts")){
			reactOnDownloadContracts();
		}
		if (text.equals("Update Frquency In Services")) {
			panel=new PopupPanel(true);
//			updateFrequencyPopup.setAction("Update Frequency");
			updateFrequencyPopup.getContractStartDate().setValue(null);
			panel.add(updateFrequencyPopup);
			panel.center();
			panel.show();
		}
		
		if(text.equals("Delete And Recreate Accounting Interface Entry")){
			invoicedatepopup.showPopUp();
		}
		
		if(text.equals("Delete And Recreate Accounting Interface Payment Entry")){
			paymentdatepopup.showPopUp();
		}
		if(text.equals("Update Employee Id in User")){
			reactOnUpdateEmployeeIdInUser(model.getCompanyId());
		}
		if(text.equals("Update Customer Branch Service Address")){
			customerinfopopup.showPopUp();
		}
		if(text.equals("Update Finding Entity For Completed Services")){
			Console.log("Update Finding Entity For Completed Services clicked");
			reactOnUpdateFindings();
		}
	}
	

	
	


	

	private void reactOnUpdateServicesStatus() {
		
		Console.log("INSIDE UPDATE SERVICES");
		
		form.showWaitSymbol();
		customernamechangeasync.updateServicesStatusOpen(UserConfiguration.getCompanyId(), new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				Console.log("in on success ");
				form.showDialogMessage(result);
				form.hideWaitSymbol();
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("falied");
				form.showDialogMessage(caught.getMessage());
				form.hideWaitSymbol();
			}
		});
		
	}

	private void updateEmployee(String action,int empId,String payrollPeriod,String project,String branch) {
		form.showWaitSymbol();
		String roleName="";
		if(UserConfiguration.getRole()!=null){
			roleName=UserConfiguration.getRole().getRoleName();
		}
		genasync.updateEmployeeInfoStatus(UserConfiguration.getCompanyId(),action,empId,payrollPeriod,project,branch,roleName,new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
				
				if(result!=null&&result.equals("ERROR")){
					form.showDialogMessage("Please find error csv file.");
				
					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url = gwt + "csvservlet" + "?type=" + 151;
					Window.open(url, "test", "enabled");
					
					return;
				}
				form.showDialogMessage(result);
			}
		});
	}

	/**
	 * Date 09-05-2018 
	 * Developer : Vijay
	 * Des :- For updating payment docs with follow up date
	 */
	
	private void reactToUpdatePaymentDocsWithFollowUpDate() {
		System.out.println("Hi vijay");
		form.showWaitSymbol();
		genasync.updatePaymentDocsWithFollowupDate(model.getCompanyId(), new AsyncCallback<Integer>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Unexpected Error occured please");
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(Integer result) {
				// TODO Auto-generated method stub
				if(result==1){
					form.showDialogMessage("Follow-up date updation successful");
				}
				else if(result==-1){
					form.showDialogMessage("Unexpected Error occured please contact to support");
				}
				form.hideWaitSymbol();
			}
		});
		
	}
	/**
	 * ends here
	 */
	
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub

	}

	/**
	 * date 26/10/2017 added by komal to update followup date in lead and
	 * quotation
	 **/

	public void reactToUpdate() {
		genasync.updateLeadQuotationFollowUpDate(model.getCompanyId(),
				new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Follow-up date updation failed");
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Follow-up date updation successful");
					}
				});
	}
	
	/**
	 * Date 28-11-2018 By Vijay
	 * Des :- If billing id is created as 0 then updating records with billing Id
	 */
	private void reactonUpdateBillingDocs() {
		
		final GenricServiceAsync genAsync = GWT.create(GenricService.class);
		
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(0);
		filterVec.add(filter);
		
		querry.setFilters(filterVec);
		querry.setQuerryObject(new BillingDocument());
		
		form.showWaitSymbol();
		
		genAsync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				if(result.size()>0){
					genAsync.save(result, new AsyncCallback<ArrayList<ReturnFromServer>>() {
						
						@Override
						public void onSuccess(ArrayList<ReturnFromServer> result) {
							// TODO Auto-generated method stub
							form.showDialogMessage("Data Updated Sucessfully!");
							form.hideWaitSymbol();
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
						}
					});
				}
				else{
					form.hideWaitSymbol();
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	/**
	 * ends here
	 */
	
	private void reactonUpdateGRN() {
		form.showWaitSymbol();
		genasync.updateGRNDocs(model.getCompanyId(), new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
				form.showDialogMessage(result);
			}
		});
		
	}
	
	/**
	 * Date 28-01-2019 By Vijay NBHC Inventory Management Updating product inventory with Warehouse type
	 * to identify warehouse and cluster warehouse
	 */
	private void reactonUpdateWarehouseMaster() {
		form.showWaitSymbol();
		genasync.updateProductInventoryWithWarehouseType(model.getCompanyId(), new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(String result) {
				if(result.equals("failed")){
					form.showDialogMessage("Please activate process config!");
				}
				else if(result.equals("Success")){
					form.showDialogMessage("Records Updating Process Started!");
				}
				form.hideWaitSymbol();
			}
		});
		
	}
	
	/**
	 * Date 10-05-2019 by Vijay 
	 * Des :- Creating Storage location and Storage Bin
	 */
	private void reactonCreateStorageLocationBinforExistingWarehouses() {
//		form.hideWaitSymbol();
		genasync.createStoraLocationBin(model.getCompanyId(), new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
//				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
//				form.hideWaitSymbol();
				if(result.equals("Success")){
					form.showDialogMessage("Storage Location Bin Creating Process Started!");
				}
			}
		});
	}

	
	private void reactonUpdateContractDisconitinue() {

		genasync.updateContractDiscontinued(model.getCompanyId(), new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				if(result.equals("Success")){
					form.showDialogMessage("Records Updating Process Started!");
				}
			}
		});
		
	}
	

	/**
	 * Date 27-08-2019 by Vijay 
	 * Des :- NBHC CCPM Rate contracts Service value updation for Contract PNL Report 
	 */
	private void reactonRateContractServiceValueUpdation() {
		Date fromDate = datepopup.getDbfromDate().getValue();
		Date toDate = datepopup.getDbToDate().getValue();
		String branch = datepopup.getOlbBranch().getValue();
		
		genasync.updateServiceValueForRateContract(fromDate,toDate,branch,model.getCompanyId(),new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				datepopup.hidePopUp();
			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				form.showDialogMessage(result);
				datepopup.hidePopUp();
			}
		});
	}
	private void saveCustomerBranch(){
		Company c = new Company();
		form.showWaitSymbol();
//		datamigAsync =  GWT.create(DataMigrationService.class);
//		datamigAsync.uploadCustomerBranch(c.getCompanyId(), new AsyncCallback<ArrayList<CustomerBranchDetails>>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				form.hideWaitSymbol();
//			}
//
//			@Override
//			public void onSuccess(ArrayList<CustomerBranchDetails> result) {
//				// TODO Auto-generated method stub
//				if(result==null){
//					form.hideWaitSymbol();
//					form.showDialogMessage("Invalid Excel File Format.");
//					return;
//				}
//				if(result.size() == 1 && result.get(0).getCount() == -2){
//					form.hideWaitSymbol();
//					form.showDialogMessage("You can only upload 150 branches.");
//					return;
//				}
//				for(CustomerBranchDetails obj:result){
//					if(obj.getCount()==-1){
//						form.hideWaitSymbol();
//						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
//						final String url = gwt + "csvservlet" + "?type=" + 178;
//						Window.open(url, "test", "enabled");
//						form.showDialogMessage("Please find error file.");
//						return;
//					}
//				}
//				
//				datamigAsync.saveCustomerBranch(result, new AsyncCallback<String>() {
//
//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//						form.hideWaitSymbol();
//					}
//
//					@Override
//					public void onSuccess(String result) {
//						// TODO Auto-generated method stub
//						form.hideWaitSymbol();
//						form.showDialogMessage("Customer branch upload process started.");
//					}
//				});
//				
//			}
//		});
		
		documentuploadAsync.customerBranchUpload(c.getCompanyId(), new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();

			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
				form.showDialogMessage(result);

			}
		});
		
		
		
	}
	
	
	private void reactonUpdateServicesWithWMSFlag() {
		CommonServiceAsync commonService = GWT.create(CommonService.class);
		form.showWaitSymbol();
		
		commonService.updateWMSServicesFlag(model.getCompanyId(), new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.showDialogMessage("RPC Failed");
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				form.showDialogMessage(result);
				form.hideWaitSymbol();
			}
		});
	}
	
	
	private void reactonVendorProductPrice() {
		final Company c = new Company();
		form.showWaitSymbol();
		datamigAsync =  GWT.create(DataMigrationService.class);
		datamigAsync.uploadVendorProductPrice(c.getCompanyId(), new AsyncCallback<ArrayList<VendorPriceListDetails>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();

			}

			@Override
			public void onSuccess(ArrayList<VendorPriceListDetails> result) {

				if(result==null){
					form.hideWaitSymbol();
					form.showDialogMessage("Invalid file.");
					return;
				}
				for(VendorPriceListDetails obj:result){
					if(obj.getCount()==-1){
						form.hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 184;
						Window.open(url, "test", "enabled");
						form.showDialogMessage("Please find error file.");
						return;
					}
				}
				datamigAsync.vednorProductPrice(c.getCompanyId(), "Vendor Product Price", new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						form.showDialogMessage(result);
						form.hideWaitSymbol();
					}
				} );
				
			}
		});
	}
	
	
	
	private void reactonCTCTemplate() {

		final Company c = new Company();
		form.showWaitSymbol();
		datamigAsync =  GWT.create(DataMigrationService.class);
		
		datamigAsync.validateCTCTemplate(c.getCompanyId(), new AsyncCallback<ArrayList<CTCTemplate>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<CTCTemplate> result) {
				// TODO Auto-generated method stub


				if(result==null){
					form.hideWaitSymbol();
					form.showDialogMessage("Invalid file.");
					return;
				}
				for(CTCTemplate obj:result){
					if(obj.getCount()==-1){
						form.hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 185;
						Window.open(url, "test", "enabled");
						form.showDialogMessage("Please find error file.");
						return;
					}
				}
			
				datamigAsync.uploadCTCTemplate(c.getCompanyId(), "CTC Template", new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						form.showDialogMessage(result);
						form.hideWaitSymbol();
					}
				});
				
			}
		});
	}
	
	
	private void reactonUpdateAssetDetails() {

		final Company c = new Company();
		form.showWaitSymbol();
		
		datamigAsync.validateUpdateAssetDetails(c.getCompanyId(), new AsyncCallback<ArrayList<AssetMovementInfo>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<AssetMovementInfo> result) {
				// TODO Auto-generated method stub
				if(result==null){
					form.hideWaitSymbol();
					form.showDialogMessage("Invalid file.");
					return;
				}
				for(AssetMovementInfo obj:result){
					if(obj.getCount()==-1){
						form.hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 187;
						Window.open(url, "test", "enabled");
						form.showDialogMessage("Please find error file.");
						return;
					}
				}
				
				datamigAsync.uploadAssetDetails(c.getCompanyId(), AppConstants.UPDATEASSETDETAILS, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();	
					}

					@Override
					public void onSuccess(String result) {
						form.showDialogMessage(result);
						form.hideWaitSymbol();						
					}
				});
			}
		});
	}

	private void reactonCTCAllocationUpload() {

		final Company c = new Company();
		form.showWaitSymbol();
		String loggedInUser = LoginPresenter.loggedInUser;
		datamigAsync.validateCTCAllocation(c.getCompanyId(),loggedInUser, new AsyncCallback<ArrayList<CTCTemplate>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<CTCTemplate> result) {


				// TODO Auto-generated method stub
				if(result==null){
					form.hideWaitSymbol();
					form.showDialogMessage("Invalid Excel File Format.");
					return;
				}
				for(CTCTemplate obj:result){
					if(obj.getCount()==-1){
						form.hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 198;
						Window.open(url, "test", "enabled");
						form.showDialogMessage("Please find error file.");
						return;
					}
				}
				
				form.showDialogMessage("CTC Allocation Upload Process Started");

				form.hideWaitSymbol();
				
			}
		});
		
	}
	
	private void reactonCreditNoteUpload() {
		final Company c = new Company();
		form.showWaitSymbol();
		datamigAsync.validateAndUploadCreditNote(c.getCompanyId(), new AsyncCallback<ArrayList<CreditNote>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<CreditNote> result) {
				// TODO Auto-generated method stub
				if(result==null){
					form.hideWaitSymbol();
					form.showDialogMessage("Invalid Excel File Format!");
					return;
				}
				for(CreditNote obj:result){
					if(obj.getCount()==-1){
						form.hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 199;
						Window.open(url, "test", "enabled");
						form.showDialogMessage("Please find error file.");
						return;
					}
				}
				
				form.showDialogMessage("Credit Note Upload Process Started!");

				form.hideWaitSymbol();
				
			}
		});
	}

	
	private void reactonUploadInhouseServices() {
		final Company c = new Company();
		form.showWaitSymbol();
		
	
		datamigAsync.validateInhouseSerivces(c.getCompanyId(), new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<String> exceldata) {


				// TODO Auto-generated method stub
				Console.log("Result"+exceldata.size());
				if(exceldata.contains("-1")){
					Console.log("Found some no stack services");
					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url = gwt + "csvservlet" + "?type=" + 202;
					Window.open(url, "test", "enabled");
					
					exceldata.remove("-1");
					Console.log("Result == "+exceldata.size());

					datamigAsync.validateAndUploadInhouseSerivces(c.getCompanyId(),exceldata, new AsyncCallback<ArrayList<StackMaster>>() {

						@Override
						public void onFailure(Throwable caught) {
							
						}

						@Override
						public void onSuccess(ArrayList<StackMaster> result) {
							// TODO Auto-generated method stub
							if(result==null){
								form.hideWaitSymbol();
								form.showDialogMessage("Invalid Excel File Format!");
								return;
							}
							for(StackMaster obj:result){
								if(obj.getCount()==-1){
									form.hideWaitSymbol();
									String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url = gwt + "csvservlet" + "?type=" + 201;
									Window.open(url, "test", "enabled");
									form.showDialogMessage("Please find error file.");
									return;
								}
							}
							
							form.showDialogMessage("InHouse Service Upload Process Started!");

							form.hideWaitSymbol();
						}
					});
					
				}
				else{
					Console.log("Didnt found no stack service");
					exceldata.remove("1");
					Console.log("Result == "+exceldata.size());

					datamigAsync.validateAndUploadInhouseSerivces(c.getCompanyId(),exceldata, new AsyncCallback<ArrayList<StackMaster>>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onSuccess(ArrayList<StackMaster> result) {
							// TODO Auto-generated method stub
							if(result==null){
								form.hideWaitSymbol();
								form.showDialogMessage("Invalid Excel File Format!");
								return;
							}
							for(StackMaster obj:result){
								if(obj.getCount()==-1){
									form.hideWaitSymbol();
									String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url = gwt + "csvservlet" + "?type=" + 201;
									Window.open(url, "test", "enabled");
									form.showDialogMessage("Please find error file.");
									return;
								}
							}
							
							form.showDialogMessage("InHouse Service Upload Process Started!");

							form.hideWaitSymbol();
						}
					});
				}
			
			}
		});
//		datamigAsync.validateInhouseSerivces(c.getCompanyId(), new AsyncCallback<String>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				
//			}
//
//			@Override
//			public void onSuccess(String result) {
//				// TODO Auto-generated method stub
//				Console.log("Result"+result);
//				if(result.contains("-1")){
//					Console.log("Found some no stack services");
//					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
//					final String url = gwt + "csvservlet" + "?type=" + 202;
//					Window.open(url, "test", "enabled");
//					
//					datamigAsync.validateAndUploadInhouseSerivces(c.getCompanyId(), new AsyncCallback<ArrayList<StackMaster>>() {
//
//						@Override
//						public void onFailure(Throwable caught) {
//							
//						}
//
//						@Override
//						public void onSuccess(ArrayList<StackMaster> result) {
//							// TODO Auto-generated method stub
//							if(result==null){
//								form.hideWaitSymbol();
//								form.showDialogMessage("Invalid Excel File Format!");
//								return;
//							}
//							for(StackMaster obj:result){
//								if(obj.getCount()==-1){
//									form.hideWaitSymbol();
//									String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
//									final String url = gwt + "csvservlet" + "?type=" + 201;
//									Window.open(url, "test", "enabled");
//									form.showDialogMessage("Please find error file.");
//									return;
//								}
//							}
//							
//							form.showDialogMessage("InHouse Service Upload Process Started!");
//
//							form.hideWaitSymbol();
//						}
//					});
//					
//				}
//				else{
//					Console.log("Didnt found no stack service");
//					datamigAsync.validateAndUploadInhouseSerivces(c.getCompanyId(), new AsyncCallback<ArrayList<StackMaster>>() {
//
//						@Override
//						public void onFailure(Throwable caught) {
//							// TODO Auto-generated method stub
//							
//						}
//
//						@Override
//						public void onSuccess(ArrayList<StackMaster> result) {
//							// TODO Auto-generated method stub
//							if(result==null){
//								form.hideWaitSymbol();
//								form.showDialogMessage("Invalid Excel File Format!");
//								return;
//							}
//							for(StackMaster obj:result){
//								if(obj.getCount()==-1){
//									form.hideWaitSymbol();
//									String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
//									final String url = gwt + "csvservlet" + "?type=" + 201;
//									Window.open(url, "test", "enabled");
//									form.showDialogMessage("Please find error file.");
//									return;
//								}
//							}
//							
//							form.showDialogMessage("InHouse Service Upload Process Started!");
//
//							form.hideWaitSymbol();
//						}
//					});
//				}
//			}
//		});
		
		
		
	}
	
	private void reactonStockUpload() {
		Company comp = new Company();
		
		form.showWaitSymbol();
		datamigAsync.validateStockUpload(comp.getCompanyId(), new AsyncCallback<ArrayList<ProductInventoryView>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();

			}

			@Override
			public void onSuccess(ArrayList<ProductInventoryView> result) {

				if(result==null){
					form.hideWaitSymbol();
					form.showDialogMessage("Invalid file.");
					return;
				}
				for(ProductInventoryView obj:result){
					if(obj.getCount()==-1){
						form.hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 204;
						Window.open(url, "test", "enabled");
						form.showDialogMessage("Please find error file.");
						return;
					}
				}
				form.hideWaitSymbol();
				form.showDialogMessage("Stock Upload Process Started");
			}
		});
	}
	/**Added by sheetal:31:01:2022 
	   Des: for user creation upload program**/
	
	private void reactonUserCreation() {
		final Company c = new Company();
		form.showWaitSymbol();
		datamigAsync.validateAndUploadUser(c.getCompanyId(), new AsyncCallback<ArrayList<Employee>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<Employee> result) {
				// TODO Auto-generated method stub
				if(result==null){
					form.hideWaitSymbol();
					form.showDialogMessage("Invalid Excel File Format!");
					return;
				}
				for(Employee obj:result){
					if(obj.getCount()==-1){
						form.hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 206;
						Window.open(url, "test", "enabled");
						form.showDialogMessage("Please find error file.");
						return;
					}
				}
				
				form.showDialogMessage("User Creation Upload Process Started!");

				form.hideWaitSymbol();
				
			}
		});
	}

	private void reactonServiceCreationUpload() {
		

		final Company c = new Company();
		form.showWaitSymbol();
		datamigAsync.validateAndUploadServiceCreation(c.getCompanyId(), new AsyncCallback<ArrayList<Service>>() {
			
			@Override
			public void onSuccess(ArrayList<Service> result) {
				// TODO Auto-generated method stub
				if(result==null){
					form.hideWaitSymbol();
					form.showDialogMessage("Invalid Excel File Format!");
					return;
				}
				for(Service obj:result){
					if(obj.getCount()==-1){
						form.hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 212;
						Window.open(url, "test", "enabled");
						form.showDialogMessage("Please find error file.");
						return;
					}
				}
				
				form.showDialogMessage("Service Creation Upload Process Started!");

				form.hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable arg0) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}
		});
	}
private void saveCustomerBranchWithServiceLocation() {

		Company c = new Company();
		form.showWaitSymbol();
		datamigAsync =  GWT.create(DataMigrationService.class);
		datamigAsync.uploadCustomerBranchWithLocationandArea(c.getCompanyId(), new AsyncCallback<ArrayList<CustomerBranchServiceLocation>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<CustomerBranchServiceLocation> result) {
				// TODO Auto-generated method stub
				if(result==null){
					form.hideWaitSymbol();
					form.showDialogMessage("Invalid Excel File Format.");
					return;
				}
				if(result.size() == 1 && result.get(0).getCount() == -2){
					form.hideWaitSymbol();
					form.showDialogMessage("You can only upload 150 branches.");
					return;
				}
				for(CustomerBranchServiceLocation obj:result){
					if(obj.getCount()==-1){
						form.hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 211;
						Window.open(url, "test", "enabled");
						form.showDialogMessage("Please find error file.");
						return;
					}
				}
				
				form.showDialogMessage("Upload of Customer branch with servive location and area has started !!");
				
				form.hideWaitSymbol();
				
				
			}
		});
		
	}


		private void reactOnDownloadContracts() {
			contractDownload.showPopUp();
			contractDownload.getEmailTable().getDataprovider().getList().clear();
		}
		
		
		private void reactonDownloadContractsData() {
			 CommonServiceAsync commonAsync = GWT.create(CommonService.class);
			 Company comp = new Company();
			 List<String> list = contractDownload.getEmailTable().getDataprovider().getList();
			 ArrayList<String> emaillist = new ArrayList<String>();
			 emaillist.addAll(list);
			 Date fromDate =null;
			 Date todate = null;
			 
			 if(contractDownload.getEndDateComparator().getfromDateValue()!=null){
				 fromDate = contractDownload.getEndDateComparator().getfromDateValue();
			 }
			 if(contractDownload.getEndDateComparator().gettoDateValue()!=null){
				 todate = contractDownload.getEndDateComparator().gettoDateValue();
			 }
			 
				String Url = com.google.gwt.core.client.GWT.getModuleBaseURL(); 
				Console.log("Url"+Url);
				
				String [] urlArray = Url.split("slick_erp/");
				Console.log("final url link"+urlArray[0]);
				System.out.println("final url link"+urlArray[0]);
				String finalURL = urlArray[0];
			 commonAsync.SendContractDataOnEmail(comp.getCompanyId(), fromDate,
					 todate, contractDownload.getChkActiveContracts().getValue(),
					 emaillist, finalURL, new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onSuccess(String result) {
							
							form.showDialogMessage(result);
							contractDownload.hidePopUp();
						}
					});

		}
		
		private void updateFrequency(Date startDate) {
			form.showWaitSymbol();
			String roleName="";
			if(UserConfiguration.getRole()!=null){
				roleName=UserConfiguration.getRole().getRoleName();
			}
			genasync.updateFrequency(model.getCompanyId(), startDate,new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage("Process started"+result);
				}
			});
		}


		private void reactOnUpdateEmployeeIdInUser(long companyId){
			Console.log(" reactOnUpdateEmployeeIdInUser called");
			form.showWaitSymbol();
			genasync.updateEmployeeIdInUser(companyId, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Process Failed");
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					if(result!=null){
						form.showDialogMessage(result);	
						form.hideWaitSymbol();
					}
				}
			});
		}
		
		
		private void reactOnServiceCancellationUpload() {
			
			final Company c = new Company();
			form.showWaitSymbol();
			documentupload.validateServiceCancellationUpload(c.getCompanyId(),LoginPresenter.loggedInUser, new AsyncCallback<ArrayList<Service>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<Service> result) {
					// TODO Auto-generated method stub
					if(result==null){
						form.hideWaitSymbol();
						form.showDialogMessage("Invalid Excel File Format!");
						return;
					}
					
					for(Service obj:result){
						if(obj.getCount()==-1){
							form.hideWaitSymbol();
							String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
							final String url = gwt + "csvservlet" + "?type=" + 216;
							Window.open(url, "test", "enabled");
							form.showDialogMessage("Please find error file.");
							return;
						}
					}
					
					form.showDialogMessage("Service Cancellation Upload Process Started!");

					form.hideWaitSymbol();
				}
			});
		}
		
		public void reactOnUpdateFindings() {
			Console.log("in reactOnUpdateFindings");
			Company c = new Company();

			updateservice.updateFindingEntity(c.getCompanyId(), new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("Process failed. Check logs");
				}

				@Override
				public void onSuccess(String result) {
					if(result!=null) {
						Console.log("result!=null");
						if(result.equals("Success"))
							form.showDialogMessage("Update Successful");
						else
							form.showDialogMessage("Process failed. Check logs");
					}else
						form.showDialogMessage("Process failed. Check logs");
					
				}
			});
		}
}
