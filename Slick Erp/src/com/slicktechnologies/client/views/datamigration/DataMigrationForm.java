package com.slicktechnologies.client.views.datamigration;


import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class DataMigrationForm extends FormScreen<DummyEntityOnlyForScreen> implements ClickHandler{
	
	/* i have create an entity ie DummyEntityOnlyForScreen for only screen (ie do not save any data in it.)*/
	
	ListBox lstboxMasters,lstboxConfigurations, lstboxTransaction;
	Label lblMasters, lblConfiguration, lblTransaction, lblfilecount, lblsavedrecords;
	UploadComposite mastersUploadComposite,configurationsUploadComposite,transactionUploadComposite;
	Button btnMasterGo, btnConfigurationGo, btnTransactionGo;
	TextBox txtfilecount, txtsavedrecords;
	
	
	public DataMigrationForm() {
		super(FormStyle.DEFAULT);
//		super();
       createGui();
       form.setHeight("300px");
       /** date 26/10/2017 added by komal to update followup date in lead and quotation **/
       if(processLevelBar!=null)
		    processLevelBar.setVisibleFalse(true);

	}

	public DataMigrationForm(String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		/** date 26/10/2017 added by komal to update followup date in lead and quotation **/
	       if(processLevelBar!=null)
			    processLevelBar.setVisibleFalse(true);
	}


	private void initalizeWidget(){
		lstboxMasters = new ListBox();
		lstboxMasters.addItem("--SELECT--");
		lstboxMasters.addItem("Branch");
		lstboxMasters.addItem("City");
		/**
		 * Added Company Asset Master
		 * Date : 07-10-2016 By Anil
		 * Release : 30 Sept 2016 
		 * Project : PURCHASE MODIFICATION (NBHC) 
		 */
		lstboxMasters.addItem("Company Asset");
		lstboxMasters.addItem("Contact Person");
		lstboxMasters.addItem("Country");
		lstboxMasters.addItem("Customer");
		/** date 14/11/2017 added by komal to add customer branch in master dropdown **/
		lstboxMasters.addItem("Customer Branch");
		lstboxMasters.addItem("Department");
		lstboxMasters.addItem("Employee");
		lstboxMasters.addItem("Item Product");
		lstboxMasters.addItem("Locality");
		//lstboxMasters.addItem("Product Inventory");
		lstboxMasters.addItem("Service Product");
		lstboxMasters.addItem("State");
//		lstboxMasters.addItem("Stock Update");
		lstboxMasters.addItem("Stock Upload");
		lstboxMasters.addItem("Storage Bin");
		lstboxMasters.addItem("Storage Location");
		lstboxMasters.addItem("Vendor");
		lstboxMasters.addItem("Warehouse");
		
		
		/**
		 * Added Company Asset Master
		 * Date : 18-10-2017 By Rahul
		 * Project : Employee Salary Updation (NBHC) 
		 */	

		lstboxMasters.addItem("Update Employee");
		
		/**added by Manisha for contract upload*/
		lstboxMasters.addItem("Update Contract");
		
		/**
		 * Date : 24-12-2018 By ANIL
		 */
		lstboxMasters.addItem("Update Leave Balance");	
		
		/**
		 * @author Vijay Chougule Date :- 16-07-2020
		 * Des :- Vendor Product Price Upload
		 */
		lstboxMasters.addItem("Vendor Product Price");
		
		/**
		 * @author Vijay Chougule Date :- 29-07-2020
		 * Des :- CTC Template Upload
		 */
		lstboxMasters.addItem("CTC Template");
		
		/**@author Sheetal : 02-06-2022
		 * Des : Upload program for customer branch with service location and area**/
		lstboxMasters.addItem("Customer Branch with Service Loacation and area Upload");

		
		//***********************rahul********************* 

		lstboxConfigurations = new ListBox();
		lstboxConfigurations.addItem("--SELECT--");
		lstboxConfigurations.addItem("Category");
		lstboxConfigurations.addItem("Configurations");
		lstboxConfigurations.addItem("Type");
		
		
		/**
		 *  nidhi
		 *  5-1-2018
		 *  for upload cron job configration
		 */
		lstboxConfigurations.addItem("Cron Job Configrations");
		/**
		 * end
		 */
		
		
		//*************************************************
		
//		lstboxConfigurations = new ListBox();
//		lstboxConfigurations.addItem("--SELECT--");
		lstboxTransaction = new ListBox();
		lstboxTransaction.addItem("--SELECT--");
		lstboxTransaction.addItem("Contract");
		lstboxTransaction.addItem("Service");
		lstboxTransaction.addItem("Payment");
		lstboxTransaction.addItem("Lead");
		lstboxTransaction.addItem("Customer With Service Details");
		/**
		 *  nidhi
		 *  27-10-2017
		 *   for NBHC 
		 *   MIN Upload process	
		 **/
		lstboxTransaction.addItem(AppConstants.MINUPLOAD);
		/**
		 * end
		 */
		/*
		 *  nidhi
		 *  2-11-2017
		 *   for NBHC 
		 *   Contract Upload process	 */
		lstboxTransaction.addItem(AppConstants.CONTRACTUPLOAD);
		/**
		 * end
		 */
		/**
		 * @author Anil , Date : 19-07-2019
		 */
		lstboxTransaction.addItem("Employee Asset Allocation");
		
		/**
		 * @author Vijay Chougule
		 * Des :- To update asset Details in Service Requirement raised by Rahul for PTSPL
		 */
		lstboxTransaction.addItem(AppConstants.UPDATEASSETDETAILS);
		
		lstboxTransaction.addItem(AppConstants.CTCALOCATIONUPLOAD);
		
		lstboxTransaction.addItem(AppConstants.CREDITNOTE);

		lstboxTransaction.addItem("Upload Inhouse Services");
		
		/**@Sheetal : 31-01-2022
		 * Des : User Creation process **/
		lstboxTransaction.addItem("User Creation");
		
		lstboxTransaction.addItem(AppConstants.SERVICECREATIONUPLOAD);
		lstboxTransaction.addItem("Service Cancellation Upload");
		
		lblMasters = new Label();
		lblMasters.setText("MASTERS");
		lblConfiguration = new Label();
		lblConfiguration.setText("CONFIGURATIONS");
		lblTransaction = new Label();
		lblTransaction.setText("TRANSACTIONS");
		
		lblfilecount = new Label();
		lblfilecount.setText("Total Records found in file:");
		
		lblsavedrecords = new Label();
		lblsavedrecords.setText("Total Records Inserted in database");
		
		
		mastersUploadComposite = new UploadComposite();
		configurationsUploadComposite = new UploadComposite();
		transactionUploadComposite = new UploadComposite();
		
		btnMasterGo = new Button("Go");
		btnConfigurationGo = new Button("Go");
		btnTransactionGo = new Button("Go");
		
		txtfilecount = new TextBox();
		txtfilecount.setEnabled(false);
		txtsavedrecords = new TextBox();
		txtsavedrecords.setEnabled(false);
	}
	@Override
	public void createScreen() {

		initalizeWidget();
		
		
		/** date 26/10/2017 added by komal to update followup date for previous records **/
//		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("DataMigration", "UpdateButton")) {
//			this.processlevelBarNames = new String[] {"Update","Update Payment","Delete Attendance","Delete Payroll","Update Branch In Payslip","Update PT","Update Advance","Update Attendance and Payroll","Update Project","Update Billing","Update GRN",
//					"Update Warehouse Master","Update Storage Location Bin","Update Contract Discontinue" , "Update Service"};
//		} 
		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("DataMigration", "UpdateButton")) {
			this.processlevelBarNames = new String[] {"Update","Update Payment","Delete Attendance","Delete Payroll","Update PT","Update Advance","Update Attendance and Payroll","Update Project","Update Billing","Update GRN",
					"Update Warehouse Master","Update Storage Location Bin","Update Contract Discontinue" , "Update Service","Update RateContract Services","Update Payslip","UpdateInvoices","UpdateCNC","Update Customer Email","UpdateSerialNumber","Download Contracts","Update Frquency In Services",
					"Delete And Recreate Accounting Interface Entry","Delete And Recreate Accounting Interface Payment Entry","Update Employee Id in User","Update Customer Branch Service Address","Update Finding Entity For Completed Services"};
		}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("DataMigration", "UpdateService")){
			this.processlevelBarNames = new String[]{"Update Service","Delete Cancelled Services","View Heap Memory","Release Heap Memory","Update ServicesSStatus to Open","Update Services WMSFlag","Download Contracts","Update Frquency In Services"};
		}
		else{
			this.processlevelBarNames = new String[]{"Download Contracts"};

		}
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDataMigration=fbuilder.setlabel("Data Migration").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",lblMasters);
		FormField flblmasters = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",lstboxMasters);
		FormField flstboxMasters= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",mastersUploadComposite);
		FormField fmastersUploadComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnMasterGo);
		FormField fbtnMasterGo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",lblConfiguration);
		FormField flblConfiguration = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",lstboxConfigurations);
		FormField flstboxConfigurations= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",configurationsUploadComposite);
		FormField fconfigurationsUploadComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnConfigurationGo);
		FormField fbtnConfigurationGo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",lblTransaction);
		FormField flblTransaction = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",lstboxTransaction);
		FormField flstboxTransaction= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",transactionUploadComposite);
		FormField ftransactionUploadComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnTransactionGo);
		FormField fbtnTransactionGo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingDataMigrationStatus=fbuilder.setlabel("Data Migration Status").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("",lblfilecount);
		FormField flblfilecount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",lblsavedrecords);
		FormField flblsavedrecords= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",txtfilecount);
		FormField ftxtfilecount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",txtsavedrecords);
		FormField ftxtsavedrecords= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		
		FormField[][] formfield = { {fgroupingDataMigration,},
				  {flblmasters,flstboxMasters,fmastersUploadComposite,fbtnMasterGo},
				  {flblConfiguration,flstboxConfigurations,fconfigurationsUploadComposite,fbtnConfigurationGo},
				  {flblTransaction,flstboxTransaction,ftransactionUploadComposite,fbtnTransactionGo},
				  {fgroupingDataMigrationStatus},
				  {flblfilecount,ftxtfilecount},
				  {flblsavedrecords,ftxtsavedrecords}
				  
				  };

				this.fields=formfield;		
		
	}


//	@Override
	public void toggleAppHeaderBarMenu() {
		// TODO Auto-generated method stub
		
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		
		AuthorizationHelper.setAsPerAuthorization(Screen.DATAMIGRATION,LoginPresenter.currentModule.trim());
	
		
	}

	@Override
	public void setToNewState() {
		if(processLevelBar!=null)
		    processLevelBar.setVisibleFalse(true);
	}

	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void updateView(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
		
	}


}
