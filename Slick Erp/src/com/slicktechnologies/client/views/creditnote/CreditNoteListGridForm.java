package com.slicktechnologies.client.views.creditnote;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.CreditNote;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.AutoFitWidthApproach;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class CreditNoteListGridForm extends ViewContainer{




	ListGrid grid;
	DataSource ds;
	
	ObjectListBox<Branch> olbbranch=new ObjectListBox<Branch>();
	DateBox dbFromDate;
	DateBox dbToDate;
	Button btnGo;
	
	FlexForm form;
	
	
	HorizontalPanel horizontalpanel;
	VerticalPanel verticalpanel;
	
	public CreditNoteListGridForm() {
		super();
		createGui();
	}
	
	@Override
	protected void createGui() {
		System.out.println("CREATE GUI");
		toggleAppHeaderBarMenu();
		
		olbbranch =new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbranch);
		olbbranch.getElement().getStyle().setHeight(26, Unit.PX);
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		dbFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		dbToDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		btnGo=new Button("GO");
		
		
		horizontalpanel = new HorizontalPanel();
		
		
		VerticalPanel vpanel1=new VerticalPanel();
		VerticalPanel vpanel2=new VerticalPanel();
		VerticalPanel vpanel3=new VerticalPanel();
		VerticalPanel vpanel4=new VerticalPanel();
		VerticalPanel vpanel5=new VerticalPanel();
		
		InlineLabel lablel1=new InlineLabel("Branch");
		InlineLabel lablel3=new InlineLabel("From Date");
		InlineLabel lablel4=new InlineLabel("To Date");
		InlineLabel lablel5=new InlineLabel(". ");
		
		vpanel1.add(lablel1);
		vpanel1.add(olbbranch);
		
		vpanel3.add(lablel3);
		vpanel3.add(dbFromDate);
		
		vpanel4.add(lablel4);
		vpanel4.add(dbToDate);
		
		vpanel5.add(lablel5);
		vpanel5.add(btnGo);
		
		horizontalpanel.add(vpanel1);
		horizontalpanel.add(vpanel2);
		horizontalpanel.add(vpanel3);
		horizontalpanel.add(vpanel4);
		horizontalpanel.add(vpanel5);

		horizontalpanel.setSpacing(10);
		horizontalpanel.getElement().addClassName("technicianscheduleButtons");
		
		
		verticalpanel = new VerticalPanel();
				
		
		grid = new ListGrid();
		grid.setStyleName("GridList");
		setGridProperty(grid);
		grid.setFields(getGridFields());
		
		ds= new DataSource();
		ds.setClientOnly(true);
		setDataSourceField(ds);
		grid.setDataSource(ds);
		
		verticalpanel.getElement().addClassName("varticalGridPanel");
		grid.getElement().addClassName("technicianSchedule");
		verticalpanel.add(grid);
		
		content.add(horizontalpanel);
		content.add(verticalpanel);
		
		
	}
	
	public void setDataToListGrid(ArrayList<CreditNote> list){
		System.out.println("FORM SETTING DATA....");
		
			ds.setTestData(getGridData(list));
			grid.setDataSource(ds);
			grid.setFields(getGridFields());
			grid.fetchData();
		
		
	}

	
	
	//grid property
	private static void setGridProperty(ListGrid grid) {
	   grid.setWidth("83%");
	   grid.setHeight("70%");
	   grid.setAutoFetchData(true);
	   grid.setAlternateRecordStyles(true);
	   grid.setShowAllRecords(true);
	   grid.setShowFilterEditor(true);
	   grid.setFilterOnKeypress(true);
	   
	   grid.setWrapCells(true);	
	   grid.setFixedRecordHeights(false);
	   grid.setAutoFitData(Autofit.HORIZONTAL);	
	   grid.setAutoFitWidthApproach(AutoFitWidthApproach.BOTH);	
	   grid.setCanResizeFields(true);
	   
	   /**
	    * @author Anil
	    * @since 31-12-2020
	    * for resizing smartgrid
	    */
	   if(AppMemory.getAppMemory().enableMenuBar){
		   grid.setWidth("100%");
	   }
	}
	
	//fields
	private static ListGridField[] getGridFields() {
		 
		
	   ListGridField field1 = new ListGridField("Id","ID");
	   field1.setWrap(true);
	   field1.setAlign(Alignment.CENTER);
	   
	   
	   ListGridField field2 = new ListGridField("CreditNoteNo","Credit Note No");
	   field2.setWrap(true);
	   field2.setAlign(Alignment.CENTER);
	   
	   ListGridField field3 = new ListGridField("creditnotedate","Credit Note Date");
	   field3.setWrap(true);
	   field3.setAlign(Alignment.CENTER);
	  
	   
	   ListGridField field4 = new ListGridField("InvoiceId","Invoice Id");
	   field4.setWrap(true);
	   field4.setAlign(Alignment.CENTER);
	   
	   ListGridField field5 = new ListGridField("branch","Branch");
	   field5.setWrap(true);
	   field5.setAlign(Alignment.CENTER);
	   
	   ListGridField field6 = new ListGridField("CreditNoteAmt","Credit Note Amount");
	   field6.setWrap(true);
	   field6.setAlign(Alignment.CENTER);
	   
	   ListGridField field7 = new ListGridField("BaseAmount","Base Amount");
	   field7.setWrap(true);
	   field7.setAlign(Alignment.CENTER);
	   
	   ListGridField field8 = new ListGridField("cgst","CGST");
	   field8.setWrap(true);
	   field8.setAlign(Alignment.CENTER);
	   
	   ListGridField field9 = new ListGridField("sgst","SGST");
	   field9.setWrap(true);
	   field9.setAlign(Alignment.CENTER);
	   
	   ListGridField field10 = new ListGridField("igst","IGST");
	   field10.setWrap(true);
	   field10.setAlign(Alignment.CENTER);
	   
	   ListGridField field11 = new ListGridField("RoundOffAmt","Round Off Amount"); 
	   field11.setWrap(true);
	   field11.setAlign(Alignment.CENTER);
	   
	   ListGridField field12 = new ListGridField("remark","Remark");
	   field12.setWrap(true);
	   field12.setAlign(Alignment.CENTER);
	   
	  
	   
	   
	   
	   return new ListGridField[] { 
			   field1, field2 ,field3,field4,field5,field6,field7,field8,field9,field10,
			   field11,field12,
			   };
	}
	
	//data source field
	protected static void setDataSourceField(DataSource dataSource) {
		DataSourceIntegerField pkField = new DataSourceIntegerField("pk");  
        pkField.setHidden(true);  
        pkField.setPrimaryKey(true);
		
	   DataSourceField field1 = new DataSourceField("Id", FieldType.TEXT);
	   DataSourceField field2 = new DataSourceField("CreditNoteNo", FieldType.TEXT);
	   
	   DataSourceDateField field3 = new DataSourceDateField("creditnotedate");
	   
	   DataSourceField field4 = new DataSourceField("InvoiceId", FieldType.TEXT);
	   DataSourceField field5 = new DataSourceField("branch", FieldType.TEXT);
	   DataSourceField field6 = new DataSourceField("CreditNoteAmt", FieldType.TEXT);
	   DataSourceField field7 = new DataSourceField("BaseAmount", FieldType.TEXT);
	   DataSourceField field8 = new DataSourceField("cgst", FieldType.TEXT);
	   DataSourceField field9 = new DataSourceField("sgst", FieldType.TEXT);
	   DataSourceField field10 = new DataSourceField("igst", FieldType.TEXT);
	   DataSourceField field11 = new DataSourceField("RoundOffAmt", FieldType.TEXT);
	   DataSourceField field12 = new DataSourceField("remark", FieldType.TEXT);
	  
	   
	   dataSource.setFields(pkField,field1,field2,field3,field4,field5,field6,field7,field8,field9,field10,field11,field12);
	}
	
	
	 private static ListGridRecord[] getGridData(ArrayList<CreditNote> creditNotelist) {
		 ListGridRecord[] records = null;
		 records = new ListGridRecord[creditNotelist.size()];
		 for(int i=0;i<creditNotelist.size();i++){
			 ListGridRecord record = new ListGridRecord();
			 CreditNote creditnoteEntity=creditNotelist.get(i);
			 
			 record.setAttribute("Id", creditnoteEntity.getCount());
			 record.setAttribute("CreditNoteNo", creditnoteEntity.getTallyCreditNoteId());
			 if(creditnoteEntity.getCreditNoteDate()!=null){
				 record.setAttribute("creditnotedate", creditnoteEntity.getCreditNoteDate());
			 }else{
				 record.setAttribute("creditnotedate", "");
			 }
			 record.setAttribute("InvoiceId", creditnoteEntity.getInvoiceId());
			 if(creditnoteEntity.getBranch()!=null){
				 record.setAttribute("branch", creditnoteEntity.getBranch());
			 }
			 else{
				 record.setAttribute("branch", "");
			 }
			 record.setAttribute("CreditNoteAmt",creditnoteEntity.getCreditNoteAmt());
			 record.setAttribute("BaseAmount", creditnoteEntity.getBaseAmount());
			 record.setAttribute("cgst", creditnoteEntity.getCgstAmt());
			 record.setAttribute("sgst", creditnoteEntity.getSgstAmt());
			 record.setAttribute("igst", creditnoteEntity.getIgstAmt());
			 record.setAttribute("RoundOffAmt", creditnoteEntity.getRoundOffAmt());
			 if(creditnoteEntity.getRemark()!=null){
				 record.setAttribute("remark", creditnoteEntity.getRemark());
			 }
			 else{
				 record.setAttribute("remark", "");
			 }
			 records[i]=record;

		 }
		 System.out.println("RECORDS LENGTH : "+records.length);
		 
		 return records;
	}
	 
	 /**
		 * Toggles the app header bar menus as per screen state
		 */
		public void toggleAppHeaderBarMenu() {
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		  		
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download"))
						menus[k].setVisible(true); 
					else
						menus[k].setVisible(false);  		   
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download"))
						menus[k].setVisible(true); 
					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.CREDITNOTE,LoginPresenter.currentModule.trim());
		}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	public void showMessage(String msg){
		Window alert=new Window();
		
		alert.setWidth("25%");
        alert.setHeight("20%"); 
        alert.setTitle("Alert");
        alert.setShowMinimizeButton(false);
        alert.setIsModal(true);
        alert.setShowModalMask(true);
        alert.centerInPage();
        alert.setMargin(4);
        
//		InlineLabel msgLbl=new InlineLabel(msg);
//		alert.addItem(msgLbl);
		
//		window.setWidth(200);
//		window.setHeight(200);
//		window.setCanDragResize(true);
//		window.setShowFooter(true);

		Label label = new Label();
		label.setContents(msg);
		label.setAlign(Alignment.CENTER);
		label.setPadding(5);
		label.setHeight100();
		
		alert.addItem(label);
		
		
		alert.show();
	}
	
	
	

	public ListGrid getGrid() {
		return grid;
	}

	public void setGrid(ListGrid grid) {
		this.grid = grid;
	}

	public DataSource getDs() {
		return ds;
	}

	public void setDs(DataSource ds) {
		this.ds = ds;
	}

	public ObjectListBox<Branch> getOlbbranch() {
		return olbbranch;
	}

	public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
		this.olbbranch = olbbranch;
	}


	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}

	public Button getBtnGo() {
		return btnGo;
	}

	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}

	public FlexForm getForm() {
		return form;
	}

	public void setForm(FlexForm form) {
		this.form = form;
	}

	public HorizontalPanel getHorizontalpanel() {
		return horizontalpanel;
	}

	public void setHorizontalpanel(HorizontalPanel horizontalpanel) {
		this.horizontalpanel = horizontalpanel;
	}

	public VerticalPanel getVerticalpanel() {
		return verticalpanel;
	}

	public void setVerticalpanel(VerticalPanel verticalpanel) {
		this.verticalpanel = verticalpanel;
	}
	



}
