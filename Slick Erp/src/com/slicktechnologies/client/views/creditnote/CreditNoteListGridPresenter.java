package com.slicktechnologies.client.views.creditnote;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.dashboard.leaddashboard.LeadDashboardSearchForm;
import com.slicktechnologies.shared.CreditNote;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;

public class CreditNoteListGridPresenter implements ClickHandler, CellClickHandler{
	


	CreditNoteListGridForm form;
	GenricServiceAsync service=GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	ArrayList<CreditNote> globalCreditNoteList=new ArrayList<CreditNote>();

	ListGridRecord selectedRecord=null;
	int position;
	
	protected AppMemory mem;
	LeadDashboardSearchForm searchForm;

	public CreditNoteListGridPresenter(CreditNoteListGridForm homeForm,LeadDashboardSearchForm searchForm2) {
		this.form=homeForm;
		this.searchForm=searchForm2;
		form.grid.addCellClickHandler(this);
		mem=AppMemory.getAppMemory();
		setEventHandeling();
		retrieveData();
		
		
	}
	
	private void setEventHandeling() {
		form.getBtnGo().addClickHandler(this);
		searchForm.golbl.addClickHandler(this);

		InlineLabel label[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
		for (int k = 0; k < label.length; k++) {
			if (AppMemory.getAppMemory().skeleton.registration[k] != null)
				AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
		}
		for (int k = 0; k < label.length; k++) {
			AppMemory.getAppMemory().skeleton.registration[k] = label[k].addClickHandler(this);
		}
	}

	public static void initialize(){
		System.out.println("INSIDE CHART INITIALIZATION");
		CreditNoteListGridForm homeForm = new CreditNoteListGridForm();
		AppMemory.getAppMemory().currentScreen=Screen.CREDITNOTE;
		AppMemory.getAppMemory().skeleton.getProcessName().setText(AppConstants.ACCOUNTMODULE+"/"+"Credit Note List");
		
		try {
			LeadDashboardSearchForm searchForm=new LeadDashboardSearchForm();
			CreditNoteListGridPresenter presenter= new CreditNoteListGridPresenter(homeForm,searchForm);
				
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		AppMemory.getAppMemory().stickPnel(homeForm);	
	}
	
	public void retrieveData(){
		Date date=new Date();
		CalendarUtil.setToFirstDayOfMonth(date);
		Console.log("start Date ::::::::: "+date);
		
		Date todate=new Date();
		CalendarUtil.setToFirstDayOfMonth(todate);
		CalendarUtil.addMonthsToDate(todate, 1);
		CalendarUtil.addDaysToDate(todate, -1);
		Console.log("todate Date ::::::::: "+todate);

		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		filtervec=new Vector<Filter>();

		List<String> branchList ;
		branchList= new ArrayList<String>();
		for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
			branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
		}
	
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("creditNoteDate >=");
		filter.setDateValue(date);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("creditNoteDate <=");
		filter.setDateValue(todate);
		filtervec.add(filter);
		
		querry=new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CreditNote());
		this.retriveTable(querry);
		
	}
	
	
	public void retriveTable(MyQuerry querry){
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				globalCreditNoteList=new ArrayList<>();
				
				if(result.size()==0){
					SC.say("No result found.");
				}
				for(SuperModel model:result){
					CreditNote creditnoteEntity =(CreditNote) model;
					globalCreditNoteList.add(creditnoteEntity);
				}
				form.setDataToListGrid(globalCreditNoteList);
			}
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		}); 
	 }
	

	@Override
	public void onClick(ClickEvent event) {
		System.out.println("CLICK EVENT..!!!");
		if(event.getSource() instanceof InlineLabel){
			InlineLabel lbl= (InlineLabel) event.getSource();
			if(lbl.getText().equals("Download")){
				reactOnDownload();
			}
			
		}
		
		if (event.getSource().equals(form.getBtnGo())) {
			System.out.println("BTN GO CLICK");
			if(LoginPresenter.branchRestrictionFlag==true&&form.getOlbbranch().getSelectedIndex()==0){
				SC.say("Please select branch.");
				return;
			}
			System.out.println("FRM DT "+form.getDbFromDate().getValue());
			if (form.getOlbbranch().getSelectedIndex() == 0
					&& form.getDbFromDate().getValue()==null
					&& form.getDbToDate().getValue()==null) {		
				SC.say("Please select atleast one filter.");
				return;
			}
			else{
				searchByFromToDate();
			}
		}
		
		
		
		
	}
	
	
	
	private void searchByFromToDate() {
		System.out.println("FROM AND DATE ---");
		Long compId = UserConfiguration.getCompanyId();
		Vector<Filter> filtervec = null;
		Filter filter = null;
		MyQuerry querry;

		filtervec = new Vector<Filter>();

		
		if (form.getDbFromDate().getValue() != null) {
			filter = new Filter();
			filter.setQuerryString("creditNoteDate >=");
			filter.setDateValue(form.getDbFromDate().getValue());
			filtervec.add(filter);
		}

		if (form.getDbToDate().getValue() != null) {

			filter = new Filter();
			filter.setQuerryString("creditNoteDate <=");
			filter.setDateValue(form.getDbToDate().getValue());
			filtervec.add(filter);
		}

		if (form.getOlbbranch().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);

		}

		querry = new MyQuerry();
		querry.setQuerryObject(new CreditNote());
		querry.setFilters(filtervec);
		this.retriveTable(querry);

	}
	
	
	public void reactOnGo() {


	}

	private void reactOnDownload() {
		ArrayList<CreditNote> creditnotelist = new ArrayList<CreditNote>();
		ListGridRecord[] allRecords = form.grid.getRecords();
        if (allRecords != null) {
        	List<ListGridRecord> allRecordsList = Arrays.asList(allRecords);
	   		for(int i=0;i<allRecordsList.size();i++){
	   			 final CreditNote model=getCreditNoteDetails(Integer.parseInt(allRecordsList.get(i).getAttribute("Id").trim()));
	   			 if(model!=null){
	   				creditnotelist.add(model);
	   			 }
	   		}
        }
		csvservice.setCreditNotelist(creditnotelist, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 200;
				Window.open(url, "test", "enabled");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
//		csvservice.setComplainlist(complainList, new AsyncCallback<Void>(){
//			@Override
//			public void onFailure(Throwable caught) {
//				System.out.println("RPC call Failed"+caught);
//			}
//			
//			@Override
//			public void onSuccess(Void result) {
//				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
//				
//					final String url = gwt + "csvservlet" + "?type=" + 95;
//					Window.open(url, "test", "enabled");
//			}
//		});
		
		 
	}


	private CreditNote getCreditNoteDetails(int creditnoteId) {
		for(CreditNote creditnoteEntity : globalCreditNoteList){
			if(creditnoteEntity.getCount()== creditnoteId){
				return creditnoteEntity;
			}
		}
		return null;
	}

	@Override
	public void onCellClick(CellClickEvent event) {
		
		
	}
	


}
