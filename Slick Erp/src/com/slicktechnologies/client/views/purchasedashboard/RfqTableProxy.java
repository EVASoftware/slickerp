package com.slicktechnologies.client.views.purchasedashboard;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;

public class RfqTableProxy extends SuperTable<RequsestForQuotation>{TextColumn<RequsestForQuotation> Columncount;
TextColumn<RequsestForQuotation> ColumnPRTitle;
TextColumn<RequsestForQuotation> ColumnCreationDate;


TextColumn<RequsestForQuotation> ColumnPRCategory;
TextColumn<RequsestForQuotation> ColumnPRType;
TextColumn<RequsestForQuotation> ColumnPRGrpup;


TextColumn<RequsestForQuotation> ColumnBranch;
TextColumn<RequsestForQuotation> ColumnPurchaseEngg;
TextColumn<RequsestForQuotation> ColumnAprroverName;

TextColumn<RequsestForQuotation> ColumnExptectedDeliveryDate;
TextColumn<RequsestForQuotation> ColumnClosureDate;
TextColumn<RequsestForQuotation> ColumnStatus;


@Override
public void createTable() {
	createColumnCount();
	createColumnPRTitle();
	createColumnCreationDate();
	
	
	createColumnPRCategory();
	createColumnPRType();

	createColumnBranch();
	createColumnPurchaseEngg();
	createColumnPRApproverName();
	
	createColumnExpectedDelDate();
	createColumnClosureDate();
	createColumnStatus();
	
}

public void addColumnSorting() {
	createColumnSortingCount();
	createColumnSortingPRTitle();
	createColumnSortingExpectedDelDate();

	createColumnSortingPRCategory();
	createColumnSortingPRType();
	
	createColumnSortingBranch();
	createColumnSortingPurchaseEngg();
	createColumnSortingPRApproverName();
	
	createColumnSortingCreationDate();
	createColumnSortingClosureDate();
	createColumnSortingStatus();
}



/**************************** sorting columns *****************************************/


public void createColumnSortingPRApproverName() {
	List<RequsestForQuotation> list = getDataprovider().getList();
	columnSort = new ListHandler<RequsestForQuotation>(list);
	columnSort.setComparator(ColumnAprroverName,new Comparator<RequsestForQuotation>() {
				@Override
				public int compare(RequsestForQuotation e1,RequsestForQuotation e2) {
					if (e1 != null && e2 != null) {if (e1.getApproverName() != null&& e2.getApproverName() != null) {
							return e1.getApproverName().compareTo(e2.getApproverName());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}

public void createColumnSortingBranch() {
	List<RequsestForQuotation> list = getDataprovider().getList();
	columnSort = new ListHandler<RequsestForQuotation>(list);
	columnSort.setComparator(ColumnBranch,
			new Comparator<RequsestForQuotation>() {
				@Override
				public int compare(RequsestForQuotation e1,
						RequsestForQuotation e2) {
					if (e1 != null && e2 != null) {
						if (e1.getBranch() != null
								&& e2.getBranch() != null) {
							return e1.getBranch().compareTo(e2.getBranch());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}


public void createColumnSortingCount() {
	List<RequsestForQuotation> list = getDataprovider().getList();
	columnSort = new ListHandler<RequsestForQuotation>(list);
	columnSort.setComparator(Columncount,new Comparator<RequsestForQuotation>() {
				@Override
				public int compare(RequsestForQuotation e1,
						RequsestForQuotation e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCount() == e2.getCount()) {
							return 0;
						}
						if (e1.getCount() > e2.getCount()) {
							return 1;
						} else {
							return -1;
						}
					} else {
						return 0;
					}
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void createColumnSortingPRTitle() {
	List<RequsestForQuotation> list = getDataprovider().getList();
	columnSort = new ListHandler<RequsestForQuotation>(list);
	columnSort.setComparator(ColumnPRTitle,
			new Comparator<RequsestForQuotation>() {
				@Override
				public int compare(RequsestForQuotation e1,
						RequsestForQuotation e2) {
					if (e1 != null && e2 != null) {
						if (e1.getQuotationName() != null
								&& e2.getQuotationName() != null) {
							return e1.getQuotationName().compareTo(
									e2.getQuotationName());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void createColumnSortingCreationDate() {
	List<RequsestForQuotation> list = getDataprovider().getList();
	columnSort = new ListHandler<RequsestForQuotation>(list);
	columnSort.setComparator(ColumnCreationDate,
			new Comparator<RequsestForQuotation>() {
				@Override
				public int compare(RequsestForQuotation e1,
						RequsestForQuotation e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCreationDate() != null
								&& e2.getCreationDate() != null) {
							return e1.getCreationDate().compareTo(
									e2.getCreationDate());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}

public void createColumnSortingClosureDate() {
	List<RequsestForQuotation> list = getDataprovider().getList();
	columnSort = new ListHandler<RequsestForQuotation>(list);
	columnSort.setComparator(ColumnClosureDate,new Comparator<RequsestForQuotation>() {
		@Override
		public int compare(RequsestForQuotation e1,RequsestForQuotation e2) {
			if (e1 != null && e2 != null) {
				if (e1.getExpectedClosureDate() != null&& e2.getExpectedClosureDate() != null) {
					return e1.getExpectedClosureDate().compareTo(e2.getExpectedClosureDate());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}


public void createColumnSortingExpectedDelDate() {
	List<RequsestForQuotation> list = getDataprovider().getList();
	columnSort = new ListHandler<RequsestForQuotation>(list);
	columnSort.setComparator(ColumnExptectedDeliveryDate,
			new Comparator<RequsestForQuotation>() {
				@Override
				public int compare(RequsestForQuotation e1,
						RequsestForQuotation e2) {
					if (e1 != null && e2 != null) {
						if (e1.getDeliveryDate() != null
								&& e2.getDeliveryDate() != null) {
							return e1.getDeliveryDate().compareTo(
									e2.getDeliveryDate());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void createColumnSortingPurchaseEngg() {
	List<RequsestForQuotation> list = getDataprovider().getList();
	columnSort = new ListHandler<RequsestForQuotation>(list);
	columnSort.setComparator(ColumnPurchaseEngg,
			new Comparator<RequsestForQuotation>() {
				@Override
				public int compare(RequsestForQuotation e1,
						RequsestForQuotation e2) {
					if (e1 != null && e2 != null) {
						if (e1.getEmployee() != null
								&& e2.getEmployee() != null) {
							return e1.getEmployee().compareTo(
									e2.getEmployee());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void createColumnSortingPRType() {
	List<RequsestForQuotation> list = getDataprovider().getList();
	columnSort = new ListHandler<RequsestForQuotation>(list);
	columnSort.setComparator(ColumnPRType,
			new Comparator<RequsestForQuotation>() {
				@Override
				public int compare(RequsestForQuotation e1,
						RequsestForQuotation e2) {
					if (e1 != null && e2 != null) {
						if (e1.getQuotationType() != null
								&& e2.getQuotationType() != null) {
							return e1.getQuotationType().compareTo(e2.getQuotationType());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

//public void createColumnSortingPRGroup() {
//	List<RequsestForQuotation> list = getDataprovider().getList();
//	columnSort = new ListHandler<RequsestForQuotation>(list);
//	columnSort.setComparator(ColumnPRGrpup,
//			new Comparator<RequsestForQuotation>() {
//				@Override
//				public int compare(RequsestForQuotation e1,RequsestForQuotation e2) {
//					if (e1 != null && e2 != null) {
//						if (e1.get != null
//								&& e2.getPrGroup() != null) {
//							return e1.getPrGroup().compareTo(
//									e2.getPrGroup());
//						}
//					} else {
//						return 0;
//					}
//					return 0;
//				}
//			});
//	table.addColumnSortHandler(columnSort);
//}

public void createColumnSortingPRCategory() {
	List<RequsestForQuotation> list = getDataprovider().getList();
	columnSort = new ListHandler<RequsestForQuotation>(list);
	columnSort.setComparator(ColumnPRCategory,
			new Comparator<RequsestForQuotation>() {
				@Override
				public int compare(RequsestForQuotation e1,
						RequsestForQuotation e2) {
					if (e1 != null && e2 != null) {
						if (e1.getQuotationCategory() != null
								&& e2.getQuotationCategory() != null) {
							return e1.getQuotationCategory().compareTo(
									e2.getQuotationCategory());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void createColumnSortingStatus() {
	List<RequsestForQuotation> list = getDataprovider().getList();
	columnSort = new ListHandler<RequsestForQuotation>(list);
	columnSort.setComparator(ColumnStatus,
			new Comparator<RequsestForQuotation>() {
				@Override
				public int compare(RequsestForQuotation e1,
						RequsestForQuotation e2) {
					if (e1 != null && e2 != null) {
						if (e1.getStatus() != null
								&& e2.getStatus() != null) {
							return e1.getStatus().compareTo(e2.getStatus());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

/***********************************************************************************/

public void createColumnCreationDate() {
	final Date sub7Days=new Date();
	System.out.println("Todays Date ::: "+sub7Days);
	CalendarUtil.addDaysToDate(sub7Days,-7);
	System.out.println("Date - 7 Days ::: "+sub7Days);
	sub7Days.setHours(23);
	sub7Days.setMinutes(59);
	sub7Days.setSeconds(59);
	System.out.println("Converted Date ::: "+sub7Days);
	
	ColumnCreationDate = new TextColumn<RequsestForQuotation>() {

		@Override
		public String getCellStyleNames(Context context, RequsestForQuotation object) {
			if((object.getCreationDate().before(sub7Days))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Approved")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
		
		@Override
		public String getValue(RequsestForQuotation object) {
			if (object.getCreationDate() != null)
				return AppUtility.parseDate(object.getCreationDate());
			else
				return "";
		}
	};
	table.addColumn(ColumnCreationDate, "Date");
	table.setColumnWidth(ColumnCreationDate, 120,Unit.PX);
	ColumnCreationDate.setSortable(true);
}

public void createColumnClosureDate() {
	ColumnClosureDate = new TextColumn<RequsestForQuotation>() {

		@Override
		public String getValue(RequsestForQuotation object) {
			if (object.getExpectedClosureDate() != null)
				return AppUtility.parseDate(object.getExpectedClosureDate());
			else
				return "";
		}
	};
	table.addColumn(ColumnClosureDate, "Closure Date");
	table.setColumnWidth(ColumnClosureDate, 120,Unit.PX);
	ColumnClosureDate.setSortable(true);
}
public void createColumnExpectedDelDate() {
	ColumnExptectedDeliveryDate = new TextColumn<RequsestForQuotation>() {

		@Override
		public String getValue(RequsestForQuotation object) {
			if (object.getDeliveryDate() != null)
				return AppUtility.parseDate(object.getDeliveryDate());
			else
				return "";
		}
	};
	table.addColumn(ColumnExptectedDeliveryDate, "Delivery Date");
	table.setColumnWidth(ColumnExptectedDeliveryDate, 120,Unit.PX);
	ColumnExptectedDeliveryDate.setSortable(true);
}

public void createColumnPurchaseEngg() {
	ColumnPurchaseEngg = new TextColumn<RequsestForQuotation>() {

		@Override
		public String getValue(RequsestForQuotation object) {
			return object.getEmployee();
		}
	};
	table.addColumn(ColumnPurchaseEngg, "Purchase Engineer");
	table.setColumnWidth(ColumnPurchaseEngg, 160,Unit.PX);
	ColumnPurchaseEngg.setSortable(true);
}

public void createColumnBranch() {
	ColumnBranch = new TextColumn<RequsestForQuotation>() {

		@Override
		public String getValue(RequsestForQuotation object) {
			return object.getBranch();
		}
	};
	table.addColumn(ColumnBranch, "Branch");
	table.setColumnWidth(ColumnBranch, 90,Unit.PX);
	ColumnBranch.setSortable(true);
}

public void createColumnPRType() {
	ColumnPRType = new TextColumn<RequsestForQuotation>() {

		@Override
		public String getValue(RequsestForQuotation object) {
			return object.getQuotationType();
		}
	};
	table.addColumn(ColumnPRType, "Type");
	table.setColumnWidth(ColumnPRType, 90,Unit.PX);
	ColumnPRType.setSortable(true);
}

//public void createColumnPRGroup() {
//	ColumnPRGrpup = new TextColumn<RequsestForQuotation>() {
//
//		@Override
//		public String getValue(RequsestForQuotation object) {
//			return object.getPrGroup();
//		}
//	};
//	table.addColumn(ColumnPRGrpup, "Group");
//	table.setColumnWidth(ColumnPRGrpup, 90,Unit.PX);
//	ColumnPRGrpup.setSortable(true);
//}

public void createColumnPRCategory() {
	ColumnPRCategory = new TextColumn<RequsestForQuotation>() {

		@Override
		public String getValue(RequsestForQuotation object) {
			return object.getQuotationCategory();
		}
	};
	table.addColumn(ColumnPRCategory, "Category");
	table.setColumnWidth(ColumnPRCategory, 90,Unit.PX);
	ColumnPRCategory.setSortable(true);
}

public void createColumnStatus() {
	ColumnStatus = new TextColumn<RequsestForQuotation>() {

		@Override
		public String getValue(RequsestForQuotation object) {
			return object.getStatus();
		}
	};
	table.addColumn(ColumnStatus, "Status");
	table.setColumnWidth(ColumnStatus, 90,Unit.PX);
	ColumnStatus.setSortable(true);
}

public void createColumnCount() {
	Columncount = new TextColumn<RequsestForQuotation>() {

		@Override
		public String getValue(RequsestForQuotation object) {
			return object.getCount() + "";
		}
	};
	table.addColumn(Columncount, "ID");
	table.setColumnWidth(Columncount, 90,Unit.PX);
	Columncount.setSortable(true);
}

public void createColumnPRTitle() {
	ColumnPRTitle = new TextColumn<RequsestForQuotation>() {

		@Override
		public String getValue(RequsestForQuotation object) {
			return object.getQuotationName();
		}
	};
	table.addColumn(ColumnPRTitle, "Title");
	table.setColumnWidth(ColumnPRTitle, 90,Unit.PX);
	ColumnPRTitle.setSortable(true);
}

public void createColumnPRApproverName() {
	ColumnAprroverName = new TextColumn<RequsestForQuotation>() {

		@Override
		public String getValue(RequsestForQuotation object) {
			return object.getApproverName();
		}
	};
	table.addColumn(ColumnAprroverName, "Approver");
	table.setColumnWidth(ColumnAprroverName, 90,Unit.PX);
	ColumnAprroverName.setSortable(true);
}




@Override
protected void initializekeyprovider() {

}

@Override
public void addFieldUpdater() {

}

@Override
public void setEnable(boolean state) {

}

@Override
public void applyStyle() {
	table.setAlwaysShowScrollBars(true);

}}
