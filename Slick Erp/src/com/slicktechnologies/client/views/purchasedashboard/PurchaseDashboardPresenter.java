package com.slicktechnologies.client.views.purchasedashboard;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.gargoylesoftware.htmlunit.javascript.host.Console;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
//import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentForm;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentPresenter;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequisitionForm;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequitionPresenter;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationForm;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationPresenter;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;

public class PurchaseDashboardPresenter implements ClickHandler{PurchaseDashboardForm form;
PurchaseDashboardSearch searchpopup;
protected GenricServiceAsync service=GWT.create(GenricService.class);

public  PurchaseDashboardPresenter(PurchaseDashboardForm form,PurchaseDashboardSearch searchpopup)
{
	this.form=form;
	this.searchpopup=searchpopup;
	SetEventHandling();
	searchpopup.applyHandler(this);
	form.getBtnGo().addClickHandler(this);
	
	Date date=new Date();
	System.out.println("Today Date ::::::::: "+date);
	// **** Here We Add 7 Days to current date and pass this date to default search.
	CalendarUtil.addDaysToDate(date,7);
    System.out.println("Changed Date + 7 ::::::::: "+date);
	
    Date todaysDate=new Date();
    System.out.println("Today Date ::::::::: "+todaysDate);
    CalendarUtil.addDaysToDate(todaysDate,-7);
    System.out.println("Changed Date - 7::::::::: "+todaysDate);
    
    Long compId=UserConfiguration.getCompanyId();
    System.out.println("Company Id :: "+compId);
	
    
	Vector<Filter> filtervec=null;
	Filter filter = null;
	MyQuerry querry;
	List<String> statusList;
	
	filtervec=new Vector<Filter>();
	statusList=new ArrayList<String>();
	statusList.add(PurchaseRequisition.CREATED);
	statusList.add(PurchaseRequisition.REQUESTED);
	statusList.add(PurchaseRequisition.APPROVED);
	
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(compId);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("purchasedetails.status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	
	/**
	 * Developed by : Rohan added this branch  
	 */
		List<String> branchList ;
		branchList= new ArrayList<String>();
		System.out.println("item count in branch "+LoginPresenter.globalBranch.size());
		for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
			branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
		}
		System.out.println("branchList list "+branchList.size());
	
		filter = new Filter();
		filter.setQuerryString("purchasedetails.branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
	/**
	 * ends here 
	 */
	
	
	filter = new Filter();
	filter.setQuerryString("purchasedetails.creationDate <=");
	filter.setDateValue(date);
	filtervec.add(filter);
	
	querry=new MyQuerry();
	querry.setFilters(filtervec);
	querry.setQuerryObject(new PurchaseRequisition());
	this.retriveTable(querry,form.getBillingTable());
	
	
	
	
	////////////////////////////////////Create Querry For RFQ///////////////////////////// 
	
	filtervec=new Vector<Filter>();
	statusList=new ArrayList<String>();
	statusList.add(RequsestForQuotation.CREATED);
	statusList.add(RequsestForQuotation.REQUESTED);
	statusList.add(RequsestForQuotation.APPROVED);
	
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(compId);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	
	/**
	 * Developed by : Rohan added this branch  
	 */
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
	
	/**
	 * ends here 
	 */
	
	filter = new Filter();
	filter.setQuerryString("creationDate <=");
	filter.setDateValue(date);
	filtervec.add(filter);
	
	querry=new MyQuerry();
	querry.setQuerryObject(new RequsestForQuotation());
	querry.setFilters(filtervec);
	retriveTable(querry,form.getInvoiceTable());
	
	///////////Create Querry For LOI/////////////////////////////////////////////////////
	
	filtervec=new Vector<Filter>();
	statusList=new ArrayList<String>();
	statusList.add(LetterOfIntent.CREATED);
	statusList.add(LetterOfIntent.REQUESTED);
	statusList.add(LetterOfIntent.APPROVED);
	
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(compId);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	
	/**
	 * Developed by : Rohan added this branch  
	 */
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
	
	/**
	 * ends here 
	 */
	
	filter = new Filter();
	filter.setQuerryString("LoiDate <=");
	filter.setDateValue(date);
	filtervec.add(filter);
	
	querry=new MyQuerry();
	querry.setQuerryObject(new LetterOfIntent());
	querry.setFilters(filtervec);
	retriveTable(querry,form.getPaymentTable());
	
	////////////////////Create Querry For PO//////////////////////
	
	filtervec=new Vector<Filter>();
	statusList=new ArrayList<String>();
	statusList.add(PurchaseOrder.CREATED);
	statusList.add(PurchaseOrder.REQUESTED);
	
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(compId);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	
	/**
	 * Developed by : Rohan added this branch  
	 */
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
	
	/**
	 * ends here 
	 */
	
	filter = new Filter();
	filter.setQuerryString("creationDate <=");
	filter.setDateValue(date);
	filtervec.add(filter);
	
	querry=new MyQuerry();
	querry.setQuerryObject(new PurchaseOrder());
	querry.setFilters(filtervec);
	retriveTable(querry,form.getExpenseTable());
	
	
	setTableSelectionOnPR();
	setTableSelectionOnRFQ();
	setTableSelectionOnLOI();
	setTableSelectionOnPO();
	
	}

public static void initalize()
{
	PurchaseDashboardForm purchasedashForm = new PurchaseDashboardForm();
	AppMemory.getAppMemory().currentScreen=Screen.HOME;
	AppMemory.getAppMemory().skeleton.getProcessName().setText("Dashboard/Purchase");
	PurchaseDashboardSearch searchpopup=new PurchaseDashboardSearch();
	PurchaseDashboardPresenter presenter= new PurchaseDashboardPresenter(purchasedashForm,searchpopup);
	AppMemory.getAppMemory().stickPnel(purchasedashForm);	
}

@Override
public void onClick(ClickEvent event) {
	
	if(event.getSource() instanceof InlineLabel)
	{
		InlineLabel lbl= (InlineLabel) event.getSource();
		String text= lbl.getText();
		if(text.equals("Search"))
		searchpopup.showPopUp();
	
		if(text.equals("Go")){
			reactOnGo();
		}
	}
	
//	if(event.getSource().equals(form.getBtnGo())){if(form.getDbFromDate().getValue()==null||form.getDbToDate().getValue()==null){
//		final GWTCAlert alert = new GWTCAlert();
//		alert.alert("Select From and To Date.");
//	}
//	if(form.getDbFromDate().getValue()!=null&&form.getDbToDate()!=null){
//		Date formDate=form.getDbFromDate().getValue();
//		Date toDate=form.getDbToDate().getValue();
//		if(toDate.equals(formDate)||toDate.after(formDate)){
//			System.out.println("From Date ::::::::: "+formDate);
//			CalendarUtil.addDaysToDate(formDate,7);
//			System.out.println("Changed Date ::::::::: "+formDate);
//			System.out.println("To Date ::::::::: "+toDate);
//		
//			if(toDate.before(formDate)){
//				System.out.println("Inside Date Condition"); 
//				searchByFromToDate();
//			}
//			else{
//				final GWTCAlert alert = new GWTCAlert();
//				alert.alert("From Date and To Date Difference Can Not Be More Than 7 Days.");
//			}
//		}
//		else{
//			final GWTCAlert alert = new GWTCAlert();
//			alert.alert("To Date should be greater than From Date.");
//		}
//	}}
	
	
	if(event.getSource().equals(form.getBtnGo())){
		if(form.getOlbbranch().getSelectedIndex()==0&&
		form.getOblEmployee().getSelectedIndex()==0&&
		form.getDbFromDate().getValue()==null&&form.getDbToDate().getValue()==null){
	
		final GWTCAlert alert = new GWTCAlert();
		alert.alert("Select atleast one field to search");
	}
		
	else{	
		
		if(form.getOblEmployee().getSelectedIndex()!=0&&form.getOlbbranch().getSelectedIndex()==0
		   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
			
			System.out.println("salesperson ");
			searchByFromToDate();
			
		}
		else if(form.getOblEmployee().getSelectedIndex()==0&&form.getOlbbranch().getSelectedIndex()!=0
				   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
			
			
			System.out.println("branch ");
			searchByFromToDate();
		}
		else if(form.getOblEmployee().getSelectedIndex()!=0&&form.getOlbbranch().getSelectedIndex()!=0
				   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
			
			System.out.println("for both sales person and branch");
			searchByFromToDate();
		}
		else{
		System.out.println("Remaining all situations ");
		
		if(form.getDbFromDate().getValue()==null||form.getDbToDate().getValue()==null){
			final GWTCAlert alert = new GWTCAlert();
			alert.alert("Select From and To Date.");
		}
		 if(form.getDbFromDate().getValue()!=null&&form.getDbToDate()!=null){
		Date formDate=form.getDbFromDate().getValue();
		Date toDate=form.getDbToDate().getValue();
		if(toDate.equals(formDate)||toDate.after(formDate)){
			System.out.println("From Date ::::::::: "+formDate);
			CalendarUtil.addDaysToDate(formDate,7);
			System.out.println("Changed Date ::::::::: "+formDate);
			System.out.println("To Date ::::::::: "+toDate);
		
			if(toDate.before(formDate)){
				System.out.println("Inside Date Condition"); 
				searchByFromToDate();
			}
			else{
				final GWTCAlert alert = new GWTCAlert();
				alert.alert("From Date and To Date Difference Can Not Be More Than 7 Days.");
			}
		}
		else{
			final GWTCAlert alert = new GWTCAlert();
			alert.alert("To Date should be greater than From Date.");
		}
		}
		}
	}
	}
	
	
}

public void SetEventHandling()
{
	 InlineLabel label[] =AppMemory.getAppMemory().skeleton.getMenuLabels();
	  for(int k=0;k<label.length;k++)
	  {
		 if( AppMemory.getAppMemory().skeleton.registration[k]!=null)
		    AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
	  }
	 
	  for( int k=0;k<label.length;k++)
	  {
		  AppMemory.getAppMemory().skeleton.registration[k]= label[k].addClickHandler(this);
	  }
}

public void reactOnGo()
{ 
	
	if(!searchpopup.personInfo.getId().getValue().equals("")&&!searchpopup.personInfo.getName().getValue().equals("")&&!searchpopup.personInfo.getPhone().getValue().equals(""))
	{
		MyQuerry quer=createPRFilter(new PurchaseRequisition());
		 this.form.prTable.connectToLocal();
		retriveTable(quer, form.getBillingTable());
		 
		quer=createRFQFilter();
		 this.form.rfqTable.connectToLocal();
		 retriveTable(quer, form.getInvoiceTable());
		 
		 quer=createLOIFilter();
		 this.form.loiTable.connectToLocal();
		 retriveTable(quer, form.getPaymentTable());
		 
		quer=createPOFilter();
		 this.form.poTable.connectToLocal();
		 retriveTable(quer, form.getExpenseTable());
		 
		 searchpopup.hidePopUp();
	}
	
	if(searchpopup.personInfo.getId().getValue().equals("")&&searchpopup.personInfo.getName().getValue().equals("")&&searchpopup.personInfo.getPhone().getValue().equals(""))
	{
		GWTCAlert alertMsg=new GWTCAlert();
		alertMsg.alert("Please enter vendor information");
	}
	
	
	 
}

private MyQuerry createPRFilter(PurchaseRequisition purchasereq)
{
	Vector<Filter>filterVec = new Vector<Filter>();
	 Filter temp;
	 
	 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
			temp.setQuerryString("personInfo.count");
			filterVec.add(temp);	
		}
		
		if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.personInfo.getName().getValue());
			temp.setQuerryString("personInfo.fullName");
			filterVec.add(temp);	
		}
		
		if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
			temp.setQuerryString("personInfo.cellNumber");
			filterVec.add(temp);	
		}
		
		MyQuerry purchReq=new MyQuerry();
		purchReq.setQuerryObject(new PurchaseRequisition());
		purchReq.setFilters(filterVec);
		
		return purchReq;
}

private MyQuerry createRFQFilter()
{
	Vector<Filter>filterVec = new Vector<Filter>();
	 Filter temp;
	 
	 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
			temp.setQuerryString("vendorDetails.vendorId");
			filterVec.add(temp);	
		}
		
		if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.personInfo.getName().getValue());
			temp.setQuerryString("vendorDetails.vendorName");
			filterVec.add(temp);	
		}
		
		if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
			temp.setQuerryString("vendorDetails.vendorPhone");
			filterVec.add(temp);	
		}
		
		MyQuerry rfq=new MyQuerry();
		rfq.setQuerryObject(new RequsestForQuotation());
		rfq.setFilters(filterVec);
		
		return rfq;
}

private MyQuerry createLOIFilter()
{
	Vector<Filter>filterVec = new Vector<Filter>();
	 Filter temp;
	 
	 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
			temp.setQuerryString("vendorinfo.vendorId");
			filterVec.add(temp);	
		}
		
		if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.personInfo.getName().getValue());
			temp.setQuerryString("vendorinfo.vendorName");
			filterVec.add(temp);	
		}
		
		if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
			temp.setQuerryString("vendorinfo.vendorPhone");
			filterVec.add(temp);	
		}
		
		MyQuerry loi=new MyQuerry();
		loi.setQuerryObject(new LetterOfIntent());
		loi.setFilters(filterVec);
		
		return loi;
}

private MyQuerry createPOFilter()
{
	Vector<Filter>filterVec = new Vector<Filter>();
	 Filter temp;
	 
	 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
			temp.setQuerryString("vendorDetails.vendorId");
			filterVec.add(temp);	
		}
		
		if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.personInfo.getName().getValue());
			temp.setQuerryString("vendorDetails.vendorName");
			filterVec.add(temp);	
		}
		
		if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
			temp.setQuerryString("vendorDetails.vendorPhone");
			filterVec.add(temp);	
		}
		
		MyQuerry po=new MyQuerry();
		po.setQuerryObject(new PurchaseOrder());
		po.setFilters(filterVec);
		
		return po;
}

public <T> void retriveTable(MyQuerry querry,final SuperTable<T>table)
 {
			table.connectToLocal();
			service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					for(SuperModel model:result)
					{
						table.getListDataProvider().getList().add((T) model);
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
				caught.printStackTrace();
					
				}
			}); 
 }


 public void setTableSelectionOnPR()
 {
	 final NoSelectionModel<PurchaseRequisition> selectionModelMyObj = new NoSelectionModel<PurchaseRequisition>();
	 
	 
     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
     {
         @Override
         public void onSelectionChange(SelectionChangeEvent event) 
         {
        	 /**** Date 31-01-2019 by Vijay for NBHC Inventory management approval status button *****/
        	 if(PrTableProxy.approvalStatusflag==false){
        		 
        	 final PurchaseRequisition billentity=selectionModelMyObj.getLastSelectedObject();
        	 final PurchaseRequisitionForm form = new PurchaseRequisitionForm();
        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Purchase Requisition",Screen.PURCHASEREQUISITE);
			 PurchaseRequitionPresenter presenter=new PurchaseRequitionPresenter(form,billentity);
			 AppMemory.getAppMemory().stickPnel(form);
			 
			 Timer timer=new Timer() 
        	 {
 				@Override
 				public void run() {
 					 form.updateView(billentity);
 					 form.setToViewState();
 					 PurchaseRequisition billingdoc=new PurchaseRequisition();
 				/*	 billingdoc.setBillId(billentity.getCount());
 					 billingdoc.setBillingDate(billentity.getBillingDate());
 					 billingdoc.setBillingCategory(billentity.getBillingCategory());
 					 billingdoc.setBillingGroup(billentity.getBillingGroup());
 					 billingdoc.setBillingType(billentity.getBillingType());
 					 billingdoc.setSalesAmount(billentity.getTotalSalesAmount());
 					 billingdoc.setBillAmount(billentity.getTotalBillingAmount());
 					 billingdoc.setBillstatus(billentity.getStatus());
 					 form.getDosalesamount().setValue(billingdoc.getSalesAmount());
 					 form.getDototalbillamt().setValue(billingdoc.getBillAmount());
 					 form.getBillingDocumentTable().getDataprovider().getList().add(billingdoc);
 					*/
				     PurchaseRequitionPresenter presenter=(PurchaseRequitionPresenter) form.getPresenter();
				    // presenter.form.setMenus();
				     
 					
 				}
 			};
             timer.schedule(3700); 
             
			 }
             else{
            	 PrTableProxy.approvalStatusflag = false;
             }
             
          }
         
         
     };
     // Add the handler to the selection model
     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
     // Add the selection model to the table
     form.prTable.getTable().setSelectionModel(selectionModelMyObj);
     
 }
 
 public void setTableSelectionOnRFQ()
 {
	 final NoSelectionModel<RequsestForQuotation> selectionModelMyObj = new NoSelectionModel<RequsestForQuotation>();
	 
     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
     {
         @Override
         public void onSelectionChange(SelectionChangeEvent event) 
         {
        	 final RequsestForQuotation inventity=selectionModelMyObj.getLastSelectedObject();
        	 final RequestForQuotationForm form = new RequestForQuotationForm();
        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/RFQ",Screen.REQUESTFORQUOTATION);
        	 RequestForQuotationPresenter presenter=new RequestForQuotationPresenter(form,inventity);
			 AppMemory.getAppMemory().stickPnel(form);
		
        	 
			 Timer timer=new Timer() 
        	 {
 				@Override
 				public void run() {
 					form.updateView(inventity);
 					 form.setToViewState();
				    RequestForQuotationPresenter presenter=(RequestForQuotationPresenter) form.getPresenter();
				    presenter.form.setMenuAsPerStatus();
 				}
 			};
             timer.schedule(3700); 
          }
     };
     // Add the handler to the selection model
     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
     // Add the selection model to the table
     form.rfqTable.getTable().setSelectionModel(selectionModelMyObj);
 }
 
 public void setTableSelectionOnLOI()
 {
	 final NoSelectionModel<LetterOfIntent> selectionModelMyObj = new NoSelectionModel<LetterOfIntent>();
	 
     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
     {
         @Override
         public void onSelectionChange(SelectionChangeEvent event) 
         {
        	 final LetterOfIntent payentity=selectionModelMyObj.getLastSelectedObject();
        	 final LetterOfIntentForm form = new LetterOfIntentForm();
        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Letter Of Intent(LOI)",Screen.LETTEROFINTENT);
        	 LetterOfIntentPresenter presenter=new LetterOfIntentPresenter(form,payentity);
			 AppMemory.getAppMemory().stickPnel(form);
		
        	 
			 Timer timer=new Timer() 
        	 {
 				@Override
 				public void run() {
 					form.updateView(payentity);
 					 form.setToViewState();
				    LetterOfIntentPresenter presenter=(LetterOfIntentPresenter) form.getPresenter();
				   presenter.form.setMenuAsPerStatus();
 				}
 			};
             timer.schedule(3700); 
          }
     };
     // Add the handler to the selection model
     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
     // Add the selection model to the table
     form.loiTable.getTable().setSelectionModel(selectionModelMyObj);
 }

 public void setTableSelectionOnPO()
 {
	 final NoSelectionModel<PurchaseOrder> selectionModelMyObj = new NoSelectionModel<PurchaseOrder>();
	 
     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
     {
         @Override
         public void onSelectionChange(SelectionChangeEvent event) 
         {
        	 final PurchaseOrder expentity=selectionModelMyObj.getLastSelectedObject();
        	 final PurchaseOrderForm form = new PurchaseOrderForm();
        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Purchase Order",Screen.PURCHASEORDER);
        	 PurchaseOrderPresenter presenter=new PurchaseOrderPresenter(form,expentity);
			 AppMemory.getAppMemory().stickPnel(form);
		
        	 
			 Timer timer=new Timer() 
        	 {
 				@Override
 				public void run() {
 					form.updateView(expentity);
 					 form.setToViewState();
 					PurchaseOrderPresenter presenter=(PurchaseOrderPresenter) form.getPresenter();
 					presenter.form.setMenuAsPerStatus();
 				}
 			};
             timer.schedule(3700); 
          }
     };
     // Add the handler to the selection model
     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
     // Add the selection model to the table
     form.poTable.getTable().setSelectionModel(selectionModelMyObj);
 }
 
 
 private void searchByFromToDate(){
		
		
		System.out.println("Hiiiiiiiiiiii........");
		
		Long compId=UserConfiguration.getCompanyId();
	    System.out.println("Company Id :: "+compId);
		
		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		List<String> statusList;
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(PurchaseRequisition.CREATED);
		statusList.add(PurchaseRequisition.REQUESTED);
		statusList.add(PurchaseRequisition.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("purchasedetails.status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("purchasedetails.creationDate >=");
		filter.setDateValue(form.getDbFromDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("purchasedetails.creationDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("for service    ");
			filter = new Filter();
			filter.setQuerryString("purchasedetails.branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);
		}
		
		if(form.getOblEmployee().getSelectedIndex()!=0){
			System.out.println("for service    ");
			filter = new Filter();
			filter.setQuerryString("employee");
			filter.setStringValue(form.getOblEmployee().getValue());
			filtervec.add(filter);
		}
		
		querry=new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new PurchaseRequisition());
		form.getBillingTable().connectToLocal();
		this.retriveTable(querry,form.getBillingTable());
		
		
		
		
		////////////////////////////////////Create Querry For RFQ///////////////////////////// 
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(RequsestForQuotation.CREATED);
		statusList.add(RequsestForQuotation.REQUESTED);
		statusList.add(RequsestForQuotation.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate >=");
		filter.setDateValue(form.getDbFromDate().getValue());
		filtervec.add(filter);
		}
		
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);
		}
		
		if(form.getOblEmployee().getSelectedIndex()!=0){
			filter = new Filter();
			filter.setQuerryString("employee");
			filter.setStringValue(form.getOblEmployee().getValue());
			filtervec.add(filter);
		}
		
		querry=new MyQuerry();
		querry.setQuerryObject(new RequsestForQuotation());
		querry.setFilters(filtervec);
		form.getInvoiceTable().connectToLocal();
		retriveTable(querry,form.getInvoiceTable());
		
		///////////Create Querry For LOI/////////////////////////////////////////////////////
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(LetterOfIntent.CREATED);
		statusList.add(LetterOfIntent.REQUESTED);
		statusList.add(LetterOfIntent.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("LoiDate >=");
		filter.setDateValue(form.getDbFromDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("LoiDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("for service    ");
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);
		}
		
		if(form.getOblEmployee().getSelectedIndex()!=0){
			System.out.println("for service    ");
			filter = new Filter();
			filter.setQuerryString("employee");
			filter.setStringValue(form.getOblEmployee().getValue());
			filtervec.add(filter);
		}
		
		
		querry=new MyQuerry();
		querry.setQuerryObject(new LetterOfIntent());
		querry.setFilters(filtervec);
		form.getPaymentTable().connectToLocal();
		retriveTable(querry,form.getPaymentTable());
		
		////////////////////Create Querry For PO//////////////////////
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(PurchaseOrder.CREATED);
		statusList.add(PurchaseOrder.REQUESTED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate >=");
		filter.setDateValue(form.getDbFromDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("for service    ");
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);
		}
		
		if(form.getOblEmployee().getSelectedIndex()!=0){
			System.out.println("for service    ");
			filter = new Filter();
			filter.setQuerryString("employee");
			filter.setStringValue(form.getOblEmployee().getValue());
			filtervec.add(filter);
		}
		
		querry=new MyQuerry();
		querry.setQuerryObject(new PurchaseOrder());
		querry.setFilters(filtervec);
		form.getExpenseTable().connectToLocal();
		retriveTable(querry,form.getExpenseTable());
	}}
