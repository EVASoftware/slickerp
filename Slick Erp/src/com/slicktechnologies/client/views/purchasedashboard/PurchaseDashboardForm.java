package com.slicktechnologies.client.views.purchasedashboard;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.MyInlineLabel;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentPresenter;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentPresenterTableProxy;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenterTableProxy;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequisitionPresenterTable;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequitionPresenter;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationPresenter;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationPresenterTableProxy;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class PurchaseDashboardForm extends ViewContainer implements ClickHandler{
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	/** String array repersenting the table header menus*/
	String[] tableheaderbarnames={"New","Download"};
	
	/** Flow panel for holding the Table header menus */
	FlowPanel tableheader;
	
	/** Billing Table which can come on the form, this component can be made generic */
	public PrTableProxy prTable;
	
	/** Invoice Table which can come on form component can be made generic */
	public RfqTableProxy rfqTable;
	
	/** Payment table which can come on form component can be made generic */
	public LoiTableProxy loiTable;
	/** Expense table which can come on form component can be made generic */
	public PoTableProxy poTable;
	
	//**************************rohan added this fields in dashboard for searching by branch and sales person  
	
	ObjectListBox<Branch> olbbranch=new ObjectListBox<Branch>();
	ObjectListBox<Employee> oblEmployee = new ObjectListBox<Employee>();

//************************************changes ends here ********************
	
	
	DateBox dbFromDate;
	DateBox dbToDate;
	Button btnGo;
	
	public PurchaseDashboardForm()
	{
		//Toggle the App header bar menu , In this case only search should come
		toggleAppHeaderBarMenu();
		//Create the Gui, actual Gui gets created here.
		createGui();
		//Sets the event handling
  }	
	private void initializeWidget(){
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		dbFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		dbToDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		btnGo=new Button("GO");
		
		
		olbbranch =new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbranch);
		
		oblEmployee = new ObjectListBox<Employee>();
		initalizEmployeeNameListBox();
		
	}
	
	
	protected void initalizEmployeeNameListBox() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Employee());
		oblEmployee.MakeLive(querry);
	}
	
			
	/**
	 * Toggles the app header bar menu.
	 */
	private void toggleAppHeaderBarMenu() {
		InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
		for(int k=0;k<menus.length;k++)
		{
			String text=menus[k].getText();
			if(text.contains("Search"))
			{
				menus[k].setVisible(true); 
			}
			else
				menus[k].setVisible(false);  		   
			
		}
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.ViewContainer#createGui()
	 */
	@Override
	public void createGui() 
	{
		initializeWidget();
		
		FlowPanel panel= new FlowPanel();
		
		panel.getElement().getStyle().setMarginTop(10, Unit.PX);
		panel.getElement().getStyle().setMarginLeft(75, Unit.PX);
		panel.getElement().getStyle().setMarginRight(75, Unit.PX);
		
		FlowPanel innerpanel= new FlowPanel();
		
		InlineLabel blank = new InlineLabel("    ");
		
		//************rohan added here search by branch and sales person***************  
		
		
		InlineLabel branch = new InlineLabel("  Branch  ");
		branch.setSize("90px", "20px");
		innerpanel.add(branch);
		innerpanel.add(olbbranch);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
//		
		
		InlineLabel salesPerson = new InlineLabel(" Purchase Engineer  ");
		salesPerson.setSize("90px", "20px");
		innerpanel.add(salesPerson);
		innerpanel.add(oblEmployee);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
		
//**************************changes ends here ********************************
		
		
		InlineLabel fromDate = new InlineLabel("  From Date ");
		fromDate.setSize("90px", "20px");
		innerpanel.add(fromDate);
		innerpanel.add(dbFromDate);
		
		InlineLabel toDate = new InlineLabel("  To Date ");
		innerpanel.add(toDate);
		innerpanel.add(dbToDate);
		innerpanel.add(blank);
		innerpanel.add(blank);
		innerpanel.add(blank);
		innerpanel.add(blank);
		innerpanel.add(btnGo);
		
		panel.add(innerpanel);
		content.add(panel);
		
		//Create the Four Scroll Panel To hold four types of Screen
		ScrollPanel[] scrollPanes=new ScrollPanel[4];
		//String array to hold captions
		String[]captions={"Purchase Requisition","RFQ","Letter of Intent(LOI)","Purchase Order"};
		//Screen array to hold new Screen object for the purpose of Redirection
		Screen[]screens={Screen.PURCHASEREQUISITE,Screen.REQUESTFORQUOTATION,Screen.LETTEROFINTENT,Screen.PURCHASEORDER};	
		
		//Create  Flow Panel,Inside Flow Panel 
		for(int i=0;i<captions.length;i++)
		{
			FlowPanel holder= new FlowPanel();
			tableheader= new FlowPanel();
			tableheader.getElement().setId("tableheader");
			// Provides Table Header which is responsible for new or Download Buttons
			setTableHeaderBar(screens[i]);
			
			tableheader.getElement().getStyle().setMarginTop(10, Unit.PX);
			tableheader.getElement().getStyle().setMarginBottom(10, Unit.PX);
			
			scrollPanes[i]= new ScrollPanel();
			scrollPanes[i].setHeight("150px");
		
			holder.add(tableheader);
			holder.add(scrollPanes[i]);
			//Caption Panel inside which a Flow Panel is added
			CaptionPanel cap= new CaptionPanel(captions[i]);
			cap.add(holder);
			cap.getElement().setClassName("tablecaption");
			//Add caption Panel on Content
		    content.add(cap);
		}
		//Add Billing Table inside the Scroll Panel
		prTable = new PrTableProxy();
		scrollPanes[0].add(prTable.content);

		//Add Invoice Table inside the Scroll Panel
		rfqTable = new RfqTableProxy();
		scrollPanes[1].add(rfqTable.content);
		
		//Add Payment Panel inside the table
		loiTable = new LoiTableProxy();
		scrollPanes[2].add(loiTable.content);
		
		//Add Expense Table
		poTable = new PoTableProxy();
		scrollPanes[3].add(poTable.content);
		
		content.getElement().setId("homeview");
	  }
	
	
	public void setTableHeaderBar(Screen screen)
	{
		if(screen == Screen.PURCHASEREQUISITE)
		{
			MyInlineLabel lbl1,lbl;
			lbl1=new MyInlineLabel("New",screen);
			getTableheader().add(lbl1);

			lbl=new MyInlineLabel("Download",screen);
			getTableheader().add(lbl);
			lbl1.getElement().setId("tableheadermenu");
			lbl1.addClickHandler(this);

			lbl.getElement().setId("tableheadermenu");
			lbl.addClickHandler(this);
			return;
		}
		if(screen == Screen.REQUESTFORQUOTATION)
		{
			MyInlineLabel lbl1,lbl;
			lbl1=new MyInlineLabel("New",screen);
			getTableheader().add(lbl1);

			lbl=new MyInlineLabel("Download",screen);
			getTableheader().add(lbl);
			lbl1.getElement().setId("tableheadermenu");
			lbl1.addClickHandler(this);

			lbl.getElement().setId("tableheadermenu");
			lbl.addClickHandler(this);
			return;
		}
		if(screen == Screen.LETTEROFINTENT)
		{
			MyInlineLabel lbl1,lbl;
			lbl1=new MyInlineLabel("New",screen);
			getTableheader().add(lbl1);
			lbl=new MyInlineLabel("Download",screen);
			getTableheader().add(lbl);
			lbl1.getElement().setId("tableheadermenu");
			lbl1.addClickHandler(this);	

			lbl.getElement().setId("tableheadermenu");
			lbl.addClickHandler(this);
			return;
		}
		if(screen == Screen.PURCHASEORDER)
		{
			MyInlineLabel lbl1,lbl;
			lbl1=new MyInlineLabel("New",screen);
			getTableheader().add(lbl1);

			lbl=new MyInlineLabel("Download",screen);
			getTableheader().add(lbl);
			
		
			lbl.getElement().setId("tableheadermenu");
			lbl.addClickHandler(this);
			lbl1.getElement().setId("tableheadermenu");
			lbl1.addClickHandler(this);	

			return;
		}
		
		for(int j=0;j<getTableheaderbarnames().length;j++)
		{
			MyInlineLabel lbl;
			lbl=new MyInlineLabel(getTableheaderbarnames()[j],screen);
            getTableheader().add(lbl);
			lbl.getElement().setId("tableheadermenu");
			lbl.addClickHandler(this);
		}
	}

		public String[] getTableheaderbarnames() {
			return tableheaderbarnames;
		}

		public void setTableheaderbarnames(String[] tableheaderbarnames) {
			this.tableheaderbarnames = tableheaderbarnames;
		}

		/**
		 * Gets the tableheader.
		 *
		 * @return the tableheader
		 */
		public FlowPanel getTableheader() {
			return tableheader;
		}

		/**
		 * Sets the tableheader.
		 *
		 * @param tableheader the new tableheader
		 */
		public void setTableheader(FlowPanel tableheader) {
			this.tableheader = tableheader;
		}


		public PrTableProxy getBillingTable() {
			return prTable;
		}

		public void setBillingTable(PrTableProxy billingTable) {
			this.prTable = billingTable;
		}

		public RfqTableProxy getInvoiceTable() {
			return rfqTable;
		}

		public void setInvoiceTable(RfqTableProxy invoiceTable) {
			this.rfqTable = invoiceTable;
		}

		public LoiTableProxy getPaymentTable() {
			return loiTable;
		}

		public void setPaymentTable(LoiTableProxy paymentTable) {
			this.loiTable = paymentTable;
		}
		
		public PoTableProxy getExpenseTable() {
			return poTable;
		}

		public void setExpenseTable(PoTableProxy expenseTable) {
			this.poTable = expenseTable;
		}

		/* (non-Javadoc)
		 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
		 */
		@Override
		public void onClick(ClickEvent event) {
			if(event.getSource() instanceof MyInlineLabel)
			{
				MyInlineLabel lbl=(MyInlineLabel) event.getSource();
				redirectOnScreens(lbl);
				
			}
			
		}
		
		private void redirectOnScreens(MyInlineLabel lbl)
		{
			Screen screen=(Screen) lbl.redirectScreen;
			String title=lbl.getText().trim();
			switch(screen)
			{
			  case PURCHASEREQUISITE:
				 	if(title.equals("New"))
				  	{
				  		PurchaseRequitionPresenter.initalize();
				  	}
					if(title.equals("Download"))
					{
						reactOnPRDownLoad();
					}
					break;
					
			  case REQUESTFORQUOTATION:
				  
				 	if(title.equals("New"))
				  	{
				  		RequestForQuotationPresenter.initalize();
				  	}
					if(title.equals("Download"))
					{
						reactOnRFQDownLoad();
					}
					break;
					
			  case LETTEROFINTENT:
				  
				 	if(title.equals("New"))
				  	{
				  		LetterOfIntentPresenter.initalize();
				  	}
					if(title.equals("Download"))
					{
						reactOnLOIDownLoad();
					}
					break;
					
			  case PURCHASEORDER:
				 
				  	if(title.equals("New"))
				  	{
				  		PurchaseOrderPresenter.initalize();
				  	}
					if(title.equals("Download"))
					{
						reactOnPODownLoad();
					}
					break;
			default:
				break;
					
			}
		}

		private void reactOnPRDownLoad()
		{
			ArrayList<PurchaseRequisition> PRArray=new ArrayList<PurchaseRequisition>();
			List<PurchaseRequisition> list=(List<PurchaseRequisition>) prTable.getListDataProvider().getList();
			PRArray.addAll(list);
			
			csvservice.setPR(PRArray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}

				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+47;
					Window.open(url, "test", "enabled");
				}
			});
		}
		
		private void reactOnRFQDownLoad()
		{
			ArrayList<RequsestForQuotation> reqQuotationarray=new ArrayList<RequsestForQuotation>();
			List<RequsestForQuotation> list=(List<RequsestForQuotation>) rfqTable.getListDataProvider().getList();
			reqQuotationarray.addAll(list);
			
			csvservice.setRequestForQuotation(reqQuotationarray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}

				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+43;
					Window.open(url, "test", "enabled");
				}
			});
		}
		
		private void reactOnLOIDownLoad()
		{
			ArrayList<LetterOfIntent> LOIarray=new ArrayList<LetterOfIntent>();
			List<LetterOfIntent> list=(List<LetterOfIntent>) loiTable.getListDataProvider().getList();
			LOIarray.addAll(list);
			
			csvservice.setLOI(LOIarray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}

				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+45;
					Window.open(url, "test", "enabled");
				}
			});
		}

		private void reactOnPODownLoad()
		{
			ArrayList<PurchaseOrder> POArray=new ArrayList<PurchaseOrder>();
			List<PurchaseOrder> list=(List<PurchaseOrder>) poTable.getListDataProvider().getList();
			POArray.addAll(list);
			
			csvservice.setPO(POArray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}

				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+46;
					Window.open(url, "test", "enabled");
				}
			});
		}

		
	@Override
	public void applyStyle() {
		
	}
	
	public PrTableProxy getPrTable() {
		return prTable;
	}
	public void setPrTable(PrTableProxy prTable) {
		this.prTable = prTable;
	}
	public RfqTableProxy getRfqTable() {
		return rfqTable;
	}
	public void setRfqTable(RfqTableProxy rfqTable) {
		this.rfqTable = rfqTable;
	}
	public LoiTableProxy getLoiTable() {
		return loiTable;
	}
	public void setLoiTable(LoiTableProxy loiTable) {
		this.loiTable = loiTable;
	}
	public PoTableProxy getPoTable() {
		return poTable;
	}
	public void setPoTable(PoTableProxy poTable) {
		this.poTable = poTable;
	}
	public DateBox getDbFromDate() {
		return dbFromDate;
	}
	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}
	public DateBox getDbToDate() {
		return dbToDate;
	}
	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}
	public Button getBtnGo() {
		return btnGo;
	}
	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}
	public ObjectListBox<Branch> getOlbbranch() {
		return olbbranch;
	}
	public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
		this.olbbranch = olbbranch;
	}
	public ObjectListBox<Employee> getOblEmployee() {
		return oblEmployee;
	}
	public void setOblEmployee(ObjectListBox<Employee> oblEmployee) {
		this.oblEmployee = oblEmployee;
	}
	
	

}
