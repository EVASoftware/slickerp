package com.slicktechnologies.client.views.purchasedashboard;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;

public class PoTableProxy extends SuperTable<PurchaseOrder> {

TextColumn<PurchaseOrder> LOIIDColumn;
TextColumn<PurchaseOrder> dateColumn;
TextColumn<PurchaseOrder> closureDateColumn;
TextColumn<PurchaseOrder> responseDateColumn;
TextColumn<PurchaseOrder> employeeColumn;
TextColumn<PurchaseOrder> loiGroupColumn;
TextColumn<PurchaseOrder> loitypeColumn;
TextColumn<PurchaseOrder> statusColumn;
TextColumn<PurchaseOrder> ColumnTitle;
TextColumn<PurchaseOrder> ColumnBranch;
TextColumn<PurchaseOrder> ColumnAprroverName;
TextColumn<PurchaseOrder> ColumnVendorName;

public PoTableProxy() {
	super();
}

@Override
public void createTable() {
	addColumnLOIid();
	addColumnTitle();
	addColumnLOIDate();
	
	addColumnPoVendor();
	addColumnLoiGroup();
	addColumnLoiType();
	
	addColumnBranch();
	addColumnEmployee();
	addColumnApprover();

	
	addColumnDeliveryDateColumn();
	addColumnClosureDateColumn();
	addColumnStatus();
	

}



public void addColumnSorting() {
	addSortinggetLOIID();
	addSortinggetLOIDate();
	addSortingColumnTitle();
	addSortingVendorName();
//	addSortingColumnvendor();
	addSortinggetLOICategory();
	addSortinggetLOIType();
	
	addSortingColumnBranch();
	addSortinggetEmployee();
	addSortingColumnApprover();
	
	addSortinggetClosureDate();
	addSortinggetResponseDate();
	addSortinggetStatus();

}



/************************* sorting column *************************************************/

public void addSortingColumnTitle() {
	List<PurchaseOrder> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseOrder>(list);
	columnSort.setComparator(ColumnTitle,
			new Comparator<PurchaseOrder>() {
				@Override
				public int compare(PurchaseOrder e1, PurchaseOrder e2) {
					if (e1 != null && e2 != null) {
						if (e1.getPOName() != null
								&& e2.getPOName() != null) {
							return e1.getPOName().compareTo(
									e2.getPOName());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}


public void addSortingVendorName() {
	List<PurchaseOrder> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseOrder>(list);
	columnSort.setComparator(ColumnVendorName,new Comparator<PurchaseOrder>() {
				@Override
				public int compare(PurchaseOrder e1, PurchaseOrder e2) {
					if (e1 != null && e2 != null) {
						if (e1.getVendorDetails().get(0).getVendorName() != null&& e2.getVendorDetails().get(0).getVendorName()!= null) {
							return e1.getVendorDetails().get(0).getVendorName().compareTo(e2.getVendorDetails().get(0).getVendorName());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}


public void addSortingColumnBranch() {
	List<PurchaseOrder> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseOrder>(list);
	columnSort.setComparator(ColumnBranch,
			new Comparator<PurchaseOrder>() {
				@Override
				public int compare(PurchaseOrder e1, PurchaseOrder e2) {
					if (e1 != null && e2 != null) {
						if (e1.getBranch() != null
								&& e2.getBranch() != null) {
							return e1.getBranch().compareTo(
									e2.getBranch());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void addSortingColumnApprover() {
	List<PurchaseOrder> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseOrder>(list);
	columnSort.setComparator(ColumnAprroverName,
			new Comparator<PurchaseOrder>() {
				@Override
				public int compare(PurchaseOrder e1, PurchaseOrder e2) {
					if (e1 != null && e2 != null) {
						if (e1.getApproverName() != null
								&& e2.getApproverName() != null) {
							return e1.getApproverName().compareTo(
									e2.getApproverName());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}



public void addSortinggetStatus() {
	List<PurchaseOrder> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseOrder>(list);
	columnSort.setComparator(statusColumn,
			new Comparator<PurchaseOrder>() {
				@Override
				public int compare(PurchaseOrder e1, PurchaseOrder e2) {
					if (e1 != null && e2 != null) {
						if (e1.getStatus() != null
								&& e2.getStatus() != null) {
							return e1.getStatus().compareTo(e2.getStatus());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void addSortinggetLOIType() {
	List<PurchaseOrder> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseOrder>(list);
	columnSort.setComparator(loitypeColumn,
			new Comparator<PurchaseOrder>() {
				@Override
				public int compare(PurchaseOrder e1, PurchaseOrder e2) {
					if (e1 != null && e2 != null) {
						if (e1.getLOItype() != null
								&& e2.getLOItype() != null) {
							return e1.getLOItype().compareTo(
									e2.getLOItype());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void addSortinggetLOICategory() {
	List<PurchaseOrder> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseOrder>(list);
	columnSort.setComparator(loiGroupColumn,
			new Comparator<PurchaseOrder>() {
				@Override
				public int compare(PurchaseOrder e1, PurchaseOrder e2) {
					if (e1 != null && e2 != null) {
						if (e1.getLOIGroup() != null
								&& e2.getLOIGroup() != null) {
							return e1.getLOIGroup().compareTo(
									e2.getLOIGroup());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void addSortinggetEmployee() {
	List<PurchaseOrder> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseOrder>(list);
	columnSort.setComparator(employeeColumn,
			new Comparator<PurchaseOrder>() {
				@Override
				public int compare(PurchaseOrder e1, PurchaseOrder e2) {
					if (e1 != null && e2 != null) {
						if (e1.getEmployee() != null
								&& e2.getEmployee() != null) {
							return e1.getEmployee().compareTo(
									e2.getEmployee());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}



public void addSortinggetResponseDate() {
	List<PurchaseOrder> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseOrder>(list);
	columnSort.setComparator(responseDateColumn,
			new Comparator<PurchaseOrder>() {
				@Override
				public int compare(PurchaseOrder e1, PurchaseOrder e2) {
					if (e1 != null && e2 != null) {
						if (e1.getDeliveryDate() != null
								&& e2.getDeliveryDate() != null) {
							return e1.getDeliveryDate().compareTo(
									e2.getDeliveryDate());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void addSortinggetClosureDate() {
	List<PurchaseOrder> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseOrder>(list);
	columnSort.setComparator(closureDateColumn,
			new Comparator<PurchaseOrder>() {
				@Override
				public int compare(PurchaseOrder e1, PurchaseOrder e2) {
					if (e1 != null && e2 != null) {
						if (e1.getExpectedClosureDate() != null
								&& e2.getExpectedClosureDate() != null) {
							return e1.getExpectedClosureDate().compareTo(
									e2.getExpectedClosureDate());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void addSortinggetLOIDate() {
	List<PurchaseOrder> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseOrder>(list);
	columnSort.setComparator(dateColumn, new Comparator<PurchaseOrder>() {
		@Override
		public int compare(PurchaseOrder e1, PurchaseOrder e2) {
			if (e1 != null && e2 != null) {
				if (e1.getCreationDate() != null
						&& e2.getCreationDate() != null) {
					return e1.getCreationDate().compareTo(
							e2.getCreationDate());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);

}

public void addSortinggetLOIID() {
	List<PurchaseOrder> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseOrder>(list);
	columnSort.setComparator(LOIIDColumn, new Comparator<PurchaseOrder>() {
		@Override
		public int compare(PurchaseOrder e1, PurchaseOrder e2) {
			if (e1 != null && e2 != null) {
				if (e1.getCount() == e2.getCount()) {
					return 0;
				}
				if (e1.getCount() > e2.getCount()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);

}



//private void addSortingColumnvendor() {
//	List<PurchaseOrder> list = getDataprovider().getList();
//	columnSort = new ListHandler<PurchaseOrder>(list);
//	columnSort.setComparator(dateColumn, new Comparator<PurchaseOrder>() {
//		@Override
//		public int compare(PurchaseOrder e1, PurchaseOrder e2) {
//			if (e1 != null && e2 != null) {
//				if (e1.getCreationDate() != null
//						&& e2.getCreationDate() != null) {
//					return e1.getCreationDate().compareTo(
//							e2.getCreationDate());
//				}
//			} else {
//				return 0;
//			}
//			return 0;
//		}
//	});
//	table.addColumnSortHandler(columnSort);
//}

/***************************************************************************/

private void addColumnPoVendor() {
	ColumnVendorName = new TextColumn<PurchaseOrder>() {
		@Override
		public String getValue(PurchaseOrder object) {
			String venName="";
			object.getVendorDetails();
			for(int i=0;i<object.getVendorDetails().size();i++){
				if(object.getVendorDetails().get(i).getStatus()==true){
					venName=object.getVendorDetails().get(i).getVendorName();
					break;
				}
			}
			return venName;
		}
	};
	table.addColumn(ColumnVendorName, "Vendor Name");
	table.setColumnWidth(ColumnVendorName, 130,Unit.PX);
	ColumnVendorName.setSortable(true);
}

public void addColumnTitle() {
	ColumnTitle = new TextColumn<PurchaseOrder>() {

		@Override
		public String getValue(PurchaseOrder object) {
			return object.getPOName();
		}
	};
	table.addColumn(ColumnTitle, "Title");
	table.setColumnWidth(ColumnTitle, 90,Unit.PX);
	ColumnTitle.setSortable(true);
}

public void addColumnLOIDate() {
	final Date sub7Days=new Date();
	System.out.println("Todays Date ::: "+sub7Days);
	CalendarUtil.addDaysToDate(sub7Days,-7);
	System.out.println("Date - 7 Days ::: "+sub7Days);
	sub7Days.setHours(23);
	sub7Days.setMinutes(59);
	sub7Days.setSeconds(59);
	System.out.println("Converted Date ::: "+sub7Days);
	
	dateColumn = new TextColumn<PurchaseOrder>() {

		@Override
		public String getCellStyleNames(Context context, PurchaseOrder object) {
			if((object.getPODate().before(sub7Days))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Approved")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
		
		@Override
		public String getValue(PurchaseOrder object) {
			if (object.getPODate() != null)
				return AppUtility.parseDate(object.getPODate());
			else
				return " ";
		}
	};
	table.addColumn(dateColumn, "Date");
	table.setColumnWidth(dateColumn, 120,Unit.PX);
	dateColumn.setSortable(true);

}

public void addColumnStatus() {
	statusColumn = new TextColumn<PurchaseOrder>() {

		@Override
		public String getValue(PurchaseOrder object) {
			return object.getStatus();
		}
	};
	table.addColumn(statusColumn, "Status");
	table.setColumnWidth(statusColumn, 90,Unit.PX);
	statusColumn.setSortable(true);
}

public void addColumnLoiType() {
	loitypeColumn = new TextColumn<PurchaseOrder>() {

		@Override
		public String getValue(PurchaseOrder object) {
			return object.getLOItype();
		}
	};
	table.addColumn(loitypeColumn, "Type");
	table.setColumnWidth(loitypeColumn, 90,Unit.PX);
	loitypeColumn.setSortable(true);
}

public void addColumnLoiGroup() {
	loiGroupColumn = new TextColumn<PurchaseOrder>() {

		@Override
		public String getValue(PurchaseOrder object) {
			return object.getLOIGroup();
		}
	};
	table.addColumn(loiGroupColumn, "Category");
	table.setColumnWidth(loiGroupColumn, 90,Unit.PX);
	loiGroupColumn.setSortable(true);
}

public void addColumnApprover() {
	ColumnAprroverName = new TextColumn<PurchaseOrder>() {

		@Override
		public String getValue(PurchaseOrder object) {
			return object.getBranch();
		}
	};
	table.addColumn(ColumnAprroverName, "Approver");
	table.setColumnWidth(ColumnAprroverName, 90,Unit.PX);
	ColumnAprroverName.setSortable(true);
}

public void addColumnBranch() {
	ColumnBranch = new TextColumn<PurchaseOrder>() {

		@Override
		public String getValue(PurchaseOrder object) {
			return object.getBranch();
		}
	};
	table.addColumn(ColumnBranch, "Branch");
	table.setColumnWidth(ColumnBranch, 90,Unit.PX);
	ColumnBranch.setSortable(true);
}

public void addColumnEmployee() {
	employeeColumn = new TextColumn<PurchaseOrder>() {

		@Override
		public String getValue(PurchaseOrder object) {
			return object.getEmployee();
		}
	};
	table.addColumn(employeeColumn, "Purchase Engineer");
	table.setColumnWidth(employeeColumn, 160,Unit.PX);
	employeeColumn.setSortable(true);
}



public void addColumnDeliveryDateColumn() {
	responseDateColumn = new TextColumn<PurchaseOrder>() {

		@Override
		public String getValue(PurchaseOrder object) {

			if (object.getDeliveryDate() != null)
				return AppUtility.parseDate(object.getDeliveryDate());
			else
				return "";
		}
	};
	table.addColumn(responseDateColumn, "Delivery Date");
	table.setColumnWidth(responseDateColumn, 120,Unit.PX);
	responseDateColumn.setSortable(true);
}

public void addColumnClosureDateColumn() {
	closureDateColumn = new TextColumn<PurchaseOrder>() {

		@Override
		public String getValue(PurchaseOrder object) {
			if (object.getExpectedClosureDate() != null)
				return AppUtility
						.parseDate(object.getExpectedClosureDate());
			else
				return "N/A";
		}
	};
	table.addColumn(closureDateColumn, "Closure Date");
	table.setColumnWidth(closureDateColumn, 120,Unit.PX);
	closureDateColumn.setSortable(true);
}



public void addColumnLOIid() {
	LOIIDColumn = new TextColumn<PurchaseOrder>() {

		@Override
		public String getValue(PurchaseOrder object) {
			return object.getCount() + "";
		}
	};
	table.addColumn(LOIIDColumn, "ID");
	table.setColumnWidth(LOIIDColumn, 90,Unit.PX);
	LOIIDColumn.setSortable(true);
}



@Override
protected void initializekeyprovider() {
	
}

@Override
public void addFieldUpdater() {
	
}

@Override
public void setEnable(boolean state) {
	
}

@Override
public void applyStyle() {
	
}
}
