package com.slicktechnologies.client.views.purchasedashboard;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;

public class LoiTableProxy extends SuperTable<LetterOfIntent> {

TextColumn<LetterOfIntent> LOIIDColumn;
TextColumn<LetterOfIntent> dateColumn;
TextColumn<LetterOfIntent> closureDateColumn;
TextColumn<LetterOfIntent> responseDateColumn;
TextColumn<LetterOfIntent> employeeColumn;
TextColumn<LetterOfIntent> loiGroupColumn;
TextColumn<LetterOfIntent> loitypeColumn;
TextColumn<LetterOfIntent> statusColumn;
TextColumn<LetterOfIntent> ColumnTitle;

TextColumn<LetterOfIntent> ColumnBranch;
TextColumn<LetterOfIntent> ColumnAprroverName;
TextColumn<LetterOfIntent> ColumnVendorName;


public LoiTableProxy() {
	super();
}

@Override
public void createTable() {
	addColumnLOIid();
	addColumnTitle();
	addColumnLOIDate();
	
	addColumnLoiVendor();
	addColumnLoiGroup();
	addColumnLoiType();
	
	addColumnBranch();
	addColumnEmployee();
	addColumnApprover();

	
	addColumnDeliveryDateColumn();
	addColumnClosureDateColumn();
	addColumnStatus();
	

}

public void addColumnSorting() {
	addSortinggetLOIID();
	addSortinggetLOIDate();
	addSortinggetTitle();
	addSortingVendorName();
	addSortinggetLOICategory();
	addSortinggetLOIType();
	
	addSortingColumnBranch();
	addSortinggetEmployee();
	addSortingColumnApprover();
	
	addSortinggetClosureDate();
	addSortinggetResponseDate();
	addSortinggetStatus();

}

/************************* sorting column *************************************************/

public void addSortinggetTitle() {
	List<LetterOfIntent> list = getDataprovider().getList();
	columnSort = new ListHandler<LetterOfIntent>(list);
	columnSort.setComparator(ColumnTitle,new Comparator<LetterOfIntent>() {
				@Override
				public int compare(LetterOfIntent e1, LetterOfIntent e2) {
					if (e1 != null && e2 != null) {
						if (e1.getTitle() != null&& e2.getTitle() != null) {
							return e1.getTitle().compareTo(e2.getTitle());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}


public void addSortingVendorName() {
	List<LetterOfIntent> list = getDataprovider().getList();
	columnSort = new ListHandler<LetterOfIntent>(list);
	columnSort.setComparator(ColumnVendorName,new Comparator<LetterOfIntent>() {
				@Override
				public int compare(LetterOfIntent e1, LetterOfIntent e2) {
					if (e1 != null && e2 != null) {
						if (e1.getVendorinfo().get(0).getVendorName() != null&& e2.getVendorinfo().get(0).getVendorName()!= null) {
							return e1.getVendorinfo().get(0).getVendorName().compareTo(e2.getVendorinfo().get(0).getVendorName());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}




public void addSortingColumnTitle() {
	List<LetterOfIntent> list = getDataprovider().getList();
	columnSort = new ListHandler<LetterOfIntent>(list);
	columnSort.setComparator(ColumnTitle,
			new Comparator<LetterOfIntent>() {
				@Override
				public int compare(LetterOfIntent e1, LetterOfIntent e2) {
					if (e1 != null && e2 != null) {
						if (e1.getTitle() != null
								&& e2.getTitle() != null) {
							return e1.getTitle().compareTo(
									e2.getTitle());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void addSortingColumnBranch() {
	List<LetterOfIntent> list = getDataprovider().getList();
	columnSort = new ListHandler<LetterOfIntent>(list);
	columnSort.setComparator(ColumnBranch,
			new Comparator<LetterOfIntent>() {
				@Override
				public int compare(LetterOfIntent e1, LetterOfIntent e2) {
					if (e1 != null && e2 != null) {
						if (e1.getBranch() != null
								&& e2.getBranch() != null) {
							return e1.getBranch().compareTo(
									e2.getBranch());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void addSortingColumnApprover() {
	List<LetterOfIntent> list = getDataprovider().getList();
	columnSort = new ListHandler<LetterOfIntent>(list);
	columnSort.setComparator(ColumnAprroverName,
			new Comparator<LetterOfIntent>() {
				@Override
				public int compare(LetterOfIntent e1, LetterOfIntent e2) {
					if (e1 != null && e2 != null) {
						if (e1.getApproverName() != null
								&& e2.getApproverName() != null) {
							return e1.getApproverName().compareTo(
									e2.getApproverName());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}



public void addSortinggetStatus() {
	List<LetterOfIntent> list = getDataprovider().getList();
	columnSort = new ListHandler<LetterOfIntent>(list);
	columnSort.setComparator(statusColumn,
			new Comparator<LetterOfIntent>() {
				@Override
				public int compare(LetterOfIntent e1, LetterOfIntent e2) {
					if (e1 != null && e2 != null) {
						if (e1.getStatus() != null
								&& e2.getStatus() != null) {
							return e1.getStatus().compareTo(e2.getStatus());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void addSortinggetLOIType() {
	List<LetterOfIntent> list = getDataprovider().getList();
	columnSort = new ListHandler<LetterOfIntent>(list);
	columnSort.setComparator(loitypeColumn,
			new Comparator<LetterOfIntent>() {
				@Override
				public int compare(LetterOfIntent e1, LetterOfIntent e2) {
					if (e1 != null && e2 != null) {
						if (e1.getLoiType() != null
								&& e2.getLoiType() != null) {
							return e1.getLoiType().compareTo(
									e2.getLoiType());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void addSortinggetLOICategory() {
	List<LetterOfIntent> list = getDataprovider().getList();
	columnSort = new ListHandler<LetterOfIntent>(list);
	columnSort.setComparator(loiGroupColumn,
			new Comparator<LetterOfIntent>() {
				@Override
				public int compare(LetterOfIntent e1, LetterOfIntent e2) {
					if (e1 != null && e2 != null) {
						if (e1.getLoiGroup() != null
								&& e2.getLoiGroup() != null) {
							return e1.getLoiGroup().compareTo(
									e2.getLoiGroup());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void addSortinggetEmployee() {
	List<LetterOfIntent> list = getDataprovider().getList();
	columnSort = new ListHandler<LetterOfIntent>(list);
	columnSort.setComparator(employeeColumn,
			new Comparator<LetterOfIntent>() {
				@Override
				public int compare(LetterOfIntent e1, LetterOfIntent e2) {
					if (e1 != null && e2 != null) {
						if (e1.getEmployee() != null
								&& e2.getEmployee() != null) {
							return e1.getEmployee().compareTo(
									e2.getEmployee());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}



public void addSortinggetResponseDate() {
	List<LetterOfIntent> list = getDataprovider().getList();
	columnSort = new ListHandler<LetterOfIntent>(list);
	columnSort.setComparator(responseDateColumn,
			new Comparator<LetterOfIntent>() {
				@Override
				public int compare(LetterOfIntent e1, LetterOfIntent e2) {
					if (e1 != null && e2 != null) {
						if (e1.getDeliveryDate() != null
								&& e2.getDeliveryDate() != null) {
							return e1.getDeliveryDate().compareTo(
									e2.getDeliveryDate());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void addSortinggetClosureDate() {
	List<LetterOfIntent> list = getDataprovider().getList();
	columnSort = new ListHandler<LetterOfIntent>(list);
	columnSort.setComparator(closureDateColumn,
			new Comparator<LetterOfIntent>() {
				@Override
				public int compare(LetterOfIntent e1, LetterOfIntent e2) {
					if (e1 != null && e2 != null) {
						if (e1.getExpectedClosureDate() != null
								&& e2.getExpectedClosureDate() != null) {
							return e1.getExpectedClosureDate().compareTo(
									e2.getExpectedClosureDate());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void addSortinggetLOIDate() {
	List<LetterOfIntent> list = getDataprovider().getList();
	columnSort = new ListHandler<LetterOfIntent>(list);
	columnSort.setComparator(dateColumn, new Comparator<LetterOfIntent>() {
		@Override
		public int compare(LetterOfIntent e1, LetterOfIntent e2) {
			if (e1 != null && e2 != null) {
				if (e1.getCreationDate() != null
						&& e2.getCreationDate() != null) {
					return e1.getCreationDate().compareTo(
							e2.getCreationDate());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);

}

public void addSortinggetLOIID() {
	List<LetterOfIntent> list = getDataprovider().getList();
	columnSort = new ListHandler<LetterOfIntent>(list);
	columnSort.setComparator(LOIIDColumn, new Comparator<LetterOfIntent>() {
		@Override
		public int compare(LetterOfIntent e1, LetterOfIntent e2) {
			if (e1 != null && e2 != null) {
				if (e1.getCount() == e2.getCount()) {
					return 0;
				}
				if (e1.getCount() > e2.getCount()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);

}



/***************************************************************************/

private void addColumnLoiVendor() {
	ColumnVendorName = new TextColumn<LetterOfIntent>() {
		@Override
		public String getValue(LetterOfIntent object) {
			String venName="";
			object.getVendorinfo();
			for(int i=0;i<object.getVendorinfo().size();i++){
//				if(object.getVendorinfo().get(i).getStatus()==true){
					venName= venName+object.getVendorinfo().get(i).getVendorName()+",";
//					break;
//				}
			}
			return venName;
		}
	};
	table.addColumn(ColumnVendorName, "Vendor Name");
	table.setColumnWidth(ColumnVendorName, 130,Unit.PX);
	ColumnVendorName.setSortable(true);
}

public void addColumnTitle() {
	ColumnTitle = new TextColumn<LetterOfIntent>() {

		@Override
		public String getValue(LetterOfIntent object) {
			return object.getTitle() + "";
		}
	};
	table.addColumn(ColumnTitle, "Title");
	table.setColumnWidth(ColumnTitle, 90,Unit.PX);
	ColumnTitle.setSortable(true);
}

public void addColumnLOIDate() {
	final Date sub7Days=new Date();
	System.out.println("Todays Date ::: "+sub7Days);
	CalendarUtil.addDaysToDate(sub7Days,-7);
	System.out.println("Date - 7 Days ::: "+sub7Days);
	sub7Days.setHours(23);
	sub7Days.setMinutes(59);
	sub7Days.setSeconds(59);
	System.out.println("Converted Date ::: "+sub7Days);
	
	dateColumn = new TextColumn<LetterOfIntent>() {
		
		@Override
		public String getCellStyleNames(Context context, LetterOfIntent object) {
			if((object.getLoiDate().before(sub7Days))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Approved")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
		
		@Override
		public String getValue(LetterOfIntent object) {
			if (object.getLoiDate() != null)
				return AppUtility.parseDate(object.getLoiDate());
			else
				return " ";
		}
	};
	table.addColumn(dateColumn, "Date");
	table.setColumnWidth(dateColumn, 120,Unit.PX);
	dateColumn.setSortable(true);

}

public void addColumnStatus() {
	statusColumn = new TextColumn<LetterOfIntent>() {

		@Override
		public String getValue(LetterOfIntent object) {
			return object.getStatus();
		}
	};
	table.addColumn(statusColumn, "Status");
	table.setColumnWidth(statusColumn, 90,Unit.PX);
	statusColumn.setSortable(true);
}

public void addColumnLoiType() {
	loitypeColumn = new TextColumn<LetterOfIntent>() {

		@Override
		public String getValue(LetterOfIntent object) {
			return object.getLoiType();
		}
	};
	table.addColumn(loitypeColumn, "Type");
	table.setColumnWidth(loitypeColumn, 90,Unit.PX);
	loitypeColumn.setSortable(true);
}

public void addColumnLoiGroup() {
	loiGroupColumn = new TextColumn<LetterOfIntent>() {

		@Override
		public String getValue(LetterOfIntent object) {
			return object.getLoiGroup();
		}
	};
	table.addColumn(loiGroupColumn, "Category");
	table.setColumnWidth(loiGroupColumn, 90,Unit.PX);
	loiGroupColumn.setSortable(true);
}

public void addColumnApprover() {
	ColumnAprroverName = new TextColumn<LetterOfIntent>() {

		@Override
		public String getValue(LetterOfIntent object) {
			return object.getBranch();
		}
	};
	table.addColumn(ColumnAprroverName, "Approver");
	table.setColumnWidth(ColumnAprroverName, 90,Unit.PX);
	ColumnAprroverName.setSortable(true);
}

public void addColumnBranch() {
	ColumnBranch = new TextColumn<LetterOfIntent>() {

		@Override
		public String getValue(LetterOfIntent object) {
			return object.getBranch();
		}
	};
	table.addColumn(ColumnBranch, "Branch");
	table.setColumnWidth(ColumnBranch, 90,Unit.PX);
	ColumnBranch.setSortable(true);
}

public void addColumnEmployee() {
	employeeColumn = new TextColumn<LetterOfIntent>() {

		@Override
		public String getValue(LetterOfIntent object) {
			return object.getEmployee();
		}
	};
	table.addColumn(employeeColumn, "Purchase Engineer");
	table.setColumnWidth(employeeColumn, 160,Unit.PX);
	employeeColumn.setSortable(true);
}



public void addColumnDeliveryDateColumn() {
	responseDateColumn = new TextColumn<LetterOfIntent>() {

		@Override
		public String getValue(LetterOfIntent object) {

			if (object.getDeliveryDate() != null)
				return AppUtility.parseDate(object.getDeliveryDate());
			else
				return "";
		}
	};
	table.addColumn(responseDateColumn, "Delivery Date");
	table.setColumnWidth(responseDateColumn, 120,Unit.PX);
	responseDateColumn.setSortable(true);
}

public void addColumnClosureDateColumn() {
	closureDateColumn = new TextColumn<LetterOfIntent>() {

		@Override
		public String getValue(LetterOfIntent object) {
			if (object.getExpectedClosureDate() != null)
				return AppUtility
						.parseDate(object.getExpectedClosureDate());
			else
				return "N/A";
		}
	};
	table.addColumn(closureDateColumn, "Closure Date");
	table.setColumnWidth(closureDateColumn, 120,Unit.PX);
	closureDateColumn.setSortable(true);
}



public void addColumnLOIid() {
	LOIIDColumn = new TextColumn<LetterOfIntent>() {

		@Override
		public String getValue(LetterOfIntent object) {
			return object.getCount() + "";
		}
	};
	table.addColumn(LOIIDColumn, "ID");
	table.setColumnWidth(LOIIDColumn, 90,Unit.PX);
	LOIIDColumn.setSortable(true);
}



@Override
protected void initializekeyprovider() {
	
}

@Override
public void addFieldUpdater() {
	
}

@Override
public void setEnable(boolean state) {
	
}

@Override
public void applyStyle() {
	
}}
