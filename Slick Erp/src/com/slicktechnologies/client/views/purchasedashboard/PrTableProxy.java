package com.slicktechnologies.client.views.purchasedashboard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovalStatusPopup;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;

public class PrTableProxy extends SuperTable<PurchaseRequisition>{TextColumn<PurchaseRequisition> Columncount;
TextColumn<PurchaseRequisition> ColumnPRTitle;
TextColumn<PurchaseRequisition> ColumnCreationDate;


TextColumn<PurchaseRequisition> ColumnPRCategory;
TextColumn<PurchaseRequisition> ColumnPRType;
TextColumn<PurchaseRequisition> ColumnPRGrpup;


TextColumn<PurchaseRequisition> ColumnBranch;
TextColumn<PurchaseRequisition> ColumnPurchaseEngg;
TextColumn<PurchaseRequisition> ColumnAprroverName;

TextColumn<PurchaseRequisition> ColumnExptectedDeliveryDate;
TextColumn<PurchaseRequisition> ColumnClosureDate;
TextColumn<PurchaseRequisition> ColumnStatus;

/**
 * Date 30-01-2019 by Vijay
 * Des :- NBHC Inventory management added Approval level and Pending With approver Name
 */
Column<PurchaseRequisition,String> columnApprovalStatus;
GenricServiceAsync genservice = GWT.create(GenricService.class);
final GWTCAlert alert = new GWTCAlert(); 
public static boolean approvalStatusflag = false;
/**
 * ends here
 */

@Override
public void createTable() {
	createColumnCount();
	createColumnPRTitle();
	createColumnCreationDate();
	/**
	 * Date 30-01-2019 by Vijay
	 * Des :- NBHC Inventory management added Approval level and Pending With approver Name
	 */
	if(LoginPresenter.automaticPROrderQtyFlag){
		createColumnApprovalStatus();
	}
	/**
	 * ends here
	 */
	createColumnPRCategory();
	createColumnPRType();
	createColumnPRGroup();

	createColumnBranch();
	createColumnPurchaseEngg();
	/**
	 * Date 29-08-2019 by Vijay
	 * Des :- NBHC Inventory Management dont need below two columns in dashboard
	 */
	if(!LoginPresenter.automaticPROrderQtyFlag){
		createColumnPRApproverName();
		createColumnExpectedDelDate();
	}
	
//	createColumnClosureDate();
	createColumnStatus();
	
}


public void addColumnSorting() {
	createColumnSortingCount();
	createColumnSortingPRTitle();
	createColumnSortingExpectedDelDate();

	createColumnSortingPRCategory();
	createColumnSortingPRType();
	createColumnSortingPRGroup();
	
	createColumnSortingBranch();
	createColumnSortingPurchaseEngg();
	createColumnSortingPRApproverName();
	
	createColumnSortingCreationDate();
//	createColumnSortingClosureDate();
	createColumnSortingStatus();
}



/**************************** sorting columns *****************************************/


public void createColumnSortingPRApproverName() {
	List<PurchaseRequisition> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseRequisition>(list);
	columnSort.setComparator(ColumnAprroverName,
			new Comparator<PurchaseRequisition>() {
				@Override
				public int compare(PurchaseRequisition e1,
						PurchaseRequisition e2) {
					if (e1 != null && e2 != null) {
						if (e1.getApproverName() != null
								&& e2.getApproverName() != null) {
							return e1.getApproverName().compareTo(
									e2.getApproverName());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}

public void createColumnSortingBranch() {
	List<PurchaseRequisition> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseRequisition>(list);
	columnSort.setComparator(ColumnBranch,
			new Comparator<PurchaseRequisition>() {
				@Override
				public int compare(PurchaseRequisition e1,
						PurchaseRequisition e2) {
					if (e1 != null && e2 != null) {
						if (e1.getBranch() != null
								&& e2.getBranch() != null) {
							return e1.getBranch().compareTo(e2.getBranch());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}


public void createColumnSortingCount() {
	List<PurchaseRequisition> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseRequisition>(list);
	columnSort.setComparator(Columncount,new Comparator<PurchaseRequisition>() {
				@Override
				public int compare(PurchaseRequisition e1,
						PurchaseRequisition e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCount() == e2.getCount()) {
							return 0;
						}
						if (e1.getCount() > e2.getCount()) {
							return 1;
						} else {
							return -1;
						}
					} else {
						return 0;
					}
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void createColumnSortingPRTitle() {
	List<PurchaseRequisition> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseRequisition>(list);
	columnSort.setComparator(ColumnPRTitle,
			new Comparator<PurchaseRequisition>() {
				@Override
				public int compare(PurchaseRequisition e1,
						PurchaseRequisition e2) {
					if (e1 != null && e2 != null) {
						if (e1.getPrTitle() != null
								&& e2.getPrTitle() != null) {
							return e1.getPrTitle().compareTo(
									e2.getPrTitle());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void createColumnSortingCreationDate() {
	List<PurchaseRequisition> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseRequisition>(list);
	columnSort.setComparator(ColumnCreationDate,
			new Comparator<PurchaseRequisition>() {
				@Override
				public int compare(PurchaseRequisition e1,
						PurchaseRequisition e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCreationDate() != null
								&& e2.getCreationDate() != null) {
							return e1.getCreationDate().compareTo(
									e2.getCreationDate());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}

//public void createColumnSortingClosureDate() {
//	List<PurchaseRequisition> list = getDataprovider().getList();
//	columnSort = new ListHandler<PurchaseRequisition>(list);
//	columnSort.setComparator(ColumnClosureDate,new Comparator<PurchaseRequisition>() {
//		@Override
//		public int compare(PurchaseRequisition e1,PurchaseRequisition e2) {
//			if (e1 != null && e2 != null) {
//				if (e1.getExpectedClosureDate() != null&& e2.getExpectedClosureDate() != null) {
//					return e1.getExpectedClosureDate().compareTo(e2.getExpectedClosureDate());
//				}
//			} else {
//				return 0;
//			}
//			return 0;
//		}
//	});
//	table.addColumnSortHandler(columnSort);
//}


public void createColumnSortingExpectedDelDate() {
	List<PurchaseRequisition> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseRequisition>(list);
	columnSort.setComparator(ColumnExptectedDeliveryDate,
			new Comparator<PurchaseRequisition>() {
				@Override
				public int compare(PurchaseRequisition e1,
						PurchaseRequisition e2) {
					if (e1 != null && e2 != null) {
						if (e1.getExpectedDeliveryDate() != null
								&& e2.getExpectedDeliveryDate() != null) {
							return e1.getExpectedDeliveryDate().compareTo(
									e2.getExpectedDeliveryDate());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void createColumnSortingPurchaseEngg() {
	List<PurchaseRequisition> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseRequisition>(list);
	columnSort.setComparator(ColumnPurchaseEngg,
			new Comparator<PurchaseRequisition>() {
				@Override
				public int compare(PurchaseRequisition e1,
						PurchaseRequisition e2) {
					if (e1 != null && e2 != null) {
						if (e1.getEmployee() != null
								&& e2.getEmployee() != null) {
							return e1.getEmployee().compareTo(
									e2.getEmployee());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void createColumnSortingPRType() {
	List<PurchaseRequisition> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseRequisition>(list);
	columnSort.setComparator(ColumnPRType,
			new Comparator<PurchaseRequisition>() {
				@Override
				public int compare(PurchaseRequisition e1,
						PurchaseRequisition e2) {
					if (e1 != null && e2 != null) {
						if (e1.getPrType() != null
								&& e2.getPrType() != null) {
							return e1.getPrType().compareTo(e2.getPrType());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void createColumnSortingPRGroup() {
	List<PurchaseRequisition> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseRequisition>(list);
	columnSort.setComparator(ColumnPRGrpup,
			new Comparator<PurchaseRequisition>() {
				@Override
				public int compare(PurchaseRequisition e1,PurchaseRequisition e2) {
					if (e1 != null && e2 != null) {
						if (e1.getPrGroup() != null&& e2.getPrGroup() != null) {
							return e1.getPrGroup().compareTo(e2.getPrGroup());
						}
						if (e1.getPrGroup().equals("")&& e2.getPrGroup().equals("")) {
							return 0;
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void createColumnSortingPRCategory() {
	List<PurchaseRequisition> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseRequisition>(list);
	columnSort.setComparator(ColumnPRCategory,
			new Comparator<PurchaseRequisition>() {
				@Override
				public int compare(PurchaseRequisition e1,
						PurchaseRequisition e2) {
					if (e1 != null && e2 != null) {
						if (e1.getPrCategory() != null
								&& e2.getPrCategory() != null) {
							return e1.getPrCategory().compareTo(
									e2.getPrCategory());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

public void createColumnSortingStatus() {
	List<PurchaseRequisition> list = getDataprovider().getList();
	columnSort = new ListHandler<PurchaseRequisition>(list);
	columnSort.setComparator(ColumnStatus,
			new Comparator<PurchaseRequisition>() {
				@Override
				public int compare(PurchaseRequisition e1,
						PurchaseRequisition e2) {
					if (e1 != null && e2 != null) {
						if (e1.getStatus() != null
								&& e2.getStatus() != null) {
							return e1.getStatus().compareTo(e2.getStatus());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

/***********************************************************************************/

public void createColumnCreationDate() {
	
	final Date sub7Days=new Date();
	System.out.println("Todays Date ::: "+sub7Days);
	CalendarUtil.addDaysToDate(sub7Days,-7);
	System.out.println("Date - 7 Days ::: "+sub7Days);
	sub7Days.setHours(23);
	sub7Days.setMinutes(59);
	sub7Days.setSeconds(59);
	System.out.println("Converted Date ::: "+sub7Days);
	
	ColumnCreationDate = new TextColumn<PurchaseRequisition>() {

		@Override
		public String getCellStyleNames(Context context, PurchaseRequisition object) {
//			 if((object.getCreationDate().before(add7Days)&&object.getCreationDate().after(new Date()))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested")||object.getStatus().equals("Approved"))){
//				 return "red";
//			 }
			 if((object.getCreationDate().before(sub7Days))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Approved")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
		
		@Override
		public String getValue(PurchaseRequisition object) {
			if (object.getCreationDate() != null)
				return AppUtility.parseDate(object.getCreationDate());
			else
				return "";
		}
	};
	table.addColumn(ColumnCreationDate, "Date");
	table.setColumnWidth(ColumnCreationDate, 120,Unit.PX);
	ColumnCreationDate.setSortable(true);
}

//public void createColumnClosureDate() {
//	ColumnClosureDate = new TextColumn<PurchaseRequisition>() {
//
//		@Override
//		public String getValue(PurchaseRequisition object) {
//			if (object.getExpectedClosureDate() != null)
//				return AppUtility.parseDate(object.getExpectedClosureDate());
//			else
//				return "";
//		}
//	};
//	table.addColumn(ColumnClosureDate, "Closure Date");
//	table.setColumnWidth(ColumnClosureDate, 120,Unit.PX);
//	ColumnClosureDate.setSortable(true);
//}
public void createColumnExpectedDelDate() {
	ColumnExptectedDeliveryDate = new TextColumn<PurchaseRequisition>() {

		@Override
		public String getValue(PurchaseRequisition object) {
			if (object.getExpectedDeliveryDate() != null)
				return AppUtility.parseDate(object.getExpectedDeliveryDate());
			else
				return "";
		}
	};
	table.addColumn(ColumnExptectedDeliveryDate, "Delivery Date");
	table.setColumnWidth(ColumnExptectedDeliveryDate, 120,Unit.PX);
	ColumnExptectedDeliveryDate.setSortable(true);
}

public void createColumnPurchaseEngg() {
	ColumnPurchaseEngg = new TextColumn<PurchaseRequisition>() {

		@Override
		public String getValue(PurchaseRequisition object) {
			return object.getEmployee();
		}
	};
	table.addColumn(ColumnPurchaseEngg, "Purchase Engineer");
	table.setColumnWidth(ColumnPurchaseEngg, 160,Unit.PX);
	ColumnPurchaseEngg.setSortable(true);
}

public void createColumnBranch() {
	ColumnBranch = new TextColumn<PurchaseRequisition>() {

		@Override
		public String getValue(PurchaseRequisition object) {
			return object.getBranch();
		}
	};
	table.addColumn(ColumnBranch, "Branch");
	table.setColumnWidth(ColumnBranch, 90,Unit.PX);
	ColumnBranch.setSortable(true);
}

public void createColumnPRType() {
	ColumnPRType = new TextColumn<PurchaseRequisition>() {

		@Override
		public String getValue(PurchaseRequisition object) {
			return object.getPrType();
		}
	};
	table.addColumn(ColumnPRType, "Type");
	table.setColumnWidth(ColumnPRType, 90,Unit.PX);
	ColumnPRType.setSortable(true);
}

public void createColumnPRGroup() {
	ColumnPRGrpup = new TextColumn<PurchaseRequisition>() {

		@Override
		public String getValue(PurchaseRequisition object) {
			return object.getPrGroup();
		}
	};
	table.addColumn(ColumnPRGrpup, "Group");
	table.setColumnWidth(ColumnPRGrpup, 90,Unit.PX);
	ColumnPRGrpup.setSortable(true);
}

public void createColumnPRCategory() {
	ColumnPRCategory = new TextColumn<PurchaseRequisition>() {

		@Override
		public String getValue(PurchaseRequisition object) {
			return object.getPrCategory();
		}
	};
	table.addColumn(ColumnPRCategory, "Category");
	table.setColumnWidth(ColumnPRCategory, 90,Unit.PX);
	ColumnPRCategory.setSortable(true);
}

public void createColumnStatus() {
	ColumnStatus = new TextColumn<PurchaseRequisition>() {

		@Override
		public String getValue(PurchaseRequisition object) {
			return object.getStatus();
		}
	};
	table.addColumn(ColumnStatus, "Status");
	table.setColumnWidth(ColumnStatus, 90,Unit.PX);
	ColumnStatus.setSortable(true);
}

public void createColumnCount() {
	Columncount = new TextColumn<PurchaseRequisition>() {

		@Override
		public String getValue(PurchaseRequisition object) {
			return object.getCount() + "";
		}
	};
	table.addColumn(Columncount, "ID");
	table.setColumnWidth(Columncount, 90,Unit.PX);
	Columncount.setSortable(true);
}

public void createColumnPRTitle() {
	ColumnPRTitle = new TextColumn<PurchaseRequisition>() {

		@Override
		public String getValue(PurchaseRequisition object) {
			return object.getPrTitle();
		}
	};
	table.addColumn(ColumnPRTitle, "Title");
	table.setColumnWidth(ColumnPRTitle, 90,Unit.PX);
	ColumnPRTitle.setSortable(true);
}

public void createColumnPRApproverName() {
	ColumnAprroverName = new TextColumn<PurchaseRequisition>() {

		@Override
		public String getValue(PurchaseRequisition object) {
			return object.getApproverName();
		}
	};
	table.addColumn(ColumnAprroverName, "Approver");
	table.setColumnWidth(ColumnAprroverName, 90,Unit.PX);
	ColumnAprroverName.setSortable(true);
}




@Override
protected void initializekeyprovider() {

}

@Override
public void addFieldUpdater() {

}

@Override
public void setEnable(boolean state) {

}

@Override
public void applyStyle() {
	table.setAlwaysShowScrollBars(true);

}

	/**
	 * Date 30-01-2019 by Vijay
	 * Des :- NBHC Inventory management added Approval level and Pending With approver Name
	 */
	
		private void createColumnApprovalStatus() {
			ButtonCell buttoncell = new ButtonCell();
			columnApprovalStatus = new  Column<PurchaseRequisition, String>(buttoncell) {
				
				@Override
				public String getValue(PurchaseRequisition object) {
					return "Approval Status";
				}
			};
			table.addColumn(columnApprovalStatus, "Approval Status");
			table.setColumnWidth(columnApprovalStatus, 100, Unit.PX);
			
			
			columnApprovalStatus.setFieldUpdater(new FieldUpdater<PurchaseRequisition, String>() {
				
				@Override
				public void update(int index, PurchaseRequisition object, String value) {
					approvalStatusflag=true;

					Filter filter = new Filter();
					filter.setStringValue(AppConstants.PURCHASEREQUISION);
					filter.setQuerryString("businessprocesstype");

					Filter filterOnid = new Filter();
					filterOnid.setIntValue(object.getCount());
					filterOnid.setQuerryString("businessprocessId");

					Vector<Filter> vecfilter = new Vector<Filter>();
					vecfilter.add(filter);
					vecfilter.add(filterOnid);
					MyQuerry querry = new MyQuerry(vecfilter, new Approvals());

					genservice.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							caught.printStackTrace();
						}
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							ArrayList<Approvals> array = new ArrayList<Approvals>();
							if (result.size() != 0) {
								for (SuperModel model : result) {
									Approvals approval = (Approvals) model;
									array.add(approval);
								}
								
								Comparator<Approvals> comparator = new Comparator<Approvals>() {  
									@Override  
									public int compare(Approvals o1, Approvals o2) {
										Integer approvalCount1=o1.getCount();
										Integer approvalCount2=o2.getCount();
										return approvalCount1.compareTo(approvalCount2);  
									}  
								};  
								Collections.sort(array,comparator);
								
								ApprovalStatusPopup statusPopup=new ApprovalStatusPopup();
								PopupPanel panel=new PopupPanel(true);
								statusPopup.clear();
								statusPopup.getApprovalStatusTbl().getDataprovider().getList().addAll(array);
								panel.add(statusPopup);
								panel.center();
								panel.show();
								
				
							} else {
								alert.alert("No document found in approval.");
							}
						}
					});
				
				}
			});
		}	
		
	/**
	 * ends here	
	 */
				

}
