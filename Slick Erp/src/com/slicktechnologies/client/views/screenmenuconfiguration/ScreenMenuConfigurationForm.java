package com.slicktechnologies.client.views.screenmenuconfiguration;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.DemoConfigService;
import com.slicktechnologies.client.services.DemoConfigServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.screenmenuconfiguration.MenuConfiguration;
import com.slicktechnologies.shared.screenmenuconfiguration.ScreenMenuConfiguration;

public class ScreenMenuConfigurationForm extends FormTableScreen<ScreenMenuConfiguration> implements ClickHandler, ChangeHandler {

	CheckBox cbStatus;
	ObjectListBox<Type> oblDocumentName;
	ObjectListBox<ConfigCategory> oblModuleName;
	ListBox lbMenuName;
	
	TextBox tbName;
	TextBox tbLink;
	
	Button addMenu;
	
	
	MenuConfigurationTable menuConfigurationTbl;
	
	ScreenMenuConfiguration screenObj;
	
	public ScreenMenuConfigurationForm(SuperTable<ScreenMenuConfiguration> table,int mode, boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		table.connectToLocal();
	}
	
	private void initalizeWidget() {
		
		lbMenuName=new ListBox();
		lbMenuName.addItem("--SELECT--");
		lbMenuName.addItem("Reports");
		lbMenuName.addItem("Help");
		lbMenuName.addItem("Navigation");
		lbMenuName.addItem("Upload");
		
		cbStatus = new CheckBox();
		cbStatus.setValue(true);
		cbStatus.addClickHandler(this);

		oblModuleName = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblModuleName, Screen.MODULENAME);
		oblDocumentName = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(this.oblDocumentName, Screen.DOCUMENTNAME);
		oblModuleName.addChangeHandler(this);

		tbName=new TextBox();
		tbLink=new TextBox();
		
		addMenu = new Button("Add");
		addMenu.addClickHandler(this);
		
		menuConfigurationTbl = new MenuConfigurationTable();

	}
	
	@Override
	public void createScreen() {
		initalizeWidget();

		/**
		 *  Date : 10/02/2021 By Priyanka.
		 *  Des : Process bar buttons for Default Configuration Requirement.
		 */
		this.processlevelBarNames = new String[] { "New" , "Populate" , "Delete" , "Delete All" };

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();

		FormField fgrpingScrMenuConfg = fbuilder.setlabel("Screen Menu Configuration").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("* Module Name", oblModuleName);
		FormField foblModuleName = fbuilder.setMandatory(true).setMandatoryMsg("Module Name is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Document Type", oblDocumentName);
		FormField foblDocumentName = fbuilder.setMandatory(true).setMandatoryMsg("Document Type is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Menu Name", lbMenuName);
		FormField flbMenuName = fbuilder.setMandatory(true).setMandatoryMsg("Menu Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", cbStatus);
		FormField fcheckBoxStatus = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Name", tbName);
		FormField ftbName = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Link", tbLink);
		FormField ftbLink = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", addMenu);
		FormField faddMenu = fbuilder.setMandatory(false).setMandatoryMsg("Authorization is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", menuConfigurationTbl.getTable());
		FormField ftable = fbuilder.setMandatory(false).setColSpan(4).build();

		FormField[][] formfield = {
				{ fgrpingScrMenuConfg },
				{ foblModuleName,foblDocumentName,flbMenuName,fcheckBoxStatus },
				{ ftbName,ftbLink,faddMenu},
				{ ftable }, 
			};
		this.fields = formfield;
	}

	@Override
	public void updateModel(ScreenMenuConfiguration model) {
		if (oblModuleName.getValue() != null){
			model.setModuleName(oblModuleName.getValue());
		}
		if(oblDocumentName.getValue() != null){
			model.setDocumentName(oblDocumentName.getValue(oblDocumentName.getSelectedIndex()));
		}
		if(lbMenuName.getSelectedIndex()!= 0){
			model.setMenuName(lbMenuName.getValue(lbMenuName.getSelectedIndex()));
		}
		if (cbStatus.getValue() != null){
			model.setStatus(cbStatus.getValue());
		}
		if(menuConfigurationTbl.getValue()!=null){
			model.setMenuConfigurationList(menuConfigurationTbl.getValue());
		}
		screenObj=model;
		
		manageScreenMenuConfgDropDown(model);
		presenter.setModel(model);
	}

	@Override
	public void updateView(ScreenMenuConfiguration view) {
		screenObj=view;
		if (view.getModuleName()!= null)
			oblModuleName.setValue(view.getModuleName());
		
		if (oblModuleName.getSelectedIndex() != 0) {
			ConfigCategory cat = oblModuleName.getSelectedItem();
			if (cat != null) {
				AppUtility.makeLiveTypeDropDown(oblDocumentName,cat.getCategoryName(), cat.getCategoryCode(),cat.getInternalType());
			}
		}
		
		if (view.getDocumentName() != null)
			oblDocumentName.setValue(view.getDocumentName());
		
		cbStatus.setValue(view.isStatus());
		
		if(view.getMenuName()!=null){
			for(int i=1;i<=lbMenuName.getItemCount();i++){
				if(lbMenuName.getValue(i).equals(view.getMenuName())){
					lbMenuName.setSelectedIndex(i);
					break;
				}
			}
		}
		
		if (view.getMenuConfigurationList()!= null)
			menuConfigurationTbl.setValue(view.getMenuConfigurationList());
		presenter.setModel(view);

	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		SuperModel model=new ScreenMenuConfiguration();
		model=screenObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.USERAUTHORIZATION, screenObj.getCount(), null,null,null, false, model, null);
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		if(screenObj!=null){
			SuperModel model=new ScreenMenuConfiguration();
			model=screenObj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.USERAUTHORIZATION, screenObj.getCount(), null,null,null, false, model, null);
		}
	}
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")|| text.contains("Sillenium Test")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Edit") || text.contains("Discard")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SCREENMENUCONFIGURATION,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource() == addMenu) {
			String msg="";
			if(tbName.getValue().equals("")){
				msg=msg+"Please enter name."+"\n";
			}
			if(tbLink.getValue().equals("")){
				if(!lbMenuName.getValue(lbMenuName.getSelectedIndex()).equals("Navigation")){
					msg=msg+"Please enter link."+"\n";
				}
			}
			
			if(!msg.equals("")){
				showDialogMessage(msg);
				return;
			}
			MenuConfiguration obj=new MenuConfiguration();
			obj.setName(tbName.getValue());
			obj.setLink(tbLink.getValue());
			obj.setStatus(true);
			
			if(menuConfigurationTbl.getValue()!=null&&menuConfigurationTbl.getValue().size()!=0){
				for(MenuConfiguration menu:menuConfigurationTbl.getValue()){
					if(menu.getName().equals(obj.getName())){
						showDialogMessage(obj.getName()+" already added!");
						return;
					}
				}
			}
			
			menuConfigurationTbl.getValue().add(obj);
			
			
		}
	}
	
	@Override
	public boolean validate() {
		boolean superValidate = super.validate();
		if (superValidate == false) {
			return false;
		}
		return true;
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		menuConfigurationTbl.setEnable(state);
	}

	@Override
	public void clear() {
		super.clear();
		menuConfigurationTbl.connectToLocal();
	}

	@Override
	public void onChange(ChangeEvent event) {
		if (event.getSource().equals(oblModuleName)) {
			if (oblModuleName.getSelectedIndex() != 0) {
				ConfigCategory cat = oblModuleName.getSelectedItem();
				if (cat != null) {
					AppUtility.makeLiveTypeDropDown(oblDocumentName,cat.getCategoryName(), cat.getCategoryCode(),cat.getInternalType());
				}
			}
		}
	}
	
	public void manageScreenMenuConfgDropDown(ScreenMenuConfiguration entity) {
		ScreeenState scrState = AppMemory.getAppMemory().currentState;

		if (scrState.equals(ScreeenState.NEW)) {
			LoginPresenter.globalScrMenuConf.add(entity);
		}
		if (scrState.equals(ScreeenState.EDIT)) {
			for (int i = 0; i < LoginPresenter.globalScrMenuConf.size(); i++) {
				if (LoginPresenter.globalScrMenuConf.get(i).getId().equals(entity.getId())) {
					LoginPresenter.globalScrMenuConf.add(entity);
					LoginPresenter.globalScrMenuConf.remove(i);
					break;
				}
			}
		}
	}

}
