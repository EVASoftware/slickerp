package com.slicktechnologies.client.views.screenmenuconfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.DataMigrationService;
import com.slicktechnologies.client.services.DataMigrationServiceAsync;
import com.slicktechnologies.client.services.DataMigrationTaskQueueService;
import com.slicktechnologies.client.services.DataMigrationTaskQueueServiceAsync;
import com.slicktechnologies.client.services.DocumentUploadService;
import com.slicktechnologies.client.services.DocumentUploadServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.datamigration.DataMigrationForm;
import com.slicktechnologies.client.views.datamigration.uploadOptionPopUp;
import com.slicktechnologies.shared.ContractUploadDetails;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.MINUploadExcelDetails;
import com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.servicerelated.AssetMovementInfo;

public class UploadPopup extends AbsolutePanel implements ClickHandler {

	Button downloadFormat;
	UploadComposite upload;
	Button btnOk,btnCancel;
	
	InlineLabel headingLabel;
	
	String uploadDoc;
	String url;
	
	HashMap<String,String> transactionTypeMap=new HashMap<String, String>();
	
	DataMigrationServiceAsync datamigAsync = GWT.create(DataMigrationService.class);
	DataMigrationTaskQueueServiceAsync dataTaskAsync = GWT.create(DataMigrationTaskQueueService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	uploadOptionPopUp CustomerUploadOpt;
	DocumentUploadServiceAsync documentuploadAsync = GWT.create(DocumentUploadService.class);
	ConditionDialogBox conditionPopup = new ConditionDialogBox(
			"Do you want to cancel inactive Customer Branch's open services? It will not cancel completed services. ", AppConstants.YES,
			AppConstants.NO);
	PopupPanel panel;
	TextArea info;
	public static boolean isDownloadClickedFromDifferentProgram=false;//Ashwini Patil Date:22-11-2023
	


	private void initializeWidget(){
		addDataToMap();
		
		downloadFormat=new Button("Download Format");
		upload=new UploadComposite();
		upload.getElement().getStyle().setHeight(25, Unit.PX);
		btnOk=new Button("Ok");
		btnOk.getElement().getStyle().setWidth(80, Unit.PX);
		btnCancel=new Button("Cancel");
		btnCancel.getElement().getStyle().setWidth(80, Unit.PX);
		info=new TextArea();
		info.setHeight("300px");
		info.setWidth("550px");
		
//		btnOk.addClickHandler(this);
//		btnCancel.addClickHandler(this);
		downloadFormat.addClickHandler(this);
		
		CustomerUploadOpt = new uploadOptionPopUp();
		CustomerUploadOpt.getLblOk().addClickHandler(this);
		CustomerUploadOpt.getLblCancel().addClickHandler(this);
		
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
	}
	
	
	
	private void addDataToMap() {
		transactionTypeMap.put("Branch","Master");
		transactionTypeMap.put("City","Master");
		transactionTypeMap.put("Company Asset","Master");
		transactionTypeMap.put("Contact Person","Master");
		transactionTypeMap.put("Country","Master");
		transactionTypeMap.put("Customer","Master");
		transactionTypeMap.put("Customer Branch","Master");
		transactionTypeMap.put("Department","Master");
		transactionTypeMap.put("Employee","Master");
		transactionTypeMap.put("Item Product","Master");
		transactionTypeMap.put("Locality","Master");
		transactionTypeMap.put("Service Product","Master");
		transactionTypeMap.put("State","Master");
		transactionTypeMap.put("Stock Update","Master");
		transactionTypeMap.put("Storage Bin","Master");
		transactionTypeMap.put("Storage Location","Master");
		transactionTypeMap.put("Vendor","Master");
		transactionTypeMap.put("Warehouse","Master");
		transactionTypeMap.put("Update Employee","Master");
		transactionTypeMap.put("Update Contract","Master");
		transactionTypeMap.put("Update Leave Balance","Master");
		transactionTypeMap.put("Vendor Product Price","Master");
		transactionTypeMap.put("CTC Template","Master");
		
		
		transactionTypeMap.put("Category","Configuration");
		transactionTypeMap.put("Configurations","Configuration");
		transactionTypeMap.put("Type","Configuration");
		transactionTypeMap.put("Cron Job Configrations","Configuration");
		
		transactionTypeMap.put("Contract","Transaction");
		transactionTypeMap.put("Service","Transaction");
		transactionTypeMap.put("Payment","Transaction");
		transactionTypeMap.put("Lead","Transaction");
		transactionTypeMap.put("Customer With Service Details","Transaction");
		transactionTypeMap.put(AppConstants.MINUPLOAD,"Transaction");
		transactionTypeMap.put(AppConstants.CONTRACTUPLOAD,"Transaction");
		transactionTypeMap.put(AppConstants.UPDATEASSETDETAILS,"Transaction");
		transactionTypeMap.put("User Creation","Transaction");//Added by sheetal:31-01-2022
		
		
		transactionTypeMap.put("Multisite Contract Upload","Transaction Upload");
		transactionTypeMap.put("Category","Transaction Upload");
		
		transactionTypeMap.put(AppConstants.PAYMENTUPLOAD,AppConstants.PAYMENTUPLOAD);
		transactionTypeMap.put(AppConstants.TALLYPAYMENTUPLOAD,AppConstants.TALLYPAYMENTUPLOAD);
		
		transactionTypeMap.put("Upload Consumables","CNC");
		transactionTypeMap.put(AppConstants.ACTIVATEORDEACTIVATECUSTOMERBRANCH,AppConstants.CUSTOMERBRANCH); //Ashwini Patil Date:3-10-2023 for orion
		transactionTypeMap.put(AppConstants.SERVICEPRODUCT,AppConstants.PRODUCTMODULE); //Ashwini Patil Date:20-10-2023
		transactionTypeMap.put(AppConstants.ITEMPRODUCT,AppConstants.PRODUCTMODULE); //Ashwini Patil Date:20-10-2023
		transactionTypeMap.put(AppConstants.UpdateZohoCustomerId,"Customer");//Ashwini Patil Date:4-06-2024 for rex zoho integration
		transactionTypeMap.put(AppConstants.reverseServiceCompletionAndRemoveDuplicate,"Customer Service");
	}



	public UploadPopup() {
		initializeWidget();
		
		headingLabel=new InlineLabel();
		headingLabel.getElement().getStyle().setFontSize(15, Unit.PX);
		headingLabel.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		headingLabel.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		InlineLabel step1=new InlineLabel("Step 1 : Download template.");
		step1.getElement().getStyle().setFontSize(12, Unit.PX);
		step1.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		step1.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		InlineLabel step2=new InlineLabel("Step 2 : Read instruction on columns header.");
		step2.getElement().getStyle().setFontSize(12, Unit.PX);
		step2.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		step2.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		InlineLabel step3=new InlineLabel("Step 3 : Choose file to upload.");
		step3.getElement().getStyle().setFontSize(12, Unit.PX);
		step3.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		step3.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		add(headingLabel,175,20);
		
		add(step1,10,60);
		add(downloadFormat,10,85);
		
		add(step2,225,60);
		
		add(step3,10,125);
		add(upload,10,150);
		
		add(btnOk,100,200);
		add(btnCancel,220,200);
		
		
		setSize("450px","250px");
		this.getElement().setId("login-form");
	}
	
	
	public UploadPopup(String information) {
		initializeWidget();
		
		
		
		
		headingLabel=new InlineLabel();
		headingLabel.getElement().getStyle().setFontSize(15, Unit.PX);
		headingLabel.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		headingLabel.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		InlineLabel step1=new InlineLabel("Step 1 : Download template.");
		step1.getElement().getStyle().setFontSize(12, Unit.PX);
		step1.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		step1.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		InlineLabel step2=new InlineLabel("Step 2 : Read instruction on columns header.");
		step2.getElement().getStyle().setFontSize(12, Unit.PX);
		step2.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		step2.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		InlineLabel step3=new InlineLabel("Step 3 : Choose file to upload.");
		step3.getElement().getStyle().setFontSize(12, Unit.PX);
		step3.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		step3.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		add(headingLabel,175,20);
		
		add(info,10,10);
		
		add(step1,10,330);
		add(downloadFormat,10,355);
		
		add(step2,225,330);
		
		add(step3,10,395);
		add(upload,10,420);
		
		add(btnOk,100,470);
		add(btnCancel,220,470);
		
		
		setSize("600px","550px");
		this.getElement().setId("login-form");
	}
	
	public void clear(){
		upload.clear();
		headingLabel.setText("");
		url="";
		uploadDoc="";
		
	}
	
	public boolean validate(){
		GWTCAlert alert=new GWTCAlert();
		if(upload.getValue()==null||upload.getValue().getName()==null||upload.getValue().getName().equals("")){
			alert.alert("Please upload excel!");
			return false;
		}
		
		if(!transactionTypeMap.containsKey(uploadDoc)){
			alert.alert("Invalid upload.");
			Console.log("Invalid upload from Upload popup. uploadDoc= "+uploadDoc);
			return false;
		}		
		return true;
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		Console.log("upload popup onclick isDownloadClickedFromDifferentProgram="+isDownloadClickedFromDifferentProgram);
		if(event.getSource().equals(downloadFormat)){
			if(isDownloadClickedFromDifferentProgram)
				return;
			Window.open(url, "test", "enabled");
		}
		if (event.getSource() == CustomerUploadOpt.getLblOk()) {
			System.out.println("Here You Go for Transaction");
			Company c = new Company();
			String name =uploadDoc;
			boolean serviceStatus = CustomerUploadOpt.serviceCreate.getValue();
			boolean billingStatus = CustomerUploadOpt.billCreate.getValue();
			CustomerUploadOpt.hidePopUp();
			Console.log("Here You Go for Transaction");
			String loginPerson = LoginPresenter.loggedInUser;
			datamigAsync.savedContractTransactionDetails(c.getCompanyId(),name, serviceStatus, billingStatus, loginPerson,new AsyncCallback<ArrayList<String>>() {
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					showDialogMessage("Failed");
				}

				@Override
				public void onSuccess(ArrayList<String> result) {
					// TODO Auto-generated method stub
					ArrayList<String> error = result;
					
					if (error.contains("1")) {
						System.out.println("Inside Else of onSuccess");
						showDialogMessage("Upload Process start Sucessfully");

					} else {
						result.remove("-1");
						if(result.size()>0){
							showDialogMessage("Invalid Excel File. \n "+ result.toString());
						}else{
							showDialogMessage("Invalid Excel File. \n ");
						}
						
					}
				}
			});

		}

		if (event.getSource() == CustomerUploadOpt.getLblCancel()) {
			CustomerUploadOpt.hidePopUp();
		}
		
		if(event.getSource()==conditionPopup.getBtnOne()){
			Console.log("in conditionPopup.getBtnOne() click");
			final DocumentUploadServiceAsync datauploadService = GWT.create(DocumentUploadService.class);

			final Company company = new Company();

			showWaitSymbol();
			datauploadService.validateCustomerBranchStatusUpdation(company.getCompanyId(),true, new AsyncCallback<Integer>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					hideWaitSymbol();
				}

				@Override
				public void onSuccess(Integer result) {
					Console.log("in validateCustomerBranchStatusUpdation success");
					if(result==null||result==-1){
						hideWaitSymbol();
						showDialogMessage("Invalid file.");
						return;
					}
					if(result==-2){
						hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 218;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
					datauploadService.updateCustomerBranchStatus(company.getCompanyId(),true,new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result) {
							Console.log("in updateCustomerBranchStatus success");
							// TODO Auto-generated method stub
							hideWaitSymbol();
							showDialogMessage(result);
						}
					});
					
					
				}
			});
		
			
		
			
			
			
			
			panel.hide();
		}
		if(event.getSource()==conditionPopup.getBtnTwo()){

			Console.log("in conditionPopup.getBtnTwo() click");
			final DocumentUploadServiceAsync datauploadService = GWT.create(DocumentUploadService.class);

			final Company company = new Company();

			showWaitSymbol();
			datauploadService.validateCustomerBranchStatusUpdation(company.getCompanyId(),false, new AsyncCallback<Integer>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					hideWaitSymbol();
				}

				@Override
				public void onSuccess(Integer result) {
					Console.log("in validateCustomerBranchStatusUpdation success");
					if(result==null||result==-1){
						hideWaitSymbol();
						showDialogMessage("Invalid file.");
						return;
					}
					
					datauploadService.updateCustomerBranchStatus(company.getCompanyId(),false,new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result) {
							Console.log("in updateCustomerBranchStatus success");
							// TODO Auto-generated method stub
							hideWaitSymbol();
							showDialogMessage(result);
						}
					});
					
					
				}
			});
		
		
			panel.hide();
		}
	}
	
	
	public void uploadDocuments(PopupPanel popPanel){
		String transactionType=transactionTypeMap.get(uploadDoc);
		Console.log("transactionType="+transactionType);
		Console.log("uploadDoc="+uploadDoc);
		if(transactionType.equals("Master")){
			uploadMasterDocuments(uploadDoc,popPanel);
		}else if(transactionType.equals("Configuration")){
			uploadConfigurationDocuments(uploadDoc,popPanel);
		}else if(transactionType.equals("Transaction")){
			uploadTransactionDocuments(uploadDoc,popPanel);
		}else if(transactionType.equals("Transaction Upload")){
			if(uploadDoc.equals("Multisite Contract Upload")){
				multiSiteContractUpload(popPanel);
			}else if(uploadDoc.equals("Purchase Requisition Upload")||uploadDoc.equals("Purchase Requisition Upload With Master Price")){
				boolean flag=false;
				if(uploadDoc.equals("Purchase Requisition Upload With Master Price")){
					flag=true;
				}
				purchaseRequisitionUpload(flag,popPanel);
			}
		}
		else if(transactionType.equals(AppConstants.PAYMENTUPLOAD)){
			reactonPaymentUpload(popPanel);
		}
		else if(transactionType.equals(AppConstants.TALLYPAYMENTUPLOAD)){
			reactonTallyPaymentUpload(popPanel);
			
		}
		else if(transactionType.equals(AppConstants.CUSTOMERBRANCH)){//Ashwini Patil Date:3-10-2023 for orion
			if(uploadDoc.equals(AppConstants.ACTIVATEORDEACTIVATECUSTOMERBRANCH))
				reactOnActivateDeactivateCustomerBranch(popPanel);
			
		}
		else if(transactionType.equals(AppConstants.PRODUCTMODULE)){//Ashwini Patil Date:3-10-2023 for orion
			if(uploadDoc.equals(AppConstants.SERVICEPRODUCT))
				reactOnProductUpload(AppConstants.SERVICEPRODUCT);
			if(uploadDoc.equals(AppConstants.ITEMPRODUCT))
				reactOnProductUpload(AppConstants.ITEMPRODUCT);
		}else if(transactionType.equals("Customer")){
			if(uploadDoc.equals(AppConstants.UpdateZohoCustomerId))
				reactonUpdateZohoCustomerId();
		}else if(transactionType.equals("Customer Service")&&uploadDoc.equals(AppConstants.reverseServiceCompletionAndRemoveDuplicate)) {
			reactonReverseServiceCompletion();
		}
	}
	
	

	



	public Button getDownloadFormat() {
		return downloadFormat;
	}

	public void setDownloadFormat(Button downloadFormat) {
		this.downloadFormat = downloadFormat;
	}

	public UploadComposite getUpload() {
		return upload;
	}

	public void setUpload(UploadComposite upload) {
		this.upload = upload;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}



	public InlineLabel getHeadingLabel() {
		return headingLabel;
	}



	public void setHeadingLabel(InlineLabel headingLabel) {
		this.headingLabel = headingLabel;
	}



	public String getUploadDoc() {
		return uploadDoc;
	}



	public void setUploadDoc(String uploadDoc) {
		this.uploadDoc = uploadDoc;
	}



	public String getUrl() {
		return url;
	}



	public void setUrl(String url) {
		this.url = url;
	}



	public HashMap<String, String> getTransactionTypeMap() {
		return transactionTypeMap;
	}



	public void setTransactionTypeMap(HashMap<String, String> transactionTypeMap) {
		this.transactionTypeMap = transactionTypeMap;
	}
	
	public void showDialogMessage(String msg){
		GWTCAlert alert=new GWTCAlert();
		alert.alert(msg);
	}
	protected GWTCGlassPanel glassPanel;
	public void showWaitSymbol() {
		glassPanel = new GWTCGlassPanel();
		glassPanel.show();
		
		
	}
	public void hideWaitSymbol() {
	   glassPanel.hide();
		
	}
	
	public void uploadTransactionDocuments(final String name, final PopupPanel popPanel){
		Company c=new Company();

		if (name.equals("Employee Asset Allocation")) {
			showWaitSymbol();
			datamigAsync = GWT.create(DataMigrationService.class);
			datamigAsync.saveEmployeeAsset(UserConfiguration.getCompanyId(),new AsyncCallback<ArrayList<EmployeeAsset>>() {
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					hideWaitSymbol();
				}
	
				@Override
				public void onSuccess(ArrayList<EmployeeAsset> result) {
					// TODO Auto-generated method stub
					if (result == null) {
						hideWaitSymbol();
						showDialogMessage("Invalid file.");
						return;
					}
	
					datamigAsync.validateEmployeeAsset(result,new AsyncCallback<ArrayList<EmployeeAsset>>() {
	
					@Override
					public void onFailure(Throwable caught) {
						hideWaitSymbol();
					}
	
					@Override
					public void onSuccess(ArrayList<EmployeeAsset> result) {
	
						for (EmployeeAsset obj : result) {
							if (obj.getCount() == -1) {
								hideWaitSymbol();
								String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url = gwt+ "csvservlet"+ "?type="+ 173;
								Window.open(url,"test","enabled");
								showDialogMessage("Please find error file.");
								return;
							}
						}
						
						datamigAsync.uploadEmployeeAsset(result,new AsyncCallback<ArrayList<EmployeeAsset>>() {
	
							@Override
							public void onFailure(Throwable caught) {
								hideWaitSymbol();
							}
	
							@Override
							public void onSuccess(ArrayList<EmployeeAsset> result) {
								hideWaitSymbol();
								showDialogMessage("Employee asset upload process started.");
								String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url = gwt+ "csvservlet"+ "?type="+ 173;
								Window.open(url,"test","enabled");
								popPanel.hide();
							}
						});
					}
				});
				}
			});

		} else if (name.equals("Customer With Service Details")) {
			CustomerUploadOpt.showPopUp();
			CustomerUploadOpt.serviceCreate.setValue(false);
			CustomerUploadOpt.billCreate.setValue(false);
			System.out.println("GEt herer... ");
		} else if (name.equals(AppConstants.MINUPLOAD)) {
			showWaitSymbol();
			String loginPerson = LoginPresenter.loggedInUser;
			datamigAsync = GWT.create(DataMigrationService.class);
			datamigAsync.savedUploadTransactionDetails(c.getCompanyId(), name, false, false, loginPerson,new AsyncCallback<MINUploadExcelDetails>() {
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("Failed");
					hideWaitSymbol();
				}
	
				@Override
				public void onSuccess(MINUploadExcelDetails resultOne) {
					if (resultOne.getErrorList().size() > 0) {
						showDialogMessage("Invalid Excel File");
						csvservice.setMinUploadError(resultOne.getErrorList(),new AsyncCallback<Void>() {
							@Override
							public void onFailure(Throwable caught) {
								System.out.println("RPC call Failed"+ caught);
								hideWaitSymbol();
							}
	
							@Override
							public void onSuccess(Void result) {
								String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url = gwt+ "csvservlet"+ "?type="+ 130;
								Window.open(url,"test","enabled");
								hideWaitSymbol();
							}
						});
					} else {
						MINUploadExcelDetails mindtTwo = new MINUploadExcelDetails();
						mindtTwo = resultOne;
						datamigAsync.saveMinUploadProcess(mindtTwo,new AsyncCallback<MINUploadExcelDetails>() {
	
						@Override
						public void onSuccess(MINUploadExcelDetails resultTwo) {
							if (resultTwo.getErrorList().size() > 0) {
								showDialogMessage("Invalid Excel File");
								csvservice.setMinUploadError(resultTwo.getErrorList(),new AsyncCallback<Void>() {
	
									@Override
									public void onFailure(Throwable caught) {
										System.out.println("RPC call Failed"+ caught);
										hideWaitSymbol();
									}
	
									@Override
									public void onSuccess(Void result) {
										String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
										final String url = gwt+ "csvservlet"+ "?type="+ 130;
										Window.open(url,"test","enabled");
										hideWaitSymbol();
									}
								});
							} else {
	
								MINUploadExcelDetails mindtThree = new MINUploadExcelDetails();
								mindtThree = resultTwo;
								datamigAsync.checkMinUploadServiceListDetails(mindtThree,new AsyncCallback<MINUploadExcelDetails>() {
	
								@Override
								public void onSuccess(MINUploadExcelDetails resultThree) {
									if (resultThree.getErrorList().size() > 0) {
	
										showDialogMessage("Invalid Excel File");
										csvservice.setMinUploadError(resultThree.getErrorList(),new AsyncCallback<Void>() {
	
											@Override
											public void onFailure(Throwable caught) {
												System.out.println("RPC call Failed"+ caught);
												hideWaitSymbol();
											}
	
											@Override
											public void onSuccess(Void result) {
												String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
												final String url = gwt+ "csvservlet"+ "?type="+ 130;
												Window.open(url,"test","enabled");
												hideWaitSymbol();
											}
										});
	
									} else {
										MINUploadExcelDetails mindtFour = new MINUploadExcelDetails();
										mindtFour = resultThree;
										datamigAsync.updateProductInventoryViewList(mindtFour,new AsyncCallback<MINUploadExcelDetails>() {
	
										@Override
										public void onSuccess(MINUploadExcelDetails resultFour) {
	
											if (resultFour.getErrorList().size() > 0) {
	
												showDialogMessage("Invalid Excel File");
												csvservice.setMinUploadError(resultFour.getErrorList(),new AsyncCallback<Void>() {
	
													@Override
													public void onFailure(Throwable caught) {
														System.out.println("RPC call Failed"+ caught);
														hideWaitSymbol();
													}
	
													@Override
													public void onSuccess(Void result) {
														String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
														final String url = gwt+ "csvservlet"+ "?type="+ 130;
														Window.open(url,"test","enabled");
														hideWaitSymbol();
													}
												});
	
											} else {
												MINUploadExcelDetails mindtFive = new MINUploadExcelDetails();
												mindtFive = resultFour;
												datamigAsync.ServiceUploadToCompleteStatus(mindtFive,new AsyncCallback<MINUploadExcelDetails>() {
	
												@Override
												public void onSuccess(MINUploadExcelDetails resultFive) {
													ArrayList<String> error = resultFive.getErrorList();
	
													if (error.contains("1")) {
														hideWaitSymbol();
														showDialogMessage("Data Upload Process  Start..!!");
													}
													if (!error.contains("1")&& error.size() > 0) {
														csvservice.setMinUploadError(error,new AsyncCallback<Void>() {
	
															@Override
															public void onFailure(Throwable caught) {
																System.out.println("RPC call Failed"+ caught);
																hideWaitSymbol();
															}
	
															@Override
															public void onSuccess(Void result) {
																hideWaitSymbol();
																String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
																final String url = gwt+ "csvservlet"+ "?type="+ 130;
																Window.open(url,"test","enabled");
															}
														});
													}
	
												}
	
												@Override
												public void onFailure(Throwable caught) {
													System.out.println("RPC call Failed"+ caught);
													hideWaitSymbol();
												}
											});
											}
										}
	
										@Override
										public void onFailure(Throwable caught) {
											System.out.println("RPC call Failed"+ caught);
											hideWaitSymbol();
										}
									});
									}
								}
	
								@Override
								public void onFailure(Throwable caught) {
									System.out.println("RPC call Failed"+ caught);
									hideWaitSymbol();
									showDialogMessage("Error while Uploaded excel details");
								}
							});
							}
	
						}
	
						@Override
						public void onFailure(Throwable caught) {
							System.out.println("RPC call Failed"+ caught);
							hideWaitSymbol();
						}
					});
					}
				}
			});
			
		}/**
		 * end
		 **/
		/**
		 * nidhi 2-11-2017 for nbhc contract Upload process
		 */
		else if (name.equals(AppConstants.CONTRACTUPLOAD)) {
			showWaitSymbol();
			String loginPerson = LoginPresenter.loggedInUser;
			datamigAsync.savedContractUploadTransactionDetails(c.getCompanyId(), name, loginPerson,new AsyncCallback<ContractUploadDetails>() {

				@Override
				public void onFailure(Throwable caught) {
					
				}

				@Override
				public void onSuccess(ContractUploadDetails resultOne) {
					
					if(resultOne.getErrorList().size()>0){

						showDialogMessage("Invalid Excel File");
						csvservice.setMinUploadError(resultOne.getErrorList(), new AsyncCallback<Void>() {

							@Override
							public void onFailure(Throwable caught) {
								System.out.println("RPC call Failed"+caught);
								hideWaitSymbol();
							}

							@Override
							public void onSuccess(Void result) {
								String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url=gwt + "csvservlet"+"?type="+131;
								Window.open(url, "test", "enabled");
								hideWaitSymbol();
							}
						});
					
					}else{
						datamigAsync.checkContractUploadDetails(resultOne,new AsyncCallback<ContractUploadDetails>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								System.out.println("RPC call Failed"+caught);
								hideWaitSymbol();
							}

							@Override
							public void onSuccess(ContractUploadDetails resultTwo) {
								if(resultTwo.getErrorList().size()>0){

									showDialogMessage("Invalid Excel File");
									csvservice.setMinUploadError(resultTwo.getErrorList(), new AsyncCallback<Void>() {

										@Override
										public void onFailure(Throwable caught) {
											System.out.println("RPC call Failed"+caught);
											hideWaitSymbol();
										}

										@Override
										public void onSuccess(Void result) {
											String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
											final String url=gwt + "csvservlet"+"?type="+131;
											Window.open(url, "test", "enabled");
											hideWaitSymbol();
										}
									});
								}else{
									datamigAsync.CheckUploadContractDetail(resultTwo, new AsyncCallback<ContractUploadDetails>() {
										
										@Override
										public void onSuccess(ContractUploadDetails resultThree) {
											if(resultThree.getErrorList().size()>0){

												showDialogMessage("Invalid Excel File");
												csvservice.setMinUploadError(resultThree.getErrorList(), new AsyncCallback<Void>() {

													@Override
													public void onFailure(Throwable caught) {
														System.out.println("RPC call Failed"+caught);
														hideWaitSymbol();
													}

													@Override
													public void onSuccess(Void result) {
														String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
														final String url=gwt + "csvservlet"+"?type="+131;
														Window.open(url, "test", "enabled");
														hideWaitSymbol();
													}
												});
											}else{
												datamigAsync.UploadContractDetails(resultThree, new AsyncCallback<ContractUploadDetails>() {
													
													@Override
													public void onSuccess(ContractUploadDetails resultFour) {
														// TODO Auto-generated method stub
														if(resultFour.getErrorList().size()>0){

															showDialogMessage("Invalid Excel File");
															csvservice.setMinUploadError(resultFour.getErrorList(), new AsyncCallback<Void>() {

																@Override
																public void onFailure(Throwable caught) {
																	System.out.println("RPC call Failed"+caught);
																	hideWaitSymbol();
																}

																@Override
																public void onSuccess(Void result) {
																	String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
																	final String url=gwt + "csvservlet"+"?type="+131;
																	Window.open(url, "test", "enabled");
																	hideWaitSymbol();
																}
															});
														}else{
															datamigAsync.finallyUploadContractDetails(resultFour, new AsyncCallback<ArrayList<String>>() {
																
																@Override
																public void onSuccess(ArrayList<String> result) {
																	// TODO Auto-generated method stub
																	if(result.size() >1 && !result.contains("1")){

																		showDialogMessage("Invalid Excel File");
																		csvservice.setMinUploadError(result, new AsyncCallback<Void>() {

																			@Override
																			public void onFailure(Throwable caught) {
																				System.out.println("RPC call Failed"+caught);
																				hideWaitSymbol();
																			}

																			@Override
																			public void onSuccess(Void result) {
																				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
																				final String url=gwt + "csvservlet"+"?type="+131;
																				Window.open(url, "test", "enabled");
																				hideWaitSymbol();
																			}
																		});
																	}else if(result.contains("1") && result.size() == 1){
																		showDialogMessage("Contract data uploading process started successfully.!! ");
																		hideWaitSymbol();
																	}else{
																		showDialogMessage("There is some error Problem. Uploading process stoped.");
																		hideWaitSymbol();
																	}
																}
																
																@Override
																public void onFailure(Throwable caught) {
																	// TODO Auto-generated method stub
																	System.out.println("RPC call Failed"+caught);
																	hideWaitSymbol();
																}
															});
														}
													}
													
													@Override
													public void onFailure(Throwable caught) {
														// TODO Auto-generated method stub
														
													}
												});
											}
											
										}
										
										@Override
										public void onFailure(Throwable caught) {
											// TODO Auto-generated method stub
											System.out.println("RPC call Failed"+caught);
											hideWaitSymbol();
										}
									});
								}
							}
						} );
					}
					
				}
			});
			
		
		}else if(name.equals(AppConstants.UPDATEASSETDETAILS)){
			reactonUpdateAssetDetails();
		}
		else if(name.equals(AppConstants.CTCALOCATIONUPLOAD)){
			reactonCTCAllocationUpload();
		}
		else if(name.equals("User Creation")){
			reactonUserCreation();
		}
		
		 else {
			datamigAsync.savedTransactionDetails(c.getCompanyId(),name, new AsyncCallback<ArrayList<Integer>>() {
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("Failed");
				}

				@Override
				public void onSuccess(ArrayList<Integer> result) {
					if (result.contains(-1)) {
						showDialogMessage("Invalid Excel File");
					}else if (result.contains(-2)) {
						showDialogMessage("Customer does not exist Please create first");
					} else if (result.contains(-3)) {
						showDialogMessage("Product does not exist Please create first");
					}else {
						showDialogMessage("Successfull");
					}
				}
			});
		}
	
	}
	
	public void uploadConfigurationDocuments(final String name, final PopupPanel popPanel){
		Company c=new Company();
		datamigAsync.savedConfigDetails(c.getCompanyId(), name,new AsyncCallback<ArrayList<Integer>>() {
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("Failed");
			}

			@Override
			public void onSuccess(ArrayList<Integer> result) {
				if(name.equalsIgnoreCase("Category")){
					if(result.contains(-1)){
						Console.log("inside Category on success error occurs");
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 188;
						Window.open(url, "test", "enabled");
						return;
					}
					else{
						Console.log("Category upload successfully");
						showDialogMessage("Category upload successfully.");
						popPanel.hide();
					}
				}
				
				if(name.equalsIgnoreCase("Type")){
					if(result.contains(-1)){
						Console.log("inside Category on success error occurs");
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 189;
						Window.open(url, "test", "enabled");
						return;
					}
					else{
						Console.log("Type upload successfully");
						showDialogMessage("Type upload successfully.");
						popPanel.hide();
					}
				}
				
				
				
				if (result.contains(-1)) {
					showDialogMessage("Invalid Excel File");
				} else {
					System.out.println("Inside Else of onSuccess");
					if(name.equalsIgnoreCase("Category")||name.equalsIgnoreCase("Type")){
					}else{
						showDialogMessage("Successfull");
						popPanel.hide();
					}
					Company c=new Company();
					String categoryType=AppConstants.GLOBALRETRIEVALCATEGORY+"-"+c.getCompanyId();
					LoginPresenter.globalMakeLiveCategory(categoryType);
					String configType=AppConstants.GLOBALRETRIEVALCONFIG+"-"+c.getCompanyId();
					LoginPresenter.globalMakeLiveConfig(configType);
					String typeType=AppConstants.GLOBALRETRIEVALTYPE+"-"+c.getCompanyId();
					LoginPresenter.globalMakeLiveType(typeType);
				}
			}
		});
	}
	
	public void uploadMasterDocuments(final String name, final PopupPanel popPanel){
		
		if(name.equalsIgnoreCase("Update Leave Balance")){
			updateLeaveBalance(popPanel);
			return;
		}else if(name.equalsIgnoreCase("Customer Branch")){
			saveCustomerBranch(popPanel);
			return;
		}else if(name.equalsIgnoreCase("Vendor Product Price")){
			reactonVendorProductPrice(popPanel);
			return;
		}else if(name.equalsIgnoreCase("CTC Template")){
			reactonCTCTemplate(popPanel);
			return;
		}
		
		Company c=new Company();
		datamigAsync.savedRecordsDeatils(c.getCompanyId(), name, true,LoginPresenter.loggedInUser,new AsyncCallback<ArrayList<Integer>>() {
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("Failed");

			}

			@Override
			public void onSuccess(ArrayList<Integer> result) {

				if (name.equalsIgnoreCase("Update Employee")) {
					if (result.contains(-1)) {
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet"+ "?type=" + 186;
						Window.open(url, "test", "enabled");
						return;
					} else {
						Console.log("Employee update successfully");
						showDialogMessage("Employee  update successfully.");
						popPanel.hide();
					}
				}
				if (name.equalsIgnoreCase("City")) {
					if (result.contains(-1)) {
						Console.log("inside update employee on success error occurs");
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet"+ "?type=" + 191;
						Window.open(url, "test", "enabled");
						return;
					} else {
						Console.log("City update successfully");
						showDialogMessage("City update successfully");
						popPanel.hide();
					}
				}
				if (name.equalsIgnoreCase("Locality")) {
					Console.log("in if (name.equalsIgnoreCase(Locality))" );
					if (result.contains(-1)) {
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet"+ "?type=" + 192;
						Window.open(url, "test", "enabled");
						return;
					} else {
						Console.log("Locality update successfully");
						showDialogMessage("Locality update successfully");
						popPanel.hide();
					}
				}

				if (result.contains(-1)) {
					showDialogMessage("Invalid Excel File");
				} else if (result.contains(-2)) {
					showDialogMessage("Error: Branch Name Already Exist!!");
				} else if (result.contains(-3)) {
					showDialogMessage("Error: City Name Already Exist!!");
				} else if (result.contains(-4)) {
					showDialogMessage("Error: Country Name Already Exist!!");
				} else if (result.contains(-5)) {
					showDialogMessage("Error: Customer Already Exist!!");
				} else if (result.contains(-6)) {
					showDialogMessage("Error: Department And Parent Department Already Exist!!");
				} else if (result.contains(-7)) {
					showDialogMessage("Error: Employee Name Already Exist!!");
				} else if (result.contains(-8)) {
					showDialogMessage("Error: Product Code Already Exist!!");
				} else if (result.contains(-9)) {
					showDialogMessage("Error: Locality Name Already Exist!!");
				} else if (result.contains(-10)) {
					showDialogMessage("Error: Procode Code Already Exist!!");
				} else if (result.contains(-11)) {
					showDialogMessage("Error: State Name Already Exist!!");
				} else if (result.contains(-12)) {
					showDialogMessage("Error: Storage Bin Already Exist!!");
				} else if (result.contains(-13)) {
					showDialogMessage("Error: Storage Location Already Exist!!");
				} else if (result.contains(-14)) {
					showDialogMessage("Error: Vendor Name Already Exist!!");
				} else if (result.contains(-15)) {
					showDialogMessage("Error: WareHouse Already Exist!!");
				} else if (result.contains(-16)) {
					showDialogMessage("Error: Asset Name Already Exists!!");
				} else if (result.contains(-17)) {
					showDialogMessage("Error: Please set date format to date column!!");
				} else if (result.contains(-18)) {
					showDialogMessage("Error: Please upload .xls format file!!");
				} else if (result.contains(-19)) {
					showDialogMessage("Error: Stock is already exist!!");
				} else if (result.contains(-20)) {
					showDialogMessage("Error: Contract/customer id does not exist!!");
				} else if (result.contains(-21)) {
					showDialogMessage("Error: No contract found for updation!!");
				} else if (result.contains(-22)) {
					showDialogMessage("Error: Customer branch is already exist!!");
				} else if (result.contains(-23)) {
					showDialogMessage("Error: Customer does not exist!!");
				} else if (result.contains(-24)) {
					showDialogMessage("Error: Please enter values in all Mandatory Fields !!");
				} else if (result.contains(-25)) {
					showDialogMessage("Error: Servicing Branch does not exist or does not match Please check !!");
				} else if (result.contains(-26)) {
					showDialogMessage("Error: City does not exist or does not match Please check !!");
				} else if (result.contains(-27)) {
					showDialogMessage("Error: Duplicate employee name found in the excel please check !!");
				} else {
					System.out.println("Successfully");
					if (name.equalsIgnoreCase("Update Employee")
							|| name.equalsIgnoreCase("Locality")
							|| name.equalsIgnoreCase("City")) {
					} else {
						showDialogMessage("Sucessfully");
						popPanel.hide();
					}
				}
			}
		});
	}
	
	private void updateLeaveBalance(final PopupPanel popPanel) {
		showWaitSymbol();
		dataTaskAsync.getEmployeeLeaveDetails(UserConfiguration.getCompanyId(),new AsyncCallback<ArrayList<EmployeeLeaveBalance>>() {
			@Override
			public void onFailure(Throwable caught) {
				hideWaitSymbol();
				System.out.println("First FAIL RPC...");
				popPanel.hide();
			}
			@Override
			public void onSuccess(ArrayList<EmployeeLeaveBalance> result) {
				System.out.println("FIRST RPC...");
				if (result != null && result.size() != 0) {
					if (result.get(0).getEmpId() == -1) {
						hideWaitSymbol();
						showDialogMessage("Invalid excel format.");
					} else {
						dataTaskAsync.validateEmployeeLeaveDetails(result,new AsyncCallback<ArrayList<EmployeeLeaveBalance>>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated
								// method stub
								hideWaitSymbol();
								System.out.println("SECOND FAIL RPC...");
								popPanel.hide();
							}

							@Override
							public void onSuccess(ArrayList<EmployeeLeaveBalance> result) {
								System.out.println("SECOND RPC...");
								if (result.get(0).getEmpId() == -1) {
									hideWaitSymbol();
									showDialogMessage("Please find excel.");

									String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url = gwt+ "csvservlet"+ "?type="+ 164;
									Window.open(url,"test","enabled");
								} else {
									dataTaskAsync.updateLeaveBalance(result,new AsyncCallback<String>() {
										@Override
										public void onFailure(Throwable caught) {
											hideWaitSymbol();
											popPanel.hide();
										}

										@Override
										public void onSuccess(String result) {
											hideWaitSymbol();
											showDialogMessage("Leave balance update successful.");
											popPanel.hide();
										}
									});
								}
							}
						});
					}
				}
			}
		});
	}
	
	private void saveCustomerBranch(PopupPanel popPanel){
		Company c = new Company();
		showWaitSymbol();
		datamigAsync =  GWT.create(DataMigrationService.class);
		datamigAsync.uploadCustomerBranch(c.getCompanyId(), new AsyncCallback<ArrayList<CustomerBranchDetails>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<CustomerBranchDetails> result) {
				// TODO Auto-generated method stub
				if(result==null){
					hideWaitSymbol();
					showDialogMessage("Invalid Excel File Format.");
					return;
				}
				if(result.size() == 1 && result.get(0).getCount() == -2){
					hideWaitSymbol();
					showDialogMessage("You can only upload 150 branches.");
					return;
				}
				for(CustomerBranchDetails obj:result){
					if(obj.getCount()==-1){
						hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 178;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
				}
				
				datamigAsync.saveCustomerBranch(result, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
						showDialogMessage("Customer branch upload process started.");
					}
				});
				
			}
		});
	}
	
	private void reactonVendorProductPrice(PopupPanel popPanel) {
		final Company c = new Company();
		showWaitSymbol();
		datamigAsync =  GWT.create(DataMigrationService.class);
		datamigAsync.uploadVendorProductPrice(c.getCompanyId(), new AsyncCallback<ArrayList<VendorPriceListDetails>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();

			}

			@Override
			public void onSuccess(ArrayList<VendorPriceListDetails> result) {

				if(result==null){
					hideWaitSymbol();
					showDialogMessage("Invalid file.");
					return;
				}
				for(VendorPriceListDetails obj:result){
					if(obj.getCount()==-1){
						hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 184;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
				}
				datamigAsync.vednorProductPrice(c.getCompanyId(), "Vendor Product Price", new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						showDialogMessage(result);
						hideWaitSymbol();
					}
				} );
				
			}
		});
	}
	
	
	
	private void reactonCTCTemplate(PopupPanel popPanel) {

		final Company c = new Company();
		showWaitSymbol();
		datamigAsync =  GWT.create(DataMigrationService.class);
		
		datamigAsync.validateCTCTemplate(c.getCompanyId(), new AsyncCallback<ArrayList<CTCTemplate>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<CTCTemplate> result) {
				// TODO Auto-generated method stub


				if(result==null){
					hideWaitSymbol();
					showDialogMessage("Invalid file.");
					return;
				}
				for(CTCTemplate obj:result){
					if(obj.getCount()==-1){
						hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 185;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
				}
			
				datamigAsync.uploadCTCTemplate(c.getCompanyId(), "CTC Template", new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						showDialogMessage(result);
						hideWaitSymbol();
					}
				});
				
			}
		});
	}
	
	private void reactonUpdateAssetDetails() {

		final Company c = new Company();
		showWaitSymbol();
		
		datamigAsync.validateUpdateAssetDetails(c.getCompanyId(), new AsyncCallback<ArrayList<AssetMovementInfo>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<AssetMovementInfo> result) {
				// TODO Auto-generated method stub
				if(result==null){
					hideWaitSymbol();
					showDialogMessage("Invalid file.");
					return;
				}
				for(AssetMovementInfo obj:result){
					if(obj.getCount()==-1){
						hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 187;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
				}
				
				datamigAsync.uploadAssetDetails(c.getCompanyId(), AppConstants.UPDATEASSETDETAILS, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						hideWaitSymbol();	
					}

					@Override
					public void onSuccess(String result) {
						showDialogMessage(result);
						hideWaitSymbol();						
					}
				});
			}
		});
	}
	
	public void multiSiteContractUpload(final PopupPanel popPanel){

		String entityName = "Contract Upload";
		final Company comp = new Company();
		showWaitSymbol();
		documentuploadAsync.readContractUploadExcelFile(entityName, comp.getCompanyId(), new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<String> contractExcellist) {
				// TODO Auto-generated method stub
				if(contractExcellist.size()==1){
					showDialogMessage(contractExcellist.get(0));
					hideWaitSymbol();
				}
				else{
					documentuploadAsync.reactonValidateContractUpload(contractExcellist, comp.getCompanyId(), new AsyncCallback<ArrayList<String>>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							hideWaitSymbol();
						}

						@Override
						public void onSuccess(ArrayList<String> contractexcel) {
							// TODO Auto-generated method stub
							if(contractexcel.size()==1){
								showDialogMessage(contractexcel.get(0));
								hideWaitSymbol();
							}
							else{
								documentuploadAsync.reactonContractUpload(contractexcel.get(1), comp.getCompanyId(), new AsyncCallback<String>() {

									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										hideWaitSymbol();
									}

									@Override
									public void onSuccess(String result) {
										// TODO Auto-generated method stub
										showDialogMessage(result);
										hideWaitSymbol();
										popPanel.hide();
									}
								});
							}
						}
					});
				 }
			}
		});
	}
	
	public void purchaseRequisitionUpload(final boolean masterPriceFlag, final PopupPanel popPanel){
//		final boolean masterPriceFlag=false;
//		if(name.equals("Purchase Requisition Upload With Master Price")){
//			masterPriceFlag=true;
//		}
		
		String entityName="Purchase Requisition Upload";
		final Company company = new Company();
		if(entityName.equals("Purchase Requisition Upload")){ 
		
		   String blobkeyURL=upload.getValue().getUrl();
		   showWaitSymbol();
		   documentuploadAsync.reactOnPurchaseRequisitionExcelRead(entityName,company.getCompanyId(),blobkeyURL,new AsyncCallback<ArrayList<String>>() {
				 @Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					showDialogMessage(caught.getMessage());	
					hideWaitSymbol();
				}
				@Override
				public void onSuccess(ArrayList<String> excelData) {
					// TODO Auto-generated method stub
					if (excelData.size() == 1) {
						showDialogMessage(excelData.get(0));
						hideWaitSymbol();
					}else{
						System.out.println(" Excel Heading read Sucessfully Validation Strat  ");
						documentuploadAsync.reactOnPurchaseRequisitionExcelValidation(masterPriceFlag,excelData,company.getCompanyId(),new AsyncCallback<ArrayList<String>>() {
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								showDialogMessage("Please Check Enter Values");	
								hideWaitSymbol();
							}
							@Override
							public void onSuccess(ArrayList<String> prExcelData) {
								// TODO Auto-generated method stub
								if (prExcelData.size() == 1) {
									showDialogMessage(prExcelData.get(0));
									hideWaitSymbol();
								}else{
									documentuploadAsync.reactOnPurchaseRequisitionExcelUpload(masterPriceFlag,prExcelData.get(1),company.getCompanyId(),new AsyncCallback<String>() {
										@Override
										public void onFailure(Throwable caught) {
											hideWaitSymbol();	
										}
		
										@Override
										public void onSuccess(String result) {
											showDialogMessage(result);
											hideWaitSymbol();	
											popPanel.hide();
										}
									});
								}
							}
					});
				}
				}
		});
	 }	
	
	}
	
	
	private void reactonPaymentUpload(final PopupPanel popPanel) {
		
		
		final DocumentUploadServiceAsync datauploadService = GWT.create(DocumentUploadService.class);

		final Company company = new Company();

		showWaitSymbol();
		datauploadService.validatePaymentUploader(company.getCompanyId(), new AsyncCallback<ArrayList<CustomerPayment>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<CustomerPayment> result) {
				if(result==null){
					hideWaitSymbol();
					showDialogMessage("Invalid file.");
					popPanel.hide();
					return;
				}
				for(CustomerPayment obj:result){
					if(obj.getCount()==-1){
						hideWaitSymbol();
						popPanel.hide();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 190;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
				}
				
				datauploadService.uploadPaymentDetails(company.getCompanyId(), "Payment Updation", new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
						showDialogMessage(result);
						popPanel.hide();
					}
				});
				
				
			}
		});
	
		
	}

	private void reactonTallyPaymentUpload(final PopupPanel popPanel) {
		final Company company = new Company();
		final DocumentUploadServiceAsync datauploadService = GWT.create(DocumentUploadService.class);
		showWaitSymbol();
		datauploadService.validateTallyPaymentUploader(company.getCompanyId(), new AsyncCallback<ArrayList<CustomerPayment>>() {

			@Override
			public void onFailure(Throwable arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<CustomerPayment> result) {
				// TODO Auto-generated method stub
				if(result==null){
					hideWaitSymbol();
					showDialogMessage("Excel File Header Not Matched");
					popPanel.hide();
					return;
				}
				for(CustomerPayment obj:result){
					if(obj.getCount()==-1){
						hideWaitSymbol();
						popPanel.hide();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 193;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
				}
				
				datauploadService.uploadTallyPaymentDetails(company.getCompanyId(), "Payment Updation", new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable arg0) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
					}

					@Override
					public void onSuccess(String result1) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
						showDialogMessage(result1);
						popPanel.hide();
					}
				});
			}
		});
	
		
	}

	
	private void reactonCTCAllocationUpload() {

		final Company c = new Company();
		showWaitSymbol();
		String loggedInUser = LoginPresenter.loggedInUser;
		datamigAsync.validateCTCAllocation(c.getCompanyId(),loggedInUser, new AsyncCallback<ArrayList<CTCTemplate>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<CTCTemplate> result) {


				// TODO Auto-generated method stub
				if(result==null){
					hideWaitSymbol();
					showDialogMessage("Invalid Excel File Format.");
					return;
				}
				for(CTCTemplate obj:result){
					if(obj.getCount()==-1){
						hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 198;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
				}
				
				showDialogMessage("CTC Allocation Upload Process Started");

				hideWaitSymbol();
				
			}
		});
		
	}
	/**Added by sheetal:31-01-2022 
	   Des: for user creation upload program**/
	
	private void reactonUserCreation() {
		final Company c = new Company();
		showWaitSymbol();
		datamigAsync.validateAndUploadUser(c.getCompanyId(), new AsyncCallback<ArrayList<Employee>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<Employee> result) {
				// TODO Auto-generated method stub
				if(result==null){
					hideWaitSymbol();
					showDialogMessage("Invalid Excel File Format!");
					return;
				}
				for(Employee obj:result){
					if(obj.getCount()==-1){
						hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 206;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
				}
				
				showDialogMessage("User Creation Upload Process Started!");

				hideWaitSymbol();
				
			}
		});
	}
	
	private void reactOnActivateDeactivateCustomerBranch(final PopupPanel popPanel){
		popPanel.hide();
		panel = new PopupPanel(true);
		panel.add(conditionPopup);
		panel.setGlassEnabled(true);
		panel.show();
		panel.center();
	}
	
	
	//Ashwini Patil Date:20-10-2023 As per Nitin sir upload programs from data migration and screen should call the same common method so copying code from datamigrationpresenter 
	private void reactOnProductUpload(final String name){
		final Company c = new Company();
		datamigAsync.savedRecordsDeatils(c.getCompanyId(), name, true,LoginPresenter.loggedInUser,
				new AsyncCallback<ArrayList<Integer>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(ArrayList<Integer> result) {
						// TODO Auto-generated method stub
						if (name.equalsIgnoreCase("Service Product")) {
							if (result.contains(-1)) {
								String gwt = com.google.gwt.core.client.GWT
										.getModuleBaseURL();
								final String url = gwt + "csvservlet"
										+ "?type=" + 196;
								Window.open(url, "test", "enabled");
								showDialogMessage("Please find error file!");
							} else if(result.contains(-2)) {
								showDialogMessage("Excel sheet header not matched Please get latest Excel sheet to upload! ");
							}
							else if(result.contains(0)){
								Console.log("Service Product Upload Process Started");
								showDialogMessage("Service Product Upload Process Started");
							}
							else{
								Console.log("Failed"+result);
							}
								
							return;
						}
						if (name.equalsIgnoreCase("Item Product")) {
							if (result.contains(-1)) {
								String gwt = com.google.gwt.core.client.GWT
										.getModuleBaseURL();
								final String url = gwt + "csvservlet"
										+ "?type=" + 197;
								Window.open(url, "test", "enabled");
								showDialogMessage("Please find error file!");
							} else if(result.contains(-2)) {
								showDialogMessage("Excel sheet header not matched Please get latest Excel sheet to upload! ");
							}
							else if(result.contains(0)){
								Console.log("Item Product Upload Process Started");
								showDialogMessage("Item Product Upload Process Started");
							}
							else{
								Console.log("Failed"+result);
							}
								
							return;
						}
										
							
					}
			
		});
	}
	public TextArea getInfo() {
		return info;
	}

	public void setInfo(TextArea info) {
		this.info = info;
	}

	public void reactonUpdateZohoCustomerId() {
		Console.log("in reactonUpdateZohoCustomerId");
		Company c = new Company();
		showWaitSymbol();
		documentuploadAsync.UpdateZohoCustomerId(new AsyncCallback<ArrayList<String>>() {
			
			@Override
			public void onSuccess(ArrayList<String> result) {
				if(result!=null)
					Console.log("result size="+result.size());
				
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 194;
				Window.open(url, "test", "enabled");
				showDialogMessage("Please find file for the updates.");
				hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("Failed");
				hideWaitSymbol();
			}
		});
	}
	
	public void reactonReverseServiceCompletion() {

		Console.log("in reactonReverseServiceCompletion");
		Company c = new Company();
		showWaitSymbol();
		documentuploadAsync.ReverseServiceCompletionAndRemoveDuplicate(new AsyncCallback<ArrayList<String>>() {
			
			@Override
			public void onSuccess(ArrayList<String> result) {
				if(result!=null)
					Console.log("result size="+result.size());
				
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 222;
				Window.open(url, "test", "enabled");
				showDialogMessage("Please find file for the updates.");
				hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("Failed");
				hideWaitSymbol();
			}
		});
	
	}
}
