package com.slicktechnologies.client.views.screenmenuconfiguration;

import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.services.DemoConfigService;
import com.slicktechnologies.client.services.DemoConfigServiceAsync;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.screenmenuconfiguration.ScreenMenuConfiguration;

public class ScreenMenuConfigurationPresenter extends FormTableScreenPresenter<ScreenMenuConfiguration>  {
	ScreenMenuConfigurationForm form;
	DemoConfigServiceAsync democonfigAsync = GWT.create(DemoConfigService.class);
	public ScreenMenuConfigurationPresenter(FormTableScreen<ScreenMenuConfiguration> view,ScreenMenuConfiguration model) {
		super(view, model);
		form = (ScreenMenuConfigurationForm) view;
		form.getSupertable().connectToLocal();
		form.menuConfigurationTbl.connectToLocal();
		form.retriveTable(getUserQuery());
		form.setPresenter(this);
	}

	public static ScreenMenuConfigurationForm initialize() {
		ScreenMenuConfigurationTable gentableScreen = new ScreenMenuConfigurationTable();
		ScreenMenuConfigurationForm form = new ScreenMenuConfigurationForm(gentableScreen,FormTableScreen.UPPER_MODE, true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		ScreenMenuConfigurationPresenter presenter = new ScreenMenuConfigurationPresenter(form, new ScreenMenuConfiguration());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	public MyQuerry getUserQuery() {
		MyQuerry quer = new MyQuerry(new Vector<Filter>(), new ScreenMenuConfiguration());
		return quer;
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals("New")) {
			form.setToNewState();
			initialize();
		}
		
		if(text.equals("Populate")){
			reactOnDefaultConfiguration();
		}
		
		if(text.equals("Delete")){
			reactOnDelete();
		}
		
		if(text.equals("Delete All")){
			reactOnDeleteAll();
		}
	}

	private void reactOnDeleteAll() {
		System.out.println("in side default configuration");
		Company comp = new Company();
		democonfigAsync.deleteAllScreenConfiguration(comp.getCompanyId(), new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Window.alert("Failed Please try again..!! ");
				caught.printStackTrace();


			}

			@Override
			public void onSuccess(String result) {
				
				form.getSupertable().connectToLocal();
				Window.alert("Screen Menu Configuration Data delete Successfully..!");
	
			}
		});
		
		
	}

	@Override
	public void reactOnDelete() {
		// TODO Auto-generated method stub
		form.getSupertable().getDataprovider().getList().remove(model);
		super.reactOnDelete();
	}

	private void reactOnDefaultConfiguration() {
		System.out.println("in side default configuration");
		Company comp = new Company();
		democonfigAsync.createScreenMenuData(comp.getCompanyId(), new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Window.alert("Failed Please try again..!! ");
				caught.printStackTrace();


			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				Window.alert("Screen Menu Configuration Process Started Successfully.It will take some time to update..!");
	
			}
		});
		
	}


	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void makeNewModel() {
		model = new ScreenMenuConfiguration();
	}
	
	public void setModel(ScreenMenuConfiguration entity) {
		model = entity;
	}
}
