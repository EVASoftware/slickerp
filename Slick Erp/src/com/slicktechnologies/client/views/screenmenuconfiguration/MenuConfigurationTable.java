package com.slicktechnologies.client.views.screenmenuconfiguration;

import java.util.Date;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.screenmenuconfiguration.MenuConfiguration;

public class MenuConfigurationTable extends SuperTable<MenuConfiguration> {

	TextColumn<MenuConfiguration> menuNameCol;
	TextColumn<MenuConfiguration> linkCol;
	Column<MenuConfiguration, String> deleteColumn;
	Column<MenuConfiguration, Boolean> statusColumn;
	TextColumn<MenuConfiguration> viewStatusColumn;

	public MenuConfigurationTable() {
		super();
	}

	@Override
	public void createTable() {
		addColumnMenuName();
		addColumnLink();
		addColumnStatus();
		setFieldUpdaterOnStatus();
		addColumngetdelete();
		createFieldUpdaterdeleteColumn();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}

	private void addColumnMenuName() {
		menuNameCol = new TextColumn<MenuConfiguration>() {
			@Override
			public String getValue(MenuConfiguration object) {
				return object.getName().trim();
			}
		};
		table.addColumn(menuNameCol, "Name");
		table.setColumnWidth(menuNameCol, 200, Unit.PX);
		menuNameCol.setSortable(true);
	}

	private void addColumnLink() {
		linkCol = new TextColumn<MenuConfiguration>() {
			@Override
			public String getValue(MenuConfiguration object) {
				return object.getLink().trim();
			}
		};
		table.addColumn(linkCol, "Type");
		table.setColumnWidth(linkCol, 200, Unit.PX);
		linkCol.setSortable(true);
	}

	protected void addColumnViewgetstatus() {
		viewStatusColumn = new TextColumn<MenuConfiguration>() {
			@Override
			public String getValue(MenuConfiguration object) {
				return object.isStatus() + "";
			}
		};
		table.addColumn(viewStatusColumn, "Status");
	}

	private void addColumnStatus() {
		CheckboxCell cb = new CheckboxCell();
		statusColumn = new Column<MenuConfiguration, Boolean>(cb) {

			@Override
			public Boolean getValue(MenuConfiguration object) {
				return object.isStatus();
			}
		};
		table.addColumn(statusColumn, "Status");
		table.setColumnWidth(statusColumn, 200, Unit.PX);
		statusColumn.setSortable(true);
	}

	private void addColumngetdelete() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<MenuConfiguration, String>(btnCell) {

			public String getValue(MenuConfiguration object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
	}

	private void createFieldUpdaterdeleteColumn() {
		deleteColumn.setFieldUpdater(new FieldUpdater<MenuConfiguration, String>() {
			public void update(int index, MenuConfiguration object,String value) {
				getDataprovider().getList().remove(index);
				table.redrawRow(index);
			}
		});
	}

	protected void setFieldUpdaterOnStatus() {
		statusColumn.setFieldUpdater(new FieldUpdater<MenuConfiguration, Boolean>() {
			@Override
			public void update(int index, MenuConfiguration object,
					Boolean value) {
				object.setStatus(value);
				getDataprovider().getList();
				table.redrawRow(index);
			}
		});
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setEnable(boolean state) {

		for (int i = table.getColumnCount() - 1; i > -1; i--) {
			table.removeColumn(i);
		}
		if (state == true) {
			addColumnMenuName();
			addColumnLink();
			addColumnStatus();
			setFieldUpdaterOnStatus();
			addColumngetdelete();
			createFieldUpdaterdeleteColumn();
		}else {
			addColumnMenuName();
			addColumnLink();
			addColumnViewgetstatus();
		}
	}

	@Override
	public void applyStyle() {

	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<MenuConfiguration>() {
			@Override
			public Object getKey(MenuConfiguration item) {
				if (item == null) {
					return null;
				} else
					return new Date().getTime();
			}
		};

	}

}
