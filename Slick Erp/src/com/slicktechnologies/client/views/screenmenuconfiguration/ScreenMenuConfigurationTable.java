package com.slicktechnologies.client.views.screenmenuconfiguration;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.screenmenuconfiguration.ScreenMenuConfiguration;

public class ScreenMenuConfigurationTable extends SuperTable<ScreenMenuConfiguration> {
	
	TextColumn<ScreenMenuConfiguration> moduleNameCol;
	TextColumn<ScreenMenuConfiguration> documentNameCol;
	
	TextColumn<ScreenMenuConfiguration> menuNameCol;
	TextColumn<ScreenMenuConfiguration> statusCol;
	
	public ScreenMenuConfigurationTable() {
		super();
	}

	@Override
	public void createTable() {
		addColumnModuleName();
		addColumnDocumentName();
		addColumnMenuName();
		addColumnStatus();
	}

	private void addColumnMenuName() {
		menuNameCol = new TextColumn<ScreenMenuConfiguration>() {
			@Override
			public String getValue(ScreenMenuConfiguration object) {
				return object.getMenuName().trim();
			}
		};
		table.addColumn(menuNameCol, "Menu Name");
		table.setColumnWidth(menuNameCol, 130, Unit.PX);
		menuNameCol.setSortable(true);
	}

	private void addColumnModuleName() {
		moduleNameCol = new TextColumn<ScreenMenuConfiguration>() {
			@Override
			public String getValue(ScreenMenuConfiguration object) {
				return object.getModuleName().trim();
			}
		};
		table.addColumn(moduleNameCol, "Module Name");
		table.setColumnWidth(moduleNameCol, 130, Unit.PX);
		moduleNameCol.setSortable(true);
	}
	
	private void addColumnDocumentName() {
		documentNameCol = new TextColumn<ScreenMenuConfiguration>() {
			@Override
			public String getValue(ScreenMenuConfiguration object) {
				return object.getDocumentName().trim();
			}
		};
		table.addColumn(documentNameCol, "Document Name");
		table.setColumnWidth(documentNameCol, 130, Unit.PX);
		documentNameCol.setSortable(true);
	}
	
	private void addColumnStatus() {
		statusCol=new TextColumn<ScreenMenuConfiguration>() {
			@Override
			public String getValue(ScreenMenuConfiguration object) {
				if(object.isStatus()==true){
					return "Active";
				}
				return "Inactive";	
			}
		};
		table.addColumn(statusCol, "Status");
		table.setColumnWidth(statusCol, 90, Unit.PX);
		statusCol.setSortable(true);
	}
	
	public void addColumnSorting(){
		addSortingOnModuleCol();
		addSortingOnDocumentNameCol();
		addSortingOnMenuNameCol();
		addSortingOnStatusCol();
	}

	

	

	private void addSortingOnModuleCol() {
		List<ScreenMenuConfiguration> list = getDataprovider().getList();
		columnSort = new ListHandler<ScreenMenuConfiguration>(list);
		columnSort.setComparator(moduleNameCol,new Comparator<ScreenMenuConfiguration>() {
			@Override
			public int compare(ScreenMenuConfiguration e1,ScreenMenuConfiguration e2) {
				if (e1 != null && e2 != null) {
					if (e1.getModuleName() != null&& e2.getModuleName() != null) {
						return e1.getModuleName().compareTo(e2.getModuleName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addSortingOnDocumentNameCol() {
		List<ScreenMenuConfiguration> list = getDataprovider().getList();
		columnSort = new ListHandler<ScreenMenuConfiguration>(list);
		columnSort.setComparator(documentNameCol,new Comparator<ScreenMenuConfiguration>() {
			@Override
			public int compare(ScreenMenuConfiguration e1,ScreenMenuConfiguration e2) {
				if (e1 != null && e2 != null) {
					if (e1.getDocumentName() != null&& e2.getDocumentName() != null) {
						return e1.getDocumentName().compareTo(e2.getDocumentName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addSortingOnMenuNameCol() {
		List<ScreenMenuConfiguration> list = getDataprovider().getList();
		columnSort = new ListHandler<ScreenMenuConfiguration>(list);
		columnSort.setComparator(menuNameCol,new Comparator<ScreenMenuConfiguration>() {
			@Override
			public int compare(ScreenMenuConfiguration e1,ScreenMenuConfiguration e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMenuName() != null&& e2.getMenuName() != null) {
						return e1.getMenuName().compareTo(e2.getMenuName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addSortingOnStatusCol() {
		List<ScreenMenuConfiguration> list = getDataprovider().getList();
		columnSort = new ListHandler<ScreenMenuConfiguration>(list);
		columnSort.setComparator(statusCol,new Comparator<ScreenMenuConfiguration>() {
			@Override
			public int compare(ScreenMenuConfiguration e1,ScreenMenuConfiguration e2) {
				if (e1 != null && e2 != null) {
					if (e1.isStatus() == e2.isStatus())
						return 0;
					else if (e1.isStatus() == true)
						return 1;
					else
						return -1;
				} else
					return -1;
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	@Override
	protected void initializekeyprovider() {
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
