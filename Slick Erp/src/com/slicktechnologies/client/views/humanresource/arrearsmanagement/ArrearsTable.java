package com.slicktechnologies.client.views.humanresource.arrearsmanagement;

import java.util.Date;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.common.arrears.ArrearsInfoBean;

public class ArrearsTable extends SuperTable<ArrearsInfoBean> {

	TextColumn<ArrearsInfoBean> nameColumn , operationColumn;
	Column<ArrearsInfoBean,String> deleteColumn;
	TextColumn<ArrearsInfoBean> viewFromDateCol;
	TextColumn<ArrearsInfoBean> viewDateToCol;
	TextColumn<ArrearsInfoBean> prodPriceColumn;
	Column<ArrearsInfoBean , String> prodEditPriceColumn;
	TextColumn<ArrearsInfoBean> viewCompName;
	TextColumn<ArrearsInfoBean> getProjectNameCol;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		createColumnnameColumn();
		createEditPriceColumn();
		createViewFromDateColumn();
		createViewToDateColumn();
		createOperationColumn();
		createCompName();
		getProjectNameCol();
		createColumndeleteColumn();
		addFieldUpdater();
	}

	private void getProjectNameCol() {
		// TODO Auto-generated method stub
		getProjectNameCol = new TextColumn<ArrearsInfoBean>() {
			@Override
			public String getValue(ArrearsInfoBean object) {
				if(object.getProjectName()!=null){
					return object.getProjectName();
				}
				return "";
			}
		};
		table.addColumn(getProjectNameCol, "Project");
		// table.setColumnWidth(prodNameColumn, 100,Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		createFieldUpdaterdeleteColumn();
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		 for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
		 
		 if(state == true){
			 	createColumnnameColumn();
			 	createEditPriceColumn();
				createViewFromDateColumn();
				createViewToDateColumn();
				createOperationColumn();
				createCompName();
				getProjectNameCol();
				createColumndeleteColumn();
				addFieldUpdater();

				}else{
					//table.setWidth("100%");
					createColumnnameColumn();
					createViewPriceColumn();
					createViewFromDateColumn();
					createViewToDateColumn();
					createOperationColumn();
					createCompName();
					getProjectNameCol();
				}
		}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	protected void createColumnnameColumn()
	{
	
		nameColumn=new TextColumn<ArrearsInfoBean>()
				{
			@Override
			public String getValue(ArrearsInfoBean object)
			{
				return object.getHead();
			}
				};
				table.addColumn(nameColumn,"Name");
			//	table.setColumnWidth(nameColumn, 100,Unit.PX);
	}
	
	protected void createOperationColumn()
	{
		
		operationColumn=new TextColumn<ArrearsInfoBean>()
				{
			@Override
			public String getValue(ArrearsInfoBean object)
			{
				if(object.getOperation()!=null){
					return object.getOperation();
				}
				return "";
			}
			};
				table.addColumn(operationColumn,"Operation");
			//	table.setColumnWidth(operationColumn, 100,Unit.PX);
	
	}

	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<ArrearsInfoBean,String>(btnCell)
				{
			@Override
			public String getValue(ArrearsInfoBean object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
			//	table.setColumnWidth(deleteColumn, 100,Unit.PX);
	}
		protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<ArrearsInfoBean,String>()
				{
			@Override
			public void update(int index,ArrearsInfoBean object,String value)
			{
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	protected void createViewFromDateColumn() {
		viewFromDateCol = new TextColumn<ArrearsInfoBean>() {
			@Override
			public String getValue(ArrearsInfoBean object) {
				if (object.getFromDate() != null) {
					return object.getFromDate();
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(viewFromDateCol, "From Date");
	//	table.setColumnWidth(viewFromDateCol, 100, Unit.PX);
	}
	
	protected void createViewToDateColumn() {
		viewDateToCol = new TextColumn<ArrearsInfoBean>() {
			@Override
			public String getValue(ArrearsInfoBean object) {
				if (object.getToDate() != null) {
					return object.getToDate();
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(viewDateToCol, "To Date");
		//table.setColumnWidth(viewDateToCol, 100, Unit.PX);
	}
	protected void createEditPriceColumn()
	{
		EditTextCell editCell=new EditTextCell();
		prodEditPriceColumn=new Column<ArrearsInfoBean,String>(editCell)
				{
			@Override
			public String getValue(ArrearsInfoBean object)
			{
				if(object.getAmount()==0){
					return 0+"";}
				else{
					return object.getAmount()+"";}
			}
				};
				table.addColumn(prodEditPriceColumn,"# Amount");
			//	table.setColumnWidth(prodEditPriceColumn, 120,Unit.PX);
	}
	
	
	protected void createViewPriceColumn()
	{
		
		prodPriceColumn=new TextColumn<ArrearsInfoBean>()
				{
			@Override
			public String getValue(ArrearsInfoBean object)
			{
				if(object.getAmount()==0){
					return 0+"";}
				else{
					return object.getAmount()+"";}
			}
				};
				table.addColumn(prodPriceColumn,"Amount");
			//	table.setColumnWidth(prodPriceColumn, 100,Unit.PX);
	}
	protected void createCompName()
	{
		viewCompName=new TextColumn<ArrearsInfoBean>()
		{
			@Override
			public String getValue(ArrearsInfoBean object)
			{
				return object.getEarningCompName();
			}
		};
			table.addColumn(viewCompName,"Component Name");
			//	table.setColumnWidth(prodNameColumn, 100,Unit.PX);
	}
}
