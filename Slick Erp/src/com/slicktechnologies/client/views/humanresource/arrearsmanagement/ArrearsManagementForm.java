package com.slicktechnologies.client.views.humanresource.arrearsmanagement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.arrears.ArrearsDetails;
import com.slicktechnologies.shared.common.arrears.ArrearsInfoBean;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.hrtaxstructure.EmployeeInvestment;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

public class ArrearsManagementForm extends FormScreen<ArrearsDetails> implements ClickHandler{
	
	EmployeeInfoComposite pic;
	Button btnAdd;
	ArrearsTable arrearsTable;
	ArrearsDetails arrearsDetails;
	ObjectListBox<String> olbOperation;
	DateBox dbFromDate;
	DateBox dbToDate;
	TextBox tbHead;
	DoubleBox dblAmount;
	
	GenricServiceAsync async = GWT.create(GenricService.class);
	
	public ArrearsManagementForm(){
		super();
		btnAdd.addClickHandler(this);
		createGui();
	}
	
	public ArrearsManagementForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		btnAdd.addClickHandler(this);
		createGui();
		
	}

	private void initalizeWidget()
	{
		pic=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		
		
		btnAdd = new Button("ADD");
		arrearsTable = new ArrearsTable();
		arrearsTable.connectToLocal();
		olbOperation  = new ObjectListBox<String>();
		olbOperation.addItem("--SELECT--");
		olbOperation.addItem("ADD");
		olbOperation.addItem("DEDUCT");
//		dbFromDate = new DateBox();
		dbFromDate=new DateBoxWithYearSelector();
//		dbToDate = new DateBox();
		dbToDate=new DateBoxWithYearSelector();
		tbHead = new TextBox();
		dblAmount = new DoubleBox();
		}

	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames=new String[]{AppConstants.NEW};
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeInformation=fbuilder.setlabel("Employee Arrears Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("",pic);
		FormField fpic= fbuilder.setMandatory(true).setMandatoryMsg("Employee information is mandatory").setRowSpan(0).setColSpan(6).build();
		
			
		fbuilder = new FormFieldBuilder("",btnAdd);
		FormField fbtnAdd= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",arrearsTable.getTable());
		FormField fTable= fbuilder.setRowSpan(0).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("Head", tbHead);
		FormField ftbHead= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("From Date", dbFromDate);
		FormField fdbFromDate= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("To Date", dbToDate);
		FormField fdbToDate= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Operation", olbOperation);
		FormField folbOperation= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Amount", dblAmount);
		FormField fdblAmount= fbuilder.setRowSpan(0).setColSpan(0).build();
		setFormat();
		
				
		FormField[][] formfield = {   
				{fgroupingEmployeeInformation},
				{fpic},
				{ftbHead,fdblAmount ,fdbFromDate ,fdbToDate ,folbOperation , fbtnAdd },
				{fTable}
				
		};

		this.fields=formfield;		

	}


	@Override
	public void updateModel(ArrearsDetails model) 
	{
		if(pic.getValue()!=null)
		{
			model.setEmpId(pic.getEmployeeId());
			model.setEmpName(pic.getEmployeeName());
			model.setEmpCellNo(pic.getCellNumber());

		}
		if(arrearsTable.getDataprovider().getList().size()>0){
			List<ArrearsInfoBean> list = arrearsTable.getDataprovider().getList();
			model.setArrearsList(list);
		}
		
		arrearsDetails=model;
		
		presenter.setModel(model);


	}


	@Override
	public void updateView(ArrearsDetails view) 
	{
		arrearsDetails=view;
		
		pic.setEmployeeId(view.getEmpId());
		if(view.getEmpName()!=null)
			pic.setEmployeeName(view.getEmpName());	
			pic.setCellNumber(view.getEmpCellNo());
		if(view.getArrearsList().size()>0){
			arrearsTable.getDataprovider().setList(view.getArrearsList());
		}
		
		presenter.setModel(view);
	}

	

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION) || text.equals("Search"))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false); 
				}
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Search")/*||text.equals("Print")*/||text.equals(AppConstants.NAVIGATION)||text.contains("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.EMPLOYEEINVESTMENT,LoginPresenter.currentModule.trim());
	}

	@Override
	public void setCount(int count)
	{
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		this.arrearsTable.setEnable(state);
		
	}

	public EmployeeInfoComposite getPic() {
		return pic;
	}

	public void setPic(EmployeeInfoComposite pic) {
		this.pic = pic;
	}

	
	public Button getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(Button btnAdd) {
		this.btnAdd = btnAdd;
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new EmployeeInvestment();
		model=arrearsDetails;
		AppUtility.addDocumentToHistoryTable(AppConstants.HRMODULE,"Adhoc Settlement", arrearsDetails.getCount(), arrearsDetails.getEmpId(),arrearsDetails.getEmpName(),arrearsDetails.getEmpCellNo(), false, model, null);		
	}
	

	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
			
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		boolean val= super.validate();
		if(val==false)
			return val;
		if(arrearsTable.getDataprovider().getList().size()==0){
			showDialogMessage("Employee Arrears List can't be empty");
			return false;
		}
		return true;
	}


	@Override
	public void clear() {
		super.clear();
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(btnAdd)){
			if(tbHead.getValue() == null){
				showDialogMessage("Please enter Head.");
				return;
			}
			if(dbFromDate.getValue() == null || dbToDate == null){
				showDialogMessage("From date and to date are mandatory.");
				return;
			}
			if(olbOperation.getSelectedIndex()==0){
				showDialogMessage("Please select operation.");
				return;
			}
			if(dblAmount.getValue()==null){
				showDialogMessage("Please add amount.");
				return;
			}
				ArrearsInfoBean bean = getArrearsBeanObject();
				
				if(arrearsTable.getDataprovider().getList().size()>0){
					List<ArrearsInfoBean> list = arrearsTable.getDataprovider().getList();
					list.add(bean);
					arrearsTable.getDataprovider().setList(list);
					}else{
						arrearsTable.getDataprovider().getList().add(bean);
					}
				
		
		}
	}

	public void retriveEmployeeArrears() {
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = new Filter();
		
		filter.setQuerryString("empId");
		filter.setIntValue(pic.getEmployeeId());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ArrearsDetails());

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							showDialogMessage("Employee Arrears Already Exist!");
							pic.clear();
						}
					}
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("No Record Found");
					}
				});
	}

	
	public void setFormat()
	{
		/*********************Date Time Format***************************************************/
		DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MMM");
		DateBox.DefaultFormat defformat=new DateBox.DefaultFormat(format);
		dbFromDate.setFormat(defformat);
		//dbFromDate.setValue(new Date());
		dbToDate.setFormat(defformat);
	//	dbFromDate.setValue(new Date());
		/*****************************************************************************************/
	}
	public String getFromDate()
	{
		DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MMM");
		if(dbFromDate.getValue()==null){
			return null;
		}
		else
		{
			return format.format(dbFromDate.getValue());
		}
	}
	public String getToDate()
	{
		DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MMM");
		if(dbToDate.getValue()==null){
			return null;
		}
		else
		{
			return format.format(dbToDate.getValue());
		}
	}
	private ArrearsInfoBean getArrearsBeanObject(){
		ArrearsInfoBean bean = new ArrearsInfoBean();
		if(tbHead.getValue()!=null){
			bean.setHead(tbHead.getValue());
		}
		if(dbFromDate.getValue()!=null){
			bean.setFromDate(getFromDate());
		}
		if(dbToDate.getValue()!=null){
			bean.setToDate(getToDate());
		}
		if(olbOperation.getSelectedIndex()!=0){
			bean.setOperation(olbOperation.getValue(olbOperation.getSelectedIndex()));
		}
		if(dblAmount.getValue()!=null){
			bean.setAmount(dblAmount.getValue());
		}
		return bean;
	}
}
