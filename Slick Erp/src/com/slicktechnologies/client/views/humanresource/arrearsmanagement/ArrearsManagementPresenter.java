package com.slicktechnologies.client.views.humanresource.arrearsmanagement;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.common.arrears.ArrearsDetails;
import com.slicktechnologies.shared.common.cnc.CNC;

public class ArrearsManagementPresenter extends FormScreenPresenter<ArrearsDetails> implements RowCountChangeEvent.Handler , ChangeHandler ,SelectionHandler<Suggestion>{



	ArrearsManagementForm form;
	protected GenricServiceAsync service = GWT.create(GenricService.class);
	
	CsvServiceAsync csvservice = GWT.create(CsvService.class);

	
	public ArrearsManagementPresenter(FormScreen<ArrearsDetails> view, ArrearsDetails model) {
		super(view, model);
		form = (ArrearsManagementForm) view;
		form.setPresenter(this);
		form.arrearsTable.getTable().addRowCountChangeHandler(this);
		form.getSearchpopupscreen().getPopup().setSize("100%", "100%");
//		form.earningtable.getTable().addRowCountChangeHandler(this);
//		form.setEnable(false);
	
		form.pic.getId().addSelectionHandler(this);
		form.pic.getName().addSelectionHandler(this);
		form.pic.getPhone().addSelectionHandler(this);
		//		form.setToViewState();

	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		if(text.equals(AppConstants.NEW)){
			reactToNew();
		}
	}
	@Override
	public void reactOnPrint() {
//		if(form.pic!=null){
		//	final String url = GWT.getModuleBaseURL() + "processpayslip" + "?Id="+ model.getId();
	//		Window.open(url, "test", "enabled");
//		}
	}

	@Override
	public void reactOnEmail() {

	}

	@Override
	public void reactOnDownload() {
		ArrayList<ArrearsDetails> adhocsettlementArray=new ArrayList<ArrearsDetails>();
		List<ArrearsDetails> list=(List<ArrearsDetails>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();

		adhocsettlementArray.addAll(list);
		csvservice.setAdhocsettlementlist(adhocsettlementArray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}

			@Override
			public void onSuccess(Void result) {

				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+158;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	@Override
	protected void makeNewModel() {
		model = new ArrearsDetails();

	}

	

	public void setModel(ArrearsDetails entity) {
		model = entity;
	}

	public static ArrearsManagementForm initialize() {

		ArrearsManagementForm form = new ArrearsManagementForm();

		ArrearsManagementTableProxy gentable = new ArrearsManagementTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		ArrearsManagementSearchProxy.staticSuperTable = gentable;
		SearchPopUpScreen<ArrearsDetails> searchpopup = new ArrearsManagementSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		ArrearsManagementPresenter presenter = new ArrearsManagementPresenter(form,new ArrearsDetails());
		AppMemory.getAppMemory().stickPnel(form);
		return form;

	}
	
	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
//		List<CtcComponent> list = form.earningtable.getDataprovider().getList();
//
//		Double value = calculateGrossEarning(list);
//		if (value != null) {
//			form.dbGrossWithoutOT.setValue(value);
//			value=value+form.dbOvertimeAmount.getValue();
//			form.grossEarning.setValue(value);
//			// form.ctcCopy.setValue(value);
//			Double ctc = form.grossEarning.getValue();
//			form.grossEarning.setValue(ctc);
////			System.out.println("CTC ::::::::::: "+ctc);
//		}

	}


	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.arrears.ArrearsDetails")
	public static  class ArrearsManagementSearch extends SearchPopUpScreen<ArrearsDetails>{

		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}}


	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		// TODO Auto-generated method stub
		if (event.getSource().equals(form.pic.getId())||event.getSource().equals(form.pic.getName())||event.getSource().equals(form.pic.getPhone())) {
			form.retriveEmployeeArrears();
		}
	};
	
	private void reactToNew()
	{
		form.setToNewState();
		this.initialize();
		form.toggleAppHeaderBarMenu();
	}


}
