package com.slicktechnologies.client.views.humanresource.deduction.lwf;

import java.util.Date;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWF;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWFBean;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;

public class LWFBeanTable extends SuperTable<LWFBean>{

	TextColumn<LWFBean> getLWFEmployeeContributionCol;
	TextColumn<LWFBean> getLWFEmployerContributionCol;
	TextColumn<LWFBean> getLWFDeductionMonthCol;
	TextColumn<LWFBean> getLWFPeriodCol;
	TextColumn<LWFBean> getLWFFromAmount;
	TextColumn<LWFBean> getLWFToAmount;
	Column<LWFBean,String> deleteColumn;
	
	@Override
	public void createTable() {
//		getLWFEmployeeContributionCol(); 
//		getLWFEmployerContributionCol(); 
		getLWFDeductionMonthCol();       
//		getLWFPeriodCol();               
//		getLWFFromAmount();              
//		getLWFToAmount();   
		createColumndeleteColumn();
		addFieldUpdater(); 
	}

	private void getLWFEmployeeContributionCol() {
		getLWFEmployeeContributionCol=new TextColumn<LWFBean>() {
			@Override
			public String getValue(LWFBean object) {
				if(object.getEmployeeContribution()!=0){
					return object.getEmployeeContribution()+"";
				}
				return "";
			}
		};
		table.addColumn(getLWFEmployeeContributionCol, "Employee Contribution");
		getLWFEmployeeContributionCol.setSortable(true);	
	}
	
	private void getLWFEmployerContributionCol() {
		getLWFEmployerContributionCol=new TextColumn<LWFBean>() {
			@Override
			public String getValue(LWFBean object) {
				if(object.getEmployerContribution()!=0){
					return object.getEmployerContribution()+"";
				}
				return "";
			}
		};
		table.addColumn(getLWFEmployerContributionCol, "Employer Contribution");
		getLWFEmployerContributionCol.setSortable(true);	
	}
	
	private void getLWFDeductionMonthCol() {
		getLWFDeductionMonthCol=new TextColumn<LWFBean>() {
			@Override
			public String getValue(LWFBean object) {
				if(object.getDeductionMonth() != null){
					return object.getDeductionMonth();
				}
				return "";
			}
		};
		table.addColumn(getLWFDeductionMonthCol, "Deduction Month");
		getLWFDeductionMonthCol.setSortable(true);	
	}
	
	private void getLWFPeriodCol() {
		getLWFPeriodCol=new TextColumn<LWFBean>() {
			@Override
			public String getValue(LWFBean object) {
				if(object.getDeductionMonth() != null){
					return object.getPeriod();
				}
				return "";
			}
		};
		table.addColumn(getLWFPeriodCol, "Period");
		getLWFPeriodCol.setSortable(true);	
	}
                           
	private void getLWFFromAmount() {
		getLWFFromAmount=new TextColumn<LWFBean>() {
			@Override
			public String getValue(LWFBean object) {
				if(object.getFromAmt()!=0){
					return object.getFromAmt()+"";
				}
				return "";
			}
		};
		table.addColumn(getLWFFromAmount, "From Amount");
		getLWFFromAmount.setSortable(true);	
	}
	
	private void getLWFToAmount() {
		getLWFToAmount=new TextColumn<LWFBean>() {
			@Override
			public String getValue(LWFBean object) {
				if(object.getToAmt()!=0){
					return object.getToAmt()+"";
				}
				return "";
			}
		};
		table.addColumn(getLWFToAmount, "To Amount");
		getLWFToAmount.setSortable(true);	
	}

	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<LWFBean,String>(btnCell)
				{
			@Override
			public String getValue(LWFBean object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
	}
	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<LWFBean,String>()
				{
			@Override
			public void update(int index,LWFBean object,String value)
			{
				getDataprovider().getList().remove(object);

				table.redrawRow(index);
			}
				});
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		createFieldUpdaterdeleteColumn();
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		for(int i=table.getColumnCount()-1;i>-1;i--){
			table.removeColumn(i); 
		}
		 
		if (state == true) {
//			getLWFEmployeeContributionCol();
//			getLWFEmployerContributionCol();
			getLWFDeductionMonthCol();
//			getLWFPeriodCol();
//			getLWFFromAmount();
//			getLWFToAmount();
			createColumndeleteColumn();
			addFieldUpdater();
		} else {
//			getLWFEmployeeContributionCol();
//			getLWFEmployerContributionCol();
			getLWFDeductionMonthCol();
//			getLWFPeriodCol();
//			getLWFFromAmount();
//			getLWFToAmount();
		}
	 
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
