package com.slicktechnologies.client.views.humanresource.deduction.lwf;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.views.humanresource.deduction.pt.PTForm;
import com.slicktechnologies.client.views.humanresource.deduction.pt.PTPresenter;
import com.slicktechnologies.client.views.humanresource.deduction.pt.PTTable;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWF;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;

public class LWFPresenter extends FormTableScreenPresenter<LWF>{
	LWFForm form;
	public LWFPresenter(FormTableScreen<LWF> view,LWF model) {
		super(view, model);
		form=(LWFForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getPTQuery());
		form.setPresenter(this);
	}

	private MyQuerry getPTQuery() {
		MyQuerry quer=new MyQuerry();
		quer.setQuerryObject(new LWF());
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		quer.getFilters().add(filter);
		return quer;
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("New")){
			form.setToNewState();
			this.initialize();
		}
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		model=new LWF();
	}
	
	public static void initialize(){
		LWFTable gentableScreen=new LWFTable();
		LWFForm  form=new  LWFForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		LWFPresenter  presenter=new  LWFPresenter  (form,new LWF());
		AppMemory.getAppMemory().stickPnel(form);
	}


}
