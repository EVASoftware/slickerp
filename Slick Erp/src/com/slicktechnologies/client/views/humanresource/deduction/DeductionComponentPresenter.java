package com.slicktechnologies.client.views.humanresource.deduction;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;


import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;

public class DeductionComponentPresenter extends FormTableScreenPresenter<CtcComponent>
{

	final GenricServiceAsync async = GWT.create(GenricService.class);
	DeductionComponentForm form;

	public DeductionComponentPresenter (FormTableScreen<CtcComponent> view,
			CtcComponent model) {
		super(view, model);
		form=(DeductionComponentForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getSalaryConfigQuery());
		getEarningComponent() ;
		form.setPresenter(this);
	}

	
	
	private void getEarningComponent() {
		MyQuerry querry=new MyQuerry();
		Filter filter=null;
		Vector<Filter> filtervec=new Vector<Filter>();
		
		Company c=new Company();
		 
		filter=new Filter();
		filter.setBooleanvalue(false);
		filter.setQuerryString("isDeduction");
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		  
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CtcComponent());
		 
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("Leave result size : "+result.size());
				if (result.size() != 0) {
					form.lbEarningComponent.clear();
					form.lbEarningComponent.addItem("--SELECT--");
					form.lbEarningComponent.addItem("Gross Earning");
					for(SuperModel model:result){
						CtcComponent entity=(CtcComponent) model;
						form.lbEarningComponent.addItem(entity.getName());
					}
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Failure!!!!!!!!!!1");
			}
		});
	}
	
	
	

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
	{
		InlineLabel lbl= (InlineLabel) e.getSource();
//		if(lbl.getText().contains("Delete"))
//		{
//			
//			if(this.form.validate())
//			{
//			List<CtcComponent>data=form.getSupertable().getDataprovider().getList();
//			/*
//			 * Deleting
//			 */
//			for(int i=0;i<data.size();i++)
//			{
//				if(data.get(i).getId().equals(model.getId()))
//				{
//					data.remove(i);
//				}
//				form.getSupertable().getDataprovider().refresh();
//			}
//			reactOnDelete();
//			
//			}
//		}
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}

	}





	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void makeNewModel() {
		model=new CtcComponent();

	}

	public MyQuerry getSalaryConfigQuery()
	{
		MyQuerry quer=new MyQuerry();
		quer.setQuerryObject(new CtcComponent());
		Filter filter=new Filter();
		filter.setQuerryString("isDeduction");
		filter.setBooleanvalue(true);
		quer.getFilters().add(filter);
		return quer;
	}

	public void setModel(CtcComponent entity)
	{
		model=entity;
	}


	public static void initalize()
	{
		DeductionComponentTable gentableScreen=new DeductionComponentTable();

		DeductionComponentForm  form=new  DeductionComponentForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();

		/// CountryPresenterTable gentableSearch=GWT.create( CountryPresenterTable.class);
		////   CountryPresenterSearch.staticSuperTable=gentableSearch;
		////     CountryPresenterSearch searchpopup=GWT.create( CountryPresenterSearch.class);
		////         form.setSearchpopupscreen(searchpopup);

		DeductionComponentPresenter  presenter=new  DeductionComponentPresenter  (form,new CtcComponent());
		AppMemory.getAppMemory().stickPnel(form);



	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.SalaryHead")
	public static class  SalaryHeadPresenterTable extends SuperTable<CtcComponent> implements GeneratedVariableRefrence{

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub

		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub

		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub

		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub

		}

	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.SalaryHead")
	public static class  SalaryHeadPresenterSearch extends SearchPopUpScreen<  CtcComponent>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}

	};

	private void reactTo()
	{
	}
}
