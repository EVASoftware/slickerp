package com.slicktechnologies.client.views.humanresource.deduction.pt;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class PTForm extends FormTableScreen<ProfessionalTax> implements ClickHandler{
	
	TextBox tbPtId;
	ObjectListBox<State> oblState;
	TextBox tbComponentName;
	TextBox tbShortName;
	DoubleBox dbPtAmountMonthly;
	DoubleBox dbRangeFromAmt;
	DoubleBox dbRangeToAmt;
	ListBox lbGender;
	CheckBox cbStatus;
	CheckBox cbIsExceptionForSpecificMonth;
	ListBox lbMonth;
	DoubleBox dbSpecificMonthAmount;
	
	public PTForm(SuperTable<ProfessionalTax> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		tbPtId.setEnabled(false);
	}
	
	private void initializeWidget(){
		tbPtId=new TextBox();
		tbPtId.setEnabled(false);
		
		oblState=new ObjectListBox<State>();
		State.makeOjbectListBoxLive(oblState);
		
		tbComponentName=new TextBox();
		tbShortName=new TextBox();
		dbPtAmountMonthly=new DoubleBox();
		dbRangeFromAmt=new DoubleBox();
		dbRangeToAmt=new DoubleBox();
		
		lbGender=new ListBox();
		lbGender.addItem("--SELECT--");
		lbGender.addItem("All");
		lbGender.addItem("Male");
		lbGender.addItem("Female");
		
		cbStatus=new CheckBox();
		cbStatus.setValue(true);
		
		cbIsExceptionForSpecificMonth=new CheckBox();
		cbIsExceptionForSpecificMonth.setValue(false);
		cbIsExceptionForSpecificMonth.addClickHandler(this);
		
		lbMonth=new ListBox();
		lbMonth.addItem("--SELECT--");
		lbMonth.addItem("JAN");
		lbMonth.addItem("FEB");
		lbMonth.addItem("MAR");
		lbMonth.addItem("APR");
		lbMonth.addItem("MAY");
		lbMonth.addItem("JUN");
		lbMonth.addItem("JUL");
		lbMonth.addItem("AUG");
		lbMonth.addItem("SEP");
		lbMonth.addItem("OCT");
		lbMonth.addItem("NOV");
		lbMonth.addItem("DEC");
		lbMonth.setEnabled(false);
		
		dbSpecificMonthAmount=new DoubleBox();
		dbSpecificMonthAmount.setEnabled(false);
		
	}

	@Override
	public void toggleAppHeaderBarMenu() {

		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		} else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		} else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		 AuthorizationHelper.setAsPerAuthorization(Screen.PROFESSIONALTAX,LoginPresenter.currentModule.trim());
	}

	@Override
	public void createScreen() {
//		super.createScreen();
		initializeWidget();
		
		this.processlevelBarNames = new String[] { "New" };

		// Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fGrpingPTinfo = fbuilder.setlabel("Professional Tax Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("PT Id", tbPtId);
		FormField ftbPtId = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Name", tbComponentName);
		FormField ftbComponentName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Name is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Short Name", tbShortName);
		FormField ftbShortName = fbuilder.setMandatory(true)
				.setMandatoryMsg(" Short Name is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", cbStatus);
		FormField fstatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* PT Amount", dbPtAmountMonthly);
		FormField famount = fbuilder.setMandatory(true).setRowSpan(0)
				.setColSpan(0).setMandatoryMsg("PT Amount is mandatory").build();

		fbuilder = new FormFieldBuilder("* From Amount", dbRangeFromAmt);
		FormField fdbRangeFromAmt = fbuilder.setMandatory(true).setRowSpan(0)
				.setColSpan(0).setMandatoryMsg("From Amount is mandatory").build();
		
		fbuilder = new FormFieldBuilder("To Amount", dbRangeToAmt);
		FormField fdbRangeToAmt = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).setMandatoryMsg("").build();

		fbuilder = new FormFieldBuilder("* Gender", lbGender);
		FormField flbGender = fbuilder.setMandatory(true)
				.setMandatoryMsg("Gender is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* State", oblState);
		FormField foblState = fbuilder.setMandatory(true)
				.setMandatoryMsg(" State is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Is Exception For Specific Month ", cbIsExceptionForSpecificMonth);
		FormField fcbIsExceptionForSpecificMonth = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Specific Month", lbMonth);
		FormField flbMonthList = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Specific Month Amount", dbSpecificMonthAmount);
		FormField fdbSpecificMonthAmount = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		

		FormField[][] formfield = {
				{ fGrpingPTinfo },
				{ ftbPtId, foblState, ftbComponentName,ftbShortName},
				{ famount,fdbRangeFromAmt,fdbRangeToAmt,flbGender},
				{ fcbIsExceptionForSpecificMonth,flbMonthList,fdbSpecificMonthAmount,fstatus }

		};

		this.fields = formfield;
	}

	@Override
	public void updateModel(ProfessionalTax model) {
		if(tbComponentName.getValue()!=null){
			model.setComponentName(tbComponentName.getValue());
		}
		if(tbShortName.getValue()!=null){
			model.setShortName(tbShortName.getValue());
		}
		if(oblState.getValue()!=null){
			model.setState(oblState.getValue());
		}
		
		if(dbPtAmountMonthly.getValue()!=null){
			model.setPtAmount(dbPtAmountMonthly.getValue());
		}else{
			model.setPtAmount(0);
		}
		
		if(dbRangeFromAmt.getValue()!=null){
			model.setRangeFromAmt(dbRangeFromAmt.getValue());
		}else{
			model.setRangeFromAmt(0);
		}
		
		if(dbRangeToAmt.getValue()!=null){
			model.setRangeToAmt(dbRangeToAmt.getValue());
		}else{
			model.setRangeToAmt(0);
		}
		
		if(lbGender.getSelectedIndex()!=0){
			model.setGender(lbGender.getValue(lbGender.getSelectedIndex()));
		}else{
			model.setGender("");
		}
		
		model.setExceptionForSpecificMonth(cbIsExceptionForSpecificMonth.getValue());
		
		if(lbMonth.getSelectedIndex()!=0){
			model.setSpecificMonth(lbMonth.getValue(lbMonth.getSelectedIndex()));
		}else{
			model.setSpecificMonth("");
		}
		
		if(dbSpecificMonthAmount.getValue()!=null){
			model.setSpecificMonthAmount(dbSpecificMonthAmount.getValue());
		}else{
			model.setSpecificMonthAmount(0);
		}
		
		model.setStatus(cbStatus.getValue());
		
		presenter.setModel(model);
	}

	@Override
	public void updateView(ProfessionalTax view) {
		if(view.getCount()!=0){
			tbPtId.setValue(view.getCount()+"");
		}
		if(view.getComponentName()!=null){
			tbComponentName.setValue(view.getComponentName());
		}
		if(view.getShortName()!=null){
			tbShortName.setValue(view.getShortName());
		}
		if(view.getState()!=null){
			oblState.setValue(view.getState());
		}
		if(view.getPtAmount()!=0){
			dbPtAmountMonthly.setValue(view.getPtAmount());
		}
		if(view.getRangeFromAmt()!=0){
			dbRangeFromAmt.setValue(view.getRangeFromAmt());
		}
		if(view.getRangeToAmt()!=0){
			dbRangeToAmt.setValue(view.getRangeToAmt());
		}
		if(view.getGender()!=null){
			for(int i=0;i<lbGender.getItemCount();i++){
				if(lbGender.getItemText(i).equals(view.getGender())){
					lbGender.setSelectedIndex(i);
					break;
				}
			}
		}
		cbIsExceptionForSpecificMonth.setValue(view.isExceptionForSpecificMonth());
		
		if(view.getSpecificMonth()!=null){
			for(int i=0;i<lbMonth.getItemCount();i++){
				if(lbMonth.getItemText(i).equals(view.getSpecificMonth())){
					lbMonth.setSelectedIndex(i);
					break;
				}
			}
		}
		
		if(view.getSpecificMonthAmount()!=0){
			dbSpecificMonthAmount.setValue(view.getSpecificMonthAmount());
		}
		cbStatus.setValue(view.isStatus());
		
		presenter.setModel(view);
		
	}
	
	@Override
	public void setCount(int count)
	{
		this.tbPtId.setValue(presenter.getModel().getCount()+"");
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		boolean superVal=super.validate();
		
		if(cbIsExceptionForSpecificMonth.getValue()==true){
			String msg="";
			if(lbMonth.getSelectedIndex()==0){
				msg="Please select specific month.";
			}
			if(dbSpecificMonthAmount.getValue()==null||dbSpecificMonthAmount.getValue()==0){
				msg=msg+"\n"+"Please add specific month amount.";
			}
			if(!msg.equals("")){
				showDialogMessage(msg);
				return false;
			}
		}
		return superVal;
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbPtId.setEnabled(false);
		
		if(cbIsExceptionForSpecificMonth.getValue()==false){
			dbSpecificMonthAmount.setEnabled(false);
			lbMonth.setEnabled(false);
		}
	}

	@Override
	public void setToNewState() {
		// TODO Auto-generated method stub
		super.setToNewState();
	}

	@Override
	public void setToViewState() {
		// TODO Auto-generated method stub
		super.setToViewState();
	}

	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		super.clear();
		cbStatus.setValue(true);
		cbIsExceptionForSpecificMonth.setValue(false);
		dbSpecificMonthAmount.setEnabled(false);
		lbMonth.setEnabled(false);
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==cbIsExceptionForSpecificMonth){
			if(cbIsExceptionForSpecificMonth.getValue()==true){
				dbSpecificMonthAmount.setEnabled(true);
				lbMonth.setEnabled(true);
			}else{
				dbSpecificMonthAmount.setValue(null);
				lbMonth.setSelectedIndex(0);
				dbSpecificMonthAmount.setEnabled(false);
				lbMonth.setEnabled(false);
			}
		}
	}
	
	
	
	
	
}
