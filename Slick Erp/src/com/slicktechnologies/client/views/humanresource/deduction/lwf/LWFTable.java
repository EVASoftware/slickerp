package com.slicktechnologies.client.views.humanresource.deduction.lwf;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWF;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;

public class LWFTable extends SuperTable<LWF>{

	TextColumn<LWF> getLWFIdCol;
	TextColumn<LWF> getLWFStateCol;
	TextColumn<LWF> getLWFCompNameCol;
	TextColumn<LWF> getLWFShortNameCol;
	TextColumn<LWF> getLWFEmployeeContributionCol;
	TextColumn<LWF> getLWFEmployerContributionCol;
	TextColumn<LWF> getLWFDeductionMonthCol;
	TextColumn<LWF> getLWFStatusCol;
	
	@Override
	public void createTable() {
		getLWFIdCol();
		getLWFStateCol();
		getLWFCompNameCol();
		getLWFShortNameCol();
//		getLWFEmployeeContributionCol();
//		getLWFEmployerContributionCol();
//		getLWFDeductionMonthCol();
		getLWFStatusCol();
		
	}

	

	private void getLWFIdCol() {
		getLWFIdCol=new TextColumn<LWF>() {
			@Override
			public String getValue(LWF object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getLWFIdCol, "LWF Id");
		getLWFIdCol.setSortable(true);
		
		
	}

	private void getLWFStateCol() {
		getLWFStateCol=new TextColumn<LWF>() {
			@Override
			public String getValue(LWF object) {
				if(object.getState()!=null){
					return object.getState();
				}
				return "";
			}
		};
		table.addColumn(getLWFStateCol, "State");
		getLWFStateCol.setSortable(true);
		
	
		
		
	}

	private void getLWFCompNameCol() {
		getLWFCompNameCol=new TextColumn<LWF>() {
			@Override
			public String getValue(LWF object) {
				if(object.getName()!=null){
					return object.getName();
				}
				return "";
			}
		};
		table.addColumn(getLWFCompNameCol, "Component Name");
		getLWFCompNameCol.setSortable(true);
		
		
	}

	private void getLWFShortNameCol() {
		getLWFShortNameCol=new TextColumn<LWF>() {
			@Override
			public String getValue(LWF object) {
				if(object.getShortName()!=null){
					return object.getShortName();
				}
				return "";
			}
		};
		table.addColumn(getLWFShortNameCol, "Short Name");
		getLWFShortNameCol.setSortable(true);
		
		
	}

//	private void getLWFEmployeeContributionCol() {
//		getLWFEmployeeContributionCol=new TextColumn<LWF>() {
//			@Override
//			public String getValue(LWF object) {
//				if(object.getEmployeeContribution()!=0){
//					return object.getEmployeeContribution()+"";
//				}
//				return "";
//			}
//		};
//		table.addColumn(getLWFEmployeeContributionCol, "Employee Contribution");
//		getLWFEmployeeContributionCol.setSortable(true);	
//	}
//	
//	private void getLWFEmployerContributionCol() {
//		getLWFEmployerContributionCol=new TextColumn<LWF>() {
//			@Override
//			public String getValue(LWF object) {
//				if(object.getEmployerContribution()!=0){
//					return object.getEmployerContribution()+"";
//				}
//				return "";
//			}
//		};
//		table.addColumn(getLWFEmployerContributionCol, "Employer Contribution");
//		getLWFEmployerContributionCol.setSortable(true);	
//	}
	
//	private void getLWFDeductionMonthCol() {
//		getLWFDeductionMonthCol=new TextColumn<LWF>() {
//			@Override
//			public String getValue(LWF object) {
//				if(object.getDeductionMonth() != null){
//					return object.getDeductionMonth();
//				}
//				return "";
//			}
//		};
//		table.addColumn(getLWFDeductionMonthCol, "Deduction Month");
//		getLWFDeductionMonthCol.setSortable(true);	
//	}
	
	private void getLWFStatusCol() {
		getLWFStatusCol=new TextColumn<LWF>() {
			@Override
			public String getValue(LWF object) {
				if(object.isStatus()==true){
					return "Active";
				}
				return "Inactive";
			}
		};
		table.addColumn(getLWFStatusCol, "Status");
		getLWFStatusCol.setSortable(true);
		
		
	}
	
	
	/***9-1-2019 added by Amol for LWF table sorting****/
	public void addColumnSorting(){
		addSortingOnLWFIdCol();
		addSortingOnLWFStateCol();
		addSortingOnLWFCompNameCol();
		addSortingOnLWFShortNameCol();
		addSortingOnLWFStatusCol();
			
	}
	
	
	
	
	
	
	
	
	
	private void addSortingOnLWFStatusCol() {
		List<LWF> list = getDataprovider().getList();
		columnSort = new ListHandler<LWF>(list);
		columnSort.setComparator(getLWFStatusCol,new Comparator<LWF>() {
			@Override
			public int compare(LWF e1, LWF e2) {
				if (e1 != null && e2 != null) {
					Boolean flag1=e1.isStatus();
					Boolean flag2=e2.isStatus();
					if (flag1 != null&& flag2 != null) {
						return flag1.compareTo(flag2);
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
		
	}



	private void addSortingOnLWFShortNameCol() {
		// TODO Auto-generated method stub
		List<LWF> list=getDataprovider().getList();
		columnSort=new ListHandler<LWF>(list);
		columnSort.setComparator(getLWFShortNameCol, new Comparator<LWF>()
		  {
		  @Override
		  public int compare(LWF e1,LWF e2)
		  {
		 
			  if(e1!=null && e2!=null)
			   {
			   if( e1.getShortName()!=null && e2.getShortName()!=null){
			   return e1.getShortName().compareTo(e2.getShortName());}
			   }
			   else{
			   return 0;}
			   return 0;
			   }
			   });
			   table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnLWFCompNameCol() {
		// TODO Auto-generated method stub
		List<LWF> list = getDataprovider().getList();
		columnSort = new ListHandler<LWF>(list);
		columnSort.setComparator(getLWFCompNameCol,new Comparator<LWF>() {
			@Override
			public int compare(LWF e1, LWF e2) {
				if (e1 != null && e2 != null) {
					if (e1.getName() != null&& e2.getName() != null) {
						return e1.getName().compareTo(e2.getName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
		
	}



	private void addSortingOnLWFStateCol() {
		// TODO Auto-generated method stub
		
		List<LWF> list = getDataprovider().getList();
		columnSort = new ListHandler<LWF>(list);
		columnSort.setComparator(getLWFStateCol,new Comparator<LWF>() {
			@Override
			public int compare(LWF e1, LWF e2) {
				if (e1 != null && e2 != null) {
					if (e1.getState()!= null&& e2.getState() != null) {
						return e1.getState().compareTo(e2.getState());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
		
	}



	private void addSortingOnLWFIdCol() {
		// TODO Auto-generated method stub
		List<LWF> list = getDataprovider().getList();
		columnSort = new ListHandler<LWF>(list);
		columnSort.setComparator(getLWFIdCol, new Comparator<LWF>() {
			@Override
			public int compare(LWF e1, LWF e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
