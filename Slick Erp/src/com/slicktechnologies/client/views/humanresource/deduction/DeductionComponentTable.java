package com.slicktechnologies.client.views.humanresource.deduction;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.client.views.humanresource.ctccomponent.EarningComponentPresenter.SalaryHeadPresenterTable;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;

public class DeductionComponentTable extends SalaryHeadPresenterTable {
	TextColumn<CtcComponent> getNameColumn;
	TextColumn<CtcComponent> getStatusColumn;
	TextColumn<CtcComponent> getShortNameColumn;
	TextColumn<CtcComponent> maxCtc;
	TextColumn<CtcComponent> getMaxPerOfCTCColumn;
	TextColumn<CtcComponent> getCountColumn;

	/**
	 * nidhi 11-09-2017 for display sono field
	 * 
	 */
	TextColumn<CtcComponent> getSrNo;

	/**
	 * end
	 */
	public Object getVarRef(String varName) {
		if (varName.equals("getNameColumn"))
			return this.getNameColumn;
		if (varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if (varName.equals("getShortNameColumn"))
			return this.getShortNameColumn;
		if (varName.equals("getMaxPerOfCTCColumn"))
			return this.getMaxPerOfCTCColumn;
		if (varName.equals("getCountColumn"))
			return this.getCountColumn;
		/**
		 * nidhi 11-09-2017 get sr no
		 */
		if (varName.equals("getSrNo"))
			return this.getSrNo;
		/**
		 * end
		 */
		return null;
	}

	public DeductionComponentTable() {
		super();
	}

	@Override
	public void createTable() {
		// addColumngetCount();
		
		/**
		 * nidhi
		 * 11-09-2017
		 * for display sr no.
		 * 
		 */
		addColumnSrNo();
		/**
		 * end
		 */
		addColumngetName();
		addColumngetShortName();
		addColumngetMaxPerOfCTC();
		addColumnmaxctc();
		addColumngetStatus();
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<CtcComponent>() {
			@Override
			public Object getKey(CtcComponent item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	public void addColumnSorting() {
		// addSortinggetCount();
		/**
		 *  nidhi
		 *  11-09-2017
		 *  for sort field
		 */
		addSortingSrNo();
		/**
		 *  end
		 */
		addSortinggetName();
		addSortinggetShortName();
		addSortinggetMaxPerOfCTC();
		addSortinggetStatus();
		addSortingMaxAmt();
	}

	@Override
	public void addFieldUpdater() {
	}

	protected void addSortinggetCount() {
		List<CtcComponent> list = getDataprovider().getList();
		columnSort = new ListHandler<CtcComponent>(list);
		columnSort.setComparator(getCountColumn,
				new Comparator<CtcComponent>() {
					@Override
					public int compare(CtcComponent e1, CtcComponent e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCount() == e2.getCount()) {
								return 0;
							}
							if (e1.getCount() > e2.getCount()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		getCountColumn.setSortable(true);
	}

	protected void addSortinggetName() {
		List<CtcComponent> list = getDataprovider().getList();
		columnSort = new ListHandler<CtcComponent>(list);
		columnSort.setComparator(getNameColumn, new Comparator<CtcComponent>() {
			@Override
			public int compare(CtcComponent e1, CtcComponent e2) {
				if (e1 != null && e2 != null) {
					if (e1.getName() != null && e2.getName() != null) {
						return e1.getName().compareTo(e2.getName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	

	/**
	 * nidhi 11-09-2017
	 * 
	 */
	protected void addColumnSrNo() {
		getSrNo = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				return object.getSrNo() + "";
			}
		};
		table.addColumn(getSrNo, "Sr No.");
		getSrNo.setSortable(true);
	}

	/**
	 * end
	 */
	
	/**
	 *  nidhi
	 *  11-09-2017
	 *  method for sort field
	 */
	protected void  addSortingSrNo(){
		try {
			List<CtcComponent> list = getDataprovider().getList();
			columnSort = new ListHandler<CtcComponent>(list);
			columnSort.setComparator(getSrNo, new Comparator<CtcComponent>() {
				@Override
				public int compare(CtcComponent e1, CtcComponent e2) {
					if (e1 != null
							&& e2 != null
							) {
						if (e1.getSrNo() == e2.getSrNo()) {
							return 0;
						} else {
							if (e1.getSrNo() > e2.getSrNo())
								return 1;
						}
					}

					return -1;
				}
			});
			table.addColumnSortHandler(columnSort);
		} catch (Exception e) {
			e.printStackTrace();
			Console.log(e.getMessage());
			System.out.println(e.getMessage());
		}
		
	}
	
	/**
	 *  end
	 */

	protected void addColumngetName() {
		getNameColumn = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				return object.getName() + "";
			}
		};
		table.addColumn(getNameColumn, "Name");
		getNameColumn.setSortable(true);
	}

	protected void addSortinggetShortName() {
		List<CtcComponent> list = getDataprovider().getList();
		columnSort = new ListHandler<CtcComponent>(list);
		columnSort.setComparator(getShortNameColumn,
				new Comparator<CtcComponent>() {
					@Override
					public int compare(CtcComponent e1, CtcComponent e2) {
						if (e1 != null && e2 != null) {
							if (e1.getShortName() != null
									&& e2.getShortName() != null) {
								return e1.getShortName().compareTo(
										e2.getShortName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetShortName() {
		getShortNameColumn = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				return object.getShortName() + "";
			}
		};
		table.addColumn(getShortNameColumn, "Short Name");
		getShortNameColumn.setSortable(true);
	}

	protected void addSortinggetMaxPerOfCTC() {
		List<CtcComponent> list = getDataprovider().getList();
		columnSort = new ListHandler<CtcComponent>(list);
		columnSort.setComparator(getMaxPerOfCTCColumn,
				new Comparator<CtcComponent>() {
					@Override
					public int compare(CtcComponent e1, CtcComponent e2) {
						if (e1 != null
								&& e2 != null
								&& (e1.getMaxPerOfCTC() != null && e2
										.getMaxPerOfCTC() != null)) {
							if (e1.getMaxPerOfCTC() == e2.getMaxPerOfCTC()) {
								return 0;
							}
							if (e1.getMaxPerOfCTC() > e2.getMaxPerOfCTC()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return -1;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetMaxPerOfCTC() {

		getMaxPerOfCTCColumn = new TextColumn<CtcComponent>() {

			@Override
			public String getValue(CtcComponent object) {
				if (object.getMaxPerOfCTC() == null
						|| object.getMaxPerOfCTC() == 0)
					return "N.A";
				else
					return object.getMaxPerOfCTC() + "";

			}
		};

		table.addColumn(getMaxPerOfCTCColumn, "% of CTC Component");
		getMaxPerOfCTCColumn.setSortable(true);
	}

	protected void addSortinggetStatus() {
		List<CtcComponent> list = getDataprovider().getList();
		columnSort = new ListHandler<CtcComponent>(list);
		columnSort.setComparator(getStatusColumn,
				new Comparator<CtcComponent>() {
					@Override
					public int compare(CtcComponent e1, CtcComponent e2) {
						if (e1 != null && e2 != null) {
							if (e1.getStatus() == e2.getStatus()) {
								return 0;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				if (object.getStatus() == true)
					return "Active";
				else
					return "In Active";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
	}

	protected void addSortingMaxAmt() {
		List<CtcComponent> list = getDataprovider().getList();
		columnSort = new ListHandler<CtcComponent>(list);
		columnSort.setComparator(maxCtc, new Comparator<CtcComponent>() {
			@Override
			public int compare(CtcComponent e1, CtcComponent e2) {
				if (e1 != null
						&& e2 != null
						&& (e1.getMaxAmount() != null && e2.getMaxAmount() != null)) {
					if (e1.getMaxAmount() == e2.getMaxAmount()) {
						return 0;
					} else {
						if (e1.getMaxAmount() > e2.getMaxAmount())
							return 1;
					}
				}

				return -1;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumnmaxctc() {
		maxCtc = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				if (object.getMaxAmount() == null || object.getMaxAmount() == 0)
					return "N.A";
				else
					return object.getMaxAmount() + "";
			}
		};
		table.addColumn(maxCtc, "Flat Amount");
		maxCtc.setSortable(true);
	}

}
