package com.slicktechnologies.client.views.humanresource.deduction;


import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class DeductionComponentForm extends FormTableScreen<CtcComponent> implements ChangeHandler,ClickHandler
{
	IntegerBox ibSalaryConfigId;
	DoubleBox maxperCTC;
	IntegerBox maxAmount;
	
	TextBox tbSalaryconfigName,tbSalaryconfigShortName;
	CheckBox status;
	
	ListBox lbEarningComponent;
	
	CheckBox cbNonMonthly;
	CheckBox cbRecord;
	
	ListBox lbCondition;
	IntegerBox ibConditionValue;
	
	/**
	 *  nidhi
	 *  11-09-2017
	 *  for report sorting 
	 */
	IntegerBox tbSrNo;
	
	/**
	 *  end
	 */
	
	/**
	 * Date : 04-05-2018 By ANIL
	 * adding static gender list,which used at payroll for calculating PT 
	 */
	ListBox lbGender;
	/**
	 * Rahul added this for specific month deduction
	 * 
	 */
	CheckBox specificMonth;
	ListBox lbSpecificMonthDeduction;
	
	
	
	public DeductionComponentForm  (SuperTable<CtcComponent> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
	}


	private void initalizeWidget()
	{
		ibSalaryConfigId=new IntegerBox();
		ibSalaryConfigId.setEnabled(false);
		tbSalaryconfigName=new TextBox();
		tbSalaryconfigShortName=new TextBox();
		
		status=new CheckBox();
		status.setValue(true);
		
		
		maxperCTC=new DoubleBox();
		maxperCTC.setEnabled(false);
		
		maxAmount=new IntegerBox();
		maxAmount.setEnabled(true);
		
		lbEarningComponent=new ListBox();
		lbEarningComponent.addChangeHandler(this);
		lbEarningComponent.addItem("--SELECT--");
		
		cbRecord=new CheckBox();
		cbRecord.setValue(false);
		
		cbNonMonthly=new CheckBox();
		cbNonMonthly.setValue(false);
		
		lbCondition=new ListBox();
		lbCondition.addChangeHandler(this);
		lbCondition.addItem("--SELECT--");
		lbCondition.addItem("<");
		lbCondition.addItem(">");
		lbCondition.addItem("=");
		
		ibConditionValue=new IntegerBox();
		ibConditionValue.setEnabled(false);
		

		/**
		 *  nidhi
		 *  11-09-2017
		 *  
		 */
		
		tbSrNo = new IntegerBox();
		/**
		 *  end
		 */
		
		lbGender=new ListBox();
		lbGender.addItem("--SELECT--");
		lbGender.addItem("All");
		lbGender.addItem("Male");
		lbGender.addItem("Female");
		
		
		/**
		 * Rahul added this for specific month
		 */
		specificMonth=new CheckBox();
		specificMonth.setValue(false);
		specificMonth.addClickHandler(this);
		
		lbSpecificMonthDeduction=new ListBox();
		lbSpecificMonthDeduction.addItem("--SELECT--");
		lbSpecificMonthDeduction.addItem("JAN");
		lbSpecificMonthDeduction.addItem("FEB");
		lbSpecificMonthDeduction.addItem("MAR");
		lbSpecificMonthDeduction.addItem("APR");
		lbSpecificMonthDeduction.addItem("MAY");
		lbSpecificMonthDeduction.addItem("JUN");
		lbSpecificMonthDeduction.addItem("JUL");
		lbSpecificMonthDeduction.addItem("AUG");
		lbSpecificMonthDeduction.addItem("SEP");
		lbSpecificMonthDeduction.addItem("OCT");
		lbSpecificMonthDeduction.addItem("NOV");
		lbSpecificMonthDeduction.addItem("DEC");
		lbSpecificMonthDeduction.setEnabled(false);
		
		
	}


	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames=new String[]{"New"};

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSalaryConfigInformation=fbuilder.setlabel("Salary Config Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("* Name",tbSalaryconfigName);
		FormField ftbSalaryconfigName= fbuilder.setMandatory(true).setMandatoryMsg("Name is mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Short Name",tbSalaryconfigShortName);
		FormField ftbSalaryconfigShortName= fbuilder.setMandatory(true).setMandatoryMsg(" Short Name is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" % of CTC Component",maxperCTC);
		FormField famount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setMandatoryMsg("% of CTC is mandatory").build();

		fbuilder = new FormFieldBuilder("Flat Amount",maxAmount);
		FormField fmaxamt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setMandatoryMsg("Maximum amount is Mandatory !").build();
		
		fbuilder = new FormFieldBuilder("Non Monthly",cbNonMonthly);
		FormField fcbNonMonthly= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("CTC Component",lbEarningComponent);
		FormField flbEarningComponent= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		//Date : 07-05-2018 By ANIL changing label from record to is company's contribution
		fbuilder = new FormFieldBuilder("Is company's contribution",cbRecord);
		FormField fcbRecord= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

//		fbuilder = new FormFieldBuilder("Company's Contribution %",ibCompContribution);
//		FormField fibCompContribution= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Condition",lbCondition);
		FormField flbCondition= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Condition Value",ibConditionValue);
		FormField fibConditionValue= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		
		fbuilder = new FormFieldBuilder("Status",status);
		FormField fstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/**
		 *  nidhi
		 *  11-09-2017
		 *  for sr no field
		 */
		
		fbuilder = new FormFieldBuilder("* Sr. No.",tbSrNo);
		FormField ftbSrNo= fbuilder.setMandatory(true).setMandatoryMsg("Serial No. is mndatory").setRowSpan(0).setColSpan(0).build();
		/**
		 * end
		 */
		
		fbuilder = new FormFieldBuilder("* Gender",lbGender);
		FormField flbGender= fbuilder.setMandatory(true).setMandatoryMsg("Gender is mandetory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Specific Month",specificMonth);
		FormField fcbspecificMonth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Month",lbSpecificMonthDeduction);
		FormField flbSpecificMonthDeduction= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		FormField[][] formfield = {  
				{fgroupingSalaryConfigInformation},
//				{ftbSrNo},
//				{ftbSalaryconfigName,ftbSalaryconfigShortName,flbEarningComponent,famount,fmaxamt},
//				{fcbRecord,fcbNonMonthly,flbCondition,fibConditionValue,fstatus},
				
				{ftbSrNo,ftbSalaryconfigName,ftbSalaryconfigShortName,fstatus},
				{flbEarningComponent,famount,fmaxamt,fcbRecord},
				{flbCondition,fibConditionValue,flbGender},
				{fcbspecificMonth,flbSpecificMonthDeduction}
		};

		this.fields=formfield;		

	}


	@Override
	public void updateModel(CtcComponent model) 
	{
		if(tbSalaryconfigName.getValue()!=null){
			model.setName(tbSalaryconfigName.getValue());
		}
		if(tbSalaryconfigShortName.getValue()!=null){
			model.setShortName(tbSalaryconfigShortName.getValue());
		}
		if(status.getValue()!=null){
			model.setStatus(status.getValue());
		}
//		if (maxperCTC.getValue()!=null)
			model.setMaxPerOfCTC(maxperCTC.getValue());
//		if(maxAmount.getValue()!=null)
			model.setMaxAmount(maxAmount.getValue());
			
		if(lbEarningComponent.getSelectedIndex()!=0){
			model.setCtcComponentName(lbEarningComponent.getValue(lbEarningComponent.getSelectedIndex()));
		}
		
		if(lbEarningComponent.getSelectedIndex()==0){
			model.setCtcComponentName(null);
		}
		
		if(cbRecord.getValue()!=null){
			model.setIsRecord(cbRecord.getValue());
		}
		
		if(cbNonMonthly.getValue()!=null){
			model.setIsNonMonthly(cbNonMonthly.getValue());
		}
		
		if(lbCondition.getSelectedIndex()!=0){
			model.setCondition(lbCondition.getValue(lbCondition.getSelectedIndex()));
		}
		
		if(ibConditionValue.getValue()!=null){
			model.setConditionValue(ibConditionValue.getValue());
		}
		
		 model.setDeduction(true);
		 
		 /**
			 *  nidhi
			 *  11-09-2017
			 *  for save sr no.
			 */
				model.setSrNo(tbSrNo.getValue());
			
			/**
			 * end
			 */
		/**
		 * Rahul added this for specific month deduction
		 */
		if(lbGender.getSelectedIndex()!=0){
			model.setGender(lbGender.getValue(lbGender.getSelectedIndex()));
		}else{
			model.setGender("");
		}
		 
		model.setSpecificMonth(specificMonth.getValue());
		if(lbSpecificMonthDeduction.getSelectedIndex()!=0){
			model.setSpecificMonthValue(lbSpecificMonthDeduction.getValue(lbSpecificMonthDeduction.getSelectedIndex()));
		}
		
		presenter.setModel(model);
	}


	@Override
	public void updateView(CtcComponent view) 
	{
		if(view.getName()!=null){
			tbSalaryconfigName.setValue(view.getName());
		}
		if(view.getShortName()!=null){
			tbSalaryconfigShortName.setValue(view.getShortName());
		}
		
		if(view.getStatus()!=null){
			status.setValue(view.getStatus());
		}
		if(view.getMaxPerOfCTC()!=null&&view.getMaxPerOfCTC()!=0){
			maxperCTC.setValue(view.getMaxPerOfCTC());
		}
		if(view.getMaxAmount()!=null&&view.getMaxAmount()!=0){
			maxAmount.setValue(view.getMaxAmount());
		}
		
		if(view.getCtcComponentName()!=null){
			for(int i=0;i<lbEarningComponent.getItemCount();i++){
				if(lbEarningComponent.getItemText(i).equals(view.getCtcComponentName())){
					lbEarningComponent.setSelectedIndex(i);
					break;
				}
			}
		}
		
		if(view.getIsRecord()!=null){
			cbRecord.setValue(view.getIsRecord());
		}
		
		if(view.getIsNonMonthly()!=null){
			cbNonMonthly.setValue(view.getIsNonMonthly());
		}
		
		if(view.getCondition()!=null){
			for(int i=0;i<lbCondition.getItemCount();i++){
				if(lbCondition.getItemText(i).trim().equals(view.getCondition().trim())){
					lbCondition.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getConditionValue()!=null){
			ibConditionValue.setValue(view.getConditionValue());
		}

		/**
		 *  nidhi
		 *  11-09-2017
		 *  for display sr no.
		 */
		tbSrNo.setValue(view.getSrNo());
		
		/**
		 * end
		 */

		if(view.getGender()!=null){
			for(int i=0;i<lbGender.getItemCount();i++){
				if(lbGender.getItemText(i).equals(view.getGender())){
					lbGender.setSelectedIndex(i);
					break;
				}
			}
		}
		/**
		 * Rahul added this on 26 June 2018 for specific month deduction
		 */
		 
		specificMonth.setValue(view.isSpecificMonth());
		
		if(view.getSpecificMonthValue()!=null&&!view.getSpecificMonthValue().trim().equalsIgnoreCase("")){
			for(int i=0;i<lbSpecificMonthDeduction.getItemCount();i++){
				if(lbSpecificMonthDeduction.getItemText(i).trim().equals(view.getSpecificMonthValue())){
					lbSpecificMonthDeduction.setSelectedIndex(i);
					break;
				}
			}
		}

		presenter.setModel(view);
	}

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false); 
				}
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SALARYHEAD,LoginPresenter.currentModule.trim());
	}

	@Override
	public void setCount(int count)
	{
		this.ibSalaryConfigId.setValue(presenter.getModel().getCount());
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibSalaryConfigId.setEnabled(false);
		maxAmount.setEnabled(state);
		maxperCTC.setEnabled(false);
		ibConditionValue.setEnabled(false);
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		
		if(maxAmount.getValue()!=null){
			maxAmount.setEnabled(true);
		}
		if(maxperCTC.getValue()!=null){
			maxperCTC.setEnabled(true);
		}
		if(ibConditionValue.getValue()!=null){
			ibConditionValue.setEnabled(true);
		}
		this.processLevelBar.setVisibleFalse(false);
	}


	@Override
	public void setToNewState() {
		super.setToNewState();
		cbNonMonthly.setValue(false);
		cbRecord.setValue(false);
		specificMonth.setValue(false);
	}


	@Override
	public boolean validate() {
		boolean val= super.validate();
		if(val==false)
			return val;
		if(this.maxAmount.getValue()==null&&this.maxperCTC.getValue()==null)
		{
			showDialogMessage("Both Maximum Percentage and Maximum Absolute amount Cannot be emptey !");
			return false;
		}
		
		if(this.maxAmount.getValue()!=null&&this.maxperCTC.getValue()!=null)
		{
			showDialogMessage("Fill either Maximum Percentage or Maximum Absolute amount not both.");
			return false;
			
		}
		if(this.lbCondition.getSelectedIndex()!=0&&ibConditionValue.getValue()==null){
			showDialogMessage("Please Enter Condition Value!");
			return false;
		}
		
		/**
		 *  nidhi
		 *  11-09-2017
		 *  check sr no validation
		 */
		if(!this.checkSrNo()){
			showDialogMessage("Serial Number should not be duplicate");
			return false;
		}
		/**
		 * end
		 */
		
		return true;
	}

	
	/**
	 *  nidh
	 *  11-09-2017
	 *   for check srno duplication
	 * @return
	 */
	
	public boolean checkSrNo(){
		boolean flag = true;
		try {
			List<CtcComponent> ctcList = this.getSupertable().getDataprovider().getList();
			
			for(CtcComponent ctc : ctcList){
					if(ctc.getSrNo() == tbSrNo.getValue()){
						if(AppMemory.getAppMemory().currentState == ScreeenState.EDIT) //&& ctc.getName().equalsIgnoreCase(tbSalaryconfigName.getValue())// 
						{		
							return true;
						}else{
							return false;
						}
					}
				}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return flag;
	}


	@Override
	public void clear() {
		super.clear();
		status.setValue(true);
	}


	@Override
	public void onChange(ChangeEvent event) {
		System.out.println("INSIDE CHANGE HANDLER METHOD");
		
		if(event.getSource().equals(lbEarningComponent)){
			if(lbEarningComponent.getSelectedIndex()!=0){
//				maxAmount.setEnabled(false);
				maxperCTC.setEnabled(true);
//				maxAmount.setValue(null);
				
			}else{
				maxAmount.setEnabled(true);
				maxperCTC.setEnabled(false);
				maxperCTC.setValue(null);
			}
		}
		if(event.getSource().equals(lbCondition)){
			if(lbCondition.getSelectedIndex()!=0){
				ibConditionValue.setEnabled(true);
			}else{
				ibConditionValue.setEnabled(false);
				ibConditionValue.setValue(null);
			}
		}
		
	}


	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(specificMonth)){
			boolean checked=specificMonth.getValue();
			if(checked){
				lbSpecificMonthDeduction.setEnabled(true);
			}else{
				lbSpecificMonthDeduction.setEnabled(false);
			}
		}
		
	}

}
