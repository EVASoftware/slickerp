package com.slicktechnologies.client.views.humanresource.deduction.lwf;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWF;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWFBean;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class LWFForm extends FormTableScreen<LWF> implements ClickHandler {
	TextBox tbLWFId;
	ObjectListBox<State> oblState;
	TextBox tbComponentName;
	TextBox tbShortName;
	DoubleBox dbEmployeeContribution;
	DoubleBox dbEmployerContribution;
	CheckBox cbStatus;
	ListBox lbMonth;
	DoubleBox dbFromAmount;
	DoubleBox dbToAmount;
	ObjectListBox<ConfigCategory> olbPeriod;
	LWFBeanTable lwfBeanTable;
	Button btAdd;
	

	public LWFForm(SuperTable<LWF> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		tbLWFId.setEnabled(false);
		btAdd.addClickHandler(this);
	}
	
	private void initializeWidget(){
		tbLWFId=new TextBox();
		tbLWFId.setEnabled(false);
		
		oblState=new ObjectListBox<State>();
		State.makeOjbectListBoxLive(oblState);
		
		tbComponentName=new TextBox();
		tbShortName=new TextBox();
		dbEmployeeContribution=new DoubleBox();
		dbEmployerContribution=new DoubleBox();
		
		cbStatus=new CheckBox();
		cbStatus.setValue(true);
			
		lbMonth=new ListBox();
		lbMonth.addItem("--SELECT--");
		lbMonth.addItem("JAN");
		lbMonth.addItem("FEB");
		lbMonth.addItem("MAR");
		lbMonth.addItem("APR");
		lbMonth.addItem("MAY");
		lbMonth.addItem("JUN");
		lbMonth.addItem("JUL");
		lbMonth.addItem("AUG");
		lbMonth.addItem("SEP");
		lbMonth.addItem("OCT");
		lbMonth.addItem("NOV");
		lbMonth.addItem("DEC");
//		lbMonth.setEnabled(false);
		
		olbPeriod = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbPeriod,Screen.ADDPAYMENTTERMS);
		dbFromAmount = new DoubleBox();
		dbToAmount = new DoubleBox();
		lwfBeanTable = new LWFBeanTable();
		lwfBeanTable.connectToLocal();
		
		btAdd = new Button("ADD");
	}

	@Override
	public void toggleAppHeaderBarMenu() {

		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		} else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		} else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		 AuthorizationHelper.setAsPerAuthorization(Screen.LWF,LoginPresenter.currentModule.trim());
	}

	@Override
	public void createScreen() {
//		super.createScreen();
		initializeWidget();
		
		this.processlevelBarNames = new String[] { "New" };

		// Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fGrpingLWFinfo = fbuilder.setlabel("LWF Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("LWF Id", tbLWFId);
		FormField FtbLWFId = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();


		fbuilder = new FormFieldBuilder("* Name", tbComponentName);
		FormField ftbComponentName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Name is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Short Name", tbShortName);
		FormField ftbShortName = fbuilder.setMandatory(true)
				.setMandatoryMsg(" Short Name is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", cbStatus);
		FormField fcbStatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Employee Contribution", dbEmployeeContribution);
		FormField fdbEmployeeContribution = fbuilder.setMandatory(true).setRowSpan(0)
				.setColSpan(0).setMandatoryMsg("Employee Contribution is mandatory").build();

		fbuilder = new FormFieldBuilder("* Employer Contribution", dbEmployerContribution);
		FormField fdbEmployerContribution = fbuilder.setMandatory(true).setRowSpan(0)
				.setColSpan(0).setMandatoryMsg("Employer Contribution is mandatory").build();
		
		fbuilder = new FormFieldBuilder("* State", oblState);
		FormField foblState = fbuilder.setMandatory(true)
				.setMandatoryMsg(" State is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		

		fbuilder = new FormFieldBuilder("Deduction Month", lbMonth);
		FormField flbMonth = fbuilder.setMandatory(false).setMandatoryMsg("Deduction Month is Mandatory.").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Period", olbPeriod);
		FormField folbPeriod = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("From Amount", dbFromAmount);
		FormField fdbFromAmount = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("To Amount", dbToAmount);
		FormField fdbToAmount = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", lwfBeanTable.getTable());
		FormField flwfBeanTable = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		FormField fGrpingLWFContriinfo = fbuilder.setlabel("LWF Contribution")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		FormField fGrpingDeduction = fbuilder.setlabel("Deduction Month")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", btAdd);
		FormField fbtAdd = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		FormField[][] formfield = {
				{ fGrpingLWFinfo },
				{ foblState, ftbComponentName, ftbShortName, fcbStatus },
				{ fGrpingLWFContriinfo },
				{ fdbEmployeeContribution, fdbEmployerContribution,fdbFromAmount, fdbToAmount },
				{ fGrpingDeduction},
				{ flbMonth, fbtAdd },
				{ flwfBeanTable }
		};

		this.fields = formfield;
	}

	@Override
	public void updateModel(LWF model) {
		if(tbComponentName.getValue()!=null){
			model.setName(tbComponentName.getValue());
		}
		if(tbShortName.getValue()!=null){
			model.setShortName(tbShortName.getValue());
		}
		if(oblState.getValue()!=null){
			model.setState(oblState.getValue());
		}
		
		
		if(dbEmployeeContribution.getValue()!=null){
			model.setEmployeeContribution(dbEmployeeContribution.getValue());
		}else{
			model.setEmployeeContribution(0);
		}
		
		if(dbEmployerContribution.getValue()!=null){
			model.setEmployerContribution(dbEmployerContribution.getValue());
		}else{
			model.setEmployerContribution(0);
		}
//		if(lbMonth.getSelectedIndex()!=0){
//			model.setDeductionMonth(lbMonth.getValue(lbMonth.getSelectedIndex()));
//		}else{
//			model.setDeductionMonth("");
//		}
		
		model.setStatus(cbStatus.getValue());
//		if(olbPeriod.getSelectedIndex() != 0){
//			model.setPeriod(olbPeriod.getValue(olbPeriod.getSelectedIndex()));
//		}else{
//			model.setPeriod("");
//		}
		if(dbFromAmount.getValue()!=null){
			model.setFromAmt(dbFromAmount.getValue());
		}
		if(dbToAmount.getValue()!=null){
			model.setToAmt(dbToAmount.getValue());
		}
		if(lwfBeanTable.getDataprovider().getList() != null){
			model.setLwfList(lwfBeanTable.getDataprovider().getList());
		}
		presenter.setModel(model);
	}

	@Override
	public void updateView(LWF view) {
		if(view.getCount()!=0){
			tbLWFId.setValue(view.getCount()+"");
		}
		if(view.getName()!=null){
			tbComponentName.setValue(view.getName());
		}
		if(view.getShortName()!=null){
			tbShortName.setValue(view.getShortName());
		}
		if(view.getState()!=null){
			oblState.setValue(view.getState());
		}
		if(view.getEmployeeContribution()!=0){
			dbEmployeeContribution.setValue(view.getEmployeeContribution());
		}
		if(view.getEmployerContribution()!=0){
			dbEmployerContribution.setValue(view.getEmployerContribution());
		}
		
//		if(view.getDeductionMonth()!=null){
//			for(int i=0;i<lbMonth.getItemCount();i++){
//				if(lbMonth.getItemText(i).equals(view.getPeriod())){
//					lbMonth.setSelectedIndex(i);
//					break;
//				}
//			}
//		}
		
		cbStatus.setValue(view.isStatus());
		dbFromAmount.setValue(view.getFromAmt());
		dbToAmount.setValue(view.getToAmt());
//		if(view.getPeriod()!=null){
//			olbPeriod.setValue(view.getPeriod());
//		}
		if(view.getLwfList() !=null){
			lwfBeanTable.getDataprovider().setList(view.getLwfList());
		}
		presenter.setModel(view);
		
	}
	
	@Override
	public void setCount(int count)
	{
		this.tbLWFId.setValue(presenter.getModel().getCount()+"");
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		boolean superVal=super.validate();
		
		if(lwfBeanTable.getDataprovider().getList().size()==0){
			showDialogMessage("Please add deduction period.");
			return false;
		}
		
		return superVal;
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbLWFId.setEnabled(false);
		lwfBeanTable.setEnable(state);
		
	}

	@Override
	public void setToNewState() {
		// TODO Auto-generated method stub
		super.setToNewState();
	}

	@Override
	public void setToViewState() {
		// TODO Auto-generated method stub
		super.setToViewState();
	}

	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		super.clear();
		cbStatus.setValue(true);
		
		//lbMonth.setEnabled(false);
		lwfBeanTable.connectToLocal();
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(btAdd)){
			
			LWFBean bean = new LWFBean();
//			if(dbEmployeeContribution.getValue()!=null){
//				bean.setEmployeeContribution(dbEmployeeContribution.getValue());
//			}else{
//				bean.setEmployeeContribution(0);
//			}
//			
//			if(dbEmployerContribution.getValue()!=null){
//				bean.setEmployerContribution(dbEmployerContribution.getValue());
//			}else{
//				bean.setEmployerContribution(0);
//			}
				
			if(lbMonth.getSelectedIndex()!=0){
				for(LWFBean lwfBean:lwfBeanTable.getDataprovider().getList()){
					if(lwfBean.getPeriod().equals(lbMonth.getValue(lbMonth.getSelectedIndex()))){
						showDialogMessage("Month is already added.");
						return;
					}
				}
				bean.setDeductionMonth(lbMonth.getValue(lbMonth.getSelectedIndex()));
			}else{
				bean.setDeductionMonth("");
				showDialogMessage("Please add deduction month.");
				return;
			}
			
//			if(olbPeriod.getSelectedIndex() != 0){
//				bean.setPeriod(olbPeriod.getValue(olbPeriod.getSelectedIndex()));
//			}else{
//				bean.setPeriod("");
//			}
//			
//			if(dbFromAmount.getValue()!=null){
//				bean.setFromAmt(dbFromAmount.getValue());
//			}
//			if(dbToAmount.getValue()!=null){
//				bean.setToAmt(dbToAmount.getValue());
//			}
			
			lwfBeanTable.getDataprovider().getList().add(bean);
		}
	}
	
	
	
	
	


}
