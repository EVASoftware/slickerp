package com.slicktechnologies.client.views.humanresource.deduction.pt;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;

public class PTTable extends SuperTable<ProfessionalTax> {

	TextColumn<ProfessionalTax> getPtIdCol;
	TextColumn<ProfessionalTax> getPtStateCol;
	TextColumn<ProfessionalTax> getPtCompNameCol;
	TextColumn<ProfessionalTax> getPtShortNameCol;
	TextColumn<ProfessionalTax> getPtAmtCol;
	TextColumn<ProfessionalTax> getPtStatusCol;
	
	@Override
	public void createTable() {
		getPtIdCol();
		getPtStateCol();
		getPtCompNameCol();
		getPtShortNameCol();
		getPtAmtCol();
		getPtStatusCol();
		
	}

	

	private void getPtIdCol() {
		getPtIdCol=new TextColumn<ProfessionalTax>() {
			@Override
			public String getValue(ProfessionalTax object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getPtIdCol, "PT Id");
		getPtIdCol.setSortable(true);
		
		
	}

	private void getPtStateCol() {
		getPtStateCol=new TextColumn<ProfessionalTax>() {
			@Override
			public String getValue(ProfessionalTax object) {
				if(object.getState()!=null){
					return object.getState();
				}
				return "";
			}
		};
		table.addColumn(getPtStateCol, "State");
		getPtStateCol.setSortable(true);
		
	
		
		
	}

	private void getPtCompNameCol() {
		getPtCompNameCol=new TextColumn<ProfessionalTax>() {
			@Override
			public String getValue(ProfessionalTax object) {
				if(object.getComponentName()!=null){
					return object.getComponentName();
				}
				return "";
			}
		};
		table.addColumn(getPtCompNameCol, "Component Name");
		getPtCompNameCol.setSortable(true);
		
		
	}

	private void getPtShortNameCol() {
		getPtShortNameCol=new TextColumn<ProfessionalTax>() {
			@Override
			public String getValue(ProfessionalTax object) {
				if(object.getShortName()!=null){
					return object.getShortName();
				}
				return "";
			}
		};
		table.addColumn(getPtShortNameCol, "Short Name");
		getPtShortNameCol.setSortable(true);
		
		
	}

	private void getPtAmtCol() {
		getPtAmtCol=new TextColumn<ProfessionalTax>() {
			@Override
			public String getValue(ProfessionalTax object) {
				if(object.getPtAmount()!=0){
					return object.getPtAmount()+"";
				}
				return "";
			}
		};
		table.addColumn(getPtAmtCol, "Amount");
		getPtAmtCol.setSortable(true);
		
	
	}
	
	private void getPtStatusCol() {
		getPtStatusCol=new TextColumn<ProfessionalTax>() {
			@Override
			public String getValue(ProfessionalTax object) {
				if(object.isStatus()==true){
					return "Active";
				}
				return "Inactive";
			}
		};
		table.addColumn(getPtStatusCol, "Status");
		getPtStatusCol.setSortable(true);
		
		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void addColumnSorting() {
//		super.addColumnSorting();
		addSortingOnPtIdCol();
		addSortingOnStateCol();
		addSortingOnCompNameCol();
		addSortingOnShortNameCol();
		addSortingOnAmtCol();
		addSortingOnStatus();
	}



	private void addSortingOnPtIdCol() {
		// TODO Auto-generated method stub
		List<ProfessionalTax> list = getDataprovider().getList();
		columnSort = new ListHandler<ProfessionalTax>(list);
		columnSort.setComparator(getPtIdCol, new Comparator<ProfessionalTax>() {
			@Override
			public int compare(ProfessionalTax e1, ProfessionalTax e2) {
				if (e1 != null&& e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					} else {
						if (e1.getCount() > e2.getCount())
							return 1;
					}
				}

				return -1;
			}
		});
		table.addColumnSortHandler(columnSort);
	}



	private void addSortingOnStateCol() {
		// TODO Auto-generated method stub
		List<ProfessionalTax> list = getDataprovider().getList();
		columnSort = new ListHandler<ProfessionalTax>(list);
		columnSort.setComparator(getPtStateCol,new Comparator<ProfessionalTax>() {
			@Override
			public int compare(ProfessionalTax e1, ProfessionalTax e2) {
				if (e1 != null && e2 != null) {
					if (e1.getShortName() != null&& e2.getShortName() != null) {
						return e1.getShortName().compareTo(e2.getShortName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}



	private void addSortingOnCompNameCol() {
		// TODO Auto-generated method stub
		List<ProfessionalTax> list = getDataprovider().getList();
		columnSort = new ListHandler<ProfessionalTax>(list);
		columnSort.setComparator(getPtCompNameCol,new Comparator<ProfessionalTax>() {
			@Override
			public int compare(ProfessionalTax e1, ProfessionalTax e2) {
				if (e1 != null && e2 != null) {
					if (e1.getComponentName() != null&& e2.getComponentName() != null) {
						return e1.getComponentName().compareTo(e2.getComponentName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}



	private void addSortingOnShortNameCol() {
		// TODO Auto-generated method stub
		List<ProfessionalTax> list = getDataprovider().getList();
		columnSort = new ListHandler<ProfessionalTax>(list);
		columnSort.setComparator(getPtShortNameCol,new Comparator<ProfessionalTax>() {
			@Override
			public int compare(ProfessionalTax e1, ProfessionalTax e2) {
				if (e1 != null && e2 != null) {
					if (e1.getShortName() != null&& e2.getShortName() != null) {
						return e1.getShortName().compareTo(e2.getShortName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}



	private void addSortingOnAmtCol() {
		// TODO Auto-generated method stub
		List<ProfessionalTax> list = getDataprovider().getList();
		columnSort = new ListHandler<ProfessionalTax>(list);
		columnSort.setComparator(getPtAmtCol, new Comparator<ProfessionalTax>() {
			@Override
			public int compare(ProfessionalTax e1, ProfessionalTax e2) {
				if (e1 != null&& e2 != null) {
					if (e1.getPtAmount() == e2.getPtAmount()) {
						return 0;
					} else {
						if (e1.getPtAmount() > e2.getPtAmount())
							return 1;
					}
				}

				return -1;
			}
		});
		table.addColumnSortHandler(columnSort);
	}



	private void addSortingOnStatus() {
		// TODO Auto-generated method stub
		List<ProfessionalTax> list = getDataprovider().getList();
		columnSort = new ListHandler<ProfessionalTax>(list);
		columnSort.setComparator(getPtStatusCol,new Comparator<ProfessionalTax>() {
			@Override
			public int compare(ProfessionalTax e1, ProfessionalTax e2) {
				if (e1 != null && e2 != null) {
					if (e1.isStatus() == e2.isStatus()) {
						return 0;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	

}
