package com.slicktechnologies.client.views.humanresource.deduction.pt;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;

public class PTPresenter extends FormTableScreenPresenter<ProfessionalTax>{
	PTForm form;
	public PTPresenter(FormTableScreen<ProfessionalTax> view,ProfessionalTax model) {
		super(view, model);
		form=(PTForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getPTQuery());
		form.setPresenter(this);
	}

	private MyQuerry getPTQuery() {
		MyQuerry quer=new MyQuerry();
		quer.setQuerryObject(new ProfessionalTax());
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		quer.getFilters().add(filter);
		return quer;
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("New")){
			form.setToNewState();
			this.initialize();
		}
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		model=new ProfessionalTax();
	}
	
	public static void initialize(){
		PTTable gentableScreen=new PTTable();
		PTForm  form=new  PTForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		PTPresenter  presenter=new  PTPresenter  (form,new ProfessionalTax());
		AppMemory.getAppMemory().stickPnel(form);
	}

}
