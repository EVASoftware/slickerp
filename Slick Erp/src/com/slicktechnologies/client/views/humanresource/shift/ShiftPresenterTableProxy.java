package com.slicktechnologies.client.views.humanresource.shift;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.client.views.humanresource.shift.ShiftPresenter.ShiftPresenterTable;
import com.slicktechnologies.client.utility.Screen;

public class ShiftPresenterTableProxy extends ShiftPresenterTable {
  TextColumn<Shift> setCategoryColumn;
  TextColumn<Shift> getFromTimeColumn;
  TextColumn<Shift> getToTimeColumn;
  TextColumn<Shift> getShiftNameColumn;
  TextColumn<Shift> getShortNameColumn;
  TextColumn<Shift> getCountColumn;
  /** date 27.8.2018 added by komal for status**/
  TextColumn<Shift> getStatusColumn;
  
  public Object getVarRef(String varName)
  {
  if(varName.equals("setCategoryColumn"))
  return this.setCategoryColumn;
  if(varName.equals("getFromTimeColumn"))
  return this.getFromTimeColumn;
  if(varName.equals("getToTimeColumn"))
  return this.getToTimeColumn;
  if(varName.equals("getShiftNameColumn"))
  return this.getShiftNameColumn;
  if(varName.equals("getShortNameColumn"))
  return this.getShortNameColumn;
  if(varName.equals("getCountColumn"))
  return this.getCountColumn;
  /** date 27.8.2018 added by komal for status**/
  if(varName.equals("getStatusColumn"))
	  return this.getStatusColumn;
   return null ;
  }
  public ShiftPresenterTableProxy()
  {
  super();
  }
  @Override public void createTable() {
  addColumngetCount();
  addColumnsetCategory();
  addColumngetShiftName();
  addColumngetShortName();
  addColumngetFromTime();
  addColumngetToTime();
  /** date 27.8.2018 added by komal for status**/
  addcolumngetStatus();
  }
  @Override
  protected void initializekeyprovider() {
  keyProvider= new ProvidesKey<Shift>()
  {
  @Override
  public Object getKey(Shift item)
  {
  if(item==null)
  {
  return null;
  }
  else
  return item.getId();
  }
  };
  }
  @Override
  public void setEnable(boolean state)
  {
  }
  @Override
  public void applyStyle()
  {
  }
   public void addColumnSorting(){
  addSortinggetCount();
  addSortingsetCategory();
  addSortinggetShiftName();
  addSortinggetShortName();
  addSortinggetFromTime();
  addSortinggetToTime();
  addStatusSorting();
  }
  @Override public void addFieldUpdater() {
  }
  protected void addSortinggetCount()
  {
  List<Shift> list=getDataprovider().getList();
  columnSort=new ListHandler<Shift>(list);
  columnSort.setComparator(getCountColumn, new Comparator<Shift>()
  {
  @Override
  public int compare(Shift e1,Shift e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCount()== e2.getCount()){
  return 0;}
  if(e1.getCount()> e2.getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCount()
  {
  getCountColumn=new TextColumn<Shift>()
  {
  @Override
  public String getValue(Shift object)
  {
  if( object.getCount()==-1)
  return "N.A";
  else return object.getCount()+"";
  }
  };
  table.addColumn(getCountColumn,"Id");
  getCountColumn.setSortable(true);
  }
  protected void addSortingsetCategory()
  {
  List<Shift> list=getDataprovider().getList();
  columnSort=new ListHandler<Shift>(list);
  columnSort.setComparator(setCategoryColumn, new Comparator<Shift>()
  {
  @Override
  public int compare(Shift e1,Shift e2)
  {
  if(e1!=null && e2!=null)
  {
	  return e1.getCategory().compareTo(e2.getCategory());
  }
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumnsetCategory()
  {
  setCategoryColumn=new TextColumn<Shift>()
  {
  @Override
  public String getValue(Shift object)
  {
  return object.getCategory();
  }
  };
  table.addColumn(setCategoryColumn,"Shift Category");
  setCategoryColumn.setSortable(true);
  }
  protected void addSortinggetShiftName()
  {
  List<Shift> list=getDataprovider().getList();
  columnSort=new ListHandler<Shift>(list);
  columnSort.setComparator(getShiftNameColumn, new Comparator<Shift>()
  {
  @Override
  public int compare(Shift e1,Shift e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getShiftName()!=null && e2.getShiftName()!=null){
  return e1.getShiftName().compareTo(e2.getShiftName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetShiftName()
  {
  getShiftNameColumn=new TextColumn<Shift>()
  {
  @Override
  public String getValue(Shift object)
  {
  return object.getShiftName()+"";
  }
  };
  table.addColumn(getShiftNameColumn,"Shift");
  getShiftNameColumn.setSortable(true);
  }
  protected void addSortinggetShortName()
  {
  List<Shift> list=getDataprovider().getList();
  columnSort=new ListHandler<Shift>(list);
  columnSort.setComparator(getShortNameColumn, new Comparator<Shift>()
  {
  @Override
  public int compare(Shift e1,Shift e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getShortName()!=null && e2.getShortName()!=null){
  return e1.getShortName().compareTo(e2.getShortName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetShortName()
  {
  getShortNameColumn=new TextColumn<Shift>()
  {
  @Override
  public String getValue(Shift object)
  {
  return object.getShortName()+"";
  }
  };
  table.addColumn(getShortNameColumn,"Short Name");
  getShortNameColumn.setSortable(true);
  }
  protected void addSortinggetFromTime()
  {
  List<Shift> list=getDataprovider().getList();
  columnSort=new ListHandler<Shift>(list);
  columnSort.setComparator(getFromTimeColumn, new Comparator<Shift>()
  {
  @Override
  public int compare(Shift e1,Shift e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getFromTime()== e2.getFromTime()){
  return 0;}
  if(e1.getFromTime()> e2.getFromTime()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetFromTime()
  {
  getFromTimeColumn=new TextColumn<Shift>()
  {
  @Override
  public String getValue(Shift object)
  {
  return object.getFromTime()+"";
  }
  };
  table.addColumn(getFromTimeColumn,"In Time(24hr)");
  getFromTimeColumn.setSortable(true);
  }
  protected void addSortinggetToTime()
  {
  List<Shift> list=getDataprovider().getList();
  columnSort=new ListHandler<Shift>(list);
  columnSort.setComparator(getToTimeColumn, new Comparator<Shift>()
  {
  @Override
  public int compare(Shift e1,Shift e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getToTime()== e2.getToTime()){
  return 0;}
  if(e1.getToTime()> e2.getToTime()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetToTime()
  {
  getToTimeColumn=new TextColumn<Shift>()
  {
  @Override
  public String getValue(Shift object)
  {
  return object.getToTime()+"";
  }
  };
  table.addColumn(getToTimeColumn,"Out Time(24hr)");
  getToTimeColumn.setSortable(true);
  }
  /** date 27.8.2018 added by komal for status**/
  protected void addcolumngetStatus()
  {
  getStatusColumn=new TextColumn<Shift>()
  {
  @Override
  public String getValue(Shift object)
  {
	  if(object.isStatus()){
		  return "Active";
	  }else{
		  return "Inactive";
	  }
  
  }
  };
  table.addColumn(getStatusColumn,"Status");
  getStatusColumn.setSortable(true);
  }
  public void addStatusSorting()
  {
  	List<Shift> list=getDataprovider().getList();
  	columnSort=new ListHandler<Shift>(list);
  	columnSort.setComparator(getStatusColumn, new Comparator<Shift>()
  			{
  				
  				@Override
  				public int compare(Shift e1, Shift e2) 
  				{
  					if(e1!=null && e2!=null)
  					{
  						if(e1.isStatus()==e2.isStatus())
  							
  						return 0;
  						else if(e1.isStatus()==true)
  							return 1;
  						else
  							return -1;
  					}
  					else
  						return -1;
  				}
  			});
  	table.addColumnSortHandler(columnSort);
  }
}
