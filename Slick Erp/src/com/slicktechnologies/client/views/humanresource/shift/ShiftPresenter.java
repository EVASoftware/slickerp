package com.slicktechnologies.client.views.humanresource.shift;
import com.google.gwt.event.dom.client.ClickEvent;

import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
/**
 *  FormTableScreen presenter template
 */
public class ShiftPresenter extends FormTableScreenPresenter<Shift>  {

	//Token to set the concrete form
	ShiftForm form;
	
	public ShiftPresenter (FormTableScreen<Shift> view,	Shift model) {
		super(view, model);
		form=(ShiftForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getShiftQuery());
		
		form.setPresenter(this);
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("Delete"))
		{
			
			if(this.form.validate())
			{
			List<Shift>data=form.getSupertable().getDataprovider().getList();
			/*
			 * Deleting
			 */
			for(int i=0;i<data.size();i++)
			{
				if(data.get(i).getId().equals(model.getId()))
				{
					data.remove(i);
				}
				form.getSupertable().getDataprovider().refresh();
			}
			reactOnDelete();
			
			}
		}
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
	}
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new Shift();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getShiftQuery()
	{
		
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new Shift());
		
		return quer;
	}
	
	
	public void setModel(Shift entity)
	{
		model=entity;
	}
	
	public static void initalize()
	{
			 ShiftPresenterTable gentableScreen=new ShiftPresenterTableProxy();
			
			ShiftForm  form=new  ShiftForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			  //// ShiftPresenterTable gentableSearch=GWT.create( ShiftPresenterTable.class);
			  ////   ShiftPresenterSearch.staticSuperTable=gentableSearch;
			  ////     ShiftPresenterSearch searchpopup=GWT.create( ShiftPresenterSearch.class);
			  ////         form.setSearchpopupscreen(searchpopup);
			
			 ShiftPresenter  presenter=new  ShiftPresenter  (form,new Shift());
			 AppMemory.getAppMemory().stickPnel(form);
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.Shift")
		 public static class  ShiftPresenterTable extends SuperTable<Shift> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.Shift")
			 public static class  ShiftPresenterSearch extends SearchPopUpScreen<Shift>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					return true;
				}};
				
				public void reactTo()
				{
					
				}

}
