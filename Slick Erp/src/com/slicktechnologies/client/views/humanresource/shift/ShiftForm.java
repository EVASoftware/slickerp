package com.slicktechnologies.client.views.humanresource.shift;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.*;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.event.dom.client.*;
/**
 * FormTablescreen template.
 */
public class ShiftForm extends FormTableScreen<Shift>  implements ClickHandler {

	//Token to add the varialble declarations
	ObjectListBox<Config> olbShiftCategory;
	TextBox tbShiftName,tbShortName;
	DoubleBox ibFromTime,ibToTime;
	
	TextArea tbDescription;
	/** date 11.8.2018 added by komal for intimegrace  and outtimegraceperiod **/
	DoubleBox dbIntimeGracePeriod;
	DoubleBox dbOuttimeGracePeriod;

	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;
	/** date 27.8.2018 added by komal for shift status**/
	CheckBox cbStatus ;

	public ShiftForm  (SuperTable<Shift> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		cbStatus.setValue(true);
	}

	private void initalizeWidget()
	{

		ibFromTime = new DoubleBox();
		ibToTime = new DoubleBox();
		olbShiftCategory = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbShiftCategory,Screen.SHIFTCATEGORY);
		tbShiftName=new TextBox();
		tbShortName=new TextBox();
		tbDescription=new TextArea();
		
		ibFromTime.addKeyPressHandler(new NumbersOnly());
		ibToTime.addKeyPressHandler(new NumbersOnly());
		/** date 11.8.2018 added by komal for intimegrace  and outtimegraceperiod **/
		dbIntimeGracePeriod = new DoubleBox();
		dbOuttimeGracePeriod = new DoubleBox();
		/** date 27.8.2018 added by komal for shift status**/
		cbStatus = new CheckBox("Status");
		cbStatus.setValue(true);
		 
	
	}

	
	
	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames=new String[]{"New"};//"Delete",
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingShiftInformation=fbuilder.setlabel("Shift Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("* Shift Name",tbShiftName);
		FormField ftbShiftName= fbuilder.setMandatory(true).setMandatoryMsg("Shift Name is Mandatory !").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Short Name",tbShortName);
		FormField ftbShortName= fbuilder.setMandatory(true).setMandatoryMsg("Short Name is Mandatory !").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Shift Category",olbShiftCategory);
		FormField folbShiftCategory= fbuilder.setMandatory(true).setMandatoryMsg("Shift Category is Mandatory !").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* From Time(24hrs Format)",ibFromTime);
		FormField fibFromTime= fbuilder.setMandatory(true).setMandatoryMsg("From Time is Mandatory !").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* To Time(24hrs Format)",ibToTime);
		FormField fibToTime= fbuilder.setMandatory(true).setMandatoryMsg("To Time is Mandatory !").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Description",tbDescription);
		FormField ftbDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		/** date 11.8.2018 added by komal for intimegrace  and outtimegraceperiod **/
		fbuilder = new FormFieldBuilder("Intime Grace Period",dbIntimeGracePeriod);
		FormField fdbIntimeGracePeriod= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Outtime Grace Period",dbOuttimeGracePeriod);
		FormField fdbOuttimeGracePeriod= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/** date 27.8.2018 added by komal for shift status**/
		fbuilder = new FormFieldBuilder("",cbStatus);
		FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingShiftInformation},
				{ftbShiftName,ftbShortName},
				{folbShiftCategory,fibFromTime,fibToTime},
				{fdbIntimeGracePeriod , fdbOuttimeGracePeriod , fcbStatus},/** date 11.8.2018 added by komal for intimegrace  and outtimegraceperiod **/
				{ftbDescription},
		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Shift model) 
	{
		if(tbShiftName.getValue()!=null)
			model.setShiftName(tbShiftName.getValue());
		if(tbShortName.getValue()!=null)
			model.setShortName(tbShortName.getValue());
		if(olbShiftCategory.getValue()!=null)
			model.setCategory(olbShiftCategory.getValue());
		model.setFromTime(ibFromTime.getValue());
		model.setToTime(ibToTime.getValue());
		if(tbDescription.getValue()!=null)
			model.setShiftDesc(tbDescription.getValue());
		/** date 11.8.2018 added by komal for intimegrace  and outtimegraceperiod **/
		if(dbIntimeGracePeriod.getValue()!=null){
			model.setIntimeGracePeriod(dbIntimeGracePeriod.getValue());
		}
		if(dbOuttimeGracePeriod.getValue()!=null){
			model.setOuttimeGracePeriod(dbOuttimeGracePeriod.getValue());
		}
		/** date 27.8.2018 added by komal for shift status**/
		model.setStatus(cbStatus.getValue());
		presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Shift view) 
	{
		if(view.getShiftName()!=null)
			tbShiftName.setValue(view.getShiftName());
		if(view.getShortName()!=null)
			tbShortName.setValue(view.getShortName());
		if(view.getCategory()!=null)
			olbShiftCategory.setValue(view.getCategory());
		if(view.getFromTime()!=null)
			ibFromTime.setValue(view.getFromTime());
		if(view.getToTime()!=null)
			ibToTime.setValue(view.getToTime());
		if(view.getShiftDesc()!=null)
			tbDescription.setValue(view.getShiftDesc());
		/** date 11.8.2018 added by komal for intimegrace  and outtimegraceperiod **/
		dbIntimeGracePeriod.setValue(view.getIntimeGracePeriod());
		dbOuttimeGracePeriod.setValue(view.getOuttimeGracePeriod());
		/** date 27.8.2018 added by komal for shift status**/
		cbStatus.setValue(cbStatus.getValue());

		presenter.setModel(view);
	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SHIFT,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */



	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		boolean superValidate= super.validate();
		if(superValidate==false)
			return false;
		
	   boolean formValidate=timeValidation();
	   if(formValidate==false)
		   return false;
		return true;

	}

	public boolean timeValidation()
	{
		if(ibFromTime.getValue()==null)
		{
			showDialogMessage("From Time Should be in valid format.");
			return false;

		}

		if(ibToTime.getValue()==null)
		{
			showDialogMessage("To Time Should be in valid format.");
			return false;
		}

		
		if(ibFromTime.getValue()!=null&&ibToTime.getValue()!=null)
		{
			if((ibFromTime.getValue()>24)||ibToTime.getValue()>24)
			{
				showDialogMessage("Time Cannot be greater then 24.");
				return false;

			}
			if((ibFromTime.getValue()<0)||(ibToTime.getValue()<0))	
			{
				showDialogMessage(" Time Cannot be Less then 0.");
				return false;

			}
		}

		
//		if(ibFromTime.getValue()!=null&&ibToTime.getValue()!=null)
//		{
//
//
//			if(ibFromTime.getValue()>ibToTime.getValue())
//			{
//				showDialogMessage("To Time Should be greater then From Time.");
//				return false;
//
//			}	
//
//		}

		return true;
	}

	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
		//isWo.setValue(false);
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		super.clear();
		cbStatus.setValue(true);
	}




}



