package com.slicktechnologies.client.views.humanresource.advancetype;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;
import java.util.List;

import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneType;
import com.slicktechnologies.client.views.humanresource.advancetype.AdvanceTypePresenter.AdvanceTypePresenterTable;

public class AdvanceTypePresenterTableProxy extends AdvanceTypePresenterTable {
	TextColumn<LoneType> getCountColumn;
	TextColumn<LoneType> getStatusColumn;
	TextColumn<LoneType> getMaxAdvanceColumn;
	TextColumn<LoneType> getAdvanceTypeNameColumn;
	TextColumn<LoneType> getAdvanceDurationColumn;
	TextColumn<LoneType> getAdvanceInterestColumn;
	

	public Object getVarRef(String varName) {
		
		if (varName.equals("getCountColumn"))
			return this.getCountColumn;
		if (varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if (varName.equals("getMaxAdvanceColumn"))
			return this.getMaxAdvanceColumn;
		if (varName.equals("getAdvanceTypeNameColumn"))
			return this.getAdvanceTypeNameColumn;
		if (varName.equals("getAdvanceDurationColumn"))
			return this.getAdvanceDurationColumn;
		if (varName.equals("getAdvanceInterestColumn"))
			return this.getAdvanceInterestColumn;
		
		return null;
	}

	public AdvanceTypePresenterTableProxy() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetAdvanceTypeName();
		addColumngetAdvanceDuration();
		addColumngetAdvanceInterest();
		addColumngetMaxAdvance();
		addColumngetStatus();
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<LoneType>() {
			@Override
			public Object getKey(LoneType item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetAdvanceTypeName();
		addSortinggetAdvanceDuration();
		addSortinggetAdvanceInterest();
		addSortinggetMaxAdvance();
		addSortinggetStatus();
	}

	@Override
	public void addFieldUpdater() {
	}
	
	protected void addColumngetCount() {
		getCountColumn = new TextColumn<LoneType>() {
			@Override
			public String getValue(LoneType object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		getCountColumn.setSortable(true);
		table.setColumnWidth(getCountColumn,"110px"); /////////////////
	}
	
	protected void addColumngetAdvanceTypeName() {
		getAdvanceTypeNameColumn = new TextColumn<LoneType>() {
			@Override
			public String getValue(LoneType object) {
				return object.getAdvanceTypeName() + "";
			}
		};
		table.addColumn(getAdvanceTypeNameColumn, "Name");
		getAdvanceTypeNameColumn.setSortable(true);
		// table.setColumnWidth(getAdvanceTypeNameColumn,"170px");
		// //////////////////////
	}
	
	protected void addColumngetAdvanceDuration() {
		getAdvanceDurationColumn = new TextColumn<LoneType>() {
			@Override
			public String getValue(LoneType object) {
				return object.getAdvanceDuration() + "";
			}
		};
		table.addColumn(getAdvanceDurationColumn, "Duration In Months");
		getAdvanceDurationColumn.setSortable(true);
		// table.setColumnWidth(getAdvanceDurationColumn,"110px");
		// ///////////////////
	}
	
	protected void addColumngetAdvanceInterest() {
		getAdvanceInterestColumn = new TextColumn<LoneType>() {
			@Override
			public String getValue(LoneType object) {
				return object.getAdvanceInterest() + "";
			}
		};
		table.addColumn(getAdvanceInterestColumn, "Interest Rate(%)");
		getAdvanceInterestColumn.setSortable(true);
		// table.setColumnWidth(getAdvanceInterestColumn,"110px");
		// ////////////////////////
	}
	
	protected void addColumngetMaxAdvance() {
		getMaxAdvanceColumn = new TextColumn<LoneType>() {
			@Override
			public String getValue(LoneType object) {
				return object.getMaxAdvance() + "";
			}
		};
		table.addColumn(getMaxAdvanceColumn, "Maximum Advance");
		getMaxAdvanceColumn.setSortable(true);
		// table.setColumnWidth(getMaxAdvanceColumn,"110px");
		// /////////////////////////////
	}
	
	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<LoneType>() {
			@Override
			public String getValue(LoneType object) {
				if (object.getStatus() == true)
					return "Active";
				else
					return "In Active";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
		// table.setColumnWidth(getStatusColumn,"110px");
		// //////////////////////////////
	}

	
	

	protected void addSortinggetCount() {
		List<LoneType> list = getDataprovider().getList();
		columnSort = new ListHandler<LoneType>(list);
		columnSort.setComparator(getCountColumn, new Comparator<LoneType>() {
			@Override
			public int compare(LoneType e1, LoneType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetAdvanceTypeName() {
		List<LoneType> list = getDataprovider().getList();
		columnSort = new ListHandler<LoneType>(list);
		columnSort.setComparator(getAdvanceTypeNameColumn,
				new Comparator<LoneType>() {
					@Override
					public int compare(LoneType e1, LoneType e2) {
						if (e1 != null && e2 != null) {
							if (e1.getAdvanceTypeName() != null
									&& e2.getAdvanceTypeName() != null) {
								return e1.getAdvanceTypeName().compareTo(
										e2.getAdvanceTypeName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetAdvanceDuration() {
		List<LoneType> list = getDataprovider().getList();
		columnSort = new ListHandler<LoneType>(list);
		columnSort.setComparator(getAdvanceDurationColumn,
				new Comparator<LoneType>() {
					@Override
					public int compare(LoneType e1, LoneType e2) {
						if (e1 != null && e2 != null) {
							if (e1.getAdvanceDuration() == e2
									.getAdvanceDuration()) {
								return 0;
							}
							if (e1.getAdvanceDuration() > e2
									.getAdvanceDuration()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetAdvanceInterest() {
		List<LoneType> list = getDataprovider().getList();
		columnSort = new ListHandler<LoneType>(list);
		columnSort.setComparator(getAdvanceInterestColumn,
				new Comparator<LoneType>() {
					@Override
					public int compare(LoneType e1, LoneType e2) {
						if (e1 != null && e2 != null) {
							if (e1.getAdvanceInterest() == e2
									.getAdvanceInterest()) {
								return 0;
							}
							if (e1.getAdvanceInterest() > e2
									.getAdvanceInterest()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetMaxAdvance() {
		List<LoneType> list = getDataprovider().getList();
		columnSort = new ListHandler<LoneType>(list);
		columnSort.setComparator(getMaxAdvanceColumn,
				new Comparator<LoneType>() {
					@Override
					public int compare(LoneType e1, LoneType e2) {
						if (e1 != null && e2 != null) {
							if (e1.getMaxAdvance() == e2.getMaxAdvance()) {
								return 0;
							}
							if (e1.getMaxAdvance() > e2.getMaxAdvance()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetStatus() {
		List<LoneType> list = getDataprovider().getList();
		columnSort = new ListHandler<LoneType>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<LoneType>() {
			@Override
			public int compare(LoneType e1, LoneType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() == e2.getStatus()) {
						return 0;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

}
