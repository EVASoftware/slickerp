package com.slicktechnologies.client.views.humanresource.advancetype;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneType;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class AdvanceTypeForm  extends FormTableScreen<LoneType> implements ClickHandler
{

	IntegerBox ibadvanceDuration,ibmaxAdvanceAllow;
	DoubleBox dbadvanceInterest;
	TextBox tbadvanceTypeName;
	CheckBox status;



	public AdvanceTypeForm  (SuperTable<LoneType> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();

	}

	private void initalizeWidget()
	{

		ibadvanceDuration=new IntegerBox();
		dbadvanceInterest=new DoubleBox();
		tbadvanceTypeName=new TextBox();
		ibmaxAdvanceAllow=new IntegerBox();
		status= new CheckBox();
		status.setValue(true);


	}

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;

		fbuilder = new FormFieldBuilder();
		FormField fgroupingAdvanceInformation=fbuilder.setlabel("Advance Type Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("* Name",tbadvanceTypeName);
		FormField ftbadvanceName= fbuilder.setMandatory(true).setMandatoryMsg("Name is mandatroy!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Duration In Months",ibadvanceDuration);
		FormField fibadvanceDuration= fbuilder.setMandatory(true).setMandatoryMsg("Duration is mandatroy!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Interest Rate(%)",dbadvanceInterest);
		FormField fdbadvanceInterest= fbuilder.setMandatory(true).setMandatoryMsg("Interest Rate is mandatroy!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",status);
		FormField fstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Maximum Advance Amount",ibmaxAdvanceAllow);
		FormField fibmaxAdvanceAllow= fbuilder.setMandatory(true).setMandatoryMsg("Maximum Advance Amount is mandatroy!").setRowSpan(0).setColSpan(0).build();



		FormField[][] formfield = {   {fgroupingAdvanceInformation},
				{ftbadvanceName,fstatus},
				{fibadvanceDuration,fibmaxAdvanceAllow,fdbadvanceInterest},
		};


		this.fields=formfield;

	}

	@Override
	public void updateModel(LoneType model) 
	{

		if(tbadvanceTypeName.getValue()!=null)
			model.setAdvanceTypeName(tbadvanceTypeName.getValue());
		model.setAdvanceDuration(ibadvanceDuration.getValue());
		model.setAdvanceInterest(dbadvanceInterest.getValue());
		if(status.getValue()!=null)
			model.setStatus(status.getValue());
		model.setMaxAdvance(ibmaxAdvanceAllow.getValue());
		presenter.setModel(model);
	}



	@Override
	public void updateView(LoneType view) 
	{
		if(view.getAdvanceTypeName()!=null)
			tbadvanceTypeName.setValue(view.getAdvanceTypeName());
		if(view.getStatus()!=null)
			status.setValue(view.getStatus());
		ibadvanceDuration.setValue(view.getAdvanceDuration());
		dbadvanceInterest.setValue(view.getAdvanceInterest());
		ibmaxAdvanceAllow.setValue(view.getMaxAdvance());



		presenter.setModel(view);
	}



	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.ADVANCETYPE,LoginPresenter.currentModule.trim());
	}


	

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
		
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub

	}
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}
	@Override
	public void clear() {
		super.clear();
		status.setValue(true);
	}


}
