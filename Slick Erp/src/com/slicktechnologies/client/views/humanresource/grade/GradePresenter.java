package com.slicktechnologies.client.views.humanresource.grade;
import com.google.gwt.event.dom.client.ClickEvent;

import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.humanresourcelayer.Grade;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneType;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;

import java.util.Vector;

import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.slicktechnologies.shared.common.paymentlayer.*;
import com.google.gwt.event.dom.client.*;
/**
 *  FormTableScreen presenter template
 */
public class GradePresenter extends FormTableScreenPresenter<Grade>  {

	//Token to set the concrete form
	GradeForm form;
	
	public GradePresenter (FormTableScreen<Grade> view,
			Grade model) {
		super(view, model);
		form=(GradeForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getGradeQuery());
		form.setPresenter(this);
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {

		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("Delete"))
		{
			
			if(this.form.validate())
			{
			List<Grade>data=form.getSupertable().getDataprovider().getList();
			/*
			 * Deleting
			 */
			for(int i=0;i<data.size();i++)
			{
				if(data.get(i).getId().equals(model.getId()))
				{
					data.remove(i);
				}
				form.getSupertable().getDataprovider().refresh();
			}
			reactOnDelete();
			
			}
		}
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}

	}
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new Grade();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getGradeQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new Grade());
		return quer;
	}
	
	public void setModel(Grade entity)
	{
		model=entity;
	}
	
	public static void initalize()
	{
			 GradePresenterTable gentableScreen=GWT.create( GradePresenterTable.class);
			
			GradeForm  form=new  GradeForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			  //// GradePresenterTable gentableSearch=GWT.create( GradePresenterTable.class);
			  ////   GradePresenterSearch.staticSuperTable=gentableSearch;
			  ////     GradePresenterSearch searchpopup=GWT.create( GradePresenterSearch.class);
			  ////         form.setSearchpopupscreen(searchpopup);
			
			 GradePresenter  presenter=new  GradePresenter  (form,new Grade());
			AppMemory.getAppMemory().stickPnel(form);
			
			
		 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.Grade")
		 public static class  GradePresenterTable extends SuperTable<Grade> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.Grade")
			 public static class  GradePresenterSearch extends SearchPopUpScreen<  Grade>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
				
				  private void reactTo()
		  {
		}

}
