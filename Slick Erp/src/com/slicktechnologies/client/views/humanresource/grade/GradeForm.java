package com.slicktechnologies.client.views.humanresource.grade;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.humanresourcelayer.Grade;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.event.dom.client.*;
/**
 * FormTablescreen template.
 */
public class GradeForm extends FormTableScreen<Grade>  implements ClickHandler {

	//Token to add the varialble declarations
  CheckBox cbStatus;
  TextBox tbGradeName,tbGradeCode;
  TextArea taDescription;
  
	
	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;
	
	
	
	public GradeForm  (SuperTable<Grade> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		
	}
	
	private void initalizeWidget()
	{
		
	cbStatus=new CheckBox();
	cbStatus.setValue(true);
	tbGradeName=new TextBox();
	tbGradeCode=new TextBox();
	taDescription=new TextArea();
	}

	/*
	 * Method template to create the formtable screen
	 */
	
	@Override
	public void createScreen() {
		
		//Token to initialize the processlevel menus.
	    initalizeWidget();
		
	    this.processlevelBarNames=new String[]{"Delete","New"};
		
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////
	    
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
fbuilder = new FormFieldBuilder();
FormField fgroupingGradeInformation=fbuilder.setlabel("Grade Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
fbuilder = new FormFieldBuilder("* Grade Name",tbGradeName);
FormField ftbGradeName= fbuilder.setMandatory(true).setMandatoryMsg("Grade Name is mandatory!").setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("* Grade Code",tbGradeCode);
FormField ftbGradeCode= fbuilder.setMandatory(true).setMandatoryMsg("Grade Code is mandatory!").setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("Description",taDescription);
FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
fbuilder = new FormFieldBuilder("Status",cbStatus);
FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		
		
		FormField[][] formfield = {   {fgroupingGradeInformation},
  {ftbGradeName,ftbGradeCode,fcbStatus},
  {ftaDescription}
  };

		
        this.fields=formfield;		
   }

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Grade model) 
	{
		
	if(tbGradeName.getValue()!=null)
	  model.setGradeName(tbGradeName.getValue());
	if(tbGradeCode.getValue()!=null)
	  model.setGradeCode(tbGradeCode.getValue());
	if(taDescription.getValue()!=null)
	  model.setDescription(taDescription.getValue());
	if(cbStatus.getValue()!=null)
	  model.setStatus(cbStatus.getValue());
		
	presenter.setModel(model);
		
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Grade view) 
	{
	if(view.getGradeName()!=null)
	  tbGradeName.setValue(view.getGradeName());
	if(view.getGradeCode()!=null)
	  tbGradeCode.setValue(view.getGradeCode());
	if(view.getDescription()!=null)
	  taDescription.setValue(view.getDescription());
	if(view.getStatus()!=null)
	  cbStatus.setValue(view.getStatus());
  
	presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.GRADE,LoginPresenter.currentModule.trim());
	}
	
	

	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
		cbStatus.setValue(true);
		
	}
	
	

}
