package com.slicktechnologies.client.views.humanresource.ctc;


import java.util.Collections;
import java.util.Comparator;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.DecimalWithSinglePrecion;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.history.HistoryPopup;
import com.slicktechnologies.client.views.popups.EmailPopup;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.Bonus;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.PaidLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;

public class CTCForm extends FormScreen<CTC> implements ClickHandler, ChangeHandler
{
	EmployeeInfoComposite pic;
	/**
	 * Date : 04-05-2018 BY ANIL
	 * commented calendar 
	 * Removing calendar dependency from CTC
	 */
//	TextBox leaveCalendar;
	
	/**
	 * End
	 */
	
	DateBox dbFromDate,dbToDate;
	TextBox CTCId;
	DoubleBox grossEarning,ctcAmount,difference,ctcCopy;
	CheckBox status;
	SalaryHeadPresenterTableProxy earningtable;
	
	
	FormField fgrossEarning;
	FormField fsecgrossearnings;
	FormField fdifference;
	FormField fdbToDate, fdbfromDate;
	 FormField fdbMonthlyCtcAmount;
	TextBox tbStatus;
	CTC ctcobj;
	
	
	
	/**
	 * Date : 28-04-2018 By ANIL
	 * Adding deduction component at employee level
	 * FOR Sasha ERP/Facility Mgmt
	 */
	SalaryHeadPresenterTableProxy deductionTable;
	
	/**
	 * Date : 03-05-2018 By ANIL
	 * Adding companies deduction at employee level
	 * For Sasha ERP/Facility Mgmt
	 */
	
	SalaryHeadPresenterTableProxy companyContributionTable;
	DoubleBox dbEmployeeTotalDeduction;
	DoubleBox dbCompanyTotalDeduction;
	
	FormField fempTotDeduction, fcomTotDeduction;
	
	/**
	 * Date : 04-05-2018 BY ANIL
	 * 
	 */
	DateBox dbEffectiveFrom;
	
	/**
	 * Date : 07-05-2018 BY ANIL
	 * Added Tab Panel
	 */
	TabPanel tabPanel ;
	
	CheckBox cbPaidLeave;
	
	/***
	 * Date : 21-08-2018 By ANIL
	 */
	TextBox tbCtcTempId;
	TextBox tbCtcTempName;
	DateBox dbCtcTempDate;
	TextBox tbCtcTempCreatedBy;
	
	/**
	 * Date : 27-08-2018 By ANIL
	 * Updated Paid Leave and Bonus 
	 */
	RadioButton rbMonthly;
	RadioButton rbAnnually;
	DoubleBox dbPaidLeaveAmt;
	DoubleBox dbBonusAmt;
	CheckBox cbBonus;
	ListBox lbMonth;
	/**
	 * End
	 */
	/**Date 20-4-2019 by Amol
	 * added for total CTC Amount
	 */
	DoubleBox dbMonthlyCtcAmount;
	
	 /**
	  * @author Anil , Date : 19-06-2019
	  * Paid Leave and bonus as per the formula 
	  * for riseon raised by Rahul Tiwari
	  */
	   boolean isPlAndBonusAsPerTheFormula=false;
	   CheckBox cbIncludePlInEarning;
	   CheckBox cbIncludeBonusInEarning;
	   TextBox tbPlCorrespondanceName;
	   TextBox tbBonusCorrespondanceName;
	   ObjectListBox<PaidLeave> oblPaidLeave;
	   
	   ObjectListBox<Bonus> oblBonus;
	   InlineLabel blankLable;
	   
	   /**
	     * @author Anil , Date : 12-07-2019
	     * Capturing project name for processing multiple site payroll
	     * raised by Rahul tiwari for riseon
	     */
	   
	   ObjectListBox<HrProject> project;
	   DateBox dbEffectiveTo; //dbEffectiveFrom
	
	
	public  CTCForm() {
		super(FormStyle.DEFAULT);
		createGui();
		grossEarning.setEnabled(false);
		grossEarning.getElement().setClassName("footer-grid");
		grossEarning.setWidth("100px");
		grossEarning.addKeyPressHandler(new DecimalWithSinglePrecion());
		
		
//		InlineLabel lbl = fgrossEarning.getHeaderLabel();
//		lbl.getElement().setClassName("footer-grid");
//		InlineLabel secgrosslabel = fsecgrossearnings.getHeaderLabel();
//		InlineLabel differencelabel = fdifference.getHeaderLabel();
//		secgrosslabel.getElement().setClassName("footer-grid");
//		differencelabel.getElement().setClassName("footer-grid");

		ctcCopy.getElement().setClassName("footer-grid");
		ctcCopy.setEnabled(false);
		ctcCopy.setWidth("100px");
		difference.getElement().setClassName("footer-grid");
		difference.setEnabled(false);
		difference.setWidth("100px");

		dbMonthlyCtcAmount.getElement().setClassName("footer-grid");
		dbMonthlyCtcAmount.setEnabled(false);
		dbMonthlyCtcAmount.setWidth("100px");
		
		
		dbEmployeeTotalDeduction.getElement().setClassName("footer-grid");
		dbEmployeeTotalDeduction.setEnabled(false);
		dbEmployeeTotalDeduction.setWidth("100px");
		dbCompanyTotalDeduction.getElement().setClassName("footer-grid");
		dbCompanyTotalDeduction.setEnabled(false);
		dbCompanyTotalDeduction.setWidth("100px");

//		InlineLabel empDedLbl = fempTotDeduction.getHeaderLabel();
//		empDedLbl.getElement().setClassName("footer-grid");
//
//		InlineLabel compDedLbl = fcomTotDeduction.getHeaderLabel();
//		compDedLbl.getElement().setClassName("footer-grid");
		
		tbCtcTempCreatedBy.setEnabled(false);
		tbCtcTempId.setEnabled(false);
		tbCtcTempName.setEnabled(false);
		dbCtcTempDate.setEnabled(false);
		
		 cbPaidLeave.setValue(false);
		 dbPaidLeaveAmt.setEnabled(false);
		 
		 cbBonus.setValue(false);
		 dbBonusAmt.setEnabled(false);
		 
		 rbMonthly.setValue(true);
		 rbAnnually.setValue(false);
		 lbMonth.setEnabled(false);
		 
		oblPaidLeave.setEnabled(false);
		oblBonus.setEnabled(false);
		cbIncludeBonusInEarning.setValue(false);
		cbIncludePlInEarning.setValue(false);
		
		blankLable.setVisible(false);
	}

	public CTCForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}


	private void initalizeWidget()
	{
		/**
		 * @author Anil ,  Date ; 17-06-2019
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CTCTemplate", "PlAndBonusAsPerTheFormula")){
			isPlAndBonusAsPerTheFormula=true;
		}
		System.out.println(" isPlAndBonusAsPerTheFormula "+isPlAndBonusAsPerTheFormula);
	
		CTCId=new TextBox();
		CTCId.setEnabled(false);
		
		pic=AppUtility.employeeInfoComposite(new EmployeeInfo(),true);
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		
		
//		dbFromDate.setEnabled(false);
//		dbToDate.setEnabled(false);
				
//		leaveCalendar=new TextBox();
//		leaveCalendar.setEnabled(false);
		
		grossEarning=new DoubleBox();
		
		ctcAmount=new DoubleBox();
		ctcAmount.setValue(0d);
		
		ctcCopy=new DoubleBox();
		difference=new DoubleBox();
		ctcCopy.setValue(0d);
		difference.setValue(0d);
		
		dbMonthlyCtcAmount=new DoubleBox();
		dbMonthlyCtcAmount.setValue(0d);
		
		status=new CheckBox();
		status.setValue(true);
		earningtable=new SalaryHeadPresenterTableProxy();
		tbStatus=new TextBox();
		tbStatus.setEnabled(false);
		tbStatus.setText(CTC.CREATED);
		
		
		
		deductionTable=new SalaryHeadPresenterTableProxy();
		
		companyContributionTable=new SalaryHeadPresenterTableProxy();
		dbEmployeeTotalDeduction=new DoubleBox();
		dbEmployeeTotalDeduction.setEnabled(false);
		dbCompanyTotalDeduction=new DoubleBox();
		dbCompanyTotalDeduction.setEnabled(false);
		
		dbEffectiveFrom=new DateBoxWithYearSelector();
		
		FlowPanel flowPanel1=new FlowPanel();
		FlowPanel flowPanel2=new FlowPanel();
		FlowPanel flowPanel3=new FlowPanel();
		
		VerticalPanel verPanel1=new VerticalPanel();
		VerticalPanel verPanel2=new VerticalPanel();
		VerticalPanel verPanel3=new VerticalPanel();
		VerticalPanel verPanel4=new VerticalPanel();
		HorizontalPanel horPanel=new HorizontalPanel();
		HorizontalPanel horPanel1=new HorizontalPanel();
		horPanel1.setSpacing(0);
		
		InlineLabel lbl = new InlineLabel("Total");
		lbl.getElement().setClassName("footer-grid");
		
		InlineLabel secgrosslabel = new InlineLabel("CTC Amount");
		secgrosslabel.getElement().setClassName("footer-grid");
		
		InlineLabel differencelabel =new InlineLabel("Difference");
		differencelabel.getElement().setClassName("footer-grid");
		
		InlineLabel differencelabel1=new InlineLabel("Gross Earning Monthly");
		differencelabel1.getElement().setClassName("footer-grid");
		
		InlineLabel empDedLbl = new InlineLabel("Employee Total Deduction");
		empDedLbl.getElement().setClassName("footer-grid");

		InlineLabel compDedLbl = new InlineLabel("Company Total Deduction");
		compDedLbl.getElement().setClassName("footer-grid");
		
	
		
		
		verPanel1.add(earningtable.getTable());
		verPanel1.setWidth("100%");
		verPanel1.setHeight("350px");
		verPanel1.add(lbl);
		verPanel1.add(grossEarning);
		verPanel1.add(secgrosslabel);
		verPanel1.add(ctcCopy);
		verPanel1.add(differencelabel);
		verPanel1.add(difference);
		verPanel1.add(differencelabel1);
		verPanel1.add(dbMonthlyCtcAmount);
		
		
		
		
		
		verPanel2.add(deductionTable.getTable());
		verPanel2.setWidth("100%");
		verPanel2.setHeight("350px");
		verPanel2.add(empDedLbl);
		verPanel2.add(dbEmployeeTotalDeduction);
		
		verPanel3.add(companyContributionTable.getTable());
		verPanel3.setWidth("100%");
		verPanel3.setHeight("350px");
		verPanel3.add(compDedLbl);
		verPanel3.add(dbCompanyTotalDeduction);
		
		flowPanel1.add(verPanel1);
//		flowPanel1.add(horPanel);
		flowPanel2.add(verPanel2);
		flowPanel3.add(verPanel3);
		
		tabPanel = new TabPanel();
		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				 int tabId = event.getSelectedItem();
				 System.out.println("TAB ID "+tabId);
				 if(tabId==0){
					 earningtable.getTable().redraw();
				 }else if(tabId==1){
					 deductionTable.getTable().redraw();
				 }else if(tabId==2){
					 companyContributionTable.getTable().redraw();
				 }					
			}
		});
		tabPanel.add(flowPanel1,"Earnings");
		tabPanel.add(flowPanel2,"Deductions");
		tabPanel.add(flowPanel3,"Company Contribution");
		tabPanel.setSize("100%", "350px");
		// select first tab
		tabPanel.selectTab(0);
		
		cbPaidLeave=new CheckBox();
		
		
		tbCtcTempId=new TextBox();
		tbCtcTempName=new TextBox();
		tbCtcTempCreatedBy=new TextBox();
		dbCtcTempDate=new DateBoxWithYearSelector();
		tbCtcTempCreatedBy.setEnabled(false);
		tbCtcTempId.setEnabled(false);
		tbCtcTempName.setEnabled(false);
		dbCtcTempDate.setEnabled(false);
		
		rbMonthly=new RadioButton("Monthly");
		rbMonthly.setValue(true);
		rbMonthly.addClickHandler(this);
		
		rbAnnually=new RadioButton("Annually");
		rbAnnually.setValue(false);
		rbAnnually.addClickHandler(this);
		
		cbPaidLeave = new CheckBox();
		cbPaidLeave.setValue(false);
		cbPaidLeave.addClickHandler(this);
		
		dbPaidLeaveAmt=new DoubleBox();
		dbPaidLeaveAmt.setEnabled(false);
		
		cbBonus=new CheckBox();
		cbBonus.setValue(false);
		cbBonus.addClickHandler(this);
		
		dbBonusAmt=new DoubleBox();
		dbBonusAmt.setEnabled(false);
		
		lbMonth=new ListBox();
		lbMonth.addItem("--SELECT--");
		lbMonth.addItem("JAN");
		lbMonth.addItem("FEB");
		lbMonth.addItem("MAR");
		lbMonth.addItem("APR");
		lbMonth.addItem("MAY");
		lbMonth.addItem("JUN");
		lbMonth.addItem("JUL");
		lbMonth.addItem("AUG");
		lbMonth.addItem("SEP");
		lbMonth.addItem("OCT");
		lbMonth.addItem("NOV");
		lbMonth.addItem("DEC");
		lbMonth.setEnabled(false);
		
		cbIncludeBonusInEarning=new CheckBox();
		 cbIncludePlInEarning=new CheckBox();
		 tbPlCorrespondanceName=new TextBox();
		 tbBonusCorrespondanceName=new TextBox();
		 oblPaidLeave=new ObjectListBox<PaidLeave>();
		 Filter temp=new Filter();
		 temp.setQuerryString("status");
		 temp.setBooleanvalue(true);
		 MyQuerry querry=new MyQuerry();
		 querry.setQuerryObject(new PaidLeave());
		 querry.getFilters().add(temp);
		 oblPaidLeave.MakeLive(querry);
		 oblPaidLeave.addChangeHandler(this);
		 oblPaidLeave.setEnabled(false);
		 
		 oblBonus=new ObjectListBox<Bonus>();
		 Filter temp1=new Filter();
		 temp1.setQuerryString("status");
		 temp1.setBooleanvalue(true);
		 MyQuerry querry1=new MyQuerry();
		 querry1.setQuerryObject(new Bonus());
		 querry1.getFilters().add(temp1);
		 oblBonus.MakeLive(querry1);
		 oblBonus.addChangeHandler(this);
		 oblBonus.setEnabled(false);
		
		 blankLable=new InlineLabel();
		 blankLable.setVisible(false);
		 
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
		
		dbEffectiveTo=new DateBoxWithYearSelector();
		
		
	}

	@Override
	public void createScreen() {

		this.processlevelBarNames=new String[]{AppConstants.SUBMIT,AppConstants.NEW,AppConstants.EMAILCTC};//Ashwini Patil	
		initalizeWidget();

		//Token to initialize the processlevel menus.

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeInformation=fbuilder.setlabel("Employee Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("CTC ID",CTCId);
		FormField fCTCId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",pic);
		FormField fpic= fbuilder.setMandatory(true).setMandatoryMsg("Employee information is mandatory").setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCTCInformation=fbuilder.setlabel("CTC Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* From Date",dbFromDate);
		fdbfromDate= fbuilder.setMandatory(true).setMandatoryMsg("From Date is Manadatory !").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* To Date",dbToDate);
	    fdbToDate= fbuilder.setMandatory(true).setMandatoryMsg("To Date is Manadatory !").setRowSpan(0).setColSpan(0).build();
	    
		fbuilder = new FormFieldBuilder("Status",tbStatus);
		FormField fstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Total CTC Amount",ctcAmount);
		FormField fctcamount= fbuilder.setMandatory(true).setMandatoryMsg("Total CTC amount is mandatory !").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fEarningInformation=fbuilder.setlabel("CTC Component").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder("",earningtable.getTable());
		FormField fearningtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder=new FormFieldBuilder("Total", grossEarning);
		
		fgrossEarning= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
		
        fbuilder=new FormFieldBuilder("CTC Amount",ctcCopy);
	    fsecgrossearnings= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	     
//	    fbuilder=new FormFieldBuilder("* Calendar",leaveCalendar);
//	    FormField fcalendar= fbuilder.setMandatory(true).setMandatoryMsg("Calendar is mandatory!").setRowSpan(0).setColSpan(0).build();
	     
	    fbuilder=new FormFieldBuilder("Difference",difference);
	    fdifference= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	    
	    fbuilder=new FormFieldBuilder("Total Monthly CTC",dbMonthlyCtcAmount);
	    fdbMonthlyCtcAmount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	    
	    fbuilder = new FormFieldBuilder();
		FormField fdeductionComponent=fbuilder.setlabel("Deduction Component").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",deductionTable.getTable());
		FormField fdeductionTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		fbuilder=new FormFieldBuilder("Employee Total Deduction",dbEmployeeTotalDeduction);
	    fempTotDeduction= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	    
		
		fbuilder = new FormFieldBuilder();
		FormField fCompaniesComponent=fbuilder.setlabel("Company Contribution Component").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",companyContributionTable.getTable());
		FormField fCompaniesTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder=new FormFieldBuilder("Company Total Deduction",dbCompanyTotalDeduction);
	    fcomTotDeduction= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	    
	    fbuilder = new FormFieldBuilder("* Effective From Date",dbEffectiveFrom);
	    FormField fdbEffectiveFrom= fbuilder.setMandatory(true).setMandatoryMsg("Effective From Date is Manadatory !").setRowSpan(0).setColSpan(0).build();
	    
	    
	    fbuilder=new FormFieldBuilder(" ",tabPanel);
	    FormField ftabPanel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

	    
//	    fbuilder = new FormFieldBuilder("Is Paid Leave Applicable",cbPaidLeave);
//	    FormField fcbIsPaidLeaveApplicable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	    fbuilder = new FormFieldBuilder();
		FormField fgroupingCtcTemp=fbuilder.setlabel("Ctc Template Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Ctc Template Id",tbCtcTempId);
	    FormField ftbCtcTempId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	    fbuilder = new FormFieldBuilder("Ctc Template Name",tbCtcTempName);
	    FormField ftbCtcTempName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	    fbuilder = new FormFieldBuilder("Ctc Template Allocated By",tbCtcTempCreatedBy);
	    FormField ftbCtcTempCreatedBy= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	    fbuilder = new FormFieldBuilder("Ctc Template Allocation Date",dbCtcTempDate);
	    FormField fdbCtcTempDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	    fbuilder = new FormFieldBuilder();
		FormField fgrpingPlandBonus=fbuilder.setlabel("Paid Leave and Bonus Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("Paid Leave", cbPaidLeave);
		FormField fpaidLeave = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Paid Leave Amount", dbPaidLeaveAmt);
		FormField fdbPaidLeaveAmt = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Bonus", cbBonus);
		FormField fcbBonus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Bonus Amount", dbBonusAmt);
		FormField fdbBonusAmt = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Monthly", rbMonthly);
		FormField frbMonthly = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Annually", rbAnnually);
		FormField frbAnnually = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Month", lbMonth);
		FormField flbMonth = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
	   
		
		/**
	     * 
	     */
	    
	    fbuilder=new FormFieldBuilder("Include in earning",cbIncludePlInEarning);
		FormField fcbIncludePlInEarning =fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Include in earning",cbIncludeBonusInEarning);
		FormField fcbIncludeBonusInEarning =fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Correspondence Name",tbPlCorrespondanceName);
		FormField ftbPlCorrespondanceName =fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Correspondence Name",tbBonusCorrespondanceName);
		FormField ftbBonusCorrespondanceName =fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Paid Leave",oblPaidLeave);
		FormField foblPaidLeave =fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Bonus",oblBonus);
		FormField foblBonus =fbuilder.setMandatory(false).build();
		
		fbuilder = new FormFieldBuilder(" ", blankLable);
		FormField blankLable = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project", project);
		FormField fproject = fbuilder.setMandatory(false).setMandatoryMsg("Project is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Effective To Date",dbEffectiveTo);
	    FormField fdbEffectiveTo= fbuilder.setMandatory(true).setMandatoryMsg("Effective To Date is Manadatory !").setRowSpan(0).setColSpan(0).build();
	    
	    
		 if(isPlAndBonusAsPerTheFormula){
			 FormField[][] formfield = {   
					 {fgroupingEmployeeInformation},
					 {fpic},
					 {fgroupingCTCInformation},
					 {fCTCId,fdbfromDate,fdbToDate,fdbEffectiveFrom},
					 {fdbEffectiveTo,fproject,fctcamount,fstatus},
						
//					 {fgrpingPlandBonus},
//					 {frbMonthly,frbAnnually,flbMonth},
//					 {fpaidLeave,fdbPaidLeaveAmt,fcbBonus,fdbBonusAmt},
						
					 {fEarningInformation},
					 {ftabPanel},
					 
					 {fgrpingPlandBonus},
					 {fpaidLeave,fcbIncludePlInEarning,fcbBonus,fcbIncludeBonusInEarning,},
					 {foblPaidLeave,fdbPaidLeaveAmt,foblBonus,fdbBonusAmt,},
					 {ftbPlCorrespondanceName,blankLable,ftbBonusCorrespondanceName,blankLable},
				
					 
					 {fgroupingCtcTemp},
					 {ftbCtcTempId,ftbCtcTempName,ftbCtcTempCreatedBy,fdbCtcTempDate}
				};

				this.fields=formfield; 
		 }else{
			FormField[][] formfield = {   
					{fgroupingEmployeeInformation},
					{fpic},
					{fgroupingCTCInformation},
					{fCTCId,fdbfromDate,fdbToDate,fdbEffectiveFrom},
					{fdbEffectiveTo,fproject,fctcamount,fstatus},
					
					{fgrpingPlandBonus},
					{frbMonthly,frbAnnually,flbMonth},
					{fpaidLeave,fdbPaidLeaveAmt,fcbBonus,fdbBonusAmt},
					
					{fEarningInformation},
					{ftabPanel},
					{fgroupingCtcTemp},
					{ftbCtcTempId,ftbCtcTempName,ftbCtcTempCreatedBy,fdbCtcTempDate}
			};
	
			this.fields=formfield;
		 }

	}




	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Print")||text.equals(AppConstants.NAVIGATION)){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);  
				}
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.equals(AppConstants.NAVIGATION)){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}

		AuthorizationHelper.setAsPerAuthorization(Screen.CTC,LoginPresenter.currentModule.trim());

	}



	@Override
	public void updateModel(CTC model) {
		if(pic.getValue()!=null)
		{
			model.setEmpid(pic.getEmployeeId());
			model.setEmployeeName(pic.getEmployeeName());
			model.setEmpCellNo(pic.getCellNumber());
			model.setBranch(pic.getBranch());
			model.setDepartment(pic.getDepartment());
			model.setEmployeeType(pic.getEmployeeType());
			model.setEmployeedDesignation(pic.getEmpDesignation());
			model.setEmployeeRole(pic.getEmpRole());
		}
		if(dbFromDate.getValue()!=null)
			model.setFromdate(dbFromDate.getValue());
		if(dbToDate.getValue()!=null)
			model.setTodate(dbToDate.getValue());
		if(status.getValue()!=null)
			model.setCTCstatus(status.getValue());
		Comparator<CtcComponent> compareded = new Comparator<CtcComponent>() {
			@Override
			public int compare(CtcComponent o1,CtcComponent o2) {
				if(o1.getSrNo()!=0&&o2.getSrNo()!=0){
					return Integer.valueOf(o1.getSrNo()).compareTo(Integer.valueOf(o2.getSrNo()));
				}
				return 0;
			}
		};
		Collections.sort(earningtable.getValue(), compareded);
		if(earningtable.getValue()!=null)
			model.setEarning(earningtable.getValue());
		
		if(grossEarning.getValue()!=null)
			model.setGrossEarning(grossEarning.getValue());
		if(ctcAmount.getValue()!=null)
			model.setCtcAmount(ctcAmount.getValue());
		
//		if(leaveCalendar.getValue()!=null)
//			model.setCalender(leaveCalendar.getValue());
		
		if(tbStatus.getValue()!=null)
			model.setStatus(tbStatus.getValue());
		
		if(deductionTable.getValue()!=null){
			model.setDeduction(deductionTable.getValue());
		}
		
		if(dbEmployeeTotalDeduction.getValue()!=null){
			model.setEmployeesTotalDeduction(dbEmployeeTotalDeduction.getValue());
		}
		
		if(companyContributionTable.getValue()!=null){
			model.setCompaniesContribution(companyContributionTable.getValue());
		}
		
		if(dbCompanyTotalDeduction.getValue()!=null){
			model.setCompaniesTotalDeduction(dbCompanyTotalDeduction.getValue());
		}
		
		if(dbEffectiveFrom.getValue()!=null){
			model.setEffectiveFromDate(dbEffectiveFrom.getValue());
		}
		
		model.setPaidLeaveApplicable(cbPaidLeave.getValue());
		
		if(tbCtcTempId.getValue()!=null&&!tbCtcTempId.getValue().equals("")){
			model.setCtcTemplateId(Integer.parseInt(tbCtcTempId.getValue().trim()));
		}
		if(tbCtcTempName.getValue()!=null){
			model.setCtcTemplateName(tbCtcTempName.getValue());
		}
		if(tbCtcTempCreatedBy.getValue()!=null){
			model.setCtcTempAllocatedBy(tbCtcTempCreatedBy.getValue());
		}
		if(dbCtcTempDate.getValue()!=null){
			model.setCtcTempAllocationDate(dbCtcTempDate.getValue());
		}
		
		model.setMonthly(rbMonthly.getValue());
		model.setAnnually(rbAnnually.getValue());
		model.setBonus(cbBonus.getValue());
		
		if(dbPaidLeaveAmt.getValue()!=null){
			model.setPaidLeaveAmt(dbPaidLeaveAmt.getValue());
		}else{
			model.setPaidLeaveAmt(0);
		}
		if(dbBonusAmt.getValue()!=null){
			model.setBonusAmt(dbBonusAmt.getValue());
		}else{
			model.setBonusAmt(0);
		}
		
		if(lbMonth.getSelectedIndex()!=0){
			model.setMonth(lbMonth.getValue(lbMonth.getSelectedIndex()));
		}else{
			model.setMonth("");
		}
		
//		if(dbMonthlyCtcAmount.getValue()!=0){
//			model.setDbMonthlyCtcAmount(dbMonthlyCtcAmount.getValue());
//		}
		
		if(oblPaidLeave.getSelectedIndex()!=0){
			model.setPlName(oblPaidLeave.getValue());
		}else{
			model.setPlName("");
		}
		
		if(oblBonus.getSelectedIndex()!=0){
			model.setBonusName(oblBonus.getValue());
		}else{
			model.setBonusName("");
		}
		
		if(tbPlCorrespondanceName.getValue()!=null){
			model.setPlCorresponadenceName(tbPlCorrespondanceName.getValue());
		}
		
		if(tbBonusCorrespondanceName.getValue()!=null){
			model.setBonusCorresponadenceName(tbBonusCorrespondanceName.getValue());
		}
		
		model.setPlIncludedInEarning(cbIncludePlInEarning.getValue());
		model.setBonusIncludedInEarning(cbIncludeBonusInEarning.getValue());
		
		if(dbEffectiveTo.getValue()!=null){
			model.setEffectiveToDate(dbEffectiveTo.getValue());
		}
		
		if(project.getValue()!=null){
			model.setProjectName(project.getValue());
		}else{
			model.setProjectName("");
		}
		
		ctcobj=model;
		
		presenter.setModel(model);

	}

	@Override
	public void updateView(CTC view) {
		
		ctcobj=view;
	
		CTCId.setValue(view.getCount()+"");
			pic.setEmployeeId(view.getEmpid());
		if(view.getEmployeeName()!=null)
			pic.setEmployeeName(view.getEmployeeName());
		if(view.getEmpCellNo()!=null)
			pic.setCellNumber(view.getEmpCellNo());
		if(view.getBranch()!=null)
			pic.setBranch(view.getBranch());
		if(view.getDepartment()!=null)
			pic.setDepartment(view.getDepartment());
		if(view.getEmployeeType()!=null)
			pic.setEmployeeType(view.getEmployeeType());
		if(view.getEmployeedDesignation()!=null)
			pic.setEmpDesignation(view.getEmployeedDesignation());
		if(view.getEmployeeRole()!=null)
			pic.setEmpRole(view.getEmployeeRole());
		if(view.getFromdate()!=null)
			dbFromDate.setValue(view.getFromdate());
		if(view.getTodate()!=null)
			dbToDate.setValue(view.getTodate());
		if(view.getCTCstatus()!=null)
			status.setValue(view.getCTCstatus());
		Comparator<CtcComponent> compareded = new Comparator<CtcComponent>() {
			@Override
			public int compare(CtcComponent o1,CtcComponent o2) {
				if(o1.getSrNo()!=0&&o2.getSrNo()!=0){
					return Integer.valueOf(o1.getSrNo()).compareTo(Integer.valueOf(o2.getSrNo()));
				}
				return 0;
			}
		};
		Collections.sort(view.getEarning(), compareded);
		if(view.getEarning()!=null)
			earningtable.setValue(view.getEarning());
		if(view.getGrossEarning()!=null){
			grossEarning.setValue(view.getGrossEarning());
		}
		if(view.getCtcAmount()!=null)
		{
			ctcAmount.setValue(view.getCtcAmount());
			ctcCopy.setValue(ctcAmount.getValue());
			difference.setValue(0d);
			
		}
		
//		if(view.getCalendar()!=null)
//			leaveCalendar.setValue(view.getCalendar());
		
		
		if(view.getCtcAmount()!=null)
			this.earningtable.ctcAmount=view.getCtcAmount();
		if(view.getStatus()!=null)
			tbStatus.setValue(view.getStatus());
		
		if(view.getDeduction()!=null){
			deductionTable.setValue(view.getDeduction());
		}
		
		dbEmployeeTotalDeduction.setValue(view.getEmployeesTotalDeduction());
		
		if(view.getCompaniesContribution()!=null){
			companyContributionTable.setValue(view.getCompaniesContribution());
		}
		
		dbCompanyTotalDeduction.setValue(view.getCompaniesTotalDeduction());
		
		if(view.getEffectiveFromDate()!=null){
			dbEffectiveFrom.setValue(view.getEffectiveFromDate());
		}
		
		cbPaidLeave.setValue(view.isPaidLeaveApplicable());
		
		if(view.getCtcTemplateId()!=0){
			tbCtcTempId.setValue(view.getCtcTemplateId()+"");
		}
		
		if(view.getCtcTemplateName()!=null){
			tbCtcTempName.setValue(view.getCtcTemplateName());
		}
		if(view.getCtcTempAllocatedBy()!=null){
			tbCtcTempCreatedBy.setValue(view.getCtcTempAllocatedBy());
		}
		if(view.getCtcTempAllocationDate()!=null){
			dbCtcTempDate.setValue(view.getCtcTempAllocationDate());
		}
		
		rbMonthly.setValue(view.isMonthly());
		rbAnnually.setValue(view.isAnnually());
		cbBonus.setValue(view.isBonus());
		
		if(view.getPaidLeaveAmt()!=0){
			dbPaidLeaveAmt.setValue(view.getPaidLeaveAmt());
		}
		if(view.getBonusAmt()!=0){
			dbBonusAmt.setValue(view.getBonusAmt());
		}
		
		if(view.getMonth()!=null){
			for(int i=0;i<lbMonth.getItemCount();i++){
				if(lbMonth.getItemText(i).equals(view.getMonth())){
					lbMonth.setSelectedIndex(i);
					break;
				}
			}
		}
		
			dbMonthlyCtcAmount.setValue(view.getCtcAmount()/12);
			
			if(view.getPlName()!=null){
				oblPaidLeave.setValue(view.getPlName());
			}
			if(view.getBonusName()!=null){
				oblBonus.setValue(view.getBonusName());
			}
			if(view.getPlCorresponadenceName()!=null){
				tbPlCorrespondanceName.setValue(view.getPlCorresponadenceName());
			}
			if(view.getBonusCorresponadenceName()!=null){
				tbBonusCorrespondanceName.setValue(view.getBonusCorresponadenceName());
			}
			cbIncludePlInEarning.setValue(view.isPlIncludedInEarning());
			cbIncludeBonusInEarning.setValue(view.isBonusIncludedInEarning());
		
			if(view.getEffectiveToDate()!=null){
				dbEffectiveTo.setValue(view.getEffectiveToDate());
			}
			if(view.getProjectName()!=null){
				project.setValue(view.getProjectName());
			}
		presenter.setModel(view);
	}

	@Override
	public void setCount(int count)
	{
		this.CTCId.setValue(presenter.getModel().getCount()+"");
	}

	@Override
	public void clear() {
		
		super.clear();
		status.setValue(true);
		tbStatus.setText(CTC.CREATED);
		tbStatus.setEnabled(false);
		
		cbPaidLeave.setValue(false);
		dbPaidLeaveAmt.setEnabled(false);
		 
		cbBonus.setValue(false);
		dbBonusAmt.setEnabled(false);
		 
		rbMonthly.setValue(true);
		rbAnnually.setValue(false);
		lbMonth.setEnabled(false);
		
		oblPaidLeave.setEnabled(false);
		oblBonus.setEnabled(false);
		cbIncludeBonusInEarning.setValue(false);
		cbIncludePlInEarning.setValue(false);
	}

	@Override
	public boolean validate() {
		boolean superValidate= super.validate();
		if(superValidate==false)
			return false;
		if(cbPaidLeave.getValue()==true&&dbPaidLeaveAmt.getValue()==null){
			showDialogMessage("Please add paid leave amount.");
			return false;
		}
		if(cbBonus.getValue()==true&&dbBonusAmt.getValue()==null){
			showDialogMessage("Please slect bonus amount.");
			return false;
		}
		if((cbPaidLeave.getValue()==true ||cbBonus.getValue()==true)
				&&(rbMonthly.getValue()==false&&rbAnnually.getValue()==false)){
			showDialogMessage("Please select monthly/annually.");
			return false;
		}
		
		if(rbAnnually.getValue()==true&&lbMonth.getSelectedIndex()==0){
			showDialogMessage("Please select month.");
			return false;
		}
		if(ctcAmount.getValue().equals(grossEarning.getValue())==false)
		{
			showDialogMessage("Gross Amount should be equal to CTC amount!");
			return false;
		}
		
		if(this.earningtable.validate()==false)
		{
			showDialogMessage("Please Enter Valid Amount !");
			return false;
		}
		return true;
	}

	

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		if(cbPaidLeave.getValue()==true){
			dbPaidLeaveAmt.setEnabled(true);
		}else{
			dbPaidLeaveAmt.setEnabled(false);
		}
		if(cbBonus.getValue()==true){
			dbBonusAmt.setEnabled(true);
		}else{
			dbBonusAmt.setEnabled(false);
		}
		
		if(rbAnnually.getValue()==true){
			lbMonth.setSelectedIndex(0);
			lbMonth.setEnabled(true);
		}else{
			lbMonth.setSelectedIndex(0);
			lbMonth.setEnabled(false);
		}
		blankLable.setVisible(false);
	}

	@Override
	public void setEnable(boolean status)
	{
		super.setEnable(status);
		this.grossEarning.setEnabled(false);
		ctcCopy.setEnabled(false);
		difference.setEnabled(false);
		CTCId.setEnabled(false);
		
		tbStatus.setEnabled(false);
		earningtable.setEnable(status);
		
//		dbFromDate.setEnabled(false);
//		dbToDate.setEnabled(false);
//		leaveCalendar.setEnabled(false);
		
		deductionTable.setEnable(status);
		companyContributionTable.setEnable(status);
		
		dbEmployeeTotalDeduction.setEnabled(false);
		dbCompanyTotalDeduction.setEnabled(false);
		
		tbCtcTempCreatedBy.setEnabled(false);
		tbCtcTempId.setEnabled(false);
		tbCtcTempName.setEnabled(false);
		dbCtcTempDate.setEnabled(false);
		
		blankLable.setVisible(false);
		
//		if(cbPaidLeave.getValue()==true){
//			dbPaidLeaveAmt.setEnabled(true);
//		}else{
//			dbPaidLeaveAmt.setEnabled(false);
//		}
//		if(cbBonus.getValue()==true){
//			dbBonusAmt.setEnabled(true);
//		}else{
//			dbBonusAmt.setEnabled(false);
//		}
//		
//		if(rbAnnually.getValue()==true){
//			lbMonth.setSelectedIndex(0);
//			lbMonth.setEnabled(true);
//		}else{
//			lbMonth.setSelectedIndex(0);
//			lbMonth.setEnabled(false);
//		}
	}
//***************************************************************************************************//
	
	public void toggleProcessLevelMenu()
	{
        CTC entity=(CTC) presenter.getModel();
            String status=entity.getStatus();
            
            for(int i=0;i<getProcesslevelBarNames().length;i++)
            {
            	
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();
			Console.log("label.getText()="+label.getText());
			if(status.equals(CTC.ACTIVE))
			{
				if(text.equals(AppConstants.SUBMIT)){
					label.setVisible(false);
				}
			}
	}
	}
	
	public void setMenuAsPerStatus()
	{
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
		
	}
	
	public void setAppHeaderBarAsPerStatus()
	{
		CTC ctc=(CTC) presenter.getModel();
		String status=ctc.getStatus().trim();
		if(status.equals(CTC.ACTIVE))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Edit")||text.contains("Email")||text.equals("Download"))
				{
					menus[k].setVisible(false); 
				}
				else{
					menus[k].setVisible(true);  
				}
			}
		}
	}
	
	@Override
	public void setToViewState() 
	{
		super.setToViewState();
		setMenuAsPerStatus();
		
		SuperModel model=new CTC();
		model=ctcobj;
		if(HistoryPopup.historyObj!=null){
			AppUtility.addDocumentToHistoryTable(HistoryPopup.historyObj.getModuleName(),AppConstants.CTC, ctcobj.getCount(), ctcobj.getEmpid(),ctcobj.getEmployeeName(),ctcobj.getEmpCellNo(), false, model, null);
			HistoryPopup.historyObj=null;
		}else{
		AppUtility.addDocumentToHistoryTable(LoginPresenter.currentModule,AppConstants.CTC, ctcobj.getCount(), ctcobj.getEmpid(),ctcobj.getEmployeeName(),ctcobj.getEmpCellNo(), false, model, null);
		}
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==cbPaidLeave){
			if(cbPaidLeave.getValue()==true){
				dbPaidLeaveAmt.setEnabled(true);
			}else{
				dbPaidLeaveAmt.setValue(null);
				dbPaidLeaveAmt.setEnabled(false);
			}
		}
		
		if(event.getSource()==cbBonus){
			if(cbBonus.getValue()==true){
				dbBonusAmt.setEnabled(true);
			}else{
				dbBonusAmt.setValue(null);
				dbBonusAmt.setEnabled(false);
			}
		}
		if(event.getSource()==rbMonthly){
			if(rbMonthly.getValue()==true){
				rbAnnually.setValue(false);
				lbMonth.setSelectedIndex(0);
				lbMonth.setEnabled(false);
			}
		}
		if(event.getSource()==rbAnnually){
			if(rbAnnually.getValue()==true){
				rbMonthly.setValue(false);
				lbMonth.setEnabled(true);
			}
		}
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		earningtable.getTable().redraw();
		deductionTable.getTable().redraw();
		companyContributionTable.getTable().redraw();
	}
	
}
