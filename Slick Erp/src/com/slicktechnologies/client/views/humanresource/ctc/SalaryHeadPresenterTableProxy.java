package com.slicktechnologies.client.views.humanresource.ctc;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.user.cellview.client.RowStyles;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.RowCountChangeEvent;

import java.util.Comparator;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextInputCell;

import java.util.List;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.dom.client.Style.Unit;
import com.itextpdf.text.log.SysoCounter;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.ctccomponent.EarningComponentPresenter.SalaryHeadPresenterTable;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;

public class SalaryHeadPresenterTableProxy extends SalaryHeadPresenterTable {

	TextColumn<CtcComponent> getNameColumn;
	TextColumn<CtcComponent> getShortNameColumn;
	TextColumn<CtcComponent> maxperCTC;
	TextColumn<CtcComponent> maxCtcAbsolute;
	TextColumn<CtcComponent> yearlyTotal; // Monthly Amount

	TextColumn<CtcComponent> uneditabeamount;
	Column<CtcComponent, String> getAmountColumn;

//	TextColumn<CtcComponent> getHeadTypeColumn;
//	TextColumn<CtcComponent> getCountColumn;
//
//	Column<CtcComponent, Boolean> selectcolumn;
//	public MultiSelectionModel<CtcComponent> msm;

	public static Double ctcAmount;
	
	public boolean validatetable = true;

	/**
	 * nidhi 11-09-2017 for display sono field
	 * 
	 */
	TextColumn<CtcComponent> getSrNo;

	/**
	 * end
	 */
	
	/**
	 * Date : 28-04-2018 By ANIL 
	 * Adding delete column at component level
	 */
	Column<CtcComponent, String> getDeleteColumn;
	Column<CtcComponent, String> getEditableMonthlyAmtCol;
	
	/**
	 * Date : 21-08-2018 By ANIL
	 * 
	 */
	TextColumn<CtcComponent> getReviseColumn;
	TextColumn<CtcComponent> getOldAmountMonthlyColumn;
	TextColumn<CtcComponent> getOldAmountAnnuallyColumn;
	
	public boolean rowEventFlag;
	
	public boolean adhocPaymentFlag=false;
	
	/**
	 * @author Anil , Date : 11-05-2019
	 * Max Percent column is editable for only EARING HRA component
	 * requirement raised by Sonu for charlene hospitality
	 */
	Column<CtcComponent, String> getEditableMaxPercentCol;
	
	
	public SalaryHeadPresenterTableProxy() {
		super();
		setHeight("250px");
		rowEventFlag=true;
	}
	
	public SalaryHeadPresenterTableProxy(boolean adhocPaymentFlag) {
		super(adhocPaymentFlag);
		this.adhocPaymentFlag=adhocPaymentFlag;
		createTable(adhocPaymentFlag);
		setHeight("250px");
		rowEventFlag=true;
	}

	@Override
	public void createTable() {
		// addColumngetCount();

		/**
		 * nidhi
		 * 11-09-2017
		 * for display sr no.
		 * 
		 */
		addColumnSrNo();
		/**
		 * end
		 */
		addColumngetName();
		addColumngetShortName();

		/**
		 * @author Anil , Date : 11-05-2019
		 * Needed percent column to be edited for HRA
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CTCTemplate", "AutoCalculateHR")){
			getEditableMaxPercentCol();
		}else{
			addColumngetMaxperCtc();
		}
		
		/**
		 * End
		 */
		addColumngetMaxctcAbsolute();
//		addColumngetMonthley();
		getEditableMonthlyAmtCol();
		
		addColumngetAmount();
		
//		getReviseColumn();
//		getOldAmountMonthlyColumn();
//		getOldAmountAnnuallyColumn();
		
		getDeleteColumn();

		// addSelectColumn();
		addFieldUpdater();
//		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	private void getEditableMaxPercentCol() {
		EditTextCell editCell = new EditTextCell();
		getEditableMaxPercentCol = new Column<CtcComponent,String>(editCell) {
			@Override
			public String getValue(CtcComponent object){
				if (object.getMaxPerOfCTC() == null|| object.getMaxPerOfCTC() == 0)
					return "N.A";
				return object.getMaxPerOfCTC() + "";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,CtcComponent object, NativeEvent event) {
				if(object.getShortName().equalsIgnoreCase("HRA")){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(getEditableMaxPercentCol, "Max Allowed (% of CTC)");
		getEditableMaxPercentCol.setSortable(true);
		table.setColumnWidth(getEditableMaxPercentCol, 20, Unit.PCT);
		
		getEditableMaxPercentCol.setFieldUpdater(new FieldUpdater<CtcComponent, String>() {

			@Override
			public void update(int index, CtcComponent object, String value) {
				// TODO Auto-generated method stub
				GWTCAlert alert=new GWTCAlert();
				if(value!=null&&!value.equals("")){
					double maxPercent=0;
					try{
						maxPercent=Double.parseDouble(value);
						object.setMaxPerOfCTC(maxPercent);
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					}catch(NumberFormatException e){
						alert.alert("Please enter numeric value only.");
					}
					
				}else{
					alert.alert("Please enter maximum percent.");
				}
				table.redraw();
			}
			
		});
	}

	public void createTable(boolean adhocPaymentFlag) {
		// addColumngetCount();

		/**
		 * nidhi
		 * 11-09-2017
		 * for display sr no.
		 * 
		 */
		addColumnSrNo();
		/**
		 * end
		 */
		addColumngetName();
		addColumngetShortName();

		/**
		 * @author Anil , Date : 11-05-2019
		 * Needed percent column to be edited for HRA
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CTCTemplate", "AutoCalculateHR")){
			getEditableMaxPercentCol();
		}else{
			addColumngetMaxperCtc();
		}
		/**
		 * End
		 */
		
		
		addColumngetMaxctcAbsolute();
//		addColumngetMonthley();
		getEditableMonthlyAmtCol();
		
		addColumngetAmount();
		
		getReviseColumn();
		getOldAmountMonthlyColumn();
		getOldAmountAnnuallyColumn();
		
		getDeleteColumn();

		// addSelectColumn();
		addFieldUpdater();
//		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}

	

	private void getReviseColumn() {
		// TODO Auto-generated method stub
		getReviseColumn = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				return object.isRevised() + "";
			}
		};
		table.addColumn(getReviseColumn, "Is Revise");
		getReviseColumn.setSortable(true);
		table.setColumnWidth(getReviseColumn, 10, Unit.PCT);
	}

	private void getOldAmountMonthlyColumn() {
		// TODO Auto-generated method stub
		getOldAmountMonthlyColumn = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				if(object.getOldAmountMonthly()!=0){
					return object.getOldAmountMonthly() + "";
				}
				return "";
				
			}
		};
		table.addColumn(getOldAmountMonthlyColumn, "Previous Amount Monthly");
		getOldAmountMonthlyColumn.setSortable(true);
		table.setColumnWidth(getOldAmountMonthlyColumn, 10, Unit.PCT);
	}

	private void getOldAmountAnnuallyColumn() {
		// TODO Auto-generated method stub
		getOldAmountAnnuallyColumn = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				if(object.getOldAmountAnnually()!=0){
					return object.getOldAmountAnnually() + "";
				}
				return "";
				
			}
		};
		table.addColumn(getOldAmountAnnuallyColumn, "Previous Amount Annully");
		getOldAmountAnnuallyColumn.setSortable(true);
		table.setColumnWidth(getOldAmountAnnuallyColumn, 10, Unit.PCT);
	}

	private void getDeleteColumn() {
		ButtonCell cell=new ButtonCell();
		getDeleteColumn=new Column<CtcComponent, String>(cell) {
			@Override
			public String getValue(CtcComponent object) {
				return "Delete";
			}
		};
		table.addColumn(getDeleteColumn,"");
		table.setColumnWidth(getDeleteColumn, 10, Unit.PCT);
		
		getDeleteColumn.setFieldUpdater(new FieldUpdater<CtcComponent, String>() {
			@Override
			public void update(int index, CtcComponent object, String value) {
				getDataprovider().getList().remove(index);
				table.redraw();
			}
		});
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<CtcComponent>() {
			@Override
			public Object getKey(CtcComponent item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
		for (int i = this.table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);
		addColumnSrNo();
		addColumngetName();
		addColumngetShortName();
//		addColumngetMaxperCtc();
//		addColumngetMaxctcAbsolute();
		
		if (state == true) {
			
			/**
			 * @author Anil , Date : 11-05-2019
			 * Needed percent column to be edited for HRA
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CTCTemplate", "AutoCalculateHR")){
				getEditableMaxPercentCol();
			}else{
				addColumngetMaxperCtc();
			}
			addColumngetMaxctcAbsolute();
			/**
			 * End
			 */
			
			getEditableMonthlyAmtCol();
			addColumngetAmount();
			/**
			 * Date : 21-08-2018 BY ANIL
			 */
			if(adhocPaymentFlag){
				getReviseColumn();
				getOldAmountMonthlyColumn();
				getOldAmountAnnuallyColumn();
			}
			/**
			 * Date : 28-04-2018 By ANIL
			 */
			getDeleteColumn();
			addFieldUpdater();

		}
		if (state == false) {
			/**
			 * @author Anil , Date : 11-05-2019
			 * Needed percent column to be edited for HRA
			 */
			addColumngetMaxperCtc();
			addColumngetMaxctcAbsolute();
			/**
			 * End
			 */
			addColumngetMonthley();
			addColumnUneditableamt();
			/**
			 * Date : 21-08-2018 BY ANIL
			 */
			if(adhocPaymentFlag){
				getReviseColumn();
				getOldAmountMonthlyColumn();
				getOldAmountAnnuallyColumn();
			}
			
		}
		
//		/**
//		 * Date : 28-04-2018 By ANIL
//		 */
//		if(state == true){
//			getDeleteColumn();
//		}

	}

	@Override
	public void applyStyle() {
	}

	public void addColumnSorting() {
		addSortinggetName();
		addSortinggetShortName();
		/**
		 *  nidhi
		 *  11-09-2017
		 *  for sort field
		 */
		addSortingSrNo();
		/**
		 *  end
		 */

		addSortinggetMaxPerCtc();
	}

	@Override
	public void addFieldUpdater() {
		// addSelectFieldupdater();
		addColumngetAmountFieldUpdater();
	}

//	public void addSelectColumn() {
//		msm = new MultiSelectionModel<CtcComponent>();
//		CheckboxCell selectcell = new CheckboxCell(true, false);
//		table.setSelectionModel(msm);
//		selectcolumn = new Column<CtcComponent, Boolean>(selectcell) {
//			@Override
//			public Boolean getValue(CtcComponent object) {
//				return msm.isSelected(object);
//			}
//		};
//		table.addColumn(selectcolumn, "Select");
//	}
//
//	public void addSelectFieldupdater() {
//		selectcolumn.setFieldUpdater(new FieldUpdater<CtcComponent, Boolean>() {
//			@Override
//			public void update(int index, CtcComponent object, Boolean value) {
//				try {
//					msm.setSelected(object, value);
//				} catch (Exception e) {
//
//				}
//				table.redrawRow(index);
//				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//			}
//		});
//	}

	
	/****************************************** Column method ******************************************/
	
//	protected void addColumngetCount() {
//		getCountColumn = new TextColumn<CtcComponent>() {
//			@Override
//			public String getValue(CtcComponent object) {
//				if (object.getCount() == -1)
//					return "N.A";
//				else
//					return object.getCount() + "";
//			}
//		};
//		table.addColumn(getCountColumn, "Id");
//	}

	protected void addColumngetName() {
		getNameColumn = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				return object.getName() + "";
			}
		};
		table.addColumn(getNameColumn, "Name");
		getNameColumn.setSortable(true);
		table.setColumnWidth(getNameColumn, 10, Unit.PCT);
	}

	protected void addColumngetShortName() {
		getShortNameColumn = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				return object.getShortName() + "";
			}
		};
		table.addColumn(getShortNameColumn, "Short Name");
		getShortNameColumn.setSortable(true);
		table.setColumnWidth(getShortNameColumn, 10, Unit.PCT);
	}

	protected void addColumngetMaxperCtc() {
		maxperCTC = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object)

			{
				if (object.getMaxPerOfCTC() == null
						|| object.getMaxPerOfCTC() == 0)
					return "N.A";
				return object.getMaxPerOfCTC() + "";
			}
		};
		table.addColumn(maxperCTC, "Max Allowed (% of CTC)");
		maxperCTC.setSortable(true);
		// maxperCTC.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		table.setColumnWidth(maxperCTC, 20, Unit.PCT);
	}

	protected void addColumngetMaxctcAbsolute() {
		maxCtcAbsolute = new TextColumn<CtcComponent>() {

			@Override
			public String getValue(CtcComponent object) {
				if (object.getMaxAmount() == null)
					return "N.A";
				return object.getMaxAmount() + "";
			}
		};

		table.addColumn(maxCtcAbsolute, "Max Abs. Amt.");
		table.setColumnWidth(maxCtcAbsolute, 20, Unit.PCT);
	}

	protected void addColumngetAmount() {
		TextInputCell cell = new TextInputCell();

		getAmountColumn = new Column<CtcComponent, String>(cell) {

			@Override
			public String getValue(CtcComponent object) {

				return object.getAmount() + "";
			}
		};

		table.addColumn(getAmountColumn, "Yearly Amount");
		table.setColumnWidth(getAmountColumn, 20, Unit.PCT);
	}

	protected void addColumnUneditableamt() {
		uneditabeamount = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				return object.getAmount() + "";
			}
		};
		table.addColumn(uneditabeamount, "Yearly Amount");
		table.setColumnWidth(uneditabeamount, "100px");
	}

	private void getEditableMonthlyAmtCol() {
		TextInputCell cell = new TextInputCell();
		getEditableMonthlyAmtCol=new Column<CtcComponent, String>(cell) {
			@Override
			public String getValue(CtcComponent object) {
				/**
				 * @author Anil , Date : 20-04-2019
				 */
//				int a = (int) (object.getAmount() / 12);
//				return a + "";
				
				double monthly=round(object.getAmount()/12,2);
				return monthly+"";
				
				/**
				 * End
				 */
			}
		};
		table.addColumn(getEditableMonthlyAmtCol,"Monthly Amount");
		table.setColumnWidth(getEditableMonthlyAmtCol, 20, Unit.PCT);
		
		
		getEditableMonthlyAmtCol.setFieldUpdater(new FieldUpdater<CtcComponent, String>() {

			@Override
			public void update(int index, CtcComponent object, String value) {
				double monthlyAmount=0;
				try{
					monthlyAmount=Double.parseDouble(value);
					

					Double amount = monthlyAmount*12;
					Double maxallowedAmount;
					if (ctcAmount != null) {
						if ((!(object.getMaxPerOfCTC() == null || object.getMaxPerOfCTC() == 0))) {
									maxallowedAmount = (ctcAmount * (object.getMaxPerOfCTC())) / 100;
						} else {
							maxallowedAmount = object.getMaxAmount().doubleValue();
						}
						if (amount <= maxallowedAmount) {
							object.setAmount(amount);
							AppUtility.ApplyWhiteColoronRow(SalaryHeadPresenterTableProxy.this,index);
						} else {
							// write code to make color red
							validatetable = false;
							object.setAmount(amount);
							// table.getRowElement(index).addClassName("redBackground");
							GWTCAlert alert = new GWTCAlert();
							// alert.alert("Amount Can't be greater then maximum percentage of ctc!");
							// alert.show();
							AppUtility.ApplyRedColoronRow(SalaryHeadPresenterTableProxy.this,index);
						}
					}
					
				}catch(NumberFormatException e){
					object.setAmount(0d);
					GWTCAlert alert=new GWTCAlert();
					alert.alert("Only numeric value allowed!");
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				table.redrawRow(index);
				table.redraw();
				
			}
		});
	}
	
	protected void addColumngetMonthley() {
		yearlyTotal = new TextColumn<CtcComponent>() {

			@Override
			public String getValue(CtcComponent object) {
				// TODO Auto-generated method stub
//				int a = (int) (object.getAmount() / 12);
//				return a + "";
				
				double monthly=round(object.getAmount()/12,2);
				return monthly+"";
			}
		};

		table.addColumn(yearlyTotal, "Monthly Amount");
		table.setColumnWidth(yearlyTotal, 20, Unit.PCT);
	}

	/******************************************* Column Sorting Method *****************************************/

	protected void addSortinggetName() {
		List<CtcComponent> list = getDataprovider().getList();
		columnSort = new ListHandler<CtcComponent>(list);
		columnSort.setComparator(getNameColumn, new Comparator<CtcComponent>() {
			@Override
			public int compare(CtcComponent e1, CtcComponent e2) {
				if (e1 != null && e2 != null) {
					if (e1.getName() != null && e2.getName() != null) {
						return e1.getName().compareTo(e2.getName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetShortName() {
		List<CtcComponent> list = getDataprovider().getList();
		columnSort = new ListHandler<CtcComponent>(list);
		columnSort.setComparator(getShortNameColumn,
				new Comparator<CtcComponent>() {
					@Override
					public int compare(CtcComponent e1, CtcComponent e2) {
						if (e1 != null && e2 != null) {
							if (e1.getShortName() != null
									&& e2.getShortName() != null) {
								return e1.getShortName().compareTo(
										e2.getShortName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetMaxPerCtc() {
		List<CtcComponent> list = getDataprovider().getList();
		columnSort = new ListHandler<CtcComponent>(list);
		columnSort.setComparator(maxperCTC, new Comparator<CtcComponent>() {
			@Override
			public int compare(CtcComponent e1, CtcComponent e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMaxPerOfCTC() == e2.getMaxPerOfCTC()) {
						return 0;
					}
					if (e1.getMaxPerOfCTC() > e2.getMaxPerOfCTC()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	/****************************************** Column Updater method ******************************************/

	protected void addColumngetAmountFieldUpdater() {
		getAmountColumn.setFieldUpdater(new FieldUpdater<CtcComponent, String>() {
			@Override
			public void update(int index, CtcComponent object,String value) {
				// table.getRowElement(index).getStyle().clearBackgroundColor();
				try {
					Double amount = Double.parseDouble(value);
					Double maxallowedAmount;
					if (ctcAmount != null) {
						if ((!(object.getMaxPerOfCTC() == null || object.getMaxPerOfCTC() == 0))) {
									maxallowedAmount = (ctcAmount * (object.getMaxPerOfCTC())) / 100;
						} else {
							maxallowedAmount = object.getMaxAmount().doubleValue();
						}
						if (amount <= maxallowedAmount) {
							object.setAmount(amount);
							AppUtility.ApplyWhiteColoronRow(SalaryHeadPresenterTableProxy.this,index);
						} else {
							// write code to make color red
							validatetable = false;
							object.setAmount(amount);
							// table.getRowElement(index).addClassName("redBackground");
							GWTCAlert alert = new GWTCAlert();
							// alert.alert("Amount Can't be greater then maximum percentage of ctc!");
							// alert.show();
							AppUtility.ApplyRedColoronRow(SalaryHeadPresenterTableProxy.this,index);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					object.setAmount(0d);
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//				table.redrawRow(index);
				table.redraw();
				
			}
		});
	}

	/************************************************************************************/

	public boolean validate() {
		// loop the data
		double amount = 0;
		double maxallowedAmount = 0;

		for (CtcComponent temp : getDataprovider().getList()) {
			amount = temp.getAmount();

			if ((!(temp.getMaxPerOfCTC() == null || temp.getMaxPerOfCTC() == 0))) {
				maxallowedAmount = (ctcAmount * (temp.getMaxPerOfCTC())) / 100;
			} else {
				maxallowedAmount = temp.getMaxAmount().doubleValue();
			}
			if (amount > maxallowedAmount)
				return false;
		}
		return true;
	}
	
	/**
	 * nidhi 11-09-2017
	 * 
	 */
	protected void addColumnSrNo() {
		getSrNo = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				return object.getSrNo() + "";
			}
		};
		table.addColumn(getSrNo, "Sr No.");
		getSrNo.setSortable(true);
	}

	/**
	 * end
	 */
	
	
	/**
	 *  nidhi
	 *  11-09-2017
	 *  method for sort field
	 */
	protected void  addSortingSrNo(){
		try {
			List<CtcComponent> list = getDataprovider().getList();
			columnSort = new ListHandler<CtcComponent>(list);
			columnSort.setComparator(getSrNo, new Comparator<CtcComponent>() {
				@Override
				public int compare(CtcComponent e1, CtcComponent e2) {
					if (e1 != null
							&& e2 != null
							) {
						if (e1.getSrNo() == e2.getSrNo()) {
							return 0;
						} else {
							if (e1.getSrNo() > e2.getSrNo())
								return 1;
						}
					}

					return -1;
				}
			});
			table.addColumnSortHandler(columnSort);
		} catch (Exception e) {
			e.printStackTrace();
			Console.log(e.getMessage());
			System.out.println(e.getMessage());
		}
		
	}
	
	/**
	 *  end
	 */
	
	
//	@Override
//	public void setValue(List<CtcComponent> list) {
//		// tRAVERSE THE DATA PROVIDER LIST
//		List<CtcComponent> head = getListDataProvider().getList();
//		for (CtcComponent temp : list) {
//			if (head.contains(temp)) {
//				head.remove(temp);
//				head.add(temp);
//			}
//		}
//	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}

}
