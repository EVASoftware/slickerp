package com.slicktechnologies.client.views.humanresource.ctc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;

public interface CTCServiceAsync  {
	public void save(CTC ctc,AsyncCallback<ReturnFromServer>callback);

}
