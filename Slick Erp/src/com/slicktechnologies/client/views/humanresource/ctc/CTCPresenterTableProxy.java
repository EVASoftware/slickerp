package com.slicktechnologies.client.views.humanresource.ctc;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.client.views.humanresource.ctc.CTCpresenter.CTCPresenterTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;

public class CTCPresenterTableProxy extends CTCPresenterTable {
	TextColumn<CTC> getCtcAmountColumn;

	TextColumn<CTC> getEmployeeIdColumn;
	TextColumn<CTC> getEmployeeNameColumn;
	TextColumn<CTC> getEmployeeTypeColumn;
	TextColumn<CTC> getEmployeeDesignationColumn;
	TextColumn<CTC> getEmployeeRoleColumn;
	TextColumn<CTC> getBranchColumn;
	TextColumn<CTC> getDepartmentColumn;
	TextColumn<CTC> getCellNumberColumn;
	TextColumn<CTC> calendar;

	TextColumn<CTC> getCountColumn;
	
	/**
	 * Date : 11-05-2018 By ANIL
	 * Status column
	 */
	TextColumn<CTC> getCtcStatusCol;
	
	/**
	 * Date : 19-09-2018 By ANIL
	 */
	
	TextColumn<CTC> getCtcTempIdCol;
	TextColumn<CTC> getCtcTempAllocatedByCol;
	TextColumn<CTC> getCtcTempAllocationDate;
	TextColumn<CTC> getCtcTempName;
	
	
	/**
	 * @author Anil , Date : 13-07-2019
	 */
	TextColumn<CTC> getProjectNameCol;
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("getCtcAmountColumn"))
			return this.getCtcAmountColumn;

		if(varName.equals("getEmployeeIdColumn"))
			return this.getEmployeeIdColumn;
		if(varName.equals("getEmployeeNameColumn"))
			return this.getEmployeeNameColumn;
		if(varName.equals("getEmployeeTypeColumn"))
			return this.getEmployeeTypeColumn;
		if(varName.equals("getEmployeeDesignationColumn"))
			return this.getEmployeeDesignationColumn;
		if(varName.equals("getEmployeeRoleColumn"))
			return this.getEmployeeRoleColumn;
		if(varName.equals("getBranchColumn"))
			return this.getBranchColumn;
		if(varName.equals("getDepartmentColumn"))
			return this.getDepartmentColumn;

		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		return null ;
	}
	public CTCPresenterTableProxy()
	{
		super();
	}
	@Override 
	public void createTable() {
		addColumngetCount();
		addColumngetEmployeeId();
		addColumngetEmployeeName();
		addColumncell();
//		addColumncalendar();
		addColumngetBranch();
		addColumngetDepartment();
		
		addColumngetEmployeeType();
		addColumngetEmployeeDesignation();
		
		addColumngetEmployeeRole();
		addColumngetCtcAmount();
		
		getCtcTempIdCol();
		getCtcTempName(); 
		getCtcTempAllocatedByCol(); 
		getCtcTempAllocationDate(); 
		          
		
		getProjectNameCol();
		getCtcStatusCol();
		
		applyStyle();
		
		
		
		
		
		
		
	}
	private void getProjectNameCol() {
		// TODO Auto-generated method stub
		getProjectNameCol=new TextColumn<CTC>() {
			@Override
			public String getValue(CTC object) {
				if(object.getProjectName()!=null){
					return object.getProjectName();
				}else{
					return "";
				}
			}
		};
		table.addColumn(getProjectNameCol, "Project");
		getProjectNameCol.setSortable(true);
	}
	private void getCtcTempIdCol() {
		// TODO Auto-generated method stub
		getCtcTempIdCol=new TextColumn<CTC>() {
			@Override
			public String getValue(CTC object) {
				
				if(object.getCtcTemplateId()!=0){
					return object.getCtcTemplateId()+"";
				}
				return "";
			}
		};
		table.addColumn(getCtcTempIdCol, "CTC Template Id");
		getCtcTempIdCol.setSortable(true);
	}
	private void getCtcTempName() {
		// TODO Auto-generated method stub
		
	}
	private void getCtcTempAllocatedByCol() {
		// TODO Auto-generated method stub
		
	}
	private void getCtcTempAllocationDate() {
		// TODO Auto-generated method stub
		getCtcTempAllocationDate=new TextColumn<CTC>() {
			@Override
			public String getValue(CTC object) {
				
				if(object.getCtcTempAllocationDate()!=null){
					return AppUtility.parseDate(object.getCtcTempAllocationDate())+"";
				}
				return "";
			}
		};
		table.addColumn(getCtcTempAllocationDate, "CTC Template Allocation Date");
		getCtcTempAllocationDate.setSortable(true);
	}
	private void getCtcStatusCol() {
		// TODO Auto-generated method stub
		getCtcStatusCol=new TextColumn<CTC>() {
			@Override
			public String getValue(CTC object) {
				return object.getStatus();
			}
		};
		table.addColumn(getCtcStatusCol, "Status");
		getCtcStatusCol.setSortable(true);
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<CTC>()
				{
			@Override
			public Object getKey(CTC item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	
	{
		table.setColumnWidth(getCountColumn,"100px");
		table.setColumnWidth(getEmployeeIdColumn,"100px");
		table.setColumnWidth(getEmployeeNameColumn,"150px");
		table.setColumnWidth(getCellNumberColumn,"150px");
		table.setColumnWidth(calendar,"100px");
		table.setColumnWidth(getBranchColumn,"100px");
		table.setColumnWidth(getDepartmentColumn,"140px");
		table.setColumnWidth(getEmployeeTypeColumn,"140px");
		table.setColumnWidth(getEmployeeDesignationColumn,"140px");
		table.setColumnWidth(getEmployeeRoleColumn,"140px");
		table.setColumnWidth(getCtcAmountColumn,"140px");
		table.setColumnWidth(getCtcStatusCol,"110px");
		table.setColumnWidth(getCtcTempIdCol,"100px");
		table.setColumnWidth(getCtcTempAllocationDate,"100px");
		table.setColumnWidth(getProjectNameCol,"100px");
		
	}
	public void addColumnSorting(){
		addSortinggetEmployeeId();
		addSortinggetEmployeeName();
		addSortinggetBranch();
		addSortinggetDepartment();
		
		addSortinggetEmployeeType();
		addSortinggetEmployeeDesignation();
	
		addSortinggetEmployeeRole();
		addSortinggetCtcAmount();
		addSortinggetCalendar();
		addSortinggetCount();
		addSortinggetCellNumber();	
		addSortingOnStatusCol();
		addSortingOnProjectCol();
		
	}
	private void addSortingOnProjectCol() {
		// TODO Auto-generated method stub
		List<CTC> list = getDataprovider().getList();
		columnSort = new ListHandler<CTC>(list);
		columnSort.setComparator(getProjectNameCol, new Comparator<CTC>() {
			@Override
			public int compare(CTC e1, CTC e2) {
				if (e1 != null && e2 != null) {
					if (e1.getProjectName() != null&& e2.getProjectName() != null) {
						return e1.getProjectName().compareTo(e2.getProjectName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortingOnStatusCol() {
		// TODO Auto-generated method stub
		List<CTC> list = getDataprovider().getList();
		columnSort = new ListHandler<CTC>(list);
		columnSort.setComparator(getCtcStatusCol, new Comparator<CTC>() {
			@Override
			public int compare(CTC e1, CTC e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null&& e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	
	}
	@Override public void addFieldUpdater() {
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<CTC>()
				{
			@Override
			public String getValue(CTC object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"Id");
				getCountColumn.setSortable(true);
	}
	
	
	protected void addSortinggetCount()
	{
		List<CTC> list=getDataprovider().getList();
		columnSort=new ListHandler<CTC>(list);
		columnSort.setComparator(getCountColumn, new Comparator<CTC>()
				{
			@Override
			public int compare(CTC e1,CTC e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	
	
	
	
	protected void addSortinggetEmployeeId()
	{
		List<CTC> list=getDataprovider().getList();
		columnSort=new ListHandler<CTC>(list);
		columnSort.setComparator(getEmployeeIdColumn, new Comparator<CTC>()
				{
			@Override
			public int compare(CTC e1,CTC e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getEmpid()== e2.getEmpid()){
						return 0;}
					if(e1.getEmpid()> e2.getEmpid()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployeeId()
	{
		getEmployeeIdColumn=new TextColumn<CTC>()
				{
			@Override
			public String getValue(CTC object)
			{
				return object.getEmpid()+"";
			}
				};
				table.addColumn(getEmployeeIdColumn,"Employee Id");
				getEmployeeIdColumn.setSortable(true);
	}
	protected void addSortinggetEmployeeName()
	{
		List<CTC> list=getDataprovider().getList();
		columnSort=new ListHandler<CTC>(list);
		columnSort.setComparator(getEmployeeNameColumn, new Comparator<CTC>()
				{
			@Override
			public int compare(CTC e1,CTC e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeeName()!=null && e2.getEmployeeName()!=null){
						return e1.getEmployeeName().compareTo(e2.getEmployeeName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployeeName()
	{
		getEmployeeNameColumn=new TextColumn<CTC>()
				{
			@Override
			public String getValue(CTC object)
			{
				return object.getEmployeeName()+"";
			}
				};
				table.addColumn(getEmployeeNameColumn,"Employee Name");
				getEmployeeNameColumn.setSortable(true);
	}
	protected void addSortinggetBranch()
	{
		List<CTC> list=getDataprovider().getList();
		columnSort=new ListHandler<CTC>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<CTC>()
				{
			@Override
			public int compare(CTC e1,CTC e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<CTC>()
				{
			@Override
			public String getValue(CTC object)
			{
				return object.getBranch()+"";
			}
				};
				table.addColumn(getBranchColumn,"Branch");
				getBranchColumn.setSortable(true);
	}
	protected void addSortinggetDepartment()
	{
		List<CTC> list=getDataprovider().getList();
		columnSort=new ListHandler<CTC>(list);
		columnSort.setComparator(getDepartmentColumn, new Comparator<CTC>()
				{
			@Override
			public int compare(CTC e1,CTC e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDepartment()!=null && e2.getDepartment()!=null){
						return e1.getDepartment().compareTo(e2.getDepartment());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetDepartment()
	{
		getDepartmentColumn=new TextColumn<CTC>()
				{
			@Override
			public String getValue(CTC object)
			{
				return object.getDepartment()+"";
			}
				};
				table.addColumn(getDepartmentColumn,"Department");
				getDepartmentColumn.setSortable(true);
	}
	
	protected void addSortinggetEmployeeType()
	{
		List<CTC> list=getDataprovider().getList();
		columnSort=new ListHandler<CTC>(list);
		columnSort.setComparator(getEmployeeTypeColumn, new Comparator<CTC>()
				{
			@Override
			public int compare(CTC e1,CTC e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeeType()!=null && e2.getEmployeeType()!=null){
						return e1.getEmployeeType().compareTo(e2.getEmployeeType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployeeType()
	{
		getEmployeeTypeColumn=new TextColumn<CTC>()
				{
			@Override
			public String getValue(CTC object)
			{
				return object.getEmployeeType()+"";
			}
				};
				table.addColumn(getEmployeeTypeColumn,"Employee Type");
				getEmployeeTypeColumn.setSortable(true);
	}
	protected void addSortinggetEmployeeDesignation()
	{
		List<CTC> list=getDataprovider().getList();
		columnSort=new ListHandler<CTC>(list);
		columnSort.setComparator(getEmployeeDesignationColumn, new Comparator<CTC>()
				{
			@Override
			public int compare(CTC e1,CTC e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeedDesignation()!=null && e2.getEmployeedDesignation()!=null){
						return e1.getEmployeedDesignation().compareTo(e2.getEmployeedDesignation());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployeeDesignation()
	{
		getEmployeeDesignationColumn=new TextColumn<CTC>()
				{
			@Override
			public String getValue(CTC object)
			{
				return object.getEmployeedDesignation()+"";
			}
				};
				table.addColumn(getEmployeeDesignationColumn,"Designation");
				getEmployeeDesignationColumn.setSortable(true);
	}
	
	protected void addSortinggetEmployeeRole()
	{
		List<CTC> list=getDataprovider().getList();
		columnSort=new ListHandler<CTC>(list);
		columnSort.setComparator(getEmployeeRoleColumn, new Comparator<CTC>()
				{
			@Override
			public int compare(CTC e1,CTC e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeeRole()!=null && e2.getEmployeeRole()!=null){
						return e1.getEmployeeRole().compareTo(e2.getEmployeeRole());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployeeRole()
	{
		getEmployeeRoleColumn=new TextColumn<CTC>()
				{
			@Override
			public String getValue(CTC object)
			{
				return object.getEmployeeRole()+"";
			}
				};
				table.addColumn(getEmployeeRoleColumn,"Role");
				getEmployeeRoleColumn.setSortable(true);
	}
	protected void addSortinggetCtcAmount()
	{
		List<CTC> list=getDataprovider().getList();
		columnSort=new ListHandler<CTC>(list);
		columnSort.setComparator(getCtcAmountColumn, new Comparator<CTC>()
				{
			@Override
			public int compare(CTC e1,CTC e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCtcAmount()== e2.getCtcAmount()){
						return 0;}
					if(e1.getCtcAmount()> e2.getCtcAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCtcAmount()
	{
		getCtcAmountColumn=new TextColumn<CTC>()
				{
			@Override
			public String getValue(CTC object)
			{
				return object.getCtcAmount()+"";
			}
				};
				table.addColumn(getCtcAmountColumn,"CTC Amount");
				getCtcAmountColumn.setSortable(true);
	}
	
	
	protected void addColumncalendar()
	{
		calendar=new TextColumn<CTC>()
				{
			@Override
			public String getValue(CTC object)
			{
				return object.getCalendar()+"";
			}
				};
				table.addColumn(calendar,"Calendar");
				calendar.setSortable(true);
	}
	
	
	
	
	
	protected void addSortinggetCalendar()
	{
		List<CTC> list=getDataprovider().getList();
		columnSort=new ListHandler<CTC>(list);
		columnSort.setComparator(calendar, new Comparator<CTC>()
				{
			@Override
			public int compare(CTC e1,CTC e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCalendar()!=null&& e2.getCalendar()!=null){
					
						return e1.getCalendar().compareTo(e2.getCalendar());
					}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addColumncell()
	{
		getCellNumberColumn=new TextColumn<CTC>()
				{
			@Override
			public String getValue(CTC object)
			{
				return object.getEmpCellNo()+"";
			}
				};
				table.addColumn(getCellNumberColumn,"Cell Number");
				getCellNumberColumn.setSortable(true);
	}
	
	
	
	
	
	protected void addSortinggetCellNumber()
	{
		List<CTC> list=getDataprovider().getList();
		columnSort=new ListHandler<CTC>(list);
		columnSort.setComparator(getCellNumberColumn, new Comparator<CTC>()
				{
			@Override
			public int compare(CTC e1,CTC e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getEmpCellNo()!=null&& e2.getEmpCellNo()!=null){
					
						return e1.getEmpCellNo().compareTo(e2.getEmpCellNo());
					}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
}
