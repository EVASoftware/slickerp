package com.slicktechnologies.client.views.humanresource.ctc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.popups.EmailPopup;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.Esic;
import com.slicktechnologies.shared.common.humanresourcelayer.ProvidentFund;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.OtEarningComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWF;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWFBean;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;

public class CTCpresenter extends FormScreenPresenter<CTC> implements RowCountChangeEvent.Handler, ValueChangeHandler<Double>,SelectionHandler<Suggestion> {

	public static CTCForm form;
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	CTCServiceAsync ctcservice = GWT.create(CTCService.class);
	protected GenricServiceAsync service = GWT.create(GenricService.class);

	EmailPopup emailPopup=new EmailPopup(); //Ashwini Patil
	/**
	 * @author Anil , Date : 28-06-2019
	 * for updated PF logic
	 */
	ProvidentFund pf;
	
	/**
	 * @author Anil , Date : 26-07-2019
	 * for updated ESIC logic
	 */
	Esic esic;
	
	public CTCpresenter(FormScreen<CTC> view, CTC model) {
		super(view, model);
		form = (CTCForm) view;
		
		
		retrieveEarningTable();
		form.pic.addSelectionHandler(this);
		form.ctcAmount.addValueChangeHandler(this);
		form.earningtable.getTable().addRowCountChangeHandler(this);
		
		form.getSearchpopupscreen().getPopup().setSize("100%", "100%");
		SalaryHeadPresenterTableProxy.ctcAmount = form.ctcAmount.getValue();
		
//		form.leaveCalendar.setEnabled(false);
		
		form.dbFromDate.setEnabled(false);
		form.dbToDate.setEnabled(false);
		
		form.deductionTable.getTable().addRowCountChangeHandler(this);
		form.companyContributionTable.getTable().addRowCountChangeHandler(this);
		
		emailPopup.getLblOk().addClickHandler(this); //Ashwini Patil
		form.setPresenter(this);
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		super.onClick(event);
		if(event.getSource() ==emailPopup.getLblOk()){
			
			ArrayList<String> emailIdList = new ArrayList<String>();
			if(emailPopup.getEmailTable().getDataprovider().getList().size() > 0){	
				Console.log("in if");
				for(EmployeeBranch branch :emailPopup.getEmailTable().getDataprovider().getList()){
					emailIdList.add(branch.getBranchName());	
				}
			}else{
				Console.log("in else");
				form.showDialogMessage("Please add emailId in email information.");
				return;
			}
			Console.log("email popup ok button clicked");
			CommonServiceAsync commonSer = GWT.create(CommonService.class);
			
			commonSer.sendCTCEmail(UserConfiguration.getCompanyId(), emailIdList, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					Console.log("Error : "+ caught);
				}

				@Override
				public void onSuccess(Void result) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Email sending process started. Please check your inbox after few minutes.");
					emailPopup.hidePopUp();
				}
			});
		}
	}
	/**
	 * This method retrieve the ctc component to earning table.
	 */

	private void retrieveEarningTable() {

		Vector<Filter> vecfilter = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setBooleanvalue(true);
		filter.setQuerryString("status");
		vecfilter.add(filter);

//		filter = new Filter();
//		filter.setBooleanvalue(false);
//		filter.setQuerryString("isDeduction");
//		vecfilter.add(filter);

		MyQuerry quer = new MyQuerry(vecfilter, new CtcComponent());

		service.getSearchResult(quer,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// form.hideWaitSymbol();
				form.earningtable.connectToLocal();
				form.deductionTable.connectToLocal();
				form.companyContributionTable.connectToLocal();
				ArrayList<CtcComponent>compList=new ArrayList<CtcComponent>();
				for (SuperModel model : result) {
					CtcComponent entity=(CtcComponent)model;
//					form.earningtable.getListDataProvider().getList().add(entity);
					compList.add(entity);
				}
				Comparator<CtcComponent> ctcSrNoComp = new Comparator<CtcComponent>() {
					@Override
					public int compare(CtcComponent o1,CtcComponent o2) {
						if(o1.getSrNo()!=0&&o2.getSrNo()!=0){
							return Integer.valueOf(o1.getSrNo()).compareTo(Integer.valueOf(o2.getSrNo()));
						}
						return 0;
					}
				};
				Collections.sort(compList, ctcSrNoComp);
				
//				for(CtcComponent ctc:compList){
//					if(ctc.isDeduction()==false){
//						form.earningtable.getListDataProvider().getList().add(ctc);
//					}else{
//						if(ctc.getIsRecord()==false){
//							form.deductionTable.getListDataProvider().getList().add(ctc);
//						}else{
//							form.companyContributionTable.getListDataProvider().getList().add(ctc);
//						}
//					}
//				}
				retrieveEsicDetails(compList);
//				retrievePFDetails(compList);
//				retrievePtDetails(compList);
				
			}
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				form.hideWaitSymbol();
			}
		});
	}
	
	/**
	 * @author Anil , Date : 25-07-2019
	 * @param compList
	 */
	
	protected void retrieveEsicDetails(final ArrayList<CtcComponent> compList) {
		Vector<Filter> vecfilter = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setBooleanvalue(true);
		filter.setQuerryString("status");
		vecfilter.add(filter);

		MyQuerry quer = new MyQuerry(vecfilter, new Esic());
		service.getSearchResult(quer,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				if(result!=null&&result.size()!=0){
					
					for(int i=0;i<compList.size();i++){
						if(compList.get(i).getShortName().trim().equalsIgnoreCase("ESIC")){
							compList.remove(i);
							i--;
						}
					}
					
					ArrayList<Esic> esicList=new ArrayList<Esic>();
					for(SuperModel model:result){
						esic=(Esic) model;
						esicList.add(esic);
					}
					
					ArrayList<CtcComponent>list=getEsicList(esicList);
					if(list!=null&&list.size()!=0){
						compList.addAll(list);
					}
				}
				
				retrievePFDetails(compList);
			}

			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				form.hideWaitSymbol();
			}
		});
	}
	
	protected ArrayList<CtcComponent> getEsicList(ArrayList<Esic> esicList) {
		// TODO Auto-generated method stub
		ArrayList<CtcComponent> ctcCompList=new ArrayList<CtcComponent>();
		for(Esic esic:esicList){
			CtcComponent comp=new CtcComponent();
			comp.setCtcComponentName("Gross-Washing");
			comp.setDeduction(true);
			comp.setIsRecord(false);
			comp.setName(esic.getEsicName());
			comp.setShortName(esic.getEsicShortName());
			comp.setMaxPerOfCTC(esic.getEmployeeContribution());
			comp.setCondition(">");
			comp.setConditionValue((int) esic.getEmployeeToAmount());
			
			comp.setFromRangeAmount(esic.getEmployeeFromAmount());
			comp.setToRangeAmount(esic.getEmployeeToAmount());
			ctcCompList.add(comp);
			
			
			CtcComponent comp1=new CtcComponent();
			comp1.setCtcComponentName("Gross-Washing");
			comp1.setDeduction(true);
			comp1.setIsRecord(true);
			comp1.setName(esic.getEsicName());
			comp1.setShortName(esic.getEsicShortName());
			comp1.setMaxPerOfCTC(esic.getEmployerContribution());
			comp1.setCondition(">");
			comp1.setConditionValue((int) esic.getEmployerToAmount());
			comp1.setFromRangeAmount(esic.getEmployerFromAmount());
			comp1.setToRangeAmount(esic.getEmployerToAmount());
			ctcCompList.add(comp1);
		}
		return ctcCompList;
	}

	/**
	 * @author Anil , Date : 28-06-2019
	 * @param compList
	 */
	
	protected void retrievePFDetails(final ArrayList<CtcComponent> compList) {
		Vector<Filter> vecfilter = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setBooleanvalue(true);
		filter.setQuerryString("status");
		vecfilter.add(filter);

		MyQuerry quer = new MyQuerry(vecfilter, new ProvidentFund());
		service.getSearchResult(quer,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				if(result!=null&&result.size()!=0){
					
					for(int i=0;i<compList.size();i++){
						if(compList.get(i).getShortName().trim().equalsIgnoreCase("PF")){
							compList.remove(i);
							i--;
						}
					}
					
					ArrayList<ProvidentFund> pflist=new ArrayList<ProvidentFund>();
					for(SuperModel model:result){
						pf=(ProvidentFund) model;
						pflist.add(pf);
					}
					
					ArrayList<CtcComponent>list=getPfList(pflist);
					if(list!=null&&list.size()!=0){
						compList.addAll(list);
					}
					
				}
				retrievePtDetails(compList);
				
			}

			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				form.hideWaitSymbol();
			}
		});
	}
	
	private ArrayList<CtcComponent> getPfList(ArrayList<ProvidentFund> pflist) {

		ArrayList<CtcComponent> ctcCompList=new ArrayList<CtcComponent>();
		for(ProvidentFund pf:pflist){
			CtcComponent comp=new CtcComponent();
			comp.setCtcComponentName("Gross-HRA");
			comp.setDeduction(true);
			comp.setIsRecord(false);
			comp.setName(pf.getPfName());
			comp.setShortName(pf.getPfShortName());
			comp.setMaxPerOfCTC(pf.getEmployeeContribution());
			ctcCompList.add(comp);
			
			
			CtcComponent comp1=new CtcComponent();
			comp1.setCtcComponentName("Gross-HRA");
			comp1.setDeduction(true);
			comp1.setIsRecord(true);
			comp1.setName(pf.getPfName());
			comp1.setShortName(pf.getPfShortName());
			comp1.setMaxPerOfCTC(pf.getEmployerContribution());
			comp1.setPercent1(pf.getEmpoyerPF());
			comp1.setPercent2(pf.getEdli());
			ctcCompList.add(comp1);
		}
		return ctcCompList;
	
	}

	protected void retrievePtDetails(final ArrayList<CtcComponent> compList) {
		
		for(int i=0;i<compList.size();i++){
			if(compList.get(i).getShortName().trim().equalsIgnoreCase("PT")){
				compList.remove(i);
				i--;
			}
		}
		
		Vector<Filter> vecfilter = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setBooleanvalue(true);
		filter.setQuerryString("status");
		vecfilter.add(filter);

		MyQuerry quer = new MyQuerry(vecfilter, new ProfessionalTax());
		service.getSearchResult(quer,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<ProfessionalTax> ptlist=new ArrayList<ProfessionalTax>();
				for(SuperModel model:result){
					ProfessionalTax pt=(ProfessionalTax) model;
					ptlist.add(pt);
				}
				
				ArrayList<CtcComponent>list=getPtList(ptlist);
				if(list!=null&&list.size()!=0){
					compList.addAll(list);
				}
				retrieveLwfDeatils(compList);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				form.hideWaitSymbol();
			}
		});
	}
	
	private ArrayList<CtcComponent> getPtList(ArrayList<ProfessionalTax> ptlist) {
		ArrayList<CtcComponent> ctcCompList=new ArrayList<CtcComponent>();
		for(ProfessionalTax pt:ptlist){
			CtcComponent comp=new CtcComponent();
			comp.setCtcComponentName("Gross Earning");
			comp.setDeduction(true);
			comp.setIsRecord(false);
			comp.setName(pt.getComponentName());
			comp.setShortName(pt.getShortName());
			comp.setMaxAmount((int) (pt.getPtAmount()*12));
			comp.setGender(pt.getGender());
			comp.setCondition(">");
			if(pt.getRangeToAmt()!=0){
				comp.setConditionValue((int) pt.getRangeToAmt());
			}else{
				comp.setConditionValue((int) pt.getRangeFromAmt());
			}
			ctcCompList.add(comp);
		}
		return ctcCompList;
		
	}
	
	private void retrieveLwfDeatils(final ArrayList<CtcComponent> compList) {
		
		for(int i=0;i<compList.size();i++){
			if(compList.get(i).getShortName().trim().equalsIgnoreCase("LWF")
					||compList.get(i).getShortName().trim().equalsIgnoreCase("MLWF")
					||compList.get(i).getShortName().trim().equalsIgnoreCase("MWF")){
				compList.remove(i);
				i--;
			}
		}
		Vector<Filter> vecfilter = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setBooleanvalue(true);
		filter.setQuerryString("status");
		vecfilter.add(filter);

		MyQuerry quer = new MyQuerry(vecfilter, new LWF());
		service.getSearchResult(quer,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<LWF> lwfList=new ArrayList<LWF>();
				for(SuperModel model:result){
					LWF lwf=(LWF) model;
					lwfList.add(lwf);
				}
				
				ArrayList<CtcComponent>list=getLwfList(lwfList);
				if(list!=null&&list.size()!=0){
					compList.addAll(list);
				}
				
				for(CtcComponent ctc:compList){
					if(ctc.isDeduction()==false){
						form.earningtable.getListDataProvider().getList().add(ctc);
					}else{
						if(ctc.getIsRecord()==false){
							form.deductionTable.getListDataProvider().getList().add(ctc);
						}else{
							form.companyContributionTable.getListDataProvider().getList().add(ctc);
						}
					}
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				form.hideWaitSymbol();
			}
		});
		
	}
	
	private ArrayList<CtcComponent> getLwfList(ArrayList<LWF> lwfList) {
		ArrayList<CtcComponent> ctcCompList=new ArrayList<CtcComponent>();
		for(LWF lwf:lwfList){
			for(LWFBean month:lwf.getLwfList()){
				CtcComponent comp=new CtcComponent();
				comp.setCtcComponentName("Gross Earning");
				comp.setDeduction(true);
				comp.setIsRecord(false);
				comp.setName(lwf.getName()+" "+month.getDeductionMonth());
				comp.setShortName(lwf.getShortName());
				comp.setMaxAmount((int) (lwf.getEmployeeContribution()));
//				comp.setMaxAmount((int) (lwf.getEmployeeContribution()*(12/lwf.getLwfList().size())));
				comp.setCondition(">");
				if(lwf.getToAmt()!=0){
					comp.setConditionValue((int) lwf.getToAmt());
				}else{
					comp.setConditionValue((int) lwf.getFromAmt());
				}
				comp.setSpecificMonth(true);
				comp.setSpecificMonthValue(month.getDeductionMonth());
				ctcCompList.add(comp);
				
				
				CtcComponent comp1=new CtcComponent();
				comp1.setCtcComponentName("Gross Earning");
				comp1.setDeduction(true);
				comp1.setIsRecord(true);
				comp1.setName(lwf.getName()+" "+month.getDeductionMonth());
				comp1.setShortName(lwf.getShortName());
				comp1.setMaxAmount((int) (lwf.getEmployerContribution()));
//				comp1.setMaxAmount((int) (lwf.getEmployerContribution()*(12/lwf.getLwfList().size())));
				comp1.setCondition(">");
				if(lwf.getToAmt()!=0){
					comp1.setConditionValue((int) lwf.getToAmt());
				}else{
					comp1.setConditionValue((int) lwf.getFromAmt());
				}
				comp1.setSpecificMonth(true);
				comp1.setSpecificMonthValue(month.getDeductionMonth());
				ctcCompList.add(comp1);
			}
		}
		return ctcCompList;
	}

	@Override
	public void reactOnDownload() {
		ArrayList<CTC> custarray = new ArrayList<CTC>();
		List<CTC> list = (List<CTC>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();		
		custarray.addAll(list);

		csvservice.setctclist(custarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 30;
				Window.open(url, "test", "enabled");
			}
		});
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		
		if (text.equals(AppConstants.SUBMIT)) {
			changeSatatus();
		}
		if (text.equals(AppConstants.NEW)){
			initalize();
		}
		if (text.equals(AppConstants.EMAILCTC)){
			Console.log("email clicked");
			reactOnCTCEmail();//Ashwini Patil
		}
	}
	

	@Override
	public void reactOnPrint() {
		final String url = GWT.getModuleBaseURL() + "pdfctc" + "?Id="+ model.getId();
		Window.open(url, "test", "enabled");
	}

	@Override
	public void reactOnEmail() {

	}

	@Override
	protected void makeNewModel() {

	}

	public static CTCForm initalize() {
		CTCForm form = new CTCForm();
		CTCPresenterTable gentable = new CTCPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		CTCPresenterSearch.staticSuperTable = gentable;
		CTCSearchForm searchpopup = new CTCSearchForm();
		form.setSearchpopupscreen(searchpopup);

		CTCpresenter presenter = new CTCpresenter(form, new CTC());
		AppMemory.getAppMemory().stickPnel(form);
		form.setToNewState();
		return form;
	}

	
	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		
		if(event.getSource().equals(form.earningtable.getTable())){
			List<CtcComponent> list = form.earningtable.getDataprovider().getList();
			Double value = calculateGrossEarning(list);
			if (value != null) {
				form.grossEarning.setValue(value);
				// form.ctcCopy.setValue(value);
				Double ctc = form.ctcAmount.getValue();
				if (ctc != null){
					form.difference.setValue(ctc - value);
				}
			}
			
			form.dbEmployeeTotalDeduction.setValue(updateDeductionTbl(form.deductionTable.getValue()));
			form.deductionTable.getTable().redraw();
			form.dbCompanyTotalDeduction.setValue(updateDeductionTbl(form.companyContributionTable.getValue()));
			form.companyContributionTable.getTable().redraw();
		}
		
		if(event.getSource().equals(form.deductionTable.getTable())){
			List<CtcComponent> list = form.deductionTable.getDataprovider().getList();
			Double value = calculateGrossEarning(list);
			if (value != null) {
				form.dbEmployeeTotalDeduction.setValue(value);
			}
		}
		
		if(event.getSource().equals(form.companyContributionTable.getTable())){
			List<CtcComponent> list = form.companyContributionTable.getDataprovider().getList();
			Double value = calculateGrossEarning(list);
			if (value != null) {
				form.dbCompanyTotalDeduction.setValue(value);
			}
		}
		
		
	}

	private double updateDeductionTbl(List<CtcComponent> value) {
		
		/**
		 * Date : 29-05-2018 By ANIL
		 */
		double pfMaxFixedValue=0;
		String deductionTypeName="";
		boolean pfFlag=false;
		List<ProcessConfiguration> processConfigList=new ArrayList<ProcessConfiguration>();
		for(ProcessConfiguration procConfig:LoginPresenter.globalProcessConfig){
			if((procConfig.getProcessName().equals("PfMaxFixedValue")&&procConfig.isConfigStatus()==true)
					||(procConfig.getProcessName().equals("EsicForDirect")&&procConfig.isConfigStatus()==true)){
				processConfigList.add(procConfig);
			}
		}
		if(processConfigList!=null){
			for(ProcessConfiguration processConf:processConfigList){
				for(ProcessTypeDetails ptDetails:processConf.getProcessList()){
					if(processConf.getProcessName().equalsIgnoreCase("PfMaxFixedValue")&&ptDetails.isStatus()==true){
						pfMaxFixedValue=Double.parseDouble(ptDetails.getProcessType().trim());
					}
					if(processConf.getProcessName().equalsIgnoreCase("EsicForDirect")&&ptDetails.isStatus()==true){
						deductionTypeName=ptDetails.getProcessType().trim();
					}
				}
			}
		}
		/**
		 * End
		 */
		
		String numberRange="";
		String empGender="";
		if(form.pic.getValue()!=null){
			EmployeeInfo info=form.pic.getEmpentity();
			if(info!=null&&info.getGender()!=null){
				empGender=info.getGender();
				numberRange=info.getNumberRangeAsEmpTyp();
			}
		}
		System.out.println();
		System.out.println("UPDATING DEDUCTIONS..");
		System.out.println();
		double sum=0;
		boolean ptFlag=false;
		for(CtcComponent deduction:value){
			double totalAmount = 0;
			boolean genderFlag=true;
			String genderforDeduction="";
			if(deduction.getGender()!=null){
				genderforDeduction=deduction.getGender();
			}
			if(!empGender.equals("")&&!genderforDeduction.equals("")){
				if(genderforDeduction.equals("All")){
					genderFlag=true;
				}else if(empGender.equals(genderforDeduction)){
					genderFlag=true;
				}else{
					genderFlag=false;
				}
			}else if(empGender.equals("")&&genderforDeduction.equals("")){
				genderFlag=true;
			}else if(!empGender.equals("")&&genderforDeduction.equals("")){
				genderFlag=false;
			}else if(empGender.equals("")&&!genderforDeduction.equals("")){
				if(genderforDeduction.equals("All")){
					genderFlag=true;
				}else{
					genderFlag=false;
				}
			}
			if(deduction.isDeduction()==true){
				
				if(deduction.getCtcComponentName()!=null&&!deduction.getCtcComponentName().equals("")
						&&deduction.getMaxPerOfCTC()!=null&&deduction.getMaxPerOfCTC()!=0
						&&(deduction.getMaxAmount()==null||deduction.getMaxAmount()==0)&&genderFlag){
					double amount=getTotalAmountOfEarningComponent(deduction.getCtcComponentName());
//					System.out.println("TOTAL AMT : "+amount);
					
					/**
					 * Date : 29-08-2018 BY ANIL
					 * updates ESCI Calculation for direct employee of Sasha
					 */
					if(deductionTypeName!=null&&!deductionTypeName.equals("")
							/**Rahul Commented this line to effect on all type of employees**&&numberRange.equalsIgnoreCase("Direct")****/
							&&deduction.getShortName().equalsIgnoreCase("ESIC")){
						amount=amount-getTotalAmountOfEarningComponent(deductionTypeName);
					}
					/**
					 * End
					 */
					
					if(deduction.getCondition()!=null&&!deduction.getCondition().equals("")
							&&deduction.getConditionValue()!=null&&deduction.getConditionValue()!=0){
						 
						 if(deduction.getCondition().equals("<")){
							 if(amount<=(deduction.getConditionValue()*12)){
								 totalAmount=amount*deduction.getMaxPerOfCTC()/100;
							 }
						 }else if(deduction.getCondition().equals(">")){
							 if(amount>=(deduction.getConditionValue()*12)){
								 totalAmount=amount*deduction.getMaxPerOfCTC()/100;
							 }
						 }else if(deduction.getCondition().equals("=")){
							 if(amount==(deduction.getConditionValue()*12)){
								 totalAmount=amount*deduction.getMaxPerOfCTC()/100;
							 }
						 }
					}else{
						
						/**
						 * Date : 29-05-2018 By ANIL
						 * added above code for PF Calculation
						 * this calculation is basically for direct employee whose pf is deducted on basic and Da
						 * Date : 23-07-2018 By ANIL
						 * Commented PF calculation code
						 */
						
//						if(pfMaxFixedValue!=0&&deduction.getShortName().equalsIgnoreCase("PF")&&pfFlag==false){
//							System.out.println("PF COUNTER...........");
//							pfFlag=true;
//							amount=0;
//							for(CtcComponent ded:value){
//								if(ded.getShortName().equalsIgnoreCase("PF")){
//									amount+=getTotalAmountOfEarningComponent(ded.getCtcComponentName());
//								}
//							}
//							if(amount>pfMaxFixedValue*12){
//								amount=pfMaxFixedValue*12;
//							}
//						}else if(pfMaxFixedValue!=0&&deduction.getShortName().equalsIgnoreCase("PF")&&pfFlag==true){
//							amount=0;
//						}
						
						/**
						 * End
						 */
						
						totalAmount=amount*deduction.getMaxPerOfCTC()/100;
					}
				}else if((deduction.getCtcComponentName()!=null&&!deduction.getCtcComponentName().equals(""))
						&&(deduction.getMaxPerOfCTC()==null||deduction.getMaxPerOfCTC()==0)
						&&(deduction.getMaxAmount()!=null&&deduction.getMaxAmount()!=0)&&(genderFlag||deduction.getShortName().equalsIgnoreCase("LWF"))){
					
					double amount=getTotalAmountOfEarningComponent(deduction.getCtcComponentName());
					System.out.println("INSIDE FLAT AMT "+deduction.getName()+" "+"TOTAL AMT : "+amount);
					if(deduction.getCondition()!=null&&!deduction.getCondition().equals("")
							&&deduction.getConditionValue()!=null&&deduction.getConditionValue()!=0){
						 
						 if(deduction.getCondition().equals("<")){
							 if(amount<=(deduction.getConditionValue()*12)){
								 if(deduction.getShortName().equalsIgnoreCase("PT")){
									 if(ptFlag){
										 if(validateDeduction(value,deduction,amount)){
											 totalAmount=deduction.getMaxAmount();
											 ptFlag=true;
										 }
									 }else{
										 totalAmount=deduction.getMaxAmount();
										 ptFlag=true;
									 }
								 }else{
									 totalAmount=deduction.getMaxAmount();
								 }
							 }
						 }else if(deduction.getCondition().equals(">")){
//							 if(amount>=deduction.getConditionValue()){
//								 totalAmount=deduction.getMaxAmount();
//							 }
							 System.out.println("PT AMT : "+amount+" Conditional Value : "+deduction.getConditionValue()*12+" "+deduction.getCondition());
							 if(amount>=(deduction.getConditionValue()*12)){
							 if(deduction.getShortName().equalsIgnoreCase("PT")){
								 if(ptFlag){
									 if(validateDeduction(value,deduction,amount)){
										 totalAmount=deduction.getMaxAmount();
										 ptFlag=true;
									 }
								 }else{
									 totalAmount=deduction.getMaxAmount();
									 ptFlag=true;
								 }
							 }else{
								 totalAmount=deduction.getMaxAmount();
							 }}
							 
						 }else if(deduction.getCondition().equals("=")){
//							 if(amount==deduction.getConditionValue()){
//								 totalAmount=deduction.getMaxAmount();
//							 }
							 if(amount==(deduction.getConditionValue()*12)){
							 if(deduction.getShortName().equalsIgnoreCase("PT")){
								 if(ptFlag){
									 if(validateDeduction(value,deduction,amount)){
										 totalAmount=deduction.getMaxAmount();
										 ptFlag=true;
									 }
								 }else{
									 totalAmount=deduction.getMaxAmount();
									 ptFlag=true;
								 }
							 }else{
								 totalAmount=deduction.getMaxAmount();
							 }}
						 }
					}else{
//						totalAmount=deduction.getMaxAmount();
						if(deduction.getShortName().equalsIgnoreCase("PT")){
							 if(ptFlag){
								 if(validateDeduction(value,deduction,amount)){
									 totalAmount=deduction.getMaxAmount();
									 ptFlag=true;
								 }
							 }else{
								 totalAmount=deduction.getMaxAmount();
								 ptFlag=true;
							 }
						 }else{
							 totalAmount=deduction.getMaxAmount();
						 }
					}
					
				}
				
				/**
				 * @author Anil , Date : 28-06-2019
				 * calculating Pf on the basis of updated logic
				 */
				if(deduction.getShortName().equalsIgnoreCase("PF")
						&&deduction.getCtcComponentName().equalsIgnoreCase("Gross-HRA")){
					if(deduction.isDeduction()){
						if(deduction.getIsRecord()==true){
							totalAmount=getTotalAmountOfPFComponent(pf,false,true,pfMaxFixedValue);
						}else{
							totalAmount=getTotalAmountOfPFComponent(pf,true,false,pfMaxFixedValue);
						}
					}
				}
				
				/**
				 * @author Anil , Date : 26-07-2019
				 * calculating ESIC on the basis of updated logic
				 */
				if(deduction.getShortName().equalsIgnoreCase("ESIC")
						&&deduction.getCtcComponentName().equalsIgnoreCase("Gross-Washing")){
					if(deduction.isDeduction()){
						if(deduction.getIsRecord()==true){
							totalAmount=getTotalAmountOfEsicComponent(esic,false,true);
						}else{
							totalAmount=getTotalAmountOfEsicComponent(esic,true,false);
						}
					}
				}
				
				deduction.setAmount(totalAmount);
			}
			sum+=totalAmount;
		}
		System.out.println("SUM : "+sum);
		return sum;
	}
	
	private double getTotalAmountOfEsicComponent(Esic esic, boolean employeeContribution,boolean employerContribution) {
		// TODO Auto-generated method stub
		double amount = 0;
		double rateAmount=0;
		
		if(esic!=null){
			for(CtcComponent ctc:form.earningtable.getValue()){
				rateAmount=rateAmount+(ctc.getAmount()/12);
			}
			for(OtEarningComponent earnComp:esic.getCompList()){
				for(CtcComponent ctc:form.earningtable.getValue()){
					if(ctc.getName().equals(earnComp.getComponentName())){
						amount=amount+ ctc.getAmount();
					}
				}
			}
		}
		
		if(employeeContribution){
			if(rateAmount>=esic.getEmployeeFromAmount()&&rateAmount<=esic.getEmployeeToAmount()){
				amount=(amount*esic.getEmployeeContribution())/100;
			}else{
				amount=0;
			}
		}
		if(employerContribution){
			if(rateAmount>=esic.getEmployerFromAmount()&&rateAmount<=esic.getEmployerToAmount()){
				amount=(amount*esic.getEmployerContribution())/100;
			}else{
				amount=0;
			}
		}
		return getEsicRoundOff(amount);
	}
	

	public double getEsicRoundOff(double amount){
		int esicAmount=0;
		esicAmount=(int) amount;
		
		if(esicAmount<amount){
			esicAmount++;
		}
		return esicAmount;
	}
	
	private boolean validateDeduction(List<CtcComponent> value,CtcComponent deduction, double amount) {
		for(CtcComponent ctc:value){
			if(deduction.getShortName().equals(ctc.getShortName())
					&&deduction.getGender().equals(ctc.getGender())){
				if(ctc.getAmount()!=0){
					
//					if(ctc.getMaxAmount()>=amount&&ctc.getMaxAmount()<deduction.getMaxAmount()){
//						return false;
//					}else{
//						ctc.setAmount(0d);;
//						return true;
//					}
					
					if(ctc.getConditionValue()*12>=amount){
						if(ctc.getConditionValue()*12<deduction.getConditionValue()*12){
							return false;
						}else{
							ctc.setAmount(0d);;
							return true;
						}
					}else{
						ctc.setAmount(0d);;
						return true;
					}
				}
			}
		}
		return true;
	}

	private double getTotalAmountOfEarningComponent(String componentName){
		double amount = 0;
		for(CtcComponent ctc:form.earningtable.getValue()){
			if(componentName.equals("Gross Earning")){
				amount=amount+ctc.getAmount();
			}
			if(ctc.getName().equals(componentName)){
				return ctc.getAmount();
			}
		}
		return amount;
	}

	private Double calculateGrossEarning(List<CtcComponent> earnings) {
		Double sum = 0.0;
		for (CtcComponent temp : earnings) {
			if (temp.getAmount() != null){
				sum = sum + temp.getAmount();
			}
		}
		return sum;

	}

	@Override
	public void onValueChange(ValueChangeEvent<Double> event) {
		System.out.println("inside On change..........");
		Double value = form.ctcAmount.getValue();
		if (value == null){
			form.ctcAmount.setValue(0d);
		}

		SalaryHeadPresenterTableProxy.ctcAmount = form.ctcAmount.getValue();

		form.ctcCopy.setValue(value);
		System.out.println("Value of ctc" + form.ctcCopy.getValue());
		System.out.println("Value  " + value);
		Double gross = form.grossEarning.getValue();
		if (gross != null){
			form.difference.setValue(value - gross);
		}
		
		/**Date 20-4-2019  by Amol 
		 * to add total Monthly CTC
		 */
		if(event.getSource().equals(form.ctcAmount)){
			double yearlyAmount=0;
			double monthlyAmount=0;
			if(form.ctcAmount!=null){
				yearlyAmount=form.ctcAmount.getValue();
				monthlyAmount=yearlyAmount/12;
				form.dbMonthlyCtcAmount.setValue(monthlyAmount);
			}
		}

	}

	

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		System.out.println("inside On Selection ..........");
		form.pic.reactOnSelection(event);
		setCalendar();

	}
	
	private void setCalendar() {
		
//		EmployeeInfo info = form.pic.getValue();
//		if (info != null) {
//			Calendar cal = info.getLeaveCalendar();
//			System.out.println("Calendar  is " + info.getLeaveCalendar());
//			if (cal == null) {
//				form.showDialogMessage("Calendar Not Allocated to This Employee !");
//				form.dbFromDate.setValue(null);
//				form.dbToDate.setValue(null);
//				form.dbFromDate.getTextBox().setText("");
//				form.dbToDate.getTextBox().setText("");
//				form.leaveCalendar.setValue("");
//			} else {
//				form.leaveCalendar.setValue(cal.toString());
//				form.dbFromDate.setValue(cal.getCalStartDate());
//				form.dbToDate.setValue(cal.getCalEndDate());
//			}
//		}
	}
	
	private void changeSatatus() {
		model.setStatus(CTC.ACTIVE);
		service.save(model, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				form.setMenuAsPerStatus();
				form.setToViewState();
				form.tbStatus.setText(CTC.ACTIVE);
				form.showDialogMessage("CTC  submitted !");

			}
		});
	}

	@Override
	public void reactOnSave() {

		if (validateAndUpdateModel()) {
			// Async querry is going to start. We do not want user to do any
			// thing untill it completes.
			view.showWaitSymbol();
			ctcservice.save(model, new AsyncCallback<ReturnFromServer>() {

				@Override
				public void onSuccess(ReturnFromServer result) {
					// Async run sucessfully , hide the wait symbol
					view.hideWaitSymbol();
					// set the count returned from server to this model
					if (result == null) {
						form.showDialogMessage("CTC already exists for this Employee in this Calender Year!!!");
						form.clear();
						List<CtcComponent> list = form.earningtable.getDataprovider().getList();
						for (int i = 0; i < list.size(); i++) {
							list.get(i).setAmount(0.0);
						}
						return;
					}
					model.setCount(result.count);
					// set the id from server to this model
					model.setId(result.id);
					form.setToViewState();
					// Set count on view
					form.setCount(result.count);
					form.showDialogMessage("Data Successfully Saved !",
							GWTCAlert.OPTION_ROUNDED_BLUE,
							GWTCAlert.OPTION_ANIMATION);
				}

				@Override
				public void onFailure(Throwable caught) {
					view.hideWaitSymbol();
					view.showDialogMessage("Data Save Unsuccessful! Try again.");
					caught.printStackTrace();

				}
			});
		}
	}
	
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.CTC")
	public static class CTCPresenterSearch extends SearchPopUpScreen<CTC> {
		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.CTC")
	public static class CTCPresenterTable extends SuperTable<CTC> implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			return null;
		}

		@Override
		public void createTable() {

		}

		@Override
		protected void initializekeyprovider() {

		}

		@Override
		public void addFieldUpdater() {

		}

		@Override
		public void setEnable(boolean state) {

		}

		@Override
		public void applyStyle() {

		}

	}
	
	
	public double getTotalAmountOfPFComponent(ProvidentFund pf,boolean isEmployee,boolean isEmployer,double cappedAmt){
		double amount = 0;
		
		if(pf!=null){
			for(OtEarningComponent earnComp:pf.getCompList()){
				for(CtcComponent ctc:form.earningtable.getValue()){
					if(ctc.getName().equals(earnComp.getComponentName())){
						amount=amount+ ctc.getAmount();
					}
				}
			}
		}
		if(cappedAmt!=0){
			if(amount>(cappedAmt*12)){
				amount=cappedAmt*12;
			}
		}
		if(isEmployee){
			amount=(amount*pf.getEmployeeContribution())/100;
		}
		if(isEmployer){
			amount=(amount*pf.getEmployerContribution())/100;
		}
		return amount;
	}
	
	//Ashwini Patil
	public void reactOnCTCEmail()
	{
		emailPopup.showPopUp();
		Console.log("emailPopup.showPopUp() called");
	}
	

}
