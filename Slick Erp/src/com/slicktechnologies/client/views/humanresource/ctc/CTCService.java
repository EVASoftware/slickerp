package com.slicktechnologies.client.views.humanresource.ctc;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
@RemoteServiceRelativePath("ctcservice")
public interface CTCService extends RemoteService {
	
	public ReturnFromServer save(CTC ctc);

}
