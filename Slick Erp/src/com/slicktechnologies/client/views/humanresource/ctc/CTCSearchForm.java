package com.slicktechnologies.client.views.humanresource.ctc;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.popups.GeneralComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneType;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class CTCSearchForm extends SearchPopUpScreen<CTC>{
	
	public EmployeeInfoComposite personInfo;
	
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Department> olbDepartMent;
	public ObjectListBox<Config> olbEmploymentType;
	public ObjectListBox<Config> olbDesignation;
	public ObjectListBox<Config> olbRole;
	public ObjectListBox<Calendar> olbcalender;
	public ListBox ctcStatus;
	public IntegerBox ctcid;
//	public DateComparator dateComparator;

	/**
	 * Date : 19-09-2018 BY ANIL
	 * 
	 */
	public IntegerBox ibTemplateId;
	/**Date 15-5-2019 ctc template name added by Amol**/
	GeneralComposite generalComposite;
	
	/**
     * @author Anil , Date : 13-07-2019
     * Capturing project name for processing multiple site payroll
     * raised by Rahul tiwari for riseon
     */
   
   ObjectListBox<HrProject> project;

	public Object getVarRef(String varName)
	{
		return null ;
	}
	public CTCSearchForm()
	{
		super();
		createGui();
	}
	public void initWidget()
	{
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		olbDepartMent=new ObjectListBox<Department>();
		AppUtility.makeSalesPersonListBoxDepartment(olbDepartMent);
		
		olbEmploymentType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbEmploymentType,Screen.EMPLOYEETYPE);
		
		olbDesignation=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbDesignation,Screen.EMPLOYEEDESIGNATION);
		
		olbRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbRole,Screen.EMPLOYEEROLE);
		
		olbcalender=new ObjectListBox<Calendar>();
		olbcalender.MakeLive(new MyQuerry(new Vector<Filter>(),new Calendar()));
		ctcid=new IntegerBox();
		ctcStatus=new ListBox();
		AppUtility.setStatusListBox(ctcStatus,CTC.getStatusList());
		//dateComparator= new DateComparator("creationDate",new CTC());
		MyQuerry qu = new MyQuerry();
		Company c = new Company();
		System.out.println("Company Id :: " + c.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		
		
		
		qu.setFilters(filtervec);
		qu.setQuerryObject(new CTCTemplate());
		
		ibTemplateId=new IntegerBox();
		generalComposite = new GeneralComposite("", qu);
		
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Department",olbDepartMent);
		FormField folbdepartment= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Employment Type",olbEmploymentType);
		FormField folbtype= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Employee Designation",olbDesignation);
		FormField folbdesignation= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Employee Role",olbRole);
		FormField folbrole= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Status",ctcStatus);
		FormField fstatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/*builder = new FormFieldBuilder("Application From Date",dateComparator.getFromDate());
		FormField fdbFromDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Application To Date",dateComparator.getToDate());
		FormField fdbToDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		 */
		
		builder = new FormFieldBuilder("Calender Name",olbcalender);
		FormField fcalendername= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("CTC Id",ctcid);
		FormField fctcid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("CTC Template Id",ibTemplateId);
		FormField fibTemplateId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("CTC Template",generalComposite);
		FormField fgeneralComposite=builder.setMandatory(false).setMandatoryMsg("").setColSpan(0).build();
		
		builder = new FormFieldBuilder("Project", project);
		FormField fproject = builder.setMandatory(false).setMandatoryMsg("Project is mandatory").setRowSpan(0).setColSpan(0).build();
		

		this.fields=new FormField[][]{

				{fctcid,fcalendername,folbBranch,folbdepartment},
				{folbtype,folbdesignation,folbrole,fstatus},
				{fibTemplateId,fgeneralComposite,fproject},
				{fpersonInfo}
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;


		if(olbDepartMent.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(olbDepartMent.getValue().trim());
			temp.setQuerryString("department");
			filtervec.add(temp);
		}

		if(olbcalender.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(olbcalender.getValue().trim());
			temp.setQuerryString("calendarName");
			filtervec.add(temp);
		}

		if(olbRole.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbRole.getValue().trim());
			temp.setQuerryString("employeeRole");
			filtervec.add(temp);
		}
		if(olbDesignation.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbDesignation.getValue().trim());
			temp.setQuerryString("employeedDesignation");
			filtervec.add(temp);
		}
		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if(olbEmploymentType.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbEmploymentType.getValue().trim());
			temp.setQuerryString("employeeType");
			filtervec.add(temp);
		}
		
		if(ctcid.getValue()!=null)
		{  
			temp=new Filter();
			temp.setIntValue(ctcid.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		
		
		
		
		if(ibTemplateId.getValue()!=null)
		{  
			temp=new Filter();
			temp.setIntValue(ibTemplateId.getValue());
			temp.setQuerryString("ctcTemplateId");
			filtervec.add(temp);
		}
		Console.log("General composite NAME"+generalComposite.name.getValue());
		
		if(generalComposite.name.getValue()!=null && !generalComposite.name.getValue().equals("")){
			SuperModel model=generalComposite.info.get(generalComposite.name.getValue());
			Console.log("General composite size"+model.getCount());
			temp=new Filter();
			temp.setIntValue(model.getCount());
			temp.setQuerryString("ctcTemplateId");
			filtervec.add(temp);
		}
		
		if(project.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(project.getValue().trim());
			temp.setQuerryString("projectName");
			filtervec.add(temp);
		}
		
		

//		if(personInfo.getIdValue()!=-1)
//		{  
//			temp=new Filter();
//			temp.setIntValue(personInfo.getIdValue());
//			temp.setQuerryString("empid");
//			filtervec.add(temp);
//		}
//
//		if(!(personInfo.getFullNameValue().equals("")))
//		{
//			temp=new Filter();
//			temp.setStringValue(personInfo.getFullNameValue());
//			temp.setQuerryString("employeeName");
//			filtervec.add(temp);
//		}
//		if(personInfo.getCellValue()!=-1l)
//		{
//			temp=new Filter();
//			temp.setLongValue(personInfo.getCellValue());
//			temp.setQuerryString("empCellNo");
//			filtervec.add(temp);
//		}
		
		if(!personInfo.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
			temp.setQuerryString("empid");
			filtervec.add(temp);
		}

		if(!(personInfo.getName().getValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(personInfo.getName().getValue());
			temp.setQuerryString("employeeName");
			filtervec.add(temp);
		}
//		if(!personInfo.getPhone().getValue().equals(""))
//		{
//			temp=new Filter();
//			temp.setLongValue(personInfo.getCellNumber());
//			temp.setQuerryString("empCellNo");
//			filtervec.add(temp);
//		}
		
		
		if(ctcStatus.getSelectedIndex()!=0)
		{
			temp=new Filter();
			int index=ctcStatus.getSelectedIndex();
			String selValue=ctcStatus.getItemText(index);
			temp.setStringValue(selValue);
			temp.setQuerryString("status");
			filtervec.add(temp);
		}

		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CTC());
		return querry;
	}
	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}

	
}
