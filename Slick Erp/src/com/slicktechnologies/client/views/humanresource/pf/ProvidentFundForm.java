package com.slicktechnologies.client.views.humanresource.pf;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.humanresource.overtime.OtEarningCompTable;
import com.slicktechnologies.shared.common.humanresourcelayer.ProvidentFund;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.OtEarningComponent;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ProvidentFundForm extends FormTableScreen<ProvidentFund> implements ClickHandler{



	TextBox tbProvidentFundId;
	TextBox tbProvidentFundName;
	TextBox tbPfShortName;
	CheckBox cbStatus;
	ListBox lbEarningComponent;
	OtEarningCompTable otEarningCompTbl;
	Button btnAdd;
	
	DoubleBox dbEmployeeContribution;
	DoubleBox dbEmployerContribution;
	
	DoubleBox dbEmprPf;
	DoubleBox dbEmprEdli;
	
	DateBox dbEffectiveFrom;
	
	
	public ProvidentFundForm(SuperTable<ProvidentFund> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		cbStatus.setValue(true);
		otEarningCompTbl.connectToLocal();
	}

	private void initalizeWidget() {

		tbProvidentFundId = new TextBox();
		tbProvidentFundId.setEnabled(false);
		
		tbProvidentFundName=new TextBox();
		
		tbPfShortName=new TextBox();
		
		
		cbStatus = new CheckBox();
		cbStatus = new CheckBox();
		cbStatus.setValue(true);
		
		lbEarningComponent=new ListBox();
		lbEarningComponent.addItem("--SELECT--");
		otEarningCompTbl=new OtEarningCompTable();
		btnAdd=new Button("Add");
		btnAdd.addClickHandler(this);
		
		dbEmployeeContribution=new DoubleBox();
		dbEmployerContribution=new DoubleBox();
		
		dbEmprPf=new DoubleBox();
		dbEmprEdli=new DoubleBox();
		
		dbEffectiveFrom=new DateBoxWithYearSelector();
		
	}
	
	
	@Override
	public void createScreen() {
		initalizeWidget();

		this.processlevelBarNames = new String[] { "New" };
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPLInformation = fbuilder.setlabel("Provident Fund Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("PF ID", tbProvidentFundId);
		FormField ftbProvidentFundId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* PF Short Name", tbPfShortName);
		FormField ftbPfShortName = fbuilder.setMandatory(true)
				.setMandatoryMsg("PF Short Name is mandatory !")
				.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* PF Name", tbProvidentFundName);
		FormField ftbProvidentFundName = fbuilder.setMandatory(true)
				.setMandatoryMsg("PF Name is mandatory !")
				.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", cbStatus);
		FormField fcbStatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Earning Component", lbEarningComponent);
		FormField foblEarningComp = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fbtnAdd = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", otEarningCompTbl.getTable());
		FormField fotEarningCompTbl = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("* Employee Contribution", dbEmployeeContribution);
		FormField fdbEmployeeContribution = fbuilder.setMandatory(true).setMandatoryMsg("Employee Contribution is mandatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Employer Contribution", dbEmployerContribution);
		FormField fdbEmployerContribution = fbuilder.setMandatory(true).setMandatoryMsg("Employer Contribution is mandatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Employer PF", dbEmprPf);
		FormField fdbEmprPf = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("EDLI", dbEmprEdli);
		FormField fdbEmprEdli = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgrpingEpfInformation = fbuilder.setlabel("Employer PF Bifurcation")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Effective From", dbEffectiveFrom);
		FormField fdbEffectiveFrom = fbuilder.setMandatory(true).setMandatoryMsg("Effective From is mandatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		FormField[][] formfield = {
				{ fgroupingPLInformation },
				{ ftbProvidentFundId, ftbPfShortName,ftbProvidentFundName,fcbStatus },
				{ fdbEmployeeContribution,fdbEmployerContribution,fdbEffectiveFrom},
				{ foblEarningComp,fbtnAdd},
				{ fotEarningCompTbl},
				{ fgrpingEpfInformation},
				{ fdbEmprPf,fdbEmprEdli }
				
				
				
		};
		this.fields = formfield;
	}
	
	@Override
	public void updateModel(ProvidentFund model) {
		
		if(tbProvidentFundName.getValue()!=null){
			model.setPfName(tbProvidentFundName.getValue());
		}
		if(tbPfShortName.getValue()!=null){
			model.setPfShortName(tbPfShortName.getValue());
		}
		if (cbStatus.getValue() != null)
			model.setStatus(cbStatus.getValue());
		
		if(otEarningCompTbl.getValue()!=null){
			model.setCompList(otEarningCompTbl.getValue());
		}
		if(dbEmployeeContribution.getValue()!=null){
			model.setEmployeeContribution(dbEmployeeContribution.getValue());
		}else{
			model.setEmployeeContribution(0);
		}
		if(dbEmployerContribution.getValue()!=null){
			model.setEmployerContribution(dbEmployerContribution.getValue());
		}else{
			model.setEmployerContribution(0);
		}
		
		if(dbEmprPf.getValue()!=null){
			model.setEmpoyerPF(dbEmprPf.getValue());
		}else{
			model.setEmpoyerPF(0);
		}
		if(dbEmprEdli.getValue()!=null){
			model.setEdli(dbEmprEdli.getValue());
		}else{
			model.setEdli(0);
		}
		
		if(dbEffectiveFrom.getValue()!=null){
			model.setEffectiveFromDate(dbEffectiveFrom.getValue());
		}
		presenter.setModel(model);
	}
	
	@Override
	public void updateView(ProvidentFund view) {
		this.tbProvidentFundId.setValue(view.getCount() + "");
		if (view.getPfName() != null)
			tbProvidentFundName.setValue(view.getPfName());
		if (view.getPfShortName() != null)
			tbPfShortName.setValue(view.getPfShortName());
			cbStatus.setValue(view.isStatus());
		if(view.getCompList()!=null){
			otEarningCompTbl.setValue(view.getCompList());
		}
		if(view.getEmployeeContribution()!=0){
			dbEmployeeContribution.setValue(view.getEmployeeContribution());
		}
		if(view.getEmployerContribution()!=0){
			dbEmployerContribution.setValue(view.getEmployerContribution());
		}
		
		if(view.getEmpoyerPF()!=0){
			dbEmprPf.setValue(view.getEmpoyerPF());
		}
		if(view.getEdli()!=0){
			dbEmprEdli.setValue(view.getEdli());
		}
		
		if(view.getEffectiveFromDate()!=null){
			dbEffectiveFrom.setValue(view.getEffectiveFromDate());
		}

		presenter.setModel(view);
	}
	
	
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")) {
					menus[k].setVisible(true);
				} else{
					menus[k].setVisible(false);
				}

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PROVIDENTFUND,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void setCount(int count) {
		this.tbProvidentFundId.setValue(presenter.getModel().getCount() + "");
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}


	@Override
	public boolean validate() {
		boolean superRes = super.validate();

		if (superRes == false){
			return false;
		}
		
		if(otEarningCompTbl.getValue()==null||otEarningCompTbl.getValue().size()==0){
			showDialogMessage("Please add earning component.");
			return false;
		}
		
		if(dbEmprPf.getValue()!=null||dbEmprEdli.getValue()!=null){
			double emprpf=0;
			double edli=0;
			double sum=0;
			if(dbEmprPf.getValue()!=null){
				emprpf=dbEmprPf.getValue();
			}
			if(dbEmprEdli.getValue()!=null){
				edli=dbEmprEdli.getValue();
			}
			sum=emprpf+edli;
			if(dbEmployerContribution.getValue()!=sum){
				showDialogMessage("Sum of employer bifurcation should be equal to employer contribution.");
				return false;
			}
		}
		
		if(getSupertable().getValue()!=null){
			for(ProvidentFund pf:getSupertable().getValue()){
				if(!tbProvidentFundId.getValue().equals("")){
					int id=Integer.parseInt(tbProvidentFundId.getValue());
					if(id!=pf.getCount()&&pf.isStatus()==true){
						showDialogMessage("To create new pf , you must inactive previous one.");
						return false;
					}
				}else{
					if(pf.isStatus()==true){
						showDialogMessage("To create new pf , you must inactive previous one.");
						return false;
					}
				}
			}
		}
		
		
		
		return true;
	}

	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource().equals(btnAdd)){
			if(lbEarningComponent.getSelectedIndex()!=0){
				if(otEarningCompTbl.getValue()!=null&&otEarningCompTbl.getValue().size()!=0){
					for(OtEarningComponent obj:otEarningCompTbl.getValue()){
						if(obj.getComponentName().equals(lbEarningComponent.getValue(lbEarningComponent.getSelectedIndex()))){
							showDialogMessage("Component already added.");
							return;
						}
					}
					OtEarningComponent ot=new OtEarningComponent();
					ot.setComponentName(lbEarningComponent.getValue(lbEarningComponent.getSelectedIndex()));
					otEarningCompTbl.getValue().add(ot);
				}else{
					OtEarningComponent ot=new OtEarningComponent();
					ot.setComponentName(lbEarningComponent.getValue(lbEarningComponent.getSelectedIndex()));
					otEarningCompTbl.getValue().add(ot);
				}
			}else{
				for(int i=1;i<lbEarningComponent.getItemCount();i++){
					boolean validCompFlag=false;
					String compName=lbEarningComponent.getItemText(i);
					for(OtEarningComponent obj:otEarningCompTbl.getValue()){
						if(obj.getComponentName().equals(compName)){
							validCompFlag=true;
						}
					}
					if(!validCompFlag){
						OtEarningComponent ot=new OtEarningComponent();
						ot.setComponentName(compName);
						otEarningCompTbl.getValue().add(ot);
					}
				}
//				showDialogMessage("Please select earning component.");
			}
			
		}
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		
		System.out.println("INSIDE SET ENABLE METHOD   ");
		tbProvidentFundId.setEnabled(false);
		otEarningCompTbl.setEnable(state);

	}

	@Override
	public void clear() {
		super.clear();
		cbStatus.setValue(false);
		cbStatus.setValue(true);
		tbProvidentFundId.setEnabled(false);
		otEarningCompTbl.connectToLocal();
	}


}
