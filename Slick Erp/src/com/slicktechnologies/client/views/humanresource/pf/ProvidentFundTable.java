package com.slicktechnologies.client.views.humanresource.pf;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.ProvidentFund;

public class ProvidentFundTable extends SuperTable<ProvidentFund> {
	TextColumn<ProvidentFund> getCountColumn;
	TextColumn<ProvidentFund> getNameColumn;
	TextColumn<ProvidentFund> getEmployerContributionColumn;
	TextColumn<ProvidentFund> getEmployeeContributionColumn;
	TextColumn<ProvidentFund> getStatusColumn;
	  
	public ProvidentFundTable() {
		super();
	}
	  
	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetName();
		addColumngetEmployeeContribution();
		addColumngetEmployerContribution();
		addColumngetStatus();
	}

	private void addColumngetEmployeeContribution() {
		// TODO Auto-generated method stub
		getEmployeeContributionColumn = new TextColumn<ProvidentFund>() {
			@Override
			public String getValue(ProvidentFund object) {
				return object.getEmployeeContribution() + "";
			}
		};
		table.addColumn(getEmployeeContributionColumn, "Employee Contribution");
		getEmployeeContributionColumn.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<ProvidentFund>() {
			@Override
			public Object getKey(ProvidentFund item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	@Override
	public void addFieldUpdater() {
	}
	
	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetName();
		addSortingEmployerContributionCol();
		addSortingEmployeeContributionCol();
		addSortingStatus();
	}

	

	protected void addSortinggetCount() {
		List<ProvidentFund> list = getDataprovider().getList();
		columnSort = new ListHandler<ProvidentFund>(list);
		columnSort.setComparator(getCountColumn, new Comparator<ProvidentFund>() {
			@Override
			public int compare(ProvidentFund e1, ProvidentFund e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<ProvidentFund>() {
			@Override
			public String getValue(ProvidentFund object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		getCountColumn.setSortable(true);
	}

	protected void addSortinggetName() {
		List<ProvidentFund> list = getDataprovider().getList();
		columnSort = new ListHandler<ProvidentFund>(list);
		columnSort.setComparator(getNameColumn, new Comparator<ProvidentFund>() {
			@Override
			public int compare(ProvidentFund e1, ProvidentFund e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPfName() != null && e2.getPfName() != null) {
						return e1.getPfName().compareTo(e2.getPfName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortingEmployerContributionCol() {
		List<ProvidentFund> list = getDataprovider().getList();
		columnSort = new ListHandler<ProvidentFund>(list);
		columnSort.setComparator(getEmployerContributionColumn,new Comparator<ProvidentFund>() {
			
			@Override
			public int compare(ProvidentFund e1, ProvidentFund e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployerContribution() == e2.getEmployerContribution()) {
						return 0;
					}
					if (e1.getEmployerContribution() > e2.getEmployerContribution()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
			
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingEmployeeContributionCol() {
		List<ProvidentFund> list = getDataprovider().getList();
		columnSort = new ListHandler<ProvidentFund>(list);
		columnSort.setComparator(getEmployeeContributionColumn,new Comparator<ProvidentFund>() {
			
			@Override
			public int compare(ProvidentFund e1, ProvidentFund e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployeeContribution() == e2.getEmployeeContribution()) {
						return 0;
					}
					if (e1.getEmployeeContribution() > e2.getEmployeeContribution()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
			
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingStatus() {
		List<ProvidentFund> list = getDataprovider().getList();
		columnSort = new ListHandler<ProvidentFund>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<ProvidentFund>() {
			@Override
			public int compare(ProvidentFund e1, ProvidentFund e2) {
				if (e1 != null && e2 != null) {
					if (e1.isStatus() == true)
						return 1;
					else
						return -1;

				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetName() {
		getNameColumn = new TextColumn<ProvidentFund>() {
			@Override
			public String getValue(ProvidentFund object) {
				return object.getPfName() + "";
			}
		};
		table.addColumn(getNameColumn, "Name");
		getNameColumn.setSortable(true);
	}

	

	protected void addColumngetEmployerContribution() {
		getEmployerContributionColumn = new TextColumn<ProvidentFund>() {
			@Override
			public String getValue(ProvidentFund object) {
				return object.getEmployerContribution() + "";
			}
		};
		table.addColumn(getEmployerContributionColumn, "Employer Contribution");
		getEmployerContributionColumn.setSortable(true);
	}
	

	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<ProvidentFund>() {
			@Override
			public String getValue(ProvidentFund object) {
				if (object.isStatus() == true)
					return "Active";
				else
					return "In Active";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
	}

}
