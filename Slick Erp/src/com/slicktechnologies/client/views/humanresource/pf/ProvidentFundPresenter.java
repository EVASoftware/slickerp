package com.slicktechnologies.client.views.humanresource.pf;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.ProvidentFund;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;

public class ProvidentFundPresenter extends FormTableScreenPresenter<ProvidentFund> {

	ProvidentFundForm form;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	public ProvidentFundPresenter(FormTableScreen<ProvidentFund> view, ProvidentFund model) {
		super(view, model);
		form = (ProvidentFundForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getOvertimeQuery());
		/**
		 * @author Anil , Date : 08-06-2019
		 */
		getEarningComponent() ;
		form.setPresenter(this);
	}
	
	private void getEarningComponent() {
		MyQuerry querry=new MyQuerry();
		Filter filter=null;
		Vector<Filter> filtervec=new Vector<Filter>();
		
		Company c=new Company();
		 
		filter=new Filter();
		filter.setBooleanvalue(false);
		filter.setQuerryString("isDeduction");
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		  
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CtcComponent());
		 
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("Leave result size : "+result.size());
				if (result.size() != 0) {
					form.lbEarningComponent.clear();
					form.lbEarningComponent.addItem("--SELECT--");
//					form.lbEarningComponent.addItem("Gross Earning");
					for(SuperModel model:result){
						CtcComponent entity=(CtcComponent) model;
						form.lbEarningComponent.addItem(entity.getName());
					}
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Failure!!!!!!!!!!1");
			}
		});
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel lbl = (InlineLabel) e.getSource();
		if (lbl.getText().contains("New")) {
			form.setToNewState();
			this.initalize();
		}
	}
	
	public static void initalize() {
		ProvidentFundTable gentableScreen = new ProvidentFundTable();
		ProvidentFundForm form = new ProvidentFundForm(gentableScreen,FormTableScreen.UPPER_MODE, true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		ProvidentFundPresenter presenter = new ProvidentFundPresenter(form,new ProvidentFund());
		AppMemory.getAppMemory().stickPnel(form);

	}

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}

	@Override
	protected void makeNewModel() {
		model = new ProvidentFund();
	}
	
	public MyQuerry getOvertimeQuery() {
		MyQuerry quer = new MyQuerry(new Vector<Filter>(), new ProvidentFund());
		return quer;
	}
	
	
	

}
