package com.slicktechnologies.client.views.humanresource.leavetype;

import com.google.gwt.event.dom.client.ClickEvent;

import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.simplesoftwares.client.library.appskeleton.AppMemory;

/**
 * FormTableScreen presenter template
 */
public class LeaveTypePresenter extends FormTableScreenPresenter<LeaveType> {

	// Token to set the concrete form
	LeaveTypeForm form;

	public LeaveTypePresenter(FormTableScreen<LeaveType> view, LeaveType model) {
		super(view, model);
		form = (LeaveTypeForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getLeaveTypeQuery());
		form.setPresenter(this);
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel lbl = (InlineLabel) e.getSource();
		
//		if (lbl.getText().contains("Delete")) {
//
//			if (this.form.validate()) {
//				List<LeaveType> data = form.getSupertable().getDataprovider().getList();
//				/*
//				 * Deleting
//				 */
//				for (int i = 0; i < data.size(); i++) {
//					if (data.get(i).getId().equals(model.getId())) {
//						data.remove(i);
//					}
//					form.getSupertable().getDataprovider().refresh();
//				}
//				reactOnDelete();
//			}
//		}
		if (lbl.getText().contains("New")) {
			form.setToNewState();
			this.initalize();
		}
	}

	@Override
	public void reactOnPrint() {

	}

	@Override
	public void reactOnEmail() {

	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {

		model = new LeaveType();
	}

	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getLeaveTypeQuery() {
		MyQuerry quer = new MyQuerry(new Vector<Filter>(), new LeaveType());
		return quer;
	}

	public void setModel(LeaveType entity) {
		model = entity;
	}

	public static void initalize() {
		LeaveTypePresenterTable gentableScreen = new LeaveTypePresenterTableProxy();
		LeaveTypeForm form = new LeaveTypeForm(gentableScreen,FormTableScreen.UPPER_MODE, true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();

		// // LeaveTypePresenterTable gentableSearch=GWT.create(
		// LeaveTypePresenterTable.class);
		// // LeaveTypePresenterSearch.staticSuperTable=gentableSearch;
		// // LeaveTypePresenterSearch searchpopup=GWT.create(
		// LeaveTypePresenterSearch.class);
		// // form.setSearchpopupscreen(searchpopup);

		LeaveTypePresenter presenter = new LeaveTypePresenter(form,new LeaveType());
		AppMemory.getAppMemory().stickPnel(form);

	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.LeaveType")
	public static class LeaveTypePresenterTable extends SuperTable<LeaveType>
			implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub

		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub

		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub

		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub

		}
	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.LeaveType")
	public static class LeaveTypePresenterSearch extends SearchPopUpScreen<LeaveType> {

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
	};

	private void reactTo() {
	}

}
