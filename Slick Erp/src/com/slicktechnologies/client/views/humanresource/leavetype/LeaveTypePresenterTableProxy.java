package com.slicktechnologies.client.views.humanresource.leavetype;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import java.util.List;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;
import com.slicktechnologies.client.views.humanresource.leavetype.LeaveTypePresenter.LeaveTypePresenterTable;

public class LeaveTypePresenterTableProxy extends LeaveTypePresenterTable {
	TextColumn<LeaveType> getShortNameColumn;
	TextColumn<LeaveType> getNameColumn;
	TextColumn<LeaveType> getMinDaysColumn;
	TextColumn<LeaveType> getMaxDaysColumn;
	TextColumn<LeaveType> getCountColumn;
	TextColumn<LeaveType> getStatusColumn;

	public Object getVarRef(String varName) {
		if (varName.equals("getShortNameColumn"))
			return this.getShortNameColumn;
		if (varName.equals("getNameColumn"))
			return this.getNameColumn;

		if (varName.equals("getMinDaysColumn"))
			return this.getMinDaysColumn;
		if (varName.equals("getMaxDaysColumn"))
			return this.getMaxDaysColumn;
		if (varName.equals("getCountColumn"))
			return this.getCountColumn;
		return null;
	}

	public LeaveTypePresenterTableProxy() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetName();
		addColumngetShortName();
		addColumngetMinDays();
		addColumngetMaxDays();
		addColumngetStatus();
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<LeaveType>() {
			@Override
			public Object getKey(LeaveType item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	@Override
	public void addFieldUpdater() {
	}
	
	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetName();
		addSortinggetShortName();
		addSortinggetMinDays();
		addSortinggetMaxDays();
		addSortingStatus();
	}

	

	protected void addSortinggetCount() {
		List<LeaveType> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveType>(list);
		columnSort.setComparator(getCountColumn, new Comparator<LeaveType>() {
			@Override
			public int compare(LeaveType e1, LeaveType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<LeaveType>() {
			@Override
			public String getValue(LeaveType object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		getCountColumn.setSortable(true);
	}

	protected void addSortinggetName() {
		List<LeaveType> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveType>(list);
		columnSort.setComparator(getNameColumn, new Comparator<LeaveType>() {
			@Override
			public int compare(LeaveType e1, LeaveType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getName() != null && e2.getName() != null) {
						return e1.getName().compareTo(e2.getName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetShortName() {
		List<LeaveType> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveType>(list);
		columnSort.setComparator(getShortNameColumn,
				new Comparator<LeaveType>() {
					@Override
					public int compare(LeaveType e1, LeaveType e2) {
						if (e1 != null && e2 != null) {
							if (e1.getShortName() != null
									&& e2.getShortName() != null) {
								return e1.getShortName().compareTo(
										e2.getShortName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetMinDays() {
		List<LeaveType> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveType>(list);
		columnSort.setComparator(getMinDaysColumn, new Comparator<LeaveType>() {
			@Override
			public int compare(LeaveType e1, LeaveType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMinDays() == e2.getMinDays()) {
						return 0;
					}
					if (e1.getMinDays() > e2.getMinDays()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetMaxDays() {
		List<LeaveType> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveType>(list);
		columnSort.setComparator(getMaxDaysColumn, new Comparator<LeaveType>() {
			@Override
			public int compare(LeaveType e1, LeaveType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMaxDays() == e2.getMaxDays()) {
						return 0;
					}
					if (e1.getMaxDays() > e2.getMaxDays()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingStatus() {
		List<LeaveType> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveType>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<LeaveType>() {
			@Override
			public int compare(LeaveType e1, LeaveType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() == true)
						return 1;
					else
						return -1;

				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetName() {
		getNameColumn = new TextColumn<LeaveType>() {
			@Override
			public String getValue(LeaveType object) {
				return object.getName() + "";
			}
		};
		table.addColumn(getNameColumn, "Name");
		getNameColumn.setSortable(true);
	}

	

	protected void addColumngetShortName() {
		getShortNameColumn = new TextColumn<LeaveType>() {
			@Override
			public String getValue(LeaveType object) {
				return object.getShortName() + "";
			}
		};
		table.addColumn(getShortNameColumn, "Short Name");
		getShortNameColumn.setSortable(true);
	}

	

	protected void addColumngetMinDays() {
		getMinDaysColumn = new TextColumn<LeaveType>() {
			@Override
			public String getValue(LeaveType object) {
				return object.getMinDays() + "";
			}
		};
		table.addColumn(getMinDaysColumn, "Min Days");
		getMinDaysColumn.setSortable(true);
	}

	

	protected void addColumngetMaxDays() {
		getMaxDaysColumn = new TextColumn<LeaveType>() {
			@Override
			public String getValue(LeaveType object) {
				return object.getMaxDays() + "";
			}
		};
		table.addColumn(getMaxDaysColumn, "Max Days");
		getMaxDaysColumn.setSortable(true);
	}

	

	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<LeaveType>() {
			@Override
			public String getValue(LeaveType object) {
				if (object.getStatus() == true)
					return "Active";
				else
					return "In Active";
			}
		};
		table.addColumn(getMaxDaysColumn, "Max Days");
		getMaxDaysColumn.setSortable(true);
	}

}
