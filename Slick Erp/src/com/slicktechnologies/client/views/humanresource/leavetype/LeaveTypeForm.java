package com.slicktechnologies.client.views.humanresource.leavetype;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.event.dom.client.*;

/**
 * FormTablescreen template.
 */
public class LeaveTypeForm extends FormTableScreen<LeaveType> implements ClickHandler {

	// Token to add the varialble declarations
	IntegerBox ibMinDays,ibMaximumDays,ibDocumentRequiredMinimumDays;
	CheckBox cbCountWeekend,cbCountHoliday,cbCancellationAllow,cbPaidLeave,
			 cbDocumentRequired,cbRelapseAllowCarryOver,cbencashable,cbstatus;
	
	IntegerBox ibmaxcarryforward;
	TextBox ibLeaveTypeId;
	TextBox tbLeaveTypeName, tbLeaveTypeShortName;
	
	CheckBox cbMonthlyAllocation;
	DoubleBox dbMonthlyAllocatedLeave;

	// Token to add the concrete presenter class name
	// protected <PresenterClassName> presenter;
	
	
	/**
	 * Date : 29-05-2018 By ANIL
	 * 
	 */
	CheckBox cbCompensatoryLeave;
	IntegerBox ibNoOfDaysToAvail;

	public LeaveTypeForm(SuperTable<LeaveType> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		cbstatus.setValue(true);
	}

	private void initalizeWidget() {

		ibMinDays = new IntegerBox();
		ibLeaveTypeId = new TextBox();
		ibLeaveTypeId.setEnabled(false);
		ibMaximumDays = new IntegerBox();
		cbCountWeekend = new CheckBox();
		cbCountHoliday = new CheckBox();
		cbCancellationAllow = new CheckBox();
		cbPaidLeave = new CheckBox();
		cbDocumentRequired = new CheckBox();
		cbDocumentRequired.addClickHandler(this);
		ibDocumentRequiredMinimumDays = new IntegerBox();
		ibDocumentRequiredMinimumDays.setEnabled(false);
		cbRelapseAllowCarryOver = new CheckBox();
		cbRelapseAllowCarryOver.addClickHandler(this);
		ibmaxcarryforward = new IntegerBox();
		ibmaxcarryforward.setEnabled(false);
		tbLeaveTypeName = new TextBox();
		tbLeaveTypeShortName = new TextBox();
		cbencashable = new CheckBox();
		cbstatus = new CheckBox();
		cbstatus.setValue(true);
		
		cbMonthlyAllocation=new CheckBox();
		cbMonthlyAllocation.addClickHandler(this);
		dbMonthlyAllocatedLeave=new DoubleBox();
		dbMonthlyAllocatedLeave.setEnabled(false);
		
		
		cbCompensatoryLeave=new CheckBox();
		cbCompensatoryLeave.setValue(false);
		cbCompensatoryLeave.addClickHandler(this);
		
		ibNoOfDaysToAvail=new IntegerBox();
		ibNoOfDaysToAvail.setEnabled(false);
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		// Token to initialize the processlevel menus.
		initalizeWidget();
		// ////////////////////////// Form Field
		// Initialization////////////////////////////////////////////////////

		this.processlevelBarNames = new String[] { "New" };
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingLeaveTypeInformation = fbuilder
				.setlabel("Leave Type Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		fbuilder = new FormFieldBuilder("LeaveType ID", ibLeaveTypeId);
		FormField ftLeaveTypeId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* LeaveType Name", tbLeaveTypeName);
		FormField ftbLeaveTypeName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Leave Type Name is mandatory !")
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Short Name", tbLeaveTypeShortName);
		FormField ftbLeaveTypeShortName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Leave Type Short Name is mandatory !")
				.setRowSpan(0).setColSpan(0).build();
		// fbuilder = new
		// FormFieldBuilder("Partial Days Allow",cbPartialDaysAllow);
		// FormField fcbPartialDaysAllow=
		// fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Count Weekend", cbCountWeekend);
		FormField fcbCountWeekend = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Count Holiday", cbCountHoliday);
		FormField fcbCountHoliday = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cancellation Allow",
				cbCancellationAllow);
		FormField fcbCancellationAllow = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Minimum Days", ibMinDays);
		FormField fibMinDays = fbuilder.setMandatory(true)
				.setMandatoryMsg("Minimum Leave Day is mandatory !")
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Maximum Days", ibMaximumDays);
		FormField fibMaximumDays = fbuilder.setMandatory(true)
				.setMandatoryMsg("Maximum Leave Day is mandatory !")
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Paid Leave", cbPaidLeave);
		FormField fcbPaidLeave = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Document Required", cbDocumentRequired);
		FormField fcbDocumentRequired = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Document Required Minimum Days",
				ibDocumentRequiredMinimumDays);
		FormField fibDocumentRequiredMinimumDays = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder(" Allow Carryforward",
				cbRelapseAllowCarryOver);
		FormField fcbRelapseAllowCarryOver = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Maximum Carry Forward",
				ibmaxcarryforward);
		FormField fibmaxcarryforward = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();
		
		
		FormField fLeaveAllocationInformation = fbuilder
				.setlabel("Leave Allocation Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Monthly Allocation", cbMonthlyAllocation);
		FormField fcbMonthlyAllocation = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Monthly Allocation Leave", dbMonthlyAllocatedLeave);
		FormField fcbMonthlyAllocatonLeave = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField frelapseInformation = fbuilder
				.setlabel("Leave Carryforward Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Encashable", cbencashable);
		FormField fcbencashable = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Status", cbstatus);
		FormField fcbstatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
		FormField fCompesatoryGrouping = fbuilder
				.setlabel("Compensatory Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Compensatory Leave", cbCompensatoryLeave);
		FormField fcbCompensatoryLeave = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("No. Of Days To Avail", ibNoOfDaysToAvail);
		FormField fibNoOfDaysToAvail = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// //////////////////////////////Lay Out Making
		// Code///////////////////////////////////////////////////////////////

		FormField[][] formfield = {
				{ fgroupingLeaveTypeInformation },
				{ ftLeaveTypeId, ftbLeaveTypeName, ftbLeaveTypeShortName,fibMinDays },
				{ fibMaximumDays, fcbPaidLeave,fcbCountHoliday, fcbCountWeekend },
				{ fcbDocumentRequired,fibDocumentRequiredMinimumDays,fcbCancellationAllow},
				{ fLeaveAllocationInformation,fCompesatoryGrouping},
				{ fcbMonthlyAllocation,fcbMonthlyAllocatonLeave,fcbCompensatoryLeave,fibNoOfDaysToAvail},
//				{ fCompesatoryGrouping},
//				{ fcbCompensatoryLeave,fibNoOfDaysToAvail},
				{ frelapseInformation },
				{ fcbRelapseAllowCarryOver, fibmaxcarryforward, fcbencashable,fcbstatus }, 
				};
		this.fields = formfield;
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(LeaveType model) {

		if (tbLeaveTypeName.getValue() != null)
			model.setName(tbLeaveTypeName.getValue());
		if (tbLeaveTypeShortName.getValue() != null)
			model.setShortName(tbLeaveTypeShortName.getValue());
		// if(cbPartialDaysAllow.getValue()!=null)
		// model.setPartialDayAllow(cbPartialDaysAllow.getValue());
		if (cbCountWeekend.getValue() != null)
			model.setCountWeekend(cbCountWeekend.getValue());
		if (cbCountHoliday.getValue() != null)
			model.setCountHoliday(cbCountHoliday.getValue());
		if (cbCancellationAllow.getValue() != null)
			model.setCancellationAllow(cbCancellationAllow.getValue());
		if (ibMinDays.getValue() != null)
			model.setMinDays(ibMinDays.getValue());
		if (ibMaximumDays.getValue() != null)
			model.setMaxDays(ibMaximumDays.getValue());
		if (cbPaidLeave.getValue() != null)
			model.setUnpaidLeave(cbPaidLeave.getValue());
		if (cbDocumentRequired.getValue() != null)
			model.setDocumentRequired(cbDocumentRequired.getValue());
		if (ibDocumentRequiredMinimumDays.getValue() != null)
			model.setDoumentRequiredMinDays(ibDocumentRequiredMinimumDays.getValue());
		if (cbRelapseAllowCarryOver.getValue() != null)
			model.setRelapseAllowCarryOver(cbRelapseAllowCarryOver.getValue());
		if (ibmaxcarryforward.getValue() != null)
			model.setMaxCarryForward(ibmaxcarryforward.getValue());
		if (cbencashable.getValue() != null)
			model.setLeaveEncashable(cbencashable.getValue());
		
		if (cbMonthlyAllocation.getValue() != null)
			model.setMonthlyAllocation(cbMonthlyAllocation.getValue());
		
		if (dbMonthlyAllocatedLeave.getValue() != null)
			model.setMonthlyAllocationLeave(dbMonthlyAllocatedLeave.getValue());
		
		if (cbstatus.getValue() != null)
			model.setStatus(cbstatus.getValue());
		
		if(cbCompensatoryLeave.getValue()!=null){
			model.setCompensatoryLeave(cbCompensatoryLeave.getValue());
		}
		if(ibNoOfDaysToAvail.getValue()!=null){
			model.setNo_of_days_to_avail(ibNoOfDaysToAvail.getValue());
		}else{
			model.setNo_of_days_to_avail(0);
		}

		presenter.setModel(model);

	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(LeaveType view) {
		this.ibLeaveTypeId.setValue(view.getCount() + "");
		if (view.getName() != null)
			tbLeaveTypeName.setValue(view.getName());
		if (view.getShortName() != null)
			tbLeaveTypeShortName.setValue(view.getShortName());
		// if(view.getPartialDayAllow()!=null)
		// cbPartialDaysAllow.setValue(view.getPartialDayAllow());
		if (view.getCountWeekend() != null)
			cbCountWeekend.setValue(view.getCountWeekend());
		if (view.getCountHoliday() != null)
			cbCountHoliday.setValue(view.getCountHoliday());
		if (view.getCancellationAllow() != null)
			cbCancellationAllow.setValue(view.getCancellationAllow());
		ibMinDays.setValue(view.getMinDays());
		ibMaximumDays.setValue(view.getMaxDays());
		if (view.getPaidLeave() != null)
			cbPaidLeave.setValue(view.getPaidLeave());
		if (view.getDocumentRequired() != null)
			cbDocumentRequired.setValue(view.getDocumentRequired());
		ibDocumentRequiredMinimumDays.setValue(view.getDoumentRequiredMinDays());
		if (view.getRelapseAllowCarryOver() != null)
			cbRelapseAllowCarryOver.setValue(view.getRelapseAllowCarryOver());
		ibmaxcarryforward.setValue(view.getMaxCarryForward());
		if (view.getLeaveEncashable() != null)
			cbencashable.setValue(view.getLeaveEncashable());
		
		if (view.getMonthlyAllocation() != null)
			cbMonthlyAllocation.setValue(view.getMonthlyAllocation());
		if (view.getMonthlyAllocationLeave() != null)
			dbMonthlyAllocatedLeave.setValue(view.getMonthlyAllocationLeave());
		
		if (view.getStatus() != null)
			cbstatus.setValue(view.getStatus());
		
		cbCompensatoryLeave.setValue(view.isCompensatoryLeave());
		if(view.getNo_of_days_to_avail()!=0){
			ibNoOfDaysToAvail.setValue(view.getNo_of_days_to_avail());
		}

		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.LEAVETYPE,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value.
	 */
	@Override
	public void setCount(int count) {
		this.ibLeaveTypeId.setValue(presenter.getModel().getCount() + "");
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

	/**
	 * Validations
	 */

	@Override
	public boolean validate() {
		boolean superRes = super.validate();

		if (superRes == false)
			return false;

		if (ibMaximumDays.getValue() != null&& ibMaximumDays.getValue() != null) {

			if (ibMaximumDays.getValue() < ibMinDays.getValue()) {
				showDialogMessage("Maximum Day should not be less then minimum days.");
				return false;
			}
		}

		if (cbDocumentRequired.getValue() == true) {

			if (ibDocumentRequiredMinimumDays.getValue() == null) {
				showDialogMessage("Document required minimum days is mandatory.");
				return false;
			}
			int mindays = ibMinDays.getValue();
			int maxdays = ibMaximumDays.getValue();
			int docReqMinimumdays = ibDocumentRequiredMinimumDays.getValue();

			// 5>10 || 5<2
			if (docReqMinimumdays > maxdays || docReqMinimumdays < mindays) {
				showDialogMessage("Document required minimum days should be between Minimum Days and Maximum Days!!");
				return false;
			}
		}

		if (cbRelapseAllowCarryOver.getValue() == true&& ibmaxcarryforward.getValue() == null) {
			this.showDialogMessage("Maximum carry forward is mandatory.");
			return false;
		}

		if (cbRelapseAllowCarryOver.getValue() == true&& ibmaxcarryforward.getValue() != null) {
			if (ibmaxcarryforward.getValue() > ibMaximumDays.getValue()) {
				this.showDialogMessage("Carry forward days cannot be greater then Maximum Leave Days !");
				return false;
			}
		}
		
		if(cbCompensatoryLeave.getValue()==true&&ibNoOfDaysToAvail.getValue()==null){
			showDialogMessage("Please add no. of days to avail compensatory leave.");
			return false;
		}
		return true;
	}

	@Override
	public void onClick(ClickEvent event) {
		if (cbRelapseAllowCarryOver.getValue() == true) {
			ibmaxcarryforward.setEnabled(true);
		} else {
			ibmaxcarryforward.setEnabled(false);
			ibmaxcarryforward.setValue(null);
		}
		if (cbDocumentRequired.getValue() == true) {
			ibDocumentRequiredMinimumDays.setEnabled(true);
		} else {
			ibDocumentRequiredMinimumDays.setEnabled(false);
			ibDocumentRequiredMinimumDays.setValue(null);
		}
		
		if (cbMonthlyAllocation.getValue() == true) {
			dbMonthlyAllocatedLeave.setEnabled(true);
		} else {
			dbMonthlyAllocatedLeave.setEnabled(false);
			dbMonthlyAllocatedLeave.setValue(null);
		}
		
		if (cbCompensatoryLeave.getValue() == true) {
			ibNoOfDaysToAvail.setEnabled(true);
		} else {
			ibNoOfDaysToAvail.setEnabled(false);
			ibNoOfDaysToAvail.setValue(null);
		}
		
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibLeaveTypeId.setEnabled(false);

		if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			cbCountHoliday.setEnabled(false);
			cbCountWeekend.setEnabled(false);
			this.cbPaidLeave.setEnabled(false);
			cbDocumentRequired.setEnabled(false);
			cbencashable.setEnabled(false);
			cbRelapseAllowCarryOver.setEnabled(false);
			cbCancellationAllow.setEnabled(false);
			if (cbRelapseAllowCarryOver.getValue() == false)
				ibmaxcarryforward.setEnabled(false);
			if (cbDocumentRequired.getValue() == false)
				ibDocumentRequiredMinimumDays.setEnabled(false);
			cbMonthlyAllocation.setEnabled(false);
			if(cbMonthlyAllocation.getValue()==false){
				dbMonthlyAllocatedLeave.setEnabled(false);
			}
		}

		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			ibmaxcarryforward.setEnabled(false);
			ibDocumentRequiredMinimumDays.setEnabled(false);
			dbMonthlyAllocatedLeave.setEnabled(false);
			ibNoOfDaysToAvail.setEnabled(false);
		}

	}

	@Override
	public void clear() {
		super.clear();
		cbCancellationAllow.setValue(false);
		cbCountHoliday.setValue(false);
		cbCountWeekend.setValue(false);
		cbDocumentRequired.setValue(false);
		cbencashable.setValue(false);
		cbPaidLeave.setValue(false);
		cbRelapseAllowCarryOver.setValue(false);
		cbMonthlyAllocation.setValue(false);
		cbstatus.setValue(true);
		cbCompensatoryLeave.setValue(false);

	}

}
