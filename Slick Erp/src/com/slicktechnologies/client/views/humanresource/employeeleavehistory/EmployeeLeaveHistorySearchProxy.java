package com.slicktechnologies.client.views.humanresource.employeeleavehistory;

import java.util.Vector;

import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;

public class EmployeeLeaveHistorySearchProxy extends SearchPopUpScreen<EmployeeLeave> {
	EmployeeInfoComposite personInfo;
	DateComparator dateComparator;
	
	public EmployeeLeaveHistorySearchProxy(boolean b) {
		// TODO Auto-generated constructor stub
		super(b);
		getPopup().setHeight("300px");
		createGui();
	}
	
	public void initWidget(){
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee Id");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
		
		dateComparator= new DateComparator("leaveDate",new EmployeeLeave());
	}
	
	public void createScreen(){
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
	
		builder = new FormFieldBuilder("From Date",dateComparator.getFromDate());
		FormField fdbFromDate= builder.setMandatory(false).setRowSpan(1).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date",dateComparator.getToDate());
		FormField fdbToDate= builder.setMandatory(false).setRowSpan(1).setColSpan(0).build();
		
		
		this.fields=new FormField[][]{
			{fpersonInfo},
			{fdbFromDate,fdbToDate}
		};
	}

	@Override
	public MyQuerry getQuerry() {
		// TODO Auto-generated method stub
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(!personInfo.getId().getValue().equals("")){  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
			temp.setQuerryString("emplId");
			filtervec.add(temp);
		}
		
		if(dateComparator.getValue()!=null){
			filtervec.addAll(dateComparator.getValue());
		}
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new EmployeeLeave());
		return querry;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

}
