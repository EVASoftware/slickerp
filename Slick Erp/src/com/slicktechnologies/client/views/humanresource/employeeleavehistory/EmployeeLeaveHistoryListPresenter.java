package com.slicktechnologies.client.views.humanresource.employeeleavehistory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class EmployeeLeaveHistoryListPresenter extends TableScreenPresenter<EmployeeLeave> {

	TableScreen<EmployeeLeave>form;
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	GeneralServiceAsync genservice = GWT.create(GeneralService.class);
	
	public EmployeeLeaveHistoryListPresenter(TableScreen<EmployeeLeave> view,EmployeeLeave model) {
		super(view, model);
		form=view;
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.EMPLOYEELEAVEHISTORY,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	public static void initialize(){
		EmployeeLeaveHistoryTableProxy table=new EmployeeLeaveHistoryTableProxy();
		EmployeeLeaveHistoryListForm form=new EmployeeLeaveHistoryListForm(table);
		EmployeeLeaveHistorySearchProxy.staticSuperTable=table;
		EmployeeLeaveHistorySearchProxy searchPopUp=new EmployeeLeaveHistorySearchProxy(false);
		form.setSearchpopupscreen(searchPopUp);
		EmployeeLeaveHistoryListPresenter presenter=new EmployeeLeaveHistoryListPresenter(form, new EmployeeLeave());
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}
	
	public void reactOnDownload() {
		ArrayList<EmployeeLeave> empLeaveArray = new ArrayList<EmployeeLeave>();
		List<EmployeeLeave> list = (List<EmployeeLeave>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		empLeaveArray.addAll(list);
		csvservice.setEmployeeLeaveList(empLeaveArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 167;
				Window.open(url, "test", "enabled");
			}
		});
	}

	@Override
	protected void reactOnGo() {
		// TODO Auto-generated method stub
		super.reactOnGo();
		
//		int empId=0;
//		Date fromDate=null;
//		Date toDate=null;
//		
//		EmployeeLeaveHistorySearchProxy searchForm =(EmployeeLeaveHistorySearchProxy) form.getSearchpopupscreen();
//		
//		if(!searchForm.personInfo.getId().getValue().equals("")){
//			empId=Integer.parseInt(searchForm.personInfo.getId().getValue());
//		}
//		if(searchForm.dateComparator.getFromDate().getValue()!=null){
//			fromDate=searchForm.dateComparator.getFromDate().getValue();
//		}
//		if(searchForm.dateComparator.getToDate().getValue()!=null){
//			toDate=searchForm.dateComparator.getToDate().getValue();
//		}
//		genservice.getEmployeeLeave(UserConfiguration.getCompanyId(), empId, fromDate, toDate, new AsyncCallback<ArrayList<EmployeeLeave>>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				
//			}
//
//			@Override
//			public void onSuccess(ArrayList<EmployeeLeave> result) {
//				// TODO Auto-generated method stub
//				
//				form.getSuperTable().connectToLocal();
//				form.getSuperTable().getListDataProvider().getList().addAll(result);
//			}
//		});
		
	}
	
	

}
