package com.slicktechnologies.client.views.humanresource.employeeleavehistory;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;

public class EmployeeLeaveHistoryTableProxy extends SuperTable<EmployeeLeave> {

	TextColumn<EmployeeLeave> getColEmpId;
	TextColumn<EmployeeLeave> getColEmpName;
	TextColumn<EmployeeLeave> getColLeaveDate;
	TextColumn<EmployeeLeave> getColLeaveHours;
	TextColumn<EmployeeLeave> getColLeaveInDays;
	TextColumn<EmployeeLeave> getColLeaveName;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		getColEmpId();
		getColEmpName();
		getColLeaveDate();
		getColLeaveHours();
		getColLeaveInDays();
		getColLeaveName();
	}

	private void getColEmpId() {
		// TODO Auto-generated method stub
		getColEmpId=new TextColumn<EmployeeLeave>() {
			
			@Override
			public String getValue(EmployeeLeave object) {
				// TODO Auto-generated method stub
				return object.getEmplId()+"";
			}
		};
		table.addColumn(getColEmpId, "Employee Id");
		getColEmpId.setSortable(true);
	}

	private void getColEmpName() {
		// TODO Auto-generated method stub
		getColEmpName=new TextColumn<EmployeeLeave>() {
			
			@Override
			public String getValue(EmployeeLeave object) {
				// TODO Auto-generated method stub
				if(object.getEmployee()!=null){
					return object.getEmployee().getFullName();
				}
				return "";
			}
		};
		table.addColumn(getColEmpName, "Employee Name");
		getColEmpName.setSortable(true);
		
	}

	private void getColLeaveDate() {
		// TODO Auto-generated method stub
		getColLeaveDate=new TextColumn<EmployeeLeave>() {
			
			@Override
			public String getValue(EmployeeLeave object) {
				// TODO Auto-generated method stub
				return AppUtility.parseDate(object.getLeaveDate())+"";
			}
		};
		table.addColumn(getColLeaveDate, "Leave Date");
		getColLeaveDate.setSortable(true);
	}

	private void getColLeaveHours() {
		// TODO Auto-generated method stub
		getColLeaveHours=new TextColumn<EmployeeLeave>() {
			
			@Override
			public String getValue(EmployeeLeave object) {
				// TODO Auto-generated method stub
				return object.getLeaveHrs()+"";
			}
		};
		table.addColumn(getColLeaveHours, "Leave Hours");
		getColLeaveHours.setSortable(true);
	}

	private void getColLeaveInDays() {
		// TODO Auto-generated method stub
		getColLeaveInDays=new TextColumn<EmployeeLeave>() {
			
			@Override
			public String getValue(EmployeeLeave object) {
				// TODO Auto-generated method stub
				if(object.getEmployee()!=null){
					return object.getLeaveHrs()/object.getEmployee().getLeaveCalendar().getWorkingHours()+"";
				}
				return "";
			}
		};
		table.addColumn(getColLeaveInDays, "Leave(In Days)");
		getColLeaveInDays.setSortable(true);
	}

	private void getColLeaveName() {
		// TODO Auto-generated method stub
		getColLeaveName=new TextColumn<EmployeeLeave>() {
			
			@Override
			public String getValue(EmployeeLeave object) {
				// TODO Auto-generated method stub
				return object.getLeaveTypeName()+"";
			}
		};
		table.addColumn(getColLeaveName, "Leave Name");
		getColLeaveName.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addColumnSorting() {
		// TODO Auto-generated method stub
		super.addColumnSorting();
		addSortingOnEmpIdCol();
		addSortingOnEmpNameCol();
		addSortingOnLeaveNameCol();
		addSortingOnLeaveDateCol();
		addSortingOnLeaveHourCol();
		addSortingOnLeaveInDayCol();
	}

	private void addSortingOnEmpIdCol() {
		// TODO Auto-generated method stub
		List<EmployeeLeave> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeLeave>(list);
		columnSort.setComparator(getColEmpId, new Comparator<EmployeeLeave>()
		{
			@Override
			public int compare(EmployeeLeave e1,EmployeeLeave e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getEmplId()== e2.getEmplId()){
						return 0;}
					if(e1.getEmplId()> e2.getEmplId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnEmpNameCol() {
		// TODO Auto-generated method stub
		List<EmployeeLeave> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeLeave>(list);
		columnSort.setComparator(getColEmpName, new Comparator<EmployeeLeave>()
		{
			@Override
			public int compare(EmployeeLeave e1,EmployeeLeave e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().getFullName().compareTo(e2.getEmployee().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnLeaveNameCol() {
		// TODO Auto-generated method stub
		List<EmployeeLeave> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeLeave>(list);
		columnSort.setComparator(getColLeaveName, new Comparator<EmployeeLeave>()
		{
			@Override
			public int compare(EmployeeLeave e1,EmployeeLeave e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getLeaveTypeName()!=null && e2.getLeaveTypeName()!=null){
						return e1.getLeaveTypeName().compareTo(e2.getLeaveTypeName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnLeaveDateCol() {
		// TODO Auto-generated method stub
		List<EmployeeLeave> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeLeave>(list);
		columnSort.setComparator(getColLeaveDate, new Comparator<EmployeeLeave>()
		{
			@Override
			public int compare(EmployeeLeave e1,EmployeeLeave e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getLeaveDate()!=null && e2.getLeaveDate()!=null){
						return e1.getLeaveDate().compareTo(e2.getLeaveDate());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnLeaveHourCol() {
		// TODO Auto-generated method stub
		List<EmployeeLeave> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeLeave>(list);
		columnSort.setComparator(getColLeaveHours, new Comparator<EmployeeLeave>()
		{	
			@Override
			public int compare(EmployeeLeave e1,EmployeeLeave e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getLeaveHrs()== e2.getLeaveHrs()){
						return 0;}
					if(e1.getLeaveHrs()> e2.getLeaveHrs()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
		}});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnLeaveInDayCol() {
		// TODO Auto-generated method stub
		List<EmployeeLeave> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeLeave>(list);
		columnSort.setComparator(getColLeaveInDays, new Comparator<EmployeeLeave>()
		{	
			@Override
			public int compare(EmployeeLeave e1,EmployeeLeave e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getLeaveHrs()== e2.getLeaveHrs()){
						return 0;}
					if(e1.getLeaveHrs()> e2.getLeaveHrs()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
		}});
		table.addColumnSortHandler(columnSort);
	}
	
	

}
