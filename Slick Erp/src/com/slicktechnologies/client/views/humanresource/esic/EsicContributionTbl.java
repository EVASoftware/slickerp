package com.slicktechnologies.client.views.humanresource.esic;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.EsicContributionPeriod;

public class EsicContributionTbl extends SuperTable<EsicContributionPeriod>{

	TextColumn<EsicContributionPeriod>getFromPeriodCol;
	TextColumn<EsicContributionPeriod>getToPeriodCol;
	Column<EsicContributionPeriod,String>deleteCol;
	
	@Override
	public void createTable() {
		getFromPeriodCol();
		getToPeriodCol();
		deleteCol();
	}

	private void getFromPeriodCol() {
		getFromPeriodCol=new TextColumn<EsicContributionPeriod>() {
			@Override
			public String getValue(EsicContributionPeriod object) {
				if(object.getFromPeriod()!=null){
					return object.getFromPeriod();
				}
				return "";
			}
		};
		table.addColumn(getFromPeriodCol, "From Period");
	}

	private void getToPeriodCol() {
		getToPeriodCol=new TextColumn<EsicContributionPeriod>() {
			@Override
			public String getValue(EsicContributionPeriod object) {
				if(object.getToPeriod()!=null){
					return object.getToPeriod();
				}
				return "";
			}
		};
		table.addColumn(getToPeriodCol, "To Period");
	}

	private void deleteCol() {
		ButtonCell cell=new ButtonCell();
		deleteCol=new Column<EsicContributionPeriod,String>(cell) {
			@Override
			public String getValue(EsicContributionPeriod object) {
				return "Delete";
			}
		};
		table.addColumn(deleteCol, " ");
		
		deleteCol.setFieldUpdater(new FieldUpdater<EsicContributionPeriod, String>() {
			@Override
			public void update(int index, EsicContributionPeriod object, String value) {
				// TODO Auto-generated method stub
				getDataprovider().getList().remove(index);
				table.redraw();
			}
		});
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);
		
		if (state == true) {
			getFromPeriodCol();
			getToPeriodCol();
			deleteCol();
		}else{
			getFromPeriodCol();
			getToPeriodCol();
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
