package com.slicktechnologies.client.views.humanresource.esic;

import java.util.Date;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.overtime.OtEarningCompTable;
import com.slicktechnologies.shared.common.humanresourcelayer.Esic;
import com.slicktechnologies.shared.common.humanresourcelayer.EsicContributionPeriod;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.OtEarningComponent;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class EsicForm extends FormTableScreen<Esic> implements ClickHandler{



	TextBox tbEsicId;
	TextBox tbEsicName;
	TextBox tbEsicShortName;
	CheckBox cbStatus;
	ListBox lbEarningComponent;
	OtEarningCompTable otEarningCompTbl;
	Button btnAdd;
	
	DoubleBox dbEmployeeContribution;
	DoubleBox dbEmployerContribution;
	
	DateBox dbEffectiveFrom;
	
	DoubleBox dbEmployeeFromAmount;
	DoubleBox dbEmployeeToAmount;
	
	DoubleBox dbEmployerFromAmount;
	DoubleBox dbEmployerToAmount;
	
	DateBox lbFromContributionPeriod;
	DateBox lbToContributionPeriod;
	Button btnAddPeriod;
	
	EsicContributionTbl contributionPeriodTbl;
	
	
	public EsicForm(SuperTable<Esic> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		cbStatus.setValue(true);
		otEarningCompTbl.connectToLocal();
//		setFormat();
	}
	public void setFormat(){
		DateTimeFormat format=DateTimeFormat.getFormat("MMM");
		DateBox.DefaultFormat defformat=new DateBox.DefaultFormat(format);
		lbFromContributionPeriod.setFormat(defformat);
		lbToContributionPeriod.setFormat(defformat);
	}

	private void initalizeWidget() {

		tbEsicId = new TextBox();
		tbEsicId.setEnabled(false);
		
		tbEsicName=new TextBox();
		
		tbEsicShortName=new TextBox();
		
		
		cbStatus = new CheckBox();
		cbStatus = new CheckBox();
		cbStatus.setValue(true);
		
		lbEarningComponent=new ListBox();
		lbEarningComponent.addItem("--SELECT--");
		otEarningCompTbl=new OtEarningCompTable();
		btnAdd=new Button("Add");
		btnAdd.addClickHandler(this);
		
		dbEmployeeContribution=new DoubleBox();
		dbEmployerContribution=new DoubleBox();
		
		dbEffectiveFrom=new DateBoxWithYearSelector();
		
		dbEmployeeFromAmount=new DoubleBox();
		dbEmployeeToAmount=new DoubleBox();
		
		dbEmployerFromAmount=new DoubleBox();
		dbEmployerToAmount=new DoubleBox();
		
		
		DateTimeFormat format=DateTimeFormat.getFormat("MMM");
		DateBox.DefaultFormat defformat=new DateBox.DefaultFormat(format);
		lbFromContributionPeriod=new DateBoxWithYearSelector();
		lbFromContributionPeriod.setFormat(defformat);
		lbToContributionPeriod=new DateBoxWithYearSelector();
		lbToContributionPeriod.setFormat(defformat);
		btnAddPeriod=new Button("Add");
		btnAddPeriod.addClickHandler(this);
		contributionPeriodTbl=new EsicContributionTbl();
				
		
	}
	
	
	@Override
	public void createScreen() {
		initalizeWidget();

		this.processlevelBarNames = new String[] { "New" };
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPLInformation = fbuilder.setlabel("Esic Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Esic ID", tbEsicId);
		FormField ftbEsicId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Esic Short Name", tbEsicShortName);
		FormField ftbEsicShortName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Esic Short Name is mandatory !")
				.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Esic Name", tbEsicName);
		FormField ftbEsicName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Esic Name is mandatory !")
				.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", cbStatus);
		FormField fcbStatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Earning Component", lbEarningComponent);
		FormField foblEarningComp = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fbtnAdd = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", otEarningCompTbl.getTable());
		FormField fotEarningCompTbl = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("* Employee Contribution", dbEmployeeContribution);
		FormField fdbEmployeeContribution = fbuilder.setMandatory(true).setMandatoryMsg("Employee Contribution is mandatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Employer Contribution", dbEmployerContribution);
		FormField fdbEmployerContribution = fbuilder.setMandatory(true).setMandatoryMsg("Employer Contribution is mandatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Effective From", dbEffectiveFrom);
		FormField fdbEffectiveFrom = fbuilder.setMandatory(true).setMandatoryMsg("Effective From is mandatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgrpingEmp = fbuilder.setlabel("Employee contribution range")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgrpingEmpr = fbuilder.setlabel("Employer contribution range")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("* From Amount", dbEmployeeFromAmount);
		FormField fdbEmployeeFromAmount = fbuilder.setMandatory(true).setMandatoryMsg("Employee Contribution from amount is mandatory !").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* To Amount", dbEmployeeToAmount);
		FormField fdbEmployeeToAmount = fbuilder.setMandatory(true).setMandatoryMsg("Employee Contribution to amount is mandatory !").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* From Amount", dbEmployerFromAmount);
		FormField fdbEmployerFromAmount = fbuilder.setMandatory(true).setMandatoryMsg("Employer Contribution from amount is mandatory !").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* To Amount", dbEmployerToAmount);
		FormField fdbEmployerToAmount = fbuilder.setMandatory(true).setMandatoryMsg("Employer Contribution to amount is mandatory !").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgrpingEsicPeriod = fbuilder.setlabel("Esic contribution period")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("From Contribution Period", lbFromContributionPeriod);
		FormField flbFromContributionPeriod = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Contribution Period", lbToContributionPeriod);
		FormField flbToContributionPeriod = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("", btnAddPeriod);
		FormField fbtnAddPeriod = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("", contributionPeriodTbl.getTable());
		FormField fcontributionPeriodTbl = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
	
		
		FormField[][] formfield = {
				{ fgroupingPLInformation },
				{ ftbEsicId, ftbEsicShortName,ftbEsicName,fcbStatus },
				{ fdbEmployeeContribution,fdbEmployerContribution,fdbEffectiveFrom},
				{ foblEarningComp,fbtnAdd},
				{ fotEarningCompTbl},
				{ fgrpingEmp,fgrpingEmpr},
				{ fdbEmployeeFromAmount,fdbEmployeeToAmount,fdbEmployerFromAmount,fdbEmployerToAmount },
				{ fgrpingEsicPeriod},
				{ flbFromContributionPeriod,flbToContributionPeriod,fbtnAddPeriod},
				{ fcontributionPeriodTbl}
				
				
				
		};
		this.fields = formfield;
	}
	
	@Override
	public void updateModel(Esic model) {
		
		if(tbEsicName.getValue()!=null){
			model.setEsicName(tbEsicName.getValue());
		}
		if(tbEsicShortName.getValue()!=null){
			model.setEsicShortName(tbEsicShortName.getValue());
		}
		if (cbStatus.getValue() != null)
			model.setStatus(cbStatus.getValue());
		
		if(otEarningCompTbl.getValue()!=null){
			model.setCompList(otEarningCompTbl.getValue());
		}
		if(dbEmployeeContribution.getValue()!=null){
			model.setEmployeeContribution(dbEmployeeContribution.getValue());
		}else{
			model.setEmployeeContribution(0);
		}
		if(dbEmployerContribution.getValue()!=null){
			model.setEmployerContribution(dbEmployerContribution.getValue());
		}else{
			model.setEmployerContribution(0);
		}
		
		if(dbEffectiveFrom.getValue()!=null){
			model.setEffectiveFromDate(dbEffectiveFrom.getValue());
		}
		
		if(dbEmployeeFromAmount.getValue()!=null){
			model.setEmployeeFromAmount(dbEmployeeFromAmount.getValue());
		}else{
			model.setEmployeeFromAmount(0);
		}
		if(dbEmployeeToAmount.getValue()!=null){
			model.setEmployeeToAmount(dbEmployeeToAmount.getValue());
		}else{
			model.setEmployeeToAmount(0);
		}
		
		if(dbEmployerFromAmount.getValue()!=null){
			model.setEmployerFromAmount(dbEmployerFromAmount.getValue());
		}else{
			model.setEmployerFromAmount(0);
		}
		if(dbEmployerToAmount.getValue()!=null){
			model.setEmployerToAmount(dbEmployerToAmount.getValue());
		}else{
			model.setEmployerToAmount(0);
		}
		
		if(contributionPeriodTbl.getValue()!=null){
			model.setContributionPeriodList(contributionPeriodTbl.getValue());
		}
		
		presenter.setModel(model);
	}
	
	@Override
	public void updateView(Esic view) {
		this.tbEsicId.setValue(view.getCount() + "");
		if (view.getEsicName() != null)
			tbEsicName.setValue(view.getEsicName());
		if (view.getEsicShortName() != null)
			tbEsicShortName.setValue(view.getEsicShortName());
			cbStatus.setValue(view.isStatus());
		if(view.getCompList()!=null){
			otEarningCompTbl.setValue(view.getCompList());
		}
		if(view.getEmployeeContribution()!=0){
			dbEmployeeContribution.setValue(view.getEmployeeContribution());
		}
		if(view.getEmployerContribution()!=0){
			dbEmployerContribution.setValue(view.getEmployerContribution());
		}
		if(view.getEffectiveFromDate()!=null){
			dbEffectiveFrom.setValue(view.getEffectiveFromDate());
		}
		

		if(view.getEmployeeFromAmount()!=0){
			dbEmployeeFromAmount.setValue(view.getEmployeeFromAmount());
		}
		if(view.getEmployeeToAmount()!=0){
			dbEmployeeToAmount.setValue(view.getEmployeeToAmount());
		}
		
		if(view.getEmployerFromAmount()!=0){
			dbEmployerFromAmount.setValue(view.getEmployerFromAmount());
		}
		if(view.getEmployerToAmount()!=0){
			dbEmployerToAmount.setValue(view.getEmployerToAmount());
		}
		if(view.getContributionPeriodList()!=null){
			contributionPeriodTbl.setValue(view.getContributionPeriodList());
		}

		presenter.setModel(view);
	}
	
	
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")) {
					menus[k].setVisible(true);
				} else{
					menus[k].setVisible(false);
				}

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PROVIDENTFUND,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void setCount(int count) {
		this.tbEsicId.setValue(presenter.getModel().getCount() + "");
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}


	@Override
	public boolean validate() {
		boolean superRes = super.validate();

		if (superRes == false){
			return false;
		}
		
		if(otEarningCompTbl.getValue()==null||otEarningCompTbl.getValue().size()==0){
			showDialogMessage("Please add earning component.");
			return false;
		}
		
		if(getSupertable().getValue()!=null){
			for(Esic pf:getSupertable().getValue()){
				if(!tbEsicId.getValue().equals("")){
					int id=Integer.parseInt(tbEsicId.getValue());
					if(id!=pf.getCount()&&pf.isStatus()==true){
						showDialogMessage("To create new esic , you must inactive previous one.");
						return false;
					}
				}else{
					if(pf.isStatus()==true){
						showDialogMessage("To create new esic , you must inactive previous one.");
						return false;
					}
				}
			}
		}
		
		
		
		return true;
	}

	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource().equals(btnAdd)){
			if(lbEarningComponent.getSelectedIndex()!=0){
				if(otEarningCompTbl.getValue()!=null&&otEarningCompTbl.getValue().size()!=0){
					for(OtEarningComponent obj:otEarningCompTbl.getValue()){
						if(obj.getComponentName().equals(lbEarningComponent.getValue(lbEarningComponent.getSelectedIndex()))){
							showDialogMessage("Component already added.");
							return;
						}
					}
					OtEarningComponent ot=new OtEarningComponent();
					ot.setComponentName(lbEarningComponent.getValue(lbEarningComponent.getSelectedIndex()));
					otEarningCompTbl.getValue().add(ot);
				}else{
					OtEarningComponent ot=new OtEarningComponent();
					ot.setComponentName(lbEarningComponent.getValue(lbEarningComponent.getSelectedIndex()));
					otEarningCompTbl.getValue().add(ot);
				}
			}else{
				for(int i=1;i<lbEarningComponent.getItemCount();i++){
					boolean validCompFlag=false;
					String compName=lbEarningComponent.getItemText(i);
					for(OtEarningComponent obj:otEarningCompTbl.getValue()){
						if(obj.getComponentName().equals(compName)){
							validCompFlag=true;
						}
					}
					if(!validCompFlag){
						OtEarningComponent ot=new OtEarningComponent();
						ot.setComponentName(compName);
						otEarningCompTbl.getValue().add(ot);
					}
				}
//				showDialogMessage("Please select earning component.");
			}
			
		}
		
		if(event.getSource()==btnAddPeriod){
			if(validContributionPeriod()){
				EsicContributionPeriod obj=new EsicContributionPeriod();
				obj.setFromPeriod(getContributionPeriod(lbFromContributionPeriod));
				obj.setToPeriod(getContributionPeriod(lbToContributionPeriod));
				contributionPeriodTbl.getDataprovider().getList().add(obj);
			}
		}
	}
	
	private boolean validContributionPeriod() {
		DateTimeFormat format = DateTimeFormat.getFormat("MMM");
//		if (dbDateBox.getValue() == null) {
//			return null;
//		} else {
//			return format.format(dbDateBox.getValue());
//		}
		Console.log("INSIDE DB FROM : "+lbFromContributionPeriod.getValue()+" INSIDE DB TO : "+lbToContributionPeriod.getValue());
		if(lbToContributionPeriod.getValue().before(lbFromContributionPeriod.getValue())){
			showDialogMessage("To period must be greater than from period.");
			return false;
		}
		if(contributionPeriodTbl.getValue().size()!=0){
			for(EsicContributionPeriod obj:contributionPeriodTbl.getValue()){
				Date fromPeriod=format.parse(obj.getFromPeriod());
				Date toPeriod=format.parse(obj.getToPeriod());
				
				Console.log("FROM : "+fromPeriod+" TO : "+toPeriod);
				
				if((lbFromContributionPeriod.getValue().after(fromPeriod)||lbFromContributionPeriod.getValue().equals(fromPeriod))&&(lbFromContributionPeriod.getValue().before(toPeriod)||lbFromContributionPeriod.getValue().equals(toPeriod))){
					showDialogMessage("From period is overlapping.");
					return false;
				}
				
				if((lbToContributionPeriod.getValue().after(fromPeriod)||lbToContributionPeriod.getValue().equals(fromPeriod))&&(lbToContributionPeriod.getValue().before(toPeriod)||lbToContributionPeriod.getValue().equals(toPeriod))){
					showDialogMessage("To period is overlapping.");
					return false;
				}
				
				if(lbFromContributionPeriod.getValue().before(fromPeriod)&&lbToContributionPeriod.getValue().after(toPeriod)){
					showDialogMessage("Overlapping existing period.");
					return false;
				}
			}
		}
		
		return true;
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		
		System.out.println("INSIDE SET ENABLE METHOD   ");
		tbEsicId.setEnabled(false);
		otEarningCompTbl.setEnable(state);
		contributionPeriodTbl.setEnable(state);

	}

	@Override
	public void clear() {
		super.clear();
		cbStatus.setValue(false);
		cbStatus.setValue(true);
		tbEsicId.setEnabled(false);
		otEarningCompTbl.connectToLocal();
		contributionPeriodTbl.connectToLocal();
	}
	
	public String getContributionPeriod(DateBox dbDateBox) {
		DateTimeFormat format = DateTimeFormat.getFormat("MMM");
		if (dbDateBox.getValue() == null) {
			return null;
		} else {
			return format.format(dbDateBox.getValue());
		}
	}


}
