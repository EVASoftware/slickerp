package com.slicktechnologies.client.views.humanresource.esic;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.Esic;

public class EsicPresenterTable extends SuperTable<Esic> {
	TextColumn<Esic> getCountColumn;
	TextColumn<Esic> getNameColumn;
	TextColumn<Esic> getEmployerContributionColumn;
	TextColumn<Esic> getEmployeeContributionColumn;
	TextColumn<Esic> getStatusColumn;
	  
	public EsicPresenterTable() {
		super();
	}
	  
	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetName();
		addColumngetEmployeeContribution();
		addColumngetEmployerContribution();
		addColumngetStatus();
	}

	private void addColumngetEmployeeContribution() {
		// TODO Auto-generated method stub
		getEmployeeContributionColumn = new TextColumn<Esic>() {
			@Override
			public String getValue(Esic object) {
				return object.getEmployeeContribution() + "";
			}
		};
		table.addColumn(getEmployeeContributionColumn, "Employee Contribution");
		getEmployeeContributionColumn.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<Esic>() {
			@Override
			public Object getKey(Esic item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	@Override
	public void addFieldUpdater() {
	}
	
	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetName();
		addSortingEmployerContributionCol();
		addSortingEmployeeContributionCol();
		addSortingStatus();
	}

	

	protected void addSortinggetCount() {
		List<Esic> list = getDataprovider().getList();
		columnSort = new ListHandler<Esic>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Esic>() {
			@Override
			public int compare(Esic e1, Esic e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<Esic>() {
			@Override
			public String getValue(Esic object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		getCountColumn.setSortable(true);
	}

	protected void addSortinggetName() {
		List<Esic> list = getDataprovider().getList();
		columnSort = new ListHandler<Esic>(list);
		columnSort.setComparator(getNameColumn, new Comparator<Esic>() {
			@Override
			public int compare(Esic e1, Esic e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEsicName() != null && e2.getEsicName() != null) {
						return e1.getEsicName().compareTo(e2.getEsicName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortingEmployerContributionCol() {
		List<Esic> list = getDataprovider().getList();
		columnSort = new ListHandler<Esic>(list);
		columnSort.setComparator(getEmployerContributionColumn,new Comparator<Esic>() {
			
			@Override
			public int compare(Esic e1, Esic e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployerContribution() == e2.getEmployerContribution()) {
						return 0;
					}
					if (e1.getEmployerContribution() > e2.getEmployerContribution()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
			
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingEmployeeContributionCol() {
		List<Esic> list = getDataprovider().getList();
		columnSort = new ListHandler<Esic>(list);
		columnSort.setComparator(getEmployeeContributionColumn,new Comparator<Esic>() {
			
			@Override
			public int compare(Esic e1, Esic e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployeeContribution() == e2.getEmployeeContribution()) {
						return 0;
					}
					if (e1.getEmployeeContribution() > e2.getEmployeeContribution()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
			
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingStatus() {
		List<Esic> list = getDataprovider().getList();
		columnSort = new ListHandler<Esic>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Esic>() {
			@Override
			public int compare(Esic e1, Esic e2) {
				if (e1 != null && e2 != null) {
					if (e1.isStatus() == true)
						return 1;
					else
						return -1;

				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetName() {
		getNameColumn = new TextColumn<Esic>() {
			@Override
			public String getValue(Esic object) {
				return object.getEsicName() + "";
			}
		};
		table.addColumn(getNameColumn, "Name");
		getNameColumn.setSortable(true);
	}

	

	protected void addColumngetEmployerContribution() {
		getEmployerContributionColumn = new TextColumn<Esic>() {
			@Override
			public String getValue(Esic object) {
				return object.getEmployerContribution() + "";
			}
		};
		table.addColumn(getEmployerContributionColumn, "Employer Contribution");
		getEmployerContributionColumn.setSortable(true);
	}
	

	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<Esic>() {
			@Override
			public String getValue(Esic object) {
				if (object.isStatus() == true)
					return "Active";
				else
					return "In Active";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
	}

}
