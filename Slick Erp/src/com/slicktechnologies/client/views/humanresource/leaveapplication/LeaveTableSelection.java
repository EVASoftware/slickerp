package com.slicktechnologies.client.views.humanresource.leaveapplication;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;

/**
 * This class applies selection model on Super Table.
 * Since it also implements {@link SelectionChangeEvent.Handler} so we need not apply
 * Handler from outside.
 * On click of table row any form get filled , The functionality comes from this class.
 * @author Kamala
 * @param <T>
 */

public class LeaveTableSelection<T> extends NoSelectionModel<LeaveApplication> implements  SelectionChangeEvent.Handler
{
  protected LeaveApplicationForm mv;
	
	/**
	 * Zero args constructor
	 */
   public LeaveTableSelection()
	{
		super();
	}
   /**
    * 
    * @param mv Ui screen which will get filled on row click.
    * @param keyProvider Key Provider object
    */
	
	public LeaveTableSelection(LeaveApplicationForm mv,ProvidesKey<LeaveApplication>keyProvider)
	{
		super(keyProvider);
		this.mv=mv;
		addSelectionChangeHandler(this);
	}
	
	/**
	 * Crucial method provides functionality of view filling on row click.
	 * To Do: Need to exactly decide weather click is on pop up table or in normal table.
	 * @author Kamala
	 * 
	 */
	@Override
	public void onSelectionChange(SelectionChangeEvent event) 
	{	
		// Get the clicked object
		LeaveApplication obj=LeaveTableSelection.this.getLastSelectedObject();
		//Type cast in super model(if the obj is not type of supermodel functionality will break)
		
		LeaveApplication model=(LeaveApplication) obj;
        if(mv==null)
        	return;
        mv.getPresenter().setModel(model);
        //Clears the screen
		mv.clear();
		
		if(UserConfiguration.getInfo().getEmpCount()==obj.getEmpid())
		{
		  
		   mv.rbself.setValue(true);
		   
		}
		else
		{
			
			mv.eic.setEmployeeId(obj.getEmpid());
			mv.eic.setEmployeeName(obj.getEmployeeName());
			mv.eic.setCellNumber(obj.getEmpCellNo());
			mv.eic.setBranch(obj.getBranch());
			mv.eic.setDepartment(obj.getDepartment());
			mv.eic.setEmployeeType(obj.getEmployeeType());
			mv.eic.setEmpDesignation(obj.getEmployeedDesignation());
			mv.eic.setEmpRole(obj.getEmployeeRole());
			mv.rbothers.setValue(true);
			
			
			
		}
		
		
		mv.setToViewState();
		
		//If row is click is in pop up we need to hide it
		if(mv.getSearchpopupscreen()!=null)
		{
			System.out.println("MMMMMMMMMMMMMMMMMMMMMMMM"+mv.getSearchpopupscreen());
			mv.getSearchpopupscreen().hidePopUp();	 
			
		}
		
		setEmployeeLeaveApplicationHistoryTable();
		setEmployeeLeaveBalanceTableAndDropDown(obj);
		
	}
	
	//Fill all the data as per row click
	
	
	
	
	private void setEmployeeLeaveBalanceTableAndDropDown(final LeaveApplication view)
	{
GenricServiceAsync async=GWT.create(GenricService.class);
		
		if(view!=null)
		{
		   
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(new LeaveBalance());
			Filter filter=new Filter();
			filter.setQuerryString("empInfo.empCount");
			filter.setIntValue(view.getEmpid());
			querry.setQuerryObject(new LeaveBalance());
			querry.getFilters().add(filter);
			mv.showWaitSymbol();
			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					LeaveBalance balance = null;
					for(SuperModel model :result)
					{
					   balance=(LeaveBalance) model;
					   System.out.println("Size of Model +"+result.size());
					   if(balance.getValidtill().before(new Date()))
					   {
						   continue;
					   }
					}
					LeaveApplicationPresenter presenter=(LeaveApplicationPresenter) mv.getPresenter();
					presenter.leaveBalance=balance;
					
					ArrayList<AllocatedLeaves>leaveAlloc=balance.getAllocatedLeaves();
					ArrayList<AllocatedLeaves>leavetype=new ArrayList<AllocatedLeaves>();
					for(AllocatedLeaves all:leaveAlloc)
					{
						
						     leavetype.add(all);
					}
					mv.leaveInfo.connectToLocal();
					mv.leaveInfo.setValue(leaveAlloc);
					mv.olbLeaveType.removeAllItems();
					mv.olbLeaveType.setListItems(leavetype);
					mv.updateView(view);
					mv.hideWaitSymbol();
				   
					
				  
				}
				
				@Override
				public void onFailure(Throwable caught) {
					mv.hideWaitSymbol();
					
				}
			});
	}}
	
	
	private void setEmployeeLeaveApplicationHistoryTable()
	{
		try
		 {
			int id= Integer.parseInt(mv.eic.getId().getText().trim());
			mv.getSupertable().connectToLocal();
			Filter filter=new Filter();
			filter.setQuerryString("empid");
			filter.setIntValue(id);
			MyQuerry quer=new MyQuerry();
			quer.getFilters().add(filter);
			quer.setQuerryObject(new LeaveApplication());
			mv.retriveTable(quer);
			
			
			
			
			
			
		 }
		 catch(Exception e)
		 {
			 
		 }
	}
   
}
