package com.slicktechnologies.client.views.humanresource.leaveapplication;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;

import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;

public class LeaveApplicationTable extends SuperTable<LeaveApplication> {
	TextColumn<LeaveApplication> getCountColumn;
	TextColumn<LeaveApplication> getLeaveTypeColumn;
	TextColumn<LeaveApplication> getFromDateColumn;
	TextColumn<LeaveApplication> getToDateColumn;
	TextColumn<LeaveApplication> getDaysColumn;
	TextColumn<LeaveApplication> getEmployeeIdColumn;
	TextColumn<LeaveApplication> getEmployeeNameColumn;
	TextColumn<LeaveApplication> getEmployeeCellColumn;
	TextColumn<LeaveApplication> getEmployeeBranchColumn;
	TextColumn<LeaveApplication> getEmployeeDepartmentColumn;
	TextColumn<LeaveApplication> getEmployeeTypeColumn;
	TextColumn<LeaveApplication> getEmployeeDesignationColumn;
	TextColumn<LeaveApplication> getEmployeeRoleColumn;
	TextColumn<LeaveApplication> getApproverNameColumn;
	TextColumn<LeaveApplication> getStatusColumn;

	public LeaveApplicationTable() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetLeaveType();
		addColumngetFromDate();
		addColumngetToDate();
		addColumngetDays();
		addColumngetEmployeeId();
		addColumngetEmployeeName();
		addColumngetEmployeeCell();
		addColumngetEmployeeBranch();
		addColumngetEmployeeDepartment();
		addColumngetEmployeeType();
		addColumngetEmployeeDesignation();
		addColumngetEmployeeRole();
		addColumngetApproverName();
		addColumngetStatus();

	}

	private void addColumngetEmployeeRole() {
		getEmployeeRoleColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return object.getEmployeeRole();
			}
		};
		table.addColumn(getEmployeeRoleColumn, "Role");
		getEmployeeRoleColumn.setSortable(true);
		table.setColumnWidth(getEmployeeRoleColumn, 100, Unit.PX);

	}

	private void addColumngetEmployeeType() {
		getEmployeeTypeColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return object.getEmployeeType() + "";
			}
		};
		table.addColumn(getEmployeeTypeColumn, "Type");
		getEmployeeTypeColumn.setSortable(true);
		table.setColumnWidth(getEmployeeTypeColumn, 100, Unit.PX);

	}

	private void addColumngetEmployeeBranch() {
		getEmployeeBranchColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return object.getBranch() + "";
			}
		};
		table.addColumn(getEmployeeBranchColumn, "Branch");
		getEmployeeBranchColumn.setSortable(true);
		table.setColumnWidth(getEmployeeBranchColumn, 100, Unit.PX);

	}

	private void addColumngetEmployeeCell() {
		getEmployeeCellColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return object.getEmpCellNo() + "";
			}
		};
		table.addColumn(getEmployeeCellColumn, "Cell");
		getEmployeeCellColumn.setSortable(true);
		table.setColumnWidth(getEmployeeCellColumn, 110, Unit.PX);

	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<LeaveApplication>() {
			@Override
			public Object getKey(LeaveApplication item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	public void addColumnSorting() {
		addSortinggetEmployeeId();
		addSortinggetEmployeeName();
		addSortingEmployeeCell();
		addSortinggetEmployeeBranch();
		addSortinggetEmployeeDepartment();
		addSortingEmployeeType();
		addSortinggetEmployeeDesignation();
		addSdortingEmployeeRole();
		addSortinggetLeaveType();
		addSortinggetFromDate();
		addSortinggetToDate();
		addSortinggetDays();
		addSortinggetApproverName();
		addSortinggetCount();
		addSortinggetStatus();

	}

	private void addSortingEmployeeCell() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getEmployeeCellColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmpCellNo() == e2.getEmpCellNo()) {
								return 0;
							}
							if (e1.getEmpCellNo() > e2.getEmpCellNo()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	private void addSdortingEmployeeRole() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getEmployeeRoleColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmployeeRole() != null
									&& e2.getEmployeeRole() != null) {
								return e1.getEmployeeRole().compareTo(
										e2.getEmployeeRole());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	private void addSortingEmployeeType() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getEmployeeTypeColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getBranch() != null
									&& e2.getBranch() != null) {
								return e1.getEmployeeType().compareTo(
										e2.getEmployeeType());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	private void addSortinggetEmployeeBranch() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getEmployeeBranchColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getBranch() != null
									&& e2.getBranch() != null) {
								return e1.getBranch().compareTo(e2.getBranch());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	// ******************Count***********************************//

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		table.setColumnWidth(getCountColumn, 100, Unit.PX);
		getCountColumn.setSortable(true);
		table.addColumnSortHandler(columnSort);
	}

	/**************************** Employee Id ***************************************/

	protected void addSortinggetEmployeeId() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getEmployeeIdColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmpid() == e2.getEmpid()) {
								return 0;
							}
							if (e1.getEmpid() > e2.getEmpid()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetEmployeeId() {
		getEmployeeIdColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return object.getEmpid() + "";
			}
		};
		table.addColumn(getEmployeeIdColumn, "Employee Id");
		getEmployeeIdColumn.setSortable(true);
		table.setColumnWidth(getEmployeeIdColumn, 100, Unit.PX);
	}

	/*************************** Employee Name *********************************/

	protected void addSortinggetEmployeeName() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getEmployeeNameColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmployeeName() != null
									&& e2.getEmployeeName() != null) {
								return e1.getEmployeeName().compareTo(
										e2.getEmployeeName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetEmployeeName() {
		getEmployeeNameColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return object.getEmployeeName() + "";
			}
		};
		table.addColumn(getEmployeeNameColumn, "Name");
		getEmployeeNameColumn.setSortable(true);
		table.setColumnWidth(getEmployeeNameColumn, 76, Unit.PX);
	}

	/***************************** Employee Designation ************************************/
	protected void addSortinggetEmployeeDesignation() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getEmployeeDesignationColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmployeedDesignation() != null
									&& e2.getEmployeedDesignation() != null) {
								return e1.getEmployeedDesignation().compareTo(
										e2.getEmployeedDesignation());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetEmployeeDesignation() {
		getEmployeeDesignationColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return object.getEmployeedDesignation() + "";
			}
		};
		table.addColumn(getEmployeeDesignationColumn, "Designation");
		getEmployeeDesignationColumn.setSortable(true);
		table.setColumnWidth(getEmployeeDesignationColumn, 110, Unit.PX);
	}

	/********************************** Employee Department *******************************/
	protected void addSortinggetEmployeeDepartment() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getEmployeeDepartmentColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getDepartment() != null
									&& e2.getDepartment() != null) {
								return e1.getDepartment().compareTo(
										e2.getDepartment());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetEmployeeDepartment() {
		getEmployeeDepartmentColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return object.getDepartment() + "";
			}
		};
		table.addColumn(getEmployeeDepartmentColumn, "Department");
		getEmployeeDepartmentColumn.setSortable(true);
		table.setColumnWidth(getEmployeeDepartmentColumn, 120, Unit.PX);
	}

	/******************************* Leave Type *************************************/

	protected void addSortinggetLeaveType() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getLeaveTypeColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getLeaveType() != null
									&& e2.getLeaveType() != null) {
								return e1.getLeaveType().compareTo(
										e2.getLeaveType());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetLeaveType() {
		getLeaveTypeColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return object.getLeaveType() + "";
			}
		};
		table.addColumn(getLeaveTypeColumn, "Leave Type");
		getLeaveTypeColumn.setSortable(true);
		table.setColumnWidth(getLeaveTypeColumn, 100, Unit.PX);
	}

	/**************************** From Date ***********************************/

	protected void addSortinggetFromDate() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getFromDateColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getFromdate() != null
									&& e2.getFromdate() != null) {
								return e1.getFromdate().compareTo(
										e2.getFromdate());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetFromDate() {
		getFromDateColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return AppUtility.parseDate(object.getFromdate());
			}
		};
		table.addColumn(getFromDateColumn, "From Date");
		getFromDateColumn.setSortable(true);
		table.setColumnWidth(getFromDateColumn, 100, Unit.PX);
	}

	/******************************** To Date ******************************************/

	protected void addSortinggetToDate() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getToDateColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getTodate() != null
									&& e2.getTodate() != null) {
								return e1.getTodate().compareTo(e2.getTodate());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetToDate() {
		getToDateColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return AppUtility.parseDate(object.getTodate()) + "";
			}
		};
		table.addColumn(getToDateColumn, "To Date");
		getToDateColumn.setSortable(true);
		table.setColumnWidth(getToDateColumn, 100, Unit.PX);
	}

	/***************************** No Of Days ************************************/

	protected void addSortinggetDays() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getDaysColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getNoOfDays() != null
									&& e2.getNoOfDays() != null) {
								return e1.getNoOfDays().compareTo(
										e2.getNoOfDays());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetDays() {
		getDaysColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return object.getNoOfDays() + "";
			}
		};
		table.addColumn(getDaysColumn, "Days");
		getDaysColumn.setSortable(true);
		table.setColumnWidth(getDaysColumn, 60, Unit.PX);
	}

	/************************************ Status ************************************/

	protected void addSortinggetStatus() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getStatusColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getStatus() != null
									&& e2.getStatus() != null) {
								return e1.getStatus().compareTo(e2.getStatus());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return object.getStatus() + "";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
		table.setColumnWidth(getStatusColumn, 100, Unit.PX);
	}

	/********************************** Approver Name *************************************/

	protected void addSortinggetApproverName() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getApproverNameColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getApproverName() != null
									&& e2.getApproverName() != null) {
								return e1.getApproverName().compareTo(
										e2.getApproverName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetApproverName() {
		getApproverNameColumn = new TextColumn<LeaveApplication>() {
			@Override
			public String getValue(LeaveApplication object) {
				return object.getApproverName() + "";
			}
		};
		table.addColumn(getApproverNameColumn, "Approver");
		getApproverNameColumn.setSortable(true);
		table.setColumnWidth(getApproverNameColumn, 150, Unit.PX);
	}

	@Override
	public void addFieldUpdater() {

	}

	protected void addSortinggetCount() {
		List<LeaveApplication> list = getDataprovider().getList();
		columnSort = new ListHandler<LeaveApplication>(list);
		columnSort.setComparator(getCountColumn,
				new Comparator<LeaveApplication>() {
					@Override
					public int compare(LeaveApplication e1, LeaveApplication e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCount() > e2.getCount())
								return 1;
							else if (e1.getCount() < e2.getCount())
								return -1;
							else
								return 0;

						} else
							return -1;

					};
				});
		table.addColumnSortHandler(columnSort);
	}

}
