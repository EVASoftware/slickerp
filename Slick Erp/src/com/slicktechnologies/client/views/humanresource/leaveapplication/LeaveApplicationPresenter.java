package com.slicktechnologies.client.views.humanresource.leaveapplication;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenTablePresenter;
import com.slicktechnologies.client.approvalutility.ApprovableFormTableScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
//import com.slicktechnologies.client.services.MaxAllowedLeaveService;
//import com.slicktechnologies.client.services.MaxAllowedLeaveServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.calendar.CalendarPresenter;


public class LeaveApplicationPresenter extends
		ApprovableFormScreenTablePresenter<LeaveApplication> implements
		SelectionHandler<Suggestion>, ChangeHandler, ValueChangeHandler<Date> {

	LeaveApplicationForm form;

	CsvServiceAsync csvservice = GWT.create(CsvService.class);

	final GenricServiceAsync genricservice = GWT.create(GenricService.class);
//	MaxAllowedLeaveServiceAsync maxleavser = GWT.create(MaxAllowedLeaveService.class);

	LeaveBalance leaveBalance;
	
	static boolean timeReportFlag=false;

	public static ArrayList<LeaveApplication> appliedLeaveAppl;

	public LeaveApplicationPresenter(ApprovableFormTableScreen<LeaveApplication> view,LeaveApplication model) {
		super(view, model);
		form = (LeaveApplicationForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getLeaveApplicationQuery());
		form.setPresenter(this);
		
		form.olbLeaveType.addChangeHandler(this);
		form.dbFromDate.addValueChangeHandler(this);
		form.dbToDate.addValueChangeHandler(this);
		/********************** LA001 ******************************/
		EmployeeInfo info = UserConfiguration.getInfo();
		form.eic.setValue(info);
		
		/****************************************************/
		form.eic.addSelectionHandler(this);

		reactOnDateChangeEvent();
		reactOnLeaveTypeChangeEvents();
		setLeaveBalanceInformation(info,false,null);
		getAppliedLeaveApplication(info.getEmpCount(),0);
	}

	
	/***
	 * 
	 * @param info
	 * this method gets all the previously applied leaves of selected employee
	 * and stores in leave list which is used to validate the newly applying leave date 
	 */
	public void getAppliedLeaveApplication(int emplId,final int appCount) {
		
		System.out.println("Leave Application For Employee "+ emplId);

			System.out.println("Info not null");
			List<String> statusList=new ArrayList<String>();
			statusList.add(LeaveApplication.CREATED);
			statusList.add(LeaveApplication.REQUESTED);
			statusList.add(LeaveApplication.APPROVED);
			
			Vector<Filter>filterVec =new Vector<Filter>();
			Filter filter=null;
			MyQuerry querry = new MyQuerry();
			
			filter = new Filter();
			filter.setQuerryString("empid");
			filter.setIntValue(emplId);
			filterVec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("status IN");
			filter.setList(statusList);
			filterVec.add(filter);
			
			querry.setFilters(filterVec);
			querry.setQuerryObject(new LeaveApplication());
			genricservice.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
//					form.hideWaitSymbol();
					appliedLeaveAppl=new ArrayList<LeaveApplication>();
					System.out.println("Leave result size : "+result.size());
					if (result.size() == 0) {
						System.out.println("result zero list size ........... "+appliedLeaveAppl.size());
						return;
					}else{
						for(SuperModel model:result){
							LeaveApplication levApp  = (LeaveApplication) model;
							if(levApp.getCount()!=appCount){
								appliedLeaveAppl.add(levApp);
							}
						}
						System.out.println("Applied leave list size ........... "+appliedLeaveAppl.size());
					}
				}
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("Failure!!!!!!!!!!1");
				}
			});
	}

	/***************************************Query  ***************************************************/	

	/**
	 * Gets the leave application query.
	 *
	 * @return the leave application query
	 */
	public MyQuerry getLeaveApplicationQuery() {
		int empId=UserConfiguration.getInfo().getEmpCount();
		Filter namefilter = new Filter();
		namefilter.setIntValue(empId);
		namefilter.setQuerryString("empid");
		Vector<Filter> vecfilter = new Vector<Filter>();
		vecfilter.add(namefilter);
		MyQuerry quer = new MyQuerry(vecfilter, new LeaveApplication());
		return quer;
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();

		String text = label.getText().trim();

		if (text.equals("New")) {
			form.setToNewState();
			initalize();
		}
		LeaveApplication appl = (LeaveApplication) getModel();
		if (text.equals(ManageApprovals.APPROVALREQUEST)) {
			if(validateBalanceLeave()){
				int noRequestedDay = form.ibNoOfDays.getValue();
				appl.setStatus(LeaveApplication.REQUESTED);
				reactToLeaveRequest(noRequestedDay);
				/**
				 * Date :04-05-2018 BY ANIL
				 * Setting employee id and employee name in approval request
				 */
				
				form.getManageapproval().setBpId(form.eic.getEmployeeId()+"");
				form.getManageapproval().setBpName(form.eic.getEmployeeName());
				/**
				 * End
				 */
				
				form.getManageapproval().reactToRequestForApproval();
			}else{
				form.showDialogMessage("Leave applied days is exceeding leave balance !");
			}
			
		}

		if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST)) {
			int noRequestedDay = form.ibNoOfDays.getValue();
			appl.setStatus(LeaveApplication.CREATED);
			reactToLeaveRequest(-noRequestedDay);
			form.getManageapproval().reactToApprovalRequestCancel();
		}

	}
	
	public boolean validateBalanceLeave(){
		double balanceLeave=form.olbLeaveType.getSelectedItem().getBalance();
		int noRequestedDay = form.ibNoOfDays.getValue();
		System.out.println("BALANCE LEAVE :: "+balanceLeave);
		System.out.println("NO OF DAYS :: "+noRequestedDay);
		if(balanceLeave<noRequestedDay){
			return false;
		}
		return true;
	}
	
	@Override
	public void reactOnPrint() {

	}

	@Override
	public void reactOnDownload() {
		ArrayList<LeaveApplication> leaveAppArray = new ArrayList<LeaveApplication>();
		List<LeaveApplication> list = (List<LeaveApplication>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();

		leaveAppArray.addAll(list);
		csvservice.setleaveapplicationlist(leaveAppArray,new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 8;
				Window.open(url, "test", "enabled");
			}
		});
	}

	
	@Override
	public void reactOnEmail() {

	}
	
	@Override
	protected void makeNewModel() {
		model = new LeaveApplication();
	}
	
	public void setModel(LeaveApplication entity) {
		model = entity;
	}

	/**
	 * Initalize.
	 * 
	 * @return
	 */
	public static LeaveApplicationForm initalize() {
		LeaveFormTable gentableScreen = new LeaveFormTable();

		LeaveApplicationForm form = new LeaveApplicationForm(gentableScreen,ApprovableFormTableScreen.LOWER_MODE, true);
		LeaveApplicationTable gentableSearch = new LeaveApplicationTable();
		gentableSearch.setView(form);
		gentableSearch.applySelectionModle();
		
		LeavePresenterSearchProxy.staticSuperTable = gentableSearch;
		LeavePresenterSearchProxy searchpopup = new LeavePresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		LeaveApplicationPresenter presenter = new LeaveApplicationPresenter(form, new LeaveApplication());
		AppMemory.getAppMemory().stickPnel(form);
		return form;

	}

	@Override
	public void onChange(ChangeEvent event) {
//		System.out.println("INSIDE CHANGE HANDLER MATHOD !!!!!!");
//		if(event.getSource().equals(form.olbLeaveType)){
//			if(form.olbLeaveType.getValue()!=null&&form.dbFromDate.getValue()!=null&&form.dbToDate.getValue()!=null){
//				if(form.dbFromDate.getValue().equals(form.dbToDate.getValue())||form.dbFromDate.getValue().before(form.dbToDate.getValue())){
//					getMaxLeaveAllowedMonthWise();
//				}
//			}
//		}
		
		
		
	}
	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		System.out.println("INSIDE DATE VALUE HANDLER MATHOD !!!!!!");
		if(event.getSource().equals(form.dbFromDate)){
			if(form.olbLeaveType.getValue()!=null&&form.dbFromDate.getValue()!=null&&form.dbToDate.getValue()!=null){
				if(form.dbFromDate.getValue().equals(form.dbToDate.getValue())||form.dbFromDate.getValue().before(form.dbToDate.getValue())){
//					getMaxLeaveAllowedMonthWise();
					checkTimeReportDetails();
				}
			}
		}
		
		if(event.getSource().equals(form.dbToDate)){
			if(form.olbLeaveType.getValue()!=null&&form.dbFromDate.getValue()!=null&&form.dbToDate.getValue()!=null){
				if(form.dbFromDate.getValue().equals(form.dbToDate.getValue())||form.dbFromDate.getValue().before(form.dbToDate.getValue())){
//					getMaxLeaveAllowedMonthWise();
					checkTimeReportDetails();
				}
			}
		}
		
	}
	
	
	
	
	
	
	private void checkTimeReportDetails() {
		System.out.println("INSIDE CHECK TIME REPORT DETAILS METHOD ");
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MM-dd");
		
		Date fromDate=form.dbFromDate.getValue();
		Date toDate=form.dbToDate.getValue();
		
		System.out.println("FORM DATE : "+fromDate);
		System.out.println("TO DATE : "+toDate);
		
		Date currStDate = CalendarUtil.copyDate(fromDate);
		Date currEdDate = CalendarUtil.copyDate(toDate);
		
		int noDays = CalendarUtil.getDaysBetween(currStDate, currEdDate);
		noDays++;

		HashSet<Integer> hset =new HashSet<Integer>();
		for (int i = 1; i < noDays + 1; i++) {
			Date demo=currStDate;
			format = DateTimeFormat.getFormat("M");
			String month = format.format(demo);
			int intMonth = Integer.parseInt(month);
			hset.add(intMonth);
			CalendarUtil.addDaysToDate(currStDate, 1);
		}
		
		ArrayList<Integer> hset2List =new ArrayList<Integer>();
		hset2List.addAll(hset);
		
		ArrayList<String> monthList=new ArrayList<String>();
		
		for(int i=0;i<hset2List.size();i++){
			String month=getMonthName(hset2List.get(i));
			monthList.add(month);
		}
		
		String month1="";
		String month2="";
		
		if(hset2List.size()==1){
			month1=getMonthName(hset2List.get(0));
		}else{
			month1=getMonthName(hset2List.get(0));
			month2=getMonthName(hset2List.get(1));
		}
		
		
		boolean isSameDate=false;
		
		Date wkSrtDtFrm = calculatePeriodStartDate(fromDate);
		Date wkEndDtFrm = calculatePeriodEndDate(fromDate);
		
		Date wkSrtDtTo = calculatePeriodStartDate(toDate);
		Date wkEndDtTo = calculatePeriodEndDate(toDate);
		
		if(wkSrtDtFrm.equals(wkSrtDtTo)&&wkEndDtFrm.equals(wkEndDtTo)){
			isSameDate=true;
		}
		
		int empId=Integer.parseInt(form.eic.getId().getValue());
		if(hset2List.size()==1){
			if(isSameDate==true){
				getTimeReportDetails(empId ,wkSrtDtFrm,month1,wkSrtDtFrm,month1);
			}else{
				getTimeReportDetails(empId ,wkSrtDtFrm,month1,wkSrtDtTo,month1);
			}
			
		}else{
			if(isSameDate==true){
				getTimeReportDetails(empId ,wkSrtDtFrm,month1,wkSrtDtFrm,month2);
			}else{
				getTimeReportDetails(empId ,wkSrtDtFrm,month1,wkSrtDtTo,month2);
			}
		}
		
	}
	
	
	
	public String getMonthName(int monthNo){
		String monthName=null;
		
		switch(monthNo)
		{
		case 1:
			monthName="January";
			break;
		case 2:
			monthName="February";
			break;
		case 3:
			monthName="March";
			break;
		case 4:
			monthName="April";
			break;
		case 5:
			monthName="May";
			break;
		case 6:
			monthName="June";
			break;
		case 7:
			monthName="July";
			break;
		case 8:
			monthName="August";
			break;
		case 9:
			monthName="September";
			break;
		case 10:
			monthName="October";
			break;
		case 11:
			monthName="November";
			break;
		case 12:
			monthName="December";
			break;
			
		}
		
		return monthName;
	}
	
	
	public int getMonthNumber(String monthName) {
	     int monthNo;
	     switch (monthName) {
	         case "January":
	        	 monthNo =0;
	             break;
	         case "February":
	        	 monthNo =1;
	             break;
	         case "March":
	        	 monthNo =2;
	             break;
	         case "April":
	        	 monthNo =3;
	             break;
	         case "May":
	        	 monthNo =4;
	             break;
	         case "June":
	        	 monthNo =5;
	             break;
	         case "July":
	        	 monthNo =6;
	             break;
	         case "August":
	        	 monthNo =7;
	             break;
	         case "September":
	        	 monthNo =8;
	             break;
	         case "October":
	        	 monthNo =9;
	             break;
	         case "November":
	        	 monthNo =10;
	             break;
	         case "December":
	        	 monthNo =11;
	             break;
	             
	         default:
	             throw new IllegalArgumentException("Invalid Month " );
	     }
	     return monthNo;
	}
	
	public static Date calculatePeriodStartDate(Date date) {
		Date currDate = date;
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MM-dd");
		String dates = format.format(date);
		currDate = format.parse(dates);
		format = DateTimeFormat.getFormat("c");
		String dayOfWeek = format.format(date);
		int intdayOfWeek = Integer.parseInt(dayOfWeek);
		CalendarUtil.addDaysToDate(currDate, -intdayOfWeek);
		
		return currDate;
	}
	
	
	public static Date calculatePeriodEndDate(Date date) {

		Date currDate = date;
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MM-dd");
		String dates = format.format(date);
		currDate = format.parse(dates);
		format = DateTimeFormat.getFormat("c");
		String dayOfWeek = format.format(date);
		int intdayOfWeek = Integer.parseInt(dayOfWeek);
		int dif = 6 - intdayOfWeek;
		CalendarUtil.addDaysToDate(currDate, dif);
		return currDate;
	}
	
//	private void getMaxLeaveAllowedMonthWise(){
//		int leaveAppCount=0;
//		if(!form.ibApplicationId.getValue().equals("")){
//			leaveAppCount=Integer.parseInt(form.ibApplicationId.getValue());
//		}
//		form.showWaitSymbol();
//		maxleavser.retrieveMaxAllowed(model.getCompanyId(),leaveAppCount, Integer.parseInt(form.eic.getId().getValue()),form.dbToDate.getValue(), form.olbLeaveType.getSelectedItem(),new AsyncCallback<Double>(){
//
//			@Override
//			public void onFailure(Throwable caught) {
////				form.showDialogMessage("Unexpected Error Occurred");
//				form.hideWaitSymbol();
//			}
//			@Override
//			public void onSuccess(Double result) {
//				
//				form.setMaxLeaveTillMonth(result);
//				
//				System.out.println("MAX ALLOWED LEAVES :::::::::: "+form.getMaxLeaveTillMonth());
//				form.hideWaitSymbol();
//				
//				if(form.ibNoOfDays.getValue()>form.getMaxLeaveTillMonth()){
//					form.showDialogMessage("Max allowed leaves till this month is "+form.getMaxLeaveTillMonth()+" !");
//					form.dbFromDate.setValue(null);
//					form.dbToDate.setValue(null);
//					form.ibNoOfDays.setValue(null);
//				}else{
//					checkTimeReportDetails();
//				}
//			}
//		});
//	}
	/****************************************************************************************************/
	
	/**
	 * Sets the leave Balance table which contains previous leave balance for
	 * that Employee.The method querry the server and get Leave Balance Object
	 * for Employee. The Querry is set such that only active Leave Will Come in
	 * Screen.Again the Method work on Condition that At a given instance of
	 * time only one Active Leave Balance will exist that is its valid till is
	 * greater then current date. Also this method sets the Drop Down Leave
	 * Type.
	 *
	 * @param info Employee for which the table will be set.
	 *            
	 */
	public void setLeaveBalanceInformation(EmployeeInfo info,final boolean isupdateView,final String leaveType) {
		GenricServiceAsync async = GWT.create(GenricService.class);
		System.out.println("Setting Leave Balance For Employee "+ info.getEmpCount()+" : "+info.getFullName());

		if (info != null) {
			Vector<Filter>filterVec =new Vector<Filter>();
			Filter filter=null;
			MyQuerry querry = new MyQuerry();
			
			filter = new Filter();
			filter.setQuerryString("empInfo.empCount");
			filter.setIntValue(info.getEmpCount());
			filterVec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("validtill >");
			filter.setDateValue(new Date());
			filterVec.add(filter);
			querry.getFilters().addAll(filterVec);
			querry.setQuerryObject(new LeaveBalance());
//			form.showWaitSymbol();
			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
//					form.hideWaitSymbol();
					form.leaveInfo.connectToLocal();
					System.out.println("result size : "+result.size());
					if (result.size() == 0) {
//						form.hideWaitSymbol();
						return;
					}else{
						for(SuperModel model:result){
							leaveBalance  = (LeaveBalance) model;
							ArrayList<AllocatedLeaves> leaveAlloc = leaveBalance.getLeaveGroup().getAllocatedLeaves();
							form.leaveInfo.setValue(leaveAlloc);
							form.olbLeaveType.removeAllItems();
							form.olbLeaveType.setListItems(leaveAlloc);
							if(isupdateView==true){
								form.olbLeaveType.setValue(leaveType);
								form.fillInViewMode();
							}
						}
					}
//					form.hideWaitSymbol();
				}
				@Override
				public void onFailure(Throwable caught) {
//					form.hideWaitSymbol();
				}
			});
		}
	}

	/*********************************** REACT ON DATE CHANGE *****************************************/
	
	/**
	 * Sets the handler on From Date. On Date Change Event duration is set.
	 */
	public void reactOnDateChangeEvent() {
		
		form.dbFromDate.addValueChangeHandler(new ValueChangeHandler<Date>() {
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				if (form.dbFromDate.getValue() != null&& form.dbToDate.getValue() != null) {
					Date frDate = CalendarUtil.copyDate(form.dbFromDate.getValue());
					Date toDate = CalendarUtil.copyDate(form.dbToDate.getValue());
					setDuration(frDate, toDate);
				}
			}
		});

		form.dbToDate.addValueChangeHandler(new ValueChangeHandler<Date>() {
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				if (form.dbFromDate.getValue() != null&& form.dbToDate.getValue() != null) {
					Date frDate = CalendarUtil.copyDate(form.dbFromDate.getValue());
					Date toDate = CalendarUtil.copyDate(form.dbToDate.getValue());
					setDuration(frDate, toDate);
				}
			}
		});
		
	}
	
	/**
	 * Set Duration method for calculating the no of days of leave based on
	 * leave type selection and based on weekend.
	 */

	public void setDuration(Date fromDate, Date toDate) {

		int dayduration = CalendarUtil.getDaysBetween(fromDate, toDate) + 1;
		System.out.println("Day Duration is " + dayduration);

		LeaveType type = form.olbLeaveType.getSelectedItem();
		System.out.println("Week End Value is " + type.getCountWeekend());
		if (type != null) {
			if (!type.getCountWeekend()) {
				int weekday = calculateWeakEnd(fromDate, toDate, form.eic.getValue().getLeaveCalendar());
				System.out.println("Value of Week End " + weekday);
				dayduration = dayduration - weekday;
			}
			if (!type.getCountHoliday()) {
				EmployeeInfo info = form.eic.getValue();
				int holiday = calculateCountHoliday(info, fromDate, toDate);
				dayduration = dayduration - holiday;
			}
		}
		form.ibNoOfDays.setValue(dayduration);

	}

	/**
	 * Utility Method to Calculate Week End Duration between two Date. TO dO :
	 * Week end is hard coded.It Should come from Shift and Calendar.
	 * 
	 * @return
	 */
	private int calculateWeakEnd(Date fromDate, Date toDate, Calendar cal) {
		int weekend = 0;
		if (cal == null){
			return 0;
		}
		Date d1fromdate = CalendarUtil.copyDate(fromDate);
		Date d2toate = CalendarUtil.copyDate(toDate);
		
		DateTimeFormat format = DateTimeFormat.getFormat("c");
		// To calculate weekend days between from and to dates
		while (d1fromdate.before(d2toate) || d1fromdate.equals(d2toate)) {
			System.out.println(""+d1fromdate);
			boolean b = CalendarPresenter.isWeekleyOff(cal.getWeeklyoff(),d1fromdate);
			if (b) {
				weekend++;
				System.out.println("Weekend "+weekend);
			}
			CalendarUtil.addDaysToDate(d1fromdate, 1);
		}
		// To check flag count weekend is true or not in leavetype
		return weekend;
	}
	
	/**
	 * Method to Calculate no of Comapny Holidays between two date ranges for a
	 * particular Employee.
	 * 
	 * @param info
	 * @return no of Holiday dates between two date ranges including boundry
	 *         dates.
	 */
	private int calculateCountHoliday(EmployeeInfo info, Date fromDate,Date toDate) {
		if (fromDate == null || toDate == null){
			System.out.println("Date Null");
			return 0;
		}

		int day = 0;
		if (info.getLeaveCalendar() == null){
			System.out.println("Calendar Not Assigned To Employee");
			return 0;
		}
		ArrayList<Holiday> holidays = info.getLeaveCalendar().getHoliday();
		System.out.println("From  Date :: "+fromDate);
		System.out.println("To  Date :: "+toDate);
		for (Holiday holiday : holidays) {
			Date tempdDate = holiday.getHolidaydate();
			System.out.println("Holiday Date :: "+tempdDate);
			if (tempdDate.equals(fromDate))
				day++;
			if (tempdDate.equals(toDate))
				day++;
			if (tempdDate.after(fromDate) && tempdDate.before(toDate))
				day++;
		}
		System.out.println("holiday :: "+day);
		return day;
	}
	
	

	/*********************************** REACT ON LEAVE TYPE CHANGE *****************************************/
	
	/**
	 * Sets the handler on leave type. Sets From Date and To Date Box as Per
	 * Leave Type.
	 */
	public void reactOnLeaveTypeChangeEvents() {
		form.olbLeaveType.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				form.dbFromDate.setValue(null);
				form.dbToDate.setValue(null);
				form.ibNoOfDays.setText("");
				if (form.olbLeaveType.getSelectedItem() != null) {
					LeaveType type = form.olbLeaveType.getSelectedItem();
					if (type != null) {
						form.ibleavemaxdays.setValue(type.getMaxDays());
						form.ibleavemindays.setValue(type.getMinDays());
					}
				} else {
					form.ibleavemaxdays.setText("");
					form.ibleavemindays.setText("");
				}
			}
		});
	}

	
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		EmployeeInfo info = form.eic.getValue();
		if (info != null){
			refreshLeaveApplicationData(info);
		}
	}

	
	/**
	 * Refreshes The Data of Leave Application for passed Employee.
	 * 
	 */
	public void refreshLeaveApplicationData(EmployeeInfo info) {
		try {
			int count = Integer.parseInt(form.eic.getId().getValue());
			info.setEmpCount(count);
			getAppliedLeaveApplication(info.getEmpCount(),0);
			setLeaveBalanceInformation(info,false,null);
			setEmployeeLeaveApplicationHistoryTable(info.getEmpCount());
			
		} catch (Exception e) {

		}
	}
	
	/**
	 * Sets the employee leave application history table that is Lower Table on
	 * User Interface.
	 */
	public void setEmployeeLeaveApplicationHistoryTable(int id) {
		try {
			Filter filter = new Filter();
			filter.setQuerryString("empid");
			filter.setIntValue(id);
			MyQuerry quer = new MyQuerry();
			quer.getFilters().add(filter);
			quer.setQuerryObject(new LeaveApplication());
			form.retriveTable(quer);
		} catch (Exception e) {

		}
	}
	
	

	/*********************************REACT ON Request/Cancel  *******************************************/
	
	/**
	 * Manipulates Leave Requested amount on view by pased number of days.
	 * 
	 * @param days
	 */
	private void reactToLeaveRequest(int days) {
		System.out.println("Inside React to leave..... ");
		LeaveType selLeave = form.olbLeaveType.getSelectedItem();
		System.out.println("Leave Type :: "+selLeave);
		if (selLeave != null) {
			List<AllocatedLeaves> allocatedLeaves = form.leaveInfo.getListDataProvider().getList();
			for (AllocatedLeaves al : allocatedLeaves) {
				if (al.getName().equals(selLeave.getName())) {
					al.setRequested(al.getRequested() + days);
					form.leaveInfo.getListDataProvider().refresh();
					break;
				}
			}
			leaveBalance.setAllocatedLeaves(form.leaveInfo.getValue());
			saveLeaveBalance();
		}
	}
	
	
	/**
	 * Method to Save the Leave Balance is Called when User Request the
	 * Application.
	 */
	private void saveLeaveBalance() {
		GenricServiceAsync async = GWT.create(GenricService.class);
		for (AllocatedLeaves l : leaveBalance.getAllocatedLeaves()) {
			System.out.println("leave type & status--------"+ l.getRequested() + "   " + l.getName());
		}
		async.save(leaveBalance, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				updateUpperTable();
				List<LeaveApplication> applicatios = form.getSupertable().getListDataProvider().getList();
				for (LeaveApplication appl : applicatios) {
					System.out.println("ID OF Application " + appl.getId());
					System.out.println("Status OF Application "+ appl.getStatus());
				}
			}
		});
	}
	
	
	private void updateUpperTable() {
		LeaveApplication appl = (LeaveApplication) getModel();
		for (LeaveApplication temp : form.getSupertable().getListDataProvider().getList()) {
			if (temp.getId().equals(appl.getId())) {
				temp.setStatus(appl.getStatus());
			}
		}
	}
	

	


	public LeaveBalance getLeaveBalance() {
		return leaveBalance;
	}

	public void setLeaveBalance(LeaveBalance leaveBalance) {
		this.leaveBalance = leaveBalance;
	}

/***************************************************************************************************/
	
	private void getTimeReportDetails(final int empId,Date startDateFrom,String month1,final Date startDateTo,final String month2) {
		
		System.out.println("EMP ID --- "+empId);
		System.out.println("START DATE FROM --- "+startDateFrom);
		System.out.println("MONTH 1 --- "+month1);
		System.out.println("START DATE TO --- "+startDateTo);
		System.out.println("MONTH 2 --- "+month2);
		
		
		MyQuerry querry = new MyQuerry();
		Filter filter=null;
		Vector<Filter> temp=new Vector<Filter>();

		filter = new Filter();
		filter.setQuerryString("empid");
		filter.setIntValue(empId);
		temp.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("fromdate");
		filter.setDateValue(startDateFrom);
		temp.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("month");
		filter.setStringValue(month1);
		temp.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setStringValue(LeaveApplication.APPROVED);
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new TimeReport());
		
		genricservice.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					timeReportFlag=true;
					System.out.println("INSIDE TIME REPORT ALREADY EXIST & FLAGE "+timeReportFlag);
					return;
					
				} else{
					System.out.println("INSIDE TIME REPORT NOT EXIST & FLAGE "+timeReportFlag);
					getTimeReportDetails(empId, startDateTo,month2);
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		});

	}
	


	
	private void getTimeReportDetails(int empId,Date startDate,String month) {
		
		System.out.println("EMP ID --- "+empId);
		System.out.println("START DATE  --- "+startDate);
		System.out.println("MONTH  --- "+month);
		
		MyQuerry querry = new MyQuerry();
		Filter filter=null;
		Vector<Filter> temp=new Vector<Filter>();

		filter = new Filter();
		filter.setQuerryString("empid");
		filter.setIntValue(empId);
		temp.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("fromdate");
		filter.setDateValue(startDate);
		temp.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("month");
		filter.setStringValue(month);
		temp.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setStringValue(LeaveApplication.APPROVED);
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new TimeReport());
		
		genricservice.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					timeReportFlag=true;
					System.out.println("INSIDE TIME REPORT ALREADY EXIST & FLAGE "+timeReportFlag);
				}else{
					
					timeReportFlag=false;
					System.out.println("INSIDE TIME REPORT NOT EXIST & FLAGE "+timeReportFlag);
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		});

	}
	
	
	

	
}
