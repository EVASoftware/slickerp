package com.slicktechnologies.client.views.humanresource.leaveapplication;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ProcessLevelBar;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormTableScreen;
import com.slicktechnologies.client.approvalutility.ApprovableScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.history.HistoryPopup;
import com.slicktechnologies.client.views.humanresource.calendar.CalendarPresenter;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeHRProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class LeaveApplicationForm extends ApprovableFormTableScreen<LeaveApplication> implements ClickHandler,ApprovableScreen {

	// Token to add the varialble declarations
	RadioButton rbself, rbothers;
	EmployeeInfoComposite eic;
	LeaveInformationTable leaveInfo;
	
	TextBox ibApplicationId;
	ObjectListBox<AllocatedLeaves> olbLeaveType;
	IntegerBox ibleavemaxdays, ibleavemindays;
	
	ObjectListBox<Employee> olbApproverName;
	DateBox dbFromDate, dbToDate;
	IntegerBox ibNoOfDays;
	TextBox tbStatus;
	
	TextArea tbReason;
	UploadComposite ucUploadTAndCs;
	
	AddressComposite addressOnLeave;
	PhoneNumberBox pnbLandlinePhone, pnbCellPhoneNo1, pnbCellPhoneNo2;
	/**
	 * This Variable stores the leave Count of leave Application in created state. 
	 */
	int leaveInCreatedState=0;
	
	
	public double maxLeaveTillMonth;
	LeaveApplication leaveapplicationobj;

	public LeaveApplicationForm(SuperTable<LeaveApplication> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		this.tbStatus.setText(EmployeeHRProcess.CREATED);
	}

	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget() {

		rbself = new RadioButton("group1");
		rbself.addClickHandler(this);
		rbself.setValue(true);
		
		rbothers = new RadioButton("group1");
		rbothers.addClickHandler(this);
		
		eic = AppUtility.employeeInfoComposite(new EmployeeInfo(), true);
		eic.setEnable(false);
		
		leaveInfo = new LeaveInformationTable();
		
		ibApplicationId = new TextBox();
		ibApplicationId.setEnabled(false);
		
		olbLeaveType = new ObjectListBox<AllocatedLeaves>();
		
		ibleavemindays = new IntegerBox();
		ibleavemindays.setEnabled(false);
		
		ibleavemaxdays = new IntegerBox();
		ibleavemaxdays.setEnabled(false);
		
		olbApproverName = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbApproverName);
		
		dbFromDate = new DateBoxWithYearSelector();
		dbFromDate.getTextBox().setReadOnly(true);
		
		dbToDate = new DateBoxWithYearSelector();
		dbToDate.getTextBox().setReadOnly(true);

		ibNoOfDays = new IntegerBox();
		ibNoOfDays.setEnabled(false);
		
		tbStatus = new TextBox();
		tbStatus.setEnabled(false);
		
		tbReason = new TextArea();
		
		ucUploadTAndCs = new UploadComposite();

		addressOnLeave = new AddressComposite();
		addressOnLeave.setNonManadatory(true);
		
		pnbLandlinePhone = new PhoneNumberBox();
		pnbCellPhoneNo1 = new PhoneNumberBox();
		pnbCellPhoneNo2 = new PhoneNumberBox();
		
	}
	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames = new String[] {ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST, "New" };
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingusertype = fbuilder.setlabel("User Type").widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Self", rbself);
		FormField frbself = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Others", rbothers);
		FormField frbothers = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fEmployeeInformation = fbuilder
				.setlabel("Employee Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", eic);
		FormField femployeeinfo = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingLeaveBalanceInformation = fbuilder
				.setlabel("Leave Balance Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", leaveInfo.getTable());
		FormField fleaveinfo = fbuilder.setMandatory(false).setColSpan(4)
				.setRowSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingLeaveApplicationDetails = fbuilder
				.setlabel("Leave Application Details")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Application ID", ibApplicationId);
		FormField fApplicationId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Leave Type", olbLeaveType);
		FormField folbLeaveType = fbuilder.setMandatory(true)
				.setMandatoryMsg("Leave Type is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Maximum Days", ibleavemaxdays);
		FormField fibleavemaxdays = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Minimum Days", ibleavemindays);
		FormField fibleavemindays = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Approver Name", olbApproverName);
		FormField folbApproverName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Approver Name is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		

		fbuilder = new FormFieldBuilder("* From Date", dbFromDate);
		FormField fdbFromDate = fbuilder.setMandatory(true)
				.setMandatoryMsg("From date is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* To Date", dbToDate);
		FormField fdbToDate = fbuilder.setMandatory(true)
				.setMandatoryMsg("To date is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("No Of Days", ibNoOfDays);
		FormField fibNoOfDays = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status", tbStatus);
		FormField ftbStatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingReasonForLeave = fbuilder
				.setlabel("Reason For Leave").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Reason", tbReason);
		
		FormField ftbReason = fbuilder.setMandatory(true)
				.setMandatoryMsg("Reason is mandatory!").setRowSpan(0)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDocuments = fbuilder.setlabel("Documents")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Upload T&Cs", ucUploadTAndCs);
		FormField fucUploadTAndCs = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAddressComposite = fbuilder
				.setlabel("Contact Information When On Leave")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		fbuilder = new FormFieldBuilder(" Landline Phone", pnbLandlinePhone);
		FormField fpnbLandlinePhone = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder(" Cell Phone No 1", pnbCellPhoneNo1);
		FormField fpnbCellPhoneNo1 = fbuilder.setMandatory(false)
				.setMandatoryMsg("Cell Phone No. 1 is mandatory!")
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cell Phone No 2", pnbCellPhoneNo2);
		FormField fpnbCellPhoneNo2 = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", addressOnLeave);
		FormField fAddressComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(4).build();
		
		// Code///////////////////////////////////////////////////////////////

		FormField[][] formfield = {
				{ fgroupingusertype },
				{ frbself, frbothers },
				{ fEmployeeInformation },
				{ femployeeinfo },
				{ fgroupingLeaveBalanceInformation },
				{ fleaveinfo },
				{ fgroupingLeaveApplicationDetails },
				{ fApplicationId, folbLeaveType, fibleavemindays,fibleavemaxdays },
				{ folbApproverName, fdbFromDate, fdbToDate, fibNoOfDays },
				{ ftbStatus },
				{ fgroupingReasonForLeave },
				{ ftbReason },
				{ fgroupingDocuments },
				{ fucUploadTAndCs },
				{ fgroupingAddressComposite },
				{ fpnbLandlinePhone, fpnbCellPhoneNo1, fpnbCellPhoneNo2 },
				{ fAddressComposite } 
				};
		this.fields = formfield;
	}

	@Override
	public void updateModel(LeaveApplication model) {
		this.ibApplicationId.setValue(model.getCount() + "");
		if (olbLeaveType.getValue() != null)
			model.setLeaveType(olbLeaveType.getValue());
		if (dbFromDate.getValue() != null)
			model.setFromdate(dbFromDate.getValue());
		if (dbToDate.getValue() != null)
			model.setTodate(dbToDate.getValue());
		if (tbReason.getValue() != null)
			model.setLeaveReason(tbReason.getValue());
		if (ucUploadTAndCs.getValue() != null)
			model.setDocument(ucUploadTAndCs.getValue());
		if (ibNoOfDays.getValue() != null)
			model.setNoOfDays(ibNoOfDays.getValue());
		if (tbStatus.getValue() != null)
			model.setStatus(tbStatus.getValue());
		if (olbApproverName.getValue() != null)
			model.setApproverName(olbApproverName.getValue());
		if (addressOnLeave.getValue() != null)
			model.setAddress(addressOnLeave.getValue());
		if (pnbLandlinePhone.getValue() != null)
			model.setLandline(pnbLandlinePhone.getValue());
		if (pnbCellPhoneNo1.getValue() != null)
			model.setCellNo1(pnbCellPhoneNo1.getValue());
		if (pnbCellPhoneNo2.getValue() != null)
			model.setCellNo2(pnbCellPhoneNo2.getValue());

		if (eic != null) {
			model.setEmpid(eic.getEmployeeId());
			model.setEmployeeName(eic.getEmployeeName());
			model.setEmpCellNo(eic.getCellNumber());
			model.setBranch(eic.getBranch());
			model.setDepartment(eic.getDepartment());
			model.setEmployeeType(eic.getEmployeeType());
			model.setEmployeedDesignation(eic.getEmpDesignation());
			model.setEmployeeRole(eic.getEmpRole());
		}
		
		final LeaveApplicationPresenter presenter = (LeaveApplicationPresenter) getPresenter();
		if (leaveInfo.getValue() != null) {
			model.setLeaveBalance(presenter.getLeaveBalance());
		}
		
		/**
		 * to get previous applied leave list and refresh the table data below. 
		 */
		Timer t = new Timer() {
			@Override
			public void run() {
				EmployeeInfo info=eic.getValue();
				presenter.refreshLeaveApplicationData(info);
			}
		};
		t.schedule(3000);
		leaveapplicationobj=model;
		presenter.setModel(model);
	}

	@Override
	public void updateView(LeaveApplication view) {
		
		
		leaveapplicationobj=view;
//		if (view.getLeaveType() != null) {
//			// First Fill the Object List Box from Allocated Leaves.
//			olbLeaveType.removeAllItems();
//			this.leaveInfo.connectToLocal();
//			// Fill the List Box
//			LeaveBalance bal = view.getLeaveBalance();
//			if (bal == null)
//				;
//			else {
//				if (bal.getAllocatedLeaves() != null) {
//					olbLeaveType.setListItems(bal.getAllocatedLeaves());
//					leaveInfo.setValue(bal.getAllocatedLeaves());
//				}
//			}
//			olbLeaveType.setValue(view.getLeaveType());
//		}

		ibApplicationId.setValue(view.getCount() + "");

		if (view.getFromdate() != null)
			dbFromDate.setValue(view.getFromdate());
		if (view.getTodate() != null)
			dbToDate.setValue(view.getTodate());
		if (view.getLeaveReason() != null)
			tbReason.setValue(view.getLeaveReason());
		if (view.getDocument() != null)
			ucUploadTAndCs.setValue(view.getDocument());
		ibNoOfDays.setValue(view.getNoOfDays());
		if (view.getStatus() != null)
			tbStatus.setValue(view.getStatus());
		if (view.getApproverName() != null)
			olbApproverName.setValue(view.getApproverName());
		if (view.getAddress() != null)
			addressOnLeave.setValue(view.getAddress());
		if (view.getLandline() != null)
			pnbLandlinePhone.setValue(view.getLandline());
		if (view.getCellNo1() != null)
			pnbCellPhoneNo1.setValue(view.getCellNo1());
		if (view.getCellNo2() != null)
			pnbCellPhoneNo2.setValue(view.getCellNo2());

		if (eic != null) {

			eic.setEmployeeId(view.getEmpid());
			eic.setEmployeeName(view.getEmployeeName());
			eic.setCellNumber(view.getEmpCellNo());
			eic.setBranch(view.getBranch());
			eic.setDepartment(view.getDepartment());
			eic.setEmployeeType(view.getEmployeeType());
			eic.setEmpDesignation(view.getEmployeedDesignation());
			eic.setEmpRole(view.getEmployeeRole());
			
			EmployeeInfo info =eic.getValue();
			
			LeaveApplicationPresenter presenter = (LeaveApplicationPresenter) getPresenter();
			if(view.getLeaveType()!=null){
				presenter.setLeaveBalanceInformation(info,true,view.getLeaveType());
				
			}
			int count = Integer.parseInt(eic.getId().getValue());
			presenter.getAppliedLeaveApplication(count,view.getCount());
			presenter.setEmployeeLeaveApplicationHistoryTable(count);
			
		}

		if (UserConfiguration.getInfo().getEmpCount() != eic.getEmployeeId()){
			rbothers.setValue(true);
		}
		else{
			rbself.setValue(true);
		}

		// Set the Leave History Table

//		fillInViewMode();
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */

	}

	public void fillInViewMode() {
		if (olbLeaveType.getItemCount() != 0) {
			LeaveType ltype = olbLeaveType.getSelectedItem();
			ibleavemindays.setValue(ltype.getMinDays());
			ibleavemaxdays.setValue(ltype.getMaxDays());
		}
	}
	
	// Hand written code shift in presenter

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")|| text.equals("Search")|| text.equals(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else{
					menus[k].setVisible(false);
				}
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")|| text.equals(AppConstants.NAVIGATION)){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")|| text.equals("Search")|| text.equals(AppConstants.NAVIGATION)){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.LEAVEAPPLICATION,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value.
	 */
	@Override
	public void setCount(int count) {
		this.ibApplicationId.setValue(presenter.getModel().getCount() + "");
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibApplicationId.setEnabled(false);
		tbStatus.setEnabled(false);
		ibNoOfDays.setEnabled(false);
		eic.setEnable(false);
		ibleavemaxdays.setEnabled(false);
		ibleavemindays.setEnabled(false);
		System.out.println("Called SET ENABLED " + rbothers.getValue());
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		setAppHeaderBarAsPerStatus();
		toggleProcesslevelbar();
		
		SuperModel model=new LeaveApplication();
		model=leaveapplicationobj;
		if(HistoryPopup.historyObj!=null){
			System.out.println("History Object is Null .....");
			AppUtility.addDocumentToHistoryTable(HistoryPopup.historyObj.getModuleName(),AppConstants.LEAVEAPPLICATION, leaveapplicationobj.getCount(), leaveapplicationobj.getEmpid(),leaveapplicationobj.getEmployeeName(),leaveapplicationobj.getEmpCellNo(), false, model, null);
			HistoryPopup.historyObj=null;
		}else{
			AppUtility.addDocumentToHistoryTable(LoginPresenter.currentModule,AppConstants.LEAVEAPPLICATION, leaveapplicationobj.getCount(), leaveapplicationobj.getEmpid(),leaveapplicationobj.getEmployeeName(),leaveapplicationobj.getEmpCellNo(), false, model, null);
		}
		
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		setAppHeaderBarAsPerStatus();
		toggleProcesslevelbar();
		this.processLevelBar.setVisibleFalse(false);

	}


	@Override
	public void clear() {

		EmployeeInfo empinfo = eic.getValue();
		boolean selected = rbself.getValue();
		super.clear();
		if (selected == true){
			rbself.setValue(true);
		}
		else{
			rbothers.setValue(true);
		}
		this.tbStatus.setText(LeaveApplication.CREATED);
		if (empinfo != null){
			eic.setValue(empinfo);
		}

	}

	/*************************** On Click method ***********************************************/
	
	/**
	 * 
	 * Event handling methods
	 */

	@Override
	public void onClick(ClickEvent event) {
		if (rbself.getValue() == true) {
			setToNewState();
			eic.setEnable(false);
			eic.setValue(UserConfiguration.getInfo());
			LeaveApplicationPresenter leavepresenter = (LeaveApplicationPresenter) presenter;
			leavepresenter.refreshLeaveApplicationData(eic.getValue());
			rbself.setValue(true);
		}
		if (rbothers.getValue() == true) {
			setToNewState();
			rbothers.setValue(true);
			LeaveApplicationPresenter presenter = (LeaveApplicationPresenter) this.presenter;
			presenter.makeNewModel();
			eic.clear();
			eic.setEnable(true);
			this.leaveInfo.clear();
			this.olbLeaveType.removeAllItems();
			this.getSupertable().clear();
		}
	}

	/*********************************** Validate Method ***********************************************/
	
	/**
	 * 
	 * validate method for validations
	 */

	@Override
	public boolean validate() {
		boolean superRes = super.validate();
		if (superRes == false){
			return false;
		}
		
		Date d1 = dbFromDate.getValue();
		Date d2 = dbToDate.getValue();

		if (d1 != null && d2 != null) {
			if (d1.before(d2) || d1.equals(d2)) {
				;
			} else {
				dbFromDate.setValue(null);
				dbToDate.setValue(null);
				ibNoOfDays.setValue(null);
				this.showDialogMessage("From date should be less than To date!");
				return false;
			}
		}

		EmployeeInfo info=eic.getValue();
		
		
		if(dbFromDate.getValue().before(info.getLeaveCalendar().getCalStartDate())){
			this.showDialogMessage("From date should be less tha calendar start date!");
			return false;
		}
		
		if(dbToDate.getValue().after(info.getLeaveCalendar().getCalEndDate())){
			this.showDialogMessage("To date should be less tha calendar end date!");
			return false;
		}
		
		if(isWeeklyOff()){
			this.showDialogMessage("Leave duartion is Weekly Off !");
			return false;
		}
		if(isHoliday()){
			this.showDialogMessage("Leave duartion is Holiday !");
			return false;
		}
		if(!validLeaveAppliedDate()){
			this.showDialogMessage("Leave is already applied for this duration !");
			return false;
		}
		
		int getNoOfDays = 0;
		int leaveMinDays = 0;
		int leaveMaxDays = 0;
		
		double leaveBalance = olbLeaveType.getSelectedItem().getBalance();
		double requested = olbLeaveType.getSelectedItem().getRequested();
		double approved=olbLeaveType.getSelectedItem().getApproved();
		
		if (ibNoOfDays.getValue() != null) {
			getNoOfDays = ibNoOfDays.getValue();
		}
		if (ibNoOfDays.getValue() != null) {
			leaveMinDays = ibleavemindays.getValue();
		}
		if (ibNoOfDays.getValue() != null) {
			leaveMaxDays = ibleavemaxdays.getValue();
		}

		if (getNoOfDays < leaveMinDays) {
			this.showDialogMessage("Leave applied days should exceed minimum days allowed !");
			return false;
		}

//		if (getNoOfDays > leaveMaxDays) {
//			this.showDialogMessage("Leave applied days is exceeding maximum days allowed !");
//			return false;
//		}

		if (leaveBalance < getNoOfDays) {
			this.showDialogMessage("Leave applied days is exceeding leave balance !");
			return false;
		}

		if ((requested +approved+ getNoOfDays+leaveInCreatedState) > leaveBalance) {
			this.showDialogMessage("Sum of Leave Applied and Leave Requested should be less "+ "than Leave Balance !");
			return false;
		}
		
		if(LeaveApplicationPresenter.timeReportFlag==true){
			this.showDialogMessage("Time report for this duration is already approved!");
			return false;
		}

		return true;
	}
	
	public boolean isWeeklyOff(){
		EmployeeInfo info=eic.getValue();
		Date levFrmDt=dbFromDate.getValue();
		Date levToDt=dbToDate.getValue();
		int weekend=0;
		
		int dayduration = CalendarUtil.getDaysBetween(levFrmDt, levToDt) + 1;
//		System.out.println("WeekOff Duration is " + dayduration);
		
		Date d1fromdate = CalendarUtil.copyDate(levFrmDt);
		Date d2toate = CalendarUtil.copyDate(levToDt);
		
		while (d1fromdate.before(d2toate) || d1fromdate.equals(d2toate)) {
//			System.out.println(""+d1fromdate);
			boolean weeklyOff = CalendarPresenter.isWeekleyOff(info.getLeaveCalendar().getWeeklyoff(),d1fromdate);
			if (weeklyOff) {
				weekend++;
//				System.out.println("Weekend V: "+weekend);
			}
			CalendarUtil.addDaysToDate(d1fromdate, 1);
		}
		
		if(dayduration==weekend){
			return true;
		}
		
		return false;
	}

	public boolean isHoliday(){
		EmployeeInfo info=eic.getValue();
		Date levFrmDt=dbFromDate.getValue();
		Date levToDt=dbToDate.getValue();
		int day=0;
		int dayduration = CalendarUtil.getDaysBetween(levFrmDt, levToDt) + 1;
//		System.out.println("HoliDay Duration is " + dayduration);
		
		ArrayList<Holiday> holidays = info.getLeaveCalendar().getHoliday();
		for (Holiday holiday : holidays) {
			Date tempdDate = holiday.getHolidaydate();
//			System.out.println("Holiday Date :: "+tempdDate);
			
			if(levFrmDt.equals(levToDt)){
				if (tempdDate.equals(levFrmDt)){
					day++;
				}
			}else{
				if (tempdDate.equals(levFrmDt)){
					day++;
				}
				if (tempdDate.equals(levToDt)){
					day++;
				}
			}
		}
//		System.out.println("holiday v:: "+day);
		if(dayduration==day){
			return true;
		}
		return false;
	}
	
	
	public boolean validLeaveAppliedDate(){
		System.out.println("Leave Application Size :: "+LeaveApplicationPresenter.appliedLeaveAppl.size());
		
		Date levFrmDt=dbFromDate.getValue();
		Date levToDt=dbToDate.getValue();
		
		if(LeaveApplicationPresenter.appliedLeaveAppl.size()!=0){
			ArrayList<LeaveApplication> levApp=LeaveApplicationPresenter.appliedLeaveAppl;
			System.out.println("Size :: "+levApp.size());
			
			for(int i=0;i<levApp.size();i++){
				Date appliedLevFrmDt=levApp.get(i).getFromdate();
				Date appliedLevToDt=levApp.get(i).getTodate();
				
				if(levFrmDt.equals(appliedLevFrmDt)){
					return false;
				}
				if(levFrmDt.equals(appliedLevToDt)){
					return false;
				}
				if (levFrmDt.after(appliedLevFrmDt) && levFrmDt.before(appliedLevToDt)){
					return false;
				}
				if(levToDt.equals(appliedLevFrmDt)){
					return false;
				}
				if(levToDt.equals(appliedLevToDt)){
					return false;
				}
				if (levToDt.after(appliedLevFrmDt) && levToDt.before(appliedLevToDt)){
					return false;
				}
				
				
				if(levApp.get(i).getStatus().equals(LeaveApplication.CREATED)){
					if(levApp.get(i).getLeaveType().equals(olbLeaveType.getValue())){
						leaveInCreatedState=leaveInCreatedState+levApp.get(i).getNoOfDays();
					}
				}
			}
		}
		
		return true; 
	}
	
	
	
//	public boolean validateLeaveMonthlyWise(){
//		Date frmDate=dbFromDate.getValue();
//		Date toDate=dbToDate.getValue();
//		
//		
//		EmployeeInfo info=eic.getValue();
//		
//		Date calendarStartDate=info.getLeaveCalendar().getCalStartDate();
//		Date calendarEndDate=info.getLeaveCalendar().getCalEndDate();
//		
//		System.out.println("Calender STAR DATE ::: "+calendarStartDate);
//		System.out.println("Calender END DATE ::: "+calendarEndDate);
//		
//		LeaveType leaveTyp=olbLeaveType.getSelectedItem();
//		
//		String leaveName=olbLeaveType.getValue();
//		int maxLeave=ibleavemaxdays.getValue();
//		
//		getMonthBetweenDate(calendarStartDate,calendarEndDate);
//		
//		return true;
//	}
	
	
//	public int getMonthBetweenDate(Date fromDate,Date toDate){
//		int noOfMonth = 0;
//		
//		Date strtDate = fromDate;
//		int strtDtMonth=strtDate.getMonth();
//		System.out.println("START Month Number ----- "+strtDtMonth+"  Year "+strtDate.getYear());
//		
//		Date endDate = toDate;
//		int endDtMonth=endDate.getMonth();
//		System.out.println("END Month Number ----- "+endDtMonth+"  Year "+endDate.getYear());
//		
//		
//		
//		return noOfMonth;
//	}
	
//	private static int monthsBetweenIgnoreDays(LocalDate start, LocalDate end) {
//	    start = start.withDayOfMonth(1);
//	    end = end.withDayOfMonth(1);
//	    return Months.monthsBetween(start, end).getMonths();
//	}
	
	/***************************set App header and toggle bar method ******************************************/
	
	public void setAppHeaderBarAsPerStatus() {

		LeaveApplication model = (LeaveApplication) presenter.getModel();
		String status = model.getStatus();
		boolean bool = status.equals(LeaveApplication.REJECTED)|| status.equals(LeaveApplication.APPROVED)|| status.equals(LeaveApplication.REQUESTED);
		
		if (bool)
		{
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")) {
					menus[k].setVisible(true);
				} else{
					menus[k].setVisible(false);
				}
			}
		}

	}

	public void toggleProcesslevelbar() {
		LeaveApplication model = (LeaveApplication) presenter.getModel();
		String status = model.getStatus();
		boolean bool = status.equals(LeaveApplication.APPROVED)|| status.equals(LeaveApplication.REJECTED);

		ProcessLevelBar bar = getProcessLevelBar();
		InlineLabel[] lbls = bar.btnLabels;
		if (bool) {
			for (InlineLabel temp : lbls) {
				String lbltext = temp.getText();
				if (lbltext.equals(ManageApprovals.APPROVALREQUEST))
					temp.setVisible(false);
				if (lbltext.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					temp.setVisible(false);
			}
		}
		if (status.equals(LeaveApplication.REQUESTED)) {
			for (InlineLabel temp : lbls) {
				String lbltext = temp.getText();
				if (lbltext.equals(ManageApprovals.APPROVALREQUEST))
					temp.setVisible(false);
				if (lbltext.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					temp.setVisible(true);
			}
		}

	}

	@Override
	public TextBox getstatustextbox() {
		return this.tbStatus;
	}

	public double getMaxLeaveTillMonth() {
		return maxLeaveTillMonth;
	}

	public void setMaxLeaveTillMonth(double maxLeaveTillMonth) {
		this.maxLeaveTillMonth = maxLeaveTillMonth;
	}
	
	

}
