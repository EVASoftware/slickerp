package com.slicktechnologies.client.views.humanresource.holidaytype;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import java.util.Comparator;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;
import java.util.List;
import java.util.Date;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.HolidayType;
import com.slicktechnologies.client.views.humanresource.holidaytype.HolidayTypePresenter.HolidayTypePresenterTable;
import com.slicktechnologies.client.utility.Screen;

public class HolidayTypePresenterTableProxy extends HolidayTypePresenterTable {
  TextColumn<HolidayType> getHolidayTypeNameColumn;
  TextColumn<HolidayType> getStatusColumn;
  TextColumn<HolidayType> getCountColumn;
  public Object getVarRef(String varName)
  {
  if(varName.equals("getHolidayTypeNameColumn"))
  return this.getHolidayTypeNameColumn;
  if(varName.equals("getStatusColumn"))
  return this.getStatusColumn;
  if(varName.equals("getCountColumn"))
  return this.getCountColumn;
   return null ;
  }
  public HolidayTypePresenterTableProxy()
  {
  super();
  }
  @Override public void createTable() {
  addColumngetCount();
  addColumngetHolidayTypeName();
  addColumngetStatus();
  }
  @Override
  protected void initializekeyprovider() {
  keyProvider= new ProvidesKey<HolidayType>()
  {
  @Override
  public Object getKey(HolidayType item)
  {
  if(item==null)
  {
  return null;
  }
  else
  return item.getId();
  }
  };
  }
  @Override
  public void setEnable(boolean state)
  {
  }
  @Override
  public void applyStyle()
  {
  }
   public void addColumnSorting(){
  addSortinggetCount();
  addSortinggetHolidayTypeName();
  addSortinggetStatus();
  }
  @Override public void addFieldUpdater() {
  }
  protected void addSortinggetCount()
  {
  List<HolidayType> list=getDataprovider().getList();
  columnSort=new ListHandler<HolidayType>(list);
  columnSort.setComparator(getCountColumn, new Comparator<HolidayType>()
  {
  @Override
  public int compare(HolidayType e1,HolidayType e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCount()== e2.getCount()){
  return 0;}
  if(e1.getCount()> e2.getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCount()
  {
  getCountColumn=new TextColumn<HolidayType>()
  {
  @Override
  public String getValue(HolidayType object)
  {
  if( object.getCount()==-1)
  return "N.A";
  else return object.getCount()+"";
  }
  };
  table.addColumn(getCountColumn,"Id");
  getCountColumn.setSortable(true);
  }
  protected void addSortinggetHolidayTypeName()
  {
  List<HolidayType> list=getDataprovider().getList();
  columnSort=new ListHandler<HolidayType>(list);
  columnSort.setComparator(getHolidayTypeNameColumn, new Comparator<HolidayType>()
  {
  @Override
  public int compare(HolidayType e1,HolidayType e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getHolidayTypeName()!=null && e2.getHolidayTypeName()!=null){
  return e1.getHolidayTypeName().compareTo(e2.getHolidayTypeName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetHolidayTypeName()
  {
  getHolidayTypeNameColumn=new TextColumn<HolidayType>()
  {
  @Override
  public String getValue(HolidayType object)
  {
  return object.getHolidayTypeName()+"";
  }
  };
  table.addColumn(getHolidayTypeNameColumn,"Holiday Type");
  getHolidayTypeNameColumn.setSortable(true);
  }
  protected void addSortinggetStatus()
  {
  List<HolidayType> list=getDataprovider().getList();
  columnSort=new ListHandler<HolidayType>(list);
  columnSort.setComparator(getStatusColumn, new Comparator<HolidayType>()
  {
  @Override
  public int compare(HolidayType e1,HolidayType e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getStatus()== e2.getStatus()){
  return 0;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetStatus()
  {
  getStatusColumn=new TextColumn<HolidayType>()
  {
  @Override
  public String getValue(HolidayType object)
  {
  if( object.getStatus()==true)
  return "Active";
  else 
  return "In Active";
  }
  };
  table.addColumn(getStatusColumn,"Status");
  getStatusColumn.setSortable(true);
  }
}
