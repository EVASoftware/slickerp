package com.slicktechnologies.client.views.humanresource.holidaytype;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.HolidayType;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

// TODO: Auto-generated Javadoc
/**
 * Ui for Holiday Type
 */
public class HolidayTypeForm extends FormTableScreen<HolidayType> implements ClickHandler 
{
	
	/** Text Box corresponding to name of Holiday Type */
	TextBox tbholidaytypename;
	
	/** Text Box corresponding to description  of Holiday Type*/
	TextArea tbholidaytypedesc;
	
	/** Check Box Corresponding to Status of Holiday Type */
	CheckBox cbstatus;
	
	/**
	 * Instantiates a new holiday type form.
	 *
	 * @param table the table
	 * @param mode the mode
	 * @param captionmode the captionmode
	 */
	public HolidayTypeForm  (SuperTable<HolidayType> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		
	}
	
	/**
	 * Initalize widget.
	 */
	private void initalizeWidget()
	{
		
		tbholidaytypename = new TextBox();
		tbholidaytypedesc = new TextArea();
		cbstatus = new CheckBox();
		cbstatus.setValue(true);
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#createScreen()
	 */
	@Override
	public void createScreen() {
		
		//Token to initialize the processlevel menus.
	    initalizeWidget();
		
	    this.processlevelBarNames=new String[]{"New"};
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingHolidayTypeInformation=fbuilder.setlabel("Holiday Type Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("* Holiday Type",tbholidaytypename);
		FormField ftbholidaytypename= fbuilder.setMandatory(true).setMandatoryMsg("Holiday Type is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbstatus);
		FormField fcbstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Description",tbholidaytypedesc);
		FormField ftbtypedesc= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		

		
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		
		
  FormField[][] formfield = {   {fgroupingHolidayTypeInformation},
  {ftbholidaytypename,fcbstatus},
  {ftbtypedesc},
  };

		
        this.fields=formfield;		
   }

	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#updateModel(java.lang.Object)
	 */
	@Override
	public void updateModel(HolidayType model) 
	{
		
	if(tbholidaytypename.getValue()!=null)
	  model.setHolidayTypeName(tbholidaytypename.getValue());
	if(cbstatus.getValue()!=null)
	  model.setStatus(cbstatus.getValue());
	if(tbholidaytypedesc.getValue()!=null)
	  model.setDescription(tbholidaytypedesc.getValue());
	

	presenter.setModel(model);
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#updateView(java.lang.Object)
	 */
	@Override
	public void updateView(HolidayType view) 
	{
	if(view.getHolidayTypeName()!=null)
	  tbholidaytypename.setValue(view.getHolidayTypeName());
	if(view.getDescription()!=null)
	  tbholidaytypedesc.setValue(view.getDescription());
	if(view.getStatus()!=null)
		cbstatus.setValue(view.getStatus());
		
		presenter.setModel(view);
	}
	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#toggleAppHeaderBarMenu()
	 */
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.HOLIDAYTYPE,LoginPresenter.currentModule.trim());
	}
	
	

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#setEnable(boolean)
	 */
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#clear()
	 */
	@Override
	public void clear() {
		super.clear();
		cbstatus.setValue(true);
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}
	
	


}
