package com.slicktechnologies.client.views.humanresource.paidleave;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.humanresource.overtime.OtEarningCompTable;
import com.slicktechnologies.shared.common.humanresourcelayer.PaidLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.OtEarningComponent;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class PaidLeaveForm extends FormTableScreen<PaidLeave> implements ClickHandler{


	TextBox tbPaidLeaveId;
	TextBox tbPaidLeaveName;
	CheckBox cbStatus;
	ListBox lbEarningComponent;
	OtEarningCompTable otEarningCompTbl;
	Button btnAdd;
	
	DoubleBox dbRate;
	
	
	public PaidLeaveForm(SuperTable<PaidLeave> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		cbStatus.setValue(true);
		otEarningCompTbl.connectToLocal();
	}

	private void initalizeWidget() {

		tbPaidLeaveId = new TextBox();
		tbPaidLeaveId.setEnabled(false);
		
		tbPaidLeaveName=new TextBox();
		
		cbStatus = new CheckBox();
		cbStatus = new CheckBox();
		cbStatus.setValue(true);
		
		lbEarningComponent=new ListBox();
		lbEarningComponent.addItem("--SELECT--");
		otEarningCompTbl=new OtEarningCompTable();
		btnAdd=new Button("Add");
		btnAdd.addClickHandler(this);
		
		dbRate=new DoubleBox();
		
	}
	
	
	@Override
	public void createScreen() {
		initalizeWidget();

		this.processlevelBarNames = new String[] { "New" };
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPLInformation = fbuilder.setlabel("Paid Leave Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Paid Leave ID", tbPaidLeaveId);
		FormField ftbPaidLeaveId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Paid Leave Name", tbPaidLeaveName);
		FormField ftbPaidLeaveName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Paid Leave Name is mandatory !")
				.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", cbStatus);
		FormField fcbStatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Earning Component", lbEarningComponent);
		FormField foblEarningComp = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fbtnAdd = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", otEarningCompTbl.getTable());
		FormField fotEarningCompTbl = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("* Rate", dbRate);
		FormField fdbRate = fbuilder.setMandatory(true).setMandatoryMsg("Rate is mandatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		FormField[][] formfield = {
				{ fgroupingPLInformation },
				{ ftbPaidLeaveId, ftbPaidLeaveName,fdbRate,fcbStatus },
				{ foblEarningComp,fbtnAdd},
				{ fotEarningCompTbl},
				
				
				
		};
		this.fields = formfield;
	}
	
	@Override
	public void updateModel(PaidLeave model) {
		
		if(tbPaidLeaveName.getValue()!=null){
			model.setPaidLeaveName(tbPaidLeaveName.getValue());
		}
		if (cbStatus.getValue() != null)
			model.setStatus(cbStatus.getValue());
		
		if(otEarningCompTbl.getValue()!=null){
			model.setOtEarningCompList(otEarningCompTbl.getValue());
		}
		if(dbRate.getValue()!=null){
			model.setRate(dbRate.getValue());
		}else{
			model.setRate(0);
		}
		presenter.setModel(model);
	}
	
	@Override
	public void updateView(PaidLeave view) {
		this.tbPaidLeaveId.setValue(view.getCount() + "");
		if (view.getPaidLeaveName() != null)
			tbPaidLeaveName.setValue(view.getPaidLeaveName());
			cbStatus.setValue(view.isStatus());
		if(view.getOtEarningCompList()!=null){
			otEarningCompTbl.setValue(view.getOtEarningCompList());
		}
		if(view.getRate()!=0){
			dbRate.setValue(view.getRate());
		}

		presenter.setModel(view);
	}
	
	
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")) {
					menus[k].setVisible(true);
				} else{
					menus[k].setVisible(false);
				}

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PAIDLEAVE,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void setCount(int count) {
		this.tbPaidLeaveId.setValue(presenter.getModel().getCount() + "");
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}


	@Override
	public boolean validate() {
		boolean superRes = super.validate();

		if (superRes == false){
			return false;
		}
		
		if(otEarningCompTbl.getValue()==null||otEarningCompTbl.getValue().size()==0){
			showDialogMessage("Please add earning component.");
			return false;
		}
		
		
		return true;
	}

	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource().equals(btnAdd)){
			if(lbEarningComponent.getSelectedIndex()!=0){
				if(otEarningCompTbl.getValue()!=null&&otEarningCompTbl.getValue().size()!=0){
					for(OtEarningComponent obj:otEarningCompTbl.getValue()){
						if(obj.getComponentName().equals(lbEarningComponent.getValue(lbEarningComponent.getSelectedIndex()))){
							showDialogMessage("Component already added.");
							return;
						}
					}
					OtEarningComponent ot=new OtEarningComponent();
					ot.setComponentName(lbEarningComponent.getValue(lbEarningComponent.getSelectedIndex()));
					otEarningCompTbl.getValue().add(ot);
				}else{
					OtEarningComponent ot=new OtEarningComponent();
					ot.setComponentName(lbEarningComponent.getValue(lbEarningComponent.getSelectedIndex()));
					otEarningCompTbl.getValue().add(ot);
				}
			}else{
				showDialogMessage("Please select earning component.");
			}
			
		}
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		
		System.out.println("INSIDE SET ENABLE METHOD   ");
		tbPaidLeaveId.setEnabled(false);
		otEarningCompTbl.setEnable(state);

	}

	@Override
	public void clear() {
		super.clear();
		cbStatus.setValue(false);
		cbStatus.setValue(true);
		tbPaidLeaveId.setEnabled(false);
		otEarningCompTbl.connectToLocal();
	}

}
