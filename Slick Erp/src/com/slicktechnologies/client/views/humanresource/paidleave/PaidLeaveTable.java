package com.slicktechnologies.client.views.humanresource.paidleave;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.PaidLeave;

public class PaidLeaveTable extends SuperTable<PaidLeave> {
	TextColumn<PaidLeave> getCountColumn;
	TextColumn<PaidLeave> getNameColumn;
	TextColumn<PaidLeave> getRateColumn;
	TextColumn<PaidLeave> getStatusColumn;
	  
	public PaidLeaveTable() {
		super();
	}
	  
	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetName();
		addColumngetRateName();
		addColumngetStatus();
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<PaidLeave>() {
			@Override
			public Object getKey(PaidLeave item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	@Override
	public void addFieldUpdater() {
	}
	
	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetName();
		addSortingRateCol();
		addSortingStatus();
	}

	

	protected void addSortinggetCount() {
		List<PaidLeave> list = getDataprovider().getList();
		columnSort = new ListHandler<PaidLeave>(list);
		columnSort.setComparator(getCountColumn, new Comparator<PaidLeave>() {
			@Override
			public int compare(PaidLeave e1, PaidLeave e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<PaidLeave>() {
			@Override
			public String getValue(PaidLeave object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		getCountColumn.setSortable(true);
	}

	protected void addSortinggetName() {
		List<PaidLeave> list = getDataprovider().getList();
		columnSort = new ListHandler<PaidLeave>(list);
		columnSort.setComparator(getNameColumn, new Comparator<PaidLeave>() {
			@Override
			public int compare(PaidLeave e1, PaidLeave e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPaidLeaveName() != null && e2.getPaidLeaveName() != null) {
						return e1.getPaidLeaveName().compareTo(e2.getPaidLeaveName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortingRateCol() {
		List<PaidLeave> list = getDataprovider().getList();
		columnSort = new ListHandler<PaidLeave>(list);
		columnSort.setComparator(getRateColumn,new Comparator<PaidLeave>() {
			
			@Override
			public int compare(PaidLeave e1, PaidLeave e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRate() == e2.getRate()) {
						return 0;
					}
					if (e1.getRate() > e2.getRate()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
			
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingStatus() {
		List<PaidLeave> list = getDataprovider().getList();
		columnSort = new ListHandler<PaidLeave>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<PaidLeave>() {
			@Override
			public int compare(PaidLeave e1, PaidLeave e2) {
				if (e1 != null && e2 != null) {
					if (e1.isStatus() == true)
						return 1;
					else
						return -1;

				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetName() {
		getNameColumn = new TextColumn<PaidLeave>() {
			@Override
			public String getValue(PaidLeave object) {
				return object.getPaidLeaveName() + "";
			}
		};
		table.addColumn(getNameColumn, "Name");
		getNameColumn.setSortable(true);
	}

	

	protected void addColumngetRateName() {
		getRateColumn = new TextColumn<PaidLeave>() {
			@Override
			public String getValue(PaidLeave object) {
				return object.getRate() + "";
			}
		};
		table.addColumn(getRateColumn, "Short Name");
		getRateColumn.setSortable(true);
	}
	

	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<PaidLeave>() {
			@Override
			public String getValue(PaidLeave object) {
				if (object.isStatus() == true)
					return "Active";
				else
					return "In Active";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
	}

}
