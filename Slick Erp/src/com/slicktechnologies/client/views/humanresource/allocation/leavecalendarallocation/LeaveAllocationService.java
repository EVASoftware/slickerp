package com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.client.views.humanresource.allocation.Allocation;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveAllocationProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;

@RemoteServiceRelativePath("leaveallocationservice")
public interface LeaveAllocationService extends RemoteService,Allocation{
	public ArrayList<AllocationResult> allocateLeave(ArrayList<LeaveBalance> leavebalance);
	
	public ArrayList<AllocationResult> deallocateCalendar(ArrayList<EmployeeInfo> allocatedEmp);
}
