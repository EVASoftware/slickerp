package com.slicktechnologies.client.views.humanresource.allocation.payslipallocation;

import java.util.Date;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.humanresource.allocation.EmployeeSearch;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipAllocationProcess;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class PaySlipForm extends  FormScreen<PaySlipAllocationProcess>  implements ClickHandler {


	EmployeeSearch empSearch;
	Button btnallocate;
	protected DateBox salaryPeriod;
	
	
	public  PaySlipForm() {
//		super();
		super(FormStyle.DEFAULT);
		createGui();
		processLevelBar.btnLabels[0].setVisible(true);
		processLevelBar.btnLabels[1].setVisible(true);
		processLevelBar.btnLabels[2].setVisible(true);
		processLevelBar.btnLabels[3].setVisible(true);
		salaryPeriod.getElement().getStyle().setWidth(120,Unit.PX);
	}

	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget()
	{
		empSearch=new EmployeeSearch();
//		salaryPeriod=new DateBox();
		salaryPeriod=new DateBoxWithYearSelector();
	
	}

	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen() {
		initalizeWidget();
		//Token to initialize the processlevel menus.
		//Ashwini Patil Date:24-08-2023 added "Update leaves" option for sunrise leave days not matching issue
		this.processlevelBarNames=new String[]{"Process Payroll","Freeze Payroll",AppConstants.CREATEACCOUNTINGINTERFACE,"Update Leaves"};/** date 3.11.2018 added by komal(accounting interface)**/
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPayRollPeriod=fbuilder.setlabel("Pay Roll Period").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Pay Roll Period",salaryPeriod);
		FormField fPayRollPeriod=fbuilder.setMandatory(true).setMandatoryMsg("Pay Roll is Mandatory !").setColSpan(0).build();
		
		
		FormField fgroupingInformation=fbuilder.setlabel("Apply Filters").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("",empSearch);
		FormField fapplyfilter= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		setFormat();
		
		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		FormField[][] formfield = {  
				{fgroupingPayRollPeriod},
				{fPayRollPeriod},
				{fgroupingInformation},
				{fapplyfilter},
		};
		this.fields=formfield;
	}


	public void setFormat()
	{
		/*********************Date Time Format***************************************************/
		DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MMM");
		DateBox.DefaultFormat defformat=new DateBox.DefaultFormat(format);
		salaryPeriod.setFormat(defformat);
		salaryPeriod.setValue(new Date());
		/*****************************************************************************************/
	}
	
	@Override
	public void updateModel(PaySlipAllocationProcess model) 
	{
	}

	@Override
	public void updateView(PaySlipAllocationProcess view) 
	{
		presenter.setModel(view);
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PROCESSPAYROLL,LoginPresenter.currentModule.trim());
	}

	@Override
	public void onClick(ClickEvent event) {
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		processLevelBar.btnLabels[0].setVisible(true);
		processLevelBar.btnLabels[1].setVisible(true);
		processLevelBar.btnLabels[3].setVisible(true);
	}
	
	public UnAllocatedEmployeeInformationTable getUnAllocated() {
		return empSearch.getUnAllocated();
	}
	public List<AllocationResult> getUnAllocatedValues() {
		return empSearch.getUnAllocatedValues();
	}

	public String getPayRollPeriod()
	{
		DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MMM");
		if(salaryPeriod.getValue()==null){
			return null;
		}
		else
		{
			return format.format(salaryPeriod.getValue());
		}
	}
	
	

}
