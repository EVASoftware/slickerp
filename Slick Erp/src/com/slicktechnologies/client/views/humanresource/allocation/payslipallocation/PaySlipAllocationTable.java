package com.slicktechnologies.client.views.humanresource.allocation.payslipallocation;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipAllocationProcess;

public class PaySlipAllocationTable extends SuperTable<PaySlipAllocationProcess>
{

	TextColumn<PaySlipAllocationProcess> userColumn;
	TextColumn<PaySlipAllocationProcess> allocatedToColumn;
	TextColumn<PaySlipAllocationProcess> monthColumn;
	TextColumn<PaySlipAllocationProcess> systemTimeColumn;
	TextColumn<PaySlipAllocationProcess> payslipPeriodColumn;
	
	
	
	public PaySlipAllocationTable() {
		super();
		connectToLocal();
	}

	public PaySlipAllocationTable(UiScreen<PaySlipAllocationProcess> mv) {
		super(mv);
	}

	private void createUserColumn()
	{
		userColumn=new TextColumn<PaySlipAllocationProcess>() {

			@Override
			public String getValue(PaySlipAllocationProcess object) {
			
				return object.getUser();
			}
		};
		table.addColumn(userColumn, "User");
	}
	
	
	
	
	
	private void createAllocatedToColumn()
	{
		allocatedToColumn=new TextColumn<PaySlipAllocationProcess>() {

			@Override
			public String getValue(PaySlipAllocationProcess object) {
			
				return object.getAllocatedTo();
			}
		};
		table.addColumn(allocatedToColumn, "Allocated To");
	}
	
	private void createSystemTimeColumn()
	{
		systemTimeColumn=new TextColumn<PaySlipAllocationProcess>() {

			@Override
			public String getValue(PaySlipAllocationProcess object) {
				String pattern="yyyy.MM.dd HH:mm:ss";
				DateTimeFormat format=DateTimeFormat.getFormat(pattern);
				String time=format.format(object.getSystemTime());
			
				return time;
			}
		};
		table.addColumn(systemTimeColumn, "System Time");
	}
	
	
	private void createPaySlipTimeColumn()
	{
		payslipPeriodColumn=new TextColumn<PaySlipAllocationProcess>() {

			@Override
			public String getValue(PaySlipAllocationProcess object) {
				
				String pattern="yyyy-MMMM";
				DateTimeFormat format=DateTimeFormat.getFormat(pattern);
				if(object.getSalaryPeriod()==null)
					return "N.A";
				String time=format.format(object.getSalaryPeriod());
			
				return time;
			}
		};
		table.addColumn(payslipPeriodColumn, "Pay Roll Period");
	}
	@Override
	public void createTable() {
		
		createUserColumn();
		createAllocatedToColumn();
		createPaySlipTimeColumn();
		//createMonthColumn();
		//createSystemTimeColumn();
	}

	@Override
	protected void initializekeyprovider() {
		
		
	}

	@Override
	public void addFieldUpdater() {
		
		
	}

	@Override
	public void setEnable(boolean state) {
		
		
	}

	@Override
	public void applyStyle() {
		
		
	}

	@Override
	public void setView(UiScreen<PaySlipAllocationProcess> view) {
		
	}
	
}
