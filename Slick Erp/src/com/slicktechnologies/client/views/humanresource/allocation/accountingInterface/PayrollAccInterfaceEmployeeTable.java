package com.slicktechnologies.client.views.humanresource.allocation.accountingInterface;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.salesperson.SalesPersonInfo;

public class PayrollAccInterfaceEmployeeTable extends SuperTable<PaySlip> {

	TextColumn<PaySlip> getColEmpId;
	TextColumn<PaySlip> getColEmpName;
	TextColumn<PaySlip> getColGrossEarning;
	TextColumn<PaySlip> getColDeductions;
	TextColumn<PaySlip> getColumnNetPayable;
	TextColumn<PaySlip> getColumnCompanyContribution;
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addColumnEmployeeId();
		addColumnEmployeeName();
		addColumnGrossEarning();
		addColumnDeductions();
		addColumnNetEarning();
		addColumnCompanyContribution();
	}

	private void addColumnEmployeeId() {

		getColEmpId = new TextColumn<PaySlip>() {

			@Override
			public String getValue(PaySlip object) {
				return object.getEmpid() + "";
			}
		};
		table.addColumn(getColEmpId, "Employee Id");
		table.setColumnWidth(getColEmpId, 90, Unit.PX);
	}

	private void addColumnEmployeeName() {

		getColEmpName = new TextColumn<PaySlip>() {

			@Override
			public String getValue(PaySlip object) {
				return object.getEmployeeName() + "";
			}
		};
		table.addColumn(getColEmpName, "Employee Name");
		table.setColumnWidth(getColEmpName, 90, Unit.PX);
	}
	private void addColumnGrossEarning() {

		getColGrossEarning = new TextColumn<PaySlip>() {

			@Override
			public String getValue(PaySlip object) {
				return object.getGrossEarning()+ "";
			}
		};
		table.addColumn(getColGrossEarning, "Gross Earning");
		table.setColumnWidth(getColGrossEarning, 90, Unit.PX);
	}

	private void addColumnDeductions() {

		getColDeductions = new TextColumn<PaySlip>() {

			@Override
			public String getValue(PaySlip object) {
				return object.getTotalDeduction() + "";
			}
		};
		table.addColumn(getColDeductions, "Deductions");
		table.setColumnWidth(getColDeductions, 90, Unit.PX);
	}

	private void addColumnNetEarning() {

		getColumnNetPayable = new TextColumn<PaySlip>() {

			@Override
			public String getValue(PaySlip object) {
				return object.getNetEarning() + "";
			}
		};
		table.addColumn(getColumnNetPayable, "Net Payable");
		table.setColumnWidth(getColumnNetPayable, 90, Unit.PX);
	}

	private void addColumnCompanyContribution() {

		getColumnCompanyContribution = new TextColumn<PaySlip>() {

			@Override
			public String getValue(PaySlip object) {
				return object.getTotalCompanyContribution()+ "";
			}
		};
		table.addColumn(getColumnCompanyContribution, "Company Contribution");
		table.setColumnWidth(getColumnCompanyContribution, 90, Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
