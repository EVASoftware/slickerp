package com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation;

import java.util.ArrayList;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveAllocationProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;

/**
 * The Class LeaveAllocationPresenter.
 */
public class LeaveAllocationPresenter extends
		FormScreenPresenter<LeaveAllocationProcess> implements ClickHandler,ChangeHandler {

	/** The form. */
	LeaveAllocationForm form;
	/** The gentable screen. */
	static LeaveAllocationTable gentableScreen;
	/** The leaveallocateservice. */
	LeaveAllocationServiceAsync leaveallocateservice = GWT.create(LeaveAllocationService.class);

	/**
	 * Instantiates a new leave allocation presenter.
	 *
	 * @param view the view
	 *            
	 * @param model  the model
	 *           
	 */
	public LeaveAllocationPresenter(FormScreen<LeaveAllocationProcess> view,LeaveAllocationProcess model) {
		super(view, model);
		form = (LeaveAllocationForm) view;
		form.olbcalender.addChangeHandler(this);
		form.getSearch().btnGo.addClickHandler(this);
	}

	@Override
	public void reactOnPrint() {
	}
	
	@Override
	public void reactOnEmail() {
	}

	@Override
	protected void makeNewModel() {
		model = new LeaveAllocationProcess();
	}

	/**
	 * Initalize.
	 */
	public static void initalize() {
		AppMemory.getAppMemory().skeleton.isMenuVisble=false;
		gentableScreen = new LeaveAllocationTable();
		LeaveAllocationForm form = new LeaveAllocationForm();
		LeaveAllocationPresenter presenter = new LeaveAllocationPresenter(form,new LeaveAllocationProcess());
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel lbl = (InlineLabel) e.getSource();
		if (lbl.getText().contains("Allocate")) {
			if (!validateAndUpdateModel()){
				return;
			}else{
				if(validateOvertimeLeave()){
					reactOnAllocate();
				}
				else{
					form.showDialogMessage("Overtime Leave is not added in "+form.olbleavegroup.getValue()+" !");
				}
			}
		}
		
		/**
		 * Date : 18-07-2018 BY ANIL
		 */
		if (lbl.getText().contains("Deallocate")) {
			List<EmployeeInfo> empArray = form.search.getAllocatedValues();
			if(empArray!=null&&empArray.size()!=0){
				ArrayList<EmployeeInfo> empInfoList=new ArrayList<EmployeeInfo>();
				empInfoList.addAll(empArray);
				form.showWaitSymbol();
				leaveallocateservice.deallocateCalendar(empInfoList, new AsyncCallback<ArrayList<AllocationResult>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(ArrayList<AllocationResult> result) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
						form.showDialogMessage("Deallocation process successful.");
					}
				});
			}else{
				form.showDialogMessage("No employee found deallocation.");
			}
		}
	}
	
	public boolean validateOvertimeLeave(){
//		System.out.println("INSIDE OT LEAVE VALIDATE");
//		Overtime ovrTym=form.olbovertime.getSelectedItem();
//		LeaveGroup levGrp=form.olbleavegroup.getSelectedItem();
//		System.out.println("IS LEAVE "+ovrTym.isLeave());
//		if(ovrTym.isLeave()){
//			for(int i=0;i<levGrp.getAllocatedLeaves().size();i++){
//				System.out.println("Leave GRP ---- "+levGrp.getAllocatedLeaves().get(i).getName());
//				System.out.println("OT ---- "+ovrTym.getLeaveName());
//				if(levGrp.getAllocatedLeaves().get(i).getName().trim().equals(ovrTym.getLeaveName().trim())){
//					return true;
//				}
//			}
//		}else{
//			System.out.println("INSIDE ELSE ");
			return true;
//		}
//		
//		
//		return false;
	}
	
	
	
	/**
	 * React on allocate.
	 */
	private void reactOnAllocate() {
		if (!validateAndUpdateModel()){
			return;
		}

		List<AllocationResult> array = form.search.getUnAllocatedValues();

		if (array == null){
			return;
		}

		ArrayList<EmployeeInfo> empInfoarray = new ArrayList<EmployeeInfo>();

		for (AllocationResult res : array) {
			empInfoarray.add(res.info);
		}

		ArrayList<LeaveBalance> allocateLeavesArray = new ArrayList<LeaveBalance>();

		if (empInfoarray.size() == 0) {
			form.showDialogMessage("No Employee found for the applicable filter !");
			return;
		}

		allocateLeavesArray = getLeaveBalanceList(empInfoarray);

		form.showWaitSymbol();
		leaveallocateservice.allocateLeave(allocateLeavesArray,new AsyncCallback<ArrayList<AllocationResult>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				view.showDialogMessage("Failed to execute Leave Allocation Process !");
			}
			@Override
			public void onSuccess(ArrayList<AllocationResult> result) {
				form.hideWaitSymbol();
				List<EmployeeInfo> allocatedEmployees = new ArrayList<EmployeeInfo>();
				List<AllocationResult> unallocatedEmployees = new ArrayList<AllocationResult>();
				for (AllocationResult res : result) {
					if (res.status == true)
						allocatedEmployees.add(res.getInfo());
					else
						unallocatedEmployees.add(res);
					
					updateGlobalEmployeeInfo(res.getInfo());
				}
				form.search.setUnAllocatedValues(unallocatedEmployees);
				form.search.setAllocatedValues(allocatedEmployees);
				view.showDialogMessage("Leave Allocation Process Successfully executed !",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
			}
		});
	}
	
	

	/**
	 * Takes {@link EmployeeInfo} e List and using form attributes returns a
	 * List of {@link LeaveBalance} Also Sets Calendar To Employees.
	 * 
	 * @param empInfoList the emp info list
	 *            
	 * @return the array list
	 */
	private ArrayList<LeaveBalance> getLeaveBalanceList(ArrayList<EmployeeInfo> empInfoList) {
		ArrayList<LeaveBalance> allocateLeavesArray = new ArrayList<LeaveBalance>();
		for (EmployeeInfo temp : empInfoList) {
			LeaveBalance leavebal = new LeaveBalance();
			temp.setLeaveCalendar(form.olbcalender.getSelectedItem());
			temp.setLeaveGroupName(form.olbleavegroup.getSelectedItem().getGroupName());
//			temp.setOvertime(form.olbovertime.getSelectedItem());
//			System.out.println("OVERTIME "+temp.getOvertime().getName());
			leavebal.setEmpInfo(temp);
			leavebal.setLeaveGroup(form.olbleavegroup.getSelectedItem());
			leavebal.setValidfrom(form.fromdate.getValue());
			leavebal.setValidtill(form.todate.getValue());

			allocateLeavesArray.add(leavebal);

		}

		return allocateLeavesArray;
	}
	
	/**
	 * Method creates and save {@link LeaveBalance} for Selected
	 * {@link EmployeeInfo}.
	 * 
	 */
	private void createAndSaveLeaveBalance() {
	}
	
	@Override
	public void reactOnSave() {
		createAndSaveLeaveBalance();
	}

	
	
	/*******************************************************************************************************/
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt
	 * .event.dom.client.ChangeEvent)
	 */
	@Override
	public void onChange(ChangeEvent event) {

		if (event.getSource() == form.olbcalender) {
			if (form.olbcalender.getSelectedIndex() != 0) {
				Calendar cal = form.olbcalender.getSelectedItem();
				form.fromdate.setValue(cal.getCalStartDate());
				form.todate.setValue(cal.getCalEndDate());
			} else {
				form.fromdate.getTextBox().setText("");
				form.todate.getTextBox().setText("");
			}
		}

	}

/*******************************************************************************************************/
/**
 * On Click method 
 */

	public void onClick(ClickEvent event) {
		super.onClick(event);
		if (event.getSource() == form.getSearch().btnGo) {
			System.out.println("Inside On GO Click..........");
			ArrayList<Filter> employeeFilter = form.getSearch().getEmployeeFilter();
			getAllocationStatusAndUpdateTable(employeeFilter);
		}
	}
	
	/**
	 * Gets the Allocation Status depending upon the querry applied by user.
	 */
	
	private List<AllocationResult> getAllocationStatusAndUpdateTable(ArrayList<Filter> filter) {
		System.out.println("Inside list METHOD ");
		// The List of Allocation Result to be filled from server
		final List<AllocationResult> lis = new ArrayList<AllocationResult>();
		
		if (filter == null){
			return null;
		}
		
		form.showWaitSymbol();
		leaveallocateservice.getAllocationStatus(filter,new AsyncCallback<ArrayList<AllocationResult>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
			@Override
			public void onSuccess(ArrayList<AllocationResult> result) {
				form.hideWaitSymbol();
				updateTable(result);
			}
		});
		return lis;
	}
	
	/**
	 * Utility Method to Update allocated and UnAllocated Tables based on upon
	 * AllocationResult.
	 * 
	 * @param result Class Allocation Result.
	 *            
	 */
	private void updateTable(ArrayList<AllocationResult> result) {
		List<EmployeeInfo> allocatedEmployees = new ArrayList<EmployeeInfo>();
		List<AllocationResult> unallocatedEmployees = new ArrayList<AllocationResult>();

		for (AllocationResult res : result) {
			if (res.status == true) {
				allocatedEmployees.add(res.getInfo());
			} else {
				unallocatedEmployees.add(res);
			}
		}
		form.search.setAllocatedValues(allocatedEmployees);
		form.search.setUnAllocatedValues(unallocatedEmployees);
	}
	
	/******************************************************************************************************/
	
	
	protected void updateGlobalEmployeeInfo(EmployeeInfo employeeInfo) {

		for(int i=0;i<LoginPresenter.globalEmployeeInfo.size();i++)
		{
			if(LoginPresenter.globalEmployeeInfo.get(i).getId().equals(employeeInfo.getId()))
			{
				LoginPresenter.globalEmployeeInfo.add(employeeInfo);
				LoginPresenter.globalEmployeeInfo.remove(i);
			}
		}
	}
	
}
