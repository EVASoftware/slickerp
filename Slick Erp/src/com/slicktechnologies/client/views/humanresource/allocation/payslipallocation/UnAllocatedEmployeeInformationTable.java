package com.slicktechnologies.client.views.humanresource.allocation.payslipallocation;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;

public class UnAllocatedEmployeeInformationTable extends SuperTable<AllocationResult> {

	TextColumn<AllocationResult> Id;
	TextColumn<AllocationResult> employeeName;
	TextColumn<AllocationResult> employeeCell;
	TextColumn<AllocationResult> employeeBranch;
	TextColumn<AllocationResult> employeeDepartment;
	TextColumn<AllocationResult> employeeType;
	TextColumn<AllocationResult> employeeDesignation;
	TextColumn<AllocationResult> employeeRole;
//	TextColumn<AllocationResult> resun;
	TextColumn<AllocationResult> result;
	
	private Column<AllocationResult, String> delete;
	
	
	public UnAllocatedEmployeeInformationTable() 
	{
		super();
	}
	
	
	@Override
	public void createTable() {
		addColumnId();
		addColumnEmployeeName();
		addColumnEmployeeCell();
		addColumnEmployeeBranch();
		addColumnEmployeeDepartment();
		addColumnEmployeeDesignation();
		
		addColumnEmployeeType();
		addColumnEmployeeRole();
		/**
		 * Date : 03-05-2018 By ANIL
		 * this column is added to show whether the calendar is allocated to employee or not
		 * for ctc allocation 
		 */
		addColumnReson();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	    addColumnSorting();
	}
	
	

	
	
	public void addColumnId()
	{
		Id = new TextColumn<AllocationResult>() {
			@Override
			public String getValue(AllocationResult object) {
				return object.getEmpCount()+"";
			}
		};
		table.addColumn(Id,"ID");
	}
	
	public void addColumnReson()
	{
		result = new TextColumn<AllocationResult>() {
			@Override
			public String getValue(AllocationResult object) {
				return object.getReason();
			}
		};
		table.addColumn(result,"Comments");
	}
	
	public void addColumnResult()
	{
		result = new TextColumn<AllocationResult>() {
			@Override
			public String getValue(AllocationResult object) {
				if(object.isStatus()==true)
					return "Processed";
				else
					return "Failed";
			}
		};
		table.addColumn(result,"Status");
	}
	
	public void addColumnEmployeeName()
	{
		employeeName = new TextColumn<AllocationResult>() {
			@Override
			public String getValue(AllocationResult object) {
				
				return object.getFullName()+"";
			}
		};
		table.addColumn(employeeName,"Name");
	}
	
	public void addColumnEmployeeCell()
	{
		employeeCell = new TextColumn<AllocationResult>() {
			@Override
			public String getValue(AllocationResult object) {
				return object.getCellNumber()+"";
			}
		};
		table.addColumn(employeeCell,"Cell Number");
	}

	  private void addColumnDelete()
		{
			ButtonCell btnCell= new ButtonCell();
			delete = new Column<AllocationResult, String>(btnCell) {

				@Override
				public String getValue(AllocationResult object) {
					
					return "Delete";
				}
			};
			table.addColumn(delete,"Delete");
		}
	  
	  void addColumnEmployeeBranch()
	  {
		  employeeBranch=new TextColumn<AllocationResult>() {

			@Override
			public String getValue(AllocationResult object) {
				// TODO Auto-generated method stub
				return object.getBranch()+"";
			}
		};
		
		table.addColumn(employeeBranch,"Branch");
	  }
	  
	  void addColumnEmployeeDepartment()
	  {
		  employeeDepartment=new TextColumn<AllocationResult>() {

				@Override
				public String getValue(AllocationResult object) {
					// TODO Auto-generated method stub
					return object.getDepartment()+"";
				}
			};
			table.addColumn(employeeDepartment,"Department");
	  }

	 void addColumnEmployeeDesignation()
	 {
		 employeeDesignation=new TextColumn<AllocationResult>() {

				@Override
				public String getValue(AllocationResult object) {
					// TODO Auto-generated method stub
					return object.getDesignation()+"";
				}
			};
			table.addColumn(employeeDesignation,"Designation");
	 }
	
		void addColumnEmployeeType()
		{
			employeeType=new TextColumn<AllocationResult>() {

				@Override
				public String getValue(AllocationResult object) {
					// TODO Auto-generated method stub
					return object.getEmployeeType();
				}
			};
			table.addColumn(employeeType,"Type");

		}
		
		
		
		
		void addColumnEmployeeRole()
		{
			employeeRole=new TextColumn<AllocationResult>() {

				@Override
				public String getValue(AllocationResult object) {
					// TODO Auto-generated method stub
					return object.getEmployeerole();
				}
			};
			table.addColumn(employeeRole,"Role");
		}
	  
		private void setFieldUpdaterOnDelete()
		{
			delete.setFieldUpdater(new FieldUpdater<AllocationResult, String>() {
				
				@Override
				public void update(int index, AllocationResult object, String value) {
					getDataprovider().getList().remove(index);
				
				}
			});
		}

	
		
		
	

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<AllocationResult>() {
			@Override
			public Object getKey(AllocationResult item) {
				if(item==null)
					return null;
				else
					return item.getCount();
			}
		};
	
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	
	
	

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	
	protected void addSortinggetBranch()
	{
		List<AllocationResult> list=getDataprovider().getList();
		columnSort=new ListHandler<AllocationResult>(list);
		columnSort.setComparator(employeeBranch, new Comparator<AllocationResult>()
				{
			@Override
			public int compare(AllocationResult e1,AllocationResult e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggeName()
	{
		List<AllocationResult> list=getDataprovider().getList();
		columnSort=new ListHandler<AllocationResult>(list);
		columnSort.setComparator(employeeName, new Comparator<AllocationResult>()
				{
			@Override
			public int compare(AllocationResult e1,AllocationResult e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getFullName().compareTo(e2.getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggeType()
	{
		List<AllocationResult> list=getDataprovider().getList();
		columnSort=new ListHandler<AllocationResult>(list);
		columnSort.setComparator(employeeType, new Comparator<AllocationResult>()
				{
			@Override
			public int compare(AllocationResult e1,AllocationResult e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getEmployeeType().compareTo(e2.getEmployeeType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggeDepartment()
	{
		List<AllocationResult> list=getDataprovider().getList();
		columnSort=new ListHandler<AllocationResult>(list);
		columnSort.setComparator(employeeDepartment, new Comparator<AllocationResult>()
				{
			@Override
			public int compare(AllocationResult e1,AllocationResult e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getDepartment().compareTo(e2.getDepartment());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggeDesignation()
	{
		List<AllocationResult> list=getDataprovider().getList();
		columnSort=new ListHandler<AllocationResult>(list);
		columnSort.setComparator(employeeDesignation, new Comparator<AllocationResult>()
				{
			@Override
			public int compare(AllocationResult e1,AllocationResult e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getDesignation().compareTo(e2.getDesignation());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggRole()
	{
		List<AllocationResult> list=getDataprovider().getList();
		columnSort=new ListHandler<AllocationResult>(list);
		columnSort.setComparator(employeeRole, new Comparator<AllocationResult>()
				{
			@Override
			public int compare(AllocationResult e1,AllocationResult e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getEmployeerole().compareTo(e2.getEmployeerole());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCellNumber()
	{
		List<AllocationResult> list=getDataprovider().getList();
		columnSort=new ListHandler<AllocationResult>(list);
		columnSort.setComparator(employeeCell, new Comparator<AllocationResult>()
				{
			@Override
			public int compare(AllocationResult e1,AllocationResult e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCellNumber()== e2.getCellNumber()){
						return 0;}
					if(e1.getCellNumber()> e2.getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCount()
	  {
	  List<AllocationResult> list=getDataprovider().getList();
	  columnSort=new ListHandler<AllocationResult>(list);
	  columnSort.setComparator(Id, new Comparator<AllocationResult>()
	  {
	  @Override
	  public int compare(AllocationResult e1,AllocationResult e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if(e1.getCount()== e2.getCount()){
	  return 0;}
	  if(e1.getCount()> e2.getCount()){
	  return 1;}
	  else{
	  return -1;}
	  }
	  else{
	  return 0;}
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  }
	
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetBranch();
		addSortinggeDepartment();
		addSortinggeDesignation();
		addSortinggeType();
		addSortinggRole();
		addSortinggeName();
		addSortinggetCellNumber();
		
	}


	@Override
	public void setEnable(boolean state) {
		for(int i=table.getColumnCount()-1;i>=0;i--)
			table.removeColumn(i);
		if(state==true)
		{
			addColumnId();
			addColumnEmployeeName();
			addColumnEmployeeCell();
			addColumnEmployeeBranch();
			addColumnEmployeeDepartment();
			addColumnEmployeeDesignation();
			
			addColumnEmployeeType();
			addColumnEmployeeRole();
			addColumnDelete();
			setFieldUpdaterOnDelete();
		}
		else
		{
			addColumnId();
			addColumnEmployeeName();
			addColumnEmployeeCell();
			addColumnEmployeeBranch();
			addColumnEmployeeDepartment();
			addColumnEmployeeDesignation();
			
			addColumnEmployeeType();
			addColumnEmployeeRole();
			addColumnResult();
			addColumnReson();
		}
		
	}

}
