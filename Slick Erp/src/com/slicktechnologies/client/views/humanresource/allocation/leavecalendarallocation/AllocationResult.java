package com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation;

import java.io.Serializable;

import com.googlecode.objectify.Key;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;

/**
 * Stores the result of Allocation For each Employee.
 * 
 */
public class AllocationResult implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4417506565586789070L;
	
	/** The reason for which allocation failed */
	public String reason;
	public boolean status;
	public EmployeeInfo info;

	/**
	 * Instantiates a new employee allocation result.
	 */
	public AllocationResult() {
		super();
		reason="";
		info=new EmployeeInfo();
	}

	/**
	 * Gets the reason.
	 *
	 * @return the reason
	 */
	public  String getReason() {
		return reason;
	}

	/**
	 * Sets the reason.
	 *
	 * @param reason the new reason
	 */
	public  void setReason(String reason) {
		if(reason!=null)
		   this.reason = reason.trim();
	}

	public int getCount() {
		return info.getCount();
	}

	public void setCount(int long1) {
		info.setCount(long1);
	}

	public Long getCompanyId() {
		return info.getCompanyId();
	}

	public Long getId() {
		return info.getId();
	}

	public void setDeleted(boolean deleted) {
		info.setDeleted(deleted);
	}

	public void setCompanyId(Long companyId) {
		info.setCompanyId(companyId);
	}

	public String getBranch() {
		return info.getBranch();
	}

	public void setBranch(String branch) {
		info.setBranch(branch);
	}

	public Long getCellNumber() {
		return info.getCellNumber();
	}

	public void setCellNumber(Long cellNumber) {
		info.setCellNumber(cellNumber);
	}

	public String getFullName() {
		return info.getFullName();
	}

	public void setFullName(String fullName) {
		info.setFullName(fullName);
	}

	public int getEmpCount() {
		return info.getEmpCount();
	}

	public void setEmpCount(int count) {
		info.setEmpCount(count);
	}

	public String getDesignation() {
		return info.getDesignation();
	}

	public void setDesignation(String designation) {
		info.setDesignation(designation);
	}

	public Key<Calendar> getCalenderKey() {
		return info.getCalenderKey();
	}

	public void setCalenderKey(Key<Calendar> calenderKey) {
		info.setCalenderKey(calenderKey);
	}

	public String getDepartment() {
		return info.getDepartment();
	}

	public void setDepartment(String department) {
		info.setDepartment(department);
	}

	public String getEmployeerole() {
		return info.getEmployeerole();
	}

	public void setEmployeerole(String employeerole) {
		info.setEmployeerole(employeerole);
	}

	public String getEmployeeType() {
		return info.getEmployeeType();
	}

	public void setEmployeeType(String employeeType) {
		info.setEmployeeType(employeeType);
	}

	public String getCountry() {
		return info.getCountry();
	}

	public void setCountry(String country) {
		info.setCountry(country);
	}

	public Calendar getLeaveCalendar() {
		return info.getLeaveCalendar();
	}

	public void setLeaveCalendar(Calendar leaveCalendar) {
		info.setLeaveCalendar(leaveCalendar);
	}

	public String getCalendarName() {
		return info.getCalendarName();
	}

	public void setCalendarName(String calendarName) {
		info.setCalendarName(calendarName);
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public EmployeeInfo getInfo() {
		return info;
	}

	public void setInfo(EmployeeInfo info) {
		this.info = info;
	}
	
	
   
}
