package com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveAllocationProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;


public interface LeaveAllocationServiceAsync {
	public void allocateLeave(ArrayList<LeaveBalance> leavebalance,AsyncCallback<ArrayList<AllocationResult>> callback);
	public void  getAllocationStatus(ArrayList<Filter>filter,AsyncCallback<ArrayList<AllocationResult>> callback);
	
	public void deallocateCalendar(ArrayList<EmployeeInfo> allocatedEmp,AsyncCallback<ArrayList<AllocationResult>> callback);
}
