package com.slicktechnologies.client.views.humanresource.allocation.accountingInterface;

import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.datepicker.client.DateBox;
import com.ibm.icu.text.DateFormat;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CompanyPayrollRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;

public class PayrollAccInterfacePopup extends PopupScreen {

	ObjectListBox<Branch> olbbBranch;	
	DateBox dbPeriod;
	ObjectListBox<Config> olbNumberRange;
	Button btGo;
	PayrollAccInterfaceEmployeeTable empTable;
	
	public PayrollAccInterfacePopup() {
		super();
		empTable.connectToLocal();
		getPopup().setWidth("830px");
		empTable.getTable().setHeight("400px");
		empTable.getTable().setWidth("800px");
		createGui();
		DateTimeFormat dateFormat=DateTimeFormat.getFormat("yyyy-MMM");
		dbPeriod.setFormat(new DateBox.DefaultFormat(dateFormat));	
	}
	
	
	private void initializeWidget(){
		
		olbbBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbBranch);
		dbPeriod=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat=DateTimeFormat.getFormat("yyyy-MMM");
		dbPeriod.setFormat(new DateBox.DefaultFormat(dateFormat));
		olbNumberRange = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbNumberRange, Screen.NUMBERRANGE);
		empTable = new PayrollAccInterfaceEmployeeTable();
		btGo = new Button("GO");
		
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAccountingInterface=fbuilder.setlabel("Accounting Interface Popup").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Month", dbPeriod);
		FormField fdbMonth= fbuilder.setMandatory(true).setMandatoryMsg("Month is Mandatory.").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Branch" , olbbBranch);
		FormField folbbBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory.").setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("* Number Range", olbNumberRange);
		FormField folbNumberRange = fbuilder.setMandatory(true).setMandatoryMsg("Number Range is Mandatory.").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btGo);
		FormField fbtGo = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", empTable.getTable());
		FormField fempTable = fbuilder.setRowSpan(0).setColSpan(4).build();
		
		FormField[][] formfield = {  
				{fgroupingAccountingInterface},
				{fdbMonth ,folbbBranch , folbNumberRange , fbtGo},
				{fempTable }
		};
		this.fields=formfield;
	}


	public Button getBtGo() {
		return btGo;
	}


	public void setBtGo(Button btGo) {
		this.btGo = btGo;
	}


	public PayrollAccInterfaceEmployeeTable getEmpTable() {
		return empTable;
	}


	public void setEmpTable(PayrollAccInterfaceEmployeeTable empTable) {
		this.empTable = empTable;
	}


	public ObjectListBox<Branch> getOlbbBranch() {
		return olbbBranch;
	}


	public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
		this.olbbBranch = olbbBranch;
	}


	public DateBox getDbPeriod() {
		return dbPeriod;
	}


	public void setDbPeriod(DateBox dbPeriod) {
		this.dbPeriod = dbPeriod;
	}


	public ObjectListBox<Config> getOlbNumberRange() {
		return olbNumberRange;
	}


	public void setOlbNumberRange(ObjectListBox<Config> olbNumberRange) {
		this.olbNumberRange = olbNumberRange;
	}
	public MyQuerry getQuerry(){
		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setBooleanvalue(true);
		temp.setQuerryString("isFreeze");
		filtervec.add(temp);
		
		if(this.getDbPeriod().getValue() != null){
			temp=new Filter();
			temp.setQuerryString("salaryPeriod");
			temp.setStringValue(DateTimeFormat.getFormat("yyyy-MMM").format(this.getDbPeriod().getValue()));
			filtervec.add(temp);
		}
		if(this.getOlbbBranch().getSelectedIndex() != 0){
			temp=new Filter();
			temp.setQuerryString("branch");
			temp.setStringValue(this.getOlbbBranch().getValue());
			filtervec.add(temp);
		}
		querry.setFilters(filtervec);
		querry.setQuerryObject(new PaySlip());
		
		return querry;
 }
	public MyQuerry getCompContributionQuerry(){
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;	
		if(dbPeriod.getValue() != null)
		{		
			temp=new Filter();
			temp.setStringValue(DateTimeFormat.getFormat("yyyy-MMM").format(this.getDbPeriod().getValue()));
			temp.setQuerryString("payrollMonth");
			filtervec.add(temp);
		}
	
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CompanyPayrollRecord());
		return querry;

	}

}
