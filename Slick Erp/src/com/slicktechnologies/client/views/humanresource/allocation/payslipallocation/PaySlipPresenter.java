package com.slicktechnologies.client.views.humanresource.allocation.payslipallocation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.humanresource.allocation.EmployeeSearch;
import com.slicktechnologies.client.views.humanresource.allocation.accountingInterface.PayrollAccInterfacePopup;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.client.views.humanresource.timereport.EmployeeTimeReportDetails;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.GeneralKeyValueBean;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CompanyPayrollRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipAllocationProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReportEntrey;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class PaySlipPresenter extends
		FormScreenPresenter<PaySlipAllocationProcess> implements ClickHandler {
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	PaySlipForm form;
	EmployeeSearch empSearch;
	static PaySlipAllocationTable gentableScreen;
	PaySlipAllocationServiceAsync payslipAllocationService = GWT
			.create(PaySlipAllocationService.class);
	GenricServiceAsync asyncgen = GWT.create(GenricService.class);
	/** date 17.7.2018 added by komal **/
	GeneralServiceAsync service = GWT.create(GeneralService.class);
	ArrayList<EmployeeTimeReportDetails> empTRList;
	/**
	 * date 3.11.2018 added by komal to create accounting interface of freez
	 * payroll
	 **/
	PayrollAccInterfacePopup payrollAccInterfacePopup = new PayrollAccInterfacePopup();
	ArrayList<CompanyPayrollRecord> companyDeductionList = new ArrayList<CompanyPayrollRecord>();
	FromAndToDateBoxPopup datePopUp=new FromAndToDateBoxPopup();
	PopupPanel datePanel=new PopupPanel();
	int hrLicenseCount;
	int noOfPayrollProcessedInMonth;
	boolean isHrLicenseCountFetched=false;
	CommonServiceAsync commonService = GWT
			.create(CommonService.class);
	InformationPopup infoPopup=new InformationPopup("Unable to Process Payroll");//Ashwini Patil Date:14-09-2023
	PopupPanel infoPanel=new PopupPanel();
	public PaySlipPresenter(UiScreen<PaySlipAllocationProcess> view,
			PaySlipAllocationProcess model) {
		super(view, model);
		form = (PaySlipForm) view;
		form.empSearch.getBtnGo().addClickHandler(this);
		/*** Date 23-2-2019 
		 * @author Amol
		 * added download option for unallocated employee
		 */
		form.empSearch.btnDownload.addClickHandler(this);
		/** date 3.11.2018 added by komal **/
		payrollAccInterfacePopup.getLblOk().addClickHandler(this);
		payrollAccInterfacePopup.getLblCancel().addClickHandler(this);
		payrollAccInterfacePopup.getBtGo().addClickHandler(this);
		datePopUp.getBtnOne().addClickHandler(this);
		datePopUp.getBtnTwo().addClickHandler(this);
		infoPopup.getBtnOne().addClickHandler(this);
		infoPopup.getBtnTwo().addClickHandler(this);
		infoPopup.getInfo().setEnabled(false);
	}

	public static void initalize() {
		AppMemory.getAppMemory().skeleton.isMenuVisble=false;
		gentableScreen = new PaySlipAllocationTable();
		PaySlipForm form = new PaySlipForm();
		PaySlipPresenter presenter = new PaySlipPresenter(form,
				new PaySlipAllocationProcess());
		AppMemory.getAppMemory().stickPnel(form);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel lbl = (InlineLabel) e.getSource();
		if (lbl.getText().contains("Process Payroll")) {
			// if(validateTimeReport()){
			/*
			 * Commented by Rahul Verma Date: 07 July 2018
			 */
			// processPayRoll();
			/*
			 * Ends
			 */
			processNewPayRoll();
			// }
		}

		if (lbl.getText().contains("Freeze Payroll")) {
			reactOnFreezePayroll();
		}
		/** date 3.11.2018 added by komal for accounting interface popup **/
		if (lbl.getText().equalsIgnoreCase(
				AppConstants.CREATEACCOUNTINGINTERFACE)) {
			reactOnCreateAccountingInterface();
		}
		//Ashwini Patil Date:24-08-2023 for sunrise leave days not matching issue
		if (lbl.getText().equalsIgnoreCase("Update Leaves")) {
			datePanel=new PopupPanel(true);
			datePanel.add(datePopUp);
			datePanel.center();
			datePanel.show();
		}
	}

	private void reactOnFreezePayroll() {
		if (form.empSearch.getAllocatedValues() != null
				&& form.empSearch.getAllocatedValues().size() != 0) {
			ArrayList<EmployeeInfo> empInfoList = new ArrayList<EmployeeInfo>();
			empInfoList.addAll(form.empSearch.getAllocatedValues());

			form.showWaitSymbol();
			payslipAllocationService.freezePayroll(empInfoList,
					form.getPayRollPeriod(), LoginPresenter.loggedInUser,
					new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
							form.showDialogMessage(result);

						}
					});

		} else {
			form.showDialogMessage("No processed payroll found.");
		}
	}
	ArrayList<EmployeeInfo> empInfoarray;
	private void processNewPayRoll() {
		if (!validateAndUpdateModel()) {
			return;
		}
		List<AllocationResult> array = form.empSearch.getUnAllocatedValues();

		if (array == null) {
			return;
		}
		empInfoarray = new ArrayList<EmployeeInfo>();

		for (AllocationResult res : array) {
			empInfoarray.add(res.info);
		}

		if (empInfoarray.size() == 0) {
			form.showDialogMessage("No Employee found for the applicable filter !");
			return;
		}

		form.showWaitSymbol();
		/** date 17.7.2018 added by komal for payroll through taskqueue **/
		List<ProcessConfiguration> processList = LoginPresenter.globalProcessConfig;
		int cnt = 0;
		for (int k = 0; k < processList.size(); k++) {
			if (processList.get(k).getProcessName().trim()
					.equalsIgnoreCase("ProcessPayRollThroughTaskQueue")) {

				for (int i = 0; i < processList.get(k).getProcessList().size(); i++) {
					if (processList.get(k).getProcessList().get(i).isStatus() == true) {
						try {
							cnt = Integer.parseInt(processList.get(k)
									.getProcessList().get(i).getProcessType());
							break;
						} catch (Exception e) {
							cnt = 0;
						}

					}
				}
				
			}	
    	}
		final int cntfinal=cnt;
		if(!isHrLicenseCountFetched) {
			Console.log("fetching hr license count. company id="+empInfoarray.get(0).getCompanyId());
			commonService.getNumberOfHRLicense(empInfoarray.get(0).getCompanyId(), new AsyncCallback<Integer>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					Console.log("failed to load hrlicesnes count");
					hrLicenseCount=0;
				}

				@Override
				public void onSuccess(Integer result) {
					hrLicenseCount=result;
					isHrLicenseCountFetched=true;
					Console.log("in success hrLicenseCount="+result);
				}
			});
		}
		
		commonService.getNumberOfPayrollProcessedInMonth(empInfoarray.get(0).getCompanyId(), form.getPayRollPeriod(), new AsyncCallback<Integer>() {

			@Override
			public void onFailure(Throwable caught) {
				Console.log("failed to load payroll count");
				noOfPayrollProcessedInMonth=0;
			}

			@Override
			public void onSuccess(Integer result) {
				// TODO Auto-generated method stub
				noOfPayrollProcessedInMonth=result;
				Console.log("in success payrollcount="+result);
			}
		});
		
		
		
		Timer timer = new Timer() {
			@Override
			public void run() {
				if(hrLicenseCount==0){
					form.showDialogMessage("You have not subscribed for payroll. Kindly buy payroll licenses at support@evasoftwaresolutions.com");
					form.hideWaitSymbol();
					return;
				}
				
				
				int noOfAllowedPayrolls=hrLicenseCount-noOfPayrollProcessedInMonth;
				
				Console.log("noOfAllowedPayrolls="+noOfAllowedPayrolls);
				Console.log("employees selected="+empInfoarray.size());
				if(noOfAllowedPayrolls>=empInfoarray.size()){
				
		    	if(cntfinal !=0 && empInfoarray.size() > cntfinal ){
		    		Console.log("Employee count : "+ cntfinal);
		    		String projectName = "";
		    		if(form.empSearch.project.getSelectedIndex()!=0) {
		    			projectName = form.empSearch.project.getValue();
		        		Console.log("projectName "+projectName);
		    		}
		    		service.paySlipAllocationTaskQueue(empInfoarray, form.getPayRollPeriod(), null, null,projectName, new AsyncCallback<Void>() {
						@Override
						public void onSuccess(Void result) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
							view.showDialogMessage("Pay Roll Process has started. "+"\n"+"Please wait..",
									GWTCAlert.OPTION_ROUNDED_BLUE,
									GWTCAlert.OPTION_ANIMATION);
							form.empSearch.getUnAllocated().connectToLocal();
							form.empSearch.setAllocatedValues(empInfoarray);
							
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
							view.showDialogMessage("Failed to process Pay Roll !");
						}
					});
		    		
		    	}else{
		    		Console.log("Employee count : "+  "else condition");
				payslipAllocationService.allocatePaySlip(empInfoarray,form.getPayRollPeriod(), null, null,false,new AsyncCallback<ArrayList<AllocationResult>>() {
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						view.showDialogMessage("Failed to process Pay Roll !");
					}
					@Override
					public void onSuccess(ArrayList<AllocationResult> result) {
						form.hideWaitSymbol();
						List<EmployeeInfo> allocatedEmployees = new ArrayList<EmployeeInfo>();
						List<AllocationResult> unallocatedEmployees = new ArrayList<AllocationResult>();
						for (AllocationResult res : result) {
							if (res.status == true)
								allocatedEmployees.add(res.info);
							else
								unallocatedEmployees.add(res);
						}
						form.empSearch.setUnAllocatedValues(unallocatedEmployees);
						form.empSearch.setAllocatedValues(allocatedEmployees);
						form.empSearch.unAllocated.setEnable(false);
						
						if (allocatedEmployees.size() != 0){
//							final String url = com.google.gwt.core.client.GWT.getModuleBaseURL() + "processpayslip";
//							Window.open(url, "test", "enabled");
							
							view.showDialogMessage("Pay Roll Processed ",
							GWTCAlert.OPTION_ROUNDED_BLUE,
							GWTCAlert.OPTION_ANIMATION);
						}
					}
				});
				}
		    	
			}else{
				form.hideWaitSymbol();
				Console.log("calling info popup");
				String msg="Total HR Licenses per month ="+hrLicenseCount+"\n";
				msg+="Already processed ="+noOfPayrollProcessedInMonth+"\n";
				msg+="Trying to process ="+empInfoarray.size()+"\n";
				msg+="Either reduce number of employees for payroll or buy additional licenses at support@evasoftwaresolutions.com";
				infoPopup.getInfo().setText(msg);					
				infoPanel=new PopupPanel(true);
				infoPanel.add(infoPopup);
				infoPanel.center();
				infoPanel.show();
				
			}				
			}
		};
		timer.schedule(2000);
				
	}

	@Override
	public void reactOnPrint() {
	}

	@Override
	public void reactOnEmail() {
	}

	@Override
	protected void makeNewModel() {
		model = new PaySlipAllocationProcess();
	}

	private void processPayRoll() {
		if (!validateAndUpdateModel()) {
			return;
		}
		List<AllocationResult> array = form.empSearch.getUnAllocatedValues();

		if (array == null) {
			return;
		}
		ArrayList<EmployeeInfo> empInfoarray = new ArrayList<EmployeeInfo>();

		for (AllocationResult res : array) {
			empInfoarray.add(res.info);
		}

		if (empInfoarray.size() == 0) {
			form.showDialogMessage("No Employee found for the applicable filter !");
			return;
		}

		form.showWaitSymbol();
		payslipAllocationService.allocatePaySlip(empInfoarray,form.getPayRollPeriod(), null, null,false,new AsyncCallback<ArrayList<AllocationResult>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				view.showDialogMessage("Failed to process Pay Roll !");
			}
			@Override
			public void onSuccess(ArrayList<AllocationResult> result) {
				form.hideWaitSymbol();
				List<EmployeeInfo> allocatedEmployees = new ArrayList<EmployeeInfo>();
				List<AllocationResult> unallocatedEmployees = new ArrayList<AllocationResult>();
				for (AllocationResult res : result) {
					if (res.status == true)
						allocatedEmployees.add(res.info);
					else
						unallocatedEmployees.add(res);
				}
				form.empSearch.setUnAllocatedValues(unallocatedEmployees);
				form.empSearch.setAllocatedValues(allocatedEmployees);
				form.empSearch.unAllocated.setEnable(false);
				final String url = com.google.gwt.core.client.GWT.getModuleBaseURL() + "processpayslip";

				if (allocatedEmployees.size() != 0){
					Window.open(url, "test", "enabled");
					view.showDialogMessage("Pay Roll Processed ",
					GWTCAlert.OPTION_ROUNDED_BLUE,
					GWTCAlert.OPTION_ANIMATION);
				}
			}
		});
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		if (event.getSource() == form.empSearch.btnGo) {
			/**Date 27-3-2019
			 * Added by Amol to refresh the data of Unalloacted employee table and Allocated employee table
			 */
			form.empSearch.allocated.connectToLocal();
			form.empSearch.unAllocated.connectToLocal();
			
			/**
			 * 
			 */
			ArrayList<Filter> employeeFilter = form.empSearch.getEmployeeFilter();
			getAllocationStatusAndUpdateTable(employeeFilter);
		}
		if (event.getSource().equals(payrollAccInterfacePopup.getLblCancel())) {
			payrollAccInterfacePopup.hidePopUp();
		}
		if (event.getSource().equals(payrollAccInterfacePopup.getLblOk())) {
			createAccountingInterface();
		}
		if (event.getSource().equals(payrollAccInterfacePopup.getBtGo())) {
			reactOnGoButton();
		}
		
		/*** Date 23-2-2019 
		 * @author Amol
		 * added download option for unallocated employee
		 */
		if(event.getSource().equals(form.empSearch.btnDownload)){
			
			 if(form.empSearch.getUnAllocated().getDataprovider().getList().size()!=0){
				 System.out.println("unallocated employee onclick");
				reactOnDownload();
			}else{
				form.showDialogMessage("No unprocessed records !");
			}
		}
		
		if(event.getSource().equals(datePopUp.getBtnOne())){
			Date fromdate=null;
			Date todate=null;
			if(datePopUp.getFromDate().getValue()!=null&&datePopUp.getToDate().getValue()!=null){
				
				Console.log("fromdate="+datePopUp.getFromDate().getValue()+" todate="+datePopUp.getToDate().getValue());					
				Date formDate;
				Date toDate;
				formDate=datePopUp.getFromDate().getValue();
				toDate=datePopUp.getToDate().getValue();
				
				int diffdays = AppUtility.getDifferenceDays(formDate, toDate);
				Console.log("diffdays "+diffdays);
				if(diffdays>31){
					form.showDialogMessage("Please select from date and to date range within 31 days");
					return;
				}
			}else{
				form.showDialogMessage("Please select date");
				return;
			}
			
			if(datePopUp.getFromDate().getValue()!=null) {
				fromdate=datePopUp.getFromDate().getValue();
				CalendarUtil.addDaysToDate(fromdate, 1);
			}
			if(datePopUp.getToDate().getValue()!=null) {
				todate=datePopUp.getToDate().getValue();
				CalendarUtil.addDaysToDate(todate, 1);
			}
			Console.log("fromdate="+fromdate+" todate:"+todate);
			genService.deleteDuplicateLeaves(model.getCompanyId(), fromdate, todate, new  AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Process failed!");
				}

				@Override
				public void onSuccess(Void result) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Process has started successfully! Please check after 30 minutes");
				}
			});
			
			datePanel.hide();
		}
		if(event.getSource().equals(datePopUp.getBtnTwo())){
			datePanel.hide();
		}
		if(event.getSource().equals(infoPopup.getBtnOne())){
			infoPanel.hide();
		}
		if(event.getSource().equals(infoPopup.getBtnTwo())){
			infoPanel.hide();
		}
	}

	/**
	 * Gets the Allocation Status depending upon the querry applied by user.
	 */
	private List<AllocationResult> getAllocationStatusAndUpdateTable(
			ArrayList<Filter> filter) {
		// The List of Allocation Result to be filled from server
		final List<AllocationResult> lis = new ArrayList<AllocationResult>();
		System.out.println("Size is " + lis.size());
		if (filter == null) {
			return null;
		}
		form.showWaitSymbol();
		String period = form.getPayRollPeriod();
		payslipAllocationService.getAllocationStatus(filter, period, null,
				null, new AsyncCallback<ArrayList<AllocationResult>>() {
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(ArrayList<AllocationResult> result) {
						form.hideWaitSymbol();
						updateTable(result);
						form.empSearch.unAllocated.setEnable(true);

						List<AllocationResult> unallocatedEmpList = new ArrayList<AllocationResult>();
						unallocatedEmpList = form.empSearch
								.getUnAllocatedValues();

						DateTimeFormat format = DateTimeFormat
								.getFormat("yyyy-MM-dd");
						format = DateTimeFormat.getFormat("M");
						Date payRollDate = form.salaryPeriod.getValue();
						System.out.println("PAYROLL DATE    :: -- "
								+ payRollDate);

						String month = format.format(payRollDate);
						int intMonth = Integer.parseInt(month);
						String monthName = getMonthName(intMonth);

						Console.log("PAYROLL MONTH :: " + monthName + ""
								+ intMonth);

						System.out.println("PAYROLL MONTH  -- " + intMonth
								+ " :: " + monthName);
						empTRList = new ArrayList<EmployeeTimeReportDetails>();
						if (unallocatedEmpList.size() != 0) {
							for (int i = 0; i < unallocatedEmpList.size(); i++) {
								System.out.println("EMP ID "
										+ unallocatedEmpList.get(i)
												.getEmpCount()
										+ " "
										+ unallocatedEmpList.get(i)
												.getFullName());
								getTimeReportDetails(
										unallocatedEmpList.get(i).getEmpCount(),
										unallocatedEmpList.get(i).getFullName(),
										monthName);
							}
						}
					}
				});
		return lis;
	}

	private void updateTable(ArrayList<AllocationResult> result) {
		List<EmployeeInfo> allocatedEmployees = new ArrayList<EmployeeInfo>();
		List<AllocationResult> unallocatedEmployees = new ArrayList<AllocationResult>();

		for (AllocationResult res : result) {
			if (res.status == true) {
				System.out.println("Employee " + res.getFullName() + "--");
				System.out.println("Employee " + res.status);
				allocatedEmployees.add(res.getInfo());
			} else {
				System.out.println("Employee " + res.getFullName() + "00");
				System.out.println("Employee " + res.status);
				unallocatedEmployees.add(res);
			}
		}
		form.empSearch.setAllocatedValues(allocatedEmployees);
		form.empSearch.setUnAllocatedValues(unallocatedEmployees);
	}

	public boolean validateTimeReport() {
		int cntr = 0;
		String msg = "";
		if (empTRList.size() != 0) {
			for (int i = 0; i < empTRList.size(); i++) {
				if (empTRList.get(i).isValidForPayRoll() == false) {
					msg = msg + "Time Report for "
							+ empTRList.get(i).getEmpName()
							+ " is not filled/approved !" + "\n";
					cntr++;
				}
			}
			if (cntr > 0) {
				form.showDialogMessage(msg);
				return false;
			}
		}
		return true;
	}

	private void getTimeReportDetails(final int empId, final String empName,
			final String monthName) {
		Console.log("INSIDE TIME REPORT");
		MyQuerry querry = new MyQuerry();
		Filter temp = null;
		Vector<Filter> filtervec = new Vector<Filter>();

		temp = new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(model.getCompanyId());
		filtervec.add(temp);

		temp = new Filter();
		temp.setIntValue(empId);
		temp.setQuerryString("empid");
		filtervec.add(temp);

		temp = new Filter();
		temp.setStringValue(monthName);
		temp.setQuerryString("month");
		filtervec.add(temp);

		temp = new Filter();
		temp.setStringValue(TimeReport.APPROVED);
		temp.setQuerryString("status");
		filtervec.add(temp);

		querry.setQuerryObject(new TimeReport());
		querry.setFilters(filtervec);

		asyncgen.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("Unexpected Error !");
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("TimeReport Result Size :: "
								+ result.size());
						Console.log("TimeReport Result Size :: "
								+ result.size());
						ArrayList<Date> dateList = new ArrayList<Date>();
						if (result.size() != 0) {
							for (SuperModel model : result) {
								TimeReport entity = (TimeReport) model;
								System.out.println("TIME SHEET SIZE :: "
										+ entity.getTimeReportEntry().size());
								for (int i = 0; i < entity.getTimeReportEntry()
										.size(); i++) {
									dateList.add(entity.getTimeReportEntry()
											.get(i).getDate());
								}
							}
							System.out.println("TOTAL DATE SHEET SIZE :: "
									+ dateList.size());
							Console.log("TOTAL DATE SHEET SIZE :: "
									+ dateList.size());
							Collections.sort(dateList);

							boolean isValid = checkingDatesInTimeReport(
									dateList, monthName);

							if (isValid == false) {
								EmployeeTimeReportDetails empTR = new EmployeeTimeReportDetails();
								empTR.setEmpId(empId);
								empTR.setEmpName(empName);
								empTR.setValidForPayRoll(false);
								empTRList.add(empTR);
							} else {
								EmployeeTimeReportDetails empTR = new EmployeeTimeReportDetails();
								empTR.setEmpId(empId);
								empTR.setEmpName(empName);
								empTR.setValidForPayRoll(true);
								empTRList.add(empTR);
							}
						} else {
							EmployeeTimeReportDetails empTR = new EmployeeTimeReportDetails();
							empTR.setEmpId(empId);
							empTR.setEmpName(empName);
							empTR.setValidForPayRoll(false);
							empTRList.add(empTR);
						}
					}

				});
	}

	public boolean checkingDatesInTimeReport(ArrayList<Date> dateList,
			String month) {
		Console.log("INSIDE CHECKING TIME REPORT");
		DateTimeFormat fmt = DateTimeFormat.getFormat("yyyy-MM-dd");
		Console.log("NEW MONTH NUMBER : " + getMonthNumber(month));
		String stDt = calulateStartDateOfMonth(getMonthNumber(month));
		String enDt = calulateEndDateOfMonth(getMonthNumber(month));

		Date startDate = fmt.parse(stDt);
		Date endDate = fmt.parse(enDt);

		System.out.println("MONTH START DATE :: " + startDate);
		System.out.println("MONTH END DATE :: " + endDate);

		Console.log("MONTH START DATE :: " + startDate);
		Console.log("MONTH END DATE :: " + endDate);

		int noOfDays = CalendarUtil.getDaysBetween(startDate, endDate);
		noOfDays++;

		System.out.println("NO OF DAYS IN MONTH :: " + noOfDays);
		Console.log("NO OF DAYS IN MONTH :: " + noOfDays);

		// Monthly Days Loop
		for (int i = 0; i < noOfDays; i++) {
			boolean isDate = dateList.contains(startDate);
			if (isDate == false) {
				System.out.println("DATE IS NOT IN TR " + startDate);
				Console.log("DATE IS NOT IN TR " + startDate);
				return false;
			}

			CalendarUtil.addDaysToDate(startDate, 1);
		}
		return true;
	}

	public String calulateEndDateOfMonth(int selectedMonth) {
		DateTimeFormat fmt = DateTimeFormat.getFormat("yyyy-MM-dd");
		String endDate;
		Date date = new Date();
		date.setMonth(selectedMonth + 1);
		CalendarUtil.setToFirstDayOfMonth(date);
		CalendarUtil.addDaysToDate(date, -1);
		endDate = fmt.format(date);
		Console.log("END DATE OF MONTH " + endDate);
		return endDate;
	}

	public String calulateStartDateOfMonth(int selectedMonth) {
		DateTimeFormat fmt = DateTimeFormat.getFormat("yyyy-MM-dd");
		String startDate;
		Date date = new Date();
		date.setDate(1);
		date.setMonth(selectedMonth);
		startDate = fmt.format(date);
		Console.log("START DATE OF MONTH " + startDate);
		return startDate;
	}

	public int getMonthNumber(String monthName) {
		int monthNo;
		switch (monthName) {
		case "January":
			monthNo = 0;
			break;
		case "February":
			monthNo = 1;
			break;
		case "March":
			monthNo = 2;
			break;
		case "April":
			monthNo = 3;
			break;
		case "May":
			monthNo = 4;
			break;
		case "June":
			monthNo = 5;
			break;
		case "July":
			monthNo = 6;
			break;
		case "August":
			monthNo = 7;
			break;
		case "September":
			monthNo = 8;
			break;
		case "October":
			monthNo = 9;
			break;
		case "November":
			monthNo = 10;
			break;
		case "December":
			monthNo = 11;
			break;

		default:
			throw new IllegalArgumentException("Invalid Month ");
		}
		return monthNo;
	}

	public String getMonthName(int monthNo) {
		String monthName = null;

		switch (monthNo) {
		case 1:
			monthName = "January";
			break;
		case 2:
			monthName = "February";
			break;
		case 3:
			monthName = "March";
			break;
		case 4:
			monthName = "April";
			break;
		case 5:
			monthName = "May";
			break;
		case 6:
			monthName = "June";
			break;
		case 7:
			monthName = "July";
			break;
		case 8:
			monthName = "August";
			break;
		case 9:
			monthName = "September";
			break;
		case 10:
			monthName = "October";
			break;
		case 11:
			monthName = "November";
			break;
		case 12:
			monthName = "December";
			break;

		}

		return monthName;
	}

	private void reactOnCreateAccountingInterface() {
		payrollAccInterfacePopup.showPopUp();
		payrollAccInterfacePopup.getEmpTable().getDataprovider().getList()
				.clear();
	}

	private void createAccountingInterface() {
		form.showWaitSymbol();
		GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry querry = new MyQuerry();
		Filter filter = null;
		Vector<Filter> filterVec = new Vector<Filter>();
		filter = new Filter();
		filter.setQuerryString("branch");
		filter.setStringValue(payrollAccInterfacePopup.getOlbbBranch().getValue());
		filterVec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("accountType");
		filter.setStringValue(payrollAccInterfacePopup.getOlbNumberRange().getValue());
		filterVec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("documentGL");
		filter.setStringValue(DateTimeFormat.getFormat("yyyy-MMM").format(payrollAccInterfacePopup.getDbPeriod().getValue()));
		filterVec.add(filter);
		
		querry.setFilters(filterVec);
		querry.setQuerryObject(new AccountingInterface());
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				AccountingInterface accountingInterface = null;
				if(result.size() >0){
					accountingInterface = (AccountingInterface)result.get(0);
				}
				if(accountingInterface != null 
					&& accountingInterface.getStatus().equalsIgnoreCase(AccountingInterface.TALLYSYNCED)){
					form.hideWaitSymbol();
					payrollAccInterfacePopup.hidePopUp();
					form.showDialogMessage("Accounting Interface for entered period and branch is already synched.");
				}else if(accountingInterface != null 
						&& accountingInterface.getStatus().equalsIgnoreCase(AccountingInterface.TALLYCREATED)){
					//int count = accountingInterface.getCount();
					accountingInterface = getAccountingInterface(accountingInterface);
					saveAccountingInterface(accountingInterface);
					
				}else{
					accountingInterface = getAccountingInterface(new AccountingInterface());
					saveAccountingInterface(accountingInterface);
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});	
						
		
	}

	private void reactOnGoButton() {
		if(payrollAccInterfacePopup.getDbPeriod().getValue() == null){
			form.showDialogMessage("Please select month.");
			return;
		}
		if (payrollAccInterfacePopup.getOlbbBranch().getSelectedIndex() == 0) {
			form.showDialogMessage("Please select branch.");
			return;
		}
		if (payrollAccInterfacePopup.getOlbNumberRange().getSelectedIndex() == 0) {
			form.showDialogMessage("Please select number range.");
			return;
		}
	
		GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry querry = payrollAccInterfacePopup.getQuerry();
		service.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						// List<PaySlip> array =
						getSalarySlipDetails(result);
						// payrollAccInterfacePopup.getEmpTable().getDataprovider().setList(array);
					}

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub

					}
				});
	}

	private void getSalarySlipDetails(final ArrayList<SuperModel> paySlipList) {
		final List<PaySlip> array = new ArrayList<PaySlip>();
		final Map<Integer, Employee> empMap = new HashMap<Integer, Employee>();
		final Map<Integer, Double> compContributionMap = new HashMap<Integer, Double>();
		String numRange = "", branch = "";
		if (payrollAccInterfacePopup.getOlbNumberRange().getSelectedIndex() != 0) {
			numRange = payrollAccInterfacePopup.getOlbNumberRange().getValue();
		}
		if (payrollAccInterfacePopup.getOlbbBranch().getSelectedIndex() != 0) {
			branch = payrollAccInterfacePopup.getOlbbBranch().getValue();
		}
		for (Employee emp : LoginPresenter.globalEmployee) {
			if (emp.getNumberRange().equals(numRange)
					&& emp.getBranchName().equals(branch)) {
				empMap.put(emp.getCount(), emp);
			}
		}
		GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry querry = payrollAccInterfacePopup.getCompContributionQuerry();
		service.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						double value = 0;
						for (SuperModel model : result) {
							CompanyPayrollRecord compPayroll = (CompanyPayrollRecord) model;
							companyDeductionList.add(compPayroll);
							if (compContributionMap.containsKey(compPayroll
									.getEmpId())) {
								value = compContributionMap.get(compPayroll
										.getEmpId());
								value += compPayroll.getCtcAmount();
								compContributionMap.put(compPayroll.getEmpId(),
										value);
							} else {
								compContributionMap.put(compPayroll.getEmpId(),
										compPayroll.getCtcAmount());
							}
						}
						double totalContribution = 0;
						for (SuperModel s : paySlipList) {
							PaySlip slip = (PaySlip) s;
							if (empMap.containsKey(slip.getEmpid())) {
								if (compContributionMap.containsKey(slip
										.getEmpid())) {
									totalContribution = compContributionMap
											.get(slip.getEmpid());
									slip.setTotalCompanyContribution(totalContribution);
								}
								array.add(slip);
							}
						}
						payrollAccInterfacePopup.getEmpTable()
								.getDataprovider().setList(array);
					}

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub

					}
				});

	}

	private AccountingInterface getAccountingInterface(AccountingInterface tallyentity) {

		//AccountingInterface tallyentity = new AccountingInterface();
		List<PaySlip> paySlipList = payrollAccInterfacePopup.getEmpTable().getDataprovider().getList();
		ArrayList<GeneralKeyValueBean> valueBean = new ArrayList<GeneralKeyValueBean>();
		Map<String ,Double> employeeDeductionMap = new HashMap<String ,Double>();
		Map<String ,Double> companydeductionMap = new HashMap<String ,Double>();
		Map<String ,String> componentMap = new HashMap<String ,String>();
		
		double amount = 0;
		double totalSalary = 0;
		double totalGrossSalary = 0;
		for(PaySlip paySlip :paySlipList){
			for(CtcComponent comp : paySlip.getDeductionList()){
				if(employeeDeductionMap.containsKey(comp.getShortName())){
					amount = employeeDeductionMap.get(comp.getShortName());
					employeeDeductionMap.put(comp.getShortName(), comp.getActualAmount() + amount );
				}else{
					employeeDeductionMap.put(comp.getShortName(), comp.getActualAmount());
				}
				componentMap.put(comp.getShortName() , comp.getName());
			}
			totalSalary += paySlip.getGrossEarningActualWithoutOT();
			totalGrossSalary += paySlip.getNetEarningWithoutOT();
		}
		for(CompanyPayrollRecord record : companyDeductionList){
			if(companydeductionMap.containsKey(record.getCtcCompShortName())){
				amount = companydeductionMap.get(record.getCtcCompShortName());
				companydeductionMap.put(record.getCtcCompShortName(), record.getCtcAmount() + amount);
			}else{
				companydeductionMap.put(record.getCtcCompShortName(), record.getCtcAmount());
			}
			componentMap.put(record.getCtcCompShortName() , record.getCtcComponentName());
		}
		for (Map.Entry<String,Double> entry : employeeDeductionMap.entrySet())  {
            Console.log("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            
            GeneralKeyValueBean bean = new GeneralKeyValueBean();
            if(entry.getKey().equalsIgnoreCase("PT")){
                  bean.setName(entry.getKey());
                  bean.setValue(-entry.getValue());
                  bean.setGroupName("Professional Tax Payable");
                  valueBean.add(bean);
            }else if(entry.getKey().equals("TDS")){
            	 bean.setName(entry.getKey());
                 bean.setValue(-entry.getValue());
                 bean.setGroupName("TDS on Salary - 192B");
                 valueBean.add(bean);
            }else if(entry.getKey().equalsIgnoreCase("LWF") || entry.getKey().equalsIgnoreCase("PF") || entry.getKey().equalsIgnoreCase("ESIC")){
            	 bean.setName(entry.getKey());
                 bean.setValue(-entry.getValue());
                 bean.setGroupName(entry.getKey()+" "+"Employee Contribution");
                 valueBean.add(bean);
            }else{
            	bean.setName(componentMap.get(entry.getKey()));
                bean.setValue(-entry.getValue());
                bean.setGroupName(componentMap.get(entry.getKey()));
                valueBean.add(bean);
            }
		}
		
		for (Map.Entry<String,Double> entry : companydeductionMap.entrySet())  {
            Console.log("Key = " + entry.getKey() + ", Value = " + entry.getValue());
           
            GeneralKeyValueBean bean = new GeneralKeyValueBean();
            bean.setName(entry.getKey());
            bean.setValue(-entry.getValue());
            bean.setGroupName(entry.getKey()+" "+"Employer Contribution");
            valueBean.add(bean);
            GeneralKeyValueBean bean1 = new GeneralKeyValueBean();
            bean1.setName(componentMap.get(entry.getKey()));
            bean1.setValue(entry.getValue());
            bean1.setGroupName(entry.getKey()+" "+"Expense");
            bean1.setStatus(true);
            valueBean.add(bean1);
		}
		tallyentity.setPayRollList(valueBean);
		
		tallyentity.setAccountingInterfaceCreationDate(new Date());
		 
		tallyentity.setAccountingInterfaceCreatedBy(LoginPresenter.loggedInUser);
		
		tallyentity.setDocumentStatus("Freezed");
		
		tallyentity.setStatus(AppConstants.STATUS);
		
		tallyentity.setModule(AppConstants.HRMODULE);
		
		tallyentity.setDocumentType("Payroll");
		
		tallyentity.setDocumentID(0);

		tallyentity.setDocumentTitle("");
		
		tallyentity.setDocumentDate(payrollAccInterfacePopup.getDbPeriod().getValue());
		
		tallyentity.setDocumentGL(DateTimeFormat.getFormat("yyyy-MMM").format(payrollAccInterfacePopup.getDbPeriod().getValue()));
	
		tallyentity.setReferenceDocumentNo1("");
		
		tallyentity.setReferenceDocumentDate1(null);
		
		tallyentity.setReferenceDocumentType1("");
		
		tallyentity.setReferenceDocumentNo2("");
		
		tallyentity.setReferenceDocumentDate2(null);
		
		tallyentity.setReferenceDocumentType2("");
		
		tallyentity.setAccountType("");
	
		tallyentity.setCustomerID(0);
		
		tallyentity.setCustomerName("");
		
		tallyentity.setCustomerCell(0);
		
		tallyentity.setVendorID(0);
	
		tallyentity.setVendorName("");

		tallyentity.setVendorCell(0);
		
		tallyentity.setEmployeeID(0);
		
		tallyentity.setEmployeeName("");
		
		tallyentity.setBranch(payrollAccInterfacePopup.getOlbbBranch().getValue(payrollAccInterfacePopup.getOlbbBranch().getSelectedIndex()));

		tallyentity.setPersonResponsible(LoginPresenter.loggedInUser);
		
		tallyentity.setRequestedBy(LoginPresenter.loggedInUser);
		
		tallyentity.setApprovedBy(LoginPresenter.loggedInUser);
	
		tallyentity.setPaymentMethod("");
		
		tallyentity.setPaymentDate(null);
		
		tallyentity.setChequeNumber("");
	
		tallyentity.setChequeDate(null);
		
		tallyentity.setBankName("");
	
		tallyentity.setBankAccount("");
		
		tallyentity.setTransferReferenceNumber(0);
		
		tallyentity.setTransactionDate(null);
		
		tallyentity.setContractStartDate(null);
		
		tallyentity.setContractEndDate(null);
		
		tallyentity.setProductID(0);
		
		tallyentity.setProductCode("");
		
		tallyentity.setProductName("");
		
		tallyentity.setQuantity(0);
		
		tallyentity.setProductDate(null);
		
		tallyentity.setProdDuration(0);
		
		tallyentity.setProdServices(0);
		
		tallyentity.setUnitOfMeasurement("");
		
		tallyentity.setProductPrice(0);
		
		// if(VATpercent!=0){
		// tallyentity.setVatPercent(0);
		// }
		//
		// if(VATamount!=0){
		// tallyentity.setVatAmount(VATamount);
		// }
		//
		// if(VATglAccount!=0){
		// tallyentity.setVatGlaccount(VATglAccount);
		// }
		//
		// if(serviceTaxPercent!=0){
		// tallyentity.setServiceTaxPercent(serviceTaxPercent);
		// }
		//
		// if(serviceTaxAmount!=0){
		// tallyentity.setServiceTaxAmount(serviceTaxAmount);
		// }
		//
		// if(serviceTaxGLaccount!=0){
		// tallyentity.setServiceTaxGlAccount(serviceTaxGLaccount);
		// }
		//
		// if(cForm!=null){
		 tallyentity.setcForm("");
		// }
		//
		// if(cFormPercent!=0){
		// tallyentity.setcFormPercent(cFormPercent);
		// }
		//
		// if(cFormAmount!=0){
		// tallyentity.setcFormAmount(cFormAmount);
		// }
		//
		// if(cFormGlAccount!=0){
		// tallyentity.setcFormGlaccount(cFormGlAccount);
		// }
		//
		// if(totalAmount!=0){
		tallyentity.setTotalAmount(totalSalary);
		// }
		//
		// if(netPayable!=0){
		tallyentity.setNetPayable(totalGrossSalary);
		// }
		//
		//
		// if(contractCategory!=null){
		tallyentity.setConractCategory("");
		// }
		//
		// if(amountRecieved!=0){
		// tallyentity.setAmountRecieved(amountRecieved);
		// }
		// if(baseAmount!=0){
		// tallyentity.setBaseAmount(baseAmount);
		// }
		// if(tdsPercentage!=0){
		// tallyentity.setTdsPercentage(tdsPercentage);
		// }
		// if(tdsAmount!=0){
		// tallyentity.setTdsAmount(tdsAmount);
		// }
		//
		// if(otherCharges1!=null){
		tallyentity.setOtherCharges1("");
		// }
		//
		// if(otherChargesAmount1!=0){
		// tallyentity.setOtherChargesAmount1(otherChargesAmount1);
		// }
		//
		// if(otherCharges2!=null){
		tallyentity.setOtherCharges2("");
		// }
		//
		// if(otherChargesAmount2!=0){
		// tallyentity.setOtherChargesAmount2(otherChargesAmount2);
		// }
		//
		// if(otherCharges3!=null){
		 tallyentity.setOtherCharges3("");
		// }
		//
		// if(otherChargesAmount3!=0){
		// tallyentity.setOtherChargesAmount3(otherChargesAmount3);
		// }
		//
		// if(otherCharges4!=null){
		tallyentity.setOtherCharges4("");
		// }
		//
		// if(otherChargesAmount4!=0){
		// tallyentity.setOtherChargesAmount4(otherChargesAmount4);
		// }
		//
		// if(otherCharges5!=null){
		tallyentity.setOtherCharges5("");
		// }
		//
		// if(otherChargesAmount5!=0){
		// tallyentity.setOtherChargesAmount5(otherChargesAmount5);
		// }
		//
		// if(otherCharges6!=null){
		tallyentity.setOtherCharges6("");
		// }
		//
		// if(otherChargesAmount6!=0){
		// tallyentity.setOtherChargesAmount6(otherChargesAmount6);
		// }
		//
		// if(otherCharges7!=null){
		tallyentity.setOtherCharges7("");
		// }
		//
		// if(otherChargesAmount7!=0){
		// tallyentity.setOtherChargesAmount7(otherChargesAmount7);
		// }
		//
		// if(otherCharges8!=null){
		tallyentity.setOtherCharges8("");
		// }
		//
		// if(otherChargesAmount8!=0){
		// tallyentity.setOtherChargesAmount8(otherChargesAmount8);
		// }
		//
		// if(otherCharges9!=null){
		tallyentity.setOtherCharges9("");
		// }
		//
		// if(otherChargesAmount9!=0){
		// tallyentity.setOtherChargesAmount9(otherChargesAmount9);
		// }
		//
		// if(otherCharges10!=null){
		tallyentity.setOtherCharges10("");
		// }
		//
		// if(otherChargesAmount10!=0){
		// tallyentity.setOtherChargesAmount10(otherChargesAmount10);
		// }
		//
		// if(otherCharges11!=null){
		tallyentity.setOtherCharges11("");
		// }
		//
		// if(otherChargesAmount11!=0){
		// tallyentity.setOtherChargesAmount11(otherChargesAmount11);
		// }
		//
		// if(otherCharges12!=null){
		tallyentity.setOtherCharges12("");
		// }
		//
		// if(otherChargesAmount12!=0){
		// tallyentity.setOtherChargesAmount12(otherChargesAmount12);
		// }
		//
		// if(Address!=null){
		tallyentity.setAddress("");
		// }
		//
		// if(Locality!=null){
		tallyentity.setLocality("");
		// }
		//
		// if(Landmark!=null){
		tallyentity.setLandmark("");
		// }
		//
		// if(Country!=null){
		tallyentity.setCountry("");
		// }
		//
		// if(State!=null){
		tallyentity.setState("");
		// }
		//
		// if(City!=null){
		tallyentity.setCity("");
		// }
		//
		// if(Pincode!=0){
		tallyentity.setPincode(0);
		// }
		//
		// if(billingPeroidFromDate!=null){
		tallyentity.setBillingPeroidFromDate(null);
		// }
		//
		// if(billingPeroidToDate!=null){
		tallyentity.setBillingPeroidToDate(null);
		// }
		//
		// if(warehouse!=null&&warehouse.trim().length()>0){
		// tallyentity.setWareHouse(warehouse.trim());
		// }
		// if(warehouseCode!=null&&warehouseCode.trim().length()>0){
		// tallyentity.setWareHouseCode(warehouseCode);
		// }
		// if(productRefid!=null&&productRefid.trim().length()>0){
		// tallyentity.setProductRefId(productRefid.trim());
		// }
		// if(direction!=null&&direction.trim().length()>0){
		// tallyentity.setDirection(direction.trim());
		// }
		// if(sourceSystem!=null&&sourceSystem.trim().length()>0){
		// tallyentity.setSourceSystem(sourceSystem.trim());
		// }
		// if(destinationSystem!=null&&destinationSystem.trim().length()>0){
		// tallyentity.setDestinationSystem(destinationSystem.trim());
		// }
		//
		// if(referenceDocumentNumber2!=null){
		tallyentity.setCustomerRefId("");
		// }
		//
		// if(Remark!=null){
		if(payrollAccInterfacePopup.getOlbNumberRange().getSelectedIndex()!=0){
		 tallyentity.setAccountType(payrollAccInterfacePopup.getOlbNumberRange().getValue());
		}
		// }
		//
		// if(hsnSac !=null){
		 tallyentity.setHsnSac("");
		// }
		// if(gstn !=null){
		// tallyentity.setGstn(gstn);
		// }
		// if(groupName1 !=null){
		// tallyentity.setGroupName1(groupName1);
		// }
		// if(groupName2 !=null){
		// tallyentity.setGroupName2(groupName2);
		// }
		// if(tax1 !=null){
		// tallyentity.setTax1(tax1);
		// }
		// if(tax2!=null){
		// tallyentity.setTax2(tax2);
		// }
		// tallyentity.setDiscount(discount);
		// tallyentity.setIndirectExpenses(indirectExpenses);
		// tallyentity.setCompanyId(companyId);
		// tallyentity.setAddress1(address2);
		// tallyentity.setSaddress(saddrLine1);
		// tallyentity.setSaddress2(saddrLine2);
		// tallyentity.setSlocality(saddrLocality);
		// tallyentity.setSlandmark(saddrLandmark);
		// tallyentity.setScountry(saddrCountry);
		// tallyentity.setSstate(saddrState);
		// tallyentity.setScity(saddrCity);
		// tallyentity.setSpincode(saddrPin);
		return tallyentity;
	}
	private void saveAccountingInterface(AccountingInterface accountingInterface){
		GenricServiceAsync service = GWT.create(GenricService.class);
		service.save(accountingInterface,
				new AsyncCallback<ReturnFromServer>() {

					@Override
					public void onSuccess(ReturnFromServer result) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
						payrollAccInterfacePopup.hidePopUp();
						form.showDialogMessage("Accounting Interface created successfully.");
						
					}

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
						System.out.println("Exception :" + caught);
					}
				});

	}
	
	
	/*** Date 23-2-2019 
	 * @author Amol
	 * added download option for unallocated employee
	 */
	@Override
	public void reactOnDownload(){
		ArrayList<AllocationResult> unallocatedemployeeArray = new ArrayList<AllocationResult>();
		List<AllocationResult> unallocatedEmpList = (List<AllocationResult>)form.empSearch.getUnAllocated().getDataprovider().getList();
		unallocatedemployeeArray.addAll(unallocatedEmpList);
		csvservice.setUnallocatedemployeelist(unallocatedemployeeArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 166;
				Window.open(url, "test", "enabled");
			}
		});
	
	}
	
	
}
