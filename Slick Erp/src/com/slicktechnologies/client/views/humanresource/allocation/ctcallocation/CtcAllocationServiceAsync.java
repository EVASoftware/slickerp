package com.slicktechnologies.client.views.humanresource.allocation.ctcallocation;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;


public interface CtcAllocationServiceAsync {
	public void  getAllocationStatus(ArrayList<Filter>filter,AsyncCallback<ArrayList<AllocationResult>> callback);
	/**
	 * 
	 * @param allocationArray
	 * @param ctcTemplate
	 * @param applicableFromDate
	 * @param applicableToDate
	 * @param effectiveFromDate
	 * @param createdBy
	 * @author Anil , Date : 13-07-2019
	 * adding project name for multisite payroll
	 * @param effectiveToDate
	 * @param projectName
	 * @return
	 */
	public void  allocateCtc(ArrayList<AllocationResult> allocationArray,CTCTemplate ctcTemplate,Date applicableFromDate,Date applicableToDate,Date effectiveFromDate,String createdBy,Date effectiveToDate,String projectName,AsyncCallback<ArrayList<AllocationResult>> callback);
	public void  reviseSalary(ArrayList<EmployeeInfo> allocationArray,CTCTemplate ctcTemplate,Date applicableFromDate,Date applicableToDate,Date effectiveFromDate,String createdBy,Date effectiveToDate,String projectName,boolean tempWiseRevisionFlag,AsyncCallback<ArrayList<EmployeeInfo>> callback);
}
