package com.slicktechnologies.client.views.humanresource.allocation.payslipallocation;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.personlayer.Employee;

@RemoteServiceRelativePath("payslipallocationservice")
public interface PaySlipAllocationService extends RemoteService{
	/**
	 * Date : 24-11-2018 By ANIL
	 * @param isProvisional-if this variable is true that time we will only calculate payroll we will not save payroll
	 */
	public ArrayList<AllocationResult> allocatePaySlip(ArrayList<EmployeeInfo> empinfoarray,String payRollPeriod,Date fromDate,Date toDate,boolean isProvisional);
   
	public ArrayList<AllocationResult> getAllocationStatus(ArrayList<Filter> filter,String salaryPeriod,Date frmDate,Date toDate) ;
	/** date 06.08.2018 added by komal for full and final salary deduction calcaulation**/
	public String saveArrearsData(Employee emp, double noticePeriod , double shortFall , Date dateOfResignation , Date lastWorkingDate);

	/**
	 * Date :14-09-2018 BY Anil 
	 * For freezing payroll
	 */
	public String freezePayroll(ArrayList<EmployeeInfo> empInfoList,String salaryPeriod,String freezeBy);
	
	/**
	 * Date:24-11-2018 By ANIL
	 */
	public String getProvisionalPayroll(ArrayList<AllocationResult> result,String salaryPeriod);

}
