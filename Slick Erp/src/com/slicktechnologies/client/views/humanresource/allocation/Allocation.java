package com.slicktechnologies.client.views.humanresource.allocation;

import java.util.ArrayList;

import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;


/**
 * Represents Allocation.Any Process which Allocates 
 * Something to Employees will extend to this Interface.
 * @author Intel
 *
 */
public interface Allocation 
{
  
  /**
   * Returns List of Allocation Status corresponding to Filters for Particular Allocation. 
   * @param filter filter corresponding to which Employees will be searched for their Allocation status.
   * @return array of list of {@link AllocationResult} corresponding to Filter
   */
  public ArrayList<AllocationResult> getAllocationStatus(ArrayList<Filter> filter);
}
