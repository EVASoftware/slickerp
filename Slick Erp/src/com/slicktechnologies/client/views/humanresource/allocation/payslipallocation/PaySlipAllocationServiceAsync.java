package com.slicktechnologies.client.views.humanresource.allocation.payslipallocation;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipAllocationProcess;
import com.slicktechnologies.shared.common.personlayer.Employee;

public interface PaySlipAllocationServiceAsync {
	/**
	 * Date : 24-11-2018 By ANIL
	 * @param isProvisional-if this variable is true that time we will only calculate payroll we will not save payroll
	 */
	public void allocatePaySlip(ArrayList<EmployeeInfo> empinfoarray,String payRollPeriod,Date fromDate,Date toDate,boolean isProvisional,AsyncCallback<ArrayList<AllocationResult>> callback);
	public void  getAllocationStatus(ArrayList<Filter> filter,String salaryPeriod,Date frmDate,Date toDate,AsyncCallback<ArrayList<AllocationResult>> callback) ;
	/** date 06.08.2018 added by komal for full and final salary deduction calcaulation**/
	public void saveArrearsData(Employee emp ,double noticePeriod , double shortFall , Date dateOfResignation , Date lastWorkingDate, AsyncCallback<String> callback);
	/**
	 * Date :14-09-2018 BY Anil 
	 * For freezing payroll
	 */
	public void freezePayroll(ArrayList<EmployeeInfo> empInfoList,String salaryPeriod,String freezeBy,AsyncCallback<String> callback);
	
	/**
	 * Date:24-11-2018 By ANIL
	 */
	public void getProvisionalPayroll(ArrayList<AllocationResult> result,String salaryPeriod,AsyncCallback<String> callback);
}
