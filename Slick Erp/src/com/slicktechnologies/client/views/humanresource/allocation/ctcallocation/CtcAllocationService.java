package com.slicktechnologies.client.views.humanresource.allocation.ctcallocation;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.client.views.humanresource.allocation.Allocation;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;

@RemoteServiceRelativePath("ctcallocationservice")
public interface CtcAllocationService extends RemoteService,Allocation {
	/**
	 * 
	 * @param allocationArray
	 * @param ctcTemplate
	 * @param applicableFromDate
	 * @param applicableToDate
	 * @param effectiveFromDate
	 * @param createdBy
	 * @author Anil , Date : 13-07-2019
	 * adding project name for multisite payroll
	 * @param effectiveToDate
	 * @param projectName
	 * @return
	 */
	public ArrayList<AllocationResult> allocateCtc(ArrayList<AllocationResult> allocationArray,CTCTemplate ctcTemplate,Date applicableFromDate,Date applicableToDate,Date effectiveFromDate,String createdBy,Date effectiveToDate,String projectName);
	public ArrayList<EmployeeInfo> reviseSalary(ArrayList<EmployeeInfo> allocationArray,CTCTemplate ctcTemplate,Date applicableFromDate,Date applicableToDate,Date effectiveFromDate,String createdBy,Date effectiveToDate,String projectName,boolean tempWiseRevisionFlag);
}
