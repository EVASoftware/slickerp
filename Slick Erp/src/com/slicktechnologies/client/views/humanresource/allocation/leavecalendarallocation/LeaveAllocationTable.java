package com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveAllocationProcess;

public class LeaveAllocationTable extends SuperTable<LeaveAllocationProcess>
{

	TextColumn<LeaveAllocationProcess> userColumn;
	TextColumn<LeaveAllocationProcess> allocatedToColumn;
	TextColumn<LeaveAllocationProcess> validFromColumn;
	TextColumn<LeaveAllocationProcess> validTillColumn;
	TextColumn<LeaveAllocationProcess> systemTimeColumn;
	TextColumn<LeaveAllocationProcess> calendar;
	
	
	public LeaveAllocationTable() {
		super();
		connectToLocal();
	}

	public LeaveAllocationTable(UiScreen<LeaveAllocationProcess> mv) {
		super(mv);
		// TODO Auto-generated constructor stub
	}

	private void createUserColumn()
	{
		userColumn=new TextColumn<LeaveAllocationProcess>() {

			@Override
			public String getValue(LeaveAllocationProcess object) {
			
				return object.getUser();
			}
		};
		table.addColumn(userColumn, "User");
	}
	
	private void createAllocatedToColumn()
	{
		allocatedToColumn=new TextColumn<LeaveAllocationProcess>() {

			@Override
			public String getValue(LeaveAllocationProcess object) {
			
				return object.getAllocatedTo();
			}
		};
		table.addColumn(allocatedToColumn, "Allocated To");
	}
	
	private void createValidfromColumn()
	{
		validFromColumn=new TextColumn<LeaveAllocationProcess>() {

			@Override
			public String getValue(LeaveAllocationProcess object) {
			
				return AppUtility.parseDate(object.getValidFrom());
			}
		};
		table.addColumn(validFromColumn, "Valid From");
	}
	
	private void createValidToColumn()
	{
		validTillColumn=new TextColumn<LeaveAllocationProcess>() {

			@Override
			public String getValue(LeaveAllocationProcess object) {
			
				return AppUtility.parseDate(object.getValidTo());
			}
		};
		table.addColumn(validTillColumn, "Valid Till");
	}
	
	private void createSystemTimeColumn()
	{
		systemTimeColumn=new TextColumn<LeaveAllocationProcess>() {

			@Override
			public String getValue(LeaveAllocationProcess object) {
			
				String pattern="yyyy.MM.dd HH:mm:ss";
				DateTimeFormat format=DateTimeFormat.getFormat(pattern);
				String time=format.format(object.getSystemTime());
				return time;
			}
		};
		table.addColumn(systemTimeColumn, "System Time");
	}
	
	
	private void createColumnCalendar()
	{
		calendar=new TextColumn<LeaveAllocationProcess>() {

			@Override
			public String getValue(LeaveAllocationProcess object) {
			
				return object.getCalendarName()+"";
			}
		};
		table.addColumn(calendar, "Calendar");
	}
	@Override
	public void createTable() {
		
		createUserColumn();
		createColumnCalendar();
		createAllocatedToColumn();
		createValidfromColumn();
		createValidToColumn();
		createSystemTimeColumn();
	}

	@Override
	protected void initializekeyprovider() {
		
		
	}

	@Override
	public void addFieldUpdater() {
		
		
	}

	@Override
	public void setEnable(boolean state) {
		
		
	}

	@Override
	public void applyStyle() {
		
		
	}

	@Override
	public void setView(UiScreen<LeaveAllocationProcess> view) {
		
	}
	
}
