package com.slicktechnologies.client.views.humanresource.allocation;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.client.views.humanresource.allocation.payslipallocation.UnAllocatedEmployeeInformationTable;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;


public class EmployeeSearch extends Composite {
	public EmployeeInfoComposite personInfo;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Department> olbDepartMent;
	public ObjectListBox<Config> olbEmploymentType;
	public ObjectListBox<Config> olbDesignation;
	public ObjectListBox<Country> olbCountry;
	public ObjectListBox<Config> olbRole;
    public AllocatedEmployeesTable allocated;
    public UnAllocatedEmployeeInformationTable unAllocated;
    public Button btnGo;
    public FlowPanel content;
   
    
    /** The form. */
	private FlexForm form;
	
	/**
	 * Date : 12-05-2018 By ANIL
	 * added ctc allocation process flag 
	 */
	boolean isCtcAllocationProcessFlag=false;
	
	/**
	 * Date : 20-07-2018 By ANIL
	 * Added number range process for Sasha ERP
	 */
	ObjectListBox<Config> olbcNumberRange;
	public ObjectListBox<HrProject> project;
	
	/**
	 * Date : 28-07-2018 By ANIL
	 * adding static gender list,which used at payroll for calculating PT 
	 */
	ListBox lbGender;
	
	/*** Date 23-2-2019 
	 * @author Amol
	 * added download option for unallocated employee
	 */
    public Button btnDownload;
	public EmployeeSearch()
	{
		super();
		createGui();
		initWidget(content);
	}
	
	public EmployeeSearch(boolean isCtcAllocationProcess)
	{
		super();
		isCtcAllocationProcessFlag=isCtcAllocationProcess;
		createGui();
		initWidget(content);
	}
	public void initWidget()
	{
		content=new FlowPanel();
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		olbDepartMent=new ObjectListBox<Department>();
		AppUtility.makeSalesPersonListBoxDepartment(olbDepartMent);
		olbEmploymentType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbEmploymentType,Screen.EMPLOYEETYPE);
		olbDesignation=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbDesignation,Screen.EMPLOYEEDESIGNATION);
		olbRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbRole,Screen.EMPLOYEEROLE);
		olbCountry=new ObjectListBox<Country>();
		olbCountry.MakeLive(new MyQuerry(new Vector<Filter>(),new Country()));
		
		if(isCtcAllocationProcessFlag==true){
			allocated=new AllocatedEmployeesTable(true);
		}else{
			allocated=new AllocatedEmployeesTable();
		}
		unAllocated=new UnAllocatedEmployeeInformationTable();
		btnGo=new Button("Go");
		
		olbcNumberRange = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange, Screen.NUMBERRANGE);
		
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
		
		lbGender=new ListBox();
		lbGender.addItem("--SELECT--");
		lbGender.addItem("Male");
		lbGender.addItem("Female");
		lbGender.addItem("Other");
		
		
		btnDownload=new Button("Download");
		
	}
	public void createGui()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(3).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Department",olbDepartMent);
		FormField folbdepartment= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Employment Type",olbEmploymentType);
		FormField folbtype= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Employee Designation",olbDesignation);
		FormField folbdesignation= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Employee Role",olbRole);
		FormField folbrole= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Country",olbCountry);
		FormField folbCountry= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder(); 
		FormField fEmployeeInformation=builder.setlabel("Unprocessed").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
	    builder=new FormFieldBuilder("",unAllocated.getTable());
	    FormField employeeTable=builder.setColSpan(3).build();
	     
	    builder = new FormFieldBuilder(); 
	    FormField falEmployeeInformation=builder.setlabel("Processed").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
	    builder=new FormFieldBuilder("",allocated.getTable());
	    FormField alemployeeTable=builder.setColSpan(3).build();
	     
	    builder = new FormFieldBuilder("",btnGo);
	    FormField btnGo= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();


	    builder = new FormFieldBuilder("Number Range",olbcNumberRange);
		FormField folbcNumberRange= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Project", project);
		FormField fproject = builder.setMandatory(false).setMandatoryMsg("Project is mandatory").setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Gender",lbGender);
		FormField flbGender= builder.setMandatory(false).setMandatoryMsg("Gender is mandetory").setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("",btnDownload);
		FormField fbtnDownload= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		

		FormField[][]fields=new FormField[][]{
				{fpersonInfo},
				{folbCountry,folbBranch,folbdepartment,},
				{folbtype,folbdesignation,folbrole},
				{folbcNumberRange,fproject,flbGender},
				{btnGo},
				{fEmployeeInformation},
				{fbtnDownload},
				{employeeTable},
				{falEmployeeInformation},
				{alemployeeTable},
				
		};
		
      form=new FlexForm(fields,FormStyle.ROWFORM);
		
		applyStyle();
		content.add(form);
	
	}
	public ArrayList<Filter>  getEmployeeFilter()
	{
		ArrayList<Filter> filtervec=new ArrayList<Filter>();
		Filter temp=null;
		temp=new Filter();
		temp.setQuerryString("companyId");
		if(UserConfiguration.getCompanyId()!=null)
			temp.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setBooleanvalue(true);
		temp.setQuerryString("status");
		filtervec.add(temp);
		
		if(lbGender.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(lbGender.getValue(lbGender.getSelectedIndex()).trim());
			temp.setQuerryString("gender");
			filtervec.add(temp);
		}
		
		if(project.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(project.getValue().trim());
			temp.setQuerryString("projectName");
			filtervec.add(temp);
		}

		if(olbcNumberRange.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(olbcNumberRange.getValue().trim());
			temp.setQuerryString("numberRangeAsEmpTyp");
			filtervec.add(temp);
		}

		if(olbDepartMent.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(olbDepartMent.getValue().trim());
			temp.setQuerryString("department");
			filtervec.add(temp);
		}
		
		

		if(olbCountry.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(olbCountry.getValue().trim());
			temp.setQuerryString("country");
			filtervec.add(temp);
		}

		if(olbEmploymentType.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(olbEmploymentType.getValue().trim());
			temp.setQuerryString("employeeType");
			filtervec.add(temp);
		}

		if(olbRole.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbRole.getValue().trim());
			temp.setQuerryString("employeerole");
			filtervec.add(temp);
		}
		if(olbDesignation.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbDesignation.getValue().trim());
			temp.setQuerryString("designation");
			filtervec.add(temp);
		}
		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		


		if(!personInfo.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
			temp.setQuerryString("empCount");
			filtervec.add(temp);
		}

		if(!(personInfo.getName().getValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(personInfo.getName().getValue());
			temp.setQuerryString("fullName");
			filtervec.add(temp);
		}
		if(!personInfo.getPhone().getValue().equals(""))
		{
			temp=new Filter();
			temp.setLongValue(personInfo.getCellNumber());
			temp.setQuerryString("cellNumber");
			filtervec.add(temp);
		}

		
	
		
		return filtervec;
	}
	
	public AllocatedEmployeesTable getAllocated() {
		return allocated;
	}
	public void setAllocated(AllocatedEmployeesTable allocated) {
		this.allocated = allocated;
	}
	public UnAllocatedEmployeeInformationTable getUnAllocated() {
		return unAllocated;
	}
	public void setUnAllocated(UnAllocatedEmployeeInformationTable unAllocated) {
		this.unAllocated = unAllocated;
	}
	
	public void setAllocatedValues(List<EmployeeInfo> employeeInformation)
	{
		if(employeeInformation!=null)
		{
			allocated.connectToLocal();
			allocated.getListDataProvider().setList(employeeInformation);
		}
	}
	
	public void setUnAllocatedValues(List<AllocationResult> employeeInformation)
	{
		if(employeeInformation!=null)
		{
			unAllocated.connectToLocal();
			unAllocated.getListDataProvider().setList(employeeInformation);
		}
	}
	
	public List<EmployeeInfo>  getAllocatedValues()
	{
		if(allocated!=null)
		{
			return allocated.getValue();
		}
		
		return null;
	}
	
	public List<AllocationResult> getUnAllocatedValues()
	{
		if(unAllocated!=null)
		{
			return unAllocated.getValue();
		}
		
		return null;
	}
	
	public void applyStyle()
	{
		form.setWidth("98%");
		
		
	}
	public Button getBtnGo() {
		return btnGo;
	}
	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}

	public Button getBtnDownload() {
		return btnDownload;
	}

	public void setBtnDownload(Button btnDownload) {
		this.btnDownload = btnDownload;
	}
	
	
}
