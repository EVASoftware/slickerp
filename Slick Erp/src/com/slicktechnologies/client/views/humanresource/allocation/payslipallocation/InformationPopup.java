package com.slicktechnologies.client.views.humanresource.allocation.payslipallocation;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;

public class InformationPopup extends AbsolutePanel{
	
	public TextArea info;
	public Button btnOne;
	public Button btnTwo;

	

	public InformationPopup(String header) {
		super();

		btnOne=new Button("Ok");
		btnTwo=new Button("Cancel");
		info=new TextArea();
		info.setHeight("150px");
		info.setWidth("300px");
		
		
		InlineLabel displayMessage = new InlineLabel(header);
		add(displayMessage,10,10);
		
		add(info,10,50);
	
		
		add(btnOne,100,230);
		add(btnTwo,170,230); 
		setSize("320px", "300px");
		this.getElement().setId("form");
		
	
	}
	
	public InformationPopup(String header,boolean callfromRateContract) {
		super();

		btnOne=new Button("Ok");
		btnTwo=new Button("Cancel");
		info=new TextArea();
		info.setHeight("150px");
		info.setWidth("300px");
		
		
		InlineLabel displayMessage = new InlineLabel(header);
		add(displayMessage,10,10);
		
		add(info,10,50);
	
		if(callfromRateContract) {
			add(btnOne,50,380);
			add(btnTwo,230,380);
		}else {
			add(btnOne,100,230);
			add(btnTwo,170,230); 
		}
		setSize("320px", "300px");
		this.getElement().setId("form");
		
	
	}
	
	public TextArea getInfo() {
		return info;
	}
	
	public void setInfo(TextArea info) {
		this.info = info;
	}

	public Button getBtnOne() {
		return btnOne;
	}

	public void setBtnOne(Button btnOne) {
		this.btnOne = btnOne;
	}

	public Button getBtnTwo() {
		return btnTwo;
	}

	public void setBtnTwo(Button btnTwo) {
		this.btnTwo = btnTwo;
	}


}