package com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation;

import java.util.Date;
import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.allocation.EmployeeSearch;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveAllocationProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class LeaveAllocationForm extends FormScreen<LeaveAllocationProcess>implements ClickHandler {

	ObjectListBox<Calendar> olbcalender;
	DateBox fromdate, todate;
	ObjectListBox<LeaveGroup> olbleavegroup;
	
//	ObjectListBox<Overtime> olbovertime;
	
	
	EmployeeSearch search;
//	Button btnallocate;

	public LeaveAllocationForm() {
		super(FormStyle.DEFAULT);
//		super();
		createGui();
		processLevelBar.btnLabels[0].setVisible(true);
		/**
		 * Date :18-07-2018 By ANIL
		 */
		processLevelBar.btnLabels[1].setVisible(true);
	}

	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget() {

		olbcalender = new ObjectListBox<Calendar>();
		Calendar.makeObjectListBoxLive(olbcalender);
		
		fromdate = new DateBox();
		fromdate.setEnabled(false);
		todate = new DateBox();
		todate.setEnabled(false);
		
		olbleavegroup = new ObjectListBox<LeaveGroup>();
		olbleavegroup.MakeLive(new MyQuerry(new Vector<Filter>(),new LeaveGroup()));
		
//		olbovertime = new ObjectListBox<Overtime>();
//		olbovertime.MakeLive(new MyQuerry(new Vector<Filter>(),new Overtime()));
		
		search = new EmployeeSearch();
		search.btnDownload.setVisible(false);//Ashwini Patil hiding the button as there is no code written for button click
		
		//		btnallocate = new Button("Allocate");
		
	}

	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen() {
		initalizeWidget();
		// Token to initialize the processlevel menus.
		this.processlevelBarNames = new String[] { "Allocate" ,"Deallocate"};
		
		// Token to initialize formfield
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingLeaveGroupInformation = fbuilder.setlabel("Leave Group Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Calender Name", olbcalender);
		FormField fcalendername = fbuilder.setMandatory(true)
				.setMandatoryMsg("Calender is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("From Date", fromdate);
		FormField ffromdate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("To Date", todate);
		FormField ftodate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Leave Group", olbleavegroup);
		FormField fleavegroup = fbuilder.setMandatory(true)
				.setMandatoryMsg("Leave Group is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("* Overtime", olbovertime);
//		FormField folbovertime = fbuilder.setMandatory(true)
//				.setMandatoryMsg("Overtime is mandatory!").setRowSpan(0)
//				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingInformation = fbuilder.setlabel("Apply Filters").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", search);
		FormField fapplyfilter = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		// //////////////////////////////Lay Out Making
		// Code///////////////////////////////////////////////////////////////

		FormField[][] formfield = { 
				{ fgroupingLeaveGroupInformation },
				{ fcalendername, ffromdate, ftodate, fleavegroup },
//				{ folbovertime},
				{ fgroupingInformation },
				{ fapplyfilter },
		};

		this.fields = formfield;

	}

	/******////////////////// updateModel & updateView method is not called \\\\\\\\\\\\\\\\\\\*********** 
	
	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(LeaveAllocationProcess model) {

		if (fromdate.getValue() != null)
			model.setValidFrom(fromdate.getValue());
		if (todate.getValue() != null)
			model.setValidTo(todate.getValue());
		model.setSystemTime(new Date());
		if (olbcalender.getValue() != null)
			model.setCalendarName(olbcalender.getValue());

		// model.setUser(UserConfiguration.getUserconfig().getUser().getUserName());

		presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(LeaveAllocationProcess view) {

		presenter.setModel(view);
	}

	
	/***************************************************************************************************/
	
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard")) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}
		/**
		 * @author Anil , Date : 22-06-2019
		 * Earlier in the module name parameter we are passing  HR Admin but it should be HR Configuration
		 */
//		AuthorizationHelper.setAsPerAuthorization(Screen.LEAVEALLOCATION,
//				LoginPresenter.currentModule.trim());
		AuthorizationHelper.setAsPerAuthorization(Screen.LEAVEALLOCATION,"HR Configuration");
	}

	@Override
	public void onClick(ClickEvent event) {

	}

	@Override
	public void setEnable(boolean bool) {
		super.setEnable(bool);
		fromdate.setEnabled(false);
		todate.setEnabled(false);
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		processLevelBar.btnLabels[0].setVisible(true);
		/**
		 * Date :18-07-2018 By ANIL
		 */
		processLevelBar.btnLabels[1].setVisible(true);
	}

	@Override
	public boolean validate() {
		boolean superVal = super.validate();
		if (superVal == false)
			return false;
		// Validation to make sure that no Past Allocation is happening
		// that is the valid till of Calendar should be less then current date.
		if (this.todate.getValue().before(new Date())) {
			/**
			 * @author Anil,Date :23-01-2019
			 * Back dated calendar allocation should be active for generating back dated payroll
			 * for Orion raised By Sonu
			 */
//			showDialogMessage("To date should be greater  than Current Date ! ");
//			return false;
//			showDialogMessage("Please note that you are assigning backdated calendar!");
			/**
			 * End
			 */
		}
		return true;
	}

	/********************************** getter and Setter **************************************************/
	
	public EmployeeSearch getSearch() {
		return search;
	}

	public void setSearch(EmployeeSearch search) {
		this.search = search;
	}

//	public Button getBtnallocate() {
//		return btnallocate;
//	}
//
//	public void setBtnallocate(Button btnallocate) {
//		this.btnallocate = btnallocate;
//	}

	public ObjectListBox<Calendar> getOlbcalender() {
		return olbcalender;
	}

	public void setOlbcalender(ObjectListBox<Calendar> olbcalender) {
		this.olbcalender = olbcalender;
	}

	public DateBox getFromdate() {
		return fromdate;
	}

	public void setFromdate(DateBox fromdate) {
		this.fromdate = fromdate;
	}

	public DateBox getTodate() {
		return todate;
	}

	public void setTodate(DateBox todate) {
		this.todate = todate;
	}

	public ObjectListBox<LeaveGroup> getOlbleavegroup() {
		return olbleavegroup;
	}

	public void setOlbleavegroup(ObjectListBox<LeaveGroup> olbleavegroup) {
		this.olbleavegroup = olbleavegroup;
	}

//	public ObjectListBox<Overtime> getOlbovertime() {
//		return olbovertime;
//	}
//
//	public void setOlbovertime(ObjectListBox<Overtime> olbovertime) {
//		this.olbovertime = olbovertime;
//	}
	
	

}
