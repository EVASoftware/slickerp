package com.slicktechnologies.client.views.humanresource.country;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.client.views.humanresource.country.CountryPresenter.CountryPresenterTable;
import com.slicktechnologies.client.utility.Screen;

public class CountryPresenterTableProxy extends CountryPresenterTable {
	TextColumn<Country> getCountryNameColumn;
	TextColumn<Country> getCurrencyColumn;
	TextColumn<Country> getLanguageColumn;
	TextColumn<Country> getCalendarNameColumn;
	TextColumn<Country> getCountColumn;
	TextColumn<Country> getCountryCodeColumn;
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("getCountryNameColumn"))
			return this.getCountryNameColumn;
		if(varName.equals("getCurrencyColumn"))
			return this.getCurrencyColumn;
		if(varName.equals("getLanguageColumn"))
			return this.getLanguageColumn;
		if(varName.equals("getCalendarNameColumn"))
			return this.getCalendarNameColumn;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		if(varName.equals("getCountryCodeColumn"))
			return this.getCountryCodeColumn;
		
		return null ;
	}
	public CountryPresenterTableProxy()
	{
		super();
	}
	@Override public void createTable() {
		addColumngetCount();
		addColumngetCountryName();
		addColumngetCurrency(); //uncommented by Ashwini Patil Date: 14-04-2022
		addColumngetLanguage();
		//addColumngetCalendarName();
		addColumnCountryCode();
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Country>()
				{
			@Override
			public Object getKey(Country item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetCountryName();
		addSortinggetCurrency();
		addSortinggetLanguage();
		addSortinggetCalendarName();
		addSortinggetCountryCode();
	}
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetCount()
	{
		List<Country> list=getDataprovider().getList();
		columnSort=new ListHandler<Country>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Country>()
				{
			@Override
			public int compare(Country e1,Country e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<Country>()
				{
			@Override
			public String getValue(Country object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				table.setColumnWidth(getCountColumn, 100,Unit.PX);
				getCountColumn.setSortable(true);
	}
	protected void addSortinggetCountryName()
	{
		List<Country> list=getDataprovider().getList();
		columnSort=new ListHandler<Country>(list);
		columnSort.setComparator(getCountryNameColumn, new Comparator<Country>()
				{
			@Override
			public int compare(Country e1,Country e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCountryName()!=null && e2.getCountryName()!=null){
						return e1.getCountryName().compareTo(e2.getCountryName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCountryName()
	{
		getCountryNameColumn=new TextColumn<Country>()
				{
			@Override
			public String getValue(Country object)
			{
				return object.getCountryName()+"";
			}
				};
				table.addColumn(getCountryNameColumn,"Country");
				table.setColumnWidth(getCountryNameColumn, 110, Unit.PX);
				getCountryNameColumn.setSortable(true);
	}
	protected void addSortinggetCurrency()
	{
		List<Country> list=getDataprovider().getList();
		columnSort=new ListHandler<Country>(list);
		columnSort.setComparator(getCurrencyColumn, new Comparator<Country>()
				{
			@Override
			public int compare(Country e1,Country e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCurrency()!=null && e2.getCurrency()!=null){
						return e1.getCurrency().compareTo(e2.getCurrency());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCurrency()
	{
		getCurrencyColumn=new TextColumn<Country>()
				{
			@Override
			public String getValue(Country object)
			{
				return object.getCurrency()+"";
			}
				};
				table.addColumn(getCurrencyColumn,"Currency");
				table.setColumnWidth(getCurrencyColumn, 110, Unit.PX);
				getCurrencyColumn.setSortable(true);
	}
	protected void addSortinggetLanguage()
	{
		List<Country> list=getDataprovider().getList();
		columnSort=new ListHandler<Country>(list);
		columnSort.setComparator(getLanguageColumn, new Comparator<Country>()
				{
			@Override
			public int compare(Country e1,Country e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getLanguage()!=null && e2.getLanguage()!=null){
						return e1.getLanguage().compareTo(e2.getLanguage());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetLanguage()
	{
		getLanguageColumn=new TextColumn<Country>()
				{
			@Override
			public String getValue(Country object)
			{
				return object.getLanguage()+"";
			}
				};
				table.addColumn(getLanguageColumn,"Language");
				table.setColumnWidth(getLanguageColumn, 100,Unit.PX);
				getLanguageColumn.setSortable(true);
	}
	protected void addSortinggetCalendarName()
	{
		List<Country> list=getDataprovider().getList();
		columnSort=new ListHandler<Country>(list);
		columnSort.setComparator(getCalendarNameColumn, new Comparator<Country>()
				{
			@Override
			public int compare(Country e1,Country e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCalendarName()!=null && e2.getCalendarName()!=null){
						return e1.getCalendarName().compareTo(e2.getCalendarName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCalendarName()
	{
		getCalendarNameColumn=new TextColumn<Country>()
				{
			@Override
			public String getValue(Country object)
			{
				if(object.getCalendarName()!=null)
					return object.getCalendarName()+"";
				else
					return null;
			}
				};
				table.addColumn(getCalendarNameColumn,"Calendar Name");
				getCalendarNameColumn.setSortable(true);
	}
	
	private void addColumnCountryCode() {
		getCountryCodeColumn=new TextColumn<Country>()
		{
		@Override
		public String getValue(Country object)
		{
			if(object.getCountryCode()!=null)
				return object.getCountryCode()+"";
			else
				return null;
		}
		};
		table.addColumn(getCountryCodeColumn,"Country Code");
		table.setColumnWidth(getCountColumn, 100,Unit.PX);
		getCountColumn.setSortable(true);
	}
	
	protected void addSortinggetCountryCode()
	{
		List<Country> list=getDataprovider().getList();
		columnSort=new ListHandler<Country>(list);
		columnSort.setComparator(getCountryCodeColumn, new Comparator<Country>()
				{
			@Override
			public int compare(Country e1,Country e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCountryCode()!=null && e2.getCountryCode()!=null){
						return e1.getCountryCode().compareTo(e2.getCountryCode());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
}
