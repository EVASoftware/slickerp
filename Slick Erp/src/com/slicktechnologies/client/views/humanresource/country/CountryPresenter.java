package com.slicktechnologies.client.views.humanresource.country;
import com.google.gwt.event.dom.client.ClickEvent;

import java.util.Vector;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;

import java.util.Vector;

import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.slicktechnologies.shared.common.paymentlayer.*;
import com.google.gwt.event.dom.client.*;
/**
 *  FormTableScreen presenter template
 */
public class CountryPresenter extends FormTableScreenPresenter<Country>  {

	//Token to set the concrete form
	CountryForm form;
	
	public CountryPresenter (FormTableScreen<Country> view,
			Country model) {
		super(view, model);
		form=(CountryForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getCountryQuery());
		form.setPresenter(this);
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}

		
	}
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new Country();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getCountryQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new Country());
		return quer;
	}
	
	public void setModel(Country entity)
	{
		model=entity;
	}
	
	public static void initalize()
	{
			 CountryPresenterTable gentableScreen=new CountryPresenterTableProxy();
			
			CountryForm  form=new  CountryForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			  //// CountryPresenterTable gentableSearch=GWT.create( CountryPresenterTable.class);
			  ////   CountryPresenterSearch.staticSuperTable=gentableSearch;
			  ////     CountryPresenterSearch searchpopup=GWT.create( CountryPresenterSearch.class);
			  ////         form.setSearchpopupscreen(searchpopup);
			
			 CountryPresenter  presenter=new  CountryPresenter  (form,new Country());
			AppMemory.getAppMemory().stickPnel(form);
			
			
		 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.Country")
		 public static class  CountryPresenterTable extends SuperTable<Country> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.Country")
			 public static class  CountryPresenterSearch extends SearchPopUpScreen<  Country>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
				  private void reactTo()
				  {
				}

}
