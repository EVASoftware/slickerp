package com.slicktechnologies.client.views.humanresource.country;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
/**
 * FormTablescreen template.
 */
public class CountryForm extends FormTableScreen<Country>  implements ClickHandler {

	//Token to add the varialble declarations

	// ObjectListBox<Config> lbCurrency;
	IntegerBox ibCountryId;
	TextBox tbCountryName,tbLanguage,tbCountryCode;
	ObjectListBox<Config> olbCurrency; 	//Ashwini Patil Date: 14-04-2022
	//ObjectListBox<LeaveCalendar> olbCalendarName;
	


	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;



	public CountryForm  (SuperTable<Country> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();

	}

	private void initalizeWidget()
	{

		//lbCurrency=new ObjectListBox<Config>();
		//AppUtility.MakeLiveConfig(lbCurrency, Screen.CURRENCY);
		ibCountryId=new IntegerBox();
		ibCountryId.setEnabled(false);
		tbCountryName=new TextBox();
		tbLanguage=new TextBox();
		
		//Ashwini Patil
		 olbCurrency=new ObjectListBox<Config>();
         AppUtility.MakeLiveConfig(olbCurrency,Screen.CURRENCY);
		//olbCalendarName = new ObjectListBox<LeaveCalendar>();
		this.initalizeCalendarNameListBox();
		tbCountryCode = new TextBox();

	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();


		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCountryInformation=fbuilder.setlabel("Country Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("Country Id",ibCountryId);
		FormField fibCountryId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Country",tbCountryName);
		FormField ftbCountryName= fbuilder.setMandatory(true).setMandatoryMsg("Country Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Language",tbLanguage);
		FormField ftbLanguage= fbuilder.setMandatory(true).setMandatoryMsg("Language is mandatory!").setRowSpan(0).setColSpan(0).build();
		//fbuilder = new FormFieldBuilder("* Currency",lbCurrency);
		//FormField flbCurrency= fbuilder.setMandatory(true).setMandatoryMsg("Currency is mandatory !").setRowSpan(0).setColSpan(0).build();
		//fbuilder = new FormFieldBuilder("Calendar Name ",olbCalendarName);
		//FormField folbcalendarname= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Currency",olbCurrency);
		FormField ftbCurrency= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Country Code",tbCountryCode);
		FormField ftbCountryCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingCountryInformation},
				{ftbCountryName,ftbLanguage,ftbCurrency,ftbCountryCode},

		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Country model) 
	{

		if(tbCountryName.getValue()!=null)
			model.setCountryName(tbCountryName.getValue());
		if(tbLanguage.getValue()!=null)
			model.setLanguage(tbLanguage.getValue());

		
		
			/*model.setCalendar(olbCalendarName.getSelectedItem());
			
			if(olbCalendarName.getValue()==null)
				model.setCalendarName("");
			else
				model.setCalendarName(olbCalendarName.getValue());*/
		if(olbCurrency.getValue()!=null)
			model.setCurrency(olbCurrency.getValue());
		if(tbCountryCode.getValue()!=null)
			model.setCountryCode(tbCountryCode.getValue());
			
		manageCountryDropDown(model);
		
		
		presenter.setModel(model);

	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Country view) 
	{

		if(view.getCountryName()!=null)
			tbCountryName.setValue(view.getCountryName());
		if(view.getLanguage()!=null)
			tbLanguage.setValue(view.getLanguage());

		/*if(view.getCalendarName()!=null)
		{
			
			olbCalendarName.setValue(view.getCalendarName());
		}*/
		if(view.getCurrency()!=null)
			olbCurrency.setValue(view.getCurrency());
		
		if(view.getCountryCode()!=null)
			tbCountryCode.setValue(view.getCountryCode());

		presenter.setModel(view);
	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.COUNTRY,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		this.ibCountryId.setValue(presenter.getModel().getCount());
	}


	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub

	}


	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibCountryId.setEnabled(false);

	}

	protected void initalizeCalendarNameListBox()
	{
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Calendar());
		//olbCalendarName.MakeLive(querry);
	}

	
	public void manageCountryDropDown(Country countryModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalCountry.add(countryModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalCountry.size();i++)
			{
				if(LoginPresenter.globalCountry.get(i).getId().equals(countryModel.getId()))
				{
					LoginPresenter.globalCountry.add(countryModel);
					LoginPresenter.globalCountry.remove(i);
				}
			}
		}
	}



}
