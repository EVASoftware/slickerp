package com.slicktechnologies.client.views.humanresource.calendar;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formscreen.*;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar.WeekleyOff;
import com.slicktechnologies.shared.common.personlayer.Employee;

/**
 * FormScreen presenter template.
 */
public class CalendarPresenter extends FormScreenPresenter<Calendar> implements ValueChangeHandler<String> {

	// Token to set the concrete FormScreen class name
	CalendarForm form;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	final CsvServiceAsync csvasync = GWT.create(CsvService.class);

	public CalendarPresenter(FormScreen<Calendar> view, Calendar model) {
		super(view, model);
		form = (CalendarForm) view;
		form.getTbcalendarname().addValueChangeHandler(this);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		if (text.equals(AppConstants.SUBMIT)){
			changeSatatus();
		}
		if (text.equals(AppConstants.NEW)){
			CalendarPresenter.initalize();
		}
	}

	@Override
	public void reactOnPrint() {

	}

	@Override
	public void reactOnEmail() {

	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model = new Calendar();
	}

	public void reactOnDownload() {
		ArrayList<Calendar> custarray = new ArrayList<Calendar>();
		List<Calendar> list = (List<Calendar>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		System.out.println("ddddddddddddd");
		custarray.addAll(list);

		csvasync.setLeaveCalendar(custarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);

			}

			@Override
			public void onSuccess(Void result) {

				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 7;
				Window.open(url, "test", "enabled");

			}
		});
	}

	public static void initalize() {
		AppMemory.getAppMemory().currentState = ScreeenState.NEW;
		CalendarForm form = new CalendarForm();

		CalendarPresenterTable gentable = new CalendarPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		CalendarPresenterSearch.staticSuperTable = gentable;
		CalendarPresenterSearch searchpopup = new CalendarPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		CalendarPresenter presenter = new CalendarPresenter(form,
				new Calendar());
		AppMemory.getAppMemory().stickPnel(form);
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.LeaveCalendar")
	public static class CalendarPresenterSearch extends
			SearchPopUpScreen<Calendar> {

		/*
		 * @Override public MyQuerry getQuerry() { MyQuerry quer=new
		 * MyQuerry(new Vector<Filter>(), new LeaveCalendar()); return quer; }};
		 */

		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			return true;
		}
	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.LeaveCalendar")
	public static class CalendarPresenterTable extends SuperTable<Calendar>
			implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub

		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub

		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub

		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub

		}
	};

	private void changeSatatus() {
		model.setStatus(Calendar.ACTIVE);
		async.save(model, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");

			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				form.setMenuAsPerStatus();
				form.setToViewState();
				form.calStatus.setText(Calendar.ACTIVE);
				form.showDialogMessage("Calendar submitted sucessfully.");

				ManageCalendarGlobalList(model);
			}
		});
	}



	/**
	 * 
	 * @param wo
	 * @param d
	 * @return This method get weeklyoff and date as parameter and return
	 *         weeklyoff day.
	 */
	public static boolean isWeekleyOff(WeekleyOff wo, Date d) {
		DateTimeFormat format = DateTimeFormat.getFormat("c");
		String dayOfWeek = format.format(d);
//		System.out.println("vvv" + dayOfWeek);
		if (dayOfWeek.equals("0"))
			return wo.isSUNDAY();
		if (dayOfWeek.equals("1"))
			return wo.isMONDAY();
		if (dayOfWeek.equals("2"))
			return wo.isTUESDAY();
		if (dayOfWeek.equals("3"))
			return wo.isWEDNESDAY();
		if (dayOfWeek.equals("4"))
			return wo.isTHRUSDAY();
		if (dayOfWeek.equals("5"))
			return wo.isFRIDAY();
		if (dayOfWeek.equals("6"))
			return wo.isSATAURDAY();
		return false;
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		ScreeenState scr = AppMemory.getAppMemory().currentState;

		if (scr.equals(ScreeenState.NEW)) {
			if (event.getSource().equals(form.getTbcalendarname())) {
				validateCalendarName();
			}
		}
		
	}
	private void validateCalendarName() {
		if (!form.getTbcalendarname().getValue().equals("")) {
			String custnameval = form.getTbcalendarname().getValue();
			if (custnameval != null) {

				final MyQuerry querry = new MyQuerry();
				Vector<Filter> filtervec = new Vector<Filter>();
				Filter temp = null;

				temp = new Filter();
				temp.setQuerryString("companyId");
				temp.setLongValue(model.getCompanyId());
				filtervec.add(temp);

				temp = new Filter();
				temp.setQuerryString("calName");
				temp.setStringValue(custnameval);
				filtervec.add(temp);

				querry.setFilters(filtervec);
				querry.setQuerryObject(new Calendar());
				async.getSearchResult(querry,
						new AsyncCallback<ArrayList<SuperModel>>() {
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");

							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								System.out.println("Success" + result.size());
								if (result.size() != 0) {
									form.showDialogMessage("Calendar Name Already Exists");
									form.getTbcalendarname().setValue("");
									
								}
								if (result.size() == 0) {
								}
							}
						});
			}
		}
	}
	
	
	protected void ManageCalendarGlobalList(Calendar model) {
		LoginPresenter.globalCalendarlist.add(model);
	}
	

}
