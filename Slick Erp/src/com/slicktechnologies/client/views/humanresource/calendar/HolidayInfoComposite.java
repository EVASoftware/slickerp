package com.slicktechnologies.client.views.humanresource.calendar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.itextpdf.text.log.SysoCounter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.HolidayType;

public class HolidayInfoComposite extends Composite implements ClickHandler, HasValue<ArrayList< Holiday>>,CompositeInterface {
	
private FlowPanel content;
	
	private Button addButton;
	private HolidayTable holidayTable;
	private TextBox tbholidayname;
	private ObjectListBox<HolidayType> olbHolidayType;
	private DateBox dbholidaydate;
	DateTimeFormat df =  DateTimeFormat.getFormat("dd-MM-yyyy");
	private CalendarForm form;
	
	
	
	
	public HolidayInfoComposite() {
		    content=new FlowPanel();
		    initWidget(content);
		    
		    createTableStructure();
		    holidayTable.connectToLocal();
		    
		   
		    applyStyle();
	}
	
	private void createTableStructure()
	{
        Grid prodgrid = new Grid(2,4);
		
		InlineLabel hname = new InlineLabel("Holiday Name");
		InlineLabel htype = new InlineLabel("Holiday Type");
		InlineLabel hdate = new InlineLabel("Holiday Date");
		
		tbholidayname=new TextBox();
		olbHolidayType=new ObjectListBox<HolidayType>();
		this.initalizeHolidayTypeListBox();
		dbholidaydate=new DateBoxWithYearSelector();
		dbholidaydate.setFormat(new DateBox.DefaultFormat(df));
		addButton= new Button("Add");
		addButton.addClickHandler(this);
		
		prodgrid.setWidget(0,0,hname);
		prodgrid.setWidget(1, 0,tbholidayname);
		prodgrid.setWidget(0,1,htype);
		prodgrid.setWidget(1, 1,olbHolidayType);
		prodgrid.setWidget(0,2,hdate);
		prodgrid.setWidget(1, 2,dbholidaydate);
		prodgrid.setWidget(0,3,new InlineLabel("Add"));
		prodgrid.setWidget(1, 3,addButton);
		
		prodgrid.setCellPadding(3);
		prodgrid.setCellSpacing(3);
		content.add(prodgrid);
		
		holidayTable =new HolidayTable();
		content.add(holidayTable.content);
	}

	@Override
	public void onClick(ClickEvent event) {
			
		String holiDayName=this.tbholidayname.getText().trim();
		 if(holiDayName.equals(""))
		 {
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Holiday Name Cannot be empty !");
		     return; 
		 }
		 
		 String holidayType=olbHolidayType.getValue();
		 if(holidayType==null)
		 {
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Holiday Type Cannot be empty !");
		     return;  
		 }
		 
		 Date holidayDate=dbholidaydate.getValue();
		 if(holidayDate==null)
		 {
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Holiday Date Cannot be empty !");
		     return;  
		 }
		 Holiday holiday=new Holiday();
		  Date convDate=df.parse(df.format(holidayDate));
		  
		  if(form.dbstartdate.getValue()==null ||form.dbenddate.getValue()==null)
			 {
				 final GWTCAlert alert = new GWTCAlert(); 
			     alert.alert("Please Select Calendar Date Range !");
			     return;  
			 }
		 Date fromDate=form.dbstartdate.getValue();
		 Date toDate=form.dbenddate.getValue();
		 /**Added by sheetal:03-01-2022
		  * Des:Holiday List Information Error is coming while creating calendar**/
		 if(AppUtility.formatDate(AppUtility.parseDate(convDate)).before(AppUtility.formatDate(AppUtility.parseDate(fromDate))))
	//	 if(convDate.before(fromDate)||convDate.after(toDate))
		 {
			 System.out.println("Holiday From Date "+convDate);
			 System.out.println("From Date "+fromDate);
			 System.out.println("To Date "+toDate);
			 System.out.println("Before From Date "+convDate.before(fromDate));
			 System.out.println("After To Date "+convDate.after(toDate));
			 final GWTCAlert alert = new GWTCAlert(); 
		     alert.alert("Holiday Date Should be in Calendar Period !");
		     return;  
		 }
		 
		 
		  
		  holiday.setHolidayname(holiDayName);
		  holiday.setHolidayType(holidayType);
		  holiday.setHolidaydate(convDate);
			if(holidayTable.validate(holiday)==false)
			 {
				 final GWTCAlert alert = new GWTCAlert(); 
			     alert.alert("Please add Valid Holiday\n.Holiday name and date should be unique!");
			     return;
			 }
			else
			{
				 this.holidayTable.getDataprovider().getList().add(holiday);
				 tbholidayname.setText("");
				 dbholidaydate.setValue(null);
				 olbHolidayType.setSelectedIndex(0);
			}
			 
			
			 
   			
   			 
			 		
	}
	
	@Override
	public void setEnable(boolean status) 
	{
		tbholidayname.setEnabled(status);
		olbHolidayType.setEnabled(status);
		dbholidaydate.setEnabled(status);
		holidayTable.setEnabled(status);
	}

	public void clear()
	{
		this.holidayTable.connectToLocal();
		this.tbholidayname.setText("");
		this.dbholidaydate.getTextBox().setText("");
		this.olbHolidayType.setSelectedIndex(0);
	}

	 
	public boolean validate()
	{
		return true;
	}

	
	
	public void applyStyle()
	{
		content.setWidth("98%");
	}


	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<ArrayList< Holiday>> handler) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList< Holiday> getValue() {
		List<Holiday>list=this.holidayTable.getDataprovider().getList();
		ArrayList<Holiday>vec=new ArrayList<Holiday>();
		vec.addAll(list);
		return vec;
		
	}

	@Override
	public void setValue(ArrayList< Holiday> value) {
		holidayTable.connectToLocal();
		holidayTable.getDataprovider().getList().addAll(value);
		
	}

	@Override
	public void setValue(ArrayList< Holiday> value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}
	
	protected void initalizeHolidayTypeListBox()
	{
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new HolidayType());
		olbHolidayType.MakeLive(querry);
	}
	

	/***********************************Getters and Setters**********************************************/
	
	
	public HolidayTable getHolidayTable() {
		return holidayTable;
	}

	public void setHolidayTable(HolidayTable holidayTable) {
		this.holidayTable = holidayTable;
	}

	public ObjectListBox<HolidayType> getOlbHolidayType() {
		return olbHolidayType;
	}

	public void setOlbHolidayType(ObjectListBox<HolidayType> olbHolidayType) {
		this.olbHolidayType = olbHolidayType;
	}

	public TextBox getTbholidayname() {
		return tbholidayname;
	}

	public void setTbholidayname(TextBox tbholidayname) {
		this.tbholidayname = tbholidayname;
	}

	
	public DateBox getDbholidaydate() {
		return dbholidaydate;
	}

	public void setDbholidaydate(DateBox dbholidaydate) {
		this.dbholidaydate = dbholidaydate;
	}

	public CalendarForm getForm() {
		return form;
	}

	public void setForm(CalendarForm form) {
		this.form = form;
	}

	
  
	
	
}
