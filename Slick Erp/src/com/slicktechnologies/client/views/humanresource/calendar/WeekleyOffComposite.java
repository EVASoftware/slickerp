package com.slicktechnologies.client.views.humanresource.calendar;

import java.awt.Checkbox;

import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar.WeekleyOff;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;

public class WeekleyOffComposite extends Composite implements HasValue<WeekleyOff>,ClickHandler,CompositeInterface
{
 Grid woGrid;
 FlowPanel panel;
 

 
 
 
 public WeekleyOffComposite() {
	super();
	createGui();
	applyStyle();
}




public WeekleyOffComposite(Grid woGrid, FlowPanel panel) {
	super();
	this.woGrid = woGrid;
	this.panel = panel;
	
}
 
private void createGui()
{
	woGrid=new Grid(2,7);
	String[]woLabels=new String[] {"Sunday","Monday",
			"Tuesday","Wednesday",
			"Thrusday","Friday",
			"Saturday"};
	int i=0;
	for(String lab:woLabels)
	{
	  InlineLabel label=new InlineLabel();
	  label.setText(lab);
	  CheckBox cb=new CheckBox();
	  woGrid.setWidget(0,i, label);
	  woGrid.setWidget(1,i, cb);
	  i++;
		
	}
	
	panel=new FlowPanel();
	panel.add(woGrid);
	initWidget(woGrid);
}




@Override
public HandlerRegistration addValueChangeHandler(
		ValueChangeHandler<WeekleyOff> handler) {
	// TODO Auto-generated method stub
	return null;
}




@Override
public WeekleyOff getValue() {
	
	WeekleyOff wo=new WeekleyOff();
	CheckBox cbSun=(CheckBox) woGrid.getWidget(1,0);
	CheckBox cbMun=(CheckBox) woGrid.getWidget(1,1);
	CheckBox cbTues=(CheckBox) woGrid.getWidget(1,2);
	CheckBox cbWed=(CheckBox) woGrid.getWidget(1,3);
	CheckBox cbThrus=(CheckBox) woGrid.getWidget(1,4);
	CheckBox cbFri=(CheckBox) woGrid.getWidget(1,5);
	CheckBox cbSat=(CheckBox) woGrid.getWidget(1,6);
	
	if(cbSun.getValue())
		wo.setSUNDAY(true);
	if(cbMun.getValue())
		wo.setMONDAY(true);
	if(cbTues.getValue())
		wo.setTUESDAY(true);
	if(cbWed.getValue())
		wo.setWEDNESDAY(true);
	if(cbThrus.getValue())
		wo.setTHRUSDAY(true);
	if(cbFri.getValue())
		wo.setFRIDAY(true);
	if(cbSat.getValue())
		wo.setSATAURDAY(true);
	
	
	return wo;
}




@Override
public void setValue(WeekleyOff value) {
	
	
	CheckBox cbSun=(CheckBox) woGrid.getWidget(1,0);
	CheckBox cbMun=(CheckBox) woGrid.getWidget(1,1);
	CheckBox cbTues=(CheckBox) woGrid.getWidget(1,2);
	CheckBox cbWed=(CheckBox) woGrid.getWidget(1,3);
	CheckBox cbThrus=(CheckBox) woGrid.getWidget(1,4);
	CheckBox cbFri=(CheckBox) woGrid.getWidget(1,5);
	CheckBox cbSat=(CheckBox) woGrid.getWidget(1,6);
	
	if(value.isSUNDAY())
		cbSun.setValue(true);
	if(value.isMONDAY())
		cbMun.setValue(true);
	if(value.isTUESDAY())
		cbTues.setValue(true);
	if(value.isWEDNESDAY())
		cbWed.setValue(true);
	if(value.isTHRUSDAY())
		cbThrus.setValue(true);
	if(value.isFRIDAY())
		cbFri.setValue(true);
	if(value.isSATAURDAY())
		cbSat.setValue(true);
	
	
}




@Override
public void setValue(WeekleyOff value, boolean fireEvents) {
	
	CheckBox cbSun=(CheckBox) woGrid.getWidget(1,0);
	CheckBox cbMun=(CheckBox) woGrid.getWidget(1,1);
	CheckBox cbTues=(CheckBox) woGrid.getWidget(1,2);
	CheckBox cbWed=(CheckBox) woGrid.getWidget(1,3);
	CheckBox cbThrus=(CheckBox) woGrid.getWidget(1,4);
	CheckBox cbFri=(CheckBox) woGrid.getWidget(1,5);
	CheckBox cbSat=(CheckBox) woGrid.getWidget(1,6);
	
	
	if(value.isSUNDAY())
		cbSun.setValue(fireEvents);
	if(value.isMONDAY())
		cbMun.setValue(fireEvents);
	if(value.isTUESDAY())
		cbTues.setValue(fireEvents);
	if(value.isWEDNESDAY())
		cbWed.setValue(fireEvents);
	if(value.isTHRUSDAY())
		cbThrus.setValue(fireEvents);
	if(value.isFRIDAY())
		cbFri.setValue(fireEvents);
	if(value.isSATAURDAY())
		cbSat.setValue(fireEvents);
	
	
	
	
}




@Override
public void setEnable(boolean status) 
{
	CheckBox cbSun=(CheckBox) woGrid.getWidget(1,0);
	CheckBox cbMun=(CheckBox) woGrid.getWidget(1,1);
	CheckBox cbTues=(CheckBox) woGrid.getWidget(1,2);
	CheckBox cbWed=(CheckBox) woGrid.getWidget(1,3);
	CheckBox cbThrus=(CheckBox) woGrid.getWidget(1,4);
	CheckBox cbFri=(CheckBox) woGrid.getWidget(1,5);
	CheckBox cbSat=(CheckBox) woGrid.getWidget(1,6);
	
	cbSun.setEnabled(status);
	cbMun.setEnabled(status);
	cbTues.setEnabled(status);
	cbWed.setEnabled(status);
	cbThrus.setEnabled(status);
	cbFri.setEnabled(status);
	cbSat.setEnabled(status);
	
	
}




@Override
public void clear() {
	Console.log("inside clear method of weekly composite");
	CheckBox cbSun=(CheckBox) woGrid.getWidget(1,0);
	CheckBox cbMun=(CheckBox) woGrid.getWidget(1,1);
	CheckBox cbTues=(CheckBox) woGrid.getWidget(1,2);
	CheckBox cbWed=(CheckBox) woGrid.getWidget(1,3);
	CheckBox cbThrus=(CheckBox) woGrid.getWidget(1,4);
	CheckBox cbFri=(CheckBox) woGrid.getWidget(1,5);
	CheckBox cbSat=(CheckBox) woGrid.getWidget(1,6);
	
	boolean status=false;
	cbSun.setValue(status);
	cbMun.setValue(status);
	cbTues.setValue(status);
	cbWed.setValue(status);
	cbThrus.setValue(status);
	cbFri.setValue(status);
	cbSat.setValue(status);
}




	@Override
	public boolean validate() {
		return true;
	}

	private void applyStyle() {
		panel.setHeight("250px");
		panel.setWidth("100%%");
		for (int i = 0; i < 7; i++)
			woGrid.getColumnFormatter().addStyleName(i, "firstcolumn");
	}




	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

}
