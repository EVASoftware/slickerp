package com.slicktechnologies.client.views.humanresource.calendar;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.client.views.humanresource.calendar.CalendarPresenter.CalendarPresenterTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;

public class CalendarPresenterTableProxy extends CalendarPresenterTable {
  TextColumn<Calendar> getStatusColumn;
  TextColumn<Calendar> getCalNameColumn;
  TextColumn<Calendar> getCalOwnerNameColumn;
  TextColumn<Calendar> getCountColumn;
  TextColumn<Calendar> getFromDate;
  TextColumn<Calendar> getToDate;
  public Object getVarRef(String varName)
  {
  if(varName.equals("getStatusColumn"))
  return this.getStatusColumn;
  if(varName.equals("getCalNameColumn"))
  return this.getCalNameColumn;
  if(varName.equals("getCalOwnerNameColumn"))
  return this.getCalOwnerNameColumn;
  if(varName.equals("getCountColumn"))
  return this.getCountColumn;
   return null ;
  }
  public CalendarPresenterTableProxy()
  {
  super();
  }
  @Override public void createTable() {
  addColumngetCount();
  addColumngetCalName();
  addColumngetFromDate();
  addColumngetToDate();
  addColumngetCalOwnerName();
  addColumngetStatus();
  }
  @Override
  protected void initializekeyprovider() {
  keyProvider= new ProvidesKey<Calendar>()
  {
  @Override
  public Object getKey(Calendar item)
  {
  if(item==null)
  {
  return null;
  }
  else
  return item.getId();
  }
  };
  }
  @Override
  public void setEnable(boolean state)
  {
  }
  @Override
  public void applyStyle()
  {
  }
   public void addColumnSorting(){
  addSortinggetCount();
  addSortinggetStatus();
  addSortinggetCalName();
  addSortinggetCalOwnerName();
  addSortinggetToDate();
  addSortinggetFromDate();
  }
  @Override public void addFieldUpdater() {
  }
  protected void addSortinggetCount()
  {
  List<Calendar> list=getDataprovider().getList();
  columnSort=new ListHandler<Calendar>(list);
  columnSort.setComparator(getCountColumn, new Comparator<Calendar>()
  {
  @Override
  public int compare(Calendar e1,Calendar e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCount()== e2.getCount()){
  return 0;}
  if(e1.getCount()> e2.getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCount()
  {
  getCountColumn=new TextColumn<Calendar>()
  {
  @Override
  public String getValue(Calendar object)
  {
  if( object.getCount()==-1)
  return "N.A";
  else return object.getCount()+"";
  }
  };
  table.addColumn(getCountColumn,"Id");
  getCountColumn.setSortable(true);
  }
  protected void addSortinggetStatus()
  {
  List<Calendar> list=getDataprovider().getList();
  columnSort=new ListHandler<Calendar>(list);
  columnSort.setComparator(getStatusColumn, new Comparator<Calendar>()
  {
  @Override
  public int compare(Calendar e1,Calendar e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getStatus()!=null && e2.getStatus()!=null){
  return e1.getStatus().compareTo(e2.getStatus());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetStatus()
  {
  getStatusColumn=new TextColumn<Calendar>()
  {
  @Override
  public String getValue(Calendar object)
  {
  return object.getStatus()+"";
  }
  };
  table.addColumn(getStatusColumn,"Status");
  getStatusColumn.setSortable(true);
  }
  protected void addSortinggetCalName()
  {
  List<Calendar> list=getDataprovider().getList();
  columnSort=new ListHandler<Calendar>(list);
  columnSort.setComparator(getCalNameColumn, new Comparator<Calendar>()
  {
  @Override
  public int compare(Calendar e1,Calendar e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getCalName()!=null && e2.getCalName()!=null){
  return e1.getCalName().compareTo(e2.getCalName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCalName()
  {
  getCalNameColumn=new TextColumn<Calendar>()
  {
  @Override
  public String getValue(Calendar object)
  {
  return object.getCalName()+"";
  }
  };
  table.addColumn(getCalNameColumn,"Calendar Name");
  getCalNameColumn.setSortable(true);
  }
  protected void addSortinggetCalOwnerName()
  {
  List<Calendar> list=getDataprovider().getList();
  columnSort=new ListHandler<Calendar>(list);
  columnSort.setComparator(getCalOwnerNameColumn, new Comparator<Calendar>()
  {
  @Override
  public int compare(Calendar e1,Calendar e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getCalOwnerName()!=null && e2.getCalOwnerName()!=null){
  return e1.getCalOwnerName().compareTo(e2.getCalOwnerName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCalOwnerName()
  {
  getCalOwnerNameColumn=new TextColumn<Calendar>()
  {
  @Override
  public String getValue(Calendar object)
  {
  return object.getCalOwnerName()+"";
  }
  };
  table.addColumn(getCalOwnerNameColumn,"Responsible Person");
  getCalOwnerNameColumn.setSortable(true);
  }
  
  
  protected void addSortinggetFromDate()
  {
  List<Calendar> list=getDataprovider().getList();
  columnSort=new ListHandler<Calendar>(list);
  columnSort.setComparator(getFromDate, new Comparator<Calendar>()
  {
  @Override
  public int compare(Calendar e1,Calendar e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getCalStartDate()!=null && e2.getCalEndDate()!=null){
  return e1.getCalStartDate().compareTo(e2.getCalStartDate());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetFromDate()
  {
  getFromDate=new TextColumn<Calendar>()
  {
  @Override
  public String getValue(Calendar object)
  {
  return AppUtility.parseDate(object.getCalStartDate());
  }
  };
  table.addColumn(getFromDate,"From Date");
  getFromDate.setSortable(true);
  }
  
  
  
  
  
  protected void addSortinggetToDate()
  {
  List<Calendar> list=getDataprovider().getList();
  columnSort=new ListHandler<Calendar>(list);
  columnSort.setComparator(getToDate, new Comparator<Calendar>()
  {
  @Override
  public int compare(Calendar e1,Calendar e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getCalEndDate()!=null && e2.getCalEndDate()!=null){
  return e1.getCalEndDate().compareTo(e2.getCalEndDate());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetToDate()
  {
  getToDate=new TextColumn<Calendar>()
  {
  @Override
  public String getValue(Calendar object)
  {
  return AppUtility.parseDate(object.getCalEndDate());
  }
  };
  table.addColumn(getToDate,"To Date");
  getToDate.setSortable(true);
  }
}
