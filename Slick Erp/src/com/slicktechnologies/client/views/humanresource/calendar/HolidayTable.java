package com.slicktechnologies.client.views.humanresource.calendar;

import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;

public class HolidayTable extends SuperTable<Holiday> {
	
	TextColumn<Holiday> hnameColumn;
	TextColumn<Holiday> htypeColumn;
	TextColumn<Holiday> hdateColumn;
	private Column<Holiday, String> delete;
	
	
	public HolidayTable() {
		super();
	}
	
	 @Override public void createTable() {
		  addColumnHolidayname();
	      addColumnHolidaytype();
		  addColumnHolidaydate();
		  addColumnDelete();
		  setFieldUpdaterOnDelete();
	      table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
			
		  }
		 
	
	
	public void addColumnHolidayname()
	{
		hnameColumn = new TextColumn<Holiday>() {
			@Override
			public String getValue(Holiday object) {
				return object.getHolidayname();
			}
		};
		table.addColumn(hnameColumn,"Holiday Name");
	}
	
	public void addColumnHolidaytype()
	{
		htypeColumn = new TextColumn<Holiday>() {
			@Override
			public String getValue(Holiday object) {
				return object.getHolidayType();
			}
		};
		table.addColumn(htypeColumn,"Holiday Type");
	}

	
	public void addColumnHolidaydate()
	{
		hdateColumn = new TextColumn<Holiday>() {
			@Override
			public String getValue(Holiday object) {
				return AppUtility.parseDate(object.getHolidaydate());
			}
		};
		table.addColumn(hdateColumn,"Holiday Date");
	}

	
	private void addColumnDelete()
	{
		ButtonCell btnCell= new ButtonCell();
		delete = new Column<Holiday, String>(btnCell) {

			@Override
			public String getValue(Holiday object) {
				
				return "Delete";
			}
		};
		table.addColumn(delete,"Delete");
	}

	private void setFieldUpdaterOnDelete()
	{
		delete.setFieldUpdater(new FieldUpdater<Holiday, String>() {
			
			@Override
			public void update(int index, Holiday object, String value) {
				getDataprovider().getList().remove(index);
				table.redrawRow(index);
			
			}
		});
	}

		
	private void addeditColumn()
	{
		addColumnHolidayname();
		addColumnHolidaytype();
		addColumnHolidaydate();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	}
	
	private void addViewColumn()
	{
		
		addColumnHolidayname();
		addColumnHolidaytype();
		addColumnHolidaydate();
		
		
	}
	
		

	public  void setEnabled(boolean state)
	{
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
	}


	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<Holiday>() {
			@Override
			public Object getKey(Holiday item) {
				if(item==null)
					return null;
				else
					return item.getId();
			}
		};
	
		
	}

		
	

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}


	public HolidayTable(UiScreen<Holiday> mv) {
		super(mv);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	public boolean validate(Holiday holiady)
	{
		List<Holiday>lis=getListDataProvider().getList();
	    boolean res=true;
	    for(Holiday hol:lis)
	    {
	    	if(hol.getHolidaydate().equals(holiady.getHolidaydate()))
	    		return false;
	    	String name=hol.getHolidayname().trim();
	    	String holidayName=holiady.getHolidayname().trim();
	    	if(name.equals(holidayName))
	    		return false;
	    	

	    }
	    return true;
	}

}
