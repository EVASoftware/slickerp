package com.slicktechnologies.client.views.humanresource.calendar;
import java.util.Date;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.DecimalWithSinglePrecion;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.relationalLayer.EmployeeRelation;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;


public class CalendarForm extends FormScreen<Calendar> implements ClickHandler,ValueChangeHandler<Date> {

//Token to add the varialble declarations
	ObjectListBox<EmployeeRelation> olbownername;
  DateBox dbstartdate,dbenddate;
  TextBox ibcalendarid;
 // CheckBox cbcdefault;
  TextBox tbcalendarname,calStatus;
  HolidayInfoComposite tbholidaycomposite;
  WeekleyOffComposite woComp;
  DoubleBox workingHours;
  
  
  //RuleAssignmentComposite ruleComposite;
  
  
	
	public  CalendarForm() {
		super();
		createGui();
		this.calStatus.setText(Calendar.CREATED);
		dbstartdate.addValueChangeHandler(this);
		dbenddate.addValueChangeHandler(this);
		tbholidaycomposite.setForm(this);
	
	}

	public CalendarForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		
		
	}

	
	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget()
	{
		
	olbownername=new ObjectListBox<EmployeeRelation>();
//	AppUtility.makeSalesPersonListBoxLive(olbownername);
	/**
	 * @author Vijay 
	 * Des :- Loading dropdown as per role definition configuration
	 */
	olbownername.makeEmployeeLive(AppConstants.HRCONFIG, AppConstants.CALENDAR, "Person Responsible");

	
	dbstartdate=new DateBoxWithYearSelector();
	dbenddate=new DateBoxWithYearSelector();
	ibcalendarid=new TextBox();
	ibcalendarid.setEnabled(false);
	//cbcdefault=new CheckBox();
	//cbcdefault.setValue(true);
	tbcalendarname=new TextBox();
	tbholidaycomposite=new HolidayInfoComposite();
	//ruleComposite=new RuleAssignmentComposite();
	
    calStatus=new TextBox();
    calStatus.setEnabled(false);
	woComp=new WeekleyOffComposite();
	workingHours=new DoubleBox();
	workingHours.addKeyPressHandler(new DecimalWithSinglePrecion());
   
   
    
	}
	
	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen() {
		
	    
		initalizeWidget();
		
		
		//Token to initialize the processlevel menus.
	
		this.processlevelBarNames=new String[]{AppConstants.SUBMIT,AppConstants.NEW};		
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
fbuilder = new FormFieldBuilder();
FormField fgroupingCalendarInformation=fbuilder.setlabel("Calendar Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
fbuilder = new FormFieldBuilder("Calendar ID",ibcalendarid);
FormField fibcalendarid= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("* Calendar Name",tbcalendarname);
FormField ftbcalendarname= fbuilder.setMandatory(true).setMandatoryMsg("Calendar Name can't be empty").setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("* Responsible Person",olbownername);
FormField folbownername= fbuilder.setMandatory(true).setMandatoryMsg("Please select owner name").setRowSpan(0).setColSpan(0).build();
//fbuilder = new FormFieldBuilder("Default",cbcdefault);
//FormField fcbcdefault= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("* Valid From",dbstartdate);
FormField fdbstartdate= fbuilder.setMandatory(true).setMandatoryMsg("Please select start date").setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("* Valid Till",dbenddate);
FormField fdbenddate= fbuilder.setMandatory(true).setMandatoryMsg("Please select end date").setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder();
FormField fgroupingHolidayInfo=fbuilder.setlabel("Holiday List Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
fbuilder = new FormFieldBuilder("",tbholidaycomposite);
FormField ftbholidaycomposite= fbuilder.setMandatory(false).setMandatoryMsg("Please add holiday list").setRowSpan(0).setColSpan(4).build();

FormField fruleGrouping= fbuilder.setMandatory(true).setMandatoryMsg("Select Aplicability").setRowSpan(0).setColSpan(4).build();
fbuilder = new FormFieldBuilder("Status",calStatus);
FormField fcalStatus= fbuilder.setMandatory(true).setMandatoryMsg("Please select end date").setRowSpan(0).setColSpan(0).build();

FormField fgroupingWo=fbuilder.setlabel("Weekly Off").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
fbuilder = new FormFieldBuilder("",woComp);
FormField fieldWo=fbuilder.setRowSpan(0).setColSpan(4).build();

fbuilder = new FormFieldBuilder("* Working Hours",workingHours);
FormField fieldWorkinghrs=fbuilder.setRowSpan(0).setColSpan(0).setMandatory(true).
setMandatoryMsg("Working Hours is Mandatory !").build();





///////////////////////////////////////////////////////////////////////////////////////a//////////////////////////
		
////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		
		
  FormField[][] formfield = {{fgroupingCalendarInformation},
  {fibcalendarid,ftbcalendarname,folbownername},
  {fdbstartdate,fdbenddate,fieldWorkinghrs,fcalStatus},
  {fgroupingWo},
  {fieldWo},
  {fgroupingHolidayInfo},
  {ftbholidaycomposite},
  
  };

  this.fields=formfield;
}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Calendar model) 
	{
		
	if(tbcalendarname.getValue()!=null)
	  model.setCalName(tbcalendarname.getValue());
	if(olbownername.getValue()!=null)
	  model.setCalOwnerName(olbownername.getValue());
	
	if(dbstartdate.getValue()!=null)
	{
	  model.setCalStartDate(dbstartdate.getValue());
	 
	}
	if(dbenddate.getValue()!=null)
	{
	  model.setCalEndDate(dbenddate.getValue());
	 
	}
	if(tbholidaycomposite.getValue()!=null)
	   model.setHoliday(tbholidaycomposite.getValue());
	if(calStatus.getValue()!=null)
		model.setStatus(calStatus.getValue());
	if(woComp.getValue()!=null)
		model.setWeeklyoff(woComp.getValue());
	if(workingHours.getValue()!=null)
		model.setWorkingHours(workingHours.getValue());
	
	
	presenter.setModel(model);
	}

	
	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Calendar view) 
	{
		ibcalendarid.setValue(view.getCount()+"");
		
	if(view.getCalName()!=null)
	  tbcalendarname.setValue(view.getCalName());
	if(view.getCalOwnerName()!=null)
	  olbownername.setValue(view.getCalOwnerName());
	if(view.getCalStartDate()!=null)
	  dbstartdate.setValue(view.getCalStartDate());
	if(view.getCalEndDate()!=null)
	  dbenddate.setValue(view.getCalEndDate());
	  
	if(view.getHoliday()!=null)
	  tbholidaycomposite.setValue(view.getHoliday());
  if(view.getStatus()!=null)
	  calStatus.setValue(view.getStatus());
  
  if(view.getWeeklyoff()!=null)
	  woComp.setValue(view.getWeeklyoff());
  
  if(view.getWorkingHours()!=0)
	  workingHours.setValue(view.getWorkingHours());
	  
	 
	
  
  presenter.setModel(view);
	}

	// Hand written code shift in presenter
	
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search"))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CALENDAR,LoginPresenter.currentModule.trim());
	}
	public void setAppHeaderBarAsPerStatus()
	{
		Calendar calendar=(Calendar) presenter.getModel();
		String status=calendar.getStatus().trim();
		if(status.equals(Calendar.ACTIVE))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Edit")||text.contains("Email")||text.equals("Download")||
						text.equals("Print")||text.contains("Rows:")|| text.contains("Message") || text.contains("Follow Up"))//Ashwini Patil Date:15-06-2023
				{
					menus[k].setVisible(false); 
				}
				else
					menus[k].setVisible(true);  		   

			}

		}
	}
	
	public void toggleProcessLevelMenu()
	{
            Calendar entity=(Calendar) presenter.getModel();
            String status=entity.getStatus();
            for(int i=0;i<getProcesslevelBarNames().length;i++)
            {
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();
				
			if(status.equals(Calendar.ACTIVE))
			{

				if(text.equals(AppConstants.SUBMIT))
					label.setVisible(false);



			}
	}
	}
	
	public void setMenuAsPerStatus()
	{
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
		
	}
	

	
	
	@Override
	public void setToViewState() {
		// TODO Auto-generated method stub
		super.setToViewState();
		setMenuAsPerStatus();
	}
	
	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
	this.ibcalendarid.setValue(presenter.getModel().getCount()+"");
	}
	
	

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibcalendarid.setEnabled(false);
		
	}


	
	@Override
	public void onClick(ClickEvent event) {
		
	}
	
	
	public HolidayInfoComposite getTbholidaycomposite() {
		return tbholidaycomposite;
	}

	public void setTbholidaycomposite(HolidayInfoComposite tbholidaycomposite) {
		this.tbholidaycomposite = tbholidaycomposite;
	}

	@Override
	public boolean validate() {
		boolean superRes= super.validate();
		if(superRes==false)
			return false;
		
		Date validfrom = this.dbstartdate.getValue();
		Date validtill = this.dbenddate.getValue();
		
		if((validfrom.before(validtill)&&validtill.after(validfrom))==false)
			{
			dbenddate.setValue(null);
			dbstartdate.setValue(null);
			dbenddate.getTextBox().setText("");
			dbstartdate.getTextBox().setText("");
			this.showDialogMessage("Valid From date should be less than Valid To date");
			return false;
			}
			
		
		if(validfrom.equals(validtill))
		{
			dbenddate.setValue(null);
			dbstartdate.setValue(null);
			dbenddate.getTextBox().setText("");
			dbstartdate.getTextBox().setText("");
			this.showDialogMessage("Valid From date cannot be equal to Valid To date");
			return false;
		}
		
		if(workingHours.getValue()!=null && workingHours.getValue()<=0)
		{
			this.showDialogMessage("Working hours cannot be zero  or negative !");
			return false;
		}
		String x="";
		for(Holiday temp:this.tbholidaycomposite.getValue())
		{
		
			System.out.println("Holo"+temp.getHolidaydate());
			boolean wo=CalendarPresenter.isWeekleyOff(woComp.getValue(),temp.getHolidaydate());
					if(wo==true)
					{
						x=x+temp.getHolidayname()+" "+"is on Weekleey Off !"+"\n";
					}
		
		}
		
		if(x.equals(""))
			return true;
		
		if(x.equals("")==false)
		{
			this.showDialogMessage(x);
			return false;
		}
		
		
	
	return true;
	
	
	}

	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
		
		//this.dbenddate.setEnabled(false);
		//this.dbstartdate.setEnabled(false);
		this.calStatus.setEnabled(false);
		this.processLevelBar.setVisibleFalse(false);
	}

	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		if(dbstartdate.getValue()!=null&&dbenddate.getValue()!=null)
		{
			Date startDate=dbstartdate.getValue();
			Date endDate=dbenddate.getValue();
			if(startDate.after(endDate))
			{
				showDialogMessage("Valid From Date Should be less then Valid Till Date");
				dbenddate.getTextBox().setText("");
			}
		}
		
	}

	
	
	/**************************************************************************************************/
	public ObjectListBox<EmployeeRelation> getOlbownername() {
		return olbownername;
	}

	public void setOlbownername(ObjectListBox<EmployeeRelation> olbownername) {
		this.olbownername = olbownername;
	}

	public DateBox getDbstartdate() {
		return dbstartdate;
	}

	public void setDbstartdate(DateBox dbstartdate) {
		this.dbstartdate = dbstartdate;
	}

	public DateBox getDbenddate() {
		return dbenddate;
	}

	public void setDbenddate(DateBox dbenddate) {
		this.dbenddate = dbenddate;
	}

	public TextBox getIbcalendarid() {
		return ibcalendarid;
	}

	public void setIbcalendarid(TextBox ibcalendarid) {
		this.ibcalendarid = ibcalendarid;
	}

	public TextBox getTbcalendarname() {
		return tbcalendarname;
	}

	public void setTbcalendarname(TextBox tbcalendarname) {
		this.tbcalendarname = tbcalendarname;
	}

	public TextBox getCalStatus() {
		return calStatus;
	}

	public void setCalStatus(TextBox calStatus) {
		this.calStatus = calStatus;
	}

	public WeekleyOffComposite getWoComp() {
		return woComp;
	}

	public void setWoComp(WeekleyOffComposite woComp) {
		this.woComp = woComp;
	}

	public DoubleBox getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(DoubleBox workingHours) {
		this.workingHours = workingHours;
	}

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		tbholidaycomposite.getHolidayTable().getTable().redraw();
	}
	
	

}
