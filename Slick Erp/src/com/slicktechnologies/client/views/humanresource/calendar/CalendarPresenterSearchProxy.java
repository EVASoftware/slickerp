package com.slicktechnologies.client.views.humanresource.calendar;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.humanresource.calendar.CalendarPresenter.CalendarPresenterSearch;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class CalendarPresenterSearchProxy extends CalendarPresenterSearch {
  public TextBox calName;
  public ObjectListBox<Employee> calOwnerName;
  public IntegerBox id;
  public ListBox lbstatus;
  public Object getVarRef(String varName)
  {
  if(varName.equals("calName"))
  return this.calName;
  if(varName.equals("calOwnerName"))
  return this.calOwnerName;
   return null ;
  }
  public CalendarPresenterSearchProxy()
  {
  super();
  createGui();
  }
  public void initWidget()
  {
  calName= new TextBox();
  calOwnerName= new ObjectListBox<Employee>();
  AppUtility.makeSalesPersonListBoxLive(calOwnerName);
  lbstatus=new ListBox();
  id=new IntegerBox();
  
  AppUtility.setStatusListBox(lbstatus,Calendar.getStatusList());
  }
  public void createScreen()
  {
  initWidget();
  FormFieldBuilder builder;
  builder = new FormFieldBuilder("Calendar Name",calName);
  FormField fcalName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
  builder = new FormFieldBuilder("Responsible Person",calOwnerName);
  FormField fcalOwnerName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
  builder = new FormFieldBuilder("Status",lbstatus);
  FormField flbstatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
  
  builder = new FormFieldBuilder("ID",id);
  FormField fid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
  this.fields=new FormField[][]{
  {fid,fcalName,fcalOwnerName,flbstatus}
  };
  }
  public MyQuerry getQuerry()
  {
  Vector<Filter> filtervec=new Vector<Filter>();
  Filter temp=null;
  if(calName.getValue().trim().equals("")==false){
  temp=new Filter();temp.setStringValue(calName.getValue().trim());
  filtervec.add(temp);
  temp.setQuerryString("calName");
  filtervec.add(temp);
  }
  if(calOwnerName.getSelectedIndex()!=0){
  temp=new Filter();temp.setStringValue(calOwnerName.getValue().trim());
  temp.setQuerryString("calOwnerName");
  filtervec.add(temp);
  }
  
  if(id.getValue()!=null){
	  temp=new Filter();
	  temp.setIntValue(id.getValue());
	  temp.setQuerryString("count");
	  filtervec.add(temp);
	  }
  
  if(lbstatus.getSelectedIndex()!=0){
	  int index=lbstatus.getSelectedIndex();
	  String seltext=lbstatus.getItemText(index);
	  temp=new Filter();
	  temp.setStringValue(seltext.trim());
	  temp.setQuerryString("status");
	  filtervec.add(temp);
	  }
  MyQuerry querry= new MyQuerry();
  querry.setFilters(filtervec);
  querry.setQuerryObject(new Calendar());
  return querry;
  }
  
  
@Override
public boolean validate() {
	// TODO Auto-generated method stub
	return super.validate();
}
  
  
}
