package com.slicktechnologies.client.views.humanresource.advance;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

// TODO: Auto-generated Javadoc
/**
 * Gets the culmative Previous Balance of all approved Loan of an Employee.
 */
@RemoteServiceRelativePath("loanservice")
public interface LoanService extends RemoteService
{
  
  /**
   * Gets the previous balance.
   *
   * @param empId id  of Employee for which we need the Loan Previous Balance.
   * @param companyId the company id 
   * @return the previous balance culmative balance of all Loan Applications.
   */
  public double getPreviousBalance(int empId,long companyId);
  
}
