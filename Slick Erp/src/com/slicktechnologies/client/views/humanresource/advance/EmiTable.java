package com.slicktechnologies.client.views.humanresource.advance;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Emi;




public class EmiTable extends SuperTable<Emi>{

	TextColumn<Emi> month;
	TextColumn<Emi> balance;
	TextColumn<Emi> EMI;
	TextColumn<Emi> status;
	TextColumn<Emi> dateColumn;
	TextColumn<Emi> columnprincipal;
	TextColumn<Emi> interest;
	TextColumn<Emi> totalpayment;
	TextColumn<Emi> LoanPaidtoDate;
	private Column<Emi, String> delete;
	
	
	public EmiTable() 
	{
		super();
	}
	
	
	@Override
	public void createTable() {
		addColumnMonth();
		addColumnDate();
		addColumnPrincipal();
		addColumnInterest();
		addColumnEMI();
		
		
		//addColumnTotalPayment();
		addColumnBalance();
		addColumnLoanPaidtoDate();
		
		addColumnStatus();
		
		//addColumnDelete();
		//setFieldUpdaterOnDelete();
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	

	private void addeditColumn()
	{
		addColumnMonth();
		addColumnDate();
		addColumnPrincipal();
		addColumnInterest();
		addColumnEMI();
		
		//addColumnTotalPayment();
	
		addColumnBalance();
		addColumnLoanPaidtoDate();
		addColumnStatus();
		//addColumnDelete();
		//setFieldUpdaterOnDelete();
	}
	
	private void addViewColumn()
	{
		addColumnMonth();
		addColumnDate();
		addColumnPrincipal();
		addColumnInterest();
		addColumnEMI();
		
		
		//addColumnTotalPayment();
	
		addColumnBalance();
		addColumnLoanPaidtoDate();
		
		addColumnStatus();
	}
	public void addColumnLoanPaidtoDate()
	{
		LoanPaidtoDate=new TextColumn<Emi>() {

			@Override
			public String getValue(Emi object) {
				double loanperc=object.getMonthlyLoanPaidtoDate();
				
				return loanperc+"%";
			}
		};
		table.addColumn(LoanPaidtoDate,"Loan Paid To Date");
		table.setColumnWidth(LoanPaidtoDate, 150,Unit.PX);
	}
	public void addColumnPrincipal()
	{
		columnprincipal=new TextColumn<Emi>() {

			@Override
			public String getValue(Emi object) {
				int prin=object.getMonthlyprincipalAmt();
				return prin+"";
			}
		};	
		table.addColumn(columnprincipal,"Principal (A)");
	}
	
	public void addColumnInterest()
	{
		interest=new TextColumn<Emi>() {
			
			@Override
			public String getValue(Emi object) {
				int inter=object.getMonthlyInterest();
				return inter+"";
			}
		};
		table.addColumn(interest,"Interest (B)");
	}
	
	public void addColumnTotalPayment()
	{
		totalpayment= new TextColumn<Emi>() {
			
			@Override
			public String getValue(Emi object) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		table.addColumn(totalpayment,"Total Payement(A+B)");
		table.setColumnWidth(totalpayment, 150,Unit.PX);
	}
	public void addColumnStatus()
	{
		status=new TextColumn<Emi>(){
			@Override
public String getValue(Emi object) {
				
				return object.getMonthlyStatus();
			}
		};
		table.addColumn(status,"Status");
	}
	public void addColumnMonth()
	{
		
		month = new TextColumn<Emi>() {
			@Override
			public String getValue(Emi object) {
				
				return object.getInstallmentNumber()+"";
			}
		};
		Emi adv=new Emi();
	//	int durn=adv.getDurationInMonth();
		//System.out.println("months"+durn);
		
		table.addColumn(month,"Months");
		
		
	}
	
	public void addColumnBalance()
	{
		balance = new TextColumn<Emi>() {
			@Override
			public String getValue(Emi object) {
				
				return object.getMonthlyBalance()+"";
			}
		};
		table.addColumn(balance,"Balance");
	}
	
	public void addColumnEMI()
	{
		EMI = new TextColumn<Emi>() {
			@Override
			public String getValue(Emi object) {
				return object.getMonthlyPayment()+"";
			}
		};
		table.addColumn(EMI,"Total Payment (A+B)");
		table.setColumnWidth(EMI, 150,Unit.PX);
	}
	public void addColumnDate()
	{
		dateColumn=new TextColumn<Emi>() {

			@Override
			public String getValue(Emi object) {
				//Date st=object.getFromdate();
				String date=AppUtility.parseDate(object.getDate());
				return date;
			}
		};
		table.addColumn(dateColumn,"Date");
		
	}

	  private void addColumnDelete()
		{
			ButtonCell btnCell= new ButtonCell();
			delete = new Column<Emi, String>(btnCell) {

				@Override
				public String getValue(Emi object) {
					
					return "Delete";
				}
			};
			table.addColumn(delete,"Delete");
		}

		
	
	
		public  void setEnabled(boolean state)
		{
			int tablecolcount=this.table.getColumnCount();
			for(int i=tablecolcount-1;i>-1;i--)
			  table.removeColumn(i);
			if(state ==true)
				addeditColumn();
			if(state==false)
				addViewColumn();
		}

	

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<Emi>() {
			@Override
			public Object getKey(Emi item) {
				if(item==null)
					return null;
				else
					return null;
			}
		};
	
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
