package com.slicktechnologies.client.views.humanresource.advance;

import com.google.gwt.user.client.ui.*;

import java.util.Date;

import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.mywidgets.*;
import com.slicktechnologies.client.approvalutility.ApprovableFormTableScreen;
import com.slicktechnologies.client.approvalutility.ApprovableScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.*;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.history.HistoryPopup;
import com.slicktechnologies.client.views.humanresource.leaveapplication.LeaveApplicationPresenter;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeHRProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneType;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeHandler;


public class AdvanceForm extends ApprovableFormTableScreen<Loan> implements ClickHandler,ApprovableScreen{

	DateBox dbFromDate,dbToDate;
	ObjectListBox<Employee> olbApprover;
	IntegerBox ibDuration,dbAdvanceAmonut,ibmaxAdvanceAmount;
	TextBox ibAdvanceId;
	TextBox tbStatus;
	TextArea taReason;
	DoubleBox dbPreviousBalance,dbInterest,ibDeductionMonthly,balAmount;
	
	int duration;
	ObjectListBox<LoneType> olbAdvanceType;
	RadioButton selfButton;
	RadioButton otherButoon;
	EmployeeInfoComposite pic;

	EmiTable advanceinfotable;

	AdvancePresenter pre;
	Loan loanobj;

	public AdvanceForm  (SuperTable<Loan> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		this.tbStatus.setText(EmployeeHRProcess.CREATED);
		advanceinfotable.connectToLocal();
		balAmount.setEnabled(false);

	}

	/**
	 * Method template to initialize the declared variables.
	 */
	
	private void initalizeWidget()
	{

		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();

		dbToDate.setEnabled(false);
		ibDuration=new IntegerBox();
		ibDuration.setEnabled(false);
		ibAdvanceId=new TextBox();
		ibAdvanceId.setEnabled(false);
		dbFromDate.getTextBox().setReadOnly(true);;


		tbStatus=new TextBox();
		tbStatus.setEnabled(false);

		olbApprover = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbApprover);
		taReason=new TextArea();
		dbPreviousBalance=new DoubleBox();
		dbPreviousBalance.setEnabled(false);
		dbAdvanceAmonut=new IntegerBox();
		dbInterest=new DoubleBox();
		dbInterest.setEnabled(false);
		ibDeductionMonthly=new DoubleBox();
		ibDeductionMonthly.setEnabled(false);
	
		this.olbAdvanceType=new ObjectListBox<LoneType>();
		
		this.initalizeAdvanceTypeListBox();


		olbApprover = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbApprover);
		taReason=new TextArea();
		dbPreviousBalance=new DoubleBox();
		dbPreviousBalance.setEnabled(false);
		dbAdvanceAmonut=new IntegerBox();
		dbInterest=new DoubleBox();
		dbInterest.setEnabled(false);
		ibDeductionMonthly=new DoubleBox();
		ibDeductionMonthly.setEnabled(false);
		
		this.olbAdvanceType=new ObjectListBox<LoneType>();
		
		this.initalizeAdvanceTypeListBox();

		selfButton=new RadioButton("Group1");
		selfButton.setValue(true);
		selfButton.addClickHandler(this);
		otherButoon=new RadioButton("Group1");
		otherButoon.addClickHandler(this);

		pic=AppUtility.employeeInfoComposite(new EmployeeInfo(),true);
		pic.setEnable(false);
		ibmaxAdvanceAmount=new IntegerBox();
		ibmaxAdvanceAmount.setEnabled(false);
		changeBackgroundColor(this.ibmaxAdvanceAmount,"blue");

		advanceinfotable=new EmiTable();
		balAmount=new DoubleBox();


	}

	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen() {
		initalizeWidget();

		//Token to initialize the processlevel menus.

		this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.SUBMIT,ManageApprovals.CANCELAPPROVALREQUEST,"New"};

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeInformation=fbuilder.setlabel("Employee Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAdvanceInformation=fbuilder.setlabel("Advance Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder =new FormFieldBuilder("Advance id",ibAdvanceId);
		FormField fibAdvanceId=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Previous Balance",dbPreviousBalance);
		FormField fdbPreviousBalance= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Advance Amonut",dbAdvanceAmonut);
		FormField fdbAdvanceAmonut= fbuilder.setMandatory(true).setMandatoryMsg("Advance Amonut is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("% Interest",dbInterest);
		FormField fdbInterest= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Duration In Months",ibDuration);
		FormField fibDuration= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Reason",taReason);
		FormField ftaReason= fbuilder.setMandatory(true).setMandatoryMsg("Reason is mandatory!").setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Advance From Date",dbFromDate);
		FormField fdbFromDate= fbuilder.setMandatory(true).setMandatoryMsg("Advance From Date is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("To Date",dbToDate);
		FormField fdbToDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Deduction Monthly",ibDeductionMonthly);
		FormField fdbDeductionMonthly= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder("Status",tbStatus);
		FormField ftbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Approver",olbApprover);
		FormField ftbApprover= fbuilder.setMandatory(true).
				setMandatoryMsg("Approver Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		
		
		fbuilder = new FormFieldBuilder("* Advance Type",olbAdvanceType);
		FormField folbAdvanceType= fbuilder.setMandatory(true).setMandatoryMsg("AdvanceType is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Self",selfButton);
		FormField fselfButton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Others",otherButoon);
		FormField fotherButoon= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",pic);
		FormField fpic= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Maximum Advance Amount",ibmaxAdvanceAmount);
		FormField fibmaxAdvanceAmount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder =new FormFieldBuilder("",advanceinfotable.getTable());
		FormField fadvanceinfotable=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder =new FormFieldBuilder("Balnce Amount",balAmount);
		FormField fbalaMount=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {  {fselfButton,fotherButoon}, {fgroupingEmployeeInformation},
				{fpic},
				{fgroupingAdvanceInformation},
				{fibAdvanceId,folbAdvanceType,fdbAdvanceAmonut,fdbFromDate},
				{fdbInterest,fdbPreviousBalance,fdbToDate,fibDuration},
				{fbalaMount},
				{fdbDeductionMonthly,fibmaxAdvanceAmount,ftbApprover,ftbStatus},
				{ftaReason},{fadvanceinfotable}
		};

		this.fields=formfield;

	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Loan model) 
	{
		if(dbAdvanceAmonut.getValue()!=null)
			model.setAdvanceAmount(dbAdvanceAmonut.getValue());
		if(dbInterest.getValue()!=null)
			model.setAdvInterest(dbInterest.getValue());
		if(taReason.getValue()!=null)
			model.setAdvReason(taReason.getValue());
		if(dbFromDate.getValue()!=null)
			model.setFromdate(dbFromDate.getValue());
		if(dbToDate.getValue()!=null)
			model.setTodate(dbToDate.getValue());
		if(tbStatus.getValue()!=null)
			model.setStatus(tbStatus.getValue());
		if(olbApprover.getValue()!=null)
			model.setApproverName(olbApprover.getValue());
		if(olbAdvanceType.getValue()!=null)
			model.setAdvanceType(olbAdvanceType.getValue());
		if(balAmount.getValue()!=null)
			model.setBalance(balAmount.getValue());
		if(balAmount.getValue()==null)
			model.setBalance(0d);
		if(pic!=null)
		{
			model.setEmpid(pic.getEmployeeId());
			model.setEmployeeName(pic.getEmployeeName());
			model.setEmpCellNo(pic.getCellNumber());
			model.setBranch(pic.getBranch());
			model.setDepartment(pic.getDepartment());
			model.setEmployeeType(pic.getEmployeeType());
			model.setEmployeedDesignation(pic.getEmpDesignation());
			model.setEmployeeRole(pic.getEmpRole());
		}

		model.setDeductionMonthly(ibDeductionMonthly.getValue());
		model.setDurationInMonth(ibDuration.getValue());
		
		loanobj=model;
		
		presenter.setModel(model);

	}





	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Loan view) 
	{
		loanobj=view;
		
		if(view.getCount()!=-1)
			ibAdvanceId.setValue(view.getCount()+"");
		if(view.getAdvanceAmount()!=null)
			dbAdvanceAmonut.setValue(view.getAdvanceAmount());
		if(view.getAdvInterest()!=null)
			dbInterest.setValue(view.getAdvInterest());
		if(view.getAdvReason()!=null)
			taReason.setValue(view.getAdvReason());
		if(view.getFromdate()!=null)
			dbFromDate.setValue(view.getFromdate());
		if(view.getTodate()!=null)
			dbToDate.setValue(view.getTodate());
		if(view.getStatus()!=null)
			tbStatus.setValue(view.getStatus());
		if(view.getApproverName()!=null)
			olbApprover.setValue(view.getApproverName());
		if(view.getAdvanceType()!=null)
		{
			olbAdvanceType.setValue(view.getAdvanceType());
			LoneType type=olbAdvanceType.getSelectedItem();
			ibmaxAdvanceAmount.setValue(type.getMaxAdvance());
			ibDeductionMonthly.setValue(view.getDeductionMonthly());
			ibDuration.setValue(view.getDurationInMonth());
		}

		if(pic!=null)
		{
			pic.setEmployeeId(view.getEmpid());
			pic.setEmployeeName(view.getEmployeeName());
			pic.setBranch(view.getBranch());
			pic.setDepartment(view.getDepartment());
			pic.setEmployeeType(view.getEmployeeType());
			pic.setEmpDesignation(view.getEmployeedDesignation());
			pic.setEmpRole(view.getEmployeeRole());
			pic.setCellNumber(view.getEmpCellNo());
		}
		AdvancePresenter presen=(AdvancePresenter) presenter;
		presen.refreshAdvanceApplicationHistoryTable(pic.getEmployeeId());
		
		if(UserConfiguration.getInfo().getEmpCount()!=pic.getEmployeeId())
			otherButoon.setValue(true);
		else
			selfButton.setValue(true);
			
		ibDeductionMonthly.setValue(view.getDeductionMonthly());
		ibDuration.setValue(view.getDurationInMonth());
		
		AdvancePresenter pres=(AdvancePresenter) presenter;
		if(view.getStatus().equals(Loan.APPROVED)||view.getStatus().equals(Loan.ADVANCECLOSED))
			advanceinfotable.setValue(view.getLoanEmi());

		else
			pres.calculateEMI();
		

		if(view.getPrevBalance()!=null)
		{
			dbPreviousBalance.setValue(view.getPrevBalance());
		}
		
		if(view.getBalance()!=null)
			balAmount.setValue(view.getBalance());
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */

	}

	// Hand written code shift in presenter


	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.ADVANCE,LoginPresenter.currentModule.trim());

	}

	

	public void setAppHeaderBarAsPerStatus()
	{

		Loan model=(Loan) presenter.getModel();
		String status=model.getStatus();
		boolean bool=status.equals(Loan.APPROVED)||status.equals(Loan.ADVANCECLOSED)||
				status.equals(Loan.REQUESTED)||status.equals(Loan.REJECTED);

		if(bool)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}

		}
		
		if(status.equals(Loan.CREATED)) {
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Edit")||text.contains("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}


	}



	public void toggleProcesslevelbar()
	{
		
		Loan model=(Loan) presenter.getModel();
		String status=model.getStatus();
		boolean bool=status.equals(Loan.APPROVED)||status.equals(Loan.ADVANCECLOSED)||
				status.equals(Loan.REJECTED);

		ProcessLevelBar bar=getProcessLevelBar();
		InlineLabel[] lbls=bar.btnLabels;
		if(bool)
		{
			for(InlineLabel temp:lbls)
			{
				String lbltext=temp.getText();
				if(lbltext.equals(ManageApprovals.APPROVALREQUEST))
					temp.setVisible(false);
				if(lbltext.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					temp.setVisible(false);
				if(lbltext.equals(ManageApprovals.SUBMIT))
					temp.setVisible(false);

			}

		}
		
		if(status.equals(Loan.REQUESTED))
		{
			for(InlineLabel temp:lbls)
			{
				String lbltext=temp.getText();
				if(lbltext.equals(ManageApprovals.APPROVALREQUEST))
					temp.setVisible(false);
				if(lbltext.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					temp.setVisible(true);
				if(lbltext.equals(ManageApprovals.SUBMIT))
					temp.setVisible(false);
			}
		}
		
		if(status.equals(Loan.CREATED)) {
		
			for(InlineLabel temp:lbls)
			{
				String lbltext=temp.getText();
				if(getManageapproval().isSelfApproval()){
					if(lbltext.equals(ManageApprovals.SUBMIT))
						temp.setVisible(true);
					if(lbltext.equals(ManageApprovals.APPROVALREQUEST))
						temp.setVisible(false);
				}else{
					if(lbltext.equals(ManageApprovals.SUBMIT))
						temp.setVisible(false);
					if(lbltext.equals(ManageApprovals.APPROVALREQUEST))
						temp.setVisible(true);
				}
			}
			
		}


	}




	@Override
	public void setToViewState() {
		super.setToViewState();
		setAppHeaderBarAsPerStatus();
		toggleProcesslevelbar();
		
		
		SuperModel model=new Loan();
		model=loanobj;
		if(HistoryPopup.historyObj!=null){
			AppUtility.addDocumentToHistoryTable(HistoryPopup.historyObj.getModuleName(),AppConstants.ADVANCEREQUEST, loanobj.getCount(), loanobj.getEmpid(),loanobj.getEmployeeName(),loanobj.getEmpCellNo(), false, model, null);
			HistoryPopup.historyObj=null;
		}else{
		AppUtility.addDocumentToHistoryTable(LoginPresenter.currentModule,AppConstants.ADVANCEREQUEST, loanobj.getCount(), loanobj.getEmpid(),loanobj.getEmployeeName(),loanobj.getEmpCellNo(), false, model, null);
		}
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		setAppHeaderBarAsPerStatus();
		toggleProcesslevelbar();
		this.processLevelBar.setVisibleFalse(false);
	}


	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		this.ibAdvanceId.setValue(presenter.getModel().getCount()+"");
	}


	@Override
	public void onClick(ClickEvent event) 
	{
		
		if(selfButton.getValue()==true)	
		{
			pic.setEnable(false);
			setToNewState();
			pic.setValue(UserConfiguration.getInfo());
			AdvancePresenter advnpresenter=(AdvancePresenter) presenter;
			advnpresenter.refreshAdvanceScreen(pic.getEmployeeId());
			selfButton.setValue(true);
		}
		if(otherButoon.getValue()==true)
		{
			pic.setEnable(true);
			setToNewState();
			this.dbPreviousBalance.setText("");
			pic.clear();
			this.getSupertable().clear();
			AdvancePresenter advnpresenter=(AdvancePresenter) presenter;
			advnpresenter.makeNewModel();
			otherButoon.setValue(true);
			pic.setEnable(true);
			

			
		}
		
	}



	@Override
	public void setEnable(boolean state) {
		
		super.setEnable(state);
		ibAdvanceId.setEnabled(false);
		tbStatus.setEnabled(false);
		ibDuration.setEnabled(false);
		ibmaxAdvanceAmount.setEnabled(false);
		dbInterest.setEnabled(false);
		dbToDate.setEnabled(false);
		dbPreviousBalance.setEnabled(false);
		pic.setEnable(false);
		ibDeductionMonthly.setEnabled(false);
		dbFromDate.getTextBox().setReadOnly(true);
		balAmount.setEnabled(false);





	}


	/*******************************************************************************************/

	protected void initalizeDepartMentListBox()
	{
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Department());
		
	}


	

	

	protected void initalizeAdvanceTypeListBox()
	{
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new LoneType());
		olbAdvanceType.MakeLive(querry);
	}




	@Override
	public boolean validate() {
		
		
		boolean superRes= super.validate();
		if(superRes==false)
			return false;

		Date date=new Date();
		Date fromdate=dbFromDate.getValue();
		LoneType type=olbAdvanceType.getSelectedItem();

		int maxamt=type.getMaxAdvance();
		int amt=dbAdvanceAmonut.getValue();

		if(amt==0)
		{
			dbAdvanceAmonut.setValue(null);
			this.showDialogMessage("You have entered 0 amount ..please enter correct amount");
			changeBackgroundColor(this.dbAdvanceAmonut,"PINK");
			return false;

		}
		else if(fromdate.before(date))
		{
			    dbFromDate.setValue(null);
				this.showDialogMessage("Advance Date Should be greater or equal to current date.");
				return false;
			
		}
		else if(amt>maxamt)
		{
			
			dbAdvanceAmonut.setValue(null);
			this.showDialogMessage("Amount limit exceeding! Please enter amount lower than maximum limit");
			return false;
		}


		return true;
 }

	private void changeBackgroundColor(Widget widg,String color)
	{
		//widg.getElement().getStyle().setBackgroundColor(color);
		widg.getElement().getStyle().setColor(color);
	}


	public EmployeeInfoComposite getPic() {
		return pic;
	}

	public void setPic(EmployeeInfoComposite pic) {
		this.pic = pic;
	}


	@Override
	public TextBox getstatustextbox() {
		// TODO Auto-generated method stub
		return this.tbStatus;
	}



	@Override
	public void clear()
	{
		EmployeeInfo empinfo=pic.getValue();
		Double previousBalance=dbPreviousBalance.getValue();
		boolean selected=selfButton.getValue();
		super.clear();
		if(selected==true)
			selfButton.setValue(true);
		else
			otherButoon.setValue(true);
		tbStatus.setText(Loan.CREATED);
		advanceinfotable.connectToLocal();
		if(empinfo!=null)
		   pic.setValue(empinfo);
		if(previousBalance!=null)
		{
			dbPreviousBalance.setValue(previousBalance);
		}
		

	}
	
	
	//Code To Apply Handlers
	public void applyChangeHandlers(ChangeHandler handler)
	{
		olbAdvanceType.addChangeHandler(handler);
		dbAdvanceAmonut.addChangeHandler(handler);
		
		
	}
	
	
	
	public void applyValueChangeHandler(ValueChangeHandler<Date>handler)
	{
		dbFromDate.addValueChangeHandler(handler);
	}

}
