package com.slicktechnologies.client.views.humanresource.advance;

import com.google.gwt.user.client.rpc.AsyncCallback;

// TODO: Auto-generated Javadoc
/**
 * The Interface LoanServiceAsync.
 */
public interface LoanServiceAsync 
{
	 
 	/**
 	 * Gets the previous balance.
 	 *
 	 * @param empId the emp id
 	 * @param companyId the company id
 	 * @param callback the callback
 	 * @return the previous balance
 	 */
 	public void getPreviousBalance(int empId,long companyId,AsyncCallback<Double>callback);
}
