package com.slicktechnologies.client.views.humanresource.advance;

import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneType;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.settings.employee.EmployeePresenter.EmployeePresenterSearch;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;

public class AdvancePresenterSearchProxy extends SearchPopUpScreen<Loan> {
  
  public EmployeeInfoComposite personInfo;
  public ObjectListBox<Branch> olbBranch;
  public ObjectListBox<Department> olbDepartMent;
  public ObjectListBox<Config> olbEmploymentType;
  public ObjectListBox<Config> olbDesignation;
  public ObjectListBox<Config> olbRole;
  public ObjectListBox<LoneType>olbAdvanceType;
  public ObjectListBox<Employee> olbReportsTo;
  public ListBox lbStatus;
  public DateComparator dateComparator;
  public IntegerBox ibId;
 
 
 
  public Object getVarRef(String varName)
  {
	  return null ;
  }
  public AdvancePresenterSearchProxy()
  {
	  super();
	  createGui();
  }
  public void initWidget()
  {
	  	personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
	  
	  olbBranch= new ObjectListBox<Branch>();
	  AppUtility.makeBranchListBoxLive(olbBranch);
	  
	  olbDepartMent=new ObjectListBox<Department>();
	  AppUtility.makeSalesPersonListBoxDepartment(olbDepartMent);
	  olbEmploymentType=new ObjectListBox<Config>();
	  AppUtility.MakeLiveConfig(olbEmploymentType,Screen.EMPLOYEETYPE);
	  olbDesignation=new ObjectListBox<Config>();
	  AppUtility.MakeLiveConfig(olbDesignation,Screen.EMPLOYEEDESIGNATION);
	  olbRole=new ObjectListBox<Config>();
	  AppUtility.MakeLiveConfig(olbRole,Screen.EMPLOYEEROLE);
	  olbReportsTo= new ObjectListBox<Employee>();
	  AppUtility.makeSalesPersonListBoxLive(olbReportsTo);
	  lbStatus= new ListBox();
	  AppUtility.setStatusListBox(lbStatus,Loan.getStatusList());
	  
	  olbAdvanceType=new ObjectListBox<LoneType>();
	  this.makeAdvanceTypeListBoxLive();
	  
	  dateComparator= new DateComparator("fromdate",new Loan());
	  ibId=new IntegerBox();
  }
  public void createScreen()
  {
	  initWidget();
	  FormFieldBuilder builder;
	  builder = new FormFieldBuilder("",personInfo);
	  FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
	  builder = new FormFieldBuilder("Branch",olbBranch);
	  FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	  
	  builder = new FormFieldBuilder("Department",olbDepartMent);
	  FormField folbdepartment= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	  builder = new FormFieldBuilder("Employment Type",olbEmploymentType);
	  FormField folbtype= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	  
	  builder = new FormFieldBuilder("Employee Designation",olbDesignation);
	  FormField folbdesignation= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	  
	  builder = new FormFieldBuilder("Employee Role",olbRole);
	  FormField folbrole= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	  
	  
	  builder = new FormFieldBuilder("Approver",olbReportsTo);
	  FormField folbSalesPerson= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	  
	  
	  builder = new FormFieldBuilder("Status",lbStatus);
	  FormField fstatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	  
	  builder = new FormFieldBuilder("Advance Type",olbAdvanceType);
	  FormField folbAdvanceType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	  
	  builder = new FormFieldBuilder("Loan Start From Date",dateComparator.getFromDate());
	  FormField fdbFromDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	  
	  builder = new FormFieldBuilder("Loan Start To Date",dateComparator.getToDate());
	  FormField fdbToDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	  
	  builder = new FormFieldBuilder("ID",ibId);
	  FormField fdbid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
  
	  this.fields=new FormField[][]{
			  {fdbid,fdbFromDate,fdbToDate,folbBranch},
			  {folbdepartment,folbtype,folbdesignation,folbrole},
			  {folbSalesPerson,folbAdvanceType,fstatus},
			  {fpersonInfo}
	  };
	  
  }
  
  public MyQuerry getQuerry()
  {
	  Vector<Filter> filtervec=new Vector<Filter>();
	  Filter temp=null;
	  
	  
	  if(olbDepartMent.getSelectedIndex()!=0)
	  {
		  temp=new Filter();
		  temp.setStringValue(olbDepartMent.getValue().trim());
		  temp.setQuerryString("department");
		  filtervec.add(temp);
	  }
	  
	  
	  if(lbStatus.getSelectedIndex()!=0)
	  {
		 
		  temp=new Filter();
		  int item=lbStatus.getSelectedIndex();
		  String sel=lbStatus.getItemText(item);
		  temp.setStringValue(sel);
		  temp.setQuerryString("status");
		  filtervec.add(temp);
	  }
	  
	  if(ibId.getValue()!=null)
	  {
		 
		  temp=new Filter();
		  int item=ibId.getValue();
		 
		  temp.setIntValue(item);
		  temp.setQuerryString("count");
		  filtervec.add(temp);
	  }
	  
	  
	  if(olbRole.getSelectedIndex()!=0)
	  {
	  temp=new Filter();temp.setStringValue(olbRole.getValue().trim());
	  temp.setQuerryString("employeeRole");
	  filtervec.add(temp);
	  }
	  if(olbDesignation.getSelectedIndex()!=0)
	  {
	  temp=new Filter();temp.setStringValue(olbDesignation.getValue().trim());
	  temp.setQuerryString("employeedDesignation");
	  filtervec.add(temp);
	  }
	  if(olbBranch.getSelectedIndex()!=0){
	  temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
	  temp.setQuerryString("branch");
	  filtervec.add(temp);
	  }
	  if(olbEmploymentType.getSelectedIndex()!=0){
		  temp=new Filter();temp.setStringValue(olbEmploymentType.getValue().trim());
		  temp.setQuerryString("employeeType");
		  filtervec.add(temp);
		  }
	  if(olbAdvanceType.getSelectedIndex()!=0){
		  temp=new Filter();temp.setStringValue(olbAdvanceType.getValue().trim());
		  temp.setQuerryString("advanceType");
		  filtervec.add(temp);
		  }
		
		 
	  if(olbReportsTo.getSelectedIndex()!=0){
	  temp=new Filter();temp.setStringValue(olbReportsTo.getValue().trim());
	  temp.setQuerryString("approverName");
	  filtervec.add(temp);
	  }
	 
	  if(dateComparator.getValue()!=null)
	  {
	     filtervec.addAll(dateComparator.getValue());
	  }
	  
	  if(!personInfo.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
			temp.setQuerryString("empid");
			filtervec.add(temp);
		}
	
		if(!(personInfo.getName().getValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(personInfo.getName().getValue());
			temp.setQuerryString("employeeName");
			filtervec.add(temp);
		}
		if(!personInfo.getPhone().getValue().equals(""))
		{
			temp=new Filter();
			temp.setLongValue(personInfo.getCellNumber());
			temp.setQuerryString("empCellNo");
			filtervec.add(temp);
		}
	  
		  
		  
	//	  if(personInfo.getIdValue()!=-1)
	//	  {  
	//	  temp=new Filter();
	//	  temp.setIntValue(personInfo.getIdValue());
	//	  temp.setQuerryString("empid");
	//	  filtervec.add(temp);
	//	  }
	//	  
	//	  if(!(personInfo.getFullNameValue().equals("")))
	//	  {
	//	  temp=new Filter();
	//	  temp.setStringValue(personInfo.getFullNameValue());
	//	  temp.setQuerryString("employeeName");
	//	  filtervec.add(temp);
	//	  }
	//	  if(personInfo.getCellValue()!=-1l)
	//	  {
	//	  temp=new Filter();
	//	  temp.setLongValue(personInfo.getCellValue());
	//	  temp.setQuerryString("empCellNo");
	//	  filtervec.add(temp);
	//	  }
  
		  MyQuerry querry= new MyQuerry();
		  querry.setFilters(filtervec);
		  querry.setQuerryObject(new Loan());
		  return querry;
  }
  
  public void makeAdvanceTypeListBoxLive()
  {
	  MyQuerry querry=new MyQuerry();
	  Filter filter=new Filter();
	  filter.setQuerryString("status");
	  filter.setBooleanvalue(true);
	  querry.setQuerryObject(new LoneType());
	  olbAdvanceType.MakeLive(querry);
	  querry.getFilters().add(filter);
  }
@Override
public boolean validate() {
	if(LoginPresenter.branchRestrictionFlag)
	{
	if(olbBranch.getSelectedIndex()==0)
	{
		showDialogMessage("Select Branch");
		return false;
	}
	}
	return true;
}
  
}
