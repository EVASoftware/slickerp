package com.slicktechnologies.client.views.humanresource.advance;
import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;

import java.util.*;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneEmi;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneType;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Emi;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenTablePresenter;
import com.slicktechnologies.client.approvalutility.ApprovableFormTableScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.advancetype.AdvanceTypeForm;
// TODO: Auto-generated Javadoc
/**
 * Presenter for Advance Form.
 * 
 */
public class AdvancePresenter extends ApprovableFormScreenTablePresenter<Loan> 
implements ChangeHandler,
ValueChangeHandler<Date>,SelectionHandler<Suggestion>
{
	
	/** **********************Attributes******************************************************. */

	AdvanceForm form;
	
	/** The table. */
	EmiTable table;



	/**
	 * Instantiates a new advance presenter.
	 *
	 * @param view the view
	 * @param model the model
	 */
	public AdvancePresenter (ApprovableFormTableScreen<Loan> view,
			Loan model) {
		super(view, model);
		form=(AdvanceForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getAdvanceQuery());
		form.applyChangeHandlers(this);
		form.applyValueChangeHandler(this);
		form.setPresenter(this);
		if(UserConfiguration.getInfo()!=null)
			form.pic.setValue(UserConfiguration.getInfo());
		EmployeeInfo info=UserConfiguration.getInfo();
		refreshPrevioudBalance(info.getEmpCount(), info.getCompanyId());
		form.pic.addSelectionHandler(this);
		
	}

	/* (non-Javadoc)
	 * @see com.slicktechnologies.client.approvalutility.ApprovableFormScreenTablePresenter#reactToProcessBarEvents(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		super.reactToProcessBarEvents(e);
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();

		if(text.equals("New"))
		{
			form.setToNewState();
			initalize();
		}

		if(text.equals(ManageApprovals.APPROVALREQUEST))
		{

			model.setStatus(Loan.REQUESTED);
			updateUpperTable();
	
		}
		
		if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
		{
			model.setStatus(Loan.CREATED);
			updateUpperTable();
		}

		if(text.equals(ManageApprovals.SUBMIT)){
		
			form.getManageapproval().submitLbl=label;
			System.out.println("SUBMIT BF:: "+form.getManageapproval().submitLbl.getText());
			form.getManageapproval().submitLbl.setText("Processing...");
			System.out.println("SUBMIT AF:: "+form.getManageapproval().submitLbl.getText());
			form.getManageapproval().reactToSubmit();
			Console.log("Submitted loan");
			
		}

	}



	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#makeNewModel()
	 */
	@Override
	protected void makeNewModel() {
		model=new Loan();

	}

	/**
	 * Gets the advance query.
	 *
	 * @return the advance query
	 */
	public MyQuerry getAdvanceQuery()
	{
		EmployeeInfo info=UserConfiguration.getInfo();
		Filter filter=new Filter();
		filter.setQuerryString("empid");
		filter.setIntValue(info.getEmpCount());
		MyQuerry quer=new MyQuerry();
		quer.setQuerryObject(new Loan());
		quer.getFilters().add(filter);
		return quer;
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#setModel(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	public void setModel(Loan entity)
	{
		model=entity;
	}
	
	/**
	 * Initalize.
	 * @return 
	 */
	public static AdvanceForm initalize()
	{
		AdvanceTable gentableScreen=new AdvanceTable();
		AdvanceForm  form=new  AdvanceForm(gentableScreen,ApprovableFormTableScreen.LOWER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();

		AdvancePresenterTableProxy gentableSearch=new AdvancePresenterTableProxy();
		AdvancePresenterSearchProxy.staticSuperTable=gentableSearch;
		gentableSearch.setView(form);
		gentableSearch.applySelectionModle();
		AdvancePresenterSearchProxy searchpopup=new AdvancePresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		AdvancePresenter  presenter=new  AdvancePresenter  (form,new Loan());
		AppMemory.getAppMemory().stickPnel(form);
		return form;


	}





	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter#reactOnDownload()
	 */
	@Override
	public void reactOnDownload()
	{
		ArrayList<Loan> advarray=new ArrayList<Loan>();
		List<Loan> list=(List<Loan>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();

		advarray.addAll(list);
		CsvServiceAsync csvservice=GWT.create(CsvService.class);

		csvservice.setadvancelist(advarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}

			@Override
			public void onSuccess(Void result) {

				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+9;
				Window.open(url, "test", "enabled");
			}
		});

	}



	/**
	 * Calculate emi.
	 */
	public void calculateEMI()
	{
		
		form.advanceinfotable.connectToLocal();
		double amt=form.ibDeductionMonthly.getValue();
		int dur=form.ibDuration.getValue();
		double famt=form.dbAdvanceAmonut.getValue();
		int loanamt=form.dbAdvanceAmonut.getValue();
		double inter=form.dbInterest.getValue();

		for(int i=1;i<=dur;i++)
		{
			Emi ad=new Emi();
			Date st=form.dbFromDate.getValue();
			double monthinter;
			if(inter>0)
				monthinter= (double) (famt*inter*1/12*1/100);
			else
				monthinter= 0;
			System.out.println("interest"+monthinter);
			double principal=(double) (amt-monthinter);
			famt=famt-principal;
			double amttill=(famt/loanamt)*100;
			double amttilldate=Math.round(100-amttill);

			/***set all value*****/
			ad.setMonthlyBalance((int) famt);
			ad.setMonthlyPayment((int) amt);
			ad.setInstallemtNumber(i);
			ad.setMonthlyInterest((int) monthinter);
			ad.setMonthlyprincipalAmt((int) principal);
			ad.setMonthlyLoanPaidtoDate((int) amttilldate);
			ad.setMonthlyStatus("Unpaid");
			CalendarUtil.addMonthsToDate(st, i);
			ad.setDate(st);

			form.advanceinfotable.getDataprovider().getList().add(ad);

		}

	}

















	/**
	 * Calculate dedction.
	 */
	public void calculateDedction(){

		Integer amount=form.dbAdvanceAmonut.getValue();
		Double interest=form.dbInterest.getValue();
		Integer dur=form.ibDuration.getValue();
		if(amount==null||interest==null||dur==null)
		{
			form.ibDeductionMonthly.setText("");
			return;
		}
		if(interest>0){
			double r=interest/12/100;
			double mat=Math.pow(1+r, dur);
			double deductionAmount=(double) (amount*r*mat/((mat)-1));
			deductionAmount = Math.round(deductionAmount*100)/100.0d;
			form.ibDeductionMonthly.setValue(deductionAmount);		
		}else if(interest==0){
			double deductionAmount=(double)amount/dur;
			deductionAmount = Math.round(deductionAmount*100)/100.0d;
			form.ibDeductionMonthly.setValue(deductionAmount);
		}
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt.event.dom.client.ChangeEvent)
	 */
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource()==form.olbAdvanceType)
		{

			if(form.olbAdvanceType.getValue()!=null)
				setLoanAttributes();
			else
				clearAdvanceGrouping();
			
			
			
		}
		if(event.getSource()==form.dbAdvanceAmonut)
		{
			if(form.dbAdvanceAmonut.getValue()!=null)
			  setLoanAttributes();
			else
				clearAdvanceGrouping();
		}

	}

  /**
   * Gets the to date.
   *
   * @param fromDate Date from which to Date is to be calculated
   * @param duration duration in months.
   * @return to date after adding duration in months in fromDate.
   */
	public Date getToDate(Date fromDate,int duration)
	{
		if(fromDate!=null)
		{
			
			//Calendar cal = Calendar.getInstance();
			Date a=CalendarUtil.copyDate(fromDate);
			Integer b=duration;
			if(b!=null)
			{
				CalendarUtil.addMonthsToDate(a, b);
				
			}
			form.dbToDate.setValue(a);
			return a;

		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {


		if(event.getSource()==form.dbFromDate)
			setLoanAttributes();
	}
	

	






  /**
   * Method refreshes the Previous Balance as Per Employee ID.
   * @param empId id of Employee
   * @param companyId id of Company
   */
	protected void refreshPrevioudBalance(int empId,Long companyId)
	{

		form.showWaitSymbol();
		LoanServiceAsync async=GWT.create(LoanService.class);
		async.getPreviousBalance(empId, companyId,new AsyncCallback<Double>() 
				{


			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();

			}

			@Override
			public void onSuccess(Double result) 
			{
				form.dbPreviousBalance.setValue(result);	
				form.hideWaitSymbol();

			}
				});
	}



	/**
	 * Method refreshes the Advance Screen as Per Employee Id.
	 *
	 * @param empId the emp id
	 */
	public void refreshAdvanceScreen(int empId)
	{
		refreshAdvanceApplicationHistoryTable(empId);
		refreshPrevioudBalance(empId, UserConfiguration.getCompanyId());
	}
	
	/**
	 * Method refreshes the AdvanceApplication Table as Per Employee Id.
	 *
	 * @param empId the emp id
	 */
	public void refreshAdvanceApplicationHistoryTable(int empId)
	{
		Filter filter=new Filter();
		filter.setQuerryString("empid");
		filter.setIntValue(empId);
		MyQuerry quer=new MyQuerry();
		quer.getFilters().add(filter);
		quer.setQuerryObject(new Loan());
		form.retriveTable(quer);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.SelectionHandler#onSelection(com.google.gwt.event.logical.shared.SelectionEvent)
	 */
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {

		form.pic.reactOnSelection(event);
		if(form.pic!=null)
		{
			try{
				int empId=form.pic.getEmployeeId();
				refreshAdvanceScreen(empId);
			}
			catch(Exception e)
			{
				
			}
		}
	


	}
	
	/**
	 *Utility Method to Clear the Advance Grouping.
	 */

	public void clearAdvanceGrouping()
	{
		if(form.olbAdvanceType.getValue()==null)
		{
		form.ibDuration.setText("");
		form.ibmaxAdvanceAmount.setText("");;
		form.dbInterest.setText("");
		form.ibDeductionMonthly.setText("");
		}
		form.advanceinfotable.connectToLocal();
	}

	/**
	 * Method insures that at any stage of User Interaction with UI
	 * if there is a valid selection in Advance Type,from Date and Advance Amount
	 * to Date , Monthly deduction and Emi will get calculated.
	 * If this Condition is false than Emi Table is Cleared.
	 */
	public void setLoanAttributes()
	{
		form.advanceinfotable.connectToLocal();
		if(form.olbAdvanceType.getValue()!=null)
			setAdvanceTypeProperties();
		if(form.olbAdvanceType.getValue()!=null&&form.dbFromDate.getValue()!=null&&form.dbAdvanceAmonut
				.getValue()!=null)
		{
			
			Date fromDate=CalendarUtil.copyDate(form.dbFromDate.getValue());
			int duration=form.ibDuration.getValue();
			getToDate(fromDate,duration);
			calculateDedction();
			calculateEMI();
			

		}

		
			

	}
	
	/**
	 * Fills Advance Type Properties
	 * Releated to Advance Type.
	 */
	public void setAdvanceTypeProperties()
	{
		LoneType type=form.olbAdvanceType.getSelectedItem();
		form.ibDuration.setValue(type.getAdvanceDuration());
		form.ibmaxAdvanceAmount.setValue(type.getMaxAdvance());
		form.dbInterest.setValue(type.getAdvanceInterest());
	}










	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnPrint()
	 */
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnEmail()
	 */
	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub

	}
	
	 private void updateUpperTable()
     {
   	Loan appl=(Loan) getModel();
   	for(Loan temp:form.getSupertable().getListDataProvider().getList())
   	{
   		System.out.println("Amount List ="+temp.getAdvanceAmount());
   		System.out.println("ID List ="+temp.getId());
   		
   		System.out.println("Amount ="+appl.getAdvanceAmount());
   		System.out.println("ID ="+appl.getId());
   		if(temp.getId().equals(appl.getId()))
   		{
   			temp.setStatus(appl.getStatus());
   		}
   	}
     }

	 public void reactOnSave()
	 {
		 int id=form.pic.getEmployeeId();
		
		 GenricServiceAsync async=GWT.create(GenricService.class);
		 MyQuerry querry=new MyQuerry();
		 Filter filter=new Filter();
		 filter.setQuerryString("empid");
		 filter.setIntValue(id);
		 querry.getFilters().add(filter);
		 
		 filter=new Filter();
		 filter.setQuerryString("status");
		 filter.setStringValue("Active");
		 
		 querry.setQuerryObject(new CTC());
		 querry.getFilters().add(filter);
		 async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if(result.size()==0)
				{
				
					form.setEnable(false);
					form.showDialogMessage("Advance request is probhited as no Ctc exists !");
					return;
				}
				
				if(!updateTable())
				{
					//view.clear();
					
					return;
				 }
				
				view.showWaitSymbol();
				
				service.save(model,new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) {
						view.hideWaitSymbol();
						model.setCount(result.count);
						model.setId(result.id);
						
						view.setToViewState();
//						makeNewModel();
						view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE , GWTCAlert.OPTION_ANIMATION);
												
					}
					
					@Override
					public void onFailure(Throwable caught) {
						view.hideWaitSymbol();
						view.showDialogMessage("Data Save Unsuccessful! Try again.");
						
					}
				});
				
			}
		});
		
		 
	 }

}
