package com.slicktechnologies.client.views.humanresource.advance;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;

import java.util.List;

import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;

public class AdvanceTable extends SuperTable<Loan> {
	TextColumn<Loan> id;
	TextColumn<Loan> advanceType;
	TextColumn<Loan> fromDate;
	TextColumn<Loan> toDate;
	TextColumn<Loan> amount;
	TextColumn<Loan> duration;
	TextColumn<Loan> employeeId;
	TextColumn<Loan> employeeName;
	TextColumn<Loan> employeeCell;
	TextColumn<Loan> employeeBranch;
	TextColumn<Loan> employeeDepartment;
	TextColumn<Loan> employeeType;
	TextColumn<Loan> employeeDesignation;
	TextColumn<Loan> employeeRole;
	TextColumn<Loan> approverName;
	TextColumn<Loan> status;
	TextColumn<Loan> balance;
	TextColumn<Loan> monthleyDeduction;


	public AdvanceTable()
	{
		super();
	}
	@Override public void createTable() {
		addColumngetCount();
		addColumngetEmployeeId();
		addColumngetEmployeeName();
		addColumngetAdvanceType();
		addColumngetFromDate();
		addColumngetToDate();
		addColumngetAmount();
		addColumngetBalanceAmount();
		addColumngetDuration();
		addColumnDeductionMonthly();
	    addColumngetApproverName();
		addColumngetStatus();
		applyStyle();
	}

	private void addColumnDeductionMonthly() {
		monthleyDeduction=new TextColumn<Loan>() {

			@Override
			public String getValue(Loan object) {
				// TODO Auto-generated method stub
				return object.getDeductionMonthly()+"";
			}
		};
		table.addColumn(monthleyDeduction, "Monthly Deduction");
		monthleyDeduction.setSortable(true);
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Loan>()
				{
			@Override
			public Object getKey(Loan item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}

	@Override
	public void setEnable(boolean state)
	{
	}

	@Override
	public void applyStyle()
	{
		table.setColumnWidth(id, "100px");
		table.setColumnWidth(employeeId,"140px");
		table.setColumnWidth(employeeName,"150px");
		table.setColumnWidth(approverName,"150px");
		table.setColumnWidth(amount,"110px");
		table.setColumnWidth(fromDate,"110px");
		table.setColumnWidth(toDate,"110px");
		table.setColumnWidth(balance,"110px");
		table.setColumnWidth(duration,"120px");
		
		table.setColumnWidth(monthleyDeduction,"160px");
		table.setColumnWidth(advanceType,"110px");
		table.setColumnWidth(status,"100px");
	}

	public void addColumnSorting(){

		addSortinggetFromDate();
		addSortingToDate();
		addSortinggetDuration();
		addSortinggetAmount();
		addSortinggetEmployeeId();
		addSortinggetEmployeeName();
		addSortingEmployeeCell();
		addSortinggetEmployeeBranch();
		addSortinggetEmployeeDepartment();
		addSortingEmployeeType();
		addSortinggetEmployeeDesignation();
		addSdortingEmployeeRole();
		addSortinggetApproverName();
		addSortinggetStatus();
		addSortinggetAdvanceType();
		addSortingDeductionMonthly();
		addSortinggetBalanceAmount();
		addSortinId();
	}
	
	protected void addSortingDeductionMonthly()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(monthleyDeduction, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getDeductionMonthly()== e2.getDeductionMonthly()){
						return 0;}
					if(e1.getDeductionMonthly()> e2.getDeductionMonthly()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	@Override public void addFieldUpdater() {
	}
	protected void addColumngetCount()
	{
		id=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getCount()+"";
			}
				};
				table.addColumn(id,"Id");
				table.setColumnWidth(id, 27,Unit.PX);
				id.setSortable(true);
	}
	protected void addSortinggetEmployeeId()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(employeeId, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getEmpid()== e2.getEmpid()){
						return 0;}
					if(e1.getEmpid()> e2.getEmpid()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployeeId()
	{
		employeeId=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getEmpid()+"";
			}
				};
				table.addColumn(employeeId,"Employee ID");
				employeeId.setSortable(true);
				table.setColumnWidth(employeeId, 130,Unit.PX);
	}
	protected void addSortinggetEmployeeName()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(employeeName, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeeName()!=null && e2.getEmployeeName()!=null){
						return e1.getEmployeeName().compareTo(e2.getEmployeeName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployeeName()
	{
		employeeName=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getEmployeeName()+"";
			}
				};
				table.addColumn(employeeName,"Name");
				employeeName.setSortable(true);
				table.setColumnWidth(employeeName, 170,Unit.PX);
	}

	protected void addSortinggetApproverName()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(approverName, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getApproverName()!=null && e2.getApproverName()!=null){
						return e1.getApproverName().compareTo(e2.getApproverName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetApproverName()
	{
		approverName=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getApproverName()+"";
			}
				};
				table.addColumn(approverName,"Approver");
				approverName.setSortable(true);
				table.setColumnWidth(approverName,"180px");
	}
	protected void addSortinggetStatus()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(status, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		status=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(status,"Status");
				status.setSortable(true);
	}



	protected void addSortinggetFromDate()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(fromDate, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getFromdate()!=null && e2.getFromdate()!=null){
						return e1.getFromdate().compareTo(e2.getFromdate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetFromDate()
	{
		fromDate=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return AppUtility.parseDate(object.getFromdate());
			}
				};
				table.addColumn(fromDate,"Loan Start Date");
				fromDate.setSortable(true);
				table.setColumnWidth(fromDate,120,Unit.PX);
	}

	protected void addSortinggetToDate()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(toDate, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getTodate()!=null && e2.getTodate()!=null){
						return e1.getTodate().compareTo(e2.getTodate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetToDate()
	{
		toDate=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return AppUtility.parseDate(object.getTodate());
			}
				};
				table.addColumn(toDate,"Loan end date");
				toDate.setSortable(true);
				table.setColumnWidth(toDate,120,Unit.PX);
	}

	protected void addSortinggetDuration()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(duration, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDurationInMonth()!=null && e2.getDurationInMonth()!=null){
						return e1.getDurationInMonth().compareTo(e2.getDurationInMonth());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingToDate()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(toDate, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getTodate()!=null && e2.getTodate()!=null){
						return e1.getTodate().compareTo(e2.getTodate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetDuration()
	{
		duration=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getDurationInMonth()+"";
			}
				};
				table.addColumn(duration,"Duration");
				duration.setSortable(true);
				table.setColumnWidth(duration, 75,Unit.PX);
	}



	protected void addSortinggetBalanceAmount()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(balance, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBalance()!=null && e2.getBalance()!=null){
						return e1.getBalance().compareTo(e2.getBalance());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBalanceAmount()
	{
		balance=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getAdvanceAmount()+"";
			}
				};
				table.addColumn(balance,"Balance");
				balance.setSortable(true);
				table.setColumnWidth(amount, 90,Unit.PX);

	}

	protected void addSortinggetAmount()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(amount, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getAdvanceAmount()!=null && e2.getAdvanceAmount()!=null){
						return e1.getAdvanceAmount().compareTo(e2.getAdvanceAmount());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetAmount()
	{
		amount=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getAdvanceAmount()+"";
			}
				};
				table.addColumn(amount,"Amount");
				amount.setSortable(true);
				table.setColumnWidth(amount, 90,Unit.PX);









	}

	protected void addSortinggetAdvanceType()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(advanceType, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getAdvanceType()!=null&&e2.getAdvanceType()!=null)
					  e1.getAdvanceType().compareTo(e2.getAdvanceType());
				}
				return 1;
			}});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetAdvanceType()
	{
		advanceType=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getAdvanceType()+"";
			}
				};
				table.addColumn(advanceType,"AdvanceType");
				advanceType.setSortable(true);
				table.setColumnWidth(advanceType,130 ,Unit.PX);
	}

	private void addColumngetEmployeeRole() {
		employeeRole=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getEmployeeRole();
			}
				};
				table.addColumn(employeeRole,"Role");
				employeeRole.setSortable(true);

	}
	private void addColumngetEmployeeType() {
		employeeType=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getEmployeeType()+"";
			}
				};
				table.addColumn(employeeType,"Type");
				employeeType.setSortable(true);

	}
	private void addColumngetEmployeeBranch() {
		employeeBranch=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getBranch()+"";
			}
				};
				table.addColumn(employeeBranch,"Branch");
				employeeBranch.setSortable(true);

	}
	private void addColumngetEmployeeCell() {
		employeeCell=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getEmpCellNo()+"";
			}
				};
				table.addColumn(employeeCell,"Cell");
				employeeCell.setSortable(true);

	}

	private void addSortingEmployeeCell() {
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(employeeCell, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getEmpCellNo()== e2.getEmpCellNo()){
						return 0;}
					if(e1.getEmpCellNo()> e2.getEmpCellNo()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);

	}
	private void addSdortingEmployeeRole() {
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(employeeRole, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeeRole()!=null && e2.getEmployeeRole()!=null){
						return e1.getEmployeeRole().compareTo(e2.getEmployeeRole());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);

	}
	private void addSortingEmployeeType() {
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(employeeType, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getEmployeeType().compareTo(e2.getEmployeeType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);

	}
	private void addSortinggetEmployeeBranch() {
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(employeeBranch, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);

	}

	/*****************************Employee Designation************************************/
	protected void addSortinggetEmployeeDesignation()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(employeeDesignation, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeedDesignation()!=null && e1.getEmployeedDesignation()!=null){
						return e1.getEmployeedDesignation().compareTo(e2.getEmployeedDesignation());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployeeDesignation()
	{
		employeeDesignation=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getEmployeedDesignation()+"";
			}
				};
				table.addColumn(employeeDesignation,"Designation");
				employeeDesignation.setSortable(true);
	}


	/**********************************Employee Department*******************************/
	protected void addSortinggetEmployeeDepartment()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(employeeDepartment, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDepartment()!=null && e2.getDepartment()!=null){
						return e1.getDepartment().compareTo(e2.getDepartment());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployeeDepartment()
	{
		employeeDepartment=new TextColumn<Loan>()
				{
			@Override
			public String getValue(Loan object)
			{
				return object.getDepartment()+"";
			}
				};
				table.addColumn(employeeDepartment,"Department");
				employeeDepartment.setSortable(true);
	}
	
	protected void addSortinId()
	{
		List<Loan> list=getDataprovider().getList();
		columnSort=new ListHandler<Loan>(list);
		columnSort.setComparator(id, new Comparator<Loan>()
				{
			@Override
			public int compare(Loan e1,Loan e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}


}
