package com.slicktechnologies.client.views.humanresource.advance;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.client.utility.Screen;

public class AdvancePresenterTableProxy extends SuperTable<Loan> {
	TextColumn<Loan> getAdvanceTypeColumn;
	TextColumn<Loan> getAdvanceAmountColumn;
	TextColumn<Loan> getAdvInterestColumn;
	TextColumn<Loan> getEmployeeFullNameColumn;
	TextColumn<Loan> getEmployeeCellColumn;
	TextColumn<Loan> getEmployeeIdColumn;
	TextColumn<Loan> getEmployeeDepartmentColumn;
	TextColumn<Loan> getEmployeeTypeColumn;
	TextColumn<Loan> getEmployeeDesignationColumn;
	TextColumn<Loan> getDeductionAmountColumn;
	TextColumn<Loan> getApproverNameColumn;
	TextColumn<Loan> getStatusColumn;
	TextColumn<Loan> getCountColumn;
	TextColumn<Loan> getBranchColumn;
	TextColumn<Loan> getDesignationColumn;
	TextColumn<Loan> getDepartmentColumn;
	TextColumn<Loan> getRollColumn;
	TextColumn<Loan> getAdvanceBalance;

	// TextColumn<Advance> getAdvanceIdColumn;

	public Object getVarRef(String varName) {
		if (varName.equals("getAdvanceTypeColumn"))
			return this.getAdvanceTypeColumn;
		if (varName.equals("getAdvanceAmountColumn"))
			return this.getAdvanceAmountColumn;
		if (varName.equals("getAdvInterestColumn"))
			return this.getAdvInterestColumn;
		if (varName.equals("getEmployeeFullNameColumn"))
			return this.getEmployeeFullNameColumn;
		if (varName.equals("getEmployeeCellColumn"))
			return this.getEmployeeCellColumn;
		if (varName.equals("getEmployeeIdColumn"))
			return this.getEmployeeIdColumn;
		if (varName.equals("getEmployeeDepartmentColumn"))
			return this.getEmployeeDepartmentColumn;
		if (varName.equals("getEmployeeDesignationColumn"))
			return this.getEmployeeDesignationColumn;
		if (varName.equals("getDeductionAmountColumn"))
			return this.getDeductionAmountColumn;
		if (varName.equals("getApproverNameColumn"))
			return this.getApproverNameColumn;
		if (varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if (varName.equals("getCountColumn"))
			return this.getCountColumn;

		return null;
	}

	public AdvancePresenterTableProxy() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetEmployeeId();
		addColumngetEmployeeFullName();
		addColumngetEmployeeCell();
		addColumngetBranchName();
		addColumngetEmployeeDesignation();
		addColumngetEmployeeDepartment();
		addColumngetEmployeeRole();
		addColumngetEmploymentType();
		addColumngetAdvanceAmount();
		addColumngetAdvInterest();
		addColumngetAdvanceType();
		addColumngetApproverName();
		addColumngetDeductionAmount();
		addColumngetStatus();
		table.setColumnWidth(getCountColumn, "100px");
		table.setColumnWidth(getEmployeeIdColumn, "140px");
		table.setColumnWidth(getEmployeeFullNameColumn, "150px");
		table.setColumnWidth(getApproverNameColumn, "150px");
		table.setColumnWidth(getEmployeeCellColumn, "120px");
		table.setColumnWidth(getBranchColumn, "110px");
		table.setColumnWidth(getEmployeeDesignationColumn, "110px");
		table.setColumnWidth(getEmployeeDepartmentColumn, "110px");
		table.setColumnWidth(getAdvanceAmountColumn, "110px");
		table.setColumnWidth(getAdvInterestColumn, "90px");
		table.setColumnWidth(getDeductionAmountColumn, "110px");
		table.setColumnWidth(getAdvanceTypeColumn, "110px");

		table.setColumnWidth(getDeductionAmountColumn, "100px");
		table.setColumnWidth(getStatusColumn, "100px");
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<Loan>() {
			@Override
			public Object getKey(Loan item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	public void addColumnSorting() {
		addSortingID();
		addSortinggetEmployeeId();
		addSortinggetEmployeeFullName();
		addSortinggetEmployeeCell();
		addSortingEmploymeRole();
		addSortinggetBranchName();

		addSortinggetemployeeType();
		addSortinggetEmployeeDesignation();

		addSortinggetEmployeeDepartment();
		addSortinggetAdvanceAmount();
		addSortinggetAdvInterest();
		addSortinggetAdvanceType();
		addSortinggetApproverName();
		addSortinggetDeductionAmount();
		addSortinggetStatus();
	}

	@Override
	public void addFieldUpdater() {
	}

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		table.addColumnSortHandler(columnSort);
	}
	

	protected void addSortingID() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Loan>() {
			@Override
			public int compare(Loan e1, Loan e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetEmployeeId() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getEmployeeIdColumn, new Comparator<Loan>() {
			@Override
			public int compare(Loan e1, Loan e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmpid() == e2.getEmpid()) {
						return 0;
					}
					if (e1.getEmpid() > e2.getEmpid()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetEmployeeId() {
		getEmployeeIdColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getEmpid() + "";
			}
		};
		table.addColumn(getEmployeeIdColumn, "Employee ID");
		getEmployeeIdColumn.setSortable(true);
	}

	protected void addSortinggetEmployeeDesignation() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getEmployeeDesignationColumn,
				new Comparator<Loan>() {
					@Override
					public int compare(Loan e1, Loan e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmployeedDesignation() != null
									&& e2.getEmployeedDesignation() != null) {
								return e1.getEmployeedDesignation().compareTo(
										e2.getEmployeedDesignation());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetEmployeeDesignation() {
		getEmployeeDesignationColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getEmployeedDesignation() + "";
			}
		};
		table.addColumn(getEmployeeDesignationColumn, "Designation");
		getEmployeeDesignationColumn.setSortable(true);
	}

	protected void addSortinggetEmployeeCell() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getEmployeeCellColumn, new Comparator<Loan>() {
			@Override
			public int compare(Loan e1, Loan e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmpCellNo() == e2.getEmpCellNo()) {
						return 0;
					}
					if (e1.getEmpCellNo() > e2.getEmpCellNo()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetEmployeeCell() {
		getEmployeeCellColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getEmpCellNo() + "";
			}
		};
		table.addColumn(getEmployeeCellColumn, "Employee Cell");
		getEmployeeCellColumn.setSortable(true);
	}

	protected void addSortinggetEmployeeFullName() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getEmployeeFullNameColumn,
				new Comparator<Loan>() {
					@Override
					public int compare(Loan e1, Loan e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmployeeName() != null
									&& e2.getEmployeeName() != null) {
								return e1.getEmployeeName().compareTo(
										e2.getEmployeeName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetEmployeeFullName() {
		getEmployeeFullNameColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getEmployeeName() + "";
			}
		};
		table.addColumn(getEmployeeFullNameColumn, "Employee Name");
		getEmployeeFullNameColumn.setSortable(true);
	}

	protected void addSortinggetEmployeeDepartment() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getEmployeeDepartmentColumn,
				new Comparator<Loan>() {
					@Override
					public int compare(Loan e1, Loan e2) {
						if (e1 != null && e2 != null) {
							if (e1.getDepartment() != null
									&& e2.getDepartment() != null) {
								return e1.getDepartment().compareTo(
										e2.getDepartment());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetEmployeeDepartment() {
		getEmployeeDepartmentColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getDepartment() + "";
			}
		};
		table.addColumn(getEmployeeDepartmentColumn, "Department");
		getEmployeeDepartmentColumn.setSortable(true);
	}

	protected void addSortinggetAdvanceAmount() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getAdvanceAmountColumn,
				new Comparator<Loan>() {
					@Override
					public int compare(Loan e1, Loan e2) {
						if (e1 != null && e2 != null) {
							if (e1.getAdvanceAmount() == e2.getAdvanceAmount()) {
								return 0;
							}
							if (e1.getAdvanceAmount() > e2.getAdvanceAmount()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetAdvanceAmount() {
		getAdvanceAmountColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getAdvanceAmount() + "";
			}
		};
		table.addColumn(getAdvanceAmountColumn, "Amount");
		getAdvanceAmountColumn.setSortable(true);
	}

	protected void addSortinggetAdvInterest() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getAdvInterestColumn, new Comparator<Loan>() {
			@Override
			public int compare(Loan e1, Loan e2) {
				if (e1 != null && e2 != null) {
					if (e1.getAdvInterest() == e2.getAdvInterest()) {
						return 0;
					}
					if (e1.getAdvInterest() > e2.getAdvInterest()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetAdvInterest() {
		getAdvInterestColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getAdvInterest() + "";
			}
		};
		table.addColumn(getAdvInterestColumn, "Interest");
		getAdvInterestColumn.setSortable(true);
	}

	protected void addSortinggetAdvanceType() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getAdvanceTypeColumn, new Comparator<Loan>() {
			@Override
			public int compare(Loan e1, Loan e2) {
				if (e1 != null && e2 != null) {
					if (e1.getAdvanceType() != null
							&& e2.getAdvanceType() != null) {
						return e1.getAdvanceType().compareTo(
								e2.getAdvanceType());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetAdvanceType() {
		getAdvanceTypeColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getAdvanceType() + "";
			}
		};
		table.addColumn(getAdvanceTypeColumn, "AdvanceType");
		getAdvanceTypeColumn.setSortable(true);
	}

	protected void addSortinggetApproverName() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getApproverNameColumn, new Comparator<Loan>() {
			@Override
			public int compare(Loan e1, Loan e2) {
				if (e1 != null && e2 != null) {
					if (e1.getApproverName() != null
							&& e2.getApproverName() != null) {
						return e1.getApproverName().compareTo(
								e2.getApproverName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetApproverName() {
		getApproverNameColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getApproverName() + "";
			}
		};
		table.addColumn(getApproverNameColumn, "Approver Name");
		getApproverNameColumn.setSortable(true);
	}

	protected void addSortinggetDeductionAmount() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getDeductionAmountColumn,
				new Comparator<Loan>() {
					@Override
					public int compare(Loan e1, Loan e2) {
						if (e1 != null && e2 != null) {
							if (e1.getDeductionMonthly() == e2
									.getDeductionMonthly()) {
								return 0;
							}
							if (e1.getDeductionMonthly() > e2
									.getDeductionMonthly()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetDeductionAmount() {
		getDeductionAmountColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getDeductionMonthly() + "";
			}
		};
		table.addColumn(getDeductionAmountColumn, "Deduction");
		getDeductionAmountColumn.setSortable(true);
	}

	protected void addSortinggetStatus() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Loan>() {
			@Override
			public int compare(Loan e1, Loan e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null && e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getStatus() + "";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
	}

	protected void addSortinggetBranchName() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<Loan>() {
			@Override
			public int compare(Loan e1, Loan e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null && e2.getBranch() != null) {
						return e1.getBranch().compareTo(e2.getBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetBranchName() {
		getBranchColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getBranch() + "";
			}
		};
		table.addColumn(getBranchColumn, "Branch");
		getBranchColumn.setSortable(true);
	}

	protected void addSortinggetDesignation() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getDesignationColumn, new Comparator<Loan>() {
			@Override
			public int compare(Loan e1, Loan e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployeedDesignation() != null
							&& e2.getEmployeedDesignation() != null) {
						return e1.getEmployeedDesignation().compareTo(
								e2.getEmployeedDesignation());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetDesignation() {
		getDesignationColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getEmployeedDesignation() + "";
			}
		};
		table.addColumn(getDesignationColumn, "Designation");
		table.setColumnWidth(getDesignationColumn, "110px");
		getDesignationColumn.setSortable(true);
	}

	protected void addSortingEmploymeRole() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getRollColumn, new Comparator<Loan>() {
			@Override
			public int compare(Loan e1, Loan e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployeeRole() != null
							&& e2.getEmployeeRole() != null) {
						return e1.getEmployeeRole().compareTo(
								e2.getEmployeeRole());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetEmployeeRole() {
		getRollColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getEmployeeRole();
			}
		};
		table.addColumn(getRollColumn, "Employee Role");
		table.setColumnWidth(getRollColumn, "130px");
		getRollColumn.setSortable(true);
	}

	protected void addColumngetEmploymentType() {
		getEmployeeTypeColumn = new TextColumn<Loan>() {
			@Override
			public String getValue(Loan object) {
				return object.getEmployeeType() + "";
			}
		};
		table.addColumn(getEmployeeTypeColumn, "Employee Type");
		table.setColumnWidth(getEmployeeTypeColumn, "130px");
		getEmployeeTypeColumn.setSortable(true);
	}

	protected void addSortinggetemployeeType() {
		List<Loan> list = getDataprovider().getList();
		columnSort = new ListHandler<Loan>(list);
		columnSort.setComparator(getEmployeeTypeColumn, new Comparator<Loan>() {
			@Override
			public int compare(Loan e1, Loan e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployeeType() != null
							&& e2.getEmployeeType() != null) {
						return e1.getEmployeeType().compareTo(
								e2.getEmployeeType());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

}
