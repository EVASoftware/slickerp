package com.slicktechnologies.client.views.humanresource.ctcallocation;

import java.util.ArrayList;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.humanresource.allocation.ctcallocation.CtcAllocationService;
import com.slicktechnologies.client.views.humanresource.allocation.ctcallocation.CtcAllocationServiceAsync;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.LeaveAllocationService;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.LeaveAllocationServiceAsync;
import com.slicktechnologies.client.views.popups.GeneralComposite;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.cnc.StaffingDetails;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;

public class CTCAllocationPresenter extends FormScreenPresenter<DummyEntityOnlyForScreen>{

	
	CTCAllocationForm form;
	
	CtcAllocationServiceAsync ctcAllocationService = GWT.create(CtcAllocationService.class);
	GenricServiceAsync asyncgen = GWT.create(GenricService.class);
	
	public CTCAllocationPresenter(UiScreen<DummyEntityOnlyForScreen> view,DummyEntityOnlyForScreen model) {
		super(view, model);
		form = (CTCAllocationForm) view;
		form.empSearch.getBtnGo().addClickHandler(this);
	}

	public static void initialize(){
		AppMemory.getAppMemory().skeleton.isMenuVisble=false;
		CTCAllocationForm form = new CTCAllocationForm();
		CTCAllocationPresenter presenter = new CTCAllocationPresenter(form,new DummyEntityOnlyForScreen());
		AppMemory.getAppMemory().stickPnel(form);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		System.out.println("Process lvl button clicked");
		InlineLabel lbl = (InlineLabel) e.getSource();
		if (lbl.getText().contains("Allocate CTC")) {
			System.out.println("Allocate CTC clicked!!");
			reactOnAllocate();
		}
		if (lbl.getText().contains(AppConstants.SALARYREVISIONANDARREARS)) {
			System.out.println("SALARY REVISION..");
//			reactOnAllocate();
			
			reactOnSalaryRevision();
		}
	}
	
	private void reactOnSalaryRevision() {
		// TODO Auto-generated method stub
		if (!validateAndUpdateModel()){
			return;
		}
		
		if(form.empSearch.getAllocatedValues()==null){
			form.showDialogMessage("No allocated result to process.");
			return;
		}
		List<EmployeeInfo> array =form.empSearch.getAllocatedValues();
		if (array == null){
			form.showDialogMessage("No allocated result to process.");
			return;
		}
		
		CTCTemplate ctcTemplate = null;
		if(form.generalComposite.name.getValue()!=null||!form.generalComposite.name.getValue().equals("")){
			ctcTemplate=(CTCTemplate) form.generalComposite.getModel();
		}else{
			form.showDialogMessage("Please select CTC Template.");
			return;
		}
		
		
		form.showWaitSymbol();
		
		ArrayList<EmployeeInfo> allcationResult =new ArrayList<EmployeeInfo>();
		allcationResult.addAll(array);
		
		ctcAllocationService.reviseSalary(allcationResult,ctcTemplate,form.dbApplicableFormDate.getValue(),form.dbApplicableToDate.getValue(),form.dbEffectiveFromDate.getValue(),LoginPresenter.loggedInUser,form.dbEffectiveTo.getValue(),form.project.getValue(),form.cbTemplateWiseRevision.getValue(),new AsyncCallback<ArrayList<EmployeeInfo>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				view.showDialogMessage("Failed to execute Salary Revision Process !");
			}
			@Override
			public void onSuccess(ArrayList<EmployeeInfo> result) {
				form.hideWaitSymbol();
				List<EmployeeInfo> allocatedEmployees = new ArrayList<EmployeeInfo>();
//				List<AllocationResult> unallocatedEmployees = new ArrayList<AllocationResult>();
//				for (AllocationResult res : result) {
//					if (res.status == true)
//						allocatedEmployees.add(res.getInfo());
//					else
//						unallocatedEmployees.add(res);
//				}
//				form.empSearch.setUnAllocatedValues(unallocatedEmployees);
//				form.empSearch.setAllocatedValues(allocatedEmployees);
				form.empSearch.setAllocatedValues(result);
				view.showDialogMessage("Salary Revision Process Successfully executed !",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
			}
		});
	}

	private void reactOnAllocate() {
		System.out.println("inside reactOnAllocate()");
		if (!validateAndUpdateModel()){
			return;
		}
		if(form.empSearch.getUnAllocatedValues()==null){
			form.showDialogMessage("No unallocated result to process.");
			return;
		}
		List<AllocationResult> array =form.empSearch.getUnAllocatedValues();
		if (array == null){
			form.showDialogMessage("No unallocated result to process.");
			return;
		}
//		for(AllocationResult allRes:array){
//			if(allRes.getLeaveCalendar()==null){
//				form.showDialogMessage("Please select only those employee whome calendar is assiged.");
//				return;
//			}
//		}
		
		
//		if(form.olbCtcTemplate.getSelectedIndex()!=0){
//			ctcTemplate=form.olbCtcTemplate.getSelectedItem();
//		}else{
//			form.showDialogMessage("Please select CTC Template.");
//			return;
//		}
		
		
		
		/**Date 6-2-2019
		 * @author Amol*****/
		
		CTCTemplate ctcTemplate = null;
		if(form.generalComposite.name.getValue()!=null||!form.generalComposite.name.getValue().equals("")){
			ctcTemplate=(CTCTemplate) form.generalComposite.getModel();
		}else{
			form.showDialogMessage("Please select CTC Template.");
			return;
		}
		
     System.out.println("ctctemplate"+ctcTemplate);
		
		
		
		
//		ArrayList<EmployeeInfo> empInfoarray = new ArrayList<EmployeeInfo>();
//		for (AllocationResult res : array) {
//			empInfoarray.add(res.info);
//		}

//		ArrayList<LeaveBalance> allocateLeavesArray = new ArrayList<LeaveBalance>();
//		if (empInfoarray.size() == 0) {
//			form.showDialogMessage("No Employee found for the applicable filter !");
//			return;
//		}
//		allocateLeavesArray = getLeaveBalanceList(empInfoarray);

		form.showWaitSymbol();
		
		ArrayList<AllocationResult> allcationResult =new ArrayList<AllocationResult>();
		allcationResult.addAll(array);
//		System.out.println("ctctemplate11"+ctcTemplate);
		ctcAllocationService.allocateCtc(allcationResult,ctcTemplate,form.dbApplicableFormDate.getValue(),form.dbApplicableToDate.getValue(),form.dbEffectiveFromDate.getValue(),LoginPresenter.loggedInUser,form.dbEffectiveTo.getValue(),form.project.getValue(),new AsyncCallback<ArrayList<AllocationResult>>() {
		
			@Override
			
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				view.showDialogMessage("Failed to execute CTC Allocation Process !");
			}
			
			@Override
			public void onSuccess(ArrayList<AllocationResult> result) {
				form.hideWaitSymbol();
				List<EmployeeInfo> allocatedEmployees = new ArrayList<EmployeeInfo>();
				List<AllocationResult> unallocatedEmployees = new ArrayList<AllocationResult>();
				for (AllocationResult res : result) {
					if (res.status == true)
						allocatedEmployees.add(res.getInfo());
					else
						unallocatedEmployees.add(res);
					
					updateGlobalEmployeeInfo(res.getInfo());

				}
				form.empSearch.setUnAllocatedValues(unallocatedEmployees);
				form.empSearch.setAllocatedValues(allocatedEmployees);
				view.showDialogMessage("CTC Allocation Process Successfully executed !",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
			}
		});
	}
	 
	
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		if (event.getSource() == form.empSearch.btnGo) {
			ArrayList<Filter> employeeFilter = form.empSearch.getEmployeeFilter();
			getAllocationStatusAndUpdateTable(employeeFilter);
		}
	 
		   
		
		
		
		
	}
	
	
	private List<AllocationResult> getAllocationStatusAndUpdateTable(ArrayList<Filter> filter) {
		System.out.println("Inside list METHOD ");
		// The List of Allocation Result to be filled from server
		final List<AllocationResult> lis = new ArrayList<AllocationResult>();
		
		if (filter == null){
			return null;
		}
		
		form.showWaitSymbol();
		ctcAllocationService.getAllocationStatus(filter,new AsyncCallback<ArrayList<AllocationResult>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
			@Override
			public void onSuccess(ArrayList<AllocationResult> result) {
				form.hideWaitSymbol();
				updateTable(result);
			}
		});
		return lis;
	}
	
	private void updateTable(ArrayList<AllocationResult> result) {
		List<EmployeeInfo> allocatedEmployees = new ArrayList<EmployeeInfo>();
		List<AllocationResult> unallocatedEmployees = new ArrayList<AllocationResult>();

		for (AllocationResult res : result) {
			if (res.status == true && res.getInfo().isCtcAllocated()==true) {
				allocatedEmployees.add(res.getInfo());
			} else {
				unallocatedEmployees.add(res);
			}
		}
		form.empSearch.setAllocatedValues(allocatedEmployees);
		form.empSearch.setUnAllocatedValues(unallocatedEmployees);
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}
	
	protected void updateGlobalEmployeeInfo(EmployeeInfo employeeInfo) {

		for(int i=0;i<LoginPresenter.globalEmployeeInfo.size();i++)
		{
			if(LoginPresenter.globalEmployeeInfo.get(i).getId().equals(employeeInfo.getId()))
			{
				LoginPresenter.globalEmployeeInfo.add(employeeInfo);
				LoginPresenter.globalEmployeeInfo.remove(i);
			}
		}
	}
	
}
