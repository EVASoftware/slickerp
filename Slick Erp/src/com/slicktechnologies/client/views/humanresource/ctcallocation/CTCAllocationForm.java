package com.slicktechnologies.client.views.humanresource.ctcallocation;

import java.awt.Checkbox;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.allocation.EmployeeSearch;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.client.views.humanresource.allocation.payslipallocation.UnAllocatedEmployeeInformationTable;
import com.slicktechnologies.client.views.popups.GeneralComposite;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class CTCAllocationForm extends FormScreen<DummyEntityOnlyForScreen>{



	EmployeeSearch empSearch;
	Button btnallocate;
	ObjectListBox<CTCTemplate>olbCtcTemplate; 
	/*Date 5-2-2019
	 * @author Amol 
	 * ****/
	GeneralComposite generalComposite;
	/**
	 * Date : 10-05-2018 By ANIL
	 * 
	 */
	DateBox dbApplicableFormDate;
	DateBox dbApplicableToDate;
	DateBox dbEffectiveFromDate;
	
	/**
     * @author Anil , Date : 12-07-2019
     * Capturing project name for processing multiple site payroll
     * raised by Rahul tiwari for riseon
     */
   
   ObjectListBox<HrProject> project;
   DateBox dbEffectiveTo;
   
   /**
    * @author Anil , Date : 03-10-2019
    * Template wise revision
    * raised by Sasha
    * want to revise salary after changing template
    */
   CheckBox cbTemplateWiseRevision;
	
	
	public  CTCAllocationForm() {
		super(FormStyle.DEFAULT);
		createGui();
//		processLevelBar.btnLabels[0].setVisible(true);
		processLevelBar.setVisibleFalse(true);
//		olbCtcTemplate.getElement().getStyle().setWidth(221,Unit.PX);
		cbTemplateWiseRevision.setValue(false);
	}


	private void initalizeWidget(){
		empSearch=new EmployeeSearch(true);
		empSearch.btnDownload.setVisible(false);//Ashwini Patil hiding the button as there is no code written for button click
		
		Filter temp=new Filter();
		temp.setBooleanvalue(true);
		temp.setQuerryString("status");
		Vector<Filter> vecTemp=new Vector<Filter>();
		vecTemp.add(temp);
		olbCtcTemplate=new ObjectListBox<CTCTemplate>();
		olbCtcTemplate.MakeLive(new MyQuerry(vecTemp, new CTCTemplate()));
		
		dbApplicableFormDate=new DateBoxWithYearSelector();
		dbApplicableToDate=new DateBoxWithYearSelector();
		dbEffectiveFromDate=new DateBoxWithYearSelector();
		MyQuerry qu = new MyQuerry();
		Company c = new Company();
		System.out.println("Company Id :: " + c.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CtcAllocation", "LoadHrTemplate")){
			ArrayList<String> categoryList=new ArrayList<String>();
			categoryList.add("HR");
			categoryList.add("Hr");
			categoryList.add("hr");
			categoryList.add("");
			
			filter = new Filter();
			filter.setQuerryString("category IN");
			filter.setList(categoryList);
			filtervec.add(filter);
		}

		
		qu.setFilters(filtervec);
		qu.setQuerryObject(new CTCTemplate());
		/**6-2-2019  "ctc template"  suggestion box Added by Amol
		 * 
		 */
		generalComposite = new GeneralComposite("", qu);
		
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
		dbEffectiveTo=new DateBoxWithYearSelector();
		
		cbTemplateWiseRevision=new CheckBox();
		cbTemplateWiseRevision.setValue(false);
	}

	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames=new String[]{"Allocate CTC",AppConstants.SALARYREVISIONANDARREARS};
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPayRollPeriod=fbuilder.setlabel("CTC Template ").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* CTC Template",olbCtcTemplate);
		FormField fPayRollPeriod=fbuilder.setMandatory(true).setMandatoryMsg("CTC Template is Mandatory !").setColSpan(0).build();
		
		
		FormField fgroupingInformation=fbuilder.setlabel("Apply Filters").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("",empSearch);
		FormField fapplyfilter= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Applicable From",dbApplicableFormDate);
		FormField fdbApplicableFormDate=fbuilder.setMandatory(true).setMandatoryMsg("From Date is Mandatory !").setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Applicable To",dbApplicableToDate);
		FormField fdbApplicableToDate=fbuilder.setMandatory(true).setMandatoryMsg("To Date is Mandatory !").setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Effective From",dbEffectiveFromDate);
		FormField fdbEffectiveFromDate=fbuilder.setMandatory(true).setMandatoryMsg("Effective From Date is Mandatory !").setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("*CTC Template",generalComposite);
		FormField fgeneralComposite=fbuilder.setMandatory(true).setMandatoryMsg("CTC Template is  Mandatory !").setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project", project);
		FormField fproject = fbuilder.setMandatory(false).setMandatoryMsg("Project is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Effective To Date",dbEffectiveTo);
	    FormField fdbEffectiveTo= fbuilder.setMandatory(true).setMandatoryMsg("Effective To Date is Manadatory !").setRowSpan(0).setColSpan(0).build();
	    
	    fbuilder = new FormFieldBuilder("Template wise salary revision", cbTemplateWiseRevision);
		FormField fcbTemplateWiseRevision = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CtcAllocation", "SiteWisePayrollForReliever")){
			 FormField[][] formfield = {  
						{fgroupingPayRollPeriod},
						{fgeneralComposite,fdbApplicableFormDate,fdbApplicableToDate,fdbEffectiveFromDate},
						{fdbEffectiveTo,fproject,fcbTemplateWiseRevision},
						{fgroupingInformation},
						{fapplyfilter},
				};
				this.fields=formfield;
		}else{
			 FormField[][] formfield = {  
						{fgroupingPayRollPeriod},
						{fgeneralComposite,fdbApplicableFormDate,fdbApplicableToDate,fdbEffectiveFromDate},
						{fdbEffectiveTo,fcbTemplateWiseRevision},
						{fgroupingInformation},
						{fapplyfilter},
				};
				this.fields=formfield;
		}
	    
	   
	}


	@Override
	public void updateModel(DummyEntityOnlyForScreen model) 
	{
	}

	@Override
	public void updateView(DummyEntityOnlyForScreen view) 
	{
		presenter.setModel(view);
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PROCESSPAYROLL,LoginPresenter.currentModule.trim());
	}

	

	@Override
	public void setToNewState() {
		super.setToNewState();
//		processLevelBar.btnLabels[0].setVisible(true);
		processLevelBar.setVisibleFalse(true);
	}
	
	public UnAllocatedEmployeeInformationTable getUnAllocated() {
		return empSearch.getUnAllocated();
	}
	public List<AllocationResult> getUnAllocatedValues() {
		return empSearch.getUnAllocatedValues();
	}

	
	
	


}
