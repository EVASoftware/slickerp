package com.slicktechnologies.client.views.humanresource.companyscontribution;

import java.util.ArrayList;
import java.util.Vector;
import java.util.List;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.arrears.ArrearsDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CompanyPayrollRecord;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class CompanysContributionListPresenter extends TableScreenPresenter<CompanyPayrollRecord> {
	TableScreen<CompanyPayrollRecord>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	public CompanysContributionListPresenter(TableScreen<CompanyPayrollRecord> view, CompanyPayrollRecord model) {
		super(view, model);
		form=view;
		System.out.println("INSIDE CONSTRUCTOR.....");
//		view.retriveTable(getProductTransactionListQuery());//Ashwini Patil Date:23-08-2024 to stop auto loading

		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.COMPANYSCONTRIBUTION,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	private MyQuerry getProductTransactionListQuery() {
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new CompanyPayrollRecord());
		return quer;
	}

//	public CompanysContributionListPresenter(TableScreen<CompanyPayrollRecord> view,CompanyPayrollRecord model,MyQuerry querry) {
//		super(view, model);
//		form=view;
//		view.retriveTable(querry);
//		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.COMPANYSCONTRIBUTION,LoginPresenter.currentModule.trim());
//		if(isDownload==false){
//			form.getSearchpopupscreen().getDwnload().setVisible(false);
//		}
//
//	}
	
	public static void initalize()
	{
		CompanysContributionListTableProxy table=new CompanysContributionListTableProxy();
		CompanysContributionListForm form=new CompanysContributionListForm(table);
		
		CompanyContributionListSearchProxy.staticSuperTable=table;
		CompanyContributionListSearchProxy searchPopUp=new CompanyContributionListSearchProxy(false);
		form.setSearchpopupscreen(searchPopUp);
		
		CompanysContributionListPresenter presenter=new CompanysContributionListPresenter(form,new CompanyPayrollRecord());
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
//	public static void initalize(MyQuerry querry)
//	{
//		CompanysContributionListTableProxy table=new CompanysContributionListTableProxy();
//		CompanysContributionListForm form=new CompanysContributionListForm(table);
//		
//		CompanyContributionListSearchProxy.staticSuperTable=table;
//		CompanyContributionListSearchProxy searchPopUp=new CompanyContributionListSearchProxy(false);
//		form.setSearchpopupscreen(searchPopUp);
//		
//		CompanysContributionListPresenter presenter=new CompanysContributionListPresenter(form,new CompanyPayrollRecord(),querry);
//		form.setToNewState();
//		AppMemory.getAppMemory().stickPnel(form);
//	}

	
	@Override
	protected void makeNewModel() {
		model=new CompanyPayrollRecord();
	}
	
	@Override
	public void reactOnDownload() {
		ArrayList<CompanyPayrollRecord> companycontributionArray=new ArrayList<CompanyPayrollRecord>();
		List<CompanyPayrollRecord> list=(List<CompanyPayrollRecord>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();

		companycontributionArray.addAll(list);
		csvservice.setCompanycontributionlist(companycontributionArray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}

			@Override
			public void onSuccess(Void result) {

				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+159;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	
	
	
	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}
}
