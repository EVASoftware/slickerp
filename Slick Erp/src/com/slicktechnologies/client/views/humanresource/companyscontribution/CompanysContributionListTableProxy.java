package com.slicktechnologies.client.views.humanresource.companyscontribution;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CompanyPayrollRecord;
import com.slicktechnologies.shared.common.inventory.GRN;

public class CompanysContributionListTableProxy extends SuperTable<CompanyPayrollRecord> {

	TextColumn<CompanyPayrollRecord> getPayRollMonthColoumn;
	TextColumn<CompanyPayrollRecord> getEmpidColoumn;
	TextColumn<CompanyPayrollRecord> getEmpNameColoumn;
	TextColumn<CompanyPayrollRecord> getCtcComponentName;
	TextColumn<CompanyPayrollRecord> getAmount;
	
	
	public CompanysContributionListTableProxy() {
		super();
	}
	
	@Override
	public void createTable() {
		addColumnPayrollMonth();
		addColumnEmpId();
		addColumnEmpName();
		addColumnCtcComponentName();
		addColumnCtcAmount();
	}

	public void addColumnSorting() {
		addSortingPayrollMonth();
		addSortingEmpId();
		addSortingEmpName();
		addSortingCtcComponentName();
		addSortingCtcAmount();
	}
	
	
	
	
	
	private void addSortingPayrollMonth() {
		List<CompanyPayrollRecord> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyPayrollRecord>(list);
		columnSort.setComparator(getPayRollMonthColoumn, new Comparator<CompanyPayrollRecord>() {
			@Override
			public int compare(CompanyPayrollRecord e1, CompanyPayrollRecord e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPayrollMonth() != null && e2.getPayrollMonth() != null) {
						return e1.getPayrollMonth().compareTo(e2.getPayrollMonth());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingEmpId() {
		List<CompanyPayrollRecord> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyPayrollRecord>(list);
		columnSort.setComparator(getEmpidColoumn, new Comparator<CompanyPayrollRecord>() {
			@Override
			public int compare(CompanyPayrollRecord e1, CompanyPayrollRecord e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmpId() == e2.getEmpId()) {
						return 0;
					}
					if (e1.getEmpId() > e2.getEmpId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingEmpName() {
		List<CompanyPayrollRecord> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyPayrollRecord>(list);
		columnSort.setComparator(getEmpNameColoumn, new Comparator<CompanyPayrollRecord>() {
			@Override
			public int compare(CompanyPayrollRecord e1, CompanyPayrollRecord e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmpName() != null && e2.getEmpName() != null) {
						return e1.getEmpName().compareTo(e2.getEmpName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingCtcComponentName() {
		List<CompanyPayrollRecord> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyPayrollRecord>(list);
		columnSort.setComparator(getCtcComponentName, new Comparator<CompanyPayrollRecord>() {
			@Override
			public int compare(CompanyPayrollRecord e1, CompanyPayrollRecord e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCtcComponentName() != null && e2.getCtcComponentName() != null) {
						return e1.getCtcComponentName().compareTo(e2.getCtcComponentName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingCtcAmount() {
		List<CompanyPayrollRecord> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyPayrollRecord>(list);
		columnSort.setComparator(getAmount, new Comparator<CompanyPayrollRecord>() {
			@Override
			public int compare(CompanyPayrollRecord e1, CompanyPayrollRecord e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCtcAmount() == e2.getCtcAmount()) {
						return 0;
					}
					if (e1.getCtcAmount() > e2.getCtcAmount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	

	private void addColumnPayrollMonth() {
		getPayRollMonthColoumn = new TextColumn<CompanyPayrollRecord>() {
			@Override
			public String getValue(CompanyPayrollRecord object) {
				return object.getPayrollMonth()+ "";
			}
		};
		table.addColumn(getPayRollMonthColoumn, "Payroll Month");
		table.setColumnWidth(getPayRollMonthColoumn,110,Unit.PX);
		getPayRollMonthColoumn.setSortable(true);
	}

	private void addColumnEmpId() {
		getEmpidColoumn = new TextColumn<CompanyPayrollRecord>() {
			@Override
			public String getValue(CompanyPayrollRecord object) {
				return object.getEmpId()+ "";
			}
		};
		table.addColumn(getEmpidColoumn, "Employee Id");
		table.setColumnWidth(getEmpidColoumn,110,Unit.PX);
		getEmpidColoumn.setSortable(true);
	}

	private void addColumnEmpName() {
		getEmpNameColoumn = new TextColumn<CompanyPayrollRecord>() {
			@Override
			public String getValue(CompanyPayrollRecord object) {
				return object.getEmpName();
			}
		};
		table.addColumn(getEmpNameColoumn, "Employee Name");
		table.setColumnWidth(getEmpNameColoumn,110,Unit.PX);
		getEmpNameColoumn.setSortable(true);
	}

	private void addColumnCtcComponentName() {
		getCtcComponentName = new TextColumn<CompanyPayrollRecord>() {
			@Override
			public String getValue(CompanyPayrollRecord object) {
				return object.getCtcComponentName()+ "";
			}
		};
		table.addColumn(getCtcComponentName, "Component Name");
		table.setColumnWidth(getCtcComponentName,130,Unit.PX);
		getCtcComponentName.setSortable(true);
	}

	private void addColumnCtcAmount() {
		getAmount = new TextColumn<CompanyPayrollRecord>() {
			@Override
			public String getValue(CompanyPayrollRecord object) {
				return object.getCtcAmount()+ "";
			}
		};
		table.addColumn(getAmount, "Amount");
		table.setColumnWidth(getAmount,110,Unit.PX);
		getAmount.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
