package com.slicktechnologies.client.views.humanresource.companyscontribution;

import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CompanyPayrollRecord;

public class CompanyContributionListSearchProxy extends SearchPopUpScreen<CompanyPayrollRecord> {

	EmployeeInfoComposite personInfo;
	ListBox lbMonth;
	
	
	
	/**8-1-2019 added by amol for from date and to date**/
	DateBoxWithYearSelector dbFromDate;
	DateBoxWithYearSelector dbToDate;
	
	public CompanyContributionListSearchProxy() {
		super();
		createGui();
	}
	
	public CompanyContributionListSearchProxy(boolean b) {
		super(b);
		createGui();
		setFormat();
	}
	
	
	public void initWidget()
	{
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
		
		lbMonth=new ListBox();
		lbMonth.addItem("--SELECT--");
		lbMonth.addItem("January");
		lbMonth.addItem("February");
		lbMonth.addItem("March");
		lbMonth.addItem("April");
		lbMonth.addItem("May");
		lbMonth.addItem("June");
		lbMonth.addItem("July");
		lbMonth.addItem("August");
		lbMonth.addItem("September");
		lbMonth.addItem("October");
		lbMonth.addItem("November");
		lbMonth.addItem("December");
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		
		
		setFormat();
		
		
		
	}
	
	public void setFormat(){
		DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MMM");
		DateBox.DefaultFormat defformat=new DateBox.DefaultFormat(format);
		dbFromDate.setFormat(defformat);
		dbFromDate.setValue(new Date());
		
		dbToDate.setFormat(defformat);
		dbToDate.setValue(new Date());
	}
	
	public String getFromDate(){
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MMM");
		if (dbFromDate.getValue() == null) {
			return null;
		} else {
			return format.format(dbFromDate.getValue());
		}
	}
	
	public String getToDate(){
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MMM");
		if (dbToDate.getValue() == null) {
			return null;
		} else {
			return format.format(dbToDate.getValue());
		}
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		builder = new FormFieldBuilder("Month",lbMonth);
		FormField flbMonth= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("From",dbFromDate);
		FormField fdbApplicableFromDate=builder.setMandatory(false).setMandatoryMsg("From Date is Mandatory !").setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To",dbToDate);
		FormField fdbApplicableToDate=builder.setMandatory(false).setMandatoryMsg("To Date is Mandatory !").setRowSpan(0).setColSpan(0).build();
		
		
		this.fields=new FormField[][]{
				{fpersonInfo},
				{fdbApplicableFromDate,fdbApplicableToDate}
		};
	}
	
	@Override
	  public MyQuerry getQuerry() {
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MMM");
		
		
		if(dbFromDate.getValue()!=null&&dbToDate.getValue()!=null){
//			DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MMM");
			
			Date fromDate=CalendarUtil.copyDate(dbFromDate.getValue());
			Date toDate=CalendarUtil.copyDate(dbToDate.getValue());
			
			ArrayList<String> monthList=new ArrayList<String>();
			
			String frmDt=format.format(fromDate);
			String toDt=format.format(toDate);
			if(frmDt.equals(toDt)){
				temp=new Filter();
				temp.setStringValue(frmDt);
				temp.setQuerryString("payrollMonth");
				filtervec.add(temp);
			}else{
				while(!frmDt.equals(toDt)){
					monthList.add(frmDt);
					CalendarUtil.addMonthsToDate(fromDate, 1);
					frmDt=format.format(fromDate);
				}
				
				temp=new Filter();
				temp.setList(monthList);
				temp.setQuerryString("payrollMonth IN");
				filtervec.add(temp);
			}
			
			
		}else if(dbFromDate.getValue()!=null&&dbToDate.getValue()==null){
			temp=new Filter();
			temp.setStringValue(getFromDate());
			temp.setQuerryString("payrollMonth");
			filtervec.add(temp);
		}else if(dbFromDate.getValue()==null&&dbToDate.getValue()!=null){
			temp=new Filter();
			temp.setStringValue(getToDate());
			temp.setQuerryString("payrollMonth");
			filtervec.add(temp);
		}
   		
		
		
		
		
		
		
		if(!personInfo.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
			temp.setQuerryString("empId");
			filtervec.add(temp);
		}
		
		if(!(personInfo.getName().getValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(personInfo.getName().getValue());
			temp.setQuerryString("empName");
			filtervec.add(temp);
		}
		
		if(lbMonth.getSelectedIndex()!=0)
		{
			Date date=new Date(); 
			
			DateTimeFormat fmt = DateTimeFormat.getFormat("yyyy");
			String year=fmt.format(date);
			int month=lbMonth.getSelectedIndex();
			String salaryPeriod;
			
			switch(month){
			
			case 1:
				salaryPeriod=year+"-Jan";
				break;
			case 2:
				salaryPeriod=year+"-Feb";
				break;
			case 3:
				salaryPeriod=year+"-Mar";
				break;
			case 4:
				salaryPeriod=year+"-Apr";
				break;
			case 5:
				salaryPeriod=year+"-May";
				break;
			case 6:
				salaryPeriod=year+"-Jun";
				break;
			case 7:
				salaryPeriod=year+"-Jul";
				System.out.println("MONTH---"+salaryPeriod);
				break;
			case 8:
				salaryPeriod=year+"-Aug";
				break;
			case 9:
				salaryPeriod=year+"-Sep";
				break;
			case 10:
				salaryPeriod=year+"-Oct";
				break;
			case 11:
				salaryPeriod=year+"-Nov";
				break;
			case 12:
				salaryPeriod=year+"-Dec";
				break;
			default:
				salaryPeriod=year+"";
				break;	
				
			}
			
			temp=new Filter();
			temp.setStringValue(salaryPeriod);
			temp.setQuerryString("payrollMonth");
			filtervec.add(temp);
		}
		

		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CompanyPayrollRecord());
		return querry;

	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		
		
		
		if (dbFromDate.getValue() != null && dbToDate.getValue() != null) {
			if (dbToDate.getValue().before(dbFromDate.getValue())) {
				showDialogMessage("To date should be after from date.");
				return false;
			}
		}
		return true;
	}
		
		
	}


