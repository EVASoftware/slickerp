package com.slicktechnologies.client.views.humanresource.overtime;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.poi.ss.formula.ptg.TblPtg;

import com.google.appengine.api.search.query.ExpressionParser.negation_return;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.HolidayType;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.OtEarningComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class OvertimeForm extends FormTableScreen<Overtime> implements ClickHandler, ChangeHandler, ValueChangeHandler<Integer>{

	TextBox ibOvertimeId;
	TextBox tbOvertimeName;// tbOvertimeShortName;
	ObjectListBox<String> olbOvertimeShortName; //Ashwini Patil Date:29-04-2022 Description: Since OT short names are fixed instead of textbox providing drop down as per Nitin sir's Instruction
	CheckBox cbHourly, cbFlateRate,cbstatus;
	DoubleBox dblHourlyRate,dbFlateRate,dbPercentOf1DSal ;
	
	CheckBox cbCountLeave;
	DoubleBox dbMultiplierHour;
	ObjectListBox<LeaveType> olbleaves;
	
	/**
	 * Date : 30-05-2018 By ANIL
	 * Adding holiday type listbox for which compensatory leave is applicable
	 */
	ObjectListBox<HolidayType> olbHolidayType;
	RadioButton rbAppForWeeklyOff;
	RadioButton rbAppForNormalDays;
	
	/**
	 * @author Anil , Date : 07-06-2019
	 * For adding new OT type : OT(Formula)
	 * For Riseon by Rahul Tiwari
	 */
//	ObjectListBox<CtcComponent> lbEarningComponent;
	ListBox lbEarningComponent;
	OtEarningCompTable otEarningCompTbl;
	Button btnAdd;
	
	ListBox lbApplicableDays;
	IntegerBox ibApplicableDays;
	DoubleBox dbWrokingHours;
	DoubleBox dbRate;
	CheckBox cbOtForHourly;
	CheckBox cbOtForFlat;
	
	CheckBox cbPayOTAsOther;
	TextBox tbOtCorrespondanceName;
	
	
	
	
	public OvertimeForm(SuperTable<Overtime> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		cbstatus.setValue(true);
		dbFlateRate.setEnabled(false);
		dblHourlyRate.setEnabled(false);
		dbMultiplierHour.setEnabled(false);
		olbleaves.setEnabled(false);
		
		
		cbOtForFlat.setValue(false);
		cbOtForHourly.setValue(false);
		
		disableEnableFormulaOtWidget();
	}

	private void initalizeWidget() {

		ibOvertimeId = new TextBox();
		ibOvertimeId.setEnabled(false);
		tbOvertimeName = new TextBox();
		tbOvertimeName.setTitle(" Name the overtime appropriately ");
		//tbOvertimeShortName = new TextBox();
		olbOvertimeShortName=new ObjectListBox<String>(); 		
		olbOvertimeShortName.addItem("--SELECT--");
		olbOvertimeShortName.addItem("OT");
		olbOvertimeShortName.addItem("DOT");
		olbOvertimeShortName.addItem("NHOT");
		olbOvertimeShortName.addItem("PHOT");
		olbOvertimeShortName.setTitle("Select the short name for overtime \r\n"
				+ "OT - Normal OT\r\n"
				+ "DOT - Double OT\r\n"
				+ "NHOT - National Holiday OT\r\n"
				+ "PHOT - Public Holiday OT");
		
		cbHourly = new CheckBox();
		cbHourly.addClickHandler(this);
		cbHourly.setTitle("Hourly rate per hour fixed (Not based on salary structure)");
		
		dblHourlyRate=new DoubleBox();
		dblHourlyRate.setTitle("Hourly rate per hour fixed (Not based on salary structure)");
		
		cbFlateRate = new CheckBox();
		cbFlateRate.addClickHandler(this);
		cbFlateRate.setTitle("Flat OT irrespective of number of hours worked extra (Not based on salary structure)");
		
		dbFlateRate=new DoubleBox();
		dbFlateRate.setTitle("Flat OT irrespective of number of hours worked extra (Not based on salary structure)");
		
		dbPercentOf1DSal=new DoubleBox();
		dbPercentOf1DSal.setTitle("% of Gross Earning Per day ");
		
		cbCountLeave = new CheckBox();
		cbCountLeave.addClickHandler(this);
		cbCountLeave.setTitle(" If leave is to be given as OT ");
		
		dbMultiplierHour=new DoubleBox();
		dbMultiplierHour.setTitle("If leave is to be given based on OT hours i.e. 5 hours additional leave for 5 hours OT if factor 1, if factor 2 then 10, if 0.5 then 2.5 hours. \r\n"
				+ "OT Leave = OT hours * Factor ");
		
		olbleaves=new ObjectListBox<LeaveType>();
		initializePaidLeaveType();
		olbleaves.setEnabled(false);
		olbleaves.setTitle(" OT leave type to be given ");
//		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new LeaveType());
//		olbleaves.MakeLive(querry);
		
		
		cbstatus = new CheckBox();
		cbstatus.setValue(true);
		
		olbHolidayType=new ObjectListBox<HolidayType>();
		olbHolidayType.setTitle("OT to be applicable for type of holiday");
		MyQuerry querry = new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		querry.setQuerryObject(new HolidayType());
		olbHolidayType.MakeLive(querry);
		
		rbAppForNormalDays=new RadioButton("group1", "Normal Days");
		rbAppForNormalDays.setValue(false);
		rbAppForNormalDays.addClickHandler(this);
		rbAppForNormalDays.setTitle(" OT to be calculated on normal working day");
		rbAppForWeeklyOff=new RadioButton("group1", "Weekly Off");
		rbAppForWeeklyOff.setValue(false);
		rbAppForWeeklyOff.addClickHandler(this);
		rbAppForWeeklyOff.setTitle("OT to be calculated on Week Off days");
		
		olbHolidayType.addChangeHandler(this);
		
		
//		lbEarningComponent=new ObjectListBox<CtcComponent>();
//		MyQuerry q1=new MyQuerry();
//		Filter f1=new Filter();
//		f1.setQuerryString("status");
//		f1.setBooleanvalue(true);
//		q1.getFilters().add(f1);
//		
//		Filter f2=new Filter();
//		f2.setQuerryString("isDeduction");
//		f2.setBooleanvalue(false);
//		q1.getFilters().add(f2);
//		
//		q1.setQuerryObject(new CtcComponent());
//		lbEarningComponent.MakeLive(q1);
//		lbEarningComponent.addChangeHandler(this);
		
		lbEarningComponent=new ListBox();
		lbEarningComponent.addChangeHandler(this);
		lbEarningComponent.addItem("--SELECT--");
		lbEarningComponent.setTitle(" Calculate OT based on earning components defined here");
		
		otEarningCompTbl=new OtEarningCompTable();
		btnAdd=new Button("Add");
		btnAdd.addClickHandler(this);
		
		lbApplicableDays=new ListBox();
		lbApplicableDays.addItem("--SELECT--");
		lbApplicableDays.addItem("Monthly Days");
		lbApplicableDays.addItem("Paid Days");
		lbApplicableDays.addItem("Working Days");
		lbApplicableDays.addChangeHandler(this);
		lbApplicableDays.setTitle("Monthly Days - Calendars days i.e. 30 day for April for OT  calculation\r\n"
				+ "Paid Days - OT will be based on paid days ( excl. unpaid days)\r\n"
				+ "Working Days  - OT will be based on working days applicable for salary month (All working days incl. unpaid leave");
		
		ibApplicableDays=new IntegerBox();
		ibApplicableDays.addValueChangeHandler(this);
		ibApplicableDays.setTitle("Calculate OT based on fixed working days");
		
		dbWrokingHours=new DoubleBox();
		dbWrokingHours.setTitle("Working hours per day i.e. 8 or 9 etc.");
		dbRate=new DoubleBox();
		dbRate.setTitle("Factor by which OT to be multiplied based on earning components ");
		
		cbOtForHourly=new CheckBox();
		cbOtForHourly.setValue(false);
		cbOtForHourly.addClickHandler(this);
		cbOtForHourly.setTitle("Per hour earning  (Earning components / applicable days / working hours) * OT Hours * Rate (Multiplication Factor)");
		
		cbOtForFlat=new CheckBox();
		cbOtForFlat.setValue(false);
		cbOtForFlat.addClickHandler(this);
		cbOtForFlat.setTitle("Per hour earning  (Earning components / applicable days / working hours) *  Rate (Multiplication Factor)");
		
		cbPayOTAsOther=new CheckBox();
		cbPayOTAsOther.setValue(false);
		cbPayOTAsOther.setTitle("To rename OT label on salary slip as entered in correspondence name. It will not print OT hours as well.");
		tbOtCorrespondanceName=new TextBox();
		
		disableEnableFormulaOtWidget();
	}
	
	
	@Override
	public void createScreen() {
		initalizeWidget();

		this.processlevelBarNames = new String[] { "New" };
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingOvertimeInformation = fbuilder.setlabel("Overtime Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Overtime ID", ibOvertimeId);
		FormField fOvertimeId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Overtime Name", tbOvertimeName);
		FormField ftbOvertimeName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Overtime Name is mandatory !")
				.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Short Name", olbOvertimeShortName);//tbOvertimeShortName
		FormField ftbOvertimeTypeShortName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Overtime Short Name is mandatory !")
				.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Hourly", cbHourly);
		FormField fcbHourly = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Hourly Rate", dblHourlyRate);
		FormField fdblHourlyRate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Flat", cbFlateRate);
		FormField fcbFlateRate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Flat Rate", dbFlateRate);
		FormField fdbFlateRate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
		
		
		fbuilder = new FormFieldBuilder("Is Leave", cbCountLeave);
		FormField fcbCountLeave = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Multiplication Factor", dbMultiplierHour);
		FormField fdbMultiplierHour = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Compensatory Leave", olbleaves);
		FormField folbleaves = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("% of CTC", dbPercentOf1DSal);
		fbuilder = new FormFieldBuilder("% of Earning Per day", dbPercentOf1DSal);//old label % of Rate/Day
		FormField fdbPercentOf1DSal = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Status", cbstatus);
		FormField fcbstatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgrpingHourlyot = fbuilder.setlabel("Hourly OT")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgrpingFlatRate = fbuilder.setlabel("Flat OT")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgrpingRateperday = fbuilder.setlabel("Rate/Day OT")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgrpingCompensatory = fbuilder.setlabel("Compensatory OT")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Applicable for Holiday Type", olbHolidayType);
		FormField folbHolidayType = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", rbAppForWeeklyOff);
		FormField frbAppForWeeklyOff = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", rbAppForNormalDays);
		FormField frbAppForNormalDays = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
		FormField fgroupingOtFormula = fbuilder.setlabel("OT Formula (Based on earning components) ")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Earning Component", lbEarningComponent);
		FormField foblEarningComp = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fbtnAdd = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", otEarningCompTbl.getTable());
		FormField fotEarningCompTbl = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Applicable Days", lbApplicableDays);
		FormField flbApplicableDays = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Applicable Days", ibApplicableDays);
		FormField fibApplicableDays = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Working Hours", dbWrokingHours);
		FormField fdbWrokingHours = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Hourly", cbOtForHourly);
		FormField fcbOtForHourly = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Flat", cbOtForFlat);
		FormField fcbOtForFlat = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Rate", dbRate);
		FormField fdbRate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Pay OT As Other", cbPayOTAsOther);
		FormField fcbPayOTAsOther = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Correspondence Name", tbOtCorrespondanceName);
		FormField ftbOtCorrespondanceName = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		FormField[][] formfield = {
				{ fgroupingOvertimeInformation },
				{ fOvertimeId, ftbOvertimeName, ftbOvertimeTypeShortName,fcbstatus },
				{ frbAppForWeeklyOff,frbAppForNormalDays},
				{ fgrpingHourlyot, fgrpingFlatRate },
				{ fcbHourly, fdblHourlyRate, fcbFlateRate, fdbFlateRate },
				{ fgrpingRateperday},
				{ fdbPercentOf1DSal},
				{ fgrpingCompensatory },
				{ fcbCountLeave, fdbMultiplierHour,folbleaves,folbHolidayType},
				
				{ fgroupingOtFormula},
				{ fcbOtForHourly,fcbOtForFlat,fcbPayOTAsOther,ftbOtCorrespondanceName},
				{ foblEarningComp,fbtnAdd},
				{ fotEarningCompTbl},
				{ flbApplicableDays,fibApplicableDays,fdbWrokingHours,fdbRate},
				
				
				
		};
		this.fields = formfield;
	}
	
	public void disableEnableFormulaOtWidget(){
		if(cbOtForFlat.getValue()==false&&cbOtForHourly.getValue()==false){
			lbEarningComponent.setSelectedIndex(0);
			lbEarningComponent.setEnabled(false);
			
			otEarningCompTbl.connectToLocal();
			btnAdd.setEnabled(false);
			
			lbApplicableDays.setSelectedIndex(0);
			lbApplicableDays.setEnabled(false);
			
			ibApplicableDays.setValue(null);
			ibApplicableDays.setEnabled(false);
			
			dbWrokingHours.setValue(null);
			dbWrokingHours.setEnabled(false);
			
			dbRate.setValue(null);
			dbRate.setEnabled(false);
			
			cbPayOTAsOther.setValue(false);
			tbOtCorrespondanceName.setValue("");
			tbOtCorrespondanceName.setEnabled(false);
			
			if(cbHourly.getValue()==true||cbFlateRate.getValue()==true){
				dbPercentOf1DSal.setValue(null);
				dbPercentOf1DSal.setEnabled(false);
			}else{
				dbPercentOf1DSal.setValue(null);
				dbPercentOf1DSal.setEnabled(true);
			}
			
		}else{
			lbEarningComponent.setEnabled(true);
			btnAdd.setEnabled(true);
			lbApplicableDays.setEnabled(true);
			ibApplicableDays.setEnabled(true);
			dbWrokingHours.setEnabled(true);
			dbRate.setEnabled(true);
			
//			cbPayOTAsOther.setValue(false);
			tbOtCorrespondanceName.setEnabled(true);
			
			if(lbApplicableDays.getSelectedIndex()!=0){
				ibApplicableDays.setValue(null);
				ibApplicableDays.setEnabled(false);
			}else{
				ibApplicableDays.setEnabled(true);
			}
			
			if(ibApplicableDays.getValue()!=null&&ibApplicableDays.getValue()!=0){
				lbApplicableDays.setSelectedIndex(0);
				lbApplicableDays.setEnabled(false);
			}else{
				lbApplicableDays.setEnabled(true);
			}
			
			
			cbFlateRate.setValue(false);
			dbFlateRate.setValue(null);
			dbFlateRate.setEnabled(false);
			
			cbHourly.setValue(false);
			dblHourlyRate.setValue(null);
			dblHourlyRate.setEnabled(false);
			
			dbPercentOf1DSal.setValue(null);
			dbPercentOf1DSal.setEnabled(false);
			
			
		}
	}
	
	
	@Override
	public void updateModel(Overtime model) {
		if (tbOvertimeName.getValue() != null)
			model.setName(tbOvertimeName.getValue());
//		if (tbOvertimeShortName.getValue() != null)
//			model.setShortName(tbOvertimeShortName.getValue());
		Console.log("olbOvertimeShortName selected item="+olbOvertimeShortName.getSelectedItem());
		Console.log("olbOvertimeShortName value="+olbOvertimeShortName.getValue());
		Console.log("olbOvertimeShortName getSelectedItemText="+olbOvertimeShortName.getSelectedItemText());
		Console.log("olbOvertimeShortName getSelectedIndex="+olbOvertimeShortName.getSelectedIndex());
		if (olbOvertimeShortName.getSelectedIndex() != 0)
		{	
			model.setShortName(olbOvertimeShortName.getSelectedItemText());
		}
		
		if (cbHourly.getValue() != null)
			model.setHourly(cbHourly.getValue());
//		if (dblHourlyRate.getValue() != null)
			model.setHourlyRate(dblHourlyRate.getValue());
		if (cbFlateRate.getValue() != null)
			model.setFlat(cbFlateRate.getValue());
//		if (dbFlateRate.getValue() != null)
			model.setFlatRate(dbFlateRate.getValue());
//		if (dbPercentOf1DSal.getValue() != null)
			model.setPercentOfCTC(dbPercentOf1DSal.getValue());
		if (cbCountLeave.getValue() != null)
			model.setLeave(cbCountLeave.getValue());
//		if (dbMultiplierHour.getValue() != null)
			model.setLeaveMultiplier(dbMultiplierHour.getValue());
//		if(olbleaves.getValue()!=null){
			model.setLeaveName(olbleaves.getValue());
//		}
		if (cbstatus.getValue() != null)
			model.setStatus(cbstatus.getValue());
		
		if(olbHolidayType.getValue()!=null){
			model.setHolidayType(olbHolidayType.getValue());
		}else{
			model.setHolidayType("");
		}
		if(rbAppForWeeklyOff.getValue()!=null){
			model.setWeeklyOff(rbAppForWeeklyOff.getValue());
		}
		if(rbAppForNormalDays.getValue()!=null){
			model.setNormalDays(rbAppForNormalDays.getValue());
		}
		
		/**
		 * @author Anil , Date : 08-06-2019
		 */
		model.setFormulaOtFlat(cbOtForFlat.getValue());
		model.setFormulaOtHourly(cbOtForHourly.getValue());
		
		if(otEarningCompTbl.getValue()!=null){
			model.setOtEarningCompList(otEarningCompTbl.getValue());
		}
		if(lbApplicableDays.getSelectedIndex()!=0){
			model.setApplicableDaysStr(lbApplicableDays.getValue(lbApplicableDays.getSelectedIndex()));
		}else{
			model.setApplicableDaysStr("");
		}
		
		if(ibApplicableDays.getValue()!=null){
			model.setApplicableDays(ibApplicableDays.getValue());
		}else{
			model.setApplicableDays(0);
		}
		
		if(dbWrokingHours.getValue()!=null){
			model.setWorkingHours(dbWrokingHours.getValue());
		}else{
			model.setWorkingHours(0);
		}
		if(dbRate.getValue()!=null){
			model.setRate(dbRate.getValue());
		}else{
			model.setRate(0);
		}
		model.setPayOtAsOther(cbPayOTAsOther.getValue());
		model.setOtCorrespondanceName(tbOtCorrespondanceName.getValue());
		
		manageBranchDropDown(model);
		presenter.setModel(model);
	}
	
	@Override
	public void updateView(Overtime view) {
		this.ibOvertimeId.setValue(view.getCount() + "");
		if (view.getName() != null)
			tbOvertimeName.setValue(view.getName());
//		if (view.getShortName() != null)
//			tbOvertimeShortName.setValue(view.getShortName());
		if (view.getShortName() != null)
			olbOvertimeShortName.setValue(view.getShortName());
		if (view.isHourly() != null)
			cbHourly.setValue(view.isHourly());
		if (view.getHourlyRate() != 0)
			dblHourlyRate.setValue(view.getHourlyRate());
		if (view.isFlat() != null)
			cbFlateRate.setValue(view.isFlat());
		if (view.getFlatRate() != 0)
			dbFlateRate.setValue(view.getFlatRate());
		if (view.getPercentOfCTC() != 0)
			dbPercentOf1DSal.setValue(view.getPercentOfCTC());
		if (view.isLeave() != null)
			cbCountLeave.setValue(view.isLeave());
		if (view.getLeaveMultiplier() != 0)
			dbMultiplierHour.setValue(view.getLeaveMultiplier());
		if(view.getLeaveName()!=null)
			olbleaves.setValue(view.getLeaveName());
		if (view.isStatus() != null)
			cbstatus.setValue(view.isStatus());
		if(view.getHolidayType()!=null){
			olbHolidayType.setValue(view.getHolidayType());
		}
		rbAppForWeeklyOff.setValue(view.isWeeklyOff());
		rbAppForNormalDays.setValue(view.isNormalDays());
		
		
		cbOtForFlat.setValue(view.isFormulaOtFlat());
		cbOtForHourly.setValue(view.isFormulaOtHourly());
		if(view.getOtEarningCompList()!=null){
			otEarningCompTbl.setValue(view.getOtEarningCompList());
		}
		if(view.getApplicableDaysStr()!=null){
			for(int i=0;i<lbApplicableDays.getItemCount();i++){
				if(view.getApplicableDaysStr().equals(lbApplicableDays.getValue(i))){
					lbApplicableDays.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getApplicableDays()!=0){
			ibApplicableDays.setValue(view.getApplicableDays());
		}
		
		if(view.getWorkingHours()!=0){
			dbWrokingHours.setValue(view.getWorkingHours());
		}
		if(view.getRate()!=0){
			dbRate.setValue(view.getRate());
		}
		cbPayOTAsOther.setValue(view.isPayOtAsOther());
		if(view.getOtCorrespondanceName()!=null){
			tbOtCorrespondanceName.setValue(view.getOtCorrespondanceName());
		}

		presenter.setModel(view);
	}
	
	
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")||text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else{
					menus[k].setVisible(false);
				}

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")||text.contains(AppConstants.NAVIGATION)){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")||text.contains(AppConstants.NAVIGATION)){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.OVERTIME,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void setCount(int count) {
		this.ibOvertimeId.setValue(presenter.getModel().getCount() + "");
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		
		System.out.println("INSIDE SET TO EDIT METHOD ");
		if(dbFlateRate.getValue()!=null){
			dbFlateRate.setEnabled(true);
		}else{
			dbFlateRate.setEnabled(false);
		}
		
		if(dblHourlyRate.getValue()!=null){
			dblHourlyRate.setEnabled(true);
		}else{
			dblHourlyRate.setEnabled(false);
		}
		
		if(dbMultiplierHour.getValue()!=null){
			dbMultiplierHour.setEnabled(true);
			olbleaves.setEnabled(true);
		}else{
			dbMultiplierHour.setEnabled(false);
			olbleaves.setEnabled(false);
		}
		
		if(dbPercentOf1DSal.getValue()!=null){
			dbPercentOf1DSal.setEnabled(true);
		}else{
			dbPercentOf1DSal.setEnabled(false);
		}
		
		disableEnableFormulaOtWidget();
		
		this.processLevelBar.setVisibleFalse(false);
	}


	@Override
	public boolean validate() {
		boolean superRes = super.validate();

		if (superRes == false){
			return false;
		}
//		int counter=0;
		if (dblHourlyRate.getValue()!=null) {
			if (dblHourlyRate.getValue()==0) {
				this.showDialogMessage("Hourly Rate can not be zero.");
				return false;
			}
		}
		if (dbFlateRate.getValue()!=null) {
			if (dbFlateRate.getValue()==0) {
				this.showDialogMessage("Flate Rate can not be zero.");
				return false;
			}
		}
		
		if (dbMultiplierHour.getValue()!=null ) {
			if (dbMultiplierHour.getValue()==0) {
				this.showDialogMessage("Multiplier Rate can not be zero.");
				return false;
			}
		}
		
		if (dbPercentOf1DSal.getValue()!=null ) {
			if (dbPercentOf1DSal.getValue()==0) {
				this.showDialogMessage("Percent Rate can not be zero.");
				return false;
			}
		}
		if(cbCountLeave.getValue()==true && olbleaves.getValue()==null){
			this.showDialogMessage("Please select Leave Type!");
			return false;
		}
		
		if(dblHourlyRate.getValue()==null&&dbFlateRate.getValue()==null&&dbMultiplierHour.getValue()==null&&dbPercentOf1DSal.getValue()==null&&cbOtForFlat.getValue()==false&&cbOtForHourly.getValue()==false){
			this.showDialogMessage("Please fill overtime details!");
			return false;
		}
		
		if(olbHolidayType.getValue()!=null
				&&(rbAppForWeeklyOff.getValue()==true||rbAppForNormalDays.getValue()==true)){
			showDialogMessage("Please select any one among holiday type,weekly off and normal days.");
			return false;
		}
		
		/**
		 * @author Anil , Date : 08-06-2019
		 */
		if(cbOtForFlat.getValue()==true||cbOtForHourly.getValue()==true){
			String msg="";
			boolean otValFlag=false;
			if(otEarningCompTbl.getValue()==null||otEarningCompTbl.getValue().size()==0){
//				showDialogMessage("Please add any earning component for formula OT.");
//				return false;
				msg=msg+"Please add any earning component for formula OT."+"\n";
				otValFlag=true;
			}
			if(lbApplicableDays.getSelectedIndex()==0&&ibApplicableDays.getValue()==null&&ibApplicableDays.getValue()==0){
//				showDialogMessage("Please select applicable days.");
//				return false;
				msg=msg+"Please select applicable days."+"\n";
				otValFlag=true;
			}
			
			if(dbWrokingHours.getValue()==null||dbWrokingHours.getValue()==0){
//				showDialogMessage("Please select working hours.");
//				return false;
				msg=msg+"Please select working hours."+"\n";
				otValFlag=true;
			}
			
			if(dbRate.getValue()==null||dbRate.getValue()==0){
//				showDialogMessage("Please select rate.");
//				return false;
				msg=msg+"Please select rate.";
				otValFlag=true;
			}
			
			if(otValFlag){
				showDialogMessage(msg);
				return false;
			}
			
			if(olbHolidayType.getValue()==null&&rbAppForWeeklyOff.getValue()==false&&rbAppForNormalDays.getValue()==false){
				showDialogMessage("Please select atleast either holiday type,weekly off and normal days.");
				return false;
			}
		}
		
		
		
		return true;
	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource().equals(cbHourly)){
			if (cbHourly.getValue() == true) {
				dblHourlyRate.setEnabled(true);
				
				cbFlateRate.setValue(false);
				dbFlateRate.setValue(null);
				dbFlateRate.setEnabled(false);
				
				cbCountLeave.setValue(false);
				dbMultiplierHour.setValue(null);
				dbMultiplierHour.setEnabled(false);
				
				dbPercentOf1DSal.setValue(null);
				dbPercentOf1DSal.setEnabled(false);
				
				olbleaves.setSelectedIndex(0);
				olbleaves.setEnabled(false);
				
				/**
				 * @author Anil , Date : 08-06-2019
				 */
				cbOtForFlat.setValue(false);
				cbOtForHourly.setValue(false);
				disableEnableFormulaOtWidget();
			}
			
			if (cbHourly.getValue() == false) {
				dblHourlyRate.setValue(null);
				dblHourlyRate.setEnabled(false);
				
				cbFlateRate.setValue(false);
				dbFlateRate.setValue(null);
				dbFlateRate.setEnabled(false);
				
				cbCountLeave.setValue(false);
				dbMultiplierHour.setValue(null);
				dbMultiplierHour.setEnabled(false);
				
				dbPercentOf1DSal.setValue(null);
				dbPercentOf1DSal.setEnabled(true);
				
				olbleaves.setSelectedIndex(0);
				olbleaves.setEnabled(false);
			}
		}
		
		if(event.getSource().equals(cbFlateRate)){
			if (cbFlateRate.getValue() == true) {
				dbFlateRate.setEnabled(true);
				
				cbHourly.setValue(false);
				dblHourlyRate.setValue(null);
				dblHourlyRate.setEnabled(false);
				
				cbCountLeave.setValue(false);
				dbMultiplierHour.setValue(null);
				dbMultiplierHour.setEnabled(false);
				
				dbPercentOf1DSal.setValue(null);
				dbPercentOf1DSal.setEnabled(false);
				
				olbleaves.setSelectedIndex(0);
				olbleaves.setEnabled(false);
				
				/**
				 * @author Anil , Date : 08-06-2019
				 */
				cbOtForFlat.setValue(false);
				cbOtForHourly.setValue(false);
				disableEnableFormulaOtWidget();
			}
			
			if (cbFlateRate.getValue() == false) {
				dbFlateRate.setValue(null);
				dbFlateRate.setEnabled(false);
				
				cbHourly.setValue(false);
				dblHourlyRate.setValue(null);
				dblHourlyRate.setEnabled(false);
				
				cbCountLeave.setValue(false);
				dbMultiplierHour.setValue(null);
				dbMultiplierHour.setEnabled(false);
				
				dbPercentOf1DSal.setValue(null);
				dbPercentOf1DSal.setEnabled(true);
				
				olbleaves.setSelectedIndex(0);
				olbleaves.setEnabled(false);
			}
		}
		
		
		if(event.getSource().equals(cbCountLeave)){
			if (cbCountLeave.getValue() == true) {
				dbMultiplierHour.setEnabled(true);
				olbleaves.setEnabled(true);
				
				cbHourly.setValue(false);
				dblHourlyRate.setValue(null);
				dblHourlyRate.setEnabled(false);
				
				cbFlateRate.setValue(false);
				dbFlateRate.setValue(null);
				dbFlateRate.setEnabled(false);
				
//				dbPercentOf1DSal.setValue(null);
				dbPercentOf1DSal.setEnabled(true);
			}
			
			if (cbCountLeave.getValue() == false) {
				dbMultiplierHour.setValue(null);
				dbMultiplierHour.setEnabled(false);
				
				olbleaves.setSelectedIndex(0);
				olbleaves.setEnabled(false);
				
				cbHourly.setValue(false);
				dblHourlyRate.setValue(null);
				dblHourlyRate.setEnabled(false);
				
				cbFlateRate.setValue(false);
				dbFlateRate.setValue(null);
				dbFlateRate.setEnabled(false);
				
//				dbPercentOf1DSal.setValue(null);
				dbPercentOf1DSal.setEnabled(true);
			}
		}
		if(event.getSource().equals(rbAppForNormalDays)){
			if(rbAppForNormalDays.getValue()==true){
				rbAppForWeeklyOff.setValue(false);
			}
		}
		if(event.getSource().equals(rbAppForWeeklyOff)){
			if(rbAppForWeeklyOff.getValue()==true){
				rbAppForNormalDays.setValue(false);
			}
		}
		/**
		 * @author Anil , Date : 07-06-2019
		 */
		if(event.getSource().equals(cbOtForHourly)){
			if(cbOtForHourly.getValue()==true){
				cbOtForFlat.setValue(false);
			}
			disableEnableFormulaOtWidget();
		}
		
		if(event.getSource().equals(cbOtForFlat)){
			if(cbOtForFlat.getValue()==true){
				cbOtForHourly.setValue(false);
			}
			disableEnableFormulaOtWidget();
		}
		
		if(event.getSource().equals(btnAdd)){
//			if(cbOtForFlat.getValue()==true){
//				cbOtForHourly.setValue(false);
//			}
			if(lbEarningComponent.getSelectedIndex()!=0){
				if(otEarningCompTbl.getValue()!=null&&otEarningCompTbl.getValue().size()!=0){
					for(OtEarningComponent obj:otEarningCompTbl.getValue()){
						if(obj.getComponentName().equals(lbEarningComponent.getValue(lbEarningComponent.getSelectedIndex()))){
							showDialogMessage("Component already added.");
							return;
						}
					}
					OtEarningComponent ot=new OtEarningComponent();
					ot.setComponentName(lbEarningComponent.getValue(lbEarningComponent.getSelectedIndex()));
					otEarningCompTbl.getValue().add(ot);
				}else{
					OtEarningComponent ot=new OtEarningComponent();
					ot.setComponentName(lbEarningComponent.getValue(lbEarningComponent.getSelectedIndex()));
					otEarningCompTbl.getValue().add(ot);
				}
			}else{
				
				/**
				 * @author Anil @since 02-03-2021
				 * on click of add button adding all components
				 */
				if(otEarningCompTbl.getValue()!=null&&otEarningCompTbl.getValue().size()>0){
					for(OtEarningComponent obj:otEarningCompTbl.getValue()){
						if(obj.getComponentName().equals("Gross Earning")){
							showDialogMessage("Please select earning component.");
							return;
						}
					}
				}
				
				List<OtEarningComponent> otList=new ArrayList<OtEarningComponent>();
				for(int i=1;i<lbEarningComponent.getItemCount();i++){
					String componentName=lbEarningComponent.getItemText(i);
					if(componentName.equals("Gross Earning")){
						continue;
					}
					boolean validationCheckFlag=false;
					if(otEarningCompTbl.getValue()!=null&&otEarningCompTbl.getValue().size()!=0){
						for(OtEarningComponent obj:otEarningCompTbl.getValue()){
							if(obj.getComponentName().equals(componentName)){
								validationCheckFlag=true;
								break;
							}
						}
						
						if(validationCheckFlag){
							continue;
						}
						OtEarningComponent ot=new OtEarningComponent();
						ot.setComponentName(componentName);
						otList.add(ot);
					}else{
						OtEarningComponent ot=new OtEarningComponent();
						ot.setComponentName(componentName);
						otList.add(ot);
					}
				}
				
				if(otList.size()!=0){
					otEarningCompTbl.getValue().addAll(otList);
				}
				
				
				
			}
			
		}
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		
		System.out.println("INSIDE SET ENABLE METHOD   ");
		ibOvertimeId.setEnabled(false);
		dbFlateRate.setEnabled(false);
		dblHourlyRate.setEnabled(false);
		dbMultiplierHour.setEnabled(false);
		olbleaves.setEnabled(false);
		
		if(cbOtForFlat.getValue()==true||cbOtForHourly.getValue()==true){
			
		}else{
			cbOtForFlat.setValue(false);
			cbOtForHourly.setValue(false);
			disableEnableFormulaOtWidget();
		}
		otEarningCompTbl.setEnable(state);

	}

	@Override
	public void clear() {
		super.clear();
		cbHourly.setValue(false);
		dblHourlyRate.setValue(null);
		dblHourlyRate.setValue(null);
		cbFlateRate.setValue(false);
		cbCountLeave.setValue(false);
		cbstatus.setValue(true);
		
		rbAppForNormalDays.setValue(false);
		rbAppForWeeklyOff.setValue(false);
		
		cbOtForFlat.setValue(false);
		cbOtForHourly.setValue(false);
		
		disableEnableFormulaOtWidget();

	}
	
	public void initializePaidLeaveType(){
		MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("leaveStatus");
		filter.setBooleanvalue(true);
		temp.add(filter);
		
//		filter=new Filter();
//		filter.setQuerryString("paidLeave");
//		filter.setBooleanvalue(true);
//		temp.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("isCompensatoryLeave");
		filter.setBooleanvalue(true);
		temp.add(filter);
		
		
		
		querry.setFilters(temp);
		querry.setQuerryObject(new LeaveType());
		
		olbleaves.MakeLive(querry);
		  
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==olbHolidayType){
			if(olbHolidayType.getSelectedIndex()!=0){
				rbAppForNormalDays.setValue(false);
				rbAppForWeeklyOff.setValue(false);
			}
		}
		/**
		 * @author Anil , Date : 07-06-2019
		 */
		
		if(event.getSource()==lbApplicableDays){
			if(lbApplicableDays.getSelectedIndex()!=0){
				ibApplicableDays.setValue(null);
				ibApplicableDays.setEnabled(false);
			}
			disableEnableFormulaOtWidget();
		}
		
	}
	/**Date 19-4-2019 added by Amol to solve the logout issue**/
	public void manageBranchDropDown(Overtime overtimeModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalOvertimeList.add(overtimeModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalOvertimeList.size();i++)
			{
				if(LoginPresenter.globalOvertimeList.get(i).getId().equals(overtimeModel.getId()))
				{
					LoginPresenter.globalOvertimeList.add(overtimeModel);
					LoginPresenter.globalOvertimeList.remove(i);
				}
			}
		}
	}

	@Override
	public void onValueChange(ValueChangeEvent<Integer> event) {
		// TODO Auto-generated method stub
		if(event.getSource()==ibApplicableDays){
			if(ibApplicableDays.getValue()!=null&&ibApplicableDays.getValue()!=0){
				lbApplicableDays.setSelectedIndex(0);
				lbApplicableDays.setEnabled(false);
			}
			disableEnableFormulaOtWidget();
		}
	}

}
