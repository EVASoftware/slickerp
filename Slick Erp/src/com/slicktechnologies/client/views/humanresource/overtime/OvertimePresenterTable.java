package com.slicktechnologies.client.views.humanresource.overtime;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;

public class OvertimePresenterTable extends SuperTable<Overtime> {

	TextColumn<Overtime> getShortNameColumn;
	TextColumn<Overtime> getNameColumn;
	TextColumn<Overtime> getCountColumn;
	TextColumn<Overtime> getStatusColumn;
	  
	public OvertimePresenterTable() {
		super();
	}
	  
	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetName();
		addColumngetShortName();
		addColumngetStatus();
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<Overtime>() {
			@Override
			public Object getKey(Overtime item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	@Override
	public void addFieldUpdater() {
	}
	
	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetName();
		addSortinggetShortName();
		addSortingStatus();
	}

	

	protected void addSortinggetCount() {
		List<Overtime> list = getDataprovider().getList();
		columnSort = new ListHandler<Overtime>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Overtime>() {
			@Override
			public int compare(Overtime e1, Overtime e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<Overtime>() {
			@Override
			public String getValue(Overtime object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		getCountColumn.setSortable(true);
	}

	protected void addSortinggetName() {
		List<Overtime> list = getDataprovider().getList();
		columnSort = new ListHandler<Overtime>(list);
		columnSort.setComparator(getNameColumn, new Comparator<Overtime>() {
			@Override
			public int compare(Overtime e1, Overtime e2) {
				if (e1 != null && e2 != null) {
					if (e1.getName() != null && e2.getName() != null) {
						return e1.getName().compareTo(e2.getName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetShortName() {
		List<Overtime> list = getDataprovider().getList();
		columnSort = new ListHandler<Overtime>(list);
		columnSort.setComparator(getShortNameColumn,
				new Comparator<Overtime>() {
					@Override
					public int compare(Overtime e1, Overtime e2) {
						if (e1 != null && e2 != null) {
							if (e1.getShortName() != null
									&& e2.getShortName() != null) {
								return e1.getShortName().compareTo(
										e2.getShortName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingStatus() {
		List<Overtime> list = getDataprovider().getList();
		columnSort = new ListHandler<Overtime>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Overtime>() {
			@Override
			public int compare(Overtime e1, Overtime e2) {
				if (e1 != null && e2 != null) {
					if (e1.isStatus() == true)
						return 1;
					else
						return -1;

				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetName() {
		getNameColumn = new TextColumn<Overtime>() {
			@Override
			public String getValue(Overtime object) {
				return object.getName() + "";
			}
		};
		table.addColumn(getNameColumn, "Name");
		getNameColumn.setSortable(true);
	}

	

	protected void addColumngetShortName() {
		getShortNameColumn = new TextColumn<Overtime>() {
			@Override
			public String getValue(Overtime object) {
				return object.getShortName() + "";
			}
		};
		table.addColumn(getShortNameColumn, "Short Name");
		getShortNameColumn.setSortable(true);
	}
	

	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<Overtime>() {
			@Override
			public String getValue(Overtime object) {
				if (object.isStatus() == true)
					return "Active";
				else
					return "In Active";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
	}

}
