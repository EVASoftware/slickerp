package com.slicktechnologies.client.views.humanresource.overtime;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.client.utility.Screen;
public class OvertimePresenter extends FormTableScreenPresenter<Overtime> {

	OvertimeForm form;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	public OvertimePresenter(FormTableScreen<Overtime> view, Overtime model) {
		super(view, model);
		form = (OvertimeForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getOvertimeQuery());
		/**
		 * @author Anil , Date : 08-06-2019
		 */
		getEarningComponent() ;
		form.setPresenter(this);
	}
	
	private void getEarningComponent() {
		MyQuerry querry=new MyQuerry();
		Filter filter=null;
		Vector<Filter> filtervec=new Vector<Filter>();
		
		Company c=new Company();
		 
		filter=new Filter();
		filter.setBooleanvalue(false);
		filter.setQuerryString("isDeduction");
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		  
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CtcComponent());
		 
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("Leave result size : "+result.size());
				if (result.size() != 0) {
					form.lbEarningComponent.clear();
					form.lbEarningComponent.addItem("--SELECT--");
					form.lbEarningComponent.addItem("Gross Earning");
					
					//29-05-2023 It was found that to calculate net payable we need to calculate deductions. To calculate deductions we need gross + OT. Due to this cross logic esic was not getting calculated so rolling back the changes
//					form.lbEarningComponent.addItem("Net Payable");//Ashwini Patil Date:14-04-2023 For Spick and span
					for(SuperModel model:result){
						CtcComponent entity=(CtcComponent) model;
						form.lbEarningComponent.addItem(entity.getName());
					}
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Failure!!!!!!!!!!1");
			}
		});
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel lbl = (InlineLabel) e.getSource();
		if (lbl.getText().contains("New")) {
			form.setToNewState();
			this.initalize();
		}
	}
	
	public static void initalize() {
		OvertimePresenterTable gentableScreen = new OvertimePresenterTable();
		OvertimeForm form = new OvertimeForm(gentableScreen,FormTableScreen.UPPER_MODE, true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();


		OvertimePresenter presenter = new OvertimePresenter(form,new Overtime());
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
		
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("HR Configuration/Overtime",Screen.OVERTIME);
          
	}

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}

	@Override
	protected void makeNewModel() {
		model = new Overtime();
	}
	
	public MyQuerry getOvertimeQuery() {
		MyQuerry quer = new MyQuerry(new Vector<Filter>(), new Overtime());
		return quer;
	}
	
	
	

}
