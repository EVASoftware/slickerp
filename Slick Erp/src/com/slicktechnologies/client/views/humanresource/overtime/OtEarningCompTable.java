package com.slicktechnologies.client.views.humanresource.overtime;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.OtEarningComponent;

public class OtEarningCompTable extends SuperTable<OtEarningComponent> {

	TextColumn<OtEarningComponent> getColComponentName;
	Column<OtEarningComponent,String> getColDelete;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		getColComponentName();
		getColDelete();
	}

	private void getColDelete() {
		// TODO Auto-generated method stub
		ButtonCell cell=new ButtonCell();
		getColDelete=new Column<OtEarningComponent, String>(cell) {
			
			@Override
			public String getValue(OtEarningComponent object) {
				// TODO Auto-generated method stub
				return "Delete";
			}
		};
		table.addColumn(getColDelete, "");
		
		getColDelete.setFieldUpdater(new FieldUpdater<OtEarningComponent, String>() {
			
			@Override
			public void update(int index, OtEarningComponent object, String value) {
				// TODO Auto-generated method stub
				getDataprovider().getList().remove(index);
				table.redraw();
			}
		});
	}

	private void getColComponentName() {
		// TODO Auto-generated method stub
		getColComponentName=new TextColumn<OtEarningComponent>() {
			
			@Override
			public String getValue(OtEarningComponent object) {
				// TODO Auto-generated method stub
				if(object.getComponentName()!=null){
					return object.getComponentName();
				}
				return "";
			}
		};
		table.addColumn(getColComponentName, "Name");
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);
		
		if (state == true) {
			getColComponentName();
			getColDelete();
		}else{
			getColComponentName();
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
