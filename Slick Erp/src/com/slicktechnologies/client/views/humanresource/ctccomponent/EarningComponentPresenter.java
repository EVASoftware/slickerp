package com.slicktechnologies.client.views.humanresource.ctccomponent;

import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;


import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;

public class EarningComponentPresenter extends FormTableScreenPresenter<CtcComponent>
{

	EarningComponentForm form;

	public EarningComponentPresenter (FormTableScreen<CtcComponent> view,
			CtcComponent model) {
		super(view, model);
		form=(EarningComponentForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getSalaryConfigQuery());
		form.setPresenter(this);
	}


	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
	{
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("Delete"))
		{
			
		
			{
			List<CtcComponent>data=form.getSupertable().getDataprovider().getList();
			/*
			 * Deleting
			 */
			for(int i=0;i<data.size();i++)
			{
				if(data.get(i).getId().equals(model.getId()))
				{
					data.remove(i);
				}
				form.getSupertable().getDataprovider().refresh();
			}
			reactOnDelete();
			
			}
		}
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}

	}





	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void makeNewModel() {
		model=new CtcComponent();

	}

	public MyQuerry getSalaryConfigQuery()
	{
		MyQuerry quer=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("isDeduction");
		filter.setBooleanvalue(false);
		quer.setQuerryObject(new CtcComponent());
		quer.getFilters().add(filter);
		return quer;
	}

	public void setModel(CtcComponent entity)
	{
		model=entity;
	}


	public static void initalize()
	{
		SalaryHeadPresenterTable gentableScreen=new EarningComponentTable();

		EarningComponentForm  form=new  EarningComponentForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();

		/// CountryPresenterTable gentableSearch=GWT.create( CountryPresenterTable.class);
		////   CountryPresenterSearch.staticSuperTable=gentableSearch;
		////     CountryPresenterSearch searchpopup=GWT.create( CountryPresenterSearch.class);
		////         form.setSearchpopupscreen(searchpopup);

		EarningComponentPresenter  presenter=new  EarningComponentPresenter  (form,new CtcComponent());
		AppMemory.getAppMemory().stickPnel(form);



	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.SalaryHead")
	public static class  SalaryHeadPresenterTable extends SuperTable<CtcComponent> implements GeneratedVariableRefrence{

		public SalaryHeadPresenterTable() {
			// TODO Auto-generated constructor stub
			super();
		}
		
		public SalaryHeadPresenterTable(boolean adhocPaymentFlag) {
			// TODO Auto-generated constructor stub
			super(adhocPaymentFlag);
		}

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub

		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub

		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub

		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub

		}

	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.SalaryHead")
	public static class  SalaryHeadPresenterSearch extends SearchPopUpScreen<  CtcComponent>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}

	};

	private void reactTo()
	{
	}
}
