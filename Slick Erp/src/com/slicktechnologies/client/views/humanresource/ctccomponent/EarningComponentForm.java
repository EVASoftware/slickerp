package com.slicktechnologies.client.views.humanresource.ctccomponent;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class EarningComponentForm extends FormTableScreen<CtcComponent> {

	IntegerBox ibSalaryConfigId;
	DoubleBox maxperCTC;
	IntegerBox maxAmount;
	TextBox tbSalaryconfigName, tbSalaryconfigShortName;
	CheckBox status;
	/**
	 * Rahul Verma Added this on 26 June 2016 Frequency of that compoment
	 */
	ListBox lBFrequency;

	/**
	 * nidhi 11-09-2017 for report sorting
	 */
	IntegerBox tbSrNo;

	/**
	 * end
	 */

	/**
	 * Rahul added on 14 July 2018 Description: By Default false. Componenets
	 * which we want to exclude from payroll will be added here
	 */
	CheckBox excludeinPaySlip;

	/**
	 * Ends
	 */

	public EarningComponentForm(SuperTable<CtcComponent> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
	}

	private void initalizeWidget() {
		ibSalaryConfigId = new IntegerBox();
		ibSalaryConfigId.setEnabled(false);
		tbSalaryconfigName = new TextBox();
		tbSalaryconfigShortName = new TextBox();

		status = new CheckBox();
		status.setValue(true);
		maxperCTC = new DoubleBox();
		maxAmount = new IntegerBox();

		/**
		 * nidhi 11-09-2017
		 * 
		 */

		tbSrNo = new IntegerBox();
		/**
		 * end
		 */
		/**
		 * Rahul Added this on 26 June 2018
		 */
		/**
		 * Rahul Added this on 14 July 2018
		 */
		excludeinPaySlip = new CheckBox();
		excludeinPaySlip.setValue(false);

	}

	@Override
	public void createScreen() {

		// Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames = new String[] { "New" };

		// Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSalaryConfigInformation = fbuilder
				.setlabel("Salary Config Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(6).build();
		fbuilder = new FormFieldBuilder("SalaryConfig ID", ibSalaryConfigId);
		FormField fibSalaryConfigId = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Name", tbSalaryconfigName);
		FormField ftbSalaryconfigName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Name is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Short Name", tbSalaryconfigShortName);
		FormField ftbSalaryconfigShortName = fbuilder.setMandatory(true)
				.setMandatoryMsg(" Short Name is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder(" Maximum % of CTC", maxperCTC);
		FormField famount = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).setMandatoryMsg("% of CTC is mandatory").build();

		fbuilder = new FormFieldBuilder("Maximum Amount", maxAmount);
		FormField fmaxamt = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).setMandatoryMsg("Maximum amount is Mandatory !")
				.build();

		fbuilder = new FormFieldBuilder("Status", status);
		FormField fstatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		/**
		 * nidhi 11-09-2017 for sr no field
		 */

		fbuilder = new FormFieldBuilder("* Sr. No.", tbSrNo);
		FormField ftbSrNo = fbuilder.setMandatory(true)
				.setMandatoryMsg("Serial No. is mndatory").setRowSpan(0)
				.setColSpan(0).build();
		/**
		 * end
		 */

		fbuilder = new FormFieldBuilder("Exclude in Payslip", excludeinPaySlip);
		FormField fexcludeinPaySlip = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		FormField[][] formfield = {
				{ fgroupingSalaryConfigInformation },
				{ ftbSrNo, ftbSalaryconfigName, ftbSalaryconfigShortName,
						famount, fmaxamt, fstatus }, { fexcludeinPaySlip }

		};

		this.fields = formfield;

	}

	@Override
	public void updateModel(CtcComponent model) {
		if (tbSalaryconfigName.getValue() != null)
			model.setName(tbSalaryconfigName.getValue());
		if (tbSalaryconfigShortName.getValue() != null)
			model.setShortName(tbSalaryconfigShortName.getValue());

		if (status.getValue() != null)
			model.setStatus(status.getValue());

		// if(maxperCTC.getValue()!=null)
		model.setMaxPerOfCTC(maxperCTC.getValue());

		// if(maxAmount.getValue()!=null)
		model.setMaxAmount(maxAmount.getValue());

		/**
		 * nidhi 11-09-2017 for save sr no.
		 */
		model.setSrNo(tbSrNo.getValue());

		/**
		 * end
		 */
		if (excludeinPaySlip.getValue() != null)
			model.setExcludeInPayRoll(excludeinPaySlip.getValue());

		presenter.setModel(model);

	}

	@Override
	public void updateView(CtcComponent view) {
		if (view.getName() != null)
			tbSalaryconfigName.setValue(view.getName());
		if (view.getShortName() != null)
			tbSalaryconfigShortName.setValue(view.getShortName());

		if (view.getStatus() != null)
			status.setValue(view.getStatus());
		if (view.getMaxPerOfCTC() != null)
			maxperCTC.setValue(view.getMaxPerOfCTC());
		if (view.getMaxAmount() != null)
			maxAmount.setValue(view.getMaxAmount());

		/**
		 * nidhi 11-09-2017 for display sr no.
		 */
		tbSrNo.setValue(view.getSrNo());

		/**
		 * end
		 */
		try{
		excludeinPaySlip.setValue(view.isExcludeInPayRoll());
		}catch(Exception e){
			e.printStackTrace();
		}

		presenter.setModel(view);
	}

	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SALARYHEAD,
				LoginPresenter.currentModule.trim());
	}

	@Override
	public void setCount(int count) {
		this.ibSalaryConfigId.setValue(presenter.getModel().getCount());
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibSalaryConfigId.setEnabled(false);
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		boolean val = super.validate();
		if (val == false)
			return val;
		if (this.maxAmount.getValue() == null
				&& this.maxperCTC.getValue() == null) {
			showDialogMessage("Both Maximum Percentage and Maximum Absolute amount Cannot be empty !");
			return false;
		}

		if (this.maxAmount.getValue() != null
				&& this.maxperCTC.getValue() != null) {
			showDialogMessage("Fill either Maximum Percentage or Maximum Absolute amount not both.");
			return false;

		}

		/**
		 * nidhi 11-09-2017 check sr no validation
		 */
		if (!this.checkSrNo()) {
			showDialogMessage("Serial Number should not be duplicate");
			return false;
		}
		/**
		 * end
		 */

		return true;
	}

	/**
	 * nidh 11-09-2017 for check srno duplication
	 * 
	 * @return
	 */

	public boolean checkSrNo() {
		boolean flag = true;
		try {
			List<CtcComponent> ctcList = this.getSupertable().getDataprovider()
					.getList();

			for (CtcComponent ctc : ctcList) {
				if (ctc.getSrNo() == tbSrNo.getValue()) {
					System.out.println(" get state   -- "
							+ AppMemory.getAppMemory().currentState + "  \n  "
							+ ScreeenState.EDIT + "  \n  name -- "
							+ ctc.getCtcComponentName() + " \n name text -- "
							+ tbSalaryconfigName.getValue());
					if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT
							&& ctc.getName().equalsIgnoreCase(
									tbSalaryconfigName.getValue())) {
						return true;
					} else {
						return false;
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return flag;
	}

	/**
	 * end
	 */

	@Override
	public void clear() {
		super.clear();
		status.setValue(true);
		excludeinPaySlip.setValue(false);
	}

}
