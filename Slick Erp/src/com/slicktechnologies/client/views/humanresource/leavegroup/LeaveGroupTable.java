package com.slicktechnologies.client.views.humanresource.leavegroup;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;

public class LeaveGroupTable extends SuperTable<AllocatedLeaves> {

	TextColumn<AllocatedLeaves> leaveTypeColumn;
	TextColumn<AllocatedLeaves> maximumDaysColumn;
//	TextColumn<AllocatedLeaves> allocatedDaysColumn;
	
	private Column<AllocatedLeaves, String> delete;
	
	
	public LeaveGroupTable() 
	{
		super();
	}
	
	
	@Override
	public void createTable() {
		addColumnLeaveType();
		addColumnMaximumDays();
		//addColumnAllocatedDays();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}

	private void addeditColumn()
	{
		addColumnLeaveType();
		addColumnMaximumDays();
		//addColumnAllocatedDays();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	}
	
	private void addViewColumn()
	{
		addColumnLeaveType();
		addColumnMaximumDays();
		//addColumnAllocatedDays();
	}
	
	
	public void addColumnLeaveType()
	{
		leaveTypeColumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				return object.getName();
			}
		};
		table.addColumn(leaveTypeColumn,"Leave Type");
	}
	
	public void addColumnMaximumDays()
	{
		maximumDaysColumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				
				return object.getMaxDays()+"";
			}
		};
		table.addColumn(maximumDaysColumn,"Maximum Days");
	}
	
//	public void addColumnAllocatedDays()
//	{
//		allocatedDaysColumn = new TextColumn<AllocatedLeaves>() {
//			@Override
//			public String getValue(AllocatedLeaves object) {
//				return object.getDaysAllocated()+"";
//			}
//		};
//		table.addColumn(allocatedDaysColumn,"Allocated Days");
//	}

	  private void addColumnDelete()
		{
			ButtonCell btnCell= new ButtonCell();
			delete = new Column<AllocatedLeaves, String>(btnCell) {

				@Override
				public String getValue(AllocatedLeaves object) {
					
					return "Delete";
				}
			};
			table.addColumn(delete,"Delete");
		}

		private void setFieldUpdaterOnDelete()
		{
			delete.setFieldUpdater(new FieldUpdater<AllocatedLeaves, String>() {
				
				@Override
				public void update(int index, AllocatedLeaves object, String value) {
					getDataprovider().getList().remove(object);
				
				}
			});
		}

	
	
		public  void setEnabled(boolean state)
		{
			int tablecolcount=this.table.getColumnCount();
			for(int i=tablecolcount-1;i>-1;i--)
			  table.removeColumn(i);
			if(state ==true)
				addeditColumn();
			if(state==false)
				addViewColumn();
		}

	

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<AllocatedLeaves>() {
			@Override
			public Object getKey(AllocatedLeaves item) {
				if(item==null)
					return null;
				else
					return item.getId();
			}
		};
	
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
