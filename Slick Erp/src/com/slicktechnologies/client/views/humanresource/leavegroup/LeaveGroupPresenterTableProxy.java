package com.slicktechnologies.client.views.humanresource.leavegroup;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;
import java.util.List;

import com.slicktechnologies.client.views.humanresource.leavegroup.LeaveGroupPresenter.LeaveGroupPresenterTable;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;

public class LeaveGroupPresenterTableProxy extends LeaveGroupPresenterTable {
  TextColumn<LeaveGroup> getGroupNameColumn;
  TextColumn<LeaveGroup> getStatusColumn;
  TextColumn<LeaveGroup> getCountColumn;
  public Object getVarRef(String varName)
  {
  if(varName.equals("getGroupNameColumn"))
  return this.getGroupNameColumn;
  if(varName.equals("getStatusColumn"))
  return this.getStatusColumn;
  if(varName.equals("getCountColumn"))
  return this.getCountColumn;
   return null ;
  }
  public LeaveGroupPresenterTableProxy()
  {
  super();
  }
  @Override public void createTable() {
  addColumngetCount();
  addColumngetGroupName();
  addColumngetStatus();
  }
  @Override
  protected void initializekeyprovider() {
  keyProvider= new ProvidesKey<LeaveGroup>()
  {
  @Override
  public Object getKey(LeaveGroup item)
  {
  if(item==null)
  {
  return null;
  }
  else
  return item.getId();
  }
  };
  }
  @Override
  public void setEnable(boolean state)
  {
  }
  @Override
  public void applyStyle()
  {
  }
   public void addColumnSorting(){
  addSortinggetCount();
  addSortinggetGroupName();
  addSortinggetStatus();
  }
  @Override public void addFieldUpdater() {
  }
  protected void addSortinggetCount()
  {
  List<LeaveGroup> list=getDataprovider().getList();
  columnSort=new ListHandler<LeaveGroup>(list);
  columnSort.setComparator(getCountColumn, new Comparator<LeaveGroup>()
  {
  @Override
  public int compare(LeaveGroup e1,LeaveGroup e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCount()== e2.getCount()){
  return 0;}
  if(e1.getCount()> e2.getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCount()
  {
  getCountColumn=new TextColumn<LeaveGroup>()
  {
  @Override
  public String getValue(LeaveGroup object)
  {
  if( object.getCount()==-1)
  return "N.A";
  else return object.getCount()+"";
  }
  };
  table.addColumn(getCountColumn,"Id");
  getCountColumn.setSortable(true);
  }
  protected void addSortinggetGroupName()
  {
  List<LeaveGroup> list=getDataprovider().getList();
  columnSort=new ListHandler<LeaveGroup>(list);
  columnSort.setComparator(getGroupNameColumn, new Comparator<LeaveGroup>()
  {
  @Override
  public int compare(LeaveGroup e1,LeaveGroup e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getGroupName()!=null && e2.getGroupName()!=null){
  return e1.getGroupName().compareTo(e2.getGroupName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetGroupName()
  {
  getGroupNameColumn=new TextColumn<LeaveGroup>()
  {
  @Override
  public String getValue(LeaveGroup object)
  {
  return object.getGroupName()+"";
  }
  };
  table.addColumn(getGroupNameColumn,"Group");
  getGroupNameColumn.setSortable(true);
  }
  protected void addSortinggetStatus()
  {
  List<LeaveGroup> list=getDataprovider().getList();
  columnSort=new ListHandler<LeaveGroup>(list);
  columnSort.setComparator(getStatusColumn, new Comparator<LeaveGroup>()
  {
  @Override
  public int compare(LeaveGroup e1,LeaveGroup e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getStatus()== e2.getStatus()){
  return 0;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetStatus()
  {
  getStatusColumn=new TextColumn<LeaveGroup>()
  {
  @Override
  public String getValue(LeaveGroup object)
  {
  if( object.getStatus()==true)
  return "Active";
  else 
  return "In Active";
  }
  };
  table.addColumn(getStatusColumn,"Status");
  getStatusColumn.setSortable(true);
  }
}
