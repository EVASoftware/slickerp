package com.slicktechnologies.client.views.humanresource.leavegroup;

import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;

public class LeaveGroupPresenter extends FormTableScreenPresenter<LeaveGroup> {
	
	LeaveGroupForm form;

	public LeaveGroupPresenter(FormTableScreen<LeaveGroup> view,
			LeaveGroup model) {
		super(view, model);
		form=(LeaveGroupForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getLeaveGroupQuery());
		form.setPresenter(this);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {

		InlineLabel lbl= (InlineLabel) e.getSource();
//		if(lbl.getText().contains("Delete"))
//		{
//			
//			if(this.form.validate())
//			{
//			List<LeaveGroup>data=form.getSupertable().getDataprovider().getList();
//			/*
//			 * Deleting
//			 */
//			for(int i=0;i<data.size();i++)
//			{
//				if(data.get(i).getId().equals(model.getId()))
//				{
//					data.remove(i);
//				}
//				form.getSupertable().getDataprovider().refresh();
//			}
//			reactOnDelete();
//			
//			}
//		}
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		model=new LeaveGroup();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getLeaveGroupQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new LeaveGroup());
		return quer;
	}
	
	public void setModel(LeaveGroup entity)
	{
		model=entity;
	}
	
	
	public static void initalize()
	{
			LeaveGroupPresenterTable gentableScreen=new LeaveGroupPresenterTableProxy();
			
			LeaveGroupForm  form=new  LeaveGroupForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			  //// LeaveGroupPresenterTable gentableSearch=GWT.create( LeaveGroupPresenterTable.class);
			  ////   LeaveGroupPresenterSearch.staticSuperTable=gentableSearch;
			  ////     LeaveGroupPresenterSearch searchpopup=GWT.create( LeaveGroupPresenterSearch.class);
			  ////         form.setSearchpopupscreen(searchpopup);
			
			LeaveGroupPresenter  presenter=new  LeaveGroupPresenter(form,new LeaveGroup());
			AppMemory.getAppMemory().stickPnel(form);
		 
	}
	

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.LeaveGroup")
	 public static class  LeaveGroupPresenterTable extends SuperTable<LeaveGroup> implements GeneratedVariableRefrence{

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub
			
		}} ;
		
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.LeaveGroup")
		 public static class  LeaveGroupPresenterSearch extends SearchPopUpScreen<LeaveGroup>{

			@Override
			public MyQuerry getQuerry() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean validate() {
				// TODO Auto-generated method stub
				return true;
			}};
			
			
			public void reactTo()
			{
				
			}



	
	
	

}
