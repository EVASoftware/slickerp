package com.slicktechnologies.client.views.humanresource.leavegroup;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class LeaveGroupForm extends FormTableScreen<LeaveGroup> implements ClickHandler, HasValue<ArrayList<AllocatedLeaves>>, CompositeInterface {

	TextBox tbgroupname;
	CheckBox cbgroupstatus;
	
	ObjectListBox<LeaveType> olbLeavetype;
	Button btnadd;
	LeaveGroupTable leaveGroupTable;
	
//	IntegerBox ibmaximumdays, iballocateddays;

	// Token to add the concrete presenter class name
	// protected <PresenterClassName> presenter;

	public LeaveGroupForm(SuperTable<LeaveGroup> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		leaveGroupTable.connectToLocal();
	}

	private void initalizeWidget() {

		tbgroupname = new TextBox();
		
		cbgroupstatus = new CheckBox();
		cbgroupstatus.setValue(true);
		
		olbLeavetype = new ObjectListBox<LeaveType>();
		this.initalizeLeaveTypeListBox();
		
		btnadd = new Button("Add");
		btnadd.addClickHandler(this);
		leaveGroupTable = new LeaveGroupTable();
		
		
//		ibmaximumdays = new IntegerBox();
//		ibmaximumdays.setEnabled(false);
//		iballocateddays = new IntegerBox();
		
		
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {
		// Token to initialize the processlevel menus.
		initalizeWidget();
		this.processlevelBarNames = new String[] { "New" };

		// Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingGroupInformation = fbuilder
				.setlabel("Group Information").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(3).build();

		fbuilder = new FormFieldBuilder("* Group Name", tbgroupname);
		FormField ftbgroupname = fbuilder.setMandatory(true)
				.setMandatoryMsg("Group Name can't be empty").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", cbgroupstatus);
		FormField fcbstatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDaysInformation = fbuilder
				.setlabel("Leave Type Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("Leave Type", olbLeavetype);
		FormField folbleavetype = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("Maximum Days", ibmaximumdays);
//		FormField fibmaximumdays = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Allocated Days", iballocateddays);
//		FormField fiballocateddays = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", btnadd);
		FormField fbtnadd = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", leaveGroupTable.getTable());
		FormField fleaveallocatedtable = fbuilder.setMandatory(true)
				.setMandatoryMsg("Leave allocation table can't be empty")
				.setRowSpan(0).setColSpan(3).build();

		// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// //////////////////////////////Lay Out Making
		// Code///////////////////////////////////////////////////////////////

		FormField[][] formfield = { 
				{ fgroupingGroupInformation },
				{ ftbgroupname, fcbstatus },
				{ fgroupingDaysInformation },
				{ folbleavetype, fbtnadd }, 
				{ fleaveallocatedtable } 
				};

		this.fields = formfield;
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(LeaveGroup model) {

		if (tbgroupname != null)
			model.setGroupName(tbgroupname.getValue());
		if (cbgroupstatus != null)
			model.setStatus(cbgroupstatus.getValue());
		List<AllocatedLeaves> daysalloc = leaveGroupTable.getDataprovider().getList();
		ArrayList<AllocatedLeaves> alloc = new ArrayList<AllocatedLeaves>();
		alloc.addAll(daysalloc);
		model.setAllocatedLeaves(alloc);

		presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(LeaveGroup view) {
		if (view.getGroupName() != null)
			tbgroupname.setValue(view.getGroupName());
		if (view.getStatus() != null)
			cbgroupstatus.setValue(view.getStatus());
		leaveGroupTable.setValue(view.getAllocatedLeaves());
		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.LEAVEGROUP,
		LoginPresenter.currentModule.trim());
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
//		ibmaximumdays.setEnabled(false);
		leaveGroupTable.setEnable(state);
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

	/**
	 * shows only active leave in the list box.
	 */
	protected void initalizeLeaveTypeListBox() {
		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("leaveStatus");
		filter.setBooleanvalue(true);

		querry.setQuerryObject(new LeaveType());
		querry.getFilters().add(filter);
		olbLeavetype.MakeLive(querry);
	}

	@Override
	public void onClick(ClickEvent event) {
		/*
		 * Code for adding leave type to Leave Group Table and validation for no
		 * leave type should be duplicated
		 */
		int flag = 0;

		if (olbLeavetype.getSelectedItem() != null) {
			LeaveType ltype = olbLeavetype.getSelectedItem();
			
			int retrieveMaxDays = ltype.getMaxDays();
			// Create Allocated Leaves Object
			AllocatedLeaves allocatedLeaves = new AllocatedLeaves();
			String a = this.olbLeavetype.getValue();

			int b = retrieveMaxDays;
			allocatedLeaves.setName(a);
			allocatedLeaves.setMaxDays(b);
			Double db = (double) b;
			allocatedLeaves.setBalance(db);
			allocatedLeaves.setRequested(0d);
			allocatedLeaves.setRejected(0d);
			allocatedLeaves.setApproved(0d);
			allocatedLeaves.setMinDays(ltype.getMinDays());
			allocatedLeaves.setCancellationAllow(ltype.getCancellationAllow());
			allocatedLeaves.setCountWeekend(ltype.getCountWeekend());
			allocatedLeaves.setCountHoliday(ltype.getCountHoliday());
			allocatedLeaves.setDocumentRequired(ltype.getDocumentRequired());
			allocatedLeaves.setDoumentRequiredMinDays(ltype.getDoumentRequiredMinDays());
			allocatedLeaves.setLeaveEncashable(ltype.getLeaveEncashable());
			allocatedLeaves.setMaxCarryForward(ltype.getMaxCarryForward());
			allocatedLeaves.setRelapseAllowCarryOver(ltype.getRelapseAllowCarryOver());
			allocatedLeaves.setUnpaidLeave(ltype.getPaidLeave());
			allocatedLeaves.setMonthlyAllocation(ltype.getMonthlyAllocation());
			allocatedLeaves.setMonthlyAllocationLeave(ltype.getMonthlyAllocationLeave());
			
			/**
			 * Date : 05-06-2018 By ANIL
			 * setting no. of days to avail complementory leave 
			 */
			allocatedLeaves.setCompensatoryLeave(ltype.isCompensatoryLeave());
			allocatedLeaves.setNo_of_days_to_avail(ltype.getNo_of_days_to_avail());
			
			/**
			 * End
			 */
			
			/**
			 * Date : 05-06-2018 By ANIL
			 */
			allocatedLeaves.setShortName(ltype.getShortName());
			/**
			 * End
			 */
			
			
			// //////////////////////////////////////////////////////////////////////////////////
			List<AllocatedLeaves> daysalloc = leaveGroupTable.getDataprovider().getList();
			ArrayList<AllocatedLeaves> alloc = new ArrayList<AllocatedLeaves>();
			alloc.addAll(daysalloc);

			String leaveType = "";

			for (int i = 0; i < alloc.size(); i++) {
				leaveType = alloc.get(i).getName();
				if (leaveType.equals(a) || a.equals("")) {
					flag = 1;
					olbLeavetype.setSelectedIndex(0);
				} else {
					flag = 0;
				}
			}

			if (flag == 0 && olbLeavetype.getSelectedItem() != null) {
				this.leaveGroupTable.getDataprovider().getList().add(allocatedLeaves);
				olbLeavetype.setSelectedIndex(0);
			}
		}

	}

	@Override
	public HandlerRegistration addValueChangeHandler(ValueChangeHandler<ArrayList<AllocatedLeaves>> handler) {
		return null;
	}

	@Override
	public void fireEvent(GwtEvent<?> event) {

	}

	@Override
	public ArrayList<AllocatedLeaves> getValue() {
		List<AllocatedLeaves> list = this.leaveGroupTable.getDataprovider().getList();
		ArrayList<AllocatedLeaves> vec = new ArrayList<AllocatedLeaves>();
		vec.addAll(list);
		return vec;
	}

	@Override
	public void setValue(ArrayList<AllocatedLeaves> value) {
		leaveGroupTable.connectToLocal();
		leaveGroupTable.getDataprovider().getList().addAll(value);
	}

	@Override
	public void setValue(ArrayList<AllocatedLeaves> value, boolean fireEvents) {

	}

	@Override
	public void clear() {
		super.clear();
		cbgroupstatus.setValue(true);
		leaveGroupTable.clear();

	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		boolean superValidate = super.validate();
		boolean salesLineItemValidate = true;
		if (this.leaveGroupTable.getDataprovider().getList().size() == 0) {
			showDialogMessage("Please Select at least one Leave Type !");
			salesLineItemValidate = false;
		}
		return salesLineItemValidate && superValidate;
	}

}
