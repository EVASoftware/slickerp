package com.slicktechnologies.client.views.humanresource.project;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.humanresource.project.HrProjectPresenter.ProjectPresenterSearch;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;

public class HrProjectSearchProxy extends ProjectPresenterSearch{
	
	public PersonInfoComposite personInfo;
	public IntegerBox tbContractId;
	public IntegerBox tbProjectId;
	ObjectListBox<EmployeeInfo> olbSuperVisorInfo;
	ObjectListBox<EmployeeInfo> olbManagerInfo;
	
	/**project objectlistbox added by amol  22-11-2018*/
	ObjectListBox<HrProject> project;
	
	
	/**status objectlistbox added by amol12-1-2018**/
	ObjectListBox<String> olbStatus;
	
	/**Date 28-3-2019 added by Amol**/
	public ObjectListBox<Branch> olbBranch;
	
	public HrProjectSearchProxy() {
		super();
		// TODO Auto-generated constructor stub
		createGui();
	}
	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
//		super.createScreen();
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		builder = new FormFieldBuilder("Contract Id",tbContractId);
		FormField ftbContractId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Project Id",tbProjectId);
		FormField ftbProjectId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Supervisor",olbSuperVisorInfo);
		FormField folbSuperVisorInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Manager",olbManagerInfo);
		FormField folbManagerInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder= new FormFieldBuilder("Project Name",project);
		FormField fproject=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder=new FormFieldBuilder("Status",olbStatus);
		FormField folbStatus=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		
		
		builder = new FormFieldBuilder();
		FormField fgroupingCustomerDetails=builder.setlabel("Customer Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		builder = new FormFieldBuilder();
		FormField fgroupingProjectDetails=builder.setlabel("Project Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				{fgroupingCustomerDetails},
				{fpersonInfo},
				{fgroupingProjectDetails},
				{ftbProjectId,folbSuperVisorInfo,folbManagerInfo,fproject},
				{folbStatus,folbBranch}
		};
		
	}
	
	@Override
	public MyQuerry getQuerry() {
		// TODO Auto-generated method stub
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(project.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(project.getValue().trim());
			temp.setQuerryString("projectName");
			filtervec.add(temp);
		}
		
		if(olbStatus.getSelectedIndex()!=0){
			boolean status=false;
			if(olbStatus.getValue(olbStatus.getSelectedIndex()).equals("Active")){
				status=true;
			}
			temp=new Filter();
			temp.setBooleanvalue(status);
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		
		
		if(olbSuperVisorInfo.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbSuperVisorInfo.getValue().trim());
			temp.setQuerryString("supervisor");
			filtervec.add(temp);
		}
		if(olbManagerInfo.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbManagerInfo.getValue().trim());
			temp.setQuerryString("manager");
			filtervec.add(temp);
		}
		if(personInfo.getIdValue()!=-1){
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("personinfo.count");
		  filtervec.add(temp);
		}
		if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("personinfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("personinfo.cellNumber");
		  filtervec.add(temp);
		  }
		  
		  if(tbContractId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbContractId.getValue());
				temp.setQuerryString("contractId");
				filtervec.add(temp);
		 }
		  if(tbProjectId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbProjectId.getValue());
				temp.setQuerryString("count");
				filtervec.add(temp);
		 }
		
			if(olbBranch.getSelectedIndex()!=0){
				temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
				temp.setQuerryString("branch");
				filtervec.add(temp);
			}
			
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new HrProject());
		return querry;
	}

	   private void initWidget() {
		// TODO Auto-generated method stub
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		Filter temp=new Filter();
		temp.setBooleanvalue(true);
		temp.setQuerryString("status");
		personInfo=new PersonInfoComposite(querry,false);
		tbContractId=new IntegerBox();
		tbProjectId=new IntegerBox();
		olbSuperVisorInfo=new ObjectListBox<EmployeeInfo>();
		EmployeeInfo.makeObjectListBoxLive(olbSuperVisorInfo);
		olbSuperVisorInfo=new ObjectListBox<EmployeeInfo>();
		EmployeeInfo.makeObjectListBoxLive(olbSuperVisorInfo);
		olbManagerInfo=new ObjectListBox<EmployeeInfo>();
		EmployeeInfo.makeObjectListBoxLive(olbManagerInfo);
		
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
		
		olbStatus=new ObjectListBox<String>();
		olbStatus.addItem("--SELECT--");
		olbStatus.addItem("Active");
		olbStatus.addItem("Inactive");
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
	}
	
	
}
