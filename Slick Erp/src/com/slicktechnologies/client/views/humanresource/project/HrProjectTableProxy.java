package com.slicktechnologies.client.views.humanresource.project;

import java.util.Comparator;
import java.util.List;

import com.slicktechnologies.client.views.humanresource.project.HrProjectPresenter.ProjectPresenterTable;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;

public class HrProjectTableProxy extends ProjectPresenterTable {
	TextColumn<HrProject> customerCellNumberColumn;
	TextColumn<HrProject> customerFullNameColumn;
	TextColumn<HrProject> customerIdColumn;
	TextColumn<HrProject> contractCountColumn;
	TextColumn<HrProject> projectCountColumn;
	TextColumn<HrProject> projectStartDateColumn;
	TextColumn<HrProject> projectEndDateColumn;
	TextColumn<HrProject> projectSupervisorColumn;
	TextColumn<HrProject> projectManagerColumn;
	TextColumn<HrProject> projectNameColumn;
	
	/**22-11-2018 added by amol for status **/
    TextColumn<HrProject> StatusColumn;
    
    /***28-2-2019 added by Amol***/
    TextColumn<HrProject> getBranchColumn;
    
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addprojectCountColumn();
		addprojectNameColumn();
		addprojectStartDateColumn();
		addprojectEndDateColumn();
		//addcontractCountColumn();
		addcustomerIdColumn();
		addcustomerFullNameColumn();
		addcustomerCellNumberColumn();
		addprojectSupervisorColumn();
		addprojectManagerColumn();
		/**Date 28-3-2019 added by Amol**/
		addColumngetBranch();
		addStatusColumn();
	}
	
//	@Override
//	public void addColumnSorting() {
	//	// TODO Auto-generated method stub
//	}

	private void addprojectNameColumn() {
		// TODO Auto-generated method stub
		projectNameColumn = new TextColumn<HrProject>() {

			@Override
			public String getValue(HrProject object) {
				// TODO Auto-generated method stub
				return object.getProjectName();
			}

		};

		table.addColumn(projectNameColumn, "Project Name");
		table.setColumnWidth(projectNameColumn, 130, Unit.PX);
		projectNameColumn.setSortable(true);
	}

	private void addprojectManagerColumn() {
		// TODO Auto-generated method stub
		projectManagerColumn = new TextColumn<HrProject>() {

			@Override
			public String getValue(HrProject object) {
				// TODO Auto-generated method stub
				return object.getManager();
			}

		};

		table.addColumn(projectManagerColumn, "Manager Name");
		table.setColumnWidth(projectManagerColumn, 130, Unit.PX);
		projectManagerColumn.setSortable(true);
	}

	private void addprojectSupervisorColumn() {
		// TODO Auto-generated method stub
		projectSupervisorColumn = new TextColumn<HrProject>() {

			@Override
			public String getValue(HrProject object) {
				// TODO Auto-generated method stub
				return object.getSupervisor();
			}

		};

		table.addColumn(projectSupervisorColumn, "Supervisor Name");
		table.setColumnWidth(projectSupervisorColumn, 130, Unit.PX);
		projectSupervisorColumn.setSortable(true);
	}

	private void addprojectEndDateColumn() {
		// TODO Auto-generated method stub
		projectEndDateColumn = new TextColumn<HrProject>() {

			@Override
			public String getValue(HrProject object) {
				// TODO Auto-generated method stub
				if(object.getToDate()!=null)
					return AppUtility.parseDate(object.getToDate());
				else
					return "N.A.";
			}

		};

		table.addColumn(projectEndDateColumn, "End Date");
		table.setColumnWidth(projectEndDateColumn, 130, Unit.PX);
		projectEndDateColumn.setSortable(true);
	}

	private void addprojectStartDateColumn() {
		// TODO Auto-generated method stub
		projectStartDateColumn = new TextColumn<HrProject>() {

			@Override
			public String getValue(HrProject object) {
				// TODO Auto-generated method stub
				if(object.getFromDate()!=null)
					return AppUtility.parseDate(object.getFromDate());
				else
					return "N.A.";
			}

		};

		table.addColumn(projectStartDateColumn, "Start Date");
		table.setColumnWidth(projectStartDateColumn, 130, Unit.PX);
		projectStartDateColumn.setSortable(true);
	}

	private void addprojectCountColumn() {
		// TODO Auto-generated method stub
		projectCountColumn = new TextColumn<HrProject>() {

			@Override
			public String getValue(HrProject object) {
				// TODO Auto-generated method stub
				return object.getCount()+"";
			}

		};

		table.addColumn(projectCountColumn, "Project Id");
		table.setColumnWidth(projectCountColumn, 130, Unit.PX);
		projectCountColumn.setSortable(true);
	}

//	private void addcontractCountColumn() {
//		// TODO Auto-generated method stub
//		contractCountColumn = new TextColumn<HrProject>() {
//
//			@Override
//			public String getValue(HrProject object) {
//				// TODO Auto-generated method stub
//				return object.getContractId()+"";
//			}
//
//		};
//
//		table.addColumn(contractCountColumn, "Contract Id");
//		table.setColumnWidth(contractCountColumn, 130, Unit.PX);
//		contractCountColumn.setSortable(true);
//	}

	private void addcustomerIdColumn() {
		// TODO Auto-generated method stub
		customerIdColumn = new TextColumn<HrProject>() {

			@Override
			public String getValue(HrProject object) {
				// TODO Auto-generated method stub
				if(object.getPersoninfo()!=null)//Ashwini Patil Date:26-07-2023 projects were not getting populated in search table
					return object.getPersoninfo().getCount()+"";
				else
					return "";
			}

		};

		table.addColumn(customerIdColumn, "Customer Id");
		table.setColumnWidth(customerIdColumn, 130, Unit.PX);
		customerIdColumn.setSortable(true);
	}

	private void addcustomerFullNameColumn() {
		// TODO Auto-generated method stub
		customerFullNameColumn = new TextColumn<HrProject>() {

			@Override
			public String getValue(HrProject object) {
				// TODO Auto-generated method stub
				if(object.getPersoninfo()!=null)//Ashwini Patil Date:26-07-2023 projects were not getting populated in search table
					return object.getPersoninfo().getFullName();
				else
					return "";
			}

		};

		table.addColumn(customerFullNameColumn, "Customer Name");
		table.setColumnWidth(customerFullNameColumn, 130, Unit.PX);
		customerFullNameColumn.setSortable(true);
	}

	private void addcustomerCellNumberColumn() {
		// TODO Auto-generated method stub
		customerCellNumberColumn = new TextColumn<HrProject>() {

			@Override
			public String getValue(HrProject object) {
				// TODO Auto-generated method stub
				if(object.getPersoninfo()!=null)//Ashwini Patil Date:26-07-2023 projects were not getting populated in search table
					return object.getPersoninfo().getCellNumber()+"";
				else
					return "";
			}

		};

		table.addColumn(customerCellNumberColumn, "Customer Cell");
		table.setColumnWidth(customerCellNumberColumn, 130, Unit.PX);
		customerCellNumberColumn.setSortable(true);
	}
	
	private void addStatusColumn() {
		StatusColumn=new TextColumn<HrProject>() {
			public String getValue(HrProject object) {
		if(object.getStatus()){
			 return "Active";
			}else{
				return "Inactive";
			}
			}
		};
		
		table.addColumn(StatusColumn, "Status");
		table.setColumnWidth(StatusColumn, 130, Unit.PX);
		StatusColumn.setSortable(true);
		
		
	}
	
	protected void addColumngetBranch() {
		getBranchColumn = new TextColumn<HrProject>() {
			@Override
			public String getValue(HrProject object) {
				return object.getBranch();
			}
		};
		table.addColumn(getBranchColumn, "Branch");
		table.setColumnWidth(getBranchColumn, 130, Unit.PX);
		getBranchColumn.setSortable(true);
	}
	
            /** 22-12-2018 added by amol sorting for columns**/
	@Override
	public void addColumnSorting() {
		
		 addSortingcustomerCellNumberColumn();
		 addSortingcustomerFullNameColumn();
		 addSortingcustomerIdColumn();
		// addSortingcontractCountColumn();
		 addSortingprojectCountColumn();
		 addSortingprojectStartDateColumn();
		 addSortingprojectEndDateColumn();
		 addSortingprojectSupervisorColumn();
		 addSortingprojectManagerColumn();
		 addSortingprojectNameColumn();
		 /**date 28-3-2019 added by Amol**/
			addSortinggetBranch();
		 addSortingStatusColumn();
	}
	
	public void addSortingcustomerCellNumberColumn() {
		List<HrProject> list = getDataprovider().getList();
		columnSort = new ListHandler<HrProject>(list);
		columnSort.setComparator(customerCellNumberColumn,
				new Comparator<HrProject>() {
					@Override
					public int compare(HrProject e1, HrProject e2) {if (e1 != null && e2 != null) {
						if (e1.getPersoninfo().getCellNumber() != null
								&& e2.getPersoninfo().getCellNumber() != null) {
							return e1.getPersoninfo().getCellNumber().compareTo(e2.getPersoninfo().getCellNumber());
						}
					} else {
						return 0;
					}
					return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	public void addSortingcustomerFullNameColumn() {

		List<HrProject> list = getDataprovider().getList();
		columnSort = new ListHandler<HrProject>(list);
		columnSort.setComparator(customerFullNameColumn,new Comparator<HrProject>() {
			@Override
			public int compare(HrProject e1, HrProject e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPersoninfo().getFullName() != null
							&& e2.getPersoninfo().getFullName() != null) {
						return e1.getPersoninfo().getFullName().compareTo(e2.getPersoninfo().getFullName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}
	public void addSortingcustomerIdColumn() {
		List<HrProject> list=getDataprovider().getList();
	 columnSort=new ListHandler<HrProject>(list);
	  columnSort.setComparator(customerIdColumn, new Comparator<HrProject>()
	  {
	  @Override
	  public int compare(HrProject e1,HrProject e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if(e1.getPersoninfo().getCount()== e2.getPersoninfo().getCount()){
	  return 0;}
	  if(e1.getPersoninfo().getCount()> e2.getPersoninfo().getCount()){
	  return 1;}
	  else{
	  return -1;}
	  }
	  else{
	  return 0;}
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  
		
	}
//	public void addSortingcontractCountColumn() {
//		List<HrProject> list=getDataprovider().getList();
//		  columnSort=new ListHandler<HrProject>(list);
//		  columnSort.setComparator(contractCountColumn, new Comparator<HrProject>()
//		  {
//		  @Override
//		  public int compare(HrProject e1,HrProject e2)
//		  {
//		  if(e1!=null && e2!=null)
//		  {
//		  if( e1.getContractId()!=null && e2.getContractId()!=null){
//		  return e1.getContractId().compareTo(e2.getContractId());}
//		  }
//		  else{
//		  return 0;}
//		  return 0;
//		  }
//		  });
//		  table.addColumnSortHandler(columnSort);
//		  
//	
//	}
	
	 public void addSortingprojectCountColumn() {
		 List<HrProject> list=getDataprovider().getList();
		  columnSort=new ListHandler<HrProject>(list);
		  columnSort.setComparator(projectCountColumn, new Comparator<HrProject>()
		  {
		  @Override
		  public int compare(HrProject e1,HrProject e2)
		  {
			  if(e1!=null && e2!=null)
		  {
		  if(e1.getCount()== e2.getCount()){
		  return 0;}
		  if(e1.getCount()> e2.getCount()){
		  return 1;}
		  else{
		  return -1;}
		  }
		  else{
		  return 0;}
		  }
		  });
		  table.addColumnSortHandler(columnSort);
		  
		 
	 }
	
	public void addSortingprojectStartDateColumn() {
		List<HrProject> list=getDataprovider().getList();
		  columnSort=new ListHandler<HrProject>(list);
		  columnSort.setComparator(projectStartDateColumn, new Comparator<HrProject>()
		  {
		  @Override
		  public int compare(HrProject e1,HrProject e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getFromDate()!=null && e2.getFromDate()!=null){
		  return e1.getFromDate().compareTo(e2.getFromDate());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
		  
		  
		
	}
	
	public void addSortingprojectEndDateColumn() {
		List<HrProject> list=getDataprovider().getList();
		  columnSort=new ListHandler<HrProject>(list);
		  columnSort.setComparator(projectEndDateColumn, new Comparator<HrProject>()
		  {
		  @Override
		  public int compare(HrProject e1,HrProject e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getToDate()!=null && e2.getToDate()!=null){
		  return e1.getToDate().compareTo(e2.getToDate());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
		  
		
	}
	
	
	public void addSortingprojectSupervisorColumn() {
		List<HrProject> list=getDataprovider().getList();
		  columnSort=new ListHandler<HrProject>(list);
		  columnSort.setComparator(projectSupervisorColumn, new Comparator<HrProject>()
		  {
		  @Override
		  public int compare(HrProject e1,HrProject e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getSupervisor()!=null && e2.getSupervisor()!=null){
		  return e1.getSupervisor().compareTo(e2.getSupervisor());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
		  
		
	}
	
	public void addSortingprojectManagerColumn() {
		List<HrProject> list=getDataprovider().getList();
		  columnSort=new ListHandler<HrProject>(list);
		  columnSort.setComparator(projectManagerColumn, new Comparator<HrProject>()
		  {
		  @Override
		  public int compare(HrProject e1,HrProject e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getManager()!=null && e2.getManager()!=null){
		  return e1.getManager().compareTo(e2.getManager());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}
	
	public void addSortingprojectNameColumn() {
		List<HrProject> list=getDataprovider().getList();
		  columnSort=new ListHandler<HrProject>(list);
		  columnSort.setComparator(projectNameColumn, new Comparator<HrProject>()
		  {
		  @Override
		  public int compare(HrProject e1,HrProject e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getProjectName()!=null && e2.getProjectName()!=null){
		  return e1.getProjectName().compareTo(e2.getProjectName());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
		  
		
	}

	public void addSortingStatusColumn() {
		List<HrProject> list=getDataprovider().getList();
		columnSort=new ListHandler<HrProject>(list);
		columnSort.setComparator(StatusColumn, new Comparator<HrProject>()
				{
					
					@Override
					public int compare(HrProject e1, HrProject e2) 
					{
						if(e1!=null && e2!=null)
						{
							if(e1.getStatus()==e2.getStatus())
								
							return 0;
							else if(e1.getStatus()==true)
								return 1;
							else
								return -1;
						}
						else
							return -1;
					}
				});
		table.addColumnSortHandler(columnSort);
		
		
		
		
		
		
	}
	
	protected void addSortinggetBranch() {
		List<HrProject> list = getDataprovider().getList();
		columnSort = new ListHandler<HrProject>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<HrProject>() {
			@Override
			public int compare(HrProject e1, HrProject e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null && e2.getBranch() != null) {
						return e1.getBranch().compareTo(e2.getBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	
	
	
	

}
