
package com.slicktechnologies.client.views.humanresource.project;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.cnc.CNCPresenter.CNCPresenterSearch;
import com.slicktechnologies.client.views.projectallocation.ProjectAllocationForm;
import com.slicktechnologies.client.views.projectallocation.ProjectAllocationPresenter;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;

public class HrProjectPresenter extends FormScreenPresenter<HrProject>implements SelectionHandler<Suggestion> {
	
	HrProjectForm form;
	CsvServiceAsync csvservice = GWT.create(CsvService.class);

	public HrProjectPresenter(FormScreen<HrProject> view, HrProject model) {
		super(view, model);
		form=(HrProjectForm) view;
		//form.getSupertable().connectToLocal();
		//form.retriveTable(getProjectQuery());
		form.personInfo.getId().addSelectionHandler(this);
		form.personInfo.getName().addSelectionHandler(this);
		form.personInfo.getPhone().addSelectionHandler(this);
		
		form.setPresenter(this);
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel lbl= (InlineLabel) e.getSource();
		String text = lbl.getText().trim();
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
		if(lbl.getText().contains("View Project Allocation")){
			reactOnViewProjectAllocation();
		}
	}

	private void reactOnViewProjectAllocation() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("projectName");
		filter.setStringValue(model.getProjectName());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new ProjectAllocation());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No Project found.");
					return;
				}
				if(result.size()!=0){
					final ProjectAllocation projectAllocation=(ProjectAllocation) result.get(0);
					final ProjectAllocationForm projectform=ProjectAllocationPresenter.initalize();
					projectform.loadCustomerBranch(form.getPersonInfo().getIdValue(), null);
					
					Timer timer=new Timer() {
						@Override
						public void run() {
							
							if(form.getOblCustomerBranch().getSelectedIndex()!=0){
								projectform.getOblCustomerBranch().setValue(form.getOblCustomerBranch().getValue());
							}
							projectform.updateView(projectAllocation);
							projectform.setToViewState();
						}
					};
					timer.schedule(1000);
				}
			}
		});
	
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		model=new HrProject();
	}

	
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getProjectQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new HrProject());
		return quer;
	}
	
	public void setModel(HrProject entity)
	{
		model=entity;
	}
	
	public static HrProjectForm initalize()
	{
		ProjectPresenterTable gentableScreen=new ProjectPresenterTableProxy();
			
		HrProjectForm  form=new  HrProjectForm();
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			  //// ProjectPresenterTable gentableSearch=GWT.create( ProjectPresenterTable.class);
			  ////   ProjectPresenterSearch.staticSuperTable=gentableSearch;
			  ////     ProjectPresenterSearch searchpopup=GWT.create(ProjectPresenterSearch.class);
			  ////         form.setSearchpopupscreen(searchpopup);
			ProjectPresenterTable gentable = new HrProjectTableProxy();
			gentable.setView(form);
			gentable.applySelectionModle();
			CNCPresenterSearch.staticSuperTable = gentable;
			HrProjectSearchProxy searchpopup = new HrProjectSearchProxy();
			form.setSearchpopupscreen(searchpopup);

			HrProjectPresenter  presenter=new  HrProjectPresenter(form,new HrProject());
			
			//Ashwini Patil changed name from "Human Resource/Project" to "HR Configuration/Project"
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("HR Configuration/Project",Screen.HRPROJECT);
			AppMemory.getAppMemory().stickPnel(form);
			
			return form;
		 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.HrProject")
		 public static class  ProjectPresenterTable extends SuperTable<HrProject> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.HrProject")
			 public static class  ProjectPresenterSearch extends SearchPopUpScreen<HrProject>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
				
				public void reactTo()
				{
					
				}


				public void reactOnDownload() {
					ArrayList<HrProject> hrprojectArray = new ArrayList<HrProject>();
					List<HrProject> list = (List<HrProject>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
					hrprojectArray.addAll(list);
					csvservice.setHRprojectlist(hrprojectArray, new AsyncCallback<Void>() {
						@Override
						public void onFailure(Throwable caught) {
							System.out.println("RPC call Failed" + caught);
						}

						@Override
						public void onSuccess(Void result) {
							String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
							final String url = gwt + "csvservlet" + "?type=" + 163;
							Window.open(url, "test", "enabled");
						}
					});
				}

				@Override
				public void onSelection(SelectionEvent<Suggestion> event) {
					if (event.getSource().equals(form.getPersonInfo().getId())
							|| event.getSource().equals(
									form.getPersonInfo().getName())
							|| event.getSource().equals(
									form.getPersonInfo().getPhone())) {
						form.oblCustomerBranch.clear();
						form.loadCustomerBranch(form.getPersonInfo().getIdValue(), null);
						
						form.overtimeTable.setCustId(form.getPersonInfo().getIdValue());
						form.overtimeTable.setState(form.oblState.getValue());
						for(Overtime empProj:form.overtimeTable.getValue()){
							empProj.setSiteLocation(null);
						}
						form.overtimeTable.setEnable(true);
					}		
				}
	
	
}
//package com.slicktechnologies.client.views.humanresource.project;
//
//import java.util.List;
//import java.util.Vector;
//
//import com.google.gwt.core.shared.GWT;
//import com.google.gwt.event.dom.client.ClickEvent;
//import com.google.gwt.user.client.ui.InlineLabel;
//import com.simplesoftwares.client.library.appskeleton.AppMemory;
//import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
//import com.simplesoftwares.client.library.appstructure.SuperTable;
//import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
//import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
//import com.simplesoftwares.client.library.appstructure.search.Filter;
//import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
//import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
//import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
//import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
//import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
//
//public class HrProjectPresenter extends FormTableScreenPresenter<HrProject> {
//	
//	HrProjectForm form;
//
//	public HrProjectPresenter(FormTableScreen<HrProject> view, HrProject model) {
//		super(view, model);
//		form=(HrProjectForm) view;
//		form.getSupertable().connectToLocal();
//		form.retriveTable(getProjectQuery());
//		form.setPresenter(this);
//	}
//
//	@Override
//	public void reactToProcessBarEvents(ClickEvent e) {
//		InlineLabel lbl= (InlineLabel) e.getSource();
//		if(lbl.getText().contains("New"))
//		{
//			form.setToNewState();
//			this.initalize();
//		}
//	}
//
//	@Override
//	public void reactOnPrint() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void reactOnEmail() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	protected void makeNewModel() {
//		model=new HrProject();
//	}
//
//	
//	
//	/*
//	 * Method template to set Myquerry object
//	 */
//	public MyQuerry getProjectQuery()
//	{
//		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new HrProject());
//		return quer;
//	}
//	
//	public void setModel(HrProject entity)
//	{
//		model=entity;
//	}
//	
//	public static void initalize()
//	{
//		ProjectPresenterTable gentableScreen=new ProjectPresenterTableProxy();
//			
//		HrProjectForm  form=new  HrProjectForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
//			gentableScreen.setView(form);
//			gentableScreen.applySelectionModle();
//			
//			  //// ProjectPresenterTable gentableSearch=GWT.create( ProjectPresenterTable.class);
//			  ////   ProjectPresenterSearch.staticSuperTable=gentableSearch;
//			  ////     ProjectPresenterSearch searchpopup=GWT.create(ProjectPresenterSearch.class);
//			  ////         form.setSearchpopupscreen(searchpopup);
//			
//			HrProjectPresenter  presenter=new  HrProjectPresenter(form,new HrProject());
//			AppMemory.getAppMemory().stickPnel(form);
//			
//			
//		 
//	}
//	
//	
//		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.HrProject")
//		 public static class  ProjectPresenterTable extends SuperTable<HrProject> implements GeneratedVariableRefrence{
//
//			@Override
//			public Object getVarRef(String varName) {
//				// TODO Auto-generated method stub
//				return null;
//			}
//
//			@Override
//			public void createTable() {
//				// TODO Auto-generated method stub
//				
//			}
//
//			@Override
//			protected void initializekeyprovider() {
//				// TODO Auto-generated method stub
//				
//			}
//
//			@Override
//			public void addFieldUpdater() {
//				// TODO Auto-generated method stub
//				
//			}
//
//			@Override
//			public void setEnable(boolean state) {
//				// TODO Auto-generated method stub
//				
//			}
//
//			@Override
//			public void applyStyle() {
//				// TODO Auto-generated method stub
//				
//			}} ;
//			
//			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.HrProject")
//			 public static class  ProjectPresenterSearch extends SearchPopUpScreen<HrProject>{
//
//				@Override
//				public MyQuerry getQuerry() {
//					// TODO Auto-generated method stub
//					return null;
//				}
//
//				@Override
//				public boolean validate() {
//					// TODO Auto-generated method stub
//					return true;
//				}};
//				
//				
//				public void reactTo()
//				{
//					
//				}
//
//
//	
//	
//}
