package com.slicktechnologies.client.views.humanresource.project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.StaffingDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.OtEarningComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.smartgwt.client.widgets.grid.ListGrid;

public class HrProjectForm extends FormScreen<HrProject> implements ClickHandler, ChangeHandler {
	HrProject hrProject ;
	/*** Date 22-07-2020 Budget hours and Project code removed as per the requirements****/
	TextBox tbprojectname;
//	TextBox tbProjectCode;
	//PersonInfoComposite personinfo;
	TextArea notes;
	ObjectListBox<EmployeeInfo> olbteam;
	TextBox olbtask;
	Button addteam;
	TeamTable teamtable;
	TaskTable tasktable;
	Button addtask;
	/*
	 * Ashwini Patil 
	 * Date:11-07-2023 
	 * Spick and spine want to do payroll of one project in which every employee has a weekly off defined ie a specific day like monday or tuesday etc
	 * but employee can work on wo day and can take weekly off on another day
	 * at the end of month we have to calculate no of wo for that employee and need to pay salary as per no of days worked. 
	 * so defining this rotationalWeeklyOff flag to manage this
	 */
	CheckBox cbprojectstatus,cbRotationalWeeklyOff;
	TextArea tadescription;
	ProjectAllocation projectallocation;
	EmployeeInfo emp;
	CNC cncObject ;
	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;
	// Budget Budget
	/**
	 * Date : 31-05-2018 By ANIL
	 * Adding client ,budgeting hours ,from date and to date
	 */
	PersonInfoComposite personInfo;
//	DoubleBox dbBudgetedHours;
	DateBox dtFromDate;
	DateBox dtToDate;
	
	/**
	 * Date : 16-07-2018 By ANIL
	 */
	ObjectListBox<Overtime> oblOtList;
	Button btnAdd;
	OvertimeTable overtimeTable;
	
	/**
	 * Date : 23-07-2018 By ANIL
	 */
	ObjectListBox<EmployeeInfo> oblEmployee;
	ObjectListBox<Config> oblDesignation;
	ObjectListBox<Branch> oblBranch;
	
	
	/** date 14.8.2018 added by komal**/
	ObjectListBox<EmployeeInfo> olbSuperVisorInfo;
	ObjectListBox<EmployeeInfo> olbManagerInfo;
	ObjectListBox<EmployeeInfo> olbBranchManagerInfo;
	EmployeeInfoComposite employeeInfo;
	final List<EmployeeInfo> empProjectList = new ArrayList<EmployeeInfo>();
	//Added by Priyanka
	final List<StaffingDetails> sdDeginationList = new ArrayList<StaffingDetails>();
	/**
	 * Date : 30-10-2018 @author Anil
	 */
	TextBox tbNatureOfWork;
	/**
	 * date 10.1.2019 added by komal
	 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
	 *               which one is allowed to mark attendance
	 **/
	DoubleBox syncFrequency;
	DoubleBox submitFrequency;
	DoubleBox allowedRadius;
	DoubleBox locationLatitude;
	DoubleBox locationLongitude;
	/** date 16.08.2019 added by komal to store minimum ot hours**/
	DoubleBox dbMiOtMinutes;
	
	
	/**
	 * @author Anil , Date : 29-11-2019 Capturing paid days for payroll,
	 *         currently per day salary calculated on the basis of month days
	 */

	DoubleBox dbPaidDaysInMonth;
	CheckBox cbExceptionForMonth;
	DoubleBox dbPaidDaysForExceptionalMonth;
	ListBox lbMonth;
	TextBox tbExtraDayCompName;
		ObjectListBox<CustomerBranchDetails> oblCustomerBranch;
	List<CustomerBranchDetails> customerBranchList=new ArrayList<CustomerBranchDetails>();
	final GenricServiceAsync async =GWT.create(GenricService.class);
	
	/**
	 * @author Anil
	 * @since 24-07-2020
	 * Adding field payroll start day which will define payroll cycle
	 */
	IntegerBox ibPayrollStartDay;
	boolean payrollCycleFlag;
	
	/**
	 * @author Anil
	 * @since 20-08-2020
	 * Sunrise facility : state and site location changes
	 * Raised by Rahul Tiwari
	 */
	ObjectListBox<State> oblState;
	boolean addStateFlag=false;
	
	/**
	 * @author Anil
	 * @since 28-08-2020
	 * EnableSiteLocation
	 */
//	boolean siteLocation;
	ObjectListBox<Config> oblDesignation1;
	ObjectListBox<Overtime> oblOtList1;
	Button btnAdd1;
	Button btnClearOT;
	OvertimeTable relieverOtTable;
		Button btnClear;
		
	/**
	 * @author Anil
	 * @since 04-02-2021
	 * adding pf capping amount raised by Rahul for sun rise	
	 */
	DoubleBox dbPfCappingAmt;
	

	IntegerBox fixedPayrollDays;//Ashwini Patil Date:2-11-2023 for spick and span
		
	public static ArrayList<Integer> deletedOtIdlist = new ArrayList<Integer>();
	
	/*
	 * Ashwini Patil 
	 * Date:22-01-2024 
	 * for Advance Fm since their client given unplanned holidays
	 * If holiday is not defined in calendar and we try to upload attendance with holiday they system gives error.
	 * they want to upload such holiday even if it is not defined in calendar.
	 * We are giving this flag and allowing to upload unplanned holiday if this flag is active
	 */	
	CheckBox cbAllowUnplannedHoliday;
	public HrProjectForm() {
		super(FormStyle.DEFAULT);
		createGui();
		overtimeTable.connectToLocal();
		payrollCycleFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("HrProject", "PayrollDayForMultiplePayrollCycle");
		addStateFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "AddStatefield");
		
//		siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("HrProject", "EnableSiteLocation");
		relieverOtTable.connectToLocal();
	}
	
	private void initalizeWidget() {
//		tbProjectCode = new TextBox();
		tbprojectname = new TextBox();
		tadescription = new TextArea();
		cbprojectstatus = new CheckBox();
		cbRotationalWeeklyOff= new CheckBox();
		cbAllowUnplannedHoliday=new CheckBox();
		olbtask = new TextBox();

		cbprojectstatus.setValue(true);
		cbRotationalWeeklyOff.setValue(false);
		cbRotationalWeeklyOff.setTitle("Set this to true only if employee's calendar has weekly off defined and still you wnat to allow employee to adjust WO on another day. Also payroll will be generated on basis of no of days worked");
		cbAllowUnplannedHoliday.setValue(false);
		cbAllowUnplannedHoliday.setTitle("set this to true if you want to upload attendance as holiday even if that holiday is not defined in calendar.");
		
		olbteam = new ObjectListBox<EmployeeInfo>();
		EmployeeInfo.makeObjectListBoxLive(olbteam);
		teamtable = new TeamTable();
		teamtable.connectToLocal();
		tasktable = new TaskTable();
		tasktable.connectToLocal();
		// personinfo=AppUtility.customerInfoComposite(new Customer());
		addtask = new Button("Add");
		addtask.addClickHandler(this);
		addteam = new Button("Add");
		addteam.addClickHandler(this);
		/*
		 * personinfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		 * personinfo
		 * .getCustomerName().getHeaderLabel().setText("Customer Name");
		 * personinfo.getCustomerCell().getHeaderLabel().setText(
		 * "Customer Cell"); personinfo.setValidate(false);
		 */

		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo = new PersonInfoComposite(querry, false);
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");

//		dbBudgetedHours = new DoubleBox();
		dtFromDate = new DateBoxWithYearSelector();
		dtToDate = new DateBoxWithYearSelector();
		
		oblOtList=new ObjectListBox<Overtime>();
		Filter filter=new Filter();
		filter.setBooleanvalue(true);
		filter.setQuerryString("status");
		MyQuerry querry1=new MyQuerry();
		querry1.getFilters().add(filter);
		querry1.setQuerryObject(new Overtime());
		oblOtList.MakeLive(querry1);
		btnAdd=new Button("Add");
		btnAdd.addClickHandler(this);
		
		overtimeTable=new OvertimeTable();
		

		oblBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(oblBranch);
		oblBranch.addChangeHandler(this);
		

		oblDesignation=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblDesignation,Screen.EMPLOYEEDESIGNATION);
		oblDesignation.addChangeHandler(this);
		
//		oblEmployee=new ObjectListBox<EmployeeInfo>();
//		AppUtility.makeSalesPersonListBoxLive(oblEmployee);
		
		/** date 18.8.2018 added by komal **/
		olbManagerInfo = new ObjectListBox<EmployeeInfo>();
		olbManagerInfo.makeEmployeeLive(AppConstants.HRMODULE, "Project Allocation", "Manager");		
		olbSuperVisorInfo = new ObjectListBox<EmployeeInfo>();
		olbSuperVisorInfo.makeEmployeeLive(AppConstants.HRMODULE, "Project Allocation", "Supervisor");
		olbBranchManagerInfo = new ObjectListBox<EmployeeInfo>();
		olbBranchManagerInfo.makeEmployeeLive(AppConstants.HRMODULE, "Project Allocation", "Branch Manager");
		employeeInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),true);
		employeeInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		employeeInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		employeeInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
		/**
		 * end komal
		 */
		
		
		oblEmployee=new ObjectListBox<EmployeeInfo>();
//		AppUtility.makeSalesPersonListBoxLive(oblEmployee);
		
		GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
	
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		query.setFilters(filtervec);
		
		query.setQuerryObject(new EmployeeInfo());
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				for(SuperModel s : result){
					EmployeeInfo emp = (EmployeeInfo) s;
					if(emp.isLeaveAllocated())
						empProjectList.add(emp);
				}
				initializeEmployeeDropDown(oblEmployee,empProjectList,null,false);
				Console.log(" method call"+empProjectList.size());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
		tbNatureOfWork=new TextBox();
		/**
		 * date 10.1.2019 added by komal
		 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
		 *               which one is allowed to mark attendance
		 **/
		syncFrequency = new DoubleBox();
		submitFrequency = new DoubleBox();
		allowedRadius = new DoubleBox();
		locationLatitude = new DoubleBox();
		locationLongitude = new DoubleBox();
		
		/** date 16.08.2019 added by komal to store minimum ot hours**/
		dbMiOtMinutes = new DoubleBox();
		
		dbPaidDaysInMonth=new DoubleBox();
		cbExceptionForMonth=new CheckBox();
		cbExceptionForMonth.setValue(false);
		cbExceptionForMonth.addClickHandler(this);
		dbPaidDaysForExceptionalMonth=new DoubleBox();
		dbPaidDaysForExceptionalMonth.setEnabled(false);

		lbMonth = new ListBox();
		lbMonth.addItem("--SELECT--");
		lbMonth.addItem("JAN");
		lbMonth.addItem("FEB");
		lbMonth.addItem("MAR");
		lbMonth.addItem("APR");
		lbMonth.addItem("MAY");
		lbMonth.addItem("JUN");
		lbMonth.addItem("JUL");
		lbMonth.addItem("AUG");
		lbMonth.addItem("SEP");
		lbMonth.addItem("OCT");
		lbMonth.addItem("NOV");
		lbMonth.addItem("DEC");
		lbMonth.setEnabled(false);
			
		tbExtraDayCompName=new TextBox();
			oblCustomerBranch=new ObjectListBox<CustomerBranchDetails>();
		oblCustomerBranch.addItem("--SELECT--");
		
		ibPayrollStartDay=new IntegerBox();
		fixedPayrollDays=new IntegerBox();
		fixedPayrollDays.setTitle("Generate payroll by fixed paid days. For example if 26, payroll will be generated by 26 days for all months including February. If 25 days worked, one day less salary and if 27 days worked, one additional day salary.");
		Console.log("");
		oblState=new ObjectListBox<State>();
		State.makeOjbectListBoxLive(oblState);
//		oblState.setEnabled(false);
		
		oblDesignation1=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblDesignation1,Screen.EMPLOYEEDESIGNATION);
		oblDesignation1.addChangeHandler(this);
		
		oblOtList1=new ObjectListBox<Overtime>();
		Filter filter1=new Filter();
		filter1.setBooleanvalue(true);
		filter1.setQuerryString("status");
		MyQuerry querry11=new MyQuerry();
		querry11.getFilters().add(filter1);
		querry11.setQuerryObject(new Overtime());
		oblOtList1.MakeLive(querry11);
		btnAdd1=new Button("Add");
		btnAdd1.addClickHandler(this);
		
		btnClearOT =new Button("Clear");
		btnClearOT.addClickHandler(this);
		relieverOtTable=new OvertimeTable(true);
			btnClear =new Button("Clear");
	btnClear.addClickHandler(this);
	
		dbPfCappingAmt=new DoubleBox();
	}

	
	protected void initializeEmployeeDropDown(ObjectListBox<EmployeeInfo> oblEmployee,List<EmployeeInfo> empProjectList,String projectName,boolean isCncProject) {
		// TODO Auto-generated method stub
		ArrayList<EmployeeInfo>unAllocatedEmp=new ArrayList<EmployeeInfo>();
		for(EmployeeInfo info:empProjectList){
			
			if(isCncProject){
				if(projectName!=null&&projectName.equals(info.getProjectName())){
					unAllocatedEmp.add(info);
//					Console.log(info.getEmpCount()+" / "+info.getFullName()+" / "+info.getDesignation());
				}
			}else{
				if(info.getProjectName()==null||info.getProjectName().equals("")
						||(projectName!=null&&projectName.equals(info.getProjectName()))){
					unAllocatedEmp.add(info);
//					Console.log(info.getEmpCount()+" / "+info.getFullName()+" / "+info.getDesignation());
				}
			}
			
		}
		Console.log("method inside"+empProjectList.size());
		oblEmployee.setListItems(unAllocatedEmp);
		
		/**
		 * @author Anil
		 * @since 17-12-2020
		 * updating employee name by adding employee id in it separated by '/'
		 */
		ArrayList<String> empNameList=new ArrayList<String>();
		for(int i=0;i<oblEmployee.getItemCount();i++){
			if(empNameList.contains(oblEmployee.getItemText(i))){
				Console.log("EMPLOYEE NAME REPEATED "+oblEmployee.getItemText(i));
				EmployeeInfo info=oblEmployee.getItems().get(i-1);
				oblEmployee.setItemText(i, info.getFullName()+" / "+info.getEmpCount());
				Console.log("EMPLOYEE NAME REPEATED "+oblEmployee.getItemText(i));
			}
			empNameList.add(oblEmployee.getItemText(i));
		}
		
	}

	/*
	 * Method template to create the formtable screen
	 */
	 public void onClick(ClickEvent event)
	    {
		 
		ArrayList<Overtime>overTimeList=new ArrayList<Overtime>();
		 if(event.getSource().equals(btnClear)){
			 for(Overtime ot:overtimeTable.getDataprovider().getList()){
				 if(ot.getName()!=null&&!ot.getName().equals("")){
					 overTimeList.add(ot);
				 }
				 
			 }
			 Console.log("overTimeList "+overTimeList.size());
			 overtimeTable.getDataprovider().getList().removeAll(overTimeList);
		 }
		 /**
		  *  Date : 18-02-2021 Added by Priyanka.
		  *  Reliver OT list get clear after click on clear Button
		  */
		 if(event.getSource().equals(btnClearOT)){
			 for(Overtime ot:relieverOtTable.getDataprovider().getList()){  
				 if(ot.getName()!=null&&!ot.getName().equals("")){
					 overTimeList.add(ot);
				 }
				 
			 }
			 Console.log("overTimeList "+overTimeList.size());
			 relieverOtTable.getDataprovider().getList().removeAll(overTimeList);
		 }
		 
		 /**
		  *  End
		  */

		 
		 
		 
	        if(event.getSource().equals(addtask))
	        {
	        	if(this.olbtask.getValue()!=null)
	        		reactOnAddTask();
	        }
	        if(event.getSource().equals(addteam))
	        {
	        	if(this.olbteam.getValue()!=null)
	        		reactOnAddEmployee();
	        }
	     
	        
	        
	        
	        /** date 18.8.2018 added by komal**/
//	        if(event.getSource().equals(btnAdd)){
//	        	if(employeeInfo.getId().getValue().equals("")){
//					showDialogMessage("Please add employee information!");
//				}else{
//					if(oblOtList.getSelectedIndex()==0){
//		        		showDialogMessage("Please select overtime.");
//		        	}else{
//		        		Overtime ot1=oblOtList.getSelectedItem();
//		        		ot1.setEmpDesignation(employeeInfo.getEmpDesignation());
//		        		ot1.setEmpId(employeeInfo.getEmployeeId());
//		        		ot1.setEmpName(employeeInfo.getEmployeeName());
//		        		boolean flag=false;
//		        		for(Overtime ot:overtimeTable.getDataprovider().getList()){
//		        			if(ot1.getName().equals(ot.getName())
//		        					&&employeeInfo.getEmployeeId()==ot.getEmpId()){
//		        				showDialogMessage("Overtime already added.");
//		        				flag=true;
//		        				break;
//		        			}
//		        		}
//		        		if(!flag)
//		        			overtimeTable.getDataprovider().getList().add(ot1);
//		        		
//					}
//				}
//	        }
	        
	        
	        if(event.getSource().equals(btnAdd)){
	        	
	        	/**
	        	 * @author Vijay Date - 02-10-2020
	        	 * Des :- Overtime non mandatory with process confi 
	        	 * requirement raised by Rahul 
	        	 */
	        	boolean overtimeNonMandatoryFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("HrProject", AppConstants.PC_HRPROJECTOVERTIMENONMANDATORY);
	        	if(overtimeNonMandatoryFlag){
	        		reactonOvertimeNonMandatory();
	        	}
	        	else {
	        		
	        		if(oblOtList.getSelectedIndex()==0){
		        		showDialogMessage("Please select overtime.");
		        	}else{
		        		Overtime ot1=oblOtList.getSelectedItem();
		        		
		        		if(oblEmployee.getSelectedIndex()!=0){
		        			/**
		        			 * Date-12-2-2019 added by Amol to solve the issue of "same overtime cannot be  added to different employee"
		        			 */
		        			
		        			Overtime overtime=new Overtime();
		        			
		        			overtime.setCompanyId(ot1.getCompanyId());
		        			overtime.setCount(ot1.getCount());
		        			
		        			overtime.setFlat(ot1.isFlat());
		        			overtime.setFlatRate(ot1.getFlatRate());
		        			
		        			overtime.setHolidayType(ot1.getHolidayType());
		        			overtime.setHourly(ot1.isHourly());
		        			overtime.setHourlyRate(ot1.getHourlyRate());
		        			
		        			overtime.setLeave(ot1.isLeave());
		        			overtime.setLeaveMultiplier(ot1.getLeaveMultiplier());
		        			overtime.setLeaveName(ot1.getLeaveName());
		        			
		        			overtime.setName(ot1.getName());
		        			overtime.setNormalDays(ot1.isNormalDays());
		        			
		        			overtime.setPercentOfCTC(ot1.getPercentOfCTC());
		        			
		        			overtime.setShortName(ot1.getShortName());
		        			overtime.setStatus(ot1.isStatus());
		        			
		        			overtime.setWeeklyOff(ot1.isWeeklyOff());
		        			
		        			/**
		        			 * @author Anil , Date : 11-06-2019
		        			 * updating formula oT details
		        			 */
		        			updateFormulaOt(overtime,ot1);
		        			
		        			
		        			/**
		        			 * 
		        			 */
		        			
		        			EmployeeInfo employee=oblEmployee.getSelectedItem();
	              /**Date 21-8-2019 by Amol show alert if employee joining date is greater than or equal to 
	                 current (todays) date,and do not allow to add    while add a employee in project **/	        			
		        			Date date=new Date();
		        			if(employee.getDateOfJoining()!=null){
		        				if(employee.getDateOfJoining().after(date)){
		        					
		        					Console.log("Employee date of joining should be greater than current date" );
		        					showDialogMessage("Current Date Should be Greater Than Date Of Joining");
		        					return;
		        				}
		        			}
		        			
		        			
		        			
		        			
		        			
		        			
		        		
//		        			if(employee.getProjectName()!=null&&!employee.getProjectName().equals("")){
//		        				if(hrProject!=null&&hrProject.getCount()!=0){
//		        						if(tbprojectname.getValue()!=null&&!tbprojectname.getValue().equals("")){
//		        							if(!tbprojectname.getValue().equals(employee.getProjectName())){
//		        								showDialogMessage("Employee already added in  "+employee.getProjectName());
//		        		        				return;
//		        							}
//		        						}
//		        				}else{
//		        					showDialogMessage("Employee already added in  "+employee.getProjectName());
//			        				return;
//		        				}
//		        				
//		        			}
		        			
		        			
		        			
		        			
		        			/**
		        			 * 
		        			 */
//		        			ot1.setEmpDesignation(employee.getDesignation());
//			        		ot1.setEmpId(employee.getEmpCount());
//			        		ot1.setEmpName(employee.getFullName());
		        			
		        			overtime.setEmpDesignation(employee.getDesignation());
		        			overtime.setEmpId(employee.getEmpCount());
		        			overtime.setEmpName(employee.getFullName());
		        			/**
		        			 * 
		        			 */
			        		boolean flag=false;
			        		for(Overtime ot:overtimeTable.getDataprovider().getList()){
			        		
			        		/**
			        		 * @author Anil
			        		 * @since 03-09-2020
			        		 * if site location is already entered then same to be added in new
			        		 * raise by Rahul Tiwari for Sun facility
			        		 */
			        			
//			        			if(overtime.getName().equals(ot.getName())
//			        					&&employee.getEmpCount()==ot.getEmpId()){
//			        				showDialogMessage("Overtime already added.");
//			        				flag=true;
//			        				break;
//			        			}
			        			if(employee.getEmpCount()==ot.getEmpId()){
			        				overtime.setSiteLocation(ot.getSiteLocation());
			        				if(overtime.getName().equals(ot.getName())){
				        				showDialogMessage("Overtime already added.");
				        				flag=true;
				        				break;
			        				}
			        				
			        				/**
			        				 * @author Anil @since 20-05-2021
			        				 * If same type of OT is already is added then it should not add another type of OT
			        				 * Raised by Rahul Tiwari
			        				 */
			        				if(ot.getShortName()!=null&&overtime.getShortName().equals(ot.getShortName())&&overtime.isNormalDays()&&ot.isNormalDays()){
				        				showDialogMessage("Normal day overtime already added.");
				        				flag=true;
				        				break;
			        				}
			        				if(ot.getShortName()!=null&&overtime.getShortName().equals(ot.getShortName())&&overtime.isWeeklyOff()&&ot.isWeeklyOff()){
				        				showDialogMessage("Weekly off overtime already added.");
				        				flag=true;
				        				break;
			        				}
			        				if(ot.getShortName()!=null&&overtime.getShortName().equals(ot.getShortName())&&overtime.getHolidayType()!=null&&ot.getHolidayType()!=null
			        						&&overtime.getHolidayType().equals(ot.getHolidayType())){
				        				showDialogMessage(overtime.getHolidayType() +" overtime already added.");
				        				flag=true;
				        				break;
			        				}
			        			}
			        					
			        		
			        		}
			        		if(!flag){
			        			/**
			        			 * 
			        			 */
//			        			overtimeTable.getDataprovider().getList().add(ot1);
			        			overtimeTable.getDataprovider().getList().add(overtime);
			        			/**
			        			 * 
			        			 */
			        		}
		        		}else{
		        			List<EmployeeInfo>empList=oblEmployee.getItems();
		        			System.out.println("EMP LIST SIZE : "+empList.size());
		        			ArrayList<Overtime> otList=new ArrayList<Overtime>();
			        		for(EmployeeInfo employee:empList){
			        			Overtime overtime=new Overtime();
			        			boolean flag=false;
				        		for(Overtime ot:overtimeTable.getDataprovider().getList()){
				        			/**
				        			 * @author Anil
				        			 * @since 04-09-2020
				        			 * if site location is already entered then same to be added in new
				        			 * raise by Rahul Tiwari for Sun facility
				        			 */
//				        			if(ot1.getName().equals(ot.getName())&&employee.getEmpCount()==ot.getEmpId()){
//				        				flag=true;
////				        				break;
//				        			}
				        			
				        			if(employee.getEmpCount()==ot.getEmpId()){
				        				overtime.setSiteLocation(ot.getSiteLocation());
				        				if(ot1.getName().equals(ot.getName())){
					        				flag=true;
					        				break;
				        				}
				        				
				        				/**
				        				 * @author Anil @since 20-05-2021
				        				 * If same type of OT is already is added then it should not add another type of OT
				        				 * Raised by Rahul Tiwari
				        				 */
				        				if(ot.getShortName()!=null&&ot1.getShortName().equals(ot.getShortName())&&ot1.isNormalDays()&&ot.isNormalDays()){
//					        				showDialogMessage("Normal day overtime already added.");
					        				flag=true;
					        				break;
				        				}
				        				if(ot.getShortName()!=null&&ot1.getShortName().equals(ot.getShortName())&&ot1.isWeeklyOff()&&ot.isWeeklyOff()){
//					        				showDialogMessage("Weekly off overtime already added.");
					        				flag=true;
					        				break;
				        				}
				        				if(ot.getShortName()!=null&&ot1.getShortName().equals(ot.getShortName())&&ot1.getHolidayType()!=null&&ot.getHolidayType()!=null
				        						&&ot1.getHolidayType().equals(ot.getHolidayType())){
//					        				showDialogMessage(overtime.getHolidayType() +" overtime already added.");
					        				flag=true;
					        				break;
				        				}
				        			}
				        			
				        		}
				        		
				        		Date date=new Date();
			        			if(employee.getDateOfJoining()!=null){
			        				if(employee.getDateOfJoining().after(date)){
			        					continue;
			        				}
			        			}
			        			
//			        			overtime=ot1;
			        			
			        			overtime.setCompanyId(ot1.getCompanyId());
			        			overtime.setCount(ot1.getCount());
			        			
			        			overtime.setFlat(ot1.isFlat());
			        			overtime.setFlatRate(ot1.getFlatRate());
			        			
			        			overtime.setHolidayType(ot1.getHolidayType());
			        			overtime.setHourly(ot1.isHourly());
			        			overtime.setHourlyRate(ot1.getHourlyRate());
			        			
			        			overtime.setLeave(ot1.isLeave());
			        			overtime.setLeaveMultiplier(ot1.getLeaveMultiplier());
			        			overtime.setLeaveName(ot1.getLeaveName());
			        			
			        			overtime.setName(ot1.getName());
			        			overtime.setNormalDays(ot1.isNormalDays());
			        			
			        			overtime.setPercentOfCTC(ot1.getPercentOfCTC());
			        			
			        			overtime.setShortName(ot1.getShortName());
			        			overtime.setStatus(ot1.isStatus());
			        			
			        			overtime.setWeeklyOff(ot1.isWeeklyOff());
			        			
			        			/**
			        			 * @author Anil , Date : 11-06-2019
			        			 * updating formula oT details
			        			 */
			        			updateFormulaOt(overtime,ot1);
			        			
			        			overtime.setEmpDesignation(employee.getDesignation());
			        			overtime.setEmpId(employee.getEmpCount());
			        			overtime.setEmpName(employee.getFullName());
			        			
			        			if(hrProject!=null&&hrProject.getCount()!=0){
			        				if(employee.getProjectName()!=null&&!employee.getProjectName().equals("")
			        						&&hrProject.getProjectName().equals(employee.getProjectName())){
//			        					flag=false;
			        				}else{
//			        					flag=true;
			        					continue;
			        				}
			        			}
			        			
				        		if(!flag)
				        			otList.add(overtime);
			        		}
			        		overtimeTable.getDataprovider().getList().addAll(otList);
		        		}
		        	}
	        	}
	        	
	        	
	        
//	        }
	    }
	        
	        if(event.getSource()==cbExceptionForMonth){
				if(cbExceptionForMonth.getValue()==true){
					lbMonth.setEnabled(true);
					dbPaidDaysForExceptionalMonth.setEnabled(true);
				}else{
					lbMonth.setSelectedIndex(0);
					lbMonth.setEnabled(false);
					dbPaidDaysForExceptionalMonth.setValue(null);
					dbPaidDaysForExceptionalMonth.setEnabled(false);
				}
			}
	        
	        
		if (event.getSource().equals(btnAdd1)) {

			if (oblOtList1.getSelectedIndex() == 0) {
				showDialogMessage("Please select overtime.");
				return;
			} else {
				Overtime ot1 = oblOtList1.getSelectedItem();

				if (oblDesignation1.getSelectedIndex() != 0) {
					
					Overtime overtime = new Overtime();

					overtime.setCompanyId(ot1.getCompanyId());
					overtime.setCount(ot1.getCount());

					overtime.setFlat(ot1.isFlat());
					overtime.setFlatRate(ot1.getFlatRate());

					overtime.setHolidayType(ot1.getHolidayType());
					overtime.setHourly(ot1.isHourly());
					overtime.setHourlyRate(ot1.getHourlyRate());

					overtime.setLeave(ot1.isLeave());
					overtime.setLeaveMultiplier(ot1.getLeaveMultiplier());
					overtime.setLeaveName(ot1.getLeaveName());

					overtime.setName(ot1.getName());
					overtime.setNormalDays(ot1.isNormalDays());

					overtime.setPercentOfCTC(ot1.getPercentOfCTC());

					overtime.setShortName(ot1.getShortName());
					overtime.setStatus(ot1.isStatus());

					overtime.setWeeklyOff(ot1.isWeeklyOff());
					updateFormulaOt(overtime, ot1);

					overtime.setEmpDesignation(oblDesignation1.getValue());
					
					boolean flag = false;
					for (Overtime ot : relieverOtTable.getDataprovider().getList()) {
						if (overtime.getEmpDesignation().equals(ot.getEmpDesignation())
								&&overtime.getName().equals(ot.getName())) {
							showDialogMessage("Overtime already added.");
							flag = true;
							break;
						}
						
						/**
        				 * @author Anil @since 20-05-2021
        				 * If same type of OT is already is added then it should not add another type of OT
        				 * Raised by Rahul Tiwari
        				 */
						if (overtime.getEmpDesignation().equals(ot.getEmpDesignation())){
	        				if(overtime.getShortName().equals(ot.getShortName())&&overtime.isNormalDays()&&ot.isNormalDays()){
		        				showDialogMessage("Normal day overtime already added.");
		        				flag=true;
		        				break;
	        				}
	        				if(overtime.getShortName().equals(ot.getShortName())&&overtime.isWeeklyOff()&&ot.isWeeklyOff()){
		        				showDialogMessage("Weekly off overtime already added.");
		        				flag=true;
		        				break;
	        				}
	        				if(overtime.getShortName().equals(ot.getShortName())&&overtime.getHolidayType()!=null&&ot.getHolidayType()!=null
	        						&&overtime.getHolidayType().equals(ot.getHolidayType())){
		        				showDialogMessage(overtime.getHolidayType() +" overtime already added.");
		        				flag=true;
		        				break;
	        				}
						}
        				
					}
					if (!flag) {
						relieverOtTable.getDataprovider().getList().add(overtime);
					}
				} else {
					
					ArrayList<Overtime> otList = new ArrayList<Overtime>();
					for (Config designation: oblDesignation1.getItems()) {
						boolean flag = false;
						for (Overtime ot : relieverOtTable.getDataprovider().getList()) {
							if (ot1.getName().equals(ot.getName())&& designation.getName().equals(ot.getEmpDesignation())) {
								flag = true;
							}
							
							/**
	        				 * @author Anil @since 20-05-2021
	        				 * If same type of OT is already is added then it should not add another type of OT
	        				 * Raised by Rahul Tiwari
	        				 */
							if (designation.getName().equals(ot.getEmpDesignation())){
		        				if(ot1.getShortName().equals(ot.getShortName())&&ot1.isNormalDays()&&ot.isNormalDays()){
		//	        				showDialogMessage("Normal day overtime already added.");
			        				flag=true;
			        				break;
		        				}
		        				if(ot1.getShortName().equals(ot.getShortName())&&ot1.isWeeklyOff()&&ot.isWeeklyOff()){
		//	        				showDialogMessage("Weekly off overtime already added.");
			        				flag=true;
			        				break;
		        				}
		        				if(ot1.getShortName().equals(ot.getShortName())&&ot1.getHolidayType()!=null&&ot.getHolidayType()!=null
		        						&&ot1.getHolidayType().equals(ot.getHolidayType())){
		//	        				showDialogMessage(overtime.getHolidayType() +" overtime already added.");
			        				flag=true;
			        				break;
		        				}
							}
						}
						
						if(flag){
							continue;
						}

						Overtime overtime = new Overtime();

						overtime.setCompanyId(ot1.getCompanyId());
						overtime.setCount(ot1.getCount());

						overtime.setFlat(ot1.isFlat());
						overtime.setFlatRate(ot1.getFlatRate());

						overtime.setHolidayType(ot1.getHolidayType());
						overtime.setHourly(ot1.isHourly());
						overtime.setHourlyRate(ot1.getHourlyRate());

						overtime.setLeave(ot1.isLeave());
						overtime.setLeaveMultiplier(ot1.getLeaveMultiplier());
						overtime.setLeaveName(ot1.getLeaveName());

						overtime.setName(ot1.getName());
						overtime.setNormalDays(ot1.isNormalDays());

						overtime.setPercentOfCTC(ot1.getPercentOfCTC());

						overtime.setShortName(ot1.getShortName());
						overtime.setStatus(ot1.isStatus());

						overtime.setWeeklyOff(ot1.isWeeklyOff());

						updateFormulaOt(overtime, ot1);

						overtime.setEmpDesignation(designation.getName());
//						overtime.setEmpId(employee.getEmpCount());
//						overtime.setEmpName(employee.getFullName());

						if (!flag)
							otList.add(overtime);
					}
					relieverOtTable.getDataprovider().getList().addAll(otList);
				}
			}
		}
	 }
	 
	

	private void updateFormulaOt(Overtime overtime, Overtime ot1) {
		// TODO Auto-generated method stub
		if(ot1!=null){
			overtime.setFormulaOtFlat(ot1.isFormulaOtFlat());
			overtime.setFormulaOtHourly(ot1.isFormulaOtHourly());
			if(ot1.getOtEarningCompList()!=null){
				for(OtEarningComponent obj:ot1.getOtEarningCompList()){
					obj.setId(ot1.getId());
				}
				overtime.setOtEarningCompList(ot1.getOtEarningCompList());
			}
			
			overtime.setApplicableDaysStr(ot1.getApplicableDaysStr());
			overtime.setApplicableDays(ot1.getApplicableDays());
			overtime.setWorkingHours(ot1.getWorkingHours());
			overtime.setRate(ot1.getRate());
			overtime.setPayOtAsOther(ot1.isPayOtAsOther());
			overtime.setOtCorrespondanceName(ot1.getOtCorrespondanceName());
		}
	 }

	private void reactOnAddEmployee()
	    {
	        EmployeeInfo asset =olbteam.getSelectedItem();
	        List<EmployeeInfo> list = teamtable.getDataprovider().getList();
	        if(list != null)
	        {
	            if(list.contains(asset)==false)
	        	list.add(asset);
	           
	        }
	        olbteam.setSelectedIndex(0);
	    }

	 
	 private void reactOnAddTask()
	    {
	        String asset =olbtask.getText();
	        List<String> list = tasktable.getDataprovider().getList();
	        if(list != null)
	        {
	            if(list.contains(asset)==false)
	        	list.add(asset);
	           
	        }
	        olbtask.setText("");
	    }

	@Override
	public void createScreen() {
		payrollCycleFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("HrProject", "PayrollDayForMultiplePayrollCycle");
		
		addStateFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "AddStatefield");
		
//		siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("HrProject", "EnableSiteLocation");
//		Console.log("SITE LOCATIOONNN "+siteLocation);
		
		//Token to initialize the processlevel menus.
	    initalizeWidget();
		
	    this.processlevelBarNames=new String[]{"New","View Project Allocation"};
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		/*FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",personinfo);*/
		//FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		FormField fgroupingProjectInformation=fbuilder.setlabel("Project Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Project Name",tbprojectname);
		FormField ftbprojectname= fbuilder.setMandatory(true).setMandatoryMsg("Project Name is Mandatory").setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("* Project Code",tbProjectCode);
//		FormField ftbprojectcode= fbuilder.setMandatory(true).setMandatoryMsg("Project Code is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbprojectstatus);
		FormField fcbstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Rotational Weekly Off",cbRotationalWeeklyOff);
		FormField fcbRotationalWeeklyOff= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Allow Unplanned Holiday",cbAllowUnplannedHoliday);
		FormField fcbAllowUnplannedHoliday= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Description",tadescription);
		FormField ftadescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		FormField fgroupingteamInformation=fbuilder.setlabel("Team Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		 fbuilder = new FormFieldBuilder("Employee", olbteam);
	        FormField folbEmployee = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	        fbuilder = new FormFieldBuilder("", addteam);
	        FormField fbtnEmp = fbuilder.build();
	        fbuilder = new FormFieldBuilder("", teamtable.getTable());
	        FormField femptable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	        
	        FormField fgroupingtaskInformation=fbuilder.setlabel("Task Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			
			 fbuilder = new FormFieldBuilder("Task", olbtask);
		        FormField folbTask = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		        fbuilder = new FormFieldBuilder("", addtask);
		        FormField fbtnTask = fbuilder.build();
		        fbuilder = new FormFieldBuilder("", tasktable.getTable());
		        FormField ftasktable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		       
		        fbuilder = new FormFieldBuilder("",personInfo);
				FormField fpersonInfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
				
//				fbuilder = new FormFieldBuilder("Budgeted Hours",dbBudgetedHours);
//				FormField fdbBudgetedHours= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
				
				fbuilder = new FormFieldBuilder("From Date",dtFromDate);
				FormField fdtFromDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
				
				fbuilder = new FormFieldBuilder("To Date",dtToDate);
				FormField fdtToDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
				
		FormField fgroupingOtInfo = fbuilder.setlabel("Overtime Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Overtime", oblOtList);
		FormField foblOtList = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fbtnAdd = fbuilder.build();
		fbuilder = new FormFieldBuilder("", overtimeTable.getTable());
		FormField fovertimeTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Branch",oblBranch);
		FormField foblBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Employee Designation",oblDesignation);
		FormField flbRole= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Employee",oblEmployee);
		FormField foblEmployee= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
				
		fbuilder = new FormFieldBuilder("Supervisor",olbSuperVisorInfo);
		FormField folbSuperVisorInfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();

		fbuilder = new FormFieldBuilder("Manager",olbManagerInfo);
		FormField folbManagerInfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Branch Manager",olbBranchManagerInfo);
		FormField folbBranchManagerInfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();

		fbuilder = new FormFieldBuilder("",employeeInfo);
		FormField femployeeInfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();

		fbuilder = new FormFieldBuilder("Nature of Work",tbNatureOfWork);
		FormField ftbNatureOfWork= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * date 10.1.2019 added by komal
		 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
		 *               which one is allowed to mark attendance
		 **/
		fbuilder = new FormFieldBuilder("Sync Frequency",syncFrequency);
		FormField fsyncFrequency= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Submit Frequency",submitFrequency);
		FormField fsubmitFrequency= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Allowed Radius",allowedRadius);
		FormField fallowedRadius= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project Latitude",locationLatitude);
		FormField flocationLatitude= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project Longitude",locationLongitude);
		FormField flocationLongitude= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingAppInfo = fbuilder.setlabel("Additional Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		/** date 16.08.2019 added by komal to store minimum ot hours**/
		fbuilder = new FormFieldBuilder("Min OT Minutes",dbMiOtMinutes);
		FormField fdbMiOtMinutes= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		/**
		 * 
		 */
		FormField fgroupingPaidDaysInMonth=fbuilder.setlabel("HR Paid Days Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Paid days in month",dbPaidDaysInMonth);
		FormField fdbPaidDaysInMonth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Is Exception For Specific Month",cbExceptionForMonth);
		FormField fcbExceptionForMonth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Specific Month",lbMonth);
		FormField flbMonth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Paid days in specific monnth",dbPaidDaysForExceptionalMonth);
		FormField fdbPaidDaysForExceptionalMonth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Component name for extra day",tbExtraDayCompName);
		FormField ftbExtraDayCompName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
				fbuilder = new FormFieldBuilder("Customer Branch",oblCustomerBranch);
		FormField flbCustomerBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Payroll (Start Day)",ibPayrollStartDay);
		FormField fibPayrollStartDay = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setVisible(payrollCycleFlag).build();

		fbuilder = new FormFieldBuilder("Fixed Payroll Days",fixedPayrollDays);
		FormField ffixedPayrollDays = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		
		fbuilder = new FormFieldBuilder("State",oblState);
		FormField foblState = fbuilder.setMandatory(false).setMandatoryMsg("State is mandatory.").setRowSpan(0).setColSpan(0).setVisible(addStateFlag).build();

		
		
		FormField fGroupingRelieversOt = fbuilder
				.setlabel("Relievers Overtime Details").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Designation", oblDesignation1);
		FormField foblDesignation1 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Overtime", oblOtList1);
		FormField foblOtList1 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", btnAdd1);
		FormField fbtnAdd1 = fbuilder.build();
		fbuilder = new FormFieldBuilder("", relieverOtTable.getTable());
		FormField frelieverOtTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder=new FormFieldBuilder("",btnClearOT);
		FormField fbtnClearOt=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder=new FormFieldBuilder("",btnClear);
		FormField fbtnClear=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("PF Capping Amount",dbPfCappingAmt);
		FormField fdbPfCappingAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		
		
  FormField[][] formfield = {   
		  
  {fgroupingProjectInformation},
  
  {fpersonInfo},
//  {fdbBudgetedHours,fdtFromDate,fdtToDate},
//  
//  {ftbprojectname,ftbprojectcode,foblBranch,flbCustomerBranch},
//  {folbSuperVisorInfo , folbManagerInfo ,folbBranchManagerInfo,ftbNatureOfWork},/** date 18.8.2018 added by komal**/
//  {ftadescription,fcbstatus},
  
  /*** Date 22-07-2020 Budget hours and Project code removed as per the requirments****/
  {fdtFromDate,fdtToDate,ftbprojectname,flbCustomerBranch},
  {foblBranch,folbSuperVisorInfo,folbManagerInfo,folbBranchManagerInfo},
  {ftbNatureOfWork , fibPayrollStartDay,foblState,fcbstatus,fcbRotationalWeeklyOff},/** date 18.8.2018 added by komal**/
  {fdbPfCappingAmt,ffixedPayrollDays,fcbAllowUnplannedHoliday},
  {ftadescription},
 /* {fgroupingteamInformation},
  {folbEmployee,fbtnEmp},
  {femptable},
  {fgroupingtaskInformation},
  {folbTask,fbtnTask},
  {ftasktable}*/
  {fgroupingAppInfo},
  {fallowedRadius , flocationLatitude , flocationLongitude }, /** date 12.1.2019 added by komal to add details related to attendance app(Nitin sir's requirement)**/
  {fsyncFrequency , fsubmitFrequency ,fdbMiOtMinutes},
  
  {fGroupingRelieversOt},
  {foblDesignation1,foblOtList1,fbtnAdd1,fbtnClearOt}, /** Date - 17/02/2021 Add clear button as per requiremnet by Rahul Tiwari. **/
  {frelieverOtTable},
  
  
  {fgroupingOtInfo},
  {flbRole,foblEmployee,foblOtList,fbtnAdd},
  {fbtnClear},
  //{femployeeInfo ,foblOtList,fbtnAdd},/** date 18.8.2018 changed by komal**/
  {fovertimeTable},
  
//  {fgroupingPaidDaysInMonth},
//  {fdbPaidDaysInMonth,fcbExceptionForMonth,flbMonth,fdbPaidDaysForExceptionalMonth},
//  {ftbExtraDayCompName}
  };

  	this.fields=formfield;		
   }

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(HrProject model) {

		if (tbprojectname.getValue() != null)
			model.setProjectName(tbprojectname.getValue());
//		if (tbProjectCode.getValue() != null)
//			model.setProjectCode(tbProjectCode.getValue());
		if (cbprojectstatus.getValue() != null)
			model.setStatus(cbprojectstatus.getValue());
		if(cbRotationalWeeklyOff.getValue()!=null)
			model.setRotationalWeeklyOff(cbRotationalWeeklyOff.getValue());
		if (tadescription.getValue() != null)
			model.setDescription(tadescription.getValue());
		if (teamtable.getValue() != null)
			model.setTeamtable(teamtable.getValue());
		if (tasktable.getValue() != null)
			model.setTasktable(tasktable.getValue());
		
		if(personInfo.getValue()!=null){
			model.setPersoninfo(personInfo.getValue());
		}
//		if(dbBudgetedHours.getValue()!=null){
//			model.setBudgetedHours(dbBudgetedHours.getValue());
//		}
		if(dtFromDate.getValue()!=null){
			model.setFromDate(dtFromDate.getValue());
		}
		if(dtToDate.getValue()!=null){
			model.setToDate(dtToDate.getValue());
		}
		
		if(overtimeTable.getValue()!=null){
//			model.setOtList(overtimeTable.getValue());
			model.setOtList(getupdatedlist(overtimeTable.getValue()));
			if(model.getOtList().size()>150) {
				model.setSeperateEntityFlag(true);
			}
			if(model.isSeperateEntityFlag() && deletedOtIdlist!=null && deletedOtIdlist.size()>0){
				model.setDeletedOtIdlist(deletedOtIdlist);
			}
		}
		/** date 18.8.2018 added by komal **/
		if(olbSuperVisorInfo.getSelectedIndex()!=0){
			model.setSupervisor(olbSuperVisorInfo.getValue(olbSuperVisorInfo.getSelectedIndex()));
		}else{
			model.setSupervisor("");
		}
		if(olbManagerInfo.getSelectedIndex()!=0){
			model.setManager(olbManagerInfo.getValue(olbManagerInfo.getSelectedIndex()));
		}else{
			model.setManager("");
		}
		if(olbBranchManagerInfo.getSelectedIndex()!=0){
			model.setBranchManager(olbBranchManagerInfo.getValue(olbBranchManagerInfo.getSelectedIndex()));
		}else{
			model.setBranchManager("");
		}
		/**
		 * Date : 11-10-2018 By ANIL
		 */
		if(oblBranch.getValue()!=null){
			model.setBranch(oblBranch.getValue());
		}
		
		if(tbNatureOfWork.getValue()!=null){
			model.setNatureOfWork(tbNatureOfWork.getValue());
		}
		/**
		 * date 10.1.2019 added by komal
		 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
		 *               which one is allowed to mark attendance
		 **/
		if(submitFrequency.getValue() != null){
			model.setSubmitFrequency(submitFrequency.getValue());
		}
		if(syncFrequency.getValue() != null){
			model.setSyncFrequency(syncFrequency.getValue());
		}
		if(allowedRadius.getValue() != null){
			model.setAllowedRadius(allowedRadius.getValue());
		}
		if(locationLatitude.getValue() != null){
			model.setLocationLatitude(locationLatitude.getValue());
		}
		if(locationLongitude.getValue() != null){
			model.setLocationLongitude(locationLongitude.getValue());
		}
		if(dbMiOtMinutes.getValue() != null){
			model.setMiOtMinutes(dbMiOtMinutes.getValue());
		}
		
		if(dbPaidDaysInMonth.getValue()!=null){
			model.setPaidDaysInMonth(dbPaidDaysInMonth.getValue());
		}else{
			model.setPaidDaysInMonth(0);
		}
		model.setExceptionForMonth(cbExceptionForMonth.getValue());
		if(lbMonth.getSelectedIndex()!=0){
			model.setSpecificMonth(lbMonth.getValue(lbMonth.getSelectedIndex()));
		}else{
			model.setSpecificMonth("");
		}
		if(dbPaidDaysForExceptionalMonth.getValue()!=null){
			model.setPaidDaysForExceptionalMonth(dbPaidDaysForExceptionalMonth.getValue());
		}else{
			model.setPaidDaysForExceptionalMonth(0);
		}
		
		if(tbExtraDayCompName.getValue()!=null){
			model.setExtraDayCompName(tbExtraDayCompName.getValue());
		}
			if(oblCustomerBranch.getValue()!=null){
			model.setCustomerBranch(oblCustomerBranch.getValue());
		}else{
			model.setCustomerBranch("");
		}
		
		if(ibPayrollStartDay.getValue()!=null){
			model.setPayrollStartDay(ibPayrollStartDay.getValue());
		}else{
			model.setPayrollStartDay(null);
		}	
		
		model.setState(oblState.getValue());
		
		if(relieverOtTable.getValue()!=null){
			model.setRelieversOtList(relieverOtTable.getValue());
		}
		
		if(dbPfCappingAmt.getValue()!=null){
			model.setPfCappingAmt(dbPfCappingAmt.getValue());
		}else{
			model.setPfCappingAmt(0);
		}
		
		
		if(fixedPayrollDays.getValue()!=null){
			model.setFixedPayrollDays(fixedPayrollDays.getValue());
		}else{
			model.setFixedPayrollDays(null);
		}	
		
		if(cbAllowUnplannedHoliday.getValue()!=null)
			model.setAllowUnplannedHoliday(cbAllowUnplannedHoliday.getValue());
		
		manageProjectDropDown(model);
		
		presenter.setModel(model);
	}

	
	
	

	

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(HrProject view) {
		hrProject=view;
//		AppUtility.MakeLiveConfig(oblDesignation,Screen.EMPLOYEEDESIGNATION);
		
		if (view.getProjectName() != null)
			tbprojectname.setValue(view.getProjectName());
		
		if(view.getCncProjectCount()==0){
			initializeEmployeeDropDown(oblEmployee, empProjectList, view.getProjectName(),false);
		}else
			initializeEmployeeDropDown(oblEmployee, empProjectList, view.getProjectName(),true);//Ashwini Patil Date:12-02-2024
		
		
//		if (view.getProjectCode() != null)
//			tbProjectCode.setValue(view.getProjectCode());
		if (view.getStatus() != null)
			cbprojectstatus.setValue(view.getStatus());
		cbRotationalWeeklyOff.setValue(view.isRotationalWeeklyOff());
		if (view.getDescription() != null)
			tadescription.setValue(view.getDescription());
		if (view.getTeamtable() != null)
			teamtable.setValue(view.getTeamtable());
		if (view.getTasktable() != null)
			tasktable.setValue(view.getTasktable());
		
		if(view.getPersoninfo()!=null){
			personInfo.setValue(view.getPersoninfo());
		}
//		if(view.getBudgetedHours()!=0){
//			dbBudgetedHours.setValue(view.getBudgetedHours());
//		}
		if(view.getFromDate()!=null){
			dtFromDate.setValue(view.getFromDate());
		}
		if(view.getToDate()!=null){
			dtToDate.setValue(view.getToDate());
		}
		
//		if(view.getOtList()!=null){
//			overtimeTable.setValue(view.getOtList());
//		}
		
		if(view.getOtList()==null || view.getOtList().size()==0){
			final CommonServiceAsync commonserviceasync = GWT.create(CommonService.class);
			final GWTCGlassPanel gpanel=new GWTCGlassPanel();
			gpanel.show();
			commonserviceasync.getHrProjectOvertimelist(view.getCount(), view.getCompanyId(), new AsyncCallback<ArrayList<HrProjectOvertime>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					gpanel.hide();
				}

				@Override
				public void onSuccess(ArrayList<HrProjectOvertime> result) {
					// TODO Auto-generated method stub
					Console.log("OT List "+result.size());
					ArrayList<Overtime> list = new ArrayList<Overtime>();
					if(result.size()!=0){
						for(HrProjectOvertime ot : result){
//							Overtime overtime = ot;
							list.add(ot);
						}
					}
					Console.log("updated OT List "+list.size());
					overtimeTable.setValue(list);
					gpanel.hide();
				}
			});

		}
		else{
			if(view.getOtList()!=null){
				overtimeTable.setValue(view.getOtList());
			}
		}	
		/** date 18.8.2018 added by komal **/
		if(view.getSupervisor()!=null){
			olbSuperVisorInfo.setValue(view.getSupervisor());
		}
		if(view.getManager()!=null){
			olbManagerInfo.setValue(view.getManager());
		}
		if(view.getBranchManager()!=null){
			olbBranchManagerInfo.setValue(view.getBranchManager());
		}
		
		/**
		 * Date : 11-10-2018 BY ANIL
		 */
		if(view.getBranch()!=null&&!view.getBranch().equals("")){
			oblBranch.setValue(view.getBranch());
			updateEmployeeDropDown();
		}
		
		if(view.getNatureOfWork()!=null){
			tbNatureOfWork.setValue(view.getNatureOfWork());
		}
		/**
		 * date 10.1.2019 added by komal
		 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
		 *               which one is allowed to mark attendance
		 **/
		syncFrequency.setValue(view.getSyncFrequency());
		submitFrequency.setValue(view.getSubmitFrequency());
		allowedRadius.setValue(view.getAllowedRadius());
		locationLatitude.setValue(view.getLocationLatitude());
		locationLongitude.setValue(view.getLocationLongitude());
		dbMiOtMinutes.setValue(view.getMiOtMinutes());
			if(view.getCustomerBranch()!=null&&!view.getCustomerBranch().equals("")&&view.getPersoninfo()!=null&&view.getPersoninfo().getCount()!=0){
			loadCustomerBranch(view.getPersoninfo().getCount(), view.getCustomerBranch());
			oblCustomerBranch.setValue(view.getCustomerBranch());
		}
		if(view.getPaidDaysInMonth()!=0){
			dbPaidDaysInMonth.setValue(view.getPaidDaysInMonth());
		}
		cbExceptionForMonth.setValue(view.isExceptionForMonth());
		if(view.getSpecificMonth()!=null){
			for(int i=1;i<lbMonth.getItemCount();i++){
				if(lbMonth.getItemText(i).equals(view.getSpecificMonth())){
					lbMonth.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getPaidDaysForExceptionalMonth()!=0){
			dbPaidDaysForExceptionalMonth.setValue(view.getPaidDaysForExceptionalMonth());
		}
		
		if(view.getExtraDayCompName()!=null){
			tbExtraDayCompName.setValue(view.getExtraDayCompName());
		}
		
		if(view.getPayrollStartDay()!=null){
			ibPayrollStartDay.setValue(view.getPayrollStartDay());
		}
		
		if(view.getFixedPayrollDays()!=null){
			fixedPayrollDays.setValue(view.getFixedPayrollDays());
		}
		if(view.getState()!=null){
			oblState.setValue(view.getState());
		}
		
		if(view.getRelieversOtList()!=null){
			relieverOtTable.setValue(view.getRelieversOtList());
		}
		cbAllowUnplannedHoliday.setValue(view.isAllowUnplannedHoliday());
		dbPfCappingAmt.setValue(dbPfCappingAmt.getValue());
		
		// added by Priyanka
		if(view.getCncProjectCount()!=0){
			Console.log("Inside checking employee Degignation");
			checkEmployeeDesiganation(view.getCncProjectCount());
		}
		
		presenter.setModel(view);

	}
	private void checkEmployeeDesiganation(int cncProjectId) {
		
		Console.log("Inside Method of Check emp designation "+cncProjectId);
		MyQuerry querry=new MyQuerry();
		
		Filter filter =null;
		Vector<Filter> vecList=new Vector<Filter>();
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		vecList.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(cncProjectId);
		vecList.add(filter);
		
		querry.setFilters(vecList);
		querry.setQuerryObject(new ProjectAllocation());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Console.log("Failure...");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				Console.log("Success...");
				HashSet<String> desgHs = new  HashSet<String>();
				if(result!=null&&result.size()!=0){
					Console.log("Success...1");
					ProjectAllocation cnc=(ProjectAllocation) result.get(0);
					
					boolean flag = false;
					
					if(cnc.getEmployeeProjectAllocationList()!=null&&cnc.getEmployeeProjectAllocationList().size()!=0){
						Console.log("Success...2");
						for (EmployeeProjectAllocation sd : cnc.getEmployeeProjectAllocationList()) {
							desgHs.add(sd.getEmployeeRole());
							
							/**
							 * @author Anil @since 21-05-2021
							 * Filtering designation and employee drop down by Project allocation designation
							 * Raised by Rahul Tiwari
							 */
							EmployeeInfo info=updateEmployeeDesignation(sd);
							
							flag = true;
						}	
					}
					else{
						flag = false;
						getEmployeeProjectDataFromSeprateEntity(cnc);
					}
					
					if(flag){
						processData(cnc,desgHs);
					}
//					for(EmployeeInfo info:empProjectList){
//						if(cnc.getProjectName()!=null&&cnc.getProjectName().equals(info.getProjectName())){
//							desgHs.add(info.getDesignation());
//							Console.log(info.getEmpCount()+" / "+info.getFullName()+" / "+info.getDesignation());
//						}
//					}
//					
//					/**
//					 * @author Anil @since 24-05-2021
//					 */
////					List<Config> ogDesgList=oblDesignation.getItems();
//					List<Config> ogDesgList=new ArrayList<Config>();
//					if(LoginPresenter.globalConfig!=null&&LoginPresenter.globalConfig.size()!=0){
//						Console.log("Success...2a..");
//						for(Config obj:LoginPresenter.globalConfig){
//							if(obj.getType()==13&&obj.isStatus()){
//								ogDesgList.add(obj);
//							}
//						}
//					}else{
//						Console.log("Success...2b..");
//						ogDesgList=oblDesignation.getItems();
//					}
//					
//					List<Config> updatedDesgList=new ArrayList<Config>();
//					
//					if(desgHs!=null&&desgHs.size()!=0&&ogDesgList!=null&&ogDesgList.size()!=0){
//						Console.log("Success...3.."+desgHs.size()+" | "+ogDesgList.size());
//						for(String desg:desgHs){
//							for(Config ogDesg:ogDesgList){
//								if(ogDesg.getName().equals(desg)){
//									updatedDesgList.add(ogDesg);
//								}
//							}
//						}
//					}
//					
//					if(updatedDesgList!=null&&updatedDesgList.size()!=0){
//						Console.log("Success...4.. "+updatedDesgList.size());
//						oblDesignation.setListItems(updatedDesgList);
//						
//						initializeEmployeeDropDown(oblEmployee, empProjectList, cnc.getProjectName(),true);
//					}
				}
			}
		});
	}

	protected void getEmployeeProjectDataFromSeprateEntity(final ProjectAllocation cnc) {
		
		MyQuerry querry=new MyQuerry();
		
		Filter filter =null;
		Vector<Filter> vecList=new Vector<Filter>();
		
		filter=new Filter();
		filter.setQuerryString("projectAllocationId");
		filter.setIntValue(cnc.getCount());
		vecList.add(filter);
		
		querry.setFilters(vecList);
		querry.setQuerryObject(new EmployeeProjectAllocation());
		
		showWaitSymbol();
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				hideWaitSymbol();

				ArrayList<EmployeeProjectAllocation> list = new ArrayList<EmployeeProjectAllocation>();
				for(SuperModel model : result){
					EmployeeProjectAllocation empProject = (EmployeeProjectAllocation) model;
					list.add(empProject);
				}
				Console.log("list size "+list.size());

				if(list.size()>0){
					
					HashSet<String> desgHs = new  HashSet<String>();

					for (EmployeeProjectAllocation sd : list) {
						desgHs.add(sd.getEmployeeRole());
						
						/**
						 * @author Anil @since 21-05-2021
						 * Filtering designation and employee drop down by Project allocation designation
						 * Raised by Rahul Tiwari
						 */
						EmployeeInfo info=updateEmployeeDesignation(sd);
						
					}
					
					processData(cnc,desgHs);

				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				hideWaitSymbol();
			}
		});
		
	}

	protected void processData(ProjectAllocation cnc, HashSet<String> desgHs) {

		for(EmployeeInfo info:empProjectList){
			if(cnc.getProjectName()!=null&&cnc.getProjectName().equals(info.getProjectName())){
				desgHs.add(info.getDesignation());
				Console.log(info.getEmpCount()+" / "+info.getFullName()+" / "+info.getDesignation());
			}
		}
		
		/**
		 * @author Anil @since 24-05-2021
		 */
//		List<Config> ogDesgList=oblDesignation.getItems();
		List<Config> ogDesgList=new ArrayList<Config>();
		if(LoginPresenter.globalConfig!=null&&LoginPresenter.globalConfig.size()!=0){
			Console.log("Success...2a..");
			for(Config obj:LoginPresenter.globalConfig){
				if(obj.getType()==13&&obj.isStatus()){
					ogDesgList.add(obj);
				}
			}
		}else{
			Console.log("Success...2b..");
			ogDesgList=oblDesignation.getItems();
		}
		
		List<Config> updatedDesgList=new ArrayList<Config>();
		
		if(desgHs!=null&&desgHs.size()!=0&&ogDesgList!=null&&ogDesgList.size()!=0){
			Console.log("Success...3.."+desgHs.size()+" | "+ogDesgList.size());
			for(String desg:desgHs){
				for(Config ogDesg:ogDesgList){
					if(ogDesg.getName().equals(desg)){
						updatedDesgList.add(ogDesg);
					}
				}
			}
		}
		
		if(updatedDesgList!=null&&updatedDesgList.size()!=0){
			Console.log("Success...4.. "+updatedDesgList.size());
			oblDesignation.setListItems(updatedDesgList);
			
			initializeEmployeeDropDown(oblEmployee, empProjectList, cnc.getProjectName(),true);
		}
		
	}

	/**
	 * @author Anil @since 21-05-2021
	 * here we updating employee designation from project allocation
	 * Raised by Rahul Tiwari for FM
	 */
	protected EmployeeInfo updateEmployeeDesignation(EmployeeProjectAllocation sd) {
		// TODO Auto-generated method stub
		if(sd!=null){
			int employeeId=0;
			if(sd.getEmployeeId()!=null&&!sd.getEmployeeId().equals("")){
				try{
					employeeId=Integer.parseInt(sd.getEmployeeId());
				}catch(Exception e){
					
				}
				Console.log("Employee Proj "+employeeId+" / "+sd.getEmployeeName());
			}
			if(empProjectList!=null){
				Console.log("Employee Proj not null ");
				for(EmployeeInfo info:empProjectList){
					if(employeeId==info.getEmpCount()){
						Console.log("DESG "+info.getDesignation()+" / "+sd.getEmployeeRole());
						if(sd.getEmployeeRole()!=null&&!sd.getEmployeeRole().equals("")){
							info.setDesignation(sd.getEmployeeRole());
						}
						return info;
					}
				}
			}
		}
		return null;
	}

	public void loadCustomerBranch(int custId,final String custBranch) {
		customerBranchList=new ArrayList<CustomerBranchDetails>();
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(custId);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				oblCustomerBranch.clear();
				oblCustomerBranch.addItem("--SELECT--");
				showDialogMessage("failed." + caught);
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<CustomerBranchDetails> custBranchList=new ArrayList<CustomerBranchDetails>();
				if(result.size()!=0){
					for(SuperModel model:result){
						CustomerBranchDetails obj=(CustomerBranchDetails) model;
						custBranchList.add(obj);
						customerBranchList.add(obj);
					}
					Console.log("custBranchList Size11 "+custBranchList.size());
					
					Console.log("customerBranchList Size222 "+customerBranchList.size());
				}
				
				if(custBranchList.size()!=0){
					oblCustomerBranch.setListItems(custBranchList);
					if(custBranch!=null){
						oblCustomerBranch.setValue(custBranch);
					}else{
						oblCustomerBranch.setEnabled(true);
					}
				}else{
					oblCustomerBranch.addItem("--SELECT--");
				}
			}
		});
	}
	/**
	 * Toggles the app header bar menus as per screen state
	 */
	
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")
						|| text.contains("Search")
						|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")
						|| text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Edit") || text.contains("Discard")
						|| text.contains("Search") 
						|| text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		AuthorizationHelper.setAsPerAuthorization(Screen.HRPROJECT,LoginPresenter.currentModule.trim());
	}

	 public boolean validate()
	    {
	       
		 boolean superValidate = super.validate();
	        
	        if(superValidate==false)
	        	return false;
	       /* if(teamtable.getDataprovider().getList().size() == 0)
	        {
	            showDialogMessage("Please Select at least one Employee !");
	            return false;
	            
	            
	        }
	        
	        if(tasktable.getDataprovider().getList().size() == 0)
	        {
	            showDialogMessage("Please Add at least one Task !");
	            return false;
	            
	            
	        }*/
	        /** date 9.5.2019 added by komal for shift mandatory validation **/
	        if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("HrProject", "MakeShiftMandatory") ){
	        	for(Overtime overtime :overtimeTable.getDataprovider().getList()){
	        		if(overtime.getShift() == null || overtime.getShift().equals("") || overtime.getShift().equalsIgnoreCase("--SELECT--")){
	        			 showDialogMessage("Please assign shift to all employeees.");
	        			return false;
	        		}
	        	}
	        }
	        
			return true;
	        
	    }

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		/**
		 * @author Anil
		 * @since 21-08-2020
		 */
		if(hrProject!=null){
			System.out.println("projectAllocationObj 1");
			if(hrProject.getPersoninfo()!=null){
				overtimeTable.setCustId(hrProject.getPersoninfo().getCount());
				System.out.println("projectAllocationObj 2 "+overtimeTable.getCustId());
			}
			if(hrProject.getState()!=null){
				overtimeTable.setState(hrProject.getState());
				System.out.println("projectAllocationObj 3 "+overtimeTable.getState());
			}
		}
		overtimeTable.setEnable(state);
		relieverOtTable.setEnable(state);
//		oblState.setEnabled(false);
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		
		deletedOtIdlist = new ArrayList<Integer>();
		
		/**
		 * Date : 11-10-2018 BY ANIL
		 */
		if(oblBranch.getValue()!=null){
			oblBranch.setEnabled(false);
		}
			processLevelBar.setVisibleFalse(false);
		if(cbExceptionForMonth.getValue()==true){
			lbMonth.setEnabled(true);
			dbPaidDaysForExceptionalMonth.setEnabled(true);
		}else{
			lbMonth.setEnabled(false);
			dbPaidDaysForExceptionalMonth.setEnabled(false);
		}
		
		if(hrProject!=null){
			if(hrProject.getCncProjectCount()!=0){
				ibPayrollStartDay.setEnabled(false);
				oblState.setEnabled(false);
			}
		}
	}

	@Override
	public void clear() {
		
		super.clear();
		cbprojectstatus.setValue(true);
		cbRotationalWeeklyOff.setValue(false);
		cbAllowUnplannedHoliday.setValue(false);
		overtimeTable.connectToLocal();
		hrProject=null;
		
		cbExceptionForMonth.setValue(false);
		dbPaidDaysForExceptionalMonth.setEnabled(false);
		lbMonth.setEnabled(false);
		
		relieverOtTable.connectToLocal();
	}

	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource()==oblDesignation){
			updateEmployeeDropDown();
		}
		if(event.getSource()==oblBranch){
			updateEmployeeDropDown();
		}
	}

	private void updateEmployeeDropDown() {
		
		boolean isCncProject=false;
		
		if(hrProject!=null){
			if(hrProject.getCncProjectCount()!=0){
				isCncProject=true;
			}
		}
		
		String projectName="";
		if(tbprojectname.getValue()!=null){
			projectName=tbprojectname.getValue();
		}
		String designation="";
		if(oblDesignation.getSelectedIndex()!=0){
			designation=oblDesignation.getValue();
		}
		String branch="";
		if(oblBranch.getSelectedIndex()!=0){
			branch=oblBranch.getValue();
		}
		ArrayList<EmployeeInfo> listEmployee = new  ArrayList<EmployeeInfo>();
		
		if(isCncProject){
			if(!projectName.equals("")){
				for(EmployeeInfo emp:empProjectList){
					if(emp.getProjectName()!=null&&!emp.getProjectName().equals("")&&emp.getProjectName().equals(projectName)){
						listEmployee.add(emp);
					}
				}
			}
//			else{
//				for(EmployeeInfo emp:empProjectList){
//					if(emp.getProjectName()==null||emp.getProjectName().equals("")){
//						listEmployee.add(emp);
//					}
//				}
//			}
		}else{
			if(!projectName.equals("")){
				for(EmployeeInfo emp:empProjectList){
					if(emp.getProjectName().equals(projectName)||emp.getProjectName()==null||emp.getProjectName().equals("")){
						listEmployee.add(emp);
					}
				}
			}
			else{
				for(EmployeeInfo emp:empProjectList){
					if(emp.getProjectName()==null||emp.getProjectName().equals("")){
						listEmployee.add(emp);
					}
				}
			}
		}
		
//		if(!projectName.equals("")){
//			for(EmployeeInfo emp:empProjectList){
//				if(emp.getProjectName().equals(projectName)||emp.getProjectName()==null||emp.getProjectName().equals("")){
//					listEmployee.add(emp);
//				}
//			}
//		}
//		else{
//			for(EmployeeInfo emp:empProjectList){
//				if(emp.getProjectName()==null||emp.getProjectName().equals("")){
//					listEmployee.add(emp);
//				}
//			}
////			listEmployee.addAll(empProjectList);
//		}
		
		
		
//		ArrayList<Employee> listEmployee = LoginPresenter.globalEmployee;
		ArrayList<EmployeeInfo> aciveEmployeeList = new ArrayList<EmployeeInfo>();
		Comparator<EmployeeInfo> employeeArrayComparator = new Comparator<EmployeeInfo>() {
			public int compare(EmployeeInfo c1, EmployeeInfo c2) {
				String employeeName1 = c1.getFullName();
				String employeeName2 = c2.getFullName();

				return employeeName1.compareTo(employeeName2);
			}
		};
		Collections.sort(listEmployee, employeeArrayComparator);
		
         
		for (int i = 0; i < listEmployee.size(); i++) {
//			
	
			if (listEmployee.get(i).isStatus()) {
				if(!designation.equals("")&&!branch.equals("")){
					if(listEmployee.get(i).getDesignation().equals(designation)&&listEmployee.get(i).getBranch().equals(branch)){
						aciveEmployeeList.add(listEmployee.get(i));
					}
				}else if(!designation.equals("")&&branch.equals("")){
					if(listEmployee.get(i).getDesignation().equals(designation)){
						aciveEmployeeList.add(listEmployee.get(i));
					}
				}else if(designation.equals("")&&!branch.equals("")){
					if(listEmployee.get(i).getBranch().equals(branch)){
						aciveEmployeeList.add(listEmployee.get(i));
					}
				}else{
					aciveEmployeeList.add(listEmployee.get(i));
					
				}
			}
		}
		oblEmployee.getItems().clear();
		oblEmployee.getItems().addAll(aciveEmployeeList);
		
		oblEmployee.setListItems(oblEmployee.getItems());
		/**
		 * @author Anil
		 * @since 17-12-2020
		 * updating employee name by adding employee id in it separated by '/'
		 */
		ArrayList<String> empNameList=new ArrayList<String>();
		for(int i=0;i<oblEmployee.getItemCount();i++){
			if(empNameList.contains(oblEmployee.getItemText(i))){
				Console.log("EMPLOYEE NAME REPEATED "+oblEmployee.getItemText(i));
				EmployeeInfo info=oblEmployee.getItems().get(i-1);
				oblEmployee.setItemText(i, info.getFullName()+" / "+info.getEmpCount());
				Console.log("EMPLOYEE NAME REPEATED "+oblEmployee.getItemText(i));
			}
			empNameList.add(oblEmployee.getItemText(i));
		}
		
	}

	public PersonInfoComposite getPersonInfo() {
		return personInfo;
	}

	public void setPersonInfo(PersonInfoComposite personInfo) {
		this.personInfo = personInfo;
	}

	public ObjectListBox<CustomerBranchDetails> getOblCustomerBranch() {
		return oblCustomerBranch;
	}

	public void setOblCustomerBranch(
			ObjectListBox<CustomerBranchDetails> oblCustomerBranch) {
		this.oblCustomerBranch = oblCustomerBranch;
	}

	/**
	 * @author Vijay Chougule
	 * Des :- when new project created or updated existing project then updating its HRproject global list
	 */
	private void manageProjectDropDown(HrProject projectModel) {

		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalHRProject.add(projectModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalHRProject.size();i++)
			{
				if(LoginPresenter.globalHRProject.get(i).getId().equals(projectModel.getId()))
				{
					LoginPresenter.globalHRProject.add(projectModel);
					LoginPresenter.globalHRProject.remove(i);
				}
			}
		}
	
	}
	/**
	 * ends here
	 */
	
	
	 private void reactonOvertimeNonMandatory() {

 		if(oblEmployee.getSelectedIndex()!=0){
     		Overtime overtime=new Overtime();
 			EmployeeInfo employee=oblEmployee.getSelectedItem();
           /**Date 21-8-2019 by Amol show alert if employee joining date is greater than or equal to 
              current (todays) date,and do not allow to add    while add a employee in project **/	        			
 			Date date=new Date();
 			if(employee.getDateOfJoining()!=null){
 				if(employee.getDateOfJoining().after(date)){
 					
 					Console.log("Employee date of joining should be greater than current date" );
 					showDialogMessage("Current Date Should be Greater Than Date Of Joining");
 					return;
 				}
 			}
 			overtime.setEmpDesignation(employee.getDesignation());
 			overtime.setEmpId(employee.getEmpCount());
 			overtime.setEmpName(employee.getFullName());
 			
 			/**
 			 * @author Anil
 			 * @since 15-02-2022
 			 * In case process configuration is active but overtime is selected then ot shoulb be added
 			 * raised by Nitin Sir and Poonam
 			 */
 			try{
	 			if(oblOtList.getSelectedIndex()!=0){
		 			Overtime ot1=oblOtList.getSelectedItem();
					overtime.setCompanyId(ot1.getCompanyId());
					overtime.setCount(ot1.getCount());
					overtime.setFlat(ot1.isFlat());
					overtime.setFlatRate(ot1.getFlatRate());
					overtime.setHolidayType(ot1.getHolidayType());
					overtime.setHourly(ot1.isHourly());
					overtime.setHourlyRate(ot1.getHourlyRate());
					overtime.setLeave(ot1.isLeave());
					overtime.setLeaveMultiplier(ot1.getLeaveMultiplier());
					overtime.setLeaveName(ot1.getLeaveName());
					overtime.setName(ot1.getName());
					overtime.setNormalDays(ot1.isNormalDays());
					overtime.setPercentOfCTC(ot1.getPercentOfCTC());
					overtime.setShortName(ot1.getShortName());
					overtime.setStatus(ot1.isStatus());
					overtime.setWeeklyOff(ot1.isWeeklyOff());
					updateFormulaOt(overtime,ot1);
	 			}
 			}catch(Exception e){
 				
 			}
 			
     		overtimeTable.getDataprovider().getList().add(overtime);
     		
 		}else{
 			List<EmployeeInfo>empList=oblEmployee.getItems();
 			System.out.println("EMP LIST SIZE : "+empList.size());
 			ArrayList<Overtime> otList=new ArrayList<Overtime>();
     		for(EmployeeInfo employee:empList){
     			Overtime overtime=new Overtime();
	        		
	        		Date date=new Date();
     			if(employee.getDateOfJoining()!=null){
     				if(employee.getDateOfJoining().after(date)){
     					continue;
     				}
     			}
     			
     			
     			overtime.setEmpDesignation(employee.getDesignation());
     			overtime.setEmpId(employee.getEmpCount());
     			overtime.setEmpName(employee.getFullName());
     			
     			if(hrProject!=null&&hrProject.getCount()!=0){
     				if(employee.getProjectName()!=null&&!employee.getProjectName().equals("")
     						&&hrProject.getProjectName().equals(employee.getProjectName())){
//     					flag=false;
     				}else{
//     					flag=true;
     					continue;
     				}
     			}
     			
     			/**
     			 * @author Anil
     			 * @since 15-02-2022
     			 * In case process configuration is active but overtime is selected then ot shoulb be added
     			 * raised by Nitin Sir and Poonam
     			 */
     			try{
    	 			if(oblOtList.getSelectedIndex()!=0){
    		 			Overtime ot1=oblOtList.getSelectedItem();
    					overtime.setCompanyId(ot1.getCompanyId());
    					overtime.setCount(ot1.getCount());
    					overtime.setFlat(ot1.isFlat());
    					overtime.setFlatRate(ot1.getFlatRate());
    					overtime.setHolidayType(ot1.getHolidayType());
    					overtime.setHourly(ot1.isHourly());
    					overtime.setHourlyRate(ot1.getHourlyRate());
    					overtime.setLeave(ot1.isLeave());
    					overtime.setLeaveMultiplier(ot1.getLeaveMultiplier());
    					overtime.setLeaveName(ot1.getLeaveName());
    					overtime.setName(ot1.getName());
    					overtime.setNormalDays(ot1.isNormalDays());
    					overtime.setPercentOfCTC(ot1.getPercentOfCTC());
    					overtime.setShortName(ot1.getShortName());
    					overtime.setStatus(ot1.isStatus());
    					overtime.setWeeklyOff(ot1.isWeeklyOff());
    					updateFormulaOt(overtime,ot1);
    	 			}
     			}catch(Exception e){
     				
     			}
     			
	        			otList.add(overtime);
     		}
     		overtimeTable.getDataprovider().getList().addAll(otList);
 		}
 	
	 }

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		relieverOtTable.getTable().redraw();
		overtimeTable.getTable().redraw();
	}
	 
	 /**
	  * @author Vijay  Date :- 25-08-2022
	  * Des :- blank data also saving in this list so updated code blank data can not consider
	  * getting issue when employee saving on employee screen sunrise
	  * 
	  */
	
	private List<Overtime> getupdatedlist(List<Overtime> overtimelist) {
		List<Overtime> list = new ArrayList<Overtime>();
		for(Overtime overtime : overtimelist){
			if(overtime.getEmpName()!=null && !overtime.getEmpName().equals("")){
				list.add(overtime);
			}
		}
		Console.log("updated overtime list"+list.size());
		overtimeTable.getDataprovider().setList(list);
		Console.log("Table list "+overtimeTable.getDataprovider().getList());
		return list;
	}
	/**
	 * ends here
	 */
	
}

//package com.slicktechnologies.client.views.humanresource.project;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//
//import com.google.gwt.event.dom.client.ChangeEvent;
//import com.google.gwt.event.dom.client.ChangeHandler;
//import com.google.gwt.event.dom.client.ClickEvent;
//import com.google.gwt.event.dom.client.ClickHandler;
//import com.google.gwt.user.client.ui.Button;
//import com.google.gwt.user.client.ui.CheckBox;
//import com.google.gwt.user.client.ui.DoubleBox;
//import com.google.gwt.user.client.ui.InlineLabel;
//import com.google.gwt.user.client.ui.TextArea;
//import com.google.gwt.user.client.ui.TextBox;
//import com.google.gwt.user.datepicker.client.DateBox;
//import com.simplesoftwares.client.library.FieldType;
//import com.simplesoftwares.client.library.FormField;
//import com.simplesoftwares.client.library.FormFieldBuilder;
//import com.simplesoftwares.client.library.appskeleton.AppMemory;
//import com.simplesoftwares.client.library.appskeleton.ScreeenState;
//import com.simplesoftwares.client.library.appstructure.SuperTable;
//import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
//import com.simplesoftwares.client.library.appstructure.search.Filter;
//import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
//import com.simplesoftwares.client.library.composite.PersonInfoComposite;
//import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
//import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
//import com.slicktechnologies.client.login.LoginPresenter;
//import com.slicktechnologies.client.utility.AppUtility;
//import com.slicktechnologies.client.utility.Screen;
//import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
//import com.slicktechnologies.shared.common.businessunitlayer.Branch;
//import com.slicktechnologies.shared.common.helperlayer.Config;
//import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
//import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
//import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
//import com.slicktechnologies.shared.common.personlayer.Employee;
//import com.slicktechnologies.shared.common.role.AuthorizationHelper;
//
//public class HrProjectForm extends FormTableScreen<HrProject> implements ClickHandler, ChangeHandler {
//	
//	
//	TextBox tbprojectname;
//	TextBox tbProjectCode;
//	//PersonInfoComposite personinfo;
//	TextArea notes;
//	ObjectListBox<EmployeeInfo> olbteam;
//	TextBox olbtask;
//	Button addteam;
//	TeamTable teamtable;
//	TaskTable tasktable;
//	Button addtask;
//	CheckBox cbprojectstatus;
//	TextArea tadescription;
//	
//	//Token to add the concrete presenter class name
//	//protected <PresenterClassName> presenter;
//	// Budget Budget
//	/**
//	 * Date : 31-05-2018 By ANIL
//	 * Adding client ,budgeting hours ,from date and to date
//	 */
//	PersonInfoComposite personInfo;
//	DoubleBox dbBudgetedHours;
//	DateBox dtFromDate;
//	DateBox dtToDate;
//	
//	/**
//	 * Date : 16-07-2018 By ANIL
//	 */
//	ObjectListBox<Overtime> oblOtList;
//	Button btnAdd;
//	OvertimeTable overtimeTable;
//	
//	/**
//	 * Date : 23-07-2018 By ANIL
//	 */
//	ObjectListBox<Employee> oblEmployee;
//	ObjectListBox<Config> oblDesignation;
//	ObjectListBox<Branch> oblBranch;
//	
//	public HrProjectForm  (SuperTable<HrProject> table, int mode,boolean captionmode) {
//		super(table, mode, captionmode);
//		createGui();
//		overtimeTable.connectToLocal();
//	}
//	
//	private void initalizeWidget() {
//		tbProjectCode = new TextBox();
//		tbprojectname = new TextBox();
//		tadescription = new TextArea();
//		cbprojectstatus = new CheckBox();
//		olbtask = new TextBox();
//
//		cbprojectstatus.setValue(true);
//		olbteam = new ObjectListBox<EmployeeInfo>();
//		EmployeeInfo.makeObjectListBoxLive(olbteam);
//		teamtable = new TeamTable();
//		teamtable.connectToLocal();
//		tasktable = new TaskTable();
//		tasktable.connectToLocal();
//		// personinfo=AppUtility.customerInfoComposite(new Customer());
//		addtask = new Button("Add");
//		addtask.addClickHandler(this);
//		addteam = new Button("Add");
//		addteam.addClickHandler(this);
//		/*
//		 * personinfo.getCustomerId().getHeaderLabel().setText("Customer ID");
//		 * personinfo
//		 * .getCustomerName().getHeaderLabel().setText("Customer Name");
//		 * personinfo.getCustomerCell().getHeaderLabel().setText(
//		 * "Customer Cell"); personinfo.setValidate(false);
//		 */
//
//		MyQuerry querry = new MyQuerry();
//		querry.setQuerryObject(new Customer());
//		personInfo = new PersonInfoComposite(querry, false);
//
//		dbBudgetedHours = new DoubleBox();
//		dtFromDate = new DateBoxWithYearSelector();
//		dtToDate = new DateBoxWithYearSelector();
//		
//		oblOtList=new ObjectListBox<Overtime>();
//		Filter filter=new Filter();
//		filter.setBooleanvalue(true);
//		filter.setQuerryString("status");
//		MyQuerry querry1=new MyQuerry();
//		querry1.getFilters().add(filter);
//		querry1.setQuerryObject(new Overtime());
//		oblOtList.MakeLive(querry1);
//		btnAdd=new Button("Add");
//		btnAdd.addClickHandler(this);
//		
//		overtimeTable=new OvertimeTable();
//		
//
//		oblBranch=new ObjectListBox<Branch>();
//		AppUtility.makeBranchListBoxLive(oblBranch);
//		oblBranch.addChangeHandler(this);
//
//		oblDesignation=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(oblDesignation,Screen.EMPLOYEEDESIGNATION);
//		oblDesignation.addChangeHandler(this);
//		
//		oblEmployee=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblEmployee);
//	}
//
//	
//	/*
//	 * Method template to create the formtable screen
//	 */
//	 public void onClick(ClickEvent event)
//	    {
//	       
//	        if(event.getSource().equals(addtask))
//	        {
//	        	if(this.olbtask.getValue()!=null)
//	        		reactOnAddTask();
//	        }
//	        if(event.getSource().equals(addteam))
//	        {
//	        	if(this.olbteam.getValue()!=null)
//	        		reactOnAddEmployee();
//	        }
//	        
//	        if(event.getSource().equals(btnAdd)){
//	        	
//	        	if(oblOtList.getSelectedIndex()==0){
//	        		showDialogMessage("Please select overtime.");
//	        	}else{
//	        		Overtime ot1=oblOtList.getSelectedItem();
//	        		
//	        		if(oblEmployee.getSelectedIndex()!=0){
//	        			Employee employee=oblEmployee.getSelectedItem();
//	        			ot1.setEmpDesignation(employee.getDesignation());
//		        		ot1.setEmpId(employee.getCount());
//		        		ot1.setEmpName(employee.getFullname());
//		        		boolean flag=false;
//		        		for(Overtime ot:overtimeTable.getDataprovider().getList()){
//		        			if(ot1.getName().equals(ot.getName())
//		        					&&employee.getCount()==ot.getEmpId()){
//		        				showDialogMessage("Overtime already added.");
//		        				flag=true;
//		        				break;
//		        			}
//		        		}
//		        		if(!flag)
//		        			overtimeTable.getDataprovider().getList().add(ot1);
//	        		}else{
//	        			List<Employee>empList=oblEmployee.getItems();
//	        			System.out.println("EMP LIST SIZE : "+empList.size());
//	        			ArrayList<Overtime> otList=new ArrayList<Overtime>();
//		        		for(Employee employee:empList){
//		        			
//		        			boolean flag=false;
//			        		for(Overtime ot:overtimeTable.getDataprovider().getList()){
//			        			if(ot1.getName().equals(ot.getName())
//			        					&&employee.getCount()==ot.getEmpId()){
////			        				showDialogMessage("Overtime already added.");
//			        				flag=true;
//			        				break;
//			        			}
//			        		}
//		        			
//		        			Overtime overtime=new Overtime();
////		        			overtime=ot1;
//		        			
//		        			overtime.setCompanyId(ot1.getCompanyId());
//		        			overtime.setCount(ot1.getCount());
//		        			
//		        			overtime.setFlat(ot1.isFlat());
//		        			overtime.setFlatRate(ot1.getFlatRate());
//		        			
//		        			overtime.setHolidayType(ot1.getHolidayType());
//		        			overtime.setHourly(ot1.isHourly());
//		        			overtime.setHourlyRate(ot1.getHourlyRate());
//		        			
//		        			overtime.setLeave(ot1.isLeave());
//		        			overtime.setLeaveMultiplier(ot1.getLeaveMultiplier());
//		        			overtime.setLeaveName(ot1.getLeaveName());
//		        			
//		        			overtime.setName(ot1.getName());
//		        			overtime.setNormalDays(ot1.isNormalDays());
//		        			
//		        			overtime.setPercentOfCTC(ot1.getPercentOfCTC());
//		        			
//		        			overtime.setShortName(ot1.getShortName());
//		        			overtime.setStatus(ot1.isStatus());
//		        			
//		        			overtime.setWeeklyOff(ot1.isWeeklyOff());
//		        			
//		        			overtime.setEmpDesignation(employee.getDesignation());
//		        			overtime.setEmpId(employee.getCount());
//		        			overtime.setEmpName(employee.getFullname());
//		        			
//		        			
//			        		if(!flag)
//			        			otList.add(overtime);
//		        		}
//		        		
//		        		overtimeTable.getDataprovider().getList().addAll(otList);
//	        		}
//	        		
//	        		
//	        		
//
//	        		
//	        		
//	        	}
//	        }
//	       
//	    }
//	 
//	 private void reactOnAddEmployee()
//	    {
//	        EmployeeInfo asset =olbteam.getSelectedItem();
//	        List<EmployeeInfo> list = teamtable.getDataprovider().getList();
//	        if(list != null)
//	        {
//	            if(list.contains(asset)==false)
//	        	list.add(asset);
//	           
//	        }
//	        olbteam.setSelectedIndex(0);
//	    }
//
//	 
//	 private void reactOnAddTask()
//	    {
//	        String asset =olbtask.getText();
//	        List<String> list = tasktable.getDataprovider().getList();
//	        if(list != null)
//	        {
//	            if(list.contains(asset)==false)
//	        	list.add(asset);
//	           
//	        }
//	        olbtask.setText("");
//	    }
//
//	@Override
//	public void createScreen() {
//		
//		//Token to initialize the processlevel menus.
//	    initalizeWidget();
//		
//	    this.processlevelBarNames=new String[]{"New"};
//
//
//		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////
//		
//		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
//		
//		//Token to initialize formfield
//		FormFieldBuilder fbuilder;
//		fbuilder = new FormFieldBuilder();
//		
//		/*FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
//		fbuilder = new FormFieldBuilder("",personinfo);*/
//		//FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
//		FormField fgroupingProjectInformation=fbuilder.setlabel("Project Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("* Project Name",tbprojectname);
//		FormField ftbprojectname= fbuilder.setMandatory(true).setMandatoryMsg("Project Name can't be empty").setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("* Project Code",tbProjectCode);
//		FormField ftbprojectcode= fbuilder.setMandatory(true).setMandatoryMsg("Project Code can't be empty").setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Status",cbprojectstatus);
//		FormField fcbstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Description",tadescription);
//		FormField ftadescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
//
//		FormField fgroupingteamInformation=fbuilder.setlabel("Team Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
//		
//		 fbuilder = new FormFieldBuilder("Employee", olbteam);
//	        FormField folbEmployee = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//	        fbuilder = new FormFieldBuilder("", addteam);
//	        FormField fbtnEmp = fbuilder.build();
//	        fbuilder = new FormFieldBuilder("", teamtable.getTable());
//	        FormField femptable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
//	        
//	        FormField fgroupingtaskInformation=fbuilder.setlabel("Task Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
//			
//			 fbuilder = new FormFieldBuilder("Task", olbtask);
//		        FormField folbTask = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		        fbuilder = new FormFieldBuilder("", addtask);
//		        FormField fbtnTask = fbuilder.build();
//		        fbuilder = new FormFieldBuilder("", tasktable.getTable());
//		        FormField ftasktable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
//		       
//		        fbuilder = new FormFieldBuilder("",personInfo);
//				FormField fpersonInfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
//				
//				fbuilder = new FormFieldBuilder("Budgeted Hours",dbBudgetedHours);
//				FormField fdbBudgetedHours= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
//				
//				fbuilder = new FormFieldBuilder("From Date",dtFromDate);
//				FormField fdtFromDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
//				
//				fbuilder = new FormFieldBuilder("To Date",dtToDate);
//				FormField fdtToDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
//				
//		FormField fgroupingOtInfo = fbuilder.setlabel("Overtime Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
//
//		fbuilder = new FormFieldBuilder("Overtime", oblOtList);
//		FormField foblOtList = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("", btnAdd);
//		FormField fbtnAdd = fbuilder.build();
//		fbuilder = new FormFieldBuilder("", overtimeTable.getTable());
//		FormField fovertimeTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("Branch",oblBranch);
//		FormField foblBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
//		
//		fbuilder = new FormFieldBuilder("Employee Designation",oblDesignation);
//		FormField flbRole= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
//		
//		fbuilder = new FormFieldBuilder("Employee",oblEmployee);
//		FormField foblEmployee= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
//
//	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//		
//////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
//		
//		
//  FormField[][] formfield = {   
//		  
//  {fgroupingProjectInformation},
//  
//  {fpersonInfo},
//  {fdbBudgetedHours,fdtFromDate,fdtToDate},
//  
//  {ftbprojectname,ftbprojectcode,fcbstatus},
//  {ftadescription},
// /* {fgroupingteamInformation},
//  {folbEmployee,fbtnEmp},
//  {femptable},
//  {fgroupingtaskInformation},
//  {folbTask,fbtnTask},
//  {ftasktable}*/
//  {fgroupingOtInfo},
//  {flbRole,foblBranch,foblEmployee,foblOtList,fbtnAdd},
//  {fovertimeTable},
//  };
//
//		
//        this.fields=formfield;		
//   }
//
//	/**
//	 * method template to update the model with token entity name
//	 */
//	@Override
//	public void updateModel(HrProject model) {
//
//		if (tbprojectname.getValue() != null)
//			model.setProjectName(tbprojectname.getValue());
//		if (tbProjectCode.getValue() != null)
//			model.setProjectCode(tbProjectCode.getValue());
//		if (cbprojectstatus.getValue() != null)
//			model.setStatus(cbprojectstatus.getValue());
//		if (tadescription.getValue() != null)
//			model.setDescription(tadescription.getValue());
//		if (teamtable.getValue() != null)
//			model.setTeamtable(teamtable.getValue());
//		if (tasktable.getValue() != null)
//			model.setTasktable(tasktable.getValue());
//		
//		if(personInfo.getValue()!=null){
//			model.setPersoninfo(personInfo.getValue());
//		}
//		if(dbBudgetedHours.getValue()!=null){
//			model.setBudgetedHours(dbBudgetedHours.getValue());
//		}
//		if(dtFromDate.getValue()!=null){
//			model.setFromDate(dtFromDate.getValue());
//		}
//		if(dtToDate.getValue()!=null){
//			model.setToDate(dtToDate.getValue());
//		}
//		
//		if(overtimeTable.getValue()!=null){
//			model.setOtList(overtimeTable.getValue());
//		}
//
//		presenter.setModel(model);
//	}
//
//	
//	
//	/**
//	 * method template to update the view with token entity name
//	 */
//	@Override
//	public void updateView(HrProject view) {
//
//		if (view.getProjectName() != null)
//			tbprojectname.setValue(view.getProjectName());
//		if (view.getProjectCode() != null)
//			tbProjectCode.setValue(view.getProjectCode());
//		if (view.getStatus() != null)
//			cbprojectstatus.setValue(view.getStatus());
//		if (view.getDescription() != null)
//			tadescription.setValue(view.getDescription());
//		if (view.getTeamtable() != null)
//			teamtable.setValue(view.getTeamtable());
//		if (view.getTasktable() != null)
//			tasktable.setValue(view.getTasktable());
//		
//		if(view.getPersoninfo()!=null){
//			personInfo.setValue(view.getPersoninfo());
//		}
//		if(view.getBudgetedHours()!=0){
//			dbBudgetedHours.setValue(view.getBudgetedHours());
//		}
//		if(view.getFromDate()!=null){
//			dtFromDate.setValue(view.getFromDate());
//		}
//		if(view.getToDate()!=null){
//			dtToDate.setValue(view.getToDate());
//		}
//		
//		if(view.getOtList()!=null){
//			overtimeTable.setValue(view.getOtList());
//		}
//
//		presenter.setModel(view);
//
//	}
//
//	/**
//	 * Toggles the app header bar menus as per screen state
//	 */
//	
//	public void toggleAppHeaderBarMenu() {
//		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.equals("Save")||text.equals("Discard"))
//				{
//					menus[k].setVisible(true); 
//				}
//				else
//					menus[k].setVisible(false);  		   
//				
//			}
//		}
//		
//		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.equals("Save")||text.equals("Discard"))
//					menus[k].setVisible(true); 
//				
//				else
//					menus[k].setVisible(false);  		   
//			}
//		}
//		
//		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.equals("Discard")||text.equals("Edit"))
//						menus[k].setVisible(true); 
//				
//				else
//					menus[k].setVisible(false);  		   
//			}
//		}
//		AuthorizationHelper.setAsPerAuthorization(Screen.PROJECT,LoginPresenter.currentModule.trim());
//	}
//	
//	 public boolean validate()
//	    {
//	       
//		 boolean superValidate = super.validate();
//	        
//	        if(superValidate==false)
//	        	return false;
//	       /* if(teamtable.getDataprovider().getList().size() == 0)
//	        {
//	            showDialogMessage("Please Select at least one Employee !");
//	            return false;
//	            
//	            
//	        }
//	        
//	        if(tasktable.getDataprovider().getList().size() == 0)
//	        {
//	            showDialogMessage("Please Add at least one Task !");
//	            return false;
//	            
//	            
//	        }*/
//			return true;
//	        
//	    }
//
//	@Override
//	public void setEnable(boolean state) {
//		super.setEnable(state);
//		overtimeTable.setEnable(state);
//	}
//
//	@Override
//	public void setToEditState() {
//		super.setToEditState();
//		this.processLevelBar.setVisibleFalse(false);
//	}
//
//	@Override
//	public void clear() {
//		
//		super.clear();
//		cbprojectstatus.setValue(true);
//		overtimeTable.connectToLocal();
//	}
//
//	@Override
//	public void onChange(ChangeEvent event) {
//		if(event.getSource()==oblDesignation){
//			updateEmployeeDropDown();
//		}
//		if(event.getSource()==oblBranch){
//			updateEmployeeDropDown();
//		}
//	}
//
//	private void updateEmployeeDropDown() {
//		String designation="";
//		if(oblDesignation.getSelectedIndex()!=0){
//			designation=oblDesignation.getValue();
//		}
//		String branch="";
//		if(oblBranch.getSelectedIndex()!=0){
//			branch=oblBranch.getValue();
//		}
//		
//		ArrayList<Employee> listEmployee = LoginPresenter.globalEmployee;
//		ArrayList<Employee> aciveEmployeeList = new ArrayList<Employee>();
//		Comparator<Employee> employeeArrayComparator = new Comparator<Employee>() {
//			public int compare(Employee c1, Employee c2) {
//				String employeeName1 = c1.getFullname();
//				String employeeName2 = c2.getFullname();
//
//				return employeeName1.compareTo(employeeName2);
//			}
//		};
//		Collections.sort(listEmployee, employeeArrayComparator);
//		
//
//		for (int i = 0; i < listEmployee.size(); i++) {
//			if (listEmployee.get(i).isStatus()) {
//				if(!designation.equals("")&&!branch.equals("")){
//					if(listEmployee.get(i).getDesignation().equals(designation)
//							&&listEmployee.get(i).getBranchName().equals(branch)){
//						aciveEmployeeList.add(listEmployee.get(i));
//					}
//				}else if(!designation.equals("")&&branch.equals("")){
//					if(listEmployee.get(i).getDesignation().equals(designation)){
//						aciveEmployeeList.add(listEmployee.get(i));
//					}
//				}else if(designation.equals("")&&!branch.equals("")){
//					if(listEmployee.get(i).getBranchName().equals(branch)){
//						aciveEmployeeList.add(listEmployee.get(i));
//					}
//				}else{
//					aciveEmployeeList.add(listEmployee.get(i));
//				}
//			}
//		}
//		oblEmployee.getItems().clear();
//		oblEmployee.getItems().addAll(aciveEmployeeList);
//		oblEmployee.setListItems(oblEmployee.getItems());
//		
//	}
//
//	
//}
