package com.slicktechnologies.client.views.humanresource.project;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import java.util.Comparator;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;
import java.util.List;
import java.util.Date;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.client.views.humanresource.project.HrProjectPresenter.ProjectPresenterTable;
import com.slicktechnologies.client.utility.Screen;

public class ProjectPresenterTableProxy extends ProjectPresenterTable {
  TextColumn<HrProject> getProjectNameColumn;
  TextColumn<HrProject> getStatusColumn;
  TextColumn<HrProject> getCountColumn;
  public Object getVarRef(String varName)
  {
  if(varName.equals("getProjectNameColumn"))
  return this.getProjectNameColumn;
  if(varName.equals("getStatusColumn"))
  return this.getStatusColumn;
  if(varName.equals("getCountColumn"))
  return this.getCountColumn;
   return null ;
  }
  public ProjectPresenterTableProxy()
  {
  super();
  }
  @Override public void createTable() {
  addColumngetCount();
  addColumngetProjectName();
  addColumngetStatus();
  }
  @Override
  protected void initializekeyprovider() {
  keyProvider= new ProvidesKey<HrProject>()
  {
  @Override
  public Object getKey(HrProject item)
  {
  if(item==null)
  {
  return null;
  }
  else
  return item.getId();
  }
  };
  }
  @Override
  public void setEnable(boolean state)
  {
  }
  @Override
  public void applyStyle()
  {
  }
   public void addColumnSorting(){
  addSortinggetCount();
  addSortinggetProjectName();
  addSortinggetStatus();
  }
  @Override public void addFieldUpdater() {
  }
  protected void addSortinggetCount()
  {
  List<HrProject> list=getDataprovider().getList();
  columnSort=new ListHandler<HrProject>(list);
  columnSort.setComparator(getCountColumn, new Comparator<HrProject>()
  {
  @Override
  public int compare(HrProject e1,HrProject e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCount()== e2.getCount()){
  return 0;}
  if(e1.getCount()> e2.getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCount()
  {
  getCountColumn=new TextColumn<HrProject>()
  {
  @Override
  public String getValue(HrProject object)
  {
  if( object.getCount()==-1)
  return "N.A";
  else return object.getCount()+"";
  }
  };
  table.addColumn(getCountColumn,"Id");
  getCountColumn.setSortable(true);
  }
  protected void addSortinggetProjectName()
  {
  List<HrProject> list=getDataprovider().getList();
  columnSort=new ListHandler<HrProject>(list);
  columnSort.setComparator(getProjectNameColumn, new Comparator<HrProject>()
  {
  @Override
  public int compare(HrProject e1,HrProject e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getProjectName()!=null && e2.getProjectName()!=null){
  return e1.getProjectName().compareTo(e2.getProjectName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetProjectName()
  {
  getProjectNameColumn=new TextColumn<HrProject>()
  {
  @Override
  public String getValue(HrProject object)
  {
  return object.getProjectName()+"";
  }
  };
  table.addColumn(getProjectNameColumn,"Project Name");
  getProjectNameColumn.setSortable(true);
  }
  protected void addSortinggetStatus()
  {
  List<HrProject> list=getDataprovider().getList();
  columnSort=new ListHandler<HrProject>(list);
  columnSort.setComparator(getStatusColumn, new Comparator<HrProject>()
  {
  @Override
  public int compare(HrProject e1,HrProject e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getStatus()== e2.getStatus()){
  return 0;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetStatus()
  {
  getStatusColumn=new TextColumn<HrProject>()
  {
  @Override
  public String getValue(HrProject object)
  {
  if( object.getStatus()==true)
  return "Active";
  else 
  return "In Active";
  }
  };
  table.addColumn(getStatusColumn,"Status");
  getStatusColumn.setSortable(true);
  }
}
