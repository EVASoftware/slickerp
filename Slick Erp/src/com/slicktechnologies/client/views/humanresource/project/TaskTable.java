package com.slicktechnologies.client.views.humanresource.project;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;

public class TaskTable extends SuperTable<String>
{

	 TextColumn<String> nameColumn;
	   
	    Column<String,String> deleteColumn;
   public TaskTable()
   {
   }

   public TaskTable(UiScreen<String> view)
   {
       super(view);
   }

   protected void createColumndeleteColumn()
   {
       ButtonCell btnCell = new ButtonCell();
       deleteColumn = new Column<String,String>(btnCell) {

           public String getValue(String object)
           {
               return "Delete";
           }

       }
;
       table.addColumn(deleteColumn, "Delete");
   }

   private void createColumnName()
   {
       nameColumn = new TextColumn<String>() {

           public String getValue(String object)
           {
               return object.toString();
           }

       }
;
       table.addColumn(nameColumn, "Task Name");
       nameColumn.setSortable(true);
   }

   
   public void createTable()
   {
       createColumnName();
       
       createColumndeleteColumn();
       table.setWidth("100%");
       createFieldUpdaterdeleteColumn();
   }

   @SuppressWarnings("unchecked")
/*	private void setFieldupdaterName()
   {
       nameColumn.setFieldUpdater(new FieldUpdater<String,String>() {

           public void update(int index, String object, String value)
           {
               object.setName(value);
               
//JavaClassFileOutputException: get_constant: invalid tag

           } 
       }
);
   }
*/
   
	protected void createFieldUpdaterdeleteColumn()
   {
       deleteColumn.setFieldUpdater(new FieldUpdater<String,String>() {

           public void update(int index, String object, String value)
           {
               getDataprovider().getList().remove(object);

           }
     
       }
);
   }

   protected void initializekeyprovider()
   {
       keyProvider = new ProvidesKey<String>() {

           public Object getKey(String item)
           {
               if(item == null)
                   return null;
               else
                   return 0;
           }

          
       }
;
   }

   public void addColumnSorting()
   {
      // addNameSorting();
       
   }

   
	/*public void addNameSorting()
   {
       List list = getDataprovider().getList();
       columnSort = new com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler(list);
       columnSort.setComparator(nameColumn, new Comparator<String>() {

           public int compare(String e1, String e2)
           {
               
               if(e1 != null && e2 != null)
               {
                   if(e1.getName() != null && e2.getName() != null)
                       return e1.getName().compareTo(e2.getName());
                   else
                       return 0;
               } else
               {
                   return 0;
               }
           }
       }
);
       table.addColumnSortHandler(columnSort);
   }*/

	 @Override
		public void setEnable(boolean state)
		{
	        for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
	          if(state==true)
	          {
	        	  createColumnName();
	        	 
	      		createColumndeleteColumn();
	      		createFieldUpdaterdeleteColumn();
	          }
	          
	          else
	          {
	        	  createColumnName();
	        	 
	      		addFieldUpdater();
	      	
	          }
		}

   public void applyStyle()
   {
   }

   public void addFieldUpdater()
   {
     //  setFieldupdaterName();
   }

  
}
