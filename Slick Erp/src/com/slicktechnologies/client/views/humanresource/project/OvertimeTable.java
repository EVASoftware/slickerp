package com.slicktechnologies.client.views.humanresource.project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;

public class OvertimeTable extends SuperTable<Overtime>{
	
	TextColumn<Overtime> getColOtName;
	Column<Overtime, String> getColDeletButton , editShiftColumn;	
	TextColumn<Overtime> getColDesignation;
	TextColumn<Overtime> getColEmpId;
	TextColumn<Overtime> getColEmpName , viewShiftColumn,startDate , endDate;
	ArrayList<String> shiftList;
	HashMap<String, Shift> shiftListMap;
	Column<Overtime, Date> editStartDate ,editEndDate;
	
	ArrayList<String> overtimeList;
	/**Date 5-4-2019 by Amol
	 * added  a dropdown for Overtime Column
	 */
	Column<Overtime , String> editOvertime;
	
	/**
	 * @author Anil
	 * @since 21-08-2020
	 */
	TextColumn<Overtime> nonEditableSiteLocation;
	Column<Overtime,String> editableSiteLocation;
	int custId;
	String state;
	ArrayList<String> locationList;
	boolean siteLocationFlag=false;
	
	/**
	 * @author Anil
	 * @since 27-08-2020
	 */
	boolean reliverOt=false;
	
	public OvertimeTable() {
		super();
		/**Date 4-4-2019 by Amol
		 * set a height and width of table
		 */
		table.setWidth("100%");
		table.setHeight("350px");
		shiftList = new ArrayList<String>();
		shiftListMap = new  HashMap<String, Shift>();
		overtimeList = new ArrayList<String>();
		locationList=new ArrayList<String>();
		siteLocationFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("HrProject", "EnableSiteLocation");
	}

	@Override
	public void createTable() {
		siteLocationFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("HrProject", "EnableSiteLocation");
		
		if(siteLocationFlag){
			addEditableSiteLocationCol();
		}else{
			getColDesignation();
			getColEmpId();
			getColEmpName();
//			getColOtName();
			/**Date 5-4-2019 by Amol
			 * add dropDown for Overtime Column
			 */
			makeList();			
			addEditOvertime();
			addEditColumnShift();
			addColumnSorting();
	//		createColumnStartDateColumn();
	//		createColumnEndDateColumn();
	//		getColDeletButton();
		}
		
	}
	
	private void addEditableSiteLocationCol() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(custId);
		filtervec.add(filter);
		
		if(state!=null&&!state.equals("")){
			filter = new Filter();
//			filter.setQuerryString("billingAddress.state");
			filter.setQuerryString("contact.address.state");
			filter.setStringValue(state);
			filtervec.add(filter);
		}
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		query.setFilters(filtervec);
		
		query.setQuerryObject(new CustomerBranchDetails());
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				locationList.clear();
				locationList.add("--SELECT--");
				for (SuperModel model : result) {
					CustomerBranchDetails location=(CustomerBranchDetails) model;
					locationList.add(location.getBusinessUnitName());
				}
				System.out.println("locationList:::"+locationList.size());
				SelectionCell locLis= new SelectionCell(locationList);
				editableSiteLocation = new Column<Overtime, String>(locLis) {
					@Override
					public String getValue(Overtime object) {
						if (object.getSiteLocation() != null)
							return object.getSiteLocation();
						else
							return "";
						
					}
				};
				table.addColumn(editableSiteLocation, "#Site Location");
				
				editableSiteLocation.setFieldUpdater(new FieldUpdater<Overtime, String>() {
					@Override
					public void update(int index,Overtime object, String value) {
						if(value!=null){
							object.setSiteLocation(value);
						}
						table.redrawRow(index);
					}
				});
				
				getColDesignation();
				getColEmpId();
				getColEmpName();
				getColOtName();
				makeList();
				addEditColumnShift();
				addColumnSorting();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	private void addNonEditableSiteLocation() {
		// TODO Auto-generated method stub
		nonEditableSiteLocation = new TextColumn<Overtime>() {
			
			@Override
			public String getValue(Overtime object) {
				if(object.getSiteLocation()!=null){
					return object.getSiteLocation();
				}else{
					return "";
				}
			}
		};
		table.addColumn(nonEditableSiteLocation,"Site Location");
//		table.setColumnWidth(nonEditableSiteLocation, 120,Unit.PX);
	}
	
     /**Date 23-5-2019 by Amol
      * Added Sorting for Employee Name column
     */
	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		
		if(reliverOt){
			addDesignationCol();
			addOvertimrCol();
		}else{
			addSortingEmployeeNameColumn();
		}
		
		
	}

	public void addSortingEmployeeNameColumn() {
		List<Overtime> list=getDataprovider().getList();
		  columnSort=new ListHandler<Overtime>(list);
		  columnSort.setComparator(getColEmpName, new Comparator<Overtime>()
		  {
		  @Override
		  public int compare(Overtime e1,Overtime e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getEmpName()!=null && e2.getEmpName()!=null){
		  return e1.getEmpName().compareTo(e2.getEmpName());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
	  }

	private void addEditOvertime() {
		// TODO Auto-generated method stub
		SelectionCell overtimeSelection = new SelectionCell(
				overtimeList);		
		editOvertime = new Column<Overtime, String>(overtimeSelection) {
			@Override
			public String getValue(Overtime object) {
				// TODO Auto-generated method stub
				if (object.getName()!= null)
					return object.getName();
				else
					return "";

			}
		};
		table.addColumn(editOvertime, "#Overtime");
		table.setColumnWidth(editOvertime, 180, Unit.PX);

		editOvertime.setFieldUpdater(new FieldUpdater<Overtime, String>() {

					@Override
					public void update(int index, Overtime object,
							String value) {

						if (value.equalsIgnoreCase("--SELECT--")) {
							object.setOvertime("");
							object.setName("");
						} else {
							object.setOvertime(value);
							object.setName(value);
							for(Overtime ot : LoginPresenter.globalOvertimeList){
								if(ot.getName().equals(value)){
									//Console.log("overtime table ot name matched shortname="+ot.getShortName());
									object.setCount(ot.getCount());
									object.setCompanyId(ot.getCompanyId());
									object.setCount(ot.getCount());
									object.setFlat(ot.isFlat());
									object.setFlatRate(ot.getFlatRate());
									object.setHolidayType(ot.getHolidayType());
									object.setHourly(ot.isHourly());
									object.setHourlyRate(ot.getHourlyRate());
									object.setLeave(ot.isLeave());
									object.setLeaveMultiplier(ot.getLeaveMultiplier());
									object.setLeaveName(ot.getLeaveName());
									object.setName(ot.getName());
									object.setNormalDays(ot.isNormalDays());
									object.setPercentOfCTC(ot.getPercentOfCTC());
									object.setShortName(ot.getShortName());
									object.setStatus(ot.isStatus());
									object.setWeeklyOff(ot.isWeeklyOff());									
								}
							}
						}
						// getDataprovider().getList().remove(index);

						table.redrawRow(index);
					}
				});
	}

	private void getColDesignation() {
		getColDesignation=new TextColumn<Overtime>() {
			@Override
			public String getValue(Overtime object) {
				return object.getEmpDesignation();
			}
		};
		table.addColumn(getColDesignation, "Designation");
		getColDesignation.setSortable(true);
	}

	private void getColEmpId() {
		getColEmpId=new TextColumn<Overtime>() {
			@Override
			public String getValue(Overtime object) {
				if(object.getEmpId()!=0)
					return object.getEmpId()+"";
				else
					return "";
			}
		};
		table.addColumn(getColEmpId, "Emp Id");
	}

	private void getColEmpName() {
		getColEmpName=new TextColumn<Overtime>() {
			@Override
			public String getValue(Overtime object) {
				return object.getEmpName();
			}
		};
		table.addColumn(getColEmpName, "Emp Name");
		getColEmpName.setSortable(true);
	}

	private void getColDeletButton() {
		// TODO Auto-generated method stub
		ButtonCell cell=new ButtonCell();
		getColDeletButton=new Column<Overtime, String>(cell) {
			
			@Override
			public String getValue(Overtime object) {
				return "Delete";
			}
		};
		table.addColumn(getColDeletButton, "");
		
		getColDeletButton.setFieldUpdater(new FieldUpdater<Overtime, String>() {
			@Override
			public void update(int index, Overtime object, String value) {
				// TODO Auto-generated method stub
				if(object.getCount()!=0)
					HrProjectForm.deletedOtIdlist.add(object.getCount());
				Console.log("object.getCount() "+object.getCount());
				
				getDataprovider().getList().remove(index);
				table.redraw();
			}
		});
	}

	private void getColOtName() {
		getColOtName=new TextColumn<Overtime>() {
			@Override
			public String getValue(Overtime object) {
				return object.getName();
			}
		};
		table.addColumn(getColOtName, "OverTime");
		getColOtName.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);
		
		if(reliverOt){
			if(state==true){
				getColDesignation();
				getColOtName();
				getColDeletButton();
			}else{
				getColDesignation();
				getColOtName();
			}
			addColumnSorting();
			return;
		}

		if (state == true) {
			if(siteLocationFlag){
				addEditableSiteLocationCol();
			}else{
				getColDesignation();
				getColEmpId();
				getColEmpName();
//	     		getColOtName();
				/**
				 * 5-4-2019 added by Amol
				 */
				makeList();
				addEditOvertime();
				addEditColumnShift();
	//			getColDeletButton();
			}
		} else {
			if(siteLocationFlag){
				addNonEditableSiteLocation();
			}
			getColDesignation();
			getColEmpId();
			getColEmpName();
			getColOtName();
			getColShift();
			createViewStartdateColumn();
			createViewEnddateColumn();
		}
		addColumnSorting();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	private void addEditColumnShift() {
		// TODO Auto-generated method stub
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		/** date 27.8.2018 added by komal to load status shiftwise**/
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		
		query.setQuerryObject(new Shift());
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				shiftList.clear();
				shiftList.add("--SELECT--");
				for (SuperModel model : result) {
					Shift shift=(Shift) model;
					shiftList.add(shift.getShiftName());
					shiftListMap.put(shift.getShiftName(), shift);
				}
				System.out.println("shiftList:::"+shiftList.size());
				SelectionCell employeeListSel= new SelectionCell(shiftList);
				editShiftColumn = new Column<Overtime, String>(employeeListSel) {
					@Override
					public String getValue(Overtime object) {
						// TODO Auto-generated method stub
						if (object.getShift() != null)
							return object.getShift();
						else
							return "";
						
					}
				};
				table.addColumn(editShiftColumn, "#Shift");
	
				editShiftColumn.setFieldUpdater(new FieldUpdater<Overtime, String>() {

					@Override
					public void update(int index,
							Overtime object, String value) {
						// TODO Auto-generated method stub
						if(value!=null){
							object.setShift(value);
//							Shift shiftDetails=shiftListMap.get(value.trim());
						}
						table.redrawRow(index);
					}
				});
				createColumnStartDateColumn();
				createColumnEndDateColumn();
				getColDeletButton();			
				}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	
	}
	private void getColShift() {
		viewShiftColumn=new TextColumn<Overtime>() {
			@Override
			public String getValue(Overtime object) {
				return object.getShift();
			}
		};
		table.addColumn(viewShiftColumn, "Shift");
	}

	protected void createColumnStartDateColumn() {
		DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		editStartDate = new Column<Overtime, Date>(date) {
			@Override
			public Date getValue(Overtime object) {
				if (object.getStartDate() == null)
					return new Date();
				return object.getStartDate();
			}

		};
		editStartDate.setFieldUpdater(new FieldUpdater<Overtime, Date>() {
			@Override
			public void update(int index, Overtime object,
					Date value) {
				if (value != null) {
					object.setStartDate(value);
					System.out.println("start date"
							+ object.getStartDate());
				}
				table.redrawRow(index);
			}
		});
		table.addColumn(editStartDate,"#Start Date");
		table.setColumnWidth(editStartDate, 110,Unit.PX);

	}
	protected void createColumnEndDateColumn() {
		DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		editEndDate = new Column<Overtime, Date>(date) {
			@Override
			public Date getValue(Overtime object) {
				if (object.getEndDate() == null)
					return new Date();
				return object.getEndDate();
			}

		};
		editEndDate.setFieldUpdater(new FieldUpdater<Overtime, Date>() {
			@Override
			public void update(int index, Overtime object,
					Date value) {
				if (value != null) {
					object.setEndDate(value);
					System.out.println("end date"
							+ object.getEndDate());
				}
				table.redrawRow(index);
			}
		});
		table.addColumn(editEndDate,"#End Date");
		table.setColumnWidth(editEndDate, 110,Unit.PX);
	}
	protected void createViewStartdateColumn() {
		startDate = new TextColumn<Overtime>() {
			@Override
			public String getValue(Overtime object) {
				System.out.println("Start date"+ object.getStartDate());
					DateTimeFormat format = DateTimeFormat.getFormat("dd-MM-yyyy");
					if(object.getStartDate()!=null){
						String date = format.format(object.getStartDate());
						return date;
					}else{
						return "N.A";
					}
			}
		};
		table.addColumn(startDate,"Start Date");
		table.setColumnWidth(startDate, 110,Unit.PX);

	}
	
	protected void createViewEnddateColumn() {
		endDate = new TextColumn<Overtime>() {
			@Override
			public String getValue(Overtime object) {
				System.out.println("End Date"+ object.getEndDate());
					DateTimeFormat format = DateTimeFormat.getFormat("dd-MM-yyyy");
					if(object.getEndDate()!=null){
						String date = format.format(object.getEndDate());
						return date;
					}else{
						return "N.A";
					}
					
			}
		};
		table.addColumn(endDate,"End Date");
		table.setColumnWidth(endDate, 110,Unit.PX);

	}
	
	/**
	 * Updated by : Amol
	 * Date: 05-04-2019
	 */
	private void makeList() {	
		ArrayList<Overtime> otList = new ArrayList<Overtime>();
		otList = LoginPresenter.globalOvertimeList;
		overtimeList = new ArrayList<String>();
		overtimeList.add("--SELECT--");
	
		if (otList.size() != 0) {
			for (int i = 0; i < otList.size(); i++) {
				if (otList.get(i).isStatus() == true) {
					if (otList.get(i).getName() != null) {
						try {
							overtimeList.add(otList.get(i)
									.getName());
						} catch (Exception e) {
							System.out.println(e);
						}
					}
				}
			}
		}
	}
	/** Ends **/

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public OvertimeTable(boolean reliverOt) {
		super(reliverOt);
		this.reliverOt=true;
		createTable1();
	}

	private void createTable1() {
		getColDesignation();
		getColOtName();
		getColDeletButton();
		
		addColumnSorting();
	}
	
	private void addOvertimrCol() {
		List<Overtime> list = getDataprovider().getList();
		columnSort = new ListHandler<Overtime>(list);
		columnSort.setComparator(getColOtName, new Comparator<Overtime>() {
			@Override
			public int compare(Overtime e1, Overtime e2) {
				if (e1 != null && e2 != null) {
					if (e1.getName() != null && e2.getName() != null) {
						return e1.getName().compareTo(e2.getName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addDesignationCol() {
		List<Overtime> list = getDataprovider().getList();
		columnSort = new ListHandler<Overtime>(list);
		columnSort.setComparator(getColDesignation, new Comparator<Overtime>() {
			@Override
			public int compare(Overtime e1, Overtime e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmpDesignation() != null && e2.getEmpDesignation() != null) {
						return e1.getEmpDesignation().compareTo(e2.getEmpDesignation());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
}
