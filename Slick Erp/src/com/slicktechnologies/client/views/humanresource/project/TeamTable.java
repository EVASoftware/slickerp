// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 16-Sep-14 11:03:32 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EmpolyeeTable.java

package com.slicktechnologies.client.views.humanresource.project;

import com.google.gwt.cell.client.*;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.personlayer.Employee;

import java.io.PrintStream;
import java.util.Comparator;
import java.util.List;

public class TeamTable extends SuperTable<EmployeeInfo>
{

	 TextColumn<EmployeeInfo> nameColumn;
	   
	    Column<EmployeeInfo,String> deleteColumn;
    public TeamTable()
    {
    }

    public TeamTable(UiScreen<EmployeeInfo> view)
    {
        super(view);
    }

    protected void createColumndeleteColumn()
    {
        ButtonCell btnCell = new ButtonCell();
        deleteColumn = new Column<EmployeeInfo,String>(btnCell) {

            public String getValue(EmployeeInfo object)
            {
                return "Delete";
            }

        }
;
        table.addColumn(deleteColumn, "Delete");
    }

    private void createColumnName()
    {
        nameColumn = new TextColumn<EmployeeInfo>() {

            public String getValue(EmployeeInfo object)
            {
                return object.getFullName();
            }

        }
;
        table.addColumn(nameColumn, "Employee Name");
        nameColumn.setSortable(true);
    }

    
    public void createTable()
    {
        createColumnName();
        
        createColumndeleteColumn();
        table.setWidth("100%");
        createFieldUpdaterdeleteColumn();
    }

    @SuppressWarnings("unchecked")
	private void setFieldupdaterName()
    {
        nameColumn.setFieldUpdater(new FieldUpdater<EmployeeInfo,String>() {

            public void update(int index, EmployeeInfo object, String value)
            {
                object.setFullName(value);
                
// JavaClassFileOutputException: get_constant: invalid tag

            } 
        }
);
    }

    
	protected void createFieldUpdaterdeleteColumn()
    {
        deleteColumn.setFieldUpdater(new FieldUpdater<EmployeeInfo,String>() {

            public void update(int index, EmployeeInfo object, String value)
            {
                getDataprovider().getList().remove(object);
 
            }
      
        }
);
    }

    protected void initializekeyprovider()
    {
        keyProvider = new ProvidesKey<EmployeeInfo>() {

            public Object getKey(EmployeeInfo item)
            {
                if(item == null)
                    return null;
                else
                    return item.getId();
            }

           
        }
;
    }

    public void addColumnSorting()
    {
        addNameSorting();
        
    }

    
	public void addNameSorting()
    {
        List list = getDataprovider().getList();
        columnSort = new com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler(list);
        columnSort.setComparator(nameColumn, new Comparator<EmployeeInfo>() {

            public int compare(EmployeeInfo e1, EmployeeInfo e2)
            {
                
                if(e1 != null && e2 != null)
                {
                    if(e1.getFullName() != null && e2.getFullName() != null)
                        return e1.getFullName().compareTo(e2.getFullName());
                    else
                        return 0;
                } else
                {
                    return 0;
                }
            }
        }
);
        table.addColumnSortHandler(columnSort);
    }

	 @Override
		public void setEnable(boolean state)
		{
	        for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
	          if(state==true)
	          {
	        	  createColumnName();
	        	 
	      		createColumndeleteColumn();
	      		createFieldUpdaterdeleteColumn();
	          }
	          
	          else
	          {
	        	  createColumnName();
	        	 
	      		addFieldUpdater();
	      	
	          }
		}

    public void applyStyle()
    {
    }

    public void addFieldUpdater()
    {
        setFieldupdaterName();
    }

   

}
