package com.slicktechnologies.client.views.humanresource.attendancelist;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ChangeListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.attendance.AttendanceBean;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class AttendanceListForm extends ViewContainer implements ChangeHandler, SelectionHandler<Suggestion>, ValueChangeHandler<String>{


	ListGrid grid;
	DataSource ds;
	
	ObjectListBox<Branch> olbbranch=new ObjectListBox<Branch>();
	ObjectListBox<Employee> olbsalesPerson = new ObjectListBox<Employee>();
	DateBox dbMonth;
	
	Button btnGo;
	
//	FlexForm form;
	
	ObjectListBox<HrProject> olbProject;
	EmployeeInfoComposite pic;
	
	HorizontalPanel horizontalpanel;
	VerticalPanel verticalpanel;
	
	/**
	 * @author Anil ,Date : 28-03-2019
	 */
	PersonInfoComposite clientComposite;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	ArrayList<String> branchList=new ArrayList<String>();
	Button btnDownload;
	HorizontalPanel horizontalpanel1;
	
	/**
	 * @author Anil
	 * @since 27-08-2020
	 * EnableSiteLocation
	 */
	public boolean siteLocation=false;
	
	public AttendanceListForm() {
		super();
		siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "EnableSiteLocation");
		
		createGui();
	}
	
	@Override
	protected void createGui() {
		System.out.println("CREATE GUI");
		toggleAppHeaderBarMenu();
		
		clientComposite=AppUtility.customerInfoComposite(new Customer());
		clientComposite.getCustomerId().getHeaderLabel().setText("Customer ID");
		clientComposite.getCustomerName().getHeaderLabel().setText("Customer Name");
		clientComposite.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		clientComposite.getElement().getStyle().setWidth(120, Unit.PCT);
		
		clientComposite.getId().addSelectionHandler(this);
		clientComposite.getName().addSelectionHandler(this);
		clientComposite.getPhone().addSelectionHandler(this);
		
		clientComposite.getId().addValueChangeHandler(this);
		VerticalPanel vpanel0=new VerticalPanel();
		InlineLabel lablel0=new InlineLabel("");
		
		vpanel0.add(lablel0);
		vpanel0.add(clientComposite);
		
		horizontalpanel1 = new HorizontalPanel();
		VerticalPanel vpanel11=new VerticalPanel();
		InlineLabel lablel11=new InlineLabel("");
		
		btnDownload = new Button("Download Attendance");
		btnDownload.getElement().addClassName("buttoncss");
		vpanel11.add(lablel11);
		vpanel11.add(btnDownload);
		
		horizontalpanel1.add(vpanel0);
		horizontalpanel1.add(vpanel11);
		
		olbbranch =new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbranch);
		olbbranch.getElement().getStyle().setHeight(26, Unit.PX);
		/**8-4-2019 added by Amol**/
		olbbranch.addChangeHandler(this);
	
//		olbsalesPerson =new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbsalesPerson);
//		olbsalesPerson.getElement().getStyle().setHeight(26, Unit.PX);
		
		dbMonth=new DateBoxWithYearSelector();		
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("MMM-yyyy");
		dbMonth.setFormat(new DateBox.DefaultFormat(dateFormat));
		btnGo=new Button("GO");
		
		olbProject = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(olbProject);
		/**8-4-2019 added by Amol**/
		olbProject.addChangeHandler(this);
		
		olbProject.getElement().getStyle().setHeight(26, Unit.PX);
		olbProject.getElement().getStyle().setWidth(240, Unit.PX);
		
		pic=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		
		horizontalpanel = new HorizontalPanel();
		
		
		VerticalPanel vpanel1=new VerticalPanel();
		VerticalPanel vpanel2=new VerticalPanel();
		VerticalPanel vpanel3=new VerticalPanel();
		VerticalPanel vpanel4=new VerticalPanel();
		VerticalPanel vpanel5=new VerticalPanel();
		
		InlineLabel lablel1=new InlineLabel("Branch");
		InlineLabel lablel2=new InlineLabel("Project");
		InlineLabel lablel3=new InlineLabel("Month");
		InlineLabel lablel4=new InlineLabel("");
		InlineLabel lablel5=new InlineLabel(". ");
		
		vpanel1.add(lablel1);
		vpanel1.add(olbbranch);
		
		vpanel2.add(lablel2);
		vpanel2.add(olbProject);
		
		vpanel3.add(lablel3);
		vpanel3.add(dbMonth);
		
		vpanel4.add(lablel4);
		vpanel4.add(pic);
		
		vpanel5.add(lablel5);
		vpanel5.add(btnGo);
		
		horizontalpanel.add(vpanel3);
		horizontalpanel.add(vpanel2);
		horizontalpanel.add(vpanel1);
		horizontalpanel.add(vpanel4);
		horizontalpanel.add(vpanel5);

//		horizontalpanel.add(olbbranch);
//		horizontalpanel.add(olbsalesPerson);
//		horizontalpanel.add(dbFromDate);
//		horizontalpanel.add(dbToDate);
//		horizontalpanel.add(btnGo);
		horizontalpanel.setSpacing(10);
		horizontalpanel.getElement().addClassName("technicianscheduleButtons");
		verticalpanel = new VerticalPanel();
		
		grid = new ListGrid();
		setGridProperty(grid);
		grid.setFields(getGridFields());
		
		ds= new DataSource();
		ds.setClientOnly(true);
		setDataSourceField(ds);
		grid.setDataSource(ds);
		
		verticalpanel.getElement().addClassName("varticalGridPanel");
		grid.getElement().addClassName("technicianSchedule");
		
		verticalpanel.add(grid);
		content.add(horizontalpanel1);
		content.add(horizontalpanel);
		content.add(verticalpanel);
		
		btnDownload.setVisible(false);
	}
	
	public void setDataToListGrid(ArrayList<AttendanceBean> list, int[] daysArray){
		System.out.println("FORM SETTING DATA....");
		
		if(daysArray.length>0){
			Console.log("INSIDE DAYS NOT NULL CONDITION setDataSourceField");
//			setGridFields(grid,daysArray);
			setDataSourceField(ds,daysArray);
		}
		
		ds.setTestData(getGridData(list));
		grid.setDataSource(ds);
//		grid.setFields(getGridFields());
		if(daysArray.length>0){
			Console.log("INSIDE DAYS NOT NULL CONDITION setGridFields");
			setGridFields(grid,daysArray);
		}
		grid.fetchData();
//		grid.redraw();
	}

	@Override
	public void applyStyle() {
		content.getElement().setId("formcontent");
		horizontalpanel.getElement().setId("form");
		horizontalpanel1.getElement().setId("form");
		verticalpanel.getElement().setId("form");
	}
	
	//grid property
	private static void setGridProperty(ListGrid grid) {
	   grid.setWidth("80%");//83
	   grid.setHeight("60%");//70
	   grid.setAutoFetchData(true);
	   grid.setAlternateRecordStyles(true);
	   grid.setShowAllRecords(true);
	   grid.setShowFilterEditor(true);
	   grid.setFilterOnKeypress(true);
	   
	   grid.setWrapCells(true);	
	   grid.setFixedRecordHeights(false);
//	   grid.setAutoFitData(Autofit.HORIZONTAL);	
//	   grid.setAutoFitData(Autofit.BOTH);
//	   grid.setAutoFitWidthApproach(AutoFitWidthApproach.BOTH);	
	   grid.setCanResizeFields(true);
	   
	   /**
	    * @author Anil
	    * @since 31-12-2020
	    * for resizing smartgrid
	    */
	   if(AppMemory.getAppMemory().enableMenuBar){
		   grid.setWidth("100%");
	   }
	}
	
	//fields
	private ListGridField[] getGridFields() {
	   ListGridField field1 = new ListGridField("EmpID","Employee Id");
	   field1.setWrap(true);
	   field1.setWidth("5%");//7%
	   field1.setAlign(Alignment.LEFT);
	   
	   ListGridField field2 = new ListGridField("EmployeeName","Employee Name");
	   field2.setWrap(true);
	   field2.setWidth("7%");
	   field2.setAlign(Alignment.LEFT);
	   
	   ListGridField field3 = new ListGridField("EmpDesignation","Emp Designation");
	   field3.setWrap(true);
	   field3.setWidth("7%");
	   field3.setAlign(Alignment.LEFT);
	   
	   ListGridField field4 = new ListGridField("ProjectName","Project Name");
	   field4.setWrap(true);
	   field4.setWidth("7%");
	   field4.setAlign(Alignment.LEFT);
	   
	   ListGridField field40 = new ListGridField("Shift","Shift");
	   field40.setWrap(true);
	   field40.setWidth("7%");
	   field40.setAlign(Alignment.LEFT);
	   
	   ListGridField field5 = new ListGridField("Month","Month");
	   field5.setWrap(true);
	   field5.setWidth("5%");
	   field5.setAlign(Alignment.LEFT);
	   
	   ListGridField field7 = new ListGridField("1","1");
//	   field7.setWrap(true);
	   field7.setAlign(Alignment.CENTER);
	   
	   ListGridField field8 = new ListGridField("2","2");
//	   field8.setWrap(true);
	   field8.setAlign(Alignment.CENTER);
	   
	   ListGridField field10 = new ListGridField("3","3"); 
//	   field10.setWrap(true);
	   field10.setAlign(Alignment.CENTER);
	   
	   ListGridField field11 = new ListGridField("4","4");
//	   field11.setWrap(true);
	   field11.setAlign(Alignment.CENTER);
	   
	   ListGridField field12 = new ListGridField("5","5");
//	   field12.setWrap(true);
	   field12.setAlign(Alignment.CENTER);
	   
	   ListGridField field13 = new ListGridField("6","6");
//	   field13.setWrap(true);
	   field13.setAlign(Alignment.CENTER);
	   
	   ListGridField field14 = new ListGridField("7","7");
//	   field14.setWrap(true);
	   field14.setAlign(Alignment.CENTER);
	   
	   ListGridField field15 = new ListGridField("8","8");
//	   field15.setWrap(true);
	   field15.setAlign(Alignment.CENTER);
	   
	   ListGridField field16 = new ListGridField("9","9"); 
//	   field16.setWrap(true);
	   field16.setAlign(Alignment.CENTER);
	   
	   ListGridField field17 = new ListGridField("10","10");
//	   field17.setWrap(true);
	   field17.setAlign(Alignment.CENTER);
	   
	   ListGridField field18 = new ListGridField("11","11");
//	   field18.setWrap(true);
	   field18.setAlign(Alignment.CENTER);
	   
	   ListGridField field19 = new ListGridField("12","12");
//	   field19.setWrap(true);
	   field19.setAlign(Alignment.CENTER);
	   
	   ListGridField field20 = new ListGridField("13","13");
//	   field20.setWrap(true);
	   field20.setAlign(Alignment.CENTER);
	   
	   ListGridField field21 = new ListGridField("14","14");
//	   field21.setWrap(true);
	   field21.setAlign(Alignment.CENTER);
	   
	   ListGridField field22 = new ListGridField("15","15"); 
//	   field22.setWrap(true);
	   field22.setAlign(Alignment.CENTER);
	   
	   ListGridField field23 = new ListGridField("16","16");
//	   field23.setWrap(true);
	   field23.setAlign(Alignment.CENTER);
	   
	   ListGridField field24 = new ListGridField("17","17");
//	   field24.setWrap(true);
	   field24.setAlign(Alignment.CENTER);
	   
	   ListGridField field25 = new ListGridField("18","18");
//	   field25.setWrap(true);
	   field25.setAlign(Alignment.CENTER);
	   
	   ListGridField field26 = new ListGridField("19","19");
//	   field26.setWrap(true);
	   field26.setAlign(Alignment.CENTER);
	   
	   ListGridField field27 = new ListGridField("20","20");
//	   field27.setWrap(true);
	   field27.setAlign(Alignment.CENTER);
	   
	   ListGridField field28 = new ListGridField("21","21"); 
//	   field28.setWrap(true);
	   field28.setAlign(Alignment.CENTER);
	   
	   ListGridField field29 = new ListGridField("22","22");
//	   field29.setWrap(true);
	   field29.setAlign(Alignment.CENTER);
	   
	   ListGridField field30 = new ListGridField("23","23");
//	   field30.setWrap(true);
	   field30.setAlign(Alignment.CENTER);
	   
	   ListGridField field31 = new ListGridField("24","24");
//	   field31.setWrap(true);
	   field31.setAlign(Alignment.CENTER);
	   
	   ListGridField field32 = new ListGridField("25","25");
//	   field32.setWrap(true);
	   field32.setAlign(Alignment.CENTER);
	   
	   ListGridField field33 = new ListGridField("26","26");
//	   field33.setWrap(true);
	   field33.setAlign(Alignment.CENTER);
	   
	   ListGridField field34 = new ListGridField("27","27");
//	   field34.setWrap(true);
	   field34.setAlign(Alignment.CENTER);
	   
	   ListGridField field35 = new ListGridField("28","28");
//	   field35.setWrap(true);
	   field35.setAlign(Alignment.CENTER);
	   
	   ListGridField field36 = new ListGridField("29","29");
//	   field36.setWrap(true);
	   field36.setAlign(Alignment.CENTER);
	   
	   ListGridField field37 = new ListGridField("30","30");
//	   field37.setWrap(true);
	   field37.setAlign(Alignment.CENTER);
	   
	   ListGridField field38 = new ListGridField("31","31");
//	   field38.setWrap(true);
	   field38.setAlign(Alignment.CENTER);
	   
	   /**
	    * @author Anil,Date :24-01-2019
	    * Added total OT hours column
	    * 
	    */
	   ListGridField field39 = new ListGridField("otHours","Total OT Hours");
	   field39.setWrap(true);
	   field39.setWidth("5%");
	   field39.setAlign(Alignment.CENTER);
	   
	   /**
	    * @author Ashwini Patil
	    * Date :7-05-2022
	    * Added total working days column and Total leaves column
	    */
	   ListGridField field41 = new ListGridField("workedDays","Total Worked Days");
	   field41.setWidth("5%");
	   field41.setWrap(true);
	   field41.setAlign(Alignment.CENTER);
	   
	   ListGridField field42 = new ListGridField("leaveDays","Total Leave Days");
	   field42.setWidth("5%"); //added later
	   field42.setWrap(true);
	   field42.setAlign(Alignment.CENTER);
	   
	   ListGridField field4a = new ListGridField("siteLocation","Site Location");
	   field4a.setWrap(true);
	   field4a.setWidth("7%");
	   field4a.setAlign(Alignment.CENTER);

	   
	   if(siteLocation){
		   return new ListGridField[] { 
				   field1, field2 ,field3,field4,field4a,field40,field5,field7,field8,
				   field10,field11,field12,field13,field14,field15,field16,field17,
				   field18,field19,field20,field21,field22,field23,field24,field25,
				   field26,field27,field28,field29, field30,field31, field32,field33,field34,field35, field36,field37,field38,field39,field41,field42
				   };
	   }else{
		   return new ListGridField[] { 
				   field1, field2 ,field3,field4,field40,field5,field7,field8,
				   field10,field11,field12,field13,field14,field15,field16,field17,
				   field18,field19,field20,field21,field22,field23,field24,field25,
				   field26,field27,field28,field29, field30,field31, field32,field33,field34,field35, field36,field37,field38,field39,field41,field42
				   }; 
	   }
	}
	
	//data source field
	protected static void setDataSourceField(DataSource dataSource) {
	   DataSourceField field1 = new DataSourceField("EmpID", FieldType.TEXT);
	   DataSourceField field2 = new DataSourceField("EmployeeName", FieldType.TEXT);
	   DataSourceField field3 = new DataSourceField("EmpDesignation", FieldType.TEXT);
	   DataSourceField field4 = new DataSourceField("ProjectName", FieldType.TEXT);
	   DataSourceField field51 = new DataSourceField("Shift", FieldType.TEXT);
	   DataSourceField field5 = new DataSourceField("Month", FieldType.TEXT);
	  // DataSourceField field6 = new DataSourceField("BillingAddState", FieldType.TEXT);
	   DataSourceField field7 = new DataSourceField("1", FieldType.TEXT);
	   DataSourceField field8 = new DataSourceField("2", FieldType.TEXT);
	   DataSourceField field10 = new DataSourceField("3", FieldType.TEXT);
	   DataSourceField field11 = new DataSourceField("4", FieldType.TEXT);
	   DataSourceField field12 = new DataSourceField("5", FieldType.TEXT);
	   DataSourceField field13 = new DataSourceField("6", FieldType.TEXT);
	   DataSourceField field14 = new DataSourceField("7", FieldType.TEXT); 
	   DataSourceField field15 = new DataSourceField("8" , FieldType.TEXT);
	   DataSourceField field16 = new DataSourceField("9",FieldType.TEXT); 
	   DataSourceField field17 = new DataSourceField("10",FieldType.TEXT);
	   DataSourceField field18 = new DataSourceField("11",FieldType.TEXT);
	   DataSourceField field19 = new DataSourceField("12",FieldType.TEXT);
	   DataSourceField field20 = new DataSourceField("13",FieldType.TEXT);	   
	   DataSourceField field21 = new DataSourceField("14",FieldType.TEXT);
	   DataSourceField field22 = new DataSourceField("15",FieldType.TEXT); 
	   DataSourceField field23 = new DataSourceField("16",FieldType.TEXT);	   
	   DataSourceField field24 = new DataSourceField("17",FieldType.TEXT);	 
	   DataSourceField field25 = new DataSourceField("18",FieldType.TEXT);
	   DataSourceField field26 = new DataSourceField("19",FieldType.TEXT);	  
	   DataSourceField field27 = new DataSourceField("20",FieldType.TEXT);	   
	   DataSourceField field28 = new DataSourceField("21",FieldType.TEXT); 	 
	   DataSourceField field29 = new DataSourceField("22",FieldType.TEXT);	   
	   DataSourceField field30 = new DataSourceField("23",FieldType.TEXT);	   
	   DataSourceField field31 = new DataSourceField("24",FieldType.TEXT);	
	   DataSourceField field32 = new DataSourceField("25",FieldType.TEXT);	   
	   DataSourceField field33 = new DataSourceField("26",FieldType.TEXT);	 
	   DataSourceField field34 = new DataSourceField("27",FieldType.TEXT); 
	   DataSourceField field35 = new DataSourceField("28",FieldType.TEXT);	
	   DataSourceField field36 = new DataSourceField("29",FieldType.TEXT);	   
	   DataSourceField field37 = new DataSourceField("30",FieldType.TEXT);
	   DataSourceField field38 = new DataSourceField("31",FieldType.TEXT);
	   /**
	    * @author Anil,Date :24-01-2019
	    * Added total OT hours column
	    */
	   DataSourceField field39 = new DataSourceField("otHours",FieldType.TEXT);
	   
	   /**
	    * @author Ashwini Patil
	    * Date :07-05-2022
	    * Added total worked days and leave days
	    */
	   DataSourceField field41 = new DataSourceField("workedDays",FieldType.TEXT);
	   DataSourceField field42 = new DataSourceField("leaveDays",FieldType.TEXT);
	   
	   DataSourceField field4a = new DataSourceField("siteLocation", FieldType.TEXT);
	   
	   dataSource.setFields(field1, field2 ,field3,field4,field4a,field51 ,field5,field7,field8,
			   field10,field11,field12,field13,field14,field15,field16,field17,
			   field18,field19,field20,field21,field22,field23,field24,field25,
			   field26,field27,field28,field29, field30,field31, field32,field33,field34,field35, field36,field37,field38,field39,field41,field42);
	}
	
	
	 private ListGridRecord[] getGridData(ArrayList<AttendanceBean> attendaceList) {
		 DateTimeFormat dtf = DateTimeFormat.getFormat("dd");

		 ListGridRecord[] records = null; 
		 records = new ListGridRecord[attendaceList.size()];
		 for(int i=0;i<attendaceList.size();i++){
			 ListGridRecord record = new ListGridRecord();
			 AttendanceBean attBean = attendaceList.get(i);
			 record.setAttribute("EmpID", attBean.getEmpId());
			 
				if(attBean.getEmployeeName()!=null){
					 record.setAttribute("EmployeeName", attBean.getEmployeeName());
				}else{
					 record.setAttribute("EmployeeName", "");
				}
				
				if(attBean.getEmpDesignation()!=null){
					record.setAttribute("EmpDesignation", attBean.getEmpDesignation());
				}else{
					record.setAttribute("EmpDesignation", "");
				}
				
				if(attBean.getProjectName()!=null){
					record.setAttribute("ProjectName", attBean.getProjectName());
				}else{
					record.setAttribute("ProjectName", "");
				}
			
				if(attBean.getSiteLocation()!=null){
					record.setAttribute("siteLocation", attBean.getSiteLocation());
				}else{
					record.setAttribute("siteLocation", "");
				}
				
				if(attBean.getShift()!=null){
					record.setAttribute("Shift", attBean.getShift());
				}else{
					record.setAttribute("Shift", "");
				}
				
				if(attBean.getOvertimeType()!=null){
					record.setAttribute("Month", attBean.getMonth());
				}else{
					record.setAttribute("Month", "");
				}
				
	            int j=1;
				for (Map.Entry<Date, Attendance> entry : attBean.getAttendanceObjectMap().entrySet()) {
				    Date key = entry.getKey();
				    String value = entry.getValue().getGroup();
				    int day = Integer.parseInt(dtf.format(key));
//				    System.out.println("j =" + j +" "+"date ="+day+" "+value);
				    com.slicktechnologies.client.utils.Console.log("j =" + j +" "+"date ="+day+" "+value);
//				    if(j == day){
//				    	record.setAttribute(j+"", value);
//				    }else{
//				    	record.setAttribute(j+"", "");
//				    }
					record.setAttribute(day+"", value);
				    j++;
				}
				/**
				    * @author Anil,Date :24-01-2019
				    * Added total OT hours column
				    * 
				    */
				if(attBean.getOvertimeType()!=null){
					record.setAttribute("otHours", attBean.getOvertimeType());
				}else{
					record.setAttribute("otHours", "");
				}
				
				/**
				    * @author Ashwini Patil,Date :8-05-2022
				    * Added workedDays and leaveDays column
				    * 
				    */
				if(attBean.getWorkedDays()!=null){
					record.setAttribute("workedDays", attBean.getWorkedDays());
				}else{
					record.setAttribute("workedDays", "0");
				}
				if(attBean.getLeaveDays()!=null){
					record.setAttribute("leaveDays", attBean.getLeaveDays());
				}else{
					record.setAttribute("leaveDays", "0");
				}
				 records[i]=record;
			}
		 
//		 for(int i=0;i<attendaceList.size();i++){
//			 ListGridRecord record = new ListGridRecord();
//			 Customer customer=customerlist.get(i);
//			 s
//			 record.setAttribute("EmpID", customer.getCount());
//			 if(customer.isCompany()){
//				 record.setAttribute("CustomerName", customer.getCompanyName());
//				 record.setAttribute("POCName", customer.getFullname());
//			 }else{
//				 record.setAttribute("CustomerName", customer.getFullname());
//				 record.setAttribute("POCName", customer.getFullname());
//			 }
//			
//			 record.setAttribute("CustomerCell", customer.getCellNumber1());
//			 String billingAddress = customer.getAdress().getAddrLine1()+" "+customer.getAdress().getAddrLine2()+" "+customer.getAdress().getLandmark()+" "+customer.getAdress().getLocality() +" "+ customer.getAdress().getPin();
//			 System.out.println("billing Address =="+billingAddress);
//			 record.setAttribute("BillingAddress", billingAddress);
//			 record.setAttribute("BillingAddState",customer.getAdress().getState());
//			 record.setAttribute("BillingAddCity", customer.getAdress().getCity());
//			 record.setAttribute("BillingAddLocality", customer.getAdress().getLocality());
////			 record.setAttribute("BillingAddPin", customer.getAdress().getPin());
//			 String serviceAddress = customer.getSecondaryAdress().getAddrLine1()+" "+customer.getSecondaryAdress().getAddrLine2()+" "+customer.getSecondaryAdress().getLandmark()+" "+customer.getSecondaryAdress().getLocality()+" "+customer.getSecondaryAdress().getPin();
//			 record.setAttribute("ServiceAddress", serviceAddress);
//			 record.setAttribute("State", customer.getSecondaryAdress().getState());
//			 record.setAttribute("City", customer.getSecondaryAdress().getCity());
//			 record.setAttribute("Locality", customer.getSecondaryAdress().getLocality());
////			 record.setAttribute("Pin", customer.getSecondaryAdress().getPin());
	//		 records[i]=record;
//		 }
		 
		 Console.log("RECORDS LENGTH : "+records.length);
		 
		 return records;
	}
	 
	 /**
		 * Toggles the app header bar menus as per screen state
		 */
		public void toggleAppHeaderBarMenu() {
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download")||text.equals("Print"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		  		
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download")||text.equals("Print"))
						menus[k].setVisible(true); 
					else
						menus[k].setVisible(false);  		   
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download")||text.equals("Print"))
						menus[k].setVisible(true); 
					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.SERVICE,LoginPresenter.currentModule.trim());
		}
		public ListGrid getGrid() {
			return grid;
		}

		public void setGrid(ListGrid grid) {
			this.grid = grid;
		}

		public DataSource getDs() {
			return ds;
		}

		public void setDs(DataSource ds) {
			this.ds = ds;
		}

		public ObjectListBox<Branch> getOlbbranch() {
			return olbbranch;
		}

		public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
			this.olbbranch = olbbranch;
		}

		public DateBox getDbMonth() {
			return dbMonth;
		}

		public void setDbMonth(DateBox dbMonth) {
			this.dbMonth = dbMonth;
		}

		public Button getBtnGo() {
			return btnGo;
		}

		public void setBtnGo(Button btnGo) {
			this.btnGo = btnGo;
		}

		public ObjectListBox<HrProject> getOlbProject() {
			return olbProject;
		}

		public void setOlbProject(ObjectListBox<HrProject> olbProject) {
			this.olbProject = olbProject;
		}

		public EmployeeInfoComposite getPic() {
			return pic;
		}

		public void setPic(EmployeeInfoComposite pic) {
			this.pic = pic;
		}

		

		@Override
		public void onChange(ChangeEvent event) {
			if(event.getSource().equals(olbProject)){
					retriveBranch();
			}
			
			if(event.getSource().equals(olbbranch)){
				if(olbbranch.getSelectedIndex()==0){
					olbProject.setListItems(LoginPresenter.globalHRProject);
				}else{
					retriveProject();
				}
			}
       }

	private void retriveProject() {

		MyQuerry query = new MyQuerry();
		Company c = new Company();
		System.out.println("Company Id :: " + c.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("branch");
		filter.setStringValue(olbbranch.getValue(olbbranch.getSelectedIndex()));
		filtervec.add(filter);
		query.setFilters(filtervec);
		query.setQuerryObject(new HrProject());

		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<HrProject> list = new ArrayList<HrProject>();
				for (SuperModel model : result) {
					HrProject entity = (HrProject) model;
					list.add(entity);
				}
				olbProject.setListItems(list);
			}
		});
	}

		private void retriveBranch() {
//			HrProject project =  olbProject.getSelectedItem();
//			olbbranch.clear();
//			olbbranch.addItem("--SELECT--");
//			olbbranch.addItem(project.getBranch());
			HrProject projects =  olbProject.getSelectedItem();
			olbbranch.setValue(projects.getBranch());
		}

		public Button getBtnDownload() {
			return btnDownload;
		}

		public void setBtnDownload(Button btnDownload) {
			this.btnDownload = btnDownload;
		}
		
	public void setDataSourceField(DataSource dataSource, int[] daysArray) {
		Console.log("setDataSourceField called");
		DataSourceField field1 = new DataSourceField("EmpID", FieldType.TEXT);
		DataSourceField field2 = new DataSourceField("EmployeeName",FieldType.TEXT);
		DataSourceField field3 = new DataSourceField("EmpDesignation",FieldType.TEXT);
		DataSourceField field4 = new DataSourceField("ProjectName",FieldType.TEXT);
		DataSourceField field51 = new DataSourceField("Shift", FieldType.TEXT);
		DataSourceField field5 = new DataSourceField("Month", FieldType.TEXT);
		
		DataSourceField field5a = new DataSourceField("siteLocation",FieldType.TEXT);
		
		DataSourceField[] dsArray=null;
		int index=6;
		if(daysArray.length!=0){
			if(siteLocation){
				index=7;
				dsArray=new DataSourceField[daysArray.length+10];
			}else{
				dsArray=new DataSourceField[daysArray.length+9];
			}
			
			dsArray[0]=field1;
			dsArray[1]=field2;
			dsArray[2]=field3;
			dsArray[3]=field4;
			if(siteLocation){
				dsArray[4]=field5a;
				dsArray[5]=field51;
				dsArray[6]=field5;
			}else{
				dsArray[4]=field51;
				dsArray[5]=field5;
			}
		}
		
		for(int i =0;i<daysArray.length;i++){
			DataSourceField field = new DataSourceField(daysArray[i]+"", FieldType.TEXT);
			dsArray[i+index]=field;
		}
		
		DataSourceField field7 = new DataSourceField("1", FieldType.TEXT);
		DataSourceField field8 = new DataSourceField("2", FieldType.TEXT);
		DataSourceField field10 = new DataSourceField("3", FieldType.TEXT);
		DataSourceField field11 = new DataSourceField("4", FieldType.TEXT);
		DataSourceField field12 = new DataSourceField("5", FieldType.TEXT);
		DataSourceField field13 = new DataSourceField("6", FieldType.TEXT);
		DataSourceField field14 = new DataSourceField("7", FieldType.TEXT);
		DataSourceField field15 = new DataSourceField("8", FieldType.TEXT);
		DataSourceField field16 = new DataSourceField("9", FieldType.TEXT);
		DataSourceField field17 = new DataSourceField("10", FieldType.TEXT);
		DataSourceField field18 = new DataSourceField("11", FieldType.TEXT);
		DataSourceField field19 = new DataSourceField("12", FieldType.TEXT);
		DataSourceField field20 = new DataSourceField("13", FieldType.TEXT);
		DataSourceField field21 = new DataSourceField("14", FieldType.TEXT);
		DataSourceField field22 = new DataSourceField("15", FieldType.TEXT);
		DataSourceField field23 = new DataSourceField("16", FieldType.TEXT);
		DataSourceField field24 = new DataSourceField("17", FieldType.TEXT);
		DataSourceField field25 = new DataSourceField("18", FieldType.TEXT);
		DataSourceField field26 = new DataSourceField("19", FieldType.TEXT);
		DataSourceField field27 = new DataSourceField("20", FieldType.TEXT);
		DataSourceField field28 = new DataSourceField("21", FieldType.TEXT);
		DataSourceField field29 = new DataSourceField("22", FieldType.TEXT);
		DataSourceField field30 = new DataSourceField("23", FieldType.TEXT);
		DataSourceField field31 = new DataSourceField("24", FieldType.TEXT);
		DataSourceField field32 = new DataSourceField("25", FieldType.TEXT);
		DataSourceField field33 = new DataSourceField("26", FieldType.TEXT);
		DataSourceField field34 = new DataSourceField("27", FieldType.TEXT);
		DataSourceField field35 = new DataSourceField("28", FieldType.TEXT);
		DataSourceField field36 = new DataSourceField("29", FieldType.TEXT);
		DataSourceField field37 = new DataSourceField("30", FieldType.TEXT);
		DataSourceField field38 = new DataSourceField("31", FieldType.TEXT);
		
		DataSourceField field39 = new DataSourceField("otHours", FieldType.TEXT);
		DataSourceField field41 = new DataSourceField("workedDays",FieldType.TEXT);
		DataSourceField field42 = new DataSourceField("leaveDays",FieldType.TEXT);
		   
		dsArray[dsArray.length-3]=field39;
		dsArray[dsArray.length-2]=field41;
		dsArray[dsArray.length-1]=field42;
//		Console.log("dsArray.length after adding new fields="+dsArray.length);
		if(daysArray.length!=0){
			Console.log("UPDATED DATA SOURCE "+dsArray.length);
			dataSource.setFields(dsArray);
		}else{
			if(siteLocation){
				dataSource.setFields(field1, field2, field3, field4, field51, field5a,field5,
						field7, field8, field10, field11, field12, field13, field14,
						field15, field16, field17, field18, field19, field20, field21,
						field22, field23, field24, field25, field26, field27, field28,
						field29, field30, field31, field32, field33, field34, field35,
						field36, field37, field38, field39, field41, field42);
			}else{
				dataSource.setFields(field1, field2, field3, field4, field51, field5,
						field7, field8, field10, field11, field12, field13, field14,
						field15, field16, field17, field18, field19, field20, field21,
						field22, field23, field24, field25, field26, field27, field28,
						field29, field30, field31, field32, field33, field34, field35,
						field36, field37, field38, field39, field41, field42);
			}
			
		}
	}
	
	
	private ListGridField[] setGridFields(ListGrid grid, int[] daysArray) {

		ListGridField[] gridArray=null;
		
		ListGridField field1 = new ListGridField("EmpID", "Employee Id");
		field1.setWrap(true);
		field1.setWidth("5%");//7%
		field1.setAlign(Alignment.LEFT);

		ListGridField field2 = new ListGridField("EmployeeName","Employee Name");
		field2.setWrap(true);
		field2.setWidth("7%");
		field2.setAlign(Alignment.LEFT);

		ListGridField field3 = new ListGridField("EmpDesignation",
				"Emp Designation");
		field3.setWrap(true);
		field3.setWidth("7%");
		field3.setAlign(Alignment.LEFT);

		ListGridField field4 = new ListGridField("ProjectName", "Project Name");
		field4.setWrap(true);
		field4.setWidth("7%");
		field4.setAlign(Alignment.LEFT);

		ListGridField field40 = new ListGridField("Shift", "Shift");
		field40.setWrap(true);
		field40.setWidth("7%");
		field40.setAlign(Alignment.LEFT);

		ListGridField field5 = new ListGridField("Month", "Month");
		field5.setWrap(true);
		field5.setWidth("5%");
		field5.setAlign(Alignment.LEFT);
		
		ListGridField field4a = new ListGridField("siteLocation", "Site Location");
		field4a.setWrap(true);
		field4a.setWidth("7%");
		field4a.setAlign(Alignment.LEFT);
		
		int index=6; 
		if(daysArray.length!=0){
			if(siteLocation){
				index=7;
				gridArray=new ListGridField[daysArray.length+10];
			}else{
				gridArray=new ListGridField[daysArray.length+9];
			}
			
			gridArray[0]=field1;
			gridArray[1]=field2;
			gridArray[2]=field3;
			gridArray[3]=field4;
			if(siteLocation){
				gridArray[4]=field4a;
				gridArray[5]=field40;
				gridArray[6]=field5;
			}else{
				gridArray[4]=field40;
				gridArray[5]=field5;
			}
			
		}
		
		for(int i =0;i<daysArray.length;i++){
			ListGridField field = new ListGridField(daysArray[i]+"", daysArray[i]+"");
			field.setWrap(true);
			field.setAlign(Alignment.CENTER);
			gridArray[i+index]=field;
		}
		

		ListGridField field7 = new ListGridField("1", "1");
		field7.setWidth("3%");
		field7.setWrap(true);
		field7.setAlign(Alignment.CENTER);

		ListGridField field8 = new ListGridField("2", "2");
		field8.setWidth("3%");
		field8.setWrap(true);
		field8.setAlign(Alignment.CENTER);

		ListGridField field10 = new ListGridField("3", "3");
		field10.setWidth("3%");
		field10.setWrap(true);
		field10.setAlign(Alignment.CENTER);

		ListGridField field11 = new ListGridField("4", "4");
		field11.setWidth("3%");
		field11.setWrap(true);
		field11.setAlign(Alignment.CENTER);

		ListGridField field12 = new ListGridField("5", "5");
		field12.setWidth("3%");
		field12.setWrap(true);
		field12.setAlign(Alignment.CENTER);

		ListGridField field13 = new ListGridField("6", "6");
		field13.setWidth("3%");
		field13.setWrap(true);
		field13.setAlign(Alignment.CENTER);

		ListGridField field14 = new ListGridField("7", "7");
		field14.setWidth("3%");
		field14.setWrap(true);
		field14.setAlign(Alignment.CENTER);

		ListGridField field15 = new ListGridField("8", "8");
		field15.setWidth("3%");
		field15.setWrap(true);
		field15.setAlign(Alignment.CENTER);

		ListGridField field16 = new ListGridField("9", "9");
		field16.setWidth("3%");
		field16.setWrap(true);
		field16.setAlign(Alignment.CENTER);

		ListGridField field17 = new ListGridField("10", "10");
		field17.setWidth("3%");
		field17.setWrap(true);
		field17.setAlign(Alignment.CENTER);

		ListGridField field18 = new ListGridField("11", "11");
		field18.setWidth("3%");
		field18.setWrap(true);
		field18.setAlign(Alignment.CENTER);

		ListGridField field19 = new ListGridField("12", "12");
		field19.setWidth("3%");
		field19.setWrap(true);
		field19.setAlign(Alignment.CENTER);

		ListGridField field20 = new ListGridField("13", "13");
		field20.setWidth("3%");
		field20.setWrap(true);
		field20.setAlign(Alignment.CENTER);

		ListGridField field21 = new ListGridField("14", "14");
		field21.setWidth("3%");
		field21.setWrap(true);
		field21.setAlign(Alignment.CENTER);

		ListGridField field22 = new ListGridField("15", "15");
		field22.setWidth("3%");
		field22.setWrap(true);
		field22.setAlign(Alignment.CENTER);

		ListGridField field23 = new ListGridField("16", "16");
		field23.setWidth("3%");
		field23.setWrap(true);
		field23.setAlign(Alignment.CENTER);

		ListGridField field24 = new ListGridField("17", "17");
		field24.setWidth("3%");
		field24.setWrap(true);
		field24.setAlign(Alignment.CENTER);

		ListGridField field25 = new ListGridField("18", "18");
		field25.setWidth("3%");
		field25.setWrap(true);
		field25.setAlign(Alignment.CENTER);

		ListGridField field26 = new ListGridField("19", "19");
		field26.setWidth("3%");
		field26.setWrap(true);
		field26.setAlign(Alignment.CENTER);

		ListGridField field27 = new ListGridField("20", "20");
		field27.setWidth("3%");
		field27.setWrap(true);
		field27.setAlign(Alignment.CENTER);

		ListGridField field28 = new ListGridField("21", "21");
		field28.setWidth("3%");
		field28.setWrap(true);
		field28.setAlign(Alignment.CENTER);

		ListGridField field29 = new ListGridField("22", "22");
		field29.setWidth("3%");
		field29.setWrap(true);
		field29.setAlign(Alignment.CENTER);

		ListGridField field30 = new ListGridField("23", "23");
		field30.setWidth("3%");
		field30.setWrap(true);
		field30.setAlign(Alignment.CENTER);

		ListGridField field31 = new ListGridField("24", "24");
		field31.setWidth("3%");
		field31.setWrap(true);
		field31.setAlign(Alignment.CENTER);

		ListGridField field32 = new ListGridField("25", "25");
		field32.setWidth("3%");
		field32.setWrap(true);
		field32.setAlign(Alignment.CENTER);

		ListGridField field33 = new ListGridField("26", "26");
		field33.setWidth("3%");
		field33.setWrap(true);
		field33.setAlign(Alignment.CENTER);

		ListGridField field34 = new ListGridField("27", "27");
		field34.setWidth("3%");
		field34.setWrap(true);
		field34.setAlign(Alignment.CENTER);

		ListGridField field35 = new ListGridField("28", "28");
		field35.setWidth("3%");
		field35.setWrap(true);
		field35.setAlign(Alignment.CENTER);

		ListGridField field36 = new ListGridField("29", "29");
		field36.setWidth("3%");
//		field36.setWrap(true);
		field36.setAlign(Alignment.CENTER);

		ListGridField field37 = new ListGridField("30", "30");
		field37.setWidth("3%");
//		field37.setWrap(true);
		field37.setAlign(Alignment.CENTER);

		ListGridField field38 = new ListGridField("31", "31");
		field38.setWidth("3%");
//		field38.setWrap(true);
		field38.setAlign(Alignment.CENTER);

		/**
		 * @author Anil,Date :24-01-2019 Added total OT hours column
		 * 
		 */
		ListGridField field39 = new ListGridField("otHours", "Total OT Hours");
		field39.setWrap(true);
		field39.setWidth("5%");
		field39.setAlign(Alignment.CENTER);
		
		gridArray[gridArray.length-3]=field39;
		
		
		/**
		 * @author Ashwini Patil,Date :07-05-2022 Added worked days and leave days column
		 */
		ListGridField field41 = new ListGridField("workedDays","Total Worked Days");
		field41.setWrap(true);
		field41.setWidth("5%");
		field41.setAlign(Alignment.CENTER);
		   
		ListGridField field42 = new ListGridField("leaveDays","Total Leave Days");
		field42.setWrap(true);
		field42.setWidth("5%");
		field42.setAlign(Alignment.CENTER);
		
		
		gridArray[gridArray.length-2]=field41;//changed here
		gridArray[gridArray.length-1]=field42;
		
		if(daysArray.length!=0){
			Console.log("UPDATED DATA GRID "+gridArray.length);
			grid.setFields(gridArray);
		}else{

			grid.setFields(field1, field2, field3, field4, field40, field5, field7,
					field8, field10, field11, field12, field13, field14, field15,
					field16, field17, field18, field19, field20, field21, field22,
					field23, field24, field25, field26, field27, field28, field29,
					field30, field31, field32, field33, field34, field35, field36,
					field37, field38, field39);//, field41, field42
		}

		return new ListGridField[] { field1, field2, field3, field4, field40,
				field5, field7, field8, field10, field11, field12, field13,
				field14, field15, field16, field17, field18, field19, field20,
				field21, field22, field23, field24, field25, field26, field27,
				field28, field29, field30, field31, field32, field33, field34,
				field35, field36, field37, field38, field39};//, field41, field42
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		// TODO Auto-generated method stub
		if (event.getSource().equals(clientComposite.getId())
				|| event.getSource().equals(clientComposite.getName())
				|| event.getSource().equals(clientComposite.getPhone())) {
			Console.log("Customer Selection...");
			if(clientComposite.getValue()!=null){
				if(clientComposite.getIdValue()!=-1){
					List<HrProject> hrProjectList=new ArrayList<HrProject>();
					for(HrProject project:LoginPresenter.globalHRProject){
						if(project.getPersoninfo()!=null&&project.getPersoninfo().getCount()==clientComposite.getIdValue()){
							hrProjectList.add(project);
						}
					}
					olbProject.setListItems(hrProjectList);
				}else{
					olbProject.setListItems(LoginPresenter.globalHRProject);
				}
			}else{
				olbProject.setListItems(LoginPresenter.globalHRProject);
			}
		}
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		// TODO Auto-generated method stub
		if(event.getSource()==clientComposite.getId()){
			if(clientComposite.getIdValue()==-1){
				olbProject.setListItems(LoginPresenter.globalHRProject);
			}
		}
	}
		
		
}
