package com.slicktechnologies.client.views.humanresource.attendancelist;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;





import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.timebox.TimeBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.calendar.CalendarPresenter;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar.WeekleyOff;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.smartgwt.client.util.SC;

public class AttandanceUpdatePopup extends PopupScreen implements ValueChangeHandler<Double>, ChangeHandler{
	
	
//	Logger logger = Logger.getLogger("NameOfYourLogger");
	TextBox tbMonth;
	ObjectListBox<HrProject> oblProject;
	ObjectListBox<Branch> oblBranch;
	
	TextBox tbEmpId;
	TextBox tbEmpName;
	TextBox tbEmpDesignation;
	TextBox tbActionLabel;
	
	DateBox dbAttandanceDate;
	
	
	TextBox tmInTime;
	TextBox tmOutTime;
	
//	TimeBox tmInTime;
//	TimeBox tmOutTime;
	
	/***28-12-2018 Deepak Salve added this code for Location mark in and Mark out***/
	TextArea markInAddress;
	TextArea markOutAddress;
	/***End***/
	
	DoubleBox dbTotalWorkedHrs;
	DoubleBox dbLeaveHours;
	ObjectListBox<AllocatedLeaves> OblLeaveType;
	
	DoubleBox dbOtHours;
	ObjectListBox<Overtime> oblOvertime;
	
	ArrayList<AllocatedLeaves> leaveAlloc=new ArrayList<AllocatedLeaves>();
	Attendance attendance;
	EmployeeInfo employeeInfo;
	LeaveBalance leaveBalance;
	
	
	/**
	 * @author Anil,Date : 06-02-2019
	 * Adding weekly off and holiday radiobutton 
	 * for marking attendance from system only
	 * for EVA raised by Nitin Sir
	 */
	RadioButton rbWeeklyOff;
	RadioButton rbHoliday;
	
	public AttandanceUpdatePopup() {
		super();
		getPopup().setWidth("800px");
		createGui();
		rbWeeklyOff.setValue(false);
		rbHoliday.setValue(false);
		popup.setAutoHideEnabled(false);
	}
	
	
	private void initializeWidget(){
		employeeInfo=null;
		leaveBalance=null;
		tbMonth=new TextBox();    
		tbMonth.setEnabled(false);
		oblProject=new ObjectListBox<HrProject>(); 
		HrProject.MakeProjectListBoxLive(oblProject);
//		oblProject.setEnabled(false);
		oblBranch=new ObjectListBox<Branch>();  
		AppUtility.makeBranchListBoxLive(oblBranch);
		oblBranch.setEnabled(false);
		                                     
		tbEmpId=new TextBox();
		tbEmpId.setEnabled(false);
		tbEmpName=new TextBox();   
		tbEmpName.setEnabled(false);
		tbEmpDesignation=new TextBox(); 
		tbEmpDesignation.setEnabled(false);
		
		tmInTime=new TextBox(); 
		tmOutTime=new TextBox();
		tmInTime.setEnabled(false);
		tmOutTime.setEnabled(false);
//		tmInTime=new TimeBox(); 
//		tmOutTime=new TimeBox();
//		tmInTime=new TimeBox(new Date());                            
//		tmOutTime=new TimeBox(new Date());                           
		dbTotalWorkedHrs=new DoubleBox(); 
		
		dbAttandanceDate=new DateBoxWithYearSelector();  
		dbAttandanceDate.setEnabled(false);
		
		dbLeaveHours=new DoubleBox();                      
		OblLeaveType=new ObjectListBox<AllocatedLeaves>();                       
		                                     
		dbOtHours=new DoubleBox();  
		
		oblOvertime=new ObjectListBox<Overtime>(); 
		Filter temp=new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		MyQuerry querry=new MyQuerry();
		Vector<Filter> vecFil=new Vector<Filter>();
		vecFil.add(temp);
		querry.setFilters(vecFil);
		querry.setQuerryObject(new Overtime());
		
		oblOvertime=new ObjectListBox<Overtime>();
//		oblOvertime.MakeLive(querry);
		
		
		tbActionLabel=new TextBox();  
		tbActionLabel.setEnabled(false);
		
		dbTotalWorkedHrs.addValueChangeHandler(this);
		dbLeaveHours.addValueChangeHandler(this);
		dbOtHours.addValueChangeHandler(this);
		
		oblProject.addChangeHandler(this);
		
		OblLeaveType.addChangeHandler(this);
		oblOvertime.addChangeHandler(this);
		
		markInAddress=new TextArea();
		markInAddress.setEnabled(false);
		markOutAddress=new TextArea();
		markOutAddress.setEnabled(false);
		
		rbWeeklyOff=new RadioButton("myRadioGroup","Weekly Off");
		rbHoliday=new RadioButton("myRadioGroup","Holiday");
//		rbWeeklyOff=new RadioButton("Weekly Off");
//		rbHoliday=new RadioButton("Holiday");
		rbWeeklyOff.addClickHandler(this);
		rbHoliday.addClickHandler(this);
		rbWeeklyOff.setValue(false);
		rbHoliday.setValue(false);
		
		
	}


	@Override
	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fgrpingAttandanceUpdate=fbuilder.setlabel("Update Attandance").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("Month", tbMonth);
		FormField ftbMonth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project", oblProject);
		FormField foblProject= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Branch", oblBranch);
		FormField foblBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Employee Id", tbEmpId);
		FormField ftbEmpId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Employee Name", tbEmpName);
		FormField ftbEmpName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Employee Designation", tbEmpDesignation);
		FormField ftbEmpDesignation= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("In Time", tmInTime);
		FormField ftmInTime= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Out Time", tmOutTime);
		FormField ftmOutTime= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Worked Hours", dbTotalWorkedHrs);
		FormField fdbTotalWorkedHrs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Date", dbAttandanceDate);
		FormField fdbAttandanceDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Leave Hours", dbLeaveHours);
		FormField fdbLeaveHours= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Leave Type", OblLeaveType);
		FormField fOblLeaveType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("OT Hours", dbOtHours);
		FormField fdbOtHours= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Overtime", oblOvertime);
		FormField foblOvertime= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Mark In Location", markInAddress);
		FormField fmarkinAddress= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("Mark Out Location", markOutAddress);
		FormField fmarkoutAddress= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("Action Label", tbActionLabel);
		FormField ftbActionLabel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", rbWeeklyOff);
		FormField frbWeeklyOff= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", rbHoliday);
		FormField frbHoliday= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = {  
				{fgrpingAttandanceUpdate},
				{ftbEmpId,ftbEmpName,ftbEmpDesignation},
				{ftbMonth,foblProject,foblBranch},
				{fdbAttandanceDate,ftmInTime,ftmOutTime,},
				{fdbTotalWorkedHrs,fdbLeaveHours,fdbOtHours},
				{ftbActionLabel,fOblLeaveType,foblOvertime},
				{fmarkinAddress},
				{fmarkoutAddress},
				{frbWeeklyOff,frbHoliday}
			
		};
		this.fields=formfield;
	}
	

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		Calendar cal = employeeInfo.getLeaveCalendar();
		WeekleyOff wo = employeeInfo.getLeaveCalendar().getWeeklyoff();
		boolean iswo = CalendarPresenter.isWeekleyOff(wo,attendance.getAttendanceDate());
		boolean isHoliday = cal.isHolidayDate(attendance.getAttendanceDate());
		
		if(event.getSource()==rbWeeklyOff){
//			Calendar cal = employeeInfo.getLeaveCalendar();
//			WeekleyOff wo = employeeInfo.getLeaveCalendar().getWeeklyoff();
//			boolean iswo = CalendarPresenter.isWeekleyOff(wo,attendance.getAttendanceDate());
//			boolean isHoliday = cal.isHolidayDate(attendance.getAttendanceDate());
			
			if(rbWeeklyOff.getValue()==true){
				//if(iswo){
					attendance.setWeeklyOff(true);
					attendance.setActionLabel("WO");
					attendance.setHoliday(false);
//					attendance.setLeave(true);
					attendance.setLeaveHours(employeeInfo.getLeaveCalendar().getWorkingHours());
					attendance.setLeaveType("");
					
					dbTotalWorkedHrs.setValue(0d);
					dbLeaveHours.setValue(employeeInfo.getLeaveCalendar().getWorkingHours());
					
//				}else{
//					attendance.setWeeklyOff(false);
//					attendance.setLeave(false);
//					
//					rbWeeklyOff.setValue(false);
//					SC.say("Selected day is not marked as weekly off in calendar.");
//				}
				
			}else{
				attendance.setWeeklyOff(false);
				attendance.setLeave(false);
				attendance.setActionLabel("");
			}
		}
		if(event.getSource()==rbHoliday){
			if(rbHoliday.getValue()==true){
				if(isHoliday){
					attendance.setHoliday(true);
					attendance.setActionLabel("H");
					attendance.setWeeklyOff(false);
					attendance.setLeave(true);
					attendance.setLeaveHours(employeeInfo.getLeaveCalendar().getWorkingHours());
					attendance.setLeaveType("");
					
					dbTotalWorkedHrs.setValue(0d);
					dbLeaveHours.setValue(employeeInfo.getLeaveCalendar().getWorkingHours());
					
				}else{
					attendance.setHoliday(false);
					attendance.setLeave(false);
					
					rbHoliday.setValue(false);
					SC.say("Selected day is not marked as holiday in calendar.");
				}
				
			}else{
				attendance.setHoliday(false);
				attendance.setLeave(false);
				attendance.setActionLabel("");
			}
		}
	}
	
	public Attendance getModel(){
		Attendance model=attendance;
//		if(tmInTime.getTimeInDate()!=null&&tmInTime.getValue()!=null){//Deepaks
//			logger.log(Level.SEVERE,"InTime :: "+tmInTime.getValue());
//			model.setInTime(new Date(tmInTime.getValue()));
//			
//		}else{
//			
//			logger.log(Level.SEVERE,"InTime :: "+tmInTime.getValue());
//		}
//		if(tmOutTime.getTimeInDate()!=null&&tmOutTime.getValue()!=null){
//			logger.log(Level.SEVERE,"OutTime :: "+tmOutTime.getValue());
//			model.setOutTime(new Date(tmOutTime.getValue()));
//		}
		if(oblProject.getSelectedIndex()!=0){
			model.setProjectName(oblProject.getValue());
		}
		
		if(LoginPresenter.loggedInUser!=null){
			model.setCreatedBy(LoginPresenter.loggedInUser);
		}
		model.setCreationDate(new Date());
		
		/**
		 * @author Anil,Setting attendance status 
		 *  
		 */
		if(dbLeaveHours.getValue()!=null&&dbLeaveHours.getValue()!=0&&OblLeaveType.getSelectedIndex()!=0){
			model.setLeave(true);
			model.setLeaveHours(dbLeaveHours.getValue());//Ashwini Patil
			if(dbTotalWorkedHrs.getValue()==null||dbTotalWorkedHrs.getValue()==0){
				/**
				 * @author Anil , Date : 01-08-2019
				 */
				if((model.getActionLabel()!=null&&!model.getActionLabel().equals(""))&&(model.getActionLabel().equals("WO")||model.getActionLabel().equals("HF"))){
					
				}else{
					model.setActionLabel("A");
				}
			}
			
		}
		if(dbTotalWorkedHrs.getValue()!=null&&dbTotalWorkedHrs.getValue()!=0){
			model.setPresent(true);
			model.setTotalWorkedHours(dbTotalWorkedHrs.getValue());//Ashwini Patil
			model.setWorkedHours(dbTotalWorkedHrs.getValue());
			if(dbLeaveHours.getValue()==null||dbLeaveHours.getValue()==0){
				model.setActionLabel("P");
			}
			//Ashwini Patil Date:8-07-2022
			else if(model.getWorkingHours()==0) {
				double whours=employeeInfo.getLeaveCalendar().getWorkingHours();
				if(dbTotalWorkedHrs.getValue()<whours/2) {
					model.setActionLabel("A");
					model.setPresent(false);
				}else {
					model.setActionLabel("HF");
				}
			}else {
				if(dbTotalWorkedHrs.getValue()<model.getWorkingHours()/2) {
					model.setActionLabel("A");
					model.setPresent(false);
				}else {
					model.setActionLabel("HF");
				}				
			}
		}
		if(dbOtHours.getValue()!=null&&dbOtHours.getValue()!=0&&oblOvertime.getSelectedIndex()!=0){
			model.setOvertime(true);
			model.setOvertimeHours(dbOtHours.getValue());//Ashwini Patil
		}
		return model;
	}
	
	public void updateView(Attendance view){
		rbWeeklyOff.setValue(false);
		rbHoliday.setValue(false);
		loadEmployeeInfo(view.getEmpId());
		if(view.getMonth()!=null){
			tbMonth.setValue(view.getMonth());
		}
		/***31-12-2019 Deepak Salve added this code for markInAddress,markOutAddress field***/
		if(view.getMarkInAddress()!=null){
			markInAddress.setValue(view.getMarkInAddress());
		}
		if(view.getMarkOutAddress()!=null){
			markOutAddress.setValue(view.getMarkOutAddress());
		}
		/***End***/
		if(view.getProjectName()!=null){
			oblProject.setValue(view.getProjectName());
			HrProject project=oblProject.getSelectedItem();
			initializeOverTime(project, view.getEmpId());
		}
		if(view.getBranch()!=null){
			oblBranch.setValue(view.getBranch());
		}
		
		if(view.getEmpId()!=0){
			tbEmpId.setValue(view.getEmpId()+"");
		}
		if(view.getEmployeeName()!=null){
			tbEmpName.setValue(view.getEmployeeName());
		}
		if(view.getEmpDesignation()!=null){
			tbEmpDesignation.setValue(view.getEmpDesignation());
		}
		
		if(view.getAttendanceDate()!=null){
			dbAttandanceDate.setValue(view.getAttendanceDate());
		}
		if(view.getActionLabel()!=null){
			tbActionLabel.setValue(view.getActionLabel());
		}
		
		if(view.getInTime()!=null){
//			logger.log(Level.SEVERE,"View 1 InTime :: "+view.getInTime().getTime());
			long val=view.getInTime().getTime();
			Date date=view.getInTime();
			String val1=Double.toString(val);
//			logger.log(Level.SEVERE,"View 1 InTime :: "+val1);
//			tmInTime=new TimeBox(new Date(view.getTargetInTime()));
//			logger.log(Level.SEVERE,"InTime :: "+tmInTime.getValue());
//			tmInTime=new TimeBox(view.getTargetInTime());
//			logger.log(Level.SEVERE,"View 2 InTime :: "+view.getInTime().getTime());
//			tmInTime.setValue(view.getInTime().getTime());
//			tmInTime.setValue(view.getInTime().getTime()+"");
			tmInTime.setValue(view.getInTime().getHours()+":"+view.getInTime().getMinutes());
		}else{
//			logger.log(Level.SEVERE,"View 1 InTime :: "+view.getInTime().getTime());
//			long val=view.getInTime().getTime();
//			String val1=Double.toString(val);
//			logger.log(Level.SEVERE,"View 1 InTime :: "+val1);
		}
		if(view.getOutTime()!=null){
//			logger.log(Level.SEVERE,"View 1 outTime :: "+view.getOutTime());
//			tmOutTime=new TimeBox(view.getOutTime());
//			logger.log(Level.SEVERE,"View 1 outTime :: "+view.getOutTime().getTime());
//			tmOutTime.setValue(view.getOutTime().getTime());
			Date date=view.getInTime();
			tmOutTime.setValue(view.getOutTime().getHours()+":"+view.getOutTime().getMinutes());
		}
		dbTotalWorkedHrs.setValue(view.getTotalWorkedHours());
		if(view.getLeaveType()!=null){
			initializeLeaveType(view.getEmpId(), view.getLeaveType());
		}
		dbLeaveHours.setValue(view.getLeaveHours());
		if(view.getOvertimeType()!=null){
			oblOvertime.setValue(view.getOvertimeType());
		}
		dbOtHours.setValue(view.getOvertimeHours());
		
		attendance=view;
		updateVisbility();
	}
	
	
	private void updateVisbility() {
		if(dbLeaveHours.getValue()==null||dbLeaveHours.getValue()==0){

			dbLeaveHours.setEnabled(false);
			OblLeaveType.setEnabled(false);
		}else { //Ashwini Patil Date:15-05-2022 
			dbLeaveHours.setEnabled(true);
			OblLeaveType.setEnabled(true);			
		}
		if(dbOtHours.getValue()==null||dbOtHours.getValue()==0){
			dbOtHours.setEnabled(false);
			oblOvertime.setEnabled(false);
		}else {
			oblOvertime.setEnabled(true); //Ashwini Patil Date:15-05-2022 
		}			
		
		if(attendance.getCount()==0){
			dbLeaveHours.setEnabled(true);
			OblLeaveType.setEnabled(true);
		}
	}


	private void loadEmployeeInfo(int empId) {
		GenricServiceAsync genasync = GWT.create(GenricService.class);

		Vector<Filter>filterVec =new Vector<Filter>();
		Filter filter=null;
		MyQuerry querry = new MyQuerry();
		
		filter = new Filter();
		filter.setQuerryString("empCount");
		filter.setIntValue(empId);
		filterVec.add(filter);
		
//		filter = new Filter();
//		filter.setQuerryString("validtill >");
//		filter.setDateValue(new Date());
//		filterVec.add(filter);
		
		querry.getFilters().addAll(filterVec);
		querry.setQuerryObject(new EmployeeInfo());
		
		genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() == 0) {
					return;
				}else{
					for(SuperModel model:result){
						 employeeInfo  = (EmployeeInfo) model;
					}
					if(attendance.getCount()==0){
						if(employeeInfo.getLeaveCalendar()!=null)
							dbLeaveHours.setValue(employeeInfo.getLeaveCalendar().getWorkingHours());
						
					}
					
				}
			}
			@Override
			public void onFailure(Throwable caught) {

			}
		});
	}


	private void initializeLeaveType(int empId,final String leaveName) {
		GenricServiceAsync genasync = GWT.create(GenricService.class);

		Vector<Filter>filterVec =new Vector<Filter>();
		Filter filter=null;
		MyQuerry querry = new MyQuerry();
		
		filter = new Filter();
		filter.setQuerryString("empInfo.empCount");
		filter.setIntValue(empId);
		filterVec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("validtill >");
		filter.setDateValue(new Date());
		filterVec.add(filter);
		
		querry.getFilters().addAll(filterVec);
		querry.setQuerryObject(new LeaveBalance());
		
		genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() == 0) {
					return;
				}else{
					leaveAlloc=new ArrayList<AllocatedLeaves>();
					for(SuperModel model:result){
						LeaveBalance leaveBalance  = (LeaveBalance) model;
						leaveAlloc = leaveBalance.getLeaveGroup().getAllocatedLeaves();
						OblLeaveType.removeAllItems();
						OblLeaveType.setListItems(leaveAlloc);
					}
					
					if(leaveName!=null&&!leaveName.equals("")){
						OblLeaveType.setValue(leaveName);
					}
				}
			}
			@Override
			public void onFailure(Throwable caught) {

			}
		});
	}
	
	
	public boolean validateAttendance(){
		GWTCAlert alert=new GWTCAlert();
		if(dbTotalWorkedHrs.getValue()==null){
//			alert.alert("Total worked are hours can not be blank.");
			SC.say("Total worked are hours can not be blank.");
			return false;
		}
		double workingHours=0;
		double extraHours=0;
		double leaveHours=0;
		if(employeeInfo!=null){
			if(employeeInfo.getLeaveCalendar()!=null){

				workingHours=employeeInfo.getLeaveCalendar().getWorkingHours();
				
				if(dbTotalWorkedHrs.getValue()>workingHours){
					extraHours=dbTotalWorkedHrs.getValue()-workingHours;
					if(dbOtHours.getValue()!=null&&dbOtHours.getValue()!=0){
						if(dbOtHours.getValue()!=extraHours){
//							alert.alert("Extra working hours did not match with OT hours.");
							SC.say("Extra working hours did not match with OT hours.");
							return false;
						}else{
							if(oblOvertime.getSelectedIndex()==0){
//								alert.alert("Please select Overtime type.");
								SC.say("Please select Overtime type.");
								return false;
							}
						}
					}else{
						dbOtHours.setEnabled(true);
//						alert.alert("Please update OT hours.");
						SC.say("Please update OT hours.");
						return false;
					}
					
				}else if(dbTotalWorkedHrs.getValue()<workingHours){
					leaveHours=workingHours-dbTotalWorkedHrs.getValue();
					if(dbLeaveHours.getValue()!=null&&dbLeaveHours.getValue()!=0){
						if(rbWeeklyOff.getValue()==true||rbHoliday.getValue()==true){
							return true;
						}
						if(dbLeaveHours.getValue()!=leaveHours){
							alert.alert("Short hours did not match with leave hours.");
							SC.say("Please select project.");
							return false;
						}else{
							if(OblLeaveType.getSelectedIndex()==0){
//								alert.alert("Please select Leave type.");
								SC.say("Please select Leave type.");
								return false;
							}
						}
					}else{
						dbLeaveHours.setEnabled(true);
//						alert.alert("Please update leave hours.");
						SC.say("Please update leave hours.");
						return false;
					}
				}
				
			}else{
				SC.say("Calendar and leave allocation missing for the employee..");
				return false;
			}
		}else{
//			alert.alert("No employee found.");
			SC.say("No employee found.");
			return false;
		}
		
		if(oblProject.getSelectedIndex()==0){
//			alert.alert("Please select project.");
			SC.say("Please select project.");
			return false;
		}
		
		if(rbWeeklyOff.getValue()==true){
			if(dbTotalWorkedHrs.getValue()!=null||dbTotalWorkedHrs.getValue()!=0){
				SC.say("Can not mark as Weekly off.");
				return false;
			}
		}
		
		if(rbHoliday.getValue()==true){
			if(dbTotalWorkedHrs.getValue()!=null||dbTotalWorkedHrs.getValue()!=0){
				SC.say("Can not mark as hoilday.");
				return false;
			}
		}
		
		return true;
	}


	@Override
	public void onValueChange(ValueChangeEvent<Double> event) {
		GWTCAlert alert=new GWTCAlert();
		if(event.getSource()==dbTotalWorkedHrs){
			
			if(dbTotalWorkedHrs.getValue()==null){
//				alert.alert("Total worked are hours can not be blank.");
				SC.say("Total worked are hours can not be blank.");
			}
			double workingHours=0;
			double extraHours=0;
			double leaveHours=0;
			if(employeeInfo!=null){
				workingHours=employeeInfo.getLeaveCalendar().getWorkingHours();
//				if(dbTotalWorkedHrs.getValue()==0){
//					
//				}else 
				if(dbTotalWorkedHrs.getValue()>workingHours){
					HrProject project=null;
					if(oblProject.getValue()!=null){
						project=oblProject.getSelectedItem();
						
					}
					
					extraHours=dbTotalWorkedHrs.getValue()-workingHours;
					dbOtHours.setValue(extraHours);
					dbOtHours.setEnabled(false);
					oblOvertime.setEnabled(true);
					
					dbLeaveHours.setValue(null);
					dbLeaveHours.setEnabled(false);
					OblLeaveType.setSelectedIndex(0);
					OblLeaveType.setEnabled(false);
					
					
				}else if(dbTotalWorkedHrs.getValue()<workingHours){
					leaveHours=workingHours-dbTotalWorkedHrs.getValue();
					dbLeaveHours.setValue(leaveHours);
					dbLeaveHours.setEnabled(false);
					OblLeaveType.setEnabled(true);
					
					dbOtHours.setValue(null);
					dbOtHours.setEnabled(false);
					oblOvertime.setSelectedIndex(0);
					oblOvertime.setEnabled(false);
					
				}else{
					dbLeaveHours.setValue(null);
					dbLeaveHours.setEnabled(false);
					OblLeaveType.setSelectedIndex(0);
					OblLeaveType.setEnabled(false);
					
					dbOtHours.setValue(null);
					dbOtHours.setEnabled(false);
					oblOvertime.setSelectedIndex(0);
					oblOvertime.setEnabled(false);
					
					
					attendance.setOvertime(false);
					attendance.setOvertimeHours(0);
					attendance.setOvertimeType("");
					attendance.setTotalWorkedHours(dbTotalWorkedHrs.getValue());
					attendance.setWorkedHours(dbTotalWorkedHrs.getValue());
					attendance.setProjectNameForOT("");
					
					attendance.setLeave(false);
					attendance.setLeaveHours(0);
					attendance.setLeaveType("");
					
					/**
					 * @author Anil,Date : 06-02-2019
					 * setting attendance present flag true
					 */
					
					if(attendance.isPresent()==false){
						attendance.setPresent(true);
						attendance.setActionLabel("P");
					}
					
				}
				
			}else{
//				alert.alert("No employee found.");
				SC.say("No employee found.");
			}
		}
		
		if(event.getSource()==dbLeaveHours){
			if(dbLeaveHours.getValue()!=null&&dbLeaveHours.getValue()!=0){
				dbLeaveHours.setEnabled(true);
				OblLeaveType.setEnabled(true);
				
				dbOtHours.setValue(null);
				dbOtHours.setEnabled(false);
				oblOvertime.setSelectedIndex(0);
				oblOvertime.setEnabled(false);
			}else{
				dbLeaveHours.setEnabled(false);
				OblLeaveType.setSelectedIndex(0);
				OblLeaveType.setEnabled(false);
			}
		}
		
		if(event.getSource()==dbOtHours){
			if(dbOtHours.getValue()!=null&&dbOtHours.getValue()!=0){
				dbOtHours.setEnabled(true);
				oblOvertime.setEnabled(true);
				
				dbLeaveHours.setValue(null);
				dbLeaveHours.setEnabled(false);
				OblLeaveType.setSelectedIndex(0);
				OblLeaveType.setEnabled(false);
			}else{
				dbOtHours.setEnabled(false);
				oblOvertime.setSelectedIndex(0);
				oblOvertime.setEnabled(false);
			}
		}
	}


	@Override
	public void onChange(ChangeEvent event) {
		GWTCAlert alert=new GWTCAlert();
		if(event.getSource()==oblProject){
			if(oblProject.getSelectedIndex()!=0){
				HrProject project=oblProject.getSelectedItem();
				initializeOverTime(project,attendance.getEmpId());
			}
		}
		
		if(event.getSource()==OblLeaveType){
			if(OblLeaveType.getSelectedIndex()!=0){
				AllocatedLeaves leave=OblLeaveType.getSelectedItem();
				double leaveHours=0;
				String leaveType="";
				double workedHour=0;
				String actionLabel="";
				if(dbLeaveHours.getValue()!=null&&dbLeaveHours.getValue()!=0){
					leaveHours=dbLeaveHours.getValue();
				}else{
//					alert.alert("No leave hours.");
					SC.say("No leave hours.");
					return;
				}
				
				if(dbTotalWorkedHrs.getValue()!=null){
					workedHour=dbTotalWorkedHrs.getValue();
				}else{
//					alert.alert("No worked hours.");
					SC.say("No worked hours.");
					return;
				}
				
				if(leave.getPaidLeave()==false){
					leaveType=leave.getName();
				}else if(leave.getPaidLeave()==true&&leave.getShortName().equalsIgnoreCase("WO")){
					if(workedHour!=0){
//						alert.alert("As wroked hour is not zero, can not select WO type leave.");
						SC.say("As wroked hour is not zero, can not select WO type leave.");
						OblLeaveType.setSelectedIndex(0);
						return;
					}
					leaveType=leave.getName();
					actionLabel="WO";
				}else{
					leaveType=leave.getName();
				}
				
				if(attendance!=null){
					if(!actionLabel.equals("")){
						attendance.setActionLabel(actionLabel);
					}
					attendance.setLeave(true);
					attendance.setLeaveHours(leaveHours);
					attendance.setLeaveType(leaveType);
					attendance.setTotalWorkedHours(workedHour);
					attendance.setWorkedHours(workedHour);
					
					if(workedHour==0){
						attendance.setPresent(false);
					}
				}else{
//					alert.alert("No attendance selected.");
					SC.say("No attendance selected.");
				}
			}else{
				attendance.setLeave(false);
			}
		}
		
		if(event.getSource()==oblOvertime){
			if(oblOvertime.getSelectedIndex()!=0){
				Overtime ot=oblOvertime.getSelectedItem();
				double extraHour=0;
				String otType="";
				double workedHour=0;
				String actionLabel="";
				if(dbOtHours.getValue()!=null&&dbOtHours.getValue()!=0){
					extraHour=dbOtHours.getValue();
				}else{
//					alert.alert("No ot hours.");
					SC.say("No ot hours.");
					return;
				}
				
				if(dbTotalWorkedHrs.getValue()!=null&&dbTotalWorkedHrs.getValue()!=0){
					workedHour=dbTotalWorkedHrs.getValue();
				}else{
					SC.say("No worked hours.");
//					alert.alert("No worked hours.");
					return;
				}
				
				otType=ot.getName();
				
				if(attendance!=null){
					if(!actionLabel.equals("")){
						attendance.setActionLabel(actionLabel);
					}
					attendance.setOvertime(true);
					attendance.setOvertimeHours(extraHour);
					attendance.setOvertimeType(otType);
					attendance.setTotalWorkedHours(workedHour);
					attendance.setWorkedHours(workedHour-extraHour);
					attendance.setProjectNameForOT(oblProject.getValue());
				}else{
//					alert.alert("No attendance selected.");
					SC.say("No attendance selected.");
				}
			}else{
				attendance.setOvertime(false);
			}
		}
	}


	private void initializeOverTime(HrProject project, final int empId) {
		final ArrayList<Overtime>otlist=new ArrayList<Overtime>();
		if(project!=null&&project.getOtList()!=null){
			for(Overtime ot:project.getOtList()){
				if(ot.getEmpId()==empId&&ot.getName()!=null&&!ot.getName().equals("")){//Ashwini Patil Date:6-07-2022
					otlist.add(ot);
				}
			}
			oblOvertime.removeAllItems();
			oblOvertime.setListItems(otlist);
		}
		else {
			if(project!=null){
				final CommonServiceAsync commonserviceasync = GWT.create(CommonService.class);
				commonserviceasync.getHrProjectOvertimelist(project.getCount(), project.getCompanyId(), new AsyncCallback<ArrayList<HrProjectOvertime>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(ArrayList<HrProjectOvertime> result) {

						for(HrProjectOvertime ot : result){
//							Overtime overtime = new Overtime();
//							overtime = overtime.Myclone();
//							Overtime overtime = ot;
//							if(overtime.getEmpId()==empId&&overtime.getName()!=null&&!overtime.getName().equals("")){//Ashwini Patil Date:6-07-2022
//								otlist.add(ot);
//							}
							if(ot.getEmpId()==empId&&ot.getName()!=null&&!ot.getName().equals("")){//Ashwini Patil Date:6-07-2022
								otlist.add(ot);
							}
						}
						oblOvertime.removeAllItems();
						oblOvertime.setListItems(otlist);
						
					}
				});
			}
		}
	}


}
