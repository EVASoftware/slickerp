package com.slicktechnologies.client.views.humanresource.attendancelist;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;
import java.util.logging.Level;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.code.p.gwtchismes.client.GWTCTabPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.DataMigrationTaskQueueService;
import com.slicktechnologies.client.services.DataMigrationTaskQueueServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.popups.DownloadPopPup;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.attendance.AttendanceBean;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.twilio.sdk.resource.instance.monitor.Alert;

public class AttendanceListPresenter implements ClickHandler, CellClickHandler {

	AttendanceListForm form;
	
	
	GenricServiceAsync service=GWT.create(GenricService.class);
	GeneralServiceAsync service1 =GWT.create(GeneralService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	ArrayList<Attendance> globalAttendaceList=new ArrayList<Attendance>();
	protected AppMemory mem;
	
	/**
	 * Date : 05-10-2018 By ANIL
	 */
	AttandanceUpdatePopup updateAttandancePopup=new AttandanceUpdatePopup();
	PopupPanel panel=new  PopupPanel(true);
	DataMigrationTaskQueueServiceAsync taskQueueSer=GWT.create(DataMigrationTaskQueueService.class);
	DateTimeFormat hhMMForamt=DateTimeFormat.getFormat("HH:mm");
	
	/**
	 * @author Anil
	 * @since 04-08-2020
	 * 
	 */
	int daysArray[]=null;
	
	/**
	 * @author Anil
	 * @since 07-08-2020
	 * multicyle flag
	 */
	boolean multicyleFlag;
	HashSet<String> hsProject=new HashSet<String>();
	
	Map<String, String> leaveMap = new HashMap<String, String>(); //Ashwini Patil
	double workingHours=1;//=9;//Ashwini Patil 17-07-2023
	NumberFormat nf = NumberFormat.getFormat("0.00");
	
	ArrayList<EmployeeInfo> employeeinfolist = new ArrayList<EmployeeInfo>();
	HashMap<Integer, Double> employeeWorkingHoursMap=new HashMap<Integer, Double>();
	
	DownloadPopPup downloadformatpopup = new DownloadPopPup();//Ashwini Patil Date:7-07-2023
	static GWTCGlassPanel glassPanel=new GWTCGlassPanel();
	public AttendanceListPresenter(AttendanceListForm homeForm) {
		this.form=homeForm;
		
		form.grid.addCellClickHandler(this);
		
		updateAttandancePopup.getLblOk().addClickHandler(this);
		updateAttandancePopup.getLblCancel().addClickHandler(this);
		form.getBtnDownload().addClickHandler(this);
		mem=AppMemory.getAppMemory();
		setEventHandeling();
		setLeaveShortName(); //Ashwini Patil
		
		downloadformatpopup.getBtnDownloadFormat1().addClickHandler(this);
		downloadformatpopup.getBtnDownloadFormat2().addClickHandler(this);
		downloadformatpopup.getBtnDownloadFormat3().addClickHandler(this);
		downloadformatpopup.getLblCancel().addClickHandler(this);
	}
	
	private void setEventHandeling() {
		form.getBtnGo().addClickHandler(this);
	
		InlineLabel label[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
		for (int k = 0; k < label.length; k++) {
			if (AppMemory.getAppMemory().skeleton.registration[k] != null)
				AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
		}
		for (int k = 0; k < label.length; k++) {
			AppMemory.getAppMemory().skeleton.registration[k] = label[k].addClickHandler(this);
		}
	}

	public static void initialize(){
		System.out.println("INSIDE CHART INITIALIZATION");
		AttendanceListForm homeForm = new AttendanceListForm();
		AppMemory.getAppMemory().currentScreen=Screen.ATTENDANCElIST;
		AppMemory.getAppMemory().skeleton.getProcessName().setText(AppConstants.HRMODULE+"/"+"Attendance Report");
		
		AttendanceListPresenter presenter= new AttendanceListPresenter(homeForm);
		
		AppMemory.getAppMemory().stickPnel(homeForm);	
	}
	
	public void retrieveAttendanceData(){
		System.out.println("BTN GO CLICK");
		
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("MMM-yyyy");
		String month ="";
		if(form.getDbMonth().getValue()!=null){
			month =dateFormat.format(form.getDbMonth().getValue());
		}
		
		String branch = "";
		if(form.getOlbbranch().getSelectedIndex() !=0){
			branch = form.getOlbbranch().getValue(form.getOlbbranch().getSelectedIndex());
		}
		
		/**
		 * @author Anil
		 * @since 07-08-2020
		 */
		if(form.clientComposite.getValue()!=null){
			if(form.clientComposite.getIdValue()!=-1){
				 multicyleFlag=true;
			}
		}
		
		int empId =0;
		if(form.getPic().getId().getValue() != null && !form.getPic().getId().getValue().equals("")){
		    empId = Integer.parseInt(form.getPic().getId().getValue());
		    multicyleFlag=true;
		}
		
		String projectName = null;
		if(form.getOlbProject().getSelectedIndex() !=0){
			projectName = form.getOlbProject().getValue(form.getOlbProject().getSelectedIndex());
			multicyleFlag=false;
		}

		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		filtervec=new Vector<Filter>();
		
		/**
		 * @author Anil ,Date : 28-03-2019
		 * Adding client's details for searching reports
		 */
		if(form.clientComposite.getValue()!=null){
			if(form.clientComposite.getIdValue()!=-1){
				filter = new Filter();
				filter.setQuerryString("clientInfo.count");
				filter.setIntValue(form.clientComposite.getIdValue());
				filtervec.add(filter);
			}
		}
			
		if(form.getDbMonth().getValue() !=null){
			filter = new Filter();
			filter.setQuerryString("month");
			filter.setStringValue(dateFormat.format(form.getDbMonth().getValue()));
			filtervec.add(filter);
		}
		if(form.getOlbbranch().getSelectedIndex() !=0){
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue(form.getOlbbranch().getSelectedIndex()));
			filtervec.add(filter);
		}
		if(form.getOlbProject().getSelectedIndex() !=0){
			filter = new Filter();
			filter.setQuerryString("projectName");
			filter.setStringValue(form.getOlbProject().getValue(form.getOlbProject().getSelectedIndex()));
			filtervec.add(filter);
		}
		if(form.getPic().getId().getValue() != null && !form.getPic().getId().getValue().equals(""))
		{  
			
			filter=new Filter();
			filter.setIntValue(Integer.parseInt(form.getPic().getId().getValue()));
			filter.setQuerryString("empId");
			filtervec.add(filter);
		}

		if(!(form.getPic().getName().getValue().equals("")))
		{
			filter=new Filter();
			filter.setStringValue(form.getPic().getName().getValue());
			filter.setQuerryString("employeeName");
			filtervec.add(filter);
		}

		querry=new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Attendance());
		this.retriveTable(querry);
		
	}
	
	
	public void retriveTable(MyQuerry querry){
		System.out.println("querry : "+ querry);
		GenricServiceAsync service1=GWT.create(GenricService.class);
		service1.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				globalAttendaceList=new ArrayList<>();
				hsProject=new HashSet<String>();
				if(result.size()==0){
					SC.say("No result found.");
				}
				
				HashSet<Integer> empidlist = new HashSet<Integer>();

				for(SuperModel model:result){
					Attendance attendace=(Attendance) model;
					globalAttendaceList.add(attendace);
					empidlist.add(attendace.getEmpId());
				}
				
				Comparator<Attendance> empIdComp = new  Comparator<Attendance>() {
					@Override
					public int compare(Attendance o1, Attendance o2) {
						Integer empId1=o1.getEmpId();
						Integer empId2=o2.getEmpId();
						return empId1.compareTo(empId2);
					}
				};
				Collections.sort(globalAttendaceList,empIdComp);
				
				Console.log("globalAttendaceList size after query="+globalAttendaceList.size()+"empidlist="+empidlist);
				
				loadEmployeeWorkingHours(empidlist, globalAttendaceList.get(0).getCompanyId());
				
				
				Timer timer = new Timer() {
					@Override
					public void run() {
						Console.log("inside run");
//				final ArrayList<AttendanceBean> list = getAttendaceBeanList(globalAttendaceList);
				
				/**Date 5-3-2020 by Amol,  for blank cell while employee not marking  attendance**/
				/**
				 * @author Anil, Date:30-04-2020
				 * if we search attendance by employee then it was not getting searched
				 */
				if(form.olbProject.getSelectedIndex()!=0) {
					final HrProject hrProject= form.olbProject.getSelectedItem();
					if(hrProject.getOtList()==null || hrProject.getOtList().size()==0){
						Console.log("report condition 1 ");
						CommonServiceAsync commonservice = GWT.create(CommonService.class);
						commonservice.getHrProjectOvertimelist(hrProject.getCount(), hrProject.getCompanyId(), new AsyncCallback<ArrayList<HrProjectOvertime>>() {

							@Override
							public void onFailure(Throwable caught) {
								
							}

							@Override
							public void onSuccess(ArrayList<HrProjectOvertime> result) {
								
								ArrayList<AttendanceBean> list = getAttendaceBeanList(globalAttendaceList);

								ArrayList<Overtime> otlist = new ArrayList<Overtime>();
								for(HrProjectOvertime hrOT : result){
									otlist.add(hrOT);
								}
								
								DateTimeFormat dateFormat = DateTimeFormat.getFormat("MMM-yyyy");
								/**
								 * @author Anil
								 * @since 07-08-2020
								 */
								HashSet<Integer> projectEmpList=new HashSet<Integer>();
//								ArrayList<Integer> projectEmpList=new ArrayList<Integer>();
								ArrayList<Integer> empList=new ArrayList<Integer>();
								
								for(AttendanceBean attBean:list){
									empList.add(attBean.getEmpId());
								}
								Console.log("attendance employee List"+empList.size());
								
								for(Overtime ot:otlist){
									projectEmpList.add(ot.getEmpId());
								}
								Console.log("Project employee List"+projectEmpList.size());
								
								projectEmpList.removeAll(empList);
								Console.log("Project employee List after removing common employee"+projectEmpList.size());
								
								for(Integer empId:projectEmpList){
									for(Integer eId:empList) { //Ashwini Patil Date:23-05-2022 Description: when apply all filters other employee also get populated in attendance report
									//	Console.log("empId"+empId+" eId"+eId);
										if(empId==eId) {	
											AttendanceBean attBean=new AttendanceBean();
											Overtime obj=getEmployeeDeatils(empId,otlist);
											
											attBean.setEmpId(empId);
											attBean.setEmployeeName(obj.getEmpName());
											attBean.setEmpDesignation(obj.getEmpDesignation());
											attBean.setProjectName(hrProject.getProjectName());
											attBean.setShift(obj.getShift());
											attBean.setMonth(dateFormat.format(form.getDbMonth().getValue()));
											
											list.add(attBean);					
										}			
									}
								}
								Console.log("employee whos attendance not marked"+list.size());
								form.setDataToListGrid(list,daysArray);

							}
						});
					}
					else{
						Console.log("report condition 2 ");
						ArrayList<AttendanceBean> list = getAttendaceBeanList(globalAttendaceList);
						list= getAttendanceNotMarkedEmployeeDetails(list);
						form.setDataToListGrid(list,daysArray);
					}

					
				}
				else{
					Console.log("report condition 3 ");
					ArrayList<AttendanceBean> list = getAttendaceBeanList(globalAttendaceList);
					form.setDataToListGrid(list,daysArray);
				}
//				form.setDataToListGrid(list,daysArray);
			}
		};
		timer.schedule(2000);
			}
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		}); 
	 }
	

	protected ArrayList<AttendanceBean> getAttendanceNotMarkedEmployeeDetails(
			ArrayList<AttendanceBean> list) {
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("MMM-yyyy");
		HrProject hrProject= form.olbProject.getSelectedItem();
	
		/**
		 * @author Anil
		 * @since 07-08-2020
		 */
		HashSet<Integer> projectEmpList=new HashSet<Integer>();
//		ArrayList<Integer> projectEmpList=new ArrayList<Integer>();
		ArrayList<Integer> empList=new ArrayList<Integer>();
		
		for(AttendanceBean attBean:list){
			empList.add(attBean.getEmpId());
		}
		Console.log("attendance employee List"+empList.size());
		
		for(Overtime ot:hrProject.getOtList()){
			projectEmpList.add(ot.getEmpId());
		}
		Console.log("Project employee List"+projectEmpList.size());
		
		projectEmpList.removeAll(empList);
		Console.log("Project employee List after removing common employee"+projectEmpList.size());
		
		for(Integer empId:projectEmpList){
			for(Integer eId:empList) { //Ashwini Patil Date:23-05-2022 Description: when apply all filters other employee also get populated in attendance report
			//	Console.log("empId"+empId+" eId"+eId);
				if(empId==eId) {	
					AttendanceBean attBean=new AttendanceBean();
					Overtime obj=getEmployeeDeatils(empId,null);
					
					attBean.setEmpId(empId);
					attBean.setEmployeeName(obj.getEmpName());
					attBean.setEmpDesignation(obj.getEmpDesignation());
					attBean.setProjectName(hrProject.getProjectName());
					attBean.setShift(obj.getShift());
					attBean.setMonth(dateFormat.format(form.getDbMonth().getValue()));
					
					list.add(attBean);					
				}			
			}
		}
		Console.log("employee whos attendance not marked"+list.size());
		return list;
		
	}

	private Overtime getEmployeeDeatils(Integer empId, ArrayList<Overtime> otlist) {
		HrProject hrProject= form.olbProject.getSelectedItem();
		
		if(otlist!=null && otlist.size()>0){
			for(Overtime ot:otlist){
				if(ot.getEmpId()==empId){
					return ot;
				}
			}
		}
		else{
			for(Overtime ot:hrProject.getOtList()){
				if(ot.getEmpId()==empId){
					return ot;
				}
			}
		}
	
		
		return null;
	}

	@Override
	public void onClick(ClickEvent event) {
		System.out.println("CLICK EVENT..!!!");
		if(event.getSource() instanceof InlineLabel){
			InlineLabel lbl= (InlineLabel) event.getSource();
			
			/**
			 * @author Anil
			 * @since 07-08-2020
			 */
			if(multicyleFlag){
				Console.log("multicyleFlag "+multicyleFlag+" hsProject "+hsProject.size());
				if(hsProject.size()>1){
					Integer payrollStartDay=null;
					loop:
					for(String project:hsProject){
						Console.log("project "+project+" payrollStartDay "+payrollStartDay);
						for(HrProject obj:LoginPresenter.globalHRProject){
							if(project.equals(obj.getProjectName())){
								if(payrollStartDay!=null){
									if(obj.getPayrollStartDay()!=null){
										if(obj.getPayrollStartDay()==0){
											obj.setPayrollStartDay(1);
										}
										if(payrollStartDay!=obj.getPayrollStartDay()){
											SC.say("Please specify project name.");
											return;
										}
									}
								}else{
									if(obj.getPayrollStartDay()!=null){
										if(obj.getPayrollStartDay()==0){
											payrollStartDay=1;
										}else{
											payrollStartDay=obj.getPayrollStartDay();
										}
									}else{
										payrollStartDay=1;
									}
									
								}
								continue loop;
							}
						}
					}
				}
			}
			
			if(lbl.getText().equals("Download")){
				Console.log("Download Clicked.!!");
				reactOnDownload();
			}
			
			if(lbl.getText().equals("Print")){
				reactOnPrint();
			}
		}
		
		if (event.getSource().equals(form.getBtnGo())) {
			glassPanel.show();
			form.getBtnGo().setEnabled(false);
			Console.log("Glasspanel.show called");
			DateTimeFormat dateFormat = DateTimeFormat.getFormat("MMM-yyyy");
			System.out.println("BTN GO CLICK");
			/**
			 * @author Anil ,Date : 20-04-2019
			 * Updated as selection filter
			 */
			// System.out.println("Month "+dateFormat.format(form.getDbMonth().getValue())+" Branch :"+form.getOlbbranch().getSelectedIndex());
			
//			if (LoginPresenter.branchRestrictionFlag == true&& form.getOlbbranch().getSelectedIndex() == 0) {
//				SC.say("Please select branch.");
//				return;
//			} else if (form.pic.getEmployeeId()==-1&&(form.getOlbbranch().getSelectedIndex() == 0|| form.getDbMonth().getValue() == null)) {
//				SC.say("Please select Branch and Month.");
//				return;
//			} else {
//				retrieveAttendanceData();
//			}
			
			String msg="";
			boolean monthFlag=false;
			boolean branchFlag=false;
			boolean projectFlag=false;
			if(form.getDbMonth().getValue()==null){
				monthFlag=true;
			}
			if(form.olbbranch.getSelectedIndex()==0){
				if(form.clientComposite.getIdValue()==-1){
					branchFlag=true;
				}
			}
			if(form.olbProject.getSelectedIndex()==0){
				if(form.clientComposite.getIdValue()==-1){
					projectFlag=true;
				}
			}
			
			if(form.pic.getEmployeeId()!=-1){
				monthFlag=false;
				branchFlag=false;
				projectFlag=false;
			}
			
			if(monthFlag||branchFlag||projectFlag){
				msg="Please select ";
			}
			
			if(monthFlag&&branchFlag&&projectFlag){
				msg=msg+"month , branch and project.";
			}else if(monthFlag&&branchFlag&&!projectFlag){
				msg=msg+"month and branch.";
			}else if(monthFlag&&!branchFlag&&projectFlag){
				msg=msg+"month and project.";
			}else if(monthFlag&&!branchFlag&&!projectFlag){
				msg=msg+"month.";
			}else if(!monthFlag&&branchFlag&&projectFlag){
				msg=msg+"branch and project.";
			}else if(!monthFlag&&!branchFlag&&projectFlag){
				msg=msg+"project.";
			}else if(!monthFlag&&branchFlag&&!projectFlag){
				msg=msg+"branch.";
			}
			
			
			if(!msg.equals("")){
				SC.say(msg);
				glassPanel.hide();
				form.getBtnGo().setEnabled(true);
				return;
			}
			retrieveAttendanceData();
			/**
			 * End
			 */
			form.getBtnGo().setEnabled(true);
			glassPanel.hide();
		}
		
		if(event.getSource()==updateAttandancePopup.getLblOk()){
			if(updateAttandancePopup.validateAttendance()){
				Attendance attendance=updateAttandancePopup.getModel();
				//Console.log("attendance label before update="+attendance.getActionLabel());
				final GWTCGlassPanel glassPanel=new GWTCGlassPanel();
				
				glassPanel.show();
				taskQueueSer.updateAndOverrideAttendance(attendance, new AsyncCallback<Attendance>() {
					
					@Override
					public void onFailure(Throwable caught) {
						glassPanel.hide();
//						GWTCAlert alert =new GWTCAlert();
//						alert.alert("Network error.");
						SC.say("Network error.");
					}

					@Override
					public void onSuccess(Attendance result) {
						glassPanel.hide();
						updateAttandancePopup.hidePopUp();
						if(result==null){
							return;
						}
						
						/**
						 * @author Anil,Date : 06-02-2019
						 * for updating record on data grid ,earlier attendance date is compared with approval date 
						 */
						for(int i=0;i<globalAttendaceList.size();i++){
							if(globalAttendaceList.get(i).getEmpId()==result.getEmpId()
									&&globalAttendaceList.get(i).getAttendanceDate().equals(result.getAttendanceDate())){
								globalAttendaceList.remove(i);
								break;
							}
						}
						globalAttendaceList.add(result);
						Comparator<Attendance> empIdComp = new  Comparator<Attendance>() {
							@Override
							public int compare(Attendance o1, Attendance o2) {
								Integer empId1=o1.getEmpId();
								Integer empId2=o2.getEmpId();
								return empId1.compareTo(empId2);
							}
						};
						Collections.sort(globalAttendaceList,empIdComp);
						
						ArrayList<AttendanceBean> list = getAttendaceBeanList(globalAttendaceList);
						form.setDataToListGrid(list,daysArray);
						
						SC.say("Attendance update successful.");
						
						
					}
				});
			}
		}
		
		if(event.getSource()==updateAttandancePopup.getLblCancel()){
			updateAttandancePopup.hidePopUp();
		}
		if(event.getSource() == form.getBtnDownload()){
			downloadAttendance();
		}
	
		if(event.getSource()==downloadformatpopup.getBtnDownloadFormat1()){
			reactonDownloadFormat1();
		}
		if (event.getSource()==downloadformatpopup.getBtnDownloadFormat2()) {
			reactonDownloadFormat2();
		}
		if(event.getSource()==downloadformatpopup.getBtnDownloadFormat3()){
			downloadAttendance();
		}
		
		if (event.getSource()==downloadformatpopup.getLblCancel()) {
			downloadformatpopup.hidePopUp();
		}
	}
	

	private void reactOnPrint() {
		ArrayList<Attendance> attendaceList = new ArrayList<Attendance>();
		attendaceList.addAll(globalAttendaceList);
        if(attendaceList.size() >0){
        	Console.log("LOG 1");
	        ArrayList<AttendanceBean> attendanceBeanList = getAttendaceBeanList(attendaceList);
			csvservice.setAttendance(attendanceBeanList, new AsyncCallback<Void>(){
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				
				@Override
				public void onSuccess(Void result) {
					Console.log("LOG 6 ");
					String url1=GWT.getModuleBaseURL()+ "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"FormD_MusterRoll"+"&"+"subtype="+"FormD_MusterRoll";
					Window.open(url1, "test", "enabled");
				}
			});
        }else{
        	SC.say("Attendance list is empty");
        }
	}

	private void reactOnDownload() {
		downloadformatpopup.getBtnDownloadFormat1().setText("Standard");
		downloadformatpopup.getBtnDownloadFormat2().setText("Working Days");
		downloadformatpopup.getBtnDownloadFormat3().setText("Detailed");
		downloadformatpopup.getPopup().getElement().addClassName("downloadpopup");//to set width
		downloadformatpopup.getLblOk().setVisible(false);
		downloadformatpopup.showPopUp();
	}

	private Attendance getAttendaceDetails(int attendaceId) {
		for(Attendance object:globalAttendaceList){
			if(object.getCount()==attendaceId){
				return object;
			}
		}
		return null;
	}

	private ArrayList<AttendanceBean> getAttendaceBeanList(ArrayList<Attendance> attendanceList){
		  /**
	     * @author Anil,Date : 24-01-2019
	     * Process Name : Attendance
	     * Process Type : ShowAttendanceLabel
	     * if this flag is true then we will show action label instead of working hours
	     * For Orion BY Sonu
	     */
	    boolean actionFlag=false;
	    boolean workingHourFlag = false;
	    if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "ShowAttendanceLabel")){
	    	actionFlag=true;
	    }
	    if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "ShowWorkingHours")){
	    	workingHourFlag=true;
	    }
	    /** date 17.07.2019 added by komal to show hours in HH:mm format **/
	    
	    Console.log("LOG 2 "+actionFlag+" multicyleFlag "+multicyleFlag+" empinfolist size="+employeeinfolist.size()+" employeeWorkingHoursMap="+employeeWorkingHoursMap.size());
		
		ArrayList<AttendanceBean> list = new ArrayList<AttendanceBean>();
		Map<String , ArrayList<Attendance>> attMap = new HashMap<String ,ArrayList<Attendance>>();
		//HashSet<String> projectSet = new HashSet<String>();
		
		for(Attendance attendace : attendanceList){
			ArrayList<Attendance> attList = new ArrayList<Attendance>();
			String key ="";
			/**
			 * @author Anil
			 * @since 27-08-2020
			 */
			if(form.siteLocation){
				key = attendace.getProjectName()+"-"+attendace.getEmpId()+"-"+attendace.getMonth()+"-"+attendace.getSiteLocation();
			}else{
				key = attendace.getProjectName()+"-"+attendace.getEmpId()+"-"+attendace.getMonth();
			}
//			Console.log("key "+key +"added to attmap");
			if(attMap.containsKey(key)){
				attList = attMap.get(key);
				attList.add(attendace);
				attMap.put(key, attList);
			}else{
				attList.add(attendace);
				attMap.put(key, attList);
			}
			if(multicyleFlag){
				hsProject.add(attendace.getProjectName());
			}
			
		}
		Console.log("LOG 3 hsProject "+hsProject.size());
		
		
		for ( Map.Entry<String , ArrayList<Attendance>> entry : attMap.entrySet()) {
		    String key = entry.getKey();
		    ArrayList<Attendance> alist = entry.getValue();
		    AttendanceBean bean = new AttendanceBean();
		   
		    Attendance att = alist.get(0);
		    bean.setEmpId(att.getEmpId());
		    bean.setEmployeeName(att.getEmployeeName());
		    bean.setEmpDesignation(att.getEmpDesignation());
		    bean.setProjectName(att.getProjectName());
		    bean.setOvertimeType(att.getOvertimeType());
		    bean.setMonth(att.getMonth());
		    bean.setShift(att.getShift());
		    bean.setBranchName(att.getBranch());
		    bean.setSiteLocation(att.getSiteLocation());
		    bean.setCompanyId(att.getCompanyId());
		    
		    TreeMap<Date , Attendance> tMap = new TreeMap<Date ,Attendance>();
		    String hours = "";
			DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		    Date date = new Date();
		   
		    int noOfDays=31;
		    if(alist.size()>0){
		    	/**
		    	 * @author Anil
		    	 * @since 04-08-2020
		    	 */
		    	if(alist.get(0).getStartDate()!=null){
		    		date = CalendarUtil.copyDate(alist.get(0).getStartDate());
		    		noOfDays=CalendarUtil.getDaysBetween(alist.get(0).getStartDate(), alist.get(0).getEndDate())+1;
		    		Console.log("noOfDays "+noOfDays);
		    		bean.setFromDate(alist.get(0).getStartDate());
		    		bean.setToDate(alist.get(0).getEndDate());
		    	}else{
		    		date = CalendarUtil.copyDate(alist.get(0).getAttendanceDate());
		    		CalendarUtil.setToFirstDayOfMonth(date);
		    		Date endDate = CalendarUtil.copyDate(date);
		    		CalendarUtil.addMonthsToDate(endDate, 1);
		    		CalendarUtil.addDaysToDate(endDate, -1);
		    		Console.log("END DATE "+endDate);
		    		noOfDays=CalendarUtil.getDaysBetween(date, endDate)+1;
		    		Console.log("noOfDays1.. "+noOfDays);
		    		
		    		bean.setFromDate(CalendarUtil.copyDate(date));
		    		bean.setToDate(endDate);
		    	}
		    	CalendarUtil.resetTime(date);
//		    	CalendarUtil.setToFirstDayOfMonth(date);
//				Console.log("Inside start date of month345 : " + date);			
		    }
		    
		    int daysArray[]=new int[noOfDays];
		    for(int i=0;i<noOfDays;i++){
//		    	Console.log("Inside start date of month : " + date);
		    	tMap.put(fmt.parse(fmt.format(date)), new Attendance());
//		    	Console.log("Inside start date of month -- : " + fmt.parse(fmt.format(date)));
		    	daysArray[i]=date.getDate();
		    	CalendarUtil.addDaysToDate(date, 1);
		    }
		    this.daysArray=daysArray;
		    String daysArrStr="";
			for(int i=0;i<daysArray.length;i++){
				daysArrStr=daysArrStr+daysArray[i]+" ";
			}
//			Console.log("daysArray " + daysArrStr);
			bean.setDaysArray(daysArray);
		    /**
		    * @author Anil,Date :24-01-2019
		    * Added total OT hours column
		    * 
		    */
		    double totalOtHours=0;		    
		    double totalWorkedDays=0; //Ashwini Patil
		    double totalLeaveDays=0;  //Ashwini Patil
		    for(Attendance a : alist){
		    	Console.log("a.getActionLabel()="+a.getActionLabel()+"-"+ a.getAttendanceDate()+" count="+a.getCount());
		    	double whours=a.getWorkingHours();
    			if(a.getWorkingHours()==0)
    				whours=employeeWorkingHoursMap.get(a.getEmpId());
//    					whours=loadWorkingHours(a.getEmpId());
    			a.setWorkingHours(whours);	//06-07-2023	
    			Console.log("whours="+whours+ "a.isOvertime()="+a.isOvertime());
		    	if(a.isOvertime()){
		    		  bean.setOvertimeType(a.getOvertimeType());
		    		  Console.log("isovertime true and hours="+a.getOvertimeHours());
		    		  totalOtHours+=a.getOvertimeHours();
		    	}
		    	if(a.isPresent()){
		    		
//		    		if(a.getActionLabel().equals("P")) {
//		    			totalWorkedDays+=1;}
//		    		else{
		    		//	Console.log("in isPresent worked hours="+a.getWorkedHours()+" working hours="+a.getWorkingHours());
//		    			double whours=a.getWorkingHours();
//		    			if(a.getWorkingHours()==0) {
//		    					whours=loadWorkingHours(a.getEmpId());
//		    			Console.log("in isPresent worked hours="+a.getWorkedHours()+" working hours="+a.getWorkingHours()+"Whours="+whours);
//		    			}
		    			if(a.getWorkedHours()>=whours)
		    				totalWorkedDays+=1;
		    			else {
		    				double day=a.getWorkedHours()/whours;
		    				totalWorkedDays+=day;
//		    				Console.log("day="+day+"total="+totalWorkedDays);
		    			}
		    			if(a.getLeaveHours()>0) {
		    				double leaveDay=a.getLeaveHours()/whours;
		    				totalLeaveDays+=leaveDay;
//		    				Console.log("leaveDay="+leaveDay+"total="+totalLeaveDays);
		    			}
//		    			}
		    		/**
		    		 * 
		    		 */
		    		if(actionFlag){
		    			hours = a.getActionLabel();
		    		}else{
		    			if(workingHourFlag){
		    				hours = a.getTotalWorkedHours()+"";
//		    				Console.log("TotalWorkedHours :" + a.getTotalWorkedHours() +"-"+hours);
		    			}else{
		    				double hh = Math.floor(a.getTotalWorkedHours());
//		    				Console.log("value hh:" + hh);
		    				double mm = (a.getTotalWorkedHours() - hh) * 60.0;
//		    				Console.log("value mm:" + mm);
		    				hours = hh+":"+Math.round(mm);
		    				try{
		    					hours = hhMMForamt.format( hhMMForamt.parse(hours));
		    				}catch(Exception e){
		    					
		    				}
//		    				Console.log("value hh:mm" + hours);
		    			}
		    			if(a.getInTime() != null){
		    				hours += "   "+hhMMForamt.format(a.getInTime());
		    			}
		    			if(a.getOutTime() != null){
		    				hours += " - "+hhMMForamt.format(a.getOutTime());
		    			}
//		    			Console.log("Hours:"+hours);
		    		}
		    	}else if(a.getLeaveType().equals("")){
		    		
		    		if(!a.getActionLabel().equals("")&&a.getActionLabel()!=null) {
		    			hours = a.getActionLabel();
		    			if(!a.getActionLabel().equals("WO")&&!a.getActionLabel().equals("H"))
		    				totalLeaveDays+=1;
		    		}
		    		else if(a.getInTime() != null&&a.getOutTime() == null) {
//		    			Console.log("only intime");
		    			hours="IN";// \n"+hhMMForamt.format(a.getInTime());
		    		}
//		    		else{
//		    			Console.log("in else of IN");
//		    			hours = a.getActionLabel();
//		    			if(!a.getActionLabel().equals("WO"))
//		    				totalLeaveDays+=1;
//		    		}
		    	}else{	
		    		
		    		/**
		    		 * Date : 04-10-2018 BY ANIL
		    		 * setting action label instead of leave name and holiday name
		    		 */
		    		//hours = a.getLeaveType();
//		    		hours = a.getActionLabel();

		    		String leaveshortname=leaveMap.get(a.getLeaveType());
		    		if(leaveshortname.equals("LWP"))
		    			hours="<html><font color=\"red\">"+leaveshortname+"</font></html>";
		    		else
		    			hours="<html><font color=\"orange\">"+leaveshortname+"</font></html>";
		    		
		    		if(a.getActionLabel().equals("H"))
		    			hours=a.getActionLabel();
		    		
		    		if(!leaveshortname.equals("WO")&&!a.getActionLabel().equals("H")){
		    			totalLeaveDays+=1;
//		    			Console.log("totalLeaveDays in other than WO and H"+totalLeaveDays);
		    		}
		    			
		    	
		    		/**
		    		 * End
		    		 */
		    	}	
//		    	Console.log("leave type="+a.getLeaveType()+"and action label="+a.getActionLabel());
//		    	Console.log("attendance date :"+ a.getAttendanceDate());
		    	if(tMap.containsKey(a.getAttendanceDate())){
		    		a.setGroup(hours);
//		    		Console.log("a.setGroup(hours)="+hours);
		    		tMap.put(a.getAttendanceDate(), a);
		    	}
//		    	System.out.println("hours :"+ hours);
		    }
		    Console.log("totalOtHours="+totalOtHours);
		    bean.setOvertimeType(totalOtHours+"");
		    bean.setWorkedDays(nf.format(totalWorkedDays)+"");
		    bean.setLeaveDays(nf.format(totalLeaveDays) +"");
//		    Console.log("totalWorkedDays="+totalWorkedDays+" totalLeaveDays="+totalLeaveDays);
		    bean.setAttendanceObjectMap(tMap);
		    list.add(bean);
		}
		 Console.log("LOG 4 ");
		return list;
	}
	
	
	private void setLeaveShortName() {
		// TODO Auto-generated method stub
		Console.log("in setLeaveShortName leaveType=");
		GenricServiceAsync genasync = GWT.create(GenricService.class);

		Vector<Filter>filterVec =new Vector<Filter>();
		Filter filter=null;
		MyQuerry querry = new MyQuerry();
				
		querry.setQuerryObject(new LeaveType());
				
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				Console.log("in setLeaveShortName onSuccess result size="+result.size());
				if (result.size() == 0) {
					return;
				}else{
					for(SuperModel model:result){
						LeaveType leave  = (LeaveType) model;
						leaveMap.put(leave.getName(),leave.getShortName());
						Console.log("leave map="+leave.getName()+" - "+leave.getShortName());
					}
				}
			}
			
		});
	}
	
	//Ashwini Patil Date:7-07-2022 When we create attendance working hours are getting set to zero which creates problem. So if it is zero this method we can call to get working hours of employee.
//	private double loadWorkingHours(final int empCount) {
//		// TODO Auto-generated method stub
//		Console.log("in loadEmployeeInfo");
////		GenricServiceAsync genasync = GWT.create(GenricService.class);
////
////		Vector<Filter>filterVec =new Vector<Filter>();
////		Filter filter=null;
////		MyQuerry querry = new MyQuerry();
////				
////		filter = new Filter();
////		filter.setQuerryString("empCount");
////		filter.setIntValue(empCount);
////		filterVec.add(filter);
//////		
//////		querry.getFilters().addAll(filterVec);
////		querry.setFilters(filterVec);
////		querry.setQuerryObject(new EmployeeInfo());
////		
////		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
////
////			@Override
////			public void onFailure(Throwable caught) {
////				// TODO Auto-generated method stub
////				
////			}
////
////			@Override
////			public void onSuccess(ArrayList<SuperModel> result) {
////				// TODO Auto-generated method stub
////			//	Console.log("in loadWorkingHours onSuccess result size="+result.size());
////				if (result.size() == 0) {
////					return;
////				}else{
////					for(SuperModel model:result){
////						EmployeeInfo einfo  = (EmployeeInfo) model;
////						workingHours=einfo.getLeaveCalendar().getWorkingHours();						
////						Console.log("returning"+workingHours);
////					}
////				}
////			}
////			
////		});
//		
//		if(employeeinfolist.size()>0) {
//			for(EmployeeInfo employeeInfo : employeeinfolist) {
//				if(employeeInfo.getEmpCount()== empCount) {
//					workingHours=employeeInfo.getLeaveCalendar().getWorkingHours();						
//					Console.log("returning"+workingHours);
//				}
//			}
//		}
//		
//		return workingHours;
//	}

	
	@Override
	public void onCellClick(CellClickEvent event) {
		System.out.println("Cell Click");
		
		ListGridRecord record =  event.getRecord();
        int colNum = event.getColNum();
        ListGridField field = form.grid.getField(colNum);
        String fieldName = form.grid.getFieldName(colNum);
        String fieldTitle = field.getTitle();

        String month=record.getAttribute("Month");
        /**
         * @author Anil ,Date : 02-01-2018 
         * currently mapping date wise data for manual update
         * Need to check it with employee wise
         */
        int empId=0;
        empId=Integer.parseInt(record.getAttribute("EmpID"));
        
        Console.log("Clicked " + fieldTitle + ":" + record.getAttribute(fieldName) + " Emp Name: " + record.getAttribute("EmployeeName")+" Emp ID : "+empId);
        System.out.println("Clicked " + fieldTitle + ":" + record.getAttribute(fieldName) + "Month" + month + " Emp Name: " + record.getAttribute("EmployeeName"));
        if(fieldTitle.equals("EmpID")||fieldTitle.equals("EmployeeName")
        		||fieldTitle.equals("EmpDesignation")||fieldTitle.equals("ProjectName")||fieldTitle.equals("Month")){
        	return;
        }
        
        
        
        GWTCAlert alert=new GWTCAlert();
        DateTimeFormat format=DateTimeFormat.getFormat("MMM-yyyy");
        Date attendanceDate=null;
        try{
        	attendanceDate=format.parse(month);
        }catch(Exception e){
//        	alert.alert("Month parsing issue.");
        	SC.say("Month parsing issue.");
        	return;
        }
        int day=0;
        try{
        	day=Integer.parseInt(fieldTitle.trim());
        }catch(Exception e){
//        	alert.alert("Attendance date parsing issue.");
        	SC.say("Attendance date parsing issue.");
        	return;
        }
        attendanceDate.setDate(day);
        Attendance attendance=getAttendance(attendanceDate,empId);
        if(attendance!=null){
        	/**
        	 * Updated By:Viraj
        	 * Date: 17-06-2019
        	 * Description: To allow editing only to admin users 
        	 */
        	/**Date 6-3-2020 by Amol to allow editing to zonal coordinator raised by rahul T. for orkin**/
        	Console.log("Loggedin user role: "+UserConfiguration.getRole().getRoleName().trim());
        	if(UserConfiguration.getRole().getRoleName().trim().equals("ADMIN")||UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase("Zonal Coordinator")) {
        		Console.log("Inside admin role if condition");
        		updateAttandancePopup.showPopUp();
                updateAttandancePopup.getPopup().getElement().addClassName("attandancePopIndex");
            	updateAttandancePopup.updateView(attendance);
        	}else {
        		SC.say("User role must be admin to edit");
        	}
        }else{
//        	alert.alert("No attendance found.");
        	SC.say("No attendance found.");
        	return;
        }

	}

	private Attendance getAttendance(Date attendanceDate, int empId) {
		Attendance attendance=null;
		for(Attendance att:globalAttendaceList){
			if(att.getEmpId()==empId){
				attendance=att;
				if(att.getAttendanceDate().equals(attendanceDate)&&att.getEmpId()==empId){
					return att;
				}
			}
		}
		
		/**
		 * @author Anil,Date : 06-02-2019
		 * if attendance for that day is not found  then we will create attendance for that day
		 */
		
		Attendance newAttendance=createAttendance(attendanceDate,attendance);
		
		
		return newAttendance;
	}

	private Attendance createAttendance(Date attendanceDate,Attendance attendance) {
		// TODO Auto-generated method stub
		Attendance att=new Attendance();
		att.setActionLabel("");
		att.setAttendanceDate(attendanceDate);
		att.setBranch(attendance.getBranch());
		att.setCompanyId(attendance.getCompanyId());
		att.setCreatedBy(LoginPresenter.loggedInUser);
		att.setCreationDate(new Date());
		att.setEmpDesignation(attendance.getEmpDesignation());
		att.setEmpId(attendance.getEmpId());
		att.setEmployeeName(attendance.getEmployeeName());
		att.setMonth(attendance.getMonth());
		att.setProjectName(attendance.getProjectName());
		att.setShift(attendance.getShift());
		att.setStatus("Created");
		att.setWorkingHours(attendance.getWorkingHours());
		att.setTotalWorkedHours(0);
		att.setLeaveHours(attendance.getWorkingHours());
		
		return att;
	}

	public void downloadAttendance(){

		if(globalAttendaceList == null || globalAttendaceList.size() == 0){
			SC.say("No attendance found.");
		}else{
			csvservice.setAttendanceList(globalAttendaceList, new AsyncCallback<Void>() {
	
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}
	
				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+175;
					Window.open(url, "test", "enabled");
					
				}
			});
		}
	
	}
	
	
	private void loadEmployeeWorkingHours(HashSet<Integer> empidlist, Long companyId) {

		ArrayList<Integer> empid = new ArrayList<Integer>(empidlist);
		employeeinfolist = new ArrayList<EmployeeInfo>();
		service1.getEmployeeinfolist(empid, companyId, new  AsyncCallback<ArrayList<EmployeeInfo>>() {

			@Override
			public void onFailure(Throwable arg0) {
				
			}

			@Override
			public void onSuccess(ArrayList<EmployeeInfo> result) {
				Console.log("in success of loadEmployeeWorkingHours employeeinfolist size="+result.size());
				for(SuperModel model : result) {
					EmployeeInfo empinfo = (EmployeeInfo) model;
					employeeinfolist.add(empinfo);
					if(empinfo.getLeaveCalendar()!=null)
						employeeWorkingHoursMap.put(empinfo.getEmpCount(), empinfo.getLeaveCalendar().getWorkingHours());
				}
			}
		});
		
	}

	private void reactonDownloadFormat1() {
		ArrayList<Attendance> attendaceList = new ArrayList<Attendance>();

		attendaceList.addAll(globalAttendaceList);
        if(attendaceList.size() >0){
        	Console.log("LOG 1");
        ArrayList<AttendanceBean> attendanceBeanList = getAttendaceBeanList(attendaceList);
		csvservice.setAttendance(attendanceBeanList, new AsyncCallback<Void>(){
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			
			@Override
			public void onSuccess(Void result) {
				 Console.log("LOG 6 ");
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+150;
				Window.open(url, "test", "enabled");
			}
		});
        }else{
        	SC.say("Attendance list is empty");
        }
	}
	
	private void reactonDownloadFormat2() {
		ArrayList<Attendance> attendaceList = new ArrayList<Attendance>();

		attendaceList.addAll(globalAttendaceList);
        if(attendaceList.size() >0){
        	Console.log("LOG 1");
        ArrayList<AttendanceBean> attendanceBeanList = getAttendaceBeanList(attendaceList);
		csvservice.setAttendance(attendanceBeanList, new AsyncCallback<Void>(){
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			
			@Override
			public void onSuccess(Void result) {
				 Console.log("LOG 6 ");
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+217;
				Window.open(url, "test", "enabled");
			}
		});
        }else{
        	SC.say("Attendance list is empty");
        }
	}

}
