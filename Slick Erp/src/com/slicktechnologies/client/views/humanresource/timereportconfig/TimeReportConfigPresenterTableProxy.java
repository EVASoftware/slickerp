package com.slicktechnologies.client.views.humanresource.timereportconfig;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import java.util.Comparator;
import java.util.List;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReportConfig;
import com.slicktechnologies.client.views.humanresource.timereportconfig.TimeReportConfigPresenter.TimeReportConfigPresenterTable;
import com.slicktechnologies.client.utility.AppUtility;

public class TimeReportConfigPresenterTableProxy extends TimeReportConfigPresenterTable {
	TextColumn<TimeReportConfig> getNameColumn;
	TextColumn<TimeReportConfig> getPeriodStartDayColumn;
	TextColumn<TimeReportConfig> getCalendarNameColumn;
	TextColumn<TimeReportConfig> getPeriodColumn;
	TextColumn<TimeReportConfig> getCountColumn;
	TextColumn<TimeReportConfig> getvalidFrom;
	TextColumn<TimeReportConfig> getvalidTo;

	public Object getVarRef(String varName) {
		if (varName.equals("getNameColumn"))
			return this.getNameColumn;
		if (varName.equals("getPeriodStartDayColumn"))
			return this.getPeriodStartDayColumn;
		if (varName.equals("getCalendarNameColumn"))
			return this.getCalendarNameColumn;
		if (varName.equals("getPeriodColumn"))
			return this.getPeriodColumn;
		if (varName.equals("getCountColumn"))
			return this.getCountColumn;
		return null;
	}

	public TimeReportConfigPresenterTableProxy() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetName();
		addColumngetPeriod();
		addColumngetPeriodStartDay();
		addColumngetCalendarName();
		addColumngetValidFrom();
		addColumngetValidTo();
		getNameColumn.setSortable(true);
		getPeriodColumn.setSortable(true);
		getvalidFrom.setSortable(true);
		getvalidTo.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<TimeReportConfig>() {
			@Override
			public Object getKey(TimeReportConfig item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetValidFrom();
		addSortinggetValidTo();
		addSortinggetPeriod();
		addSortinggetPeriodStartDay();
		addSortinggetName();
		addSortinggetCalendarName();
	}

	@Override
	public void addFieldUpdater() {
	}

	protected void addSortinggetCount() {
		List<TimeReportConfig> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReportConfig>(list);
		columnSort.setComparator(getCountColumn,
				new Comparator<TimeReportConfig>() {
					@Override
					public int compare(TimeReportConfig e1, TimeReportConfig e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCount() == e2.getCount()) {
								return 0;
							}
							if (e1.getCount() > e2.getCount()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<TimeReportConfig>() {
			@Override
			public String getValue(TimeReportConfig object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		getCountColumn.setSortable(true);
	}

	protected void addColumngetName() {
		getNameColumn = new TextColumn<TimeReportConfig>() {
			@Override
			public String getValue(TimeReportConfig object) {
				return object.getName() + "";
			}
		};
		table.addColumn(getNameColumn, "Name");
	}

	protected void addColumngetPeriod() {
		getPeriodColumn = new TextColumn<TimeReportConfig>() {
			@Override
			public String getValue(TimeReportConfig object) {
				return object.getPeriod() + "";
			}
		};
		table.addColumn(getPeriodColumn, "Period");
	}

	protected void addColumngetPeriodStartDay() {

		getPeriodStartDayColumn = new TextColumn<TimeReportConfig>() {
			@Override
			public String getValue(TimeReportConfig object) {
				String[] weekDay = {"", "Sunday", "Monday", "Tuesday",
						"Wednesday", "Thrusday", "Friday", "Sataurday" };
				int index = object.getPeriodStartDay();
				return weekDay[index];
//				return "";
			}
		};
		table.addColumn(getPeriodStartDayColumn, "Period End Day");
		getPeriodStartDayColumn.setSortable(true);
	}

	protected void addColumngetCalendarName() {
		getCalendarNameColumn = new TextColumn<TimeReportConfig>() {
			@Override
			public String getValue(TimeReportConfig object) {
				return object.getCalendarName() + "";
			}
		};
		table.addColumn(getCalendarNameColumn, "Calendar Name");
		getCalendarNameColumn.setSortable(true);
	}

	protected void addSortinggetValidFrom() {
		List<TimeReportConfig> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReportConfig>(list);
		columnSort.setComparator(getvalidFrom,
				new Comparator<TimeReportConfig>() {
					@Override
					public int compare(TimeReportConfig e1, TimeReportConfig e2) {
						if (e1 != null && e2 != null) {
							return e1
									.getCalendar()
									.getCalStartDate()
									.compareTo(e2.getCalendar().getCalEndDate());
						}
						return -1;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetValidFrom() {
		getvalidFrom = new TextColumn<TimeReportConfig>() {
			@Override
			public String getValue(TimeReportConfig object) {
				return AppUtility.parseDate(object.getCalendar()
						.getCalStartDate());
			}
		};
		table.addColumn(getvalidFrom, "Valid From");
		getCountColumn.setSortable(true);
	}

	protected void addSortinggetValidTo() {
		List<TimeReportConfig> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReportConfig>(list);
		columnSort.setComparator(getvalidTo,
				new Comparator<TimeReportConfig>() {
					@Override
					public int compare(TimeReportConfig e1, TimeReportConfig e2) {
						if (e1 != null && e2 != null) {
							return e1
									.getCalendar()
									.getCalEndDate()
									.compareTo(e2.getCalendar().getCalEndDate());
						}
						return -1;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetValidTo() {
		getvalidTo = new TextColumn<TimeReportConfig>() {
			@Override
			public String getValue(TimeReportConfig object) {
				return AppUtility.parseDate(object.getCalendar()
						.getCalEndDate());
			}
		};
		table.addColumn(getvalidTo, "Valid To");
		getvalidTo.setSortable(true);
	}

	protected void addSortinggetName() {
		List<TimeReportConfig> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReportConfig>(list);
		columnSort.setComparator(getNameColumn,
				new Comparator<TimeReportConfig>() {
					@Override
					public int compare(TimeReportConfig e1, TimeReportConfig e2) {
						if (e1 != null && e2 != null) {
							if (e1.getName() != null && e2.getName() != null) {
								return e1.getName().compareTo(e2.getName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetPeriod() {
		List<TimeReportConfig> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReportConfig>(list);
		columnSort.setComparator(getPeriodColumn,
				new Comparator<TimeReportConfig>() {
					@Override
					public int compare(TimeReportConfig e1, TimeReportConfig e2) {
						if (e1 != null && e2 != null) {
							if (e1.getPeriod() != null
									&& e2.getPeriod() != null) {
								return e1.getPeriod().compareTo(e2.getPeriod());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetPeriodStartDay() {
		List<TimeReportConfig> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReportConfig>(list);
		columnSort.setComparator(getPeriodStartDayColumn,
				new Comparator<TimeReportConfig>() {
					@Override
					public int compare(TimeReportConfig e1, TimeReportConfig e2) {
						if (e1 != null && e2 != null) {
							if (e1.getPeriodStartDay() == e2
									.getPeriodStartDay()) {
								return 0;
							}
							if (e1.getPeriodStartDay() > e2.getPeriodStartDay()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetCalendarName() {
		List<TimeReportConfig> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReportConfig>(list);
		columnSort.setComparator(getCalendarNameColumn,
				new Comparator<TimeReportConfig>() {
					@Override
					public int compare(TimeReportConfig e1, TimeReportConfig e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCalendarName() != null
									&& e2.getCalendarName() != null) {
								return e1.getCalendarName().compareTo(
										e2.getCalendarName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
}
