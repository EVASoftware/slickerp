package com.slicktechnologies.client.views.humanresource.timereportconfig;

import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.views.settings.branch.BranchForm;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReportConfig;

public class TimeReportConfigPresenter extends FormTableScreenPresenter<TimeReportConfig>
{

	TimeReportConfigForm form;
	GenricServiceAsync async = GWT.create(GenricService.class);

	public TimeReportConfigPresenter(FormTableScreen<TimeReportConfig> view,TimeReportConfig model) {
		super(view, model);
		form = (TimeReportConfigForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getConfigQuery());
		form.setPresenter(this);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		if (label.getText().equals("New")) {
			TimeReportConfigPresenter.initalize();
		}
		else{
			changeStatus();
		}
	}

	public void changeStatus()
	{
		model.setStatus(TimeReportConfig.ACTIVE);
		async.save(model, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				form.status.setText(TimeReportConfig.ACTIVE);
				form.setMenuAsPerStatus();
			}
		});
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void makeNewModel() {
		model = new TimeReportConfig();

	}

	public static void initalize() {
		TimeReportConfigPresenterTable gentableScreen=new TimeReportConfigPresenterTableProxy();
		TimeReportConfigForm form=new TimeReportConfigForm(gentableScreen,FormTableScreen.UPPER_MODE,true); 
		gentableScreen.setView(form); 
		gentableScreen.applySelectionModle();
		  
//		  TimeReportConfigPresenterTable gentableSearch=GWT.create(TimeReportConfigPresenterTable.class);
//		  gentableSearch.setView(form);
//		  gentableSearch.applySelectionModle();
		  
		  //ProjectPresenterSearch.staticSuperTable=gentableSearch;
		  //ProjectPresenterSearch searchpopup=new
//		  ProjectPresenterSearchProxy(); //
//		  form.setSearchpopupscreen(searchpopup);
		  
		TimeReportConfigPresenter presenter=new TimeReportConfigPresenter(form,new TimeReportConfig());
		AppMemory.getAppMemory().stickPnel(form);
		 
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.TimeReportConfig")
	public static class TimeReportConfigPresenterSearch extends SearchPopUpScreen<TimeReportConfig> {

		@Override
		public MyQuerry getQuerry() {

			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.TimeReportConfig")
	public static class TimeReportConfigPresenterTable extends SuperTable<TimeReportConfig> implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {

			return null;
		}

		@Override
		public void createTable() {

		}

		@Override
		protected void initializekeyprovider() {

		}

		@Override
		public void addFieldUpdater() {

		}

		@Override
		public void setEnable(boolean state) {

		}

		@Override
		public void applyStyle() {

		}
	};

//	@SuppressWarnings({ "unused", "static-access" })
//	private void reactToNew() {
//		form.setToNewState();
//		this.initalize();
//		form.toggleAppHeaderBarMenu();
//	}

	public MyQuerry getConfigQuery() {
		MyQuerry quer = new MyQuerry(new Vector<Filter>(),new TimeReportConfig());
		return quer;
	}


}
