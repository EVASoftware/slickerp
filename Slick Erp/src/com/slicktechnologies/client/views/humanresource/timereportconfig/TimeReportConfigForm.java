package com.slicktechnologies.client.views.humanresource.timereportconfig;

import java.util.Date;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.mywidgets.*;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReportConfig;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
/**
 * FormTablescreen template.
 */
public class TimeReportConfigForm extends FormTableScreen<TimeReportConfig>  implements ClickHandler
,ChangeHandler{

	// Token to add the varialble declarations

	TextBox name;
	IntegerBox noDays;
	ListBox period;
	ListBox periodStartDay;
	TextArea description;
	ObjectListBox<Calendar> olbleaveCalendar;

	TextBox fromDate, toDate;
	TextBox status;

	// Token to add the concrete presenter class name
	// protected <PresenterClassName> presenter;

	public TimeReportConfigForm(SuperTable<TimeReportConfig> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();

	}

	private void initalizeWidget() {
		name = new TextBox();
		period = new ListBox();
		periodStartDay = new ListBox();
		olbleaveCalendar = new ObjectListBox<Calendar>();
		fromDate = new TextBox();
		fromDate.setEnabled(false);
		toDate = new TextBox();
		toDate.setEnabled(false);
		status = new TextBox();
		noDays = new IntegerBox();
		description = new TextArea();

		period.addItem("--SELECT--");
		period.addItem("Weekly");
		period.setEnabled(false);
		period.setSelectedIndex(1);

		periodStartDay.addItem("--SELECT--");
		periodStartDay.addItem("Sunday");
		periodStartDay.addItem("Monday");
		periodStartDay.addItem("Tuesday");
		periodStartDay.addItem("Wednesday");
		periodStartDay.addItem("Thursday");
		periodStartDay.addItem("Friday");
		periodStartDay.addItem("Saturday");
		status.setText("Created");
		status.setEnabled(false);
		this.noDays.setEnabled(false);
		this.noDays.setValue(7);

		Calendar.makeObjectListBoxLive(olbleaveCalendar);
		olbleaveCalendar.addChangeHandler(this);

	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		// Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames = new String[] { "Submit", "New" };
		// ////////////////////////////////Form Field
		// Declaration/////////////////////////////////////////////////

		// ////////////////////////// Form Field
		// Initialization////////////////////////////////////////////////////

		// Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingShiftInformation1 = fbuilder
				.setlabel("Time Reporting Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Name", name);
		FormField ftbname = fbuilder.setMandatory(true)
				.setMandatoryMsg("Name is Mandatory !").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Period", period);
		FormField ftbperiod = fbuilder.setMandatory(true)
				.setMandatoryMsg("Time Report Period is Mandatory !")
				.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Period End Day", periodStartDay);
		FormField fperiodstartday = fbuilder.setMandatory(true)
				.setMandatoryMsg("Period End Day is Mandatory !").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Number of Days", noDays);
		FormField fnoDays = fbuilder.setMandatory(true)
				.setMandatoryMsg("Number of  Days is Mandatory !")
				.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status ", status);
		FormField fstatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingShiftInformation = fbuilder
				.setlabel("Calendar Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Calendar ", olbleaveCalendar);
		FormField folbcField = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Valid From ", fromDate);
		FormField fromDate = fbuilder.setMandatory(true)
				.setMandatoryMsg(" Valid From is Mandatory !").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Valid To ", toDate);
		FormField toDate = fbuilder.setMandatory(true)
				.setMandatoryMsg("Valid To is Mandatory !").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Description", description);
		FormField ftbDescription = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();

		

		

		// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// //////////////////////////////Lay Out Making
		// Code///////////////////////////////////////////////////////////////

		FormField[][] formfield = { { fgroupingShiftInformation1 },
				{ ftbname, ftbperiod, fperiodstartday }, { fnoDays, fstatus },
				{ fgroupingShiftInformation },
				{ folbcField, fromDate, toDate }, { ftbDescription }, };
		this.fields = formfield;
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(TimeReportConfig config) {
		if (this.name.getValue() != null)
			config.setName(name.getValue());
		if (this.periodStartDay.getSelectedIndex() != 0) {
			config.setPeriodStartDay(this.periodStartDay.getSelectedIndex());
		}

		if (this.period.getSelectedIndex() != 0) {
			int selIndex = period.getSelectedIndex();
			config.setPeriod(this.period.getValue(selIndex));
		}

		if (this.description.getValue() != null) {
			config.setDescription(description.getValue());
		}

		if (this.olbleaveCalendar.getValue() != null) {
			config.setCalendar(olbleaveCalendar.getSelectedItem());
			config.setCalendarName(olbleaveCalendar.getSelectedItem());
		}

		if (this.status.getValue() != null) {
			config.setStatus(status.getValue());
		}

		if (this.noDays.getValue() != null) {
			config.setNoDays(noDays.getValue());
		}

		presenter.setModel(config);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(TimeReportConfig view) {
		if (view.getName() != null)
			name.setValue(view.getName());
		if (view.getPeriod() != null) {
			for (int i = 0; i < period.getItemCount(); i++) {
				String data = period.getItemText(i);
				if (data.equals(view.getPeriod())) {
					period.setSelectedIndex(i);
					break;
				}
			}
		}
		
		

//		this.periodStartDay.setSelectedIndex(view.periodEndDay + 1);
		this.periodStartDay.setSelectedIndex(view.getPeriodStartDay());
		this.description.setText(view.description);
		this.olbleaveCalendar.setValue(view.getCalendarName());
		Calendar cal = view.getCalendar();
		fromDate.setText(AppUtility.parseDate(cal.getCalStartDate()));
		toDate.setText(AppUtility.parseDate(cal.getCalEndDate()));
		presenter.setModel(view);
	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.TIMEREPORTCONFIG,
				LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value.
	 */

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		fromDate.setEnabled(false);
		toDate.setEnabled(false);
		status.setEnabled(false);
		this.period.setEnabled(false);
		this.noDays.setEnabled(false);
		this.noDays.setEnabled(false);
		this.processLevelBar.setVisibleFalse(false);
	}

	public void setMenuAsPerStatus() {
		TimeReportConfig config = (TimeReportConfig) presenter.getModel();
		System.out.println("val" + config);
		if (config.getStatus().equals(TimeReportConfig.ACTIVE)) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Edit"))
					menus[k].setVisible(false);

			}

			InlineLabel label[] = this.processLevelBar.btnLabels;
			for (int i = 0; i < label.length; i++) {
				if (label[i].getText().equals("New") == false)
					label[i].setVisible(false);

			}
		}

	}

	@Override
	public void onChange(ChangeEvent event) {

		if (event.getSource() == olbleaveCalendar) {
			if (olbleaveCalendar.getValue() != null) {

				Date fromDate = olbleaveCalendar.getSelectedItem().getCalStartDate();
				Date endDate = olbleaveCalendar.getSelectedItem().getCalEndDate();
				String from = AppUtility.parseDate(fromDate);
				String to = AppUtility.parseDate(endDate);

				this.fromDate.setText(from);
				this.toDate.setText(to);
			} else {
				this.fromDate.setText("");
				this.toDate.setText("");
			}
		}
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		setMenuAsPerStatus();
	}
	

	

	@Override
	public void clear() {
		super.clear();
		this.status.setText(TimeReportConfig.CREATED);
		this.noDays.setValue(7);

	}

	public void setEnabled(boolean state) {
		super.setEnable(state);
		status.setEnabled(false);

	}
}



