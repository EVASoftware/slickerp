package com.slicktechnologies.client.views.humanresource.shiftpattern;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.*;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.ShiftPattern;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
/**
 * FormTablescreen template.
 */
public class ShiftPatternForm extends FormTableScreen<ShiftPattern> implements ClickHandler,HasValue<ArrayList<Shift>>,CompositeInterface {

	//Token to add the varialble declarations
  ObjectListBox<Config> olbPatternCategory;
  IntegerBox ibPatternId;
  TextBox tbPatternName,tbShortName;
  ShiftListBox olbshift;
  Button btaddshift;
  ShiftTable shiftTable;
	
	
	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;
	
	
	
	public ShiftPatternForm  (SuperTable<ShiftPattern> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		shiftTable.connectToLocal();
	}
	
	private void initalizeWidget()
	{
		
		olbPatternCategory = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbPatternCategory,Screen.PATTERNCATEGORY);
        ibPatternId=new IntegerBox();
        ibPatternId.setEnabled(false);
        tbPatternName=new TextBox();
        tbShortName=new TextBox();
       
        btaddshift = new Button("ADD");
        btaddshift.addClickHandler(this);
        shiftTable=new ShiftTable();
        
        
		
		
		 olbshift = new ShiftListBox();
	     this.initalizeShiftListBox();
    
	}

	/*
	 * Method template to create the formtable screen
	 */
	
	@Override
	public void createScreen() {
		
		//Token to initialize the processlevel menus.
	    initalizeWidget();
		

	    this.processlevelBarNames=new String[]{"Delete","New"};


		
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////
	    
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
fbuilder = new FormFieldBuilder();
FormField fgroupingShiftPatternInformation=fbuilder.setlabel("Shift Pattern Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

fbuilder = new FormFieldBuilder("* Pattern Name",tbPatternName);
FormField ftbPatternName= fbuilder.setMandatory(true).setMandatoryMsg("Pattern Name is mandatory!").setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("* Short Name",tbShortName);
FormField ftbShortName= fbuilder.setMandatory(true).setMandatoryMsg("Short Name is mandatory!").setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("* Pattern Category",olbPatternCategory);
FormField folbPatternCategory= fbuilder.setMandatory(true).setMandatoryMsg("Category Name is mandatory!").setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder();
FormField fgroupingShiftInformation=fbuilder.setlabel("Shift Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
fbuilder = new FormFieldBuilder("Shift",olbshift);
FormField folbshift= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("",btaddshift);
FormField faddbutton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();		
fbuilder = new FormFieldBuilder("",shiftTable.getTable());
FormField fshifttable= fbuilder.setMandatory(true).setMandatoryMsg("Please add shift information").setRowSpan(0).setColSpan(4).build();

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		
		
		FormField[][] formfield = {{fgroupingShiftPatternInformation},
  {ftbPatternName,ftbShortName},
  {folbPatternCategory},{fgroupingShiftInformation},{folbshift,faddbutton},{fshifttable},
  };
		
        this.fields=formfield;		
   }

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(ShiftPattern model) 
	{
	if(tbPatternName.getValue()!=null)
	  model.setPatternName(tbPatternName.getValue());
	if(tbShortName.getValue()!=null)
	  model.setPattShortName(tbShortName.getValue());
	if(olbPatternCategory.getValue()!=null)
	  model.setPatternCategory(olbPatternCategory.getValue());
	if(shiftTable.getValue()!=null)
		model.setShift(shiftTable.getValue());
    presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(ShiftPattern view) 
	{
		
	if(view.getPatternName()!=null)
	  tbPatternName.setValue(view.getPatternName());
	if(view.getPattShortName()!=null)
	  tbShortName.setValue(view.getPattShortName());
	if(view.getPatternCategory()!=null)
	  olbPatternCategory.setValue(view.getPatternCategory());
	if(view.getShift()!=null)
		shiftTable.setValue(view.getShift());

	presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SHIFTPATTERN,LoginPresenter.currentModule.trim());	
	}
	
	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		this.ibPatternId.setValue(presenter.getModel().getCount());
	}
	
	@Override
	public void onClick(ClickEvent event) {
		     if(this.olbshift.getValue()!=null)
		     {
			    this.shiftTable.getDataprovider().getList().add(olbshift.getSelectedItem());
			    olbshift.setSelectedIndex(0);
		     }
		     
			 
		 
	
		
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibPatternId.setEnabled(false);
		shiftTable.setEnabled(state);
	}
	
	
	

	//***********************************************Business Logic Part****************************************************//
	
	
	protected void initalizeShiftListBox()
	{
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Shift());
		olbshift.MakeLive(querry);
	}

	
	/**
	 * Code for adding shift data to the Shift Table
	 */
	
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<ArrayList<Shift>> handler) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fireEvent(GwtEvent<?> event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<Shift> getValue() {
		List<Shift>list=this.shiftTable.getDataprovider().getList();
		ArrayList<Shift>vec=new ArrayList<Shift>();
		vec.addAll(list);
		return vec;
	}

	@Override
	public void setValue(ArrayList<Shift> value) {
		shiftTable.connectToLocal();
		shiftTable.getDataprovider().getList().addAll(value);
	}

	@Override
	public void setValue(ArrayList<Shift> value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validate() {
		boolean tableValue=true;
		boolean superValidate=super.validate();
		if(superValidate==false)
			return false;
		if(this.shiftTable.getDataprovider().getList().size()==0)
		
		{
			showDialogMessage("Please add at least one shift!");
			return false;
		}
		return true;
		
	}
	
	@Override
	public void clear() {
		super.clear();
		if(shiftTable!=null)
			shiftTable.clear();
		
	}

}
