package com.slicktechnologies.client.views.humanresource.shiftpattern;
import com.google.gwt.event.dom.client.ClickEvent;

import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.ShiftPattern;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
/**
 *  FormTableScreen presenter template
 */
public class ShiftPatternPresenter extends FormTableScreenPresenter<ShiftPattern>  {

	//Token to set the concrete form
	ShiftPatternForm form;
	
	public ShiftPatternPresenter (FormTableScreen<ShiftPattern> view, ShiftPattern model) {
		super(view, model);
		form=(ShiftPatternForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getShiftPatternQuery());
		form.setPresenter(this);
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("Delete"))
		{
			
			if(this.form.validate())
			{
			List<ShiftPattern>data=form.getSupertable().getDataprovider().getList();
			/*
			 * Deleting
			 */
			for(int i=0;i<data.size();i++)
			{
				if(data.get(i).getId().equals(model.getId()))
				{
					data.remove(i);
				}
				form.getSupertable().getDataprovider().refresh();
			}
			reactOnDelete();
			
			}
		}
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new ShiftPattern();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getShiftPatternQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new ShiftPattern());
		return quer;
	}
	
	public void setModel(ShiftPattern entity)
	{
		model=entity;
	}
	
	public static void initalize()
	{
			 ShiftPatternPresenterTable gentableScreen=new ShiftPatternPresenterTableProxy();
			
			ShiftPatternForm  form=new  ShiftPatternForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			  
			
			 ShiftPatternPresenter  presenter=new  ShiftPatternPresenter(form,new ShiftPattern());
			 AppMemory.getAppMemory().stickPnel(form);
			
			
		 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.shiftlayer.ShiftPattern")
		 public static class  ShiftPatternPresenterTable extends SuperTable<ShiftPattern> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.shiftpattern.ShiftPattern")
			 public static class  ShiftPatternPresenterSearch extends SearchPopUpScreen<ShiftPattern>{

				@Override
				public MyQuerry getQuerry() {
					MyQuerry querry=new MyQuerry();
					querry.setQuerryObject(new ShiftPattern());
					return querry;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
			public void reactTo()
			{
				
			}

}
