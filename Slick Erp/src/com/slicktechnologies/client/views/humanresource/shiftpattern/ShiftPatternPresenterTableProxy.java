package com.slicktechnologies.client.views.humanresource.shiftpattern;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.client.views.humanresource.shiftpattern.ShiftPatternPresenter.ShiftPatternPresenterTable;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.ShiftPattern;

public class ShiftPatternPresenterTableProxy extends ShiftPatternPresenterTable {
  TextColumn<ShiftPattern> getPatternNameColumn;
  TextColumn<ShiftPattern> getPattShortNameColumn;
  TextColumn<ShiftPattern> getPatternCategoryColumn;
  TextColumn<ShiftPattern> getCountColumn;
  public Object getVarRef(String varName)
  {
  if(varName.equals("getPatternNameColumn"))
  return this.getPatternNameColumn;
  if(varName.equals("getPattShortNameColumn"))
  return this.getPattShortNameColumn;
  if(varName.equals("getPatternCategoryColumn"))
  return this.getPatternCategoryColumn;
  if(varName.equals("getCountColumn"))
  return this.getCountColumn;
   return null ;
  }
  public ShiftPatternPresenterTableProxy()
  {
  super();
  }
  @Override public void createTable() {
  addColumngetCount();
  addColumngetPatternName();
  addColumngetPattShortName();
  addColumngetPatternCategory();
  }
  @Override
  protected void initializekeyprovider() {
  keyProvider= new ProvidesKey<ShiftPattern>()
  {
  @Override
  public Object getKey(ShiftPattern item)
  {
  if(item==null)
  {
  return null;
  }
  else
  return item.getId();
  }
  };
  }
  @Override
  public void setEnable(boolean state)
  {
  }
  @Override
  public void applyStyle()
  {
  }
   public void addColumnSorting(){
  addSortinggetCount();
  addSortinggetPatternName();
  addSortinggetPattShortName();
  addSortinggetPatternCategory();
  }
  @Override public void addFieldUpdater() {
  }
  protected void addSortinggetCount()
  {
  List<ShiftPattern> list=getDataprovider().getList();
  columnSort=new ListHandler<ShiftPattern>(list);
  columnSort.setComparator(getCountColumn, new Comparator<ShiftPattern>()
  {
  @Override
  public int compare(ShiftPattern e1,ShiftPattern e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCount()== e2.getCount()){
  return 0;}
  if(e1.getCount()> e2.getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCount()
  {
  getCountColumn=new TextColumn<ShiftPattern>()
  {
  @Override
  public String getValue(ShiftPattern object)
  {
  if( object.getCount()==-1)
  return "N.A";
  else return object.getCount()+"";
  }
  };
  table.addColumn(getCountColumn,"Id");
  getCountColumn.setSortable(true);
  }
  protected void addSortinggetPatternName()
  {
  List<ShiftPattern> list=getDataprovider().getList();
  columnSort=new ListHandler<ShiftPattern>(list);
  columnSort.setComparator(getPatternNameColumn, new Comparator<ShiftPattern>()
  {
  @Override
  public int compare(ShiftPattern e1,ShiftPattern e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getPatternName()!=null && e2.getPatternName()!=null){
  return e1.getPatternName().compareTo(e2.getPatternName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetPatternName()
  {
  getPatternNameColumn=new TextColumn<ShiftPattern>()
  {
  @Override
  public String getValue(ShiftPattern object)
  {
  return object.getPatternName()+"";
  }
  };
  table.addColumn(getPatternNameColumn,"Pattern");
  getPatternNameColumn.setSortable(true);
  }
  protected void addSortinggetPattShortName()
  {
  List<ShiftPattern> list=getDataprovider().getList();
  columnSort=new ListHandler<ShiftPattern>(list);
  columnSort.setComparator(getPattShortNameColumn, new Comparator<ShiftPattern>()
  {
  @Override
  public int compare(ShiftPattern e1,ShiftPattern e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getPattShortName()!=null && e2.getPattShortName()!=null){
  return e1.getPattShortName().compareTo(e2.getPattShortName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetPattShortName()
  {
  getPattShortNameColumn=new TextColumn<ShiftPattern>()
  {
  @Override
  public String getValue(ShiftPattern object)
  {
  return object.getPattShortName()+"";
  }
  };
  table.addColumn(getPattShortNameColumn,"Pattern Short Name");
  getPattShortNameColumn.setSortable(true);
  }
  protected void addSortinggetPatternCategory()
  {
  List<ShiftPattern> list=getDataprovider().getList();
  columnSort=new ListHandler<ShiftPattern>(list);
  columnSort.setComparator(getPatternCategoryColumn, new Comparator<ShiftPattern>()
  {
  @Override
  public int compare(ShiftPattern e1,ShiftPattern e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getPatternCategory()!=null && e2.getPatternCategory()!=null){
  return e1.getPatternCategory().compareTo(e2.getPatternCategory());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetPatternCategory()
  {
  getPatternCategoryColumn=new TextColumn<ShiftPattern>()
  {
  @Override
  public String getValue(ShiftPattern object)
  {
  return object.getPatternCategory()+"";
  }
  };
  table.addColumn(getPatternCategoryColumn,"Pattern Category");
  getPatternCategoryColumn.setSortable(true);
  }
}
