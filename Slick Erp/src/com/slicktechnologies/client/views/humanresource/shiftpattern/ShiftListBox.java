package com.slicktechnologies.client.views.humanresource.shiftpattern;

import java.util.ArrayList;
import java.util.Collection;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;

public class ShiftListBox extends ObjectListBox<Shift> {
	private final GenricServiceAsync genricservice = GWT
			.create(GenricService.class);
	
	
	public void MakeLive(MyQuerry query)
	{
		
		genricservice.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>()
			{

					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
						
					}

					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ArrayList<SuperModel> result)
					{
					
						items=new ArrayList<Shift>();
						for(SuperModel model:result)
						{
							Shift sh=(Shift) model;
							items.add(sh);
						}
						Shift shift=new Shift();
						shift.setShiftName("Weekly Off");
						shift.setShortName("W.O");
						shift.setShiftDesc("Off WeekDay");
						shift.setWeeklyoff(true);
						items.add(shift);
						 setListItems(items);
					  
						
					}
		});
	}

}
