package com.slicktechnologies.client.views.humanresource.shiftpattern;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;

public class ShiftTable extends SuperTable<Shift> {


	TextColumn<Shift> shiftNameColumn;
	private Column<Shift, String> delete;
	protected TextColumn<Shift> getToTimeColumn;
	protected TextColumn<Shift> getFromTimeColumn;

	public ShiftTable() 
	{
		super();
	}

	@Override
	public void createTable() {
		addColumnShiftName();
		addColumngetFromTime();
		addColumngetToTime();
		addColumnDelete();
		setFieldUpdaterOnDelete();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}

	private void addeditColumn()
	{
		addColumnShiftName();
		addColumngetFromTime();
		addColumngetToTime();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	}

	private void addViewColumn()
	{
		addColumnShiftName();
		addColumngetFromTime();
		addColumngetToTime();
	}

	protected void addColumngetFromTime()
	{
		getFromTimeColumn=new TextColumn<Shift>()
				{
			@Override
			public String getValue(Shift object)
			{
				return object.getFromTime()+"";
			}
				};
				table.addColumn(getFromTimeColumn,"In Time(24hr)");

	}

	protected void addColumngetToTime()
	{
		getToTimeColumn=new TextColumn<Shift>()
				{
			@Override
			public String getValue(Shift object)
			{
				return object.getToTime()+"";
			}
				};
				table.addColumn(getToTimeColumn,"Out Time(24hr)");
				getToTimeColumn.setSortable(true);
	}
	public void addColumnShiftName()
	{
		shiftNameColumn = new TextColumn<Shift>() {
			@Override
			public String getValue(Shift object) {
				return object.getShiftName();
			}
		};
		table.addColumn(shiftNameColumn,"Shift");
	}

	private void addColumnDelete()
	{
		ButtonCell btnCell= new ButtonCell();
		delete = new Column<Shift, String>(btnCell) {

			@Override
			public String getValue(Shift object) {

				return "Delete";
			}
		};
		table.addColumn(delete,"Delete");
	}

	private void setFieldUpdaterOnDelete()
	{
		delete.setFieldUpdater(new FieldUpdater<Shift, String>() {

			@Override
			public void update(int index, Shift object, String value) {
				getDataprovider().getList().remove(object);

			}
		});
	}




	public  void setEnabled(boolean state)
	{
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
			table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
	}


	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<Shift>() {
			@Override
			public Object getKey(Shift item) {
				if(item==null)
					return null;
				else
					return item.getId();
			}
		};


	}


	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
			table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();

	}

	@Override
	public void applyStyle() {

	}






}
