package com.slicktechnologies.client.views.humanresource.employeeattendance;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.humanresourcelayer.attendance.EmployeeAttendance;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class EmployeeAttendanceSearchPopUp extends SearchPopUpScreen<EmployeeAttendance>{
	public PersonInfoComposite personInfo;
	public ObjectListBox<Config> olbStatus;
	public DateComparator comparator;
	public TextBox punchId;
	public IntegerBox empId;




	public Object getVarRef(String varName)
	{
		if(varName.equals("personInfo"))
			return this.personInfo;
		if(varName.equals("olbStatus"))
			return this.olbStatus;
		if(varName.equals("comparator"))
			return this.comparator;
		if(varName.equals("punchId"))
			return this.punchId;
		if(varName.equals("empId"))
			return this.empId;
		
		return null ;
	}
	
	public EmployeeAttendanceSearchPopUp(boolean b) {
		super(b);
		createGui();
	}
	public EmployeeAttendanceSearchPopUp()
	{
		super();
		createGui();
	}
	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Employee());
	    personInfo=new PersonInfoComposite(querry, false);
		olbStatus=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbStatus,Screen.ATTENDANCECONFIG);
		comparator=new DateComparator("date",new EmployeeAttendance());
		punchId=new TextBox();
		
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(4).build();
		
		builder = new FormFieldBuilder("Status",olbStatus);
		FormField folbBStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("From Date",comparator.getFromDate());
		FormField folbfromDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date",comparator.getToDate());
		FormField folbToDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Punch ID",punchId);
		FormField fpunchId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		

		this.fields=new FormField[][]{

				{folbfromDate,folbToDate,folbBStatus,fpunchId},
				{fpersonInfo},
				
		};
		
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
	}
	
	
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;


		if(olbStatus.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(olbStatus.getValue().trim());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}

		if(comparator.getValue()!=null)
		{
			
			filtervec.addAll(comparator.getValue());
		}

		

		if(personInfo.getIdValue()!=-1)
		{  
			temp=new Filter();
			temp.setIntValue(personInfo.getIdValue());
			temp.setQuerryString("empid");
			filtervec.add(temp);
		}

		if(!(personInfo.getFullNameValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(personInfo.getFullNameValue());
			temp.setQuerryString("empName");
			filtervec.add(temp);
		}
		if(personInfo.getCellValue()!=-1l)
		{
			temp=new Filter();
			temp.setLongValue(personInfo.getCellValue());
			temp.setQuerryString("empCellNo");
			filtervec.add(temp);
		}
		
		if(!punchId.getValue().trim().equals(""))
		{
			
			temp.setStringValue(punchId.getValue().trim());
			temp.setQuerryString("punchCardNo");
			filtervec.add(temp);
		}

		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new EmployeeAttendance());
		return querry;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

	
}
