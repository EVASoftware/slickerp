package com.slicktechnologies.client.views.humanresource.employeeattendance;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextInputCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.humanresourcelayer.attendance.EmployeeAttendance;

public class EmployeeAttendanceTable extends SuperTable<EmployeeAttendance> {

	private final GenricServiceAsync async=GWT.create(GenricService.class);
	TextColumn<EmployeeAttendance> Id;
	TextColumn<EmployeeAttendance> employeeName;
	TextColumn<EmployeeAttendance> punchCardNo;
	TextColumn<EmployeeAttendance> date;
	Column<EmployeeAttendance,String>status;
	TextColumn<EmployeeAttendance>inTime;
	TextColumn<EmployeeAttendance>outTime;
	TextColumn<EmployeeAttendance>Status;
	TextColumn<EmployeeAttendance>otType;
	TextColumn<EmployeeAttendance>leavType;
	TextColumn<EmployeeAttendance>projectName;
	TextColumn<EmployeeAttendance>branch;
	TextColumn<EmployeeAttendance>employeerole;
	TextColumn<EmployeeAttendance>employeeType;
	TextColumn<EmployeeAttendance>department;
	TextColumn<EmployeeAttendance>designation;
	
	TextColumn<EmployeeAttendance>estatus;
	
	
	
	Column<EmployeeAttendance, Date> einTime;
	Column<EmployeeAttendance,Date>eoutTime;
	
	
    
	final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy HH:mm:ss");
	
	
	
	
	
	
	
	public EmployeeAttendanceTable() 
	{
		super();
		setHeight("500px");
		
	}
	/**
	 * 
	 * >branch;
	TextColumn<EmployeeAttendance>employeerole;
	TextColumn<EmployeeAttendance>employeeType;
	TextColumn<EmployeeAttendance>department;
	TextColumn<EmployeeAttendance>designation
	 */
	
	@Override
	public void createTable() {
		
		addColumnId();
		addColumnEmployeeName();
		addColumnPunch();
		addColumnDate();
		addColumnviewStatus();
		addColumnViewOtType();
		addColumnViewLeavType();
		addColumnViewProjectName();
		addColumnEmployeeinTime();
		addColumnEmployeeoutTime();
		addColumnViewEmployeerole();
		addColumnViewEmployeeType();
		addColumnViewEmployeeDepartment();
		addColumnViewEmployeeDesignation();
		
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	    addColumnSorting();
	   
	}
	
	

	
	
	public void addColumnId()
	{
		Id = new TextColumn<EmployeeAttendance>() {
			@Override
			public String getValue(EmployeeAttendance object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(Id,"Employee ID");
		table.setColumnWidth(Id, 150, Unit.PX);
	}
	
	public void addColumnEmployeeName()
	{
		employeeName = new TextColumn<EmployeeAttendance>() {
			@Override
			public String getValue(EmployeeAttendance object) {
				
				return object.getEmpName()+"";
			}
		};
		table.addColumn(employeeName,"Name");
		table.setColumnWidth(employeeName, 150, Unit.PX);
	}
	
	

	  
	 

	 void addColumnEmployeeStatus()
	 {
		 
		 TextInputCell cell=new TextInputCell();
		 status=new Column<EmployeeAttendance,String>(cell) {

				@Override
				public String getValue(EmployeeAttendance object) {
					// TODO Auto-generated method stub
					return object.getStatus();
				}
			};
			
			table.setColumnWidth(status, 80, Unit.PX);
	 }
	 
	 
	 void addFieldUpdaterStatus()
	 {
		 FieldUpdater<EmployeeAttendance,String> statusUpdater=new FieldUpdater<EmployeeAttendance, String>() {

			@Override
			public void update(int index, EmployeeAttendance object,
					String value) {
				object.setStatus(value);
				
			}
		};
		if(status!=null)
		  status.setFieldUpdater(statusUpdater);
	 }
	 
	 void addFieldUpdaterpunchInTime()
	 {
		 FieldUpdater<EmployeeAttendance,Date> statusUpdater=new FieldUpdater<EmployeeAttendance, Date>() {

			@Override
			public void update(int index, EmployeeAttendance object,
					Date value) {
				try{
					 Date val=fmt.parse(value.toString().trim());
					object.setInTime(val);
					
					
				}
				catch(Exception e)
				{
					
				}
				
			}
		};
		if(einTime!=null)
		  einTime.setFieldUpdater(statusUpdater);
		
		table.setColumnWidth(einTime, 150, Unit.PX);
	 }
	 
	 
	 void addFieldUpdaterpunchOutTime()
	 {
		 DatePickerCell date= new DatePickerCell(fmt);
		 FieldUpdater<EmployeeAttendance,Date> statusUpdater=new FieldUpdater<EmployeeAttendance, Date>(){

			@Override
			public void update(int index, EmployeeAttendance object,Date value) {
				try{
					Date val=fmt.parse(value.toString().trim());
					object.setOutTime(val);
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
				}
				
			}
		};
		if(eoutTime!=null)
		  eoutTime.setFieldUpdater(statusUpdater);
		table.setColumnWidth(eoutTime,150, Unit.PX);
	 }
	 
	 void addColumnviewStatus()
	 {
		 
		 estatus=new TextColumn<EmployeeAttendance>() {

				@Override
				public String getValue(EmployeeAttendance object) {
					// TODO Auto-generated method stub
					return object.getStatus();
				}
			};
			table.addColumn(estatus,"Status");
			table.setColumnWidth(estatus,150, Unit.PX);
	 }
	 
	void addColumnViewOtType()
	 {
		 otType=new TextColumn<EmployeeAttendance>() {

				@Override
				public String getValue(EmployeeAttendance object) {
					// TODO Auto-generated method stub
					return object.getOtType();
				}
			};
			table.addColumn(otType,"Over Time type");
			table.setColumnWidth(otType,120, Unit.PX);
	 }
	
	void addColumnViewLeavType()
		{
			leavType=new TextColumn<EmployeeAttendance>() {

				@Override
				public String getValue(EmployeeAttendance object) {
					// TODO Auto-generated method stub
					return object.getLeaveType();
				}
			};
			table.addColumn(leavType,"Leav Type");
			table.setColumnWidth(leavType,110, Unit.PX);
		}
	/*
	 * addColumnViewEmployeerole();
		addColumnViewEmployeeType();
		addColumnViewEmployeeDepartment();
		addColumnViewEmployeeDesignation();
	 */
	
	void addColumnViewEmployeeDesignation()
	{
		designation=new TextColumn<EmployeeAttendance>() {

			@Override
			public String getValue(EmployeeAttendance object) {
				// TODO Auto-generated method stub
				return object.getDesignation();
			}
		};
		table.addColumn(designation,"Designation");
		table.setColumnWidth(designation,110, Unit.PX);
	}
	
	
	void addColumnViewEmployeeDepartment()
	{
		department=new TextColumn<EmployeeAttendance>() {

			@Override
			public String getValue(EmployeeAttendance object) {
				// TODO Auto-generated method stub
				return object.getDepartment();
			}
		};
		table.addColumn(department,"Department");
		table.setColumnWidth(department,110, Unit.PX);
	}
	
	void addColumnViewEmployeeType()
	{
		employeeType=new TextColumn<EmployeeAttendance>() {

			@Override
			public String getValue(EmployeeAttendance object) {
				// TODO Auto-generated method stub
				return object.getEmployeeType();
			}
		};
		table.addColumn(employeeType,"Type");
		table.setColumnWidth(employeeType,100, Unit.PX);
	}
	
	void addColumnViewEmployeerole()
	{
		employeerole=new TextColumn<EmployeeAttendance>() {

			@Override
			public String getValue(EmployeeAttendance object) {
				// TODO Auto-generated method stub
				return object.getEmployeerole();
			}
		};
		table.addColumn(employeerole,"Role");
		table.setColumnWidth(employeerole,100, Unit.PX);
	}
	void addColumnViewProjectName()
		{
			projectName=new TextColumn<EmployeeAttendance>() {

				@Override
				public String getValue(EmployeeAttendance object) {
					// TODO Auto-generated method stub
					return object.getProjectName();
				}
			};
			table.addColumn(projectName,"Project Name");
			table.setColumnWidth(projectName,150, Unit.PX);
		}
	 
	
		void addColumnPunch()
		{
			punchCardNo=new TextColumn<EmployeeAttendance>() {

				@Override
				public String getValue(EmployeeAttendance object) {
					// TODO Auto-generated method stub
					return object.getPunchCardNo();
				}
			};
			table.addColumn(punchCardNo,"Punch Card Number");
			table.setColumnWidth(punchCardNo,150, Unit.PX);
		}
		void addColumnDate()
		{
			date=new TextColumn<EmployeeAttendance>() {

				@Override
				public String getValue(EmployeeAttendance object) {
					// TODO Auto-generated method stub
					return AppUtility.parseDate(object.getDate());
				}
			};
			table.addColumn(date,"Date");
			table.setColumnWidth(date,150, Unit.PX);
		}
		
		void addColumnEmployeeinTime()
		{
//			try {
			final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy hh:mm:ss");
				DatePickerCell date= new DatePickerCell(fmt);
				inTime=new TextColumn<EmployeeAttendance>() {

					@Override
					public String getValue(EmployeeAttendance object) {
						// TODO Auto-generated method stub
						if(object.getInTime()!=null){
							return fmt.parse(fmt.format(object.getInTime()))+"";
						}else{
							return "";
						}
					}
				};
				table.addColumn(inTime,"In Time");
				table.setColumnWidth(inTime,150, Unit.PX);
//			} catch (Exception e) {
//				e.printStackTrace();
//				System.out.println(e.getMessage());
//			}
			
		}
		
		
		void addColumnEmployeeeditinTime()
		{
			final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy hh:mm:ss");
			DatePickerCell date= new DatePickerCell(fmt);
			einTime=new Column<EmployeeAttendance,Date>(date) {

				@Override
				public Date getValue(EmployeeAttendance object) {
					// TODO Auto-generated method stub
					return fmt.parse(fmt.format(object.getInTime()));
				}
			};
			table.addColumn(einTime,"In Time");
		}
		
		void addColumnEmployeeeditoutTime()
		{
			final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy hh:mm:ss");
			DatePickerCell date= new DatePickerCell(fmt);
			eoutTime=new Column<EmployeeAttendance,Date>(date) {

				@Override
				public Date getValue(EmployeeAttendance object) {
					// TODO Auto-generated method stub
					return fmt.parse(fmt.format(object.getOutTime()));
				}
			};
			table.addColumn(eoutTime,"Out Time");
		}
		
		void addColumnEmployeeoutTime()
		{
			try {
				final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy hh:mm:ss");
				DatePickerCell date= new DatePickerCell(fmt);
				outTime=new TextColumn<EmployeeAttendance>() {

					@Override
					public String getValue(EmployeeAttendance object) {
						// TODO Auto-generated method stub
						if(object.getOutTime()!=null){
							 return fmt.format(object.getOutTime())+"";
						}else{
							return "";
						}
						
					}
				};
				table.addColumn(outTime,"Out Time");
				table.setColumnWidth(outTime,150, Unit.PX);
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
			
		}
	  
		
	
		
	

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<EmployeeAttendance>() {
			@Override
			public Object getKey(EmployeeAttendance item) {
				if(item==null)
					return null;
				else
					return item.getCount();
			}
		};
	
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
		{
			addColumnId();
			addColumnEmployeeName();
			addColumnPunch();
			addColumnDate();
			addColumnviewStatus();
			addColumnViewOtType();
			addColumnViewLeavType();
			addColumnViewProjectName();
			addColumnEmployeeinTime();
			addColumnEmployeeoutTime();
			addColumnViewEmployeerole();
			addColumnViewEmployeeType();
			addColumnViewEmployeeDepartment();
			addColumnViewEmployeeDesignation();
			/*addColumnEmployeeeditinTime();
			addColumnEmployeeeditoutTime();*/
			
			System.out.println("FFFFFFFFFFFFF");
			 
			
			
		   
		}
		if(state==false)
		{
			addColumnId();
			addColumnEmployeeName();
			addColumnPunch();
			addColumnDate();
			addColumnviewStatus();
			addColumnViewOtType();
			addColumnViewLeavType();
			addColumnViewProjectName();
			addColumnEmployeeinTime();
			addColumnEmployeeoutTime();
			addColumnViewEmployeerole();
			addColumnViewEmployeeType();
			addColumnViewEmployeeDepartment();
			addColumnViewEmployeeDesignation();
		}
		
		addColumnSorting();
		table.setWidth("auto");
	}
	
	

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	
	protected void addSortingStatus()
	{
		List<EmployeeAttendance> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeAttendance>(list);
		columnSort.setComparator(status, new Comparator<EmployeeAttendance>()
				{
			@Override
			public int compare(EmployeeAttendance e1,EmployeeAttendance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggeName()
	{
		List<EmployeeAttendance> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeAttendance>(list);
		columnSort.setComparator(employeeName, new Comparator<EmployeeAttendance>()
				{
			@Override
			public int compare(EmployeeAttendance e1,EmployeeAttendance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmpName()!=null && e2.getEmpName()!=null){
						return e1.getEmpName().compareTo(e2.getEmpName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	
	
	
	
	protected void addSortingpunch()
	{
		List<EmployeeAttendance> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeAttendance>(list);
		columnSort.setComparator(punchCardNo, new Comparator<EmployeeAttendance>()
				{
			@Override
			public int compare(EmployeeAttendance e1,EmployeeAttendance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPunchCardNo()!=null && e2.getPunchCardNo()!=null){
						return e1.getPunchCardNo().compareTo(e2.getPunchCardNo());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingDate()
	{
		List<EmployeeAttendance> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeAttendance>(list);
		columnSort.setComparator(date, new Comparator<EmployeeAttendance>()
				{
			@Override
			public int compare(EmployeeAttendance e1,EmployeeAttendance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDate()!=null && e2.getDate()!=null){
						return e1.getDate().compareTo(e2.getDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingId()
	{
		List<EmployeeAttendance> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeAttendance>(list);
		columnSort.setComparator(Id, new Comparator<EmployeeAttendance>()
				{
			@Override
			public int compare(EmployeeAttendance e1,EmployeeAttendance e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getEmpId()== e2.getEmpId()){
						return 0;}
					if(e1.getEmpId()> e2.getEmpId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingInTime()
	{
		List<EmployeeAttendance> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeAttendance>(list);
		columnSort.setComparator(inTime, new Comparator<EmployeeAttendance>()
				{
			@Override
			public int compare(EmployeeAttendance e1,EmployeeAttendance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInTime()!=null && e2.getInTime()!=null){
						return e1.getInTime().compareTo(e2.getInTime());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortingOutTime()
	{
		List<EmployeeAttendance> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeAttendance>(list);
		columnSort.setComparator(outTime, new Comparator<EmployeeAttendance>()
				{
			@Override
			public int compare(EmployeeAttendance e1,EmployeeAttendance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getOutTime()!=null && e2.getOutTime()!=null){
						return e1.getOutTime().compareTo(e2.getOutTime());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingEOutTime()
	{
		List<EmployeeAttendance> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeAttendance>(list);
		columnSort.setComparator(eoutTime, new Comparator<EmployeeAttendance>()
				{
			@Override
			public int compare(EmployeeAttendance e1,EmployeeAttendance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getOutTime()!=null && e2.getOutTime()!=null){
						return e1.getOutTime().compareTo(e2.getOutTime());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortingEinTime()
	{
		List<EmployeeAttendance> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeAttendance>(list);
		columnSort.setComparator(einTime, new Comparator<EmployeeAttendance>()
				{
			@Override
			public int compare(EmployeeAttendance e1,EmployeeAttendance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInTime()!=null && e2.getInTime()!=null){
						return e1.getInTime().compareTo(e2.getInTime());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	public void addColumnSorting(){
		if(Id!=null)
		   addSortingId();
		if(status!=null)
		   addSortingStatus();
		if(punchCardNo!=null)
		  addSortingpunch();
		if(this.employeeName!=null)
		   addSortinggeName();
		if(this.date!=null)
		   addSortingDate();
		if(this.inTime!=null)
		    addSortingInTime();
		if(this.outTime!=null)
		    addSortingOutTime();
		if(this.einTime!=null)
		   addSortingEinTime();
		if(this.eoutTime!=null)
		     addSortingEOutTime();
		}
	
	
	/*public void initializeSelectionCellStatus()
	{
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		
		filter=new Filter();
		filter.setQuerryString("type");
		filter.setIntValue(ConfigTypes.getConfigType(Screen.EMPLOYEEATTENDENCE));
		querry.getFilters().add(filter);
		
		
		final GWTCGlassPanel panel=new GWTCGlassPanel();
		panel.show();
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				panel.hide();
				caught.printStackTrace();
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if(result.size()!=0)
				{
					ArrayList<String>lis=new ArrayList<String>();
					lis.add("--Select--");
					for(SuperModel model:result)
					{
						lis.add(model.toString());
						
					}
					statusCell=new SelectionCell(lis);
					
					addColumnEmployeeStatus();
					
				}
				panel.hide();
				
			}
		});
		
	}*/

}
