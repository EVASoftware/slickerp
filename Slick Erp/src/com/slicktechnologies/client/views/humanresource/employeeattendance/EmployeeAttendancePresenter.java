package com.slicktechnologies.client.views.humanresource.employeeattendance;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.search.*;

import java.util.*;

import com.simplesoftwares.client.library.appstructure.tablescreen.*;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.shared.common.humanresourcelayer.attendance.EmployeeAttendance;

/**
 * TableScreen presenter template.
 */
public class EmployeeAttendancePresenter extends TableScreenPresenter<EmployeeAttendance> {
	TableScreen<EmployeeAttendance>form;
	public static EmployeeAttendanceTable fintable;
	
	
	public EmployeeAttendancePresenter(TableScreen<EmployeeAttendance> view,
			EmployeeAttendance model) {
		super(view, model);
		form=view;
		view.retriveTable(getServiceQuery());
		
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		EmployeeAttendance attt=new EmployeeAttendance();
		attt.setEmpName("AJAY");
		//form.getSuperTable().getListDataProvider().getList().add(attt);
		
		
	}
	
	public EmployeeAttendancePresenter(TableScreen<EmployeeAttendance> view,
			EmployeeAttendance model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals(""))
  reactTo();

		
	}

	public void reactOnPrint()
	{
		
		
	
    }
	
	
	
	
	
	

	@Override
	public void reactOnDownload() {
		CsvServiceAsync async=GWT.create(CsvService.class);
		ArrayList<EmployeeAttendance> custarray=new ArrayList<EmployeeAttendance>();
		List<EmployeeAttendance> list=form.getSuperTable().getListDataProvider().getList();
		
		custarray.addAll(list); 
		
		
		
	}

	@Override
	public void reactOnEmail() {
		
		
	}

	/*
	 * Method template to make a new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new EmployeeAttendance();
	}
	
	public static void initalize()
	{
		 fintable=new EmployeeAttendanceTable();
		EmployeeAttendanceForm form=new EmployeeAttendanceForm(fintable);
	
		
		EmployeeAttendanceSearchPopUp.staticSuperTable=fintable;
		
		EmployeeAttendanceSearchPopUp searchPopUp=new EmployeeAttendanceSearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		EmployeeAttendancePresenter presenter=new EmployeeAttendancePresenter(form, new EmployeeAttendance());
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	
	
	/*
	 * Method template to set the query object in specific to search criteria and return the same
	 * @return MyQuerry object
	 */
	public MyQuerry getServiceQuery()
	{
		MyQuerry quer=new MyQuerry();
		
		DateTimeFormat format= DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		String stringDate=format.format(new Date());
		Date currDate=format.parse(stringDate);
		Filter filter=new Filter();
		filter.setQuerryString("date");
		filter.setDateValue(currDate);
		//quer.getFilters().add(filter);
		quer.setQuerryObject(new EmployeeAttendance());
		return quer;
	}
	
	  private void reactTo()
  {
}
	 
}
