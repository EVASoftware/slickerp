package com.slicktechnologies.client.views.humanresource.uploadattendance;

import java.util.Date;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.attendance.EmpAttendanceDetails;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;

public class EmployeeAttendanceDetailsTable extends SuperTable<EmpAttendanceDetails> {
	TextColumn<EmpAttendanceDetails> getEmpIdCol;
	TextColumn<EmpAttendanceDetails> getEmpNameCol;
	TextColumn<EmpAttendanceDetails> getEmpDesgCol;
	
	TextColumn<EmpAttendanceDetails> getMonthCol;
	TextColumn<EmpAttendanceDetails> getProjectNameCol;
	TextColumn<EmpAttendanceDetails> getshiftNameCol;
	
	TextColumn<EmpAttendanceDetails> getTotalNoOfDaysCol;
	TextColumn<EmpAttendanceDetails> getTotalNoOfPresentDaysCol;
	TextColumn<EmpAttendanceDetails> getTotalNoOfHalfDayCol;
	
	TextColumn<EmpAttendanceDetails> getTotalNoOfWeeklyOffDaysCol;
	TextColumn<EmpAttendanceDetails> getTotalNoOfHolidaysCol;
	
	TextColumn<EmpAttendanceDetails> getTotalNoOfPaidDaysCol;
	TextColumn<EmpAttendanceDetails> getTotalNoOfUnpaidDaysCol;
	
	TextColumn<EmpAttendanceDetails> getTotalNoOfSingleOtCol;
	TextColumn<EmpAttendanceDetails> getTotalNoOfDoubleOtCol;
	
	TextColumn<EmpAttendanceDetails> getTotalNoOfWoOtCol;
	TextColumn<EmpAttendanceDetails> getTotalNoOfPhOtCol;
	TextColumn<EmpAttendanceDetails> getTotalNoOfNhOtCol;
	
	
	Column<EmpAttendanceDetails,String> getEditableTotalNoOfPresentDaysCol;
	Column<EmpAttendanceDetails,String> getEditableTotalNoOfHalfDayCol;
	
	Column<EmpAttendanceDetails,String> getEditableTotalNoOfWeeklyOffDaysCol;
	Column<EmpAttendanceDetails,String> getEditableTotalNoOfHolidaysCol;
	
	Column<EmpAttendanceDetails,String> getEditableLeaveShortNameCol;
	Column<EmpAttendanceDetails,String> getEditableTotalNoOfPaidDaysCol;
	Column<EmpAttendanceDetails,String> getEditableTotalNoOfUnpaidDaysCol;
	
	Column<EmpAttendanceDetails,String> getEditableTotalNoOfSingleOtCol;
	Column<EmpAttendanceDetails,String> getEditableTotalNoOfDoubleOtCol;
	
	Column<EmpAttendanceDetails,String> getEditableTotalNoOfWoOtCol;
	Column<EmpAttendanceDetails,String> getEditableTotalNoOfPhOtCol;
	Column<EmpAttendanceDetails,String> getEditableTotalNoOfNhOtCol;
	
	
	Column<EmpAttendanceDetails,String> getDeleteCol;
	
	GWTCAlert alert=new GWTCAlert();
	
//	CreateAttendanceForm utilityForm=new CreateAttendanceForm(null);
	
	@Override
	public void createTable() {
		getMonthCol();
		getProjectNameCol();
		getshiftNameCol();
		
		
		getEmpIdCol();
		getEmpDesgCol();
		getEmpNameCol();
		
		
		getTotalNoOfDaysCol();
		
//		getEditableTotalNoOfPresentDaysCol();
		getTotalNoOfPresentDaysCol();
		getEditableTotalNoOfHalfDayCol();
		
		getEditableTotalNoOfWeeklyOffDaysCol();
//		getEditableTotalNoOfHolidaysCol();
		getTotalNoOfHolidaysCol();
		
		getEditableLeaveShortNameCol();
		getEditableTotalNoOfPaidDaysCol();
		getEditableTotalNoOfUnpaidDaysCol();
		
		getEditableTotalNoOfSingleOtCol();
		getEditableTotalNoOfDoubleOtCol();
		
		getEditableTotalNoOfWoOtCol();
		getEditableTotalNoOfPhOtCol();
		getEditableTotalNoOfNhOtCol();
		
		getDeleteCol();
		
	}

	private void getEditableLeaveShortNameCol() {
		// TODO Auto-generated method stub
		EditTextCell cell=new EditTextCell();
		getEditableLeaveShortNameCol=new Column<EmpAttendanceDetails,String>(cell) {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getLeaveShortName()+"";
			}
//			@Override
//			public void onBrowserEvent(Context context, Element elem,EmpAttendanceDetails object, NativeEvent event) {
//				if(object.getTotalNoOfPaidLeave()!=0){
//					super.onBrowserEvent(context, elem, object, event);
//				}
//			}
		};
		table.addColumn(getEditableLeaveShortNameCol, "#Leave Short Name");
		getEditableLeaveShortNameCol.setSortable(true);
		table.setColumnWidth(getEditableLeaveShortNameCol,70,Unit.PX);
		
		getEditableLeaveShortNameCol.setFieldUpdater(new FieldUpdater<EmpAttendanceDetails, String>() {
			@Override
			public void update(int index, EmpAttendanceDetails object, String value) {
				// TODO Auto-generated method stub
				
				if(value!=null&&!value.equals("")){
					if(object.getEmpLeaveBalance()!=null){
						boolean validLeaveFlag=false;
						for(AllocatedLeaves leaves:object.getEmpLeaveBalance().getAllocatedLeaves()){
							if(leaves.getShortName().equals(value)&&leaves.getPaidLeave()==true){
								validLeaveFlag=true;
							}
						}
						if(validLeaveFlag){
							object.setLeaveShortName(value);
						}else{
							object.setLeaveShortName("");
							alert.alert("No such leave is assigned!");
							refreshTable();
						}
						table.redrawRow(index);
					}
				}
				
			}
		});
	}

	private void getDeleteCol() {
		ButtonCell cell=new ButtonCell();
		getDeleteCol=new Column<EmpAttendanceDetails,String>(cell) {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return "Delete";
			}
		};
		table.addColumn(getDeleteCol, "");
		getDeleteCol.setSortable(true);
		table.setColumnWidth(getDeleteCol,70,Unit.PX);
		
		getDeleteCol.setFieldUpdater(new FieldUpdater<EmpAttendanceDetails, String>() {
			@Override
			public void update(int index, EmpAttendanceDetails object, String value) {
				getDataprovider().getList().remove(index);
				table.redraw();
			}
		});
	}

	private void getEmpIdCol() {
		getEmpIdCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getEmpId()+"";
			}
		};
		table.addColumn(getEmpIdCol, "Emp Id");
		getEmpIdCol.setSortable(true);
		table.setColumnWidth(getEmpIdCol,90,Unit.PX);
	}

	private void getEmpNameCol() {
		getEmpNameCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getEmpName();
			}
		};
		table.addColumn(getEmpNameCol, "Emp Name");
		getEmpNameCol.setSortable(true);
		table.setColumnWidth(getEmpNameCol,100,Unit.PX);
		
	}

	private void getEmpDesgCol() {
		getEmpDesgCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getEmpDesignation();
			}
		};
		table.addColumn(getEmpDesgCol, "Emp Desg");
		getEmpDesgCol.setSortable(true);
		table.setColumnWidth(getEmpDesgCol,100,Unit.PX);
	}

	private void getMonthCol() {
		getMonthCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getMonth();
			}
		};
		table.addColumn(getMonthCol, "Month");
		getMonthCol.setSortable(true);
		table.setColumnWidth(getMonthCol,100,Unit.PX);
	}

	private void getProjectNameCol() {
		getProjectNameCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getProjectName();
			}
		};
		table.addColumn(getProjectNameCol, "Project Name");
		getProjectNameCol.setSortable(true);
		table.setColumnWidth(getProjectNameCol,100,Unit.PX);
	}

	private void getshiftNameCol() {
		getshiftNameCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getShiftName();
			}
		};
		table.addColumn(getshiftNameCol, "Shift");
		getshiftNameCol.setSortable(true);
		table.setColumnWidth(getshiftNameCol,100,Unit.PX);
	}

	private void getTotalNoOfDaysCol() {
		getTotalNoOfDaysCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfDays()+"";
			}
		};
		table.addColumn(getTotalNoOfDaysCol, "Total No. Of Days");
		getTotalNoOfDaysCol.setSortable(true);
		table.setColumnWidth(getTotalNoOfDaysCol,70,Unit.PX);
		
	}

	private void getTotalNoOfPresentDaysCol() {
		getTotalNoOfPresentDaysCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfPresentDays()+"";
			}
		};
		table.addColumn(getTotalNoOfPresentDaysCol, "Total No. Of Present Days");
		getTotalNoOfPresentDaysCol.setSortable(true);
		table.setColumnWidth(getTotalNoOfPresentDaysCol,70,Unit.PX);
	}

	private void getTotalNoOfHalfDayCol() {
		getTotalNoOfHalfDayCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfHalfDays()+"";
			}
		};
		table.addColumn(getTotalNoOfHalfDayCol, "Total No. Of Half Days");
		getTotalNoOfHalfDayCol.setSortable(true);
		
	}

	private void getTotalNoOfWeeklyOffDaysCol() {
		getTotalNoOfWeeklyOffDaysCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfWeeklyOff()+"";
			}
		};
		table.addColumn(getTotalNoOfWeeklyOffDaysCol, "Total No. Of Weekly Off");
		getTotalNoOfWeeklyOffDaysCol.setSortable(true);
		
	}

	private void getTotalNoOfHolidaysCol() {
		getTotalNoOfHolidaysCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfHolidays()+"";
			}
		};
		table.addColumn(getTotalNoOfHolidaysCol, "Total No. Of Holidays");
		getTotalNoOfHolidaysCol.setSortable(true);
		table.setColumnWidth(getTotalNoOfHolidaysCol,70,Unit.PX);
	}

	private void getTotalNoOfPaidDaysCol() {
		getTotalNoOfPaidDaysCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfPaidLeave()+"";
			}
		};
		table.addColumn(getTotalNoOfPaidDaysCol, "Total No. Of Paid Days");
		getTotalNoOfPaidDaysCol.setSortable(true);
		table.setColumnWidth(getTotalNoOfPaidDaysCol,70,Unit.PX);
	}

	private void getTotalNoOfUnpaidDaysCol() {
		getTotalNoOfUnpaidDaysCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfUnpdaidLeave()+"";
			}
		};
		table.addColumn(getTotalNoOfUnpaidDaysCol, "Total No. Of Unpaid Days");
		getTotalNoOfUnpaidDaysCol.setSortable(true);
		table.setColumnWidth(getTotalNoOfUnpaidDaysCol,70,Unit.PX);
	}

	private void getTotalNoOfSingleOtCol() {
		getTotalNoOfSingleOtCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfSingleOt()+"";
			}
		};
		table.addColumn(getTotalNoOfSingleOtCol, "Total No. Of Single OT");
		getTotalNoOfSingleOtCol.setSortable(true);
		table.setColumnWidth(getTotalNoOfSingleOtCol,70,Unit.PX);
		
	}

	private void getTotalNoOfDoubleOtCol() {
		getTotalNoOfDoubleOtCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfDoubleOt()+"";
			}
		};
		table.addColumn(getTotalNoOfDoubleOtCol, "Total No. Of Double OT");
		getTotalNoOfDoubleOtCol.setSortable(true);
		table.setColumnWidth(getTotalNoOfDoubleOtCol,70,Unit.PX);
		
	}

	private void getTotalNoOfWoOtCol() {
		getTotalNoOfWoOtCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfWeeklyoffOt()+"";
			}
		};
		table.addColumn(getTotalNoOfWoOtCol, "Total No. Of WO OT");
		getTotalNoOfWoOtCol.setSortable(true);
		table.setColumnWidth(getTotalNoOfWoOtCol,70,Unit.PX);
	}

	private void getTotalNoOfPhOtCol() {
		getTotalNoOfPhOtCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfPhOt()+"";
			}
		};
		table.addColumn(getTotalNoOfPhOtCol, "Total No. Of NH OT");
		getTotalNoOfPhOtCol.setSortable(true);
		table.setColumnWidth(getTotalNoOfPhOtCol,70,Unit.PX);
		
	}

	private void getTotalNoOfNhOtCol() {
		getTotalNoOfNhOtCol=new TextColumn<EmpAttendanceDetails>() {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfNhOt()+"";
			}
		};
		table.addColumn(getTotalNoOfNhOtCol, "Total No. Of NH OT");
		getTotalNoOfNhOtCol.setSortable(true);
		table.setColumnWidth(getTotalNoOfNhOtCol,70,Unit.PX);
		
	}

	private void getEditableTotalNoOfPresentDaysCol() {
		EditTextCell cell=new EditTextCell();
		
		getEditableTotalNoOfPresentDaysCol=new Column<EmpAttendanceDetails,String>(cell) {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfPresentDays()+"";
			}
		};
		table.addColumn(getEditableTotalNoOfPresentDaysCol, "#Total No. Of Present Days");
		getEditableTotalNoOfPresentDaysCol.setSortable(true);
		table.setColumnWidth(getEditableTotalNoOfPresentDaysCol,70,Unit.PX);
		
		
		getEditableTotalNoOfPresentDaysCol.setFieldUpdater(new FieldUpdater<EmpAttendanceDetails, String>() {
			@Override
			public void update(int index, EmpAttendanceDetails object, String value) {
				if(value!=null&&!value.equals("")){
					int totalNoOfPresentDays=0;
					try{
						totalNoOfPresentDays=Integer.parseInt(value);
					}catch(NumberFormatException e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Invalid Number Format.");
					}
					object.setTotalNoOfPresentDays(totalNoOfPresentDays);
					table.redrawRow(index);
				}
			}
		});
		
	}

	private void getEditableTotalNoOfHalfDayCol() {
		EditTextCell cell=new EditTextCell();
		getEditableTotalNoOfHalfDayCol=new Column<EmpAttendanceDetails,String>(cell) {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfHalfDays()+"";
			}
		};
		table.addColumn(getEditableTotalNoOfHalfDayCol, "#Total No. Of Half Days");
		getEditableTotalNoOfHalfDayCol.setSortable(true);
		table.setColumnWidth(getEditableTotalNoOfHalfDayCol,70,Unit.PX);
		
		getEditableTotalNoOfHalfDayCol.setFieldUpdater(new FieldUpdater<EmpAttendanceDetails, String>() {
			@Override
			public void update(int index, EmpAttendanceDetails object, String value) {
				if(value!=null&&!value.equals("")){
					int totalNoOfHalfDays=0;
					try{
						totalNoOfHalfDays=Integer.parseInt(value);
					}catch(NumberFormatException e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Invalid Number Format.");
					}
					object.setTotalNoOfHalfDays(totalNoOfHalfDays);
					object.setTotalNoOfPresentDays(calculateTotalPresentDays(object));
					table.redrawRow(index);
				}
			}
		});
	}

	private void getEditableTotalNoOfWeeklyOffDaysCol() {
		EditTextCell cell=new EditTextCell();
		getEditableTotalNoOfWeeklyOffDaysCol=new Column<EmpAttendanceDetails,String>(cell) {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfWeeklyOff()+"";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,EmpAttendanceDetails object, NativeEvent event) {
				if(object.getEmpInfo()!=null){
					if(!isWeeklyOffAssigned(object.getEmpInfo())){
						super.onBrowserEvent(context, elem, object, event);
					}
				}
			}
		};
		table.addColumn(getEditableTotalNoOfWeeklyOffDaysCol, "#Total No. Of WO");
		getEditableTotalNoOfWeeklyOffDaysCol.setSortable(true);
		table.setColumnWidth(getEditableTotalNoOfWeeklyOffDaysCol,70,Unit.PX);
		
		
		
		getEditableTotalNoOfWeeklyOffDaysCol.setFieldUpdater(new FieldUpdater<EmpAttendanceDetails, String>() {
			@Override
			public void update(int index, EmpAttendanceDetails object, String value) {
				if(value!=null&&!value.equals("")){
					int totalNoOfWoDays=0;
					try{
						totalNoOfWoDays=Integer.parseInt(value);
					}catch(NumberFormatException e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Invalid Number Format.");
					}
					object.setTotalNoOfWeeklyOff(totalNoOfWoDays);
					object.setTotalNoOfPresentDays(calculateTotalPresentDays(object));
					table.redrawRow(index);
					
				}
			}
		});
	}

	private void getEditableTotalNoOfHolidaysCol() {
		EditTextCell cell=new EditTextCell();
		getEditableTotalNoOfHolidaysCol=new Column<EmpAttendanceDetails,String>(cell) {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfHolidays()+"";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,EmpAttendanceDetails object, NativeEvent event) {
				Date sDate=getStartDateOfTheMonth(object.getMonth());
				Date eDate=getEndDateOfTheMonth(object.getMonth());
				if(isHolidayBetweenDates(object.getEmpInfo(), sDate, eDate,null)){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(getEditableTotalNoOfHolidaysCol, "#Total No. Of Holiday");
		getEditableTotalNoOfHolidaysCol.setSortable(true);
		table.setColumnWidth(getEditableTotalNoOfHolidaysCol,70,Unit.PX);
		
		getEditableTotalNoOfHolidaysCol.setFieldUpdater(new FieldUpdater<EmpAttendanceDetails, String>() {
			@Override
			public void update(int index, EmpAttendanceDetails object, String value) {
				if(value!=null&&!value.equals("")){
					int totalNoOfHoliday=0;
					try{
						totalNoOfHoliday=Integer.parseInt(value);
					}catch(NumberFormatException e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Invalid Number Format.");
					}
					object.setTotalNoOfHolidays(totalNoOfHoliday);
					object.setTotalNoOfPresentDays(calculateTotalPresentDays(object));
					table.redrawRow(index);
					
				}
			}
		});
		
	}

	private void getEditableTotalNoOfPaidDaysCol() {
		EditTextCell cell=new EditTextCell();
		getEditableTotalNoOfPaidDaysCol=new Column<EmpAttendanceDetails,String>(cell) {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfPaidLeave()+"";
			}
		};
		table.addColumn(getEditableTotalNoOfPaidDaysCol, "#Total No. Of Paid Leave");
		getEditableTotalNoOfPaidDaysCol.setSortable(true);
		table.setColumnWidth(getEditableTotalNoOfPaidDaysCol,70,Unit.PX);
		
		getEditableTotalNoOfPaidDaysCol.setFieldUpdater(new FieldUpdater<EmpAttendanceDetails, String>() {
			@Override
			public void update(int index, EmpAttendanceDetails object, String value) {
				if(value!=null&&!value.equals("")){
					int totalNoOfPaidLeave=0;
					try{
						totalNoOfPaidLeave=Integer.parseInt(value);
						if(object.getLeaveShortName()!=null&&!object.getLeaveShortName().equals("")){
							String leaveName=getLeaveDetails("PL", object.getLeaveShortName(), object.getEmpLeaveBalance(), totalNoOfPaidLeave);
							double balance=0;
							if(leaveName!=null){
								try{
									balance=Double.parseDouble(leaveName);
									totalNoOfPaidLeave=0;
									alert.alert("No PL is assigned/no sufficient leave balance ! "+balance);
								}catch(Exception e){
									
								}
							}else{
								totalNoOfPaidLeave=0;
								alert.alert("No PL is assigned/no sufficient leave balance !");
							}
						}else{
							totalNoOfPaidLeave=0;
							alert.alert("Please add leave short name");
						}
					}catch(NumberFormatException e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Invalid Number Format.");
					}
					object.setTotalNoOfPaidLeave(totalNoOfPaidLeave);
					object.setNoOfPl(totalNoOfPaidLeave);
					object.setTotalNoOfPresentDays(calculateTotalPresentDays(object));
					table.redrawRow(index);
					refreshTable();
					
				}
			}
		});
	}

	private void getEditableTotalNoOfUnpaidDaysCol() {
		EditTextCell cell=new EditTextCell();
		getEditableTotalNoOfUnpaidDaysCol=new Column<EmpAttendanceDetails,String>(cell) {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfUnpdaidLeave()+"";
			}
		};
		table.addColumn(getEditableTotalNoOfUnpaidDaysCol, "#Total No. Of Unpaid Leave");
		getEditableTotalNoOfUnpaidDaysCol.setSortable(true);
		table.setColumnWidth(getEditableTotalNoOfUnpaidDaysCol,70,Unit.PX);
		
		getEditableTotalNoOfUnpaidDaysCol.setFieldUpdater(new FieldUpdater<EmpAttendanceDetails, String>() {
			@Override
			public void update(int index, EmpAttendanceDetails object, String value) {
				if(value!=null&&!value.equals("")){
					int totalNoOfUnpaidLeave=0;
					try{
						totalNoOfUnpaidLeave=Integer.parseInt(value);
						String leaveName=getLeaveDetails("LWP", object.getLeaveShortName(), object.getEmpLeaveBalance(), totalNoOfUnpaidLeave);
						if(leaveName!=null){
							
						}else{
							totalNoOfUnpaidLeave=0;
							alert.alert("No LWP is assigned !");
						}
					}catch(NumberFormatException e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Invalid Number Format.");
					}
					object.setTotalNoOfUnpdaidLeave(totalNoOfUnpaidLeave);
					object.setTotalNoOfPresentDays(calculateTotalPresentDays(object));
					table.redrawRow(index);
					
				}
			}
		});	
	}

	private void getEditableTotalNoOfSingleOtCol() {
		EditTextCell cell=new EditTextCell();
		getEditableTotalNoOfSingleOtCol=new Column<EmpAttendanceDetails,String>(cell) {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfSingleOt()+"";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,EmpAttendanceDetails object, NativeEvent event) {
				String sOt=getOtDetails(object.getProject(), "sOt", object.getEmpId());
				if(sOt!=null){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(getEditableTotalNoOfSingleOtCol, "#Total No. Of Single OT");
		getEditableTotalNoOfSingleOtCol.setSortable(true);
		table.setColumnWidth(getEditableTotalNoOfSingleOtCol,70,Unit.PX);
		
		getEditableTotalNoOfSingleOtCol.setFieldUpdater(new FieldUpdater<EmpAttendanceDetails, String>() {
			@Override
			public void update(int index, EmpAttendanceDetails object, String value) {
				if(value!=null&&!value.equals("")){
					int totalNoOfSingleOT=0;
					try{
						String sOt=getOtDetails(object.getProject(), "sOt", object.getEmpId());
						if(sOt!=null){
							totalNoOfSingleOT=Integer.parseInt(value);
						}else{
							GWTCAlert alert=new GWTCAlert();
							alert.alert("No sinlge ot is assigned!");
						}
					}catch(NumberFormatException e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Invalid Number Format.");
					}
					object.setTotalNoOfSingleOt(totalNoOfSingleOT);
					table.redrawRow(index);
				}
			}
		});
		
	}

	private void getEditableTotalNoOfDoubleOtCol() {
		EditTextCell cell=new EditTextCell();
		getEditableTotalNoOfDoubleOtCol=new Column<EmpAttendanceDetails,String>(cell) {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfDoubleOt()+"";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,EmpAttendanceDetails object, NativeEvent event) {
				String dOt=getOtDetails(object.getProject(), "dOt", object.getEmpId());
				if(dOt!=null){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(getEditableTotalNoOfDoubleOtCol, "#Total No. Of Double OT");
		getEditableTotalNoOfDoubleOtCol.setSortable(true);
		table.setColumnWidth(getEditableTotalNoOfDoubleOtCol,70,Unit.PX);
		
		getEditableTotalNoOfDoubleOtCol.setFieldUpdater(new FieldUpdater<EmpAttendanceDetails, String>() {
			@Override
			public void update(int index, EmpAttendanceDetails object, String value) {
				if(value!=null&&!value.equals("")){
					int totalNoOfDouubleOT=0;
					try{
						String dOt=getOtDetails(object.getProject(), "dOt", object.getEmpId());
						if(dOt!=null){
							totalNoOfDouubleOT=Integer.parseInt(value);
						}else{
							GWTCAlert alert=new GWTCAlert();
							alert.alert("No double ot is assigned!");
						}
					}catch(NumberFormatException e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Invalid Number Format.");
					}
					object.setTotalNoOfDoubleOt(totalNoOfDouubleOT);
					table.redrawRow(index);
				}
			}
		});
		
	}

	private void getEditableTotalNoOfWoOtCol() {
		EditTextCell cell=new EditTextCell();
		getEditableTotalNoOfWoOtCol=new Column<EmpAttendanceDetails,String>(cell) {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfWeeklyoffOt()+"";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,EmpAttendanceDetails object, NativeEvent event) {
				if(object.getEmpInfo()!=null){
					String woOt=getOtDetails(object.getProject(), "woOt", object.getEmpId());
					if(isWeeklyOffAssigned(object.getEmpInfo())&&woOt!=null){
						super.onBrowserEvent(context, elem, object, event);
					}
				}
			}
		};
		table.addColumn(getEditableTotalNoOfWoOtCol, "#Total No. Of WO OT");
		getEditableTotalNoOfWoOtCol.setSortable(true);
		table.setColumnWidth(getEditableTotalNoOfWoOtCol,70,Unit.PX);
		
		getEditableTotalNoOfWoOtCol.setFieldUpdater(new FieldUpdater<EmpAttendanceDetails, String>() {
			@Override
			public void update(int index, EmpAttendanceDetails object, String value) {
				if(value!=null&&!value.equals("")){
					int totalNoOfWoOT=0;
					try{
						String woOt=getOtDetails(object.getProject(), "woOt", object.getEmpId());
						if(woOt!=null){
							totalNoOfWoOT=Integer.parseInt(value);
						}else{
							GWTCAlert alert=new GWTCAlert();
							alert.alert("WO ot is not assigned!");
						}
					}catch(NumberFormatException e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Invalid Number Format.");
					}
					object.setTotalNoOfWeeklyoffOt(totalNoOfWoOT);
					table.redrawRow(index);
				}
			}
		});
		
	}

	private void getEditableTotalNoOfPhOtCol() {
		EditTextCell cell=new EditTextCell();
		getEditableTotalNoOfPhOtCol=new Column<EmpAttendanceDetails,String>(cell) {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfPhOt()+"";
			}
			@Override
			public void onBrowserEvent(Context context, Element elem,EmpAttendanceDetails object, NativeEvent event) {
				Date sDate=getStartDateOfTheMonth(object.getMonth());
				Date eDate=getEndDateOfTheMonth(object.getMonth());
				if(object.getEmpInfo().getDateOfJoining()!=null){
					if(object.getEmpInfo().getDateOfJoining().after(sDate)){
						sDate=CalendarUtil.copyDate(object.getEmpInfo().getDateOfJoining());
					}
				}
				
				if(object.getEmpInfo().getLastWorkingDate()!=null){
					if(object.getEmpInfo().getLastWorkingDate().before(eDate)){
						eDate=CalendarUtil.copyDate(object.getEmpInfo().getLastWorkingDate());
					}
				}
				String phOt=getOtDetails(object.getProject(), "phOt", object.getEmpId());
				if(object.getTotalNoOfHolidays()!=0&&isHolidayBetweenDates(object.getEmpInfo(), sDate, eDate, "Public Holiday")&&phOt!=null){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(getEditableTotalNoOfPhOtCol, "#Total No. Of PH OT");
		getEditableTotalNoOfPhOtCol.setSortable(true);
		table.setColumnWidth(getEditableTotalNoOfPhOtCol,70,Unit.PX);
		
		getEditableTotalNoOfPhOtCol.setFieldUpdater(new FieldUpdater<EmpAttendanceDetails, String>() {
			@Override
			public void update(int index, EmpAttendanceDetails object, String value) {
				if(value!=null&&!value.equals("")){
					int totalNoOfPHOT=0;
					try{
						Date sDate=getStartDateOfTheMonth(object.getMonth());
						Date eDate=getEndDateOfTheMonth(object.getMonth());
						if(object.getEmpInfo().getDateOfJoining()!=null){
							if(object.getEmpInfo().getDateOfJoining().after(sDate)){
								sDate=CalendarUtil.copyDate(object.getEmpInfo().getDateOfJoining());
							}
						}
						
						if(object.getEmpInfo().getLastWorkingDate()!=null){
							if(object.getEmpInfo().getLastWorkingDate().before(eDate)){
								eDate=CalendarUtil.copyDate(object.getEmpInfo().getLastWorkingDate());
							}
						}
						if(isHolidayBetweenDates(object.getEmpInfo(), sDate, eDate, "Public Holiday")){
							String phOt=getOtDetails(object.getProject(), "phOt", object.getEmpId());
							if(phOt!=null){
								totalNoOfPHOT=Integer.parseInt(value);
							}else{
								GWTCAlert alert=new GWTCAlert();
								alert.alert("No public holiday ot is assigned!");
							}
						}else{
							GWTCAlert alert=new GWTCAlert();
							alert.alert("No public holiday in selected month!");
						}
						
					}catch(NumberFormatException e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Invalid Number Format.");
					}
					object.setTotalNoOfPhOt(totalNoOfPHOT);
					table.redrawRow(index);
				}
			}
		});
	}

	private void getEditableTotalNoOfNhOtCol() {
		EditTextCell cell=new EditTextCell();
		getEditableTotalNoOfNhOtCol=new Column<EmpAttendanceDetails,String>(cell) {
			@Override
			public String getValue(EmpAttendanceDetails object) {
				// TODO Auto-generated method stub
				return object.getTotalNoOfNhOt()+"";
			}
			@Override
			public void onBrowserEvent(Context context, Element elem,EmpAttendanceDetails object, NativeEvent event) {
				Date sDate=getStartDateOfTheMonth(object.getMonth());
				Date eDate=getEndDateOfTheMonth(object.getMonth());
				if(object.getEmpInfo().getDateOfJoining()!=null){
					if(object.getEmpInfo().getDateOfJoining().after(sDate)){
						sDate=CalendarUtil.copyDate(object.getEmpInfo().getDateOfJoining());
					}
				}
				
				if(object.getEmpInfo().getLastWorkingDate()!=null){
					if(object.getEmpInfo().getLastWorkingDate().before(eDate)){
						eDate=CalendarUtil.copyDate(object.getEmpInfo().getLastWorkingDate());
					}
				}
				String nhOt=getOtDetails(object.getProject(), "nhOt", object.getEmpId());
				if(object.getTotalNoOfHolidays()!=0&&isHolidayBetweenDates(object.getEmpInfo(), sDate, eDate, "National Holiday")&&nhOt!=null){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(getEditableTotalNoOfNhOtCol, "#Total No. Of NH OT");
		getEditableTotalNoOfNhOtCol.setSortable(true);
		table.setColumnWidth(getEditableTotalNoOfNhOtCol,70,Unit.PX);
		
		getEditableTotalNoOfNhOtCol.setFieldUpdater(new FieldUpdater<EmpAttendanceDetails, String>() {
			@Override
			public void update(int index, EmpAttendanceDetails object, String value) {
				if(value!=null&&!value.equals("")){
					int totalNoOfNHOT=0;
					try{
						Date sDate=getStartDateOfTheMonth(object.getMonth());
						Date eDate=getEndDateOfTheMonth(object.getMonth());
						if(object.getEmpInfo().getDateOfJoining()!=null){
							if(object.getEmpInfo().getDateOfJoining().after(sDate)){
								sDate=CalendarUtil.copyDate(object.getEmpInfo().getDateOfJoining());
							}
						}
						
						if(object.getEmpInfo().getLastWorkingDate()!=null){
							if(object.getEmpInfo().getLastWorkingDate().before(eDate)){
								eDate=CalendarUtil.copyDate(object.getEmpInfo().getLastWorkingDate());
							}
						}
						if(isHolidayBetweenDates(object.getEmpInfo(), sDate, eDate, "National Holiday")){
							String nhOt=getOtDetails(object.getProject(), "nhOt", object.getEmpId());
							if(nhOt!=null){
								totalNoOfNHOT=Integer.parseInt(value);
							}else{
								GWTCAlert alert=new GWTCAlert();
								alert.alert("No national holiday ot is assigned!");
							}
						}else{
							GWTCAlert alert=new GWTCAlert();
							alert.alert("No national holiday in selected month!");
						}
					}catch(NumberFormatException e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Invalid Number Format.");
					}
					object.setTotalNoOfNhOt(totalNoOfNHOT);
					table.redrawRow(index);
				}
			}
		});
		
	}
	
	
	
	
	
	

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		
		if (state == true){
			getEmpIdCol();
			getEmpNameCol();
			getEmpDesgCol();
			
			getMonthCol();
			getProjectNameCol();
			getshiftNameCol();
			
			getTotalNoOfDaysCol();
			
			getEditableTotalNoOfPresentDaysCol();
			getEditableTotalNoOfHalfDayCol();
			
			getEditableTotalNoOfWeeklyOffDaysCol();
//			getEditableTotalNoOfHolidaysCol();
			getTotalNoOfHolidaysCol();
			
			getEditableTotalNoOfPaidDaysCol();
			getEditableTotalNoOfUnpaidDaysCol();
			
			getEditableTotalNoOfSingleOtCol();
			getEditableTotalNoOfDoubleOtCol();
			
			getEditableTotalNoOfWoOtCol();
			getEditableTotalNoOfPhOtCol();
			getEditableTotalNoOfNhOtCol();
			
			getDeleteCol();
		}
		if (state == false){
			getEmpIdCol();
			getEmpNameCol();
			getEmpDesgCol();
			
			getMonthCol();
			getProjectNameCol();
			getshiftNameCol();
			
			getTotalNoOfDaysCol();
			getTotalNoOfPresentDaysCol();
			getTotalNoOfHalfDayCol();
			
			getTotalNoOfWeeklyOffDaysCol();
			getTotalNoOfHolidaysCol();
			
			getTotalNoOfPaidDaysCol();
			getTotalNoOfUnpaidDaysCol();
			
			getTotalNoOfSingleOtCol();
			getTotalNoOfDoubleOtCol();
			
			getTotalNoOfWoOtCol();
			getTotalNoOfPhOtCol();
			getTotalNoOfNhOtCol();
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	public int calculateTotalPresentDays(EmpAttendanceDetails object){
		int presentDays=0;
		presentDays=object.getTotalNoOfDays()-(object.getTotalNoOfWeeklyOff()+object.getTotalNoOfHolidays()+object.getTotalNoOfHalfDays()+object.getTotalNoOfPaidLeave()+object.getTotalNoOfUnpdaidLeave());
		return presentDays;
	}
	
	public boolean isWeeklyOffAssigned(EmployeeInfo empInfo){
		boolean isWo = false;
		Calendar cal=empInfo.getLeaveCalendar();
		if(cal!=null){
			if(cal.isSUNDAY()||cal.isMONDAY()||cal.isTUESDAY()||cal.isWEDNESDAY()||cal.isTHRUSDAY()||cal.isFRIDAY()||cal.isSATAURDAY()){
				isWo=true;
			}
		}
		return isWo;
	}
	
	public Date getStartDateOfTheMonth(String month){
		DateTimeFormat format=DateTimeFormat.getFormat("MMM-yyyy");
		Date startDate=null;
		startDate=format.parse(month);
		CalendarUtil.setToFirstDayOfMonth(startDate);
		Console.log("START DATE : "+startDate);
		return startDate;
	}
	
	public Date getEndDateOfTheMonth(String month){
		DateTimeFormat format=DateTimeFormat.getFormat("MMM-yyyy");
		Date endDate=null;
		endDate=format.parse(month);
		CalendarUtil.setToFirstDayOfMonth(endDate);
		CalendarUtil.addMonthsToDate(endDate, 1);
		CalendarUtil.addDaysToDate(endDate, -1);
		Console.log("END DATE : "+endDate);
		return endDate;
	}
	public boolean isHolidayBetweenDates(EmployeeInfo empInfo,Date sDate,Date eDate,String holidayName){
		boolean holidayStatus=false;
		Date startDate=CalendarUtil.copyDate(sDate);
		Date endDate=CalendarUtil.copyDate(eDate);
		Calendar cal=empInfo.getLeaveCalendar();
		if(cal!=null){
			while(!startDate.equals(endDate)){
				if(cal.isHolidayDate(startDate)){
					for(Holiday holiday:empInfo.getLeaveCalendar().getHoliday()){
						if(holidayName==null){
							return true;
						}
						if(holiday.getHolidaydate().equals(startDate)){
							if(holidayName.equalsIgnoreCase(holiday.getHolidayType())){
								return true;
							}
						}
					}
				}
				CalendarUtil.addDaysToDate(startDate, 1);
			}
		}
		return holidayStatus;
	}

	public String getOtDetails(HrProject project,String otType,int empId){
		String otName=null;
		if(project!=null){
			for(Overtime ot:project.getOtList()){
				if(ot.getEmpId()==empId){
					if(otType.equals("woOt")){
						if(ot.isWeeklyOff()==true){
							return ot.getName();
						}
					}else if(otType.equals("phOt")){
						if(ot.getHolidayType()!=null&&ot.getHolidayType().equalsIgnoreCase("Public Holiday")){
							return ot.getName();
						}
					}else if(otType.equals("nhOt")){
						if(ot.getHolidayType()!=null&&ot.getHolidayType().equalsIgnoreCase("National Holiday")){
							return ot.getName();
						}
					}else if(otType.equals("sOt")){
						if(ot.getShortName().equalsIgnoreCase("OT")){
							return ot.getName();
						}
					}else if(otType.equals("dOt")){
						if(ot.getShortName().equalsIgnoreCase("DOT")){
							return ot.getName();
						}
					}else{
						if(ot.isNormalDays()){
							return ot.getName();
						}
					}
				}
			}
		}
		return otName;
	}
	
	public String getLeaveDetails(String leaveType,String shortName,LeaveBalance empLeaveBalance,double noOfLeave){
		String leaveName=null;
		
		for(AllocatedLeaves leaves:empLeaveBalance.getAllocatedLeaves()){
			if(leaveType.equals("PL")){
				if(leaves.getPaidLeave()==true&&leaves.getShortName().equals(shortName)){
					if (leaves.getBalance() >= noOfLeave) {
						return leaves.getName();
					}else{
						leaveName=leaves.getBalance()+"";
						return leaveName;
					}
				}
			}else if(leaveType.equals("LWP")){
				if(leaves.getPaidLeave()==false){
					return leaves.getName();
				}
			}
		}
		return leaveName;
	}
	
	
	public void refreshTable(){
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--){
			table.removeColumn(i);
		}
		
		
		getMonthCol();
		getProjectNameCol();
		getshiftNameCol();
		
		
		getEmpIdCol();
		getEmpDesgCol();
		getEmpNameCol();
		
		
		getTotalNoOfDaysCol();
		
//		getEditableTotalNoOfPresentDaysCol();
		getTotalNoOfPresentDaysCol();
		getEditableTotalNoOfHalfDayCol();
		
		getEditableTotalNoOfWeeklyOffDaysCol();
//		getEditableTotalNoOfHolidaysCol();
		getTotalNoOfHolidaysCol();
		
		getEditableLeaveShortNameCol();
		getEditableTotalNoOfPaidDaysCol();
		getEditableTotalNoOfUnpaidDaysCol();
		
		getEditableTotalNoOfSingleOtCol();
		getEditableTotalNoOfDoubleOtCol();
		
		getEditableTotalNoOfWoOtCol();
		getEditableTotalNoOfPhOtCol();
		getEditableTotalNoOfNhOtCol();
		
		getDeleteCol();
	}
	
	
}
