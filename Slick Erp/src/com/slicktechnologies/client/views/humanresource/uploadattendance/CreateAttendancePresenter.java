package com.slicktechnologies.client.views.humanresource.uploadattendance;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.attendance.EmpAttendanceDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.attendance.EmployeeAttendance;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;

public class CreateAttendancePresenter extends FormScreenPresenter<EmployeeAttendance> implements SelectionHandler<Suggestion> {
	CreateAttendanceForm form;
	GeneralServiceAsync genservice=GWT.create(GeneralService.class);
	public CreateAttendancePresenter(UiScreen<EmployeeAttendance> view,EmployeeAttendance model) {
		super(view, model);
		// TODO Auto-generated constructor stub
		form=(CreateAttendanceForm) view;
		form.personInfo.getId().addSelectionHandler(this);
		form.personInfo.getName().addSelectionHandler(this);
		form.personInfo.getPhone().addSelectionHandler(this);
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}
	
	public static void initialize(){
		AppMemory.getAppMemory().skeleton.isMenuVisble=false;
		CreateAttendanceForm form=new  CreateAttendanceForm();
		CreateAttendancePresenter  presenter=new CreateAttendancePresenter(form,new EmployeeAttendance());
		AppMemory.getAppMemory().stickPnel(form);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals("Create Attendance")) {
			createAttendance();
		}
	}

	private void createAttendance() {
		if(form.table.getValue().size()!=0){
			final ArrayList<EmpAttendanceDetails> empAttendanceList=new ArrayList<EmpAttendanceDetails>();
			empAttendanceList.addAll(form.table.getValue());
			System.out.println("EMP ATTEN DET LIST : "+empAttendanceList.size());
			
			ArrayList<Integer> projectIdslist = new ArrayList<Integer>();
			for(final EmpAttendanceDetails object:empAttendanceList){
				if(object.getProject()!=null){
				if(object.getProject().getOtList()==null && object.getProject().getOtList().size()==0){
					projectIdslist.add(object.getProject().getCount());
				}
				}else
					Console.log("Project null");
			}
			Console.log("projectIdslist.size()="+projectIdslist.size());
			if(projectIdslist.size()>0){
				Company c=new Company();
				CommonServiceAsync commonservice = GWT.create(CommonService.class);
				commonservice.getHrProjectOvertimelist(projectIdslist, c.getCompanyId(), new AsyncCallback<ArrayList<HrProjectOvertime>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(ArrayList<HrProjectOvertime> result) {
						// TODO Auto-generated method stub
						
						ArrayList<Overtime> otlist = new ArrayList<Overtime>();
						for(HrProjectOvertime ot : result){
							otlist.add(ot);
						}
						createAttendanceWithOTDetails(empAttendanceList,result);

					}
				});
			}
			else{
				createAttendanceWithOTDetails(empAttendanceList,null);
			}
			
			
			
			
			
			
		}else{
			form.showDialogMessage("Please add employee and attendace details!");
		}
	}

	protected void createAttendanceWithOTDetails(ArrayList<EmpAttendanceDetails> empAttendanceList, ArrayList<HrProjectOvertime> hrotlist) {
		final ArrayList<Attendance> attendanceList=new ArrayList<Attendance>();
		
		for(final EmpAttendanceDetails object:empAttendanceList){
			final EmployeeInfo empInfo=form.empInfoHmap.get(object.getEmpId());
			final Date sDate=form.getStartDateOfTheMonth(object.getMonth());
			final Date eDate=form.getEndDateOfTheMonth(object.getMonth());
			System.out.println("sDATE  : "+sDate+" eDATE : "+eDate);
			final boolean isWeek=form.isWeeklyOffAssigned(empInfo);
			
			int woInterval=0;
			int woCounter=0;
			int intervalCounter=0;
			
			if(isWeek==false){
				try{
					woInterval=object.getTotalNoOfDays()/object.getTotalNoOfWeeklyOff();
				}catch(Exception e){
					
				}
			}
			
			while(sDate.equals(eDate)||sDate.before(eDate)){
				System.out.println("Attendance DATE : "+sDate);
				
				
				if(empInfo.getDateOfJoining()!=null){
					if(sDate.before(empInfo.getDateOfJoining())){
						CalendarUtil.addDaysToDate(sDate, 1);
						continue;
					}
				}
				
				if(empInfo.getLastWorkingDate()!=null){
					if(sDate.after(empInfo.getLastWorkingDate())){
						break;
					}
				}
				
				
				 Attendance attendance=null;
				 boolean isPresen=false;
				 boolean isWo=false;
				 boolean isHoliday=false;
				 boolean isPaidLeave=false;
				 boolean isUnpaidLeave=false;
				
				if(isWeek){
					if(form.isDateFallsOnWo(sDate, empInfo)){
						isWo=true;
					}else if(empInfo.getLeaveCalendar().isHolidayDate(sDate)){
						isHoliday=true;
					}else if(object.getTotalNoOfPaidLeave()!=0){
						isPaidLeave=true;
					}else if(object.getTotalNoOfUnpdaidLeave()!=0){
						isUnpaidLeave=true;
					}else{
						isPresen=true;
					}
				}else{
					intervalCounter++;
					if(woInterval==intervalCounter&&woCounter<=object.getTotalNoOfWeeklyOff()){
						if(empInfo.getLeaveCalendar().isHolidayDate(sDate)){
							isHoliday=true;
							intervalCounter--;
						}else{
							isWo=true;
							intervalCounter=0;
							woCounter++;
						}
					}else if(empInfo.getLeaveCalendar().isHolidayDate(sDate)){
						isHoliday=true;
					}else if(object.getTotalNoOfPaidLeave()!=0){
						isPaidLeave=true;
					}else if(object.getTotalNoOfUnpdaidLeave()!=0){
						isUnpaidLeave=true;
					}else{
						isPresen=true;
					}
				}
				
					ArrayList<Overtime> otlist = new ArrayList<Overtime>();
					if(object.getProject().getOtList()==null || object.getProject().getOtList().size()==0){
						otlist = getOtList(object.getProject().getCount(),hrotlist);
					}
					else{
						otlist = object.getProject().getOtList();
					}
					
					attendance=form.getAttendance(empInfo, object.getMonth(), sDate, isPresen, isWo,isHoliday,object,isPaidLeave,isUnpaidLeave,otlist);
					attendanceList.add(attendance);
					CalendarUtil.addDaysToDate(sDate, 1);
				
			}
		}
		System.out.println("Attendance List size : "+attendanceList.size());
		if(attendanceList.size()!=0){
			form.showWaitSymbol();
			genservice.createAttendance(attendanceList, new  AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage(result);
				}
			});
		}
		
		
	}

	private ArrayList<Overtime> getOtList(int count,ArrayList<HrProjectOvertime> hrotlist) {
		ArrayList<Overtime> otlist = new ArrayList<Overtime>();
		for(HrProjectOvertime hrot : hrotlist){
			if(hrot.getProjectId() == count){
				otlist.add(hrot);
			}
		}
		
		return otlist;
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		// TODO Auto-generated method stub
		if(event.getSource()==form.personInfo.getId()||event.getSource()==form.personInfo.getName()||event.getSource()==form.personInfo.getName()){
			EmployeeInfo info=form.personInfo.getEmpentity();
			if(info!=null){
				form.project.setValue(info.getProjectName());
			}
		}
	}
	
}
