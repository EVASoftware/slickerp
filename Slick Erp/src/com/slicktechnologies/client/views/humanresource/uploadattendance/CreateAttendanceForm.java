package com.slicktechnologies.client.views.humanresource.uploadattendance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.attendance.EmpAttendanceDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.attendance.EmployeeAttendance;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class CreateAttendanceForm extends FormScreen<EmployeeAttendance> implements ClickHandler {

	Button btnMasterGo;
	EmployeeInfoComposite personInfo;
	ObjectListBox<HrProject> project;
	
	DateBox attendancePeriod;
	
	EmployeeAttendanceDetailsTable table;
	
	int totalNoOfDaysInMonth=0;
	int totalNoOfWO=0;
	int totalNoOfHoliday=0;
	
	
	HashMap<Integer,EmployeeInfo> empInfoHmap=new HashMap<Integer,EmployeeInfo>();
	HashMap<Integer,LeaveBalance> empLeaveBalHmap=new HashMap<Integer,LeaveBalance>();
	
	GenricServiceAsync genasync=GWT.create(GenricService.class);
	public CreateAttendanceForm(){
		super(FormStyle.DEFAULT);
		createGui();
		setFormat();
		processLevelBar.btnLabels[0].setVisible(true);
	}
	
	public CreateAttendanceForm(String temp){
		
	}
	
	public CreateAttendanceForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle){
		super(processlevel, fields, formstyle);
		createGui();
		setFormat();
		processLevelBar.btnLabels[0].setVisible(true);
	}
	
	private void initalizeWidget(){
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
//		personInfo.getElement().getStyle().setWidth(120, Unit.PCT);
		
		btnMasterGo = new Button("Go");
		btnMasterGo.addClickHandler(this);
		
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
		
		attendancePeriod=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("MMM-yyyy");
		attendancePeriod.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		table=new EmployeeAttendanceDetailsTable();
		table.getTable().getElement().getStyle().setHeight(300, Unit.PX);
	}
	
	public void setFormat(){
		DateTimeFormat format=DateTimeFormat.getFormat("MMM-yyyy");
		DateBox.DefaultFormat defformat=new DateBox.DefaultFormat(format);
		attendancePeriod.setFormat(defformat);
	}
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CREATEATTENDANCE,LoginPresenter.currentModule.trim());
	}
	@Override
	public void createScreen(){
	
		initalizeWidget();

		//Token to initialize the processlevel menus.
		this.processlevelBarNames=new String[]{"Create Attendance"};

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		
		fbuilder=new FormFieldBuilder("", personInfo);
		FormField fpersonInfo=fbuilder.setRowSpan(0).setColSpan(3).build();
		
		fbuilder=new FormFieldBuilder("", btnMasterGo);
		FormField freaderCompositeGo=fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project", project);
		FormField fproject = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Month", attendancePeriod);
		FormField fattendancePeriod = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", table.getTable());
		FormField ftable = fbuilder.setRowSpan(0).setColSpan(3).build();
		
		
		FormField[][] formfield = { 
				{fpersonInfo},
				{fattendancePeriod,fproject,freaderCompositeGo},
				{ftable}
		};

		this.fields=formfield;
	}
	@Override
	public void updateModel(EmployeeAttendance model) 
	{
		
		
	}
	@Override
	public void updateView(EmployeeAttendance model)
	{
		
		
	}
	
	
	
	public String getContributionPeriod(DateBox dbDateBox) {
		DateTimeFormat format = DateTimeFormat.getFormat("MMM-yyyy");
		if (dbDateBox.getValue() == null) {
			return null;
		} else {
			return format.format(dbDateBox.getValue());
		}
	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource().equals(btnMasterGo)){
			addEmployeeAttendanceDetails();
		}
	}
	
	
	
	public void addEmployeeAttendanceDetails(){
		
		if(attendancePeriod.getValue()==null){
			showDialogMessage("Please select month!");
			return;
		}
		if((personInfo==null||personInfo.getEmployeeId()==-1)&&project.getValue()==null){
			showDialogMessage("Please select either employee or project!");
			return;
		}
		
		if(project.getValue()!=null){
			if(personInfo.getValue()!=null){
				final EmployeeInfo empInfo=personInfo.getEmpentity();
				if(empInfo.getLeaveCalendar()==null){
					showDialogMessage("Calendar is not assigned!");
					return;	
				}
				if(empInfo.isStatus()==false){
					showDialogMessage("Employee status is Inactive !");
					return;	
				}
				if(table.getValue().size()!=0){
					for(EmpAttendanceDetails empAttendance:table.getValue()){
						if(empAttendance.getEmpId()==empInfo.getEmpCount()){
							showDialogMessage("Employee already added!");
							return;
						}
					}
				}
				
				Date sDate=getStartDateOfTheMonth(getContributionPeriod(attendancePeriod));
				Date eDate=getEndDateOfTheMonth(getContributionPeriod(attendancePeriod));
				
				if(empInfo.getDateOfJoining()!=null){
					if(empInfo.getDateOfJoining().after(eDate)){
						showDialogMessage("DOJ is after month end !");
						return;
					}
				}
				
				if(empInfo.getLastWorkingDate()!=null){
					if(empInfo.getLastWorkingDate().before(sDate)){
						showDialogMessage("Last day of working befor selcted month !");
						return;
					}
				}
				
				HrProject hrProject = project.getSelectedItem();

				if(hrProject.getOtList()==null&&hrProject.getOtList().size()==0){
					CommonServiceAsync commonservice = GWT.create(CommonService.class);
					commonservice.getHrProjectOvertimelist(hrProject.getCount(), hrProject.getCompanyId(), new AsyncCallback<ArrayList<HrProjectOvertime>>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onSuccess(ArrayList<HrProjectOvertime> result) {
							if(result.size()!=0){
								ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
								for(Overtime ot : result){
									overtimelist.add(ot);
								}
								
								 addovertimeandproced(empInfo,overtimelist);

							}
						}
					});
					
				}
				else{
					 addovertimeandproced(empInfo,hrProject.getOtList());
				}

				
			}else{
				
				HrProject hrProject=project.getSelectedItem();
				
				if(hrProject.getOtList()==null&&hrProject.getOtList().size()==0){
					CommonServiceAsync commonservice = GWT.create(CommonService.class);
					commonservice.getHrProjectOvertimelist(hrProject.getCount(), hrProject.getCompanyId(), new AsyncCallback<ArrayList<HrProjectOvertime>>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onSuccess(ArrayList<HrProjectOvertime> result) {
							if(result.size()!=0){
								ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
								for(Overtime ot : result){
									overtimelist.add(ot);
								}
								loadLeaveBalance(overtimelist);
							}
							else{
								showDialogMessage("No Employee added in project!");
								return;
							}
						}
					});
				}
				else{
					if(hrProject.getOtList()!=null&&hrProject.getOtList().size()!=0){
//						checkValidateAndAddEmpAttendance();
						loadLeaveBalance(hrProject.getOtList());
					}else{
						showDialogMessage("No Employee added in project!");
						return;
					}
				}
				
				
				
			}
		}
		
//		if(personInfo.getValue()!=null){
//			EmployeeInfo empInfo=personInfo.getEmpentity();
//			if(table.getValue().size()!=0){
//				for(EmpAttendanceDetails empAttendance:table.getValue()){
//					if(empAttendance.getEmpId()==empInfo.getEmpCount()){
//						showDialogMessage("Employee already added!");
//						return;
//					}
//				}
//			}
//			if(empInfo.getProjectName()!=null&&!empInfo.getProjectName().equals("")){
//				EmpAttendanceDetails empAttenDet=new EmpAttendanceDetails();
//				empAttenDet.setEmpId(empInfo.getEmpCount());
//				empAttenDet.setEmpName(empInfo.getFullName());
//				empAttenDet.setEmpDesignation(empInfo.getDesignation());
//				empAttenDet.setProjectName(empInfo.getProjectName());
//				empAttenDet.setShiftName(empInfo.getShiftName());
//				empAttenDet.setMonth(getContributionPeriod(attendancePeriod));
//				calculateDays(empInfo, attendancePeriod.getValue());
//				empAttenDet.setTotalNoOfDays(totalNoOfDaysInMonth);
//				empAttenDet.setTotalNoOfWeeklyOff(totalNoOfWO);
//				empAttenDet.setTotalNoOfHolidays(totalNoOfHoliday);
//				empAttenDet.setTotalNoOfPresentDays(totalNoOfDaysInMonth-totalNoOfWO-totalNoOfHoliday);
//				empAttenDet.setLeaveShortName("");
//				empAttenDet.setEmpInfo(empInfo);
//				addEmployeeToMap(empInfo);
//				loadLeaveBalanceEmployeeWise(empInfo.getEmpCount(), empAttenDet);
//			}else{
//				showDialogMessage("Project is not allocated!");
//				return;
//			}
//			
//		}
	}
	
	

	private void checkValidateAndAddEmpAttendance(final ArrayList<LeaveBalance> empLeaveBalanceList, final ArrayList<Overtime> overtimelist) {
		final HrProject hrProject=project.getSelectedItem();
		HashSet<String> empNmHs=new HashSet<String>();
		HashSet<Integer> empIdHs=new HashSet<Integer>();
		for(Overtime ot:overtimelist){
			empNmHs.add(ot.getEmpName());
			empIdHs.add(ot.getEmpId());
		}
		List<String> empNmList=new ArrayList<String>(empNmHs);
		final List<Integer> empIdList=new ArrayList<Integer>(empIdHs);
		Vector<Filter> vecLis=new Vector<Filter>();
		MyQuerry query=new MyQuerry();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("fullName IN");
		temp.setList(empNmList);
		vecLis.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(UserConfiguration.getCompanyId());
		vecLis.add(temp);
		
		query.setFilters(vecLis);
		query.setQuerryObject(new EmployeeInfo());
//		showWaitSymbol();
		genasync.getSearchResult(query, new  AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
				ArrayList<EmployeeInfo> empInfoList=new ArrayList<EmployeeInfo>();
				for(SuperModel model:result){
					EmployeeInfo empInfo=(EmployeeInfo) model;
					empInfoList.add(empInfo);
				}
				System.out.println("EMP INFO LIST SIZE : "+empInfoList.size());
				
				
				if(empInfoList.size()!=0){
					Comparator<EmployeeInfo> empInfoComp=new Comparator<EmployeeInfo>() {
						@Override
						public int compare(EmployeeInfo arg0, EmployeeInfo arg1) {
							return arg0.getFullName().compareTo(arg1.getFullName());
						}
					};
					Collections.sort(empInfoList,empInfoComp);
					for(EmployeeInfo empInfo:empInfoList){
						if(empIdList!=null&&empIdList.size()!=0){
							if(empIdList.contains(empInfo.getCount())==false){
								continue;
							}
						}
						
						if(empInfo.isStatus()==false){
							continue;
						}
						
						LeaveBalance leaveBalance=getEmployeeLeaveBalance(empInfo.getEmpCount(),empLeaveBalanceList);
						if(leaveBalance==null){
							continue;
						}
						
						
						boolean continueFlag=false;
						
						if(table.getValue().size()!=0){
							for(EmpAttendanceDetails empAttendance:table.getValue()){
								if(empAttendance.getEmpId()==empInfo.getEmpCount()){
									continueFlag=true;
									break;
								}
							}
						}
						if(continueFlag){
							continue;
						}
						
						Date sDate=getStartDateOfTheMonth(getContributionPeriod(attendancePeriod));
						Date eDate=getEndDateOfTheMonth(getContributionPeriod(attendancePeriod));
						
						if(empInfo.getDateOfJoining()!=null){
							if(empInfo.getDateOfJoining().after(eDate)){
								continue;
							}
						}
						if(empInfo.getLastWorkingDate()!=null){
							if(empInfo.getLastWorkingDate().before(sDate)){
								continue;
							}
						}
						
						
						if(empInfo.getProjectName()!=null&&!empInfo.getProjectName().equals("")){
							
							if(!empInfo.getProjectName().equals(hrProject.getProjectName())){
								continue;
							}
							EmpAttendanceDetails empAttenDet=new EmpAttendanceDetails();
							empAttenDet.setEmpId(empInfo.getEmpCount());
							empAttenDet.setEmpName(empInfo.getFullName());
							empAttenDet.setEmpDesignation(empInfo.getDesignation());
							empAttenDet.setProjectName(hrProject.getProjectName());
							empAttenDet.setShiftName(getShiftName(hrProject, empInfo.getEmpCount(),overtimelist));
							empAttenDet.setMonth(getContributionPeriod(attendancePeriod));
							calculateDays(empInfo, attendancePeriod.getValue());
							empAttenDet.setTotalNoOfDays(totalNoOfDaysInMonth);
							empAttenDet.setTotalNoOfWeeklyOff(totalNoOfWO);
							empAttenDet.setTotalNoOfHolidays(totalNoOfHoliday);
							empAttenDet.setTotalNoOfPresentDays(totalNoOfDaysInMonth-totalNoOfWO-totalNoOfHoliday);
							empAttenDet.setLeaveShortName("");
							empAttenDet.setEmpInfo(empInfo);
							empAttenDet.setEmpLeaveBalance(leaveBalance);
							empAttenDet.setProject(hrProject);
							addEmployeeToMap(empInfo);
							addEmpLeaveBalToMap(leaveBalance);
							table.getDataprovider().getList().add(empAttenDet);
						}else{
							continue;
						}
					}
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}
		});
		
		
	}

	public void calculateDays(EmployeeInfo empInfo,Date date){
		totalNoOfDaysInMonth=0;
		totalNoOfWO=0;
		totalNoOfHoliday=0;
		
		Date startDate=CalendarUtil.copyDate(date);
		Date endDate=CalendarUtil.copyDate(date);
		
		CalendarUtil.setToFirstDayOfMonth(startDate);
		CalendarUtil.setToFirstDayOfMonth(endDate);
		CalendarUtil.addMonthsToDate(endDate, 1);
		CalendarUtil.addDaysToDate(endDate, -1);
		
		Console.log("START DATE : "+startDate);
		Console.log("END DATE : "+endDate);
		
		if(empInfo.getDateOfJoining()!=null){
			if(empInfo.getDateOfJoining().after(startDate)){
				startDate=CalendarUtil.copyDate(empInfo.getDateOfJoining());
			}
		}
		
		if(empInfo.getLastWorkingDate()!=null){
			if(empInfo.getLastWorkingDate().before(endDate)){
				endDate=CalendarUtil.copyDate(empInfo.getLastWorkingDate());
			}
		}
		Console.log("START DATE after checking doj : "+startDate);
		Console.log("END DATE after checking ldw : "+endDate);
		
		
		totalNoOfDaysInMonth=CalendarUtil.getDaysBetween(startDate, endDate);
		totalNoOfDaysInMonth+=1;
		Console.log("TOTAL NO. OF DAYS IN MONTH : "+totalNoOfDaysInMonth);
		
		
		Calendar cal=empInfo.getLeaveCalendar();
		if(cal!=null){
			boolean isWoDefFlag=false;
			ArrayList<Integer> dayList=new ArrayList<Integer>();
			if(cal.isSUNDAY()){
				dayList.add(0);
				isWoDefFlag=true;
			}
			if(cal.isMONDAY()){
				dayList.add(1);
				isWoDefFlag=true;
			}
			if(cal.isTUESDAY()){
				dayList.add(2);
				isWoDefFlag=true;
			}
			if(cal.isWEDNESDAY()){
				dayList.add(3);
				isWoDefFlag=true;
			}
			if(cal.isTHRUSDAY()){
				dayList.add(4);
				isWoDefFlag=true;
			}
			if(cal.isFRIDAY()){
				dayList.add(5);
				isWoDefFlag=true;
			}
			if(cal.isSATAURDAY()){
				dayList.add(6);
				isWoDefFlag=true;
			}
			
//			System.out.println("DAY LIST : "+dayList);
//			while(!startDate.equals(endDate)){
			while(startDate.equals(endDate)||startDate.before(endDate)){
//				System.out.println("ST DT : "+startDate+" ED DT : "+endDate);
				if(isWoDefFlag){
					int day=startDate.getDay();
					System.out.println("DAY : "+day);
					if(dayList.contains(day)){
						totalNoOfWO++;
					}
				}
				if(cal.isHolidayDate(startDate)){
					totalNoOfHoliday++;
				}
				CalendarUtil.addDaysToDate(startDate, 1);
			}
		}else{
			System.out.println("Calendar NULL ..");
		}
		Console.log("WO : "+totalNoOfWO+" H : "+totalNoOfHoliday);
	}
	
	public void addEmployeeToMap(EmployeeInfo info){
		empInfoHmap.put(info.getEmpCount(), info);
	}
	
	public void addEmpLeaveBalToMap(LeaveBalance leaveBalance){
		empLeaveBalHmap.put(leaveBalance.getEmpCount(), leaveBalance);
	}

	@Override
	public void setToNewState() {
		// TODO Auto-generated method stub
		super.setToNewState();
		processLevelBar.btnLabels[0].setVisible(true);
	}
	
	
	public void loadLeaveBalance(final ArrayList<Overtime> overtimelist){
//		HrProject hrProject=project.getSelectedItem();
		HashSet<String> empNmHs=new HashSet<String>();
		HashSet<Integer> empIdHs=new HashSet<Integer>();
		for(Overtime ot:overtimelist){
			empNmHs.add(ot.getEmpName());
			empIdHs.add(ot.getEmpId());
		}
		List<String> empNmList=new ArrayList<String>(empNmHs);
		final List<Integer> empIdList=new ArrayList<Integer>(empIdHs);
		Vector<Filter> vecLis=new Vector<Filter>();
		MyQuerry query=new MyQuerry();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("empInfo.fullName IN");
		temp.setList(empNmList);
		vecLis.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(UserConfiguration.getCompanyId());
		vecLis.add(temp);
		
		query.setFilters(vecLis);
		query.setQuerryObject(new LeaveBalance());
		showWaitSymbol();
		genasync.getSearchResult(query, new  AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<LeaveBalance> empLeaveBalanceList=new ArrayList<LeaveBalance>();
				if(result!=null&&result.size()!=0){
					for(SuperModel model:result){
						LeaveBalance object=(LeaveBalance) model;
						empLeaveBalanceList.add(object);
					}
					System.out.println("EMP LEAVE BAL LIST : "+empLeaveBalanceList.size());
					checkValidateAndAddEmpAttendance(empLeaveBalanceList,overtimelist);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}
		});
	}
	
	private LeaveBalance getEmployeeLeaveBalance(int empCount,
			ArrayList<LeaveBalance> empLeaveBalanceList) {
		
		for(LeaveBalance leaveBal:empLeaveBalanceList){
			if(leaveBal.getEmpCount()==empCount){
				return leaveBal;
			}
		}
		return null;
	}
	
	
	public void loadLeaveBalanceEmployeeWise(int empId,final EmpAttendanceDetails empAttendanceDetails){
		Vector<Filter> vecLis=new Vector<Filter>();
		MyQuerry query=new MyQuerry();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("empInfo.empCount");
		temp.setIntValue(empId);
		vecLis.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(UserConfiguration.getCompanyId());
		vecLis.add(temp);
		
		query.setFilters(vecLis);
		query.setQuerryObject(new LeaveBalance());
		showWaitSymbol();
		genasync.getSearchResult(query, new  AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				hideWaitSymbol();
				if(result!=null&&result.size()!=0){
					LeaveBalance object=(LeaveBalance) result.get(0);
					System.out.println("EMP LEAVE BAL LIST : "+result.size());
					empAttendanceDetails.setEmpLeaveBalance(object);
					table.getDataprovider().getList().add(empAttendanceDetails);
				}else{
					showDialogMessage("Leaves are not assigned to employee!");
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}
		});
	}
	
	public Date getStartDateOfTheMonth(String month){
		DateTimeFormat format=DateTimeFormat.getFormat("MMM-yyyy");
		Date startDate=null;
		startDate=format.parse(month);
		CalendarUtil.setToFirstDayOfMonth(startDate);
		Console.log("START DATE : "+startDate);
		return startDate;
	}
	
	public Date getEndDateOfTheMonth(String month){
		DateTimeFormat format=DateTimeFormat.getFormat("MMM-yyyy");
		Date endDate=null;
		endDate=format.parse(month);
		CalendarUtil.setToFirstDayOfMonth(endDate);
		CalendarUtil.addMonthsToDate(endDate, 1);
		CalendarUtil.addDaysToDate(endDate, -1);
		Console.log("END DATE : "+endDate);
		return endDate;
	}
	
	public int getTotalNoOfDaysBetweenDates(Date startDate,Date endDate){
		int totalNoOfDays=0;
		totalNoOfDays=CalendarUtil.getDaysBetween(startDate, endDate);
		totalNoOfDays+=1;
		Console.log("TOTAL NO. OF DAYS B/w dated : "+totalNoOfDays);
		return totalNoOfDays;
	}
	
	public boolean isWeeklyOffAssigned(EmployeeInfo empInfo){
		boolean isWo = false;
		Calendar cal=empInfo.getLeaveCalendar();
		if(cal!=null){
			if(cal.isSUNDAY()||cal.isMONDAY()||cal.isTUESDAY()||cal.isWEDNESDAY()||cal.isTHRUSDAY()||cal.isFRIDAY()||cal.isSATAURDAY()){
				isWo=true;
			}
		}
		return isWo;
	}
	
	public int getTotalNoOfWeeklyOffBetweenDates(EmployeeInfo empInfo,Date sDate,Date eDate){
		Date startDate=CalendarUtil.copyDate(sDate);
		Date endDate=CalendarUtil.copyDate(eDate);
		
		int totalWo=0;
		
		Calendar cal=empInfo.getLeaveCalendar();
		if(cal!=null){
			boolean isWoDefFlag=false;
			ArrayList<Integer> dayList=new ArrayList<Integer>();
			if(cal.isSUNDAY()){
				dayList.add(0);
				isWoDefFlag=true;
			}
			if(cal.isMONDAY()){
				dayList.add(1);
				isWoDefFlag=true;
			}
			if(cal.isTUESDAY()){
				dayList.add(2);
				isWoDefFlag=true;
			}
			if(cal.isWEDNESDAY()){
				dayList.add(3);
				isWoDefFlag=true;
			}
			if(cal.isTHRUSDAY()){
				dayList.add(4);
				isWoDefFlag=true;
			}
			if(cal.isFRIDAY()){
				dayList.add(5);
				isWoDefFlag=true;
			}
			if(cal.isSATAURDAY()){
				dayList.add(6);
				isWoDefFlag=true;
			}
			
			while(!startDate.equals(endDate)){
				if(isWoDefFlag){
					int day=startDate.getDay();
					System.out.println("DAY : "+day);
					if(dayList.contains(day)){
						totalWo++;
					}
				}
				CalendarUtil.addDaysToDate(startDate, 1);
			}
		}
		return totalWo;
	}
	
	public int getTotalNoOfHolidaysBetweenDates(EmployeeInfo empInfo,Date sDate,Date eDate){
		Date startDate=CalendarUtil.copyDate(sDate);
		Date endDate=CalendarUtil.copyDate(eDate);
		
		int totalHoliday=0;
		
		Calendar cal=empInfo.getLeaveCalendar();
		if(cal!=null){
			while(!startDate.equals(endDate)){
				if(cal.isHolidayDate(startDate)){
					totalHoliday++;
				}
				CalendarUtil.addDaysToDate(startDate, 1);
			}
		}
		return totalHoliday;
	}
	
	public boolean isHolidayBetweenDates(EmployeeInfo empInfo,Date sDate,Date eDate,String holidayName){
		boolean holidayStatus=false;
		Date startDate=CalendarUtil.copyDate(sDate);
		Date endDate=CalendarUtil.copyDate(eDate);
		Calendar cal=empInfo.getLeaveCalendar();
		if(cal!=null){
			while(!startDate.equals(endDate)){
				if(cal.isHolidayDate(startDate)){
					for(Holiday holiday:empInfo.getLeaveCalendar().getHoliday()){
						if(holiday.getHolidaydate().equals(startDate)){
							if(holidayName.equalsIgnoreCase(holiday.getHolidayname())){
								return true;
							}
						}
					}
				}
				CalendarUtil.addDaysToDate(startDate, 1);
			}
		}
		return holidayStatus;
	}
	
	public String getHolidayName(EmployeeInfo empInfo,Date date){
		String holidayName="";
		for(Holiday holiday:empInfo.getLeaveCalendar().getHoliday()){
			if(holiday.getHolidaydate().equals(date)){
				holidayName=holiday.getHolidayname();
				break;
			}
		}
		return holidayName;
	}
	
	public boolean isDateFallsOnWo(Date date,EmployeeInfo empInfo){
		boolean isFallOnWo=false;
		Calendar cal=empInfo.getLeaveCalendar();
		if(cal!=null){
			ArrayList<Integer> dayList=new ArrayList<Integer>();
			if(cal.isSUNDAY()){
				dayList.add(0);
			}
			if(cal.isMONDAY()){
				dayList.add(1);
			}
			if(cal.isTUESDAY()){
				dayList.add(2);
			}
			if(cal.isWEDNESDAY()){
				dayList.add(3);
			}
			if(cal.isTHRUSDAY()){
				dayList.add(4);
			}
			if(cal.isFRIDAY()){
				dayList.add(5);
			}
			if(cal.isSATAURDAY()){
				dayList.add(6);
			}
			
			int day=date.getDay();
			if(dayList.contains(day)){
				isFallOnWo=true;
				return isFallOnWo;
			}
		}
		
		return isFallOnWo;
	}
	
	public Attendance getAttendance(EmployeeInfo empInfo,String month,Date attendanceDate,boolean isPresent,boolean isWo,boolean isHoliday, EmpAttendanceDetails object,boolean isPaidLeave,boolean isUnpaidLeave, ArrayList<Overtime> overtimelist){
		Date date=CalendarUtil.copyDate(attendanceDate);
		
		Attendance attendace=new Attendance();
		attendace.setCompanyId(UserConfiguration.getCompanyId());
		attendace.setMonth(month);
		attendace.setStatus(ConcreteBusinessProcess.APPROVED);
		attendace.setApprovalDate(new Date());
		
		attendace.setEmpId(empInfo.getEmpCount());
		attendace.setEmployeeName(empInfo.getFullName());
		attendace.setEmpDesignation(empInfo.getDesignation());
		attendace.setProjectName(empInfo.getProjectName());
		attendace.setShift(empInfo.getShiftName());
		attendace.setBranch(empInfo.getBranch());
		attendace.setAttendanceDate(date);
		
		if(isPaidLeave){
			if(object.getTotalNoOfPaidLeave()>0){
				attendace.setLeaveType(getLeaveDetails("PL", object.getLeaveShortName(), object.getEmpLeaveBalance(), 1,false,null));
				attendace.setTotalWorkedHours(0.0);
				attendace.setWorkedHours(0.0);
				attendace.setLeaveHours(empInfo.getLeaveCalendar().getWorkingHours());
				attendace.setLeave(true);
				attendace.setActionLabel(object.getLeaveShortName());
				object.setTotalNoOfPaidLeave(object.getTotalNoOfPaidLeave()-1);
			}
			
		}else if(isUnpaidLeave){
			if(object.getTotalNoOfUnpdaidLeave()>0){
				attendace.setLeaveType(getLeaveDetails("LWP", object.getLeaveShortName(), object.getEmpLeaveBalance(), 1,false,null));
				attendace.setTotalWorkedHours(0.0);
				attendace.setWorkedHours(0.0);
				attendace.setLeaveHours(empInfo.getLeaveCalendar().getWorkingHours());
				attendace.setLeave(true);
				attendace.setActionLabel(AppConstants.A);
				object.setTotalNoOfUnpdaidLeave(object.getTotalNoOfUnpdaidLeave()-1);
			}
			
		}else if(isPresent){
			attendace.setPresent(true);
			attendace.setTotalWorkedHours(empInfo.getLeaveCalendar().getWorkingHours());
			attendace.setWorkedHours(empInfo.getLeaveCalendar().getWorkingHours());
			attendace.setActionLabel(AppConstants.P);
			
			if(object.getTotalNoOfSingleOt()!=0){
				attendace.setOvertime(true);
				attendace.setOvertimeHours(object.getTotalNoOfSingleOt());
				attendace.setProjectNameForOT(attendace.getProjectName());
				attendace.setOvertimeType(getOtDetails(object.getProject(),"sOt", empInfo.getEmpCount(),overtimelist));
				
				attendace.setTotalWorkedHours(object.getTotalNoOfSingleOt()+attendace.getTotalWorkedHours());
				object.setTotalNoOfSingleOt(0);
				
			}else if(object.getTotalNoOfDoubleOt()!=0){
				attendace.setOvertime(true);
				attendace.setOvertimeHours(object.getTotalNoOfDoubleOt());
				attendace.setProjectNameForOT(attendace.getProjectName());
				attendace.setOvertimeType(getOtDetails(object.getProject(),"dOt", empInfo.getEmpCount(),overtimelist));
				
				attendace.setTotalWorkedHours(object.getTotalNoOfDoubleOt()+attendace.getTotalWorkedHours());
				object.setTotalNoOfDoubleOt(0);
			}else if(object.getTotalNoOfHalfDays()>0){
				attendace.setTotalWorkedHours(empInfo.getLeaveCalendar().getWorkingHours()/2.0);
				attendace.setWorkedHours(empInfo.getLeaveCalendar().getWorkingHours());
				attendace.setActionLabel(AppConstants.HF);
				object.setTotalNoOfHalfDays(object.getTotalNoOfHalfDays()-1);
				
				String leaveName=getLeaveDetails("PL", object.getLeaveShortName(), object.getEmpLeaveBalance(), 1/2.0,true,object);
				if(leaveName==null){
					leaveName=getLeaveDetails("LWP", object.getLeaveShortName(), object.getEmpLeaveBalance(), 1/2.0,false,null);
				}
				if(leaveName!=null){
					attendace.setLeaveType(leaveName);
					attendace.setLeaveHours(empInfo.getLeaveCalendar().getWorkingHours()/2.0);
					attendace.setLeave(true);
				}
			}
			
		}else if(isHoliday){
			String holidayName="";
			for(Holiday holiday:empInfo.getLeaveCalendar().getHoliday()){
				if(holiday.getHolidaydate().equals(attendace.getAttendanceDate())){
					holidayName=holiday.getHolidayType();
					break;
				}
			}
			attendace.setTotalWorkedHours(0.0);
			attendace.setWorkedHours(0.0);
			attendace.setLeaveHours(empInfo.getLeaveCalendar().getWorkingHours());
			attendace.setLeave(true);
			attendace.setLeaveType(holidayName);
			attendace.setHoliday(true);
			attendace.setActionLabel(AppConstants.H);
			
			if(object.getTotalNoOfPhOt()!=0&&holidayName.equalsIgnoreCase("Public Holiday")){
				attendace.setOvertime(true);
				attendace.setOvertimeHours(object.getTotalNoOfPhOt());
				attendace.setProjectNameForOT(attendace.getProjectName());
				attendace.setOvertimeType(getOtDetails(object.getProject(),"phOt", empInfo.getEmpCount(),overtimelist));
									
				attendace.setPresent(true);
				attendace.setTotalWorkedHours(object.getTotalNoOfPhOt());
				attendace.setWorkedHours(empInfo.getLeaveCalendar().getWorkingHours());
				attendace.setActionLabel(AppConstants.P);
				
				attendace.setLeave(false);
				
				object.setTotalNoOfPhOt(0);
			}else if(object.getTotalNoOfNhOt()!=0&&holidayName.equalsIgnoreCase("National Holiday")){
				attendace.setOvertime(true);
				attendace.setOvertimeHours(object.getTotalNoOfNhOt());
				attendace.setProjectNameForOT(attendace.getProjectName());
				attendace.setOvertimeType(getOtDetails(object.getProject(),"nhOt", empInfo.getEmpCount(),overtimelist));
									
				attendace.setPresent(true);
				attendace.setTotalWorkedHours(object.getTotalNoOfNhOt());
				attendace.setWorkedHours(empInfo.getLeaveCalendar().getWorkingHours());
				attendace.setActionLabel(AppConstants.P);
				
				attendace.setLeave(false);
				
				object.setTotalNoOfNhOt(0);
			}
			
			
		}else if(isWo){
			attendace.setTotalWorkedHours(0.0);
			attendace.setWorkedHours(0.0);
			attendace.setLeaveHours(empInfo.getLeaveCalendar().getWorkingHours());
			attendace.setLeave(false);
			attendace.setWeeklyOff(true);
			attendace.setActionLabel(AppConstants.WO);
			
			if(object.getTotalNoOfWeeklyoffOt()!=0){
				attendace.setOvertime(true);
				attendace.setOvertimeHours(object.getTotalNoOfWeeklyoffOt());
				attendace.setProjectNameForOT(attendace.getProjectName());
				attendace.setOvertimeType(getOtDetails(object.getProject(),"woOt", empInfo.getEmpCount(),overtimelist));
											
				attendace.setPresent(true);
				attendace.setTotalWorkedHours(object.getTotalNoOfWeeklyoffOt());
				attendace.setWorkedHours(empInfo.getLeaveCalendar().getWorkingHours());
				attendace.setActionLabel(AppConstants.P);
				
				object.setTotalNoOfWeeklyoffOt(0);
			}
		}
		return attendace;
	}
	
	
	public String getOtDetails(HrProject project,String otType,int empId, ArrayList<Overtime> overtimelist){
		String otName=null;
		if(project!=null){
			for(Overtime ot:overtimelist){
				if(ot.getEmpId()==empId){
					if(otType.equals("woOt")){
						if(ot.isWeeklyOff()==true){
							return ot.getName();
						}
					}else if(otType.equals("phOt")){
						if(ot.getHolidayType()!=null&&ot.getHolidayType().equalsIgnoreCase("Public Holiday")){
							return ot.getName();
						}
					}else if(otType.equals("nhOt")){
						if(ot.getHolidayType()!=null&&ot.getHolidayType().equalsIgnoreCase("National Holiday")){
							return ot.getName();
						}
					}else if(otType.equals("sOt")){
						if(ot.getShortName().equalsIgnoreCase("OT")){
							return ot.getName();
						}
					}else if(otType.equals("dOt")){
						if(ot.getShortName().equalsIgnoreCase("DOT")){
							return ot.getName();
						}
					}else{
						if(ot.isNormalDays()){
							return ot.getName();
						}
					}
				}
			}
		}
		return otName;
	}
	
	
	public String getLeaveDetails(String leaveType,String shortName,LeaveBalance empLeaveBalance,double noOfLeave,boolean isHalfDay,EmpAttendanceDetails object){
		String leaveName=null;
		
		for(AllocatedLeaves leaves:empLeaveBalance.getAllocatedLeaves()){
			if(leaveType.equals("PL")){
				if(leaves.getPaidLeave()==true&&leaves.getShortName().equals(shortName)){
					if(isHalfDay){
						if (leaves.getBalance() >= noOfLeave+object.getNoOfPl()) {
							return leaves.getName();
						}
					}else{
						if (leaves.getBalance() >= noOfLeave) {
							return leaves.getName();
						}
					}
				}
			}else if(leaveType.equals("LWP")){
				if(leaves.getPaidLeave()==false){
					return leaves.getName();
				}
			}
		}
		return leaveName;
	}
	
	public String getShiftName(HrProject project,int empId, ArrayList<Overtime> overtimelist){
		String shiftName="";
		if(project!=null&&overtimelist!=null&&overtimelist.size()!=0){
			for(Overtime ot:overtimelist){
				if(ot.getEmpId()==empId&&ot.getShift()!=null&&!ot.getShift().equals("")){
					return ot.getShift();
				}
			}
		}
		return shiftName;
	}
	
protected void addovertimeandproced(EmployeeInfo empInfo, ArrayList<Overtime> overtimelist) {

		
		if(empInfo.getProjectName()!=null&&!empInfo.getProjectName().equals("")){
			if(!project.getValue().equals(empInfo.getProjectName())){
				showDialogMessage("Employee project and selected project mismatched!");
				return;
			}
			
			EmpAttendanceDetails empAttenDet=new EmpAttendanceDetails();
			empAttenDet.setEmpId(empInfo.getEmpCount());
			empAttenDet.setEmpName(empInfo.getFullName());
			empAttenDet.setEmpDesignation(empInfo.getDesignation());
			empAttenDet.setProjectName(project.getValue());
			empAttenDet.setShiftName(getShiftName(project.getSelectedItem(), empInfo.getEmpCount(),overtimelist));
			empAttenDet.setMonth(getContributionPeriod(attendancePeriod));
			
			calculateDays(empInfo, attendancePeriod.getValue());
			empAttenDet.setTotalNoOfDays(totalNoOfDaysInMonth);
			empAttenDet.setTotalNoOfWeeklyOff(totalNoOfWO);
			empAttenDet.setTotalNoOfHolidays(totalNoOfHoliday);
			empAttenDet.setTotalNoOfPresentDays(totalNoOfDaysInMonth-totalNoOfWO-totalNoOfHoliday);
			empAttenDet.setLeaveShortName("");
			empAttenDet.setEmpInfo(empInfo);
			
			HrProject hrproject = project.getSelectedItem();
			hrproject.setOtList(overtimelist);
//			empAttenDet.setProject(project.getSelectedItem());
			empAttenDet.setProject(hrproject);

			addEmployeeToMap(empInfo);
			loadLeaveBalanceEmployeeWise(empInfo.getEmpCount(), empAttenDet);
			
			
		}else{
			showDialogMessage("Project is not allocated!");
			return;
		}
		
	}
	

}
