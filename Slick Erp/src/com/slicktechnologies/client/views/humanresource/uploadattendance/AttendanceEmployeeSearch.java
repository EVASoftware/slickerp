package com.slicktechnologies.client.views.humanresource.uploadattendance;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.humanresource.employeeattendance.EmployeeAttendanceTable;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.attendance.EmployeeAttendance;

public class AttendanceEmployeeSearch  extends Composite{

	public EmployeeInfoComposite personInfo;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Department> olbDepartMent;
	public ObjectListBox<Config> olbEmploymentType;
	public ObjectListBox<Config> olbDesignation;
	public ObjectListBox<Country> olbCountry;
	public ObjectListBox<Config> olbRole;
    public EmployeeAttendanceTable allocated;
    public EmployeeAttendanceTable unAllocated;
    public Button btnGo;
    public FlowPanel content;
    
   
    
    /** The form. */
	private FlexForm form;

	
	public AttendanceEmployeeSearch()
	{
		super();
		createGui();
		initWidget(content);
	}
	public void initWidget()
	{
		content=new FlowPanel();
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		olbDepartMent=new ObjectListBox<Department>();
		AppUtility.makeSalesPersonListBoxDepartment(olbDepartMent);
		olbEmploymentType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbEmploymentType,Screen.EMPLOYEETYPE);
		olbDesignation=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbDesignation,Screen.EMPLOYEEDESIGNATION);
		olbRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbRole,Screen.EMPLOYEEROLE);
		olbCountry=new ObjectListBox<Country>();
		olbCountry.MakeLive(new MyQuerry(new Vector<Filter>(),new Country()));
		allocated=new EmployeeAttendanceTable();
		unAllocated=new EmployeeAttendanceTable();
//		unAllocated=new UnAllocatedEmployeeInformationTable();
		btnGo=new Button("Go");
		
		
	}
	public void createGui()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(3).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Department",olbDepartMent);
		FormField folbdepartment= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Employment Type",olbEmploymentType);
		FormField folbtype= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Employee Designation",olbDesignation);
		FormField folbdesignation= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Employee Role",olbRole);
		FormField folbrole= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Country",olbCountry);
		FormField folbCountry= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*builder = new FormFieldBuilder(); 
		FormField fEmployeeInformation=builder.setlabel("Un Allocated Employees").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
	     builder=new FormFieldBuilder("",unAllocated.getTable());
	     FormField employeeTable=builder.setColSpan(3).build();*/
	     
	     builder = new FormFieldBuilder(); 
	     FormField falEmployeeInformation=builder.setlabel("Allocated Employees").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
	     builder=new FormFieldBuilder("",allocated.getTable());
	     FormField alemployeeTable=builder.setColSpan(3).build();
	     
	     builder = new FormFieldBuilder(); 
	     FormField falunallocatEmpInfo=builder.setlabel("Unallocated Employees").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
	     builder=new FormFieldBuilder("",unAllocated.getTable());
	     FormField unAllemployeeTable=builder.setColSpan(3).build();
	     
        builder = new FormFieldBuilder("",btnGo);
        FormField btnGo= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();


		FormField[][]fields=new FormField[][]{
				{fpersonInfo},
				{folbCountry,folbBranch,folbdepartment,},
				{folbtype,folbdesignation,folbrole},
				{btnGo},
				{falunallocatEmpInfo},
				{unAllemployeeTable},
				{falEmployeeInformation},
				{alemployeeTable},
				
		};
		
      form=new FlexForm(fields,FormStyle.ROWFORM);
		
		applyStyle();
		content.add(form);
	
	}
	public Vector<Filter>  getEmployeeFilter()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		System.out.println(" -- " + UserConfiguration.getCompanyId());
		if(UserConfiguration.getCompanyId()!=null){
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(UserConfiguration.getCompanyId());
			filtervec.add(temp);
		}

		if(olbDepartMent.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(olbDepartMent.getValue().trim());
			temp.setQuerryString("department");
			filtervec.add(temp);
		}
		
		

		if(olbCountry.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(olbCountry.getValue().trim());
			temp.setQuerryString("country");
			filtervec.add(temp);
		}

		if(olbEmploymentType.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(olbEmploymentType.getValue().trim());
			temp.setQuerryString("employeeType");
			filtervec.add(temp);
		}

		if(olbRole.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbRole.getValue().trim());
			temp.setQuerryString("employeerole");
			filtervec.add(temp);
		}
		if(olbDesignation.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbDesignation.getValue().trim());
			temp.setQuerryString("designation");
			filtervec.add(temp);
		}
		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}

		System.out.println("person count =-- "+ personInfo.getId().getValue());
		if(!personInfo.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
			temp.setQuerryString("empCount");
			filtervec.add(temp);
		}

		
		
		if(!(personInfo.getName().getValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(personInfo.getName().getValue());
			temp.setQuerryString("fullName");
			filtervec.add(temp);
		}
		System.out.println(" cell number -- " +personInfo.getCellNumber());
		if(!personInfo.getPhone().getValue().equals(""))
		{
			temp=new Filter();
			temp.setLongValue(personInfo.getCellNumber());
			temp.setQuerryString("cellNumber");
			filtervec.add(temp);
		}

		return filtervec;
	}
	
	public EmployeeAttendanceTable getAllocated() {
		return allocated;
	}
	public void setAllocated(EmployeeAttendanceTable allocated) {
		this.allocated = allocated;
	}
	
	public void setAllocatedValues(List<EmployeeAttendance> employeeInformation)
	{
		if(employeeInformation!=null)
		{
			allocated.connectToLocal();
			allocated.getListDataProvider().setList(employeeInformation);
		}
	}
	
	
	public List<EmployeeAttendance>  getAllocatedValues(List<EmployeeInfo> employeeInformation)
	{
		if(allocated!=null)
		{
			return allocated.getValue();
		}
		
		return null;
	}
	
	/*public List<AllocationResult> getUnAllocatedValues()
	{
		if(unAllocated!=null)
		{
			return unAllocated.getValue();
		}
		
		return null;
	}*/
	
	public void applyStyle()
	{
		form.setWidth("98%");
		
		
	}
	public Button getBtnGo() {
		return btnGo;
	}
	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}
	
	

}
