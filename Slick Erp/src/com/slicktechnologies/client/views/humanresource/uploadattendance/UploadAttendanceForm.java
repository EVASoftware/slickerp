package com.slicktechnologies.client.views.humanresource.uploadattendance;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.composite.UploadReaderComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.humanresourcelayer.attendance.EmployeeAttendance;

public class UploadAttendanceForm extends FormScreen<EmployeeAttendance>
{
	private ObjectListBox<Branch>olbbranch;
	/**
	 *  nidhi	
	 *  7-08-2017
	 *  
	 */
	public UploadComposite readerComposite;
	public Button btnMasterGo;
    /*
     * Added by Sheetal -20/10/21
     */
	public Button btnDownloadFileFormat;
	
	public UploadAttendanceForm()
	{
		super();
		createGui();
	}
	
	public UploadAttendanceForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}
	
	private void initalizeWidget()
	{

		olbbranch=new ObjectListBox<Branch>();
		btnMasterGo = new Button("Go");
	
		btnDownloadFileFormat = new Button("Download Upload File");
		AppUtility.makeBranchListBoxLive(olbbranch);
		
		readerComposite=new UploadComposite();
	}
	@Override
	public void toggleAppHeaderBarMenu() 
	{
		
	}
	@Override
	public void createScreen()
	{
	
		initalizeWidget();

		//Token to initialize the processlevel menus.
//		this.processlevelBarNames=new String[]{};



		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
	
		//InlineLabel format=new InlineLabel("Id,Punch Card No,Name,Date(mm-dd-yyyy),In Time,Out Time,Status");
		fbuilder = new FormFieldBuilder("Please Upload Attendance In Format:::Id,Punch Card No,Name,Date(mm-dd-yyyy),In Time,Out Time,Status",null);
		FormField f=fbuilder.build();
		
		//fbuilder = new FormFieldBuilder("Branch",olbbranch);
		//FormField folbbranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("Upload Attendance", readerComposite);
		FormField freaderComposite=fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("", btnMasterGo);
		FormField freaderCompositeGo=fbuilder.setRowSpan(0).setColSpan(3).build();
		
		fbuilder=new FormFieldBuilder("", btnDownloadFileFormat );
		FormField freaderCompositeDownloadFileFormat=fbuilder.setRowSpan(0).setColSpan(3).build();
		
		FormField[][] formfield = { 
				{freaderComposite,freaderCompositeGo,freaderCompositeDownloadFileFormat},
				{f}
		};

		this.fields=formfield;
	}
	@Override
	public void updateModel(EmployeeAttendance model) 
	{
		
		
	}
	@Override
	public void updateView(EmployeeAttendance model)
	{
		
		
	}
	

}
