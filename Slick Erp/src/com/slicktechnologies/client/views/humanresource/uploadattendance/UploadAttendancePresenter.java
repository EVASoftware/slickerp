package com.slicktechnologies.client.views.humanresource.uploadattendance;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.DataMigrationService;
import com.slicktechnologies.client.services.DataMigrationServiceAsync;
import com.slicktechnologies.client.services.DataMigrationTaskQueueService;
import com.slicktechnologies.client.services.DataMigrationTaskQueueServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.lead.LeadPresenterSearchProxy;
import com.slicktechnologies.client.views.lead.LeadPresenterTableProxy;
import com.slicktechnologies.client.views.lead.LeadPresenter.LeadPresenterSearch;
import com.slicktechnologies.client.views.lead.LeadPresenter.LeadPresenterTable;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.attendance.AttandanceDetails;
import com.slicktechnologies.shared.common.attendance.AttandanceError;
import com.slicktechnologies.shared.common.attendance.AttandanceInfo;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.attendance.EmployeeAttendance;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar.WeekleyOff;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;

public class UploadAttendancePresenter extends FormScreenPresenter<EmployeeAttendance>
{

	public static UploadAttendanceForm form;
	final DataMigrationTaskQueueServiceAsync datamigAsync = GWT.create(DataMigrationTaskQueueService.class);
	final GenricServiceAsync async=GWT.create(GenricService.class);
	
	 DateTimeFormat format = DateTimeFormat.getFormat("c");
	CsvServiceAsync csvservice = GWT.create(CsvService.class);

	public UploadAttendancePresenter (FormScreen<EmployeeAttendance> view, EmployeeAttendance model) 
	{
		super(view, model);
		form=(UploadAttendanceForm) view;
		form.setPresenter(this);
		form.btnMasterGo.addClickHandler(this);
		form.btnDownloadFileFormat.addClickHandler(this);
	}
	@Override
	public void reactToProcessBarEvents(ClickEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnPrint() 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() 
	{
		// TODO Auto-generated method stub
		
	}
	

	public static void initalize()
	{
		form=new  UploadAttendanceForm();


		//UploadAttendancePresenterTable gentable=GWT.create(UploadAttendancePresenterTable.class);
		//gentable.setView(form);
		//gentable.applySelectionModle();
		//UploadAttendancePresenterSearch.staticSuperTable=gentable;
		
		
		//UploadAttendancePresenterSearch searchpopup=GWT.create(UploadAttendancePresenterSearch.class);
		//form.setSearchpopupscreen(searchpopup);
		
	
		UploadAttendancePresenter  presenter=new UploadAttendancePresenter(form,new EmployeeAttendance());
		AppMemory.getAppMemory().stickPnel(form);
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAttendance")
	public static  class UploadAttendancePresenterSearch extends SearchPopUpScreen<EmployeeAttendance>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};

		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAttendance")
		public static class UploadAttendancePresenterTable extends SuperTable<EmployeeAttendance> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub

			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub

			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub

			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub

			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub

			}} ;

			private void save()
			{
				async.save(model,new AsyncCallback<ReturnFromServer>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(ReturnFromServer result) {
						// TODO Auto-generated method stub
						
					}
				});
			}
			
			@Override
			public void onClick(ClickEvent event){
				try {
					super.onClick(event);
					/*
					 * Added by Sheetal   
					 */
					if (event.getSource() == form.btnDownloadFileFormat) {
						System.out.println("Download Excel File");
                        Console.log("Download file");
                        
                        Company comp = new Company();
                        String url;
                        if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.ATTENDANCE, AppConstants.ADDSINGLEANDDOUBLEOTINATTENDANCEUPLOADTEMPLATE)){
                        	url="https://docs.google.com/spreadsheets/d/1aenOakZhNnu6I9yyicVU1YL_3QuxxP5_/edit?usp=sharing&ouid=108329078979522135777&rtpof=true&sd=true";
                        	Console.log("With OT");
                        }
                       
                        else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.ATTENDANCE, AppConstants.ENABLESITELOCATION)){
                        	url="https://docs.google.com/spreadsheets/d/1fvktSaV4rJmeQ-iWMFTpjUc99aNZRymZ/edit?usp=sharing&ouid=108329078979522135777&rtpof=true&sd=true";
                           Console.log("Site Location File");
                        }
                        else{
                        	url="https://docs.google.com/spreadsheets/d/1vXQr5SCSYv3n8SXoQLFowlLvi12YQjvb/edit?usp=sharing&ouid=108329078979522135777&rtpof=true&sd=true";
                        }
                     
                        Window.open(url, "test", "enabled");
					}
					
					

					if (event.getSource() == form.btnMasterGo) {
						System.out.println("upload file");

						Company c = new Company();

						if (form.readerComposite.getValue() == null) {
							form.showDialogMessage("Please upload file");
						} else {
							form.showWaitSymbol();
							datamigAsync.saveAttandance(c.getCompanyId(), "Employee Attendance",new AsyncCallback<ArrayList<AttandanceInfo>>() {

								@Override
								public void onFailure(Throwable caught) {
									form.showDialogMessage("Failed");
									form.hideWaitSymbol();
								}

								@Override
								public void onSuccess(ArrayList<AttandanceInfo> result) {
									if(result!=null&&result.size()!=0){
										if(result.get(0).getErrorCode()==-1){
											form.hideWaitSymbol();
											form.showDialogMessage("Invalid Excel File");
										}else if(result.get(0).getErrorCode()==-2){
											form.hideWaitSymbol();
											form.showDialogMessage("Please enter month in mm/dd/yyyy format.");
										}
										else if(result.get(0).getErrorCode()==0){
											datamigAsync.validateAttandance(result, new AsyncCallback<ArrayList<AttandanceInfo>>() {

												@Override
												public void onFailure(Throwable caught) {
													form.hideWaitSymbol();
												}

												@Override
												public void onSuccess(final ArrayList<AttandanceInfo> attandanceList) {
													
													if(attandanceList.get(0).getErrorCode()==0){
//														datamigAsync.updateAttandance(result, new AsyncCallback<ArrayList<AttandanceInfo>>() {
//															@Override
//															public void onFailure(Throwable caught) {
//																form.hideWaitSymbol();
//															}
//
//															@Override
//															public void onSuccess(ArrayList<AttandanceInfo> result) {
//																form.hideWaitSymbol();
//																form.showDialogMessage("Attendance upload process started.");
//																
//															}
//														});
														
														Console.log("Total attendance upload file size "+attandanceList.size());
														
														if(attandanceList.size()>500) {
															form.showDialogMessage("upload maximum 500 records only at a time!");
															return;
														}
														
														
														
														HashSet<Integer> hsEmpId=new HashSet<Integer>();
														HashSet<String> hsProj=new HashSet<String>();
														
														
														for(AttandanceInfo info:attandanceList){
															hsEmpId.add(info.getEmpId());
															hsProj.add(info.getProjName().trim());
														}
														final ArrayList<Integer> empList=new ArrayList<Integer>(hsEmpId);
														final ArrayList<String> projList=new ArrayList<String>(hsProj);
														Console.log("TOTAL NO. OF EMP : "+empList.size());
														Console.log("NO. OF Proj : "+projList.size());

														datamigAsync.loadLeaveBalanceEntity(empList, attandanceList.get(0).getCompanyId(), new AsyncCallback<ArrayList<LeaveBalance>>() {
															
															@Override
															public void onSuccess(ArrayList<LeaveBalance> result) {
																// TODO Auto-generated method stub
																Console.log("loadLeaveBalanceEntity"+result.size());

																final List<LeaveBalance>empLeaveBalList= new ArrayList<LeaveBalance>();
																if(result!=null && result.size()>0){
																	empLeaveBalList.addAll(result);
																}
																Console.log("1 loadLeaveBalanceEntity "+result.size());

																datamigAsync.loadEmployeeInfoEntity(empList, attandanceList.get(0).getCompanyId(), new AsyncCallback<ArrayList<EmployeeInfo>>() {
																	
																	@Override
																	public void onSuccess(ArrayList<EmployeeInfo> result) {
																		// TODO Auto-generated method stub
																		Console.log("loadEmployeeInfoEntity"+result.size());

																		final List<EmployeeInfo> empInfoList = new ArrayList<EmployeeInfo>();
																		if(result!=null && result.size()>0){
																			empInfoList.addAll(result);
																		}
																		Console.log("1 loadEmployeeInfoEntity"+result.size());

																		datamigAsync.loadHRprojectEntity(projList, attandanceList.get(0).getCompanyId(), new AsyncCallback<ArrayList<HrProject>>() {
																			
																			@Override
																			public void onSuccess(ArrayList<HrProject> result) {
																				// TODO Auto-generated method stub
																				Console.log("loadHRprojectEntity"+result.size());

																				final ArrayList<HrProject>projectList = new ArrayList<HrProject>();

																				final ArrayList<Integer> projectidlist = new ArrayList<Integer>();

																				if(result!=null && result.size()>0){
																					projectList.addAll(result);
																				}
																				
																				for(HrProject project : result){
																					projectidlist.add(project.getCount());
																				}
																				
																				HashSet<String> hsProj=new HashSet<String>();

																				for(AttandanceInfo info:attandanceList){
																					hsProj.add(info.getProjName().trim());
																				}
																				ArrayList<String> projList=new ArrayList<String>(hsProj);
																				
																				Console.log("1 loadHRprojectEntity"+result.size());

																				datamigAsync.loadExistingAttendance(attandanceList, empList,projList, new AsyncCallback<ArrayList<Attendance>>() {
																					
																					@Override
																					public void onSuccess(ArrayList<Attendance> result) {
																						// TODO Auto-generated method stub
																						Console.log("loadExistingAttendance success");
																						final List<Attendance> existingAttendanceList = new ArrayList<Attendance>();
																						if(result!=null) {
																							Console.log("loadExistingAttendance"+result.size());

																							for(Attendance attendance : result){
																								existingAttendanceList.add(attendance);
																							}
																							Console.log("1 loadExistingAttendance"+result.size());
																						}else
																							Console.log("loadExistingAttendance result null");
																						Console.log("attandanceList"+attandanceList.size());
																						Console.log("empList"+empList.size());
																						Console.log("projectidlist"+projectidlist.size());

																						datamigAsync.loadHRprojectOvertimeEntity(attandanceList, empList, projectidlist, new AsyncCallback<ArrayList<HrProjectOvertime>>() {
																							
																							@Override
																							public void onSuccess(ArrayList<HrProjectOvertime> result) {
																								// TODO Auto-generated method stub
																								final ArrayList<HrProjectOvertime> HrProjectOvertimelist = new ArrayList<HrProjectOvertime>();

																								if(result!=null && result.size()>0) {
																									Console.log("loadHRprojectOvertimeEntity"+result.size());

																										
																									for(HrProjectOvertime hrprojectovertime : result){
																										HrProjectOvertimelist.add(hrprojectovertime);

																									}

																								}else
																									Console.log("loadHRprojectOvertimeEntity result null");
																								
																								Console.log("Data loading RPC done here");
																								
																								

																								/**
																								 * @author Anil,Date :24-01-2019
																								 * requirement is instead of putting working hours ,wanted to put label as 'P','HD'
																								 * For Orion By Sonu
																								 */
																								boolean labelAsInputFlag=false;
																								if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "AttendanceLabelAsInput")){
																									labelAsInputFlag=true;
																								}
																								Console.log("AttendanceLabelAsInput :: "+labelAsInputFlag+" OT/DOT FLAG "+attandanceList.get(0).isOtDotFlag());
																								
																								DateTimeFormat fmt = DateTimeFormat.getFormat("MM/dd/yyyy");
																								ArrayList<AttandanceError>errorList=new ArrayList<AttandanceError>();

																								
																								for(AttandanceInfo info:attandanceList){
																									/**
																									 * Date : 23-07-2018 BY ANIL
																									 */
																									boolean calendarFlag=false;

																									double totalNoOfDays = 0;
																									double totalNoOfPresentDays = 0;
																									double totalNoWeeklyOff=0;
																									double totalNoOfHoliday=0;
																									double totalNoOfAbsentDays=0;
																									double totalExtraHours=0;
																									int allowedWoCount=0;//Ashwini Patil Date:19-07-2023
																									
																									/**
																									 * date :16-12-2018 By ANIL
																									 * Changed the variable name form totalLeaveHours to totalLeave
																									 */
																									double totalLeave=0;
																									
//																									double woAsLeave=0;
																									double halfDayLeave=0;
																									
																									double woOt=0;
																									double hOt=0;
																									double nOt=0;
																									double nhOt=0;
																									double phOt=0;
																									
																									AttandanceError error=new AttandanceError();
																									HashMap<String , Double> leaveTotalMap = new HashMap<String , Double>();
																									
																									
																									Comparator<AttandanceDetails> attandanceComparator = new  Comparator<AttandanceDetails>() {
																										@Override
																										public int compare(AttandanceDetails o1, AttandanceDetails o2) {
																											return o1.getDate().compareTo(o2.getDate());
																										}
																									};
																									Collections.sort(info.getAttandList(),attandanceComparator);
																									
																									LeaveBalance leaveBalance=null;
																									if(empLeaveBalList!=null&&empLeaveBalList.size()!=0){
																										leaveBalance=getEmpLeaveBalance(info.getEmpId(),empLeaveBalList);
																									}
																									
																									EmployeeInfo empInfo=validateEmpId(info.getEmpId(), empInfoList);
																									

																									/**
																									 * @author Anil
																									 * @since 29-01-2021
																									 * Commenting above logic of validating existing attendance
																									 * As per the new logic of user is trying to upload attendance of same date which is uploaded then only we restrict to upload
																									 * earlier if any day attendance was found then he to delete and re upload attendance
																									 * Raised by Rahul For Sunrise
																									 */
																									ArrayList<String> existingAttendanceListDtWise=new ArrayList<String>();
																									try {
																									Label:	for(Attendance attend:existingAttendanceList){
																												if(info.getEmpId()==attend.getEmpId()){
																													existingAttendanceListDtWise.add(info.getEmpId()+""+fmt.format(attend.getAttendanceDate()));
																												}
																											}
																									} catch (Exception e) {
																									}
																									
																									/**
																									 * @author Anil , Date :04-05-2019
																									 * Validating employee attendance overlapping
																									 * if user is marking attendance for same day but for different project
																									 */
																									ArrayList<String> empIdListAsPerdate=new ArrayList<String>();

																									String msg="";
																									Console.log("info.getAttandList() size="+info.getAttandList().size());
																									int count=0;
																									for(AttandanceDetails attDet:info.getAttandList()){
																										boolean isRotationalWeeklyoffInProject=false;
																										boolean isAllowUnplannedHoliday=false;
																										count++;
																										Console.log("info.getAttandList() size="+attDet.getDate()+"month="+attDet.getDate().getMonth() +"year="+attDet.getDate().getYear());
																										HrProject project1=getProjectName(info.getProjName(),projectList);
																										WeekleyOff wo1 = empInfo.getLeaveCalendar().getWeeklyoff();
																										boolean isWoDefinedInCal1=isWeeklyOffDefined(wo1);
																										boolean isDayWo = isWeeklyOff(wo1,attDet.getDate());
																										if(project1.isRotationalWeeklyOff())
																											isRotationalWeeklyoffInProject=true;
																										if(project1.isAllowUnplannedHoliday())
																											isAllowUnplannedHoliday=true;
																										if(isDayWo)
																											allowedWoCount++;
																										
																										if(project1.isRotationalWeeklyOff()&&!isWoDefinedInCal1&&count==1) {
																											msg=msg+" / Rotational weekly off active in project but weekly off not defined in calendar.";
																											
																										}
																										/**
																										 * @author Anil , Date : 07-05-2019
																										 * If attendance is marked as OP then we will not consider that attendance for update
																										 * raised by Nitin Sir,Sonu
																										 */
																										if(attDet.getValue().equalsIgnoreCase("OP")){
																											Console.log("AttendanceLabelAsInput  :: "+attDet.getValue());
																											continue;
																										}
																										
																										
																										/**
																										 * @author Anil
																										 */
																										
																										if(labelAsInputFlag&&attDet.getValue().equalsIgnoreCase("P")){
//																											logger.log(Level.SEVERE,"AttendanceLabelAsInput P :: "+empInfo.getLeaveCalendar().getWorkingHours());
																											attDet.setValue(empInfo.getLeaveCalendar().getWorkingHours()+"");
																										}else if(labelAsInputFlag&&attDet.getValue().equalsIgnoreCase("HD")){
//																											logger.log(Level.SEVERE,"AttendanceLabelAsInput HD :: "+empInfo.getLeaveCalendar().getWorkingHours()/2);
																											attDet.setValue(empInfo.getLeaveCalendar().getWorkingHours()/2+"");
																										}
																										
																										/**
																										 * 
																										 */
																										if(!calendarFlag){
																										totalNoOfDays++;
																										
																										if(empInfo!=null){
																											if(empInfo.getDateOfJoining()!=null){
																												empInfo.getDateOfJoining().setHours(0);
																												empInfo.getDateOfJoining().setMinutes(0);
																												empInfo.getDateOfJoining().setSeconds(0);
																											}
																											if(empInfo.getLastWorkingDate()!=null){
																												empInfo.getLastWorkingDate().setHours(0);
																												empInfo.getLastWorkingDate().setMinutes(0);
																												empInfo.getLastWorkingDate().setSeconds(0);
																											}
																										}
																										
																										if(attDet.getDate().before(empInfo.getDateOfJoining())
																												&&!attDet.getValue().trim().equalsIgnoreCase("NA")){
																											System.out.println("ATT DT : "+attDet.getDate()+" JOINING DT : "+empInfo.getDateOfJoining()+" VALUE : "+attDet.getValue());
																											msg=msg+" / Attendance should not be before date of joining "+fmt.format(empInfo.getDateOfJoining());
																											break;
																										}
																										
																										if(empInfo.getLastWorkingDate()!=null&&attDet.getDate().after(empInfo.getLastWorkingDate())
																												&&!attDet.getValue().equalsIgnoreCase("NA")){
																											System.out.println("ATT DT : "+attDet.getDate()+" LAST WRKING DT : "+empInfo.getLastWorkingDate()+" VALUE : "+attDet.getValue());
																											msg=msg+" / Attendance should not be after last date of working "+fmt.format(empInfo.getLastWorkingDate());
																											break;
																										}
																										
																										if(attDet.getValue().trim().equalsIgnoreCase("END")){
																											break ;
																										}
																										
//																										if(!calendarFlag){
																											try{
																												
																												
																												double workedHrs=Double.parseDouble(attDet.getValue().trim());
																												WeekleyOff woff = empInfo.getLeaveCalendar().getWeeklyoff();
																												boolean isWoff = isWeeklyOff(woff,attDet.getDate());
																												boolean isHday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
																												
																												/**
																												 * @author Anil
																												 * @since 13-07-2020
																												 */
																												String holidayType=empInfo.getLeaveCalendar().getHolidayType(attDet.getDate());
																												String holidayLabel="H";
																												if(holidayType!=null){
																													Console.log(attDet.getDate()+ " holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
																													if(holidayType.equalsIgnoreCase("National Holiday")){
																														holidayLabel="NH";
																													}else if(holidayType.equalsIgnoreCase("Public Holiday")){
																														holidayLabel="PH";
																													}
																												}
																												if(workedHrs==empInfo.getLeaveCalendar().getWorkingHours()){
																													totalNoOfPresentDays++;
																													
																													if(isWoff){
																														woOt=woOt+workedHrs;
																														totalExtraHours+=workedHrs;
																													}else if(isHday){
																														hOt=hOt+workedHrs;
																														/**
																														 * Date : 17-08-2018 By ANIL
																														 * OT ERROR
																														 */
																														totalExtraHours+=workedHrs;
																														if(holidayLabel.equals("NH")){
																															nhOt=nhOt+workedHrs;
																														}else if(holidayLabel.equals("PH")){
																															phOt=phOt+workedHrs;
																														}
																														Console.log("--holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
																													}
																												}else if(workedHrs>empInfo.getLeaveCalendar().getWorkingHours()){
																													double extraHrs=workedHrs-empInfo.getLeaveCalendar().getWorkingHours();
																													totalExtraHours=totalExtraHours+extraHrs;
																													totalNoOfPresentDays++;
																													
																													WeekleyOff wo = empInfo.getLeaveCalendar().getWeeklyoff();
																													boolean isWo = isWeeklyOff(wo,attDet.getDate());
																													boolean isHoliday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
																													if(isWo){
																														woOt=woOt+extraHrs;
																													}else if(isHoliday){
																														hOt=hOt+extraHrs;
																													}else{
																														nOt=nOt+extraHrs;
																													}
																												}else if(workedHrs<empInfo.getLeaveCalendar().getWorkingHours()){
																													double leavaeHrs= empInfo.getLeaveCalendar().getWorkingHours()-workedHrs;
//																													halfDayLeave=halfDayLeave+leavaeHrs;
																													totalNoOfPresentDays++;
																													
																													if(isWoff){
																														woOt=woOt+leavaeHrs;
																														/**
																														 * Date : 17-08-2018 By ANIL
																														 * OT ERROR
																														 */
																														totalExtraHours+=workedHrs;
																													}else if(isHday){
																														hOt=hOt+leavaeHrs;
																														/**
																														 * Date : 17-08-2018 By ANIL
																														 * OT ERROR
																														 */
																														totalExtraHours+=workedHrs;
																														
																														if(holidayType.equals("NH")){
																															nhOt=nhOt+workedHrs;
																														}else if(holidayType.equals("PH")){
																															phOt=phOt+workedHrs;
																														}
																													}
																													/**
																													 * @author Anil , Date : 25-06-2019
																													 * will update half day leave if and only if its not on weekly off and holiday
																													 */
																													else{
																														halfDayLeave=halfDayLeave+leavaeHrs;
																													}
																												}
																												
																												
																												
																												/**
																												 * @author Anil, Date :04-05-2019
																												 */
																												String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
																												if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
																													if(empIdListAsPerdate.contains(empId_Date_Key)){
																														msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
																													}
																													empIdListAsPerdate.add(empId_Date_Key);
																												}else{
																													empIdListAsPerdate.add(empId_Date_Key);
																												}
																												
																												/**
																												 * @author Anil @since 29-01-2021
																												 */
																												if(existingAttendanceListDtWise.size()!=0){
																													if(existingAttendanceListDtWise.contains(empInfo.getEmpCount()+""+fmt.format(attDet.getDate()))){
//																														error.setEmpId(info.getEmpId());
//																														error.setEmpName(info.getEmpName());
//																														error.setProjectName(info.getProjName());
//																														error.setRemark("Attendance is already uploaded on "+fmt.format(attDet.getDate()));
//																														errorList.add(error);
//																														attandanceList.get(0).setErrorCode(-1);
																														
																														msg=msg+"Attendance is already uploaded on "+fmt.format(attDet.getDate());
																													}
																												}
																												
																												
																												
																											}catch(NumberFormatException exception){
																												
																												if(attDet.getValue().trim().equalsIgnoreCase("WO")||attDet.getValue().trim().equalsIgnoreCase("W/O")){
																													WeekleyOff wo = empInfo.getLeaveCalendar().getWeeklyoff();
																													boolean isWo = isWeeklyOff(wo,attDet.getDate());
//																													HrProject project=getProjectName(info.getProjName(),projectList);
																													
																													/**
																													 * @author Anil , Date : 20-08-2019
																													 */
																													boolean isWoDefinedInCal=isWeeklyOffDefined(wo);
																													System.out.println("WO : "+isWo+" Defined : "+isWoDefinedInCal+" Dt : "+attDet.getDate());
																													if(!isWo){
//																														woAsLeave++;
																														if(isWoDefinedInCal&&!project1.isRotationalWeeklyOff()){
																															msg=msg+" / Weekly off day is not matched. "+fmt.format(attDet.getDate());
																														}
																													}
																													totalNoWeeklyOff++;
																													
																													/**
																													 * @author Anil, Date :04-05-2019
																													 */
																													String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
																													if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
																														if(empIdListAsPerdate.contains(empId_Date_Key)){
																															msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
																														}
																														empIdListAsPerdate.add(empId_Date_Key);
																													}else{
																														empIdListAsPerdate.add(empId_Date_Key);
																													}
																													
																												}else if(attDet.getValue().trim().equalsIgnoreCase("H")){
																													boolean isHoliday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
																													if(!isHoliday&&!isAllowUnplannedHoliday){
																														msg=msg+" / Holiday date "+attDet.getDate()+" is not matched in Employee Calendar. ";
																													}
																													totalNoOfHoliday++;
																													
																													/**
																													 * @author Anil, Date :04-05-2019
																													 */
																													String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
																													if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
																														if(empIdListAsPerdate.contains(empId_Date_Key)){
																															msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
																														}
																														empIdListAsPerdate.add(empId_Date_Key);
																													}else{
																														empIdListAsPerdate.add(empId_Date_Key);
																													}
																													
																													String holidayType=empInfo.getLeaveCalendar().getHolidayType(attDet.getDate());
																													String holidayLabel="H";
																													if(holidayType!=null){
																														Console.log(attDet.getDate()+ "holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
																														if(holidayType.equalsIgnoreCase("National Holiday")){
																															holidayLabel="NH";
																														}else if(holidayType.equalsIgnoreCase("Public Holiday")){
																															holidayLabel="PH";
																														}
																													}
																													
																												}else if(attDet.getValue().trim().equalsIgnoreCase("L")){
																													totalLeave++;
																													
																													/**
																													 * @author Anil, Date :04-05-2019
																													 */
																													String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
																													if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
																														if(empIdListAsPerdate.contains(empId_Date_Key)){
																															msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
																														}
																														empIdListAsPerdate.add(empId_Date_Key);
																													}else{
																														empIdListAsPerdate.add(empId_Date_Key);
																													}
																													
																												}else if(attDet.getValue().trim().equalsIgnoreCase("A")){
																													totalNoOfAbsentDays++;
																													
																													/**
																													 * @author Anil, Date :04-05-2019
																													 */
																													String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
																													if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
																														if(empIdListAsPerdate.contains(empId_Date_Key)){
																															msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
																														}
																														empIdListAsPerdate.add(empId_Date_Key);
																													}else{
																														empIdListAsPerdate.add(empId_Date_Key);
																													}
																												}else if(attDet.getValue().trim().equalsIgnoreCase("NA")){
																													if(empInfo.getLastWorkingDate()!=null){
																														if(attDet.getDate().after(empInfo.getDateOfJoining())
																																&&attDet.getDate().before(empInfo.getLastWorkingDate())){
																															msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
																														}
																														
																													}else{
																														if(attDet.getDate().after(empInfo.getDateOfJoining())||attDet.getDate().equals(empInfo.getDateOfJoining())){
																															msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
																														}
																													}
																													
//																													if(empInfo.getLastWorkingDate()!=null&&!attDet.getDate().before(empInfo.getLastWorkingDate())){
//																														msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
//																														attandanceList.get(0).setErrorCode(-1);
//																													}
																													
																												}
																												/**
																												 * @author Anil
																												 * @since 10-07-2020
																												 * Adding condition for public holiday keyword
																												 */
																												else if(attDet.getValue().trim().contains("PH")&&validPH_OT(attDet.getValue())){
																													
																													WeekleyOff woff = empInfo.getLeaveCalendar().getWeeklyoff();
																													boolean isWoff = isWeeklyOff(woff,attDet.getDate());
																													boolean isHday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
																													
																													if(isWoff){
																														msg=msg+" / Can not mark attendance as PH on weekly off. "+attDet.getDate()+". ";
																													}else if(isHday){
																														msg=msg+" / Can not mark attendance as PH on holiday. "+attDet.getDate()+". ";
																													}
																													
																													totalNoOfHoliday++;
																													double phOtt=getTotalPhOtHours(attDet.getValue());
																													totalExtraHours=totalExtraHours+phOtt;
																													phOt+=phOtt;
																													
																													String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
																													if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
																														if(empIdListAsPerdate.contains(empId_Date_Key)){
																															msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
																														}
																														empIdListAsPerdate.add(empId_Date_Key);
																													}else{
																														empIdListAsPerdate.add(empId_Date_Key);
																													}
																													
																												}else{
																													/** date 05-07-2019 added by komal for woven fabric(short name wise leave deduction)**/
																													double value = 1;
																													if(leaveTotalMap.containsKey(attDet.getValue().trim())){
																														value = leaveTotalMap.get(attDet.getValue().trim());
																														leaveTotalMap.put(attDet.getValue().trim(), value + 1);
																													}else{
																														leaveTotalMap.put(attDet.getValue().trim(), value);
																													}
																														String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
																														if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
																															if(empIdListAsPerdate.contains(empId_Date_Key)){
																																msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
																															}
																															empIdListAsPerdate.add(empId_Date_Key);
																														}else{
																															empIdListAsPerdate.add(empId_Date_Key);
																														}
//																													msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
//																													attandanceList.get(0).setErrorCode(-1);
																												}
																												
																												
																												/**
																												 * @author Anil @since 29-01-2021
																												 */
																												if(!attDet.getValue().trim().equalsIgnoreCase("NA")&&existingAttendanceListDtWise.size()!=0){
																													if(existingAttendanceListDtWise.contains(empInfo.getEmpCount()+""+fmt.format(attDet.getDate()))){
//																														error.setEmpId(info.getEmpId());
//																														error.setEmpName(info.getEmpName());
//																														error.setProjectName(info.getProjName());
//																														error.setRemark("Attendance is already uploaded on "+fmt.format(attDet.getDate()));
//																														errorList.add(error);
//																														attandanceList.get(0).setErrorCode(-1);
																														
																														msg=msg+"Attendance is already uploaded on "+fmt.format(attDet.getDate());
																													}
																												}
																											}
																											
																											/**
																											 * Date : 18-08-2018 By ANIl
																											 * @author Anil
																											 * @since 26-11-2020
																											 * removing overriding functionality
																											 */
//																											Attendance attendance=getExistingAttendance(info.getEmpId(),existingAttendanceList,attDet.getDate());
//																											if(attendance!=null){
//																												attenListToBeRemoved.add(attendance);
//																											}
//																											
//																											EmployeeLeave empLeave=getExistingLeave(info.getEmpId(),approvedLeaveList,attDet.getDate());
//																											if(empLeave!=null){
//																												leaveListTobeRemoved.add(empLeave);
//																												updateLeaveBalance(leaveBalance,empLeave,empInfo);
//																											}
//																											
//																											EmployeeOvertime empOt=getExistingOt(info.getEmpId(),approvedOtList,attDet.getDate());
//																											if(empOt!=null){
//																												otListToBeRemoved.add(empOt);
//																											}
																										
																									}
																										Console.log("count="+count+" isRotationalWeeklyoffInProject="+isRotationalWeeklyoffInProject);
																										if(count==info.getAttandList().size()){
																											if(totalNoWeeklyOff>allowedWoCount&&isRotationalWeeklyoffInProject)
																												msg=msg+"/ Extra weekly off found. As per calendar allowed weekly off in this month are "+allowedWoCount;																						
																										}
																									}
																									
																									
																									if(leaveBalance!=null){
																										
																										/**
																										 * @author Anil , Date : 20-08-2019
																										 * We will not maintain WO as leave, will just mark attendance as weekly off
																										 * Suggested by Nitin Sir
																										 */
																										//WO as Leave condition
//																										if(woAsLeave>0){
//																											boolean woFlag=false;
//																											for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
//																												if(leave.getShortName().equalsIgnoreCase("WO")||leave.getShortName().equalsIgnoreCase("W/O")){
//																													woFlag=true;
//																													if(woAsLeave>leave.getBalance()){
//																														msg=msg+"/ Total WO leave balance is  "+leave.getBalance()+" only and requested WO leave is "+woAsLeave;
//																														attandanceList.get(0).setErrorCode(-1);
//																													}
//																													break;
//																												}
//																											}
//																											if(!woFlag){
//																												msg=msg+"/ No WO type leave is assigned to employee.";
//																												attandanceList.get(0).setErrorCode(-1);
//																											}
//																										}
																										
																										/**
																										 * End
																										 */
																										
																										//Total Paid Leave Condition
																										if(totalLeave>0){
																											boolean leaveFlag=false;
																											double afterDeductionLeaveBalance=0;
																											for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
//																												if(leave.getPaidLeave()==true&&
//																														!leave.getShortName().equalsIgnoreCase("WO")||!leave.getShortName().equalsIgnoreCase("W/O")){
																												if(leave.getPaidLeave()==true&&leave.getShortName().contains("PL")){
																													leaveFlag=true;
																													if(totalLeave>leave.getBalance()){
																														msg=msg+"/ Total paid leave balance is  "+leave.getBalance()+" only and requested leave is "+totalLeave;
																													}else{
																														afterDeductionLeaveBalance=leave.getBalance()-totalLeave;
																													}
																													break;
																												}
																											}
																											if(!leaveFlag){
																												msg=msg+"/ No paid type leave is assigned to employee.";
																											}
																											
																											//Early leave/Half day leave condition
																											if(halfDayLeave>0){
																												halfDayLeave=halfDayLeave/empInfo.getLeaveCalendar().getWorkingHours();
																												if(afterDeductionLeaveBalance>0){
																													if(halfDayLeave>afterDeductionLeaveBalance){
																														msg=msg+"/"+(halfDayLeave-afterDeductionLeaveBalance)+"  half day leave will be considered as non paid leave as no paid leave is balanced.";
																													}
																												}else{
																													msg=msg+"/ All half day leave will be considered as non paid leave as no paid leave is balanced.";
																												}
																											}
																										}
																										/** date 05-07-2019 added by komal for woven fabric(short name wise leave deduction)**/ 
																										if(leaveTotalMap.size() > 0){
																											
																													for(Map.Entry<String, Double> entry : leaveTotalMap.entrySet()){
																														boolean leaveFlag=false;
																														double afterDeductionLeaveBalance = 0;
																														Console.log("leave type key :"+  entry.getKey());
																														for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
																															
																															if((leave.getShortName().equalsIgnoreCase(entry.getKey()) && leave.getPaidLeave()==true)
																																	&&
																																	(!leave.getShortName().equalsIgnoreCase("WO")||!leave.getShortName().equalsIgnoreCase("W/O"))){
																																Console.log("leave type key :"+  entry.getValue());
																																leaveFlag=true;
																																if(entry.getValue()>leave.getBalance()){
																																	msg=msg+"/ Total "+ leave.getName()  +" leave balance is  "+leave.getBalance()+" only and requested leave is "+entry.getValue();
																																}
																																break;
																															}
																														}
																														if(!leaveFlag){
																															msg=msg+"/ No "+ entry.getKey() +" type leave is assigned to employee.";
																														}
																												}
																											
																										}
																										/**
																										 * end komal
																										 */
																									}else{
																										msg=msg+"/ No leave balance is defined against employee. ";
																									}
																									
																									
																									if(totalExtraHours>0){
																										if(projectList!=null&&projectList.size()!=0){
																											HrProject project=getProjectName(info.getProjName(),projectList);
																											if(project!=null){
																												ArrayList<Overtime>otList=new ArrayList<Overtime>();
																												/**
																												 * @author Anil
																												 * @since 28-08-2020
																												 * Relievers OT
																												 */
																												String designation=null;
																												
																												/**
																												 * @author Anil @since 13-04-2021
																												 * Updated logic of Reliever ot validation 
																												 * Raised by Rahul Tiwari for sasha
																												 */
																												if(empInfo.isReliever()){
																													if(project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
//																														otList=project.getRelieversOtList();
																														otList.addAll(project.getRelieversOtList());
																														designation=info.getEmpDesignation();
																													}
																												}
																												
																												/**
																												 * @author Vijay Date :- 08-09-2022
																												 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
																												 */
																												ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
																												overtimelist = getHRProjectOvertimelist(project,HrProjectOvertimelist);
																												/**
																												 * ends here
																												 */
//																												if(project.getOtList()!=null&&project.getOtList().size()!=0){
////																													otList=project.getOtList();
//																													otList.addAll(project.getOtList());
//																												}
																												if(overtimelist!=null&&overtimelist.size()!=0){
																													otList.addAll(overtimelist);
																												}
																												
																												if(otList.size()!=0){
																													Console.log("woOt: "+ woOt+" hOt "+hOt+" nOt "+nOt+" phOt "+phOt+" nhOt "+nhOt);
																													Console.log("Designation : "+designation+" Reliever "+empInfo.isReliever());
																													
																													if(woOt>0&&!project.isRotationalWeeklyOff()){
																														Overtime ot=getOvertimeName(otList,"WO",info.getEmpId(),designation);
																														if(ot!=null){
																															Console.log("WOT "+ot.getName());
																														}else{
																															msg=msg+"/ Weekly off OT is not added in project for employee "+info.getEmpName()+". ";
																														}
																													}
																													if(hOt>0){
																														Overtime ot=getOvertimeName(otList,"H",info.getEmpId(),designation);
																														if(ot!=null){
																															Console.log("WOT "+"HOT "+ot.getName());

																															/**
																															 * Date : 27-08-2018 By ANIL
																															 */
																															if(ot.isLeave()==true){
																																boolean leaveFlag=false;
																																for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
																																	if(leave.getName().equals(ot.getLeaveName())){
																																		leaveFlag=true;
																																		break;
																																	}
																																}
																																if(!leaveFlag){
																																	msg=msg+"/ No Leave - "+ot.getLeaveName()+" is assigned to employee.";
																																}
																															}
																															/**
																															 * End
																															 */
																														}else{
																															msg=msg+"/ Holiday OT is not added in Project. ";
																														}
																													}
																													if(nOt>0){
																														Overtime ot=getOvertimeName(otList,"N",info.getEmpId(),designation);
																														if(ot!=null){
																															Console.log("NOT "+ot.getName());
																														}else{
																															msg=msg+"/ OT is not added in Project. ";
																														}
																													}
																													
																													if(phOt>0){
																														Overtime ot=getOvertimeName(otList,"PH",info.getEmpId(),designation);
																														Console.log("phOt :: "+ot);

																														if(ot!=null){
																															Console.log("PHOT "+ot.getName());
																														}else{
																															msg=msg+"/ PH OT is not added in Project. ";
																														}
																													}
																													
																													if(nhOt>0){
																														Overtime ot=getOvertimeName(otList,"NH",info.getEmpId(),designation);
																														if(ot!=null){
																															Console.log("NHOT "+ot.getName());

																														}else{
																															msg=msg+"/ NH OT is not added in Project. ";
																														}
																													}
																												}else{
																													msg=msg+"/ Please add OT details in Project. ";
																												}
																											}else{
																												msg=msg+"/ Project name is not matched in DB. ";
																											}
																											
																										}else{
																											msg=msg+"/ Project name is not matched in DB. ";
																										}
																									}
																									
																									

																									
																									
																									
																									/**
																									 * @author Anil , Date : 09-05-2019
																									 * validating ot and dot details
																									 */
																									if(info.isOtDotFlag()==true){
																										HrProject project=getProjectName(info.getProjName(),projectList);
																										ArrayList<Overtime>otList=new ArrayList<Overtime>();
//																										if(project!=null&&project.getOtList()!=null){
//																											otList=project.getOtList();
//																										}
																										
																										/**
																										 * @author Anil
																										 * @since 28-08-2020
																										 * Relievers OT
																										 */
																										String designation=null;
																										/**
																										 * @author Anil @since 13-04-2021
																										 * Updated logic of Reliever ot validation 
																										 * Raised by Rahul Tiwari for sasha
																										 */
																										
//																										if(empInfo.isReliever()){
//																											if(project!=null&&project.getRelieversOtList()!=null){
//																												otList=project.getRelieversOtList();
//																												designation=info.getEmpDesignation();
//																											}
//																										}else{
//																											if(project!=null&&project.getOtList()!=null){
//																												otList=project.getOtList();
//																											}
//																										}
																										
																										if(empInfo.isReliever()){
																											if(project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
//																												otList=project.getRelieversOtList();
																												otList.addAll(project.getRelieversOtList());
																												designation=info.getEmpDesignation();
																											}
																										}
																										
//																										if(project.getOtList()!=null&&project.getOtList().size()!=0){
////																											otList=project.getOtList();
//																											otList.addAll(project.getOtList());
//																										}
																										/**
																										 * @author Vijay Date :- 08-09-2022
																										 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
																										 */
																										ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
																										overtimelist = getHRProjectOvertimelist(project,HrProjectOvertimelist);
																										
																										if(overtimelist!=null&&overtimelist.size()!=0){
																											otList.addAll(overtimelist);
																										}
																										
																										/**
																										 * ends here
																										 */
																										
																										
																										
																										Console.log("OT?DOT :: ");
																										
																										double singleOt=0;
																										double doubleOt=0;
																										if(info.getSingleOt()!=null&&!info.getSingleOt().equalsIgnoreCase("NA")){
																											try{
																												
																												singleOt=Double.parseDouble(info.getSingleOt());
																												if(singleOt!=0){
																													Overtime ot=getOvertimeName(otList,"OT",info.getEmpId(),designation);
																													if(ot==null){
																														msg=msg+"/ Single OT is not added in Project. ";
																													}
																												}
																											}catch(Exception e){
																												msg=msg+"/ Please add proper value against single ot column. ";
																											}
																										}
																										
																										if(info.getDoubleOt()!=null&&!info.getDoubleOt().equalsIgnoreCase("NA")){
																											try{
																												doubleOt=Double.parseDouble(info.getDoubleOt());
																												if(doubleOt!=0){
																													Overtime ot=getOvertimeName(otList,"DOT",info.getEmpId(),designation);
																													if(ot==null){
																														msg=msg+"/ Double OT is not added in Project. ";
																													}
																												}
																											}catch(Exception e){
																												msg=msg+"/ Please add proper value against double ot column. ";
																											}
																										}
																										
																										Console.log("SL "+singleOt+" DL "+doubleOt);
																									}
																									
																									
																									
																									Console.log("msg"+msg);

																									if(msg!=null && !msg.trim().equals("")) {
																										
																										error.setEmpId(info.getEmpId());
																										error.setEmpName(info.getEmpName());
																										error.setProjectName(info.getProjName());
																										
																										error.setTotalNoOfDays(totalNoOfDays);
																										error.setTotalNoOfPresentDays(totalNoOfPresentDays);
																										error.setTotalNoOfWO(totalNoWeeklyOff);
																										error.setTotalNoOfHoliday(totalNoOfHoliday);
																										error.setTotalNoOfAbsentDays(totalNoOfAbsentDays);
																										
																										if(empInfo!=null&&!calendarFlag){
																										double leaveDays=totalLeave;
																										double extraDays=totalExtraHours/empInfo.getLeaveCalendar().getWorkingHours();
																										error.setTotalNoOfLeave(leaveDays);
																										error.setTotalNoOfExtraDays(extraDays);
																										}
																										
																										error.setRemark(msg);
																										errorList.add(error);

																									}else{
																										Console.log("info.getEmpId()"+info.getEmpId()+" totalNoOfDays="+totalNoOfDays+" totalNoOfPresentDays"+totalNoOfPresentDays+" totalNoWeeklyOff="+totalNoWeeklyOff);
																									}
																										
																								}		
																								
																								Console.log("Validation part completed here");
																								if(errorList.size()!=0) {
																									Console.log("There is error in excel");
																									
																									csvservice.setAttandanceError(errorList, new AsyncCallback<Void>() {
																										
																										@Override
																										public void onSuccess(Void arg0) {
																											// TODO Auto-generated method stub
																											form.showDialogMessage("Please find attendance error csv file.");
																											form.hideWaitSymbol();
																											Console.log("1");
																											String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
																											Console.log("2");
																											final String url = gwt + "csvservlet" + "?type=" + 151;
																											Console.log("3");
																											Window.open(url, "test", "enabled");
																											Console.log("4");
																											
																										}
																										
																										@Override
																										public void onFailure(Throwable arg0) {
																											// TODO Auto-generated method stub
																											form.hideWaitSymbol();
																										}
																									});
																									
																								}
																								else {
																									Console.log("calling task queue for uploading all data");
																									
																									datamigAsync.updateAttandance(attandanceList, new AsyncCallback<ArrayList<AttandanceInfo>>() {
																										@Override
																										public void onFailure(Throwable caught) {
																											form.hideWaitSymbol();
																										}
											
																										@Override
																										public void onSuccess(ArrayList<AttandanceInfo> result) {
																											form.hideWaitSymbol();
																											form.showDialogMessage("Attendance upload process started.");
																											
																										}
																									});
																								}
																								
																							}
																							
																							@Override
																							public void onFailure(Throwable caught) {
																								// TODO Auto-generated method stub
																								
																							}
																						});
																					}
																					
																					@Override
																					public void onFailure(Throwable caught) {
																						// TODO Auto-generated method stub
																						
																						Console.log("loadExistingAttendance failure");
																					}
																				});
																			}
																			
																			@Override
																			public void onFailure(Throwable caught) {
																				// TODO Auto-generated method stub
																				
																			}
																		});
																	}
																	
																	
																	@Override
																	public void onFailure(Throwable caught) {
																		// TODO Auto-generated method stub
																		
																	}
																});
																
																
															}
															
															@Override
															public void onFailure(Throwable caught) {
																// TODO Auto-generated method stub
																
															}
														});
														
//														datamigAsync.validateAttandancepart2(attandanceList, new AsyncCallback<ArrayList<SuperModel>>() {
//															
//															@Override
//															public void onSuccess(ArrayList<SuperModel> allloadeddatalist) {
//																// TODO Auto-generated method stub
//																
//																final List<LeaveBalance>empLeaveBalList= new ArrayList<LeaveBalance>();
//																final List<EmployeeInfo> empInfoList = new ArrayList<EmployeeInfo>();
//																final List<HrProject>projectList = new ArrayList<HrProject>();
//																
//																
//																ArrayList<Integer> projectidlist = new ArrayList<Integer>();
//
//																for(SuperModel model : allloadeddatalist) {
//																	if(model.getDocumentName()!=null) {
//																		if(model.getDocumentName().equals("LeaveBalance")) {
//																			LeaveBalance leavebalance = (LeaveBalance) model;
//																			empLeaveBalList.add(leavebalance);
//																		}
//																		if(model.getDocumentName().equals("EmployeeInfo")) {
//																			EmployeeInfo employeeinfo = (EmployeeInfo) model;
//																			empInfoList.add(employeeinfo);
//																		}
//																		if(model.getDocumentName().equals("HrProject")) {
//																			HrProject herpoject = (HrProject) model;
//																			projectList.add(herpoject);
//																			projectidlist.add(herpoject.getCount());
//																		}
////																		if(model.getDocumentName().equals("ExistingAttendance")) {
////																			Attendance attendance = (Attendance) model;
////																			existingAttendanceList.add(attendance);
////																		}
////																		if(model.getDocumentName().equals("HrProjectOvertime")) {
////																			HrProjectOvertime hrprojectovertime = (HrProjectOvertime) model;
////																			HrProjectOvertimelist.add(hrprojectovertime);
////																		}
//																		if(model.getDocumentName().equals("ERROR")) {
//																			Console.log("Error at server side while data loading");
//																			return;
//																		}
//																	}
//																}
//																Console.log("Data loading 1st RPC done here");
//																
//																
//																
//
//																
//																HashSet<Integer> hsEmpId=new HashSet<Integer>();
//																for(AttandanceInfo info:attandanceList){
//																	hsEmpId.add(info.getEmpId());
//																}
//																ArrayList<Integer> empList=new ArrayList<Integer>(hsEmpId);
//
//
//																datamigAsync.validateAttandancepart3(attandanceList, empList, projectidlist, new AsyncCallback<ArrayList<SuperModel>>() {
//																	
//																	@Override
//																	public void onSuccess(ArrayList<SuperModel> result) {
//																		// TODO Auto-generated method stub
//																		
//																		List<Attendance> existingAttendanceList = new ArrayList<Attendance>();
//																		ArrayList<HrProjectOvertime> HrProjectOvertimelist = new ArrayList<HrProjectOvertime>();
//																		
//																		
//																		for(SuperModel model : result) {
//																			if(model.getDocumentName()!=null) {
//																				if(model.getDocumentName().equals("ExistingAttendance")) {
//																					Attendance attendance = (Attendance) model;
//																					existingAttendanceList.add(attendance);
//																				}
//																				if(model.getDocumentName().equals("HrProjectOvertime")) {
//																					HrProjectOvertime hrprojectovertime = (HrProjectOvertime) model;
//																					HrProjectOvertimelist.add(hrprojectovertime);
//																				}
//																				if(model.getDocumentName().equals("ERROR")) {
//																					Console.log("Error at server side while data loading");
//																					return;
//																				}
//																			}
//																		}
//																		
//																		Console.log("Data loading 2st RPC done here");
//																		
//
//																		/**
//																		 * @author Anil,Date :24-01-2019
//																		 * requirement is instead of putting working hours ,wanted to put label as 'P','HD'
//																		 * For Orion By Sonu
//																		 */
//																		boolean labelAsInputFlag=false;
//																		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "AttendanceLabelAsInput")){
//																			labelAsInputFlag=true;
//																		}
//																		Console.log("AttendanceLabelAsInput :: "+labelAsInputFlag+" OT/DOT FLAG "+attandanceList.get(0).isOtDotFlag());
//																		
//																		DateTimeFormat fmt = DateTimeFormat.getFormat("MM/dd/yyyy");
//																		ArrayList<AttandanceError>errorList=new ArrayList<AttandanceError>();
//
//																		
//																		for(AttandanceInfo info:attandanceList){
//																			/**
//																			 * Date : 23-07-2018 BY ANIL
//																			 */
//																			boolean calendarFlag=false;
//
//																			double totalNoOfDays = 0;
//																			double totalNoOfPresentDays = 0;
//																			double totalNoWeeklyOff=0;
//																			double totalNoOfHoliday=0;
//																			double totalNoOfAbsentDays=0;
//																			double totalExtraHours=0;
//																			/**
//																			 * date :16-12-2018 By ANIL
//																			 * Changed the variable name form totalLeaveHours to totalLeave
//																			 */
//																			double totalLeave=0;
//																			
////																			double woAsLeave=0;
//																			double halfDayLeave=0;
//																			
//																			double woOt=0;
//																			double hOt=0;
//																			double nOt=0;
//																			double nhOt=0;
//																			double phOt=0;
//																			
//																			AttandanceError error=new AttandanceError();
//																			HashMap<String , Double> leaveTotalMap = new HashMap<String , Double>();
//																			
//																			
//																			Comparator<AttandanceDetails> attandanceComparator = new  Comparator<AttandanceDetails>() {
//																				@Override
//																				public int compare(AttandanceDetails o1, AttandanceDetails o2) {
//																					return o1.getDate().compareTo(o2.getDate());
//																				}
//																			};
//																			Collections.sort(info.getAttandList(),attandanceComparator);
//																			
//																			LeaveBalance leaveBalance=null;
//																			if(empLeaveBalList!=null&&empLeaveBalList.size()!=0){
//																				leaveBalance=getEmpLeaveBalance(info.getEmpId(),empLeaveBalList);
//																			}
//																			
//																			EmployeeInfo empInfo=validateEmpId(info.getEmpId(), empInfoList);
//																			
//
//																			/**
//																			 * @author Anil
//																			 * @since 29-01-2021
//																			 * Commenting above logic of validating existing attendance
//																			 * As per the new logic of user is trying to upload attendance of same date which is uploaded then only we restrict to upload
//																			 * earlier if any day attendance was found then he to delete and re upload attendance
//																			 * Raised by Rahul For Sunrise
//																			 */
//																			ArrayList<String> existingAttendanceListDtWise=new ArrayList<String>();
//																			try {
//																			Label:	for(Attendance attend:existingAttendanceList){
//																						if(info.getEmpId()==attend.getEmpId()){
//																							existingAttendanceListDtWise.add(info.getEmpId()+""+fmt.format(attend.getAttendanceDate()));
//																						}
//																					}
//																			} catch (Exception e) {
//																			}
//																			
//																			/**
//																			 * @author Anil , Date :04-05-2019
//																			 * Validating employee attendance overlapping
//																			 * if user is marking attendance for same day but for different project
//																			 */
//																			ArrayList<String> empIdListAsPerdate=new ArrayList<String>();
//
//																			String msg="";
//																			for(AttandanceDetails attDet:info.getAttandList()){
//																				/**
//																				 * @author Anil , Date : 07-05-2019
//																				 * If attendance is marked as OP then we will not consider that attendance for update
//																				 * raised by Nitin Sir,Sonu
//																				 */
//																				if(attDet.getValue().equalsIgnoreCase("OP")){
//																					Console.log("AttendanceLabelAsInput  :: "+attDet.getValue());
//																					continue;
//																				}
//																				
//																				
//																				/**
//																				 * @author Anil
//																				 */
//																				
//																				if(labelAsInputFlag&&attDet.getValue().equalsIgnoreCase("P")){
////																					logger.log(Level.SEVERE,"AttendanceLabelAsInput P :: "+empInfo.getLeaveCalendar().getWorkingHours());
//																					attDet.setValue(empInfo.getLeaveCalendar().getWorkingHours()+"");
//																				}else if(labelAsInputFlag&&attDet.getValue().equalsIgnoreCase("HD")){
////																					logger.log(Level.SEVERE,"AttendanceLabelAsInput HD :: "+empInfo.getLeaveCalendar().getWorkingHours()/2);
//																					attDet.setValue(empInfo.getLeaveCalendar().getWorkingHours()/2+"");
//																				}
//																				
//																				/**
//																				 * 
//																				 */
//																				if(!calendarFlag){
//																				totalNoOfDays++;
//																				
//																				if(empInfo!=null){
//																					if(empInfo.getDateOfJoining()!=null){
//																						empInfo.getDateOfJoining().setHours(0);
//																						empInfo.getDateOfJoining().setMinutes(0);
//																						empInfo.getDateOfJoining().setSeconds(0);
//																					}
//																					if(empInfo.getLastWorkingDate()!=null){
//																						empInfo.getLastWorkingDate().setHours(0);
//																						empInfo.getLastWorkingDate().setMinutes(0);
//																						empInfo.getLastWorkingDate().setSeconds(0);
//																					}
//																				}
//																				
//																				if(attDet.getDate().before(empInfo.getDateOfJoining())
//																						&&!attDet.getValue().trim().equalsIgnoreCase("NA")){
//																					System.out.println("ATT DT : "+attDet.getDate()+" JOINING DT : "+empInfo.getDateOfJoining()+" VALUE : "+attDet.getValue());
//																					msg=msg+" / Attendance should not be before date of joining "+fmt.format(empInfo.getDateOfJoining());
//																					break;
//																				}
//																				
//																				if(empInfo.getLastWorkingDate()!=null&&attDet.getDate().after(empInfo.getLastWorkingDate())
//																						&&!attDet.getValue().equalsIgnoreCase("NA")){
//																					System.out.println("ATT DT : "+attDet.getDate()+" LAST WRKING DT : "+empInfo.getLastWorkingDate()+" VALUE : "+attDet.getValue());
//																					msg=msg+" / Attendance should not be after last date of working "+fmt.format(empInfo.getLastWorkingDate());
//																					break;
//																				}
//																				
//																				if(attDet.getValue().trim().equalsIgnoreCase("END")){
//																					break ;
//																				}
//																				
////																				if(!calendarFlag){
//																					try{
//																						
//																						
//																						double workedHrs=Double.parseDouble(attDet.getValue().trim());
//																						WeekleyOff woff = empInfo.getLeaveCalendar().getWeeklyoff();
//																						boolean isWoff = isWeeklyOff(woff,attDet.getDate());
//																						boolean isHday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
//																						
//																						/**
//																						 * @author Anil
//																						 * @since 13-07-2020
//																						 */
//																						String holidayType=empInfo.getLeaveCalendar().getHolidayType(attDet.getDate());
//																						String holidayLabel="H";
//																						if(holidayType!=null){
//																							Console.log(attDet.getDate()+ " holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
//																							if(holidayType.equalsIgnoreCase("National Holiday")){
//																								holidayLabel="NH";
//																							}else if(holidayType.equalsIgnoreCase("Public Holiday")){
//																								holidayLabel="PH";
//																							}
//																						}
//																						if(workedHrs==empInfo.getLeaveCalendar().getWorkingHours()){
//																							totalNoOfPresentDays++;
//																							
//																							if(isWoff){
//																								woOt=woOt+workedHrs;
//																								totalExtraHours+=workedHrs;
//																							}else if(isHday){
//																								hOt=hOt+workedHrs;
//																								/**
//																								 * Date : 17-08-2018 By ANIL
//																								 * OT ERROR
//																								 */
//																								totalExtraHours+=workedHrs;
//																								if(holidayLabel.equals("NH")){
//																									nhOt=nhOt+workedHrs;
//																								}else if(holidayLabel.equals("PH")){
//																									phOt=phOt+workedHrs;
//																								}
//																								Console.log("--holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
//																							}
//																						}else if(workedHrs>empInfo.getLeaveCalendar().getWorkingHours()){
//																							double extraHrs=workedHrs-empInfo.getLeaveCalendar().getWorkingHours();
//																							totalExtraHours=totalExtraHours+extraHrs;
//																							totalNoOfPresentDays++;
//																							
//																							WeekleyOff wo = empInfo.getLeaveCalendar().getWeeklyoff();
//																							boolean isWo = isWeeklyOff(wo,attDet.getDate());
//																							boolean isHoliday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
//																							if(isWo){
//																								woOt=woOt+extraHrs;
//																							}else if(isHoliday){
//																								hOt=hOt+extraHrs;
//																							}else{
//																								nOt=nOt+extraHrs;
//																							}
//																						}else if(workedHrs<empInfo.getLeaveCalendar().getWorkingHours()){
//																							double leavaeHrs= empInfo.getLeaveCalendar().getWorkingHours()-workedHrs;
////																							halfDayLeave=halfDayLeave+leavaeHrs;
//																							totalNoOfPresentDays++;
//																							
//																							if(isWoff){
//																								woOt=woOt+leavaeHrs;
//																								/**
//																								 * Date : 17-08-2018 By ANIL
//																								 * OT ERROR
//																								 */
//																								totalExtraHours+=workedHrs;
//																							}else if(isHday){
//																								hOt=hOt+leavaeHrs;
//																								/**
//																								 * Date : 17-08-2018 By ANIL
//																								 * OT ERROR
//																								 */
//																								totalExtraHours+=workedHrs;
//																								
//																								if(holidayType.equals("NH")){
//																									nhOt=nhOt+workedHrs;
//																								}else if(holidayType.equals("PH")){
//																									phOt=phOt+workedHrs;
//																								}
//																							}
//																							/**
//																							 * @author Anil , Date : 25-06-2019
//																							 * will update half day leave if and only if its not on weekly off and holiday
//																							 */
//																							else{
//																								halfDayLeave=halfDayLeave+leavaeHrs;
//																							}
//																						}
//																						
//																						
//																						
//																						/**
//																						 * @author Anil, Date :04-05-2019
//																						 */
//																						String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//																						if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//																							if(empIdListAsPerdate.contains(empId_Date_Key)){
//																								msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//																							}
//																							empIdListAsPerdate.add(empId_Date_Key);
//																						}else{
//																							empIdListAsPerdate.add(empId_Date_Key);
//																						}
//																						
//																						/**
//																						 * @author Anil @since 29-01-2021
//																						 */
//																						if(existingAttendanceListDtWise.size()!=0){
//																							if(existingAttendanceListDtWise.contains(empInfo.getEmpCount()+""+fmt.format(attDet.getDate()))){
////																								error.setEmpId(info.getEmpId());
////																								error.setEmpName(info.getEmpName());
////																								error.setProjectName(info.getProjName());
////																								error.setRemark("Attendance is already uploaded on "+fmt.format(attDet.getDate()));
////																								errorList.add(error);
////																								attandanceList.get(0).setErrorCode(-1);
//																								
//																								msg=msg+"Attendance is already uploaded on "+fmt.format(attDet.getDate());
//																							}
//																						}
//																						
//																						
//																						
//																					}catch(NumberFormatException exception){
//																						
//																						if(attDet.getValue().trim().equalsIgnoreCase("WO")||attDet.getValue().trim().equalsIgnoreCase("W/O")){
//																							WeekleyOff wo = empInfo.getLeaveCalendar().getWeeklyoff();
//																							boolean isWo = isWeeklyOff(wo,attDet.getDate());
//																							/**
//																							 * @author Anil , Date : 20-08-2019
//																							 */
//																							boolean isWoDefinedInCal=isWeeklyOffDefined(wo);
//																							System.out.println("WO : "+isWo+" Defined : "+isWoDefinedInCal+" Dt : "+attDet.getDate());
//																							if(!isWo){
////																								woAsLeave++;
//																								if(isWoDefinedInCal){
//																									msg=msg+" / Weekly off day is not matched. "+fmt.format(attDet.getDate());
//																								}
//																							}
//																							totalNoWeeklyOff++;
//																							
//																							/**
//																							 * @author Anil, Date :04-05-2019
//																							 */
//																							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//																							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//																								if(empIdListAsPerdate.contains(empId_Date_Key)){
//																									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//																								}
//																								empIdListAsPerdate.add(empId_Date_Key);
//																							}else{
//																								empIdListAsPerdate.add(empId_Date_Key);
//																							}
//																							
//																						}else if(attDet.getValue().trim().equalsIgnoreCase("H")){
//																							boolean isHoliday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
//																							if(!isHoliday){
//																								msg=msg+" / Holiday date "+attDet.getDate()+" is not matched in Employee Calendar. ";
//																							}
//																							totalNoOfHoliday++;
//																							
//																							/**
//																							 * @author Anil, Date :04-05-2019
//																							 */
//																							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//																							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//																								if(empIdListAsPerdate.contains(empId_Date_Key)){
//																									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//																								}
//																								empIdListAsPerdate.add(empId_Date_Key);
//																							}else{
//																								empIdListAsPerdate.add(empId_Date_Key);
//																							}
//																							
//																							String holidayType=empInfo.getLeaveCalendar().getHolidayType(attDet.getDate());
//																							String holidayLabel="H";
//																							if(holidayType!=null){
//																								Console.log(attDet.getDate()+ "holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
//																								if(holidayType.equalsIgnoreCase("National Holiday")){
//																									holidayLabel="NH";
//																								}else if(holidayType.equalsIgnoreCase("Public Holiday")){
//																									holidayLabel="PH";
//																								}
//																							}
//																							
//																						}else if(attDet.getValue().trim().equalsIgnoreCase("L")){
//																							totalLeave++;
//																							
//																							/**
//																							 * @author Anil, Date :04-05-2019
//																							 */
//																							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//																							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//																								if(empIdListAsPerdate.contains(empId_Date_Key)){
//																									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//																								}
//																								empIdListAsPerdate.add(empId_Date_Key);
//																							}else{
//																								empIdListAsPerdate.add(empId_Date_Key);
//																							}
//																							
//																						}else if(attDet.getValue().trim().equalsIgnoreCase("A")){
//																							totalNoOfAbsentDays++;
//																							
//																							/**
//																							 * @author Anil, Date :04-05-2019
//																							 */
//																							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//																							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//																								if(empIdListAsPerdate.contains(empId_Date_Key)){
//																									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//																								}
//																								empIdListAsPerdate.add(empId_Date_Key);
//																							}else{
//																								empIdListAsPerdate.add(empId_Date_Key);
//																							}
//																						}else if(attDet.getValue().trim().equalsIgnoreCase("NA")){
//																							if(empInfo.getLastWorkingDate()!=null){
//																								if(attDet.getDate().after(empInfo.getDateOfJoining())
//																										&&attDet.getDate().before(empInfo.getLastWorkingDate())){
//																									msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
//																								}
//																								
//																							}else{
//																								if(attDet.getDate().after(empInfo.getDateOfJoining())||attDet.getDate().equals(empInfo.getDateOfJoining())){
//																									msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
//																								}
//																							}
//																							
////																							if(empInfo.getLastWorkingDate()!=null&&!attDet.getDate().before(empInfo.getLastWorkingDate())){
////																								msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
////																								attandanceList.get(0).setErrorCode(-1);
////																							}
//																							
//																						}
//																						/**
//																						 * @author Anil
//																						 * @since 10-07-2020
//																						 * Adding condition for public holiday keyword
//																						 */
//																						else if(attDet.getValue().trim().contains("PH")&&validPH_OT(attDet.getValue())){
//																							
//																							WeekleyOff woff = empInfo.getLeaveCalendar().getWeeklyoff();
//																							boolean isWoff = isWeeklyOff(woff,attDet.getDate());
//																							boolean isHday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
//																							
//																							if(isWoff){
//																								msg=msg+" / Can not mark attendance as PH on weekly off. "+attDet.getDate()+". ";
//																							}else if(isHday){
//																								msg=msg+" / Can not mark attendance as PH on holiday. "+attDet.getDate()+". ";
//																							}
//																							
//																							totalNoOfHoliday++;
//																							double phOtt=getTotalPhOtHours(attDet.getValue());
//																							totalExtraHours=totalExtraHours+phOtt;
//																							phOt+=phOtt;
//																							
//																							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//																							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//																								if(empIdListAsPerdate.contains(empId_Date_Key)){
//																									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//																								}
//																								empIdListAsPerdate.add(empId_Date_Key);
//																							}else{
//																								empIdListAsPerdate.add(empId_Date_Key);
//																							}
//																							
//																						}else{
//																							/** date 05-07-2019 added by komal for woven fabric(short name wise leave deduction)**/
//																							double value = 1;
//																							if(leaveTotalMap.containsKey(attDet.getValue().trim())){
//																								value = leaveTotalMap.get(attDet.getValue().trim());
//																								leaveTotalMap.put(attDet.getValue().trim(), value + 1);
//																							}else{
//																								leaveTotalMap.put(attDet.getValue().trim(), value);
//																							}
//																								String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//																								if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//																									if(empIdListAsPerdate.contains(empId_Date_Key)){
//																										msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//																									}
//																									empIdListAsPerdate.add(empId_Date_Key);
//																								}else{
//																									empIdListAsPerdate.add(empId_Date_Key);
//																								}
////																							msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
////																							attandanceList.get(0).setErrorCode(-1);
//																						}
//																						
//																						
//																						/**
//																						 * @author Anil @since 29-01-2021
//																						 */
//																						if(!attDet.getValue().trim().equalsIgnoreCase("NA")&&existingAttendanceListDtWise.size()!=0){
//																							if(existingAttendanceListDtWise.contains(empInfo.getEmpCount()+""+fmt.format(attDet.getDate()))){
////																								error.setEmpId(info.getEmpId());
////																								error.setEmpName(info.getEmpName());
////																								error.setProjectName(info.getProjName());
////																								error.setRemark("Attendance is already uploaded on "+fmt.format(attDet.getDate()));
////																								errorList.add(error);
////																								attandanceList.get(0).setErrorCode(-1);
//																								
//																								msg=msg+"Attendance is already uploaded on "+fmt.format(attDet.getDate());
//																							}
//																						}
//																					}
//																					
//																					/**
//																					 * Date : 18-08-2018 By ANIl
//																					 * @author Anil
//																					 * @since 26-11-2020
//																					 * removing overriding functionality
//																					 */
////																					Attendance attendance=getExistingAttendance(info.getEmpId(),existingAttendanceList,attDet.getDate());
////																					if(attendance!=null){
////																						attenListToBeRemoved.add(attendance);
////																					}
////																					
////																					EmployeeLeave empLeave=getExistingLeave(info.getEmpId(),approvedLeaveList,attDet.getDate());
////																					if(empLeave!=null){
////																						leaveListTobeRemoved.add(empLeave);
////																						updateLeaveBalance(leaveBalance,empLeave,empInfo);
////																					}
////																					
////																					EmployeeOvertime empOt=getExistingOt(info.getEmpId(),approvedOtList,attDet.getDate());
////																					if(empOt!=null){
////																						otListToBeRemoved.add(empOt);
////																					}
//																				
//																			}
//																			}
//																			
//																			
//																			if(leaveBalance!=null){
//																				
//																				/**
//																				 * @author Anil , Date : 20-08-2019
//																				 * We will not maintain WO as leave, will just mark attendance as weekly off
//																				 * Suggested by Nitin Sir
//																				 */
//																				//WO as Leave condition
////																				if(woAsLeave>0){
////																					boolean woFlag=false;
////																					for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
////																						if(leave.getShortName().equalsIgnoreCase("WO")||leave.getShortName().equalsIgnoreCase("W/O")){
////																							woFlag=true;
////																							if(woAsLeave>leave.getBalance()){
////																								msg=msg+"/ Total WO leave balance is  "+leave.getBalance()+" only and requested WO leave is "+woAsLeave;
////																								attandanceList.get(0).setErrorCode(-1);
////																							}
////																							break;
////																						}
////																					}
////																					if(!woFlag){
////																						msg=msg+"/ No WO type leave is assigned to employee.";
////																						attandanceList.get(0).setErrorCode(-1);
////																					}
////																				}
//																				
//																				/**
//																				 * End
//																				 */
//																				
//																				//Total Paid Leave Condition
//																				if(totalLeave>0){
//																					boolean leaveFlag=false;
//																					double afterDeductionLeaveBalance=0;
//																					for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
////																						if(leave.getPaidLeave()==true&&
////																								!leave.getShortName().equalsIgnoreCase("WO")||!leave.getShortName().equalsIgnoreCase("W/O")){
//																						if(leave.getPaidLeave()==true&&leave.getShortName().contains("PL")){
//																							leaveFlag=true;
//																							if(totalLeave>leave.getBalance()){
//																								msg=msg+"/ Total paid leave balance is  "+leave.getBalance()+" only and requested leave is "+totalLeave;
//																							}else{
//																								afterDeductionLeaveBalance=leave.getBalance()-totalLeave;
//																							}
//																							break;
//																						}
//																					}
//																					if(!leaveFlag){
//																						msg=msg+"/ No paid type leave is assigned to employee.";
//																					}
//																					
//																					//Early leave/Half day leave condition
//																					if(halfDayLeave>0){
//																						halfDayLeave=halfDayLeave/empInfo.getLeaveCalendar().getWorkingHours();
//																						if(afterDeductionLeaveBalance>0){
//																							if(halfDayLeave>afterDeductionLeaveBalance){
//																								msg=msg+"/"+(halfDayLeave-afterDeductionLeaveBalance)+"  half day leave will be considered as non paid leave as no paid leave is balanced.";
//																							}
//																						}else{
//																							msg=msg+"/ All half day leave will be considered as non paid leave as no paid leave is balanced.";
//																						}
//																					}
//																				}
//																				/** date 05-07-2019 added by komal for woven fabric(short name wise leave deduction)**/ 
//																				if(leaveTotalMap.size() > 0){
//																					
//																							for(Map.Entry<String, Double> entry : leaveTotalMap.entrySet()){
//																								boolean leaveFlag=false;
//																								double afterDeductionLeaveBalance = 0;
//																								Console.log("leave type key :"+  entry.getKey());
//																								for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
//																									
//																									if((leave.getShortName().equalsIgnoreCase(entry.getKey()) && leave.getPaidLeave()==true)
//																											&&
//																											(!leave.getShortName().equalsIgnoreCase("WO")||!leave.getShortName().equalsIgnoreCase("W/O"))){
//																										Console.log("leave type key :"+  entry.getValue());
//																										leaveFlag=true;
//																										if(entry.getValue()>leave.getBalance()){
//																											msg=msg+"/ Total "+ leave.getName()  +" leave balance is  "+leave.getBalance()+" only and requested leave is "+entry.getValue();
//																										}
//																										break;
//																									}
//																								}
//																								if(!leaveFlag){
//																									msg=msg+"/ No "+ entry.getKey() +" type leave is assigned to employee.";
//																								}
//																						}
//																					
//																				}
//																				/**
//																				 * end komal
//																				 */
//																			}else{
//																				msg=msg+"/ No leave balance is defined against employee. ";
//																			}
//																			
//																			
//																			if(totalExtraHours>0){
//																				if(projectList!=null&&projectList.size()!=0){
//																					HrProject project=getProjectName(info.getProjName(),projectList);
//																					if(project!=null){
//																						ArrayList<Overtime>otList=new ArrayList<Overtime>();
//																						/**
//																						 * @author Anil
//																						 * @since 28-08-2020
//																						 * Relievers OT
//																						 */
//																						String designation=null;
//																						
//																						/**
//																						 * @author Anil @since 13-04-2021
//																						 * Updated logic of Reliever ot validation 
//																						 * Raised by Rahul Tiwari for sasha
//																						 */
//																						if(empInfo.isReliever()){
//																							if(project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
////																								otList=project.getRelieversOtList();
//																								otList.addAll(project.getRelieversOtList());
//																								designation=info.getEmpDesignation();
//																							}
//																						}
//																						
//																						/**
//																						 * @author Vijay Date :- 08-09-2022
//																						 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
//																						 */
//																						ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
//																						overtimelist = getHRProjectOvertimelist(project,HrProjectOvertimelist);
//																						/**
//																						 * ends here
//																						 */
////																						if(project.getOtList()!=null&&project.getOtList().size()!=0){
//////																							otList=project.getOtList();
////																							otList.addAll(project.getOtList());
////																						}
//																						if(overtimelist!=null&&overtimelist.size()!=0){
//																							otList.addAll(overtimelist);
//																						}
//																						
//																						if(otList.size()!=0){
//																							Console.log("woOt: "+ woOt+" hOt "+hOt+" nOt "+nOt+" phOt "+phOt+" nhOt "+nhOt);
//																							Console.log("Designation : "+designation+" Reliever "+empInfo.isReliever());
//																							
//																							if(woOt>0){
//																								Overtime ot=getOvertimeName(otList,"WO",info.getEmpId(),designation);
//																								if(ot!=null){
//																									Console.log("WOT "+ot.getName());
//																								}else{
//																									msg=msg+"/ Weekly off OT is not added in project for employee "+info.getEmpName()+". ";
//																								}
//																							}
//																							if(hOt>0){
//																								Overtime ot=getOvertimeName(otList,"H",info.getEmpId(),designation);
//																								if(ot!=null){
//																									Console.log("WOT "+"HOT "+ot.getName());
//
//																									/**
//																									 * Date : 27-08-2018 By ANIL
//																									 */
//																									if(ot.isLeave()==true){
//																										boolean leaveFlag=false;
//																										for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
//																											if(leave.getName().equals(ot.getLeaveName())){
//																												leaveFlag=true;
//																												break;
//																											}
//																										}
//																										if(!leaveFlag){
//																											msg=msg+"/ No Leave - "+ot.getLeaveName()+" is assigned to employee.";
//																										}
//																									}
//																									/**
//																									 * End
//																									 */
//																								}else{
//																									msg=msg+"/ Holiday OT is not added in Project. ";
//																								}
//																							}
//																							if(nOt>0){
//																								Overtime ot=getOvertimeName(otList,"N",info.getEmpId(),designation);
//																								if(ot!=null){
//																									Console.log("NOT "+ot.getName());
//																								}else{
//																									msg=msg+"/ OT is not added in Project. ";
//																								}
//																							}
//																							
//																							if(phOt>0){
//																								Overtime ot=getOvertimeName(otList,"PH",info.getEmpId(),designation);
//																								Console.log("phOt :: "+ot);
//
//																								if(ot!=null){
//																									Console.log("PHOT "+ot.getName());
//																								}else{
//																									msg=msg+"/ PH OT is not added in Project. ";
//																								}
//																							}
//																							
//																							if(nhOt>0){
//																								Overtime ot=getOvertimeName(otList,"NH",info.getEmpId(),designation);
//																								if(ot!=null){
//																									Console.log("NHOT "+ot.getName());
//
//																								}else{
//																									msg=msg+"/ NH OT is not added in Project. ";
//																								}
//																							}
//																						}else{
//																							msg=msg+"/ Please add OT details in Project. ";
//																						}
//																					}else{
//																						msg=msg+"/ Project name is not matched in DB. ";
//																					}
//																					
//																				}else{
//																					msg=msg+"/ Project name is not matched in DB. ";
//																				}
//																			}
//																			
//																			
//
//																			
//																			
//																			
//																			/**
//																			 * @author Anil , Date : 09-05-2019
//																			 * validating ot and dot details
//																			 */
//																			if(info.isOtDotFlag()==true){
//																				HrProject project=getProjectName(info.getProjName(),projectList);
//																				ArrayList<Overtime>otList=new ArrayList<Overtime>();
////																				if(project!=null&&project.getOtList()!=null){
////																					otList=project.getOtList();
////																				}
//																				
//																				/**
//																				 * @author Anil
//																				 * @since 28-08-2020
//																				 * Relievers OT
//																				 */
//																				String designation=null;
//																				/**
//																				 * @author Anil @since 13-04-2021
//																				 * Updated logic of Reliever ot validation 
//																				 * Raised by Rahul Tiwari for sasha
//																				 */
//																				
////																				if(empInfo.isReliever()){
////																					if(project!=null&&project.getRelieversOtList()!=null){
////																						otList=project.getRelieversOtList();
////																						designation=info.getEmpDesignation();
////																					}
////																				}else{
////																					if(project!=null&&project.getOtList()!=null){
////																						otList=project.getOtList();
////																					}
////																				}
//																				
//																				if(empInfo.isReliever()){
//																					if(project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
////																						otList=project.getRelieversOtList();
//																						otList.addAll(project.getRelieversOtList());
//																						designation=info.getEmpDesignation();
//																					}
//																				}
//																				
////																				if(project.getOtList()!=null&&project.getOtList().size()!=0){
//////																					otList=project.getOtList();
////																					otList.addAll(project.getOtList());
////																				}
//																				/**
//																				 * @author Vijay Date :- 08-09-2022
//																				 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
//																				 */
//																				ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
//																				overtimelist = getHRProjectOvertimelist(project,HrProjectOvertimelist);
//																				
//																				if(overtimelist!=null&&overtimelist.size()!=0){
//																					otList.addAll(overtimelist);
//																				}
//																				
//																				/**
//																				 * ends here
//																				 */
//																				
//																				
//																				
//																				Console.log("OT?DOT :: ");
//																				
//																				double singleOt=0;
//																				double doubleOt=0;
//																				if(!info.getSingleOt().equalsIgnoreCase("NA")){
//																					try{
//																						singleOt=Double.parseDouble(info.getSingleOt());
//																						if(singleOt!=0){
//																							Overtime ot=getOvertimeName(otList,"OT",info.getEmpId(),designation);
//																							if(ot==null){
//																								msg=msg+"/ Single OT is not added in Project. ";
//																							}
//																						}
//																					}catch(Exception e){
//																						msg=msg+"/ Please add proper value against single ot column. ";
//																					}
//																				}
//																				
//																				if(!info.getDoubleOt().equalsIgnoreCase("NA")){
//																					try{
//																						doubleOt=Double.parseDouble(info.getDoubleOt());
//																						if(doubleOt!=0){
//																							Overtime ot=getOvertimeName(otList,"DOT",info.getEmpId(),designation);
//																							if(ot==null){
//																								msg=msg+"/ Double OT is not added in Project. ";
//																							}
//																						}
//																					}catch(Exception e){
//																						msg=msg+"/ Please add proper value against double ot column. ";
//																					}
//																				}
//																				
//																				Console.log("SL "+singleOt+" DL "+doubleOt);
//																			}
//																			
//																			
//																			
//																			Console.log("msg"+msg);
//
//																			if(msg!=null && !msg.trim().equals("")) {
//																				
//																				error.setEmpId(info.getEmpId());
//																				error.setEmpName(info.getEmpName());
//																				error.setProjectName(info.getProjName());
//																				
//																				error.setTotalNoOfDays(totalNoOfDays);
//																				error.setTotalNoOfPresentDays(totalNoOfPresentDays);
//																				error.setTotalNoOfWO(totalNoWeeklyOff);
//																				error.setTotalNoOfHoliday(totalNoOfHoliday);
//																				error.setTotalNoOfAbsentDays(totalNoOfAbsentDays);
//																				
//																				if(empInfo!=null&&!calendarFlag){
//																				double leaveDays=totalLeave;
//																				double extraDays=totalExtraHours/empInfo.getLeaveCalendar().getWorkingHours();
//																				error.setTotalNoOfLeave(leaveDays);
//																				error.setTotalNoOfExtraDays(extraDays);
//																				}
//																				
//																				error.setRemark(msg);
//																				errorList.add(error);
//
//																			}
//																				
//																		}		
//																		
//																		Console.log("Validation part completed here");
//																		if(errorList.size()!=0) {
//																			Console.log("There is error in excel");
//																			
//																			csvservice.setAttandanceError(errorList, new AsyncCallback<Void>() {
//																				
//																				@Override
//																				public void onSuccess(Void arg0) {
//																					// TODO Auto-generated method stub
//																					form.showDialogMessage("Please find attendance error csv file.");
//																					form.hideWaitSymbol();
//																					Console.log("1");
//																					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
//																					Console.log("2");
//																					final String url = gwt + "csvservlet" + "?type=" + 151;
//																					Console.log("3");
//																					Window.open(url, "test", "enabled");
//																					Console.log("4");
//																					
//																				}
//																				
//																				@Override
//																				public void onFailure(Throwable arg0) {
//																					// TODO Auto-generated method stub
//																					form.hideWaitSymbol();
//																				}
//																			});
//																			
//																		}
//																		else {
//																			Console.log("calling task queue for uploading all data");
//																			
//																			datamigAsync.updateAttandance(attandanceList, new AsyncCallback<ArrayList<AttandanceInfo>>() {
//																				@Override
//																				public void onFailure(Throwable caught) {
//																					form.hideWaitSymbol();
//																				}
//					
//																				@Override
//																				public void onSuccess(ArrayList<AttandanceInfo> result) {
//																					form.hideWaitSymbol();
//																					form.showDialogMessage("Attendance upload process started.");
//																					
//																				}
//																			});
//																		}
//																		
//																		
//																	}
//																	
//																	
//																	@Override
//																	public void onFailure(Throwable arg0) {
//																		// TODO Auto-generated method stub
//																		
//																	}
//																});
//																
//																
//															}
//															
//														
//
//															@Override
//															public void onFailure(Throwable arg0) {
//																// TODO Auto-generated method stub
//																form.hideWaitSymbol();
//
//															}
//														});
															
														
														
													}else{
														form.hideWaitSymbol();
														form.showDialogMessage("Please find attendance error csv file.");
														
														String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
														final String url = gwt + "csvservlet" + "?type=" + 151;
														Window.open(url, "test", "enabled");
													}
													
													
												}
											});
										}
										else{
											form.hideWaitSymbol();
											form.showDialogMessage("Attendance uploaded Sucessfully");
											form.readerComposite.setValue(new DocumentUpload());
										}
									}
								}

							});
							
							

//							datamigAsync.saveRecords(c.getCompanyId(), "Employee Attendance",new AsyncCallback<ArrayList<Integer>>() {
//
//										@Override
//										public void onFailure(Throwable caught) {
//											form.showDialogMessage("Failed");
//										}
//
//										@Override
//										public void onSuccess(ArrayList<Integer> result) {
//
//											if (result.contains(-1)) {
//												form.showDialogMessage("Invalid Excel File");
//											}if (result.contains(-2)) {
//												form.showDialogMessage("Please enter month in mm/dd/yyyy format.");
//											}
//											else if (result.contains(-3)) {
//												form.showDialogMessage("Some Employee info not available. Or Calandare not Assign");
//											}else if (result.contains(-4)) {
//												form.showDialogMessage("Please Specify Attendance Status Properly 4");
//											} else if (result.contains(-5)) {
//												form.showDialogMessage("Please Specify Leave Type Properly 5");
//											} else if (result.contains(-6)) {
//												form.showDialogMessage("Please Specify Overtime type 6");
//											} else if (result.contains(-7)) {
//												form.showDialogMessage("Check the In and Out type 7");
//											}else if (result.contains(-8)) {
//												form.showDialogMessage("Please define project name 8");
//											} else {
//												/*form.txtfilecount.setValue(result.get(0)+ "");
//												form.txtsavedrecords.setValue(result.get(1)+ "");*/
//											//	System.out.println("Successfully  " + result.get(0) + " save re  "+result.get(1));
//												form.showDialogMessage("Attendance uploaded Sucessfully");
//											}
//
//										}
//
//									});
						
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
				
				
				
				
			}
			
			
			private HrProject getProjectName(String projName,List<HrProject> projectList) {
				for(HrProject proj:projectList){
					if(proj.getProjectName().equals(projName)){
						return proj;
					}
				}
				return null;
			}

			private LeaveBalance getEmpLeaveBalance(int empId,List<LeaveBalance> empLeaveBalList) {
				// TODO Auto-generated method stub
				for(LeaveBalance balance:empLeaveBalList){
					if(empId==balance.getEmpCount()){
						return balance;
					}
				}
				return null;
			}

			private EmployeeInfo validateEmpId(int empId, List<EmployeeInfo> empInfoList) {
				for(EmployeeInfo info:empInfoList){
					/**
					 * Date : 27-08-2018 By ANIL
					 * Also checking whether employee is active or not
					 */
//					if(empId==info.getEmpCount()){
					if(empId==info.getEmpCount()&&info.isStatus()==true){
						return info;
					}
				}
				return null;
			}

			public boolean isWeeklyOff(WeekleyOff wo,Date date){
				Console.log("date "+date);
				String dayOfWeek1 = format.format(date);
				int dayOfWeek = Integer.parseInt(dayOfWeek1);
				Console.log("dayOfWeek "+dayOfWeek);

//				Calendar c = Calendar.getInstance();
//				c.setTime(date);
//				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//				c.setTimeZone(TimeZone.getTimeZone("IST"));
//				int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
////				logger.log(Level.SEVERE,"DATE : "+date+"DAY OF WEEK : "+dayOfWeek);
				if (dayOfWeek==0)
					return wo.isSUNDAY();
				if (dayOfWeek==1)
					return wo.isMONDAY();
				if (dayOfWeek==2)
					return wo.isTUESDAY();
				if (dayOfWeek==3)
					return wo.isWEDNESDAY();
				if (dayOfWeek==4)
					return wo.isTHRUSDAY();
				if (dayOfWeek==5)
					return wo.isFRIDAY();
				if (dayOfWeek==6)
					return wo.isSATAURDAY();
				return false;
			}
			
			/**
			 * @author Anil , Date : 20-08-2019
			 * this method checks whether weekly off is defined at calendar level or not
			 */
			public boolean isWeeklyOffDefined(WeekleyOff wo){
				
				if(wo.isSUNDAY()||wo.isMONDAY()||wo.isTUESDAY()||wo.isWEDNESDAY()||wo.isTHRUSDAY()||wo.isFRIDAY()||wo.isSATAURDAY()){
					return true;
				}
				return false;
			}
			
			public double getTotalPhOtHours(String value) {
				try{
					String ph_ot = value.replaceAll("\\s", "");
					if(ph_ot.equalsIgnoreCase("PH")){
						return 0;
					}else{
						if(ph_ot.contains("-")){
							String[] phArray = ph_ot.split("-");
							String ph=phArray[0];
							String ot=phArray[1];
							if(ph.equalsIgnoreCase("PH")){
								try{
									int otNum=Integer.parseInt(ot);
									return otNum;
								}catch(Exception e){
									
								}
							}
						}
					}
				}catch(Exception e){
					
				}
				return 0;
			}



			public boolean validPH_OT(String value) {
				try{
					String ph_ot = value.replaceAll("\\s", "");
					if(ph_ot.equalsIgnoreCase("PH")){
						return true;
					}else{
						if(ph_ot.contains("-")){
							String[] phArray = ph_ot.split("-");
							String ph=phArray[0];
							String ot=phArray[1];
							if(ph.equalsIgnoreCase("PH")){
								try{
									int otNum=Integer.parseInt(ot);
									return true;
								}catch(Exception e){
									
								}
							}
						}
					}
				}catch(Exception e){
					
				}
				return false;
			}
			
			/**
			 * @author Anil
			 * @since 07-09-2020
			 * Added designation in case of reliever employee to get OT details
			 * @param designation
			 * @return
			 */
			private Overtime getOvertimeName(ArrayList<Overtime> otList, String otType, int empId,String designation) {
				for(Overtime ot:otList){
					
					if(otType.equalsIgnoreCase("WO")&&ot.isWeeklyOff()==true&&ot.getEmpId()==empId){
						return ot;
					}
					if(otType.equalsIgnoreCase("N")&&ot.isNormalDays()==true&&ot.getEmpId()==empId){
						return ot;
					}
					if((otType.equalsIgnoreCase("H")&&ot.getEmpId()==empId)&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))){
						return ot;
					}
					
					if((otType.equalsIgnoreCase("PH")&&ot.getEmpId()==empId)&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))&&ot.getHolidayType().equalsIgnoreCase("Public Holiday")){
						return ot;
					}
					
					if((otType.equalsIgnoreCase("NH")&&ot.getEmpId()==empId)&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))&&ot.getHolidayType().equalsIgnoreCase("National Holiday")){
						return ot;
					}
					
					/**
					 * @author Anil, Date :09-05-2019
					 */
					if((otType.equalsIgnoreCase("OT")&&ot.getEmpId()==empId)&&(ot.getShortName().equalsIgnoreCase("OT"))){
						return ot;
					}
					if((otType.equalsIgnoreCase("DOT")&&ot.getEmpId()==empId)&&(ot.getShortName().equalsIgnoreCase("DOT"))){
						return ot;
					}
				}
				
				for(Overtime ot:otList){
					if(ot.getEmpId()==0&&designation!=null){
						if(otType.equalsIgnoreCase("WO")&&ot.isWeeklyOff()==true&&ot.getEmpDesignation().equals(designation)){
							return ot;
						}
						if(otType.equalsIgnoreCase("N")&&ot.isNormalDays()==true&&ot.getEmpDesignation().equals(designation)){
							return ot;
						}
						if((otType.equalsIgnoreCase("H")&&ot.getEmpDesignation().equals(designation))&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))){
							return ot;
						}
						
						if((otType.equalsIgnoreCase("PH")&&ot.getEmpDesignation().equals(designation))&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))&&ot.getHolidayType().equalsIgnoreCase("Public Holiday")){
							return ot;
						}
						
						if((otType.equalsIgnoreCase("NH")&&ot.getEmpDesignation().equals(designation))&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))&&ot.getHolidayType().equalsIgnoreCase("National Holiday")){
							return ot;
						}
						
						if((otType.equalsIgnoreCase("OT")&&ot.getEmpDesignation().equals(designation))&&(ot.getShortName().equalsIgnoreCase("OT"))){
							return ot;
						}
						if((otType.equalsIgnoreCase("DOT")&&ot.getEmpDesignation().equals(designation))&&(ot.getShortName().equalsIgnoreCase("DOT"))){
							return ot;
						}
					}
				}
				
				
				return null;
			}
			
			private ArrayList<Overtime> getHRProjectOvertimelist(HrProject project,
					ArrayList<HrProjectOvertime> hrProjectOvertimelist) {
				ArrayList<Overtime> list = new ArrayList<Overtime>();

				if(project.getOtList()!=null && project.getOtList().size()>0){
					list.addAll(project.getOtList());
				}
				for(HrProjectOvertime hrpojectovertime : hrProjectOvertimelist) {
					if(hrpojectovertime.getProjectId()==project.getCount()) {
						list.add(hrpojectovertime);
					}
				}
				return list;
			}
			
}
