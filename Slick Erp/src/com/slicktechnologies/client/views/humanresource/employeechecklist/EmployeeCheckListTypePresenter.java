package com.slicktechnologies.client.views.humanresource.employeechecklist;

import java.util.Vector;

import com.google.gwt.core.shared.GWT;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeCheckListType;

public class EmployeeCheckListTypePresenter extends FormTableScreenPresenter<EmployeeCheckListType>  {

	//Token to set the concrete form
	EmployeeCheckListTypeForm form;
	CsvServiceAsync async=GWT.create(CsvService.class);
	
	public EmployeeCheckListTypePresenter (FormTableScreen<EmployeeCheckListType> view,
			EmployeeCheckListType model) {
		super(view, model);
		form=(EmployeeCheckListTypeForm) view;
		
		Console.log("inside Presenter Const");
		form.getSupertable().connectToLocal();
		form.retriveTable(getQuery());
		form.setPresenter(this);
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();		
		if(text.equals(AppConstants.NEW)){
			reactToNew();
		}		
	}
	
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new EmployeeCheckListType();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new EmployeeCheckListType());
		return quer;
	}
	
	public void setModel(EmployeeCheckListType entity)
	{
		model=entity;
	}
	
	public static EmployeeCheckListTypeForm initialize()
	{
			Console.log("Inside user form initialize");
//			 UserPresenterTable gentableScreen=GWT.create(UserPresenterTable.class);
			EmployeeCheckListTypeTableProxy gentableScreen=new EmployeeCheckListTypeTableProxy(); 
			EmployeeCheckListTypeForm  form=new  EmployeeCheckListTypeForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			Console.log("Inside user form initialize 2222222");
			EmployeeCheckListTypePresenter  presenter=new  EmployeeCheckListTypePresenter  (form,new EmployeeCheckListType());
			AppMemory.getAppMemory().stickPnel(form);
			Console.log("Inside user form initialize 333333333");
			return form;
			
			
		 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.EmployeeCheckListType")
		 public static class  EmployeeCheckListTypePresenterTable extends SuperTable<EmployeeCheckListType> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
		

	@Override
	public void reactOnDownload() {
//		
//		/**
//		 * Date 2-03-2017 added by vijay
//		 * for User Download
//		 */
//		
//		ArrayList<User> userarray=new ArrayList<User>();
//		List<User> list=form.getSupertable().getListDataProvider().getList();
//		
//		userarray.addAll(list); 
//		
//		async.setUserList(userarray, new AsyncCallback<Void>() {
//			
//			@Override
//			public void onSuccess(Void result) {
//				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//				final String url=gwt + "csvservlet"+"?type="+121;
//				Window.open(url, "test", "enabled");
//			}
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				System.out.println("RPC call Failed"+caught);
//			}
//		});
//		
//		/**
//		 * ends here
//		 */
//		
	}

	private void reactToNew()
	{
		form.setToNewState();
		this.initialize();
		form.toggleAppHeaderBarMenu();
	}
}
