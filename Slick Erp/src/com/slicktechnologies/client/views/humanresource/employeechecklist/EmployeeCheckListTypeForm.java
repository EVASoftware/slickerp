package com.slicktechnologies.client.views.humanresource.employeechecklist;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.settings.user.IPAddressTable;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeCheckListType;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.UserRole;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;

public class EmployeeCheckListTypeForm extends FormTableScreen<EmployeeCheckListType>  implements ClickHandler {

	//Token to add the varialble declarations
	ObjectListBox<Config> olbEmployeeType;	
	ObjectListBox<CheckListType> olbCheckListType;
	CheckBox cbStatus;
	
	EmployeeCheckListType employeeCheckListType;

	public EmployeeCheckListTypeForm (SuperTable<EmployeeCheckListType> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();

	}

	private void initalizeWidget()
	{
		Console.log("Inside user form initialize4444444444444444");
		olbEmployeeType = new ObjectListBox<Config>();	
		AppUtility.MakeLiveConfig(olbEmployeeType,Screen.EMPLOYEETYPE);
		olbCheckListType = new ObjectListBox<CheckListType>();
		CheckListType.initializeTypeName(olbCheckListType);
		cbStatus = new CheckBox();
		cbStatus.setValue(true);
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();
		this.processlevelBarNames=new String[]{AppConstants.NEW};


		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingInformation=fbuilder.setlabel("Employee CheckList Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("* Employee Type",olbEmployeeType);
		FormField folbEmployeeType= fbuilder.setMandatory(true).setMandatoryMsg("Employee Type is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* CheckList Type",olbCheckListType);
		FormField folbCheckListType= fbuilder.setMandatory(true).setMandatoryMsg("CheckList Type is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fcbStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingInformation},
				{folbEmployeeType,folbCheckListType,fcbStatus},
				
		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(EmployeeCheckListType model) 
	{
		if(olbEmployeeType.getSelectedIndex()!=0){
			model.setEmployeeType(olbEmployeeType.getValue(olbEmployeeType.getSelectedIndex()));
		}
		if(olbCheckListType.getSelectedIndex()!=0){
			model.setCheckListType(olbCheckListType.getSelectedItem());
		}
		model.setStatus(cbStatus.getValue());
		employeeCheckListType=model;
		presenter.setModel(model);


	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(EmployeeCheckListType view) 
	{

		employeeCheckListType=view;
		
		if(view.getEmployeeType()!= null){
			olbEmployeeType.setValue(view.getEmployeeType());
		}
		if(view.getCheckListType()!=null){
			olbCheckListType.setValue(view.getCheckListType().getCheckListName());
		}
		cbStatus.setValue(view.getStatus());
		
		presenter.setModel(view);
	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))//|| text.equals("Download")
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))//|| text.equals("Download")
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.EMPLOYEECHECKLISTTYPE,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{

	}


	@Override
	public void onClick(ClickEvent event) {

	}
	


	/**
	 * Extra validation which matches the typed pass word from retyped passwords
	 */
	@Override
	public boolean validate()
	{
		boolean result=super.validate();
		return result;

	}


	@Override
	public void clear() {

		super.clear();
		cbStatus.setValue(true); 
	}


	@Override
	public void setToViewState() {
		super.setToViewState();

		SuperModel model=new User();
		model=employeeCheckListType;
		AppUtility.addDocumentToHistoryTable(AppConstants.HRCONFIG,AppConstants.EMPLOYEECHECKLISTTYPE, employeeCheckListType.getCount(), null,null,null, false, model, null);
	
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(employeeCheckListType!=null){
			SuperModel model=new User();
			model=employeeCheckListType;
			AppUtility.addDocumentToHistoryTable(AppConstants.HRCONFIG,AppConstants.EMPLOYEECHECKLISTTYPE, employeeCheckListType.getCount(), null,null,null, false, model, null);
		
		}
	}

}
