package com.slicktechnologies.client.views.humanresource.employeechecklist;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.slicktechnologies.client.views.humanresource.employeechecklist.EmployeeCheckListTypePresenter.EmployeeCheckListTypePresenterTable;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeCheckListType;


public class EmployeeCheckListTypeTableProxy extends EmployeeCheckListTypePresenterTable {

	TextColumn<EmployeeCheckListType> getEmployeeTypeCol;
	TextColumn<EmployeeCheckListType> getCountCol;
	TextColumn<EmployeeCheckListType> getConfigTypeCol;
	TextColumn<EmployeeCheckListType> getStatusCol;
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		getCountCol();
		getEmployeeTypeCol();
		getConfigTypeCol();
		getStatusCol();
	}

	
	
	private void getCountCol() {
		// TODO Auto-generated method stub
		getCountCol=new TextColumn<EmployeeCheckListType>() {
			@Override
			public String getValue(EmployeeCheckListType object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getCountCol,"Id");
		table.setColumnWidth(getCountCol,90, Unit.PX);
		getCountCol.setSortable(true);
	}



	

	private void getEmployeeTypeCol() {
		// TODO Auto-generated method stub
		getEmployeeTypeCol=new TextColumn<EmployeeCheckListType>() {
			@Override
			public String getValue(EmployeeCheckListType object) {
				if(object.getEmployeeType()!=null){
					return object.getEmployeeType();
				}
				return "";
			}
		};
		table.addColumn(getEmployeeTypeCol,"Emplyoee Type");
		table.setColumnWidth(getEmployeeTypeCol,110, Unit.PX);
		getEmployeeTypeCol.setSortable(true);
		
	}



	private void getConfigTypeCol() {
		// TODO Auto-generated method stub
		getConfigTypeCol=new TextColumn<EmployeeCheckListType>() {
			@Override
			public String getValue(EmployeeCheckListType object) {
				if(object.getCheckListType()!=null){
					return object.getCheckListType().getCheckListName();
				}
				return "";
			}
		};
		table.addColumn(getConfigTypeCol,"CheckList Type");
		table.setColumnWidth(getConfigTypeCol,90, Unit.PX);
		getConfigTypeCol.setSortable(true);
	}



	private void getStatusCol() {
		// TODO Auto-generated method stub
		getStatusCol=new TextColumn<EmployeeCheckListType>() {
			@Override
			public String getValue(EmployeeCheckListType object) {
				if(object.getStatus() == true){
					return "Active";
				}
				return "Inactive";
			}
		};
		table.addColumn(getStatusCol,"Status");
		table.setColumnWidth(getStatusCol,110, Unit.PX);
		getStatusCol.setSortable(true);
	}



	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		addSortingOnCountCol();
	
		addSortingOnEmployeeTypeCol();
		addSortingOnCheckListTypeCol();
		addSortingOnStatusCol();
	}



	private void addSortingOnCountCol() {
		List<EmployeeCheckListType> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeCheckListType>(list);
		columnSort.setComparator(getCountCol, new Comparator<EmployeeCheckListType>()
		{
			@Override
			public int compare(EmployeeCheckListType e1,EmployeeCheckListType e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}


	private void addSortingOnEmployeeTypeCol() {
		List<EmployeeCheckListType> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeCheckListType>(list);
		columnSort.setComparator(getEmployeeTypeCol, new Comparator<EmployeeCheckListType>()
		{
			@Override
			public int compare(EmployeeCheckListType e1,EmployeeCheckListType e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeeType()!=null && e2.getEmployeeType()!=null){
						return e1.getEmployeeType().compareTo(e2.getEmployeeType());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnCheckListTypeCol() {
		List<EmployeeCheckListType> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeCheckListType>(list);
		columnSort.setComparator(getConfigTypeCol, new Comparator<EmployeeCheckListType>()
		{
			@Override
			public int compare(EmployeeCheckListType e1,EmployeeCheckListType e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCheckListType()!=null && e2.getCheckListType()!=null){
						return e1.getCheckListType().compareTo(e2.getCheckListType());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnStatusCol() {
		List<EmployeeCheckListType> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeCheckListType>(list);
		columnSort.setComparator(getStatusCol, new Comparator<EmployeeCheckListType>()
		{
			@Override
			public int compare(EmployeeCheckListType e1,EmployeeCheckListType e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
