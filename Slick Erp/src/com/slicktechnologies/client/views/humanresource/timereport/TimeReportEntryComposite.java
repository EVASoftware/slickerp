package com.slicktechnologies.client.views.humanresource.timereport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.HTMLTable.CellFormatter;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.DecimalWithSinglePrecion;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.timereport.TimeSheetLineItem.HourWorked;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.personlayer.Employee;

// TODO: Auto-generated Javadoc
/**
 * A Simple Composite Contains a Flex Table inside Scroll Panel Provides
 * Functionality for Setting Header, Row Sum and Column Sums. Right now being
 * used for Time Sheet Entry hence are only Date Composite Generates its Header
 * row on startDate and endDate attributes. So for any header changes Start date
 * and end dates needs to be set.
 * 
 * 
 * @author Komal
 */
public class TimeReportEntryComposite extends Composite implements
		CompositeInterface, ClickHandler, ValueChangeHandler<Double>,
		HasValue<List<TimeSheetLineItem>> {

	/** Flex Table for Setting User Interface Elements. */
	FlexTable table;

	/** To Provide Vertical and Horizontal Functionality. */
	ScrollPanel panel;

	/** Date corresponding to first column. */
	private Date fromDate;

	/** Date corresponding to End column. */
	private Date toDate;

	/**
	 * this counter is used while setting leaveHours. BY anil 29 june 2015
	 */
	static int leaveCounter = 0;

	/**
	 * Date : 01-06-2018 BY ANIL
	 */
	EmployeeInfo empInfo;

	/**
	 * Date : 04-06-2018 BY ANIL
	 * 
	 */

	HashMap<Integer, String> projAgainstOtMap = new HashMap<Integer, String>();

	/**
	 * Instantiates a new time report entry composite.
	 */

	public TimeReportEntryComposite() {
		super();

		table = new FlexTable();
		table.addClickHandler(this);
		panel = new ScrollPanel();
		panel.add(table);
		initWidget(panel);
		applyStyle();
		projAgainstOtMap = new HashMap<Integer, String>();
	}

	public EmployeeInfo getEmpInfo() {
		return empInfo;
	}

	public void setEmpInfo(EmployeeInfo empInfo) {
		this.empInfo = empInfo;
	}

	/**
	 * Apply style.
	 */
	public void applyStyle() {
		panel.setHeight("250px");
		panel.setWidth("100%%");

		table.setWidth("95%");
		table.getColumnFormatter().addStyleName(0, "firstcolumn");

		// apply other column style
		for (int i = 1; i < 10; i++) {
			table.getColumnFormatter().addStyleName(i, "othercolumn");
		}
	}

	// This method is called from setTimeReportSheet() method in presenter.
	public void refreshTimeSheetComposite(Date startDate, Date endDate) {
		Console.log("TymRptEtyComp Start Date:: " + startDate
				+ " || End Date:: " + endDate);
		this.fromDate = startDate;
		this.toDate = endDate;
		Console.log("TymRptEtyComp From Date:: " + fromDate + " || To Date:: "
				+ toDate);
		if (table.getRowCount() != 0) {
			Console.log("Table Row not zero.");
			changePeriod(startDate, endDate);
			clear();

		} else {
			Console.log("Table Row zero.");
			setHeaderRow(fromDate, toDate);
			generateShiftAndMachineRow();
			generateTotalRow();
		}
	}

	/**
	 * Method Changes the The Date Range Displayed on Columns. It also clears
	 * the Entry Cells.
	 *
	 * @param startDate
	 *            the start date first Date of Column.
	 * 
	 * @param endDate
	 *            the end date last Date of Column.
	 * 
	 */

	private void changePeriod(Date startDate, Date endDate) {
		clear();
		Console.log("Change Period Start Date:: " + startDate
				+ " || End Date:: " + endDate);
		int noDays = CalendarUtil.getDaysBetween(startDate, endDate);
		CellFormatter formater = table.getCellFormatter();

		InlineLabel projectName = new InlineLabel("Project");
		table.setWidget(0, 0, projectName);
		formater.setHorizontalAlignment(0, 0, HasAlignment.ALIGN_LEFT);

		/**
		 * Date : 21-05-2018 BY Setting static width for first column
		 */
		table.getCellFormatter().getElement(0, 0).getStyle()
				.setWidth(150, Unit.PX);
		table.getCellFormatter().getElement(0, 0).getStyle()
				.setBackgroundColor("aliceblue");
		/**
		 * End
		 */

		for (int i = 1; i <= noDays + 1; i++) {

			DateTimeFormat fmt = DateTimeFormat.getFormat("EEE dd MM");
			String day = fmt.format(startDate);
			InlineLabel label = (InlineLabel) table.getWidget(0, i);
			table.getCellFormatter().getElement(0, i).getStyle()
					.setBackgroundColor("beige");
			label.setText(day);

			CalendarUtil.addDaysToDate(startDate, 1);

		}

	}

	@Override
	public void clear() {
		for (int i = table.getRowCount() - 1; i >= 3; i--) {
			table.removeRow(i);
		}
		generateTotalRow();
		// Set Zero to first two rows
		for (int i = 1; i <= 8; i++) {
			InlineLabel label = (InlineLabel) table.getWidget(1, i);
			label.setText("0.0");
			label = (InlineLabel) table.getWidget(2, i);
			label.setText("0.0");
		}

	}

	/**
	 * Sets the Header Date Column on Table.
	 * 
	 * @param fromDate
	 *            First Column Date
	 * @param toDate
	 *            Last Column Date
	 * 
	 */
	private void setHeaderRow(Date fromDate, Date toDate) {

		Console.log("setheaderRow From Date:: " + fromDate + " || To Date:: "
				+ toDate);
		int noDays = CalendarUtil.getDaysBetween(fromDate, toDate);
		CellFormatter formater = table.getCellFormatter();
		InlineLabel projectName = new InlineLabel("Project");
		table.setWidget(0, 0, projectName);
		formater.setHorizontalAlignment(0, 0, HasAlignment.ALIGN_LEFT);

		/**
		 * Date : 21-05-2018 BY Setting static width for first column
		 */
		table.getCellFormatter().getElement(0, 0).getStyle()
				.setWidth(150, Unit.PX);
		table.getCellFormatter().getElement(0, 0).getStyle()
				.setBackgroundColor("aliceblue");
		/**
		 * End
		 */

		for (int i = 1; i <= noDays + 1; i++) {
			DateTimeFormat fmt = DateTimeFormat.getFormat("EEE dd MM");
			String day = fmt.format(fromDate);
			table.setWidget(0, i, new InlineLabel(day));
			table.getCellFormatter().getElement(0, i).getStyle()
					.setBackgroundColor("beige");
			CalendarUtil.addDaysToDate(fromDate, 1);
		}
		table.setWidget(0, 8, new InlineLabel("Total"));
		table.getCellFormatter().getElement(0, 8).getStyle()
				.setBackgroundColor("gainsboro");
		// Set Horizontal Allignment

		for (int i = 1; i < table.getCellCount(0); i++) {
			formater.setHorizontalAlignment(0, i, HasAlignment.ALIGN_CENTER);
		}
	}

	/**
	 * Ui Method again Genrates Second and Third Rows.Corresponding to Shift
	 * Hour and Machine Hour.
	 */
	private void generateShiftAndMachineRow() {

		int noColumn = table.getCellCount(0);
		table.setWidget(1, 0, new InlineLabel("Working Hours"));
		table.setWidget(2, 0, new InlineLabel("Machine Hours"));

		/**
		 * Date : 21-05-2018 BY Setting static width for first column
		 */
		table.getCellFormatter().getElement(1, 0).getStyle()
				.setWidth(120, Unit.PX);
		table.getCellFormatter().getElement(2, 0).getStyle()
				.setWidth(120, Unit.PX);
		table.getCellFormatter().getElement(1, 0).getStyle()
				.setBackgroundColor("aliceblue");
		table.getCellFormatter().getElement(2, 0).getStyle()
				.setBackgroundColor("aliceblue");
		/**
		 * End
		 */

		for (int i = 1; i < noColumn; i++) {
			table.setWidget(1, i, new InlineLabel("0.0"));
			table.setWidget(2, i, new InlineLabel("0.0"));
		}
		CellFormatter formater = table.getCellFormatter();
		formater.setHorizontalAlignment(1, 0, HasAlignment.ALIGN_LEFT);
		formater.setHorizontalAlignment(1, 1, HasAlignment.ALIGN_LEFT);
		for (int i = 1; i < table.getCellCount(0); i++) {
			formater.setHorizontalAlignment(1, i, HasAlignment.ALIGN_CENTER);
			table.getCellFormatter().getElement(1, i).getStyle()
					.setBackgroundColor("papayawhip");

			if (i == table.getCellCount(0) - 1) {
				table.getCellFormatter().getElement(1, i).getStyle()
						.setBackgroundColor("gainsboro");
				table.getCellFormatter().getElement(2, i).getStyle()
						.setBackgroundColor("gainsboro");
			}
			// formater.setHorizontalAlignment(1, i, HasAlignment.ALIGN_CENTER);
			formater.setHorizontalAlignment(2, i, HasAlignment.ALIGN_CENTER);
		}
	}

	/*****************************************************************************************************************************/

	public void setTableRowReadOnly(Date fromDate) {
		System.out.println("###         SET TR RO STARTTT        ###");
		Console.log("Setting table row read only");
		Date currStDate = CalendarUtil.copyDate(fromDate);
		// System.out.println("Start Date --- "+currStDate);
		DateTimeFormat fmt = DateTimeFormat.getFormat("EEE dd MM");
		String strDate = fmt.format(currStDate);

		int noOfRow = table.getRowCount();

		for (int i = 0; i < noOfRow; i++) {
			int cellCount = table.getCellCount(i);

			for (int k = 0; k < cellCount - 1; k++) {
				double sum = 0;
				InlineLabel label = (InlineLabel) table.getWidget(0, k);
				String labelDate = label.getText().trim();

				if (labelDate.equals(strDate.trim())) {
					// System.out.println("String  Date --- "+strDate+" (Row ,Col) :: ("+i+","+k+")");
					// System.out.println("Table  Date -- "+labelDate+" (Row ,Col) :: ("+i+","+k+")");
					// InlineLabel label1 = (InlineLabel) table.getWidget(i, k);
					// label1.setVisible(false);

					if (table.getWidget(i, k) instanceof DoubleBox) {
						// System.out.println("Inside Double Box--- ");
						DoubleBox box = (DoubleBox) table.getWidget(i, k);
						box.setValue(null);
						box.setEnabled(false);
						// System.out.println("After Double Box Disable --- ");
					}

					if (table.getWidget(i, k) instanceof InlineLabel) {
						// System.out.println("Inside InlineLabel --- ");
						if (i == 1) {
							InlineLabel hrsLabel = (InlineLabel) table
									.getWidget(i, k);
							hrsLabel.setText("NA");
						}
					}

				}
			}
		}

		System.out.println("         SET TR RO END        ");

	}

	@Override
	public void setEnable(boolean status) {
		int noOfRow = table.getRowCount();
		for (int i = 0; i < noOfRow; i++) {
			int cellCount = table.getCellCount(i);
			for (int k = 0; k < cellCount; k++) {
				Widget wb = table.getWidget(i, k);
				if (wb instanceof DoubleBox) {
					DoubleBox box = (DoubleBox) table.getWidget(i, k);
					box.setEnabled(status);
				}

				if (wb instanceof Button) {
					Button box = (Button) table.getWidget(i, k);
					box.setEnabled(status);
				}
				/**
				 * This is not required ,Commented by Anil 17-7-2015
				 */

				// if (wb instanceof CheckBox) {
				// CheckBox box = (CheckBox) table.getWidget(i, k);
				// box.setEnabled(status);
				// if (box.getValue()){
				// toggleVerticalColumnState(k, false);
				// }
				// else{
				// toggleVerticalColumnState(k, true);
				// }
				// }
			}
		}
		// Making delete Buttons disabled
	}

	/**
	 * This is not required ,Commented by Anil 17-7-2015
	 */

	// /**
	// * @param colIndex colIndex Index of Column which double boxes which state
	// has to be toggeled
	// * @param state True to make enable , false to make disable
	// */
	// private void toggleVerticalColumnState(int colIndex, boolean state) {
	// for (int i = 0; i < table.getRowCount(); i++) {
	// Widget widg = table.getWidget(i, colIndex);
	// if (widg instanceof DoubleBox) {
	// DoubleBox db = (DoubleBox) widg;
	// db.setEnabled(state);
	//
	// }
	// }
	// }

	@Override
	public boolean validate() {
		if (this.table.getRowCount() <= 4) {
			return false;
		}
		return true;
	}

	/**
	 * Insert a new Row in Table.Each Row Represents a Project Insertion happens
	 * only when Project is not already present. and its worked hrs in that
	 * period.Method also updates horizontal total and vertical total.
	 * 
	 * @param projName
	 *            Name of Project to be inserted.
	 * 
	 */

	public void insertEntry(String projName, boolean leaveRow, boolean otRow,String otProjName) {

		String modifiedProjName;

		if (leaveRow == true) {
			modifiedProjName = "Leave :" + projName;
		} else if (otRow == true) {
			modifiedProjName = "OT :" + projName;
		} else {
			modifiedProjName = projName;
		}

		if (isPresent(modifiedProjName) == true) {
			return;
		}

		table.removeRow(table.getRowCount() - 1);
		int rowNo = table.getRowCount();

		InlineLabel label = new InlineLabel(projName);
		if (leaveRow == true) {
			label.setText("Leave :" + projName);

		} else if (otRow == true) {
			label.setText("OT :" + projName);
			/**
			 * Date : 04-06-2018 By ANIL
			 */
			projAgainstOtMap.put(rowNo, otProjName);
		} else {
			label.setText(projName);
		}
		table.setWidget(rowNo, 0, label);
		table.getCellFormatter().getElement(rowNo, 0).getStyle()
				.setBackgroundColor("aliceblue");

		// table.getCellFormatter().getElement(rowNo,
		// 0).getStyle().setHeight(15, Unit.PX);
		// table.getWidget(rowNo, 0).setHeight("15px");

		CellFormatter formater = table.getCellFormatter();
		formater.setHorizontalAlignment(rowNo, 0, HasAlignment.ALIGN_LEFT);

		int firstRowLength = table.getCellCount(0);
		System.out.println(" insertEntry  First Row Length :: "
				+ firstRowLength);
		int entryAreaLength = firstRowLength - 2;
		System.out.println(" insertEntry  Entery Area Length :: "
				+ entryAreaLength);

		for (int k = 1; k <= entryAreaLength; k++) {
			DoubleBox db = new DoubleBox();
			db.setWidth("100%");
			/**
			 * Date : 21-05-2018 By ANIL
			 */
			// db.setHeight("30px");
			db.setHeight("15px");
			db.addKeyPressHandler(new DecimalWithSinglePrecion());
			db.addValueChangeHandler(this);
			db.setName(k + "-" + rowNo);
			table.setWidget(rowNo, k, db);
			formater.setHorizontalAlignment(rowNo, k, HasAlignment.ALIGN_CENTER);

			// table.getWidget(rowNo, k).setHeight("15px");
			// table.getCellFormatter().getElement(rowNo,
			// k).getStyle().setHeight(15, Unit.PX);

		}
		table.setWidget(rowNo, firstRowLength - 1, new InlineLabel("0.0"));
		formater.setHorizontalAlignment(rowNo, firstRowLength - 1,
				HasAlignment.ALIGN_CENTER);

		// Button button = new Button("Delete");
		Button button = new Button("-");
		button.addClickHandler(this);

		table.setWidget(rowNo, firstRowLength, button);
		formater.setHorizontalAlignment(rowNo, firstRowLength,
				HasAlignment.ALIGN_CENTER);

		generateTotalRow();
		fillVerticalTotal();
		fillHorizontalTotal();
	}

	/**
	 * 
	 * Checks weather a Project is Present or not.
	 * 
	 * @param projName
	 *            the proj name
	 * @return true, if is present
	 */
	private boolean isPresent(String projName) {
		int i = 0;
		for (i = 1; i < table.getRowCount() - 1; i++) {
			InlineLabel label = (InlineLabel) table.getWidget(i, 0);
			if (label.getText().trim().equals(projName)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Ui method genrate Last Total Row.
	 */
	public void generateTotalRow() {

		int noOfRow = table.getRowCount();
		noOfRow = table.getRowCount();
		int noOfColumn = table.getCellCount(0);
		CellFormatter formater = table.getCellFormatter();
		table.setWidget(noOfRow, 0, new InlineLabel("Total"));
		/**
		 * Date : 21-05-2018 BY Setting static width for first column
		 */
		table.getCellFormatter().getElement(noOfRow, 0).getStyle()
				.setWidth(120, Unit.PX);
		table.getCellFormatter().getElement(noOfRow, 0).getStyle()
				.setBackgroundColor("gainsboro");
		/**
		 * End
		 */

		formater.setHorizontalAlignment(noOfRow, 0, HasAlignment.ALIGN_LEFT);

		for (int i = 1; i < noOfColumn; i++) {
			table.setWidget(noOfRow, i, new InlineLabel("0.0"));
			formater.setHorizontalAlignment(noOfRow, i,
					HasAlignment.ALIGN_CENTER);
			table.getCellFormatter().getElement(noOfRow, i).getStyle()
					.setBackgroundColor("gainsboro");
		}
	}

	/**
	 * Traverse each column and than fills the vertical total .
	 * 
	 */

	public void fillVerticalTotal() {
		// System.out.println("Inside fill vertical Toatl ::: ");
		int firstRowLength = table.getCellCount(0);
		int entryAreaLength = firstRowLength - 2;
		int rowCount = table.getRowCount();

		// System.out.println("First row Length :: "+firstRowLength);
		// System.out.println("entery area length :: "+entryAreaLength);
		// System.out.println("table row count :: "+rowCount);

		double sum = 0d;
		for (int i = 1; i <= entryAreaLength; i++) {
			sum = 0d;
			for (int k = 3; k < rowCount - 1; k++) {
				if (table.getWidget(k, i) instanceof DoubleBox) {
					DoubleBox box = (DoubleBox) table.getWidget(k, i);
					if (box.getValue() == null)
						;
					else {
						sum = sum + box.getValue();
					}
				}
				if (table.getWidget(k, i) instanceof InlineLabel) {
					try {
						InlineLabel label = (InlineLabel) table.getWidget(k, i);
						Double temp = Double
								.parseDouble(label.getText().trim());
						sum = sum + temp;

					} catch (Exception e) {

					}
				}
			}
			InlineLabel lab = (InlineLabel) table.getWidget(rowCount - 1, i);
			lab.setText(sum + "");
			table.getCellFormatter().getElement(rowCount - 1, i).getStyle()
					.setBackgroundColor("gainsboro");
		}
	}

	/**
	 * Traverse each Row and than fills the horizontal total.
	 */

	public void fillHorizontalTotal() {
		// System.out.println("Inside Horizontal total method ...");

		int firstRowLength = table.getCellCount(0);
		int entryAreaLength = firstRowLength - 2;
		int noOfRow = table.getRowCount();

		double sum = 0d;
		for (int i = 3; i < noOfRow - 1; i++) {
			sum = 0d;
			for (int k = 1; k <= 7; k++) {
				if (table.getWidget(i, k) instanceof DoubleBox) {
					DoubleBox box = (DoubleBox) table.getWidget(i, k);

					if (box.getValue() == null) {
						;
					} else {
						sum = sum + box.getValue();
					}
				}
				if (table.getWidget(i, k) instanceof InlineLabel) {
					try {
						InlineLabel label = (InlineLabel) table.getWidget(i, k);
						Double temp = Double
								.parseDouble(label.getText().trim());
						sum = sum + temp;

					} catch (Exception e) {

					}
				}

				InlineLabel lab = (InlineLabel) table.getWidget(i, 8);
				lab.setText(sum + "");
				table.getCellFormatter().getElement(i, 8).getStyle()
						.setBackgroundColor("gainsboro");
			}
		}

		sum = 0;

		for (int i = 1; i <= entryAreaLength; i++) {
			int rowNo = table.getRowCount() - 1;

			InlineLabel label = (InlineLabel) table.getWidget(rowNo, i);
			String textSum = label.getText().trim();
			Double value = Double.parseDouble(textSum.trim());
			sum = sum + value;

		}
		InlineLabel lbl = (InlineLabel) table.getWidget(
				table.getRowCount() - 1, 8);
		table.getCellFormatter().getElement(table.getRowCount() - 1, 8)
				.getStyle().setBackgroundColor("gainsboro");
		lbl.setText(sum + "");

	}

	/**
	 * Insert a new Row in Table.Each Row Represents a Leave Type Method has no
	 * effect on horizontal total and vertical total.
	 * 
	 * @param leaveType
	 *            Type of Leave to be inserted.
	 * 
	 */

	// public void insertLeave(String leaveType) {
	//
	// table.removeRow(table.getRowCount() - 1);
	// int rowNo = table.getRowCount();
	// InlineLabel label = new InlineLabel(leaveType);
	// label.setText(leaveType);
	// table.setWidget(rowNo, 0, label);
	//
	// CellFormatter formater = table.getCellFormatter();
	// formater.setHorizontalAlignment(rowNo, 0, HasAlignment.ALIGN_LEFT);
	//
	// int firstRowLength = table.getCellCount(0);
	// int entryAreaLength = firstRowLength - 2;
	//
	// for (int k = 1; k <= entryAreaLength; k++) {
	// CheckBox db = new CheckBox();
	// db.setWidth("100%");
	// db.setHeight("30px");
	// table.setWidget(rowNo, k, db);
	// db.addClickHandler(this);
	//
	// formater.setHorizontalAlignment(rowNo, k, HasAlignment.ALIGN_CENTER);
	//
	// }
	// table.setWidget(rowNo, firstRowLength - 1, new InlineLabel("0.0"));
	// formater.setHorizontalAlignment(rowNo, firstRowLength -
	// 1,HasAlignment.ALIGN_CENTER);
	// Button button = new Button("Delete");
	// button.addClickHandler(this);
	//
	// table.setWidget(rowNo, firstRowLength, button);
	// formater.setHorizontalAlignment(rowNo,
	// firstRowLength,HasAlignment.ALIGN_CENTER);
	// generateTotalRow();
	// fillVerticalTotal();
	// fillHorizontalTotal();
	// }

	/****************************** ON VALUE CHANGE *****************************************/

	public void onValueChange(ValueChangeEvent<Double> event) {
		System.out.println("Inside on Value Change EVENT.....");
		Double val = event.getValue();
		if (val == null) {
			System.out.println("inside Val Chng NULL...");
		} else {
			DoubleBox box = (DoubleBox) event.getSource();
			int boxCol = Integer.parseInt(box.getName().split("-")[0]);
			int boxRow = Integer.parseInt(box.getName().split("-")[1]);

			System.out.println("Box Col---- :: " + boxCol);
			System.out.println("Box Row---- :: " + boxRow);

			InlineLabel prjlabel = (InlineLabel) table.getWidget(boxRow, 0);
			String prjtext = prjlabel.getText();
			System.out.println("Project-- " + prjtext);

			double workedHrs = getWorkingHrsSum(boxCol);
			double leaveHrs = getLeaveHrSum(boxCol);
			double otHrs = getOTHrsSum(boxCol);

			double totalWorkingHrs = 0;

			InlineLabel label = (InlineLabel) table.getWidget(1, boxCol);
			String text = label.getText();
			System.out.println("Total WO-- " + text);

			try {
				totalWorkingHrs = Double.parseDouble(text);
			} catch (Exception e) {

			}

			if (prjtext.contains("Leave :")) {
				System.out.println("Leave Hrs-- " + leaveHrs);
				if (totalWorkingHrs != 0) {
					if (totalWorkingHrs < leaveHrs) {
						box.setValue(0d);
						box.setText("0");
					}
					if (totalWorkingHrs < leaveHrs + workedHrs + otHrs) {
						box.setValue(0d);
						box.setText("0");
					}
				} else {
					box.setValue(0d);
					box.setText("0");
				}

			} else if (prjtext.contains("OT :")) {
				double actualOtHrs = 0;
				System.out.println("OT Hrs-- " + otHrs);
				if (totalWorkingHrs != 0) {
					actualOtHrs = 24 - totalWorkingHrs;
					System.out.println("MAX OT HRS :: " + actualOtHrs);
					if (leaveHrs == 0) {
						if (actualOtHrs < otHrs) {
							box.setValue(0d);
							box.setText("0");
						}
						if (actualOtHrs < otHrs + workedHrs) {
							box.setValue(0d);
							box.setText("0");
						}
					} else {
						box.setValue(0d);
						box.setText("0");
					}

				} else {
					actualOtHrs = 24;
					if (leaveHrs == 0) {
						if (actualOtHrs < otHrs) {
							box.setValue(0d);
							box.setText("0");
						}
						if (actualOtHrs < otHrs + workedHrs) {
							box.setValue(0d);
							box.setText("0");
						}
					} else {
						box.setValue(0d);
						box.setText("0");
					}
				}
			} else {
				System.out.println("prj Hrs " + workedHrs);
				if ((totalWorkingHrs < (leaveHrs + workedHrs))
						&& (leaveHrs != 0)) {
					System.out.println("leave Hrs 1" + leaveHrs);
					box.setValue(0d);
					box.setText("0");
				}
				if (totalWorkingHrs < workedHrs) {
					// box.setValue(0d);
					// box.setText("0");

					/**
					 * Date :30-05-2018 By ANIL This method auto adjust extra
					 * working hours against OT
					 */

					InlineLabel dateLbl = (InlineLabel) table.getWidget(0,
							boxCol);
					String dateInString = dateLbl.getText();
					System.out.println("Date: " + dateInString);

					String wrkTyp = "";
					if (totalWorkingHrs == 0) {
						InlineLabel wrkgLbl = (InlineLabel) table.getWidget(1,
								boxCol);
						wrkTyp = wrkgLbl.getText();
						System.out.println("Work Type/Day : " + wrkTyp);
					}

					adjustExtraHoursAgainstOT(boxRow, boxCol, totalWorkingHrs,
							workedHrs, wrkTyp, dateInString, prjtext);
				}
			}

		}
		fillVerticalTotal();
		fillHorizontalTotal();

	}

	private void adjustExtraHoursAgainstOT(int row, int col,
			double totalWorkingHrs, double workedHrs, String wrkTyp,
			String dateInString, String prjNm) {

		DateTimeFormat fmt = DateTimeFormat.getFormat("EEE dd MM");
		Date selectedDate = fmt.parse(dateInString);

		System.out.println("Selected DATE : " + selectedDate + " PRJ NM "
				+ prjNm);

		ArrayList<Overtime> otList = new ArrayList<Overtime>();
		double extraHours = workedHrs - totalWorkingHrs;
		System.out.println("ToT Wrkg Hrs : " + totalWorkingHrs + " Wrkd Hrs : "
				+ workedHrs + " Ext Hrs : " + extraHours);
		if (totalWorkingHrs != 0) {
			System.out.println("Global OT List : "
					+ LoginPresenter.globalOvertimeList.size());
			for (Overtime ot : LoginPresenter.globalOvertimeList) {
				if (ot.isStatus() == true && ot.isNormalDays() == true) {
					otList.add(ot);
				}
			}
			System.out.println("N Ot List : " + otList.size());
			if (otList.size() > 1) {

			} else if (otList.size() == 1) {
				insertOtEntry(otList.get(0).getName(), false, true, row + 1,
						col, extraHours, prjNm);
			} else {

			}
		} else {
			if (wrkTyp.trim().equalsIgnoreCase("WO")) {
				for (Overtime ot : LoginPresenter.globalOvertimeList) {
					if (ot.isStatus() == true && ot.isWeeklyOff() == true) {
						otList.add(ot);
					}
				}
				System.out.println("WO Ot List : " + otList.size());
				if (otList.size() > 1) {

				} else if (otList.size() == 1) {
					insertOtEntry(otList.get(0).getName(), false, true,
							row + 1, col, extraHours, prjNm);
				} else {

				}
			} else if (wrkTyp.trim().equalsIgnoreCase("HOLIDAY")) {
				for (Overtime ot : LoginPresenter.globalOvertimeList) {
					if (ot.isStatus() == true && ot.getHolidayType() != null
							&& !ot.getHolidayType().equals("")
							&& getEmpInfo() != null
							&& getEmpInfo().getLeaveCalendar() != null) {
						for (Holiday holiday : getEmpInfo().getLeaveCalendar()
								.getHoliday()) {
							if (holiday.getHolidaydate().equals(selectedDate)
									&& holiday.getHolidayType().equals(
											ot.getHolidayType())) {
								otList.add(ot);
							}
						}
					}
				}
				System.out.println("HOLIDAY Ot List : " + otList.size());
				if (otList.size() > 1) {

				} else if (otList.size() == 1) {
					insertOtEntry(otList.get(0).getName(), false, true,
							row + 1, col, extraHours, prjNm);
				} else {

				}
			}
		}

	}

	public void insertOtEntry(String projName, boolean leaveRow, boolean otRow,
			int row, int col, double hours, String prjNm) {
		System.out.println("IOE " + " PRJ NM " + prjNm);
		String modifiedProjName;

		if (leaveRow == true) {
			modifiedProjName = "Leave :" + projName;
		} else if (otRow == true) {
			modifiedProjName = "OT :" + projName;
		} else {
			modifiedProjName = projName;
		}

		int rowCount = 0;
		if (isPresent(modifiedProjName) == true) {
			if (getRowCountOfProject(modifiedProjName) != null) {
				rowCount = getRowCountOfProject(modifiedProjName);
				if (table.getWidget(rowCount, col) instanceof DoubleBox) {
					DoubleBox box = (DoubleBox) table.getWidget(rowCount, col);
					box.setValue(hours);
				}

				return;
			}
		}

		table.removeRow(table.getRowCount() - 1);
		int rowNo = table.getRowCount();

		InlineLabel label = new InlineLabel(projName);
		if (leaveRow == true) {
			label.setText("Leave :" + projName);

		} else if (otRow == true) {
			label.setText("OT :" + projName);
			/**
			 * Date: 04-06-2018 BY ANIL
			 */
			projAgainstOtMap.put(rowNo, prjNm);
		} else {
			label.setText(projName);
		}
		table.setWidget(rowNo, 0, label);
		table.getCellFormatter().getElement(rowNo, 0).getStyle()
				.setBackgroundColor("aliceblue");

		// table.getCellFormatter().getElement(rowNo,
		// 0).getStyle().setHeight(15, Unit.PX);
		// table.getWidget(rowNo, 0).setHeight("15px");

		CellFormatter formater = table.getCellFormatter();
		formater.setHorizontalAlignment(rowNo, 0, HasAlignment.ALIGN_LEFT);

		int firstRowLength = table.getCellCount(0);
		System.out.println(" insertEntry  First Row Length :: "
				+ firstRowLength);
		int entryAreaLength = firstRowLength - 2;
		System.out.println(" insertEntry  Entery Area Length :: "
				+ entryAreaLength);

		for (int k = 1; k <= entryAreaLength; k++) {
			DoubleBox db = new DoubleBox();
			db.setWidth("100%");
			/**
			 * Date : 21-05-2018 By ANIL
			 */
			// db.setHeight("30px");
			db.setHeight("15px");
			db.addKeyPressHandler(new DecimalWithSinglePrecion());
			db.addValueChangeHandler(this);
			db.setName(k + "-" + rowNo);

			if (k == col) {
				db.setValue(hours);
			}
			table.setWidget(rowNo, k, db);
			formater.setHorizontalAlignment(rowNo, k, HasAlignment.ALIGN_CENTER);

			// table.getWidget(rowNo, k).setHeight("15px");
			// table.getCellFormatter().getElement(rowNo,
			// k).getStyle().setHeight(15, Unit.PX);

		}
		table.setWidget(rowNo, firstRowLength - 1, new InlineLabel("0.0"));
		formater.setHorizontalAlignment(rowNo, firstRowLength - 1,
				HasAlignment.ALIGN_CENTER);

		// Button button = new Button("Delete");
		Button button = new Button("-");
		button.addClickHandler(this);

		table.setWidget(rowNo, firstRowLength, button);
		formater.setHorizontalAlignment(rowNo, firstRowLength,
				HasAlignment.ALIGN_CENTER);

		generateTotalRow();
		fillVerticalTotal();
		fillHorizontalTotal();
	}

	private Integer getRowCountOfProject(String projName) {
		int i = 0;
		for (i = 1; i < table.getRowCount() - 1; i++) {
			InlineLabel label = (InlineLabel) table.getWidget(i, 0);
			if (label.getText().trim().equals(projName)) {
				return i;
			}
		}

		return null;
	}

	/**
	 * This method is called when Delete button inrow is Called.
	 * 
	 */
	@Override
	public void onClick(ClickEvent event) {
		// System.out.println("ON Click ROW--- "+table.getCellForEvent(event).getRowIndex());
		// System.out.println("ON Click COL--- "+table.getCellForEvent(event).getCellIndex());
		int rowIndex = table.getCellForEvent(event).getRowIndex();
		// int colIndex = table.getCellForEvent(event).getCellIndex();

		// System.out.println("ROW-- "+rowIndex);
		// System.out.println("COL-- "+colIndex);

		Object widg = event.getSource();

		if (widg instanceof Button) {
			table.removeRow(rowIndex);
			fillVerticalTotal();
			fillHorizontalTotal();
		}

		// if (widg instanceof CheckBox) {
		// CheckBox cb = (CheckBox) widg;
		// makeVerticalColumnZero(colIndex);
		//
		// if (cb.getValue()){
		// toggleVerticalColumnState(colIndex, false);
		// }
		// else{
		// toggleVerticalColumnState(colIndex, true);
		// }
		//
		// fillVerticalTotal();
		// fillHorizontalTotal();
		//
		// }

	}

	/**
	 * This method is not required ,Commented by Anil 17-07-2015
	 */

	// /**
	// * Method which iterates the vertical column and set value of all double
	// * boxes to zero.
	// *
	// * @param colIndex Index of Column which double boxes are to be made zero
	// *
	// */
	// private void makeVerticalColumnZero(int colIndex) {
	// for (int i = 0; i < table.getRowCount(); i++) {
	// Widget widg = table.getWidget(i, colIndex);
	// if (widg instanceof DoubleBox) {
	// DoubleBox db = (DoubleBox) widg;
	// db.setValue(0.0);
	// }
	// }
	// }

	/**
	 * This method is not required ,Commented by Anil 17-07-2015
	 */

	// public void setCalendarHours(Double d) {
	// int noColumn = table.getCellCount(0);
	// double sum = 0;
	// int i = 1;
	// for (i = 1; i < noColumn - 1; i++) {
	// InlineLabel label = (InlineLabel) table.getWidget(1, i);
	// label.setText(d + "");
	// sum = sum + d;
	//
	// }
	// InlineLabel label = (InlineLabel) table.getWidget(1, i);
	// label.setText(sum + "");
	//
	// }

	/******************************** setting working hour in first row ***************************/

	/**
	 * 
	 * @param hrs
	 * @param date
	 * @param type
	 * 
	 *            This method sets working hour in 1st row of TimeReport Table.
	 *            this method is called from TimeReportPresenter
	 */
	public void setWorkinhHrs(double hrs, Date date, String type) {
		DateTimeFormat fmt = DateTimeFormat.getFormat("EEE dd MM");
		String strDate = fmt.format(date);
		int colCount = table.getCellCount(0);
		for (int i = 1; i < colCount; i++) {

			InlineLabel label = (InlineLabel) table.getWidget(0, i);
			String labelDate = label.getText().trim();
			if (labelDate.equals(strDate)) {
				InlineLabel wolabel = (InlineLabel) table.getWidget(1, i);
				if (type.trim().equals("")) {
					wolabel.setText(hrs + "");
				} else {
					wolabel.setText(type);
				}
			}
		}
	}

	/************* setting first row total *************/

	public void setWorkingHrsTotal(double d) {
		int colCount = table.getCellCount(0);
		InlineLabel wolabel = (InlineLabel) table.getWidget(1, colCount - 1);
		wolabel.setText(d + "");
	}

	/**************************************************************************************************************************************/

	public void insertDoubleBoxForLeave() {
		table.removeRow(table.getRowCount() - 1);
		int rowNo = table.getRowCount();

		int firstRowLength = table.getCellCount(0);
		System.out.println(" insertEntry  First Row Length :: "
				+ firstRowLength);
		int entryAreaLength = firstRowLength - 2;
		System.out.println(" insertEntry  Entery Area Length :: "
				+ entryAreaLength);

		CellFormatter formater = table.getCellFormatter();
		formater.setHorizontalAlignment(rowNo, 0, HasAlignment.ALIGN_LEFT);

		for (int k = 1; k <= entryAreaLength; k++) {
			DoubleBox db = new DoubleBox();
			db.setWidth("100%");
			db.setHeight("30px");
			db.addKeyPressHandler(new DecimalWithSinglePrecion());
			db.addValueChangeHandler(this);
			db.setName(k + "-" + rowNo);
			table.setWidget(rowNo, k, db);
			formater.setHorizontalAlignment(rowNo, k, HasAlignment.ALIGN_CENTER);

		}
		table.setWidget(rowNo, firstRowLength - 1, new InlineLabel("0.0"));
		formater.setHorizontalAlignment(rowNo, firstRowLength - 1,
				HasAlignment.ALIGN_CENTER);

		Button button = new Button("Delete");
		button.addClickHandler(this);

		table.setWidget(rowNo, firstRowLength, button);
		formater.setHorizontalAlignment(rowNo, firstRowLength,
				HasAlignment.ALIGN_CENTER);
	}

	/****************************** setting Leave hour in first row **********************************/
	/**
	 * 
	 * @param hrs
	 * @param date
	 * @param type
	 * 
	 *            This method sets Leave hour in 1st row of TimeReport Table.
	 *            this method is called from TimeReportPresenter
	 */
	public void setLeaveHrs(double hrs, Date date, String type, int index) {
		// table.removeRow(table.getRowCount() - 1);

		DateTimeFormat fmt = DateTimeFormat.getFormat("EEE dd MM");
		String strDate = fmt.format(date);
		int colCount = table.getCellCount(0);
		int rowCount = table.getRowCount();
		boolean flage = true;
		int rowIndex = 0;
		System.out.println("TOTAL ROW COUNT :: " + rowCount);
		System.out.println("LEAVE LIST INDEX :: " + index);

		// int firstRowLength = table.getCellCount(0);
		int entryAreaLength = colCount - 2;

		/**
		 * index is start from 0 to list size rowIndex is calculated from index
		 * start from row 3.
		 */

		if (leaveCounter == 0) {
			rowIndex = 3 + index;
			System.out.println("L CNT 0 ROW INDEX :: " + rowIndex);
		} else {
			int diff = index - leaveCounter;
			rowIndex = 3 + index - diff;
			System.out.println("L CNT !0 ROW INDEX :: " + rowIndex);
		}

		// to check particular leave already exist
		for (int i = 3; i < rowCount; i++) {
			InlineLabel leaveLabel = (InlineLabel) table.getWidget(i, 0);
			String leave = "Leave :" + type;
			if (leave.trim().equals(leaveLabel.getText().trim())) {
				System.out.println("LEAVE ALREADY EXIST :: " + leave);
				// System.out.println("Leave ---------------  ::: -------------- "+leaveLabel.getText());
				rowIndex = i;
				flage = false;
				leaveCounter = index;
			}
		}

		// To add new row in table
		if (rowCount == rowIndex) {
			System.out
					.println("ROW INDEX AND COUNT IS EQUAL CREATING NEW ROW ");

			CellFormatter formater = table.getCellFormatter();
			formater.setHorizontalAlignment(rowIndex, 0,
					HasAlignment.ALIGN_LEFT);

			table.setWidget(rowIndex, 0, new InlineLabel("Title"));
			formater.setHorizontalAlignment(rowIndex, 0,
					HasAlignment.ALIGN_CENTER);

			for (int k = 1; k <= entryAreaLength; k++) {
				DoubleBox db = new DoubleBox();
				db.setWidth("100%");
				db.setHeight("30px");
				db.addKeyPressHandler(new DecimalWithSinglePrecion());
				db.addValueChangeHandler(this);
				db.setName(k + "-" + rowIndex);
				db.setValue(0d);
				table.setWidget(rowIndex, k, db);
				formater.setHorizontalAlignment(rowIndex, k,
						HasAlignment.ALIGN_CENTER);

			}
			table.setWidget(rowIndex, colCount - 1, new InlineLabel("0.0"));
			formater.setHorizontalAlignment(rowIndex, colCount - 1,
					HasAlignment.ALIGN_CENTER);

			Button button = new Button("Delete");
			button.addClickHandler(this);

			table.setWidget(rowIndex, colCount, button);
			formater.setHorizontalAlignment(rowIndex, colCount,
					HasAlignment.ALIGN_CENTER);

			// int noOfRow = table.getRowCount();
			// noOfRow = table.getRowCount();
			// int noOfColumn = table.getCellCount(0);
			// CellFormatter formater = table.getCellFormatter();
			// table.setWidget(noOfRow, 0, new InlineLabel(" "));
			// formater.setHorizontalAlignment(noOfRow, 0,
			// HasAlignment.ALIGN_LEFT);
			// for (int i = 1; i < noOfColumn; i++) {
			//
			// table.setWidget(noOfRow, i, new InlineLabel("0.0"));
			// formater.setHorizontalAlignment(noOfRow,
			// i,HasAlignment.ALIGN_CENTER);
			// }

		}

		/**
		 * if condition generate leave row else condition update existing row.
		 */
		if (flage == true) {
			for (int i = 1; i < colCount; i++) {
				System.out.println("LEAVE ----" + type);
				InlineLabel leaveLabel = (InlineLabel) table.getWidget(
						rowIndex, 0);
				leaveLabel.setText("Leave :" + type);

				InlineLabel label = (InlineLabel) table.getWidget(0, i);
				String labelDate = label.getText().trim();
				if (labelDate.equals(strDate)) {

					// InlineLabel wolabel = (InlineLabel)
					// table.getWidget(rowIndex, i);
					// wolabel.setText(hrs + "");

					if (table.getWidget(rowIndex, i) instanceof DoubleBox) {
						DoubleBox box = (DoubleBox) table
								.getWidget(rowIndex, i);
						box.setValue(hrs);
					}

				}
			}
		} else {
			for (int i = 1; i < colCount; i++) {
				InlineLabel leaveLabel = (InlineLabel) table.getWidget(
						rowIndex, 0);
				leaveLabel.setText("Leave :" + type);

				InlineLabel label = (InlineLabel) table.getWidget(0, i);
				String labelDate = label.getText().trim();
				if (labelDate.equals(strDate)) {
					// InlineLabel wolabel = (InlineLabel)
					// table.getWidget(rowIndex, i);
					// if(hrs!=0){
					// wolabel.setText(hrs + "");
					if (table.getWidget(rowIndex, i) instanceof DoubleBox) {
						DoubleBox box = (DoubleBox) table
								.getWidget(rowIndex, i);
						if (box.getValue() != null) {
							box.setValue(hrs);
						}
					}
					// }
				}
			}
		}
		setLeaveHrsTotal();
	}

	public void setLeaveHrsTotal() {
		// System.out.println("inside total method::: ");
		int rowCount = table.getRowCount();
		int colCount = table.getCellCount(0);

		for (int i = 3; i < rowCount; i++) {
			double sum = 0;
			for (int j = 1; j < colCount - 1; j++) {
				// InlineLabel wolabel = (InlineLabel) table.getWidget(i, j);
				// sum+=Double.parseDouble(wolabel.getText());

				if (table.getWidget(i, j) instanceof InlineLabel) {
					InlineLabel wolabel = (InlineLabel) table.getWidget(i, j);
					sum += Double.parseDouble(wolabel.getText());
				}
				if (table.getWidget(i, j) instanceof DoubleBox) {
					DoubleBox box = (DoubleBox) table.getWidget(i, j);
					if (box.getValue() != null) {
						sum += box.getValue();
					}
				}

				// System.out.println("value "+sum);
			}
			InlineLabel wolabel = (InlineLabel) table
					.getWidget(i, colCount - 1);
			// System.out.println("Sum  "+sum);
			wolabel.setText(sum + "");
		}
	}

	/**
	 * This method is called from presenter
	 */
	public void generateTotalRowWithTotal() {

		int noOfRow = table.getRowCount();
		noOfRow = table.getRowCount();
		int noOfColumn = table.getCellCount(0);
		CellFormatter formater = table.getCellFormatter();
		table.setWidget(noOfRow, 0, new InlineLabel("Total"));
		formater.setHorizontalAlignment(noOfRow, 0, HasAlignment.ALIGN_LEFT);
		for (int i = 1; i < noOfColumn; i++) {

			table.setWidget(noOfRow, i, new InlineLabel("0.0"));
			formater.setHorizontalAlignment(noOfRow, i,
					HasAlignment.ALIGN_CENTER);
		}

		int rowCount = table.getRowCount();
		int colCount = table.getCellCount(0);

		for (int j = 1; j < colCount - 1; j++) {
			double sum = 0;
			for (int i = 3; i < rowCount - 1; i++) {

				// InlineLabel wolabel = (InlineLabel) table.getWidget(i, j);
				// sum+=Double.parseDouble(wolabel.getText());

				if (table.getWidget(i, j) instanceof DoubleBox) {
					DoubleBox box = (DoubleBox) table.getWidget(i, j);
					sum += box.getValue();
				}

				// System.out.println("value "+sum);
			}
			InlineLabel wolabel = (InlineLabel) table
					.getWidget(rowCount - 1, j);
			// System.out.println("Sum  "+sum);
			wolabel.setText(sum + "");
		}

		for (int i = rowCount - 1; i < rowCount; i++) {
			double sum = 0;
			for (int j = 1; j < colCount - 1; j++) {
				InlineLabel wolabel = (InlineLabel) table.getWidget(i, j);
				sum += Double.parseDouble(wolabel.getText());
				// System.out.println("value "+sum);
			}
			InlineLabel wolabel = (InlineLabel) table
					.getWidget(i, colCount - 1);
			// System.out.println("Sum  "+sum);
			wolabel.setText(sum + "");
		}

	}

	/**********************************************************************************************************/

	public double getOTHrsSum(int col) {
		double sum = 0;
		for (int i = 3; i < table.getRowCount() - 1; i++) {
			if (isOTRow(i)) {
				DoubleBox doub = (DoubleBox) table.getWidget(i, col);
				if (doub.getValue() != null) {
					sum = sum + doub.getValue();
				}
			}
		}
		return sum;
	}

	public double getWorkingHrsSum(int col) {
		double sum = 0;
		for (int i = 3; i < table.getRowCount() - 1; i++) {
			if (isLeaveRow(i)) {
				;
			} else if (isOTRow(i)) {
				;
			} else {
				DoubleBox doub = (DoubleBox) table.getWidget(i, col);
				if (doub.getValue() != null) {
					sum = sum + doub.getValue();
				}
			}
		}
		return sum;
	}

	private boolean isLeaveRow(int row) {
		InlineLabel label = (InlineLabel) table.getWidget(row, 0);
		String text = label.getText();
		if (text.contains("Leave :")) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isOTRow(int row) {
		InlineLabel label = (InlineLabel) table.getWidget(row, 0);
		String text = label.getText();
		if (text.contains("OT :")) {
			return true;
		} else {
			return false;
		}
	}

	/*************************************************************************************************/
	/**
	 * 27 june 2015 by anil
	 */
	public double getLeaveHrSum(int col) {
		double sum = 0;
		for (int i = 3; i < table.getRowCount() - 1; i++) {
			if (isLeaveRow(i)) {

				Widget wb = table.getWidget(i, col);
				if (wb instanceof DoubleBox) {
					System.out.println("LEV HR SUM INSIDE DBLBOX...");
					DoubleBox doub = (DoubleBox) table.getWidget(i, col);
					System.out.println("row value :: " + doub.getValue());
					if (doub.getValue() != null) {
						sum = sum + doub.getValue();
					}

				} else {
					System.out.println("LEV HR SUM INSIDE Else OF DBLBOX...");
					Widget hrInWid = table.getWidget(i, col);
					System.out.println("Widget Hr :: " + hrInWid);
					String hrInWidStr = hrInWid.toString();
					System.out.println("String Wid hr :: " + hrInWidStr);
					String hrInStr = hrInWidStr.replace(
							"<span class=" + "\"" + "gwt-InlineLabel" + "\""
									+ ">", "").replace("</span>", "");
					System.out.println("String Hr :: " + hrInStr);
					double totalWorkingHr = Double.parseDouble(hrInStr.trim());

					sum = sum + totalWorkingHr;

				}
			} else {

			}
		}
		return sum;

	}

	/**************************** Time Report GET and SET value Method **************************************/

	/****************************** TimeSheetLineItem getValue method **********************************/

	@Override
	public List<TimeSheetLineItem> getValue() {
		// System.out.println("Inside composite getValue()...");
		int rowCount = table.getRowCount();
		// System.out.println("Table row count  :: "+rowCount);

		List<TimeSheetLineItem> lineItems = new ArrayList<TimeSheetLineItem>();

		// System.out.println("composite table Loop Start here....");

		for (int travRow = 3; travRow < rowCount - 1; travRow++) {

			TimeSheetLineItem banana = new TimeSheetLineItem();

			InlineLabel labelProject = (InlineLabel) table
					.getWidget(travRow, 0);
			String projName = labelProject.getText();

			banana.setProjName(projName);

			// System.out.println("Project Name :: "+projName+" "+travRow);

			if (projName.contains("Leave :")) {
				banana.isLeave = true;
			}
			if (projName.contains("OT :")) {
				banana.isOvertime = true;
				/**
				 * Date : 04-06-2018 By ANIL setting project name against which
				 * employee had worked extra
				 */
				if (projAgainstOtMap != null
						&& projAgainstOtMap.get(travRow) != null) {
					banana.setProjAgainstOt(projAgainstOtMap.get(travRow));
				}
			}

			ArrayList<HourWorked> komal = new ArrayList<HourWorked>();

			int travColumn = 1;

			int noDays = CalendarUtil.getDaysBetween(fromDate, toDate);
			Date from = CalendarUtil.copyDate(fromDate);

			// System.out.println("Table row :: "+travRow);

			for (int i = 1; i <= noDays + 1; i++) {

				// System.out.println("Table Col :: "+travColumn);

				HourWorked xyz = new HourWorked();

				xyz.date = CalendarUtil.copyDate(from);

				Widget widg = table.getWidget(travRow, travColumn);

				if (widg instanceof DoubleBox) {
					DoubleBox dBox = (DoubleBox) table.getWidget(travRow,
							travColumn);

					if (dBox.getValue() == null) {
						xyz.hoursWorked = 0;
					} else {
						xyz.hoursWorked = dBox.getValue();
					}

				} else {
					/**
					 * these else loop is defined for the leave which is created
					 * automatically. 27 June 2015 By Anil
					 */

					// System.out.println("inside Else loop of saving system created leave ....");
					// System.out.println("Widget Hr :: "+widg);
					String hrInWidStr = widg.toString();
					// System.out.println("String Wid hr :: "+hrInWidStr);
					String hrInStr = hrInWidStr.replace(
							"<span class=" + "\"" + "gwt-InlineLabel" + "\""
									+ ">", "").replace("</span>", "");
					// System.out.println("String Hr :: "+hrInStr);

					double totalWorkingHr = Double.parseDouble(hrInStr.trim());

					xyz.hoursWorked = totalWorkingHr;

				}

				if (projName.contains("Leave :")) {
					banana.setLeave(true);
					xyz.isLeaveDate = true;
				}

				if (projName.contains("OT :")) {
					banana.setOvertime(true);
					xyz.isOverTime = true;
				}
				komal.add(xyz);

				CalendarUtil.addDaysToDate(from, 1);

				travColumn++;

			}
			banana.setHoursWorked(komal);
			lineItems.add(banana);

		}
		// System.out.println("Line Item Size :: "+lineItems.size());
		return lineItems;
	}

	/******************************** Composite setValue() Method *********************************/

	@Override
	public void setValue(List<TimeSheetLineItem> value) {
		// System.out.println("Inside Composite setvalue method......");
		// System.out.println("From Date :: "+fromDate);
		// System.out.println("To Date :: "+toDate);
		// First Change The Header that is Date rows
		int noDays = CalendarUtil.getDaysBetween(fromDate, toDate);
		// System.out.println("No. of Days :: "+noDays);

		Date tempDate = CalendarUtil.copyDate(fromDate);

		for (int i = 1; i <= noDays + 1; i++) {

			DateTimeFormat fmt = DateTimeFormat.getFormat("EEE dd MM");
			String day = fmt.format(tempDate);
			// System.out.println("Day :: "+day);
			InlineLabel label = (InlineLabel) table.getWidget(0, i);
			label.setText(day);
			CalendarUtil.addDaysToDate(tempDate, 1);

		}
		// Now Clear Shift Hours and Machine Hours

		for (int i = 1; i <= noDays + 1; i++) {

			InlineLabel label = (InlineLabel) table.getWidget(1, i);
			label.setText("0.0");
			label = (InlineLabel) table.getWidget(1, i);
			label.setText("0.0");

		}

		// Now remove all rows including total
		for (int i = table.getRowCount() - 1; i > 3; i--)
			table.removeRow(i);

		// Start inserting project rows after insertion set values
		int row = 3;
		for (TimeSheetLineItem tempo : value) {

			Collections.sort(tempo.getHoursWorked());

			if (tempo.isLeave) {
				String projName = tempo.getProjName();
				if (projName != null) {
					projName = projName.replace("Leave :", "");
				}
				insertEntry(projName, true, false,"");// ////////////////////////////////////////////////
				// System.out.println("LEAVE " + tempo.getProjName());
			} else if (tempo.isOvertime) {
				String projName = tempo.getProjName();
				if (projName != null) {
					projName = projName.replace("OT :", "");
				}
				/**
				 * Date : 04-06-2018 By ANIL
				 */
				insertEntry(projName, false, true,tempo.getProjAgainstOt());// ////////////////////////////////////////////////
				// System.out.println("OverTime " + tempo.getProjName());
			} else {
				insertEntry(tempo.getProjName(), false, false,"");// /////////////////////////////////////////
			}

			for (int col = 1; col <= 7; col++) {
				{
					// System.out.println("Proj " + tempo.getProjName());
					DoubleBox box = (DoubleBox) table.getWidget(row, col);
					if (col <= tempo.getHoursWorked().size()) {
						HourWorked any = tempo.getHoursWorked().get(col - 1);
						box.setValue(any.hoursWorked);
					}
				}
			}
			row++;
		}
		fillVerticalTotal();
		fillHorizontalTotal();

	}

	/**************************/

	@Override
	public void setValue(List<TimeSheetLineItem> value, boolean fireEvents) {

	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<List<TimeSheetLineItem>> handler) {
		return null;
	}

	/**************************************
	 * Getter and Setter
	 * 
	 * @return
	 ******************************************/

	// public static void removeLastRowOfTable(){
	// table.removeRow(table.getRowCount() - 1);
	// }

	public FlexTable getTable() {
		return table;
	}

	public ScrollPanel getPanel() {
		return panel;
	}

	public void setPanel(ScrollPanel panel) {
		this.panel = panel;
	}

	public void setTable(FlexTable table) {
		this.table = table;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		// Date periodStartDate = CalendarUtil.copyDate(fromDate);
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		// Date periodEndDate = CalendarUtil.copyDate(toDate);
		this.toDate = toDate;
	}

}
