package com.slicktechnologies.client.views.humanresource.timereport;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;


import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.*;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar.WeekleyOff;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReportEntrey;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.calendar.CalendarPresenter;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.CalendarUtil;

/**
 * FormScreen presenter template.
 */
public class TimeReportPresenter extends ApprovableFormScreenPresenter<TimeReport> implements SelectionHandler<Suggestion>, ChangeHandler {

	// Token to set the concrete FormScreen class name
	final GenricServiceAsync async = GWT.create(GenricService.class);
	public static TimeReportForm form;
	LeaveBalance leaveBalance;
	ArrayList<AllocatedLeaves> leaveAlloc;
	public  ArrayList<LeaveApplication> appliedLeaveAppl;
	int monthSize=0;
	
	EmployeeInfo empInfo;
	Date frmDate;
	Date tooDate;
	
	public TimeReportPresenter(FormScreen<TimeReport> view, TimeReport model) {
		super(view, model);

		form = (TimeReportForm) view;

		if (UserConfiguration.getInfo() != null) {
			form.pic.setValue(UserConfiguration.getInfo());
			form.pic.setEnable(false);
			/**
			 * Date : 21-05-2018 BY ANIL
			 */
			form.tabPanel.selectTab(1);
			
			setLeaveBalanceInformation(UserConfiguration.getInfo());
//			if(UserConfiguration.getInfo().getOvertime()!=null){
//				form.olbOvertime.clear();
//				form.olbOvertime.addItem("--SELECT--");
//				form.olbOvertime.addItem(UserConfiguration.getInfo().getOvertime().getName());
//			}
			/**
			 * Date : 01-06-2018 By ANIL
			 */
			form.composite.setEmpInfo(UserConfiguration.getInfo());
		}else{
			/**
			 * Date : 21-05-2018 BY ANIL
			 */
			form.tabPanel.selectTab(1);
		}

		Date startDate = calculatePeriodStartDate(new Date());
		Date endDate = calculatePeriodEndDate(new Date());
		
		System.out.println("@@@       WK STRT DT   :  "+startDate);
		System.out.println("@@@       WK END DT   :  "+endDate);
		
		Console.log("@@@       WK STRT DT   :  "+startDate);
		Console.log("@@@       WK END DT   :  "+endDate);
		
		form.getDbFromDate().setValue(startDate);
		form.getDbToDate().setValue(endDate);

		form.getComposite().getTable().addClickHandler(this);
		form.getAddProject().addClickHandler(this);
		form.addLeave.addClickHandler(this);
		form.addOvertime.addClickHandler(this);
		
		getMonth(startDate,endDate);

		MyQuerry querry = getTimeReportQuerry(startDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
		setTimeReportView(querry, startDate, endDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
		
		getAppliedLeaveApplication(Integer.parseInt(form.pic.getId().getValue()));
		
		form.pic.addSelectionHandler(this);
		form.getOlbProject().addChangeHandler(this);
		form.olbLeaveType.addChangeHandler(this);

	}
	
	
	/**
	 * Date :12-05-2018 BY ANIL
	 * this constructor is initialized from approval presenter at the time of viewing document
	 */
	
	public TimeReportPresenter(FormScreen<TimeReport> view, TimeReport model,boolean isCalledFromApprovalProcess) {
		super(view, model);

		form = (TimeReportForm) view;

//		if (model.get != null) {
//			form.pic.setValue(UserConfiguration.getInfo());
//			form.pic.setEnable(false);
			EmployeeInfo info=new EmployeeInfo();
			info.setEmpCount(model.getEmpid());
			setLeaveBalanceInformation(info);
			/**
			 * Date : 21-05-2018 BY ANIL
			 */
			form.tabPanel.selectTab(1);
//			if(UserConfiguration.getInfo().getOvertime()!=null){
//				form.olbOvertime.clear();
//				form.olbOvertime.addItem("--SELECT--");
//				form.olbOvertime.addItem(UserConfiguration.getInfo().getOvertime().getName());
//			}
//		}

//		Date startDate = calculatePeriodStartDate(new Date());
//		Date endDate = calculatePeriodEndDate(new Date());
			
		Date startDate=CalendarUtil.copyDate(model.getFromdate());	
		Date endDate=CalendarUtil.copyDate(model.getTodate());
//		System.out.println("@@@       WK STRT DT   :  "+startDate);
//		System.out.println("@@@       WK END DT   :  "+endDate);
		
//		Console.log("@@@       WK STRT DT   :  "+startDate);
//		Console.log("@@@       WK END DT   :  "+endDate);
		
		form.getDbFromDate().setValue(startDate);
		form.getDbToDate().setValue(endDate);

//		form.getComposite().getTable().addClickHandler(this);
//		form.getAddProject().addClickHandler(this);
//		form.addLeave.addClickHandler(this);
//		form.addOvertime.addClickHandler(this);
		
		getMonth(startDate,endDate);

		MyQuerry querry = getTimeReportQuerry(startDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
		setTimeReportView(querry, startDate, endDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
		
		getAppliedLeaveApplication(model.getEmpid());
		
//		form.pic.addSelectionHandler(this);
//		form.getOlbProject().addChangeHandler(this);
//		form.olbLeaveType.addChangeHandler(this);

	}
	
	/******************************************* Initialize Method 
	 * @return *******************************************************/
	public static TimeReportForm initalize() {

		TimeReportForm form = new TimeReportForm();
		TimeReportSearchTable gentable = new TimeReportSearchTable();
		gentable.setView(form);
		gentable.applySelectionModle();
		TimeReportSearchForm.staticSuperTable = gentable;
		SearchPopUpScreen<TimeReport> searchpopup = new TimeReportSearchForm();
		form.setSearchpopupscreen(searchpopup);

		TimeReportPresenter presenter = new TimeReportPresenter(form,new TimeReport());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	/********************************* React To Process Bar Event *********************************************************/

	@Override
	public void reactToProcessBarEvents(ClickEvent event) {
			System.out.println("ajay");
			InlineLabel label = (InlineLabel) event.getSource();
			if (label.getText().equals("Previous")) {
				reactToPrevious();
			}
			if (label.getText().equals("Next")) {
				reactToNext();
			}
			
			if (label.getText().equals(ManageApprovals.APPROVALREQUEST)){
				if(checkingLeaveApplicationStatus()){
					if(validBalanceLeave()){
						System.out.println("REQ4APP FRM1 :: "+form.dbFromDate.getValue());
						System.out.println("REQ4APP TO1  :: "+form.dbToDate.getValue());
						/**
						 * Date :04-05-2018 BY ANIL
						 * Setting employee id and employee name in approval request
						 */
						
						form.getManageapproval().setBpId(form.getPic().getEmployeeId()+"");
						form.getManageapproval().setBpName(form.pic.getEmployeeName());
						/**
						 * End
						 */
						
						form.getManageapproval().reactToRequestForApproval();
					}
				}else{
					form.showDialogMessage("Leave Application is in requested status. Please approve it. ");
				}
			}
			if (label.getText().equals(ManageApprovals.CANCELAPPROVALREQUEST)){
				form.getManageapproval().reactToApprovalRequestCancel();
			}
	}
	
	public boolean checkingLeaveApplicationStatus(){
		System.out.println("INSIDE CHECKING LEV APP STATUS");
		Date fromDate=form.getDbFromDate().getValue();
		Date toDate=form.getDbToDate().getValue();
		
		System.out.println("FROM DATE :: "+fromDate);
		System.out.println("TO DATE :: "+toDate);
		System.out.println("LEAV APP SIZE ::: "+appliedLeaveAppl.size());
		if(appliedLeaveAppl.size()!=0){
			
			for(int i=0;i<appliedLeaveAppl.size();i++){
				Date appliedLevFrmDt=appliedLeaveAppl.get(i).getFromdate();
				Date appliedLevToDt=appliedLeaveAppl.get(i).getTodate();
				
				
				if(appliedLevFrmDt.equals(fromDate)){
					return false;
				}
				if(appliedLevToDt.equals(fromDate)){
					return false;
				}
				if (appliedLevFrmDt.after(fromDate) && appliedLevFrmDt.before(toDate)){
					return false;
				}
				if(appliedLevFrmDt.equals(toDate)){
					return false;
				}
				if(appliedLevToDt.equals(toDate)){
					return false;
				}
				if (appliedLevToDt.after(fromDate) && appliedLevToDt.before(toDate)){
					return false;
				}
			}
			
		}
		
		return true;
	}
	
	public boolean validBalanceLeave(){
//		System.out.println("REQ4APP FRM :: "+form.dbFromDate.getValue());
//		System.out.println("REQ4APP TO  :: "+form.dbToDate.getValue());
		EmployeeInfo info=form.pic.getValue();
		ArrayList<TimeReportEntrey> tymReport=model.getTimeReportEntry();
//		System.out.println("TIME REPORT SIZE :: "+tymReport.size());
		
		List<AllocatedLeaves> leaveTable=form.leaveInformationTable.getDataprovider().getList();
//		System.out.println("LEAVE TABLE SIZE :: "+leaveTable.size());
		
		ArrayList<TimeReportLeave> leaveDetails=new ArrayList<TimeReportLeave>();
		
		
		
		if(tymReport.size()!=0){
			for(int i=0;i<tymReport.size();i++){
				
				if(tymReport.get(i).isEmployeeOnLeave()){
					boolean flag=false;
					if(leaveDetails.size()==0){
						TimeReportLeave trl=new TimeReportLeave();
						trl.setLeaveName(tymReport.get(i).getProjectName());
						trl.setLeavehrs(tymReport.get(i).getProjhours());
						
						leaveDetails.add(trl);
					}else{
						for(int j=0;j<leaveDetails.size();j++){
							if(leaveDetails.get(j).getLeaveName().trim().equals(tymReport.get(i).getProjectName().trim())){
								double leavevhr=leaveDetails.get(j).getLeavehrs();
								leavevhr=leavevhr+tymReport.get(i).getProjhours();
								leaveDetails.get(j).setLeavehrs(leavevhr);
								flag=true;
							}
						}
						if(flag==false){
							TimeReportLeave trl=new TimeReportLeave();
							trl.setLeaveName(tymReport.get(i).getProjectName());
							trl.setLeavehrs(tymReport.get(i).getProjhours());
							leaveDetails.add(trl);
						}
					}
					
				}
			}
			
			
//			System.out.println("LEAVE DETAILS LIST SIZE "+leaveDetails.size());
//			
//			for(int i=0;i<leaveDetails.size();i++){
//				System.out.println("Leave Name :: "+leaveDetails.get(i).getLeaveName());
//				System.out.println("Leave Hours :: "+leaveDetails.get(i).getLeavehrs());
//				System.out.println();
//			}
			
			if(leaveDetails.size()!=0){
//				System.out.println("inside VALIDATE -----");
				for(int i=0;i<leaveTable.size();i++){
					for(int j=0;j<leaveDetails.size();j++){
//						System.out.println("TABLE LEAVE NAME --- "+leaveTable.get(i).getName());
//						System.out.println(" LEAVE NAME --- "+leaveDetails.get(j).getLeaveName());
						String strTwo=leaveDetails.get(j).getLeaveName().replace("Leave :", "");
//						System.out.println("MOdified String ;; "+strTwo);
						if(leaveTable.get(i).getName().equals(strTwo.trim())){
//							System.out.println("HIIIIIIIII "+leaveDetails.get(j).getLeaveName());
//							System.out.println("HIIIIIIIII "+leaveTable.get(i).getBalance());
//							System.out.println("Working Hrs "+info.getLeaveCalendar().getWorkingHours());
							
							double levhrs=leaveTable.get(i).getBalance()*info.getLeaveCalendar().getWorkingHours();
//							System.out.println("TOTAL LEAVE BALANCE -- "+levhrs);
							
							if(levhrs<leaveDetails.get(j).getLeavehrs()){
								form.showDialogMessage("Leave applied days exceeds Leave balance !");
								return false;
							}
							
						}
					}
				}
			}
			
			
		}
		return true;
	}

	/********************************* React To Print *********************************************************/

	@Override
	public void reactOnPrint() {

	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model = new TimeReport();
	}

	@Override
	public void reactOnDownload() {
		CsvServiceAsync csvservice = GWT.create(CsvService.class);
		ArrayList<TimeReport> leavearray = new ArrayList<TimeReport>();
		List<TimeReport> list = (List<TimeReport>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		leavearray.addAll(list);
		csvservice.setTimeSheetList(leavearray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 54;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	/********************************* React To Email *********************************************************/
	@Override
	public void reactOnEmail() {

	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);

		if (event.getSource() == form.addProject) {
			if (form.olbProject.getValue() != null) {
				String proj = form.getOlbProject().getValue();
				form.getComposite().insertEntry(proj, false,false,"");
				form.getOlbProject().setSelectedIndex(0);
				makesTimeReportReadOnly();
			} else {

			}
		} 
		if(event.getSource() == form.addLeave) {
			String LeaveType = form.olbLeaveType.getValue();
			if (LeaveType != null) {
				form.getComposite().insertEntry(LeaveType, true,false,"");
				form.olbLeaveType.setSelectedIndex(0);
				makesTimeReportReadOnly();
			}
		}
		if(event.getSource() == form.addOvertime) {
			/**
			 * Date : 04-06-2018 By ANIL
			 */
			if(form.olbProject.getSelectedIndex()==0){
				form.showDialogMessage("Please select project first.");
				return;
			}
			if (form.olbOvertime.getSelectedIndex()!=0) {
				String LeaveType = form.olbOvertime.getValue(form.olbOvertime.getSelectedIndex());
				/**
				 * Date : 04-06-2018 By ANIL
				 */
				form.getComposite().insertEntry(LeaveType, false,true,form.olbProject.getValue());
				form.olbOvertime.setSelectedIndex(0);
				makesTimeReportReadOnly();
			}
		}

	}


	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		Date startDate = CalendarUtil.copyDate(form.getDbFromDate().getValue());
		Date endDate = CalendarUtil.copyDate(form.getDbToDate().getValue());
		
		getMonth(startDate, endDate);
		
		MyQuerry querry = getTimeReportQuerry(startDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
		setTimeReportView(querry, startDate, endDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
		if (form.getPic().getValue() != null) {
			/**
			 * Date : 01-06-2018 By ANIL
			 */
//			EmployeeInfo info = UserConfiguration.getInfo();
			EmployeeInfo info = form.getPic().getValue();
			
			form.composite.setEmpInfo(info);
			
			if (form.getOtherButoon().getValue() == true) {
				info = form.getPic().getValue();
				setLeaveBalanceInformation(info);
//				if(info.getOvertime()!=null){
//					form.olbOvertime.clear();
//					form.olbOvertime.addItem("--SELECT--");
//					form.olbOvertime.addItem(info.getOvertime().getName());
//				}
			} else {
				setLeaveBalanceInformation(info);
//				if(info.getOvertime()!=null){
//					form.olbOvertime.clear();
//					form.olbOvertime.addItem("--SELECT--");
//					form.olbOvertime.addItem(info.getOvertime().getName());
//				}
			}
		}
		getAppliedLeaveApplication(Integer.parseInt(form.pic.getId().getValue()));
	}

	@Override
	public void onChange(ChangeEvent event) {
		if (event.getSource() == form.getOlbProject()) {
			if (form.getOlbProject().getSelectedIndex() != 0)
				form.olbLeaveType.setSelectedIndex(0);
		}

		if (event.getSource() == form.olbLeaveType) {
			if (form.olbLeaveType.getSelectedIndex() != 0)
				form.getOlbProject().setSelectedIndex(0);
		}
		
	}

	
	
	
	public void getMonth(Date startDate,Date endDate) {
		
		System.out.println("INSIDE GET MONTH ........ ");
		
		Date currStDate = CalendarUtil.copyDate(startDate);
		Date currEdDate = CalendarUtil.copyDate(endDate);
		
		System.out.println("Current ST Date ::: "+currStDate);
		System.out.println("Current ED Date ::: "+currEdDate);
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MM-dd");
		
		int noDays = CalendarUtil.getDaysBetween(currStDate, currEdDate);
		noDays++;

		HashSet<Integer> hset =new HashSet<Integer>();
		for (int i = 1; i < noDays + 1; i++) {
			Date demo=currStDate;
			format = DateTimeFormat.getFormat("M");
			String month = format.format(demo);
			int intMonth = Integer.parseInt(month);
			hset.add(intMonth);
			CalendarUtil.addDaysToDate(currStDate, 1);
		}
		
		this.setMonthSize(hset.size());
		
		Console.log("MONTH LIST SIZE : "+getMonthSize());
		
		ArrayList<Integer> hset2List =new ArrayList<Integer>();
		hset2List.addAll(hset);
		
		form.lbMonth.clear();
		for(int i=0;i<hset2List.size();i++){
			String month=getMonthName(hset2List.get(i));
			form.lbMonth.addItem(month);
		}
		int lbsize=hset2List.size();
		System.out.println(" SIZEEEE :: "+lbsize);
//		if(lbsize>1){
//			form.lbMonth.setEnabled(true);
//		}
		
	}

	
	public String getMonthName(int monthNo){
		String monthName=null;
		
		switch(monthNo)
		{
		case 1:
			monthName="January";
			break;
		case 2:
			monthName="February";
			break;
		case 3:
			monthName="March";
			break;
		case 4:
			monthName="April";
			break;
		case 5:
			monthName="May";
			break;
		case 6:
			monthName="June";
			break;
		case 7:
			monthName="July";
			break;
		case 8:
			monthName="August";
			break;
		case 9:
			monthName="September";
			break;
		case 10:
			monthName="October";
			break;
		case 11:
			monthName="November";
			break;
		case 12:
			monthName="December";
			break;
			
		}
		
		return monthName;
	}
	
	
	
	public String calulateEndDateOfMonth(int selectedMonth){
		DateTimeFormat fmt = DateTimeFormat.getFormat("yyyy-MM-dd");
		System.out.println("Inside End Month ");
		System.out.println("Selected Month "+selectedMonth);
		String endDate;
		
		Date date = new Date();
		
		date.setMonth(selectedMonth+1);
		CalendarUtil.setToFirstDayOfMonth(date);
		CalendarUtil.addDaysToDate(date, -1);
		endDate=fmt.format(date);
		Console.log("END DATE OF MONTH "+endDate);
		return endDate;
	
	}
	
	public String calulateStartDateOfMonth(int selectedMonth){
		DateTimeFormat fmt = DateTimeFormat.getFormat("yyyy-MM-dd");
		System.out.println("Inside Start Month ");
		System.out.println("Selected Month "+selectedMonth);
		String startDate;
		
		Date date = new Date();
		date.setDate(1);
		date.setMonth(selectedMonth);
		
		startDate=fmt.format(date);
		
		System.out.println("Start Date Of Month "+startDate);
		Console.log("START DATE OF MONTH "+startDate);
		return startDate;
	
	}
	
	public int getMonthNumber(String monthName) {
	     int monthNo;
	     switch (monthName) {
	         case "January":
	        	 monthNo =0;
	             break;
	         case "February":
	        	 monthNo =1;
	             break;
	         case "March":
	        	 monthNo =2;
	             break;
	         case "April":
	        	 monthNo =3;
	             break;
	         case "May":
	        	 monthNo =4;
	             break;
	         case "June":
	        	 monthNo =5;
	             break;
	         case "July":
	        	 monthNo =6;
	             break;
	         case "August":
	        	 monthNo =7;
	             break;
	         case "September":
	        	 monthNo =8;
	             break;
	         case "October":
	        	 monthNo =9;
	             break;
	         case "November":
	        	 monthNo =10;
	             break;
	         case "December":
	        	 monthNo =11;
	             break;
	             
	         default:
	             throw new IllegalArgumentException("Invalid Month " );
	     }
	     return monthNo;
	}
	
	
	public void makesTimeReportReadOnly(){
		Console.log("ISIDE MAKE READ ONLY METHOD");
		System.out.println("***        MAKES TR STARTTT        ***");
		
		System.out.println("&&&        DB FRM DT   : "+form.getDbFromDate().getValue());
		System.out.println("&&&        DB TOO DT   : "+form.getDbToDate().getValue());
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MM-dd");
		
		String Date;
		Date fromDate;
		Date toDate;
		if(form.lbMonth.getSelectedIndex()==0){
			System.out.println(" INDEX 0 ");
			Date=calulateEndDateOfMonth(getMonthNumber(form.lbMonth.getValue(form.lbMonth.getSelectedIndex())));
			fromDate=format.parse(Date);
			CalendarUtil.addDaysToDate(fromDate, 1);
			toDate= CalendarUtil.copyDate(form.dbToDate.getValue());
			
		}else{
			System.out.println(" INDEX 1 ");
			Date=calulateStartDateOfMonth(getMonthNumber(form.lbMonth.getValue(form.lbMonth.getSelectedIndex())));
			fromDate = CalendarUtil.copyDate(form.dbFromDate.getValue());
			toDate=format.parse(Date);
			CalendarUtil.addDaysToDate(toDate, -1);
			
		}
		System.out.println("MONTH FROM DATE :: "+fromDate);
		System.out.println("MONTH TO DATE :: "+toDate);
		
		int noDays = CalendarUtil.getDaysBetween(fromDate, toDate);
		noDays++;
		
		System.out.println("NO OF DAYS IN BETWEEN :: "+noDays);
		
		for(int i=0;i<noDays;i++){
			if(fromDate.equals(toDate)||fromDate.before(toDate)){
				form.getComposite().setTableRowReadOnly(fromDate);
			}
			CalendarUtil.addDaysToDate(fromDate, 1);
		}
		System.out.println("***        MAKES TR ENDDD        ***");
	}
	
	/********************************** calculate Start Date Of Week **************************************************/

	/**
	 * 
	 * @param date
	 * @return This method return start date of week. this method is called in
	 *         constructor. this method is also called in search form.
	 */
	public static Date calculatePeriodStartDate(Date date) {
		Date currDate = date;
		System.out.println("Current Date ::: "+currDate);
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MM-dd");
		String dates = format.format(date);
		currDate = format.parse(dates);

		format = DateTimeFormat.getFormat("c");
		String dayOfWeek = format.format(date);
		System.out.println("Day Of week..............."+dayOfWeek);
		int intdayOfWeek = Integer.parseInt(dayOfWeek);

		CalendarUtil.addDaysToDate(currDate, -intdayOfWeek);
		System.out.println("Satrt Date ::: "+currDate);
		
		return currDate;
	}
	
	/********************************** calculate end Date Of Week **************************************************/

	/**
	 * 
	 * @param date
	 * @return 
	 * this method returns end date of week. this method is called in
	 * constructor.
	 */
	public static Date calculatePeriodEndDate(Date date) {

		Date currDate = date;
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MM-dd");
		String dates = format.format(date);
		currDate = format.parse(dates);

		format = DateTimeFormat.getFormat("c");
		String dayOfWeek = format.format(date);
		int intdayOfWeek = Integer.parseInt(dayOfWeek);
		int dif = 6 - intdayOfWeek;
		CalendarUtil.addDaysToDate(currDate, dif);
		return currDate;
	}

	/******************************************** Previous Button code ******************************************/

	/**
	 * This method is called when previous button is clicked.
	 */
	private void reactToPrevious() {
		System.out.println("INSIDE PREVIOUS BUTTON CLICK");
		final Date periodStartDate = CalendarUtil.copyDate(form.getDbFromDate().getValue());
		final Date periodEndDate = CalendarUtil.copyDate(form.getDbToDate().getValue());
		
		System.out.println("FROM DATE  ::: "+periodStartDate);
		System.out.println("TO DATE    ::: "+periodEndDate);

		if (periodStartDate == null)
			return;
		
		
		if(getMonthSize()>1&&form.lbMonth.getSelectedIndex()==1){
			System.out.println("INSIDE MONTH SIZE GREATER THAN ONE AND MONTH INDEX IS 1");
			form.lbMonth.setSelectedIndex(0);
			MyQuerry querry = getTimeReportQuerry(periodStartDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
			
			System.out.println("MODEL B4 new  FROM DATE  ::: "+model.getFromdate());
			System.out.println("MODEL B4 new TO DATE    ::: "+model.getTodate());
			makeNewModel();
			
			setTimeReportView(querry, periodStartDate, periodEndDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
			getAppliedLeaveApplication(Integer.parseInt(form.pic.getId().getValue()));
		}else{
			System.out.println("INSIDE ELSE ");

		CalendarUtil.addDaysToDate(periodStartDate, -7);
		CalendarUtil.addDaysToDate(periodEndDate, -7);
		
		getMonth(periodStartDate, periodEndDate);
			if(getMonthSize()>1&&form.lbMonth.getSelectedIndex()==0){
				System.out.println("INSIDE MONTH SIZE GREATER THAN ONE AND MONTH INDEX IS 0");
				form.lbMonth.setSelectedIndex(1);
				MyQuerry querry = getTimeReportQuerry(periodStartDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
				
				System.out.println("MODEL B4 new  FROM DATE  ::: "+model.getFromdate());
				System.out.println("MODEL B4 new TO DATE    ::: "+model.getTodate());
				makeNewModel();
				
				setTimeReportView(querry, periodStartDate, periodEndDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
				getAppliedLeaveApplication(Integer.parseInt(form.pic.getId().getValue()));
			}else{
				System.out.println("INSIDE MONTH SIZE  ONE");
				MyQuerry querry = getTimeReportQuerry(periodStartDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
				
				System.out.println("MODEL B4 new  FROM DATE  ::: "+model.getFromdate());
				System.out.println("MODEL B4 new TO DATE    ::: "+model.getTodate());
				makeNewModel();
				
				setTimeReportView(querry, periodStartDate, periodEndDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
				getAppliedLeaveApplication(Integer.parseInt(form.pic.getId().getValue()));
				
			}
		}

	}

	/******************************************** Next Button code ******************************************/
	/**
	 * this method is called when next button is clicked.
	 */

	private void reactToNext() {
		Console.log("INSIDE NEXT BUTTON CLICK");
		System.out.println("INSIDE NEXT BUTTON CLICK");
		final Date periodStartDate = CalendarUtil.copyDate(form.getDbFromDate().getValue());
		final Date periodEndDate = CalendarUtil.copyDate(form.getDbToDate().getValue());
		
		Console.log("FROM DATE  ::: "+periodStartDate);
		Console.log("TO DATE    ::: "+periodEndDate);
		System.out.println("FROM DATE  ::: "+periodStartDate);
		System.out.println("TO DATE    ::: "+periodEndDate);
		
		if (periodStartDate == null)
			return;
		
		if(getMonthSize()>1&&form.lbMonth.getSelectedIndex()==0){
			Console.log("INSIDE MONTH SIZE GREATER THAN ONE ");
			Console.log("index 0 "+form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
			System.out.println("INSIDE MONTH SIZE GREATER THAN ONE ");
			System.out.println("index 0 "+form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
			
			form.lbMonth.setSelectedIndex(1);
			Console.log("index 1 "+form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
			System.out.println("index 1 "+form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
			MyQuerry querry = getTimeReportQuerry(periodStartDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
			
			System.out.println("MODEL B4 new  FROM DATE  ::: "+model.getFromdate());
			System.out.println("MODEL B4 new TO DATE    ::: "+model.getTodate());
			makeNewModel();
			
			setTimeReportView(querry, periodStartDate, periodEndDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
			getAppliedLeaveApplication(Integer.parseInt(form.pic.getId().getValue()));
		}else{
			System.out.println("INSIDE MONTH SIZE ONE ");
			CalendarUtil.addDaysToDate(periodStartDate, 7);
			CalendarUtil.addDaysToDate(periodEndDate, 7);
			
			getMonth(periodStartDate, periodEndDate);
			
			MyQuerry querry = getTimeReportQuerry(periodStartDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
			System.out.println("MODEL B4 new  FROM DATE  ::: "+model.getFromdate());
			System.out.println("MODEL B4 new TO DATE    ::: "+model.getTodate());
			makeNewModel();
			
			setTimeReportView(querry, periodStartDate, periodEndDate,form.lbMonth.getValue(form.lbMonth.getSelectedIndex()));
			getAppliedLeaveApplication(Integer.parseInt(form.pic.getId().getValue()));
		}
	}

	

	
	
	/********************************** set leave table for selected Employee *********************************************/

	/**
	 * 
	 * @param info
	 * This method is called in constructor for loading leave details
	 * of loggedin user. this method is also called from onselection
	 * method for setting leave details of selected employee.
	 */

	private void setLeaveBalanceInformation(EmployeeInfo info) {
//		GenricServiceAsync genasync = GWT.create(GenricService.class);
		System.out.println("Setting Employee Leave Balance For "+ info.getEmpCount()+" : "+info.getFullName());

		if (info != null) {
			Vector<Filter>filterVec =new Vector<Filter>();
			Filter filter=null;
			MyQuerry querry = new MyQuerry();
			
			filter = new Filter();
			filter.setQuerryString("empInfo.empCount");
			filter.setIntValue(info.getEmpCount());
			filterVec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("validtill >");
			filter.setDateValue(new Date());
			filterVec.add(filter);
			
			querry.getFilters().addAll(filterVec);
			querry.setQuerryObject(new LeaveBalance());
			
			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.leaveInformationTable.connectToLocal();
					System.out.println("Leave result Size :: "+result.size());
					if (result.size() == 0) {
						return;
					}else{
						leaveAlloc=new ArrayList<AllocatedLeaves>();
						for(SuperModel model:result){
							leaveBalance  = (LeaveBalance) model;
//							ArrayList<AllocatedLeaves> leaveAlloc = leaveBalance.getLeaveGroup().getAllocatedLeaves();
							leaveAlloc = leaveBalance.getLeaveGroup().getAllocatedLeaves();
							form.leaveInformationTable.setValue(leaveAlloc);
							form.olbLeaveType.removeAllItems();
							form.olbLeaveType.setListItems(leaveAlloc);
							
							
						}
					}
				}
				@Override
				public void onFailure(Throwable caught) {

				}
			});
		}
	}
	
	
	
	/************************************** Return Query /Query on TimeReport Entity **********************************/

	/**
	 * 
	 * @param startDate
	 * @return 
	 * This method returns query. This method pass empid and
	 * startdate(form Date) as parameter to TimeReport Entity. This
	 * method is called from Constructor for the first time then it is
	 * called from onselection method. This method is also called from
	 * previous and next button click.
	 * 
	 */

	private MyQuerry getTimeReportQuerry(Date startDate,String month) {
		
		Console.log("START DATE --- "+startDate);
		Console.log("MONTH --- "+month);
		
		System.out.println("START DATE --- "+startDate);
		System.out.println("MONTH --- "+month);
		MyQuerry querry = new MyQuerry();
		Filter filter=null;
		Vector<Filter> temp=new Vector<Filter>();
		
		querry.setQuerryObject(new TimeReport());
		
		int id = 0;
		if (form.pic.getId().getText().trim().equals("") == false) {
			String idstring = form.pic.getId().getText();
			try {
				id = Integer.parseInt(idstring);
			} catch (Exception e) {
				return null;
			}
		}

		filter = new Filter();
		filter.setQuerryString("empid");
		filter.setIntValue(id);
		temp.add(filter);
//		querry.getFilters().add(filter);

		filter = new Filter();
		filter.setQuerryString("fromdate");
		filter.setDateValue(startDate);
		temp.add(filter);
//		querry.getFilters().add(filter);
		
		filter = new Filter();
		filter.setQuerryString("month");
		filter.setStringValue(month);
		temp.add(filter);
		
		querry.setFilters(temp);

		return querry;
	}

	
	/************************************** setting time report sheet ***************************************************/

	/**
	 * 
	 * @param querry
	 * @param startDate
	 * @param endDate
	 * 
	 * This method returns TimeReport for selected employee. this
	 * method is called in constructor for the first time. this
	 * method is also called from on table selection ,previous and
	 * next button methods. if it wont get record for employee then
	 * it call setCalendarHour method and fill time report sheet.
	 */

	private void setTimeReportView(MyQuerry querry, final Date startDate,final Date endDate,final String month) {
		System.out.println("$$$$$ 				SET TR VW START			 		$$$$$");
		System.out.println("              START DATE "+startDate);
		System.out.println("              END DATE "+endDate);
		Console.log("INSIDE TR VIEW METHOD ST CT "+startDate+" EN DT "+endDate);
//		setEmpDetailsAndDate(form.getPic().getValue(), startDate, endDate);
		form.getDbFromDate().setValue(startDate);
		form.getDbToDate().setValue(endDate);
		
		// These date pass as parameter to getLeaveDetailsInformation() methd.
		final Date levstDate = CalendarUtil.copyDate(startDate);
		final Date levenDate = CalendarUtil.copyDate(endDate);
		
//		Console.log("SetTimeReportView Leave Bal Start Date:: "+levstDate+" || End Date:: "+levenDate);

		Date stDate = CalendarUtil.copyDate(startDate);
		Date enDate = CalendarUtil.copyDate(endDate);
		
//		Console.log("SetTimeReportView Time Report Start Date:: "+stDate+" || End Date:: "+enDate);

		form.initateTimeSheetComposite(stDate, enDate);
		GenricServiceAsync asyncgen = GWT.create(GenricService.class);
		final GWTCGlassPanel panel = new GWTCGlassPanel();
		panel.show();
		
		
		System.out.println("Called showing wait symbol");
		asyncgen.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				panel.hide();
				Console.log("TR Result size "+result.size());
				if (result.size() != 0) {
					System.out.println("      SET TIME REPORT VIEW RESULT SIZE        "+result.size());
					form.updateView((TimeReport) result.get(0));
					form.setToViewState();
				} else {
					System.out.println("       ELSE SET TIME REPORT VIEW RESULT SIZE   "+result.size());
					form.setToNewState();
//					getEmpDetailsAndDate();
					for(int i=0;i<form.lbMonth.getItemCount();i++){
						if(form.lbMonth.getValue(i).trim().equals(month.trim())){
							form.lbMonth.setSelectedIndex(i);
							break;
						}
					}
					
					setCalendarHours(startDate, endDate);
					
					if (form.getPic().getValue() != null) {
						Console.log("INSIDE EMP COMP !NULL");
						System.out.println("       INSIDE SETTING LEAVE INFO TO TR");
						EmployeeInfo info = UserConfiguration.getInfo();
						if (form.getOtherButoon().getValue() == true) {
							info = form.getPic().getValue();
							getLeaveDetailsInformation(info, levstDate,levenDate);
						} else {
							getLeaveDetailsInformation(info, levstDate,levenDate);
						}
					}
				}
				
//				if(getMonthSize()>1){
//					makesTimeReportReadOnly();
//				}
				System.out.println("$$$$$ 				SET TR VW END			 		$$$$$");
			}
			@Override
			public void onFailure(Throwable caught) {
				panel.hide();
				caught.printStackTrace();
				System.out.println("          SET TR VW END   Failure        ");
			}
		});
	}

	/************************************** set the second row of table ************************************************/

	/**
	 * 
	 * @param periodStartDate
	 * @param periodEndDate
	 * This method is called when setTimeReportSheet() method return
	 * result size zero. this method checks for weekly off, holiday
	 * and set workinghours from calendar entity. this method set
	 * second row of table.
	 */

	public void setCalendarHours(Date periodStartDate1, Date periodEndDate1) {
		Console.log("INSIDE SET CALENDAR HOURS METHOD");
		Date periodStartDate = CalendarUtil.copyDate(periodStartDate1);
		Date periodEndDate = CalendarUtil.copyDate(periodEndDate1);
		
		EmployeeInfo info = form.pic.getValue();
		double sum = 0;
		if (info != null) {
			Calendar cal = info.getLeaveCalendar();
//			System.out.println("Cal is " + cal);
			if (cal != null) {

				int noDays = CalendarUtil.getDaysBetween(periodStartDate,periodEndDate);
				noDays++;
				for (int i = 1; i < noDays + 1; i++) {
//					System.out.println("Date " + periodStartDate);
					WeekleyOff wo = info.getLeaveCalendar().getWeeklyoff();
					boolean iswo = CalendarPresenter.isWeekleyOff(wo,periodStartDate);
					boolean isHoliday = cal.isHolidayDate(periodStartDate);

					if (iswo) {
						form.getComposite().setWorkinhHrs(0, periodStartDate,"WO");
					} else if (isHoliday) {
						form.getComposite().setWorkinhHrs(0, periodStartDate,"HOLIDAY");
					} else {
						form.getComposite().setWorkinhHrs(cal.getWorkingHours(), periodStartDate, "");
						sum = sum + cal.getWorkingHours();
					}
					CalendarUtil.addDaysToDate(periodStartDate, 1);
				}

				form.getComposite().setWorkingHrsTotal(sum);
//				System.out.println("sum" + sum);

			}
		}
	}

	/**
	 * @param info
	 * @param weekStartDate
	 * @param weekEndDate
	 * 
	 * This method check the Approved Leave during these duration
	 * and store all those information in list and pass that list to setLeavehrs method 
	 * which generate leave row.
	 */

	private void getLeaveDetailsInformation(EmployeeInfo info,final Date weekStartDate, final Date weekEndDate) {
		GenricServiceAsync genasync = GWT.create(GenricService.class);
		Console.log("INSIDE LEAVE DETAILS METHOD");
//		System.out.println("Employee ID " + info.getEmpCount());
//		System.out.println("Week Start Date " + weekStartDate);
//		System.out.println("Week End Date " + weekEndDate);

		if (info != null) {
			Console.log("INSIDE INFO !NULL");
			Vector<Filter> filtervec = new Vector<Filter>();
			MyQuerry querry = new MyQuerry();

			Filter filter = new Filter();
			filter.setQuerryString("empid");
			filter.setIntValue(info.getEmpCount());
			filtervec.add(filter);

			Filter statusFilter = new Filter();
			statusFilter.setQuerryString("status");
			statusFilter.setStringValue(LeaveApplication.APPROVED);
			filtervec.add(statusFilter);

			querry.setFilters(filtervec);
			querry.setQuerryObject(new LeaveApplication());

			genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					Console.log("INSIDE ON SUCCESS"+result.size());
					boolean flage = false;
					ArrayList<LeaveDetails> leaveList = new ArrayList<LeaveDetails>();
					if (result.size() == 0) {
						int counter=getMonthSize();
						Console.log("MONTH SIZE :: "+counter);
						if(counter>1){
							Console.log("MONTH SIZE GREATER THAN 1");
							makesTimeReportReadOnly();
						}
						return;
					} else {
						System.out.println("Result Size :: "+ result.size());
						for (SuperModel model : result) {
							LeaveApplication entity = (LeaveApplication) model;
							Date wkStDate = CalendarUtil.copyDate(weekStartDate);
							Date wkEnDate = CalendarUtil.copyDate(weekEndDate);

							
							Date leaveStartDt;
							Date leaveEndDt;

							Date levFrmDt = entity.getFromdate();
							Date levToDt = entity.getTodate();
							String leaveType = entity.getLeaveType();
							int noOfDays = entity.getNoOfDays();

//							System.out.println("Leave From Date :: "+ levFrmDt);
//							System.out.println("Leave To Date :: "+ levToDt);
//							System.out.println("Total Leave Days :: "+ noOfDays);
//							System.out.println("Leave Name :: "+ leaveType);

							LeaveDetails lvDet = new LeaveDetails();

							if (levFrmDt.equals(weekStartDate)|| (levFrmDt.after(weekStartDate) && (levFrmDt.equals(weekEndDate) || levFrmDt.before(weekEndDate)))) {
								leaveStartDt = levFrmDt;
//								System.out.println("Leave Start Date :: "+ leaveStartDt);
								if (levToDt.equals(weekEndDate)|| levToDt.before(weekEndDate)) {
									leaveEndDt = levToDt;
//									System.out.println("Leave End Date :: "+ leaveEndDt);
								} else {
									leaveEndDt = weekEndDate;
//									System.out.println("else Leave End Date :: "+ leaveEndDt);
								}
								lvDet.setWkSrtDate(weekStartDate);
								lvDet.setWkEndDate(weekEndDate);
								lvDet.setLvSrtDate(leaveStartDt);
								lvDet.setLvEndDate(leaveEndDt);
								lvDet.setLvName(leaveType);
								leaveList.add(lvDet);

								flage = true;

							} else if (levFrmDt.before(weekStartDate)&& (levToDt.equals(weekStartDate) || levToDt.after(weekStartDate))) {
								leaveStartDt = weekStartDate;
//								System.out.println("2 Leave Start Date :: "+ leaveStartDt);
									if (levToDt.equals(weekEndDate)|| levToDt.before(weekEndDate)) {
										leaveEndDt = levToDt;
//										System.out.println("Leave End Date :: "+ leaveEndDt);
									} else {
										leaveEndDt = weekEndDate;
//										System.out.println("else Leave End Date :: "+ leaveEndDt);
									}
									lvDet.setWkSrtDate(weekStartDate);
									lvDet.setWkEndDate(weekEndDate);
									lvDet.setLvSrtDate(leaveStartDt);
									lvDet.setLvEndDate(leaveEndDt);
									lvDet.setLvName(leaveType);
									leaveList.add(lvDet);

									flage = true;
									}
								}
								setLeaveHours(leaveList);
								if (flage == true) {
									form.getComposite().generateTotalRowWithTotal();
								}
							}
					int counter=getMonthSize();
					Console.log("MONTH SIZE :: "+counter);
					if(counter>1){
						Console.log("MONTH SIZE GREATER THAN 1");
						makesTimeReportReadOnly();
					}
					
				}

				@Override
				public void onFailure(Throwable caught) {
					Console.log("INSIDE ON FAILURE");
				}
			});
		}
	}
	
	
	/************************************** set the Fourth row of table ************************************************/

	/**
	 * 
	 * @param periodStartDate
	 * @param periodEndDate
	 *  This method is called when setTimeReportSheet() method return
	 *  result size zero. this method checks for weekly off, holiday
	 *  and set Leave hour from calendar entity. this method set
	 *  second row of table.
	 */

	public void setLeaveHours(ArrayList<LeaveDetails> leavelist) {

		System.out.println("-----------------------leave list Size  :::::::::::::::::::::: "+ leavelist.size());
		if(leavelist.size()!=0){
		EmployeeInfo info = form.pic.getValue();
		if (info != null) {
			Calendar cal = info.getLeaveCalendar();
			if (cal != null) {
				TimeReportEntryComposite.leaveCounter=0;
				form.getComposite().table.removeRow(form.getComposite().table.getRowCount() - 1);
				for (int lev = 0; lev < leavelist.size(); lev++) {
					Date wkStDate = CalendarUtil.copyDate(leavelist.get(lev).getWkSrtDate());
					Date wkEnDate = CalendarUtil.copyDate(leavelist.get(lev).getWkEndDate());

					int noDays = CalendarUtil.getDaysBetween(wkStDate, wkEnDate);
					noDays++;

					for (int i = 1; i < noDays + 1; i++) {
						
						if (wkStDate.equals(leavelist.get(lev).getLvSrtDate())&& 
								(leavelist.get(lev).getLvSrtDate().equals(leavelist.get(lev).getLvEndDate()) || 
										leavelist.get(lev).getLvSrtDate().before(leavelist.get(lev).getLvEndDate()))) {
							
							WeekleyOff wo = info.getLeaveCalendar().getWeeklyoff();
							boolean iswo = CalendarPresenter.isWeekleyOff(wo,wkStDate);
							boolean isHoliday = cal.isHolidayDate(wkStDate);
							
							if(iswo){
								System.out.println("Wo allocated leave size :: "+leaveAlloc.size());
								for(int j=0;j<leaveAlloc.size();j++){
									
									if(leaveAlloc.get(j).getName().equals(leavelist.get(lev).getLvName())){
										if(leaveAlloc.get(j).getCountWeekend()){
											form.getComposite().setLeaveHrs(cal.getWorkingHours(),leavelist.get(lev).getLvSrtDate(),leavelist.get(lev).getLvName(), lev);
										}
										else{
											form.getComposite().setLeaveHrs(0, wkStDate,leavelist.get(lev).getLvName(), lev);
										}
									}
								}
							}
							else if(isHoliday){
								System.out.println("holiday allocated leave size :: "+leaveAlloc.size());
								for(int j=0;j<leaveAlloc.size();j++){
									
									if(leaveAlloc.get(j).getName().equals(leavelist.get(lev).getLvName())){
										if(leaveAlloc.get(j).getCountHoliday()){
											form.getComposite().setLeaveHrs(cal.getWorkingHours(),leavelist.get(lev).getLvSrtDate(),leavelist.get(lev).getLvName(), lev);
										}
										else{
											form.getComposite().setLeaveHrs(0, wkStDate,leavelist.get(lev).getLvName(), lev);
										}
										
									}
								}
							}
							else{
								form.getComposite().setLeaveHrs(cal.getWorkingHours(),leavelist.get(lev).getLvSrtDate(),leavelist.get(lev).getLvName(), lev);
							}
							CalendarUtil.addDaysToDate(leavelist.get(lev).getLvSrtDate(), 1);
							
						} else {
							form.getComposite().setLeaveHrs(0, wkStDate,leavelist.get(lev).getLvName(), lev);
						}

						CalendarUtil.addDaysToDate(wkStDate, 1);
					}
				}

			}
		}
		}
	}
	
	/****************************************************************************************************/
	
	public void getAppliedLeaveApplication(int emplId) {
		System.out.println("INSIDE GET LEAVE APPLICATION METHOD  "+ emplId);
		
//		List<String> statusList=new ArrayList<String>();
//		statusList.add(LeaveApplication.CREATED);
//		statusList.add(LeaveApplication.REQUESTED);
			
		Vector<Filter>filterVec =new Vector<Filter>();
		Filter filter=null;
		MyQuerry querry = new MyQuerry();
			
		filter = new Filter();
		filter.setQuerryString("empid");
		filter.setIntValue(emplId);
		filterVec.add(filter);
			
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setStringValue(LeaveApplication.REQUESTED);
		filterVec.add(filter);
			
		querry.setFilters(filterVec);
		querry.setQuerryObject(new LeaveApplication());
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				appliedLeaveAppl=new ArrayList<LeaveApplication>();
				System.out.println("LEAVE APP RES SIZE : "+result.size());
				if (result.size() == 0) {
					System.out.println("RES SIZE 0 "+appliedLeaveAppl.size());
					return;
				}else{
					for(SuperModel model:result){
						LeaveApplication levApp  = (LeaveApplication) model;
							appliedLeaveAppl.add(levApp);
					}
					System.out.println("CREATED/REQUESTED LEAVE SIZE "+appliedLeaveAppl.size());
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Failure!!!!!!!!!!1");
			}
		});
	}

	public void setMonthSize(int monthS){
		monthSize=monthS;
	}
	public int getMonthSize(){
		return monthSize;
	}
	/*************************************** Table Proxy Class **********************************************************/

	/**
	 * 
	 * @author JUGAL This class is defined for table proxy.
	 *
	 */

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip")
	public static class TimeReportPresenterTable extends SuperTable<TimeReport>
			implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			return null;
		}

		@Override
		public void createTable() {

		}

		@Override
		protected void initializekeyprovider() {

		}

		@Override
		public void addFieldUpdater() {

		}

		@Override
		public void setEnable(boolean state) {

		}

		@Override
		public void applyStyle() {

		}

	};

	/**************************************** Search proxy Class ******************************************************/

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.SalaryHead")
	public static class TimeReportPresenterSearch extends
			SearchPopUpScreen<CtcComponent> {
		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}

	};
	
	
	/**
	 * Date : 22-05-2018 By ANIL
	 * Loading overtime and leavetype objectlistbox 
	 */
	
	public void loadOvertime_LeaveListbox(EmployeeInfo info){
		System.out.println("Load Dorp DOwn");
		if(form.pic.getIdToEmployeeInfo()!=null){
			EmployeeInfo empInfo=form.pic.getIdToEmployeeInfo().get(info.getCount());
			if(empInfo!=null){
//				Overtime overtime=empInfo.getOvertime();
//				if(overtime!=null){
//					form.olbOvertime.clear();
//					form.olbOvertime.addItem("--SELECT--");
//					form.olbOvertime.addItem(UserConfiguration.getInfo().getOvertime().getName());
//				}
				setLeaveBalanceInformation(empInfo);
			}
		}
	}
	

}
