package com.slicktechnologies.client.views.humanresource.timereport;

import java.util.Date;
import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class TimeReportSearchForm extends SearchPopUpScreen<TimeReport>{
	
	public EmployeeInfoComposite personInfo;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Config> olbEmploymentType;
	public ObjectListBox<Config> olbDesignation;
	public ObjectListBox<Config> olbRole;
	public ObjectListBox<Department> olbDepartment;
	public DateBox dbFromDate;
	public IntegerBox ibId;
	public ListBox tbStatus;
	public ListBox lbMonth;



	public Object getVarRef(String varName)
	{

		return null ;
	}
	public TimeReportSearchForm()
	{
		super();
		createGui();
	}
	public void initWidget()
	{
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		olbEmploymentType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbEmploymentType,Screen.EMPLOYEETYPE);
		olbDesignation=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbDesignation,Screen.EMPLOYEEDESIGNATION);
		olbRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbRole,Screen.EMPLOYEEROLE);
//		dbFromDate= new DateBox();
		dbFromDate = new DateBoxWithYearSelector();

		ibId=new IntegerBox();
		tbStatus=new ListBox();
		AppUtility.setStatusListBox(tbStatus,TimeReport.getStatusList());
		olbDepartment=new ObjectListBox<Department>();
		Department.makeDepartMentListBoxLive(olbDepartment);
		
		lbMonth=new ListBox();
		lbMonth.addItem("--SELECT--");
		lbMonth.addItem("January");
		lbMonth.addItem("February");
		lbMonth.addItem("March");
		lbMonth.addItem("April");
		lbMonth.addItem("May");
		lbMonth.addItem("June");
		lbMonth.addItem("July");
		lbMonth.addItem("August");
		lbMonth.addItem("September");
		lbMonth.addItem("October");
		lbMonth.addItem("November");
		lbMonth.addItem("December");
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(4).build();
		
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Employment Type",olbEmploymentType);
		FormField folbtype= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Employee Designation",olbDesignation);
		FormField folbdesignation= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Employee Role",olbRole);
		FormField folbrole= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Date",dbFromDate);
		FormField ffromDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Time Report ID",ibId);
		FormField ffId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Status",tbStatus);
		FormField ftbStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Department",olbDepartment);
		FormField folbDepartment= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Month",lbMonth);
		FormField flbMonth= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				{fpersonInfo,},
				{ffId,ffromDate,ftbStatus},
				{folbtype,folbdesignation,folbrole},
				{folbBranch,folbDepartment,flbMonth},
		};
	}
	
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;

		if(dbFromDate.getValue()!=null)
		{
			//Calculate From Date
			
			Date fromDate=TimeReportPresenter.calculatePeriodStartDate(dbFromDate.getValue());
			
			temp=new Filter();
			temp.setDateValue(fromDate);
			temp.setQuerryString("fromdate");
			filtervec.add(temp);
		}
		
		if(tbStatus.getSelectedIndex()!=0)
		{
			int selIndex=tbStatus.getSelectedIndex();
			String val=tbStatus.getItemText(selIndex);
			
			temp=new Filter();
			temp.setStringValue(val);
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
	

		if(olbRole.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbRole.getValue().trim());
			temp.setQuerryString("employeeRole");
			filtervec.add(temp);
		}
		if(olbDesignation.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbDesignation.getValue().trim());
			temp.setQuerryString("employeedDesignation");
			filtervec.add(temp);
		}
		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if(olbEmploymentType.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbEmploymentType.getValue().trim());
			temp.setQuerryString("employeeType");
			filtervec.add(temp);
		}
		
		if(ibId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if(olbDepartment.getValue()!=null){
			temp=new Filter();
			temp.setStringValue(olbDepartment.getValue());
			temp.setQuerryString("department");
			filtervec.add(temp);
		}
		
		if(!personInfo.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
			temp.setQuerryString("empid");
			filtervec.add(temp);
		}

		if(!(personInfo.getName().getValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(personInfo.getName().getValue());
			temp.setQuerryString("employeeName");
			filtervec.add(temp);
		}
		if(!personInfo.getPhone().getValue().equals(""))
		{
			temp=new Filter();
			temp.setLongValue(personInfo.getCellNumber());
			temp.setQuerryString("empCellNo");
			filtervec.add(temp);
		}
		if(lbMonth.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(lbMonth.getValue(lbMonth.getSelectedIndex()));
			temp.setQuerryString("month");
			filtervec.add(temp);
		}
		

		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new TimeReport());
		return querry;
	}
	@Override
	public boolean validate() {

		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}
}

	