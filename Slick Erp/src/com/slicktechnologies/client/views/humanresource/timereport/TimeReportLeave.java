package com.slicktechnologies.client.views.humanresource.timereport;

public class TimeReportLeave {
	
	String leaveName;
	double leavehrs;
	
	
	public TimeReportLeave() {
		
	}


	public String getLeaveName() {
		return leaveName;
	}


	public void setLeaveName(String leaveName) {
		this.leaveName = leaveName;
	}


	public double getLeavehrs() {
		return leavehrs;
	}


	public void setLeavehrs(double leavehrs) {
		this.leavehrs = leavehrs;
	}
	
	

}
