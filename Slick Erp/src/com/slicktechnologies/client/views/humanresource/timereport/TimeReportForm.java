package com.slicktechnologies.client.views.humanresource.timereport;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.history.HistoryPopup;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeHRProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReportEntrey;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;


public class TimeReportForm extends ApprovableFormScreen<TimeReport> implements ClickHandler{
	
	RadioButton selfButton;
	RadioButton otherButoon;

	EmployeeInfoComposite pic;
	
	TextBox tbId;
	DateBox dbFromDate,dbToDate;
	
	ObjectListBox<Employee> olbApprover;
	
	TextBox tbStatus;
	
	LeaveInformationTable leaveInformationTable;
	
	ObjectListBox<HrProject>olbProject;
	Button addProject;
	
	ObjectListBox<AllocatedLeaves> olbLeaveType;
	Button addLeave;
	
	
//	ListBox olbOvertime;
	
	ObjectListBox<Overtime> olbOvertime;
	
	ListBox lbMonth;
	
	Button addOvertime;
	
	TimeReportEntryComposite composite;
	
	TextBox tbBlank;
	TimeReport timereportobj;
	
	/**
	 * Date : 19-05-2018 BY ANIL
	 * Added Tab Panel
	 */
	TabPanel tabPanel ;

	public TimeReportForm()
	{
		createGui();
		this.tbStatus.setText(Loan.CREATED);
		this.tbId.setEnabled(false);
		processLevelBar.btnLabels[2].setVisible(true);
		processLevelBar.btnLabels[3].setVisible(true);

	}


	/**
	 * Method template to initialize the declared variables.
	 */
	
	private void initalizeWidget()
	{
		selfButton=new RadioButton("Group1");
		selfButton.setValue(true);
		selfButton.addClickHandler(this);
		otherButoon=new RadioButton("Group1");
		otherButoon.addClickHandler(this);
		
		pic=AppUtility.employeeInfoComposite(new EmployeeInfo(),true);
		
		tbId=new TextBox();
		
		dbFromDate=new DateBoxWithYearSelector();
		this.dbFromDate=new DateBoxWithYearSelector();
		dbFromDate.setEnabled(false);
		
		dbToDate=new DateBoxWithYearSelector();
		this.dbToDate=new DateBoxWithYearSelector();
		dbToDate.setEnabled(false);
		
		olbApprover = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbApprover);
		
		tbStatus=new TextBox();
		tbStatus.setEnabled(false);
		
		leaveInformationTable=new LeaveInformationTable();
		
		this.olbProject=new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(olbProject);
		addProject=new Button("Add");
		
		olbLeaveType=new ObjectListBox<AllocatedLeaves>();
		addLeave=new Button("Add");
		
//		olbOvertime=new ListBox();
//		olbOvertime.addItem("--SELECT--");
		
		Filter temp=new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		MyQuerry querry=new MyQuerry();
		Vector<Filter> vecFil=new Vector<Filter>();
		vecFil.add(temp);
		querry.setFilters(vecFil);
		querry.setQuerryObject(new Overtime());
		
		olbOvertime=new ObjectListBox<Overtime>();
		olbOvertime.MakeLive(querry);
		
		addOvertime=new Button("Add");
		
		lbMonth=new ListBox();
		lbMonth.setEnabled(false);
		lbMonth.addItem("January");
		lbMonth.addItem("February");
		lbMonth.addItem("March");
		lbMonth.addItem("April");
		lbMonth.addItem("May");
		lbMonth.addItem("June");
		lbMonth.addItem("July");
		lbMonth.addItem("August");
		lbMonth.addItem("September");
		lbMonth.addItem("October");
		lbMonth.addItem("November");
		lbMonth.addItem("December");
		
		composite=new TimeReportEntryComposite();
		
		tbBlank=new TextBox();
		tbBlank.setVisible(false);
		
		
		/**
		 * Date : 19-05-2018 By ANIL
		 */
		
		FlowPanel flowPanel1=new FlowPanel();
		FlowPanel flowPanel2=new FlowPanel();
		
		VerticalPanel verPanel1=new VerticalPanel();
		verPanel1.setWidth("100%");
		verPanel1.setHeight("350px");
		
		VerticalPanel verPanel2=new VerticalPanel();
		
		InlineLabel projLbl = new InlineLabel("Projects");
		InlineLabel leaveTyplbl = new InlineLabel("Leave Type");
		InlineLabel ovrTymLbl =new InlineLabel("Overtime");
		InlineLabel dotLbl1 = new InlineLabel(".");
		dotLbl1.getElement().getStyle().setColor("white");
		InlineLabel dotLbl2 = new InlineLabel(".");
		dotLbl2.getElement().getStyle().setColor("white");
		InlineLabel dotLbl3 = new InlineLabel(".");
		dotLbl3.getElement().getStyle().setColor("white");
		
		
		
		
		HorizontalPanel hrPanel=new HorizontalPanel();
		VerticalPanel verPan1=new VerticalPanel();
		verPan1.add(projLbl);
		olbProject.getElement().getStyle().setWidth(225, Unit.PX);
		olbProject.getElement().getStyle().setHeight(25, Unit.PX);
		verPan1.add(olbProject);
		hrPanel.add(verPan1);
		
		VerticalPanel verPan2=new VerticalPanel();
		verPan2.add(dotLbl1);
		verPan2.add(addProject);
		addProject.getElement().getStyle().setMarginLeft(10, Unit.PX);
		addProject.getElement().getStyle().setMarginRight(10, Unit.PX);
		hrPanel.add(verPan2);
		
		VerticalPanel verPan3=new VerticalPanel();
		verPan3.add(leaveTyplbl);
		verPan3.add(olbLeaveType);
		olbLeaveType.getElement().getStyle().setWidth(225, Unit.PX);
		olbLeaveType.getElement().getStyle().setHeight(25, Unit.PX);
		hrPanel.add(verPan3);
		
		VerticalPanel verPan4=new VerticalPanel();
		verPan4.add(dotLbl2);
		verPan4.add(addLeave);
		addLeave.getElement().getStyle().setMarginLeft(10, Unit.PX);
		addLeave.getElement().getStyle().setMarginRight(10, Unit.PX);
		hrPanel.add(verPan4);
		
		VerticalPanel verPan5=new VerticalPanel();
		verPan5.add(ovrTymLbl);
		verPan5.add(olbOvertime);
		olbOvertime.getElement().getStyle().setWidth(225, Unit.PX);
		olbOvertime.getElement().getStyle().setHeight(25, Unit.PX);
		hrPanel.add(verPan5);
		
		VerticalPanel verPan6=new VerticalPanel();
		verPan6.add(dotLbl3);
		verPan6.add(addOvertime);
		addOvertime.getElement().getStyle().setMarginLeft(10, Unit.PX);
		addOvertime.getElement().getStyle().setMarginRight(10, Unit.PX);
		hrPanel.add(verPan6);
		
		InlineLabel monthLbl=new InlineLabel("Month");
		VerticalPanel vP3=new VerticalPanel();
		vP3.add(monthLbl);
		vP3.add(lbMonth);
		vP3.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbMonth.getElement().getStyle().setWidth(200, Unit.PX);
		hrPanel.add(vP3);
		
		verPanel1.add(hrPanel);
		verPanel1.add(composite);
//		composite.table.getElement().getStyle().setMarginTop(30, Unit.PX);
		composite.table.setWidth("100%");
//		composite.table.getCellFormatter().getElement(0, 0).getStyle().setWidth(120, Unit.PX);
//		composite.table.getCellFormatter().getElement(1, 0).getStyle().setWidth(120, Unit.PX);
//		composite.table.getCellFormatter().getElement(2, 0).getStyle().setWidth(120, Unit.PX);
		
		verPanel2.add(leaveInformationTable.getTable());
		verPanel2.setWidth("100%");
		verPanel2.setHeight("350px");
		
		flowPanel1.add(verPanel1);
		flowPanel2.add(verPanel2);
		
		/**
		 * Date : 21-05-2018 By Anil
		 */
		FlowPanel flowPanel0=new FlowPanel();
		
				
		InlineLabel selfLbl=new InlineLabel("Self");
		InlineLabel otherLbl=new InlineLabel("Other");
		
		
		HorizontalPanel hP1=new HorizontalPanel();
		VerticalPanel vP1=new VerticalPanel();
		vP1.add(selfLbl);
		vP1.add(selfButton);
		hP1.add(vP1);
		
		VerticalPanel vP2=new VerticalPanel();
		vP2.add(otherLbl);
		vP2.add(otherButoon);
		vP2.getElement().getStyle().setMarginLeft(50, Unit.PX);
		hP1.add(vP2);
		
//		VerticalPanel vP3=new VerticalPanel();
//		vP3.add(monthLbl);
//		vP3.add(lbMonth);
//		vP3.getElement().getStyle().setMarginLeft(723, Unit.PX);
//		lbMonth.getElement().getStyle().setWidth(208, Unit.PX);
//		hP1.add(vP3);
		
		HorizontalPanel hP2=new HorizontalPanel();
		VerticalPanel vP4=new VerticalPanel();
		vP4.add(pic);
		pic.getPanel().setWidth("146%");
		hP2.add(vP4);
		
		flowPanel0.add(hP1);
		flowPanel0.add(hP2);
		
		tabPanel = new TabPanel();
		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				 int tabId = event.getSelectedItem();
				 System.out.println("TAB ID "+tabId);
				 if(tabId==0){
//					 earningtable.getTable().redraw();
				 }else if(tabId==1){
//					 leaveInformationTable.getTable().redraw();
				 }
				 else if(tabId==2){
					 leaveInformationTable.getTable().redraw();
				 }
			}
		});
		
		tabPanel.add(flowPanel0,"Employee Information");
		tabPanel.add(flowPanel1,"Time Report Entry");
		tabPanel.add(flowPanel2,"Leave Balance");
		tabPanel.setSize("100%", "350px");
		// select first tab
		tabPanel.selectTab(0);

		
		
		
	}

	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen() {

		initalizeWidget();

		//Token to initialize the processlevel menus.
		this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,"Previous","Next"};

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("self",selfButton);
		FormField fselfButton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Others",otherButoon);
		FormField fotherButoon= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder(" ",tbBlank);
		FormField ftbBlank= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Month",lbMonth);
		FormField flbMonth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeInformation=fbuilder.setlabel("Employee Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("",pic);
		FormField fpic= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAdvanceInformation=fbuilder.setlabel("Time Sheet Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder =new FormFieldBuilder("Time Report ID",tbId);
		FormField fibAdvanceId=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* From Date",dbFromDate);
		FormField fdbFromDate= fbuilder.setMandatory(true).setMandatoryMsg("From Date is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* To Date",dbToDate);
		FormField fdbToDate= fbuilder.setMandatory(true).setMandatoryMsg("To Date is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status",tbStatus);
		FormField ftbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Approver",olbApprover);
		FormField ftbApprover= fbuilder.setMandatory(true).setMandatoryMsg("Approver Name is Mandatory !").setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingLeave=fbuilder.setlabel("Leave Balance").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("",leaveInformationTable.getTable());
		FormField fleaveHistory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		FormField fgroupingTimeReport=fbuilder.setlabel("Time Report Entry").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Projects",olbProject);
		FormField fproj= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",addProject);
		FormField fbutton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Overtime",olbOvertime);
		FormField flbOvertime= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",addOvertime);
		FormField faddOvertime= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Leave Type",olbLeaveType);
		FormField folbLeaveType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",addLeave);
		FormField fadButton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",composite);
		FormField fComposite= fbuilder.setMandatory(true).setMandatoryMsg("Please Enter at least one row !").setRowSpan(0).setColSpan(5).build();

		
		fbuilder = new FormFieldBuilder("",tabPanel);
		FormField ftabPanel= fbuilder.setMandatory(false).setMandatoryMsg("Please Enter at least one row !").setRowSpan(0).setColSpan(5).build();

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = { 
//				{fselfButton,fotherButoon,ftbBlank,flbMonth}, 
//				{fgroupingEmployeeInformation},
//				{fpic},
				{fgroupingAdvanceInformation},
				{fibAdvanceId,fdbFromDate,fdbToDate,ftbStatus,ftbApprover},
//				{ftbStatus,ftbApprover},
//				{fgroupingLeave},
//				{fleaveHistory},
//				{fgroupingTimeReport},
//				{fproj,fbutton,folbLeaveType,fadButton},
//				{flbOvertime,faddOvertime},
//				{fComposite},
				{ftabPanel}

		};

		this.fields=formfield;

		

	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(TimeReport model) 
	{
		if(dbFromDate.getValue()!=null)
			model.setFromdate(dbFromDate.getValue());
		if(dbToDate.getValue()!=null)
			model.setTodate(dbToDate.getValue());
		if(tbStatus.getValue()!=null)
			model.setStatus(tbStatus.getValue());
		if(olbApprover.getValue()!=null)
			model.setApproverName(olbApprover.getValue());
		
		
		model.setMonth(lbMonth.getValue(lbMonth.getSelectedIndex()));
		
		composite.setFromDate(dbFromDate.getValue());
		composite.setToDate(dbToDate.getValue());
	
		if(composite.getValue()!=null)
		{
			List<TimeSheetLineItem>lineItem=composite.getValue();
		    List<TimeReportEntrey>entrylis=TimeSheetLineItem.getTimeSheetEntryListFromLineItemList(lineItem);
		    model.setTimeReportEntry(entrylis);
		}
		
		if(pic!=null)
		{
			model.setEmpid(pic.getEmployeeId());
			model.setEmployeeName(pic.getEmployeeName());
			model.setEmpCellNo(pic.getCellNumber());
			model.setBranch(pic.getBranch());
			model.setDepartment(pic.getDepartment());
			model.setEmployeeType(pic.getEmployeeType());
			model.setEmployeedDesignation(pic.getEmpDesignation());
			model.setEmployeeRole(pic.getEmpRole());
		}

		
		timereportobj=model;
		presenter.setModel(model);

	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(TimeReport view) 
	{
		timereportobj=view;

		tbId.setValue(view.getCount()+"");
		if(view.getFromdate()!=null)
			dbFromDate.setValue(view.getFromdate());
		if(view.getTodate()!=null)
			dbToDate.setValue(view.getTodate());
		if(view.getStatus()!=null)
			tbStatus.setValue(view.getStatus());
		if(view.getApproverName()!=null)
			olbApprover.setValue(view.getApproverName());

		if(pic!=null)
		{
			int id=UserConfiguration.getInfo().getEmpCount();
			if(id==view.getEmpid())
				selfButton.setValue(true);
			else
				otherButoon.setValue(true);
		}
		
		if(pic!=null)
		{

			pic.setEmployeeId(view.getEmpid());
			pic.setEmployeeName(view.getEmployeeName());
			pic.setCellNumber(view.getEmpCellNo());
			pic.setBranch(view.getBranch());
			pic.setDepartment(view.getDepartment());
			pic.setEmployeeType(view.getEmployeeType());
			pic.setEmpDesignation(view.getEmployeedDesignation());
			pic.setEmpRole(view.getEmployeeRole());

		}
		
		/**
		 * Date : 22-05-2018 By ANIL
		 * loading month list
		 */
		TimeReportPresenter pres=(TimeReportPresenter) this.presenter;
		pres.getMonth(view.getFromdate(),view.getTodate());
		
		
		/**
		 * End
		 */
		
		if(view.getMonth()!=null){
			for(int i=0;i<lbMonth.getItemCount();i++){
				if(view.getMonth().equals(lbMonth.getItemText(i))){
					lbMonth.setSelectedIndex(i);
					break;
				}
			}
		}
		
		if(view.getTimeReportEntry()!=null)
		{
			composite.setFromDate(view.getFromdate());
			composite.setToDate(view.getTodate());
			List<TimeSheetLineItem>items=TimeSheetLineItem.getTimeSheetLineItemFromTimeSheetEntry(view.getTimeReportEntry());
			composite.setValue(items);
		}
		
		TimeReportPresenter tpr=(TimeReportPresenter) presenter;
		tpr.setCalendarHours(view.getFromdate(),view.getTodate());
		tpr.loadOvertime_LeaveListbox(pic.getValue());

		if(presenter != null){
			presenter.setModel(view);
		}
	}


	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);  
				}

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION)){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);  	
				}
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.equals(AppConstants.NAVIGATION)){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);  	
				}
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.TIMEREPORT,LoginPresenter.currentModule.trim());
	}
	
	

	/**
	 * Toggle process level menu.
	 */
	public void toggleProcessLevelMenu()
	{
		TimeReport entity=(TimeReport) presenter.getModel();
		
		String status=entity.getStatus();

		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();
			if(status.equals(TimeReport.REJECTED))
			{

				if(text.contains("Previous")||text.contains("Next")||text.contains(ManageApprovals.APPROVALREQUEST)){
					label.setVisible(true);
				}
				else{
					label.setVisible(false);
				}
			}
		}

	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		this.tbId.setValue(presenter.getModel().getCount()+"");
	}


	@Override
	public void onClick(ClickEvent event) {
		
		if(selfButton.getValue()==true)	
		{
			pic.setEnable(false);
			TimeReportPresenter presenter=(TimeReportPresenter) this.presenter;
			if(UserConfiguration.getInfo()!=null){
			  pic.setValue(UserConfiguration.getInfo());
			}
			selfButton.setValue(true);
			presenter.onSelection(null);
			otherButoon.setValue(false);
			
		}
		
		if(otherButoon.getValue()==true)
		{
			pic.setEnable(true);
			clearFormElements();
			pic.clear();
			TimeReportPresenter presenter=(TimeReportPresenter) this.presenter;
			presenter.makeNewModel();
			pic.setEnable(true);
			otherButoon.setValue(true);
			selfButton.setValue(false);
		}
	}	

	public void clearFormElements()
	{
		Date periodStartDate=dbFromDate.getValue();
		Date periodEndDate=dbToDate.getValue();
		
		EmployeeInfo info=pic.getValue();
		
		
		setToNewState();
		
		super.clear();
		pic.setValue(info);
		dbFromDate.setValue(periodStartDate);
		dbToDate.setValue(periodEndDate);
		tbStatus.setText(TimeReport.CREATED);
		leaveInformationTable.connectToLocal();
		olbLeaveType.removeAllItems();
	}
	
	@Override
	public void clear()
	{
		Date periodStartDate=dbFromDate.getValue();
		Date periodEndDate=dbToDate.getValue();
		EmployeeInfo info=pic.getValue();
		super.clear();
		this.tbStatus.setText(EmployeeHRProcess.CREATED);
		dbFromDate.setValue(periodStartDate);
		dbToDate.setValue(periodEndDate);
		pic.setValue(info);
		
		if(pic.getValue()!=null)
		{
			int id=UserConfiguration.getInfo().getEmpCount();
			System.out.println("Emp Logeed in"+id);
			System.out.println("Emp  in"+pic.getValue().getId());
			if(id==info.getEmpCount())
			{
				selfButton.setValue(true);
			}
			else{
				otherButoon.setValue(true);
			}
		}
		tbStatus.setText(TimeReport.CREATED);
	}
	

	@Override
	public TextBox getstatustextbox() {
		return this.tbStatus;
	}

	@Override
	public void setEnable(boolean status)
	{
		super.setEnable(status);
		this.tbId.setEnabled(false);
		this.tbStatus.setEnabled(false);
		this.dbFromDate.setEnabled(false);
		this.dbToDate.setEnabled(false);
		this.lbMonth.setEnabled(false);
		if(selfButton.getValue()==true){
			this.pic.setEnable(false);
		}else{
			this.pic.setEnable(status);
		}
		selfButton.setEnabled(true);
		otherButoon.setEnabled(true);
		
		composite.setEnable(status);
		olbLeaveType.setEnabled(status);
		olbProject.setEnabled(status);
		olbOvertime.setEnabled(status);
		
		addLeave.setEnabled(status);
		addProject.setEnabled(status);
		addOvertime.setEnabled(status);
	}
	
	@Override
	public boolean validate()
	{
		boolean superval=super.validate();
		boolean compositeval=composite.validate();
		
		if(compositeval==false)
		{
			showDialogMessage("Please fill mandatoy fields !");
			return false;
		}
		if(superval==false){
			return false;
		}
		if(validateWorkinHours()==false){
			this.showDialogMessage("Please fill appropriate working hours!");
			return false;
		}
		
		return true;
	}
	
	public boolean validateWorkinHours(){
		
		EmployeeInfo info=pic.getValue();
		
		double workinghrs=info.getLeaveCalendar().getWorkingHours();
//		System.out.println("Working Hours ::: "+workinghrs);
		
		
		int noOfRow = composite.table.getRowCount();
		int noOfCol= composite.table.getCellCount(noOfRow-1);
		boolean flage=true;
//		System.out.println("table row count :: "+noOfRow);
//		System.out.println("table cell Count :: "+noOfCol);
		
		for (int i = noOfRow-1; i < noOfRow; i++) {
			for (int k = 1; k <= 7; k++) {
//				System.out.println("Row : "+i);
//				System.out.println("Col : "+k);
				
				Widget hrInWid =composite.table.getWidget(i, k);
//				System.out.println("Widget Hr :: "+hrInWid);
				String hrInWidStr=hrInWid.toString();
//				System.out.println("String Wid hr :: "+hrInWidStr);
				String hrInStr=hrInWidStr.replace("<span class="+"\""+"gwt-InlineLabel"+"\""+">", "").replace("</span>", "");
//				System.out.println("String Hr :: "+hrInStr);
				
				
				Widget offInWid =composite.table.getWidget(1, k);
//				System.out.println("Widget off :: "+offInWid);
				String offInWidStr=offInWid.toString();
//				System.out.println("String wid off :: "+offInWidStr);
				String offInStr=offInWidStr.replace("<span class="+"\""+"gwt-InlineLabel"+"\""+">", "").replace("</span>", "");
//				System.out.println("String off :: "+offInStr);
				
				
				double totalWorkingHr=Double.parseDouble(hrInStr.trim());
//				System.out.println("double box value ::"+totalWorkingHr);
				
				if ((totalWorkingHr== 0)&&(!offInStr.trim().equals("WO")&&!offInStr.trim().equals("HOLIDAY")&&!offInStr.trim().equals("NA"))) {
//					System.out.println("Off  :: "+offInStr);
					flage=false;
				} else {
					if((totalWorkingHr<workinghrs)&&(!offInStr.trim().equals("WO")&&!offInStr.trim().equals("HOLIDAY")&&!offInStr.trim().equals("NA"))){
//						System.out.println("Off  :: "+offInStr);
						flage=false;
					}
				}
			}
		}
		
		return flage;
	}
	
	
	@Override
	public void setToNewState() {
		super.setToNewState();
		
//		selfButton.setValue(true);
		
		processLevelBar.btnLabels[2].setVisible(true);
		processLevelBar.btnLabels[3].setVisible(true);
	}
	
	@Override
	public void setToViewState()
	{
		super.setToViewState();
		toggleProcessLevelMenu();
		
		SuperModel model=new TimeReport();
		model=timereportobj;
		if(HistoryPopup.historyObj!=null){
			AppUtility.addDocumentToHistoryTable(HistoryPopup.historyObj.getModuleName(),AppConstants.TIMEREPORT, timereportobj.getCount(), timereportobj.getEmpid(),timereportobj.getEmployeeName(),timereportobj.getEmpCellNo(), false, model, null);
			HistoryPopup.historyObj=null;
		}else{
		AppUtility.addDocumentToHistoryTable(LoginPresenter.currentModule,AppConstants.TIMEREPORT, timereportobj.getCount(), timereportobj.getEmpid(),timereportobj.getEmployeeName(),timereportobj.getEmpCellNo(), false, model, null);
		}
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		
		TimeReportPresenter presenter = (TimeReportPresenter) getPresenter();
		if(presenter.getMonthSize()>1){
			presenter.makesTimeReportReadOnly();
		}
		System.out.println("--       EDIT STATE       --");
		processLevelBar.btnLabels[0].setVisible(false);
		processLevelBar.btnLabels[1].setVisible(false);
		processLevelBar.btnLabels[2].setVisible(true);
		processLevelBar.btnLabels[3].setVisible(true);
		
	}


	/****************************************** Getter and Setter ******************************************************/
	
	
	public DateBox getDbFromDate() {
		return dbFromDate;
	}


	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}


	public DateBox getDbToDate() {
		return dbToDate;
	}


	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}


	public Button getAddProject() {
		return addProject;
	}


	public void setAddProject(Button addProject) {
		this.addProject = addProject;
	}

	public ObjectListBox<Employee> getOlbApprover() {
		return olbApprover;
	}

	public void setOlbApprover(ObjectListBox<Employee> olbApprover) {
		this.olbApprover = olbApprover;
	}

	public RadioButton getSelfButton() {
		return selfButton;
	}

	public void setSelfButton(RadioButton selfButton) {
		this.selfButton = selfButton;
	}

	public RadioButton getOtherButoon() {
		return otherButoon;
	}

	public void setOtherButoon(RadioButton otherButoon) {
		this.otherButoon = otherButoon;
	}

	public ObjectListBox<HrProject> getOlbProject() {
		return olbProject;
	}

	public void setOlbProject(ObjectListBox<HrProject> olbProject) {
		this.olbProject = olbProject;
	}

	public TextBox getTbStatus() {
		return tbStatus;
	}

	public void setTbStatus(TextBox tbStatus) {
		this.tbStatus = tbStatus;
	}

	public EmployeeInfoComposite getPic() {
		return pic;
	}

	public void setPic(EmployeeInfoComposite pic) {
		this.pic = pic;
	}

	public TextBox getTbId() {
		return tbId;
	}

	public void setTbId(TextBox tbId) {
		this.tbId = tbId;
	}

	public TimeReportEntryComposite getComposite() {
		return composite;
	}

	public void setComposite(TimeReportEntryComposite composite) {
		this.composite = composite;
	}

	public void fillVerticalTotal() {
		composite.fillVerticalTotal();
	}

	public void fillHorizontalTotal() {
		composite.fillHorizontalTotal();
	}

	public void initateTimeSheetComposite(Date startDate,Date endDate)
	{
		composite.refreshTimeSheetComposite(startDate, endDate);
	}

	public void insertEntry(String proj,boolean bool,boolean ot,String otProjName) {
		composite.insertEntry(proj,bool,ot,otProjName);

	}
	
	
	
	
	
}
