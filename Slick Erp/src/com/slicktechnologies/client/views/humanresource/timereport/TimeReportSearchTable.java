package com.slicktechnologies.client.views.humanresource.timereport;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReportEntrey;

public class TimeReportSearchTable extends SuperTable<TimeReport> {
	TextColumn<TimeReport> getBranchColumn;

	TextColumn<TimeReport> getEmployeeTypeColumn;
	TextColumn<TimeReport> designation;
	TextColumn<TimeReport> getEmployeeIdColumn;
	TextColumn<TimeReport> getEmployeeCellNumberColumn;
	TextColumn<TimeReport> getEmployeeFullNameColumn;
	TextColumn<TimeReport> department;
	public TextColumn<TimeReport> periodStartDate;
	public TextColumn<TimeReport> timeSheetId;
	public TextColumn<TimeReport> tbStatus;
	public TextColumn<TimeReport> employeeRole;
	TextColumn<TimeReport> month;

	public TimeReportSearchTable() {
		super();
	}

	@Override
	public void createTable() {

		addColumnTimeSheetId();
		addColumnMonth();
		addColumnPeriodStartDate();
		addColumngetCustomerId();
		addColumngetCustomerFullName();
		addColumngetCustomerCellNumber();

		addColumngetBranch();
		addColumngetDepartment();
		addColumngetEmployeeDesignation();
		addColumngetRole();
		addColumngetType();
		addColumngetStatus();
		applyStyle();
	}

	private void addColumnMonth() {
		month = new TextColumn<TimeReport>() {
			@Override
			public String getValue(TimeReport object) {
				return object.getMonth();
			}
		};
		table.addColumn(month, "Month");
		month.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<TimeReport>() {
			@Override
			public Object getKey(TimeReport item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
		table.setColumnWidth(timeSheetId, "100px");
		table.setColumnWidth(getBranchColumn, "150px");
		table.setColumnWidth(getEmployeeFullNameColumn, "150px");
		table.setColumnWidth(getEmployeeIdColumn, "100px");
		table.setColumnWidth(periodStartDate, "150px");
		table.setColumnWidth(getEmployeeCellNumberColumn, "150px");
		table.setColumnWidth(getEmployeeCellNumberColumn, "150px");
		table.setColumnWidth(designation, "150px");
		table.setColumnWidth(department, "150px");
		table.setColumnWidth(getEmployeeTypeColumn, "150px");
		table.setColumnWidth(employeeRole, "150px");
		table.setColumnWidth(tbStatus, "100px");
		table.setColumnWidth(month, "100px");

	}

	public void addColumnSorting() {

		addSortinggetEmployeeType();
		addSortinggetCustomerLevel();
		addSortinggetBranch();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortinggetCustomerId();
		addSortinggTimeSheetId();
		addSortinggPeriodStartDate();
		addSortinggetStatus();
		addSortinggetDepartment();
		addSortinggetRole();
		addSortingDesignation();
		addSortinggetMonth();
	}

	private void addSortinggetMonth() {
		List<TimeReport> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReport>(list);
		columnSort.setComparator(month, new Comparator<TimeReport>() {
			@Override
			public int compare(TimeReport e1, TimeReport e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMonth() != null&& e2.getMonth() != null) {
						return e1.getMonth().compareTo(e2.getMonth());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	@Override
	public void addFieldUpdater() {
	}

	protected void addSortingDesignation() {
		List<TimeReport> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReport>(list);
		columnSort.setComparator(designation, new Comparator<TimeReport>() {
			@Override
			public int compare(TimeReport e1, TimeReport e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployeedDesignation() != null
							&& e2.getEmployeedDesignation() != null) {
						return e1.getEmployeedDesignation().compareTo(
								e2.getEmployeedDesignation());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetStatus() {
		tbStatus = new TextColumn<TimeReport>() {
			@Override
			public String getValue(TimeReport object) {
				return object.getStatus();
			}
		};
		table.addColumn(tbStatus, "Status");
		tbStatus.setSortable(true);
	}

	protected void addSortinggetStatus() {
		List<TimeReport> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReport>(list);
		columnSort.setComparator(tbStatus, new Comparator<TimeReport>() {
			@Override
			public int compare(TimeReport e1, TimeReport e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null && e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetDepartment() {
		List<TimeReport> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReport>(list);
		columnSort.setComparator(department, new Comparator<TimeReport>() {
			@Override
			public int compare(TimeReport e1, TimeReport e2) {
				if (e1 != null && e2 != null) {
					if (e1.getDepartment() != null
							&& e2.getDepartment() != null) {
						return e1.getDepartment().compareTo(e2.getDepartment());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetRole() {
		employeeRole = new TextColumn<TimeReport>() {
			@Override
			public String getValue(TimeReport object) {
				return object.getEmployeeRole();
			}
		};
		table.addColumn(employeeRole, "Role");
		employeeRole.setSortable(true);
	}

	protected void addSortinggetRole() {
		List<TimeReport> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReport>(list);
		columnSort.setComparator(employeeRole, new Comparator<TimeReport>() {
			@Override
			public int compare(TimeReport e1, TimeReport e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployeeRole() != null
							&& e2.getEmployeeRole() != null) {
						return e1.getEmployeeRole().compareTo(
								e2.getEmployeeRole());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetDepartment() {
		department = new TextColumn<TimeReport>() {
			@Override
			public String getValue(TimeReport object) {
				return object.getDepartment();
			}
		};
		table.addColumn(department, "Department");
		department.setSortable(true);
	}

	protected void addSortinggTimeSheetId() {
		List<TimeReport> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReport>(list);
		columnSort.setComparator(timeSheetId, new Comparator<TimeReport>() {

			@Override
			public int compare(TimeReport e1, TimeReport e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}

				}
				return -1;
			}

		}

		);
		table.addColumnSortHandler(columnSort);
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumnTimeSheetId() {
		timeSheetId = new TextColumn<TimeReport>() {
			@Override
			public String getValue(TimeReport object) {
				return object.getCount() + "";
			}
		};
		table.addColumn(timeSheetId, "Id");
		timeSheetId.setSortable(true);
	}

	protected void addSortinggPeriodStartDate() {
		List<TimeReport> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReport>(list);
		columnSort.setComparator(periodStartDate, new Comparator<TimeReport>() {

			@Override
			public int compare(TimeReport e1, TimeReport e2) {
				if (e1 != null && e2 != null) {
					if (e1.getFromdate() != null && e2.getFromdate() != null)
						return e1.getFromdate().compareTo(e2.getFromdate());

				}
				return -1;

			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumnPeriodStartDate() {
		periodStartDate = new TextColumn<TimeReport>() {
			@Override
			public String getValue(TimeReport object) {
				if (object.getFromdate() != null) {
					return AppUtility.parseDate(object.getFromdate());
				} else
					return "";
			}
		};
		table.addColumn(periodStartDate, "Period Start Date");
		timeSheetId.setSortable(true);
		periodStartDate.setSortable(true);
	}

	protected void addSortinggetEmployeeType() {
		List<TimeReport> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReport>(list);
		columnSort.setComparator(getEmployeeTypeColumn,
				new Comparator<TimeReport>() {
					@Override
					public int compare(TimeReport e1, TimeReport e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmployeeType() != null
									&& e2.getEmployeeType() != null) {
								return e1.getEmployeeType().compareTo(
										e2.getEmployeeType());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetType() {
		getEmployeeTypeColumn = new TextColumn<TimeReport>() {
			@Override
			public String getValue(TimeReport object) {
				return object.getEmployeeType();
			}
		};
		table.addColumn(getEmployeeTypeColumn, "Employee Type");
		getEmployeeTypeColumn.setSortable(true);
	}

	protected void addSortinggetCustomerLevel() {
		List<TimeReport> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReport>(list);
		columnSort.setComparator(designation, new Comparator<TimeReport>() {
			@Override
			public int compare(TimeReport e1, TimeReport e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployeedDesignation() != null
							&& e2.getEmployeedDesignation() != null) {
						return e1.getEmployeedDesignation().compareTo(
								e2.getEmployeedDesignation());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetEmployeeDesignation() {
		designation = new TextColumn<TimeReport>() {
			@Override
			public String getValue(TimeReport object) {
				return object.getEmployeedDesignation();
			}
		};
		table.addColumn(designation, "Employee Designation");
		designation.setSortable(true);
	}

	protected void addSortinggetBranch() {
		List<TimeReport> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReport>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<TimeReport>() {
			@Override
			public int compare(TimeReport e1, TimeReport e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null && e2.getBranch() != null) {
						return e1.getBranch().compareTo(e2.getBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetBranch() {
		getBranchColumn = new TextColumn<TimeReport>() {
			@Override
			public String getValue(TimeReport object) {
				return object.getBranch();
			}
		};
		table.addColumn(getBranchColumn, "Branch");
		getBranchColumn.setSortable(true);
	}

	protected void addColumngetCustomerId() {
		getEmployeeIdColumn = new TextColumn<TimeReport>() {
			@Override
			public String getValue(TimeReport object) {
				return object.getEmpid() + "";
			}
		};
		table.addColumn(getEmployeeIdColumn, "Employee ID");
		table.setColumnWidth(getEmployeeIdColumn, "150 px");
		getEmployeeIdColumn.setSortable(true);
	}

	protected void addSortinggetCustomerId() {
		List<TimeReport> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReport>(list);
		columnSort.setComparator(getEmployeeIdColumn,
				new Comparator<TimeReport>() {
					@Override
					public int compare(TimeReport e1, TimeReport e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmpid() == e2.getEmpid()) {
								return 0;
							}
							if (e1.getEmpid() > e2.getEmpid()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetCustomerFullName() {
		List<TimeReport> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReport>(list);
		columnSort.setComparator(getEmployeeFullNameColumn,
				new Comparator<TimeReport>() {
					@Override
					public int compare(TimeReport e1, TimeReport e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmployeeName() != null
									&& e2.getEmployeeName() != null) {
								return e1.getEmployeeName().compareTo(
										e2.getEmployeeName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCustomerFullName() {
		getEmployeeFullNameColumn = new TextColumn<TimeReport>() {
			@Override
			public String getValue(TimeReport object) {
				return object.getEmployeeName() + "";
			}
		};
		table.addColumn(getEmployeeFullNameColumn, "Employee Name");
		getEmployeeFullNameColumn.setSortable(true);
	}

	protected void addSortinggetCustomerCellNumber() {
		List<TimeReport> list = getDataprovider().getList();
		columnSort = new ListHandler<TimeReport>(list);
		columnSort.setComparator(getEmployeeCellNumberColumn,
				new Comparator<TimeReport>() {
					@Override
					public int compare(TimeReport e1, TimeReport e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmpCellNo() == e2.getEmpCellNo()) {
								return 0;
							}
							if (e1.getEmpCellNo() > e2.getEmpCellNo()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCustomerCellNumber() {

		getEmployeeCellNumberColumn = new TextColumn<TimeReport>() {
			@Override
			public String getValue(TimeReport object) {
				if (object.getEmpCellNo() == 0) {
					return 0 + "";
				} else {
					return object.getEmpCellNo() + "";
				}
			}
		};
		table.addColumn(getEmployeeCellNumberColumn, "Employee Cell");
		getEmployeeCellNumberColumn.setSortable(true);
	}
}
