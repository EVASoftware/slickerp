package com.slicktechnologies.client.views.humanresource.timereport;

public class EmployeeTimeReportDetails {

	int empId;
	String empName;
	boolean isValidForPayRoll;
	
	public EmployeeTimeReportDetails() {
		
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public boolean isValidForPayRoll() {
		return isValidForPayRoll;
	}

	public void setValidForPayRoll(boolean isValidForPayRoll) {
		this.isValidForPayRoll = isValidForPayRoll;
	}
	
	
	
	
}
