package com.slicktechnologies.client.views.humanresource.timereport;

import java.util.Date;

public class LeaveDetails {

	Date wkSrtDate;
	Date wkEndDate;
	Date lvSrtDate;
	Date lvEndDate;
	String lvName;
	
	public LeaveDetails() {
	}

	public Date getWkSrtDate() {
		return wkSrtDate;
	}

	public void setWkSrtDate(Date wkSrtDate) {
		this.wkSrtDate = wkSrtDate;
	}

	public Date getWkEndDate() {
		return wkEndDate;
	}

	public void setWkEndDate(Date wkEndDate) {
		this.wkEndDate = wkEndDate;
	}

	public Date getLvSrtDate() {
		return lvSrtDate;
	}

	public void setLvSrtDate(Date lvSrtDate) {
		this.lvSrtDate = lvSrtDate;
	}

	public Date getLvEndDate() {
		return lvEndDate;
	}

	public void setLvEndDate(Date lvEndDate) {
		this.lvEndDate = lvEndDate;
	}

	public String getLvName() {
		return lvName;
	}

	public void setLvName(String lvName) {
		this.lvName = lvName;
	}
	
}
