package com.slicktechnologies.client.views.humanresource.timereport;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;

import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;

public class LeaveInformationTable extends SuperTable<AllocatedLeaves> {
	TextColumn<AllocatedLeaves> getLeaveNameColumn;
	TextColumn<AllocatedLeaves> getMaximumLeaveColumn;
	TextColumn<AllocatedLeaves> getLeaveAvailedColumn;
	TextColumn<AllocatedLeaves> getRequestedLeaveColumn;
	TextColumn<AllocatedLeaves> getApprovedLeaveColumn;
	TextColumn<AllocatedLeaves> getLeaveBalanceColumn;

	public Object getVarRef(String varName) {
		if (varName.equals("getLeaveNameColumn"))
			return this.getLeaveNameColumn;
		if (varName.equals("getMaximumLeaveColumn"))
			return this.getMaximumLeaveColumn;
		if (varName.equals("getLeaveAvailedColumn"))
			return this.getLeaveAvailedColumn;
		if (varName.equals("getRequestedLeaveColumn"))
			return this.getRequestedLeaveColumn;
		if (varName.equals("getApprovedLeaveColumn"))
			return this.getApprovedLeaveColumn;
		if (varName.equals("getLeaveBalanceColumn"))
			return this.getLeaveBalanceColumn;
		return null;
	}

	public LeaveInformationTable() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetLeaveName();
		addColumngetMaximumLeave();
		addColumngetAvailedLeave();
		addColumngetRequestedLeave();
		addColumngetApprovedLeave();
		addColumngetLeaveBalance();
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<AllocatedLeaves>() {
			@Override
			public Object getKey(AllocatedLeaves item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	public void addColumnSorting() {
		addSortinggetLeaveNameColumn();
		addSortinggetMaximumLeaveColumn();
		addSortinggetAvailedLeaveColumn();
		addSortinggetRequestedLeaveColumn();
		addSortinggetApprovedLeaveColumn() ;
		addSortinggetLeaveBalanceColumn();
	}

	@Override
	public void addFieldUpdater() {
	}

	protected void addColumngetLeaveName() {
		getLeaveNameColumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				return object.getName();
			}
		};
		table.addColumn(getLeaveNameColumn, "Leave Name");
	}

	protected void addSortinggetLeaveNameColumn() {
		List<AllocatedLeaves> list = getDataprovider().getList();
		columnSort = new ListHandler<AllocatedLeaves>(list);
		columnSort.setComparator(getLeaveNameColumn,
				new Comparator<AllocatedLeaves>() {
					@Override
					public int compare(AllocatedLeaves e1, AllocatedLeaves e2) {
						if (e1 != null && e2 != null) {
							if (e1.getName() != null && e2.getName() != null) {
								return e1.getName().compareTo(e2.getName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetMaximumLeave() {
		getMaximumLeaveColumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				return object.getMaxDays() + "";
			}
		};
		table.addColumn(getMaximumLeaveColumn, "Maximum Leave");
		getMaximumLeaveColumn.setSortable(true);
	}

	protected void addSortinggetMaximumLeaveColumn() {
		List<AllocatedLeaves> list = getDataprovider().getList();
		columnSort = new ListHandler<AllocatedLeaves>(list);
		columnSort.setComparator(getMaximumLeaveColumn,
				new Comparator<AllocatedLeaves>() {
					@Override
					public int compare(AllocatedLeaves e1, AllocatedLeaves e2) {
						if (e1 != null && e2 != null) {
							if (e1.getMaxDays() == e2.getMaxDays()) {
								return 0;
							}
							if (e1.getMaxDays() > e2.getMaxDays()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetAvailedLeave() {
		getLeaveAvailedColumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				return object.getAvailedDays() + "";
			}
		};
		table.addColumn(getLeaveAvailedColumn, "Leave Availed");
		getLeaveAvailedColumn.setSortable(true);
	}

	protected void addSortinggetAvailedLeaveColumn() {
		List<AllocatedLeaves> list = getDataprovider().getList();
		columnSort = new ListHandler<AllocatedLeaves>(list);
		columnSort.setComparator(getLeaveAvailedColumn,
				new Comparator<AllocatedLeaves>() {
					@Override
					public int compare(AllocatedLeaves e1, AllocatedLeaves e2) {
						if (e1 != null && e2 != null) {
							if (e1.getAvailedDays() == e2.getAvailedDays()) {
								return 0;
							}
							if (e1.getAvailedDays() > e2.getAvailedDays()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetRequestedLeave() {
		getRequestedLeaveColumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				if (object.getRequested() == null)
					return 0 + "";
				return object.getRequested() + "";
			}
		};
		table.addColumn(getRequestedLeaveColumn, "Leave Requested");
		getRequestedLeaveColumn.setSortable(true);
	}
	
	protected void addColumngetApprovedLeave() {
		getApprovedLeaveColumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				if (object.getApproved() == null)
					return 0 + "";
				return object.getApproved() + "";
			}
		};
		table.addColumn(getApprovedLeaveColumn, "Leave Approved");
		getApprovedLeaveColumn.setSortable(true);
	}

	protected void addSortinggetRequestedLeaveColumn() {
		List<AllocatedLeaves> list = getDataprovider().getList();
		columnSort = new ListHandler<AllocatedLeaves>(list);
		columnSort.setComparator(getRequestedLeaveColumn,
				new Comparator<AllocatedLeaves>() {
					@Override
					public int compare(AllocatedLeaves e1, AllocatedLeaves e2) {
						if (e1 != null && e2 != null) {
							if (e1.getRequested() == e2.getRequested()) {
								return 0;
							}
							if (e1.getRequested() > e2.getRequested()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetApprovedLeaveColumn() {
		List<AllocatedLeaves> list = getDataprovider().getList();
		columnSort = new ListHandler<AllocatedLeaves>(list);
		columnSort.setComparator(getApprovedLeaveColumn,new Comparator<AllocatedLeaves>() {
			@Override
			public int compare(AllocatedLeaves e1, AllocatedLeaves e2) {
				if (e1 != null && e2 != null) {
					if (e1.getApproved() == e2.getApproved()) {
						return 0;
					}
					if (e1.getApproved() > e2.getApproved()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	

	protected void addColumngetLeaveBalance() {
		getLeaveBalanceColumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				return object.getBalance() + "";
			}
		};
		table.addColumn(getLeaveBalanceColumn, "Leave Balance");
		getLeaveBalanceColumn.setSortable(true);
	}

	protected void addSortinggetLeaveBalanceColumn() {
		List<AllocatedLeaves> list = getDataprovider().getList();
		columnSort = new ListHandler<AllocatedLeaves>(list);
		columnSort.setComparator(getLeaveBalanceColumn,
				new Comparator<AllocatedLeaves>() {
					@Override
					public int compare(AllocatedLeaves e1, AllocatedLeaves e2) {
						if (e1 != null && e2 != null) {
							if (e1.getBalance() == e2.getBalance()) {
								return 0;
							}
							if (e1.getBalance() > e2.getBalance()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

}
