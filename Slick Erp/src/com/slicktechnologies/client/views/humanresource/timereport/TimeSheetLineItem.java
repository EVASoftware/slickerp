package com.slicktechnologies.client.views.humanresource.timereport;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReportEntrey;

// TODO: Auto-generated Javadoc
/**
 * Represents LineItem of TimeReport.This class is necessery for display of
 * TimeReport.
 */
public class TimeSheetLineItem {

	/** Name of Project. */
	protected String projName;

	/**
	 * List repersenting Hour worked on this Project within a Period Size of The
	 * list will always be equal to Time Reporting Period lenght.
	 * */
	List<HourWorked> hoursWorked;

	boolean isLeave, isCompany,isOvertime;
	
	/**
	 * Date : 21-05-2018 By ANIL
	 */
	boolean isLateMark,isEarlyMark;
	String inTime,outTime;
	
	
	/**
	 * Date : 04-06-2018 BY ANIL
	 */
	
	String projAgainstOt;

	/**
	 * Instantiates a new time sheet line item.
	 */
	public TimeSheetLineItem() {
		super();
		hoursWorked = new ArrayList<HourWorked>();
		isLeave = isCompany =isOvertime= false;
		
		isLateMark=isEarlyMark=false;
		inTime="";
		outTime="";
		projAgainstOt="";
	}

	
	
	
	
	public String getProjAgainstOt() {
		return projAgainstOt;
	}





	public void setProjAgainstOt(String projAgainstOt) {
		this.projAgainstOt = projAgainstOt;
	}





	public boolean isLateMark() {
		return isLateMark;
	}





	public void setLateMark(boolean isLateMark) {
		this.isLateMark = isLateMark;
	}





	public boolean isEarlyMark() {
		return isEarlyMark;
	}





	public void setEarlyMark(boolean isEarlyMark) {
		this.isEarlyMark = isEarlyMark;
	}





	public String getInTime() {
		return inTime;
	}





	public void setInTime(String inTime) {
		this.inTime = inTime;
	}





	public String getOutTime() {
		return outTime;
	}





	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}





	/**
	 * Instantiates a new time sheet line item.
	 *
	 * @param projName
	 *            the proj name
	 * @param hoursWorked
	 *            the hours worked
	 */
	public TimeSheetLineItem(String projName, List<HourWorked> hoursWorked) {
		super();
		this.projName = projName;
		this.hoursWorked = hoursWorked;
	}

	
	

	
	/**
	 * Utility Class Repersents hours Worked on a Project on Particular Date.
	 */
	public static class HourWorked implements Comparable<HourWorked> {
		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */

		/** The date. */
		Date date;

		/** The hours worked. */
		double hoursWorked;
		boolean isLeaveDate, isCompanyHoliday,isOverTime = false;

		@Override
		public int compareTo(HourWorked o) {
			if (o != null && date != null) {
				return date.compareTo(o.date);
			}
			return 0;
		}

		@Override
		public String toString() {
			return "HourWorked [date=" + date + ", hoursWorked=" + hoursWorked
					+ ", isLeaveDate=" + isLeaveDate + ", isCompanyHoliday="
					+ isCompanyHoliday +", isOverTime="
							+ isOverTime + "]";
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

		public double getHoursWorked() {
			return hoursWorked;
		}

		public void setHoursWorked(double hoursWorked) {
			this.hoursWorked = hoursWorked;
		}

		public boolean isLeaveDate() {
			return isLeaveDate;
		}

		public void setLeaveDate(boolean isLeaveDate) {
			this.isLeaveDate = isLeaveDate;
		}

		public boolean isCompanyHoliday() {
			return isCompanyHoliday;
		}

		public void setCompanyHoliday(boolean isCompanyHoliday) {
			this.isCompanyHoliday = isCompanyHoliday;
		}

		public boolean isOverTime() {
			return isOverTime;
		}

		public void setOverTime(boolean isOverTime) {
			this.isOverTime = isOverTime;
		}
		
	}

	/*************************************** TimeReportEntry 2 TimeLineItem **********************************************/

	/**
	 * ******************************Buisness Logic
	 * Part*****************************. Takes a list of
	 * {@link TimeReportEntrey} converts it in List of {@link TimeSheetLineItem}
	 * list.
	 * 
	 * @param List
	 *            of {@link TimeReportEntrey}
	 * @return Corresponding {@link TimeSheetLineItem} list
	 */

	public static List<TimeSheetLineItem> getTimeSheetLineItemFromTimeSheetEntry(List<TimeReportEntrey> lisTimeReportEntry) {
//		System.out.println("Inside TRE Item To Time Line Item List ......");

		/**
		 * First create an HashMap of Project and List representing hours worked
		 * on that Project.
		 */
		List<TimeSheetLineItem> lisTimeSheetlineItems = new ArrayList<TimeSheetLineItem>();
		HashMap<String, List<TimeReportEntrey>> map = new HashMap<String, List<TimeReportEntrey>>();

		for (TimeReportEntrey temp : lisTimeReportEntry) {
			// Try to get the list from Hash map if not present it means
			// that project is absent.

			List<TimeReportEntrey> entreylist = map.get(temp.getProjectName());

			if (entreylist == null) {
				entreylist = new ArrayList<TimeReportEntrey>();
				map.put(temp.getProjectName(), entreylist);
//				System.out.println("Map line item list Size :: " + map.size());
			}

			entreylist.add(temp);
//			System.out.println("entryList Size :: " + entreylist.size());

		}
		// Start iterating the hash map, and create List of TimeSheetLineItem

		Set<Entry<String, List<TimeReportEntrey>>> entryset = map.entrySet();

		Iterator<Entry<String, List<TimeReportEntrey>>> iter = entryset.iterator();

		while (iter.hasNext()) {
			Entry<String, List<TimeReportEntrey>> entry = iter.next();
			String projectName = entry.getKey();
			List<TimeReportEntrey> listEntry = entry.getValue();
			// Start iterating the Entry and create array List of Worked
			// Projects
			List<HourWorked> workedHour = new ArrayList<HourWorked>();
			TimeSheetLineItem lineitem = new TimeSheetLineItem();
			for (TimeReportEntrey var : listEntry) {
				HourWorked hour = new HourWorked();
				hour.date = var.getDate();
				hour.hoursWorked = var.getHoursWorked();
				
			
				
				workedHour.add(hour);
				if (var.isEmployeeOnLeave()) {
					lineitem.isLeave = true;
					hour.isLeaveDate = true;
					System.out.println("LEAVE DATE ----- "+var.getDate());
					System.out.println("LEAVE DATE ----- "+var.getHoursWorked());
					
				}
				if (var.isOvertime()) {
					System.out.println(" ot DATE ----- "+var.getDate());
					System.out.println(" ot Hours Worked ----- "+var.getHoursWorked() );
					lineitem.isOvertime = true;
					hour.isOverTime = true;
					
					/**
					 * Date :04-06-2018 BY ANIL
					 */
					if(var.getProjNameForOt()!=null){
						lineitem.setProjAgainstOt(var.getProjNameForOt());
					}
				}
			}

			lineitem.setProjName(projectName);
			lineitem.setHoursWorked(workedHour);
			lisTimeSheetlineItems.add(lineitem);

		}

		for (TimeSheetLineItem temp : lisTimeSheetlineItems) {
//			System.out.println(temp.toString());
		}

		return lisTimeSheetlineItems;

	}

	/****************************************** LINEITEMLIST 2 TIMESHEETENTERY *********************************/
	/**
	 * Gets the time sheet entry list from line item list. To Do : When Time
	 * Report Entry will Come we need to set ID of TimeReportEntry also ID
	 * should ideally be in HoursWorked List.
	 *
	 * @param items List of TimeSheet Line Items.
	 *            
	 * @return the time sheet entry list from line item list
	 */
	public static List<TimeReportEntrey> getTimeSheetEntryListFromLineItemList(List<TimeSheetLineItem> items) {
		System.out.println("Inside LIL 2 TSH");
		// Iterate the Time Sheet Line Items
		List<TimeReportEntrey> entryList = new ArrayList<TimeReportEntrey>();
		for (TimeSheetLineItem temp : items) {
			for (HourWorked var : temp.getHoursWorked()) {
				TimeReportEntrey entry = new TimeReportEntrey();
				entry.setDate(var.date);
				entry.setProjhours(var.hoursWorked);

				entry.setProjectName(temp.getProjName());
				// Ugly Hack to Distinguish from Leave Day;
				if (var.isLeaveDate == true){
					entry.setEmployeeOnLeave(true);
				}
				if (var.isOverTime == true){
					entry.setOvertime(true);
					/**
					 * Date : 04-06-2018 By ANIL
					 */
					if(entry.getProjNameForOt()!=null){
						entry.setProjNameForOt(entry.getProjNameForOt());
					}
				}
				entryList.add(entry);

			}

		}

		for (TimeReportEntrey temp : entryList) {
			System.out.println(temp.toString());
		}

		System.out.println("TimeSheet List size :: " + entryList.size());
		return entryList;

	}

	/********************************To String method ************************************************************/
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */

	@Override
	public String toString() {
		System.out.println("::  TIME SHEET LINE ITEM  ::");
		return "TimeSheetLineItem [projName=" + projName + ", hoursWorked="
				+ hoursWorked + ", isLeave=" + isLeave + ", isCompany="
				+ isCompany + "]";
	}
	
	
	/*********************************** Getter and Setter ***************************************************/

	/**
	 * Gets the proj name.
	 *
	 * @return the proj name
	 */
	public String getProjName() {
		return projName;
	}

	/**
	 * Sets the proj name.
	 *
	 * @param projName
	 *            the new proj name
	 */
	public void setProjName(String projName) {
		this.projName = projName;
	}

	/**
	 * Gets the hours worked.
	 *
	 * @return the hours worked
	 */
	public List<HourWorked> getHoursWorked() {
		return hoursWorked;
	}

	/**
	 * Sets the hours worked.
	 *
	 * @param hoursWorked
	 *            the new hours worked
	 */
	public void setHoursWorked(List<HourWorked> hoursWorked) {
		this.hoursWorked = hoursWorked;
	}
	
	public boolean isLeave() {
		return isLeave;
	}

	public void setLeave(boolean isLeave) {
		this.isLeave = isLeave;
	}

	public boolean isCompany() {
		return isCompany;
	}

	public void setCompany(boolean isCompany) {
		this.isCompany = isCompany;
	}


	public boolean isOvertime() {
		return isOvertime;
	}


	public void setOvertime(boolean isOvertime) {
		this.isOvertime = isOvertime;
	}
	
	

}
