package com.slicktechnologies.client.views.humanresource.ctctemplate;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;

public class CTCTemplateSearchProxy extends SearchPopUpScreen<CTCTemplate> {
	
	ObjectListBox<CTCTemplate>olbCtcTemplate; 
	ObjectListBox<ConfigCategory> olbCTCCategory;
	ListBox lbStatus;
	/**
	 * Date 28-3-2019
	 * added by Amol for CTC Template Id 
	 */
	IntegerBox ibCtcTempId;
	
	public CTCTemplateSearchProxy() {
		// TODO Auto-generated constructor stub
		super();
		getPopup().setHeight("300px");
		createGui();
	}
	
	private void initializeWidget(){
		Filter temp=new Filter();
		temp.setBooleanvalue(true);
		temp.setQuerryString("status");
		Vector<Filter> vecTemp=new Vector<Filter>();
		vecTemp.add(temp);
		olbCtcTemplate=new ObjectListBox<CTCTemplate>();
		olbCtcTemplate.MakeLive(new MyQuerry(vecTemp, new CTCTemplate()));
		
		olbCTCCategory = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbCTCCategory, Screen.CTCCATEGORY);
		
		lbStatus=new ListBox();
		lbStatus.addItem("--SELECT--");
		lbStatus.addItem("Active");
		lbStatus.addItem("Inactive");
		
		ibCtcTempId=new IntegerBox();
		ibCtcTempId.setEnabled(true);
	}
	
	public void createScreen()
	{
		initializeWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Ctc Template",olbCtcTemplate);
		FormField folbCtcTemplate= builder.setMandatory(false).setRowSpan(1).setColSpan(1).build();
	
		builder = new FormFieldBuilder("Ctc Category",olbCTCCategory);
		FormField folbCTCCategory= builder.setMandatory(false).setRowSpan(1).setColSpan(1).build();
		
		builder = new FormFieldBuilder("Status",lbStatus);
		FormField flbStatus= builder.setMandatory(false).setRowSpan(1).setColSpan(1).build();
		
		builder = new FormFieldBuilder("CTC Template Id",ibCtcTempId);
		FormField fibCtcTempId= builder.setMandatory(false).setRowSpan(1).setColSpan(1).build();

		this.fields=new FormField[][]{
				{folbCtcTemplate,folbCTCCategory,flbStatus},
				{fibCtcTempId}
		};
	}
	
	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(olbCtcTemplate.getValue()!=null){  
			temp=new Filter();
			temp.setStringValue(olbCtcTemplate.getValue());
			temp.setQuerryString("ctcTemplateName");
			filtervec.add(temp);
		}
		if(olbCTCCategory.getValue()!=null){
			temp=new Filter();
			temp.setStringValue(olbCTCCategory.getValue());
			temp.setQuerryString("category");
			filtervec.add(temp);
		}
		
		if(lbStatus.getSelectedIndex()!=0){
			boolean status=false;
			if(lbStatus.getValue(lbStatus.getSelectedIndex()).equals("Active")){
				status=true;
			}
			temp=new Filter();
			temp.setBooleanvalue(status);
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		
		/**
		 * Date 28-3-2019 added by Amol
		 * for Search on CTC Template Id
		 */
		if(ibCtcTempId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibCtcTempId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
			
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CTCTemplate());
		return querry;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

}
