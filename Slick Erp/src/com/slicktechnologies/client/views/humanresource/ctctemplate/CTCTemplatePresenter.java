package com.slicktechnologies.client.views.humanresource.ctctemplate;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.apache.poi.hssf.record.DBCellRecord;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.ContractPresenterSearchProxy;
import com.slicktechnologies.client.views.contract.ContractPresenterTableProxy;
import com.slicktechnologies.client.views.contract.SalesLineItemTable;
import com.slicktechnologies.client.views.contract.ContractPresenter.ContractPresenterSearch;
import com.slicktechnologies.client.views.contract.ContractPresenter.ContractPresenterTable;
import com.slicktechnologies.client.views.customer.CustomerPresenterSearchProxy;
import com.slicktechnologies.client.views.customer.CustomerPresenter.CustomerPresenterSearch;
import com.slicktechnologies.client.views.humanresource.allocation.ctcallocation.CtcAllocationService;
import com.slicktechnologies.client.views.humanresource.allocation.ctcallocation.CtcAllocationServiceAsync;
import com.slicktechnologies.client.views.humanresource.ctc.SalaryHeadPresenterTableProxy;
import com.slicktechnologies.client.views.humanresource.overtime.OvertimeForm;
import com.slicktechnologies.client.views.humanresource.overtime.OvertimePresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.Esic;
import com.slicktechnologies.shared.common.humanresourcelayer.ProvidentFund;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.OtEarningComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWF;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWFBean;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class CTCTemplatePresenter extends FormScreenPresenter<CTCTemplate> implements RowCountChangeEvent.Handler, ValueChangeHandler<Double>, ChangeHandler {
	CTCTemplateForm form;
	
	/**
	 * Date : 10-09-2018 By ANIL
	 */
	CtcAllocationServiceAsync ctcAllocationService = GWT.create(CtcAllocationService.class);
	FromAndToDateBoxPopup datePopup=new FromAndToDateBoxPopup(true);
	PopupPanel panel;
	
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	
	/**
	 * @author Anil , Date : 28-06-2019
	 * for updated PF logic
	 */
	ProvidentFund pf;
	ArrayList<CtcComponent> gratuitylist=new ArrayList<CtcComponent>();
	
	/**
	 * @author Anil , Date : 25-07-2019
	 * for updated ESIC logic
	 */
	Esic esic;

	public CTCTemplatePresenter(FormScreen<CTCTemplate> view,CTCTemplate model) {
		super(view, model);
		form=(CTCTemplateForm) view;
//		form.getSupertable().connectToLocal();
//		form.retriveTable(getCTCTemplateQuery());
		retrieveEarningTable();
		
		
		form.dbTotalCtcAmount.addValueChangeHandler(this);
		form.earningtable.getTable().addRowCountChangeHandler(this);
	    form.state.addChangeHandler(this);
	    
		form.deductionTable.getTable().addRowCountChangeHandler(this);
		form.companyContributionTable.getTable().addRowCountChangeHandler(this);
		/**date 6-3-2019 added by Amol **/
		form.dbMonthlyCtc.addValueChangeHandler(this);
		
		SalaryHeadPresenterTableProxy.ctcAmount = form.dbTotalCtcAmount.getValue();
		form.setPresenter(this);
		
		/**
		 * Date  : 10-09-2018 BY  ANIL
		 */
		datePopup.btnOne.addClickHandler(this);
		datePopup.btnTwo.addClickHandler(this);
		/**
		 * End
		 */
		
		/**
		 * @author Anil @since 02-04-2021
		 */
		form.cbIncludeBonusInEarning.addClickHandler(this);
		form.cbIncludePlInEarning.addClickHandler(this);
		
		form.dbBonusAmt.addValueChangeHandler(this);
		form.dbPaidLeaveAmt.addValueChangeHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.CTCTEMPLATE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	
	
	@Override
	public boolean validateAndUpdateModel() {
		// TODO Auto-generated method stub
		return super.validateAndUpdateModel();
	}



	public void retrieveEarningTable() {

		Vector<Filter> vecfilter = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setBooleanvalue(true);
		filter.setQuerryString("status");
		vecfilter.add(filter);

//		filter = new Filter();
//		filter.setBooleanvalue(false);
//		filter.setQuerryString("isDeduction");
//		vecfilter.add(filter);

		MyQuerry quer = new MyQuerry(vecfilter, new CtcComponent());

		service.getSearchResult(quer,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("COMPONENT RESULT SIZE "+result.size());
				// form.hideWaitSymbol();
				form.earningtable.connectToLocal();
				form.deductionTable.connectToLocal();
				form.companyContributionTable.connectToLocal();
				ArrayList<CtcComponent>compList=new ArrayList<CtcComponent>();
				for (SuperModel model : result) {
					CtcComponent entity=(CtcComponent)model;
//					form.earningtable.getListDataProvider().getList().add(entity);
					compList.add(entity);
				}
				Comparator<CtcComponent> ctcSrNoComp = new Comparator<CtcComponent>() {
					@Override
					public int compare(CtcComponent o1,CtcComponent o2) {
						if(o1.getSrNo()!=0&&o2.getSrNo()!=0){
							return Integer.valueOf(o1.getSrNo()).compareTo(Integer.valueOf(o2.getSrNo()));
						}
						return 0;
					}
				};
				Collections.sort(compList, ctcSrNoComp);
				
				
//				retrievePFDetails(compList);
				retrieveEsicDetails(compList);
				
			}
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				form.hideWaitSymbol();
			}
		});
	}
	
	protected void retrievePtDetails(final ArrayList<CtcComponent> compList) {
		
		for(int i=0;i<compList.size();i++){
			if(compList.get(i).getShortName().trim().equalsIgnoreCase("PT")){
				compList.remove(i);
				i--;
			}
		}
		
		Vector<Filter> vecfilter = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setBooleanvalue(true);
		filter.setQuerryString("status");
		vecfilter.add(filter);

		MyQuerry quer = new MyQuerry(vecfilter, new ProfessionalTax());
		service.getSearchResult(quer,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<ProfessionalTax> ptlist=new ArrayList<ProfessionalTax>();
				for(SuperModel model:result){
					ProfessionalTax pt=(ProfessionalTax) model;
					ptlist.add(pt);
				}
				
				ArrayList<CtcComponent>list=getPtList(ptlist);
				if(list!=null&&list.size()!=0){
					compList.addAll(list);
				}
				retrieveLwfDeatils(compList);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				form.hideWaitSymbol();
			}
		});
	}
	
	private ArrayList<CtcComponent> getPtList(ArrayList<ProfessionalTax> ptlist) {
		ArrayList<CtcComponent> ctcCompList=new ArrayList<CtcComponent>();
		for(ProfessionalTax pt:ptlist){
			CtcComponent comp=new CtcComponent();
			comp.setCtcComponentName("Gross Earning");
			comp.setDeduction(true);
			comp.setIsRecord(false);
			comp.setName(pt.getComponentName());
			comp.setShortName(pt.getShortName());
			comp.setMaxAmount((int) (pt.getPtAmount()*12));
			comp.setGender(pt.getGender());
			comp.setCondition(">");
			if(pt.getRangeToAmt()!=0){
				comp.setConditionValue((int) pt.getRangeToAmt());
			}else{
				comp.setConditionValue((int) pt.getRangeFromAmt());
			}
			ctcCompList.add(comp);
		}
		return ctcCompList;
		
	}
	
	/**
	 * @author Anil , Date : 25-07-2019
	 * @param compList
	 */
	
	protected void retrieveEsicDetails(final ArrayList<CtcComponent> compList) {
		Vector<Filter> vecfilter = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setBooleanvalue(true);
		filter.setQuerryString("status");
		vecfilter.add(filter);

		MyQuerry quer = new MyQuerry(vecfilter, new Esic());
		service.getSearchResult(quer,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				if(result!=null&&result.size()!=0){
					
					for(int i=0;i<compList.size();i++){
						if(compList.get(i).getShortName().trim().equalsIgnoreCase("ESIC")){
							compList.remove(i);
							i--;
						}
					}
					
					ArrayList<Esic> esicList=new ArrayList<Esic>();
					for(SuperModel model:result){
						esic=(Esic) model;
						esicList.add(esic);
					}
					
					ArrayList<CtcComponent>list=getEsicList(esicList);
					if(list!=null&&list.size()!=0){
						compList.addAll(list);
					}
				}
				
				retrievePFDetails(compList);
			}

			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				form.hideWaitSymbol();
			}
		});
	}
	
	protected ArrayList<CtcComponent> getEsicList(ArrayList<Esic> esicList) {
		// TODO Auto-generated method stub
		ArrayList<CtcComponent> ctcCompList=new ArrayList<CtcComponent>();
		for(Esic esic:esicList){
			CtcComponent comp=new CtcComponent();
			comp.setCtcComponentName("Gross-Washing");
			comp.setDeduction(true);
			comp.setIsRecord(false);
			comp.setName(esic.getEsicName());
			comp.setShortName(esic.getEsicShortName());
			comp.setMaxPerOfCTC(esic.getEmployeeContribution());
			comp.setCondition(">");
			comp.setConditionValue((int) esic.getEmployeeToAmount());
			
			comp.setFromRangeAmount(esic.getEmployeeFromAmount());
			comp.setToRangeAmount(esic.getEmployeeToAmount());
			ctcCompList.add(comp);
			
			
			CtcComponent comp1=new CtcComponent();
			comp1.setCtcComponentName("Gross-Washing");
			comp1.setDeduction(true);
			comp1.setIsRecord(true);
			comp1.setName(esic.getEsicName());
			comp1.setShortName(esic.getEsicShortName());
			comp1.setMaxPerOfCTC(esic.getEmployerContribution());
			comp1.setCondition(">");
			comp1.setConditionValue((int) esic.getEmployerToAmount());
			comp1.setFromRangeAmount(esic.getEmployerFromAmount());
			comp1.setToRangeAmount(esic.getEmployerToAmount());
			ctcCompList.add(comp1);
		}
		return ctcCompList;
	}



	/**
	 * @author Anil , Date : 28-06-2019
	 * @param compList
	 */
	
	protected void retrievePFDetails(final ArrayList<CtcComponent> compList) {
		Vector<Filter> vecfilter = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setBooleanvalue(true);
		filter.setQuerryString("status");
		vecfilter.add(filter);

		MyQuerry quer = new MyQuerry(vecfilter, new ProvidentFund());
		service.getSearchResult(quer,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				if(result!=null&&result.size()!=0){
					
					for(int i=0;i<compList.size();i++){
						if(compList.get(i).getShortName().trim().equalsIgnoreCase("PF")){
							compList.remove(i);
							i--;
						}
					}
					
					ArrayList<ProvidentFund> pflist=new ArrayList<ProvidentFund>();
					for(SuperModel model:result){
						pf=(ProvidentFund) model;
						pflist.add(pf);
					}
					
					ArrayList<CtcComponent>list=getPfList(pflist);
					if(list!=null&&list.size()!=0){
						compList.addAll(list);
					}
				}
				/**
				 * @author Anil ,Date : 03-04-2019
				 * if this(DoNotLoadPtAndLwf) process configuration is active then we will
				 * not load pt and lwf component
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CTCTemplate", "DoNotLoadPtAndLwf")){
					for(CtcComponent ctc:compList){
						if(ctc.isDeduction()==false){
							form.earningtable.getListDataProvider().getList().add(ctc);
						}else{
							if(ctc.getIsRecord()==false){
								form.deductionTable.getListDataProvider().getList().add(ctc);
							}else{
								if(ctc.getShortName().equalsIgnoreCase("Gratuity")){
									gratuitylist.add(ctc);
									
								}else{
									form.companyContributionTable.getListDataProvider().getList().add(ctc);
								}
								
							}
						}
					}
					Timer t = new Timer() {
						@Override
						public void run() {
							List<CtcComponent> empContributionList=new ArrayList<CtcComponent>();
							empContributionList.addAll(form.deductionTable.getValue());
							
							List<CtcComponent> compContributionList=new ArrayList<CtcComponent>();
							compContributionList.addAll(form.companyContributionTable.getValue());
							if(gratuitylist!=null&&gratuitylist.size()!=0){
								compContributionList.addAll(gratuitylist);
							}
							form.oblEmpDeductionComp.setListItems(empContributionList);
							form.oblCompContributionComp.setListItems(compContributionList);
						}
					};
					t.schedule(3000);
				}else{
					retrievePtDetails(compList);
				}
				
			}

			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				form.hideWaitSymbol();
			}
		});
	}
	
	private ArrayList<CtcComponent> getPfList(ArrayList<ProvidentFund> pflist) {

		ArrayList<CtcComponent> ctcCompList=new ArrayList<CtcComponent>();
		for(ProvidentFund pf:pflist){
			CtcComponent comp=new CtcComponent();
			comp.setCtcComponentName("Gross-HRA");
			comp.setDeduction(true);
			comp.setIsRecord(false);
			comp.setName(pf.getPfName());
			comp.setShortName(pf.getPfShortName());
			comp.setMaxPerOfCTC(pf.getEmployeeContribution());
			ctcCompList.add(comp);
			
			
			CtcComponent comp1=new CtcComponent();
			comp1.setCtcComponentName("Gross-HRA");
			comp1.setDeduction(true);
			comp1.setIsRecord(true);
			comp1.setName(pf.getPfName());
			comp1.setShortName(pf.getPfShortName());
			comp1.setMaxPerOfCTC(pf.getEmployerContribution());
			comp1.setPercent1(pf.getEmpoyerPF());
			comp1.setPercent2(pf.getEdli());
			ctcCompList.add(comp1);
		}
		return ctcCompList;
	
	}
	
	private void retrieveLwfDeatils(final ArrayList<CtcComponent> compList) {
		
		for(int i=0;i<compList.size();i++){
			if(compList.get(i).getShortName().trim().equalsIgnoreCase("LWF")
					||compList.get(i).getShortName().trim().equalsIgnoreCase("MLWF")
					||compList.get(i).getShortName().trim().equalsIgnoreCase("MWF")){
				compList.remove(i);
				i--;
			}
		}
		Vector<Filter> vecfilter = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setBooleanvalue(true);
		filter.setQuerryString("status");
		vecfilter.add(filter);

		MyQuerry quer = new MyQuerry(vecfilter, new LWF());
		service.getSearchResult(quer,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<LWF> lwfList=new ArrayList<LWF>();
				for(SuperModel model:result){
					LWF lwf=(LWF) model;
					lwfList.add(lwf);
				}
				
				ArrayList<CtcComponent>list=getLwfList(lwfList);
				if(list!=null&&list.size()!=0){
					compList.addAll(list);
				}
//				ArrayList<CtcComponent> gratuitylist=new ArrayList<CtcComponent>();
				for(CtcComponent ctc:compList){
					if(ctc.isDeduction()==false){
						form.earningtable.getListDataProvider().getList().add(ctc);
					}else{
						if(ctc.getIsRecord()==false){
							form.deductionTable.getListDataProvider().getList().add(ctc);
						}else{
							if(ctc.getShortName().equalsIgnoreCase("Gratuity")){
								gratuitylist.add(ctc);
							}else{
								form.companyContributionTable.getListDataProvider().getList().add(ctc);
							}
							
						}
					}
				}
				Timer t = new Timer() {
					@Override
					public void run() {
						List<CtcComponent> empContributionList=new ArrayList<CtcComponent>();
						empContributionList.addAll(form.deductionTable.getValue());
						
						List<CtcComponent> compContributionList=new ArrayList<CtcComponent>();
						compContributionList.addAll(form.companyContributionTable.getValue());
						if(gratuitylist!=null&&gratuitylist.size()!=0){
							compContributionList.addAll(gratuitylist);
						}
						
						form.oblEmpDeductionComp.setListItems(empContributionList);
						form.oblCompContributionComp.setListItems(compContributionList);
					}
				};
				t.schedule(3000);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				form.hideWaitSymbol();
			}
		});
		
	}
	
	private ArrayList<CtcComponent> getLwfList(ArrayList<LWF> lwfList) {
		ArrayList<CtcComponent> ctcCompList=new ArrayList<CtcComponent>();
		for(LWF lwf:lwfList){
			for(LWFBean month:lwf.getLwfList()){
				CtcComponent comp=new CtcComponent();
				comp.setCtcComponentName("Gross Earning");
				comp.setDeduction(true);
				comp.setIsRecord(false);
				comp.setName(lwf.getName()+" "+month.getDeductionMonth());
				comp.setShortName(lwf.getShortName());
				comp.setMaxAmount((int) (lwf.getEmployeeContribution()));
//				comp.setMaxAmount((int) (lwf.getEmployeeContribution()*(12/lwf.getLwfList().size())));
				comp.setCondition(">");
				if(lwf.getToAmt()!=0){
					comp.setConditionValue((int) lwf.getToAmt());
				}else{
					comp.setConditionValue((int) lwf.getFromAmt());
				}
				comp.setSpecificMonth(true);
				comp.setSpecificMonthValue(month.getDeductionMonth());
				ctcCompList.add(comp);
				
				
				CtcComponent comp1=new CtcComponent();
				comp1.setCtcComponentName("Gross Earning");
				comp1.setDeduction(true);
				comp1.setIsRecord(true);
				comp1.setName(lwf.getName()+" "+month.getDeductionMonth());
				comp1.setShortName(lwf.getShortName());
				comp1.setMaxAmount((int) (lwf.getEmployerContribution()));
//				comp1.setMaxAmount((int) (lwf.getEmployerContribution()*(12/lwf.getLwfList().size())));
				comp1.setCondition(">");
				if(lwf.getToAmt()!=0){
					comp1.setConditionValue((int) lwf.getToAmt());
				}else{
					comp1.setConditionValue((int) lwf.getFromAmt());
				}
				comp1.setSpecificMonth(true);
				comp1.setSpecificMonthValue(month.getDeductionMonth());
				ctcCompList.add(comp1);
			}
		}
		return ctcCompList;
	}


	private MyQuerry getCTCTemplateQuery() {
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new CTCTemplate());
		return quer;
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		if(text.equals("New")){
			reactOnNew();
		}
		
		if(text.equals("Allocate Template")){
			panel=new PopupPanel(true);
			panel.center();
			panel.add(datePopup);
			panel.show();
		}
		if(text.equals(AppConstants.CREATEOVERTIME)){
			reactOnCreateOvertime();
		}
	 if(text.equals("Copy Template")){
		 reactToCopyTemplate();
	 }
	}
	
	private void reactToCopyTemplate() {

		form.setToNewState();
		
		
		SalesLineItemTable.newBranchFlag = true;
		final CTCTemplateForm form = new CTCTemplateForm();
		CTCTemplateTable gentable = new CTCTemplateTable();
		gentable.setView(form);
		gentable.applySelectionModle();
		ContractPresenterSearch.staticSuperTable = gentable;
		CTCTemplateSearchProxy searchpopup = new CTCTemplateSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		CTCTemplatePresenter presenter = new CTCTemplatePresenter(form,new CTCTemplate());
		AppMemory.getAppMemory().stickPnel(form);
		form.showWaitSymbol();
		Timer timer = new Timer() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
				
				
				CTCTemplate ctcTemplate = new CTCTemplate();
				
				ctcTemplate.setCount(0);
				ctcTemplate.setCreatedBy("");
				
				ctcTemplate.setMonthlyCtcAmount(model.getMonthlyCtcAmount());
				ctcTemplate.setTotalCtcAmount(model.getTotalCtcAmount());
				ctcTemplate.setGender(model.getGender());
				ctcTemplate.setCategory(model.getCategory());
				ctcTemplate.setStatus(true);
				ctcTemplate.setMonthly(form.rbMonthly.getValue());
				ctcTemplate.setAnnually(form.rbAnnually.getValue());
				ctcTemplate.setMonth(model.getMonth());
				ctcTemplate.setPreviousBonusAmt(model.getPreviousBonusAmt());
				ctcTemplate.setPlCorresponadenceName(model.getPlCorresponadenceName());
				ctcTemplate.setPreviousPaidLeaveAmt(model.getPreviousPaidLeaveAmt());
				ctcTemplate.setBonusCorresponadenceName(model.getBonusCorresponadenceName());
				ctcTemplate.setBonusAmt(model.getBonusAmt());
				ctcTemplate.setBonus(form.cbBonus.getValue());
				ctcTemplate.setPaidLeaveAmt(model.getPaidLeaveAmt());
				ctcTemplate.setPaidLeavesAvailable(form.cbPaidLeave.getValue());
				ctcTemplate.setEarning(model.getEarning());
				ctcTemplate.setDeduction(model.getDeduction());
				ctcTemplate.setCompanyContribution(model.getCompanyContribution());
				form.updateView(ctcTemplate);
				
			}
		};
		timer.schedule(3000);
				
	}



	private void reactOnCreateOvertime() {
		
		OvertimePresenter.initalize();
		
		
		
	}



	public static CTCTemplateForm initialize() {
		CTCTemplateTable gentableScreen = new CTCTemplateTable();
//		CTCTemplateForm form = new CTCTemplateForm(gentableScreen,FormTableScreen.UPPER_MODE, true);
		CTCTemplateForm form = new CTCTemplateForm();
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		
		CTCTemplateSearchProxy.staticSuperTable = gentableScreen;
		CTCTemplateSearchProxy searchpopup = new CTCTemplateSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		CTCTemplatePresenter presenter = new CTCTemplatePresenter(form, new CTCTemplate());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	private void reactOnNew() {
		form.setToNewState();
		initialize();
	}

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}

	@Override
	protected void makeNewModel() {
		model=new CTCTemplate();
		retrieveEarningTable();
	}

	@Override
	public void setModel(CTCTemplate model) {
		this.model=model;
	}
	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		System.out.println("INSIDE ROW COUNT..");
		if(event.getSource()==form.earningtable.getTable()&&form.dbTotalCtcAmount.getValue()!=null){
			System.out.println("EARNING..");
			List<CtcComponent> list = form.earningtable.getDataprovider().getList();
			/**
			 * @author Anil , Date : 11-05-2019
			 * updating HRA calculation on basis basic and DA
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CTCTemplate", "AutoCalculateHR")){
				calculateHRA(list);
			}
			
			Double value = calculateGrossEarning(list);
			if (value != null) {
				form.grossEarning.setValue(value);
				Double ctc = form.dbTotalCtcAmount.getValue();
				if (ctc != null){
					form.difference.setValue(ctc - value);
				}
				Comparator<CtcComponent> ctcSrNoComp = new Comparator<CtcComponent>() {
					@Override
					public int compare(CtcComponent o1,CtcComponent o2) {
						if(o1.getSrNo()!=0&&o2.getSrNo()!=0){
							return Integer.valueOf(o1.getSrNo()).compareTo(Integer.valueOf(o2.getSrNo()));
						}
						return 0;
					}
				};
				
				Collections.sort(form.deductionTable.getValue(), ctcSrNoComp);
				Collections.sort(form.companyContributionTable.getValue(), ctcSrNoComp);
				form.dbEmployeeTotalDeduction.setValue(updateDeductionTbl(form.deductionTable.getValue()));
				form.deductionTable.getTable().redraw();
				form.dbCompanyTotalDeduction.setValue(updateDeductionTbl(form.companyContributionTable.getValue()));
				form.companyContributionTable.getTable().redraw();
			}
		}
		
		if(event.getSource().equals(form.deductionTable.getTable())&&form.dbTotalCtcAmount.getValue()!=null){
			System.out.println("DEDUCTION..");
			List<CtcComponent> list = form.deductionTable.getDataprovider().getList();
			Double value = calculateGrossEarning(list);
			if (value != null) {
				form.dbEmployeeTotalDeduction.setValue(value);
			}
		}
		
		if(event.getSource().equals(form.companyContributionTable.getTable())&&form.dbTotalCtcAmount.getValue()!=null){
			List<CtcComponent> list = form.companyContributionTable.getDataprovider().getList();
			Double value = calculateGrossEarning(list);
			if (value != null) {
				form.dbCompanyTotalDeduction.setValue(value);
			}
		}
	}
	
	private void calculateHRA(List<CtcComponent> list) {
		System.out.println("Inside HRA calculation..");
		if(list!=null&&list.size()!=0){
			boolean hraFlag=false;
			double baseAmt=0;
			double hraAmt=0;
			for(CtcComponent comp:list){
				if(comp.getShortName().equals("HRA")){
					hraFlag=true;
				}
				if(comp.getShortName().equalsIgnoreCase("Basic")||comp.getShortName().equalsIgnoreCase("DA")){
					if(comp.getAmount()!=null){
						baseAmt=baseAmt+comp.getAmount();
					}
				}
			}
			System.out.println("HRA FLAG : "+hraFlag+" Base AMT : "+baseAmt);
			if(hraFlag&&baseAmt!=0){
				for(CtcComponent comp:list){
					if(comp.getShortName().equals("HRA")){
						if(comp.getMaxPerOfCTC()!=null){
							System.out.println("HRACAL");
							hraAmt=baseAmt*comp.getMaxPerOfCTC()/100;
							comp.setAmount(hraAmt);
							System.out.println(" HRA AMT : "+hraAmt);
							break;
						}
					}
				}
			}
		}
		form.deductionTable.getTable().redraw();
		
	}



	public double updateDeductionTbl(List<CtcComponent> value) {
		
		/**
		 * Date : 29-05-2018 By ANIL
		 */
		double pfMaxFixedValue=0;
		String deductionTypeName="";
		boolean pfFlag=false;
		List<ProcessConfiguration> processConfigList=new ArrayList<ProcessConfiguration>();
		for(ProcessConfiguration procConfig:LoginPresenter.globalProcessConfig){
			if((procConfig.getProcessName().equals("PfMaxFixedValue")&&procConfig.isConfigStatus()==true)
					||(procConfig.getProcessName().equals("EsicForDirect")&&procConfig.isConfigStatus()==true)){
				processConfigList.add(procConfig);
			}
		}
		if(processConfigList!=null){
			for(ProcessConfiguration processConf:processConfigList){
				for(ProcessTypeDetails ptDetails:processConf.getProcessList()){
					if(processConf.getProcessName().equalsIgnoreCase("PfMaxFixedValue")&&ptDetails.isStatus()==true){
						pfMaxFixedValue=Double.parseDouble(ptDetails.getProcessType().trim());
					}
					if(processConf.getProcessName().equalsIgnoreCase("EsicForDirect")&&ptDetails.isStatus()==true){
						deductionTypeName=ptDetails.getProcessType().trim();
					}
				}
			}
		}
		/**
		 * End
		 */
		
//		System.out.println("PF MAX VALUE "+pfMaxFixedValue+" ESIC FOR DIRECT "+deductionTypeName);
		
		String empGender="";
		/**
		 * Date : 29-05-2018 By ANIL
		 */
		if(form.lbGender.getSelectedIndex()!=0){
			empGender=form.lbGender.getValue(form.lbGender.getSelectedIndex());
		}
		/**
		 * End
		 */
		System.out.println();
//		System.out.println("UPDATING DEDUCTIONS..");
		System.out.println();
		double sum=0;
		boolean ptFlag=false;
		for(CtcComponent deduction:value){
			double totalAmount = 0;
			boolean genderFlag=true;
			String genderforDeduction="";
			if(deduction.getGender()!=null){
				genderforDeduction=deduction.getGender();
			}
			if(!empGender.equals("")&&!genderforDeduction.equals("")){
				if(genderforDeduction.equals("All")){
					genderFlag=true;
				}else if(empGender.equals(genderforDeduction)){
					genderFlag=true;
				}else{
					genderFlag=false;
				}
			}else if(empGender.equals("")&&genderforDeduction.equals("")){
				genderFlag=true;
			}else if(!empGender.equals("")&&genderforDeduction.equals("")){
				genderFlag=false;
			}else if(empGender.equals("")&&!genderforDeduction.equals("")){
				if(genderforDeduction.equals("All")){
					genderFlag=true;
				}else{
					genderFlag=false;
				}
			}
			if(deduction.isDeduction()==true){
				
				if(deduction.getCtcComponentName()!=null&&!deduction.getCtcComponentName().equals("")
						&&deduction.getMaxPerOfCTC()!=null&&deduction.getMaxPerOfCTC()!=0
						&&(deduction.getMaxAmount()==null||deduction.getMaxAmount()==0)&&genderFlag){
					double amount=getTotalAmountOfEarningComponent(deduction.getCtcComponentName());
//					System.out.println("TOTAL AMT : "+amount);
					
					/**
					 * Date : 29-08-2018 BY ANIL
					 * updates ESCI Calculation for direct employee of Sasha
					 */
					if(deductionTypeName!=null&&!deductionTypeName.equals("")
							/**Rahul Updated ESIC CALCULATION on 25 June 2016(As it is now for all type of employees)*&&form.olbcNumberRange.getSelectedIndex()!=0&&form.olbcNumberRange.getValue().equalsIgnoreCase("Direct")***/
							&&deduction.getShortName().equalsIgnoreCase("ESIC")){
						amount=amount-getTotalAmountOfEarningComponent(deductionTypeName);
					}
					/**
					 * End
					 */
					
					if(deduction.getCondition()!=null&&!deduction.getCondition().equals("")
							&&deduction.getConditionValue()!=null&&deduction.getConditionValue()!=0){
						 
						 if(deduction.getCondition().equals("<")){
							 if(amount<=(deduction.getConditionValue()*12)){
								 totalAmount=amount*deduction.getMaxPerOfCTC()/100;
							 }
						 }else if(deduction.getCondition().equals(">")){
							 if(amount>=(deduction.getConditionValue()*12)){
								 totalAmount=amount*deduction.getMaxPerOfCTC()/100;
							 }
						 }else if(deduction.getCondition().equals("=")){
							 if(amount==(deduction.getConditionValue()*12)){
								 totalAmount=amount*deduction.getMaxPerOfCTC()/100;
							 }
						 }
					}else{
						
						/**
						 * Date : 29-05-2018 By ANIL
						 * added above code for PF Calculation
						 * this calculation is basically for direct employee whose pf is deducted on basic and Da
						 * Date : 23-07-2018 By ANIL
						 * Commented PF calculation code
						 */
//						System.out.println("PF FLAG "+pfFlag);
//						if(pfMaxFixedValue!=0&&deduction.getShortName().equalsIgnoreCase("PF")&&pfFlag==false){
//							
//							pfFlag=true;
////							System.out.println("PF COUNTER..........."+pfFlag);
//							amount=0;
//							for(CtcComponent ded:value){
//								if(ded.getShortName().trim().equalsIgnoreCase("PF")){
//									amount+=getTotalAmountOfEarningComponent(ded.getCtcComponentName());
//								}
//							}
////							System.out.println("PF AMOUNT "+amount);
//							if(amount>pfMaxFixedValue*12){
//								amount=pfMaxFixedValue*12;
//							}
//						}else if(pfMaxFixedValue!=0&&deduction.getShortName().equalsIgnoreCase("PF")&&pfFlag==true){
//							amount=0;
//						}else if(pfMaxFixedValue==0&&deduction.getShortName().equalsIgnoreCase("PF")&&pfFlag==false){
//							
//						}
						
						/**
						 * 
						 * End
						 */
						totalAmount=amount*deduction.getMaxPerOfCTC()/100;
					}
				}else if((deduction.getCtcComponentName()!=null&&!deduction.getCtcComponentName().equals(""))
						&&(deduction.getMaxPerOfCTC()==null||deduction.getMaxPerOfCTC()==0)
						&&(deduction.getMaxAmount()!=null&&deduction.getMaxAmount()!=0)&&
						(genderFlag||deduction.getShortName().equalsIgnoreCase("LWF"))){
					
					double amount=getTotalAmountOfEarningComponent(deduction.getCtcComponentName());
					System.out.println("INSIDE FLAT AMT "+deduction.getName()+" "+"TOTAL AMT : "+amount);
					if(deduction.getCondition()!=null&&!deduction.getCondition().equals("")
							&&deduction.getConditionValue()!=null&&deduction.getConditionValue()!=0){
						 
						 if(deduction.getCondition().equals("<")){
							 if(amount<=(deduction.getConditionValue()*12)){
								 if(deduction.getShortName().equalsIgnoreCase("PT")){
									 if(ptFlag){
										 if(validateDeduction(value,deduction,amount)){
											 totalAmount=deduction.getMaxAmount();
											 ptFlag=true;
										 }
									 }else{
										 totalAmount=deduction.getMaxAmount();
										 ptFlag=true;
									 }
								 }else{
									 totalAmount=deduction.getMaxAmount();
								 }
							 }
						} else if (deduction.getCondition().equals(">")) {
							// if(amount>=deduction.getConditionValue()){
							// totalAmount=deduction.getMaxAmount();
							// }
							System.out.println("PT AMT : " + amount+ " Conditional Value : "+ deduction.getConditionValue() * 12 + " "+ deduction.getCondition());
							if (amount >= (deduction.getConditionValue() * 12)) {
								System.out.println();
								System.out.println("1");
								if (deduction.getShortName().equalsIgnoreCase("PT")) {
									if (ptFlag) {
										System.out.println("3");
										if (validateDeduction(value, deduction,amount)) {
											System.out.println("4");
											totalAmount = deduction.getMaxAmount();
											ptFlag = true;
										}
										System.out.println("5");
									} else {
										System.out.println("2");
										totalAmount = deduction.getMaxAmount();
										ptFlag = true;
									}
								} else {
									System.out.println("4");
									totalAmount = deduction.getMaxAmount();
								}
							}

						}else if(deduction.getCondition().equals("=")){
//							 if(amount==deduction.getConditionValue()){
//								 totalAmount=deduction.getMaxAmount();
//							 }
							 if(amount==(deduction.getConditionValue()*12)){
							 if(deduction.getShortName().equalsIgnoreCase("PT")){
								 if(ptFlag){
									 if(validateDeduction(value,deduction,amount)){
										 totalAmount=deduction.getMaxAmount();
										 ptFlag=true;
									 }
								 }else{
									 totalAmount=deduction.getMaxAmount();
									 ptFlag=true;
								 }
							 }else{
								 totalAmount=deduction.getMaxAmount();
							 }}
						 }
					}else{
//						totalAmount=deduction.getMaxAmount();
						if(deduction.getShortName().equalsIgnoreCase("PT")){
							 if(ptFlag){
								 if(validateDeduction(value,deduction,amount)){
									 totalAmount=deduction.getMaxAmount();
									 ptFlag=true;
								 }
							 }else{
								 totalAmount=deduction.getMaxAmount();
								 ptFlag=true;
							 }
						 }else{
							 totalAmount=deduction.getMaxAmount();
						 }
					}
					
				}
				
				/**
				 * @author Anil , Date : 28-06-2019
				 * calculating Pf on the basis of updated logic
				 */
				if(deduction.getShortName().equalsIgnoreCase("PF")
						&&deduction.getCtcComponentName().equalsIgnoreCase("Gross-HRA")){
					if(deduction.isDeduction()){
						if(deduction.getIsRecord()==true){
							totalAmount=getTotalAmountOfPFComponent(pf,false,true,pfMaxFixedValue);
						}else{
							totalAmount=getTotalAmountOfPFComponent(pf,true,false,pfMaxFixedValue);
						}
					}
				}
				
				/**
				 * @author Anil , Date : 26-07-2019
				 * calculating ESIC on the basis of updated logic
				 */
				if((deduction.getShortName().equalsIgnoreCase("ESIC")
						&&deduction.getCtcComponentName().equalsIgnoreCase("Gross-Washing"))||deduction.getShortName().equalsIgnoreCase("ESIC")){
					if(deduction.isDeduction()){
						if(deduction.getIsRecord()==true){
							totalAmount=getTotalAmountOfEsicComponent(esic,false,true);
						}else{
							totalAmount=getTotalAmountOfEsicComponent(esic,true,false);
						}
					}
				}
				
				deduction.setAmount(totalAmount);
			}
			
			/**
			 * Date : 03-01-2018 By ANIL
			 * If gender is selected then LWF calculation was not happening
			 * As LWF
			 */
			
			
			
			
			sum+=totalAmount;
		}
//		System.out.println("SUM : "+sum);
		return sum;
	}
	
	private double getTotalAmountOfEsicComponent(Esic esic, boolean employeeContribution,boolean employerContribution) {
		// TODO Auto-generated method stub
		double amount = 0;
		double rateAmount=0;
		
		if(esic!=null){
			/**
			 * @author Anil @since 02-04-2021
			 * commenting above logic of calculating rate base amount
			 */
//			for(CtcComponent ctc:form.earningtable.getValue()){
//				rateAmount=rateAmount+(ctc.getAmount()/12);
//			}
			for(OtEarningComponent earnComp:esic.getCompList()){
				for(CtcComponent ctc:form.earningtable.getValue()){
					if(ctc.getName().equals(earnComp.getComponentName())){
						amount=amount+ ctc.getAmount();
						
						/**
						 * @author Anil @since 02-04-2021
						 * should be calculated on the list component
						 */
						rateAmount=rateAmount+(ctc.getAmount()/12);
					}
				}
			}
		}
		
		/**
		 * @author Anil @since 02-04-2021
		 * calculate rate amount on the basis of bonus and paid leave as well
		 * Raised by Rahul Tiwari for Alkosh
		 */
		
		if(form.cbIncludeBonusInEarning.getValue().equals(true)){
			if(form.dbBonusAmt.getValue()!=null){
				rateAmount=rateAmount+form.dbBonusAmt.getValue();
				
				amount=amount+form.dbBonusAmt.getValue();
			}
		}
		
		if(form.cbIncludePlInEarning.getValue().equals(true)){
			if(form.dbPaidLeaveAmt.getValue()!=null){
				rateAmount=rateAmount+form.dbPaidLeaveAmt.getValue();
				
				amount=amount+form.dbPaidLeaveAmt.getValue();
			}
		}
		
		if(employeeContribution){
			if(rateAmount>=esic.getEmployeeFromAmount()&&rateAmount<=esic.getEmployeeToAmount()){
				amount=(amount*esic.getEmployeeContribution())/100;
			}else{
				amount=0;
			}
		}
		if(employerContribution){
			if(rateAmount>=esic.getEmployerFromAmount()&&rateAmount<=esic.getEmployerToAmount()){
				amount=(amount*esic.getEmployerContribution())/100;
			}else{
				amount=0;
			}
		}
		return getEsicRoundOff(amount);
	}

	public double getEsicRoundOff(double amount){
		int esicAmount=0;
		esicAmount=(int) amount;
		
		if(esicAmount<amount){
			esicAmount++;
		}
		return esicAmount;
	}

	private boolean validateDeduction(List<CtcComponent> value,CtcComponent currDed, double amount) {
		System.out.println();
		System.out.println("INSIDE PT VALIDATION ");
		for(CtcComponent appDed:value){
			if(currDed.getShortName().equals(appDed.getShortName())
					&&currDed.getGender().equals(appDed.getGender())){
				if(appDed.getAmount()!=0){
					System.out.println("CTC AMT "+appDed.getAmount()+" AMOUNT "+amount+" MAX AMT "+appDed.getConditionValue()*12+" CURR MAX AMT "+currDed.getConditionValue()*12);
//					if(ctc.getConditionValue()*12>=amount&&ctc.getConditionValue()*12<deduction.getConditionValue()*12){
////						return false;
//						
//						ctc.setAmount(0d);;
//						return true;
//					}else{
////						ctc.setAmount(0d);;
////						return true;
//						return false;
//					}
					
					if(appDed.getConditionValue()*12>=amount){
						if(appDed.getConditionValue()*12<currDed.getConditionValue()*12){
							return false;
						}else{
							appDed.setAmount(0d);;
							return true;
						}
					}else{
						appDed.setAmount(0d);;
						return true;
					}
				}
			}
		}
		return true;
	}

	private double getTotalAmountOfEarningComponent(String componentName){
		double amount = 0;
		for(CtcComponent ctc:form.earningtable.getValue()){
			if(componentName.equals("Gross Earning")){
				amount=amount+ctc.getAmount();
			}
			if(ctc.getName().equals(componentName)){
				return ctc.getAmount();
			}
		}
		return amount;
	}

	private Double calculateGrossEarning(List<CtcComponent> earnings) {
		Double sum = 0.0;
		for (CtcComponent temp : earnings) {
			if (temp.getAmount() != null){
				sum = sum + temp.getAmount();
			}
		}
		return sum;

	}

	@Override
	public void onValueChange(ValueChangeEvent<Double> event) {
			/**START
		 * Date 6-3-2019 added by Amol for updating the value of TotalCTCAmount  based on the
		 *  MonthlyCTCAmount as well as updating the MonthlyCTCAmount based on TotalCTCAmount
		 */
		if(event.getSource().equals(form.dbMonthlyCtc)){
			double monthlyAmt=0;
			double yearlyAmt=0;
			if(form.dbMonthlyCtc.getValue()!=null){
				monthlyAmt=form.dbMonthlyCtc.getValue();
				yearlyAmt=monthlyAmt*12;
				form.dbTotalCtcAmount.setValue(yearlyAmt);
			}
		}
		
		if(event.getSource().equals(form.dbTotalCtcAmount)){
			double yearlyAmount=0;
			double monthlyAmount=0;
			if(form.dbTotalCtcAmount.getValue()!=null){
				yearlyAmount=form.dbTotalCtcAmount.getValue();
				monthlyAmount=yearlyAmount/12;
				form.dbMonthlyCtc.setValue(monthlyAmount);
			}
		}
		/**
		 * End 
		 */
		/**Date 22-4-2019  by Amol 
		 * to add total Monthly CTC
		 */
		if(event.getSource().equals(form.dbTotalCtcAmount)){
			double yearlyAmount=0;
			double monthlyAmount=0;
			if(form.dbTotalCtcAmount!=null){
				yearlyAmount=form.dbTotalCtcAmount.getValue();
				monthlyAmount=yearlyAmount/12;
				form.dbMonthlyCtcAmount.setValue(monthlyAmount);
			}
		}
		
		if(event.getSource().equals(form.dbMonthlyCtc)){
			if(form.dbMonthlyCtc!=null){
				form.dbMonthlyCtcAmount.setValue(form.dbMonthlyCtc.getValue());
			}
			
		}
		
		
	
		Double value = form.dbTotalCtcAmount.getValue();
		if (value == null){
			form.dbTotalCtcAmount.setValue(0d);
		}
		SalaryHeadPresenterTableProxy.ctcAmount = form.dbTotalCtcAmount.getValue();
		form.ctcCopy.setValue(value);
		Double gross = form.grossEarning.getValue();
		if (gross != null){
			form.difference.setValue(value - gross);
		}
		
		if(event.getSource().equals(form.dbBonusAmt)){
			form.dbEmployeeTotalDeduction.setValue(updateDeductionTbl(form.deductionTable.getValue()));
			form.deductionTable.getTable().redraw();
			form.dbCompanyTotalDeduction.setValue(updateDeductionTbl(form.companyContributionTable.getValue()));
			form.companyContributionTable.getTable().redraw();	
		}
		
		if(event.getSource().equals(form.dbPaidLeaveAmt)){
			form.dbEmployeeTotalDeduction.setValue(updateDeductionTbl(form.deductionTable.getValue()));
			form.deductionTable.getTable().redraw();
			form.dbCompanyTotalDeduction.setValue(updateDeductionTbl(form.companyContributionTable.getValue()));
			form.companyContributionTable.getTable().redraw();	
		}
		
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		super.onClick(event);
		
		if(event.getSource()==datePopup.btnOne){
			String msg="";
			if(datePopup.getFromDate().getValue()==null){
				msg=msg+"Please select from date."+"\n";
			}
			if(datePopup.getToDate().getValue()==null){
				msg=msg+"Please select to date."+"\n";
			}
			if(datePopup.effectiveFromDate.getValue()==null){
				msg=msg+"Please select effective from date.";
			}
			if(datePopup.effectiveToDate.getValue()==null){
				msg=msg+"Please select effective To date.";
			}
			
			if(!msg.equals("")){
				form.showDialogMessage(msg);
				return;
			}
			form.showWaitSymbol();
			/****Date 9-8-2019 added effective to date filter*****/
			ctcAllocationService.reviseSalary(null, model, datePopup.getFromDate().getValue(), datePopup.getToDate().getValue(), datePopup.effectiveFromDate.getValue(),LoginPresenter.loggedInUser,datePopup.effectiveToDate.getValue(),null,false,new AsyncCallback<ArrayList<EmployeeInfo>>(){
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
					view.showDialogMessage("Failed to execute Salary Revision Process !");
					panel.hide();
				}
				@Override
				public void onSuccess(ArrayList<EmployeeInfo> result) {
					form.hideWaitSymbol();
					panel.hide();
					if(result.size()!=0){
						view.showDialogMessage("Salary Revision Process Successfully executed !",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
					}else{
						view.showDialogMessage("No record found for allocation.",GWTCAlert.OPTION_ROUNDED_BLUE,GWTCAlert.OPTION_ANIMATION);
					}
					
				}
			});
		}
		
		if(event.getSource()==datePopup.btnTwo){
			panel.hide();
		}
		
		
		if(event.getSource()==form.cbIncludeBonusInEarning){
			form.dbEmployeeTotalDeduction.setValue(updateDeductionTbl(form.deductionTable.getValue()));
			form.deductionTable.getTable().redraw();
			form.dbCompanyTotalDeduction.setValue(updateDeductionTbl(form.companyContributionTable.getValue()));
			form.companyContributionTable.getTable().redraw();
		}
		
		if(event.getSource()==form.cbIncludePlInEarning){
			form.dbEmployeeTotalDeduction.setValue(updateDeductionTbl(form.deductionTable.getValue()));
			form.deductionTable.getTable().redraw();
			form.dbCompanyTotalDeduction.setValue(updateDeductionTbl(form.companyContributionTable.getValue()));
			form.companyContributionTable.getTable().redraw();
		}
		
	}
	
	
	
	public void reactOnDownload() {
		ArrayList<CTCTemplate> ctctemplateArray = new ArrayList<CTCTemplate>();
		List<CTCTemplate> list = (List<CTCTemplate>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		ctctemplateArray.addAll(list);
		csvservice.setCTCtemplatelist(ctctemplateArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 160;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	/**Date 7-2-2019 
	 * @author Amol 
	 * added for update deduction and company contribution data on the base of  "state" selection****/
	public void updateEmployeeAndEmployersContributions(final ArrayList<CtcComponent> compList ) {
		System.out.println("STARTUPDATEEMPLOYEEAND CONTRIBUTIONSTART");
//		final ArrayList<CtcComponent> compList = new ArrayList<CtcComponent>();
//		retrievePtDetails(compList);
		
		/***
		 * 
		 * 
		 */

		for (int i = 0; i < compList.size(); i++) {
			if (compList.get(i).getShortName().trim().equalsIgnoreCase("PT")) {
				compList.remove(i);
				i--;
			}
		}

		Vector<Filter> vecfilter = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setBooleanvalue(true);
		filter.setQuerryString("status");
		vecfilter.add(filter);
		
		if(form.state.getSelectedIndex()!=0){
			filter = new Filter();
			filter.setStringValue(form.state.getValue());
			filter.setQuerryString("state");
			vecfilter.add(filter);
		}
		

		MyQuerry quer = new MyQuerry(vecfilter, new ProfessionalTax());
		service.getSearchResult(quer,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						ArrayList<ProfessionalTax> ptlist = new ArrayList<ProfessionalTax>();
						for (SuperModel model : result) {
							ProfessionalTax pt = (ProfessionalTax) model;
							ptlist.add(pt);
						}
						ArrayList<CtcComponent> list = getPtList(ptlist);
						if (list != null && list.size() != 0) {
							compList.addAll(list);
						}
						
						
						/**
						 * 
						 */
						
						for (int i = 0; i < compList.size(); i++) {
							if (compList.get(i).getShortName().trim().equalsIgnoreCase("LWF")
									|| compList.get(i).getShortName().trim().equalsIgnoreCase("MLWF")
									|| compList.get(i).getShortName().trim().equalsIgnoreCase("MWF")) {
								compList.remove(i);
								i--;
							}
						}
						Vector<Filter> vecfilter = new Vector<Filter>();
						Filter filter = null;
						/**
						 * up
						 */
						filter = new Filter();
						filter.setBooleanvalue(true);
						filter.setQuerryString("status");
						vecfilter.add(filter);
						
						if(form.state.getSelectedIndex()!=0){
							filter = new Filter();
							filter.setStringValue(form.state.getValue());
							filter.setQuerryString("state");
							vecfilter.add(filter);
						}

						MyQuerry quer = new MyQuerry(vecfilter, new LWF());
						service.getSearchResult(quer,new AsyncCallback<ArrayList<SuperModel>>() {
									@Override
									public void onSuccess(ArrayList<SuperModel> result) {
										ArrayList<LWF> lwfList = new ArrayList<LWF>();
										for (SuperModel model : result) {
											LWF lwf = (LWF) model;
											lwfList.add(lwf);
										}

										ArrayList<CtcComponent> list = getLwfList(lwfList);
										if (list != null && list.size() != 0) {
											compList.addAll(list);
										}

										for (CtcComponent ctc : compList) {
											if (ctc.getIsRecord() == false) {
												form.deductionTable.getListDataProvider().getList().add(ctc);
											} else {
												form.companyContributionTable.getListDataProvider().getList().add(ctc);
											}
										}
										
										form.deductionTable.getTable().redraw();
										form.companyContributionTable.getTable().redraw();
										
										Timer t = new Timer() {
											@Override
											public void run() {
												List<CtcComponent> empContributionList = new ArrayList<CtcComponent>();
												empContributionList.addAll(form.deductionTable.getValue());

												List<CtcComponent> compContributionList = new ArrayList<CtcComponent>();
												compContributionList.addAll(form.companyContributionTable.getValue());

												form.oblEmpDeductionComp.setListItems(empContributionList);
												form.oblCompContributionComp.setListItems(compContributionList);
											}
										};
										t.schedule(3000);
									}

									@Override
									public void onFailure(Throwable caught) {
										caught.printStackTrace();
										form.hideWaitSymbol();
									}
								});
						
						
						
						/**
						 * 
						 */
						
					}
					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
						form.hideWaitSymbol();
					}
				});
	
		
		
		
		/***
		 * 
		 * 
		 * 
		 */

	
		System.out.println("STARTUPDATEEMPLOYEEAND CONTRIBUTION ENDDDDDDDDDDDDDDDDD");
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		System.out.println("ONCHANGED");
		if(event.getSource().equals(form.state)){
			ArrayList<CtcComponent> compList=new ArrayList<CtcComponent>();
			if(form.deductionTable.getValue()!=null&&form.deductionTable.getValue().size()!=0){
				compList.addAll(form.deductionTable.getValue());
			}
			
			if(form.companyContributionTable.getValue()!=null&&form.companyContributionTable.getValue().size()!=0){
				compList.addAll(form.companyContributionTable.getValue());
			}
			form.deductionTable.connectToLocal();
			form.companyContributionTable.connectToLocal();
			updateEmployeeAndEmployersContributions(compList);
		}
		System.out.println("ONCHANGED END");
	}
	
	public double getTotalAmountOfPFComponent(ProvidentFund pf,boolean isEmployee,boolean isEmployer,double cappedAmt){
		double amount = 0;
		
		if(pf!=null){
			for(OtEarningComponent earnComp:pf.getCompList()){
				for(CtcComponent ctc:form.earningtable.getValue()){
					if(ctc.getName().equals(earnComp.getComponentName())){
						amount=amount+ ctc.getAmount();
					}
				}
			}
		}
		if(cappedAmt!=0){
			if(amount>(cappedAmt*12)){
				amount=cappedAmt*12;
			}
		}
		if(isEmployee){
			amount=(amount*pf.getEmployeeContribution())/100;
		}
		if(isEmployer){
			amount=(amount*pf.getEmployerContribution())/100;
		}
		return amount;
	}
	

}
