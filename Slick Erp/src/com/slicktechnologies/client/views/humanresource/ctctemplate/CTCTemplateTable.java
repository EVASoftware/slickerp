package com.slicktechnologies.client.views.humanresource.ctctemplate;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;

public class CTCTemplateTable extends SuperTable<CTCTemplate> {

	TextColumn<CTCTemplate> getColCtcTempId;
	TextColumn<CTCTemplate> getColCtcTempName;
	TextColumn<CTCTemplate> getColCtcAmount;
	TextColumn<CTCTemplate> getColCtcStatus;
	
	@Override
	public void createTable() {
		getColCtcTempId();
		getColCtcTempName();
		getColCtcAmount();
		getColCtcStatus();
		
	}

	private void getColCtcTempId() {
		getColCtcTempId=new TextColumn<CTCTemplate>() {
			@Override
			public String getValue(CTCTemplate object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getColCtcTempId,"CTC Template Id");
		getColCtcTempId.setSortable(true);
	}

	private void getColCtcTempName() {
		getColCtcTempName=new TextColumn<CTCTemplate>() {
			@Override
			public String getValue(CTCTemplate object) {
				return object.getCtcTemplateName()+"";
			}
		};
		table.addColumn(getColCtcTempName,"CTC Template Name");
		getColCtcTempName.setSortable(true);
	}

	private void getColCtcAmount() {
		getColCtcAmount=new TextColumn<CTCTemplate>() {
			@Override
			public String getValue(CTCTemplate object) {
				return object.getTotalCtcAmount()+"";
			}
		};
		table.addColumn(getColCtcAmount,"Total CTC");
		getColCtcAmount.setSortable(true);
	}

	private void getColCtcStatus() {
		getColCtcStatus=new TextColumn<CTCTemplate>() {
			@Override
			public String getValue(CTCTemplate object) {
				if(object.isStatus()==true){
					return "Active";
				}
				return "Inactive";
			}
		};
		table.addColumn(getColCtcStatus,"Status");
		getColCtcStatus.setSortable(true);
	}
	
	

	@Override
	public void addColumnSorting() {
//		super.addColumnSorting();
		addSortnigOnCtcTempIdCol();
		addSortnigOnCtcTempNameCol();
		addSortnigOnCtcTempCTCAmountCol();
		addSortnigOnCtcTempStatusCol();
	}

	private void addSortnigOnCtcTempIdCol() {
		List<CTCTemplate> list = getDataprovider().getList();
		columnSort = new ListHandler<CTCTemplate>(list);
		columnSort.setComparator(getColCtcTempId, new Comparator<CTCTemplate>() {
			@Override
			public int compare(CTCTemplate e1, CTCTemplate e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortnigOnCtcTempNameCol() {
		List<CTCTemplate> list = getDataprovider().getList();
		columnSort = new ListHandler<CTCTemplate>(list);
		columnSort.setComparator(getColCtcTempName,new Comparator<CTCTemplate>() {
			@Override
			public int compare(CTCTemplate e1, CTCTemplate e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCtcTemplateName() != null&& e2.getCtcTemplateName() != null) {
						return e1.getCtcTemplateName().compareTo(e2.getCtcTemplateName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortnigOnCtcTempCTCAmountCol() {
		List<CTCTemplate> list = getDataprovider().getList();
		columnSort = new ListHandler<CTCTemplate>(list);
		columnSort.setComparator(getColCtcAmount, new Comparator<CTCTemplate>() {
			@Override
			public int compare(CTCTemplate e1, CTCTemplate e2) {
				if (e1 != null && e2 != null) {
					if (e1.getTotalCtcAmount() == e2.getTotalCtcAmount()) {
						return 0;
					}
					if (e1.getTotalCtcAmount() > e2.getTotalCtcAmount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortnigOnCtcTempStatusCol() {
		List<CTCTemplate> list = getDataprovider().getList();
		columnSort = new ListHandler<CTCTemplate>(list);
		columnSort.setComparator(getColCtcTempName,new Comparator<CTCTemplate>() {
			@Override
			public int compare(CTCTemplate e1, CTCTemplate e2) {
				if (e1 != null && e2 != null) {
					Boolean flag1=e1.isStatus();
					Boolean flag2=e2.isStatus();
					if (flag1 != null&& flag2 != null) {
						return flag1.compareTo(flag2);
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
