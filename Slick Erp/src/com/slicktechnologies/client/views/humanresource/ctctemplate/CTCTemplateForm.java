package com.slicktechnologies.client.views.humanresource.ctctemplate;

import java.awt.Checkbox;
import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.humanresource.ctc.SalaryHeadPresenterTableProxy;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.humanresourcelayer.Bonus;
import com.slicktechnologies.shared.common.humanresourcelayer.PaidLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.OtEarningComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class CTCTemplateForm extends FormScreen<CTCTemplate> implements ClickHandler, ValueChangeHandler<String>, ChangeHandler {
	TextBox tbCtcTempId;
	TextBox tbCtcTempName;
	CheckBox cbStatus;
	DoubleBox dbTotalCtcAmount;
	final static GenricServiceAsync async=GWT.create(GenricService.class);
	DoubleBox grossEarning,difference,ctcCopy;
	
	SalaryHeadPresenterTableProxy earningtable;
	SalaryHeadPresenterTableProxy deductionTable;
	
	DoubleBox dbTotalDeductonAmount;
	
	FormField fgrossEarning;
	FormField fsecgrossearnings;
	FormField fdifference;
	
	FormField fTotDeduction;
	
	/**
	 * @author Anil
	 * @since 03-05-2018
	 * 
	 * Adding companies deduction at employee level
	 * For Sasha ERP/Facility Mgmt
	 */
	
	String versionCheck;
	
	SalaryHeadPresenterTableProxy companyContributionTable;
	DoubleBox dbEmployeeTotalDeduction;
	DoubleBox dbCompanyTotalDeduction;
	
	FormField fempTotDeduction, fcomTotDeduction;
	
	/**
	 * Date : 09-05-2018 BY ANIL
	 * Added Tab Panel
	 */
	TabPanel tabPanel ;
	
	/**
	 * Date : 29-05-2018 By ANIL
	 * adding static gender list,which used at payroll for calculating PT 
	 */
	ListBox lbGender;
	
	/**
	 * Date : 29-05-2018 By ANIL
	 * Added number range process for Sasha ERP
	 */
	ObjectListBox<Config> olbcNumberRange;
	
	CTCTemplate ctcTempObj;
	
	/**
	 * Rahul added on 14 July 2018 Description: By Default true.
	 */
	CheckBox cbPaidLeave;

	/**
	 * Ends Rahul Code
	 */
	
	/**
	 * Date : 20-07-2018 BY ANIL
	 */
	ObjectListBox<CtcComponent> oblEarningComp;
	ObjectListBox<CtcComponent> oblEmpDeductionComp;
	ObjectListBox<CtcComponent> oblCompContributionComp;
	
	Button btnEarn;
	Button btnEmpDeduction;
	Button btnCompContribution;
	
	
	/**
	 * Date : 21-08-2018 BY ANIL
	 * Added revised button : if any earning component is revised as per the government circular so we have to add this extra payment in next month payroll 
	 */
	Button btnRevise;
	
	/**
	 * Date : 25-08-2018 By ANIL
	 * Updated Paid Leave and Bonus 
	 */
	RadioButton rbMonthly;
	RadioButton rbAnnually;
	
	DoubleBox dbPaidLeaveAmt;
	DoubleBox dbBonusAmt;
	CheckBox cbBonus;
	
	/**
	 * End
	 * 
	 */
	
	ListBox lbMonth;
	/** date 31.8.2018 added by komal **/
	ObjectListBox<ConfigCategory> olbCTCCategory;
	
	
	/**
	 * Date:31-12-2018 by ANIL
	 */
	Button btnRevisePaidLeaveAndBonus;
	DoubleBox dbPreviousPaidLeaveAmt;
	DoubleBox dbPreviousBonusAmt;
	InlineLabel blankLable;
	
	/**
	 * End
	 */
	/**Date 6-2-2019 
	 * @author Amol
	 * added state Listbox
	 * 
	 */
	 ObjectListBox<State>  state;
	
	/**
	 * Date 6-3-2019 
	 * added by Amol
	 * 
	 */
	 DoubleBox dbMonthlyCtc;
	 
	 /**Date 20-4-2019 by Amol
		 * added for total CTC Amount
		 */
    DoubleBox dbMonthlyCtcAmount;
		
	 
	 
	 /**
	  * @author Anil , Date : 17-06-2019
	  * Paid Leave and bonus as per the formula 
	  * for riseon raised by Rahul Tiwari
	  */
    boolean isPlAndBonusAsPerTheFormula=false;
    CheckBox cbIncludePlInEarning;
    CheckBox cbIncludeBonusInEarning;
    TextBox tbPlCorrespondanceName;
    TextBox tbBonusCorrespondanceName;
    ObjectListBox<PaidLeave> oblPaidLeave;
    
    ObjectListBox<Bonus> oblBonus;
	 
	 
	 
	 
	public  CTCTemplateForm() {
		super(FormStyle.DEFAULT);
		createGui();
		
		tbCtcTempId.setEnabled(false);
		
		grossEarning.setEnabled(false);
		grossEarning.getElement().setClassName("footer-grid");
		grossEarning.setWidth("100px");
		
		ctcCopy.getElement().setClassName("footer-grid");
		ctcCopy.setEnabled(false);
		ctcCopy.setWidth("100px");
		
		difference.getElement().setClassName("footer-grid");
		difference.setEnabled(false);
		difference.setWidth("100px");
		
		dbEmployeeTotalDeduction.getElement().setClassName("footer-grid");
		dbEmployeeTotalDeduction.setEnabled(false);
		dbEmployeeTotalDeduction.setWidth("100px");
		 
		dbCompanyTotalDeduction.getElement().setClassName("footer-grid");
		dbCompanyTotalDeduction.setEnabled(false);
		dbCompanyTotalDeduction.setWidth("100px");
		
		dbMonthlyCtcAmount.getElement().setClassName("footer-grid");
		dbMonthlyCtcAmount.setEnabled(false);
		dbMonthlyCtcAmount.setWidth("100px");
		 
		cbPaidLeave.setValue(false);
		dbPaidLeaveAmt.setEnabled(false);
		 
		cbBonus.setValue(false);
		dbBonusAmt.setEnabled(false);
		 
		rbMonthly.setValue(true);
		rbAnnually.setValue(false);
		lbMonth.setEnabled(false);
		
		btnRevisePaidLeaveAndBonus.setEnabled(false);
		dbPreviousPaidLeaveAmt.setEnabled(false);
		dbPreviousBonusAmt.setEnabled(false);
		blankLable.setVisible(false);
		
		/**** Date 6-3-2019  
		 * @author Amol added a validation for same CTC template name***/
		tbCtcTempName.addValueChangeHandler(this);
		dbMonthlyCtc.setEnabled(true);
		
		oblPaidLeave.setEnabled(false);
		oblBonus.setEnabled(false);
		cbIncludeBonusInEarning.setValue(false);
		cbIncludePlInEarning.setValue(false);
		
		}
	
//	public CTCTemplateForm(SuperTable<CTCTemplate> table, int mode,boolean captionmode) {
//		super(table, mode, captionmode);
//		createGui();
//		tbCtcTempId.setEnabled(false);
//		
//		grossEarning.setEnabled(false);
//		grossEarning.getElement().setClassName("footer-grid");
//		grossEarning.setWidth("100px");
//		
//		ctcCopy.getElement().setClassName("footer-grid");
//		ctcCopy.setEnabled(false);
//		ctcCopy.setWidth("100px");
//		
//		difference.getElement().setClassName("footer-grid");
//		difference.setEnabled(false);
//		difference.setWidth("100px");
//		
////		dbTotalDeductonAmount.getElement().setClassName("footer-grid");
////		dbTotalDeductonAmount.setEnabled(false);
////		dbTotalDeductonAmount.setWidth("100px");
////		
////		
////		InlineLabel lbl = fgrossEarning.getHeaderLabel();
////		lbl.getElement().setClassName("footer-grid");
////		InlineLabel secgrosslabel = fsecgrossearnings.getHeaderLabel();
////		secgrosslabel.getElement().setClassName("footer-grid");
////		InlineLabel differencelabel = fdifference.getHeaderLabel();
////		differencelabel.getElement().setClassName("footer-grid");
////		
////		InlineLabel totalDeductionLbl = fTotDeduction.getHeaderLabel();
////		totalDeductionLbl.getElement().setClassName("footer-grid");
////		
//		
//		 dbEmployeeTotalDeduction.getElement().setClassName("footer-grid");
//		 dbEmployeeTotalDeduction.setEnabled(false);
//		 dbEmployeeTotalDeduction.setWidth("100px");
//		 
//		 dbCompanyTotalDeduction.getElement().setClassName("footer-grid");
//		 dbCompanyTotalDeduction.setEnabled(false);
//		 dbCompanyTotalDeduction.setWidth("100px");
////		 
//////		 InlineLabel empDedLbl=fempTotDeduction.getHeaderLabel();
//////		 empDedLbl.getElement().setClassName("footer-grid");
////		 
////		 InlineLabel compDedLbl=fcomTotDeduction.getHeaderLabel();
////		 compDedLbl.getElement().setClassName("footer-grid");
//		 
//		
//		 
//		 cbPaidLeave.setValue(false);
//		 dbPaidLeaveAmt.setEnabled(false);
//		 
//		 cbBonus.setValue(false);
//		 dbBonusAmt.setEnabled(false);
//		 
//		 rbMonthly.setValue(true);
//		 rbAnnually.setValue(false);
//		 lbMonth.setEnabled(false);
//		
//		
//	}

	private void initalizeWidget(){
		/**
		 * @author Anil ,  Date ; 17-06-2019
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CTCTemplate", "PlAndBonusAsPerTheFormula")){
			isPlAndBonusAsPerTheFormula=true;
		}
		System.out.println(" isPlAndBonusAsPerTheFormula "+isPlAndBonusAsPerTheFormula);
	
		tbCtcTempId=new TextBox();
		tbCtcTempId.setEnabled(false);
		tbCtcTempName=new TextBox();
		
		cbStatus=new CheckBox();
		cbStatus.setValue(true);
		
		dbTotalCtcAmount=new DoubleBox();
		
		grossEarning=new DoubleBox();
		grossEarning.setEnabled(false);
		
		ctcCopy=new DoubleBox();
		ctcCopy.setEnabled(false);
		
		difference=new DoubleBox();
		difference.setEnabled(false);
		
		dbTotalDeductonAmount=new DoubleBox();
		dbTotalDeductonAmount.setEnabled(false);
		
		dbEmployeeTotalDeduction=new DoubleBox();
		dbEmployeeTotalDeduction.setEnabled(false);
		
		dbCompanyTotalDeduction=new DoubleBox();
		dbCompanyTotalDeduction.setEnabled(false);
		
//		earningtable=new SalaryHeadPresenterTableProxy();
		earningtable=new SalaryHeadPresenterTableProxy(true);
		
		deductionTable=new SalaryHeadPresenterTableProxy();
		companyContributionTable=new SalaryHeadPresenterTableProxy();
		
		state=new ObjectListBox<State>();
		State.makeOjbectListBoxLive(state);
		
		dbMonthlyCtc=new DoubleBox();
		dbMonthlyCtc.setEnabled(true);
		
		dbMonthlyCtcAmount=new DoubleBox();
		dbMonthlyCtcAmount.setValue(0d);
		
		
		oblEarningComp=new ObjectListBox<CtcComponent>();
		MyQuerry q1=new MyQuerry();
		
		Filter f1=new Filter();
		f1.setQuerryString("status");
		f1.setBooleanvalue(true);
		q1.getFilters().add(f1);
		
		Filter f2=new Filter();
		f2.setQuerryString("isDeduction");
		f2.setBooleanvalue(false);
		q1.getFilters().add(f2);
		
		q1.setQuerryObject(new CtcComponent());
		oblEarningComp.MakeLive(q1);
		
		
		oblEmpDeductionComp=new ObjectListBox<CtcComponent>();
		MyQuerry q2=new MyQuerry();
		
		Filter f11=new Filter();
		f11.setQuerryString("status");
		f11.setBooleanvalue(true);
		q2.getFilters().add(f11);
		
		Filter f21=new Filter();
		f21.setQuerryString("isDeduction");
		f21.setBooleanvalue(true);
		q2.getFilters().add(f21);
		
		Filter f22=new Filter();
		f22.setQuerryString("isRecord");
		f22.setBooleanvalue(false);
		q2.getFilters().add(f22);
		
		
		
		
		
		q2.setQuerryObject(new CtcComponent());
		oblEmpDeductionComp.MakeLive(q2);
		
		oblCompContributionComp=new ObjectListBox<CtcComponent>();
		MyQuerry q3=new MyQuerry();
		
		Filter f111=new Filter();
		f111.setQuerryString("status");
		f111.setBooleanvalue(true);
		q3.getFilters().add(f111);
		
		Filter f211=new Filter();
		f211.setQuerryString("isDeduction");
		f211.setBooleanvalue(true);
		q3.getFilters().add(f211);
		
		Filter f221=new Filter();
		f221.setQuerryString("isRecord");
		f221.setBooleanvalue(true);
		q3.getFilters().add(f221);
		
		q3.setQuerryObject(new CtcComponent());
		oblCompContributionComp.MakeLive(q3);
		 
		btnEarn=new Button("Add");
		btnEarn.addClickHandler(this);
		btnEarn.getElement().getStyle().setMarginRight(40, Unit.PX);
		
		btnEmpDeduction=new Button("Add");
		btnEmpDeduction.addClickHandler(this);
		btnCompContribution=new Button("Add");
		btnCompContribution.addClickHandler(this);
		
		btnRevise=new Button("Revise");
		btnRevise.addClickHandler(this);
		
		oblEarningComp.getElement().getStyle().setMarginRight(40, Unit.PX);
		oblEarningComp.getElement().getStyle().setWidth(200, Unit.PX);
		
		oblEmpDeductionComp.getElement().getStyle().setMarginRight(40, Unit.PX);
		oblEmpDeductionComp.getElement().getStyle().setWidth(200, Unit.PX);
		
		oblCompContributionComp.getElement().getStyle().setMarginRight(40, Unit.PX);
		oblCompContributionComp.getElement().getStyle().setWidth(200, Unit.PX);
		
		VerticalPanel vp1=new VerticalPanel();
		VerticalPanel vp2=new VerticalPanel();
		VerticalPanel vp3=new VerticalPanel();
		
		HorizontalPanel hp1=new HorizontalPanel();
		InlineLabel lblEarnAdd = new InlineLabel(".");
		lblEarnAdd.getElement().getStyle().setColor("White");
		InlineLabel lblAdd = new InlineLabel(".");
		lblAdd.getElement().getStyle().setColor("White");
		InlineLabel lblEarn = new InlineLabel("Ctc Component");
				
		vp1.add(lblEarn);
		vp1.add(oblEarningComp);
		hp1.add(vp1);
		
		vp2.add(lblEarnAdd);
		vp2.add(btnEarn);
		hp1.add(vp2);
		
		vp3.add(lblAdd);
		vp3.add(btnRevise);
		hp1.add(vp3);
		
		VerticalPanel vp11=new VerticalPanel();
		VerticalPanel vp21=new VerticalPanel();
		
		HorizontalPanel hp11=new HorizontalPanel();
		InlineLabel lblEmpDedAdd = new InlineLabel(".");
		InlineLabel lblEmpDed = new InlineLabel("Employee Deduction");
				
		vp11.add(lblEmpDed);
		vp11.add(oblEmpDeductionComp);
		hp11.add(vp11);
		
		vp21.add(lblEmpDedAdd);
		vp21.add(btnEmpDeduction);
		hp11.add(vp21);
		
		
		VerticalPanel vp111=new VerticalPanel();
		VerticalPanel vp211=new VerticalPanel();
		
		HorizontalPanel hp111=new HorizontalPanel();
		InlineLabel lblCompConAdd = new InlineLabel(".");
		InlineLabel lblCompCon = new InlineLabel("Company Contribution");
				
		vp111.add(lblCompCon);
		vp111.add(oblCompContributionComp);
		hp111.add(vp111);
		
		vp211.add(lblCompConAdd);
		vp211.add(btnCompContribution);
		hp111.add(vp211);
		
		
		
		
		
		FlowPanel flowPanel1=new FlowPanel();
		FlowPanel flowPanel2=new FlowPanel();
		FlowPanel flowPanel3=new FlowPanel();
		
		
		
		VerticalPanel verPanel1=new VerticalPanel();
		verPanel1.setWidth("100%");
		verPanel1.setHeight("350px");
		
		VerticalPanel verPanel2=new VerticalPanel();
		verPanel2.setWidth("100%");
		verPanel2.setHeight("350px");
		
		VerticalPanel verPanel3=new VerticalPanel();
		verPanel3.setWidth("100%");
		verPanel3.setHeight("350px");
		
		InlineLabel lbl = new InlineLabel("Total");
		lbl.getElement().setClassName("footer-grid");
		
		
		
		InlineLabel secgrosslabel = new InlineLabel("CTC Amount");
		secgrosslabel.getElement().setClassName("footer-grid");
		
		InlineLabel differencelabel =new InlineLabel("Difference");
		differencelabel.getElement().setClassName("footer-grid");
		
		InlineLabel differencelabel1 =new InlineLabel("Gross Earning Monthly");
		differencelabel1.getElement().setClassName("footer-grid");
		
		
		
		InlineLabel empDedLbl = new InlineLabel("Employee Total Deduction");
		empDedLbl.getElement().setClassName("footer-grid");

		InlineLabel compDedLbl = new InlineLabel("Company Total Deduction");
		compDedLbl.getElement().setClassName("footer-grid");
		
		verPanel1.add(hp1);
		verPanel1.add(earningtable.getTable());
		verPanel1.add(lbl);
		verPanel1.add(grossEarning);
		verPanel1.add(secgrosslabel);
		verPanel1.add(ctcCopy);
		verPanel1.add(differencelabel);
		verPanel1.add(difference);
		verPanel1.add(differencelabel1);
		verPanel1.add(dbMonthlyCtcAmount);
		
		
		verPanel2.add(hp11);
		verPanel2.add(deductionTable.getTable());
		verPanel2.add(empDedLbl);
		verPanel2.add(dbEmployeeTotalDeduction);
		
		verPanel3.add(hp111);
		verPanel3.add(companyContributionTable.getTable());
		verPanel3.add(compDedLbl);
		verPanel3.add(dbCompanyTotalDeduction);
		
		flowPanel1.add(verPanel1);
		flowPanel2.add(verPanel2);
		flowPanel3.add(verPanel3);
		
		tabPanel = new TabPanel();
		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				 int tabId = event.getSelectedItem();
				 System.out.println("TAB ID "+tabId);
				 if(tabId==0){
					 earningtable.getTable().redraw();
				 }else if(tabId==1){
					 deductionTable.getTable().redraw();
				 }else if(tabId==2){
					 companyContributionTable.getTable().redraw();
				 }					
			}
		});
		tabPanel.add(flowPanel1,"Earnings");
		tabPanel.add(flowPanel2,"Deductions");
		tabPanel.add(flowPanel3,"Company Contribution");
		tabPanel.setSize("100%", "350px");
		// select first tab
		tabPanel.selectTab(0);
		
		lbGender=new ListBox();
		lbGender.addItem("--SELECT--");
		lbGender.addItem("All");
		lbGender.addItem("Male");
		lbGender.addItem("Female");
		
		olbcNumberRange = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange, Screen.NUMBERRANGE);
		
		rbMonthly=new RadioButton("Monthly");
		rbMonthly.setValue(true);
		rbMonthly.addClickHandler(this);
		
		rbAnnually=new RadioButton("Annually");
		rbAnnually.setValue(false);
		rbAnnually.addClickHandler(this);
		
		cbPaidLeave = new CheckBox();
		cbPaidLeave.setValue(false);
		cbPaidLeave.addClickHandler(this);
		
		dbPaidLeaveAmt=new DoubleBox();
		dbPaidLeaveAmt.setEnabled(false);
		
		cbBonus=new CheckBox();
		cbBonus.setValue(false);
		cbBonus.addClickHandler(this);
		
		dbBonusAmt=new DoubleBox();
		dbBonusAmt.setEnabled(false);
		
		lbMonth=new ListBox();
		lbMonth.addItem("--SELECT--");
		lbMonth.addItem("JAN");
		lbMonth.addItem("FEB");
		lbMonth.addItem("MAR");
		lbMonth.addItem("APR");
		lbMonth.addItem("MAY");
		lbMonth.addItem("JUN");
		lbMonth.addItem("JUL");
		lbMonth.addItem("AUG");
		lbMonth.addItem("SEP");
		lbMonth.addItem("OCT");
		lbMonth.addItem("NOV");
		lbMonth.addItem("DEC");
		lbMonth.setEnabled(false);
		 /** date 31.8.2018 added by komal **/
		 olbCTCCategory = new ObjectListBox<ConfigCategory>();
		 AppUtility.MakeLiveCategoryConfig(olbCTCCategory, Screen.CTCCATEGORY);
		 
		 
		 btnRevisePaidLeaveAndBonus=new Button("Revise Paid Leave/Bonus");
		 btnRevisePaidLeaveAndBonus.addClickHandler(this);
		 btnRevisePaidLeaveAndBonus.setEnabled(false);
		 
		 dbPreviousPaidLeaveAmt=new DoubleBox();
		 dbPreviousPaidLeaveAmt.setEnabled(false);
		 dbPreviousBonusAmt=new DoubleBox();
		 dbPreviousBonusAmt.setEnabled(false);
		 
		 blankLable=new InlineLabel("  ");
		 blankLable.setVisible(false);
		 /***Date 20-5-2019 by Amol to refresh the earning table(CTC components)***/
		 earningtable.getTable().redraw();
		 
		 
		 cbIncludeBonusInEarning=new CheckBox();
		 cbIncludePlInEarning=new CheckBox();
		 tbPlCorrespondanceName=new TextBox();
		 tbBonusCorrespondanceName=new TextBox();
		 oblPaidLeave=new ObjectListBox<PaidLeave>();
		 Filter temp=new Filter();
		 temp.setQuerryString("status");
		 temp.setBooleanvalue(true);
		 MyQuerry querry=new MyQuerry();
		 querry.setQuerryObject(new PaidLeave());
		 querry.getFilters().add(temp);
		 oblPaidLeave.MakeLive(querry);
		 oblPaidLeave.addChangeHandler(this);
		 oblPaidLeave.setEnabled(false);
		 
		 oblBonus=new ObjectListBox<Bonus>();
		 Filter temp1=new Filter();
		 temp1.setQuerryString("status");
		 temp1.setBooleanvalue(true);
		 MyQuerry querry1=new MyQuerry();
		 querry1.setQuerryObject(new Bonus());
		 querry1.getFilters().add(temp1);
		 oblBonus.MakeLive(querry1);
		 oblBonus.addChangeHandler(this);
		 oblBonus.setEnabled(false);
	}


	@Override
	public void createScreen() {
		//Token to initialize the processlevel menus.
		/**Date 26-8-2019 by Amol added a create Overtime button***/
		processlevelBarNames=new String[]{AppConstants.NEW,"Allocate Template",AppConstants.CREATEOVERTIME,"Copy Template"};	
		initalizeWidget();

		// Form Field Initialization
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCtcTempGrouping=fbuilder.setlabel("CTC Template Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("CTC Template Id",tbCtcTempId);
		FormField fCTCId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/**Date 28-3-2019 by Amol added a CTC Template Name Mandatory **/
		fbuilder = new FormFieldBuilder("* CTC Template Name",tbCtcTempName);
		FormField tbCtcTempName= fbuilder.setMandatory(true).setMandatoryMsg("CTC Template Name is Mandatory !").setRowSpan(0).setColSpan(0).build();
		
	    fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField fdbTotalCtcAmount=null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CTCTemplate", "ChangeCtcLabelAsGrossEarning")){
			fbuilder = new FormFieldBuilder("* Gross Earning (Annual)",dbTotalCtcAmount);
			fdbTotalCtcAmount= fbuilder.setMandatory(true).setMandatoryMsg("Total CTC amount is mandatory !").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("* Total CTC Amount",dbTotalCtcAmount);
			fdbTotalCtcAmount= fbuilder.setMandatory(true).setMandatoryMsg("Total CTC amount is mandatory !").setRowSpan(0).setColSpan(0).build();	
		}
		
		
		fbuilder = new FormFieldBuilder();
		FormField fEarningInformation=fbuilder.setlabel("CTC Component").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder("",earningtable.getTable());
		FormField fearningtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder=new FormFieldBuilder("Total", grossEarning);
		fgrossEarning= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
        fbuilder=new FormFieldBuilder("CTC Amount",ctcCopy);
        fsecgrossearnings= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	    fbuilder=new FormFieldBuilder("Difference",difference);
	    fdifference= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	    
	    fbuilder = new FormFieldBuilder();
		FormField fdeductionComponent=fbuilder.setlabel("Deduction Component").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",deductionTable.getTable());
		FormField fdeductionTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder=new FormFieldBuilder("Total Deduction Amount",dbTotalDeductonAmount);
	    fTotDeduction= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	    
	    fbuilder = new FormFieldBuilder("",companyContributionTable.getTable());
		FormField fcompaniesContributionTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder=new FormFieldBuilder("Total Deduction Amount",dbTotalDeductonAmount);
		fcomTotDeduction= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		

	    fbuilder=new FormFieldBuilder(" ",tabPanel);
	    FormField ftabPanel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	    

		fbuilder = new FormFieldBuilder("* Gender",lbGender);
		FormField flbGender= fbuilder.setMandatory(true).setMandatoryMsg("Gender is mandetory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Number Range",olbcNumberRange);
		FormField folbcNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fgrpingPlandBonus=fbuilder.setlabel("Paid Leave and Bonus Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("Paid Leave", cbPaidLeave);
		FormField fpaidLeave = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Paid Leave Amount", dbPaidLeaveAmt);
		FormField fdbPaidLeaveAmt = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Bonus", cbBonus);
		FormField fcbBonus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Bonus Amount", dbBonusAmt);
		FormField fdbBonusAmt = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Monthly", rbMonthly);
		FormField frbMonthly = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Annually", rbAnnually);
		FormField frbAnnually = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Month", lbMonth);
		FormField flbMonth = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
//		fbuilder = new FormFieldBuilder("", btnRevise);
//		FormField fbtnRevise = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
		/** date 31.08.2018 added by komal**/
		fbuilder = new FormFieldBuilder("Category", olbCTCCategory);
		FormField folbCTCCategory = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		
		fbuilder = new FormFieldBuilder(" ", btnRevisePaidLeaveAndBonus);
		FormField fbtnRevisePaidLeaveAndBonus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder(" ", blankLable);
		FormField blankLable = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Previous Paid Leave", dbPreviousPaidLeaveAmt);
		FormField fdbPreviousPaidLeaveAmt = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Previous Bonus", dbPreviousBonusAmt);
		FormField fdbPreviousBonusAmt = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("State",state);
		FormField fstate =fbuilder.setMandatory(false).setMandatoryMsg("State is mandatory!").build();
		
		
		
		FormField ftbMonthlyCtc =null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CTCTemplate", "ChangeCtcLabelAsGrossEarning")){
			fbuilder=new FormFieldBuilder("Gross Earning (Monthly)",dbMonthlyCtc);
			ftbMonthlyCtc =fbuilder.setMandatory(false).build();
			
		}else{
			fbuilder=new FormFieldBuilder("Monthly CTC",dbMonthlyCtc);
			ftbMonthlyCtc =fbuilder.setMandatory(false).build();
			
		}
		
		
		
	    fbuilder=new FormFieldBuilder("Total Monthly CTC",dbMonthlyCtcAmount);
	    FormField fdbMonthlyCtcAmount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	    
		  fbuilder=new FormFieldBuilder("Include in earning",cbIncludePlInEarning);
		FormField fcbIncludePlInEarning =fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Include in earning",cbIncludeBonusInEarning);
		FormField fcbIncludeBonusInEarning =fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Correspondence Name",tbPlCorrespondanceName);
		FormField ftbPlCorrespondanceName =fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Correspondence Name",tbBonusCorrespondanceName);
		FormField ftbBonusCorrespondanceName =fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Paid Leave",oblPaidLeave);
		FormField foblPaidLeave =fbuilder.setMandatory(false).build();
		
		fbuilder=new FormFieldBuilder("Bonus",oblBonus);
		FormField foblBonus =fbuilder.setMandatory(false).build();
	    
		/**
		 * @author Anil , Date : 28-06-2019
		 * conflicts occurred due to working on simultaneously on same class and used process configuration arrange form component
		 */
	    if(isPlAndBonusAsPerTheFormula){

		    /**
		     * Updated By: Viraj
		     * Date: 03-06-2019
		     * Description: To remove state when DoNotLoadPtAndLwf process configuration is active
		     */
	    	if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CTCTemplate", "DoNotLoadPtAndLwf")){
	    		FormField[][] formfield = {   
						{fgroupingCtcTempGrouping},
						{fCTCId,tbCtcTempName,ftbMonthlyCtc,fdbTotalCtcAmount},
						{flbGender,folbCTCCategory,fstatus},
						
						{fEarningInformation},
						{ftabPanel},
						
						{fgrpingPlandBonus},
						
						{fbtnRevisePaidLeaveAndBonus},
						{fpaidLeave,fcbIncludePlInEarning,fcbBonus,fcbIncludeBonusInEarning,},
						{foblPaidLeave,fdbPaidLeaveAmt,foblBonus,fdbBonusAmt,},
						{ftbPlCorrespondanceName,fdbPreviousPaidLeaveAmt,ftbBonusCorrespondanceName,fdbPreviousBonusAmt},
				
		    	};
				this.fields=formfield;
	    	}else{
		    	FormField[][] formfield = {   
						{fgroupingCtcTempGrouping},
						{fCTCId,tbCtcTempName,ftbMonthlyCtc,fdbTotalCtcAmount},
						{flbGender,folbCTCCategory,fstate,fstatus},
						
						{fEarningInformation},
						{ftabPanel},
						
						{fgrpingPlandBonus},
						
						{fbtnRevisePaidLeaveAndBonus},
						{fpaidLeave,fcbIncludePlInEarning,fcbBonus,fcbIncludeBonusInEarning,},
						{foblPaidLeave,fdbPaidLeaveAmt,foblBonus,fdbBonusAmt,},
						{ftbPlCorrespondanceName,fdbPreviousPaidLeaveAmt,ftbBonusCorrespondanceName,fdbPreviousBonusAmt},
				
		    	};
				this.fields=formfield;
	    	}
	    }else{
	    	if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CTCTemplate", "DoNotLoadPtAndLwf")){
	    		FormField[][] formfield = {   
						{fgroupingCtcTempGrouping},
						{fCTCId,tbCtcTempName,ftbMonthlyCtc,fdbTotalCtcAmount},
						{flbGender,folbCTCCategory,fstatus},
						
						{fgrpingPlandBonus},
						{frbMonthly,frbAnnually,flbMonth},
						{fbtnRevisePaidLeaveAndBonus},
						{fpaidLeave,fdbPaidLeaveAmt,fcbBonus,fdbBonusAmt},
						{blankLable,fdbPreviousPaidLeaveAmt,blankLable,fdbPreviousBonusAmt},
						
						{fEarningInformation},
						{ftabPanel},
						
				};
				this.fields=formfield;
	    	}else{
				FormField[][] formfield = {   
						{fgroupingCtcTempGrouping},
						{fCTCId,tbCtcTempName,ftbMonthlyCtc,fdbTotalCtcAmount},
						{flbGender,folbCTCCategory,fstate,fstatus},
						
						{fgrpingPlandBonus},
						{frbMonthly,frbAnnually,flbMonth},
						{fbtnRevisePaidLeaveAndBonus},
						{fpaidLeave,fdbPaidLeaveAmt,fcbBonus,fdbBonusAmt},
						{blankLable,fdbPreviousPaidLeaveAmt,blankLable,fdbPreviousBonusAmt},
						
						{fEarningInformation},
						{ftabPanel},
						
				};
				this.fields=formfield;
	    	}
	    }

	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(CTCTemplate model){
		
		if(tbCtcTempName.getValue()!=null){
			model.setCtcTemplateName(tbCtcTempName.getValue());
		}
		model.setStatus(cbStatus.getValue());
		if(dbTotalCtcAmount.getValue()!=null){
			model.setTotalCtcAmount(dbTotalCtcAmount.getValue());
		}
		if(earningtable.getValue()!=null){
			model.setEarning(earningtable.getValue());
		}
		if(deductionTable.getValue()!=null){
			model.setDeduction(deductionTable.getValue());
		}
		if(grossEarning.getValue()!=null){
			model.setGrossEarning(grossEarning.getValue());
		}
		if(ctcCopy.getValue()!=null){
			model.setCtcCopy(ctcCopy.getValue());
		}
		if(difference.getValue()!=null){
			model.setDifference(difference.getValue());
		}
//		if(dbTotalDeductonAmount.getValue()!=null){
//			model.setTotalDeductionAmount(dbTotalDeductonAmount.getValue());
//		}
		
		
		if(dbEmployeeTotalDeduction.getValue()!=null){
			model.setTotalDeductionAmount(dbEmployeeTotalDeduction.getValue());
		}
		
		if(companyContributionTable.getValue()!=null){
			model.setCompanyContribution(companyContributionTable.getValue());
		}
		
		if(dbCompanyTotalDeduction.getValue()!=null){
			model.setCompanyTotalDeduction(dbCompanyTotalDeduction.getValue());
		}
		
		if(lbGender.getSelectedIndex()!=0){
			model.setGender(lbGender.getValue(lbGender.getSelectedIndex()));
		}else{
			model.setGender("");
		}
		
		if(olbcNumberRange.getValue()!=null){
			model.setNumberRange(olbcNumberRange.getValue());
		}else{
			model.setNumberRange("");
		}
		
		try {
			model.setPaidLeavesAvailable(cbPaidLeave.getValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		model.setMonthly(rbMonthly.getValue());
		model.setAnnually(rbAnnually.getValue());
		model.setBonus(cbBonus.getValue());
		
		if(dbPaidLeaveAmt.getValue()!=null){
			model.setPaidLeaveAmt(dbPaidLeaveAmt.getValue());
		}else{
			model.setPaidLeaveAmt(0);
		}
		if(dbBonusAmt.getValue()!=null){
			model.setBonusAmt(dbBonusAmt.getValue());
		}else{
			model.setBonusAmt(0);
		}
		
		if(lbMonth.getSelectedIndex()!=0){
			model.setMonth(lbMonth.getValue(lbMonth.getSelectedIndex()));
		}else{
			model.setMonth("");
		}
		/** date 31.8.2018 added by komal**/
		if(olbCTCCategory.getSelectedIndex()!=0){
			model.setCategory(olbCTCCategory.getValue(olbCTCCategory.getSelectedIndex()));
		}else{
			model.setCategory("");
		}	
		if(state.getSelectedIndex()!=0){
			model.setState(state.getValue(state.getSelectedIndex()));
			}else{
				model.setState("");
			}
		
		
		
		/**
		 * Date : 31-12-2018 By ANIL
		 */
		if(dbPreviousPaidLeaveAmt.getValue()!=null){
			model.setPreviousPaidLeaveAmt(dbPreviousPaidLeaveAmt.getValue());
			model.setPaidLeaveAndBonusRevised(true);
		}else{
			model.setPreviousPaidLeaveAmt(0);
			model.setPaidLeaveAndBonusRevised(false);
		}
		
		if(dbPreviousBonusAmt.getValue()!=null){
			model.setPreviousBonusAmt(dbPreviousBonusAmt.getValue());
			model.setPaidLeaveAndBonusRevised(true);
		}else{
			model.setPreviousBonusAmt(0);
		}

		if(dbMonthlyCtc.getValue()!=null){
			model.setMonthlyCtcAmount(dbMonthlyCtc.getValue());
		}
		
		if(oblPaidLeave.getSelectedIndex()!=0){
			model.setPlName(oblPaidLeave.getValue());
		}else{
			model.setPlName("");
		}
		
		if(oblBonus.getSelectedIndex()!=0){
			model.setBonusName(oblBonus.getValue());
		}else{
			model.setBonusName("");
		}
		
		if(tbPlCorrespondanceName.getValue()!=null){
			model.setPlCorresponadenceName(tbPlCorrespondanceName.getValue());
		}
		
		if(tbBonusCorrespondanceName.getValue()!=null){
			model.setBonusCorresponadenceName(tbBonusCorrespondanceName.getValue());
		}
		
		model.setPlIncludedInEarning(cbIncludePlInEarning.getValue());
		model.setBonusIncludedInEarning(cbIncludeBonusInEarning.getValue());
		
		ctcTempObj=model;
		presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(CTCTemplate view){
		ctcTempObj=view;
		/**Amol**/
		oblEarningComp.setSelectedIndex(0);
		tbCtcTempId.setValue(view.getCount()+"");
		if(view.getCtcTemplateName()!=null){
			tbCtcTempName.setValue(view.getCtcTemplateName());
		}
		cbStatus.setValue(view.isStatus());
		dbTotalCtcAmount.setValue(view.getTotalCtcAmount());
		SalaryHeadPresenterTableProxy.ctcAmount=view.getTotalCtcAmount();
		if(view.getEarning()!=null){
			earningtable.setValue(view.getEarning());
		}
		if(view.getDeduction()!=null){
			deductionTable.setValue(view.getDeduction());
		}
		
		if(view.getGrossEarning()!=0){
			grossEarning.setValue(view.getGrossEarning());
		}
		if(view.getCtcCopy()!=0){
			ctcCopy.setValue(view.getCtcCopy());
		}
//		if(view.getDifference()!=0){
			difference.setValue(view.getDifference());
//		}
//		if(view.getTotalDeductionAmount()!=0){
//			dbTotalDeductonAmount.setValue(view.getTotalDeductionAmount());
//		}
		if(view.getTotalDeductionAmount()!=0){
			dbEmployeeTotalDeduction.setValue(view.getTotalDeductionAmount());
		}
		if(view.getCompanyContribution()!=null){
			companyContributionTable.setValue(view.getCompanyContribution());
		}
		
		if(view.getState()!=null){
			state.setValue(view.getState());
		}
		
		
		dbCompanyTotalDeduction.setValue(view.getCompanyTotalDeduction());
		
		if(view.getGender()!=null){
			for(int i=0;i<lbGender.getItemCount();i++){
				if(lbGender.getItemText(i).trim().equals(view.getGender())){
					lbGender.setSelectedIndex(i);
					break;
				}
			}
		}
		
		if(view.getNumberRange()!=null){
			olbcNumberRange.setValue(view.getNumberRange());
		}
	
		
		/**
		 * Used try catch beacuse variable is created later. So code should not
		 * freeze
		 */
		try {
			cbPaidLeave.setValue(view.isPaidLeavesAvailable());
		} catch (Exception e) {
			e.printStackTrace();
		}
		rbMonthly.setValue(view.isMonthly());
		rbAnnually.setValue(view.isAnnually());
		cbBonus.setValue(view.isBonus());
		
		if(view.getPaidLeaveAmt()!=0){
			dbPaidLeaveAmt.setValue(view.getPaidLeaveAmt());
		}
		if(view.getBonusAmt()!=0){
			dbBonusAmt.setValue(view.getBonusAmt());
		}
		
		if(view.getMonth()!=null){
			for(int i=0;i<lbMonth.getItemCount();i++){
				if(lbMonth.getItemText(i).equals(view.getMonth())){
					lbMonth.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getCategory()!=null){
			olbCTCCategory.setValue(view.getCategory());
		}
		
		if(view.getPreviousPaidLeaveAmt()!=0){
			dbPreviousPaidLeaveAmt.setValue(view.getPreviousPaidLeaveAmt());
		}
		if(view.getPreviousBonusAmt()!=0){
			dbPreviousBonusAmt.setValue(view.getPreviousBonusAmt());
		}
		
		if(view.getMonthlyCtcAmount()!=0){
			dbMonthlyCtc.setValue(view.getMonthlyCtcAmount());
		}
		
			dbMonthlyCtcAmount.setValue(view.getTotalCtcAmount()/12);
			dbMonthlyCtcAmount.setValue(view.getMonthlyCtcAmount());
		
		if(view.getPlName()!=null){
			oblPaidLeave.setValue(view.getPlName());
		}
		if(view.getBonusName()!=null){
			oblBonus.setValue(view.getBonusName());
		}
		if(view.getPlCorresponadenceName()!=null){
			tbPlCorrespondanceName.setValue(view.getPlCorresponadenceName());
		}
		if(view.getBonusCorresponadenceName()!=null){
			tbBonusCorrespondanceName.setValue(view.getBonusCorresponadenceName());
		}
		cbIncludePlInEarning.setValue(view.isPlIncludedInEarning());
		cbIncludeBonusInEarning.setValue(view.isBonusIncludedInEarning());
		
		
		presenter.setModel(view);



	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION)||text.contains("Search"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CTCTEMPLATE,LoginPresenter.currentModule.trim());
	}

	

	
	
	@Override
	public void setToViewState() {
		super.setToViewState();
		SuperModel model=new CTCTemplate();
		model=ctcTempObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.HRCONFIG,AppConstants.CTCTEMPLATE, ctcTempObj.getCount(), null,null,null, false, model, null);
	}
	
	@Override
	public void setToNewState() {
		super.setToNewState();
		System.out.println("Inside NEW method");
		if(ctcTempObj!=null){
			SuperModel model=new CTCTemplate();
			model=ctcTempObj;
			AppUtility.addDocumentToHistoryTable(AppConstants.HRCONFIG,AppConstants.CTCTEMPLATE, ctcTempObj.getCount(), null,null,null, false, model, null);
		}
		ctcCopy.setValue(null);
		grossEarning.setValue(null);
		difference.setValue(null);
		dbEmployeeTotalDeduction.setValue(null);
		dbCompanyTotalDeduction.setValue(null);
	}
	
	
	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		earningtable.setEnable(state);
		deductionTable.setEnable(state);
		companyContributionTable.setEnable(state);
//		if(cbPaidLeave.getValue()==true){
//			dbPaidLeaveAmt.setEnabled(true);
//		}else{
//			dbPaidLeaveAmt.setEnabled(false);
//		}
//		if(cbBonus.getValue()==true){
//			dbBonusAmt.setEnabled(true);
//		}else{
//			dbBonusAmt.setEnabled(false);
//		}
//		
//		if(rbAnnually.getValue()==true){
//			lbMonth.setSelectedIndex(0);
//			lbMonth.setEnabled(true);
//		}else{
//			lbMonth.setSelectedIndex(0);
//			lbMonth.setEnabled(false);
//		}
		btnRevisePaidLeaveAndBonus.setEnabled(false);
		dbPreviousPaidLeaveAmt.setEnabled(false);
		dbPreviousBonusAmt.setEnabled(false);
		blankLable.setVisible(false);
		
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		/**
		 * @author Anil,Date : 10-01-2019 
		 * for hiding process level button on edit state of form
		 */
		if(processLevelBar!=null)
		    processLevelBar.setVisibleFalse(false);
		
		if(cbPaidLeave.getValue()==true){
			dbPaidLeaveAmt.setEnabled(true);
			oblPaidLeave.setEnabled(true);
		}else{
			dbPaidLeaveAmt.setEnabled(false);
			oblPaidLeave.setEnabled(false);
		}
		if(cbBonus.getValue()==true){
			dbBonusAmt.setEnabled(true);
			oblBonus.setEnabled(true);
		}else{
			dbBonusAmt.setEnabled(false);
			oblBonus.setEnabled(false);
		}
		
		if(rbAnnually.getValue()==true){
			lbMonth.setSelectedIndex(0);
			lbMonth.setEnabled(true);
		}else{
			lbMonth.setSelectedIndex(0);
			lbMonth.setEnabled(false);
		}
		
		if(dbPaidLeaveAmt.getValue()!=null||dbBonusAmt.getValue()!=null){
			btnRevisePaidLeaveAndBonus.setEnabled(true);
		}
	}

	@Override
	public void clear() {
		super.clear();
		cbStatus.setValue(true);
		tabPanel.selectTab(0);
//		CTCTemplatePresenter presenter=(CTCTemplatePresenter) this.presenter;
//		presenter.retrieveEarningTable();
		rbMonthly.setValue(true);
		rbAnnually.setValue(false);
		cbPaidLeave.setValue(false);
		dbPaidLeaveAmt.setEnabled(false);
		cbBonus.setValue(false);
		dbBonusAmt.setEnabled(false);
		lbMonth.setEnabled(false);
		tbCtcTempId.setEnabled(false);
		
		btnRevisePaidLeaveAndBonus.setEnabled(false);
		dbPreviousPaidLeaveAmt.setEnabled(false);
		dbPreviousBonusAmt.setEnabled(false);
		blankLable.setVisible(false);
		
		oblPaidLeave.setEnabled(false);
		oblBonus.setEnabled(false);
		cbIncludeBonusInEarning.setValue(false);
		cbIncludePlInEarning.setValue(false);
		
		/**
		 * @author Anil @since 26-02-2021
		 * for clearing the earning, deduction and company contribution table
		 */
		earningtable.connectToLocal();
		deductionTable.connectToLocal();
		companyContributionTable.connectToLocal();
	}
	
	

	@Override
	public boolean validate() {
		boolean superval=super.validate();
		
		if(cbPaidLeave.getValue()==true&&dbPaidLeaveAmt.getValue()==null){
			showDialogMessage("Please add paid leave amount.");
			return false;
		}
		if(cbBonus.getValue()==true&&dbBonusAmt.getValue()==null){
			showDialogMessage("Please slect bonus amount.");
			return false;
		}
		if((cbPaidLeave.getValue()==true ||cbBonus.getValue()==true)
				&&(rbMonthly.getValue()==false&&rbAnnually.getValue()==false)){
			showDialogMessage("Please select monthly/annually.");
			return false;
		}
		
		if(rbAnnually.getValue()==true&&lbMonth.getSelectedIndex()==0){
			showDialogMessage("Please select month.");
			return false;
		}
		if(difference.getValue()!=null&&difference.getValue()!=0){
			showDialogMessage("Difference should be zero.");
			return false;
		}
		
		return superval;
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
		if(event.getSource()==cbPaidLeave){
			if(cbPaidLeave.getValue()==true){
				dbPaidLeaveAmt.setEnabled(true);
				/**
				 * @author Anil Date : 18-06-2019
				 * 
				 */
				oblPaidLeave.setEnabled(true);
			}else{
				dbPaidLeaveAmt.setValue(null);
				dbPaidLeaveAmt.setEnabled(false);
				
				oblPaidLeave.setSelectedIndex(0);
				oblPaidLeave.setEnabled(false);
			}
		}
		
		if(event.getSource()==cbBonus){
			if(cbBonus.getValue()==true){
				dbBonusAmt.setEnabled(true);
				oblBonus.setEnabled(true);
			}else{
				dbBonusAmt.setValue(null);
				dbBonusAmt.setEnabled(false);
				
				oblBonus.setSelectedIndex(0);
				oblBonus.setEnabled(false);
				
			}
		}
		if(event.getSource()==rbMonthly){
			if(rbMonthly.getValue()==true){
				rbAnnually.setValue(false);
				lbMonth.setSelectedIndex(0);
				lbMonth.setEnabled(false);
			}
		}
		if(event.getSource()==rbAnnually){
			if(rbAnnually.getValue()==true){
				rbMonthly.setValue(false);
				lbMonth.setEnabled(true);
			}
		}
		CTCTemplatePresenter p=(CTCTemplatePresenter) presenter;
		if(event.getSource()==btnRevise){
			if(oblEarningComp.getSelectedIndex()!=0){
				boolean flag=false;
				double oldAmountMonthly=0;
				double oldAmountAnnully=0;
				for(CtcComponent ctc:earningtable.getValue()){
					/**Date 18-5-2019 added by Amol **/
					if(ctc.getName().equals(oblEarningComp.getValue())){
						flag=true;
						
						String amol=oblEarningComp.getValue();
						
						oldAmountAnnully=ctc.getAmount();
						System.out.println("AMOL"+oldAmountAnnully);
						oldAmountMonthly=ctc.getAmount()/12;
						earningtable.getValue().remove(ctc);
						break;
					}
				}
				if(flag){
					CtcComponent ctc=oblEarningComp.getSelectedItem();
					ctc.setOldAmountMonthly(oldAmountMonthly);
					ctc.setOldAmountAnnually(oldAmountAnnully);
					ctc.setRevised(true);
					earningtable.getDataprovider().getList().add(oblEarningComp.getSelectedItem());
				}else{
					showDialogMessage("No component found for revision.");
				}
			}else{
				showDialogMessage("Please select earning component.");
			}
		}
		if(event.getSource()==btnEarn){
			if(oblEarningComp.getSelectedIndex()!=0){
				boolean flag=false;
				for(CtcComponent ctc:earningtable.getValue()){
					if(ctc.getName().equals(oblEarningComp.getValue())){
						flag=true;
						showDialogMessage("Component already added.");
						break;
					}
				}
				if(!flag){
					earningtable.getDataprovider().getList().add(oblEarningComp.getSelectedItem());
				}
			}else{
				showDialogMessage("Please select earning component.");
			}
		}
		
		if(event.getSource()==btnEmpDeduction){
			if(oblEmpDeductionComp.getSelectedIndex()!=0){
				boolean flag=false;
				for(CtcComponent ctc:deductionTable.getValue()){
					if(ctc.getName().equals(oblEmpDeductionComp.getValue())){
						flag=true;
						showDialogMessage("Component already added.");
						break;
					}
				}
				if(!flag){
					deductionTable.getDataprovider().getList().add(oblEmpDeductionComp.getSelectedItem());
					p.updateDeductionTbl(deductionTable.getValue());
					deductionTable.getTable().redraw();
				}
			}else{
				showDialogMessage("Please select deduction component.");
			}
		}
		
		if(event.getSource()==btnCompContribution){
			if(oblCompContributionComp.getSelectedIndex()!=0){
				boolean flag=false;
				for(CtcComponent ctc:companyContributionTable.getValue()){
					if(ctc.getName().equals(oblCompContributionComp.getValue())){
						flag=true;
						showDialogMessage("Component already added.");
						break;
					}
				}
				if(!flag){
					companyContributionTable.getDataprovider().getList().add(oblCompContributionComp.getSelectedItem());
					p.updateDeductionTbl(companyContributionTable.getValue());
					companyContributionTable.getTable().redraw();
				}
			}else{
				showDialogMessage("Please select company contribution component.");
			}
		}
		
		
		if(event.getSource()==btnRevisePaidLeaveAndBonus){
			
			dbPreviousPaidLeaveAmt.setEnabled(true);
			dbPreviousBonusAmt.setEnabled(true);
			if(dbPaidLeaveAmt.getValue()!=null){
				dbPreviousPaidLeaveAmt.setValue(dbPaidLeaveAmt.getValue());
				dbPaidLeaveAmt.setValue(null);
				/**
				 * @author Anil , Date : 19-06-2019
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CTCTemplate", "PlAndBonusAsPerTheFormula")){
					updatePaidLeave();
				}
			}
			if(dbBonusAmt.getValue()!=null){
				dbPreviousBonusAmt.setValue(dbBonusAmt.getValue());
				dbBonusAmt.setValue(null);
				/**
				 * @author Anil , Date : 19-06-2019
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CTCTemplate", "PlAndBonusAsPerTheFormula")){
					updateBonus();
				}
			}
			
		}
		ScreeenState scr=AppMemory.getAppMemory().currentState;
		if(scr.equals(ScreeenState.NEW))
		{
			if(tbCtcTempName.getValue()!=null && !tbCtcTempName.getValue().equals(""))
				validateTemplateName();
		}
		if(scr.equals(ScreeenState.EDIT))
		{
			if(tbCtcTempName.getValue()!=null && !tbCtcTempName.getValue().equals(""))
				validateTemplateName();
		}
	}

	public DoubleBox getDbMonthlyCtc() {
		return dbMonthlyCtc;
	}

	public void setDbMonthlyCtc(DoubleBox dbMonthlyCtc) {
		this.dbMonthlyCtc = dbMonthlyCtc;
	}

	public TextBox getTbCtcTempName() {
		return tbCtcTempName;
	}

	public void setTbCtcTempName(TextBox tbCtcTempName) {
		this.tbCtcTempName = tbCtcTempName;
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		System.out.println("Inside On Value Change");
		ScreeenState scr=AppMemory.getAppMemory().currentState;
		if(scr.equals(ScreeenState.NEW))
		{
		if(event.getSource().equals(getTbCtcTempName())){
			System.out.println("Amol");
			validateTemplateName();
		}	
		}
		if(scr.equals(ScreeenState.EDIT))
		{
		if(event.getSource().equals(getTbCtcTempName())){
			validateTemplateName();
		}	
		}
		if(scr.equals(ScreeenState.VIEW))
		{
		if(event.getSource().equals(getTbCtcTempName())){
			validateTemplateName();
		}	
		}
		
	}
	
	/**Date 7-3-2019 added by Amol for avoid the duplicate CTC template name**/
	private void validateTemplateName(){
			if(!getTbCtcTempName().getValue().equals("")){
			String templatenameval=getTbCtcTempName().getValue();
			
			if(templatenameval!=null){

					final MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					
					Filter temp=null;
					temp=new Filter();
					temp.setQuerryString("ctcTemplateName");
					temp.setStringValue(templatenameval);
					filtervec.add(temp);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new CTCTemplate());
					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								showDialogMessage("An Unexpected Error occured !");
								
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								System.out.println("Success"+result.size());
								if(result.size()!=0){
									/**
									 * @author Anil ,Date : 27-03-2019
									 * after updating existing template it gives error that ctc template exist
									 * for Sasha ,raise by baljinder
									 */
//									for(SuperModel model:result){
										CTCTemplate entity= (CTCTemplate) result.get(0);
										if(tbCtcTempId.getValue()!=null&&!tbCtcTempId.getValue().equals("")){
											if(Integer.parseInt(tbCtcTempId.getValue())!=entity.getCount()){
												showDialogMessage("CTC Template Already Exists");
												getTbCtcTempName().setValue("");
											}
										}else{
											showDialogMessage("CTC Template Already Exists");
											getTbCtcTempName().setValue("");
										}
//									}
									
								}
								if(result.size()==0){
								}
							}
						});
			}
		}
	
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==oblPaidLeave){
			updatePaidLeave();
			
//			if(oblPaidLeave.getSelectedIndex()!=0){
//				PaidLeave paidLeave=oblPaidLeave.getSelectedItem();
//				double plAmt=0;
//				if(paidLeave!=null){
//					for(OtEarningComponent plComp:paidLeave.getOtEarningCompList()){
//						System.out.println("NAME : "+plComp.getComponentName());
//						for(CtcComponent earningComp:earningtable.getValue()){
//							System.out.println("EARN NAME : "+earningComp.getName());
//
//							if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
//								plAmt=plAmt+earningComp.getAmount()/12;
//							}else if(plComp.getComponentName().equals(earningComp.getName())){
//								System.out.println("AMT : "+earningComp.getAmount()/12);
//								plAmt=plAmt+earningComp.getAmount()/12;
//							}
//						}
//					}
//				}
//				System.out.println("BASE AMT : "+plAmt);
//				plAmt=plAmt*paidLeave.getRate()/100;
//				System.out.println("PL AMT : "+plAmt);
//				dbPaidLeaveAmt.setValue(plAmt);
//				
//			}else{
//				dbPaidLeaveAmt.setValue(null);	
//			}
		}
		
		if(event.getSource()==oblBonus){
			updateBonus();
		}
	}
	
	public void updatePaidLeave(){
		if(oblPaidLeave.getSelectedIndex()!=0){
			PaidLeave paidLeave=oblPaidLeave.getSelectedItem();
			double plAmt=0;
			if(paidLeave!=null){
				for(OtEarningComponent plComp:paidLeave.getOtEarningCompList()){
					System.out.println("NAME : "+plComp.getComponentName());
					for(CtcComponent earningComp:earningtable.getValue()){
						System.out.println("EARN NAME : "+earningComp.getName());

						if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
							plAmt=plAmt+earningComp.getAmount()/12;
						}else if(plComp.getComponentName().equals(earningComp.getName())){
							System.out.println("AMT : "+earningComp.getAmount()/12);
							plAmt=plAmt+earningComp.getAmount()/12;
						}
						
					}
				}
			}
			System.out.println("BASE AMT : "+plAmt);
			plAmt=plAmt*paidLeave.getRate()/100;
			System.out.println("PL AMT : "+plAmt);
			dbPaidLeaveAmt.setValue(plAmt);
			
		}else{
			dbPaidLeaveAmt.setValue(null);	
		}
	}
	
	public void updateBonus(){
		if(oblBonus.getSelectedIndex()!=0){
			Bonus bonus=oblBonus.getSelectedItem();
			double plAmt=0;
			if(bonus!=null){
				for(OtEarningComponent plComp:bonus.getOtEarningCompList()){
					System.out.println("NAME : "+plComp.getComponentName());
					for(CtcComponent earningComp:earningtable.getValue()){
						System.out.println("EARN NAME : "+earningComp.getName());

						if(plComp.getComponentName().equalsIgnoreCase("Gross Earning")){
							plAmt=plAmt+earningComp.getAmount()/12;
						}else if(plComp.getComponentName().equals(earningComp.getName())){
							System.out.println("AMT : "+earningComp.getAmount()/12);
							plAmt=plAmt+earningComp.getAmount()/12;
						}
					}
				}
			}
			System.out.println("BASE AMT : "+plAmt);
			plAmt=plAmt*bonus.getRate()/100;
			System.out.println("BONUS AMT : "+plAmt);
			dbBonusAmt.setValue(plAmt);
			
		}else{
			dbBonusAmt.setValue(null);	
		}
	}
	
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		earningtable.getTable().redraw();
		deductionTable.getTable().redraw();
		companyContributionTable.getTable().redraw();
	}
		/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		tbCtcTempId.setValue(count+"");
	}
}
