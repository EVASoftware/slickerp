package com.slicktechnologies.client.views.humanresource.employeeovertimehistory;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.EmployeeOvertime;

public class EmployeeOvertimeHistoryListTableProxy extends SuperTable<EmployeeOvertime>{

	TextColumn<EmployeeOvertime> getColEmpId;
	TextColumn<EmployeeOvertime> getColEmpName;
	TextColumn<EmployeeOvertime> getColOtDate;
	TextColumn<EmployeeOvertime> getColOtHours;
	TextColumn<EmployeeOvertime> getColOtInDays;
	TextColumn<EmployeeOvertime> getColOtName;
	
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		getColEmpId();
		getColEmpName();
		getColOtDate();
		getColOtHours();
//		getColOtInDays();
		getColOtName();
	}

	private void getColEmpId() {
		// TODO Auto-generated method stub
		getColEmpId=new TextColumn<EmployeeOvertime>() {
			
			@Override
			public String getValue(EmployeeOvertime object) {
				// TODO Auto-generated method stub
				
				if(object.getEmplId()!=0){
				return object.getEmplId()+"";
				}else{
					return "";
				}
			}
		};
		table.addColumn(getColEmpId, "Employee Id");
		getColEmpId.setSortable(true);
	}

	private void getColEmpName() {
		// TODO Auto-generated method stub
		getColEmpName=new TextColumn<EmployeeOvertime>() {
			
			@Override
			public String getValue(EmployeeOvertime object) {
				// TODO Auto-generated method stub
				if(object.getEmployee()!=null){
					return object.getEmployee().getFullName();
				}
				return "";
			}
		};
		table.addColumn(getColEmpName, "Employee Name");
		getColEmpName.setSortable(true);
		
	}

	private void getColOtDate() {
		// TODO Auto-generated method stub
		getColOtDate=new TextColumn<EmployeeOvertime>() {
			
			@Override
			public String getValue(EmployeeOvertime object) {
				// TODO Auto-generated method stub
				
				if(object.getOtDate()!=null){
				return AppUtility.parseDate(object.getOtDate())+"";
				}else{
					return "";
				}
			}
		};
		table.addColumn(getColOtDate, "OT Date");
		getColOtDate.setSortable(true);
	}

	private void getColOtHours() {
		// TODO Auto-generated method stub
		getColOtHours=new TextColumn<EmployeeOvertime>() {
			
			@Override
			public String getValue(EmployeeOvertime object) {
				// TODO Auto-generated method stub
				
				
				if(object.getOtHrs()!=0){
				return object.getOtHrs()+"";
				}else{
					return "";
				}
			}
		};
		table.addColumn(getColOtHours, "OT Hours");
		getColOtHours.setSortable(true);
	}

	private void getColOtInDays() {
		// TODO Auto-generated method stub
		getColOtInDays=new TextColumn<EmployeeOvertime>() {
			
			@Override
			public String getValue(EmployeeOvertime object) {
				// TODO Auto-generated method stub
				if(object.getEmployee()!=null&&object.getOtHrs()!=0){
					return object.getOtHrs()/object.getEmployee().getLeaveCalendar().getWorkingHours()+"";
				}
				return "";
			}
		};
		table.addColumn(getColOtInDays, "OT(In Days)");
		getColOtInDays.setSortable(true);
	}

	private void getColOtName() {
		// TODO Auto-generated method stub
		getColOtName=new TextColumn<EmployeeOvertime>() {
			
			@Override
			public String getValue(EmployeeOvertime object) {
				// TODO Auto-generated method stub
				
				if( object.getOvertime()!=null){
				return object.getOvertime()+"";
				}else{
					return "";
				}
			}
		};
		table.addColumn(getColOtName, "OT Name");
		getColOtName.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addColumnSorting() {
		// TODO Auto-generated method stub
		super.addColumnSorting();
		addSortingOnEmpIdCol();
		addSortingOnEmpNameCol();
		addSortingOnOtNameCol();
		addSortingOnOtDateCol();
		addSortingOnOtHourCol();
//		addSortingOnOtInDayCol();
	}

	private void addSortingOnEmpIdCol() {
		// TODO Auto-generated method stub
		List<EmployeeOvertime> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeOvertime>(list);
		columnSort.setComparator(getColEmpId, new Comparator<EmployeeOvertime>()
		{
			@Override
			public int compare(EmployeeOvertime e1,EmployeeOvertime e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getEmplId()== e2.getEmplId()){
						return 0;}
					if(e1.getEmplId()> e2.getEmplId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnEmpNameCol() {
		// TODO Auto-generated method stub
		List<EmployeeOvertime> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeOvertime>(list);
		columnSort.setComparator(getColEmpName, new Comparator<EmployeeOvertime>()
		{
			@Override
			public int compare(EmployeeOvertime e1,EmployeeOvertime e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().getFullName().compareTo(e2.getEmployee().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnOtNameCol() {
		// TODO Auto-generated method stub
		List<EmployeeOvertime> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeOvertime>(list);
		columnSort.setComparator(getColOtName, new Comparator<EmployeeOvertime>()
		{
			@Override
			public int compare(EmployeeOvertime e1,EmployeeOvertime e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getOvertime()!=null && e2.getOvertime()!=null){
						return e1.getOvertime().compareTo(e2.getOvertime());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnOtDateCol() {
		// TODO Auto-generated method stub
		List<EmployeeOvertime> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeOvertime>(list);
		columnSort.setComparator(getColOtDate, new Comparator<EmployeeOvertime>()
		{
			@Override
			public int compare(EmployeeOvertime e1,EmployeeOvertime e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getOtDate()!=null && e2.getOtDate()!=null){
						return e1.getOtDate().compareTo(e2.getOtDate());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnOtHourCol() {
		// TODO Auto-generated method stub
		List<EmployeeOvertime> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeOvertime>(list);
		columnSort.setComparator(getColOtHours, new Comparator<EmployeeOvertime>()
		{	
			@Override
			public int compare(EmployeeOvertime e1,EmployeeOvertime e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getOtHrs()== e2.getOtHrs()){
						return 0;}
					if(e1.getOtHrs()> e2.getOtHrs()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
		}});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnOtInDayCol() {
		// TODO Auto-generated method stub
		List<EmployeeOvertime> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeOvertime>(list);
		columnSort.setComparator(getColOtInDays, new Comparator<EmployeeOvertime>()
		{	
			@Override
			public int compare(EmployeeOvertime e1,EmployeeOvertime e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getOtHrs()== e2.getOtHrs()){
						return 0;}
					if(e1.getOtHrs()> e2.getOtHrs()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
		}});
		table.addColumnSortHandler(columnSort);
	}
	
	

}
