package com.slicktechnologies.client.views.humanresource.employeeovertimehistory;

import java.util.Vector;

import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.EmployeeOvertime;

public class EmployeeOvertimeHistoryListSearchProxy extends SearchPopUpScreen<EmployeeOvertime>{
	EmployeeInfoComposite personInfo;
	DateComparator dateComparator;
	ObjectListBox<HrProject> project;
	
	public EmployeeOvertimeHistoryListSearchProxy(boolean b) {
		// TODO Auto-generated constructor stub
		super(b);
		getPopup().setHeight("300px");
		createGui();
	}
	
	public void initWidget(){
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee Id");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
		
		dateComparator= new DateComparator("otDate",new EmployeeOvertime());
		
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
		
	}
	
	public void createScreen(){
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(2).setColSpan(4).build();
	
		builder = new FormFieldBuilder("From Date",dateComparator.getFromDate());
		FormField fdbFromDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date",dateComparator.getToDate());
		FormField fdbToDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Project Name",project);
		FormField fprojectName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		
		this.fields=new FormField[][]{
			{fpersonInfo},
			{fdbFromDate,fdbToDate,fprojectName}
			
		};
	}

	@Override
	public MyQuerry getQuerry() {
		// TODO Auto-generated method stub
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(!personInfo.getId().getValue().equals("")){  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
			temp.setQuerryString("emplId");
			filtervec.add(temp);
		}
		
		if(dateComparator.getValue()!=null){
			filtervec.addAll(dateComparator.getValue());
		}
		
		 if(project.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(project.getValue());
				temp.setQuerryString("projectName");
				filtervec.add(temp);
		 }
		
		
		
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new EmployeeOvertime());
		return querry;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

}
