package com.slicktechnologies.client.views.humanresource.employeeovertimehistory;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.employeeleavehistory.EmployeeLeaveHistoryListForm;
import com.slicktechnologies.client.views.humanresource.employeeleavehistory.EmployeeLeaveHistoryListPresenter;
import com.slicktechnologies.client.views.humanresource.employeeleavehistory.EmployeeLeaveHistorySearchProxy;
import com.slicktechnologies.client.views.humanresource.employeeleavehistory.EmployeeLeaveHistoryTableProxy;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.EmployeeOvertime;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class EmployeeOvertimeHistoryListPresenter extends TableScreenPresenter<EmployeeOvertime> {

	TableScreen<EmployeeOvertime>form;
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	GeneralServiceAsync genservice = GWT.create(GeneralService.class);
	
	public EmployeeOvertimeHistoryListPresenter(TableScreen<EmployeeOvertime> view,EmployeeOvertime model) {
		super(view, model);
		form=view;
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.EMPLOYEEOVERTIMEHISTORY,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	public static void initialize(){
		EmployeeOvertimeHistoryListTableProxy table=new EmployeeOvertimeHistoryListTableProxy();
		EmployeeOvertimeHistoryListForm form=new EmployeeOvertimeHistoryListForm(table);
		EmployeeOvertimeHistoryListSearchProxy.staticSuperTable=table;
		EmployeeOvertimeHistoryListSearchProxy searchPopUp=new EmployeeOvertimeHistoryListSearchProxy(false);
		form.setSearchpopupscreen(searchPopUp);
		EmployeeOvertimeHistoryListPresenter presenter=new EmployeeOvertimeHistoryListPresenter(form, new EmployeeOvertime());
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}
	
	public void reactOnDownload() {
		ArrayList<EmployeeOvertime> empOvertimeArray = new ArrayList<EmployeeOvertime>();
		List<EmployeeOvertime> list = (List<EmployeeOvertime>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		empOvertimeArray.addAll(list);
		
		Console.log("Employeeot Size"+empOvertimeArray.size());
		csvservice.setEmployeeOvertimeList(empOvertimeArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 176;
				Window.open(url, "test", "enabled");
			}
		});
	}

	@Override
	protected void reactOnGo() {
		// TODO Auto-generated method stub
		super.reactOnGo();
		
//		int empId=0;
//		Date fromDate=null;
//		Date toDate=null;
//		
//		EmployeeLeaveHistorySearchProxy searchForm =(EmployeeLeaveHistorySearchProxy) form.getSearchpopupscreen();
//		
//		if(!searchForm.personInfo.getId().getValue().equals("")){
//			empId=Integer.parseInt(searchForm.personInfo.getId().getValue());
//		}
//		if(searchForm.dateComparator.getFromDate().getValue()!=null){
//			fromDate=searchForm.dateComparator.getFromDate().getValue();
//		}
//		if(searchForm.dateComparator.getToDate().getValue()!=null){
//			toDate=searchForm.dateComparator.getToDate().getValue();
//		}
//		genservice.getEmployeeLeave(UserConfiguration.getCompanyId(), empId, fromDate, toDate, new AsyncCallback<ArrayList<EmployeeLeave>>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				
//			}
//
//			@Override
//			public void onSuccess(ArrayList<EmployeeLeave> result) {
//				// TODO Auto-generated method stub
//				
//				form.getSuperTable().connectToLocal();
//				form.getSuperTable().getListDataProvider().getList().addAll(result);
//			}
//		});
		
	}
	
	

}
