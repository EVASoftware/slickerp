package com.slicktechnologies.client.views.humanresource.compliance;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.itextpdf.text.pdf.qrcode.GF256;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;

public class ComplianceForm extends FormScreen<DummyEntityOnlyForScreen> {

	DateBox dbFormDate;
	DateBox dbToDate;
	
	ObjectListBox<Branch> olbBranch;
	ObjectListBox<HrProject> project;
	
	EmployeeInfoComposite personInfo;
	
	Button btnForm3A;
	Button btnFormC;
	Button btnFormD;
	Button btnForm9;
    Button btnForm6A;
	/**
	 * @author Amol ,Date 14-2-2019
	 * Adding FormF Gratuity for,
	 */
	Button btnFormF;
	
	
	/**
	 * @author Anil , Date:04-01-2019
	 * Adding Form 11,PF Declaration Form
	 */
	Button btnForm11;
	
	/**
	 * Updated By: Viraj
	 * Date: 02-02-2019
	 * Description: adding Form 2a revised
	 */
	Button btnForm2ARevised;
	/**
	 * Updated By: Amol
	 * Date: 25-03-2019
	 * Description: adding Form A Employee register
	 */
	Button btnFormA;
	
	
	boolean hideForm9=false;
	boolean hideForm11=false;
	boolean hideFormA=false;
	boolean hideBonusForms=false;
	boolean hideForm3A=false;
	boolean hideForm6A=false;
	boolean hideFormF=false;
	boolean hideFormC=false;
	boolean hideFormD=false;
	boolean hideForm2aReviced=false;
	
	
	
	public  ComplianceForm() {
//		super();
		super(FormStyle.DEFAULT);
		createGui();
	}
	
	private void initalizeWidget(){
		dbFormDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		
		olbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
		
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
		
		btnForm3A=new Button("Form 3A");
		btnForm3A.setEnabled(true);
		btnForm3A.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnForm3A.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnForm3A.getElement().getStyle().setHeight(40, Unit.PX);
		btnForm3A.getElement().getStyle().setWidth(110, Unit.PX);
		btnForm3A.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		btnFormC=new Button("Form C");
		btnFormC.setEnabled(true);
		btnFormC.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnFormC.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnFormC.getElement().getStyle().setHeight(40, Unit.PX);
		btnFormC.getElement().getStyle().setWidth(110, Unit.PX);
		btnFormC.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		
		btnFormD=new Button("Form D");
		btnFormD.setEnabled(true);
		btnFormD.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnFormD.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnFormD.getElement().getStyle().setHeight(40, Unit.PX);
		btnFormD.getElement().getStyle().setWidth(110, Unit.PX);
		btnFormD.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		btnForm9=new Button("Form 9");
		btnForm9.setEnabled(true);
		btnForm9.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnForm9.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnForm9.getElement().getStyle().setHeight(40, Unit.PX);
		btnForm9.getElement().getStyle().setWidth(110, Unit.PX);
		btnForm9.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		btnForm6A=new Button("Form 6A");
		btnForm6A.setEnabled(true);
		btnForm6A.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnForm6A.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnForm6A.getElement().getStyle().setHeight(40, Unit.PX);
		btnForm6A.getElement().getStyle().setWidth(110, Unit.PX);
		btnForm6A.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		btnForm11=new Button("Form No.11");
		btnForm11.setEnabled(true);
		btnForm11.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnForm11.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnForm11.getElement().getStyle().setHeight(40, Unit.PX);
		btnForm11.getElement().getStyle().setWidth(110, Unit.PX);
		btnForm11.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		btnForm2ARevised = new Button("Form 2A(revised)");
      	btnForm2ARevised.setEnabled(true);
		btnForm2ARevised.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnForm2ARevised.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnForm2ARevised.getElement().getStyle().setHeight(40, Unit.PX);
		btnForm2ARevised.getElement().getStyle().setWidth(110, Unit.PX);
		btnForm2ARevised.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
	btnFormA=new Button("Form A");	
	btnFormA.setEnabled(true);
		btnFormA.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnFormA.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnFormA.getElement().getStyle().setHeight(40, Unit.PX);
		btnFormA.getElement().getStyle().setWidth(110, Unit.PX);
		btnFormA.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		btnFormF=new Button("Form F");
		btnFormF.setEnabled(true);
		btnFormF.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnFormF.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnFormF.getElement().getStyle().setHeight(40, Unit.PX);
		btnFormF.getElement().getStyle().setWidth(110, Unit.PX);
		btnFormF.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
	}
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")){
					menus[k].setVisible(true);
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
//		AuthorizationHelper.setAsPerAuthorization(Screen.PROCESSPAYROLL,LoginPresenter.currentModule.trim());
	}

	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames=new String[]{""};
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingInformation=fbuilder.setlabel("Apply Filters").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(7).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPfForms=fbuilder.setlabel("PF").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(7).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingBonusForms=fbuilder.setlabel("Bonus").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(7).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEsicForms=fbuilder.setlabel("ESIC").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(7).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingTdaForms=fbuilder.setlabel("TDA").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(7).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingNominationForms=fbuilder.setlabel("Nomination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(7).build();
		
		fbuilder = new FormFieldBuilder("From",dbFormDate);
		FormField fdbApplicableFormDate=fbuilder.setMandatory(false).setMandatoryMsg("From Date is Mandatory !").setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To",dbToDate);
		FormField fdbApplicableToDate=fbuilder.setMandatory(false).setMandatoryMsg("To Date is Mandatory !").setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project", project);
		FormField fproject = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", btnForm3A);
		FormField fbtnForm3A = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();

		
		fbuilder = new FormFieldBuilder("", btnFormC);
		FormField fbtnFormC = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();

		
		fbuilder = new FormFieldBuilder("", btnFormD);
		FormField fbtnFormD = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", btnForm9);
		FormField fbtnForm9 = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", btnForm6A);
		FormField fbtnForm6A = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();

		
		fbuilder = new FormFieldBuilder("", btnForm11);
		FormField fbtnForm11 = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder=new FormFieldBuilder("",btnFormF);
		FormField  fbtnFormF=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", btnForm2ARevised);
		FormField fbtnForm2ARevised = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		
		fbuilder = new FormFieldBuilder("", btnFormA);
		FormField fbtnFormA = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fempty = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		
		
		/***Date 16-5-2019 by Amol added process configuration to hide the forms***/
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "Form9")){
			hideForm9=true;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "Form11")){
			hideForm11=true;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "FormA")){
			hideFormA=true;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "Form3A")){
			hideForm3A=true;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "Form6A")){
			hideForm6A=true;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "BonusForms")){
			hideBonusForms=true;
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "FormF")){
			hideFormF=true;
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "Form2aReivised")){
			hideForm2aReviced=true;
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "Formc")){
			hideFormC=true;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "FormD")){
			hideFormD=true;
		}
		
		
		
		FormField[] formfield1 = new FormField[5];
		int i = 0;
		if(!hideForm3A){
		formfield1[i] = fbtnForm3A;
		i++;
		}
		if(!hideForm9){
			formfield1[i] = fbtnForm9;
			i++;
		}
		if(!hideForm6A){
		formfield1[i] = fbtnForm6A;
		i++;
		}
		if(!hideForm11){
			formfield1[i] = fbtnForm11;
			i++;
		}
		if(!hideFormA){
			formfield1[i] = fbtnFormA;
			i++;
		}
		for(int j= i ; j< formfield1.length; j++){
			formfield1[j] = fempty;
		}
		if(i==0){
			fgroupingPfForms = fempty;
		}
		
		
		
		
		
		
		FormField[] formfield2 = new FormField[5];
		int m = 0;
		if(!hideFormC){
			formfield2[m] = fbtnFormC;
			m++;
			}
		if(!hideFormD){
			formfield2[m] = fbtnFormD;
			m++;
			}
		
		for(int j= m ; j< formfield2.length; j++){
			formfield2[j] = fempty;
		}
		if(m==0){
			fgroupingBonusForms = fempty;
		}
		
		
		FormField[] formfield3 = new FormField[5];
		int k = 0;
		if(!hideFormF){
			formfield3[k] = fbtnFormF;
			k++;
			}
		if(!hideForm2aReviced){
			formfield3[k] = fbtnForm2ARevised;
			k++;
			}
		
		for(int j= k ; j< formfield3.length; j++){
			formfield3[j] = fempty;
		}
		if(k==0){
			fgroupingNominationForms = fempty;
		}
		
////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		FormField[][] formfield = {  
				{fgroupingInformation},
				{fpersonInfo},
				{folbBranch,fproject,fdbApplicableFormDate,fdbApplicableToDate},
				{fgroupingPfForms},
				 formfield1,
				//{fbtnForm3A,fbtnForm9,fbtnForm6A,fbtnForm11,fbtnFormA},
				{fgroupingBonusForms},
			//	{fbtnFormC,fbtnFormD},
				formfield2,
				{fgroupingNominationForms},
				formfield3,
			//	{fbtnFormF,fbtnForm2ARevised}
				
//				{fbtnForm3A,fbtnFormC,fbtnFormD,fbtnForm9,fbtnForm6A},
//				{fbtnForm11,fbtnFormF,fbtnForm2ARevised,fbtnFormA}
		};
		this.fields=formfield;
	}

	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateView(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

}
