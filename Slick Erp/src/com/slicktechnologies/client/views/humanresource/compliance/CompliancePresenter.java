package com.slicktechnologies.client.views.humanresource.compliance;

import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class CompliancePresenter extends FormScreenPresenter<DummyEntityOnlyForScreen> {

	ComplianceForm form;
	GenricServiceAsync asyncgen = GWT.create(GenricService.class);
	
	public CompliancePresenter(UiScreen<DummyEntityOnlyForScreen> view,DummyEntityOnlyForScreen model) {
		super(view, model);
		form=(ComplianceForm) view;
		form.btnForm3A.addClickHandler(this);
		form.btnFormC.addClickHandler(this);
		form.btnFormD.addClickHandler(this);
		form.btnForm9.addClickHandler(this);
		form.btnForm6A.addClickHandler(this);
		form.btnForm11.addClickHandler(this);
		/**
		 * Updated By: Viraj
		 * Date By: 02-02-2018
		 * Description: To add Form2A revised on click function
		 */
		form.btnForm2ARevised.addClickHandler(this);
		form.btnFormF.addClickHandler(this);
		/**
		 * date 25-3-2019 added by amol
		 * 
		 */
		form.btnFormA.addClickHandler(this);

	}
	
	public static void initialize(){
		AppMemory.getAppMemory().skeleton.isMenuVisble=false;
		ComplianceForm form = new ComplianceForm();
		CompliancePresenter presenter = new CompliancePresenter(form,new DummyEntityOnlyForScreen());
		AppMemory.getAppMemory().stickPnel(form);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		super.onClick(event);
		
		if(event.getSource()==form.btnForm3A){
			if(validFilter()){
				DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy");
				final String url1=GWT.getModuleBaseURL()+ "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"FORM3A"+"&"+"subtype="+form.personInfo.getEmployeeId()+"&"+"fromDate="+format.format(form.dbFormDate.getValue())+"&"+"toDate="+format.format(form.dbToDate.getValue());
				Window.open(url1, "test", "enabled");
			}
		}
		
		if(event.getSource()==form.btnFormC){
			if(validDateFilter()){
		
				DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy");
				final String url1=GWT.getModuleBaseURL()+ "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"FormC"+"&"+"subtype="+form.personInfo.getEmployeeId()+"&"+"fromDate="+format.format(form.dbFormDate.getValue())+"&"+"toDate="+format.format(form.dbToDate.getValue());
				Window.open(url1, "test", "enabled");
			}
		}
		
		if(event.getSource()==form.btnFormD){
			if(validDateFilter()){
				DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy");
				final String url1=GWT.getModuleBaseURL()+ "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"FormD"+"&"+"subtype="+form.personInfo.getEmployeeId()+"&"+"fromDate="+format.format(form.dbFormDate.getValue())+"&"+"toDate="+format.format(form.dbToDate.getValue());
				Window.open(url1, "test", "enabled");
			}
		}
		
		
		if(event.getSource()==form.btnForm9) {
			if(form.personInfo.getEmployeeId()==-1&&form.olbBranch.getValue()==null&&form.project.getValue()==null){
				form.showDialogMessage("Please select either employee ,branch or project.");
				return;
			}
//				DateTimeFormat format=DateTimeFormat.getFormat("dd/MM/yyyy");
			final String url1=GWT.getModuleBaseURL()+ "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"Form9"+"&"+"subtype="+form.personInfo.getEmployeeId()+"&"+"Branch="+form.olbBranch.getValue()+"&"+"Project="+form.project.getValue();
			Window.open(url1, "test", "enabled");
			}
		
		
		if(event.getSource()==form.btnForm6A) {
			if(validDateFilter()){
			DateTimeFormat format=DateTimeFormat.getFormat("dd/MM/yyyy");
			final String url1=GWT.getModuleBaseURL()+ "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"Form6A"+"&"+"subtype="+"&"+"fromDate="+format.format(form.dbFormDate.getValue())+"&"+"toDate="+format.format(form.dbToDate.getValue());
			Window.open(url1, "test", "enabled");

			}
		}
		
		if(event.getSource()==form.btnForm11) {
			if(form.personInfo.getValue()!=null){
				final String url1=GWT.getModuleBaseURL()+ "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"Form11"+"&"+"subtype="+form.personInfo.getEmployeeId();
				Window.open(url1, "test", "enabled");
			}else{
				form.showDialogMessage("Please select employee.");
			}
		}
		
		/**
		 * Updated By: Viraj
		 * Date By: 02-02-2018
		 * Description: To add Form2A revised on click function
		 */
		if(event.getSource() == form.btnForm2ARevised) {
			if(form.personInfo.getValue()!=null){
				final String url1=GWT.getModuleBaseURL()+ "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"Form2A(Revised)"+"&"+"subtype="+form.personInfo.getEmployeeId();
				Window.open(url1, "test", "enabled");
			} else {
				form.showDialogMessage("Please select employee.");
			}
		}
	/**
		 * Updated By: Amol
		 * Date By: 25-03-2019
		 * Description: To add FormA on click function
		 */
		if(event.getSource() == form.btnFormA) {
			if(form.personInfo.getEmployeeId()==-1&&form.olbBranch.getValue()==null&&form.project.getValue()==null){
				form.showDialogMessage("Please select either employee ,branch or project.");
				return;
			}
				final String url1=GWT.getModuleBaseURL()+ "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"FormA"+"&"+"subtype="+form.personInfo.getEmployeeId()+"&"+"Branch="+form.olbBranch.getValue()+"&"+"Project="+form.project.getValue();
				Window.open(url1, "test", "enabled");
			}
		
		
		
		
		if(event.getSource()==form.btnFormF){
				if(form.personInfo.getValue()!=null){
				final String url1=GWT.getModuleBaseURL()+ "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"FormF"+"&"+"subtype="+form.personInfo.getEmployeeId();
				Window.open(url1, "test", "enabled");
				}else{
					form.showDialogMessage("Please select employee.");
				}
				
		}
		
		
		
	}
	
	private boolean validFilter() {
		String msg="";
		if(form.personInfo.getValue()==null||form.personInfo.getEmployeeId()==-1){
			msg+="Please select employee."+"\n";
		}
		if(form.dbFormDate.getValue()==null){
			msg+="Please select from date."+"\n";
		}
		if(form.dbToDate.getValue()==null){
			msg+="Please select to date.";
		}
		
		if(!msg.equals("")){
			form.showDialogMessage(msg);
			return false;
		}
		
		return true;
	}
	
	
	
	
       private boolean validDateFilter() {
		String msg="";
		
		if(form.dbFormDate.getValue()==null){
			msg+="Please select from date."+"\n";
		}
		if(form.dbToDate.getValue()==null){
			msg+="Please select to date.";
		}
		
		if(!msg.equals("")){
			form.showDialogMessage(msg);
			return false;
		}
		
		return true;
	}

		
			private boolean validBranchFilter(){
				String msg="";
				if(form.olbBranch.getValue()==null){
					msg+="Please select Branch."+"\n";
				}
				if(form.project.getValue()==null){
					msg+="Please select Project."+"\n";
				}
				if(!msg.equals("")){
					form.showDialogMessage(msg);
					return false;
				}
				return true;
			}
			

			

			
		

}
