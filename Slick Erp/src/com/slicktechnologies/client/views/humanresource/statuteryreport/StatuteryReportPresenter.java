package com.slicktechnologies.client.views.humanresource.statuteryreport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.TxtService;
import com.slicktechnologies.client.services.TxtServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.client.views.humanresource.allocation.payslipallocation.PaySlipAllocationService;
import com.slicktechnologies.client.views.humanresource.allocation.payslipallocation.PaySlipAllocationServiceAsync;
import com.slicktechnologies.client.views.humanresource.allocation.payslipallocation.PaySlipForm;
import com.slicktechnologies.client.views.humanresource.allocation.payslipallocation.PaySlipPresenter;
import com.slicktechnologies.client.views.humanresource.timereport.EmployeeTimeReportDetails;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipAllocationProcess;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class StatuteryReportPresenter extends FormScreenPresenter<DummyEntityOnlyForScreen> implements ClickHandler  {
	StatuteryReportForm form;
	final CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final static GenricServiceAsync async =GWT.create(GenricService.class);
	PaySlipAllocationServiceAsync payAllServ = GWT.create(PaySlipAllocationService.class);
	
	TxtServiceAsync txtservice=GWT.create(TxtService.class);
	int customerCounter;
	public static Customer customer;
	public StatuteryReportPresenter(UiScreen<DummyEntityOnlyForScreen> view,DummyEntityOnlyForScreen model) {
		super(view, model);
		
		form = (StatuteryReportForm) view;
		form.bnPF.addClickHandler(this);
		form.bnPT.addClickHandler(this);
		form.bnTDS.addClickHandler(this);
		form.bnLWF.addClickHandler(this);
//		form.bnMWF.addClickHandler(this);
		form.bnESIC.addClickHandler(this);
		form.bnPdfPF.addClickHandler(this);
		form.bnPdfLWF.addClickHandler(this);
		form.bnPdfESIC.addClickHandler(this);
		
		form.btnSalaryRegister.addClickHandler(this);
		form.btnSalarySlip.addClickHandler(this);
		form.btnSalRegInternal.addClickHandler(this);
		
		form.btnProvisionalPayrollReport.addClickHandler(this);
		/**26-12-2018 added by amol form2 btn**/
		form.bnPdfForm2.addClickHandler(this);
		
		/**
		 * @author Anil ,Date : 02-03-2019
		 */
		form.btnPFtxt.addClickHandler(this);
		
		form.btnFormTExcel.addClickHandler(this);
			/**Date 29-3-2019 added by Amol**/
		form.btnFormB.addClickHandler(this);
		form.btnFormXIIIExcel.addClickHandler(this);
		
		form.btnFormXVII.addClickHandler(this);
		
		/**Date 19-2-2020 by Amol added this method 
		 * if Customer User Is log in and its Company Type is "Facility Management"
		 * then the  CustomerUser Details should be mapped in Statutory Report Screen
		 */
		
		if (LoginPresenter.loggedInCustomer == true) {
			Console.log("Inside Statutory Constructor");
			getCustomerDetails(LoginPresenter.loggedInUser, false);
		}
		
	}

	private void getCustomerDetails(final String loggedInUser, boolean companyFlag) {
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		if (companyFlag == false) {
			filter = new Filter();
			filter.setQuerryString("fullname");
			filter.setStringValue(loggedInUser);
			filtervec.add(filter);
		} else {
			filter = new Filter();
			filter.setQuerryString("companyName");
			filter.setStringValue(loggedInUser);
			filtervec.add(filter);
		}
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if(result.size()!=0){
					Customer entity=(Customer) result.get(0);
					
					customer = entity;
					
					PersonInfo info=new PersonInfo();
					info.setCount(entity.getCount());
					if(entity.isCompany()){
						info.setFullName(entity.getCompanyName());
					}else{
						info.setFullName(entity.getFullname());
					}
					info.setCellNumber(entity.getCellNumber1());
					info.setPocName(entity.getFullname());
					
					
					form.clientComposite.setValue(info);
					form.clientComposite.setEnable(false);
					
				}else{
					
					//For Company Name........
					if(customerCounter==0){
						customerCounter++;
						getCustomerDetails(loggedInUser,true);
					}
				}
			}
		});
		
	}

	public static StatuteryReportForm initalize() {
		StatuteryReportForm form = new StatuteryReportForm();
		StatuteryReportPresenter presenter = new StatuteryReportPresenter(form,new DummyEntityOnlyForScreen());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
	}

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}

	@Override
	protected void makeNewModel() {
		
	}
	
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
		if(event.getSource().equals(form.bnPF)){
			getStatuteryReport("PF");
		}
		if(event.getSource().equals(form.bnPT)){
			getStatuteryReport("PT");
		}
		if(event.getSource().equals(form.bnLWF)){
			getStatuteryReport("LWF");
		}
		if(event.getSource().equals(form.bnTDS)){
			getStatuteryReport("TDS");
		}
		if(event.getSource().equals(form.bnESIC)){
			getStatuteryReport("ESIC");
		}
		if(event.getSource().equals(form.btnSalaryRegister)){
			getStatuteryReport("Salary Register");
		}
		if(event.getSource().equals(form.btnSalarySlip)){
			getStatuteryReport("Salary Slip");
		}
		if(event.getSource().equals(form.btnSalRegInternal)){
			getStatuteryReport("Salary Register Internal");
		}
		
		if(event.getSource().equals(form.bnPdfPF)){
			getStatuteryReport("PF PDF");
			
//			final String url=gwt + "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"PFreport"+"&"+"subtype="+"PFreport";
//			Window.open(url, "test", "enabled");
		}
		
		if(event.getSource().equals(form.bnPdfLWF)){
			getStatuteryReport("LWF PDF");
			
//			final String url2=gwt + "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"LWFReport"+"&"+"subtype="+"LWFReport";
//			Window.open(url2, "test", "enabled");
		}
		if(event.getSource().equals(form.bnPdfESIC)){
			getStatuteryReport("ESIC PDF");
			
//			final String url1=gwt + "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"ESICReport"+"&"+"subtype="+"ESICReport";
//			Window.open(url1, "test", "enabled");	
		
		}
		if(event.getSource()==form.bnPdfForm2) {
			getStatuteryReport("Form 2 MH");
		}
		
		if(event.getSource().equals(form.btnProvisionalPayrollReport)){
			getProvisionalPayrollReport();
		}
		
		if(event.getSource().equals(form.btnPFtxt)){
			getStatuteryReport("PF TXT");
		}
		
		if(event.getSource().equals(form.btnFormTExcel)){
			getStatuteryReport("Form T");
		}
		if(event.getSource().equals(form.btnFormB)){
			getStatuteryReport("Form B");
		}
	
		if(event.getSource().equals(form.btnFormXIIIExcel)){
			getEmployeeDetails("Form XIII");
		}
	
		if(event.getSource().equals(form.btnFormXVII)){
			getStatuteryReport("Form XVII");
		}
	
	
	}

	private void getProvisionalPayrollReport() {
//		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
//		filter = new Filter();
//		filter.setQuerryString("salaryPeriod");
//		filter.setStringValue(form.getPeriod());
//		filtervec.add(filter);
		
		if(form.olbbBranch.getSelectedIndex()!=0){
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.olbbBranch.getValue());
			filtervec.add(filter);
		}
		if(form.project.getSelectedIndex()!=0){
			filter = new Filter();
			filter.setQuerryString("projectName");
			filter.setStringValue(form.project.getValue());
			filtervec.add(filter);
		}
				
		ArrayList<Filter> filterList=new ArrayList<>(filtervec);
		form.showWaitSymbol();
		final String period = form.getPeriod();
		payAllServ.getAllocationStatus(filterList, period, null,null, new AsyncCallback<ArrayList<AllocationResult>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
			@Override
			public void onSuccess(ArrayList<AllocationResult> result) {
				form.hideWaitSymbol();
				if(result!=null&&result.size()!=0){
					payAllServ.getProvisionalPayroll(result, period, new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result) {
							form.hideWaitSymbol();
							final String url = GWT.getModuleBaseURL() + "payslipstatement" + "?Id="+ UserConfiguration.getCompanyId()+"&"+"Type="+"Internal";
							Window.open(url, "test", "enabled");
						}
					});
				}else{
					form.showDialogMessage("No employee found.");
				}
				
			}
		});
		
		
	}
	
	private boolean validateBranchProject(){

		
		String alertMsg="";
//		if(form.dbPeriod.getValue()==null){
//			alertMsg=alertMsg+"Please select report period."+"\n";
//		}
		if(form.olbbBranch.getSelectedIndex()==0){
			/**
			 * @author Anil ,Date :20-04-2019
			 */
			if(form.clientComposite.getIdValue()==-1){
				alertMsg=alertMsg+"Please select branch."+"\n";
			}
			
		}
		/**
		 * Date : 09-10-2018 BY ANIL
		 * Commented this condition to make project selection non mandatory,to get consolidated report branch wise
		 */
//		if(form.project.getSelectedIndex()==0){
//			alertMsg=alertMsg+"Please select project.";
//		}
		/**
		 * End
		 */
		
		if(!"".equalsIgnoreCase(alertMsg)){
			form.showDialogMessage(alertMsg);
			return false;
		}
		
		return true;
	
	}
	
	
	
	
	
	
	

	private boolean validateMonthYear() {
		
		String alertMsg="";
		if(form.dbPeriod.getValue()==null){
			alertMsg=alertMsg+"Please select report period."+"\n";
		}
		if(form.olbbBranch.getSelectedIndex()==0){
			/**
			 * @author Anil ,Date :20-04-2019
			 */
			if(form.clientComposite.getIdValue()==-1){
				alertMsg=alertMsg+"Please select branch."+"\n";
			}
			
		}
		/**
		 * Date : 09-10-2018 BY ANIL
		 * Commented this condition to make project selection non mandatory,to get consolidated report branch wise
		 */
//		if(form.project.getSelectedIndex()==0){
//			alertMsg=alertMsg+"Please select project.";
//		}
		/**
		 * End
		 */
		
		if(!"".equalsIgnoreCase(alertMsg)){
			form.showDialogMessage(alertMsg);
			return false;
		}
		
		return true;
	}
	
	
	public void getEmployeeDetails(final String reportType){
		
//		if(reportType.equalsIgnoreCase("Payroll")){
//			return;
//		}else if(reportType.equalsIgnoreCase("Attendance")){
////			return;
//		}
		
		
		if(validateBranchProject()){
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
//		  	
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
//			filter = new Filter();
//			filter.setQuerryString("salaryPeriod");
//			filter.setStringValue(form.getPeriod());
//			filtervec.add(filter);
			
			if(form.olbbBranch.getSelectedIndex()!=0){
				filter = new Filter();
				filter.setQuerryString("branchName");
				filter.setStringValue(form.olbbBranch.getValue());
				filtervec.add(filter);
			}
			if(form.project.getSelectedIndex()!=0){
				filter = new Filter();
				filter.setQuerryString("projectName");
				filter.setStringValue(form.project.getValue());
				filtervec.add(filter);
			}
			
			
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Employee());
			form.showWaitSymbol();
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
//					System.out.println("Throwable caught");
					form.hideWaitSymbol();
				}
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					if(result.size()==0){
						form.showDialogMessage("No record found!");
						form.hideWaitSymbol();
					}
					else{
						System.out.println("result"+result.size());
						ArrayList<Employee> empDetailsarray=new ArrayList<Employee>();
						for(SuperModel s: result){
							Employee emp =(Employee)s;
							empDetailsarray.add(emp);
						}
						
						
						if(empDetailsarray.size()!=0){
							Comparator<Employee> projectComp = new  Comparator<Employee>() {
								@Override
								public int compare(Employee o1, Employee o2) {
									return o1.getProjectName().compareTo(o2.getProjectName());
								}
							};
							Collections.sort(empDetailsarray,projectComp);
						}
						
						/**
						 * End
						 */
						csvservice.setFormXIII(empDetailsarray, new AsyncCallback<Void>() {
							@Override
							public void onFailure(Throwable caught) {
								System.out.println("RPC call Failed"+caught);
								form.hideWaitSymbol();
							}
							@Override
							public void onSuccess(Void result) {
								form.hideWaitSymbol();
								int reportNo=0;
								String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
								if(reportType.equalsIgnoreCase("Form XIII")){
									reportNo=174;
								}
								
	//							String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url=gwt + "csvservlet"+"?type="+reportNo;
								Window.open(url, "test", "enabled");
							}
						});
					}
				}
			});
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public void getStatuteryReport(final String reportType){
		
		if(reportType.equalsIgnoreCase("Payroll")){
			return;
		}else if(reportType.equalsIgnoreCase("Attendance")){
			return;
		}
		
		
		if(validateMonthYear()){
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("salaryPeriod");
			filter.setStringValue(form.getPeriod());
			filtervec.add(filter);
			
			if(form.olbbBranch.getSelectedIndex()!=0){
				filter = new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(form.olbbBranch.getValue());
				filtervec.add(filter);
			}
			if(form.project.getSelectedIndex()!=0){
				filter = new Filter();
				filter.setQuerryString("projectName");
				filter.setStringValue(form.project.getValue());
				filtervec.add(filter);
			}
			
			/**
			 * @author Anil ,Date : 28-03-2019
			 * Adding client's details for searching reports
			 */
			if(form.clientComposite.getValue()!=null){
				if(form.clientComposite.getIdValue()!=-1){
					filter = new Filter();
					filter.setQuerryString("clientInfo.count");
					filter.setIntValue(form.clientComposite.getIdValue());
					filtervec.add(filter);
				}
			}
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new PaySlip());
			form.showWaitSymbol();
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
//					System.out.println("Throwable caught");
					form.hideWaitSymbol();
				}
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					if(result.size()==0){
						form.showDialogMessage("No record found!");
						form.hideWaitSymbol();
					}
					else{
						System.out.println("result"+result.size());
						ArrayList<PaySlip> paysliparray=new ArrayList<PaySlip>();
						for(SuperModel s: result){
							PaySlip pay =(PaySlip)s;
							paysliparray.add(pay);
						}
						/**
						 * Date : 09-10-2018 By Anil
						 * Here we are Sorting paysliparray project wise
						 */
						
						if(paysliparray.size()!=0){
							Comparator<PaySlip> projectComp = new  Comparator<PaySlip>() {
								@Override
								public int compare(PaySlip o1, PaySlip o2) {
									return o1.getProjectName().compareTo(o2.getProjectName());
								}
							};
							Collections.sort(paysliparray,projectComp);
						}
						
						/**
						 * End
						 */
						
						
						if(reportType.equalsIgnoreCase("Salary Register")||reportType.equalsIgnoreCase("Salary Register Internal")||reportType.equalsIgnoreCase("Form 2 MH")){
							csvservice.setPaySliplist(paysliparray, new AsyncCallback<Void>() {
								@Override
								public void onFailure(Throwable caught) {
									form.hideWaitSymbol();
								}
								@Override
								public void onSuccess(Void result) {
									form.hideWaitSymbol();
									String type="";
									if(reportType.equalsIgnoreCase("Salary Register")){
										type="External";
										final String url = GWT.getModuleBaseURL() + "payslipstatement" + "?Id="+ UserConfiguration.getCompanyId()+"&"+"Type="+type;
										Window.open(url, "test", "enabled");
									}else if(reportType.equalsIgnoreCase("Salary Register Internal")){
										type="Internal";
										final String url = GWT.getModuleBaseURL() + "payslipstatement" + "?Id="+ UserConfiguration.getCompanyId()+"&"+"Type="+type;
										Window.open(url, "test", "enabled");
									}else if(reportType.equalsIgnoreCase("Form 2 MH")){
										DateTimeFormat format=DateTimeFormat.getFormat("dd/MM/yyyy");
										final String url1=GWT.getModuleBaseURL()+ "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"Form2"+"&"+"subtype="+"Form2";
										Window.open(url1, "test", "enabled");
									}
									
								}
							});
							return;
						}else if(reportType.equalsIgnoreCase("Salary Slip")){
							csvservice.setSalarySlipData(paysliparray, new AsyncCallback<Void>() {
								@Override
								public void onFailure(Throwable caught) {
									form.hideWaitSymbol();
								}
								@Override
								public void onSuccess(Void result) {
									form.hideWaitSymbol();
									/**
									 * Updated By:Viraj
									 * Date: 20-06-2019
									 * Description: Added status for printing 2 salary slips on one 
									 */
									final String url = com.google.gwt.core.client.GWT.getModuleBaseURL() + "processpayslip" + "?status=" + "true";
									Window.open(url, "test", "enabled");
								}
							});
							
							return;
						}else if(reportType.equalsIgnoreCase("PF TXT")){
							/**
							 * @author Anil,Date :02-03-2019
							 * for generating pf upload chalan in txt format
							 */
							txtservice.setPaySliplist(paysliparray, new AsyncCallback<Void>() {
								@Override
								public void onFailure(Throwable caught) {
									form.hideWaitSymbol();
								}
								@Override
								public void onSuccess(Void result) {
									form.hideWaitSymbol();
									final String url=com.google.gwt.core.client.GWT.getModuleBaseURL() + "txtservlet"+"?type="+1;
									Window.open(url, "test", "enabled");
								}
							});
							
							return;
						}
						
						csvservice.setPayslip(paysliparray, new AsyncCallback<Void>() {
							@Override
							public void onFailure(Throwable caught) {
								System.out.println("RPC call Failed"+caught);
								form.hideWaitSymbol();
							}
							@Override
							public void onSuccess(Void result) {
								form.hideWaitSymbol();
								int reportNo=0;
								String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
								if(reportType.equalsIgnoreCase("Payroll")){
									reportNo=0;
									return;
								}else if(reportType.equalsIgnoreCase("Attendance")){
									reportNo=0;
									return;
								}else if(reportType.equalsIgnoreCase("PF")){
									reportNo=143;
								}else if(reportType.equalsIgnoreCase("PT")){
									reportNo=144;
								}else if(reportType.equalsIgnoreCase("TDS")){
									reportNo=146;
								}else if(reportType.equalsIgnoreCase("LWF")){
									reportNo=148;
								}else if(reportType.equalsIgnoreCase("ESIC")){
									reportNo=145;
								}else if(reportType.equalsIgnoreCase("Form T")){
									reportNo=169;
								}
								else if(reportType.equalsIgnoreCase("Form B")){
									reportNo=171;
								}else if(reportType.equalsIgnoreCase("PF PDF")){
									final String url=gwt + "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"PFreport"+"&"+"subtype="+"PFreport";
									Window.open(url, "test", "enabled");
									reportNo=-1;
								}else if(reportType.equalsIgnoreCase("ESIC PDF")){
									final String url1=gwt + "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"ESICReport"+"&"+"subtype="+"ESICReport";
									Window.open(url1, "test", "enabled");	
									reportNo=-1;
								}else if(reportType.equalsIgnoreCase("LWF PDF")){
									final String url2=gwt + "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"LWFReport"+"&"+"subtype="+"LWFReport";
									Window.open(url2, "test", "enabled");
									reportNo=-1;
								}else if(reportType.equalsIgnoreCase("FORM XVII")){
									final String url2=gwt + "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"FORM XVII"+"&"+"subtype="+"FORM XVII";
									Window.open(url2, "test", "enabled");
									reportNo=-1;
								}else{
									reportNo=-1;
								}
								
								if(reportNo!=-1){
									final String url=gwt + "csvservlet"+"?type="+reportNo;
									Window.open(url, "test", "enabled");
								}
							}
						});
					}
				}
			});
		}
		
	}
	
	
	
}
