package com.slicktechnologies.client.views.humanresource.statuteryreport;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;

public class StatuteryReportForm extends FormScreen<DummyEntityOnlyForScreen> implements ClickHandler,ChangeHandler {

//	ListBox lbMonth;
//	ListBox lbYear;
	
	
	Button bnPF, bnPT, bnTDS, bnLWF, bnESIC,bnpayroll,bnattendence,bnPdfPF,bnPdfLWF,bnPdfESIC,bnPdfForm2;
	ObjectListBox<Branch> olbbBranch;
	
	DateBox dbPeriod;
	ObjectListBox<HrProject> project;
	Button btnSalaryRegister,btnSalarySlip,btnSalRegInternal;
	
	/**
	 * Date : 24-11-2018 By ANIL
	 * Added button provisional payroll report
	 */
	Button btnProvisionalPayrollReport;
	
	/**
	 * @author Anil, Date : 02-03-2019
	 */
	Button btnPFtxt;
	
	/**
	 * @author Anil ,Date : 28-03-2019
	 */
	PersonInfoComposite clientComposite;
/**Date 29-3-2019 added by Amol
	 * 
	 */
	Button btnFormB;
	ArrayList<String> branchList=new ArrayList<String>();
	final GenricServiceAsync async = GWT.create(GenricService.class);
	
	
	/**
	 * @author Anil, Date : 01-04-2019
	 * Added form T button
	 */
	Button btnFormTExcel;

	/**
	 * @author Amol, Date : 07-07-2019
	 * Added form XIII button
	 */
	Button btnFormXIIIExcel;
	
	/**
	 * @author Anil , Date : 10-08-2019
	 * Added form xvii
	 */
	Button btnFormXVII;
	
	public StatuteryReportForm() {
//		super();
		super(FormStyle.DEFAULT);
		createGui();
		DateTimeFormat dateFormat=DateTimeFormat.getFormat("yyyy-MMM");
		dbPeriod.setFormat(new DateBox.DefaultFormat(dateFormat));

	}

	private void initalizeWidget() {
		olbbBranch=new ObjectListBox<Branch>();
//		olbbBranch.getElement().getStyle().setHeight(30, Unit.PX);
//		olbbBranch.getElement().getStyle().setWidth(150, Unit.PX);
		AppUtility.makeBranchListBoxLive(olbbBranch);
		olbbBranch.addChangeHandler(this);
		
		bnPF = new Button("PF");
		bnPF.setEnabled(true);
		bnPF.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		bnPF.getElement().getStyle().setFontSize(100, Unit.PCT);
		bnPF.getElement().getStyle().setHeight(40, Unit.PX);
		bnPF.getElement().getStyle().setWidth(110, Unit.PX);
		bnPF.getElement().getStyle().setBackgroundColor("#D3D3D3");

		bnPT = new Button("PT");
		bnPT.setEnabled(true);
		bnPT.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		bnPT.getElement().getStyle().setFontSize(100, Unit.PCT);
		bnPT.getElement().getStyle().setHeight(40, Unit.PX);
		bnPT.getElement().getStyle().setWidth(110, Unit.PX);
		bnPT.getElement().getStyle().setBackgroundColor("#D3D3D3");

		bnTDS = new Button("TDS");
		bnTDS.setEnabled(true);
		bnTDS.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		bnTDS.getElement().getStyle().setFontSize(100, Unit.PCT);
		bnTDS.getElement().getStyle().setHeight(40, Unit.PX);
		bnTDS.getElement().getStyle().setWidth(110, Unit.PX);
		bnTDS.getElement().getStyle().setBackgroundColor("#D3D3D3");

		bnLWF = new Button("LWF");
		bnLWF.setEnabled(true);
		bnLWF.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		bnLWF.getElement().getStyle().setFontSize(100, Unit.PCT);
		bnLWF.getElement().getStyle().setHeight(40, Unit.PX);
		bnLWF.getElement().getStyle().setWidth(110, Unit.PX);
		bnLWF.getElement().getStyle().setBackgroundColor("#D3D3D3");

//		bnLWF = new Button("LWF");
//		bnLWF.setEnabled(true);
//		bnLWF.getElement().getStyle().setFontWeight(FontWeight.BOLD);
//		bnLWF.getElement().getStyle().setFontSize(100, Unit.PCT);
//		bnLWF.getElement().getStyle().setHeight(40, Unit.PX);
//		bnLWF.getElement().getStyle().setWidth(110, Unit.PX);
//		bnLWF.getElement().getStyle().setBackgroundColor("#D3D3D3");

		bnESIC = new Button("ESIC");
		bnESIC.setEnabled(true);
		bnESIC.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		bnESIC.getElement().getStyle().setFontSize(100, Unit.PCT);
		bnESIC.getElement().getStyle().setHeight(40, Unit.PX);
		bnESIC.getElement().getStyle().setWidth(110, Unit.PX);
		bnESIC.getElement().getStyle().setBackgroundColor("#D3D3D3");

		
		bnPdfPF=new Button("PF");
		bnPdfPF.setEnabled(true);
		bnPdfPF.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		bnPdfPF.getElement().getStyle().setFontSize(100, Unit.PCT);
		bnPdfPF.getElement().getStyle().setHeight(40, Unit.PX);
		bnPdfPF.getElement().getStyle().setWidth(110, Unit.PX);
		bnPdfPF.getElement().getStyle().setBackgroundColor("#D3D3D3");

		bnPdfLWF=new Button("LWF");
		bnPdfLWF.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		bnPdfLWF.getElement().getStyle().setFontSize(100, Unit.PCT);
		bnPdfLWF.getElement().getStyle().setHeight(40, Unit.PX);
		bnPdfLWF.getElement().getStyle().setWidth(110, Unit.PX);
		bnPdfLWF.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		bnPdfESIC=new Button("ESIC");
		bnPdfESIC.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		bnPdfESIC.getElement().getStyle().setFontSize(100, Unit.PCT);
		bnPdfESIC.getElement().getStyle().setHeight(40, Unit.PX);
		bnPdfESIC.getElement().getStyle().setWidth(110, Unit.PX);
		bnPdfESIC.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		
		
		/**26-11-2018 added by amol form2 btn***/
		
		bnPdfForm2=new Button("Form II");
		bnPdfForm2.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		bnPdfForm2.getElement().getStyle().setFontSize(100, Unit.PCT);
		bnPdfForm2.getElement().getStyle().setHeight(40, Unit.PX);
		bnPdfForm2.getElement().getStyle().setWidth(110, Unit.PX);
		bnPdfForm2.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		

		bnpayroll=new Button("PAYROLL");
		bnpayroll.setEnabled(true);
		bnpayroll.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		bnpayroll.getElement().getStyle().setFontSize(100, Unit.PCT);
		bnpayroll.getElement().getStyle().setHeight(40, Unit.PX);
		bnpayroll.getElement().getStyle().setWidth(110, Unit.PX);
		bnpayroll.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		
		bnattendence=new Button("ATTENDENCE");
		bnattendence.setEnabled(true);
		bnattendence.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		bnattendence.getElement().getStyle().setFontSize(100, Unit.PCT);
		bnattendence.getElement().getStyle().setHeight(40, Unit.PX);
		bnattendence.getElement().getStyle().setWidth(110, Unit.PX);
		bnattendence.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		
		dbPeriod=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat=DateTimeFormat.getFormat("yyyy-MMM");
		dbPeriod.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
		project.addChangeHandler(this);
		
		btnSalaryRegister=new Button("Salary Register");
		btnSalaryRegister.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnSalaryRegister.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnSalaryRegister.getElement().getStyle().setHeight(40, Unit.PX);
		btnSalaryRegister.getElement().getStyle().setWidth(110, Unit.PX);
		btnSalaryRegister.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		btnSalarySlip=new Button("Salary Slip");
		btnSalarySlip.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnSalarySlip.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnSalarySlip.getElement().getStyle().setHeight(40, Unit.PX);
		btnSalarySlip.getElement().getStyle().setWidth(110, Unit.PX);
		btnSalarySlip.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		btnSalRegInternal=new Button("Salary Register Internal");
		btnSalRegInternal.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnSalRegInternal.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnSalRegInternal.getElement().getStyle().setHeight(40, Unit.PX);
		btnSalRegInternal.getElement().getStyle().setWidth(110, Unit.PX);
		btnSalRegInternal.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		btnProvisionalPayrollReport=new Button("Provisional Payroll");
		btnProvisionalPayrollReport.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnProvisionalPayrollReport.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnProvisionalPayrollReport.getElement().getStyle().setHeight(40, Unit.PX);
		btnProvisionalPayrollReport.getElement().getStyle().setWidth(110, Unit.PX);
		btnProvisionalPayrollReport.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		btnPFtxt=new Button("PF");
		btnPFtxt.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnPFtxt.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnPFtxt.getElement().getStyle().setHeight(40, Unit.PX);
		btnPFtxt.getElement().getStyle().setWidth(110, Unit.PX);
		btnPFtxt.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		
		clientComposite=AppUtility.customerInfoComposite(new Customer());
		clientComposite.getCustomerId().getHeaderLabel().setText("Customer ID");
		clientComposite.getCustomerName().getHeaderLabel().setText("Customer Name");
		clientComposite.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		btnFormTExcel=new Button("Form T");
		btnFormTExcel.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnFormTExcel.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnFormTExcel.getElement().getStyle().setHeight(40, Unit.PX);
		btnFormTExcel.getElement().getStyle().setWidth(110, Unit.PX);
		btnFormTExcel.getElement().getStyle().setBackgroundColor("#D3D3D3");
		btnFormB=new Button("Form B");
		btnFormB.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnFormB.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnFormB.getElement().getStyle().setHeight(40, Unit.PX);
		btnFormB.getElement().getStyle().setWidth(110, Unit.PX);
		btnFormB.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		btnFormXIIIExcel=new Button("Form XIII");
		btnFormXIIIExcel.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnFormXIIIExcel.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnFormXIIIExcel.getElement().getStyle().setHeight(40, Unit.PX);
		btnFormXIIIExcel.getElement().getStyle().setWidth(110, Unit.PX);
		btnFormXIIIExcel.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		btnFormXVII=new Button("Form XVII");
		btnFormXVII.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		btnFormXVII.getElement().getStyle().setFontSize(100, Unit.PCT);
		btnFormXVII.getElement().getStyle().setHeight(40, Unit.PX);
		btnFormXVII.getElement().getStyle().setWidth(110, Unit.PX);
		btnFormXVII.getElement().getStyle().setBackgroundColor("#D3D3D3");
		
		
		
	}

	@Override
	public void onClick(ClickEvent event) {

	}

	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);  
				}

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);  	
				}
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);  	
				}
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.TIMEREPORT,LoginPresenter.currentModule.trim());
	}

	@Override
	public void createScreen() {

		initalizeWidget();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();

		FormField fgroupingPayRollPeriod = fbuilder
				.setlabel("Statutory Report").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(7).build();

		FormField fgroupingPayRollexcelreport = fbuilder
				.setlabel("Excel Reports").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(7).build();

		FormField fgroupingPayRollpdfreport = fbuilder
				.setlabel("Pdf Reports").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(7).build();

		
		
		
		
//		fbuilder = new FormFieldBuilder("*Month", lbMonth);
//		FormField flbMonth = fbuilder.setMandatory(true)
//				.setMandatoryMsg("Month is Mandatory!").setRowSpan(2)
//				.setColSpan(2).build();
//
//		fbuilder = new FormFieldBuilder(" *Year", lbYear);
//		FormField flbYear = fbuilder.setMandatory(true)
//				.setMandatoryMsg("Year is Mandatory!").setRowSpan(2)
//				.setColSpan(2).build();
		
		

		fbuilder = new FormFieldBuilder("Branch",olbbBranch);
		FormField folbbBranch= fbuilder.setMandatory(false).setMandatoryMsg("Branch is Mandatory").setRowSpan(2).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("", bnPF);
		FormField fbnPF = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();

		fbuilder = new FormFieldBuilder("", bnPT);
		FormField fbnPT = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();

		fbuilder = new FormFieldBuilder("", bnTDS);
		FormField fbnTDS = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();

//		fbuilder = new FormFieldBuilder("", bnMWF);
//		FormField fbnMWF = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(1).build();

		fbuilder = new FormFieldBuilder("", bnLWF);
		FormField fbnLWF = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();

		fbuilder = new FormFieldBuilder("", bnESIC);
		FormField fbnESIC = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		
		fbuilder = new FormFieldBuilder("", bnPdfPF);
		FormField fbnPdfPF = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", bnPdfLWF);
		FormField fbnPdfLWF = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", bnPdfESIC);
		FormField fbnPdfESIC = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		
		
		
		fbuilder = new FormFieldBuilder("", bnpayroll);
		FormField fbnpayroll = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", bnattendence);
		FormField fbnattendence = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		
		fbuilder = new FormFieldBuilder("", btnSalaryRegister);
		FormField fbtnSalaryRegister = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", btnSalarySlip);
		FormField fbtnSalarySlip = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Period", dbPeriod);
		FormField fdbPeriod = fbuilder.setMandatory(true)
				.setMandatoryMsg("Month is Mandatory!").setRowSpan(2)
				.setColSpan(2).build();

		fbuilder = new FormFieldBuilder("Project", project);
		FormField fproject = fbuilder.setMandatory(true)
				.setMandatoryMsg("Year is Mandatory!").setRowSpan(2)
				.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("", btnSalRegInternal);
		FormField fbtnSalRegInternal = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", btnProvisionalPayrollReport);
		FormField fbtnProvisionalPayrollReport = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		
		/**26-12-2018 form2 btn added by amol**/
		fbuilder = new FormFieldBuilder("", bnPdfForm2);
		FormField fbnPdfForm2 = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		
		FormField fgroupingtxtreport = fbuilder
				.setlabel("Txt Reports").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(7).build();
		
		fbuilder = new FormFieldBuilder("", btnPFtxt);
		FormField fbtnPFtxt = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", clientComposite);
		FormField fclientComposite = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(7).build();
		
		fbuilder = new FormFieldBuilder("", btnFormTExcel);
		FormField fbtnFormTExcel = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		
			fbuilder = new FormFieldBuilder("", btnFormB);
		FormField fbtnFormB = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", btnFormXIIIExcel);
		FormField fbtnFormXIIIExcel = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", btnFormXVII);
		FormField fbtnFormXVII = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		

		FormField[][] formfield = { { fgroupingPayRollPeriod },
//				{ flbMonth, flbYear ,folbbBranch}, 
				{fclientComposite},
				{fdbPeriod,fproject,folbbBranch},
//				{fbnpayroll,fbnattendence,fbnPF,fbnPT, fbnTDS , fbnLWF, fbnESIC},
//			    {fbtnSalaryRegister,fbtnSalarySlip,fbnPF,fbnPT, fbnTDS , fbnLWF, fbnESIC},
				{fgroupingPayRollexcelreport},
				{fbnPF,fbnLWF,fbnESIC,fbnTDS,fbnPT,fbtnFormTExcel,fbtnFormB},
				{fbtnFormXIIIExcel},
				{fgroupingPayRollpdfreport},
				{fbnPdfPF,fbnPdfLWF,fbnPdfESIC,fbtnSalaryRegister,fbtnSalarySlip,fbtnSalRegInternal},
				{fbtnProvisionalPayrollReport,fbnPdfForm2,fbtnFormXVII},
				{fgroupingtxtreport},
				{fbtnPFtxt}
		};

		this.fields = formfield;

	}

	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		
	}

	@Override
	public void updateView(DummyEntityOnlyForScreen model) {

	}
	
	public String getPeriod() {
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MMM");
		if (dbPeriod.getValue() == null) {
			return null;
		} else {
			return format.format(dbPeriod.getValue());
		}
	}

	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(project)){
			retriveBranch();
	}
	
	if(event.getSource().equals(olbbBranch)){
		retriveProject();
	}
}

	private void retriveProject() {

		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		if(olbbBranch.getSelectedIndex()!=0){
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(olbbBranch.getValue(olbbBranch.getSelectedIndex()));
			filtervec.add(filter);
		}
		
		query.setFilters(filtervec);
		query.setQuerryObject(new HrProject());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<HrProject> list=new ArrayList<HrProject>();
				for (SuperModel model : result) {
				      HrProject entity = (HrProject) model;
				      list.add(entity);
				}
				project.setListItems(list);
			}
		});
	}

	private void retriveBranch() {
		HrProject projects =  project.getSelectedItem();
		olbbBranch.setValue(projects.getBranch());
		/**
		 * 
		 */
	

	}

	

}
