package com.slicktechnologies.client.views.humanresource.bonus;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.Bonus;

public class BonusTable extends SuperTable<Bonus> {
	TextColumn<Bonus> getCountColumn;
	TextColumn<Bonus> getNameColumn;
	TextColumn<Bonus> getRateColumn;
	TextColumn<Bonus> getStatusColumn;
	  
	public BonusTable() {
		super();
	}
	  
	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetName();
		addColumngetRateName();
		addColumngetStatus();
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<Bonus>() {
			@Override
			public Object getKey(Bonus item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	@Override
	public void addFieldUpdater() {
	}
	
	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetName();
		addSortingRateCol();
		addSortingStatus();
	}

	

	protected void addSortinggetCount() {
		List<Bonus> list = getDataprovider().getList();
		columnSort = new ListHandler<Bonus>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Bonus>() {
			@Override
			public int compare(Bonus e1, Bonus e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<Bonus>() {
			@Override
			public String getValue(Bonus object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		getCountColumn.setSortable(true);
	}

	protected void addSortinggetName() {
		List<Bonus> list = getDataprovider().getList();
		columnSort = new ListHandler<Bonus>(list);
		columnSort.setComparator(getNameColumn, new Comparator<Bonus>() {
			@Override
			public int compare(Bonus e1, Bonus e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBonusName() != null && e2.getBonusName() != null) {
						return e1.getBonusName().compareTo(e2.getBonusName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortingRateCol() {
		List<Bonus> list = getDataprovider().getList();
		columnSort = new ListHandler<Bonus>(list);
		columnSort.setComparator(getRateColumn,new Comparator<Bonus>() {
			
			@Override
			public int compare(Bonus e1, Bonus e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRate() == e2.getRate()) {
						return 0;
					}
					if (e1.getRate() > e2.getRate()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
			
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingStatus() {
		List<Bonus> list = getDataprovider().getList();
		columnSort = new ListHandler<Bonus>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Bonus>() {
			@Override
			public int compare(Bonus e1, Bonus e2) {
				if (e1 != null && e2 != null) {
					if (e1.isStatus() == true)
						return 1;
					else
						return -1;

				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetName() {
		getNameColumn = new TextColumn<Bonus>() {
			@Override
			public String getValue(Bonus object) {
				return object.getBonusName() + "";
			}
		};
		table.addColumn(getNameColumn, "Name");
		getNameColumn.setSortable(true);
	}

	

	protected void addColumngetRateName() {
		getRateColumn = new TextColumn<Bonus>() {
			@Override
			public String getValue(Bonus object) {
				return object.getRate() + "";
			}
		};
		table.addColumn(getRateColumn, "Short Name");
		getRateColumn.setSortable(true);
	}
	

	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<Bonus>() {
			@Override
			public String getValue(Bonus object) {
				if (object.isStatus() == true)
					return "Active";
				else
					return "In Active";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
	}

}
