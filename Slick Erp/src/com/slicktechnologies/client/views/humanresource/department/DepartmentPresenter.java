package com.slicktechnologies.client.views.humanresource.department;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;

import java.util.List;
import java.util.Vector;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneType;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;

import java.util.Vector;

import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.slicktechnologies.shared.common.paymentlayer.*;
import com.google.gwt.event.dom.client.*;
/**
 *  FormTableScreen presenter template
 */
public class DepartmentPresenter extends FormTableScreenPresenter<Department>  {

	//Token to set the concrete form
	 DepartmentForm form;
	
	public DepartmentPresenter (FormTableScreen<Department> view,
			Department model) {
		super(view, model);
		form=(DepartmentForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getDepartmentQuery());
		form.setPresenter(this);
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel lbl= (InlineLabel) e.getSource();
		
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
	}
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new Department();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getDepartmentQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new Department());
		return quer;
	}
	
	public void setModel(Department entity)
	{
		model=entity;
	}
	
	public static void initalize()
	{
			 DepartmentPresenterTable gentableScreen=new DepartmentPresenterTableProxy();
			 TextColumn<Department>cust=(TextColumn<Department>) gentableScreen.getTable().getColumn(0);
			 gentableScreen.getTable().setColumnWidth(cust,70,Unit.PX);
				
				TextColumn<Department>salesPerson=(TextColumn<Department>) gentableScreen.getTable().getColumn(1);
				gentableScreen.getTable().setColumnWidth(salesPerson,150.0,Unit.PX);
				
				
			DepartmentForm  form=new  DepartmentForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			  //// DepartmentPresenterTable gentableSearch=GWT.create( DepartmentPresenterTable.class);
			  ////   DepartmentPresenterSearch.staticSuperTable=gentableSearch;
			  ////     DepartmentPresenterSearch searchpopup=GWT.create( DepartmentPresenterSearch.class);
			  ////         form.setSearchpopupscreen(searchpopup);
			
			 DepartmentPresenter  presenter=new  DepartmentPresenter  (form,new Department());
			AppMemory.getAppMemory().stickPnel(form);
			
			
		 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.Department")
		 public static class  DepartmentPresenterTable extends SuperTable<Department> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.Department")
			 public static class  DepartmentPresenterSearch extends SearchPopUpScreen<  Department>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}}

			
}