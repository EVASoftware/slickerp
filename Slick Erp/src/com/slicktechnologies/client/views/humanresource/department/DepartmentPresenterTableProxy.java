package com.slicktechnologies.client.views.humanresource.department;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import java.util.Comparator;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;
import java.util.List;
import java.util.Date;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.client.views.humanresource.department.DepartmentPresenter.DepartmentPresenterTable;
import com.slicktechnologies.client.utility.Screen;

public class DepartmentPresenterTableProxy extends DepartmentPresenterTable {
	TextColumn<Department> getParentDeptNameColumn;
	TextColumn<Department> getDeptNameColumn;
	TextColumn<Department> getStatusColumn;
	TextColumn<Department> getHodNameColumn;
	TextColumn<Department> getCalendarNameColumn;
	TextColumn<Department> getCountColumn;
	public Object getVarRef(String varName)
	{
		if(varName.equals("getParentDeptNameColumn"))
			return this.getParentDeptNameColumn;
		if(varName.equals("getDeptNameColumn"))
			return this.getDeptNameColumn;
		if(varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if(varName.equals("getHodNameColumn"))
			return this.getHodNameColumn;
		if(varName.equals("getCalendarNameColumn"))
			return this.getCalendarNameColumn;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		return null ;
	}
	public DepartmentPresenterTableProxy()
	{
		super();
	}
	@Override public void createTable() {
		addColumngetCount();
		addColumngetDeptName();
		addColumngetParentDeptName();
		addColumngetHodName();
		//addColumngetCalendarName();
		addColumngetStatus();
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Department>()
				{
			@Override
			public Object getKey(Department item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetDeptName();
		addSortinggetParentDeptName();
		addSortinggetHodName();
		//addSortinggetCalendarName();
		addSortinggetStatus();
	}
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetCount()
	{
		List<Department> list=getDataprovider().getList();
		columnSort=new ListHandler<Department>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Department>()
				{
			@Override
			public int compare(Department e1,Department e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<Department>()
				{
			@Override
			public String getValue(Department object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"Id");
				getCountColumn.setSortable(true);
				table.setColumnWidth(getCountColumn,"110px");
	}
	protected void addSortinggetDeptName()
	{
		List<Department> list=getDataprovider().getList();
		columnSort=new ListHandler<Department>(list);
		columnSort.setComparator(getDeptNameColumn, new Comparator<Department>()
				{
			@Override
			public int compare(Department e1,Department e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDeptName()!=null && e2.getDeptName()!=null){
						return e1.getDeptName().compareTo(e2.getDeptName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetDeptName()
	{
		getDeptNameColumn=new TextColumn<Department>()
				{
			@Override
			public String getValue(Department object)
			{
				return object.getDeptName()+"";
			}
				};
				table.addColumn(getDeptNameColumn,"Department");
				getDeptNameColumn.setSortable(true);
				table.setColumnWidth(getDeptNameColumn,"110px");
	}
	protected void addSortinggetParentDeptName()
	{
		List<Department> list=getDataprovider().getList();
		columnSort=new ListHandler<Department>(list);
		columnSort.setComparator(getParentDeptNameColumn, new Comparator<Department>()
				{
			@Override
			public int compare(Department e1,Department e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getParentDeptName()!=null && e2.getParentDeptName()!=null){
						return e1.getParentDeptName().compareTo(e2.getParentDeptName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetParentDeptName()
	{
		getParentDeptNameColumn=new TextColumn<Department>()
				{
			@Override
			public String getValue(Department object)
			{
				return object.getParentDeptName()+"";
			}
				};
				table.addColumn(getParentDeptNameColumn,"Parent Department");
				getParentDeptNameColumn.setSortable(true);
				table.setColumnWidth(getParentDeptNameColumn,"110px");
	}
	protected void addSortinggetHodName()
	{
		List<Department> list=getDataprovider().getList();
		columnSort=new ListHandler<Department>(list);
		columnSort.setComparator(getHodNameColumn, new Comparator<Department>()
				{
			@Override
			public int compare(Department e1,Department e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getHodName()!=null && e2.getHodName()!=null){
						return e1.getHodName().compareTo(e2.getHodName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetHodName()
	{
		getHodNameColumn=new TextColumn<Department>()
				{
			@Override
			public String getValue(Department object)
			{
				return object.getHodName()+"";
			}
				};
				table.addColumn(getHodNameColumn,"H.O.D");
				getHodNameColumn.setSortable(true);
				table.setColumnWidth(getHodNameColumn,"110px");
	}
	protected void addSortinggetCalendarName()
	{
		List<Department> list=getDataprovider().getList();
		columnSort=new ListHandler<Department>(list);
		columnSort.setComparator(getCalendarNameColumn, new Comparator<Department>()
				{
			@Override
			public int compare(Department e1,Department e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCalendarName()!=null && e2.getCalendarName()!=null){
						return e1.getCalendarName().compareTo(e2.getCalendarName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCalendarName()
	{
		getCalendarNameColumn=new TextColumn<Department>()
				{
			@Override
			public String getValue(Department object)
			{
				return object.getCalendarName()+"";
			}
				};
				table.addColumn(getCalendarNameColumn,"Calendar");
				getCalendarNameColumn.setSortable(true);
	}
	protected void addSortinggetStatus()
	{
		List<Department> list=getDataprovider().getList();
		columnSort=new ListHandler<Department>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Department>()
				{
			@Override
			public int compare(Department e1,Department e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getStatus()== e2.getStatus()){
						return 0;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<Department>()
				{
			@Override
			public String getValue(Department object)
			{
				if( object.getStatus()==true)
					return "Active";
				else 
					return "In Active";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				getStatusColumn.setSortable(true);
				table.setColumnWidth(getStatusColumn,"110px");
	}
}
