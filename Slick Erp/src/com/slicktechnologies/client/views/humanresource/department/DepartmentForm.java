package com.slicktechnologies.client.views.humanresource.department;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.event.dom.client.*;
/**
 * FormTablescreen template.
 */
public class DepartmentForm extends FormTableScreen<Department>  implements ClickHandler ,ChangeHandler {

	//Token to add the varialble declarations
	public ObjectListBox<Department> lbParentDepartment;
	IntegerBox ibDepartmentId;
	TextBox lbDepartmentName;
	ObjectListBox<Employee> olbHodname;
	CheckBox fstatus;
	TextArea tbDescription;
	AddressComposite addressOnLeave;
	PhoneNumberBox pnbLandlinePhone,pnbCellPhoneNo1,pnbCellPhoneNo2;
	
	Button copyCompAddrs;
	
	//ObjectListBox<LeaveCalendar> olbCalendarName;
	/** date 9.10.2018 added by komal for hod email id **/
	TextBox tbHODEmail;


	public DepartmentForm  (SuperTable<Department> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		olbHodname.addChangeHandler(this);

	}

	private void initalizeWidget()
	{

		lbParentDepartment=new ObjectListBox<Department>();
		this.initalizeDepartMentListBox();
		lbDepartmentName=new TextBox();
		ibDepartmentId=new IntegerBox();
		ibDepartmentId.setEnabled(false);
		fstatus=new CheckBox();
		fstatus.setValue(true);

		olbHodname=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbHodname);

		tbDescription=new TextArea();
		addressOnLeave = new AddressComposite();
		pnbLandlinePhone=new PhoneNumberBox();
		pnbCellPhoneNo1=new PhoneNumberBox();
		pnbCellPhoneNo2=new PhoneNumberBox();
		//olbCalendarName = new ObjectListBox<LeaveCalendar>();
		//this.initalizeCalendarNameListBox();
		
		copyCompAddrs = new Button("Copy Company Address");
		copyCompAddrs.addClickHandler(this);
		/** date 9.10.2018 added by komal for hod email id **/
		tbHODEmail = new TextBox();

	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDepartmentInformation=fbuilder.setlabel("Department Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Department ID",ibDepartmentId);
		FormField fibDepartmentId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Parent Department",lbParentDepartment);
		FormField flbParentDepartment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Department Name ",lbDepartmentName);
		FormField flbDepartmentName= fbuilder.setMandatory(true).setMandatoryMsg("Department name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",fstatus);
		FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("H.O.D Name",olbHodname);
		FormField ftbHODName= fbuilder.setMandatory(false).setMandatoryMsg("H.O.D name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Description",tbDescription);
		FormField ftbDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAddressComposite = fbuilder.setlabel("Contact Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Landline Phone",pnbLandlinePhone);
		FormField fpnbLandlinePhone= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Cell Phone No 1",pnbCellPhoneNo1);
		FormField fpnbCellPhoneNo1= fbuilder.setMandatory(true).setMandatoryMsg("Cell Phone No 1 is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cell Phone No 2",pnbCellPhoneNo2);
		FormField fpnbCellPhoneNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",addressOnLeave);
		FormField fAddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		//fbuilder = new FormFieldBuilder("Calendar Name",olbCalendarName);
		//FormField folbCalendarName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		//   rohan added this to copy company address to dept address 
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAddressInformation=fbuilder.setlabel("Address Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",copyCompAddrs);
		FormField fcopyCompAddrs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		/** date 9.10.2018 added by komal for hod email id **/
		fbuilder = new FormFieldBuilder("Email",tbHODEmail);
		FormField tbHODEmail= fbuilder.setMandatory(true).setMandatoryMsg("Email is mandatory!").setRowSpan(0).setColSpan(0).build();
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingDepartmentInformation},
				{flbDepartmentName,flbParentDepartment,ftbHODName,fcbStatus},
				{ftbDescription},
				{fgroupingAddressComposite},
				{fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2,tbHODEmail},/** date 9.10.2018 added by komal for hod email id **/
				{fgroupingAddressInformation},
				{fcopyCompAddrs},
				{fAddressComposite},
		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Department model) 
	{

		if(lbParentDepartment.getValue()!=null)
			model.setParentDeptName(lbParentDepartment.getValue());
		if(lbDepartmentName.getValue()!=null)
			model.setDeptName(lbDepartmentName.getValue());
		if(fstatus.getValue()!=null)
			model.setStatus(fstatus.getValue());
		if(olbHodname.getValue()!=null)
			model.setHodName(olbHodname.getValue());
		if(tbDescription.getValue()!=null)
			model.setDescription(tbDescription.getValue());
		if(addressOnLeave.getValue()!=null)
			model.setAddress(addressOnLeave.getValue());
		if(pnbLandlinePhone.getValue()!=null)
			model.setLandline(pnbLandlinePhone.getValue());
		if(pnbCellPhoneNo1.getValue()!=null)
			model.setCellNo1(pnbCellPhoneNo1.getValue());
		if(pnbCellPhoneNo2.getValue()!=null)
			model.setCellNo2(pnbCellPhoneNo2.getValue());
		
		//model.setCalendar(olbCalendarName.getSelectedItem());
		//if(olbCalendarName.getValue()!=null)
			//model.setCalendarName(olbCalendarName.getValue());
		//else
			//model.setCalendarName("");
		/** date 9.10.2018 added by komal for hod email id **/
		if(tbHODEmail.getValue()!=null){
			model.setEmail(tbHODEmail.getValue());
		}

		presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Department view) 
	{
		if(view.getParentDeptName()!=null)
			lbParentDepartment.setValue(view.getParentDeptName());
		if(view.getDeptName()!=null)
			lbDepartmentName.setValue(view.getDeptName());
		if(view.getStatus()!=null)
			fstatus.setValue(view.getStatus());
		if(view.getHodName()!=null)
			olbHodname.setValue(view.getHodName());
		if(view.getDescription()!=null)
			tbDescription.setValue(view.getDescription());
		if(view.getAddress()!=null)
			addressOnLeave.setValue(view.getAddress());
		if(view.getLandline()!=null)
			pnbLandlinePhone.setValue(view.getLandline());
		if(view.getCellNo1()!=null)
			pnbCellPhoneNo1.setValue(view.getCellNo1());
		if(view.getCellNo2()!=null)
			pnbCellPhoneNo2.setValue(view.getCellNo2());
		//if(view.getCalendarName()!=null)
			//olbCalendarName.setValue(view.getCalendarName());
		/** date 9.10.2018 added by komal for hod email id **/
		if(view.getEmail()!=null){
			tbHODEmail.setValue(view.getEmail());
		}
		
		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.DEPARTMENT,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		this.ibDepartmentId.setValue(presenter.getModel().getCount());
	}


	@Override
	public void onClick(ClickEvent event) {

		if(event.getSource().equals(copyCompAddrs))
		{
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Company());
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					System.out.println(" result set size +++++++"+result.size());
					
					for(SuperModel model:result)
					{
						Company comp=(Company)model;
						addressOnLeave.clear();
						addressOnLeave.setValue(comp.getAddress());
					}
				}
			});
			
		
		}
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
		ibDepartmentId.setEnabled(false);
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

	//*****************************************Buisness Logic Part**************************************************************//

	protected void initalizeDepartMentListBox()
	{
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Department());
		lbParentDepartment.MakeLive(querry);
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(olbHodname)){
			if(olbHodname.getSelectedIndex()!=0){
				Employee emp = olbHodname.getSelectedItem();
				if(emp.getEmail()!=null){
					tbHODEmail.setValue(emp.getEmail());
				}else{
					tbHODEmail.setValue("");
				}
			}else{
				tbHODEmail.setValue("");
			}
		}
	}

	


}
