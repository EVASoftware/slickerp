package com.slicktechnologies.client.views.humanresource.employeeadditionaldetails;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;

public class EmployeeAdditionalDetailTableProxy extends SuperTable<EmployeeAdditionalDetails> {
	
	TextColumn<EmployeeAdditionalDetails> getEmpidColoumn;
	TextColumn<EmployeeAdditionalDetails> getEmpNameColoumn;
	TextColumn<EmployeeAdditionalDetails> getEmpCellColoumn;
	
	
	
	public EmployeeAdditionalDetailTableProxy() {
		super();
	}
	
	
	@Override
	public void createTable() {
		addColumnEmpId();
		addColumnEmpName();
		addColumnEmpCell();
	}

	public void addColumnSorting() {
		addSortingEmpId();
		addSortingEmpName();
		addSortingEmpCell();
	}
	
	
	
	private void addColumnEmpId() {
		getEmpidColoumn = new TextColumn<EmployeeAdditionalDetails>() {
			@Override
			public String getValue(EmployeeAdditionalDetails object) {
				return object.getEmpInfo().getEmpCount()+ "";
			}
		};
		table.addColumn(getEmpidColoumn, "Employee Id");
		table.setColumnWidth(getEmpidColoumn,110,Unit.PX);
		getEmpidColoumn.setSortable(true);
	}

	private void addColumnEmpName() {
		getEmpNameColoumn = new TextColumn<EmployeeAdditionalDetails>() {
			@Override
			public String getValue(EmployeeAdditionalDetails object) {
				return object.getEmpInfo().getFullName();
			}
		};
		table.addColumn(getEmpNameColoumn, "Employee Name");
		table.setColumnWidth(getEmpNameColoumn,110,Unit.PX);
		getEmpNameColoumn.setSortable(true);
	}
	
	private void addColumnEmpCell() {
		getEmpCellColoumn = new TextColumn<EmployeeAdditionalDetails>() {
			@Override
			public String getValue(EmployeeAdditionalDetails object) {
				return object.getEmpInfo().getCellNumber()+"";
			}
		};
		table.addColumn(getEmpCellColoumn, "Employee Cell");
		table.setColumnWidth(getEmpCellColoumn,110,Unit.PX);
		getEmpCellColoumn.setSortable(true);
	}
	
	
	private void addSortingEmpCell() {
		List<EmployeeAdditionalDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<EmployeeAdditionalDetails>(list);
		columnSort.setComparator(getEmpCellColoumn, new Comparator<EmployeeAdditionalDetails>() {
			@Override
			public int compare(EmployeeAdditionalDetails e1, EmployeeAdditionalDetails e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmpInfo().getCellNumber() == e1.getEmpInfo().getCellNumber()) {
						return 0;
					}
					if (e1.getEmpInfo().getCellNumber() > e1.getEmpInfo().getCellNumber()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}


	private void addSortingEmpId() {
		List<EmployeeAdditionalDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<EmployeeAdditionalDetails>(list);
		columnSort.setComparator(getEmpidColoumn, new Comparator<EmployeeAdditionalDetails>() {
			@Override
			public int compare(EmployeeAdditionalDetails e1, EmployeeAdditionalDetails e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmpInfo().getEmpCount() == e1.getEmpInfo().getEmpCount()) {
						return 0;
					}
					if (e1.getEmpInfo().getEmpCount() > e1.getEmpInfo().getEmpCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingEmpName() {
		List<EmployeeAdditionalDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<EmployeeAdditionalDetails>(list);
		columnSort.setComparator(getEmpNameColoumn, new Comparator<EmployeeAdditionalDetails>() {
			@Override
			public int compare(EmployeeAdditionalDetails e1, EmployeeAdditionalDetails e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmpInfo().getFullName() != null && e1.getEmpInfo().getFullName() != null) {
						return e1.getEmpInfo().getFullName().compareTo(e1.getEmpInfo().getFullName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
