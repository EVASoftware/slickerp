package com.slicktechnologies.client.views.humanresource.employeeadditionaldetails;

import java.util.Vector;

import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;

public class EmployeeAdditionalDetailsSearchProxy extends SearchPopUpScreen<EmployeeAdditionalDetails> {

	EmployeeInfoComposite personInfo;
	
	
	public EmployeeAdditionalDetailsSearchProxy() {
		super();
		createGui();
	}
	
	public void initWidget()
	{
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		this.fields=new FormField[][]{
				{fpersonInfo},
		};
	}
	
	@Override
	public MyQuerry getQuerry() {
		
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(!personInfo.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
			temp.setQuerryString("empInfo.count");
			filtervec.add(temp);
		}
		
		if(!(personInfo.getName().getValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(personInfo.getName().getValue());
			temp.setQuerryString("empInfo.fullName");
			filtervec.add(temp);
		}
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new EmployeeAdditionalDetails());
		return querry;
	}

	@Override
	public boolean validate() {
		return true;
	}

}
