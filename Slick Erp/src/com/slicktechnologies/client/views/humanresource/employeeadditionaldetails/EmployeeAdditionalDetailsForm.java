package com.slicktechnologies.client.views.humanresource.employeeadditionaldetails;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAddressDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeFamilyDeatails;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class EmployeeAdditionalDetailsForm extends FormScreen<EmployeeAdditionalDetails> implements ClickHandler  {

	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	EmployeeInfoComposite eic;
	ObjectListBox<Config> olbFamilyRelation,olbAddressType;
	Button familyButton,addressButton;
	TextBox tbFirstName,tbMiddleName,tbLastName;
	DateBox dbDateOfBirth;
	MyLongBox pnbCellPhoneNo1;
	EmployeeFamilyDeatilsTable empFamTbl;
	EmployeeAddressDetailsTable empAddTbl;
	TextArea taAddress;
	
	EmployeeAdditionalDetails empAddDetObj;
	
	/**
	 * Date : 19-09-2018 BY ANIL
	 */
	TextBox tbAadharNumber;
	TextBox tbPanNumber;
	UploadComposite uploadComp;
	
	/**
	 * @author Anil,Date : 28-01-2019
	 * Adding address information at every family member level which will be used for nomination purpose
	 */
	TextBox tbAddress;
	
	
	
	public EmployeeAdditionalDetailsForm() {
		super(FormStyle.DEFAULT);
		createGui();
	}
	
	private void initalizeWidget()
	{
		eic = AppUtility.employeeInfoComposite(new EmployeeInfo(), true);
//		eic.setEnable(false);
		
		olbFamilyRelation=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbFamilyRelation,Screen.EMPLOYEEFAMILYRELATION);
		
		tbFirstName=new TextBox();

		tbMiddleName=new TextBox();

		tbLastName=new TextBox();
		
		dbDateOfBirth=new DateBoxWithYearSelector();
		
		familyButton=new Button("Add");
		familyButton.addClickHandler(this);
		
		addressButton=new Button("Add");
		addressButton.addClickHandler(this);
		
		olbAddressType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbAddressType,Screen.ADDRESSTYPE);
		
		pnbCellPhoneNo1=new MyLongBox();
		empFamTbl=new EmployeeFamilyDeatilsTable();
		
		empAddTbl=new EmployeeAddressDetailsTable();
		
		taAddress=new TextArea();
		
		tbAadharNumber=new TextBox();
		tbPanNumber=new TextBox();
		uploadComp=new UploadComposite();
		
		tbAddress=new TextBox();
	}
	
	

	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames=new String[]{"New"};
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeInformation=fbuilder.setlabel("Employee Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", eic);
		FormField femployeeinfo = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeFamilyInformation=fbuilder.setlabel("Family Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Relation",olbFamilyRelation);
		FormField folbRelation= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("First Name",tbFirstName);
		FormField ftbFirstName= fbuilder.setMandatory(false).setMandatoryMsg("First Name is Mandatory !").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Middle Name",tbMiddleName);
		FormField ftbMiddleName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Last Name",tbLastName);
		FormField ftbLastName= fbuilder.setMandatory(false).setMandatoryMsg("Last Name is Mandatory !").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Cell Phone No",pnbCellPhoneNo1);
		FormField fpnbCellPhoneNo1= fbuilder.setMandatory(false).setMandatoryMsg("Cell Phone No 1 is Mandatory !").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Date Of Birth",dbDateOfBirth);
		FormField fdbDateOfBirth= fbuilder.setMandatory(false).setMandatoryMsg("D.O.B is mandatory !").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",familyButton);
		FormField faddFamBut= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",empFamTbl.getTable());
		FormField fempFamTbl= fbuilder.setMandatory(false).setMandatoryMsg("D.O.B is mandatory !").setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeAdressInformation=fbuilder.setlabel("Address Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("Address Type",olbAddressType);
		FormField folbAddressType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Address", taAddress);
		FormField ftaAddress = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",empAddTbl.getTable());
		FormField fempAddTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",addressButton);
		FormField faddressButton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Documents",uploadComp);
		FormField fuploadComp= fbuilder.setMandatory(false).setRowSpan(2).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Aadhar Number",tbAadharNumber);
		FormField ftbAadharNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Pan Number",tbPanNumber);
		FormField ftbPanNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Address",tbAddress);
		FormField ftbAddress= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = {
				{ fgroupingEmployeeInformation },
				{ femployeeinfo},
				{ fgroupingEmployeeFamilyInformation},
				{ folbRelation,ftbFirstName,ftbMiddleName,ftbLastName},
				{ fpnbCellPhoneNo1,fdbDateOfBirth,ftbAadharNumber,ftbPanNumber},
				{ ftbAddress,faddFamBut},
				{ fempFamTbl},
				{ fuploadComp},
				{ fgroupingEmployeeAdressInformation},
				{ folbAddressType},{ftaAddress,faddressButton},{fempAddTbl},
		};
		this.fields = formfield;
		
		
	}

	@Override
	public void updateModel(EmployeeAdditionalDetails model) {
		if(eic!=null){
			model.setEmpInfo(eic.getValue());
		}
		if(empAddTbl.getValue()!=null){
			model.setEmpAddDetList(empAddTbl.getValue());
		}
		if(empFamTbl.getValue()!=null){
			model.setEmpFamDetList(empFamTbl.getValue());
		}
		if(uploadComp.getValue()!=null){
			model.setDocumentUpload(uploadComp.getValue());
		}else{
			model.setDocumentUpload(new DocumentUpload());
		}
		
		empAddDetObj=model;
	}

	@Override
	public void updateView(EmployeeAdditionalDetails view) {
		
		empAddDetObj=view;
		Console.log("inside updateView: "+view.getEmpInfo().getEmpCount() +" "+ view.getEmpInfo());
		if(view.getEmpInfo()!=null){
			eic.setValue(view.getEmpInfo());
			eic.getId().setValue(view.getEmpInfo().getEmpCount()+"");
			Console.log("inside updateView: "+eic.getId().getValue());
		}
		if(view.getEmpAddDetList()!=null){
			empAddTbl.setValue(view.getEmpAddDetList());
		}
		if(view.getEmpFamDetList()!=null){
			empFamTbl.setValue(view.getEmpFamDetList());
		}
		
		if(view.getDocumentUpload()!=null){
			uploadComp.setValue(view.getDocumentUpload());
		}
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			/**
			 * Updated By: Viraj
			 * Date: 04-06-2019
			 * Description: To add and save data from employee screen directly to employeeAdditionalDetails
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			} else {
				menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			/** Ends **/
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			/**
			 * Updated By: Viraj
			 * Date: 04-06-2019
			 * Description: To add and save data from employee screen directly to employeeAdditionalDetails
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			} else {
				menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			/** Ends **/
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.contains(AppConstants.NAVIGATION)){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			/**
			 * Updated By: Viraj
			 * Date: 04-06-2019
			 * Description: To add and save data from employee screen directly to employeeAdditionalDetails
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			} else {
				menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			/** Ends **/
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.contains(AppConstants.NAVIGATION)){
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false);
				}
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.EMPLOYEEADDITIONALDETAILS,LoginPresenter.currentModule.trim());
	}

	@Override
	public void onClick(ClickEvent event) {

		if(event.getSource().equals(familyButton)){
			System.out.println("Inside Adding Family Details..!!!");
			if(validFamilyDetails()){
				EmployeeFamilyDeatails empFam=new EmployeeFamilyDeatails();
				empFam.setFamilyRelation(olbFamilyRelation.getValue());
				empFam.setFname(tbFirstName.getValue());
				empFam.setMname(tbMiddleName.getValue());
				empFam.setLname(tbLastName.getValue());
				empFam.setCellNo(pnbCellPhoneNo1.getValue());
				empFam.setDob(dbDateOfBirth.getValue());
				/**
				 * Date : 19-09-2018 By ANIL
				 */
				empFam.setAadharNumber(tbAadharNumber.getValue());
				empFam.setPanNumber(tbPanNumber.getValue());
				/**
				 * End
				 */
				empFam.setAddress(tbAddress.getValue());
				
				empFamTbl.getDataprovider().getList().add(empFam);
				
				olbFamilyRelation.setSelectedIndex(0);
				tbFirstName.setValue(null);
				tbMiddleName.setValue(null);
				tbLastName.setValue(null);
				pnbCellPhoneNo1.setValue(null);
				dbDateOfBirth.setValue(null);
				tbAadharNumber.setValue(null);
				tbPanNumber.setValue(null);
				tbAddress.setValue(null);
			}
//			else{
//				this.showDialogMessage("Please fill family details!");
//			}
		}
		
		if(event.getSource().equals(addressButton)){
			System.out.println("Inside Adding Address Details..!!!");
			if(validAddress()){
				EmployeeAddressDetails empAdd=new EmployeeAddressDetails();
				empAdd.setAddressType(olbAddressType.getValue());
				empAdd.setFullAddress(taAddress.getValue());
				empAddTbl.getDataprovider().getList().add(empAdd);
				
				olbAddressType.setSelectedIndex(0);
				taAddress.setValue(null);
			}else{
				this.showDialogMessage("Please fill address details!");
			}
			
		}
	}
	
	public boolean validAddress(){
		if(olbAddressType.getValue()!=null&&taAddress.getValue()!=null){
			return true;
		}
		return false;
	}
	
	public boolean validFamilyDetails(){
//		if(olbFamilyRelation.getValue()!=null
//				&&tbFirstName.getValue()!=null
//				&&tbMiddleName.getValue()!=null
//				&&tbLastName.getValue()!=null
//				&&pnbCellPhoneNo1.getValue()!=null
//				&&dbDateOfBirth.getValue()!=null){
//			return true;
//		}
		String msg="";
		if(olbFamilyRelation.getSelectedIndex()==0){
			msg=msg+"Please select family relation."+"\n";
		}
		if(tbFirstName.getValue()==null||tbFirstName.getValue().equals("")){
			msg=msg+"Please fill first name."+"\n";
		}
//		if(tbMiddleName.getValue()==null||tbMiddleName.getValue().equals("")){
//			msg=msg+"Please fill middle name."+"\n";
//		}
//		if(tbLastName.getValue()==null||tbLastName.getValue().equals("")){
//			msg=msg+"Please fill last name."+"\n";
//		}
//		if(pnbCellPhoneNo1.getValue()==null){
//			msg=msg+"Please fill cell number."+"\n";
//		}
		if(dbDateOfBirth.getValue()==null){
			msg=msg+"Please select date of birth."+"\n";
		}
		/**
		 * Date : 19-09-2018 BY ANIL
		 * As per the Sasha Baljinder instruction 
		 * This is the change request.
		 */
//		if(tbAadharNumber.getValue()==null||tbAadharNumber.getValue().equals("")){
//			msg=msg+"Please fill aadhar number."+"\n";
//		}
//		
//		if(tbPanNumber.getValue()==null||tbPanNumber.getValue().equals("")){
//			msg=msg+"Please fill pan number.";
//		}
		
		/**
		 * End
		 */
		if(!msg.equals("")){
			showDialogMessage(msg);
			return false;
		}
		
		
		return true;
	}

	@Override
	public boolean validate() {
		boolean valid=super.validate();
		if(valid==false){
			return false;
		}
		if(empFamTbl.getDataprovider().getList().size()==0){
			this.showDialogMessage("Please add family details!");
			return false;
		}
//		if(empAddTbl.getDataprovider().getList().size()==0){
//			this.showDialogMessage("Please add address details!");
//			return false;
//		}
		return true;
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.eic.setEnable(false);
		this.processLevelBar.setVisibleFalse(false);
	}
	
	@Override
	public void setToViewState() {
		super.setToViewState();

		SuperModel model=new EmployeeAdditionalDetails();
		model=empAddDetObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.HRMODULE,AppConstants.EMPLOYEEADDITIONALDETAILS, empAddDetObj.getCount(), empAddDetObj.getEmpInfo().getCount(),empAddDetObj.getEmpInfo().getFullName(),empAddDetObj.getEmpInfo().getCellNumber(), false, model, null);
	
	}
	
	
	public void retriveEmployeeDetails() {
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = new Filter();
		filter.setQuerryString("empInfo.count");
		filter.setIntValue(Integer.parseInt(eic.getId().getValue()));
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new EmployeeAdditionalDetails());

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							showDialogMessage("Employee Details Already Exist!");
							eic.clear();
						}
					}
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("No Employee Found");
					}
				});
	}
	
	/**
	 * Date : 09-05-2017 By ANIL
	 * 
	 */
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		empAddTbl.setEnable(state);
		empFamTbl.setEnable(state);
	}
	/**
	 * End
	 */

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		
		empFamTbl.getTable().redraw();
		empAddTbl.getTable().redraw();
	}
	
	

}
