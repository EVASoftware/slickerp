package com.slicktechnologies.client.views.humanresource.employeeadditionaldetails;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeFamilyDeatails;
import com.slicktechnologies.shared.common.personlayer.EmployeeProjectHistory;

public class EmployeeFamilyDeatilsTable extends
		SuperTable<EmployeeFamilyDeatails> {

	TextColumn<EmployeeFamilyDeatails> getColumnEmpRel;
	TextColumn<EmployeeFamilyDeatails> getColumnEmpfname;
	TextColumn<EmployeeFamilyDeatails> getColumnEmpMname;
	TextColumn<EmployeeFamilyDeatails> getColumnEmpLname;
	TextColumn<EmployeeFamilyDeatails> getColumnEmpCellNo;
	TextColumn<EmployeeFamilyDeatails> getColumnEmpDob;
	
	TextColumn<EmployeeFamilyDeatails> getColumnAadharNumber;
	TextColumn<EmployeeFamilyDeatails> getColumnPanNumber;
	
	/**
	 * @author Anil,Date : 28-01-2019
	 * added column address
	 */
	TextColumn<EmployeeFamilyDeatails> getColumnAddress;
	/**Date 10-1-2019 by Amol**/
    Column<EmployeeFamilyDeatails,String>aadharNumber;
	Column<EmployeeFamilyDeatails,String>panNumber;
	Column<EmployeeFamilyDeatails,String>address;
	
	
	Column<EmployeeFamilyDeatails, String> deleteColumn;

	public EmployeeFamilyDeatilsTable(){
		table.setWidth("100%");
		table.setHeight("350px");
	}
	
	
	
	@Override
	public void createTable() {
		getColumnEmplRel();
		getColumnEmplfname();
		getColumnEmpMname();
		getColumnEmpLname();
		getColumnEmpCellNo();
		getColumnEmpDob();
		getColumnAadharNumber();
		getColumnPanNumber();
		getColumnAddress();
		
		createColumndeleteColumn();
		addFieldUpdater();

	}

	private void getColumnAddress() {
		getColumnAddress = new TextColumn<EmployeeFamilyDeatails>() {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getAddress() != null)
					return object.getAddress();
				else
					return "";
			}
		};
		table.addColumn(getColumnAddress, "Address");
		getColumnAddress.setSortable(true);
		
	}

	private void getColumnAadharNumber() {
		getColumnAadharNumber = new TextColumn<EmployeeFamilyDeatails>() {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getAadharNumber() != null)
					return object.getAadharNumber();
				else
					return "";
			}
		};
		table.addColumn(getColumnAadharNumber, "Aadhar Number");
		getColumnAadharNumber.setSortable(true);
	}

	private void getColumnPanNumber() {
		getColumnPanNumber = new TextColumn<EmployeeFamilyDeatails>() {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getPanNumber() != null)
					return object.getPanNumber();
				else
					return "";
			}
		};
		table.addColumn(getColumnPanNumber, "Pan Number");
		getColumnPanNumber.setSortable(true);
	}

	private void getColumnEmplRel() {
		getColumnEmpRel = new TextColumn<EmployeeFamilyDeatails>() {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getFamilyRelation() != null)
					return object.getFamilyRelation();
				else
					return "";
			}
		};
		table.addColumn(getColumnEmpRel, "Relation");
		getColumnEmpRel.setSortable(true);
	}

	private void getColumnEmplfname() {
		getColumnEmpfname = new TextColumn<EmployeeFamilyDeatails>() {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getFname() != null)
					return object.getFname();
				else
					return "";
			}
		};
		table.addColumn(getColumnEmpfname, "First Name");
		getColumnEmpfname.setSortable(true);
	}

	private void getColumnEmpMname() {

		getColumnEmpMname = new TextColumn<EmployeeFamilyDeatails>() {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getMname() != null)
					return object.getMname();
				else
					return "";
			}
		};
		table.addColumn(getColumnEmpMname, "Middle Name");
		getColumnEmpMname.setSortable(true);

	}

	private void getColumnEmpLname() {
		getColumnEmpLname = new TextColumn<EmployeeFamilyDeatails>() {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getLname() != null)
					return object.getLname();
				else
					return "";
			}
		};
		table.addColumn(getColumnEmpLname, "Last Name");
		getColumnEmpLname.setSortable(true);
	}

	private void getColumnEmpCellNo() {
		getColumnEmpCellNo = new TextColumn<EmployeeFamilyDeatails>() {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getCellNo() != null)
					return object.getCellNo() + "";
				else
					return "";
			}
		};
		table.addColumn(getColumnEmpCellNo, "Cell No.");
		getColumnEmpCellNo.setSortable(true);
	}

	private void getColumnEmpDob() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
		getColumnEmpDob = new TextColumn<EmployeeFamilyDeatails>() {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getDob() != null)
					return fmt.format(object.getDob()) + "";
				else
					return "";
			}
		};
		table.addColumn(getColumnEmpDob, "DOB");
		getColumnEmpDob.setSortable(true);
	}

	protected void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<EmployeeFamilyDeatails, String>(btnCell) {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
	}

	protected void createFieldUpdaterdeleteColumn() {
		deleteColumn.setFieldUpdater(new FieldUpdater<EmployeeFamilyDeatails, String>() {
			@Override
			public void update(int index,EmployeeFamilyDeatails object, String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}

	@Override
	protected void initializekeyprovider() {

	}

	@Override
	public void addFieldUpdater() {
		if(deleteColumn!=null)
			createFieldUpdaterdeleteColumn();
	}

	@Override
	public void setEnable(boolean state) {
		for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
	        	  
	      
	          if(state==true)
	          {
	        	getColumnEmplRel();
	      		getColumnEmplfname();
	      		getColumnEmpMname();
	      		getColumnEmpLname();
	      		getColumnEmpCellNo();
	      		getColumnEmpDob();
//	      		getColumnAadharNumber();
//	      		getColumnPanNumber();
//	      		getColumnAddress();
	      		/**date 2-10-2019 by Amol added edited column**/
	      		getAadharNumber();
	      		getPanNumber();
	      		getAddress();
	      		createColumndeleteColumn();
	      		
	      		addFieldUpdater();
	      		}
	 
	          else
	          {
	        	getColumnEmplRel();
	      		getColumnEmplfname();
	      		getColumnEmpMname();
	      		getColumnEmpLname();
	      		getColumnEmpCellNo();
	      		getColumnEmpDob();
	      		getColumnAadharNumber();
	      		getColumnPanNumber();
	      		getColumnAddress();
	      	
	          }
	}

	private void getAddress() {
		EditTextCell editCell=new EditTextCell();
		address=new Column<EmployeeFamilyDeatails,String>(editCell)
				{
			@Override
			public String getValue(EmployeeFamilyDeatails object)
			{
				if(object.getAddress()==null){
					return 0+"";}
				else{
					return object.getAddress()+"";
				}
			}
				};
				table.addColumn(address,"#Address");
				table.setColumnWidth(address, 100,Unit.PX);
				address.setFieldUpdater(new FieldUpdater<EmployeeFamilyDeatails, String>() {
					@Override
					public void update(int index, EmployeeFamilyDeatails object,String value) {
						// TODO Auto-generated method stub
						if(value!=null){
							object.setAddress(value);
							table.redraw();
						}
					}
				});
	}

	private void getPanNumber() {
		EditTextCell editCell=new EditTextCell();
		panNumber=new Column<EmployeeFamilyDeatails,String>(editCell)
				{
			@Override
			public String getValue(EmployeeFamilyDeatails object)
			{
				if(object.getPanNumber()==null){
					return 0+"";}
				else{
					return object.getPanNumber()+"";
				}
			}
				};
				table.addColumn(panNumber,"#Pan Number");
				table.setColumnWidth(panNumber, 100,Unit.PX);
				panNumber.setFieldUpdater(new FieldUpdater<EmployeeFamilyDeatails, String>() {
					@Override
					public void update(int index, EmployeeFamilyDeatails object,String value) {
						// TODO Auto-generated method stub
						if(value!=null){
							object.setPanNumber(value);
							table.redraw();
						}
					}
				});
	}

	private void getAadharNumber() {
		EditTextCell editCell=new EditTextCell();
		aadharNumber=new Column<EmployeeFamilyDeatails,String>(editCell)
				{
			@Override
			public String getValue(EmployeeFamilyDeatails object)
			{
				if(object.getAadharNumber()==null){
					return 0+"";}
				else{
					return object.getAadharNumber()+"";
				}
			}
				};
				table.addColumn(aadharNumber,"#Aadhar Number");
				table.setColumnWidth(aadharNumber, 100,Unit.PX);
				aadharNumber.setFieldUpdater(new FieldUpdater<EmployeeFamilyDeatails, String>() {
					@Override
					public void update(int index, EmployeeFamilyDeatails object,String value) {
						// TODO Auto-generated method stub
						if(value!=null){
							object.setAadharNumber(value);
							table.redraw();
						}
					}
				});
	}
	
	

	@Override
	public void applyStyle() {

	}

}
