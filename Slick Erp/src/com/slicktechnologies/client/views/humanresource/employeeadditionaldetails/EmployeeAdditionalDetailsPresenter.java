package com.slicktechnologies.client.views.humanresource.employeeadditionaldetails;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

public class EmployeeAdditionalDetailsPresenter extends FormScreenPresenter<EmployeeAdditionalDetails> implements SelectionHandler<Suggestion> {

	EmployeeAdditionalDetailsForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	public EmployeeAdditionalDetailsPresenter(UiScreen<EmployeeAdditionalDetails> view,EmployeeAdditionalDetails model) {
		super(view, model);
		form = (EmployeeAdditionalDetailsForm) view;
		
		form.eic.getId().addSelectionHandler(this);
		form.eic.getName().addSelectionHandler(this);
		form.eic.getPhone().addSelectionHandler(this);
		
		boolean isDownload = AuthorizationHelper.getDownloadAuthorization(Screen.EMPLOYEEADDITIONALDETAILS, LoginPresenter.currentModule.trim());
		if (isDownload == false) {
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
	}
	
	public static EmployeeAdditionalDetailsForm initalize() {

		EmployeeAdditionalDetailsForm form = new EmployeeAdditionalDetailsForm();

		EmployeeAdditionalDetailTableProxy gentableSearch = new EmployeeAdditionalDetailTableProxy();
		gentableSearch.setView(form);
		gentableSearch.applySelectionModle();
		EmployeeAdditionalDetailsSearchProxy.staticSuperTable = gentableSearch;
		EmployeeAdditionalDetailsSearchProxy searchpopup = new EmployeeAdditionalDetailsSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		EmployeeAdditionalDetailsPresenter presenter = new EmployeeAdditionalDetailsPresenter(form,new EmployeeAdditionalDetails());
		AppMemory.getAppMemory().stickPnel(form);
		return form;

	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
			InlineLabel lbl= (InlineLabel) e.getSource();
			
			if(lbl.getText().contains("New"))
			{
				form.setToNewState();
				initalize();
		}
		}
	

	@Override
	public void reactOnDownload() {
		ArrayList<EmployeeAdditionalDetails> woArray = new ArrayList<EmployeeAdditionalDetails>();
		List<EmployeeAdditionalDetails> list = (List<EmployeeAdditionalDetails>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		woArray.addAll(list);
		csvservice.setEmpAdditionDetails(woArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 93;
				Window.open(url, "test", "enabled");
			}
		});
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if (event.getSource().equals(form.eic.getId())||event.getSource().equals(form.eic.getName())||event.getSource().equals(form.eic.getPhone())) {
			form.retriveEmployeeDetails();
		}		
	}


	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}
	
	

}
