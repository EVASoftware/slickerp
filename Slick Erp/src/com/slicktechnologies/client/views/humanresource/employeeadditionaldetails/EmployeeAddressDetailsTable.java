package com.slicktechnologies.client.views.humanresource.employeeadditionaldetails;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAddressDetails;

public class EmployeeAddressDetailsTable extends SuperTable<EmployeeAddressDetails> {

	TextColumn<EmployeeAddressDetails> getColumnEmpAddType;
	TextColumn<EmployeeAddressDetails> getColumnEmpFullAdd;
	
	Column<EmployeeAddressDetails, String> deleteColumn;
	
	@Override
	public void createTable() {
		getColumnEmpAddType();
		getColumnEmpFullAdd();
		createColumndeleteColumn();
		addFieldUpdater();
	}

	private void getColumnEmpAddType() {
		getColumnEmpAddType = new TextColumn<EmployeeAddressDetails>() {
			@Override
			public String getValue(EmployeeAddressDetails object) {
				if (object.getAddressType()!= null)
					return object.getAddressType() + "";
				else
					return "";
			}
		};
		table.addColumn(getColumnEmpAddType, "Address Type");
		getColumnEmpAddType.setSortable(true);
	}

	private void getColumnEmpFullAdd() {
		getColumnEmpFullAdd = new TextColumn<EmployeeAddressDetails>() {
			@Override
			public String getValue(EmployeeAddressDetails object) {
				if (object.getFullAddress()!= null)
					return object.getFullAddress() + "";
				else
					return "";
			}
		};
		table.addColumn(getColumnEmpFullAdd, "Full Address");
		getColumnEmpFullAdd.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	protected void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<EmployeeAddressDetails, String>(btnCell) {
			@Override
			public String getValue(EmployeeAddressDetails object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
	}

	protected void createFieldUpdaterdeleteColumn() {
		deleteColumn.setFieldUpdater(new FieldUpdater<EmployeeAddressDetails, String>() {
			@Override
			public void update(int index,EmployeeAddressDetails object, String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	@Override
	public void addFieldUpdater() {
		if(deleteColumn!=null)
			createFieldUpdaterdeleteColumn();
	}

	/**
	 * Date : 09-05-2017 By Anil
	 */
	@Override
	public void setEnable(boolean state) {
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);

		if (state == true) {
			getColumnEmpAddType();
			getColumnEmpFullAdd();
			createColumndeleteColumn();
			addFieldUpdater();
		}else {
			getColumnEmpAddType();
			getColumnEmpFullAdd();
		}
	}
	
	/*
	 * End 
	 */

	@Override
	public void applyStyle() {
		
	}

}
