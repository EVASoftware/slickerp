package com.slicktechnologies.client.views.humanresource.employeegroup;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.Grade;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.event.dom.client.*;
/**
 * FormTablescreen template.
 */
public class EmployeeGroupForm extends FormTableScreen<EmployeeGroup>  implements ClickHandler {

	//Token to add the varialble declarations
  CheckBox cbStatus;
  TextBox tbGradeName,tbGradeCode;
  TextArea taDescription;
  EmployeeInformationTable table;
  Button button;
  ObjectListBox<Employee>olbEmployee;
  
  
	
	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;
	
	
	
	public EmployeeGroupForm  (SuperTable<EmployeeGroup> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		
	}
	
	private void initalizeWidget()
	{
		
	cbStatus=new CheckBox();
	cbStatus.setValue(true);
	tbGradeName=new TextBox();
	tbGradeCode=new TextBox();
	taDescription=new TextArea();
	table=new EmployeeInformationTable();
	button=new Button("Add Employees");
	button.addClickHandler(this);
	olbEmployee=new ObjectListBox<Employee>();
	AppUtility.makeSalesPersonListBoxLive(olbEmployee);
	}

	/*
	 * Method template to create the formtable screen
	 */
	
	@Override
	public void createScreen() {
		
		//Token to initialize the processlevel menus.
	    initalizeWidget();
		
		
		
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////
	    
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
fbuilder = new FormFieldBuilder();
FormField fgroupingGradeInformation=fbuilder.setlabel("Group Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
fbuilder = new FormFieldBuilder("* Group Name",tbGradeName);
FormField ftbGradeName= fbuilder.setMandatory(true).setMandatoryMsg("Grade Name is Mandatroy!").setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("* Group Code",tbGradeCode);
FormField ftbGradeCode= fbuilder.setMandatory(true).setMandatoryMsg("Grade Code is Mandatory!").setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("Description",taDescription);
FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
fbuilder = new FormFieldBuilder("Status",cbStatus);
FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

fbuilder = new FormFieldBuilder("",table.getTable());
FormField ftable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
FormField fgroupingemployee=fbuilder.setlabel("Add Employees").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();

fbuilder = new FormFieldBuilder("",olbEmployee);
FormField femployee= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
fbuilder = new FormFieldBuilder("",button);
FormField fbutton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();





		
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		
		
		FormField[][] formfield = {   {fgroupingGradeInformation},
  {ftbGradeName,ftbGradeCode,fcbStatus},
  {ftaDescription},
  {fgroupingemployee},
  {femployee,fbutton},
  {ftable}
  };

		
        this.fields=formfield;		
   }

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(EmployeeGroup model) 
	{
		
	if(tbGradeName.getValue()!=null)
	  model.setGroupName(tbGradeName.getValue());
	if(tbGradeCode.getValue()!=null)
		model.setGroupcode(tbGradeCode.getValue().trim());
	if(taDescription.getValue()!=null)
	  model.setDescription(taDescription.getValue());
	if(cbStatus.getValue()!=null)
	  model.setStatus(cbStatus.getValue());
	if(table.getValue()!=null)
		model.setemployees(table.getValue());
		
	presenter.setModel(model);
		
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(EmployeeGroup view) 
	{
	if(view.getGroupName()!=null)
	  tbGradeName.setValue(view.getGroupName());
	if(view.getGroupcode()!=null)
		tbGradeCode.setValue(view.getGroupcode());
	if(view.getDescription()!=null)
	  taDescription.setValue(view.getDescription());
	if(view.getStatus()!=null)
	  cbStatus.setValue(view.getStatus());
	if(view.getemployees()!=null)
		table.setValue(view.getemployees());
  
	presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.EMPLOYEEGROUP,LoginPresenter.currentModule.trim());
	}
	
	

	
	@Override
	public void onClick(ClickEvent event) {
		//Patch method for now will do will have to change 
		if(this.olbEmployee.getSelectedIndex()!=0)
		{
			Employee emp=olbEmployee.getSelectedItem();
			EmployeeInfo info=new EmployeeInfo();
			info.setCount(emp.getCount());
			info.setCellNumber(emp.getCellNumber1());
			info.setFullName(emp.getFullname());
			info.setBranch(emp.getBranchName());
			info.setDepartment(emp.getDepartMent());
			info.setDesignation(emp.getDesignation());
			info.setEmployeerole(emp.getRoleName());
			info.setEmployeeType(emp.getEmployeeType());
			table.getListDataProvider().getList().add(info);
			
		}
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
		
	}

	@Override
	public void clear() {
		
		super.clear();
		cbStatus.setValue(true);
		table.clear();
	}

}
