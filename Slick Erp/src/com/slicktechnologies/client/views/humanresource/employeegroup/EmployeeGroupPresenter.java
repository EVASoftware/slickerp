package com.slicktechnologies.client.views.humanresource.employeegroup;
import com.google.gwt.event.dom.client.ClickEvent;

import java.util.Vector;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeGroup;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
/**
 *  FormTableScreen presenter template
 */
public class EmployeeGroupPresenter extends FormTableScreenPresenter<EmployeeGroup>  {

	//Token to set the concrete form
	EmployeeGroupForm form;
	
	public EmployeeGroupPresenter (FormTableScreen<EmployeeGroup> view,
			EmployeeGroup model) {
		super(view, model);
		form=(EmployeeGroupForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getEmployeeGroupQuery());
		form.setPresenter(this);
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals(""))
			  reactTo();

		
	}
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new EmployeeGroup();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getEmployeeGroupQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new EmployeeGroup());
		return quer;
	}
	
	public void setModel(EmployeeGroup entity)
	{
		model=entity;
	}
	
	public static void initalize()
	{
			 EmployeeGroupPresenterTable gentableScreen=GWT.create( EmployeeGroupPresenterTable.class);
			
			EmployeeGroupForm  form=new  EmployeeGroupForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			  //// EmployeeGroupPresenterTable gentableSearch=GWT.create( EmployeeGroupPresenterTable.class);
			  ////   EmployeeGroupPresenterSearch.staticSuperTable=gentableSearch;
			  ////     EmployeeGroupPresenterSearch searchpopup=GWT.create( EmployeeGroupPresenterSearch.class);
			  ////         form.setSearchpopupscreen(searchpopup);
			
			 EmployeeGroupPresenter  presenter=new  EmployeeGroupPresenter  (form,new EmployeeGroup());
			AppMemory.getAppMemory().stickPnel(form);
			
			
		 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.EmployeeGroup")
		 public static class  EmployeeGroupPresenterTable extends SuperTable<EmployeeGroup> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.EmployeeGroup")
			 public static class  EmployeeGroupPresenterSearch extends SearchPopUpScreen<  EmployeeGroup>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
				
				  private void reactTo()
		  {
		}

}
