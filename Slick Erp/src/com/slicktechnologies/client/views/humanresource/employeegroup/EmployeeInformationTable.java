package com.slicktechnologies.client.views.humanresource.employeegroup;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;

public class EmployeeInformationTable extends SuperTable<EmployeeInfo> {

	TextColumn<EmployeeInfo> Id;
	TextColumn<EmployeeInfo> employeeName;
	TextColumn<EmployeeInfo> employeeCell;
	TextColumn<EmployeeInfo> employeeBranch;
	TextColumn<EmployeeInfo> employeeDepartment;
	TextColumn<EmployeeInfo> employeeType;
	TextColumn<EmployeeInfo> employeeDesignation;
	TextColumn<EmployeeInfo> employeeRole;
	
	
	
	private Column<EmployeeInfo, String> delete;
	
	
	public EmployeeInformationTable() 
	{
		super();
	}
	
	
	@Override
	public void createTable() {
		addColumnId();
		addColumnEmployeeName();
		addColumnEmployeeCell();
		addColumnEmployeeBranch();
		addColumnEmployeeDepartment();
		addColumnEmployeeDesignation();
		
		addColumnEmployeeType();
		addColumnEmployeeRole();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	    addColumnSorting();
	}
	
	

	private void addeditColumn()
	{
		addColumnId();
		addColumnEmployeeName();
		addColumnEmployeeCell();
		addColumnEmployeeBranch();
		addColumnEmployeeDepartment();
		addColumnEmployeeDesignation();
		
		addColumnEmployeeType();
		addColumnEmployeeRole();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	}
	
	private void addViewColumn()
	{
		addColumnId();
		addColumnEmployeeName();
		addColumnEmployeeCell();
		addColumnEmployeeBranch();
		addColumnEmployeeDepartment();
		addColumnEmployeeDesignation();
		
		addColumnEmployeeType();
		addColumnEmployeeRole();
		addColumnEmployeeCell();
	}
	
	
	public void addColumnId()
	{
		Id = new TextColumn<EmployeeInfo>() {
			@Override
			public String getValue(EmployeeInfo object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(Id,"ID");
	}
	
	public void addColumnEmployeeName()
	{
		employeeName = new TextColumn<EmployeeInfo>() {
			@Override
			public String getValue(EmployeeInfo object) {
				
				return object.getFullName()+"";
			}
		};
		table.addColumn(employeeName,"Name");
	}
	
	public void addColumnEmployeeCell()
	{
		employeeCell = new TextColumn<EmployeeInfo>() {
			@Override
			public String getValue(EmployeeInfo object) {
				return object.getCellNumber()+"";
			}
		};
		table.addColumn(employeeCell,"Cell Number");
	}

	  private void addColumnDelete()
		{
			ButtonCell btnCell= new ButtonCell();
			delete = new Column<EmployeeInfo, String>(btnCell) {

				@Override
				public String getValue(EmployeeInfo object) {
					
					return "Delete";
				}
			};
			table.addColumn(delete,"Delete");
		}
	  
	  void addColumnEmployeeBranch()
	  {
		  employeeBranch=new TextColumn<EmployeeInfo>() {

			@Override
			public String getValue(EmployeeInfo object) {
				// TODO Auto-generated method stub
				return object.getBranch()+"";
			}
		};
		
		table.addColumn(employeeBranch,"Branch");
	  }
	  
	  void addColumnEmployeeDepartment()
	  {
		  employeeDepartment=new TextColumn<EmployeeInfo>() {

				@Override
				public String getValue(EmployeeInfo object) {
					// TODO Auto-generated method stub
					return object.getDepartment()+"";
				}
			};
			table.addColumn(employeeDepartment,"Department");
	  }

	 void addColumnEmployeeDesignation()
	 {
		 employeeDesignation=new TextColumn<EmployeeInfo>() {

				@Override
				public String getValue(EmployeeInfo object) {
					// TODO Auto-generated method stub
					return object.getDesignation()+"";
				}
			};
			table.addColumn(employeeDesignation,"Designation");
	 }
	
		void addColumnEmployeeType()
		{
			employeeType=new TextColumn<EmployeeInfo>() {

				@Override
				public String getValue(EmployeeInfo object) {
					// TODO Auto-generated method stub
					return object.getEmployeeType();
				}
			};
			table.addColumn(employeeType,"Type");

		}
		void addColumnEmployeeRole()
		{
			employeeRole=new TextColumn<EmployeeInfo>() {

				@Override
				public String getValue(EmployeeInfo object) {
					// TODO Auto-generated method stub
					return object.getEmployeerole();
				}
			};
			table.addColumn(employeeRole,"Role");
		}
	  
		private void setFieldUpdaterOnDelete()
		{
			delete.setFieldUpdater(new FieldUpdater<EmployeeInfo, String>() {
				
				@Override
				public void update(int index, EmployeeInfo object, String value) {
					getDataprovider().getList().remove(object);
				
				}
			});
		}

	
	
		public  void setEnabled(boolean state)
		{
			int tablecolcount=this.table.getColumnCount();
			for(int i=tablecolcount-1;i>-1;i--)
			  table.removeColumn(i);
			if(state ==true)
				addeditColumn();
			if(state==false)
				addViewColumn();
		}

	

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<EmployeeInfo>() {
			@Override
			public Object getKey(EmployeeInfo item) {
				if(item==null)
					return null;
				else
					return item.getCount();
			}
		};
	
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
	}
	
	

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	
	protected void addSortinggetBranch()
	{
		List<EmployeeInfo> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeInfo>(list);
		columnSort.setComparator(employeeBranch, new Comparator<EmployeeInfo>()
				{
			@Override
			public int compare(EmployeeInfo e1,EmployeeInfo e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggeName()
	{
		List<EmployeeInfo> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeInfo>(list);
		columnSort.setComparator(employeeName, new Comparator<EmployeeInfo>()
				{
			@Override
			public int compare(EmployeeInfo e1,EmployeeInfo e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getFullName().compareTo(e2.getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggeType()
	{
		List<EmployeeInfo> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeInfo>(list);
		columnSort.setComparator(employeeType, new Comparator<EmployeeInfo>()
				{
			@Override
			public int compare(EmployeeInfo e1,EmployeeInfo e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getEmployeeType().compareTo(e2.getEmployeeType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggeDepartment()
	{
		List<EmployeeInfo> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeInfo>(list);
		columnSort.setComparator(employeeDepartment, new Comparator<EmployeeInfo>()
				{
			@Override
			public int compare(EmployeeInfo e1,EmployeeInfo e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getDepartment().compareTo(e2.getDepartment());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggeDesignation()
	{
		List<EmployeeInfo> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeInfo>(list);
		columnSort.setComparator(employeeDesignation, new Comparator<EmployeeInfo>()
				{
			@Override
			public int compare(EmployeeInfo e1,EmployeeInfo e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getDesignation().compareTo(e2.getDesignation());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggRole()
	{
		List<EmployeeInfo> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeInfo>(list);
		columnSort.setComparator(employeeRole, new Comparator<EmployeeInfo>()
				{
			@Override
			public int compare(EmployeeInfo e1,EmployeeInfo e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getEmployeerole().compareTo(e2.getEmployeerole());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCellNumber()
	{
		List<EmployeeInfo> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeInfo>(list);
		columnSort.setComparator(employeeCell, new Comparator<EmployeeInfo>()
				{
			@Override
			public int compare(EmployeeInfo e1,EmployeeInfo e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCellNumber()== e2.getCellNumber()){
						return 0;}
					if(e1.getCellNumber()> e2.getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCount()
	  {
	  List<EmployeeInfo> list=getDataprovider().getList();
	  columnSort=new ListHandler<EmployeeInfo>(list);
	  columnSort.setComparator(Id, new Comparator<EmployeeInfo>()
	  {
	  @Override
	  public int compare(EmployeeInfo e1,EmployeeInfo e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if(e1.getCount()== e2.getCount()){
	  return 0;}
	  if(e1.getCount()> e2.getCount()){
	  return 1;}
	  else{
	  return -1;}
	  }
	  else{
	  return 0;}
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  }
	
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetBranch();
		addSortinggeDepartment();
		addSortinggeDesignation();
		addSortinggeType();
		addSortinggRole();
		addSortinggeName();
		addSortinggetCellNumber();
		
	}

}
