package com.slicktechnologies.client.views.humanresource.voluntarypf;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPfHistory;

public class VoluntaryPfHistoryTable extends SuperTable<VoluntaryPfHistory> {

	TextColumn<VoluntaryPfHistory> getColVolPfCount;
	TextColumn<VoluntaryPfHistory> getColVolPfEmpId;
	TextColumn<VoluntaryPfHistory> getColVolPfEmpName;
	TextColumn<VoluntaryPfHistory> getColVolPfPayrollMonth;
	TextColumn<VoluntaryPfHistory> getColVolPfName;
	TextColumn<VoluntaryPfHistory> getColVolPfPercentage;
	TextColumn<VoluntaryPfHistory> getColVolPfWageAmount;
	TextColumn<VoluntaryPfHistory> getColVolPfAmount;
	
	/**6-12-2018 branch added by amol**/
	TextColumn<VoluntaryPfHistory> getColBranch;
	
	/**12-1-2019 added by amol for project name***/
	TextColumn<VoluntaryPfHistory>getColProject;
	
	public VoluntaryPfHistoryTable() {
		// TODO Auto-generated constructor stub
		super();
	}
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		getColVolPfCount();
		getColVolPfEmpId();
		getColVolPfEmpName();
		getColVolPfPayrollMonth();
		getColVolPfName();
		getColVolPfPercentage();
		getColVolPfWageAmount();
		getColVolPfAmount();
		getColBranch();
		getColProject();
	}

	private void getColProject() {
		// TODO Auto-generated method stub
		getColProject=new TextColumn<VoluntaryPfHistory>() {
			@Override
			public String getValue(VoluntaryPfHistory object) {
				// TODO Auto-generated method stub
				if(object.getProjectName()!=null){
				return object.getProjectName()+"";
				}else{
					return "";
				}
			
			}
			
		};
		table.addColumn(getColProject,"Project Name");
		getColProject.setSortable(true);
	}
	private void getColBranch() {
		// TODO Auto-generated method stub
		getColBranch=new TextColumn<VoluntaryPfHistory>() {
			@Override
			public String getValue(VoluntaryPfHistory object) {
				// TODO Auto-generated method stub
				return object.getBranchName()+"";
			}
			
		};
		table.addColumn(getColBranch, "Branch");
		getColBranch.setSortable(true);
	}
	private void getColVolPfName() {
		// TODO Auto-generated method stub
		getColVolPfName=new TextColumn<VoluntaryPfHistory>() {
			
			@Override
			public String getValue(VoluntaryPfHistory object) {
				// TODO Auto-generated method stub
				return object.getName()+"";
			}
		};
		table.addColumn(getColVolPfName, "Name");
		getColVolPfName.setSortable(true);
	}
	private void getColVolPfCount() {
		// TODO Auto-generated method stub
		getColVolPfCount=new TextColumn<VoluntaryPfHistory>() {
			
			@Override
			public String getValue(VoluntaryPfHistory object) {
				// TODO Auto-generated method stub
				return object.getCount()+"";
			}
		};
		table.addColumn(getColVolPfCount, "Id");
		getColVolPfCount.setSortable(true);
	}
	private void getColVolPfEmpId() {
		// TODO Auto-generated method stub
		getColVolPfEmpId=new TextColumn<VoluntaryPfHistory>() {
			
			@Override
			public String getValue(VoluntaryPfHistory object) {
				// TODO Auto-generated method stub
				return object.getEmpId()+"";
			}
		};
		table.addColumn(getColVolPfEmpId, "Employee Id");
		getColVolPfEmpId.setSortable(true);
		
	}
	private void getColVolPfEmpName() {
		// TODO Auto-generated method stub
		getColVolPfEmpName=new TextColumn<VoluntaryPfHistory>() {
			
			@Override
			public String getValue(VoluntaryPfHistory object) {
				// TODO Auto-generated method stub
				return object.getEmpName()+"";
			}
		};
		table.addColumn(getColVolPfEmpName, "Employee Name");
		getColVolPfEmpName.setSortable(true);
		
	}
	private void getColVolPfPayrollMonth() {
		// TODO Auto-generated method stub
		getColVolPfPayrollMonth=new TextColumn<VoluntaryPfHistory>() {
			
			@Override
			public String getValue(VoluntaryPfHistory object) {
				// TODO Auto-generated method stub
				return object.getPayrollMonth()+"";
			}
		};
		table.addColumn(getColVolPfPayrollMonth, "Payroll Month");
		getColVolPfPayrollMonth.setSortable(true);
	}
	private void getColVolPfPercentage() {
		// TODO Auto-generated method stub
		getColVolPfPercentage=new TextColumn<VoluntaryPfHistory>() {
			
			@Override
			public String getValue(VoluntaryPfHistory object) {
				// TODO Auto-generated method stub
				return object.getVoluntaryPfPercentage()+"";
			}
		};
		table.addColumn(getColVolPfPercentage, "Voluntary PF Percentage");
		getColVolPfPercentage.setSortable(true);
	}
	private void getColVolPfWageAmount() {
		// TODO Auto-generated method stub
		getColVolPfWageAmount=new TextColumn<VoluntaryPfHistory>() {
			
			@Override
			public String getValue(VoluntaryPfHistory object) {
				// TODO Auto-generated method stub
				return object.getVoluntaryPfwageAmount()+"";
			}
		};
		table.addColumn(getColVolPfWageAmount, "PF Wage Amount");
		getColVolPfWageAmount.setSortable(true);
	}
	private void getColVolPfAmount() {
		// TODO Auto-generated method stub
		getColVolPfAmount=new TextColumn<VoluntaryPfHistory>() {
			
			@Override
			public String getValue(VoluntaryPfHistory object) {
				// TODO Auto-generated method stub
				return object.getVoluntaryPfAmount()+"";
			}
		};
		table.addColumn(getColVolPfAmount, "PF Amount");
		getColVolPfAmount.setSortable(true);
	}
	
@Override
	public void addColumnSorting() {
		// TODO Auto-generated method stub
		super.addColumnSorting();
		addSortingOnVolPfCount();
		addSortingOnVolPfEmpId();
		addSortingOnVolPfEmpName();
		addSortingOnVolPfPayrollMonth();
		addSortingOnVolPfName();
		addSortingOnVolPfPercentage();
		addSortingOnVolPfWageAmount();
		addSortingOnVolPfAmount();
		addSortingOngetColBranch();
		addSortingOngetColProject();
		
	}
	private void addSortingOngetColProject() {
		// TODO Auto-generated method stub
		List<VoluntaryPfHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPfHistory>(list);
		columnSort.setComparator(getColProject, new Comparator<VoluntaryPfHistory>()
		{
			@Override
			public int compare(VoluntaryPfHistory e1,VoluntaryPfHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProjectName()!=null && e2.getProjectName()!=null){
						return e1.getProjectName().compareTo(e2.getProjectName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortingOngetColBranch() {
		// TODO Auto-generated method stub
		List<VoluntaryPfHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPfHistory>(list);
		columnSort.setComparator(getColBranch, new Comparator<VoluntaryPfHistory>()
		{
			@Override
			public int compare(VoluntaryPfHistory e1,VoluntaryPfHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranchName()!=null && e2.getBranchName()!=null){
						return e1.getBranchName().compareTo(e2.getBranchName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
		
	
	private void addSortingOnVolPfName() {
		// TODO Auto-generated method stub
		List<VoluntaryPfHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPfHistory>(list);
		columnSort.setComparator(getColVolPfName, new Comparator<VoluntaryPfHistory>()
		{
			@Override
			public int compare(VoluntaryPfHistory e1,VoluntaryPfHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getName()!=null && e2.getName()!=null){
						return e1.getName().compareTo(e2.getName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortingOnVolPfCount() {
		// TODO Auto-generated method stub
		List<VoluntaryPfHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPfHistory>(list);
		columnSort.setComparator(getColVolPfCount, new Comparator<VoluntaryPfHistory>()
		{
			@Override
			public int compare(VoluntaryPfHistory e1,VoluntaryPfHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortingOnVolPfEmpId() {
		// TODO Auto-generated method stub
		List<VoluntaryPfHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPfHistory>(list);
		columnSort.setComparator(getColVolPfEmpId, new Comparator<VoluntaryPfHistory>()
		{
			@Override
			public int compare(VoluntaryPfHistory e1,VoluntaryPfHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getEmpId()== e2.getEmpId()){
						return 0;}
					if(e1.getEmpId()> e2.getEmpId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}
	private void addSortingOnVolPfEmpName() {
		// TODO Auto-generated method stub
		List<VoluntaryPfHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPfHistory>(list);
		columnSort.setComparator(getColVolPfEmpName, new Comparator<VoluntaryPfHistory>()
		{
			@Override
			public int compare(VoluntaryPfHistory e1,VoluntaryPfHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmpName()!=null && e2.getEmpName()!=null){
						return e1.getEmpName().compareTo(e2.getEmpName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortingOnVolPfPayrollMonth() {
		// TODO Auto-generated method stub
		List<VoluntaryPfHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPfHistory>(list);
		columnSort.setComparator(getColVolPfPayrollMonth, new Comparator<VoluntaryPfHistory>()
		{
			@Override
			public int compare(VoluntaryPfHistory e1,VoluntaryPfHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPayrollMonth()!=null && e2.getPayrollMonth()!=null){
						return e1.getPayrollMonth().compareTo(e2.getPayrollMonth());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortingOnVolPfPercentage() {
		// TODO Auto-generated method stub
		List<VoluntaryPfHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPfHistory>(list);
		columnSort.setComparator(getColVolPfPercentage, new Comparator<VoluntaryPfHistory>()
		{
			@Override
			public int compare(VoluntaryPfHistory e1,VoluntaryPfHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getVoluntaryPfPercentage()== e2.getVoluntaryPfPercentage()){
						return 0;}
					if(e1.getVoluntaryPfPercentage()> e2.getVoluntaryPfPercentage()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortingOnVolPfWageAmount() {
		// TODO Auto-generated method stub
		List<VoluntaryPfHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPfHistory>(list);
		columnSort.setComparator(getColVolPfWageAmount, new Comparator<VoluntaryPfHistory>()
		{
			@Override
			public int compare(VoluntaryPfHistory e1,VoluntaryPfHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getVoluntaryPfwageAmount()== e2.getVoluntaryPfwageAmount()){
						return 0;}
					if(e1.getVoluntaryPfwageAmount()> e2.getVoluntaryPfwageAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	private void addSortingOnVolPfAmount() {
		// TODO Auto-generated method stub
		List<VoluntaryPfHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPfHistory>(list);
		columnSort.setComparator(getColVolPfAmount, new Comparator<VoluntaryPfHistory>()
		{
			@Override
			public int compare(VoluntaryPfHistory e1,VoluntaryPfHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getVoluntaryPfAmount()== e2.getVoluntaryPfAmount()){
						return 0;}
					if(e1.getVoluntaryPfAmount()> e2.getVoluntaryPfAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
