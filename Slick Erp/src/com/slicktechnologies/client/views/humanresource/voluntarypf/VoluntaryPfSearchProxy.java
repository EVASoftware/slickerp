package com.slicktechnologies.client.views.humanresource.voluntarypf;

import java.util.Vector;

import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPf;

public class VoluntaryPfSearchProxy extends SearchPopUpScreen<VoluntaryPf>{
	
	EmployeeInfoComposite personInfo;
	
	/**6-12-2018 added by amol for branch dropdown**/
	ObjectListBox<Branch> lbBranch;
	
	/***12-1-2019 added by amol for project dropdown**/
	ObjectListBox<HrProject>lbProject;
	
	public VoluntaryPfSearchProxy() {
		super();
		getPopup().setHeight("300px");
		createGui();
	}
	public void initWidget()
	{
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
		lbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(lbBranch);
		
		lbProject = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(lbProject);
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
	
		builder=new FormFieldBuilder("Branch",lbBranch);
        FormField flbBranch=builder.setRowSpan(0).setColSpan(0).build();		
        
    	builder =new FormFieldBuilder("Project Name",lbProject);
		FormField flbProject=builder.setMandatory(true).setMandatoryMsg("Project Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		

		this.fields=new FormField[][]{
				{fpersonInfo,flbBranch,
					flbProject},
				
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
			if(!personInfo.getId().getValue().equals(""))
			{  
				temp=new Filter();
				temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
				temp.setQuerryString("empId");
				filtervec.add(temp);
			}

			if(!(personInfo.getName().getValue().equals("")))
			{
				temp=new Filter();
				temp.setStringValue(personInfo.getName().getValue());
				temp.setQuerryString("empName");
				filtervec.add(temp);
			}
			if(!personInfo.getPhone().getValue().equals(""))
			{
				temp=new Filter();
				temp.setLongValue(personInfo.getCellNumber());
				temp.setQuerryString("empCellNo");
				filtervec.add(temp);
			}
			if(lbBranch.getSelectedIndex()!=0)
			{
				temp=new Filter();
				temp.setStringValue(lbBranch.getValue().trim());
				temp.setQuerryString("branchName");
				filtervec.add(temp);
			}
			
			if(lbProject.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(lbProject.getValue().trim());
				temp.setQuerryString("projectName");
				filtervec.add(temp);
			}
			
			
			
			
			
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new VoluntaryPf());
		return querry;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

}
