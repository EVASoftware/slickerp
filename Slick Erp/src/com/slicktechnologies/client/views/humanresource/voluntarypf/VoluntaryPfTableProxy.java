package com.slicktechnologies.client.views.humanresource.voluntarypf;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPf;

public class VoluntaryPfTableProxy extends SuperTable<VoluntaryPf> {
	
	TextColumn<VoluntaryPf> getColEmpId;
	TextColumn<VoluntaryPf> getColEmpName;
	TextColumn<VoluntaryPf> getColEmpCell;
	
	/**6-12-2018 added by amol for branch column**/
	TextColumn<VoluntaryPf> getColBranch;
	/**12-1-2019 added by amol for project name**/
	TextColumn<VoluntaryPf>getColProject;
	
	
	public VoluntaryPfTableProxy() {
		// TODO Auto-generated constructor stub
		super();
	}

	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		getColEmpId();
		getColEmpName();
		getColEmpCell();
		getColBranch();
		getColProject();
	}

	private void getColProject() {
		// TODO Auto-generated method stub
		getColProject=new TextColumn<VoluntaryPf>(){
			public String getValue(VoluntaryPf object) {
				// TODO Auto-generated method stub
				return object.getProjectName()+"";
				}
				};
				table.addColumn(getColProject, "Project Name");
				getColBranch.setSortable(true);
		}
		
	
	private void getColBranch() {
		// TODO Auto-generated method stub
		getColBranch=new TextColumn<VoluntaryPf>(){
			@Override
			public String getValue(VoluntaryPf object) {
				// TODO Auto-generated method stub
				return object.getBranchName()+"";
				}
				};
				table.addColumn(getColBranch, "Branch");
				getColBranch.setSortable(true);
	}
	private void getColEmpId() {
		// TODO Auto-generated method stub
		getColEmpId=new TextColumn<VoluntaryPf>() {
			
			@Override
			public String getValue(VoluntaryPf object) {
				// TODO Auto-generated method stub
				return object.getEmpId()+"";
			}
		};
		table.addColumn(getColEmpId, "Employee Id");
		getColEmpId.setSortable(true);
	}

	private void getColEmpName() {
		// TODO Auto-generated method stub
		getColEmpName=new TextColumn<VoluntaryPf>() {
			
			@Override
			public String getValue(VoluntaryPf object) {
				// TODO Auto-generated method stub
				return object.getEmpName()+"";
			}
		};
		table.addColumn(getColEmpName, "Employee Name");
		getColEmpName.setSortable(true);
	}

	private void getColEmpCell() {
		// TODO Auto-generated method stub
		getColEmpCell=new TextColumn<VoluntaryPf>() {
			
			@Override
			public String getValue(VoluntaryPf object) {
				// TODO Auto-generated method stub
				return object.getEmpCellNo()+"";
			}
		};
		table.addColumn(getColEmpCell, "Employee Cell");
		getColEmpCell.setSortable(true);
	}
	
	

	@Override
	public void addColumnSorting() {
		// TODO Auto-generated method stub
		super.addColumnSorting();
		addSortingOnEmpIdCol();
		addSortingOnEmpNameCol();
		addSortingOnEmpCellCol();
		addSortingOngetColBranch();
		addSortingOngetColProject();
	}

	private void addSortingOngetColProject() {
		// TODO Auto-generated method stub
		List<VoluntaryPf> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPf>(list);
		columnSort.setComparator(getColProject, new Comparator<VoluntaryPf>()
		{
			@Override
			public int compare(VoluntaryPf e1,VoluntaryPf e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProjectName()!=null && e2.getProjectName()!=null){
						return e1.getProjectName().compareTo(e2.getProjectName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOngetColBranch() {
		// TODO Auto-generated method stub
		List<VoluntaryPf> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPf>(list);
		columnSort.setComparator(getColBranch, new Comparator<VoluntaryPf>()
		{
			@Override
			public int compare(VoluntaryPf e1,VoluntaryPf e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranchName()!=null && e2.getBranchName()!=null){
						return e1.getBranchName().compareTo(e2.getBranchName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	

	private void addSortingOnEmpIdCol() {
		// TODO Auto-generated method stub
		List<VoluntaryPf> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPf>(list);
		columnSort.setComparator(getColEmpId, new Comparator<VoluntaryPf>()
		{
			@Override
			public int compare(VoluntaryPf e1,VoluntaryPf e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getEmpId()== e2.getEmpId()){
						return 0;}
					if(e1.getEmpId()> e2.getEmpId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnEmpNameCol() {
		// TODO Auto-generated method stub
		List<VoluntaryPf> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPf>(list);
		columnSort.setComparator(getColEmpName, new Comparator<VoluntaryPf>()
		{
			@Override
			public int compare(VoluntaryPf e1,VoluntaryPf e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmpName()!=null && e2.getEmpName()!=null){
						return e1.getEmpName().compareTo(e2.getEmpName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnEmpCellCol() {
		// TODO Auto-generated method stub
		List<VoluntaryPf> list=getDataprovider().getList();
		columnSort=new ListHandler<VoluntaryPf>(list);
		columnSort.setComparator(getColEmpCell, new Comparator<VoluntaryPf>()
		{
			@Override
			public int compare(VoluntaryPf e1,VoluntaryPf e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getEmpCellNo()== e2.getEmpCellNo()){
						return 0;}
					if(e1.getEmpCellNo()> e2.getEmpCellNo()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
