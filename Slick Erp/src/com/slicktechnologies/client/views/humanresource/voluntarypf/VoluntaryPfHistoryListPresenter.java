package com.slicktechnologies.client.views.humanresource.voluntarypf;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsSearchPopUp;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListForm;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListTable;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPfHistory;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class VoluntaryPfHistoryListPresenter extends TableScreenPresenter<VoluntaryPfHistory> {

	TableScreen<VoluntaryPfHistory>form;
	CsvServiceAsync csvservice = GWT.create(CsvService.class);

	public VoluntaryPfHistoryListPresenter(TableScreen<VoluntaryPfHistory> view, VoluntaryPfHistory model) {
		super(view, model);
		form=view;
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.VOLUNTARYPFHISTORY,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	public static void initialize(){
		VoluntaryPfHistoryTable table=new VoluntaryPfHistoryTable();
		VoluntaryPfHistoryListForm form=new VoluntaryPfHistoryListForm(table);
		VoluntaryPfHistorySearchProxy.staticSuperTable=table;
		VoluntaryPfHistorySearchProxy searchPopUp=new VoluntaryPfHistorySearchProxy(false);
		form.setSearchpopupscreen(searchPopUp);
		VoluntaryPfHistoryListPresenter presenter=new VoluntaryPfHistoryListPresenter(form, new VoluntaryPfHistory());
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}
	public void reactOnDownload() {
		ArrayList<VoluntaryPfHistory> voluntrypfhistoryArray = new ArrayList<VoluntaryPfHistory>();
		List<VoluntaryPfHistory> list = (List<VoluntaryPfHistory>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		voluntrypfhistoryArray.addAll(list);
		csvservice.setVoluntrypfhistorylist(voluntrypfhistoryArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 162;
				Window.open(url, "test", "enabled");
			}
		});
	}
	


}
