package com.slicktechnologies.client.views.humanresource.voluntarypf;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPf;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;

public class VoluntaryPfPresenter extends FormScreenPresenter<VoluntaryPf> implements SelectionHandler<Suggestion> {

	VoluntaryPfForm form;
	GenricServiceAsync service = GWT.create(GenricService.class);
	CsvServiceAsync csvservice = GWT.create(CsvService.class);

	public VoluntaryPfPresenter(UiScreen<VoluntaryPf> view, VoluntaryPf model) {
		super(view, model);
		form=(VoluntaryPfForm) view;
		form.setPresenter(this);
//		form.getSearchpopupscreen().getPopup().setSize("100%", "100%");
		
		form.pic.getId().addSelectionHandler(this);
		form.pic.getName().addSelectionHandler(this);
		form.pic.getPhone().addSelectionHandler(this);
	}
	
	public static void initialize(){
		VoluntaryPfForm form=new VoluntaryPfForm();
		VoluntaryPfTableProxy genTable=new VoluntaryPfTableProxy();
		genTable.setView(form);
		genTable.applySelectionModle();
		VoluntaryPfSearchProxy.staticSuperTable=genTable;
		VoluntaryPfSearchProxy search=new VoluntaryPfSearchProxy();
		form.setSearchpopupscreen(search);
		VoluntaryPfPresenter presenter=new VoluntaryPfPresenter(form, new VoluntaryPf());
		AppMemory.getAppMemory().stickPnel(form);
//		return form;
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		if(text.equals(AppConstants.NEW)){
			reactToNew();
		}
	}

	private void reactToNew() {
		form.setToNewState();
		this.initialize();
		form.toggleAppHeaderBarMenu();
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		model=new VoluntaryPf();
	}
	
	

	@Override
	public void setModel(VoluntaryPf model) {
		// TODO Auto-generated method stub
		super.setModel(model);
		this.model=model;
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		// TODO Auto-generated method stub
		if (event.getSource().equals(form.pic.getId())||event.getSource().equals(form.pic.getName())||event.getSource().equals(form.pic.getPhone())) {
			EmployeeInfo info=form.pic.getValue();
			if(info!=null&&info.getBranch()!=null){
				form.lbBranch.setValue(info.getBranch());
			}
			checkPfEntry();
		}
	}
	
	public void checkPfEntry() {

		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = new Filter();
		
		filter.setQuerryString("empId");
		filter.setIntValue(form.pic.getEmployeeId());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new VoluntaryPf());

		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					form.showDialogMessage("Employee Voluntary Pf Details Already Exist!");
					form.pic.clear();
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("No Record Found");
			}
		});
	
	}
	public void reactOnDownload() {
		ArrayList<VoluntaryPf> voluntrypfArray = new ArrayList<VoluntaryPf>();
		List<VoluntaryPf> list = (List<VoluntaryPf>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		voluntrypfArray.addAll(list);
		csvservice.setVoluntrypflist(voluntrypfArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 161;
				Window.open(url, "test", "enabled");
			}
		});

}
}
