package com.slicktechnologies.client.views.humanresource.voluntarypf;

import java.util.Vector;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPfHistory;

public class VoluntaryPfHistorySearchProxy extends SearchPopUpScreen<VoluntaryPfHistory> {

	EmployeeInfoComposite personInfo;
	DateBox dbPayrollMonth;
	ObjectListBox<Branch> OlbBranch;
	ObjectListBox<HrProject>Project;
	
	public VoluntaryPfHistorySearchProxy(boolean b) {
		// TODO Auto-generated constructor stub
		super(b);
		getPopup().setHeight("300px");
		createGui();
//		dbPayrollMonth.getElement().getStyle().setWidth(120,Unit.PX);
		DateTimeFormat dateFormat=DateTimeFormat.getFormat("yyyy-MMM");
		dbPayrollMonth.setFormat(new DateBox.DefaultFormat(dateFormat));
	}
	public void initWidget()
	{
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee Id");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
		
		dbPayrollMonth=new DateBoxWithYearSelector();
		DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MMM");
		DateBox.DefaultFormat defformat=new DateBox.DefaultFormat(format);
		dbPayrollMonth.setFormat(defformat);
		
		OlbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(OlbBranch);
		
		Project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(Project);
//				
	
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
	
		builder = new FormFieldBuilder("Payroll Period",dbPayrollMonth);
		FormField fdbPayrollMonth= builder.setMandatory(false).setRowSpan(1).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Branch",OlbBranch);
		FormField fOlbBranch= builder.setMandatory(false).setRowSpan(1).setColSpan(0).build();

		builder = new FormFieldBuilder("Project Name",Project);
		FormField fProject= builder.setMandatory(false).setRowSpan(1).setColSpan(0).build();

		this.fields=new FormField[][]{
				{fpersonInfo},
				{fdbPayrollMonth,fOlbBranch,fProject}
				
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(!personInfo.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
			temp.setQuerryString("empId");
			filtervec.add(temp);
		}

		if(!(personInfo.getName().getValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(personInfo.getName().getValue());
			temp.setQuerryString("empName");
			filtervec.add(temp);
		}
		if(!personInfo.getPhone().getValue().equals(""))
		{
			temp=new Filter();
			temp.setLongValue(personInfo.getCellNumber());
			temp.setQuerryString("empCellNo");
			filtervec.add(temp);
		}
		
		if(getPayRollPeriod()!=null){
			temp=new Filter();
			temp.setStringValue(getPayRollPeriod());
			temp.setQuerryString("payrollMonth");
			filtervec.add(temp);
		}
		
		if(OlbBranch.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(OlbBranch.getValue().trim());
			temp.setQuerryString("branchName");
			filtervec.add(temp);
		}
		
		if(Project.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(Project.getValue().trim());
			temp.setQuerryString("projectName");
			filtervec.add(temp);
		}
//		
		
		
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new VoluntaryPfHistory());
		return querry;
	}
	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

	public String getPayRollPeriod(){
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MMM");
		if (dbPayrollMonth.getValue() == null) {
			return null;
		} else {
			return format.format(dbPayrollMonth.getValue());
		}
	}
}
