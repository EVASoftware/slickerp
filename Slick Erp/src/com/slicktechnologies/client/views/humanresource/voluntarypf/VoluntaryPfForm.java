package com.slicktechnologies.client.views.humanresource.voluntarypf;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.arrears.ArrearsDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPf;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class VoluntaryPfForm extends FormScreen<VoluntaryPf> {
	
	EmployeeInfoComposite pic;
	DateBox dbFromDate;
	DateBox dbToDate;
	DoubleBox dblAmount;
	CheckBox cbStatus;
	TextBox tbVolPfName;
	
	/**added by amol for branchdropdown**/
	ObjectListBox<Branch> lbBranch;
	
	/**12-1-2019 @author Amol
	 added a  project branchlist  dropdown ***/
	ObjectListBox<HrProject>lbProject;
	VoluntaryPf voluntaryPf;
	
	
	public VoluntaryPfForm() {
		super();
		createGui();
	}
	
	
	private void initalizeWidget() {
		pic = AppUtility.employeeInfoComposite(new EmployeeInfo(), false);
		dbFromDate = new DateBoxWithYearSelector();
		dbToDate = new DateBoxWithYearSelector();
		dblAmount = new DoubleBox();
		cbStatus=new CheckBox();
		cbStatus.setValue(true);
		tbVolPfName=new TextBox();
		lbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(lbBranch);
	
		lbProject = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(lbProject);
		
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION) || text.equals("Search"))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false); 
				}
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Search")||text.equals("Print")||text.equals(AppConstants.NAVIGATION)||text.contains("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.VOLUNTARYPF,LoginPresenter.currentModule.trim());
	}

	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames=new String[]{AppConstants.NEW};
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingVolPf=fbuilder.setlabel("Voluntary PF Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",pic);
		FormField fpic= fbuilder.setMandatory(true).setMandatoryMsg("Employee information is Mandatory").setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* From Date", dbFromDate);
		FormField fdbFromDate= fbuilder.setRowSpan(0).setMandatory(true).setMandatoryMsg("From Date is Mandatory.").setColSpan(0).build();

		fbuilder = new FormFieldBuilder("To Date", dbToDate);
		FormField fdbToDate= fbuilder.setRowSpan(0).setMandatory(false).setMandatoryMsg("").setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Voluntary PF Percentage", dblAmount);
		FormField fdblAmount= fbuilder.setRowSpan(0).setMandatory(true).setMandatoryMsg("Voluntary PF Percentage is Mandatory.").setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", cbStatus);
		FormField fcbStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Name", tbVolPfName);
		FormField ftbVolPfName= fbuilder.setRowSpan(0).setMandatory(true).setMandatoryMsg("Name is Mandatory.").setColSpan(0).build();
				
		
		fbuilder=new FormFieldBuilder("* Branch",lbBranch);
        FormField flbBranch=fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(0).build();		
		
		fbuilder =new FormFieldBuilder("Project Name",lbProject);
		FormField flbProject=fbuilder.setMandatory(true).setMandatoryMsg("Project Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = {   
				{fgroupingVolPf},
				{fpic},
				{fdblAmount ,fdbFromDate ,fdbToDate ,fcbStatus},
				{ftbVolPfName,flbBranch,flbProject}
				
		};

		this.fields=formfield;		

	}



	@Override
	public void updateModel(VoluntaryPf model) {
		if(pic.getValue()!=null){
			model.setEmpId(pic.getEmployeeId());
			model.setEmpName(pic.getEmployeeName());
			model.setEmpCellNo(pic.getCellNumber());
		}
		if(dblAmount.getValue()!=null){
			model.setVoluntaryPfPercentage(dblAmount.getValue());
		}else{
			model.setVoluntaryPfPercentage(0);
		}
		if(dbFromDate.getValue()!=null){
			model.setApplicableFromDate(dbFromDate.getValue());
		}else{
			model.setApplicableFromDate(null);
		}
		if(dbToDate.getValue()!=null){
			model.setApplicableToDate(dbToDate.getValue());
		}else{
			model.setApplicableToDate(null);
		}
		if(cbStatus.getValue()!=null){
			model.setStatus(cbStatus.getValue());
		}else{
			model.setStatus(false);
		}
		
		if(tbVolPfName.getValue()!=null){
			model.setName(tbVolPfName.getValue());
		}
		voluntaryPf=model;
		presenter.setModel(model);
		
		if(lbBranch.getValue()!=null){
			model.setBranchName(lbBranch.getValue());
		}
		if(lbProject.getValue()!=null){
		model.setProjectName(lbProject.getValue());
		}
	}

	@Override
	public void updateView(VoluntaryPf view) {
		voluntaryPf=view;
		if(view.getEmpId()!=0){
			pic.setEmployeeId(view.getEmpId());
			pic.setEmployeeName(view.getEmpName());
			pic.setCellNumber(view.getEmpCellNo());
		}
		if(view.getVoluntaryPfPercentage()!=0){
			dblAmount.setValue(view.getVoluntaryPfPercentage());
		}
		if(view.getApplicableFromDate()!=null){
			dbFromDate.setValue(view.getApplicableFromDate());
		}
		if(view.getApplicableToDate()!=null){
			dbToDate.setValue(view.getApplicableToDate());
		}
		cbStatus.setValue(view.isStatus());
		
		if(view.getName()!=null){
			tbVolPfName.setValue(view.getName());
		}
		
		/**added by amol for branch**/
		
		if(view.getBranchName()!=null)
			lbBranch.setValue(view.getBranchName());
		
		if(view.getProjectName()!=null){
			lbProject.setValue(view.getProjectName());
		}
		
		presenter.setModel(view);
		
		
	}


	@Override
	public void clear() {
		// TODO Auto-generated method stub
		super.clear();
		cbStatus.setValue(true);
		
		
		
		
		
	}

	

}
