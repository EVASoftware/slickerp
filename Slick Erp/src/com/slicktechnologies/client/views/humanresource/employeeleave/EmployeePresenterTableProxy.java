package com.slicktechnologies.client.views.humanresource.employeeleave;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.client.views.settings.employee.EmployeePresenter.EmployeePresenterTable;
import com.slicktechnologies.client.utility.Screen;

public class EmployeePresenterTableProxy extends SuperTable<LeaveBalance> {
	TextColumn<LeaveBalance> getCountColumn;
	TextColumn<LeaveBalance> getFullnameColumn;
	TextColumn<LeaveBalance> getCellNumber1Column;
	TextColumn<LeaveBalance> getBranchNameColumn;
	TextColumn<LeaveBalance> getDepartmentColumn;
	TextColumn<LeaveBalance> getDesignationColumn;
	TextColumn<LeaveBalance> getEmployeeTypeColumn;
	TextColumn<LeaveBalance> isStatusColumn;
	TextColumn<LeaveBalance> getEmployeeRoleColumn;
	TextColumn<LeaveBalance> calendarColumn;
	TextColumn<LeaveBalance> leavegroupColumn;


	public Object getVarRef(String varName)
	{
		if(varName.equals("isStatusColumn"))
			return this.isStatusColumn;
		if(varName.equals("getBranchNameColumn"))
			return this.getBranchNameColumn;
		if(varName.equals("getDesignationColumn"))
			return this.getDesignationColumn;
		if(varName.equals("getCellNumber1Column"))
			return this.getCellNumber1Column;
		if(varName.equals("getFullnameColumn"))
			return this.getFullnameColumn;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		return null ;
	}
	public EmployeePresenterTableProxy()
	{
		super();
	}
	@Override public void createTable() {
		addColumngetCount();
		addColumngetFullname();
		addColumngetCellNumber1();
		addColumngetBranchName();
		addColumngetDesignation();
		addColumngetDepartment();
		addColumngetEmploymentType();
		createRoleColumn();
		createCalendarcolumn();
		createLeaveGroupColumn();
	}
	private void createLeaveGroupColumn() {

		leavegroupColumn=new TextColumn<LeaveBalance>()
				{
			@Override
			public String getValue(LeaveBalance object)
			{
				return object.getLeaveGroup().getGroupName();
			}
				};
				table.addColumn(leavegroupColumn,"Leave Group");
				leavegroupColumn.setSortable(true);
	
	}
	private void createCalendarcolumn() {

		calendarColumn=new TextColumn<LeaveBalance>()
				{
			@Override
			public String getValue(LeaveBalance object)
			{
				return object.getCalendarName();
			}
				};
				table.addColumn(calendarColumn,"Calendar");
				calendarColumn.setSortable(true);
	
	}
	private void createRoleColumn() {

		getEmployeeRoleColumn=new TextColumn<LeaveBalance>()
				{
			@Override
			public String getValue(LeaveBalance object)
			{
				return object.getEmployeerole();
			}
				};
				table.addColumn(getEmployeeRoleColumn,"Employee Role");
				table.setColumnWidth(getEmployeeRoleColumn, "140px");
				getEmployeeRoleColumn.setSortable(true);
	
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<LeaveBalance>()
				{
			@Override
			public Object getKey(LeaveBalance item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetFullname();
		addSortinggetCellNumber1();
		addSortinggetBranchName();
		addSortinggetDesignation();
		createCalendarSorting();
		createleaveGroupSorting();
		createRoleSorting();
		addSortingDepartment();
		addSortingEmploymentType();
	}
	private void createleaveGroupSorting() {
		
		List<LeaveBalance> list=getDataprovider().getList();
		columnSort=new ListHandler<LeaveBalance>(list);
		columnSort.setComparator(leavegroupColumn, new Comparator<LeaveBalance>()
				{
			@Override
			public int compare(LeaveBalance e1,LeaveBalance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getLeaveGroup().getGroupName()!=null && e2.getLeaveGroup().getGroupName()!=null){
						return e1.getLeaveGroup().getGroupName().compareTo(e2.getLeaveGroup().getGroupName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void createRoleSorting() {
		
		List<LeaveBalance> list=getDataprovider().getList();
		columnSort=new ListHandler<LeaveBalance>(list);
		columnSort.setComparator(getEmployeeRoleColumn, new Comparator<LeaveBalance>()
				{
			@Override
			public int compare(LeaveBalance e1,LeaveBalance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeerole()!=null && e2.getEmployeerole()!=null){
						return e1.getEmployeerole().compareTo(e2.getEmployeerole());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void createCalendarSorting() {
		
		List<LeaveBalance> list=getDataprovider().getList();
		columnSort=new ListHandler<LeaveBalance>(list);
		columnSort.setComparator(calendarColumn, new Comparator<LeaveBalance>()
				{
			@Override
			public int compare(LeaveBalance e1,LeaveBalance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCalendarName()!=null && e2.getCalendarName()!=null){
						return e1.getCalendarName().compareTo(e2.getCalendarName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetCount()
	{
		List<LeaveBalance> list=getDataprovider().getList();
		columnSort=new ListHandler<LeaveBalance>(list);
		columnSort.setComparator(getCountColumn, new Comparator<LeaveBalance>()
				{
			@Override
			public int compare(LeaveBalance e1,LeaveBalance e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getEmpCount()== e2.getEmpCount()){
						return 0;}
					if(e1.getEmpCount()> e2.getEmpCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<LeaveBalance>()
				{
			@Override
			public String getValue(LeaveBalance object)
			{
				if( object.getEmpInfo().getEmpCount()==-1)
					return "N.A";
				else return object.getEmpCount()+"";
			}
				};
				table.addColumn(getCountColumn,"Id");
				getCountColumn.setSortable(true);
	}
	protected void addSortinggetFullname()
	{
		List<LeaveBalance> list=getDataprovider().getList();
		columnSort=new ListHandler<LeaveBalance>(list);
		columnSort.setComparator(getFullnameColumn, new Comparator<LeaveBalance>()
				{
			@Override
			public int compare(LeaveBalance e1,LeaveBalance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getFullName()!=null && e2.getFullName()!=null){
						return e1.getFullName().compareTo(e2.getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetFullname()
	{
		getFullnameColumn=new TextColumn<LeaveBalance>()
				{
			@Override
			public String getValue(LeaveBalance object)
			{
				return object.getFullName();
			}
				};
				table.addColumn(getFullnameColumn,"Name");
				getFullnameColumn.setSortable(true);
	}
	protected void addSortinggetCellNumber1()
	{
		List<LeaveBalance> list=getDataprovider().getList();
		columnSort=new ListHandler<LeaveBalance>(list);
		columnSort.setComparator(getCellNumber1Column, new Comparator<LeaveBalance>()
				{
			@Override
			public int compare(LeaveBalance e1,LeaveBalance e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCellNumber()== e2.getCellNumber()){
						return 0;}
					if(e1.getCellNumber()> e2.getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCellNumber1()
	{
		getCellNumber1Column=new TextColumn<LeaveBalance>()
				{
			@Override
			public String getValue(LeaveBalance object)
			{
				return object.getCellNumber()+"";
			}
				};
				table.addColumn(getCellNumber1Column,"Phone");
				getCellNumber1Column.setSortable(true);
	}
	protected void addSortinggetBranchName()
	{
		List<LeaveBalance> list=getDataprovider().getList();
		columnSort=new ListHandler<LeaveBalance>(list);
		columnSort.setComparator(getBranchNameColumn, new Comparator<LeaveBalance>()
				{
			@Override
			public int compare(LeaveBalance e1,LeaveBalance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBranchName()
	{
		getBranchNameColumn=new TextColumn<LeaveBalance>()
				{
			@Override
			public String getValue(LeaveBalance object)
			{
				return object.getBranch();
			}
				};
				table.addColumn(getBranchNameColumn,"Branch");
				getBranchNameColumn.setSortable(true);
	}
	protected void addSortinggetDesignation()
	{
		List<LeaveBalance> list=getDataprovider().getList();
		columnSort=new ListHandler<LeaveBalance>(list);
		columnSort.setComparator(getDesignationColumn, new Comparator<LeaveBalance>()
				{
			@Override
			public int compare(LeaveBalance e1,LeaveBalance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDesignation()!=null && e2.getDesignation()!=null){
						return e1.getDesignation().compareTo(e2.getDesignation());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetDesignation()
	{
		getDesignationColumn=new TextColumn<LeaveBalance>()
				{
			@Override
			public String getValue(LeaveBalance object)
			{
				return object.getDesignation();
			}
				};
				table.addColumn(getDesignationColumn,"Employee Designation");
				table.setColumnWidth(getDesignationColumn, "150px");
				getDesignationColumn.setSortable(true);
	}
	
	
	protected void addSortingDepartment()
	{
		List<LeaveBalance> list=getDataprovider().getList();
		columnSort=new ListHandler<LeaveBalance>(list);
		columnSort.setComparator(getDepartmentColumn, new Comparator<LeaveBalance>()
				{
			@Override
			public int compare(LeaveBalance e1,LeaveBalance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDepartment()!=null && e1.getDepartment()!=null){
						return e1.getDepartment().compareTo(e2.getDepartment());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetDepartment()
	{
		getDepartmentColumn=new TextColumn<LeaveBalance>()
				{
			@Override
			public String getValue(LeaveBalance object)
			{
				return object.getDepartment();
			}
				};
				table.addColumn(getDepartmentColumn,"Department");
				getDepartmentColumn.setSortable(true);
	}



	protected void addSortingEmploymentType()
	{
		List<LeaveBalance> list=getDataprovider().getList();
		columnSort=new ListHandler<LeaveBalance>(list);
		columnSort.setComparator(getEmployeeTypeColumn, new Comparator<LeaveBalance>()
				{
			@Override
			public int compare(LeaveBalance e1,LeaveBalance e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeeType()!=null && e2.getEmployeeType()!=null){
						return e1.getEmployeeType().compareTo(e2.getEmployeeType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmploymentType()
	{
		getEmployeeTypeColumn=new TextColumn<LeaveBalance>()
				{
			@Override
			public String getValue(LeaveBalance object)
			{
				return object.getEmployeeType();
			}
				};
				table.addColumn(getEmployeeTypeColumn,"Employee Type");
				table.setColumnWidth(getEmployeeTypeColumn, "140px");
				getEmployeeTypeColumn.setSortable(true);
	}
}
