package com.slicktechnologies.client.views.humanresource.employeeleave;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.TextInputCell;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;

public class EmployeeLeaveBalanceTable extends SuperTable<AllocatedLeaves> {

	TextColumn<AllocatedLeaves> leaveGroupColumn;
	TextColumn<AllocatedLeaves> leaveTypeColumn;
	TextColumn<AllocatedLeaves> validFromDateColumn;
	TextColumn<AllocatedLeaves> validTillDateColumn;
	
	/**
	 * Date : 17-12-2018 By ANIL
	 * Updated Leave Balance table
	 */
	TextColumn<AllocatedLeaves> shortnamecolumn;
	Column<AllocatedLeaves, String> editShortNameColumn;
	
	TextColumn<AllocatedLeaves> namecolumn;
	TextColumn<AllocatedLeaves> maximumDaysColumn;
	Column<AllocatedLeaves, String> editmaximumDaysColumn;
	
	TextColumn<AllocatedLeaves> earnedDaysColumn;
	Column<AllocatedLeaves, String> editEarnedDaysColumn;
	
	Column<AllocatedLeaves, String> availedDaysColumn;
	Column<AllocatedLeaves, String> balanceDaysColumn;
	
	TextColumn<AllocatedLeaves> getAvaileDaysCol;
	TextColumn<AllocatedLeaves> getBalanceDaysCol;
	
	/**
	 * @author Anil , Date: 08-03-2019
	 * added opening balance column which stores the closing leave balance of previous month
	 * changed opening leave balance column to max allowed leave
	 * changed Balance leave column to closing leave balance
	 * 
	 */
	TextColumn<AllocatedLeaves> getOpeningBalanceCol;
	Column<AllocatedLeaves, String> getEditableOpeningBalanceCol;
	
	
	

	public EmployeeLeaveBalanceTable() {
		super();
	}

	public EmployeeLeaveBalanceTable(UiScreen<AllocatedLeaves> view) {
		super(view);
	}

	@Override
	public void createTable() {
		// createColumnValidFrom();
		// createColumnValidTill();
		
		if(UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
			createLeavetypeNameCol();
			editableShortNameCol();
			createColumnMaximumDays();
			getEditableOpeningBalanceCol();
			createColumnAvailedDays();
			editableEarnedLeaveCol();
			createColumnBalanceDays();
			addFieldUpdater();
		}else{
			createLeavetypeNameCol();
			createleaveShortnameCol();
			createColumnMaximumDays();
			getOpeningBalanceCol();
			createViewAvailedDays();
			createColumnEarnedDays();
			getBalanceDaysCol();
		}
		

		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}

	private void getEditableOpeningBalanceCol(){
		EditTextCell cell = new EditTextCell();
		getEditableOpeningBalanceCol=new Column<AllocatedLeaves, String>(cell) {
			
			@Override
			public String getValue(AllocatedLeaves object) {
				// TODO Auto-generated method stub
//				if(object.getClosingBalance()==0){
//					object.setClosingBalance(object.getBalance()-object.getEarned());
//				}
				return object.getOpeningBalance()+"";
			}
		};
		table.addColumn(getEditableOpeningBalanceCol, "#Opening Leave Balance");
		
		getEditableOpeningBalanceCol.setFieldUpdater(new FieldUpdater<AllocatedLeaves, String>() {
			@Override
			public void update(int index, AllocatedLeaves object, String value) {
				// TODO Auto-generated method stub
				if(value!=null&&!value.equals("")){
					try{
						double closingBalance=Double.parseDouble(value);
						object.setOpeningBalance(closingBalance);
						table.redraw();
					}catch(NumberFormatException e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Please enter numeric value only.");
					}
					
				}
			}
		});
	}
	
	
	private void getOpeningBalanceCol(){
		getOpeningBalanceCol=new TextColumn<AllocatedLeaves>() {
			
			@Override
			public String getValue(AllocatedLeaves object) {
				// TODO Auto-generated method stub
//				if(object.getClosingBalance()==0){
//					object.setClosingBalance(object.getBalance()-object.getEarned());
//				}
				return object.getOpeningBalance()+"";
			}
		};
		table.addColumn(getOpeningBalanceCol, "Opening Leave Balance");
	}
	
	private void getAvaileDaysCol() {
		// TODO Auto-generated method stub
		getAvaileDaysCol = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				return object.getAvailedDays() + "";
			}
		};
		table.addColumn(getAvaileDaysCol, "Availed Days");
	}

	private void getBalanceDaysCol() {
		// TODO Auto-generated method stub
		getBalanceDaysCol = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				return object.getBalance() + "";
			}
		};
//		table.addColumn(getBalanceDaysCol, "Balance Days");
		table.addColumn(getBalanceDaysCol, "Closing Leave Balance");
	}

	private void createColumnEarnedDays() {
		earnedDaysColumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				return object.getEarned() + "";
			}
		};
		table.addColumn(earnedDaysColumn, "Earned Days");
	}

	protected void createLeavetypeNameCol() {
		namecolumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				return object.getName();
			}
		};
		table.addColumn(namecolumn, "Leave Name");
	}

	protected void createleaveShortnameCol() {
		shortnamecolumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				return object.getShortName();
			}
		};
		table.addColumn(shortnamecolumn, "Short Name");
	}

	protected void createEditColumnMaximum() {
		EditTextCell cell = new EditTextCell();
		editmaximumDaysColumn = new Column<AllocatedLeaves, String>(cell) {
			@Override
			public String getValue(AllocatedLeaves object) {
				return object.getMaxDays() + "";
			}
		};
		/**
		 * @author Anil,Date : 08-03-2019
		 * Changing lable from opening leave balance to max allowed
		 */
//		table.addColumn(editmaximumDaysColumn, "Opening Leave Balance");
		table.addColumn(editmaximumDaysColumn, "Max Allowed");
	}

	protected void setFieldUpdaterEdit() {
		editmaximumDaysColumn.setFieldUpdater(new FieldUpdater<AllocatedLeaves, String>() {
			@Override
			public void update(int index, AllocatedLeaves object,String value) {
				try {
					int maxDays = Integer.parseInt(value.trim());
					if (maxDays <= 0)
						return;
					object.setMaxDays(maxDays);
//					double bal = (object.getMaxDays()- object.getAvailedDays())+object.getEarned();
//					object.setBalance(bal);
					table.redrawRow(index);
				} catch (Exception e) {

				}

			}
		});
	}

	protected void createColumnValidFrom() {
		validFromDateColumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				// return AppUtility.parseDate(object.getValidfrom());
				return "";
			}
		};
		table.addColumn(validFromDateColumn, "Valid From");
	}

	protected void createColumnValidTill() {
		validTillDateColumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				// return AppUtility.parseDate(object.getValidtill());
				return "";
			}
		};
		table.addColumn(validTillDateColumn, "Valid Till");
	}

	protected void createColumnMaximumDays() {
		maximumDaysColumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				return object.getMaxDays() + "";
			}
		};
		/**
		 * @author Anil,Date : 08-03-2019
		 * Changing lable from opening leave balance to max allowed
		 */
//		table.addColumn(maximumDaysColumn, "Opening Leave Balance");
		table.addColumn(maximumDaysColumn, "Max Allowed");
		
	}

	protected void createColumnAvailedDays() {
		EditTextCell editCell = new EditTextCell();
		availedDaysColumn = new Column<AllocatedLeaves, String>(editCell) {
			@Override
			public String getValue(AllocatedLeaves object) {
				if (object.getAvailedDays() == 0) {
					return 0 + "";
				} else {
					return object.getAvailedDays() + "";
				}
			}
		};
		table.addColumn(availedDaysColumn, "#Availed Days");
	}

	protected void createViewAvailedDays() {

		availedDaysColumn = new TextColumn<AllocatedLeaves>() {
			@Override
			public String getValue(AllocatedLeaves object) {
				if (object.getAvailedDays() == 0) {
					return 0 + "";
				} else {
					return object.getAvailedDays() + "";
				}
			}
		};
		table.addColumn(availedDaysColumn, "Availed Days");
	}

	protected void createColumnBalanceDays() {
		EditTextCell editCell = new EditTextCell();
		balanceDaysColumn = new Column<AllocatedLeaves, String>(editCell) {
			@Override
			public String getValue(AllocatedLeaves object) {
				// double total=object.getMaxDays()-object.getAvailedDays();
				// object.setBalance(total);

				return object.getBalance() + "";
			}
		};
//		table.addColumn(balanceDaysColumn, "Balance");
		table.addColumn(balanceDaysColumn, "#Closing Leave Balance");
		
		balanceDaysColumn.setFieldUpdater(new FieldUpdater<AllocatedLeaves, String>() {
			@Override
			public void update(int index, AllocatedLeaves object, String value) {
				// TODO Auto-generated method stub
				if(value!=null&&!value.equals("")){
					try{
						double balance=Double.parseDouble(value);
						object.setBalance(balance);
						table.redraw();
					}catch(Exception e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Please enter numeric value only.");
					}
				}
			}
		});
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<AllocatedLeaves>() {
			@Override
			public Object getKey(AllocatedLeaves item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};

	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterAvailedDays();
//		setFieldUpdaterEdit();
	}

	protected void createFieldUpdaterAvailedDays() {
		availedDaysColumn.setFieldUpdater(new FieldUpdater<AllocatedLeaves, String>() {
			@Override
			public void update(int index, AllocatedLeaves object,String value) {
				try {
					double val1 = Double.parseDouble(value.trim());
					if(val1!=object.getAvailedDays()){
						object.setAvailedDays(val1);
	//					double balance=(object.getMaxDays()-object.getAvailedDays())+object.getEarned();
						double balance=(object.getBalance()-object.getAvailedDays());
						if (balance >= 0) {
							object.setBalance(balance);
							RowCountChangeEvent.fire(table,getDataprovider().getList().size(),true);
							table.redraw();
						} else {
							GWTCAlert alert=new GWTCAlert();
							alert.alert("Balance leave can not be negative");
						}
					}
				} catch (NumberFormatException e) {
					GWTCAlert alert=new GWTCAlert();
					alert.alert("Please enter numeric value only.");
					e.printStackTrace();
				}
				table.redrawRow(index);
			}
		});

	}

	@Override
	public void setEnable(boolean state) {
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);

		if (state == true) {

			// createColumnValidFrom();
			// createColumnValidTill();
//			createLeavetypeNameCol();
//			createEditColumnMaximum();
//			createColumnAvailedDays();
//			createColumnEarnedDays();
//			createColumnBalanceDays();
			
//			createLeavetypeNameCol();
//			editableShortNameCol();
//			createColumnMaximumDays();
//			createColumnAvailedDays();
//			editableEarnedLeaveCol();
//			getBalanceDaysCol();
//			
//			addFieldUpdater();
			
			
			if(UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
//				createLeavetypeNameCol();
//				editableShortNameCol();
//				createEditColumnMaximum();
//				createColumnAvailedDays();
//				editableEarnedLeaveCol();
//				createColumnBalanceDays();
//				addFieldUpdater();
				
				createLeavetypeNameCol();
				editableShortNameCol();
				createColumnMaximumDays();
				createColumnBalanceDays();
				createColumnAvailedDays();
				editableEarnedLeaveCol();
				getEditableOpeningBalanceCol();
				addFieldUpdater();
				
			}else{
//				createLeavetypeNameCol();
//				createleaveShortnameCol();
//				createColumnMaximumDays();
//				createViewAvailedDays();
//				createColumnEarnedDays();
//				getBalanceDaysCol();
				
				createLeavetypeNameCol();
				createleaveShortnameCol();
				createColumnMaximumDays();
				getBalanceDaysCol();
				createViewAvailedDays();
				createColumnEarnedDays();
				getOpeningBalanceCol();
			}
		}

		else {

			createLeavetypeNameCol();
			createleaveShortnameCol();
			createColumnMaximumDays();
			getBalanceDaysCol();
			createViewAvailedDays();
			createColumnEarnedDays();
			getOpeningBalanceCol();

		}

	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub

	}
	
	
	protected void editableShortNameCol() {
		EditTextCell editCell = new EditTextCell();
		editShortNameColumn = new Column<AllocatedLeaves, String>(editCell) {
			@Override
			public String getValue(AllocatedLeaves object) {
				if (object.getShortName()!=null) {
					return object.getShortName();
				} 
				return "";
			}
		};
		table.addColumn(editShortNameColumn, "Short Name");
		
		editShortNameColumn.setFieldUpdater(new FieldUpdater<AllocatedLeaves, String>() {
			@Override
			public void update(int index, AllocatedLeaves object, String value) {
				// TODO Auto-generated method stub
				if(value!=null&&!value.equals("")){
					object.setShortName(value);
					table.redraw();
				}
			}
		});
	}
	
	protected void editableEarnedLeaveCol() {
		EditTextCell editCell = new EditTextCell();
		editEarnedDaysColumn = new Column<AllocatedLeaves, String>(editCell) {
			@Override
			public String getValue(AllocatedLeaves object) {
				if (object.getEarned()!=null) {
					return object.getEarned()+"";
				} 
				return "";
			}
		};
		table.addColumn(editEarnedDaysColumn, "#Earned Leave");
		
		editEarnedDaysColumn.setFieldUpdater(new FieldUpdater<AllocatedLeaves, String>() {
			@Override
			public void update(int index, AllocatedLeaves object, String value) {
				// TODO Auto-generated method stub
				if(value!=null&&!value.equals("")){
					try{
						double earned=Double.parseDouble(value);
						if(earned!=object.getEarned()){
							object.setEarned(earned);
							object.setOpeningBalance(object.getBalance());
							object.setBalance(object.getBalance()+earned);
						table.redraw();
						}
					}catch(NumberFormatException e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Please enter numeric value only.");
					}
					
				}
			}
		});
	}

}
