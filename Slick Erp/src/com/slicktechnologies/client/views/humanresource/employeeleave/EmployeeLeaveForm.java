package com.slicktechnologies.client.views.humanresource.employeeleave;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.history.HistoryPopup;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class EmployeeLeaveForm extends FormScreen<LeaveBalance> implements ClickHandler{

	EmployeeInfoComposite eic;
	
	RadioButton rbself,rbothers;
	
	TextBox tbcalendarname;
	DateBox tbvalidfrom,tbvalidtill;
	TextBox tbleavegroup;
	
	EmployeeLeaveBalanceTable empLeaveBalance;
	
	
	/**
	 * Date : 02-06-2018 By ANIL
	 * it displays leave validity.
	 * For Sasha ERP 
	 */
	LeaveValidityTable leaveTbl;

	LeaveBalance leavebalObj;

	public  EmployeeLeaveForm() {
		super(FormStyle.DEFAULT);
		createGui();
		empLeaveBalance.connectToLocal();
	}

	public EmployeeLeaveForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}


	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget()
	{
		eic=AppUtility.employeeInfoComposite(new EmployeeInfo(),true);
		eic.setEnable(false);
		tbcalendarname=new TextBox();
		tbcalendarname.setEnabled(false);
		tbvalidfrom=new DateBoxWithYearSelector();
		tbvalidfrom.setEnabled(false);
		tbvalidtill=new DateBoxWithYearSelector();
		tbvalidtill.setEnabled(false);
		tbleavegroup=new TextBox();
		tbleavegroup.setEnabled(false);
		empLeaveBalance=new EmployeeLeaveBalanceTable();
		
		
		
		
		rbself = new RadioButton("rbgroup1");
		rbself.addClickHandler(this);
		rbself.setValue(true);
		rbothers = new RadioButton("rbgroup1");
		rbothers.addClickHandler(this);
		
		leaveTbl=new LeaveValidityTable();
	}


	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen() {
		initalizeWidget();

		//Token to initialize the processlevel menus.

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeInformation=fbuilder.setlabel("Employee Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",eic);
		FormField fempinfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Calendar Name",tbcalendarname);
		FormField ftbcalendarname= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Valid From",tbvalidfrom);
		FormField ftbvalidfrom= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Valid Till",tbvalidtill);
		FormField ftbvalidtill= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingBalanceInformation=fbuilder.setlabel("Leave Balance Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Leave Group",tbleavegroup);
		FormField ftbgroupname= fbuilder.setMandatory(true).setMandatoryMsg("Leave Group not allotted!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",empLeaveBalance.getTable());
		FormField ftableempleave= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingusertype = fbuilder.setlabel("User Type").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Self",rbself);
		FormField frbself= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Others",rbothers);
		FormField frbothers= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingLeaveValidityInfo=fbuilder.setlabel("Leave Validity Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("",leaveTbl.getTable());
		FormField fleaveTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {

				{fgroupingEmployeeInformation},
				{fempinfo},{ftbcalendarname,ftbvalidfrom,ftbvalidtill},
				{fgroupingBalanceInformation},{ftbgroupname},
				{ftableempleave},
				{fgroupingLeaveValidityInfo},
				{fleaveTbl},

		};

		this.fields=formfield;

	}


	@Override
	public void updateModel(LeaveBalance model) {
		List<AllocatedLeaves> daysalloc = empLeaveBalance.getDataprovider().getList();
		ArrayList<AllocatedLeaves> alloc = new ArrayList<AllocatedLeaves>();
		alloc.addAll(daysalloc);
		model.setAllocatedLeaves(alloc);
		leavebalObj=model;
		presenter.setModel(model);



	}

	@Override
	public void updateView(LeaveBalance view) {

		leavebalObj=view;
		if(view.getEmpInfo()!=null)
			eic.setValue(view.getEmpInfo());
		eic.getId().setValue(view.getEmpInfo().getEmpCount()+"");
		if(view.getValidfrom()!=null)
			tbvalidfrom.setValue(view.getValidfrom());

		if(view.getCalendarName()!=null)
		{
			tbcalendarname.setValue(view.getCalendarName());
		}
		if(view.getLeaveGroup().getGroupName()!=null)
			tbleavegroup.setValue(view.getLeaveGroup().getGroupName());
		if(view.getValidtill()!=null)
			tbvalidtill.setValue(view.getValidtill());
		empLeaveBalance.setValue(view.getLeaveGroup().getAllocatedLeaves());
//		System.out.println("Leve Type "+"MaxL"+"LevReq"+"LevApp"+"LevRej"+"LevAvailed"+"LEAVE BALANCE");
//		for(int i=0;i<view.getLeaveGroup().getAllocatedLeaves().size();i++){
//			
//			System.out.println(view.getLeaveGroup().getAllocatedLeaves().get(i).getName()+"\t"
//					+view.getLeaveGroup().getAllocatedLeaves().get(i).getMaxDays()+"\t"
//					+view.getLeaveGroup().getAllocatedLeaves().get(i).getRequested()+"\t"
//					+view.getLeaveGroup().getAllocatedLeaves().get(i).getApproved()+"\t"
//					+view.getLeaveGroup().getAllocatedLeaves().get(i).getRejected()+"\t"
//					+view.getLeaveGroup().getAllocatedLeaves().get(i).getAvailedDays()+"\t"
//					+view.getLeaveGroup().getAllocatedLeaves().get(i).getBalance());
//		}
//
//		System.out.println("Employee CALENDER Details "+view.getEmpInfo().getLeaveCalendar().getCalName());
//		
//		System.out.println("Employee OVERTIME Details "+view.getEmpInfo().getOvertime().getName());
		
		if(view.getLeaveValidityList()!=null){
			leaveTbl.setValue(view.getLeaveValidityList());
		}
		
		presenter.setModel(view);

	}





	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.LEAVEBALANCE,LoginPresenter.currentModule.trim());
	}




	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new LeaveBalance();
		model=leavebalObj;
		if(HistoryPopup.historyObj!=null){
			AppUtility.addDocumentToHistoryTable(HistoryPopup.historyObj.getModuleName(),AppConstants.LEAVEBALANCE, leavebalObj.getCount(), leavebalObj.getEmpCount(),leavebalObj.getEmpInfo().getFullName(),leavebalObj.getEmpInfo().getCellNumber(), false, model, null);
			HistoryPopup.historyObj=null;
		}
		else{
		AppUtility.addDocumentToHistoryTable(LoginPresenter.currentModule,AppConstants.LEAVEBALANCE, leavebalObj.getCount(), leavebalObj.getEmpCount(),leavebalObj.getEmpInfo().getFullName(),leavebalObj.getEmpInfo().getCellNumber(), false, model, null);
		}	
		
	}


	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbvalidfrom.setEnabled(false);
		tbvalidtill.setEnabled(false);
		tbcalendarname.setEnabled(false);
		tbleavegroup.setEnabled(false);
		eic.setEnable(false);
		rbself.setValue(true);
		empLeaveBalance.setEnable(state);
	}





	@Override
	public void onClick(ClickEvent event) {

		if(rbself.getValue()==true)
		{
			eic.setEnable(false);
		}
		if(rbothers.getValue()==true)
		{
			eic.setEnable(true);
		}

	}



	@Override
	public boolean validate() {
		boolean superRes=super.validate();
		boolean checkdays=true;

		List<AllocatedLeaves> tabledata=empLeaveBalance.getDataprovider().getList();
		ArrayList<AllocatedLeaves> arrData=new ArrayList<AllocatedLeaves>();
		arrData.addAll(tabledata);

		for(int i=0;i<arrData.size();i++)
		{
			double balanceDays=arrData.get(i).getBalance();
			String leaveType=arrData.get(i).getName();
			if(balanceDays<0)
			{
				showDialogMessage("Availed days are exceeding maximum days for "+leaveType);
				checkdays=false;
			}
		}

		return superRes&&checkdays;
	}

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		empLeaveBalance.getTable().redraw();
		leaveTbl.getTable().redraw();
	}
}
