package com.slicktechnologies.client.views.humanresource.employeeleave;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;

public class EmployeeLeavePresenter extends FormScreenPresenter<LeaveBalance> {

	EmployeeLeaveForm form;
	CsvServiceAsync csvservice = GWT.create(CsvService.class);

	public EmployeeLeavePresenter(UiScreen<LeaveBalance> view,
			LeaveBalance model) {
		super(view, model);
		form = (EmployeeLeaveForm) view;
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		if (text.equals("Leave Balance Report")) {
			reactOnDownload();
		}

	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reactOnDownload() {
		ArrayList<LeaveBalance> leavebalancearray = new ArrayList<LeaveBalance>();
		List<LeaveBalance> list = (List<LeaveBalance>) form
				.getSearchpopupscreen().getSupertable().getDataprovider()
				.getList();
		leavebalancearray.addAll(list);
		csvservice.setleavebalancelist(leavebalancearray,
				new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed" + caught);
					}

					@Override
					public void onSuccess(Void result) {

						String gwt = com.google.gwt.core.client.GWT
								.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 11;
						Window.open(url, "test", "enabled");
					}
				});

	}

	@Override
	protected void makeNewModel() {
		model = new LeaveBalance();
	}

	public static EmployeeLeaveForm initalize() {
		EmployeeLeaveForm form = new EmployeeLeaveForm();

		EmployeePresenterTableProxy gentable = new EmployeePresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		EmployeeLeavePresenterSearchProxy.staticSuperTable = gentable;
		EmployeeLeavePresenterSearchProxy searchpopup = new EmployeeLeavePresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		// EmployeeLeavePresenterTable
		// gentable=GWT.create(EmployeeLeavePresenterTable.class);
		// gentable.setView(form);
		// gentable.applySelectionModle();
		// EmployeeLeavePresenterSearch.staticSuperTable=gentable;
		// EmployeeLeavePresenterSearch
		// searchpopup=GWT.create(EmployeeLeavePresenterSearch.class);
		// form.setSearchpopupscreen(searchpopup);

		EmployeeLeavePresenter presenter = new EmployeeLeavePresenter(form,
				new LeaveBalance());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.EmployeeLeave")
	public static class EmployeeLeavePresenterSearch extends
			SearchPopUpScreen<LeaveBalance> {

		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.EmployeeLeave")
	public static class EmployeeLeavePresenterTable extends
			SuperTable<LeaveBalance> implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub

		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub

		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub

		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub

		}
	};

	private void reactTo() {
	}

}
