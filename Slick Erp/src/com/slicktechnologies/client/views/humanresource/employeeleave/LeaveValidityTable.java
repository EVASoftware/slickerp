package com.slicktechnologies.client.views.humanresource.employeeleave;

import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveValidityInfo;

public class LeaveValidityTable extends SuperTable<LeaveValidityInfo>{
	
	TextColumn<LeaveValidityInfo> getColLeaveName;
	TextColumn<LeaveValidityInfo> getColLeaveValidUntil;
	TextColumn<LeaveValidityInfo> getColLeaveCount;

	@Override
	public void createTable() {
		getColLeaveName();
		getColLeaveCount();
		getColLeaveValidUntil();
	}

	private void getColLeaveCount() {
		getColLeaveCount=new TextColumn<LeaveValidityInfo>() {
			@Override
			public String getValue(LeaveValidityInfo object) {
				if(object.getLeaveCount()!=0){
					return object.getLeaveCount()+"";
				}
				return "";
			}
		};
		table.addColumn(getColLeaveCount,"Leave Count");
	}

	private void getColLeaveName() {
		getColLeaveName=new TextColumn<LeaveValidityInfo>() {
			@Override
			public String getValue(LeaveValidityInfo object) {
				if(object.getLeaveName()!=null){
					return object.getLeaveName();
				}
				return "";
			}
		};
		table.addColumn(getColLeaveName,"Leave Name");
		
	}

	

	private void getColLeaveValidUntil() {
		getColLeaveValidUntil=new TextColumn<LeaveValidityInfo>() {
			@Override
			public String getValue(LeaveValidityInfo object) {
				if(object.getValidUntil()!=null){
					return AppUtility.parseDate(object.getValidUntil());
				}
				return "";
			}
		};
		table.addColumn(getColLeaveValidUntil,"Valid Until");
		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
