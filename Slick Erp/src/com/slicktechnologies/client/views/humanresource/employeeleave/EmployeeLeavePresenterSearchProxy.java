package com.slicktechnologies.client.views.humanresource.employeeleave;

import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;

import java.util.Vector;

import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
import com.slicktechnologies.shared.common.personlayer.*;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;

public class EmployeeLeavePresenterSearchProxy extends
		SearchPopUpScreen<LeaveBalance> {

	public EmployeeInfoComposite personInfo;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Department> olbDepartMent;
	public ObjectListBox<Config> olbEmploymentType;
	public ObjectListBox<Config> olbDesignation;
	public ObjectListBox<Config> olbRole;
	public TextBox idCardNumber;
	public TextBox accessCardNumber;
	public ObjectListBox<Calendar> olbCalendarName;
	public ObjectListBox<LeaveGroup> olbLeaveGroup;

	public ObjectListBox<Employee> olbReportsTo;
	public ListBox lbStatus;
	public DateComparator dateComparator;
	public ObjectListBox<ConfigCategory> olbConfigCustomerCategory;
	public ObjectListBox<Config> olbCustomerGroup;
	public ObjectListBox<Type> olbConfigCustomerType;

	public ObjectListBox<Config> olbConfigCustomerLevel;
	public ObjectListBox<Config> olbConfigCustomerPriority;

	public Object getVarRef(String varName) {
		if (varName.equals("olbBranch"))
			return this.olbBranch;
		if (varName.equals("olbSalesPerson"))
			return this.olbReportsTo;
		if (varName.equals("lbStatus"))
			return this.lbStatus;
		if (varName.equals("dateComparator"))
			return this.dateComparator;
		if (varName.equals("olbConfigCustomerCategory"))
			return this.olbConfigCustomerCategory;
		if (varName.equals("olbConfigCustomerLevel"))
			return this.olbConfigCustomerLevel;
		if (varName.equals("olbConfigCustomerPriority"))
			return this.olbConfigCustomerPriority;
		if (varName.equals("olbConfigCustomerType"))
			return this.olbConfigCustomerType;
		if (varName.equals("personInfo"))
			return this.personInfo;
		return null;
	}

	public EmployeeLeavePresenterSearchProxy() {
		super();
		createGui();
	}

	public void initWidget() {
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");

		olbBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		olbDepartMent = new ObjectListBox<Department>();
		AppUtility.makeSalesPersonListBoxDepartment(olbDepartMent);
		olbEmploymentType = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbEmploymentType, Screen.EMPLOYEETYPE);
		olbDesignation = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbDesignation, Screen.EMPLOYEEDESIGNATION);
		olbRole = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbRole, Screen.EMPLOYEEROLE);
		olbReportsTo = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbReportsTo);
		idCardNumber = new TextBox();
		accessCardNumber = new TextBox();
		olbCalendarName = new ObjectListBox<Calendar>();
		Calendar.makeObjectListBoxLive(olbCalendarName);
		olbLeaveGroup = new ObjectListBox<LeaveGroup>();
		this.initializeLeaveGroupListBox();
		/*
		 * lbStatus= new ListBox(); lbStatus.addItem("--Select--");
		 * lbStatus.addItem("Active"); lbStatus.addItem("Inactive");
		 */
	}

	public void createScreen() {
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("", personInfo);
		FormField fpersonInfo = builder.setMandatory(false).setRowSpan(1)
				.setColSpan(5).build();
		builder = new FormFieldBuilder("Branch", olbBranch);
		FormField folbBranch = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Department", olbDepartMent);
		FormField folbdepartment = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		builder = new FormFieldBuilder("Employment Type", olbEmploymentType);
		FormField folbtype = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Employee Designation", olbDesignation);
		FormField folbdesignation = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Employee Role", olbRole);
		FormField folbrole = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		// builder = new FormFieldBuilder("Reports To", olbReportsTo);
		// FormField folbSalesPerson = builder.setMandatory(false).setRowSpan(0)
		// .setColSpan(0).build();
		//
		// builder = new FormFieldBuilder("ID Card No.", idCardNumber);
		// FormField fidCard = builder.setMandatory(false).setRowSpan(0)
		// .setColSpan(0).build();
		//
		// builder = new FormFieldBuilder("Access Card No.", accessCardNumber);
		// FormField facesscard = builder.setMandatory(false).setRowSpan(0)
		// .setColSpan(0).build();
		//
		// builder = new FormFieldBuilder("Status", lbStatus);
		// FormField fstatus = builder.setMandatory(false).setRowSpan(0)
		// .setColSpan(0).build();

//		builder = new FormFieldBuilder("Calendar", olbCalendarName);
//		FormField folbcalendarname = builder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();

		builder = new FormFieldBuilder("LeaveGroup", olbLeaveGroup);
		FormField flobleavegroup = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		this.fields = new FormField[][] { { fpersonInfo },
				{ folbBranch, folbdepartment, folbtype },
				{ folbdesignation,folbrole,  flobleavegroup },
			};
	}

	
	
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		if (olbDepartMent.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbDepartMent.getValue().trim());
			temp.setQuerryString("empInfo.department");
			filtervec.add(temp);
		}

		if (olbRole.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbRole.getValue().trim());
			temp.setQuerryString("empInfo.employeerole");
			filtervec.add(temp);
		}
		if (olbDesignation.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbDesignation.getValue().trim());
			temp.setQuerryString("empInfo.designation");
			filtervec.add(temp);
		}
		if (olbBranch.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("empInfo.branch");
			filtervec.add(temp);
		}
		if (olbEmploymentType.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbEmploymentType.getValue().trim());
			temp.setQuerryString("empInfo.employeeType");
			filtervec.add(temp);
		}
		
//		if (olbCalendarName.getSelectedIndex() != 0) {
//			temp = new Filter();
//			temp.setStringValue(olbCalendarName.getValue().trim());
//			temp.setQuerryString("empInfo.leaveCalendar");
//			filtervec.add(temp);
//		}
		if (olbLeaveGroup.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbLeaveGroup.getValue().trim());
			temp.setQuerryString("empInfo.leaveGroupName");
			filtervec.add(temp);
		}

		if(!personInfo.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
			temp.setQuerryString("empInfo.empCount");
			filtervec.add(temp);
		}

		if(!(personInfo.getName().getValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(personInfo.getName().getValue());
			temp.setQuerryString("empInfo.fullName");
			filtervec.add(temp);
		}
		if(!personInfo.getPhone().getValue().equals(""))
		{
			temp=new Filter();
			temp.setLongValue(personInfo.getCellNumber());
			temp.setQuerryString("empInfo.cellNumber");
			filtervec.add(temp);
		}
		
		
//		{
//			if (personInfo.getIdValue() != -1) {
//				temp = new Filter();
//				temp.setIntValue(personInfo.getIdValue());
//				temp.setQuerryString("empInfo.empCount");
//				filtervec.add(temp);
//			}
//
//			if (!(personInfo.getFullNameValue().equals(""))) {
//				temp = new Filter();
//				temp.setStringValue(personInfo.getFullNameValue());
//				temp.setQuerryString("empInfo.fullName");
//				filtervec.add(temp);
//			}
//			if (personInfo.getCellValue() != -1l) {
//				temp = new Filter();
//				temp.setLongValue(personInfo.getCellValue());
//				temp.setQuerryString("empInfo.cellNumber");
//				filtervec.add(temp);
//			}
//		}
		
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new LeaveBalance());
		return querry;
	}

	protected void initializeLeaveGroupListBox() {
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new LeaveGroup());
		olbLeaveGroup.MakeLive(querry);
	}

	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}

}
