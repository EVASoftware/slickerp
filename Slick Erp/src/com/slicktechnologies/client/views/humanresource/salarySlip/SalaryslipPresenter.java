package com.slicktechnologies.client.views.humanresource.salarySlip;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.popups.DownloadPopPup;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.server.addhocprinting.CreatePayRollPDFServlet;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;

public class SalaryslipPresenter extends FormScreenPresenter<PaySlip> implements RowCountChangeEvent.Handler {

	SalarySlipForm form;
	protected GenricServiceAsync service = GWT.create(GenricService.class);
	/** The csvservice. */
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	EmailServiceAsync emailService = GWT.create(EmailService.class);
	NewEmailPopUp emailpopup = new NewEmailPopUp();

	DownloadPopPup downloadformatpopup = new DownloadPopPup();
	
	public SalaryslipPresenter(FormScreen<PaySlip> view, PaySlip model) {
		super(view, model);
		form = (SalarySlipForm) view;
		form.earningtable.connectToLocal();
		form.setPresenter(this);
		
		/** added by amol for total deduction addrowcountchangehandlers**/
		
		form.earningtable.getTable().addRowCountChangeHandler(this);
		form.deductiontable.getTable().addRowCountChangeHandler(this);
		form.getSearchpopupscreen().getPopup().setSize("100%", "100%");
//		form.earningtable.getTable().addRowCountChangeHandler(this);
		form.setEnable(false);
//		form.setToViewState();
		
//		form.getSearchpopupscreen().getPrintlbl().addClickHandler(this);
		
		downloadformatpopup.getBtnDownloadFormat1().addClickHandler(this);
		downloadformatpopup.getBtnDownloadFormat2().addClickHandler(this);
		downloadformatpopup.getLblCancel().addClickHandler(this);
	}
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		System.out.println("ON CLICK EVENT");
//		if(event.getSource()==form.getSearchpopupscreen().getPrintlbl()){
//			System.out.println("IN SIDE PRINT STATEMENT CLICK");
//			reactOnPrintStatement();
//		}
		
		if(event.getSource()==downloadformatpopup.getBtnDownloadFormat1()){
			reactonDownloadFormat1();
		}
		if (event.getSource()==downloadformatpopup.getBtnDownloadFormat2()) {
			reactonDownloadFormat2();
		}
		if (event.getSource()==downloadformatpopup.getLblCancel()) {
			downloadformatpopup.hidePopUp();
		}
	}
	
	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		if (text.equals("Email")){
			Console.log("Check Button Name");
			reactOnEmail();
		}
	}

	@Override
	public void reactOnPrint() {
//		if(form.pic!=null){
			/**
			 * Updated By:Viraj
			 * Date: 20-06-2019
			 * Description: Added status for printing 2 salary slips on one 
			 */
			final String url = GWT.getModuleBaseURL() + "processpayslip" + "?Id="+ model.getId()+ "&" + "status=" + "false";
			Window.open(url, "test", "enabled");
//		}
	}

//

	@Override
	public void reactOnDownload() {
//		ArrayList<PaySlip> paySlipList=new ArrayList<PaySlip>();
//		List<PaySlip> list=(List<PaySlip>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
//
//		paySlipList.addAll(list);
//		csvservice.setPaySliplist(paySlipList, new AsyncCallback<Void>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				System.out.println("RPC call Failed"+caught);
//			}
//
//			@Override
//			public void onSuccess(Void result) {
//
//				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//				final String url=gwt + "csvservlet"+"?type="+55;
//				Window.open(url, "test", "enabled");
//			}
//		});
		downloadformatpopup.getBtnDownloadFormat3().setVisible(false);
		downloadformatpopup.showPopUp();
		downloadformatpopup.getLblOk().setVisible(false);
	}
	
	@Override
	protected void makeNewModel() {
		model = new PaySlip();

	}

	public MyQuerry getSalaryConfigQuery() {
		MyQuerry quer = new MyQuerry();
		quer.setQuerryObject(new PaySlip());
		return quer;
	}

	public void setModel(PaySlip entity) {
		model = entity;
	}

	public static SalarySlipForm initalize() {

		SalarySlipForm form = new SalarySlipForm();

		SalarySlipPresenterTable gentable = new SalarySlipPresenterTable();
		gentable.setView(form);
		gentable.applySelectionModle();
		SalarySlipSearchForm.staticSuperTable = gentable;
		SearchPopUpScreen<PaySlip> searchpopup = new SalarySlipSearchForm();
		form.setSearchpopupscreen(searchpopup);

		SalaryslipPresenter presenter = new SalaryslipPresenter(form,new PaySlip());
		AppMemory.getAppMemory().stickPnel(form);
		return form;

	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip")
	public static class SalaryHeadPresenterTable extends SuperTable<PaySlip> implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			return null;
		}

		@Override
		public void createTable() {

		}

		@Override
		protected void initializekeyprovider() {

		}

		@Override
		public void addFieldUpdater() {

		}

		@Override
		public void setEnable(boolean state) {

		}

		@Override
		public void applyStyle() {

		}

	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.humanresourcelayer.SalaryHead")
	public static class SalaryHeadPresenterSearch extends SearchPopUpScreen<CtcComponent> {

		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}

	};

	private Double calculateGrossEarning(List<CtcComponent> earnings) {
		Double sum = 0.0;
//		for (CtcComponent temp : earnings) {
//			if (temp.getAmount() != null)
//				sum = sum + temp.getAmount();
//		}
		
		for (CtcComponent temp : earnings) {
			if (temp.getActualAmount() != 0)
				sum = sum + temp.getActualAmount();
		}
		return sum;

	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		
		
		if(event.getSource()==form.earningtable.getTable()){
			List<CtcComponent> list = form.earningtable.getDataprovider().getList();

			Double value = calculateGrossEarning(list);
			if (value != null) {
				form.dbGrossWithoutOT.setValue(value);
				if(form.dbOvertimeAmount.getValue()!=null){
					value=value+form.dbOvertimeAmount.getValue();
				}
				form.grossEarning.setValue(value);
				/**
				 * 
				 */
//				Double ctc = form.grossEarning.getValue();
//				form.grossEarning.setValue(ctc);
				double totalDeduction=0;
				if(form.totalDeduction.getValue()!=null){
					totalDeduction=form.totalDeduction.getValue();
				}
				double netEariningWithoutOT=form.dbGrossWithoutOT.getValue()-totalDeduction;
				double netEarning=form.grossEarning.getValue()-totalDeduction;
				form.netEarning.setValue(netEarning);
				form.dbNetWithoutOT.setValue(netEariningWithoutOT);
				/**
				 * 
				 */
//				form.netEarning dbNetWithoutOT totalDeduction dbGrossWithoutOT
			}
		}
		
		if(event.getSource()==form.deductiontable.getTable()){
			List<CtcComponent> list = form.deductiontable.getDataprovider().getList();
			Double value = calculateGrossEarning(list);
			Console.log("Deduction value :" + value);
			if (value != null) {
				form.totalDeduction.setValue(value);
				double netEariningWithoutOT=form.dbGrossWithoutOT.getValue()-form.totalDeduction.getValue();
				double netEarning=form.grossEarning.getValue()-form.totalDeduction.getValue();
				form.netEarning.setValue(netEarning);
				form.dbNetWithoutOT.setValue(netEariningWithoutOT);
			}
		}
		

	}

	
	
	
	private void reactOnPrintStatement(){
		ArrayList<PaySlip> paySlipList=new ArrayList<PaySlip>();
		List<PaySlip> list=(List<PaySlip>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();

		paySlipList.addAll(list);
		csvservice.setPaySliplist(paySlipList, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				
			}

			@Override
			public void onSuccess(Void result) {
				final String url = GWT.getModuleBaseURL() + "payslipstatement" + "?Id="+ UserConfiguration.getCompanyId();
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	public void reactOnEmail() {
		Console.log("Inside React On Email");
//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true) {
//			emailService.sendSalarySlipOnMail((PaySlip) model,
//					new AsyncCallback<Void>() {
//
//						@Override
//						public void onFailure(Throwable caught) {
//							Window.alert("Resource Quota Ended ");
//							caught.printStackTrace();
//
//						}
//
//						@Override
//						public void onSuccess(Void result) {
//							Window.alert("Email Sent Sucessfully !");
//
//						}
//					});
//		}
		
		/**
		 * @author Vijay Date 19-03-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData();
		
		/**
		 * ends here
		 */
		
	}

	
	private void setEmailPopUpData() {
		form.showWaitSymbol();
		String branchName = model.getBranch();
		String fromEmailId = AppUtility.getFromEmailAddress(branchName,null);
		List<ContactPersonIdentification> employeeEmailidlist = AppUtility.getEmployeeEmailid(model.getEmpid());
		System.out.println("employeeEmailidlist "+employeeEmailidlist);
		label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Employee",-1,"","",employeeEmailidlist);
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.HRMODULE.trim(),screenName);
		emailpopup.taFromEmailId.setValue(fromEmailId);
		emailpopup.smodel = model;
		form.hideWaitSymbol();
	
	}
	
	
	
	private void reactonDownloadFormat1() {
	
		ArrayList<PaySlip> paySlipList=new ArrayList<PaySlip>();
		List<PaySlip> list=(List<PaySlip>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();

		paySlipList.addAll(list);
		csvservice.setPaySliplist(paySlipList, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}

			@Override
			public void onSuccess(Void result) {

				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+55;
				Window.open(url, "test", "enabled");
				
				downloadformatpopup.hidePopUp();
			}
		});
	}
	
	private void reactonDownloadFormat2() {
		
		ArrayList<PaySlip> paySlipList=new ArrayList<PaySlip>();
		List<PaySlip> list=(List<PaySlip>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();

		paySlipList.addAll(list);
		csvservice.setPaySliplist(paySlipList, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}

			@Override
			public void onSuccess(Void result) {

				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+215;
				Window.open(url, "test", "enabled");
				
				downloadformatpopup.hidePopUp();

			}
		});
		
	}
	
}
