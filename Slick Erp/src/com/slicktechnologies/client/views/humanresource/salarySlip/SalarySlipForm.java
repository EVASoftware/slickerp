package com.slicktechnologies.client.views.humanresource.salarySlip;

import java.util.Date;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.settings.employee.EmployeeCTCTemplateTable;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.personlayer.EmployeeCTCTemplate;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

public class SalarySlipForm extends FormScreen<PaySlip> implements ClickHandler, ChangeHandler
{
	NumberFormat df = NumberFormat.getFormat("0.00");
	EmployeeInfoComposite pic;
	
	TextBox tbSalarySlipPeriod;
	
	TextBox tbESICNo;
	TextBox tbPanNo;
	TextBox tbPFACNo;
	TextBox tbaccNo;
	TextBox tbIFSC;
	
	DoubleBox grossEarning;
	DoubleBox totalDeduction;
	DoubleBox netEarning;
	TextBox tbblanck;
	
	SalaryComponentTable earningtable;
	SalaryComponentTable deductiontable;
	
	/**
	 * Date : 14-05-2018 By ANIL
	 * 
	 */
	DoubleBox dbOvertimeAmount;
	
	/**
	 * Date : 11-07-2018 BY ANIL
	 */
	DoubleBox dbGrossWithoutOT;
	DoubleBox dbNetWithoutOT;
	
	
	PaySlip payslipobj;
	
	/**
	 * Rahul added on 14 July 2108
	 * Desc: These two fields are for paid leave and bonus
	 */
	TextBox tbBonus,tbPaidLeaves;
	/**
	 * Ends
	 */
	
	/**
	 * Rahul added on 16 July 2018
	 */
	EmployeeCTCTemplateTable empCTCTemplateTable;
	
	/**
	 * Ends
	 */

	/**
	 * Rahul added this on 17 July 2018
	 */
//	ObjectListBox<CtcComponent> olbCTCComponents;
	/**
	 * Date : 31-07-2018 By ANIL
	 * replacing list box to textbox
	 * added subtract button
	 */
	TextBox olbCTCComponents;
	DoubleBox dbCTCAmtValue;
	Button btAddCtcValue;
	Button btnSubtract;
	/**
	 * ENDS
	 */
	
	/**
	 * Date : 31-07-2018 By ANIL
	 * Added monthly rate paid leave and bonus double box 
	 * on the basis of that calculate actual paid levae and bonus
	 */
	DoubleBox dbRatePaidLeave;
	DoubleBox dbRateBonus;
	DoubleBox dbNoOfEligibleDays;
	DoubleBox dbMonthlyDays;
	
	/**
	 * Date  : 12-09-2018 By ANIL
	 */
	ObjectListBox<CtcComponent> oblEarningComp;
	ObjectListBox<CtcComponent> oblEmpDeductionComp;
	
	DoubleBox dbEarnRateAmount;
	DoubleBox dbEarnActulaAmount;
	Button btnAddEarn;
	
	DoubleBox dbDeductionRateAmount;
	DoubleBox dbDeductionActulaAmount;
	Button btnAddDeduction;
	
//	DoubleBox dbOtHours;
//	DoubleBox dbOtRate;
	
	TextBox tbHappayCardNum;
	
	ObjectListBox<CtcComponent> olbExcludedCtcComp;
	DoubleBox dbExcludedCtcAmtValue;
	DateBox dbFromDateForCTC,dbToDateForCTC;
	Button btnAddExcluded;
	
	/**
	 * End
	 */
	TextBox tbUanNo;
	
	/**
	 * @author Anil , Date : 14-09-2019
	 * Adding bank name and branch name on salary slip screen
	 */
	TextBox tbBankName;
	TextBox tbBankBranchName;
	/**
	 * @author Abhinav
	 * @since 18/10/2019
	 */
	TextBox tbProjectName;
	
	/**
	 * @author Anil
	 * @sinnce 01-09-2020
	 * site location
	 */
	TextBox tbSiteLocation;
	boolean siteLocation=false;
	
	public SalarySlipForm()
	{
		super(FormStyle.DEFAULT);
		createGui();
		tbblanck.setVisible(false);
		dbOvertimeAmount.setEnabled(false);
		dbGrossWithoutOT.setEnabled(false);
		dbNetWithoutOT.setEnabled(false);
		/**
		 * Rahul added this on 17 July 2018
		 */
		btAddCtcValue.addClickHandler(this);
		btnSubtract.addClickHandler(this);

//		tbBonus.setEnabled(false);
//		tbPaidLeaves.setEnabled(false);
		
		
		
		dbRatePaidLeave.addChangeHandler(this);
		dbRateBonus.addChangeHandler(this);
		dbNoOfEligibleDays.addChangeHandler(this);
		
		dbMonthlyDays.addChangeHandler(this);
		
		siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "EnableSiteLocation");
	}
	
	public SalarySlipForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		tbSalarySlipPeriod.setEnabled(true);
		dbOvertimeAmount.setEnabled(false);
		dbGrossWithoutOT.setEnabled(false);
		dbNetWithoutOT.setEnabled(false);
		/**
		 * Rahul added this on 17 July 2018
		 */
		btAddCtcValue.addClickHandler(this);
		btnSubtract.addClickHandler(this);
		
		dbRatePaidLeave.addChangeHandler(this);
		dbRateBonus.addChangeHandler(this);
		dbNoOfEligibleDays.addChangeHandler(this);
		dbMonthlyDays.addChangeHandler(this);

//		tbBonus.setEnabled(false);
//		tbPaidLeaves.setEnabled(false);
		
		siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "EnableSiteLocation");
	}

	private void initalizeWidget()
	{
		pic=AppUtility.employeeInfoComposite(new EmployeeInfo(),true);
		pic.getElement().getStyle().setWidth(100, Unit.PCT);
		pic.getPanel().setWidth("100%");
		pic.form.setWidth("100%");
		
		tbSalarySlipPeriod=new TextBox();
		
		tbESICNo=new TextBox();
		tbPanNo=new TextBox();
		tbPFACNo=new TextBox();
		tbaccNo=new TextBox();
		tbIFSC=new TextBox();
		tbUanNo=new TextBox();
		
		totalDeduction=new DoubleBox();
		grossEarning=new DoubleBox();
		netEarning=new DoubleBox();
		
		tbblanck = new TextBox();
		tbblanck.setVisible(false);
		
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase("Admin")){
			earningtable=new SalaryComponentTable(true);
		}else{
			earningtable=new SalaryComponentTable();
		}
		earningtable.connectToLocal();
		
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase("Admin")){
			deductiontable=new SalaryComponentTable(true);
		}else{
			deductiontable=new SalaryComponentTable();
		}
		
		deductiontable.connectToLocal();
		
		dbOvertimeAmount=new DoubleBox();
		dbOvertimeAmount.setEnabled(false);
		
		dbGrossWithoutOT=new DoubleBox();
		dbGrossWithoutOT.setEnabled(false);
		
		dbNetWithoutOT=new DoubleBox();
		dbNetWithoutOT.setEnabled(false);
		
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase("Admin")){
			this.empCTCTemplateTable=new EmployeeCTCTemplateTable(true);
		}else{
			this.empCTCTemplateTable=new EmployeeCTCTemplateTable();
		}
		/**
		 * Rahul added on 17 July 2018
		 */
//		olbCTCComponents=new ObjectListBox<CtcComponent>();
//		MyQuerry qu= new MyQuerry();
//		Filter filter=new Filter();
//		filter.setQuerryString("isDeduction");
//		filter.setBooleanvalue(false);
//		qu.getFilters().add(filter);
//		qu.setQuerryObject(new CtcComponent());
//		olbCTCComponents.MakeLive(qu);
		
		olbCTCComponents=new TextBox();
		
//		btAddCtcValue=new Button("Add");
		btAddCtcValue=new Button("+");
		dbCTCAmtValue=new DoubleBox();
		dbCTCAmtValue.setValue(0d);
		/**
		 * Ends
		 */
		tbBonus=new TextBox();
//		tbBonus.setEnabled(false);
		
		tbPaidLeaves=new TextBox();
//		tbPaidLeaves.setEnabled(false);
		
		btnSubtract=new Button("-");
		btnSubtract.getElement().getStyle().setMarginLeft(10, Unit.PX);
		
		dbRatePaidLeave=new DoubleBox();
		dbRateBonus=new DoubleBox();
		dbNoOfEligibleDays=new DoubleBox();
		dbMonthlyDays=new DoubleBox();
		
		oblEarningComp=new ObjectListBox<CtcComponent>();
		MyQuerry q1=new MyQuerry();
		
		Filter f1=new Filter();
		f1.setQuerryString("status");
		f1.setBooleanvalue(true);
		q1.getFilters().add(f1);
		
		Filter f2=new Filter();
		f2.setQuerryString("isDeduction");
		f2.setBooleanvalue(false);
		q1.getFilters().add(f2);
		
		q1.setQuerryObject(new CtcComponent());
		oblEarningComp.MakeLive(q1);
		
		
		oblEmpDeductionComp=new ObjectListBox<CtcComponent>();
		MyQuerry q2=new MyQuerry();
		
		Filter f11=new Filter();
		f11.setQuerryString("status");
		f11.setBooleanvalue(true);
		q2.getFilters().add(f11);
		
		Filter f21=new Filter();
		f21.setQuerryString("isDeduction");
		f21.setBooleanvalue(true);
		q2.getFilters().add(f21);
		
		Filter f22=new Filter();
		f22.setQuerryString("isRecord");
		f22.setBooleanvalue(false);
		q2.getFilters().add(f22);
		
		q2.setQuerryObject(new CtcComponent());
		oblEmpDeductionComp.MakeLive(q2); 
		                                          
		dbEarnRateAmount=new DoubleBox();                      
		dbEarnActulaAmount=new DoubleBox();                    
		btnAddEarn=new Button("Add"); 
		btnAddEarn.addClickHandler(this);
		                                          
		dbDeductionRateAmount=new DoubleBox();                 
		dbDeductionActulaAmount=new DoubleBox();               
		btnAddDeduction=new Button("Add");  
		btnAddDeduction.addClickHandler(this);
		                                          
//		dbOtHours=new DoubleBox();                             
//		dbOtRate=new DoubleBox();                              
		                                          
		tbHappayCardNum=new TextBox();                         
		                                          
		olbExcludedCtcComp=new ObjectListBox<CtcComponent>();
		MyQuerry qu= new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("excludeInPayRoll");
		filter.setBooleanvalue(true);
		qu.getFilters().add(filter);
		qu.setQuerryObject(new CtcComponent());
		olbExcludedCtcComp.MakeLive(qu);
		
		dbExcludedCtcAmtValue=new DoubleBox();                 
		dbFromDateForCTC=new DateBoxWithYearSelector();
		dbToDateForCTC=new DateBoxWithYearSelector();         
		btnAddExcluded=new Button("Add");   
		btnAddExcluded.addClickHandler(this);
		
		tbBankName=new TextBox();
		tbBankBranchName=new TextBox();
		tbBankBranchName=new TextBox();
		tbProjectName=new TextBox();
		
		siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "EnableSiteLocation");
		tbSiteLocation=new TextBox();
	}

	@Override
	public void createScreen() {
		initalizeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeInformation=fbuilder.setlabel("Employee Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",pic);
		FormField fpic= fbuilder.setMandatory(true).setMandatoryMsg("Employee information is mandatory").setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Salary Period",tbSalarySlipPeriod);
		FormField ftbBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("ESIC Number",tbESICNo);
		FormField ftbESIC= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Pan Number",tbPanNo);
		FormField ftbPanNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Account Number",tbaccNo);
		FormField ftbAccNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("IFSC",tbIFSC);
		FormField ftbIFSC= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Gross Salary",grossEarning);
		FormField ftbGrossSalary= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Net Salary",netEarning);
		FormField ftbNetSalary= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("Total Deduction", totalDeduction);
		FormField ftbtotalDeduction= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingEarningInformation=fbuilder.setlabel("Earning Table Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",earningtable.getTable());
		FormField fearningtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField fgroupingDeductionInformation=fbuilder.setlabel("Deduction Table Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",deductiontable.getTable());
		FormField fdeductiontable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", tbblanck);
		FormField fblanck = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Overtime",dbOvertimeAmount);
		FormField fdbOvertimeAmount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Gross Salary Without OT",dbGrossWithoutOT);
		FormField fdbGrossWithoutOT= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Net Salary Without OT",dbNetWithoutOT);
		FormField fdbNetWithoutOT= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Rahul added on 14 July 2018
		 */
		fbuilder = new FormFieldBuilder("Bonus",tbBonus);
		FormField ftbBonus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Paid Leave",tbPaidLeaves);
		FormField ftbPaidLeaves= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Rahul Verma on 16 July 2018
		 */
		FormField fgroupingCTCAllocation=fbuilder.setlabel("CTC ALLOCATION").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",empCTCTemplateTable.getTable());
		FormField fempCTCTemplateTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		FormField fgroupingArrears=fbuilder.setlabel("Arrears Allocation").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Arrear Component",olbCTCComponents);
		FormField folbCTCComponents= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Amount",dbCTCAmtValue);
		FormField fdbCTCAmtValue= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btAddCtcValue);
		FormField fbtAddCtcValue= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnSubtract);
		FormField fbtnSubtract= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		HorizontalPanel hrPanel=new HorizontalPanel();
		hrPanel.add(btAddCtcValue);
		hrPanel.add(btnSubtract);
		
		fbuilder = new FormFieldBuilder("",hrPanel);
		FormField fhrPanel= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Monthly Paid Leave Rate",dbRatePaidLeave);
		FormField fdbRatePaidLeave= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Monthly Bonus Rate",dbRateBonus);
		FormField fdbRateBonus= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Paid Days",dbNoOfEligibleDays);
		FormField fdbNoOfEligibleDays= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Monthly Days",dbMonthlyDays);
		FormField fdbMonthlyDays= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		/**
		 * 
		 * 
		 */
		fbuilder = new FormFieldBuilder("Happay Number",tbHappayCardNum);
		FormField ftbHappayCardNum= fbuilder.setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("OT Hours",dbOtHours);
//		FormField fdbOtHours= fbuilder.setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("OT Rate",dbOtRate);
//		FormField fdbOtRate= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Earning Component",oblEarningComp);
		FormField foblEarningComp= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Rate",dbEarnRateAmount);
		FormField fdbEarnRateAmount= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Actual Amount",dbEarnActulaAmount);
		FormField fdbEarnActulaAmount= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnAddEarn);
		FormField fbtnAddEarn= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Deduction Component",oblEmpDeductionComp);
		FormField foblEmpDeductionComp= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Rate",dbDeductionRateAmount);
		FormField fdbDeductionRateAmount= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Actual Amount",dbDeductionActulaAmount);
		FormField fdbDeductionActulaAmount= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnAddDeduction);
		FormField fbtnAddDeduction= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Excluded CTC Component",olbExcludedCtcComp);
		FormField folbExcludedCtcComp= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Amount",dbExcludedCtcAmtValue);
		FormField fdbExcludedCtcAmtValue= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnAddExcluded);
		FormField fbtnAddExcluded= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("PF Number",tbPFACNo);
		FormField ftbPFACNo= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("UAN Number",tbUanNo);
		FormField ftbUanNo= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Bank Name",tbBankName);
		FormField ftbBankName= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Bank Branch Name",tbBankBranchName);
		FormField ftbBankBranchName= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project Name", tbProjectName);
		FormField ftbProjectName=fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Site Location", tbSiteLocation);
		FormField ftbSiteLocation=fbuilder.setRowSpan(0).setColSpan(0).setVisible(siteLocation).build();
		
		if(UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase("Admin")){
			FormField[][] formfield = {   
					{fgroupingEmployeeInformation},
					{fpic},
					{ftbPFACNo,ftbESIC,ftbPanNo,ftbHappayCardNum},
					{ftbAccNo,ftbIFSC,ftbBankName,ftbBankBranchName},
					{ftbBranch,ftbUanNo,ftbNetSalary,fdbNetWithoutOT},
					{ftbProjectName,ftbSiteLocation},
					{fgroupingArrears},
//					{folbCTCComponents,fdbCTCAmtValue,fbtAddCtcValue,fbtnSubtract},
					{folbCTCComponents,fdbCTCAmtValue,fhrPanel},
					{fgroupingEarningInformation},
					/**
					 * Date : 14-09-2018 BY ANIL
					 */
					{foblEarningComp,fdbEarnRateAmount,fdbEarnActulaAmount,fbtnAddEarn},
					{fearningtable},
					/**
					 * Date : 14-09-2018 BY ANIL
					 */
					{fblanck,fblanck,fblanck,fdbOvertimeAmount},
//					{fblanck,fdbOtHours,fdbOtRate,fdbOvertimeAmount},
					/**
					 * End
					 */
					
					{fblanck,fblanck,fdbGrossWithoutOT,ftbGrossSalary},
					{fgroupingDeductionInformation},
					/**
					 * Date : 14-09-2018 BY ANIL
					 */
					{foblEmpDeductionComp,fdbDeductionRateAmount,fdbDeductionActulaAmount,fbtnAddDeduction},
					{fdeductiontable},
					{fblanck,fblanck,fblanck,ftbtotalDeduction},
					{fdbRateBonus,fdbRatePaidLeave,fdbNoOfEligibleDays,fdbMonthlyDays},
					{ftbBonus,ftbPaidLeaves},
					{fgroupingCTCAllocation},
					/**
					 * Date : 14-09-2018 BY ANIL
					 */
					{folbExcludedCtcComp,fdbExcludedCtcAmtValue,fbtnAddExcluded},
					{fempCTCTemplateTable}
			};
			this.fields=formfield;	
		}else{
			FormField[][] formfield = {   
					{fgroupingEmployeeInformation},
					{fpic},
					{ftbPFACNo,ftbESIC,ftbPanNo,ftbHappayCardNum},
					{ftbAccNo,ftbIFSC,ftbBankName,ftbBankBranchName},
					{ftbBranch,ftbUanNo,ftbNetSalary,fdbNetWithoutOT},
					{ftbProjectName,ftbSiteLocation},
					{fgroupingArrears},
					{folbCTCComponents,fdbCTCAmtValue,fhrPanel},
					{fgroupingEarningInformation},
					
					{fearningtable},
					
					{fblanck,fblanck,fblanck,fdbOvertimeAmount},
					{fblanck,fblanck,fdbGrossWithoutOT,ftbGrossSalary},
					{fgroupingDeductionInformation},
					{fdeductiontable},
					{fblanck,fblanck,fblanck,ftbtotalDeduction},
					{fdbRateBonus,fdbRatePaidLeave,fdbNoOfEligibleDays,fdbMonthlyDays},
					{ftbBonus,ftbPaidLeaves},
					{fgroupingCTCAllocation},
					{fempCTCTemplateTable}
			};
			this.fields=formfield;	
		}
			

	}


	@Override
	public void updateModel(PaySlip model) 
	{
		if(pic.getValue()!=null)
		{
			model.setEmpid(pic.getEmployeeId());
			model.setEmployeeName(pic.getEmployeeName());
			model.setEmpCellNo(pic.getCellNumber());
			model.setBranch(pic.getBranch());
			model.setDepartment(pic.getDepartment());
			model.setEmployeeType(pic.getEmployeeType());
			model.setEmployeedDesignation(pic.getEmpDesignation());
			model.setEmployeeRole(pic.getEmpRole());
		}
		if(tbSalarySlipPeriod.getValue()!=null)
			model.setSalaryPeriod(tbSalarySlipPeriod.getValue());
		if(tbESICNo.getValue()!=null)
			model.setTbESICNo(tbESICNo.getValue());
		if(tbaccNo.getValue()!=null)
			model.setTbaccNo(tbaccNo.getValue());
		if(tbIFSC.getValue()!=null)
			model.setTbIFSC(tbIFSC.getValue());
		if(tbPanNo.getValue()!=null)
			model.setTbPanNo(tbPanNo.getValue());
		if(tbPFACNo.getValue()!=null)
			model.setTbPFACNo(tbPFACNo.getValue());
		if(earningtable.getValue()!=null)
			model.setEarningList(earningtable.getValue());
//		if(deductiontable.getValue()!=null)
//			model.setDeductionList(deductiontable.getValue());
		if(dbOvertimeAmount.getValue()!=null){
			model.setOvertimeSalary(dbOvertimeAmount.getValue());
		}
//		if(grossEarning.getValue()!=null)
//			model.setGrossSalary(grossEarning.getValue());
		if(tbBonus.getValue()!=null){
			try{
				model.setBonus(Double.parseDouble(tbBonus.getValue()));
			}catch(Exception e){
				e.printStackTrace();
				model.setBonus(0d);
			}
			
		}
		
		if(tbPaidLeaves.getValue()!=null){
			try{
				model.setPaidLeaves(Double.parseDouble(tbPaidLeaves.getValue()));
			}catch(Exception e){
				e.printStackTrace();
				model.setPaidLeaves(0d);
			}
			
		}
		if(this.empCTCTemplateTable.getValue().size()!=0){
			model.setEmployeeCTCTemplate(empCTCTemplateTable.getValue());
		}
		
//		if(earningtable.getValue()!=null){
//			model.setEarningList(earningtable.getValue());
//		}
		
		if(deductiontable.getValue()!=null){
			model.updateDeductionList(deductiontable.getValue());
		}
		
		if(totalDeduction.getValue()!=null){
			model.setTotalDeduction(totalDeduction.getValue());
		}
		
		if(dbRateBonus.getValue()!=null){
			model.setRateBonus(dbRateBonus.getValue());
		}
		
		if(dbRatePaidLeave.getValue()!=null){
			model.setRatePaidLeava(dbRatePaidLeave.getValue());
		}
		
		if(dbNoOfEligibleDays.getValue()!=null){
			model.setPaidDays(dbNoOfEligibleDays.getValue());
		}
		
		if(dbMonthlyDays.getValue()!=null){
			model.setMonthlyDays(dbMonthlyDays.getValue().intValue());
		}
		
		if(dbNetWithoutOT.getValue()!=null){
			model.setNetEarningWithoutOT(dbNetWithoutOT.getValue());
		}
		
		if(netEarning.getValue()!=null){
			model.setNetEarning(netEarning.getValue());
		}
		
		if(dbGrossWithoutOT.getValue()!=null){
			model.setGrossEarningWithoutOT(dbGrossWithoutOT.getValue());
		}
		
		if(grossEarning.getValue()!=null){
			model.setGrossEarning(grossEarning.getValue());
		}
		
		/**
		 * Date : 14-09-2018 by ANil
		 */
		if(tbHappayCardNum.getValue()!=null){
			model.setHappayCardNumber(tbHappayCardNum.getValue());
		}
		
		/**Date 11-9-2019 by Amol**/
		if(tbUanNo.getValue()!=null){
			model.setUanNo(tbUanNo.getValue());
		}
		
		if(tbBankName.getValue()!=null){
			model.setBankName(tbBankName.getValue());
		}
		if(tbBankBranchName.getValue()!=null){
			model.setBankBranch(tbBankBranchName.getValue());
		}
		

		if(tbProjectName.getValue()!=null){
			model.setProjectName(tbProjectName.getValue());
		}
		if(tbSiteLocation.getValue()!=null){
			model.setSiteLocation(tbSiteLocation.getValue());
		}
		
		payslipobj=model;
		
		presenter.setModel(model);


	}


	@Override
	public void updateView(PaySlip view) 
	{
		payslipobj=view;
		
		pic.setEmployeeId(view.getEmpid());
		if(view.getEmployeeName()!=null)
			pic.setEmployeeName(view.getEmployeeName());
		if(view.getEmpCellNo()!=null)
			pic.setCellNumber(view.getEmpCellNo());
		if(view.getBranch()!=null)
			pic.setBranch(view.getBranch());
		if(view.getDepartment()!=null)
			pic.setDepartment(view.getDepartment());
		if(view.getEmployeeType()!=null)
			pic.setEmployeeType(view.getEmployeeType());
		if(view.getEmployeedDesignation()!=null)
			pic.setEmpDesignation(view.getEmployeedDesignation());
		if(view.getEmployeeRole()!=null)
			pic.setEmpRole(view.getEmployeeRole());
		if(view.getBranch()!=null){ //Ashwini Patil Date:3-10-2022 Since salary period was not getting sorted period is converted to numbers after fetching list from db and here doing reverse of that
			String salaryPeriod=view.getSalaryPeriod();
			String year=salaryPeriod.substring(0, 5);
			String month=salaryPeriod.substring(5);
			String sp="";
			switch(month){
			case "01":
				sp=year+"Jan";
				break;
			case "02":
				sp=year+"Feb";
				break;
			case "03":
				sp=year+"Mar";
				break;
			case "04":
				sp=year+"Apr";
				break;
			case "05":
				sp=year+"May";
				break;
			case "06":
				sp=year+"Jun";
				break;
			case "07":
				sp=year+"Jul";
				break;
			case "08":
				sp=year+"Aug";
				break;
			case "09":
				sp=year+"Sep";
				break;
			case "10":
				sp=year+"Oct";
				break;
			case "11":
				sp=year+"Nov";
				break;
			case "12":
				sp=year+"Dec";
				break;
			}				
			tbSalarySlipPeriod.setValue(sp);
		}
		if(view.getTbaccNo()!=null)
			tbaccNo.setValue(view.getTbaccNo());
		if(view.getTbESICNo()!=null)
			tbESICNo.setValue(view.getTbESICNo());
		if(view.getTbIFSC()!=null)
			tbIFSC.setValue(view.getTbIFSC());
		if(view.getTbPanNo()!=null)
			tbPanNo.setValue(view.getTbPanNo());
		if(view.getTbPFACNo()!=null)
			tbPFACNo.setValue(view.getTbPFACNo());
		if(view.getMonthlyEarnings()!=0)
			grossEarning.setValue(view.getMonthlyEarnings());
		if(view.getEarningList()!=null){
			earningtable.setDeduction(false);
//			earningtable.setPaySlip(view);
			earningtable.setValue(view.getEarningList());
		}
		if(view.getDeductionList()!=null){
			deductiontable.setDeduction(true);
//			deductiontable.setPaySlip(view);
			deductiontable.setValue(view.getDeductionList());
		}
		if(view.getOvertimeSalary()!=0){
			dbOvertimeAmount.setValue(view.getOvertimeSalary());
		}
		if(view.getGrossEarning()!=0){
			grossEarning.setValue(Double.valueOf(df.format(view.getGrossEarning())));
		}
		if(view.getTotalDeduction()!=0){
			totalDeduction.setValue(Double.valueOf(df.format(view.getTotalDeduction())));
		}
		if(view.getNetEarning()!=0){
			netEarning.setValue(Double.valueOf(df.format(view.getNetEarning())));
		}
		
		if(view.getGrossEarningActualWithoutOT()!=0){
			dbGrossWithoutOT.setValue(view.getGrossEarningActualWithoutOT());
		}
		if(view.getNetEarningWithoutOT()!=0){
			dbNetWithoutOT.setValue(view.getNetEarningWithoutOT());
		}
		
		if(view.getBonus()!=0){
			tbBonus.setValue(view.getBonus()+"");
		}
		if(view.getPaidLeaves()!=0){
			tbPaidLeaves.setValue(view.getPaidLeaves()+"");
		}
		
		if(view.getEmployeeCTCTemplate()!=null){
			this.empCTCTemplateTable.setValue(view.getEmployeeCTCTemplate());
		}
		
		if(view.getUanNo()!=null){
			this.tbUanNo.setValue(view.getUanNo());
		}
		
		
		dbRatePaidLeave.setValue(view.getRatePaidLeava());
		dbRateBonus.setValue(view.getRateBonus());
		dbNoOfEligibleDays.setValue(view.getPaidDays());
		dbMonthlyDays.setValue((double) view.getMonthlyDays());
		
		
		if(view.getHappayCardNumber()!=null){
			tbHappayCardNum.setValue(view.getHappayCardNumber());
		}
		
		if(view.getBankName()!=null){
			tbBankName.setValue(view.getBankName());
		}
		if(view.getBankBranch()!=null){
			tbBankBranchName.setValue(view.getBankBranch());
		}
		if(view.getProjectName()!=null){
			tbProjectName.setValue(view.getProjectName());
		}
		
		if(view.getSiteLocation()!=null){
			tbSiteLocation.setValue(view.getSiteLocation());
		}
		
		presenter.setModel(view);
	}

	

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false); 
				}
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Search")||text.equals("Print")||text.equals(AppConstants.NAVIGATION)||text.contains("Edit")||text.contains("Email"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SALARYSLIP,LoginPresenter.currentModule.trim());
	}

	@Override
	public void setCount(int count)
	{
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbSalarySlipPeriod.setEnabled(false);
		dbOvertimeAmount.setEnabled(false);
		earningtable.setEnable(state);
		deductiontable.setEnable(state);
		empCTCTemplateTable.setEnable(state);
		
		
		
		tbESICNo.setEnabled(state);
		tbPanNo.setEnabled(state);
		tbHappayCardNum.setEnabled(state);
		tbaccNo.setEnabled(state);
		tbIFSC.setEnabled(state);
		tbPFACNo.setEnabled(state);
		tbUanNo.setEnabled(state);
		tbBankName.setEnabled(state);
		tbBankBranchName.setEnabled(state);
		tbProjectName.setEnabled(state);
		tbSiteLocation.setEnabled(state);
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		/**
		 * Rahul added this on 17 July 2018
		 */
		olbCTCComponents.setEnabled(false);
		dbCTCAmtValue.setEnabled(false);
		btAddCtcValue.setEnabled(false);
//		earningtable.setEnable(false);
		
		
		/**
		 * Ends
		 */

		SuperModel model=new PaySlip();
		model=payslipobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.HRMODULE,AppConstants.SALARYSLIP, payslipobj.getCount(), payslipobj.getEmpid(),payslipobj.getEmployeeName(),payslipobj.getEmpCellNo(), false, model, null);
	
		
	}
	
	
	

	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
		/**
		 * Rahul added this on 17 July 2018
		 */
		olbCTCComponents.setEnabled(true);
		dbCTCAmtValue.setEnabled(true);
		btAddCtcValue.setEnabled(true);

//		earningtable.setEnable(true);
		/**
		 * Ends
		 */
		tbSalarySlipPeriod.setEnabled(false);
		
		/**
		 * Date : 14-09-2018 BY ANIL
		 */
		if(!UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase("Admin")){
			tbESICNo.setEnabled(false);
			tbPanNo.setEnabled(false);
			tbPFACNo.setEnabled(false);
			tbaccNo.setEnabled(false);
			tbIFSC.setEnabled(false);
			tbHappayCardNum.setEnabled(false);
			tbUanNo.setEnabled(false);
			tbBankName.setEnabled(false);
			tbBankBranchName.setEnabled(false);
			tbProjectName.setEnabled(false);
			
			tbSiteLocation.setEnabled(false);
		}
		/**
		 * End
		 */
		
		grossEarning.setEnabled(false);
		totalDeduction.setEnabled(false);
		netEarning.setEnabled(false);
		tbblanck.setEnabled(false);
		
//		earningtable.setEnable(false);
//		deductiontable.setEnable(false);
		
		/**
		 * Date : 14-05-2018 By ANIL
		 * 
		 */
		dbOvertimeAmount.setEnabled(false);
		
		/**
		 * Date : 11-07-2018 BY ANIL
		 */
		dbGrossWithoutOT.setEnabled(false);
		dbNetWithoutOT.setEnabled(false);
//		tbBonus.setEnabled(false);
//		tbPaidLeaves.setEnabled(false);
		/**
		 * ENDS
		 */
		pic.setEnable(false);
		
		
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		boolean val= super.validate();
		if(val==false)
			return val;
		return true;
	}


	@Override
	public void clear() {
		super.clear();
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
		if(event.getSource()==btnAddEarn){
			String msg="";
			if(oblEarningComp.getSelectedIndex()==0){
				msg=msg+"Please select earning component."+"\n";
			}
			if(dbEarnRateAmount.getValue()==null){
				msg=msg+"Please add rate amount."+"\n";
			}
			if(dbEarnActulaAmount.getValue()==null){
				msg=msg+"Please add Actual amount."+"\n";
			}
			
			if(!msg.equals("")){
				showDialogMessage(msg);
				return;
			}
			
			CtcComponent earnComp=oblEarningComp.getSelectedItem();
			
			for(CtcComponent ctcCcomp:earningtable.getValue()){
				if(ctcCcomp.getName().trim().equals(earnComp.getName().trim())){
					showDialogMessage(earnComp.getName()+" already added.");
					return;
				}
			}
			
			double amountActual=dbEarnActulaAmount.getValue();
			CtcComponent ctcComponent=new CtcComponent();
			ctcComponent.setName(earnComp.getName());
			ctcComponent.setShortName(earnComp.getShortName());
			ctcComponent.setAmount(dbEarnRateAmount.getValue()*12);
			ctcComponent.setActualAmount(Math.round(amountActual));
			ctcComponent.setRecordTobeDeletedinTable(true);
			this.earningtable.getDataprovider().getList().add(ctcComponent);
			
			double grossEarningValue=grossEarning.getValue();
			double grossEarningWithoutOTValue=dbGrossWithoutOT.getValue();
			double netEarningValue=netEarning.getValue();
			double netEarningWithoutOTValue=dbNetWithoutOT.getValue();
			
			grossEarningValue=grossEarningValue+Math.round(amountActual);
			grossEarningWithoutOTValue=grossEarningWithoutOTValue+Math.round(amountActual);
			netEarningValue=netEarningValue+Math.round(amountActual);
			netEarningWithoutOTValue=netEarningWithoutOTValue+Math.round(amountActual);
			
			grossEarning.setValue(grossEarningValue);
			dbGrossWithoutOT.setValue(grossEarningWithoutOTValue);
			netEarning.setValue(netEarningValue);
			dbNetWithoutOT.setValue(netEarningWithoutOTValue);
			
			
			
		}
		if(event.getSource()==btnAddDeduction){
			String msg="";
			if(oblEmpDeductionComp.getSelectedIndex()==0){
				msg=msg+"Please select deduction component."+"\n";
			}
			if(dbDeductionRateAmount.getValue()==null){
				msg=msg+"Please add rate amount."+"\n";
			}
			if(dbDeductionActulaAmount.getValue()==null){
				msg=msg+"Please add Actual amount."+"\n";
			}
			
			if(!msg.equals("")){
				showDialogMessage(msg);
				return;
			}
			
			CtcComponent comp=oblEmpDeductionComp.getSelectedItem();
			
			for(CtcComponent ctcCcomp:deductiontable.getValue()){
				if(ctcCcomp.getName().trim().equals(comp.getName().trim())){
					showDialogMessage(comp.getName()+" already added.");
					return;
				}
			}
			
			double amountActual=dbDeductionActulaAmount.getValue();
			CtcComponent ctcComponent=new CtcComponent();
			ctcComponent.setName(comp.getName());
			ctcComponent.setShortName(comp.getShortName());
			ctcComponent.setAmount(dbDeductionRateAmount.getValue());
			ctcComponent.setActualAmount(Math.round(amountActual));
			ctcComponent.setRecordTobeDeletedinTable(true);
			ctcComponent.setDeduction(true);
			this.deductiontable.getDataprovider().getList().add(ctcComponent);
			
			double sumOfDeduction=0;
			for(CtcComponent ctc:deductiontable.getValue()){
				sumOfDeduction=sumOfDeduction+ctc.getActualAmount();
			}
			totalDeduction.setValue(sumOfDeduction);
			
			double netEarningWithoutOt=dbNetWithoutOT.getValue();
			double netEarningOt=netEarning.getValue();
			
			netEarningOt=netEarningOt-Math.round(amountActual);
			netEarningWithoutOt=netEarningWithoutOt-Math.round(amountActual);
			
			netEarning.setValue(netEarningOt);
			dbNetWithoutOT.setValue(netEarningWithoutOt);
		}
		if(event.getSource()==btnAddExcluded){
			
			String msg="";
			if(olbExcludedCtcComp.getSelectedIndex()==0){
				msg=msg+"Please select ctc component."+"\n";
			}
			if(dbExcludedCtcAmtValue.getValue()==null){
				msg=msg+"Please add  amount."+"\n";
			}
			if(!msg.equals("")){
				showDialogMessage(msg);
				return;
			}
			DateTimeFormat fmt=DateTimeFormat.getFormat("yyyy-MMM");
			Date salaryPeriod=fmt.parse(payslipobj.getSalaryPeriod().trim());
			
			Date startDate=CalendarUtil.copyDate(salaryPeriod);
			CalendarUtil.setToFirstDayOfMonth(startDate);
			
			for(EmployeeCTCTemplate ctcCcomp:empCTCTemplateTable.getValue()){
				if(ctcCcomp.getCtcTemplateName().trim().equals(olbExcludedCtcComp.getValue().trim())){
					showDialogMessage(olbExcludedCtcComp.getValue()+" already added.");
					return;
				}
			}
			
			EmployeeCTCTemplate empTable=new EmployeeCTCTemplate(olbExcludedCtcComp.getValue(olbExcludedCtcComp.getSelectedIndex())
					,startDate,salaryPeriod,dbExcludedCtcAmtValue.getValue());
			
			this.empCTCTemplateTable.getDataprovider().getList().add(empTable);
		}
		
		if(event.getSource()==btAddCtcValue){
			
			if(olbCTCComponents.getValue()!=null&&!olbCTCComponents.getValue().equals("")){
//			if(olbCTCComponents.getSelectedIndex()!=0){
				if(dbCTCAmtValue.getValue()!=0){
					double amountActual=dbCTCAmtValue.getValue();
					CtcComponent ctcComponent=new CtcComponent();
//					String componentName=olbCTCComponents.getItemText(olbCTCComponents.getSelectedIndex());
					String componentName=olbCTCComponents.getValue();
					ctcComponent.setName(componentName);
					ctcComponent.setShortName(componentName);
					ctcComponent.setAmount(amountActual*12);
					ctcComponent.setActualAmount(Math.round(amountActual));
					ctcComponent.setRecordTobeDeletedinTable(true);
					this.earningtable.getDataprovider().getList().add(ctcComponent);
					
					double grossEarningValue=grossEarning.getValue();
					double grossEarningWithoutOTValue=dbGrossWithoutOT.getValue();
					double netEarningValue=netEarning.getValue();
					double netEarningWithoutOTValue=dbNetWithoutOT.getValue();
					
					grossEarningValue=grossEarningValue+Math.round(amountActual);
					grossEarningWithoutOTValue=grossEarningWithoutOTValue+Math.round(amountActual);
					netEarningValue=netEarningValue+Math.round(amountActual);
					netEarningWithoutOTValue=netEarningWithoutOTValue+Math.round(amountActual);
					
					grossEarning.setValue(grossEarningValue);
					dbGrossWithoutOT.setValue(grossEarningWithoutOTValue);
					netEarning.setValue(netEarningValue);
					dbNetWithoutOT.setValue(netEarningWithoutOTValue);
					
				}else{
					showDialogMessage("Please enter amount");
				}
			}else{
				showDialogMessage("Please select CTC Component");
			}
		}
		
		if(event.getSource()==btnSubtract){
			
			if(olbCTCComponents.getValue()!=null&&!olbCTCComponents.getValue().equals("")){
//			if(olbCTCComponents.getSelectedIndex()!=0){
				if(dbCTCAmtValue.getValue()!=0){
					double amountActual=dbCTCAmtValue.getValue();
					CtcComponent ctcComponent=new CtcComponent();
//					String componentName=olbCTCComponents.getItemText(olbCTCComponents.getSelectedIndex());
					
					String componentName=olbCTCComponents.getValue();
					ctcComponent.setName(componentName);
					ctcComponent.setShortName(componentName);
//					ctcComponent.setAmount(amountActual);
					ctcComponent.setActualAmount(Math.round(amountActual));
					ctcComponent.setRecordTobeDeletedinTable(true);
					ctcComponent.setDeduction(true);
					this.deductiontable.getDataprovider().getList().add(ctcComponent);
					
					double sumOfDeduction=0;
					for(CtcComponent ctc:deductiontable.getValue()){
						sumOfDeduction=sumOfDeduction+ctc.getActualAmount();
					}
					totalDeduction.setValue(sumOfDeduction);
					
					double netEarningWithoutOt=dbNetWithoutOT.getValue();
					double netEarningOt=netEarning.getValue();
					
					netEarningOt=netEarningOt-Math.round(amountActual);
					netEarningWithoutOt=netEarningWithoutOt-Math.round(amountActual);
					
					netEarning.setValue(netEarningOt);
					dbNetWithoutOT.setValue(netEarningWithoutOt);
					
				}else{
					showDialogMessage("Please enter amount");
				}
			}else{
				showDialogMessage("Please select CTC Component");
			}
		}
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		
		double monthlyDays=0;
		double paidDays=0;
		double actualPaidLeave=0;
		double actualBonus=0;
		double ratePaidLeave=0;
		double rateBonus=0;
		
		if(dbMonthlyDays.getValue()!=null){
			monthlyDays=dbMonthlyDays.getValue();
		}
		
		if(dbNoOfEligibleDays.getValue()!=null){
			paidDays=dbNoOfEligibleDays.getValue();
		}
		
		if(dbRatePaidLeave.getValue()!=null){
			ratePaidLeave=dbRatePaidLeave.getValue();
		}
		
		if(dbRateBonus.getValue()!=null){
			rateBonus=dbRateBonus.getValue();
		}
		
		
		actualPaidLeave=Math.round((ratePaidLeave/monthlyDays)*paidDays);
		actualBonus=Math.round((rateBonus/monthlyDays)*paidDays);
		
		tbPaidLeaves.setValue(actualPaidLeave+"");
		tbBonus.setValue(actualBonus+"");
		
//		if(event.getSource()==dbRatePaidLeave){
//			if(dbRatePaidLeave.getValue()!=null){
//				if(dbNoOfEligibleDays.getValue()!=null){
//					
//					
//					tbPaidLeaves.setValue(dbRatePaidLeave.getValue()*dbNoOfEligibleDays.getValue()+"");
//				}
//			}
//			
//		}
//		
//		if(event.getSource()==dbRateBonus){
//			
//		}
//		
//		if(event.getSource()==dbNoOfEligibleDays){
//			
//		}
	}

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		earningtable.getTable().redraw();
		deductiontable.getTable().redraw();
		empCTCTemplateTable.getTable().redraw();
	}
	
	

}	


