package com.slicktechnologies.client.views.humanresource.salarySlip;



import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.slicktechnologies.client.views.humanresource.ctccomponent.EarningComponentPresenter.SalaryHeadPresenterTable;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;

public class SalaryComponentTable extends SalaryHeadPresenterTable {
	TextColumn<CtcComponent> getNameColumn;
	TextColumn<CtcComponent> getAmountColumn;
	TextColumn<CtcComponent> getMonthlyRateColumn;
	TextColumn<CtcComponent> viewDeleteButton;

	Column<CtcComponent, String> deleteButton;
	
	NumberFormat df = NumberFormat.getFormat("0.00");
	
	public boolean deduction;
//	public PaySlip paySlip;
	
	/**
	 * Date : 14-09-2018 By ANIL
	 * if admin login into system then delete button is accessible to him/her
	 */
	boolean isAdmin=false;
	
	
	public SalaryComponentTable() {
		super();
	}
	
	public SalaryComponentTable(boolean isAdmin) {
		super(isAdmin);
		this.isAdmin=isAdmin;
		createTable(isAdmin);
	}

	private void createTable(boolean isAdmin2) {
		addColumngetName();
		addColumngetMonthlyRate();
		addColumngetAmount();
		addDeleteButton();
		setFieldUpdaterOnDelete();
	}

	@Override
	public void createTable() {
		addColumngetName();
		addColumngetMonthlyRate();
		addColumngetAmount();
//		addDeleteButton();
//		setFieldUpdaterOnDelete();
	}

	private void setFieldUpdaterOnDelete() {
		// TODO Auto-generated method stub
		deleteButton.setFieldUpdater(new FieldUpdater<CtcComponent, String>() {

			@Override
			public void update(int index, CtcComponent object, String value) {
//				if(object.isRecordTobeDeletedinTable()){
					getDataprovider().getList().remove(index);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					table.redraw();
					
//				}else{
//					SalarySlipForm form=new SalarySlipForm();
//					form.showDialogMessage("Not Allowed");
//				}
			}
		});
	}

	private void addDeleteButton() {
		// TODO Auto-generated method stub
		System.out.println("DELETE");
		ButtonCell btnCell = new ButtonCell();
		deleteButton = new Column<CtcComponent, String>(btnCell) {

			@Override
			public String getValue(CtcComponent object) {

				return "Delete";
			}

//			@Override
//			public void render(Context context, CtcComponent object,SafeHtmlBuilder sb) {
//				// TODO Auto-generated method stub
//				
//				if(object.isRecordTobeDeletedinTable()==true){
//					super.render(context, object, sb);
//				}
//			}
			
			
		};
		table.addColumn(deleteButton, " ");
//		table.setColumnWidth(deleteButton, 5, Unit.PCT);
		
		
		
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<CtcComponent>() {
			@Override
			public Object getKey(CtcComponent item) {
				if (item == null) {
					return null;
				} else{
					return item.getId();
				}
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();
	}

	private void addViewColumn() {
		// TODO Auto-generated method stub
		System.out.println("VIEW");
		addColumngetName();
		addColumngetMonthlyRate();
		addColumngetAmount();
//		addViewDeleteButton();
	}

	private void addViewDeleteButton() {
		// TODO Auto-generated method stub
		viewDeleteButton = new TextColumn<CtcComponent>() {

			@Override
			public String getValue(CtcComponent object) {
				// TODO Auto-generated method stub
				return "";
			}
		};
		table.addColumn(viewDeleteButton, " ");
//		table.setColumnWidth(viewDeleteButton, 5, Unit.PCT);	
	}

	private void addeditColumn() {
		// TODO Auto-generated method stub
		System.out.println("EDIT");
		addColumngetName();
		addColumngetMonthlyRate();
		addColumngetAmount();
		if(isAdmin){
		addDeleteButton();
		setFieldUpdaterOnDelete();
		}
	}

	@Override
	public void applyStyle() {
	}


	@Override
	public void addFieldUpdater() {
	}


	protected void addColumngetName() {
		getNameColumn = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				return object.getName() + "";
			}
		};
		table.addColumn(getNameColumn, "Name");
		table.setColumnWidth(getNameColumn, 55, Unit.PCT);
		getNameColumn.setSortable(true);
	}

	protected void addColumngetAmount() {
		getAmountColumn = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				double paidAmount = 0;
				
//				if(isDeduction()==false){
//					if(object.getAmount()!=0){
//						double oneDaySal=(object.getAmount()/12)/paySlip.getMonthlyDays();
//						paidAmount=oneDaySal*paySlip.getPaidDays();
//					}
					if(object.getActualAmount()!=0){
						paidAmount=object.getActualAmount();
					}
//				}else{
					/**
					 * Date : 08-06-2018 BY ANIL
					 * if deduction component is PT then we don't calculate
					 */
					
//					if(object.getShortName().equalsIgnoreCase("PT")){
//						paidAmount=object.getAmount();
//					}else{
//						double oneDaySal=object.getAmount()/paySlip.getMonthlyDays();
//						paidAmount=oneDaySal*paySlip.getPaidDays();
//					}
//				}
				return df.format(paidAmount);
			}
		};
		table.addColumn(getAmountColumn, "Amount");
//		table.setColumnWidth(getAmountColumn, 20, Unit.PCT);
	}

	
	
	protected void addColumngetMonthlyRate() {
		getMonthlyRateColumn = new TextColumn<CtcComponent>() {
			@Override
			public String getValue(CtcComponent object) {
				double paidAmount = 0;
				if(isDeduction()==false){
					if(object.getAmount()!=0){
						double monthlyDeduction=object.getAmount()/12;
						paidAmount=monthlyDeduction;
					}
				}else{
					paidAmount=object.getAmount();
				}
				return df.format(paidAmount);
			}
		};
		table.addColumn(getMonthlyRateColumn, "Rate");
//		table.setColumnWidth(getMonthlyRateColumn, 20, Unit.PCT);
	}

	
	/***************************************************************************/
	
//	public PaySlip getPaySlip() {
//		return paySlip;
//	}
//
//	public void setPaySlip(PaySlip paySlip) {
//		this.paySlip = paySlip;
//	}
//
	public boolean isDeduction() {
		return deduction;
	}

	public void setDeduction(boolean deduction) {
		this.deduction = deduction;
	}


}
