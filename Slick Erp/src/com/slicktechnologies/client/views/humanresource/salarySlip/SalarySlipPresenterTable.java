package com.slicktechnologies.client.views.humanresource.salarySlip;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.salarySlip.SalaryslipPresenter.SalaryHeadPresenterTable;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;

public class SalarySlipPresenterTable extends SalaryHeadPresenterTable {
	TextColumn<PaySlip> getBranchColumn;
	TextColumn<PaySlip> getEmployeeColumn;
	TextColumn<PaySlip> getEmployeeTypeColumn;
	TextColumn<PaySlip> employeeDesignation;
	TextColumn<PaySlip> getEmployeeIdColumn;
	TextColumn<PaySlip> getEmployeeCellNumberColumn;
	TextColumn<PaySlip> getEmployeeFullNameColumn;
	TextColumn<PaySlip> roleColumn;
	TextColumn<PaySlip> salaryPeriod;
	
	TextColumn<PaySlip> getProjectNameCol;

	public SalarySlipPresenterTable() {
		super();
	}

	@Override
	public void createTable() {
		addSalarySlipColumn();
		addColumngetCustomerId();
		addColumngetCustomerFullName();
		addColumngetCustomerCellNumber();
		addColumngetBranch();
		getProjectNameCol();
		addColumngetType();
		addColumngetEmployeeDesignation();
		addColumngetRole();

	}

	private void getProjectNameCol() {
		// TODO Auto-generated method stub
		getProjectNameCol = new TextColumn<PaySlip>() {
			@Override
			public String getValue(PaySlip object) {
				if(object.getProjectName()!=null){
					return object.getProjectName();
				}else{
					return "";
				}
			}
		};
		table.addColumn(getProjectNameCol, "Project");
		getProjectNameCol.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<PaySlip>() {
			@Override
			public Object getKey(PaySlip item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	public void addColumnSorting() {
		addSortingSalaryPeriod();
		addSortinggetEmployee();
		addSortinggetEmployeeType();
		addSortinggetCustomerLevel();
		addSortinggetBranch();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortinggetCustomerId();
		addSortingRole();
		addSortingOnProjectCol();
	}

	private void addSortingOnProjectCol() {
		List<PaySlip> list = getDataprovider().getList();
		columnSort = new ListHandler<PaySlip>(list);
		columnSort.setComparator(getProjectNameCol, new Comparator<PaySlip>() {
			@Override
			public int compare(PaySlip e1, PaySlip e2) {
				if (e1 != null && e2 != null) {
					return e1.getProjectName().compareTo(e2.getProjectName());
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	@Override
	public void addFieldUpdater() {
		
	}

	protected void addSortinggetEmployee() {
		List<PaySlip> list = getDataprovider().getList();
		columnSort = new ListHandler<PaySlip>(list);
		columnSort.setComparator(getEmployeeColumn, new Comparator<PaySlip>() {
			@Override
			public int compare(PaySlip e1, PaySlip e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployeeName() != null
							&& e2.getEmployeeName() != null) {
						return e1.getEmployeeName().compareTo(
								e2.getEmployeeName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	

	protected void addSortinggetEmployeeType() {
		List<PaySlip> list = getDataprovider().getList();
		columnSort = new ListHandler<PaySlip>(list);
		columnSort.setComparator(getEmployeeTypeColumn,
				new Comparator<PaySlip>() {
					@Override
					public int compare(PaySlip e1, PaySlip e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmployeeType() != null
									&& e2.getEmployeeType() != null) {
								return e1.getEmployeeType().compareTo(
										e2.getEmployeeType());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	

	protected void addSortinggetCustomerLevel() {
		List<PaySlip> list = getDataprovider().getList();
		columnSort = new ListHandler<PaySlip>(list);
		columnSort.setComparator(employeeDesignation,
				new Comparator<PaySlip>() {
					@Override
					public int compare(PaySlip e1, PaySlip e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmployeedDesignation() != null
									&& e2.getEmployeedDesignation() != null) {
								return e1.getEmployeedDesignation().compareTo(
										e2.getEmployeedDesignation());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	

	protected void addSortinggetBranch() {
		List<PaySlip> list = getDataprovider().getList();
		columnSort = new ListHandler<PaySlip>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<PaySlip>() {
			@Override
			public int compare(PaySlip e1, PaySlip e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null && e2.getBranch() != null) {
						return e1.getBranch().compareTo(e2.getBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	

	protected void addSortinggetCustomerId() {
		List<PaySlip> list = getDataprovider().getList();
		columnSort = new ListHandler<PaySlip>(list);
		columnSort.setComparator(getEmployeeIdColumn,
				new Comparator<PaySlip>() {
					@Override
					public int compare(PaySlip e1, PaySlip e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmpid() == e2.getEmpid()) {
								return 0;
							}
							if (e1.getEmpid() > e2.getEmpid()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetCustomerFullName() {
		List<PaySlip> list = getDataprovider().getList();
		columnSort = new ListHandler<PaySlip>(list);
		columnSort.setComparator(getEmployeeFullNameColumn,
				new Comparator<PaySlip>() {
					@Override
					public int compare(PaySlip e1, PaySlip e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmployeeName() != null
									&& e2.getEmployeeName() != null) {
								return e1.getEmployeeName().compareTo(
										e2.getEmployeeName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetCustomerCellNumber() {
		List<PaySlip> list = getDataprovider().getList();
		columnSort = new ListHandler<PaySlip>(list);
		columnSort.setComparator(getEmployeeCellNumberColumn,
				new Comparator<PaySlip>() {
					@Override
					public int compare(PaySlip e1, PaySlip e2) {
						if (e1 != null && e2 != null) {
							if (e1.getEmpCellNo() == e2.getEmpCellNo()) {
								return 0;
							}
							if (e1.getEmpCellNo() > e2.getEmpCellNo()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortingRole() {
		List<PaySlip> list = getDataprovider().getList();
		columnSort = new ListHandler<PaySlip>(list);
		columnSort.setComparator(roleColumn, new Comparator<PaySlip>() {
			@Override
			public int compare(PaySlip e1, PaySlip e2) {
				if (e1 != null && e2 != null) {
					return e1.getEmployeeRole().compareTo(e2.getEmployeeRole());
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortingSalaryPeriod() {
		List<PaySlip> list = getDataprovider().getList();
		columnSort = new ListHandler<PaySlip>(list);
		columnSort.setComparator(salaryPeriod, new Comparator<PaySlip>() {
			@Override
			public int compare(PaySlip e1, PaySlip e2) {
			
				if (e1 != null && e2 != null) {
					return e1.getSalaryPeriod().compareTo(e2.getSalaryPeriod());
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	/************************************** view Table ****************************************/
	
	protected void addSalarySlipColumn() {
		salaryPeriod = new TextColumn<PaySlip>() {
			@Override
			public String getValue(PaySlip object) {
                //Ashwini Patil Date:3-10-2022 
				String salaryPeriod=object.getSalaryPeriod();
				String year=salaryPeriod.substring(0, 5);
				String month=salaryPeriod.substring(5);
				String sp="";

				switch(month){
				case "01":
					sp=year+"Jan";
					break;
				case "02":
					sp=year+"Feb";
					break;
				case "03":
					sp=year+"Mar";
					break;
				case "04":
					sp=year+"Apr";
					break;
				case "05":
					sp=year+"May";
					break;
				case "06":
					sp=year+"Jun";
					break;
				case "07":
					sp=year+"Jul";
					break;
				case "08":
					sp=year+"Aug";
					break;
				case "09":
					sp=year+"Sep";
					break;
				case "10":
					sp=year+"Oct";
					break;
				case "11":
					sp=year+"Nov";
					break;
				case "12":
					sp=year+"Dec";
					break;
				}	
//				return object.getSalaryPeriod();
				return sp;
			}
		};
		table.addColumn(salaryPeriod, "Salary Period");
		salaryPeriod.setSortable(true);
	}
	
	protected void addColumngetCustomerId() {
		getEmployeeIdColumn = new TextColumn<PaySlip>() {
			@Override
			public String getValue(PaySlip object) {
				return object.getEmpid() + "";
			}
		};
		table.addColumn(getEmployeeIdColumn, "Employee ID");
		table.setColumnWidth(getEmployeeIdColumn, "150 px");
		getEmployeeIdColumn.setSortable(true);
	}
	
	protected void addColumngetCustomerFullName() {
		getEmployeeFullNameColumn = new TextColumn<PaySlip>() {
			@Override
			public String getValue(PaySlip object) {
				return object.getEmployeeName() + "";
			}
		};
		table.addColumn(getEmployeeFullNameColumn, "Employee Name");
		getEmployeeFullNameColumn.setSortable(true);
	}
	
	protected void addColumngetCustomerCellNumber() {
		getEmployeeCellNumberColumn = new TextColumn<PaySlip>() {
			@Override
			public String getValue(PaySlip object) {
				if (object.getEmpCellNo() == 0) {
					return 0 + "";
				} else {
					return object.getEmpCellNo() + "";
				}
			}
		};
		table.addColumn(getEmployeeCellNumberColumn, "Employee Cell");
		getEmployeeCellNumberColumn.setSortable(true);
	}
	
	protected void addColumngetBranch() {
		getBranchColumn = new TextColumn<PaySlip>() {
			@Override
			public String getValue(PaySlip object) {
				return object.getBranch();
			}
		};
		table.addColumn(getBranchColumn, "Branch");
		getBranchColumn.setSortable(true);
	}
	
	protected void addColumngetType() {
		getEmployeeTypeColumn = new TextColumn<PaySlip>() {
			@Override
			public String getValue(PaySlip object) {
				return object.getEmployeeType();
			}
		};
		table.addColumn(getEmployeeTypeColumn, "Employee Type");
		getEmployeeTypeColumn.setSortable(true);
	}
	
	protected void addColumngetRole() {

		roleColumn = new TextColumn<PaySlip>() {
			@Override
			public String getValue(PaySlip object) {
				return object.getEmployeeRole();
			}
		};
		table.addColumn(roleColumn, "Employee Role");
		roleColumn.setSortable(true);
	}

	protected void addColumngetEmployeeDesignation() {
		employeeDesignation = new TextColumn<PaySlip>() {
			@Override
			public String getValue(PaySlip object) {
				return object.getEmployeedDesignation();
			}

		};
		table.addColumn(employeeDesignation, "Employee Designation");
		table.setColumnWidth(employeeDesignation, "180px");
		employeeDesignation.setSortable(true);
	}
	
//	protected void addColumngetEmployee() {
//		getEmployeeColumn = new TextColumn<PaySlip>() {
//			@Override
//			public String getValue(PaySlip object) {
//				return object.getEmployeeName();
//			}
//		};
//		table.addColumn(getEmployeeColumn, "Sales Person");
//		getEmployeeColumn.setSortable(true);
//	}
}
