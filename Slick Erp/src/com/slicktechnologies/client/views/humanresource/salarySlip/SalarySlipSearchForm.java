package com.slicktechnologies.client.views.humanresource.salarySlip;

import java.util.Date;
import java.util.Vector;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;

public class SalarySlipSearchForm extends SearchPopUpScreen<PaySlip>{
	
	public EmployeeInfoComposite personInfo;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Config> olbEmploymentType;
	public ObjectListBox<Config> olbDesignation;
	public ObjectListBox<Config> olbRole;
	public ListBox lbMonth;

	ObjectListBox<HrProject> project;
	
	
	/**
	 * Date : 01-01-2019 BY ANIL
	 * added salary period search
	 */
	DateBox salaryPeriod;

	public Object getVarRef(String varName)
	{

		return null ;
	}
	public SalarySlipSearchForm()
	{
		super();
		createGui();
//		getPrintlbl().setVisible(true);
//		salaryPeriod.getElement().getStyle().setWidth(120,Unit.PX);
		setFormat();
	}
	public void initWidget()
	{
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		olbEmploymentType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbEmploymentType,Screen.EMPLOYEETYPE);
		olbDesignation=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbDesignation,Screen.EMPLOYEEDESIGNATION);
		olbRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbRole,Screen.EMPLOYEEROLE);
		
		lbMonth=new ListBox();
		lbMonth.addItem("--SELECT--");
		lbMonth.addItem("January");
		lbMonth.addItem("February");
		lbMonth.addItem("March");
		lbMonth.addItem("April");
		lbMonth.addItem("May");
		lbMonth.addItem("June");
		lbMonth.addItem("July");
		lbMonth.addItem("August");
		lbMonth.addItem("September");
		lbMonth.addItem("October");
		lbMonth.addItem("November");
		lbMonth.addItem("December");
		
		
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
		
//		salaryPeriod=new DateBox();
		salaryPeriod = new DateBoxWithYearSelector();

		setFormat();
	}
	
	public void setFormat()
	{
		/*********************Date Time Format***************************************************/
		DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MMM");
		DateBox.DefaultFormat defformat=new DateBox.DefaultFormat(format);
		salaryPeriod.setFormat(defformat);
		salaryPeriod.setValue(new Date());
		/*****************************************************************************************/
	}
	
	public String getPayRollPeriod()
	{
		DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MMM");
		if(salaryPeriod.getValue()==null){
			return null;
		}
		else
		{
			return format.format(salaryPeriod.getValue());
		}
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		
		builder = new FormFieldBuilder("Employment Type",olbEmploymentType);
		FormField folbtype= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Employee Designation",olbDesignation);
		FormField folbdesignation= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Employee Role",olbRole);
		FormField folbrole= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		builder = new FormFieldBuilder("Month",lbMonth);
//		FormField flbMonth= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Month",salaryPeriod);
		FormField flbMonth= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Project",project);
		FormField fproject= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		this.fields=new FormField[][]{
				{fpersonInfo},
				{folbtype,folbdesignation,folbrole},
				{folbBranch,flbMonth,fproject},
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;

		if(lbMonth.getSelectedIndex()!=0)
		{
			Date date=new Date(); 
		    
//			AppUtility.
			
			 DateTimeFormat fmt = DateTimeFormat.getFormat("yyyy");
			    // prints Monday, December 17, 2007 in the default locale
//			    GWT.log(fmt.format(today));
			
//			SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy");
			String year=fmt.format(date);
			System.out.println("YEAR---"+year);
			int month=lbMonth.getSelectedIndex();
			String salaryPeriod;
			
			switch(month){
			
			case 1:
				salaryPeriod=year+"-Jan";
				break;
			case 2:
				salaryPeriod=year+"-Feb";
				break;
			case 3:
				salaryPeriod=year+"-Mar";
				break;
			case 4:
				salaryPeriod=year+"-Apr";
				break;
			case 5:
				salaryPeriod=year+"-May";
				break;
			case 6:
				salaryPeriod=year+"-Jun";
				break;
			case 7:
				salaryPeriod=year+"-Jul";
				System.out.println("MONTH---"+salaryPeriod);
				break;
			case 8:
				salaryPeriod=year+"-Aug";
				break;
			case 9:
				salaryPeriod=year+"-Sep";
				break;
			case 10:
				salaryPeriod=year+"-Oct";
				break;
			case 11:
				salaryPeriod=year+"-Nov";
				break;
			case 12:
				salaryPeriod=year+"-Dec";
				break;
			default:
				salaryPeriod=year+"";
				break;	
				
			}
			
			
			
			
			
			temp=new Filter();
			temp.setStringValue(salaryPeriod);
			temp.setQuerryString("salaryPeriod");
			filtervec.add(temp);
		}
		
		if(getPayRollPeriod()!=null){
			temp=new Filter();
			temp.setStringValue(getPayRollPeriod());
			temp.setQuerryString("salaryPeriod");
			filtervec.add(temp);
		}
		
		if(project.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(project.getValue().trim());
			temp.setQuerryString("projectName");
			filtervec.add(temp);
		}
		
		if(olbRole.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbRole.getValue().trim());
			temp.setQuerryString("employeeRole");
			filtervec.add(temp);
		}
		if(olbDesignation.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbDesignation.getValue().trim());
			temp.setQuerryString("employeedDesignation");
			filtervec.add(temp);
		}
		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if(olbEmploymentType.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbEmploymentType.getValue().trim());
			temp.setQuerryString("employeeType");
			filtervec.add(temp);
		}
		
		if(!personInfo.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
			temp.setQuerryString("empid");
			filtervec.add(temp);
		}

		if(!(personInfo.getName().getValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(personInfo.getName().getValue());
			temp.setQuerryString("employeeName");
			filtervec.add(temp);
		}
		if(!personInfo.getPhone().getValue().equals(""))
		{
			temp=new Filter();
			temp.setLongValue(personInfo.getCellNumber());
			temp.setQuerryString("empCellNo");
			filtervec.add(temp);
		}

		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new PaySlip());
		return querry;
	}
	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}

	
}
