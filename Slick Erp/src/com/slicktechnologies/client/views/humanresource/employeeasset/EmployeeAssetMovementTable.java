package com.slicktechnologies.client.views.humanresource.employeeasset;

import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNPresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.InventoryLocationPopUp;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;

public class EmployeeAssetMovementTable extends SuperTable<EmployeeAssetBean> implements ClickHandler{
	InventoryLocationPopUp invLoc=new InventoryLocationPopUp();
	PopupPanel panel=new PopupPanel(true);
	int rowIndex;
	
	public  ArrayList<String> warehouseList;
	public  ArrayList<String> binList;
	public  ArrayList<String> storagelocList;
	public  ArrayList<Storagebin> storagelocList1;
	
	Column<EmployeeAssetBean, String> warehouseColumn;
	TextColumn<EmployeeAssetBean> warehouseViewColumn;
	
	Column<EmployeeAssetBean, String> storagebin;
	Column<EmployeeAssetBean, String> columnviewBin;
	
	Column<EmployeeAssetBean, String> storageLoc;
	TextColumn<EmployeeAssetBean> columnviewLoc;

	Column<EmployeeAssetBean, String> deleteColumn;
	Column<EmployeeAssetBean, String> addColumn;
	TextColumn<EmployeeAssetBean> getcolumnStatus;
	GWTCGlassPanel glassPanel;
	
	TextColumn<EmployeeAssetBean> columnProductID;
	TextColumn<EmployeeAssetBean> productQuantity;
	Column<EmployeeAssetBean, String> columnProductQuantity;
	
	TextColumn<EmployeeAssetBean> prodNameColumn , prodViewQtyColumn,getAvailableQtyCol,getDeductionQtyCol,getAssetCodeCol;
	
	
//	Column<EmployeeAssetBean, String> getRemarkcolumn;
	Column<EmployeeAssetBean,String> getRemark;
	TextColumn<EmployeeAssetBean> getRemarkforView;
	Column<EmployeeAssetBean, Date> getDate;
	
	public EmployeeAssetMovementTable() {
		invLoc.getAddButton().addClickHandler(this);
		invLoc.getCancelButton().addClickHandler(this);
	}
	
	@Override
	public void createTable() {
		addColumnProductID();
		getAssetCodeCol();
		createColumnprodNameColumn();
		
		getAvailableQtyCol();
		createColumnAddColumn();
//		viewColumnProductQuantity();
//		getDeductionQtyCol();
		addColumnProductQuantity();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		
	//		createColumnAddColumn();
		addColumnDate();
		viewRemark();
		
		createColumnStatus();
		createColumndeleteColumn();
		
		addFieldUpdater();
	}

	private void addColumnDate() {
		// TODO Auto-generated method stub
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		getDate=new Column<EmployeeAssetBean, Date>(date) {

			@Override
			public Date getValue(EmployeeAssetBean object) {
				// TODO Auto-generated method stub
				if(object.getDate()!=null){
					return fmt.parse(fmt.format(object.getDate()));
				}else{
				Date date=new Date();
				CalendarUtil.resetTime(date);
				object.setDate(date);
				return fmt.parse(fmt.format(object.getDate()));
				}
			}
		};
	
//		getDate.setFieldUpdater(new FieldUpdater<EmployeeAssetBean, Date>() {
//			@Override
//			public void update(int index, EmployeeAssetBean object, Date date) {
//				object.setDate(date);
//			}
//		});	
		
		table.addColumn(getDate,"Date");
		table.setColumnWidth(getDate,90,Unit.PX);
		
	}

	

	private void getAssetCodeCol() {
		// TODO Auto-generated method stub
		getAssetCodeCol = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				return object.getAssetCode();
			}
		};
		table.addColumn(getAssetCodeCol, "Asset Code");
		table.setColumnWidth(getAssetCodeCol,90,Unit.PX);
	}

	private void getDeductionQtyCol() {
		// TODO Auto-generated method stub
		getDeductionQtyCol = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				return object.getDeductQty() + "";
			}
		};
		table.addColumn(getDeductionQtyCol, "Deduction Qty.");
		table.setColumnWidth(getDeductionQtyCol,90,Unit.PX);
	}

	private void getAvailableQtyCol() {
		// TODO Auto-generated method stub
		getAvailableQtyCol = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				return object.getAvailableQty() + "";
			}
		};
		table.addColumn(getAvailableQtyCol, "Available Qty.");
		table.setColumnWidth(getAvailableQtyCol,90,Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);
		
		if (state == true) {
			addColumnProductID();
			getAssetCodeCol();
			createColumnprodNameColumn();
			
			getAvailableQtyCol();
			createColumnAddColumn();
//			viewColumnProductQuantity();
//			getDeductionQtyCol();
			addColumnProductQuantity();
			viewWarehouse();
			viewstarageLoc();
			viewstorageBin();
			
		//		createColumnAddColumn();
			addColumnDate();
			viewRemark();
			
			createColumnStatus();
			createColumndeleteColumn();
			
			addFieldUpdater();
		} else {
			table.setWidth("100%");
			addColumnProductID();
			getAssetCodeCol();
			createColumnprodNameColumn();
			
			getAvailableQtyCol();
//			viewColumnProductQuantity();
//			getDeductionQtyCol();
			addColumnProductQuantity();
			
			viewWarehouse();
			viewstarageLoc();
			viewstorageBin();
			viewRemark1();
//			addColumnRemark();
			addColumnDate();
			createColumnStatus();
		}
	}

	private void viewRemark1() {
		// TODO Auto-generated method stub
		getRemarkforView = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				return object.getRemark();
			}
		};
		table.addColumn(getRemarkforView, "Remark");
		table.setColumnWidth(getRemarkforView,153,Unit.PX);
	}

	private void viewRemark() {
		// TODO Auto-generated method stub
		EditTextCell editCell=new EditTextCell();
		getRemark = new Column<EmployeeAssetBean,String>(editCell) {
			@Override
			public String getValue(EmployeeAssetBean object) {
				if(object.getRemark()!=null){
				    return object.getRemark();
				}else{
					return "";
				}
			}
		};
		
		
		   getRemark.setFieldUpdater(new FieldUpdater<EmployeeAssetBean, String>() {
			@Override
			public void update(int index, EmployeeAssetBean object, String value) {
				object.setRemark(value);
			}
		});
		
		table.addColumn(getRemark, "Remark");
		table.setColumnWidth(getRemark,153,Unit.PX);
		
		
	}

//	private void addColumnRemark() {
//		// TODO Auto-generated method stub
//		EditTextCell editCell=new EditTextCell();
//		getRemarkcolumn=new Column<EmployeeAssetBean, String>(editCell) {
//			@Override
//			public String getValue(EmployeeAssetBean object) {
//				// TODO Auto-generated method stub
//				if(object.getRemark()!=null){
//					System.out.println("Inside Not Null");
//					return object.getRemark();
//				}else{
//					System.out.println("Inside Null value on Table");
//					return object.getRemark();
//				}
//			}
//			
////			@Override
////			public void onBrowserEvent(Context context, Element elem,EmployeeAssetBean object, NativeEvent event) {
//////				if(object.getStatus().equals("Return")||object.getStatus().equals("Scrap")){
////					super.onBrowserEvent(context, elem, object, event);
//////				}
////			}
//			
//		};
//		
//		System.out.println("getRemarkcolumn value "+getRemarkcolumn);
//		
//		table.addColumn(getRemarkcolumn, "Remark");
//		table.setColumnWidth(getRemarkcolumn,90,Unit.PX);
//	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	public void addColumnProductQuantity() {
		EditTextCell editCell = new EditTextCell();
		columnProductQuantity = new Column<EmployeeAssetBean, String>(editCell) {
			@Override
			public String getValue(EmployeeAssetBean object) {
				if(object.getStatus().equals("Issue")){
					return object.getQty() + "";
				}else{
					return object.getDeductQty() + "";
				}
				
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,EmployeeAssetBean object, NativeEvent event) {
				if(object.getStatus().equals("Return")||object.getStatus().equals("Scrap")){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(columnProductQuantity, "#Qty.");
		table.setColumnWidth(columnProductQuantity,90,Unit.PX);
	}

	// Read Only Product Quantity
	private void viewColumnProductQuantity() {
		productQuantity = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				return object.getQty() + "";
			}
		};
		table.addColumn(productQuantity, "Qty.");
		table.setColumnWidth(productQuantity,90,Unit.PX);
	}
	
	public void addColumnProductID() {
		columnProductID = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				return object.getAssetId() + "";
			}
		};
		table.addColumn(columnProductID, "Asset ID");
		table.setColumnWidth(columnProductID,90,Unit.PX);
	}
	
	public void viewstorageBin() {
		columnviewBin = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				return object.getStorageLocBin();
			}
		};
		table.addColumn(columnviewBin, "Storage Bin");
		table.setColumnWidth(columnviewBin,153,Unit.PX);
	}

	public void viewWarehouse() {
		warehouseViewColumn = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				return object.getWarehouseName();
			}
		};
		table.addColumn(warehouseViewColumn, "Warehouse");
		table.setColumnWidth(warehouseViewColumn,180,Unit.PX);
	}
	
	public void viewstarageLoc() {
		columnviewLoc = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				return object.getStorageLocName();
			}
		};
		table.addColumn(columnviewLoc, "Storage Loc.");
		table.setColumnWidth(columnviewLoc,153,Unit.PX);
	}

	public void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<EmployeeAssetBean, String>(btnCell) {
			@Override
			public String getValue(EmployeeAssetBean object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn,81,Unit.PX);

	}
	
	public void createColumnAddColumn() {
		ButtonCell btnCell = new ButtonCell();
		addColumn = new Column<EmployeeAssetBean, String>(btnCell) {
			@Override
			public String getValue(EmployeeAssetBean object) {
				return "Select Warehouse";
			}
		};
		table.addColumn(addColumn, "");
		table.setColumnWidth(addColumn,180,Unit.PX);

	}
	
	
	private void createColumnStatus() {
		getcolumnStatus = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				if(object.getStatus()!=null)
					return object.getStatus();
				else
					return "";
			}
		};
		table.addColumn(getcolumnStatus,"Status");
		table.setColumnWidth(getcolumnStatus,90,Unit.PX);
	}
	
	
	protected void createColumnprodNameColumn() {
		prodNameColumn = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				return object.getAssetName();
			}
		};
		table.addColumn(prodNameColumn, "Asset Name");
		table.setColumnWidth(prodNameColumn,120,Unit.PX);
	}

	

	protected void createViewQtyColumn() {
		prodViewQtyColumn = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				if (object.getQty() == 0) {
					return 0 + "";
				} else {
					return object.getQty() + "";
				}
			}
		};
		table.addColumn(prodViewQtyColumn, "Quantity");
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		createFieldUpdaterprodQtyColumn();
		createFieldUpdaterAdd();
//		createFieldUpdaterRemark();
		createFieldUpdateRemark();
		createFiledUpdaterDate();
		createFieldUpdaterDelete();
	}

	private void createFieldUpdateRemark() {
		// TODO Auto-generated method stub
		getRemark.setFieldUpdater(new FieldUpdater<EmployeeAssetBean, String>() {

			@Override
			public void update(int index, EmployeeAssetBean object, String value) {
				// TODO Auto-generated method stub
			    String remark;
			    remark=value;
			    if(remark!=null){
			    	object.setRemark(remark);
			    }
			}

			
	});
}		

	private void createFiledUpdaterDate() {
		// TODO Auto-generated method stub
		getDate.setFieldUpdater(new FieldUpdater<EmployeeAssetBean, Date>() {

			@Override
			public void update(int index, EmployeeAssetBean object, Date value) {
				// TODO Auto-generated method stub
				Date date=new Date();
				date=value;
				if(date!=null){
					object.setDate(date);
				}
			}
		});
	}

//	private void createFieldUpdaterRemark() {
//		// TODO Auto-generated method stub
//		getRemarkcolumn.setFieldUpdater(new FieldUpdater<EmployeeAssetBean, String>() {
//			
//			@Override
//			public void update(int index, EmployeeAssetBean object, String value) {
//				// TODO Auto-generated method stub
//				String remark=value;
//			if(object.getStatus().equals("Return")){
//				if(remark!=null&&!remark.equals("")){
//				object.setReturnAssetremark(remark);
//				}else{
//					GWTCAlert alert=new GWTCAlert();
//				    alert.alert("Please Enter Remark");
//				}
//			}
//			}
//		});
//	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==invLoc.getAddButton())
		{	
			if(!GRNPresenter.warehouseName.equals("")){
				if(invLoc.getLsWarehouse().getSelectedIndex()!=0){
					if(!invLoc.getLsWarehouse().getValue(invLoc.getLsWarehouse().getSelectedIndex()).trim().equals(GRNPresenter.warehouseName.trim())){
						GRNPresenter.showMessage("Warehouse should be "+GRNPresenter.warehouseName+".");
						return;
					}
				}
			}
			if(invLoc.getLsWarehouse().getSelectedIndex()!=0&&invLoc.getLsStorageLoc().getSelectedIndex()!=0&&invLoc.getLsStoragebin().getSelectedIndex()!=0){
				
				/** Date 27-04-2017 added by vijay for deadstock NBHC
				 * for warehouse Address and old code added into timer
				 */
				String warehouseName = invLoc.getLsWarehouse().getValue(invLoc.getLsWarehouse().getSelectedIndex());
				getWarehouseAddress(warehouseName);
				glassPanel = new GWTCGlassPanel();
				glassPanel.show();
				Timer time = new Timer() {
					
					@Override
					public void run() {
						
					
				ArrayList<EmployeeAssetBean> list=new ArrayList<EmployeeAssetBean>();
				if(getDataprovider().getList().size()!=0){
					list.addAll(getDataprovider().getList());
					for( int i=rowIndex;i<getDataprovider().getList().size();i++){
						list.get(rowIndex).setWarehouseName(invLoc.getLsWarehouse().getValue(invLoc.getLsWarehouse().getSelectedIndex()));
						list.get(rowIndex).setStorageLocName(invLoc.getLsStorageLoc().getValue(invLoc.getLsStorageLoc().getSelectedIndex()));
						list.get(rowIndex).setStorageLocBin(invLoc.getLsStoragebin().getValue(invLoc.getLsStoragebin().getSelectedIndex()));
						list.get(rowIndex).setAvailableQty(invLoc.getAvailableQty());
						getDataprovider().getList().clear();
						getDataprovider().getList().addAll(list);
						glassPanel.hide();
					}
				}
			   }
					
				};
				time.schedule(2000);
				
				panel.hide();
			}
			if(invLoc.getLsWarehouse().getSelectedIndex()==0&&invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
				GRNPresenter.showMessage("Please Select Warehouse.");
			}
			else if(invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
				GRNPresenter.showMessage("Please Select Storage Location.");
			}
			else if(invLoc.getLsStoragebin().getSelectedIndex()==0){
				GRNPresenter.showMessage("Please Select Storage Bin.");
			}
			
		}
		if(event.getSource()==invLoc.getCancelButton())
		{	
			invLoc.clear();
			panel.hide();
		}
	}

	
	
	/****************************************** Column Update methods ******************************************/
	protected void createFieldUpdaterprodQtyColumn() {
		columnProductQuantity.setFieldUpdater(new FieldUpdater<EmployeeAssetBean, String>() {
			@Override
			public void update(int index, EmployeeAssetBean object,String value) {
				try {
					double val1;
					val1 = Double.parseDouble(value.trim());
					if(object.getStatus().equals("Return")||object.getStatus().equals("Scrap")){
						
						if(val1<=object.getQty()){
							object.setDeductQty(val1);
						}else{
							object.setDeductQty(0);
							GWTCAlert alert=new GWTCAlert();
							alert.alert("Deduction quantity can not be more than issue ");
						}
						
					}else{
						object.setQty(val1);
					}
				} catch (NumberFormatException e) {
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
				table.redraw();
			}
		});
	}

	// Delete Update Method
	public void createFieldUpdaterDelete() {
		deleteColumn.setFieldUpdater(new FieldUpdater<EmployeeAssetBean, String>() {
			@Override
			public void update(int index, EmployeeAssetBean object, String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	// Delete Update Method
	public void createFieldUpdaterAdd() {
		addColumn.setFieldUpdater(new FieldUpdater<EmployeeAssetBean, String>() {
			@Override
			public void update(int index, EmployeeAssetBean object, String value) {
				
				panel=new PopupPanel(true);
				panel.add(invLoc);
				
				InventoryLocationPopUp.initialzeWarehouse(object.getAssetId());
				
				
				if(object.getWarehouseName()!=null&&object.getStorageLocName()!=null&&object.getStorageLocBin()!=null){
					System.out.println("Inside table not null list settt...........");
					for(int i=0;i<invLoc.getLsWarehouse().getItemCount();i++)
					{
						String data=invLoc.lsWarehouse.getItemText(i);
						if(data.equals(object.getWarehouseName()))
						{
							invLoc.lsWarehouse.setSelectedIndex(i);
							break;
						}
					}
					
					invLoc.getLsStorageLoc().clear();
					invLoc.lsStorageLoc.addItem("--SELECT--");
					invLoc.lsStorageLoc.addItem(object.getStorageLocName());
					
					
					invLoc.getLsStoragebin().clear();
					invLoc.lsStoragebin.addItem("--SELECT--");
					
					for(int i=0;i<invLoc.arraylist2.size();i++){
						if(invLoc.arraylist2.get(i).getWarehouseName().equals(object.getWarehouseName())
								&&invLoc.arraylist2.get(i).getStorageLocation().equals(object.getStorageLocName())){
							invLoc.lsStoragebin.addItem(invLoc.arraylist2.get(i).getStorageBin());
						}
					}
					
					for(int i=0;i<invLoc.getLsStorageLoc().getItemCount();i++)
					{
						String data=invLoc.lsStorageLoc.getItemText(i);
						if(data.equals(object.getStorageLocName()))
						{
							invLoc.lsStorageLoc.setSelectedIndex(i);
							break;
						}
					}
					for(int i=0;i<invLoc.getLsStoragebin().getItemCount();i++)
					{
						String data=invLoc.lsStoragebin.getItemText(i);
						if(data.equals(object.getStorageLocBin()))
						{
							invLoc.lsStoragebin.setSelectedIndex(i);
							break;
						}
					}
				}
				invLoc.formView();
				panel.show();
				
				
				panel.center();
				rowIndex=index;
			}
		});
	}
	
		private void getWarehouseAddress(String warehouseName) {

			final GenricServiceAsync service = GWT.create(GenricService.class);
			MyQuerry query = new MyQuerry();
			Company c=new Company();
			System.out.println("Company Id :: "+c.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("buisnessUnitName");
			filter.setStringValue(warehouseName);
			filtervec.add(filter);
			
			query.setFilters(filtervec);
			query.setQuerryObject(new WareHouse());
			
			service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					
					for(int i=0;i<result.size();i++){
						if(i==0){
							WareHouse warehouse = (WareHouse) result.get(i);
						}
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}

}
