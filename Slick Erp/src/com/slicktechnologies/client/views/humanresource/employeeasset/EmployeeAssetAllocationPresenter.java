package com.slicktechnologies.client.views.humanresource.employeeasset;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.EmployeeAssetBean;

public class EmployeeAssetAllocationPresenter extends FormScreenPresenter<EmployeeAsset> implements RowCountChangeEvent.Handler , ChangeHandler ,SelectionHandler<Suggestion>{



	EmployeeAssetAllocationForm form;
	protected GenricServiceAsync service = GWT.create(GenricService.class);
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	
	/**
	 * @author Anil , Date : 10-07-2019
	 * 
	 */
	EmployeeAssetMovementPopup assetPopup=new EmployeeAssetMovementPopup();
	
	
	public EmployeeAssetAllocationPresenter(FormScreen<EmployeeAsset> view, EmployeeAsset model) {
		super(view, model);
		form = (EmployeeAssetAllocationForm) view;
		form.setPresenter(this);
		form.employeeAssetProductTable.getTable().addRowCountChangeHandler(this);
		form.getSearchpopupscreen().getPopup().setSize("100%", "100%");
	
		form.pic.getId().addSelectionHandler(this);
		form.pic.getName().addSelectionHandler(this);
		form.pic.getPhone().addSelectionHandler(this);
		
		assetPopup.getLblOk().addClickHandler(this);
		assetPopup.getLblCancel().addClickHandler(this);

	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		if(text.equals(AppConstants.NEW)){
			reactToNew();
		}
		
		if(text.equals("Issue")){
			assetPopup.setTransactionType("Issue");
			assetPopup.setBranch(form.oblBranch.getValue());
			assetPopup.setEmpId(form.pic.getEmployeeId());
			assetPopup.setEmpName(form.pic.getEmployeeName());
			assetPopup.table.getDataprovider().getList().clear();
			List<EmployeeAssetBean> list=new ArrayList<EmployeeAssetBean>();
			for(EmployeeAssetBean bean:form.employeeAssetProductTable.getValue()){
				
				if(bean.isStockUpdated()==false){
					EmployeeAssetBean obj=new EmployeeAssetBean();
					obj.setAssetId(bean.getAssetId());
					obj.setAssetCode(bean.getAssetCode());
					obj.setAssetName(bean.getAssetName());
					obj.setAssetCategory(bean.getAssetCategory());
					obj.setPerUnitPrice(bean.getPerUnitPrice());
					obj.setPrice(bean.getPrice());
					obj.setQty(bean.getQty());
					obj.setUom(bean.getUom());
					obj.setDate(bean.getDate());
					obj.setRemark(bean.getRemark());
//					System.out.println("Get Remark into Issue :- "+bean.getReturnAssetremark());
//					obj.setReturnAssetremark(bean.getReturnAssetremark());
					obj.setStatus("Issue");
					list.add(obj);
				}
			}
			if(list.size()!=0){
				assetPopup.table.setValue(list);
				assetPopup.showPopUp();	
			}else{
				form.showDialogMessage("No material to issue.");
			}
		}
		
		if(text.equals("Return")){
			assetPopup.setTransactionType("Return");
			assetPopup.setBranch(form.oblBranch.getValue());
			assetPopup.setEmpId(form.pic.getEmployeeId());
			assetPopup.setEmpName(form.pic.getEmployeeName());
			assetPopup.table.getDataprovider().getList().clear();
			List<EmployeeAssetBean> list=new ArrayList<EmployeeAssetBean>();
			
			int srNo=1;
			for(EmployeeAssetBean bean:form.employeeAssetProductTable.getValue()){
				bean.setSrNo(srNo);
				srNo++;
				if(bean.isStockUpdated()==true&&bean.getQty()!=0){
					EmployeeAssetBean obj=new EmployeeAssetBean();
					obj.setAssetId(bean.getAssetId());
					obj.setAssetCode(bean.getAssetCode());
					obj.setAssetName(bean.getAssetName());
					obj.setAssetCategory(bean.getAssetCategory());
					obj.setPerUnitPrice(bean.getPerUnitPrice());
					obj.setPrice(bean.getPrice());
					obj.setQty(bean.getQty());
					obj.setUom(bean.getUom());
					obj.setDate(bean.getDate());
					obj.setRemark(bean.getRemark());
//					System.out.println("Get Remark into Return :- "+bean.getReturnAssetremark());
//					obj.setReturnAssetremark(bean.getReturnAssetremark());
					obj.setStatus("Return");
					
					obj.setSrNo(bean.getSrNo());
					list.add(obj);
				}
			}
			if(list.size()!=0){
				assetPopup.table.setValue(list);
				assetPopup.showPopUp();	
			}else{
				
				form.showDialogMessage("No material to return.");
			}
		}
		
		if(text.equals("Scrap")){
			assetPopup.setTransactionType("Scrap");
			assetPopup.setBranch(form.oblBranch.getValue());
			assetPopup.setEmpId(form.pic.getEmployeeId());
			assetPopup.setEmpName(form.pic.getEmployeeName());
			assetPopup.table.getDataprovider().getList().clear();
			List<EmployeeAssetBean> list=new ArrayList<EmployeeAssetBean>();
			for(EmployeeAssetBean bean:form.employeeAssetProductTable.getValue()){
				
				if(bean.isStockUpdated()==true&&bean.getQty()!=0){
					EmployeeAssetBean obj=new EmployeeAssetBean();
					obj.setAssetId(bean.getAssetId());
					obj.setAssetCode(bean.getAssetCode());
					obj.setAssetName(bean.getAssetName());
					obj.setAssetCategory(bean.getAssetCategory());
					obj.setPerUnitPrice(bean.getPerUnitPrice());
					obj.setPrice(bean.getPrice());
					obj.setQty(bean.getQty());
					obj.setUom(bean.getUom());
					obj.setStatus("Scrap");
					list.add(obj);
				}
			}
			if(list.size()!=0){
				assetPopup.table.setValue(list);
				assetPopup.showPopUp();	
			}else{
				form.showDialogMessage("No material to Scrap.");
			}
		}
	}
	@Override
	public void reactOnPrint() {
//		if(form.pic!=null){
		//	final String url = GWT.getModuleBaseURL() + "processpayslip" + "?Id="+ model.getId();
	//		Window.open(url, "test", "enabled");
//		}
	}

	@Override
	public void reactOnEmail() {

	}


	
	@Override
	protected void makeNewModel() {
		model = new EmployeeAsset();

	}

	

	public void setModel(EmployeeAsset entity) {
		model = entity;
	}

	public static EmployeeAssetAllocationForm initialize() {

		EmployeeAssetAllocationForm form = new EmployeeAssetAllocationForm();

		EmployeeAssetTableProxy gentable = new EmployeeAssetTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		EmployeeAssetPresenterSearchProxy.staticSuperTable = gentable;
		SearchPopUpScreen<EmployeeAsset> searchpopup = new EmployeeAssetPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		EmployeeAssetAllocationPresenter presenter = new EmployeeAssetAllocationPresenter(form,new EmployeeAsset());
		AppMemory.getAppMemory().stickPnel(form);
		return form;

	}
	
	@Override
	public void onRowCountChange(RowCountChangeEvent event) {

	}


	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.EmployeeAsset")
	public static  class EmployeeAssetSearch extends SearchPopUpScreen<EmployeeAsset>{

		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}}


	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		// TODO Auto-generated method stub
		if (event.getSource().equals(form.pic.getId())||event.getSource().equals(form.pic.getName())||event.getSource().equals(form.pic.getPhone())) {
			form.retriveEmployeeAsset();
		}
	};
	
	private void reactToNew()
	{
		form.setToNewState();
		this.initialize();
		form.toggleAppHeaderBarMenu();
	}
	public void reactOnDownload() {
		ArrayList<EmployeeAsset> assetallocationArray = new ArrayList<EmployeeAsset>();
		List<EmployeeAsset> list = (List<EmployeeAsset>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		assetallocationArray.addAll(list);
		csvservice.setAssetallocationlist(assetallocationArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 157;
				Window.open(url, "test", "enabled");
			}
		});
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		super.onClick(event);
		
		if(event.getSource()==assetPopup.getLblOk()){
			if(assetPopup.validate()){
				GeneralServiceAsync genService = GWT.create(GeneralService.class);
				form.showWaitSymbol();
				System.out.println("Transaction Type : "+assetPopup.getTransactionType());
//				genService.updateEmployeeAssetStock(itemList, assetPopup.getTransactionType(), new AsyncCallback<Void>() {
//
//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//						form.hideWaitSymbol();
//						assetPopup.hidePopUp();
//						form.showDialogMessage("Unexpected Error!");
//					}
//
//					@Override
//					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
//						form.hideWaitSymbol();
//						assetPopup.hidePopUp();
						
						final List<EmployeeAssetBean> list=assetPopup.table.getValue();
						if(assetPopup.getTransactionType().equals("Return")||assetPopup.getTransactionType().equals("Scrap")){
							for(EmployeeAssetBean bean:list){
								for(EmployeeAssetBean obj:form.employeeAssetProductTable.getValue()){
									if(bean.getAssetId()==obj.getAssetId()&&(bean.getStatus().equals("Return")||bean.getStatus().equals("Scrap"))){
										if(bean.getSrNo()==obj.getSrNo()){
											obj.setQty(obj.getQty()-bean.getDeductQty());
										}
//										obj.setQty(obj.getQty()-bean.getDeductQty());
									}
								}
							}
						}
						form.empAssetMovementTbl.getDataprovider().getList().addAll(list);
						model.setEmpAssetHisList(form.empAssetMovementTbl.getValue());
						model.setTransactionType(assetPopup.getTransactionType());
						
						ArrayList<EmployeeAssetBean> assetList=new ArrayList<EmployeeAssetBean>();
						for(EmployeeAssetBean bean:list){
							for(EmployeeAssetBean obj:form.employeeAssetProductTable.getValue()){
								/**
								 * @author Anil , Date : 15-10-2019
								 */
								if(bean.getAssetId()==obj.getAssetId()){
									if(obj.isStockUpdated()==false){
										obj.setStockUpdated(true);
									}
									assetList.add(obj);
								}
							}
						}
						form.employeeAssetProductTable.getTable().redraw();
//						model.setEmpAssetList(assetList);
						
						model.setEmpAssetList(form.employeeAssetProductTable.getValue());
						
//						for(EmployeeAssetBean bean1:model.getEmpAssetHisList()){
//							for(EmployeeAssetBean bean2:model.getEmpAssetList()){
//								if(bean1.getAssetId()==bean2.getAssetId()){
//									bean2.setStockUpdated(true);
//								}
//							}
//						}
						
						service.save(model, new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								form.hideWaitSymbol();
								assetPopup.hidePopUp();
							}

							@Override
							public void onSuccess(ReturnFromServer result) {
								// TODO Auto-generated method stub
								/**
								 * @author Anil
								 * @since 11-11-2020
								 * Setting stock update flag
								 */
								for(EmployeeAssetBean bean:list){
									bean.setStockUpdated(true);
								}
								
								form.hideWaitSymbol();
								assetPopup.hidePopUp();
								form.showDialogMessage("Stock updated successfully.");
							}
						});
						
						
//					}
//				});
				
			}
		}
		
		if(event.getSource()==assetPopup.getLblCancel()){
			assetPopup.hidePopUp();
		}
	}
	
	

}
