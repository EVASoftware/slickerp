package com.slicktechnologies.client.views.humanresource.employeeasset;

import java.util.Date;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;


public class EmployeeAssetProductTable extends SuperTable<EmployeeAssetBean> {

	TextColumn<EmployeeAssetBean> prodNameColumn , prodViewQtyColumn , prodPriceColumn , prodPerUnitPrice;
	Column<EmployeeAssetBean , String> prodEditQtyColumn;
	Column<EmployeeAssetBean,String> deleteColumn;
	Column<EmployeeAssetBean,Date> fromDateCol;
	TextColumn<EmployeeAssetBean> viewFromDateCol;
	Column<EmployeeAssetBean,Date> toDateCol;
	TextColumn<EmployeeAssetBean> viewDateToCol;
	
	@Override
	public void createTable() {
		createColumnprodNameColumn();
		createEditQtyColumn(); 
		createPerUnitPrice();
		createPriceColumn();
		createFromDateColumn();
		createToDateColumn();
		createColumndeleteColumn();
		addFieldUpdater();
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		createFieldUpdaterQtyColumn();
		addFieldUpdaterOnCreateFromDateColumn();
		addFieldUpdaterOnCreateToDateColumn();
		createFieldUpdaterdeleteColumn();
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		 for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
		if(state == true){
		//	createColumnprodCodeColumn();
			createColumnprodNameColumn();
			createEditQtyColumn(); 
		//	createUnitOfMeasuremetColumn();
			createPerUnitPrice();
			createPriceColumn();
			createFromDateColumn();
			createToDateColumn();
			createColumndeleteColumn();
			addFieldUpdater();
		}else{
			table.setWidth("100%");
		//	createColumnprodCodeColumn();
			createColumnprodNameColumn();
			createViewQtyColumn(); 
			//createUnitOfMeasuremetColumn();
			createPerUnitPrice();
			createPriceColumn();
			createViewFromDateColumn();
			createViewToDateColumn();
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	protected void createColumnprodNameColumn()
	{
	
		prodNameColumn=new TextColumn<EmployeeAssetBean>()
				{
			@Override
			public String getValue(EmployeeAssetBean object)
			{
				return object.getAssetName();
			}
				};
				table.addColumn(prodNameColumn,"Asset Name");
			//	table.setColumnWidth(prodNameColumn, 100,Unit.PX);
	}
	protected void createEditQtyColumn(){
		EditTextCell editCell = new EditTextCell();
		prodEditQtyColumn = new Column<EmployeeAssetBean, String>(editCell) {
			@Override
			public String getValue(EmployeeAssetBean object) {
				if (object.getQty() == 0) {
					return 0 + "";
				} else {
					return object.getQty() + "";
				}
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,EmployeeAssetBean object, NativeEvent event) {
				if(object.isStockUpdated()==false){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(prodEditQtyColumn, "# Qunatity");
	}
	
	
	protected void createViewQtyColumn()
	{
		
		prodViewQtyColumn=new TextColumn<EmployeeAssetBean>()
				{
			@Override
			public String getValue(EmployeeAssetBean object)
			{
				if(object.getQty()==0){
					return 0+"";}
				else{
					return object.getQty()+"";}
			}
				};
				table.addColumn(prodViewQtyColumn,"Quantity");
			//	table.setColumnWidth(prodViewQtyColumn, 100,Unit.PX);
	}


	protected void createPriceColumn(){
		prodPriceColumn = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				
				return object.getPrice()+"";
					
			}
		};
		table.addColumn(prodPriceColumn, "Total");
		//table.setColumnWidth(prodPriceColumn, 100, Unit.PX);
	}
	//createPerUnitPrice
	protected void createPerUnitPrice(){
		prodPerUnitPrice = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				
				return object.getPerUnitPrice()+"";
					
			}
		};
		table.addColumn(prodPerUnitPrice, "Price");
		//table.setColumnWidth(prodPriceColumn, 100, Unit.PX);
	}
	protected void createColumndeleteColumn(){
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<EmployeeAssetBean, String>(btnCell) {
			@Override
			public String getValue(EmployeeAssetBean object) {
				return "Delete";
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,EmployeeAssetBean object, NativeEvent event) {
				if(object.isStockUpdated()==false||object.getQty()==0){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(deleteColumn, "Delete");
		// table.setColumnWidth(deleteColumn, 100,Unit.PX);
	}
	protected void createFieldUpdaterQtyColumn()
	{
		prodEditQtyColumn.setFieldUpdater(new FieldUpdater<EmployeeAssetBean, String>()
				{
			@Override
			public void update(int index,EmployeeAssetBean object,String value)
			{
				try
				{
					Double val1=Double.parseDouble(value.trim());
					Double price = object.getPerUnitPrice();
					object.setPrice(val1 * price);
					object.setQty(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				
				}
				catch(Exception e)
				{
				}
				table.redrawRow(index);
			}
				});
	}
	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<EmployeeAssetBean,String>()
				{
			@Override
			public void update(int index,EmployeeAssetBean object,String value)
			{
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	private void createFromDateColumn() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		fromDateCol = new Column<EmployeeAssetBean, Date>(date) {
			@Override
			public Date getValue(EmployeeAssetBean object) {
				if (object.getFromDate() != null) {
					return fmt.parse(fmt.format(object.getFromDate()));
				} else {
					return fmt.parse(fmt.format(new Date()));
					//return null;
				}
			}
		};
		table.addColumn(fromDateCol, "#From Date");
		//table.setColumnWidth(fromDateCol, 100, Unit.PX);
		
	}
	protected void addFieldUpdaterOnCreateFromDateColumn()
	{
		fromDateCol.setFieldUpdater(new FieldUpdater<EmployeeAssetBean, Date>() {
			@Override
			public void update(int index, EmployeeAssetBean object, Date value) {
				object.setFromDate(value);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}
	
	
	private void createToDateColumn() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		toDateCol = new Column<EmployeeAssetBean, Date>(date) {
			@Override
			public Date getValue(EmployeeAssetBean object) {
				if (object.getToDate() != null) {
					return fmt.parse(fmt.format(object.getToDate()));
				} else {
					return fmt.parse(fmt.format(new Date()));
					//return null;
				}
			}
		};
		table.addColumn(toDateCol, "#To Date");
		//table.setColumnWidth(toDateCol, 100, Unit.PX);
		
	}
	protected void addFieldUpdaterOnCreateToDateColumn()
	{
		toDateCol.setFieldUpdater(new FieldUpdater<EmployeeAssetBean, Date>() {
			@Override
			public void update(int index, EmployeeAssetBean object, Date value) {
				object.setToDate(value);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}
	
	protected void createViewFromDateColumn() {
		viewFromDateCol = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				if (object.getFromDate() != null) {
					return AppUtility.parseDate(object.getFromDate());
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(viewFromDateCol, "From Date");
		//table.setColumnWidth(viewFromDateCol, 100, Unit.PX);
	}
	
	protected void createViewToDateColumn() {
		viewDateToCol = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {
				if (object.getToDate() != null) {
					return AppUtility.parseDate(object.getToDate());
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(viewDateToCol, "To Date");
		//table.setColumnWidth(viewDateToCol, 100, Unit.PX);
	}
	
}
