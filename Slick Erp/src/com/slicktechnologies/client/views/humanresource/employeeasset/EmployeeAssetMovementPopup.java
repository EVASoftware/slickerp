package com.slicktechnologies.client.views.humanresource.employeeasset;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.InventoryTransactionLineItem;
import com.slicktechnologies.shared.common.inventory.UpdateStock;

public class EmployeeAssetMovementPopup extends PopupScreen implements Handler  {

	EmployeeAssetMovementTable table;
	String transactionType="";
	int empId;
	String branch;
	String empName;
	FormField fgroupingPopup;
	String remark;
	
	public EmployeeAssetMovementPopup() {
		super();
		table.connectToLocal();
		table.getTable().setHeight("360px");
		getPopup().getElement().addClassName("setWidth");
		
		table.getTable().addRowCountChangeHandler(this);		
		createGui();
	}
	
	private void initializeWidget() {
		transactionType="";
		table=new EmployeeAssetMovementTable();
	}
	
	
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidget();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		fgroupingPopup=fbuilder.setlabel(transactionType+" Employee Asset").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", table.getTable());
		FormField ftable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		FormField[][] formfield = {  
				{fgroupingPopup},
				{ftable},
			
		};
		this.fields=formfield;
	}

	public EmployeeAssetMovementTable getTable() {
		return table;
	}

	public void setTable(EmployeeAssetMovementTable table) {
		this.table = table;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	
	
	
	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public FormField getFgroupingPopup() {
		return fgroupingPopup;
	}

	public void setFgroupingPopup(FormField fgroupingPopup) {
		this.fgroupingPopup = fgroupingPopup;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public void setPopupHeading(String transactionType){
		this.transactionType=transactionType;
		if(fgroupingPopup!=null){
			fgroupingPopup.setLabel(transactionType+" Employee Asset");
		}
	}
	
	public ArrayList<InventoryTransactionLineItem> getItemTransactionList(){
		ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
		for(EmployeeAssetBean empAssBean:table.getValue()){
			InventoryTransactionLineItem item=new InventoryTransactionLineItem();
			item.setCompanyId(UserConfiguration.getCompanyId());
			item.setBranch(branch);
			item.setDocumentType("Employee Asset Allocation");
			item.setDocumentId(empId);
			item.setDocumentDate(new Date());
			item.setDocumnetTitle(empName);
			item.setRemark(remark);
			item.setProdId(empAssBean.getAssetId());
			if(transactionType.equals("Issue")){
				item.setUpdateQty(empAssBean.getQty());
				item.setOperation(AppConstants.SUBTRACT);
			}else if(transactionType.equals("Return")){
				item.setUpdateQty(empAssBean.getDeductQty());
				item.setOperation(AppConstants.ADD);
			}else if(transactionType.equals("Scrap")){
				item.setUpdateQty(empAssBean.getDeductQty());
				item.setOperation(AppConstants.SUBTRACT);
			}
			
			item.setWarehouseName(empAssBean.getWarehouseName());
			item.setStorageLocation(empAssBean.getStorageLocName());
			item.setStorageBin(empAssBean.getStorageLocBin());
			item.setProductName(empAssBean.getAssetName());
			item.setProductCode(empAssBean.getAssetCode());
			item.setProductUOM(empAssBean.getUom());
			itemList.add(item);
		}
		return itemList;
	}
	
	public boolean validate(){
		if(table.getValue()!=null &&table.getValue().size()!=0){
			if(transactionType.equals("Issue")){
				for(EmployeeAssetBean bean:table.getValue()){
					if(bean.getQty()>bean.getAvailableQty()){
						showDialogMessage("Available quantity is less than issue quantity.");
						return false;
					}
//				if(bean.getReturnAssetremark().equals("")){
//					showDialogMessage("Remark Should not be empty");
//					return false;
//				}
			}
			}
			
			if(transactionType.equals("Return")||transactionType.equals("Scrap")){
				for(EmployeeAssetBean bean:table.getValue()){
					if(bean.getDeductQty()==0){
						showDialogMessage("Deduction quantity can not be zero. "+bean.getAssetId());
						return false;
					}
					if(bean.getWarehouseName()==null||bean.getWarehouseName().equals("")
							||bean.getStorageLocName()==null||bean.getStorageLocName().equals("")
							||bean.getStorageLocBin()==null||bean.getStorageLocBin().equals("")){
						showDialogMessage("Please select warehouse details.");
						return false;
					}
//					if(bean.getReturnAssetremark().equals("")){
//						showDialogMessage("Remark Should not be empty");
//						return false;
//					}
				}
			}
		}else{
			showDialogMessage("Nothing to update.");
			hidePopUp();
		}
		return true;
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		// TODO Auto-generated method stub
//		table.getTable().redraw();
		table.setEnable(true);
	}

}
