package com.slicktechnologies.client.views.humanresource.employeeasset;

import java.util.Vector;

import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.humanresource.employeeasset.EmployeeAssetAllocationPresenter.EmployeeAssetSearch;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.hrtaxstructure.EmployeeInvestment;

public class EmployeeAssetPresenterSearchProxy extends EmployeeAssetSearch {

	
	public EmployeeInfoComposite personInfo;
	public DateComparator startDateComparator;
	public DateComparator endDateComparator;

	public Object getVarRef(String varName)
	{
		
		if(varName.equals("personInfo"))
			return this.personInfo;
		return null ;
	}
	public EmployeeAssetPresenterSearchProxy()
	{
		super();
		getPopup().setHeight("300px");
		createGui();
	}
	public void initWidget()
	{
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
		
		
		 startDateComparator= new DateComparator("empAssetList.fromDate",new EmployeeAsset());
		  endDateComparator= new DateComparator("empAssetList.toDate",new EmployeeAsset());
	
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
	
		builder = new FormFieldBuilder("From Date (Start Date)",startDateComparator.getFromDate());
		FormField fstartDateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (Start Date)",startDateComparator.getToDate());
		FormField fstartDateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("From Date (End Date)",endDateComparator.getFromDate());
		FormField fendDateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (End Date)",endDateComparator.getToDate());
		FormField fendDateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		this.fields=new FormField[][]{
				{fpersonInfo},
				{fstartDateComparatorfrom,fstartDateComparatorto},
				{fendDateComparatorfrom,fendDateComparatorto}
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
			if(!personInfo.getId().getValue().equals(""))
			{  
				temp=new Filter();
				temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
				temp.setQuerryString("empId");
				filtervec.add(temp);
			}

			if(!(personInfo.getName().getValue().equals("")))
			{
				temp=new Filter();
				temp.setStringValue(personInfo.getName().getValue());
				temp.setQuerryString("empName");
				filtervec.add(temp);
			}
			
			if(startDateComparator.getValue()!=null){
				filtervec.addAll(startDateComparator.getValue());
			}
			
			if(endDateComparator.getValue()!=null){
				filtervec.addAll(endDateComparator.getValue());
			}
//			if(!personInfo.getPhone().getValue().equals(""))
//			{
//				temp=new Filter();
//				temp.setLongValue(personInfo.getCellNumber());
//				temp.setQuerryString("empCellNo");
//				filtervec.add(temp);
//			}
			
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new EmployeeAsset());
		return querry;
	}
	
	
	@Override
	public boolean validate() {
		
		return super.validate();
	}

}
