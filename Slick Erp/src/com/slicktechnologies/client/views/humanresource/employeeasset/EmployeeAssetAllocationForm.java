package com.slicktechnologies.client.views.humanresource.employeeasset;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.humanresource.employeeasset.EmployeeAssetProductTable;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.hrtaxstructure.EmployeeInvestment;
import com.slicktechnologies.shared.common.hrtaxstructure.EmployeeInvestmentBean;
import com.slicktechnologies.shared.common.hrtaxstructure.Investment;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

public class EmployeeAssetAllocationForm extends FormScreen<EmployeeAsset> implements ClickHandler{
	
	EmployeeInfoComposite pic;
	ProductInfoComposite productInfoComposite;
	Button btnAdd;
	EmployeeAssetProductTable employeeAssetProductTable;
	EmployeeAsset employeeAsset;
	ObjectListBox<CompanyAsset> olbAsset;
	
	GenricServiceAsync async = GWT.create(GenricService.class);
	
	/**
	 * @author Anil , Date : 10-07-2019
	 */
	EmployeeAssetMovementTable empAssetMovementTbl;
	ObjectListBox<Branch> oblBranch;
	
	public EmployeeAssetAllocationForm(){
		super(FormStyle.DEFAULT);
		btnAdd.addClickHandler(this);
		createGui();
	}
	
	public EmployeeAssetAllocationForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		btnAdd.addClickHandler(this);
		createGui();
		
	}

	private void initalizeWidget()
	{
		pic=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		/**
		 * @author Anil , Date : 09-07-2019
		 * Setting employee label
		 */
		pic.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		pic.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		pic.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
		
		
		MyQuerry querry1=new MyQuerry();
		querry1.setQuerryObject(new ItemProduct());
		productInfoComposite=new ProductInfoComposite(querry1); 
		
		/**
		 * @author Anil @since 12-08-2021
		 * As per nitin sir's requirement for First care this should be asset in label instead of product
		 */
		productInfoComposite.getProductID().getHeaderLabel().setText("Asset ID");
		productInfoComposite.getProductCode().getHeaderLabel().setText("Asset Code");
		productInfoComposite.getProductName().getHeaderLabel().setText("Asset Name");
		
		btnAdd = new Button("ADD");
		employeeAssetProductTable = new EmployeeAssetProductTable();
		employeeAssetProductTable.connectToLocal();		
		
		olbAsset = new ObjectListBox<CompanyAsset>();
		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		querry.setQuerryObject(new CompanyAsset());
		olbAsset.MakeLive(querry);
		
		empAssetMovementTbl=new EmployeeAssetMovementTable();
		empAssetMovementTbl.connectToLocal();
		
		oblBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(oblBranch);
		
		oblBranch.getElement().getStyle().setWidth(327, Unit.PX);
		
	}

	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames=new String[]{AppConstants.NEW,"Issue","Return"};
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeInformation=fbuilder.setlabel("Employee Asset Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",pic);
		FormField fpic= fbuilder.setMandatory(true).setMandatoryMsg("Employee information is mandatory").setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",productInfoComposite);
		FormField fproductInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
			
		fbuilder = new FormFieldBuilder("",btnAdd);
		FormField fbtnAdd= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",employeeAssetProductTable.getTable());
		FormField fTable= fbuilder.setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Asset", olbAsset);
		FormField folbAsset= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgrpingEmpAssetHis=fbuilder.setlabel("Employee Asset History").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",empAssetMovementTbl.getTable());
		FormField fempAssetMovementTbl= fbuilder.setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Branch", oblBranch);
		FormField foblBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory.").setRowSpan(0).setColSpan(0).build();
		InlineLabel lbl=new InlineLabel();
		lbl.setVisible(false);
		fbuilder = new FormFieldBuilder("",lbl);
		FormField flbl= fbuilder.setRowSpan(0).setColSpan(3).build();
		
		FormField[][] formfield = {   
				{fgroupingEmployeeInformation},
				{fpic},
				{foblBranch,flbl},
				{fproductInfoComposite, fbtnAdd},
//				{folbAsset , fbtnAdd },
				{fTable},
				{fgrpingEmpAssetHis},
				{fempAssetMovementTbl}
				
		};

		this.fields=formfield;		

	}


	@Override
	public void updateModel(EmployeeAsset model) 
	{
		if(pic.getValue()!=null)
		{
			model.setEmpId(pic.getEmployeeId());
			model.setEmpName(pic.getEmployeeName());
			model.setEmpCellNo(pic.getCellNumber());

		}
		if(employeeAssetProductTable.getDataprovider().getList().size()>0){
			List<EmployeeAssetBean> list = employeeAssetProductTable.getDataprovider().getList();
			model.setEmpAssetList(list);
		}
		
		if(oblBranch.getValue()!=null){
			model.setBranch(oblBranch.getValue());
		}
		
		if(empAssetMovementTbl.getValue()!=null){
			model.setEmpAssetHisList(empAssetMovementTbl.getValue());
		}
		
		model.setTransactionType("");
		
		employeeAsset=model;
		
		presenter.setModel(model);


	}


	@Override
	public void updateView(EmployeeAsset view) 
	{
		employeeAsset=view;
		
		pic.setEmployeeId(view.getEmpId());
		if(view.getEmpName()!=null)
			pic.setEmployeeName(view.getEmpName());	
			pic.setCellNumber(view.getEmpCellNo());
		if(view.getEmpAssetList().size()>0){
			employeeAssetProductTable.getDataprovider().setList(view.getEmpAssetList());
		}
		
		if(view.getBranch()!=null){
			oblBranch.setValue(view.getBranch());
		}
		
		if(view.getEmpAssetHisList()!=null){
			empAssetMovementTbl.setValue(view.getEmpAssetHisList());
		}
		
		presenter.setModel(view);
	}

	

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION) || text.equals("Search"))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false); 
				}
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Search")/*||text.equals("Print")*/||text.equals(AppConstants.NAVIGATION)||text.contains("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.EMPLOYEEINVESTMENT,LoginPresenter.currentModule.trim());
	}

	@Override
	public void setCount(int count)
	{
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		this.employeeAssetProductTable.setEnable(state);
		this.empAssetMovementTbl.setEnable(false);
		
	}

	public EmployeeInfoComposite getPic() {
		return pic;
	}

	public void setPic(EmployeeInfoComposite pic) {
		this.pic = pic;
	}

	public ProductInfoComposite getProductInfoComposite() {
		return productInfoComposite;
	}

	public void setProductInfoComposite(ProductInfoComposite productInfoComposite) {
		this.productInfoComposite = productInfoComposite;
	}

	public Button getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(Button btnAdd) {
		this.btnAdd = btnAdd;
	}

	public EmployeeAssetProductTable getEmployeeAssetProductTable() {
		return employeeAssetProductTable;
	}

	public void setEmployeeAssetProductTable(
			EmployeeAssetProductTable employeeAssetProductTable) {
		this.employeeAssetProductTable = employeeAssetProductTable;
	}

	public EmployeeAsset getEmployeeAsset() {
		return employeeAsset;
	}

	public void setEmployeeAsset(EmployeeAsset employeeAsset) {
		this.employeeAsset = employeeAsset;
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new EmployeeInvestment();
		model=employeeAsset;
		AppUtility.addDocumentToHistoryTable(AppConstants.HRMODULE,"Employee Asset Allocation", employeeAsset.getCount(), employeeAsset.getEmpId(),employeeAsset.getEmpName(),employeeAsset.getEmpCellNo(), false, model, null);		
	}
	

	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
			
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		boolean val= super.validate();
		if(val==false)
			return val;
		if(employeeAssetProductTable.getDataprovider().getList().size()==0){
			showDialogMessage("Employee Asset List can't be empty");
			return false;
		}
		return true;
	}


	@Override
	public void clear() {
		super.clear();
		empAssetMovementTbl.connectToLocal();
		
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(btnAdd)){
//			productDetails(this.productInfoComposite.getProdCode().getValue().trim(),this.productInfoComposite.getProdID().getValue().trim());
//			this.productInfoComposite.clear();
			
			/**
			 * @author Anil , Date : 10-07-2019
			 * loading asset from product master instead of Asset 
			 */
//			if(olbAsset.getSelectedIndex()!=0){
//				CompanyAsset asset = olbAsset.getSelectedItem();
//				EmployeeAssetBean bean = getEmployeeBeanObject(asset);
//				if(employeeAssetProductTable.getDataprovider().getList().size()>0){
//					List<EmployeeAssetBean> list = employeeAssetProductTable.getDataprovider().getList();
//					list.add(bean);
//					employeeAssetProductTable.getDataprovider().setList(list);
//					}else{
//						employeeAssetProductTable.getDataprovider().getList().add(bean);
//					}
//				this.olbAsset.setSelectedIndex(0);
//			}else{
//				showDialogMessage("Please select asset.");
//			}
			
			if(productInfoComposite.getValue()!=null){
				EmployeeAssetBean bean=new EmployeeAssetBean();
				bean.setAssetId(productInfoComposite.getValue().getProdID());
				bean.setAssetCode(productInfoComposite.getValue().getProductCode());
				bean.setAssetName(productInfoComposite.getProdNameValue());
				bean.setPerUnitPrice(productInfoComposite.getValue().getProductPrice());
				bean.setPrice(productInfoComposite.getValue().getProductPrice());
				bean.setQty(1.0);
				bean.setAssetCategory(productInfoComposite.getValue().getProductCategory());
				bean.setUom(productInfoComposite.getValue().getUnitofMeasure());
				/**
				 * @author Vijay Chougule Date :- 27-07-2020
				 * Des :- Removed Duplicate product validation as per the requirement raised by Rahul
				 */
//				if(employeeAssetProductTable.getDataprovider().getList().size()>0){
//					List<EmployeeAssetBean> list = employeeAssetProductTable.getDataprovider().getList();
//					for(EmployeeAssetBean empAssBean:list){
//						if(empAssBean.getAssetId()==bean.getAssetId()){
//							showDialogMessage("Asset already added.");
//							return;
//						}
//					}
//					list.add(bean);
//					employeeAssetProductTable.getDataprovider().setList(list);
//				}else{
//					employeeAssetProductTable.getDataprovider().getList().add(bean);
//				}
				employeeAssetProductTable.getDataprovider().getList().add(bean);

			}else{
				showDialogMessage("Please select product.");
			}
			
			
		}
	}

	public void retriveEmployeeAsset() {
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = new Filter();
		
		filter.setQuerryString("empId");
		filter.setIntValue(pic.getEmployeeId());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new EmployeeAsset());

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							showDialogMessage("Employee Asset Already Exist!");
							pic.clear();
						}else{
							oblBranch.setValue(pic.getBranch());
						}
						
						
					}
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("No Record Found");
					}
				});
	}

	private void productDetails(String pcode,String productId){

		final GenricServiceAsync genasync=GWT.create(GenricService.class);
		
		Vector<Filter> filtervec=new Vector<Filter>();
		int prodId=Integer.parseInt(productId);
		
		final MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("productCode");
		filter.setStringValue(pcode.trim());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
		showWaitSymbol();
		 Timer timer=new Timer() 
    	 {
				@Override
				public void run() {
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("An Unexpected error occurred!");
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					
					if(result.size()==0)
					{
						showDialogMessage("Please check whether product status is active or not!");
					}
					SalesLineItem lis=null;
					
					for(SuperModel model:result)
					{
						SuperProduct superProdEntity = (SuperProduct)model;
						lis=AppUtility.ReactOnAddProductComposite(superProdEntity);
						lis.setQuantity(1.0);
					}
//					if(employeeAssetProductTable.getDataprovider().getList().size()>0){
//						List<EmployeeAssetBean> list = employeeAssetProductTable.getDataprovider().getList();
//						list.add(lis);
//						employeeAssetProductTable.getDataprovider().setList(list);
// 					}else{
// 						employeeAssetProductTable.getDataprovider().getList().add(lis);
// 					}
 						
					}
				 });
				hideWaitSymbol();
 				}
	 	  };
          timer.schedule(3000); 
	
	}
	private EmployeeAssetBean getEmployeeBeanObject(CompanyAsset asset){
		EmployeeAssetBean bean = new EmployeeAssetBean();
		bean.setAssetName(asset.getName());
		bean.setPerUnitPrice(asset.getPrice());
		bean.setPrice(asset.getPrice());
		bean.setQty(1.0);
		bean.setAssetCategory(asset.getCategory());
		return bean;
	}

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		employeeAssetProductTable.getTable().redraw();
		empAssetMovementTbl.getTable().redraw();
	}
	
	

}

