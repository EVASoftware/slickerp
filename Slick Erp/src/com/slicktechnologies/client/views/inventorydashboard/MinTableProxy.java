package com.slicktechnologies.client.views.inventorydashboard;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;

public class MinTableProxy extends SuperTable<MaterialIssueNote>{TextColumn<MaterialIssueNote> getColumnMinId;
Column<MaterialIssueNote, Date> getColumnMinDate;
Column<MaterialIssueNote, Date> getColumnMinReqDate;
TextColumn<MaterialIssueNote> getColumnMinTitle;
TextColumn<MaterialIssueNote> getColumnMinCategory;
TextColumn<MaterialIssueNote> getColumnMinType;
TextColumn<MaterialIssueNote> getColumnMinSoId;
TextColumn<MaterialIssueNote> getColumnMinWoId;
TextColumn<MaterialIssueNote> getColumnMinBranch;
TextColumn<MaterialIssueNote> getColumnMinIssuedBy;
TextColumn<MaterialIssueNote> getColumnMinApprover;
TextColumn<MaterialIssueNote> getColumnMinStatus;

public MinTableProxy() {
	super();
}

@Override
public void createTable() {
	addColumnMinId();
	addColumnMinTitle();
	addColumnMinDate();
	addColumnMinWoId();
	addColumnMinSoId();
	addColumnMinCategory();
	addColumnMinType();
	addColumnMinIssuedBy();
	addColumnMinApprover();
	addColumnMinBranch();
	addColumnMinStatus();
}

/**********************************************************************************************************************/
private void addColumnMinId() {
	getColumnMinId = new TextColumn<MaterialIssueNote>() {
		@Override
		public String getValue(MaterialIssueNote object) {
			if (object.getCount() == -1)
				return "N.A";
			else
				return object.getCount() + "";
		}
	};
	table.addColumn(getColumnMinId, "Id");
	table.setColumnWidth(getColumnMinId,90,Unit.PX);
	getColumnMinId.setSortable(true);
}

private void addColumnMinDate() {
	DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
	DatePickerCell date = new DatePickerCell(fmt);
	
	final Date sub7Days=new Date();
	System.out.println("Todays Date ::: "+sub7Days);
	CalendarUtil.addDaysToDate(sub7Days,-7);
	System.out.println("Date - 7 Days ::: "+sub7Days);
	sub7Days.setHours(23);
	sub7Days.setMinutes(59);
	sub7Days.setSeconds(59);
	System.out.println("Converted Date ::: "+sub7Days);
	
	getColumnMinDate = new Column<MaterialIssueNote, Date>(date) {
		@Override
		public String getCellStyleNames(Context context, MaterialIssueNote object) {
			if((object.getMinDate().before(sub7Days))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Approved")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
		
		@Override
		public Date getValue(MaterialIssueNote object) {
			if (object.getMinDate()!= null) {
				return object.getMinDate();
			}
			return null;
		}
	};
	table.addColumn(getColumnMinDate, "Date");
	table.setColumnWidth(getColumnMinDate,120,Unit.PX);
	getColumnMinDate.setSortable(true);
}

//private void addColumnMinRequiredDate() {
//	DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
//	DatePickerCell date = new DatePickerCell(fmt);
//	getColumnMinReqDate = new Column<MaterialIssueNote, Date>(date) {
//		@Override
//		public Date getValue(MaterialIssueNote object) {
//			if (object.getMin!= null) {
//				return object.getMrnRequiredDate();
//			}
//			return null;
//		}
//	};
//	table.addColumn(getColumnMinReqDate, "Material Required Date");
//	getColumnMinReqDate.setSortable(true);
//}

private void addColumnMinTitle() {
	getColumnMinTitle = new TextColumn<MaterialIssueNote>() {
		@Override
		public String getValue(MaterialIssueNote object) {
			if (object.getMinTitle()!=null){
				return object.getMinTitle();
			}
			return null;
		}
	};
	table.addColumn(getColumnMinTitle, "Title");
	table.setColumnWidth(getColumnMinTitle,90,Unit.PX);
	getColumnMinTitle.setSortable(true);
}

private void addColumnMinCategory() {
	getColumnMinCategory = new TextColumn<MaterialIssueNote>() {
		@Override
		public String getValue(MaterialIssueNote object) {
			if (object.getMinCategory()!=null){
				return object.getMinCategory();
			}
			else{
				return "";
			}
		}
	};
	table.addColumn(getColumnMinCategory, "Category");
	table.setColumnWidth(getColumnMinCategory,120,Unit.PX);
	getColumnMinCategory.setSortable(true);
}

private void addColumnMinType() {
	getColumnMinType = new TextColumn<MaterialIssueNote>() {
		@Override
		public String getValue(MaterialIssueNote object) {
			if (object.getMinType()!=null){
				return object.getMinType();
			}
			return null;
		}
	};
	table.addColumn(getColumnMinType, "Type");
	table.setColumnWidth(getColumnMinType,90,Unit.PX);
	getColumnMinType.setSortable(true);
}


private void addColumnMinBranch() {
	getColumnMinBranch = new TextColumn<MaterialIssueNote>() {
		@Override
		public String getValue(MaterialIssueNote object) {
			if (object.getBranch()!=null){
				return object.getBranch();
			}
			return null;
		}
	};
	table.addColumn(getColumnMinBranch, "Branch");
	table.setColumnWidth(getColumnMinBranch,90,Unit.PX);
	getColumnMinBranch.setSortable(true);
}


private void addColumnMinIssuedBy() {
	getColumnMinIssuedBy = new TextColumn<MaterialIssueNote>() {
		@Override
		public String getValue(MaterialIssueNote object) {
			if (object.getEmployee()!=null){
				return object.getEmployee();
			}
			return null;
		}
	};
	table.addColumn(getColumnMinIssuedBy, "Issued By");
	table.setColumnWidth(getColumnMinIssuedBy,120,Unit.PX);
	getColumnMinIssuedBy.setSortable(true);
}

private void addColumnMinApprover() {
	getColumnMinApprover = new TextColumn<MaterialIssueNote>() {
		@Override
		public String getValue(MaterialIssueNote object) {
			if (object.getApproverName()!=null){
				return object.getApproverName();
			}
			return null;
		}
	};
	table.addColumn(getColumnMinApprover, "Approver");
	table.setColumnWidth(getColumnMinApprover,90,Unit.PX);
	getColumnMinApprover.setSortable(true);
}

private void addColumnMinSoId() {
	getColumnMinSoId = new TextColumn<MaterialIssueNote>() {
		@Override
		public String getValue(MaterialIssueNote object) {
			if (object.getMinSoId() == -1)
				return "N.A";
			else
				return object.getMinSoId() + "";
		}
	};
	table.addColumn(getColumnMinSoId, "Sales Oreder Id");
	table.setColumnWidth(getColumnMinSoId,135,Unit.PX);
	getColumnMinSoId.setSortable(true);
}

private void addColumnMinWoId() {
	getColumnMinWoId = new TextColumn<MaterialIssueNote>() {
		@Override
		public String getValue(MaterialIssueNote object) {
			if (object.getMinWoId() == -1)
				return "N.A";
			else
				return object.getMinWoId() + "";
		}
	};
	table.addColumn(getColumnMinWoId, "WO ID");
	table.setColumnWidth(getColumnMinWoId,90,Unit.PX);
	getColumnMinWoId.setSortable(true);
}

private void addColumnMinStatus() {
	getColumnMinStatus = new TextColumn<MaterialIssueNote>() {
		@Override
		public String getValue(MaterialIssueNote object) {
			if (object.getStatus()!=null){
				return object.getStatus();
			}
			return null;
		}
	};
	table.addColumn(getColumnMinStatus, "Status");
	table.setColumnWidth(getColumnMinStatus,90,Unit.PX);
	getColumnMinStatus.setSortable(true);
}

//private void addColumnMinWarehouse() {
//	getColumnMinWarehouse = new TextColumn<MaterialIssueNote>() {
//		@Override
//		public String getValue(MaterialIssueNote object) {
//			if (object.getMinWarehouse()!=null){
//				return object.getMinWarehouse();
//			}
//			return null;
//		}
//	};
//	table.addColumn(getColumnMinWarehouse, "Warehouse");
//	getColumnMinWarehouse.setSortable(true);
//}


/***********************************************************************************************************/



public void addColumnSorting() {
	addColumnSortingMinId();
	addColumnSortingMinDate();
	addColumnSortingMinTitle();
	addColumnSortingMinCategory();
	addColumnSortingMinType();
	addColumnSortingMinBranch();
	addColumnSortingMinIssuedBy();
	addColumnSortingMinApprover();
	addColumnSortingMinSoId();
	addColumnSortingMinWoId();
	addColumnSortingMinStatus();
}	


/***********************************************Column Sorting***************************************/	
private void addColumnSortingMinId() {
	List<MaterialIssueNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialIssueNote>(list);
	columnSort.setComparator(getColumnMinId, new Comparator<MaterialIssueNote>() {
		@Override
		public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getCount() == e2.getCount()) {
					return 0;
				}
				if (e1.getCount() > e2.getCount()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMinDate() {
	List<MaterialIssueNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialIssueNote>(list);
	columnSort.setComparator(getColumnMinDate, new Comparator<MaterialIssueNote>() {
		@Override
		public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMinDate() != null && e2.getMinDate() != null) {
					return e1.getMinDate().compareTo(e2.getMinDate());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}


private void addColumnSortingMinSoId() {
	List<MaterialIssueNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialIssueNote>(list);
	columnSort.setComparator(getColumnMinSoId, new Comparator<MaterialIssueNote>() {
		@Override
		public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMinSoId() == e2.getMinSoId()) {
					return 0;
				}
				if (e1.getMinSoId() > e2.getMinSoId()) {
					return 1;
				} 
				else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMinWoId() {
	List<MaterialIssueNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialIssueNote>(list);
	columnSort.setComparator(getColumnMinWoId, new Comparator<MaterialIssueNote>() {
		@Override
		public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMinWoId() == e2.getMinWoId()) {
					return 0;
				}
				if (e1.getMinWoId() > e2.getMinWoId()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMinTitle() {
	List<MaterialIssueNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialIssueNote>(list);
	columnSort.setComparator(getColumnMinTitle, new Comparator<MaterialIssueNote>() {
		@Override
		public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMinTitle() != null && e2.getMinTitle() != null) {
					return e1.getMinTitle().compareTo(e2.getMinTitle());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMinCategory() {
	List<MaterialIssueNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialIssueNote>(list);
	columnSort.setComparator(getColumnMinCategory, new Comparator<MaterialIssueNote>() {
		@Override
		public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMinCategory() != null && e2.getMinCategory() != null) {
					return e1.getMinCategory().compareTo(e2.getMinCategory());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMinType() {
	List<MaterialIssueNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialIssueNote>(list);
	columnSort.setComparator(getColumnMinType, new Comparator<MaterialIssueNote>() {
		@Override
		public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMinType() != null && e2.getMinType() != null) {
					return e1.getMinType().compareTo(e2.getMinType());
				}
				if (e1.getMinType().equals("") && e2.getMinType().equals("")) {
					return 0;
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}


private void addColumnSortingMinBranch() {
	List<MaterialIssueNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialIssueNote>(list);
	columnSort.setComparator(getColumnMinBranch, new Comparator<MaterialIssueNote>() {
		@Override
		public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getBranch() != null && e2.getBranch() != null) {
					return e1.getBranch().compareTo(e2.getBranch());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}


private void addColumnSortingMinIssuedBy() {
	List<MaterialIssueNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialIssueNote>(list);
	columnSort.setComparator(getColumnMinIssuedBy, new Comparator<MaterialIssueNote>() {
		@Override
		public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getEmployee() != null && e2.getEmployee() != null) {
					return e1.getEmployee().compareTo(e2.getEmployee());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMinApprover() {
	List<MaterialIssueNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialIssueNote>(list);
	columnSort.setComparator(getColumnMinApprover, new Comparator<MaterialIssueNote>() {
		@Override
		public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getApproverName() != null && e2.getApproverName() != null) {
					return e1.getApproverName().compareTo(e2.getApproverName());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}


private void addColumnSortingMinStatus() {
	List<MaterialIssueNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialIssueNote>(list);
	columnSort.setComparator(getColumnMinStatus, new Comparator<MaterialIssueNote>() {
		@Override
		public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getStatus() != null && e2.getStatus() != null) {
					return e1.getStatus().compareTo(e2.getStatus());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}



@Override
protected void initializekeyprovider() {
	
}

@Override
public void addFieldUpdater() {
	
}

@Override
public void setEnable(boolean state) {
	
}

@Override
public void applyStyle() {
	
}}
