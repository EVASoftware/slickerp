package com.slicktechnologies.client.views.inventorydashboard;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class InventoryHomeSearchProxy extends ViewContainer{
	
	protected ProductInfoComposite productInfo;
	protected MyQuerry grn,mrn,min,mmn,piv;
	protected PopupPanel popup;
	protected InlineLabel golbl;
	public HorizontalPanel horizontal;
	FlexForm form;
	
	public InventoryHomeSearchProxy() {
		super();
		createGui();
		applyStyle();
	}
	
	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new SuperProduct());
		productInfo=AppUtility.initiateSalesProductComposite(new SuperProduct());
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",productInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
		FormField[][] fields=new FormField[][]{{fpersonInfo}};
		form=new FlexForm(fields,FormStyle.ROWFORM);
		content.add(form);
		horizontal=new HorizontalPanel();
		//Go lbl
		golbl=new InlineLabel("Go");
		golbl.getElement().setId("addbutton");
		horizontal.add(golbl);
		horizontal.getElement().addClassName("centering");
	
		content.add(horizontal);
		
		popup=new PopupPanel(true);
		popup.add(content);
	}
	
	public void showPopUp()
	{
		//Clear the previous
		productInfo.clear();
		popup.center();
		popup.setSize("400px","200px");
		popup.getElement().setId("searchpopup");
	    popup.setAnimationEnabled(true);
	}
	
	public void hidePopUp()
	{
		popup.hide();
	}
	
	public void applyHandler(ClickHandler handler)
	{
		golbl.addClickHandler(handler);
	}
	
	public void createAllFilter1()
	 {
			Vector<Filter>filterVec = new Vector<Filter>();
			Filter temp;
		 
			if(productInfo.getProdID().getText().trim().equals("")==false)
			{
				temp=new Filter();
				temp.setIntValue(Integer.parseInt(productInfo.getProdID().getValue()));
				temp.setQuerryString("subProductTableMmn.materialProductId");
				filterVec.add(temp);	
			}
			
			if((productInfo.getProdCode().getText()).trim().equals("")==false)
			{
				temp=new Filter();
				temp.setStringValue(productInfo.getProdCode().getValue());
				temp.setQuerryString("subProductTableMmn.materialProductCode");
				filterVec.add(temp);	
			}
			
			if(productInfo.getProdName().getText().trim().equals("")==false)
			{
				temp=new Filter();
				temp.setStringValue(productInfo.getProdName().getValue());
				temp.setQuerryString("subProductTableMmn.materialProductName");
				filterVec.add(temp);	
			}
			
			this.grn=new MyQuerry();
			this.grn.setFilters(filterVec);
			this.grn.setQuerryObject(new GRN());
			
			this.mrn=new MyQuerry();
			this.mrn.setQuerryObject(new MaterialRequestNote());
			this.mrn.setFilters(filterVec);
			
			this.min=new MyQuerry();
			this.min.setQuerryObject(new MaterialIssueNote());
			this.min.setFilters(filterVec);
			
			this.mmn=new MyQuerry();
			this.mmn.setQuerryObject(new MaterialMovementNote());
			this.mmn.setFilters(filterVec);
			
			this.piv=new MyQuerry();
			this.piv.setQuerryObject(new ProductInventoryView());
			this.piv.setFilters(filterVec);
	}

	@Override
	protected void createGui() {
		createScreen();
	}

	@Override
	public void applyStyle() {
		content.getElement().setId("formcontent");
		form.getElement().setId("form");
	}

}
