package com.slicktechnologies.client.views.inventorydashboard;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.inventory.Inspection;

public class InspectionTableProxy extends SuperTable<Inspection> {
	TextColumn<Inspection> getGrnNumberColumn;
	TextColumn<Inspection> getCountColumn;
	TextColumn<Inspection> getInspectionTitleColumn;
	Column<Inspection, Date> getCreationDateColumn;
	TextColumn<Inspection> getGroupColumn;
	TextColumn<Inspection> getCategoryColumn;
	TextColumn<Inspection> getTypeColumn;
	TextColumn<Inspection> getBranchColumn;
	TextColumn<Inspection> getEmployeeColumn;
	TextColumn<Inspection> getApproverNameColumn;
	TextColumn<Inspection> getVendorIdColumn;
	TextColumn<Inspection> getVendorNameColumn;
	TextColumn<Inspection> getVendorCellColumn;
	TextColumn<Inspection> getStatusColumn;

	public InspectionTableProxy() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetInspectionTitle();
		addColumngetCreationDate();
		addColumngetVendorName();
		addColumngetGrnNumber();
		addColumnCategory();
		addColumnType();
		addColumnGroup();
		addColumngetEmployee();
		addColumngetApproverName();
		addColumngetBranch();
		addColumngetStatus();
//		addColumngetVendorId();
//		addColumngetVendorCell();
	}

	public void addColumnSorting() {
		addSortinggetCount();
		addSortingInspectionTitle();
		addSortingColumnGrnDate();
		addSortingGroup();
		addSortingCategory();
		addSortingType();
		addSortinggetBranch();
		addSortinggetEmployee();
		addSortinggetApproverName();
		addSortinggetVendorId();
		addSortinggetVendorName();
		addSortinggetVendorCell();
		addSortinggetGrnNumber();
		addSortinggetStatus();
	}

	/*************************************Inspection Columns Sorting*************************************************/

	protected void addSortinggetCount() {
		List<Inspection> list = getDataprovider().getList();
		columnSort = new ListHandler<Inspection>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Inspection>() {
			@Override
			public int compare(Inspection e1, Inspection e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	public void addSortingColumnGrnDate()
	{
		List<Inspection> list=getDataprovider().getList();
		columnSort=new ListHandler<Inspection>(list);
		columnSort.setComparator(getCreationDateColumn, new Comparator<Inspection>()
				{
			@Override
			public int compare(Inspection e1,Inspection e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
						return e1.getCreationDate().compareTo(e2.getCreationDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);


	}

	protected void addSortinggetVendorId() {
		List<Inspection> list = getDataprovider().getList();
		columnSort = new ListHandler<Inspection>(list);
		columnSort.setComparator(getVendorIdColumn, new Comparator<Inspection>() {
			@Override
			public int compare(Inspection e1, Inspection e2) {
				if (e1 != null && e2 != null) {
					if (e1.getVendorInfo().getCount() == e2.getVendorInfo()
							.getCount()) {
						return 0;
					}
					if (e1.getVendorInfo().getCount() > e2.getVendorInfo()
							.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addSortingInspectionTitle() {
		List<Inspection> list = getDataprovider().getList();
		columnSort = new ListHandler<Inspection>(list);
		columnSort.setComparator(getInspectionTitleColumn, new Comparator<Inspection>() {
			@Override
			public int compare(Inspection e1, Inspection e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInspectionTitle() != null && e2.getInspectionTitle() != null) {
						return e1.getInspectionTitle().compareTo(e2.getInspectionTitle());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	protected void addSortinggetVendorCell() {
		List<Inspection> list = getDataprovider().getList();
		columnSort = new ListHandler<Inspection>(list);
		columnSort.setComparator(getVendorCellColumn, new Comparator<Inspection>() {
			@Override
			public int compare(Inspection e1, Inspection e2) {
				if (e1 != null && e2 != null) {
					if (e1.getVendorInfo().getCount() == e2.getVendorInfo()
							.getCount()) {
						return 0;
					}
					if (e1.getVendorInfo().getCount() > e2.getVendorInfo()
							.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetGrnNumber() {
		List<Inspection> list = getDataprovider().getList();
		columnSort = new ListHandler<Inspection>(list);
		columnSort.setComparator(getGrnNumberColumn, new Comparator<Inspection>() {
			@Override
			public int compare(Inspection e1, Inspection e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInspectionGrnId() == e2.getInspectionGrnId()) {
						return 0;
					}
					if (e1.getInspectionGrnId() > e2.getInspectionGrnId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetBranch() {
		List<Inspection> list = getDataprovider().getList();
		columnSort = new ListHandler<Inspection>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<Inspection>() {
			@Override
			public int compare(Inspection e1, Inspection e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null && e2.getBranch() != null) {
						return e1.getBranch().compareTo(e2.getBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetVendorName() {
		List<Inspection> list = getDataprovider().getList();
		columnSort = new ListHandler<Inspection>(list);
		columnSort.setComparator(getVendorNameColumn, new Comparator<Inspection>() {
			@Override
			public int compare(Inspection e1, Inspection e2) {
				if (e1 != null && e2 != null) {
					if (e1.getVendorInfo().getFullName() != null
							&& e2.getVendorInfo().getFullName() != null) {
						return e1.getVendorInfo().getFullName()
								.compareTo(e2.getVendorInfo().getFullName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetEmployee() {
		List<Inspection> list = getDataprovider().getList();
		columnSort = new ListHandler<Inspection>(list);
		columnSort.setComparator(getEmployeeColumn, new Comparator<Inspection>() {
			@Override
			public int compare(Inspection e1, Inspection e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployee() != null && e2.getEmployee() != null) {
						return e1.getEmployee().compareTo(e2.getEmployee());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetApproverName() {
		List<Inspection> list = getDataprovider().getList();
		columnSort = new ListHandler<Inspection>(list);
		columnSort.setComparator(getApproverNameColumn, new Comparator<Inspection>() {
			@Override
			public int compare(Inspection e1, Inspection e2) {
				if (e1 != null && e2 != null) {
					if (e1.getApproverName() != null
							&& e2.getApproverName() != null) {
						return e1.getApproverName().compareTo(
								e2.getApproverName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}


	protected void addSortinggetStatus() {
		List<Inspection> list = getDataprovider().getList();
		columnSort = new ListHandler<Inspection>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Inspection>() {
			@Override
			public int compare(Inspection e1, Inspection e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null && e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}


	private void addSortingType() {
		List<Inspection> list = getDataprovider().getList();
		columnSort = new ListHandler<Inspection>(list);
		columnSort.setComparator(getTypeColumn, new Comparator<Inspection>() {
			@Override
			public int compare(Inspection e1, Inspection e2) {
				if (e1 != null && e2 != null) {
					if (e1.getType()!= null && e2.getType() != null) {
						return e1.getType().compareTo(e2.getType());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingCategory() {
		List<Inspection> list = getDataprovider().getList();
		columnSort = new ListHandler<Inspection>(list);
		columnSort.setComparator(getCategoryColumn, new Comparator<Inspection>() {
			@Override
			public int compare(Inspection e1, Inspection e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCategory()!= null && e2.getCategory() != null) {
						return e1.getCategory().compareTo(e2.getCategory());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingGroup() {
		List<Inspection> list = getDataprovider().getList();
		columnSort = new ListHandler<Inspection>(list);
		columnSort.setComparator(getGroupColumn, new Comparator<Inspection>() {
			@Override
			public int compare(Inspection e1, Inspection e2) {
				if (e1 != null && e2 != null) {
					if (e1.getGroup()!= null && e2.getGroup() != null) {
						return e1.getGroup().compareTo(e2.getGroup());
					}
					
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	/**************************************Inspection Table Columns**********************************************/
	

	protected void addColumngetBranch() {
		getBranchColumn = new TextColumn<Inspection>() {
			@Override
			public String getValue(Inspection object) {
				return object.getBranch() + "";
			}
		};
		table.addColumn(getBranchColumn, "Branch");
		table.setColumnWidth(getBranchColumn,90,Unit.PX);
		getBranchColumn.setSortable(true);
	}

	protected void addColumngetEmployee() {
		getEmployeeColumn = new TextColumn<Inspection>() {
			@Override
			public String getValue(Inspection object) {
				return object.getEmployee() + "";
			}
		};
		table.addColumn(getEmployeeColumn, "Purchase Engineer");
		table.setColumnWidth(getEmployeeColumn,130,Unit.PX);
		getEmployeeColumn.setSortable(true);
	}

	protected void addColumngetApproverName() {
		getApproverNameColumn = new TextColumn<Inspection>() {
			@Override
			public String getValue(Inspection object) {
				return object.getApproverName();
			}
		};
		table.addColumn(getApproverNameColumn, "Approver Name");
		table.setColumnWidth(getApproverNameColumn,130,Unit.PX);
		getApproverNameColumn.setSortable(true);
	}
	
	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<Inspection>() {
			@Override
			public String getValue(Inspection object) {
				return object.getStatus() + "";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		table.setColumnWidth(getStatusColumn,110,Unit.PX);
		getStatusColumn.setSortable(true);
	}

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<Inspection>() {
			@Override
			public String getValue(Inspection object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "ID");
		table.setColumnWidth(getCountColumn,100,Unit.PX);
		getCountColumn.setSortable(true);
	}

	protected void addColumngetVendorId() {
		getVendorIdColumn = new TextColumn<Inspection>() {
			@Override
			public String getValue(Inspection object) {
				if (object.getVendorInfo().getCount() == -1)
					return "N.A";
				else
					return object.getVendorInfo().getCount() + "";
			}
		};
		table.addColumn(getVendorIdColumn, "Vendor Id");
		table.setColumnWidth(getVendorIdColumn,120,Unit.PX);
		getVendorIdColumn.setSortable(true);
	}

	protected void addColumngetVendorName() {
		getVendorNameColumn = new TextColumn<Inspection>() {
			@Override
			public String getValue(Inspection object) {
				return object.getVendorInfo().getFullName() + "";
			}
		};
		table.addColumn(getVendorNameColumn, "Vendor Name");
		table.setColumnWidth(getVendorNameColumn,120,Unit.PX);
		getVendorNameColumn.setSortable(true);
	}

	protected void addColumngetVendorCell() {
		getVendorCellColumn = new TextColumn<Inspection>() {
			@Override
			public String getValue(Inspection object) {
				if (object.getVendorInfo().getCellNumber() == -1)
					return "N.A";
				else
					return object.getVendorInfo().getCellNumber() + "";
			}
		};
		table.addColumn(getVendorCellColumn, "Vendor Cell");
		table.setColumnWidth(getVendorCellColumn,120,Unit.PX);
		getVendorCellColumn.setSortable(true);
	}

	protected void addColumngetCreationDate() {
		DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		getCreationDateColumn = new Column<Inspection, Date>(date) {
			@Override
			public Date getValue(Inspection object) {
				if (object.getCreationDate() != null) {
					return object.getCreationDate();
				} else {
					object.setCreationDate(new Date());
					return new Date();
				}
			}
		};
		table.addColumn(getCreationDateColumn, "Date");
		table.setColumnWidth(getCreationDateColumn,120,Unit.PX);
		getCreationDateColumn.setSortable(true);
	}

	protected void addColumngetGrnNumber() {
		getGrnNumberColumn = new TextColumn<Inspection>() {
			@Override
			public String getValue(Inspection object) {
				return object.getInspectionGrnId() + "";
			}
		};
		table.addColumn(getGrnNumberColumn, "GRN ID");
		table.setColumnWidth(getGrnNumberColumn,100,Unit.PX);
		getGrnNumberColumn.setSortable(true);
	}
	
	private void addColumngetInspectionTitle() {
		getInspectionTitleColumn = new TextColumn<Inspection>() {
			@Override
			public String getValue(Inspection object) {
				return object.getInspectionTitle();
			}
		};
		table.addColumn(getInspectionTitleColumn, "Title");
		table.setColumnWidth(getInspectionTitleColumn,100,Unit.PX);
		getInspectionTitleColumn.setSortable(true);

	}
	

	private void addColumnGroup() {
		getGroupColumn = new TextColumn<Inspection>() {
			@Override
			public String getValue(Inspection object) {
				if(object.getGroup()!=null){
				return object.getGroup() + "";
				}else{
					return "";
				}
			}
		};
		table.addColumn(getGroupColumn, "Group");
		table.setColumnWidth(getGroupColumn,90,Unit.PX);
		getGroupColumn.setSortable(true);
	}

	private void addColumnCategory() {
		getCategoryColumn = new TextColumn<Inspection>() {
			@Override
			public String getValue(Inspection object) {
				return object.getCategory() + "";
			}
		};
		table.addColumn(getCategoryColumn, "Category");
		table.setColumnWidth(getCategoryColumn,100,Unit.PX);
		getCategoryColumn.setSortable(true);
	}

	private void addColumnType() {
		getTypeColumn = new TextColumn<Inspection>() {
			@Override
			public String getValue(Inspection object) {
				if(object.getType()!=null){
					return object.getType() + "";
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getTypeColumn, "Type");
		table.setColumnWidth(getTypeColumn,100,Unit.PX);
		getTypeColumn.setSortable(true);
	}

//	protected void addFieldUpdatergetCreationDate() {
//		getCreationDateColumn.setFieldUpdater(new FieldUpdater<Inspection, Date>() {
//			@Override
//			public void update(int index, Inspection object, Date value) {
//				object.setCreationDate(value);
//				table.redrawRow(index);
//			}
//		});
//	}

	

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<Inspection>() {
			@Override
			public Object getKey(Inspection item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	@Override
	public void addFieldUpdater() {
	}
}
