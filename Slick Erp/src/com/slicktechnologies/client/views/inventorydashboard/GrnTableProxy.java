package com.slicktechnologies.client.views.inventorydashboard;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.inventory.GRN;

public class GrnTableProxy extends SuperTable<GRN>{TextColumn<GRN> getPoNumberColumn;
TextColumn<GRN> getCountColumn;
TextColumn<GRN> getGRNTitleColumn;
Column<GRN, Date> getCreationDateColumn;
TextColumn<GRN> getGroupColumn;
TextColumn<GRN> getCategoryColumn;
TextColumn<GRN> getTypeColumn;
TextColumn<GRN> getBranchColumn;
TextColumn<GRN> getEmployeeColumn;
TextColumn<GRN> getApproverNameColumn;
TextColumn<GRN> getVendorIdColumn;
TextColumn<GRN> getVendorNameColumn;
TextColumn<GRN> getVendorCellColumn;
TextColumn<GRN> getStatusColumn;

public GrnTableProxy() {
	super();
}

@Override
public void createTable() {
	addColumngetCount();
	addColumngetGrnTitle();
	addColumngetCreationDate();
	addColumngetVendorName();
	addColumngetPoNumber();
	addColumnCategory();
	addColumnType();
	addColumnGroup();
	addColumngetEmployee();
	addColumngetApproverName();
	addColumngetBranch();
	addColumngetStatus();
}

public void addColumnSorting() {
	addSortinggetCount();
	addSortingGrnTitle();
	addSortingColumnGrnDate();
	addSortingGroup();
	addSortingCategory();
	addSortingType();
	addSortinggetBranch();
	addSortinggetEmployee();
	addSortinggetApproverName();
	addSortinggetVendorName();
	addSortinggetPoNumber();
	addSortinggetStatus();
}

/*************************************GRN Columns Sorting*************************************************/

protected void addSortinggetCount() {
	List<GRN> list = getDataprovider().getList();
	columnSort = new ListHandler<GRN>(list);
	columnSort.setComparator(getCountColumn, new Comparator<GRN>() {
		@Override
		public int compare(GRN e1, GRN e2) {
			if (e1 != null && e2 != null) {
				if (e1.getCount() == e2.getCount()) {
					return 0;
				}
				if (e1.getCount() > e2.getCount()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

public void addSortingColumnGrnDate()
{
	List<GRN> list=getDataprovider().getList();
	columnSort=new ListHandler<GRN>(list);
	columnSort.setComparator(getCreationDateColumn, new Comparator<GRN>()
			{
		@Override
		public int compare(GRN e1,GRN e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
					return e1.getCreationDate().compareTo(e2.getCreationDate());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);


}

protected void addSortinggetVendorId() {
	List<GRN> list = getDataprovider().getList();
	columnSort = new ListHandler<GRN>(list);
	columnSort.setComparator(getVendorIdColumn, new Comparator<GRN>() {
		@Override
		public int compare(GRN e1, GRN e2) {
			if (e1 != null && e2 != null) {
				if (e1.getVendorInfo().getCount() == e2.getVendorInfo()
						.getCount()) {
					return 0;
				}
				if (e1.getVendorInfo().getCount() > e2.getVendorInfo()
						.getCount()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addSortingGrnTitle() {
	List<GRN> list = getDataprovider().getList();
	columnSort = new ListHandler<GRN>(list);
	columnSort.setComparator(getGRNTitleColumn, new Comparator<GRN>() {
		@Override
		public int compare(GRN e1, GRN e2) {
			if (e1 != null && e2 != null) {
				if (e1.getTitle() != null && e2.getTitle() != null) {
					return e1.getTitle().compareTo(e2.getTitle());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
	
}

protected void addSortinggetVendorCell() {
	List<GRN> list = getDataprovider().getList();
	columnSort = new ListHandler<GRN>(list);
	columnSort.setComparator(getVendorCellColumn, new Comparator<GRN>() {
		@Override
		public int compare(GRN e1, GRN e2) {
			if (e1 != null && e2 != null) {
				if (e1.getVendorInfo().getCount() == e2.getVendorInfo()
						.getCount()) {
					return 0;
				}
				if (e1.getVendorInfo().getCount() > e2.getVendorInfo()
						.getCount()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

protected void addSortinggetPoNumber() {
	List<GRN> list = getDataprovider().getList();
	columnSort = new ListHandler<GRN>(list);
	columnSort.setComparator(getPoNumberColumn, new Comparator<GRN>() {
		@Override
		public int compare(GRN e1, GRN e2) {
			if (e1 != null && e2 != null) {
				if (e1.getPoNo() == e2.getPoNo()) {
					return 0;
				}
				if (e1.getPoNo() > e2.getPoNo()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

protected void addSortinggetBranch() {
	List<GRN> list = getDataprovider().getList();
	columnSort = new ListHandler<GRN>(list);
	columnSort.setComparator(getBranchColumn, new Comparator<GRN>() {
		@Override
		public int compare(GRN e1, GRN e2) {
			if (e1 != null && e2 != null) {
				if (e1.getBranch() != null && e2.getBranch() != null) {
					return e1.getBranch().compareTo(e2.getBranch());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

protected void addSortinggetVendorName() {
	List<GRN> list = getDataprovider().getList();
	columnSort = new ListHandler<GRN>(list);
	columnSort.setComparator(getVendorNameColumn, new Comparator<GRN>() {
		@Override
		public int compare(GRN e1, GRN e2) {
			if (e1 != null && e2 != null) {
				if (e1.getVendorInfo().getFullName() != null
						&& e2.getVendorInfo().getFullName() != null) {
					return e1.getVendorInfo().getFullName()
							.compareTo(e2.getVendorInfo().getFullName());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

protected void addSortinggetEmployee() {
	List<GRN> list = getDataprovider().getList();
	columnSort = new ListHandler<GRN>(list);
	columnSort.setComparator(getEmployeeColumn, new Comparator<GRN>() {
		@Override
		public int compare(GRN e1, GRN e2) {
			if (e1 != null && e2 != null) {
				if (e1.getEmployee() != null && e2.getEmployee() != null) {
					return e1.getEmployee().compareTo(e2.getEmployee());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

protected void addSortinggetApproverName() {
	List<GRN> list = getDataprovider().getList();
	columnSort = new ListHandler<GRN>(list);
	columnSort.setComparator(getApproverNameColumn, new Comparator<GRN>() {
		@Override
		public int compare(GRN e1, GRN e2) {
			if (e1 != null && e2 != null) {
				if (e1.getApproverName() != null
						&& e2.getApproverName() != null) {
					return e1.getApproverName().compareTo(
							e2.getApproverName());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}


protected void addSortinggetStatus() {
	List<GRN> list = getDataprovider().getList();
	columnSort = new ListHandler<GRN>(list);
	columnSort.setComparator(getStatusColumn, new Comparator<GRN>() {
		@Override
		public int compare(GRN e1, GRN e2) {
			if (e1 != null && e2 != null) {
				if (e1.getStatus() != null && e2.getStatus() != null) {
					return e1.getStatus().compareTo(e2.getStatus());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}


private void addSortingType() {
	List<GRN> list = getDataprovider().getList();
	columnSort = new ListHandler<GRN>(list);
	columnSort.setComparator(getTypeColumn, new Comparator<GRN>() {
		@Override
		public int compare(GRN e1, GRN e2) {
			if (e1 != null && e2 != null) {
				if (e1.getGrnType()!= null && e2.getGrnType() != null) {
					return e1.getGrnType().compareTo(e2.getGrnType());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addSortingCategory() {
	List<GRN> list = getDataprovider().getList();
	columnSort = new ListHandler<GRN>(list);
	columnSort.setComparator(getCategoryColumn, new Comparator<GRN>() {
		@Override
		public int compare(GRN e1, GRN e2) {
			if (e1 != null && e2 != null) {
				if (e1.getGrnCategory()!= null && e2.getGrnCategory() != null) {
					return e1.getGrnCategory().compareTo(e2.getGrnCategory());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addSortingGroup() {
	List<GRN> list = getDataprovider().getList();
	columnSort = new ListHandler<GRN>(list);
	columnSort.setComparator(getGroupColumn, new Comparator<GRN>() {
		@Override
		public int compare(GRN e1, GRN e2) {
			if (e1 != null && e2 != null) {
				if (e1.getGrnGroup() != null && e2.getGrnGroup() != null) {
					return e1.getGrnGroup().compareTo(e2.getGrnGroup());
				}
				if (e1.getGrnGroup().equals("") && e2.getGrnGroup().equals("")) {
					return 0;
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

/**************************************GRN Table Columns**********************************************/


protected void addColumngetBranch() {
	getBranchColumn = new TextColumn<GRN>() {
		@Override
		public String getValue(GRN object) {
			return object.getBranch() + "";
		}
	};
	table.addColumn(getBranchColumn, "Branch");
	table.setColumnWidth(getBranchColumn,90,Unit.PX);
	getBranchColumn.setSortable(true);
}

protected void addColumngetEmployee() {
	getEmployeeColumn = new TextColumn<GRN>() {
		@Override
		public String getValue(GRN object) {
			return object.getEmployee() + "";
		}
	};
	table.addColumn(getEmployeeColumn, "Purchase Engineer");
	table.setColumnWidth(getEmployeeColumn,160,Unit.PX);
	getEmployeeColumn.setSortable(true);
}

protected void addColumngetApproverName() {
	getApproverNameColumn = new TextColumn<GRN>() {
		@Override
		public String getValue(GRN object) {
			return object.getApproverName() + "";
		}
	};
	table.addColumn(getApproverNameColumn, "Approver");
	table.setColumnWidth(getCountColumn,90,Unit.PX);
	getApproverNameColumn.setSortable(true);
}

protected void addColumngetStatus() {
	getStatusColumn = new TextColumn<GRN>() {
		@Override
		public String getValue(GRN object) {
			return object.getStatus() + "";
		}
	};
	table.addColumn(getStatusColumn, "Status");
	table.setColumnWidth(getStatusColumn,90,Unit.PX);
	getStatusColumn.setSortable(true);
}

protected void addColumngetCount() {
	getCountColumn = new TextColumn<GRN>() {
		@Override
		public String getValue(GRN object) {
			if (object.getCount() == -1)
				return "N.A";
			else
				return object.getCount() + "";
		}
	};
	table.addColumn(getCountColumn, "Id");
	table.setColumnWidth(getCountColumn,90,Unit.PX);
	getCountColumn.setSortable(true);
}

protected void addColumngetVendorId() {
	getVendorIdColumn = new TextColumn<GRN>() {
		@Override
		public String getValue(GRN object) {
			if (object.getVendorInfo().getCount() == -1)
				return "N.A";
			else
				return object.getVendorInfo().getCount() + "";
		}
	};
	table.addColumn(getVendorIdColumn, "Vendor Id");
	table.setColumnWidth(getVendorIdColumn,120,Unit.PX);
	getVendorIdColumn.setSortable(true);
}

protected void addColumngetVendorName() {
	getVendorNameColumn = new TextColumn<GRN>() {
		@Override
		public String getValue(GRN object) {
			return object.getVendorInfo().getFullName() + "";
		}
	};
	table.addColumn(getVendorNameColumn, "Vendor Name");
	table.setColumnWidth(getVendorNameColumn,120,Unit.PX);
	getVendorNameColumn.setSortable(true);
}

protected void addColumngetVendorCell() {
	getVendorCellColumn = new TextColumn<GRN>() {
		@Override
		public String getValue(GRN object) {
			if (object.getVendorInfo().getCellNumber() == -1)
				return "N.A";
			else
				return object.getVendorInfo().getCellNumber() + "";
		}
	};
	table.addColumn(getVendorCellColumn, "Vendor Cell");
	table.setColumnWidth(getVendorCellColumn,120,Unit.PX);
	getVendorCellColumn.setSortable(true);
}

protected void addColumngetCreationDate() {
	DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
	DatePickerCell date = new DatePickerCell(fmt);
	final Date sub7Days=new Date();
	System.out.println("Todays Date ::: "+sub7Days);
	CalendarUtil.addDaysToDate(sub7Days,-7);
	System.out.println("Date - 7 Days ::: "+sub7Days);
	sub7Days.setHours(23);
	sub7Days.setMinutes(59);
	sub7Days.setSeconds(59);
	System.out.println("Converted Date ::: "+sub7Days);
	
	getCreationDateColumn = new Column<GRN, Date>(date) {
		@Override
		public String getCellStyleNames(Context context, GRN object) {
			if((object.getCreationDate().before(sub7Days))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Approved")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
		
		@Override
		public Date getValue(GRN object) {
			if (object.getCreationDate() != null) {
				return object.getCreationDate();
			} else {
				object.setCreationDate(new Date());
				return new Date();
			}
		}
	};
	table.addColumn(getCreationDateColumn, "Date");
	table.setColumnWidth(getCreationDateColumn,120,Unit.PX);
	getCreationDateColumn.setSortable(true);
}

protected void addColumngetPoNumber() {
	getPoNumberColumn = new TextColumn<GRN>() {
		@Override
		public String getValue(GRN object) {
			return object.getPoNo() + "";
		}
	};
	table.addColumn(getPoNumberColumn, "PO ID");
	table.setColumnWidth(getPoNumberColumn,90,Unit.PX);
	getPoNumberColumn.setSortable(true);
}

private void addColumngetGrnTitle() {
	getGRNTitleColumn = new TextColumn<GRN>() {
		@Override
		public String getValue(GRN object) {
			return object.getTitle();
		}
	};
	table.addColumn(getGRNTitleColumn, "Title");
	table.setColumnWidth(getGRNTitleColumn,90,Unit.PX);
	getGRNTitleColumn.setSortable(true);

}


private void addColumnType() {
	getGroupColumn = new TextColumn<GRN>() {
		@Override
		public String getValue(GRN object) {
			if(object.getGrnGroup()!=null){
			return object.getGrnGroup() + "";
			}else{
				return "";
			}
		}
	};
	table.addColumn(getGroupColumn, "Group");
	table.setColumnWidth(getGroupColumn,90,Unit.PX);
	getGroupColumn.setSortable(true);
}

private void addColumnCategory() {
	getCategoryColumn = new TextColumn<GRN>() {
		@Override
		public String getValue(GRN object) {
			if(object.getGrnCategory()!=null){
				return object.getGrnCategory() + "";
			}
			else{
				return "";
			}
		}
	};
	table.addColumn(getCategoryColumn, "Category");
	table.setColumnWidth(getCategoryColumn,90,Unit.PX);
	getCategoryColumn.setSortable(true);
}

private void addColumnGroup() {
	getTypeColumn = new TextColumn<GRN>() {
		@Override
		public String getValue(GRN object) {
			if(object.getGrnType()!=null){
				return object.getGrnType() + "";
			}
			else{
				return "";
			}
		}
	};
	table.addColumn(getTypeColumn, "Type");
	table.setColumnWidth(getTypeColumn,90,Unit.PX);
	getTypeColumn.setSortable(true);
}





@Override
protected void initializekeyprovider() {
	keyProvider = new ProvidesKey<GRN>() {
		@Override
		public Object getKey(GRN item) {
			if (item == null) {
				return null;
			} else
				return item.getId();
		}
	};
}

@Override
public void setEnable(boolean state) {
}

@Override
public void applyStyle() {
}

@Override
public void addFieldUpdater() {
}}
