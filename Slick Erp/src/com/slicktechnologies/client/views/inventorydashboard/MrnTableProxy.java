package com.slicktechnologies.client.views.inventorydashboard;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.views.popups.MaterialInfoDeatilsPopup;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;

public class MrnTableProxy extends SuperTable<MaterialRequestNote> {TextColumn<MaterialRequestNote> getColumnMrnId;
Column<MaterialRequestNote, Date> getColumnMrnDate;
Column<MaterialRequestNote, Date> getColumnMrnReqDate;
TextColumn<MaterialRequestNote> getColumnMrnTitle;
TextColumn<MaterialRequestNote> getColumnMrnCategory;
TextColumn<MaterialRequestNote> getColumnMrnType;
TextColumn<MaterialRequestNote> getColumnMrnSoId;
TextColumn<MaterialRequestNote> getColumnMrnWoId;
TextColumn<MaterialRequestNote> getColumnMrnProject;
TextColumn<MaterialRequestNote> getColumnMrnBranch;
TextColumn<MaterialRequestNote> getColumnMrnSalesPerson;
TextColumn<MaterialRequestNote> getColumnMrnRequestedBy;
TextColumn<MaterialRequestNote> getColumnMrnApprover;
TextColumn<MaterialRequestNote> getColumnMrnStatus;

/**
 * Date 06-02-2019 by Vijay
 * Des :- For NBHC Inventory Management Product details in popup
 */
 Column<MaterialRequestNote, String> getcolumnProdDetails;
 MaterialInfoDeatilsPopup materailpopup = new MaterialInfoDeatilsPopup();
 public static boolean flag = false;
/**
 * ends here
 */

public MrnTableProxy() {
	super();
}

@Override
public void createTable() {
	addColumnMrnId();
	addColumnMrnTitle();
	/*** Date 06-02-2019 by Vijay for button to display prod details **/
	addColumnViewProdDeatils();
	addColumnMrnDate();
	addColumnMrnRequiredDate();
	addColumnMrnWoId();
	addColumnMrnSoId();
	addColumnMrnCategory();
	addColumnMrnType();
	addColumnMrnRequestedBy();
	addColumnMrnApprover();
	addColumnMrnBranch();
	addColumnMrnStatus();
}



private void addColumnMrnId() {
	getColumnMrnId = new TextColumn<MaterialRequestNote>() {
		@Override
		public String getValue(MaterialRequestNote object) {
			if (object.getCount() == -1)
				return "N.A";
			else
				return object.getCount() + "";
		}
	};
	table.addColumn(getColumnMrnId, "Id");
	table.setColumnWidth(getColumnMrnId,90,Unit.PX);
	getColumnMrnId.setSortable(true);
}

private void addColumnMrnDate() {
	DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
	DatePickerCell date = new DatePickerCell(fmt);
	
	final Date sub7Days=new Date();
	System.out.println("Todays Date ::: "+sub7Days);
	CalendarUtil.addDaysToDate(sub7Days,-7);
	System.out.println("Date - 7 Days ::: "+sub7Days);
	sub7Days.setHours(23);
	sub7Days.setMinutes(59);
	sub7Days.setSeconds(59);
	System.out.println("Converted Date ::: "+sub7Days);
	
	getColumnMrnDate = new Column<MaterialRequestNote, Date>(date) {
		@Override
		public String getCellStyleNames(Context context, MaterialRequestNote object) {
			
			if(object.getMrnDate()!=null){
			if((object.getMrnDate().before(sub7Days))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Approved")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
			 }
			return "black";
		}
		
		@Override
		public Date getValue(MaterialRequestNote object) {
			if (object.getMrnDate()!= null) {
				return object.getMrnDate();
			}
			return null;
		}
	};
	table.addColumn(getColumnMrnDate, "Date");
	table.setColumnWidth(getColumnMrnDate,120,Unit.PX);
	getColumnMrnDate.setSortable(true);
}

private void addColumnMrnRequiredDate() {
	DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
	DatePickerCell date = new DatePickerCell(fmt);
	getColumnMrnReqDate = new Column<MaterialRequestNote, Date>(date) {
		@Override
		public Date getValue(MaterialRequestNote object) {
			if (object.getMrnRequiredDate()!= null) {
				return object.getMrnRequiredDate();
			}
			return null;
		}
	};
	table.addColumn(getColumnMrnReqDate, "Material Required Date");
	table.setColumnWidth(getColumnMrnReqDate,200,Unit.PX);
	getColumnMrnReqDate.setSortable(true);
}

private void addColumnMrnTitle() {
	getColumnMrnTitle = new TextColumn<MaterialRequestNote>() {
		@Override
		public String getValue(MaterialRequestNote object) {
			if (object.getMrnTitle()!=null){
				return object.getMrnTitle();
			}
			return null;
		}
	};
	table.addColumn(getColumnMrnTitle, "Title");
	table.setColumnWidth(getColumnMrnTitle,90,Unit.PX);
	getColumnMrnTitle.setSortable(true);
}

private void addColumnMrnCategory() {
	getColumnMrnCategory = new TextColumn<MaterialRequestNote>() {
		@Override
		public String getValue(MaterialRequestNote object) {
			if (object.getMrnCategory()!=null){
				return object.getMrnCategory();
			}
			else{
				return "";
			}
		}
	};
	table.addColumn(getColumnMrnCategory, "Category");
	table.setColumnWidth(getColumnMrnCategory,90,Unit.PX);
	getColumnMrnCategory.setSortable(true);
}

private void addColumnMrnType() {
	getColumnMrnType = new TextColumn<MaterialRequestNote>() {
		@Override
		public String getValue(MaterialRequestNote object) {
			if (object.getMrnType()!=null){
				return object.getMrnType();
			}
			return null;
		}
	};
	table.addColumn(getColumnMrnType, "Type");
	table.setColumnWidth(getColumnMrnType,90,Unit.PX);
	getColumnMrnType.setSortable(true);
}

private void addColumnMrnSoId() {
	getColumnMrnSoId = new TextColumn<MaterialRequestNote>() {
		@Override
		public String getValue(MaterialRequestNote object) {
			if (object.getMrnSoId() == -1)
				return "N.A";
			else
				return object.getMrnSoId() + "";
		}
	};
	table.addColumn(getColumnMrnSoId, "Sales Order Id");
	table.setColumnWidth(getColumnMrnSoId,135,Unit.PX);
	getColumnMrnSoId.setSortable(true);
}

private void addColumnMrnWoId() {
	getColumnMrnWoId = new TextColumn<MaterialRequestNote>() {
		@Override
		public String getValue(MaterialRequestNote object) {
			if (object.getMrnWoId() == -1)
				return "N.A";
			else
				return object.getMrnWoId() + "";
		}
	};
	table.addColumn(getColumnMrnWoId, "WO ID");
	table.setColumnWidth(getColumnMrnWoId,90,Unit.PX);
	getColumnMrnWoId.setSortable(true);
}


private void addColumnMrnBranch() {
	getColumnMrnBranch = new TextColumn<MaterialRequestNote>() {
		@Override
		public String getValue(MaterialRequestNote object) {
			if (object.getBranch()!=null){
				return object.getBranch();
			}
			return null;
		}
	};
	table.addColumn(getColumnMrnBranch, "Branch");
	table.setColumnWidth(getColumnMrnBranch,90,Unit.PX);
	getColumnMrnBranch.setSortable(true);
}

private void addColumnMrnRequestedBy() {
	getColumnMrnRequestedBy = new TextColumn<MaterialRequestNote>() {
		@Override
		public String getValue(MaterialRequestNote object) {
			if (object.getEmployee()!=null){
				return object.getEmployee();
			}
			return null;
		}
	};
	table.addColumn(getColumnMrnRequestedBy, "Requested By");
	table.setColumnWidth(getColumnMrnRequestedBy,117,Unit.PX);
	getColumnMrnRequestedBy.setSortable(true);
}

private void addColumnMrnApprover() {
	getColumnMrnApprover = new TextColumn<MaterialRequestNote>() {
		@Override
		public String getValue(MaterialRequestNote object) {
			if (object.getApproverName()!=null){
				return object.getApproverName();
			}
			return null;
		}
	};
	table.addColumn(getColumnMrnApprover, "Approver");
	table.setColumnWidth(getColumnMrnApprover,90,Unit.PX);
	getColumnMrnApprover.setSortable(true);
}

private void addColumnMrnStatus() {
	getColumnMrnStatus = new TextColumn<MaterialRequestNote>() {
		@Override
		public String getValue(MaterialRequestNote object) {
			if (object.getStatus()!=null){
				return object.getStatus();
			}
			return null;
		}
	};
	table.addColumn(getColumnMrnStatus, "Status");
	table.setColumnWidth(getColumnMrnStatus,90,Unit.PX);
	getColumnMrnStatus.setSortable(true);
}

public void addColumnSorting() {
	addColumnSortingMrnId();
	addColumnSortingMrnDate();
	addColumnSortingMrnReqDate();
	addColumnSortingMrnTitle();
	addColumnSortingMrnCategory();
	addColumnSortingMrnType();
	addColumnSortingMrnBranch();
	addColumnSortingMrnRequestedBy();
	addColumnSortingMrnApprover();
	addColumnSortingMrnSoId();
	addColumnSortingMrnWoId();
	addColumnSortingMrnStatus();
}	

/***********************************************Column Sorting***************************************/	
private void addColumnSortingMrnId() {
	List<MaterialRequestNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialRequestNote>(list);
	columnSort.setComparator(getColumnMrnId, new Comparator<MaterialRequestNote>() {
		@Override
		public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getCount() == e2.getCount()) {
					return 0;
				}
				if (e1.getCount() > e2.getCount()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMrnDate() {
	List<MaterialRequestNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialRequestNote>(list);
	columnSort.setComparator(getColumnMrnDate, new Comparator<MaterialRequestNote>() {
		@Override
		public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMrnDate() != null && e2.getMrnDate() != null) {
					return e1.getMrnDate().compareTo(e2.getMrnDate());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}


private void addColumnSortingMrnReqDate() {
	List<MaterialRequestNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialRequestNote>(list);
	columnSort.setComparator(getColumnMrnReqDate, new Comparator<MaterialRequestNote>() {
		@Override
		public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMrnRequiredDate() != null && e2.getMrnRequiredDate() != null) {
					return e1.getMrnRequiredDate().compareTo(e2.getMrnRequiredDate());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}
private void addColumnSortingMrnSoId() {
	List<MaterialRequestNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialRequestNote>(list);
	columnSort.setComparator(getColumnMrnSoId, new Comparator<MaterialRequestNote>() {
		@Override
		public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMrnSoId() == e2.getMrnSoId()) {
					return 0;
				}
				if (e1.getMrnSoId() > e2.getMrnSoId()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMrnWoId() {
	List<MaterialRequestNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialRequestNote>(list);
	columnSort.setComparator(getColumnMrnWoId, new Comparator<MaterialRequestNote>() {
		@Override
		public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMrnWoId() == e2.getMrnWoId()) {
					return 0;
				}
				if (e1.getMrnWoId() > e2.getMrnWoId()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMrnTitle() {
	List<MaterialRequestNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialRequestNote>(list);
	columnSort.setComparator(getColumnMrnTitle, new Comparator<MaterialRequestNote>() {
		@Override
		public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMrnTitle() != null && e2.getMrnTitle() != null) {
					return e1.getMrnTitle().compareTo(e2.getMrnTitle());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMrnCategory() {
	List<MaterialRequestNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialRequestNote>(list);
	columnSort.setComparator(getColumnMrnCategory, new Comparator<MaterialRequestNote>() {
		@Override
		public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMrnCategory() != null && e2.getMrnCategory() != null) {
					return e1.getMrnCategory().compareTo(e2.getMrnCategory());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMrnType() {
	List<MaterialRequestNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialRequestNote>(list);
	columnSort.setComparator(getColumnMrnType, new Comparator<MaterialRequestNote>() {
		@Override
		public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMrnType() != null && e2.getMrnType() != null) {
					return e1.getMrnType().compareTo(e2.getMrnType());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}



private void addColumnSortingMrnBranch() {
	List<MaterialRequestNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialRequestNote>(list);
	columnSort.setComparator(getColumnMrnBranch, new Comparator<MaterialRequestNote>() {
		@Override
		public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getBranch() != null && e2.getBranch() != null) {
					return e1.getBranch().compareTo(e2.getBranch());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}



private void addColumnSortingMrnRequestedBy() {
	List<MaterialRequestNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialRequestNote>(list);
	columnSort.setComparator(getColumnMrnRequestedBy, new Comparator<MaterialRequestNote>() {
		@Override
		public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getEmployee() != null && e2.getEmployee() != null) {
					return e1.getEmployee().compareTo(e2.getEmployee());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMrnApprover() {
	List<MaterialRequestNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialRequestNote>(list);
	columnSort.setComparator(getColumnMrnApprover, new Comparator<MaterialRequestNote>() {
		@Override
		public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getApproverName() != null && e2.getApproverName() != null) {
					return e1.getApproverName().compareTo(e2.getApproverName());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMrnStatus() {
	List<MaterialRequestNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialRequestNote>(list);
	columnSort.setComparator(getColumnMrnStatus, new Comparator<MaterialRequestNote>() {
		@Override
		public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getStatus() != null && e2.getStatus() != null) {
					return e1.getStatus().compareTo(e2.getStatus());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

@Override
protected void initializekeyprovider() {
	
}

@Override
public void addFieldUpdater() {
	
}

@Override
public void setEnable(boolean state) {
	
}

@Override
public void applyStyle() {
	
}

	/**
	 * Date 06-02-2019 by Vijay for NBHC Inventory management to show product details
	 */

	private void addColumnViewProdDeatils() {
		ButtonCell btnCell = new ButtonCell();
		getcolumnProdDetails = new Column<MaterialRequestNote, String>(btnCell) {
			
			@Override
			public String getValue(MaterialRequestNote object) {
				return "Product Details";
			}
		};
		table.addColumn(getcolumnProdDetails,"Product Info");
		table.setColumnWidth(getcolumnProdDetails, 100,Unit.PX);
		
		getcolumnProdDetails.setFieldUpdater(new FieldUpdater<MaterialRequestNote, String>() {
			
			@Override
			public void update(int index, MaterialRequestNote object, String value) {
				flag = true;
				materailpopup.showPopUp();
				materailpopup.getMrnProdTable().getDataprovider().setList(object.getSubProductTableMrn());
				materailpopup.getMrnProdTable().setEnable(false);
			}
		});
	}
	/**
	 * ends here
	 */

}
