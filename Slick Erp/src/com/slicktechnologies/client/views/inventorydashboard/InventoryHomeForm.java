package com.slicktechnologies.client.views.inventorydashboard;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.MyInlineLabel;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.inspection.InspectionPresenter;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNotePresenter;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNoteTableProxy;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNotePresenter;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNoteTableProxy;
import com.slicktechnologies.client.views.inventory.materialreuestnote.MaterialRequestNotePresenter;
import com.slicktechnologies.client.views.inventory.materialreuestnote.MaterialRequestNoteTableProxy;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNPresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNSearchTable;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;

public class InventoryHomeForm extends ViewContainer implements ClickHandler{

	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	/** String array repersenting the table header menus*/
	String[] tableheaderbarnames={"New","Download"};
	
	/** Flow panel for holding the Table header menus */
	FlowPanel tableheader;
	
	/** MRN Table which can come on form component can be made generic */
	public MrnTableProxy mrnTable;
	
	/** MIN table which can come on form component can be made generic */
	public MinTableProxy minTable;
	
	/** MMN table which can come on form component can be made generic */
	public MmnTableProxy mmntable;
	
	/** GRN Table which can come on the form, this component can be made generic */
	public GrnTableProxy grnTable;
	
	/** Inspection Table which can come on the form, this component can be made generic */
	public InspectionTableProxy inspectionTable;
	
	//**************************rohan added this fields in dashboard for searching by branch and sales person  
	
		ObjectListBox<Branch> olbbranch=new ObjectListBox<Branch>();
		//************************************changes ends here ********************
	
	DateBox dbFromDate;
	DateBox dbToDate;
	Button btnGo;
	
//	/** Product Inventory View Table which can come on the form, this component can be made generic */
//	public ProductInventoryViewTable pivTable;
	
	public InventoryHomeForm() {
		toggleAppHeaderBarMenu();
		createGui();
	}
	
	private void initializeWidget(){
		
		//***********************rohan changes here for searching for by branch and sales person ****
				olbbranch =new ObjectListBox<Branch>();
				AppUtility.makeBranchListBoxLive(olbbranch);
				
		//************************************changes ends here ********************
				
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		dbFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		dbToDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		btnGo=new Button("GO");
	}

	/**
	 * Toggles the app header bar menu.
	 */
	private void toggleAppHeaderBarMenu() {
		InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
		for(int k=0;k<menus.length;k++)
		{
			String text=menus[k].getText();
			if(text.contains("Search"))
			{
				menus[k].setVisible(true); 
			}
			else{
				menus[k].setVisible(false);  
			}
		}
	}
	
	@Override
	public void createGui() 
	{
		initializeWidget();
		
		FlowPanel panel= new FlowPanel();
		
		panel.getElement().getStyle().setMarginTop(10, Unit.PX);
		panel.getElement().getStyle().setMarginLeft(170, Unit.PX);
		panel.getElement().getStyle().setMarginRight(170, Unit.PX);
		
		FlowPanel innerpanel= new FlowPanel();
		
		InlineLabel blank = new InlineLabel("    ");
		
		//***********************rohan changes here for searching for by branch and sales person ****
		InlineLabel branch = new InlineLabel("  Branch  ");
		branch.setSize("90px", "20px");
		innerpanel.add(branch);
		innerpanel.add(olbbranch);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
//		innerpanel.add(blank);
//		
//**************************changes ends here ********************************
		
		InlineLabel fromDate = new InlineLabel("  From Date ");
		fromDate.setSize("90px", "20px");
		innerpanel.add(fromDate);
		innerpanel.add(dbFromDate);
		
		InlineLabel toDate = new InlineLabel("  To Date ");
		innerpanel.add(toDate);
		innerpanel.add(dbToDate);
		innerpanel.add(blank);
		innerpanel.add(blank);
		innerpanel.add(blank);
		innerpanel.add(blank);
		innerpanel.add(btnGo);
		
		panel.add(innerpanel);
		content.add(panel);
		
		//Create the Four Scroll Panel To hold four types of Screen
		ScrollPanel[] scrollPanes=new ScrollPanel[5];
		//String array to hold captions
		String[]captions={"MRN","MIN","MMN","GRN","Inspection"}; //,"Product Inventory View"};
		//Screen array to hold new Screen object for the purpose of Redirection
		Screen[]screens={Screen.MATERIALREQUESTNOTE,Screen.MATERIALISSUENOTE,Screen.MATERIALMOVEMENTNOTE,Screen.GRN,Screen.INSPECTION}; //,Screen.PRODUCTINVENTORYVIEW};	
		
		//Create  Flow Panel,Inside Flow Panel 
		for(int i=0;i<captions.length;i++)
		{
			FlowPanel holder= new FlowPanel();
			tableheader= new FlowPanel();
			tableheader.getElement().setId("tableheader");
			// Provides Table Header which is responsible for new or Download Buttons
			setTableHeaderBar(screens[i]);
			
			tableheader.getElement().getStyle().setMarginTop(10, Unit.PX);
			tableheader.getElement().getStyle().setMarginBottom(10, Unit.PX);
			
			scrollPanes[i]= new ScrollPanel();
			scrollPanes[i].setHeight("150px");
		
			holder.add(tableheader);
			holder.add(scrollPanes[i]);
			//Caption Panel inside which a Flow Panel is added
			CaptionPanel cap= new CaptionPanel(captions[i]);
			cap.add(holder);
			cap.getElement().setClassName("tablecaption");
			//Add caption Panel on Content
		    content.add(cap);
		}
		
		//Add MRN Table inside the Scroll Panel
		mrnTable = new MrnTableProxy();
		scrollPanes[0].add(mrnTable.content);
		
		//Add MIN Panel inside the table
		minTable = new MinTableProxy();
		scrollPanes[1].add(minTable.content);
		
		//Add MMN Table
		mmntable = new MmnTableProxy();
		scrollPanes[2].add(mmntable.content);
		
		//Add GRN Table inside the Scroll Panel
		grnTable = new GrnTableProxy();
		scrollPanes[3].add(grnTable.content);
		
		//Add Inspection Table inside the Scroll Panel
		inspectionTable = new InspectionTableProxy();
		scrollPanes[4].add(inspectionTable.content);
		
		//Add PIV Table inside the Scroll Panel
//		pivTable = new ProductInventoryViewTable();
//		scrollPanes[4].add(pivTable.content);
		
		content.getElement().setId("homeview");
	  }
	
	
	public void setTableHeaderBar(Screen screen)
	{
		for(int j=0;j<getTableheaderbarnames().length;j++)
		{
			MyInlineLabel lbl;
			lbl=new MyInlineLabel(getTableheaderbarnames()[j],screen);
            getTableheader().add(lbl);
			lbl.getElement().setId("tableheadermenu");
			lbl.addClickHandler(this);
		}
	}
	
	
	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource() instanceof MyInlineLabel)
		{
			MyInlineLabel lbl=(MyInlineLabel) event.getSource();
			redirectOnScreens(lbl);
		}
	}
	
	
	

	private void redirectOnScreens(MyInlineLabel lbl)
	{
		Screen screen=(Screen) lbl.redirectScreen;
		String title=lbl.getText().trim();
		switch(screen)
		{
		  case GRN:
			  
				if(title.equals("Download"))
				{
					reactOnGrnDownLoad();
				}
				
				if(title.equals("New"))
				{
					GRNPresenter.initalize();
				}
				
				break;
				
		  case MATERIALREQUESTNOTE:
			  
				if(title.equals("Download"))
				{
					reactOnMrnDownLoad();
				}
				
				if(title.equals("New"))
				{
					MaterialRequestNotePresenter.initialize();    		  
				}
				
				break;
				
		  case MATERIALISSUENOTE:
			  
				if(title.equals("Download"))
				{
					reactOnMinDownLoad();
				}
				
				if(title.equals("New"))
				{
					MaterialIssueNotePresenter.initialize();
				}
				
				break;
				
		  case MATERIALMOVEMENTNOTE:
			 
				if(title.equals("Download"))
				{
					reactOnMmnDownLoad();
				}
				if(title.equals("New"))
				{
					MaterialMovementNotePresenter.initialize();
				}
				break;
		  case INSPECTION:
				 
				if(title.equals("Download"))
				{
					reactOnInspectionDownLoad();
				}
				if(title.equals("New"))
				{
					InspectionPresenter.initalize();;
				}
				break;
		default:
			break;
		}
	}
	
	private void reactOnInspectionDownLoad() {
		ArrayList<Inspection> custarray = new ArrayList<Inspection>();
		List<Inspection> list = (List<Inspection>) inspectionTable.getListDataProvider().getList();
		custarray.addAll(list);
		csvservice.setInspectionDetails(custarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 82;
				Window.open(url, "test", "enabled");
			}
		});
	}

	private void reactOnGrnDownLoad()
	{
		ArrayList<GRN> grnarray=new ArrayList<GRN>();
		List<GRN> list=(List<GRN>) grnTable.getListDataProvider().getList();
		grnarray.addAll(list);
		csvservice.setGRN(grnarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+48;
				Window.open(url, "test", "enabled");
			}
		});
	}


	private void reactOnMrnDownLoad()
	{
		ArrayList<MaterialRequestNote> mrnarray=new ArrayList<MaterialRequestNote>();
		List<MaterialRequestNote> list=(List<MaterialRequestNote>) mrnTable.getListDataProvider().getList();
		mrnarray.addAll(list);
		csvservice.setMaterialRequestNote(mrnarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+63;
				Window.open(url, "test", "enabled");
			}
		});
	}


	private void reactOnMinDownLoad()
	{
		ArrayList<MaterialIssueNote> minarray=new ArrayList<MaterialIssueNote>();
		List<MaterialIssueNote> list=(List<MaterialIssueNote>) minTable.getDataprovider().getList();
		minarray.addAll(list);
		csvservice.setMaterialIssueNote(minarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+64;
				Window.open(url, "test", "enabled");
			}
		});
	}

	private void reactOnMmnDownLoad()
	{
		ArrayList<MaterialMovementNote> mmnarray=new ArrayList<MaterialMovementNote>();
		List<MaterialMovementNote> list=(List<MaterialMovementNote>) mmntable.getDataprovider().getList();
		mmnarray.addAll(list);
		csvservice.setMaterialMovementNote(mmnarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+66;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	@Override
	public void applyStyle() {
		
	}


	
	
	
	
	
	
	// ************************************** Getter and Setter ***********************************************
	
	
	public String[] getTableheaderbarnames() {
		return tableheaderbarnames;
	}


	public void setTableheaderbarnames(String[] tableheaderbarnames) {
		this.tableheaderbarnames = tableheaderbarnames;
	}


	public FlowPanel getTableheader() {
		return tableheader;
	}


	public void setTableheader(FlowPanel tableheader) {
		this.tableheader = tableheader;
	}


	public GrnTableProxy getGrnTable() {
		return grnTable;
	}


	public void setGrnTable(GrnTableProxy grnTable) {
		this.grnTable = grnTable;
	}


	public MrnTableProxy getMrnTable() {
		return mrnTable;
	}


	public void setMrnTable(MrnTableProxy mrnTable) {
		this.mrnTable = mrnTable;
	}


	public MinTableProxy getMinTable() {
		return minTable;
	}


	public void setMinTable(MinTableProxy minTable) {
		this.minTable = minTable;
	}


	public MmnTableProxy getMmntable() {
		return mmntable;
	}


	public void setMmntable(MmnTableProxy mmntable) {
		this.mmntable = mmntable;
	}

	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}

	public Button getBtnGo() {
		return btnGo;
	}

	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}

	public InspectionTableProxy getInspectionTable() {
		return inspectionTable;
	}

	public void setInspectionTable(InspectionTableProxy inspectionTable) {
		this.inspectionTable = inspectionTable;
	}

	public ObjectListBox<Branch> getOlbbranch() {
		return olbbranch;
	}

	public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
		this.olbbranch = olbbranch;
	}

	
	
	
	
	

}
