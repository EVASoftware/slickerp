package com.slicktechnologies.client.views.inventorydashboard;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.views.popups.MaterialInfoDeatilsPopup;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;

public class MmnTableProxy extends SuperTable<MaterialMovementNote> {TextColumn<MaterialMovementNote> getColumnMmnId;
Column<MaterialMovementNote, Date> getColumnMmnDate;
TextColumn<MaterialMovementNote> getColumnMmnTitle;
TextColumn<MaterialMovementNote> getColumnMmnCategory;
TextColumn<MaterialMovementNote> getColumnMmnType;
TextColumn<MaterialMovementNote> getColumnMmnTransaction;
TextColumn<MaterialMovementNote> getColumnMmnSoId;
TextColumn<MaterialMovementNote> getColumnMmnWoId;
TextColumn<MaterialMovementNote> getColumnMmnBranch;
TextColumn<MaterialMovementNote> getColumnMmnPersonResponsible;
TextColumn<MaterialMovementNote> getColumnMmnApprover;
TextColumn<MaterialMovementNote> getColumnMmnStatus;

/**
 * Date 07-02-2019 by Vijay
 * Des :- For NBHC Inventory Management Product details in popup
 */
 Column<MaterialMovementNote, String> getcolumnProdDetails;
 MaterialInfoDeatilsPopup materailpopup = new MaterialInfoDeatilsPopup();
 public static boolean flag = false;
/**
 * ends here
 */

@Override
public void createTable() {
	addColumnMmnId();
	addColumnMmnTitle();
	/*** Date 07-02-2019 by Vijay for button to display prod details **/
	addColumnViewProdDeatils();
	addColumnMmnDate();
	addColumnMmnWoId();
	addColumnMmnTransaction();
	addColumnMmnCategory();
	addColumnMmnType();
	addColumnMmnPersonResponsible();
	addColumnMmnApprover();
	addColumnMmnBranch();
	addColumnMmnStatus();
}


/**********************************************************************************************************************/
private void addColumnMmnId() {
	getColumnMmnId = new TextColumn<MaterialMovementNote>() {
		@Override
		public String getValue(MaterialMovementNote object) {
			if (object.getCount() == -1)
				return "N.A";
			else
				return object.getCount() + "";
		}
	};
	table.addColumn(getColumnMmnId, "Id");
	table.setColumnWidth(getColumnMmnId,90,Unit.PX);
	getColumnMmnId.setSortable(true);
}

private void addColumnMmnDate() {
	DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
	DatePickerCell date = new DatePickerCell(fmt);
	
	final Date sub7Days=new Date();
	System.out.println("Todays Date ::: "+sub7Days);
	CalendarUtil.addDaysToDate(sub7Days,-7);
	System.out.println("Date - 7 Days ::: "+sub7Days);
	sub7Days.setHours(23);
	sub7Days.setMinutes(59);
	sub7Days.setSeconds(59);
	System.out.println("Converted Date ::: "+sub7Days);
	
	getColumnMmnDate = new Column<MaterialMovementNote, Date>(date) {
		@Override
		public String getCellStyleNames(Context context, MaterialMovementNote object) {
			if((object.getMmnDate().before(sub7Days))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Approved")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
		
		@Override
		public Date getValue(MaterialMovementNote object) {
			if (object.getMmnDate()!= null) {
				return object.getMmnDate();
			}
			return null;
		}
	};
	table.addColumn(getColumnMmnDate, "Date");
	table.setColumnWidth(getColumnMmnDate,120,Unit.PX);
	getColumnMmnDate.setSortable(true);
}

private void addColumnMmnTitle() {
	getColumnMmnTitle = new TextColumn<MaterialMovementNote>() {
		@Override
		public String getValue(MaterialMovementNote object) {
			if (object.getMmnTitle()!=null){
				return object.getMmnTitle();
			}
			return null;
		}
	};
	table.addColumn(getColumnMmnTitle, "Title");
	table.setColumnWidth(getColumnMmnTitle,90,Unit.PX);
	getColumnMmnTitle.setSortable(true);
}

private void addColumnMmnCategory() {
	getColumnMmnCategory = new TextColumn<MaterialMovementNote>() {
		@Override
		public String getValue(MaterialMovementNote object) {
			if (object.getMmnCategory()!=null){
				return object.getMmnCategory();
			}
			return null;
		}
	};
	table.addColumn(getColumnMmnCategory, "Category");
	table.setColumnWidth(getColumnMmnCategory,90,Unit.PX);
	getColumnMmnCategory.setSortable(true);
}

private void addColumnMmnType() {
	getColumnMmnType = new TextColumn<MaterialMovementNote>() {
		@Override
		public String getValue(MaterialMovementNote object) {
			if (object.getMmnType()!=null){
				return object.getMmnType();
			}
			return null;
		}
	};
	table.addColumn(getColumnMmnType, "Type");
	table.setColumnWidth(getColumnMmnType,90,Unit.PX);
	getColumnMmnType.setSortable(true);
}

private void addColumnMmnTransaction() {
	getColumnMmnTransaction= new TextColumn<MaterialMovementNote>() {

		@Override
		public String getValue(MaterialMovementNote object) {
			return object.getMmnTransactionType();
		}
	};
	table.addColumn(getColumnMmnTransaction, "Transaction");
	table.setColumnWidth(getColumnMmnTransaction,100,Unit.PX);
	getColumnMmnTransaction.setSortable(true);
}
private void addColumnMmnBranch() {
	getColumnMmnBranch = new TextColumn<MaterialMovementNote>() {
		@Override
		public String getValue(MaterialMovementNote object) {
			if (object.getBranch()!=null){
				return object.getBranch();
			}
			return null;
		}
	};
	table.addColumn(getColumnMmnBranch, "Branch");
	table.setColumnWidth(getColumnMmnBranch,90,Unit.PX);
	getColumnMmnBranch.setSortable(true);
}


private void addColumnMmnPersonResponsible() {
	getColumnMmnPersonResponsible = new TextColumn<MaterialMovementNote>() {
		@Override
		public String getValue(MaterialMovementNote object) {
			if (object.getEmployee()!=null){
				return object.getEmployee();
			}
			return null;
		}
	};
	table.addColumn(getColumnMmnPersonResponsible, "Person Responsible");
	table.setColumnWidth(getColumnMmnPersonResponsible,160,Unit.PX);
	getColumnMmnPersonResponsible.setSortable(true);
}

private void addColumnMmnApprover() {
	getColumnMmnApprover = new TextColumn<MaterialMovementNote>() {
		@Override
		public String getValue(MaterialMovementNote object) {
			if (object.getApproverName()!=null){
				return object.getApproverName();
			}
			return null;
		}
	};
	table.addColumn(getColumnMmnApprover, "Approver");
	table.setColumnWidth(getColumnMmnApprover,90,Unit.PX);
	getColumnMmnApprover.setSortable(true);
}


private void addColumnMmnWoId() {
	getColumnMmnWoId = new TextColumn<MaterialMovementNote>() {
		@Override
		public String getValue(MaterialMovementNote object) {
			if (object.getMmnWoId() == -1)
				return "N.A";
			else
				return object.getMmnWoId() + "";
		}
	};
	table.addColumn(getColumnMmnWoId, "WO Id");
	table.setColumnWidth(getColumnMmnWoId,90,Unit.PX);
	getColumnMmnWoId.setSortable(true);
}

private void addColumnMmnStatus() {
	getColumnMmnStatus = new TextColumn<MaterialMovementNote>() {
		@Override
		public String getValue(MaterialMovementNote object) {
			if (object.getStatus()!=null){
				return object.getStatus();
			}
			return null;
		}
	};
	table.addColumn(getColumnMmnStatus, "Status");
	table.setColumnWidth(getColumnMmnStatus,90,Unit.PX);
	getColumnMmnStatus.setSortable(true);
}



/***********************************************************************************************************/

public void addColumnSorting() {
	addColumnSortingMmnId();
	addColumnSortingMmnDate();
	addColumnSortingMmnTitle();
	addColumnSortingMmnCategory();
	addColumnSortingMmnType();
	addColumnSortingMmnTransaction();
	addColumnSortingMmnBranch();
	addColumnSortingMmnPersonResponsible();
	addColumnSortingMmnApprover();
	addColumnSortingMmnWoId();
	addColumnSortingMmnStatus();
}


/***********************************************Column Sorting***************************************/	
private void addColumnSortingMmnId() {
	List<MaterialMovementNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialMovementNote>(list);
	columnSort.setComparator(getColumnMmnId, new Comparator<MaterialMovementNote>() {
		@Override
		public int compare(MaterialMovementNote e1, MaterialMovementNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getCount() == e2.getCount()) {
					return 0;
				}
				if (e1.getCount() > e2.getCount()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMmnDate() {
	List<MaterialMovementNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialMovementNote>(list);
	columnSort.setComparator(getColumnMmnDate, new Comparator<MaterialMovementNote>() {
		@Override
		public int compare(MaterialMovementNote e1, MaterialMovementNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMmnDate() != null && e2.getMmnDate() != null) {
					return e1.getMmnDate().compareTo(e2.getMmnDate());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}



private void addColumnSortingMmnWoId() {
	List<MaterialMovementNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialMovementNote>(list);
	columnSort.setComparator(getColumnMmnWoId, new Comparator<MaterialMovementNote>() {
		@Override
		public int compare(MaterialMovementNote e1, MaterialMovementNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMmnWoId() == e2.getMmnWoId()) {
					return 0;
				}
				if (e1.getMmnWoId() > e2.getMmnWoId()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMmnTitle() {
	List<MaterialMovementNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialMovementNote>(list);
	columnSort.setComparator(getColumnMmnTitle, new Comparator<MaterialMovementNote>() {
		@Override
		public int compare(MaterialMovementNote e1, MaterialMovementNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMmnTitle() != null && e2.getMmnTitle() != null) {
					return e1.getMmnTitle().compareTo(e2.getMmnTitle());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMmnCategory() {
	List<MaterialMovementNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialMovementNote>(list);
	columnSort.setComparator(getColumnMmnCategory, new Comparator<MaterialMovementNote>() {
		@Override
		public int compare(MaterialMovementNote e1, MaterialMovementNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMmnCategory() != null && e2.getMmnCategory() != null) {
					return e1.getMmnCategory().compareTo(e2.getMmnCategory());
				}
				if (e1.getMmnCategory().equals("") && e2.getMmnCategory().equals("")) {
					return 0;
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMmnType() {
	List<MaterialMovementNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialMovementNote>(list);
	columnSort.setComparator(getColumnMmnType, new Comparator<MaterialMovementNote>() {
		@Override
		public int compare(MaterialMovementNote e1, MaterialMovementNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMmnType() != null && e2.getMmnType() != null) {
					return e1.getMmnType().compareTo(e2.getMmnType());
				}
				if (e1.getMmnType().equals("") && e2.getMmnType().equals("")) {
					return 0;
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMmnTransaction() {
	List<MaterialMovementNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialMovementNote>(list);
	columnSort.setComparator(getColumnMmnTransaction, new Comparator<MaterialMovementNote>() {
		@Override
		public int compare(MaterialMovementNote e1, MaterialMovementNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getMmnTransactionType() != null && e2.getMmnTransactionType() != null) {
					return e1.getMmnTransactionType().compareTo(e2.getMmnTransactionType());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}



private void addColumnSortingMmnBranch() {
	List<MaterialMovementNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialMovementNote>(list);
	columnSort.setComparator(getColumnMmnBranch, new Comparator<MaterialMovementNote>() {
		@Override
		public int compare(MaterialMovementNote e1, MaterialMovementNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getBranch() != null && e2.getBranch() != null) {
					return e1.getBranch().compareTo(e2.getBranch());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMmnPersonResponsible() {
	List<MaterialMovementNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialMovementNote>(list);
	columnSort.setComparator(getColumnMmnPersonResponsible, new Comparator<MaterialMovementNote>() {
		@Override
		public int compare(MaterialMovementNote e1, MaterialMovementNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getEmployee() != null && e2.getEmployee() != null) {
					return e1.getEmployee().compareTo(e2.getEmployee());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

private void addColumnSortingMmnApprover() {
	List<MaterialMovementNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialMovementNote>(list);
	columnSort.setComparator(getColumnMmnApprover, new Comparator<MaterialMovementNote>() {
		@Override
		public int compare(MaterialMovementNote e1, MaterialMovementNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getApproverName() != null && e2.getApproverName() != null) {
					return e1.getApproverName().compareTo(e2.getApproverName());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}



private void addColumnSortingMmnStatus() {
	List<MaterialMovementNote> list = getDataprovider().getList();
	columnSort = new ListHandler<MaterialMovementNote>(list);
	columnSort.setComparator(getColumnMmnStatus, new Comparator<MaterialMovementNote>() {
		@Override
		public int compare(MaterialMovementNote e1, MaterialMovementNote e2) {
			if (e1 != null && e2 != null) {
				if (e1.getStatus() != null && e2.getStatus() != null) {
					return e1.getStatus().compareTo(e2.getStatus());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

@Override
protected void initializekeyprovider() {
	
}

@Override
public void addFieldUpdater() {
	
}

@Override
public void setEnable(boolean state) {
	
}

@Override
public void applyStyle() {
	
}


/**
 * Date 06-02-2019 by Vijay for NBHC Inventory management to show product details
 */

private void addColumnViewProdDeatils() {
	ButtonCell btnCell = new ButtonCell();
	getcolumnProdDetails = new Column<MaterialMovementNote, String>(btnCell) {
		
		@Override
		public String getValue(MaterialMovementNote object) {
			return "Product Details";
		}
	};
	table.addColumn(getcolumnProdDetails,"Product Info");
	table.setColumnWidth(getcolumnProdDetails, 100,Unit.PX);
	
	getcolumnProdDetails.setFieldUpdater(new FieldUpdater<MaterialMovementNote, String>() {
		
		@Override
		public void update(int index, MaterialMovementNote object, String value) {
			flag = true;
			materailpopup.showPopUp();
			materailpopup.getMrnProdTable().getDataprovider().setList(object.getSubProductTableMmn());
			materailpopup.getMrnProdTable().setEnable(false);
		}
	});
}
/**
 * ends here
 */
}
