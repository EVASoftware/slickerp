package com.slicktechnologies.client.views.inventorydashboard;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.inspection.InspectionForm;
import com.slicktechnologies.client.views.inventory.inspection.InspectionPresenter;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNoteForm;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNotePresenter;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNotePresenter;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNoteForm;
import com.slicktechnologies.client.views.inventory.materialreuestnote.MaterialRequestNoteForm;
import com.slicktechnologies.client.views.inventory.materialreuestnote.MaterialRequestNotePresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNForm;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNPresenter;
import com.slicktechnologies.client.views.purchasedashboard.PrTableProxy;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;

import java.util.Date;

import com.google.gwt.user.datepicker.client.CalendarUtil;

public class InventoryHomePresenter implements ClickHandler{
	InventoryHomeForm form;
	InventoryHomeSearchProxy searchpopup;
	protected GenricServiceAsync service=GWT.create(GenricService.class);


	public  InventoryHomePresenter(InventoryHomeForm form,InventoryHomeSearchProxy searchpopup)
	{
		this.form=form;
		this.searchpopup=searchpopup;
		SetEventHandling();
		searchpopup.applyHandler(this);
		form.getBtnGo().addClickHandler(this);
		
		Date date=new Date();
		System.out.println("Today Date ::::::::: "+date);
		// **** Here We Add 7 Days to current date and pass this date to default search.
		CalendarUtil.addDaysToDate(date,7);
	    System.out.println("Changed Date + 7 ::::::::: "+date);
		
	    Date todaysDate=new Date();
	    System.out.println("Today Date ::::::::: "+todaysDate);
	    CalendarUtil.addDaysToDate(todaysDate,-7);
	    System.out.println("Changed Date - 7::::::::: "+todaysDate);
	    
	    Long compId=UserConfiguration.getCompanyId();
	    System.out.println("Company Id :: "+compId);
		
		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		List<String> statusList;
		
		////////////////////////////////////Create Querry For GRN ///////////////////////////// 
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(GRN.CREATED);
		statusList.add(GRN.REQUESTED);
		
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("creationDate <=");
		filter.setDateValue(date);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		/**
		 * Developed by : Rohan added this branch  
		 */
			List<String> branchList ;
			branchList= new ArrayList<String>();
			System.out.println("item count in branch "+LoginPresenter.globalBranch.size());
			for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
				branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
			}
			System.out.println("branchList list "+branchList.size());
		
			filter = new Filter();
			filter.setQuerryString("branch IN");
			filter.setList(branchList);
			filtervec.add(filter);
		/**
		 * ends here 
		 */
		
		querry=new MyQuerry();
		querry.setQuerryObject(new GRN());
		querry.setFilters(filtervec);
		retriveTable(querry,form.getGrnTable());
		
		//////////////////////////////////// Create Querry For MRN ///////////////////////////// 
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(MaterialRequestNote.CREATED);
		statusList.add(MaterialRequestNote.REQUESTED);
//		statusList.add(MaterialRequestNote.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("mrnDate <=");
		filter.setDateValue(date);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		/**
		 * Developed by : Rohan added this branch  
		 */
			filter = new Filter();
			filter.setQuerryString("branch IN");
			filter.setList(branchList);
			filtervec.add(filter);
		
		/**
		 * ends here 
		 */
		
		querry=new MyQuerry();
		querry.setQuerryObject(new MaterialRequestNote());
		querry.setFilters(filtervec);
		retriveTable(querry,form.getMrnTable());
		
		////////////////////////////////////Create Querry For MIN ///////////////////////////// 
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(MaterialIssueNote.CREATED);
		statusList.add(MaterialIssueNote.REQUESTED);
//		statusList.add(MaterialIssueNote.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("minDate <=");
		filter.setDateValue(date);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		/**
		 * Developed by : Rohan added this branch  
		 */
			filter = new Filter();
			filter.setQuerryString("branch IN");
			filter.setList(branchList);
			filtervec.add(filter);
		
		/**
		 * ends here 
		 */
		
		querry=new MyQuerry();
		querry.setQuerryObject(new MaterialIssueNote());
		querry.setFilters(filtervec);
		retriveTable(querry,form.getMinTable());
		
		////////////////////////// Create Querry For MMN ////////////////////////
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(MaterialMovementNote.CREATED);
		statusList.add(MaterialMovementNote.REQUESTED);
//		statusList.add(MaterialMovementNote.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("mmnDate <=");
		filter.setDateValue(date);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		/**
		 * Developed by : Rohan added this branch  
		 */
			filter = new Filter();
			filter.setQuerryString("branch IN");
			filter.setList(branchList);
			filtervec.add(filter);
		
		/**
		 * ends here 
		 */
		
		querry=new MyQuerry();
		querry.setQuerryObject(new MaterialMovementNote());
		querry.setFilters(filtervec);
		retriveTable(querry,form.getMmntable());
		
		//////////////////////////Create Query For Inspection ////////////////////////
			
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(Inspection.CREATED);
		statusList.add(Inspection.REQUESTED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("creationDate <=");
		filter.setDateValue(date);
		filtervec.add(filter);
		
		/**
		 * Developed by : Rohan added this branch  
		 */
			filter = new Filter();
			filter.setQuerryString("branch IN");
			filter.setList(branchList);
			filtervec.add(filter);
		
		/**
		 * ends here 
		 */
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Inspection());
		querry.setFilters(filtervec);
		retriveTable(querry,form.getInspectionTable());

		
		
		setTableSelectionOnGrn();
		setTableSelectionOnMrn();
		setTableSelectionOnMin();
		setTableSelectionOnMmn();
		setTableSelectionOnInspection();
		
		
	}

	public static void initalize()
	{
		InventoryHomeForm homeForm = new InventoryHomeForm();
		AppMemory.getAppMemory().currentScreen=Screen.INVENTORYHOME;
		AppMemory.getAppMemory().skeleton.getProcessName().setText("Dashboard");
		InventoryHomeSearchProxy searchpopup=new InventoryHomeSearchProxy();
		InventoryHomePresenter presenter= new InventoryHomePresenter(homeForm,searchpopup);
		AppMemory.getAppMemory().stickPnel(homeForm);	
	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource() instanceof InlineLabel)
		{
		InlineLabel lbl= (InlineLabel) event.getSource();
		String text= lbl.getText();
		if(text.equals("Search"))
			searchpopup.showPopUp();
		
		if(text.equals("Go"))
			reactOnGo();
		}
		
//		if(event.getSource().equals(form.getBtnGo())){if(form.getDbFromDate().getValue()==null||form.getDbToDate().getValue()==null){
//			final GWTCAlert alert = new GWTCAlert();
//			alert.alert("Select From and To Date.");
//		}
//		if(form.getDbFromDate().getValue()!=null&&form.getDbToDate()!=null){
//			Date formDate=form.getDbFromDate().getValue();
//			Date toDate=form.getDbToDate().getValue();
//			if(toDate.equals(formDate)||toDate.after(formDate)){
//				System.out.println("From Date ::::::::: "+formDate);
//				CalendarUtil.addDaysToDate(formDate,7);
//				System.out.println("Changed Date ::::::::: "+formDate);
//				System.out.println("To Date ::::::::: "+toDate);
//			
//				if(toDate.before(formDate)){
//					System.out.println("Inside Date Condition"); 
//					searchByFromToDate();
//				}
//				else{
//					final GWTCAlert alert = new GWTCAlert();
//					alert.alert("From Date and To Date Difference Can Not Be More Than 7 Days.");
//				}
//			}
//			else{
//				final GWTCAlert alert = new GWTCAlert();
//				alert.alert("To Date should be greater than From Date.");
//			}
//		}}
		
		
		
		if(event.getSource().equals(form.getBtnGo())){
			if(form.getOlbbranch().getSelectedIndex()==0&&
			form.getDbFromDate().getValue()==null&&form.getDbToDate().getValue()==null){
		
			final GWTCAlert alert = new GWTCAlert();
			alert.alert("Select atleast one field to search");
		}
			
		else{	
			
			if(form.getOlbbranch().getSelectedIndex()!=0
					   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
				
				System.out.println("branch ");
				searchByFromToDate();
			}
			else{
			System.out.println("Remaining all situations ");
			
			if(form.getDbFromDate().getValue()==null||form.getDbToDate().getValue()==null){
				final GWTCAlert alert = new GWTCAlert();
				alert.alert("Select From and To Date.");
			}
			 if(form.getDbFromDate().getValue()!=null&&form.getDbToDate()!=null){
			Date formDate=form.getDbFromDate().getValue();
			Date toDate=form.getDbToDate().getValue();
			if(toDate.equals(formDate)||toDate.after(formDate)){
				System.out.println("From Date ::::::::: "+formDate);
				CalendarUtil.addDaysToDate(formDate,7);
				System.out.println("Changed Date ::::::::: "+formDate);
				System.out.println("To Date ::::::::: "+toDate);
			
				if(toDate.before(formDate)){
					System.out.println("Inside Date Condition"); 
					searchByFromToDate();
				}
				else{
					final GWTCAlert alert = new GWTCAlert();
					alert.alert("From Date and To Date Difference Can Not Be More Than 7 Days.");
				}
			}
			else{
				final GWTCAlert alert = new GWTCAlert();
				alert.alert("To Date should be greater than From Date.");
			}
			}
			}
		}
		}
		
		
	}

	/**
	 * Sets the event handling.
	 */
	public void SetEventHandling()
	{
		 InlineLabel label[] =AppMemory.getAppMemory().skeleton.getMenuLabels();
		  for(int k=0;k<label.length;k++)
		  {
			 if( AppMemory.getAppMemory().skeleton.registration[k]!=null)
			    AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
		  }
		 
		  for( int k=0;k<label.length;k++)
		  {
			  AppMemory.getAppMemory().skeleton.registration[k]= label[k].addClickHandler(this);
		  }
	}

	public void reactOnGo()
	{ 
		
		
		if(!searchpopup.productInfo.getProdID().getValue().equals("")&&!searchpopup.productInfo.getProdName().getValue().equals("")&&!searchpopup.productInfo.getProdName().getValue().equals(""))
		{
			MyQuerry quer=new MyQuerry();
			
			quer=createGrnFilter(new GRN());
			this.form.grnTable.connectToLocal();
			retriveTable(quer, form.getGrnTable());
			 
			quer=createMrnFilter(new MaterialRequestNote());
			this.form.mrnTable.connectToLocal();
			retriveTable(quer, form.getMrnTable());
			 
			quer=createMinFilter(new MaterialIssueNote());
			this.form.minTable.connectToLocal();
			retriveTable(quer, form.getMinTable());
			 
			quer=createMmnFilter(new MaterialMovementNote());
			this.form.mmntable.connectToLocal();
			retriveTable(quer, form.getMmntable());
			
			quer=createInspectionFilter(new Inspection());
			this.form.inspectionTable.connectToLocal();
			retriveTable(quer, form.getInspectionTable());
			
			searchpopup.hidePopUp();
		}
		
		
		if(searchpopup.productInfo.getProdID().getValue().equals("")&&searchpopup.productInfo.getProdName().getValue().equals("")&&searchpopup.productInfo.getProdName().getValue().equals(""))
		{
			GWTCAlert alertMsg=new GWTCAlert();
			alertMsg.alert("Please enter product information");
		}
		
	}

	private MyQuerry createInspectionFilter(Inspection inspection) {
		Vector<Filter>filterVec = new Vector<Filter>();
		Filter temp;
		 
		if(searchpopup.productInfo.getProdID().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(searchpopup.productInfo.getProdID().getValue()));
			temp.setQuerryString("items.productdetails.productID");
			filterVec.add(temp);	
		}
			
		if((searchpopup.productInfo.getProdCode().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.productInfo.getProdCode().getValue());
			temp.setQuerryString("items.productdetails.productCode");
			filterVec.add(temp);	
		}
			
		if(searchpopup.productInfo.getProdName().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.productInfo.getProdName().getValue());
			temp.setQuerryString("items.productdetails.productName");
			filterVec.add(temp);	
		}
			
		MyQuerry grnquerry=new MyQuerry();
		grnquerry.setQuerryObject(inspection);
		grnquerry.setFilters(filterVec);
			
		return grnquerry;
	}

	private MyQuerry createGrnFilter(GRN grn)
	{
		Vector<Filter>filterVec = new Vector<Filter>();
		Filter temp;
		 
		if(searchpopup.productInfo.getProdID().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(searchpopup.productInfo.getProdID().getValue()));
			temp.setQuerryString("items.productdetails.productID");
			filterVec.add(temp);	
		}
			
		if((searchpopup.productInfo.getProdCode().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.productInfo.getProdCode().getValue());
			temp.setQuerryString("items.productdetails.productCode");
			filterVec.add(temp);	
		}
			
		if(searchpopup.productInfo.getProdName().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.productInfo.getProdName().getValue());
			temp.setQuerryString("items.productdetails.productName");
			filterVec.add(temp);	
		}
			
		MyQuerry grnquerry=new MyQuerry();
		grnquerry.setQuerryObject(grn);
		grnquerry.setFilters(filterVec);
			
		return grnquerry;
	}

	private MyQuerry createMrnFilter(MaterialRequestNote min)
	{
		Vector<Filter>filterVec = new Vector<Filter>();
		Filter temp;
		 
		if(searchpopup.productInfo.getProdID().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(searchpopup.productInfo.getProdID().getValue()));
			temp.setQuerryString("subProductTableMrn.materialProductId");
			filterVec.add(temp);	
		}
			
		if((searchpopup.productInfo.getProdCode().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.productInfo.getProdCode().getValue());
			temp.setQuerryString("subProductTableMrn.materialProductCode");
			filterVec.add(temp);	
		}
			
		if(searchpopup.productInfo.getProdName().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.productInfo.getProdName().getValue());
			temp.setQuerryString("subProductTableMrn.materialProductName");
			filterVec.add(temp);	
		}
			
		MyQuerry minquerry=new MyQuerry();
		minquerry.setQuerryObject(min);
		minquerry.setFilters(filterVec);
			
		return minquerry;
	}

	private MyQuerry createMinFilter(MaterialIssueNote min)
	{
		Vector<Filter>filterVec = new Vector<Filter>();
		Filter temp;
		 
		if(searchpopup.productInfo.getProdID().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(searchpopup.productInfo.getProdID().getValue()));
			temp.setQuerryString("subProductTablemin.materialProductId");
			filterVec.add(temp);	
		}
			
		if((searchpopup.productInfo.getProdCode().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.productInfo.getProdCode().getValue());
			temp.setQuerryString("subProductTablemin.materialProductCode");
			filterVec.add(temp);	
		}
			
		if(searchpopup.productInfo.getProdName().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.productInfo.getProdName().getValue());
			temp.setQuerryString("subProductTablemin.materialProductName");
			filterVec.add(temp);	
		}
			
		MyQuerry minquerry=new MyQuerry();
		minquerry.setQuerryObject(min);
		minquerry.setFilters(filterVec);
			
		return minquerry;
	}

	private MyQuerry createMmnFilter(MaterialMovementNote mmn)
	{
		Vector<Filter>filterVec = new Vector<Filter>();
		Filter temp;
		 
		if(searchpopup.productInfo.getProdID().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(searchpopup.productInfo.getProdID().getValue()));
			temp.setQuerryString("subProductTableMmn.materialProductId");
			filterVec.add(temp);	
		}
			
		if((searchpopup.productInfo.getProdCode().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.productInfo.getProdCode().getValue());
			temp.setQuerryString("subProductTableMmn.materialProductCode");
			filterVec.add(temp);	
		}
			
		if(searchpopup.productInfo.getProdName().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.productInfo.getProdName().getValue());
			temp.setQuerryString("subProductTableMmn.materialProductName");
			filterVec.add(temp);	
		}
			
		MyQuerry mmnquerry=new MyQuerry();
		mmnquerry.setQuerryObject(mmn);
		mmnquerry.setFilters(filterVec);
			
		return mmnquerry;
	}

	private MyQuerry createPivFilter(ProductInventoryView piv)
	{
		Vector<Filter>filterVec = new Vector<Filter>();
		Filter temp;
		 
		if(searchpopup.productInfo.getProdID().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(searchpopup.productInfo.getProdID().getValue()));
			temp.setQuerryString("productinfo.prodID");
			filterVec.add(temp);	
		}
			
		if((searchpopup.productInfo.getProdCode().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.productInfo.getProdCode().getValue());
			temp.setQuerryString("productinfo.productCode");
			filterVec.add(temp);	
		}
			
		if(searchpopup.productInfo.getProdName().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.productInfo.getProdName().getValue());
			temp.setQuerryString("productinfo.productName");
			filterVec.add(temp);	
		}
			
		MyQuerry pivquerry=new MyQuerry();
		pivquerry.setQuerryObject(piv);
		pivquerry.setFilters(filterVec);
			
		return pivquerry;
	}

	public void setTableSelectionOnGrn()
	{
		 final NoSelectionModel<GRN> selectionModelMyObj = new NoSelectionModel<GRN>();
		 
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	    	 @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	    		 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/GRN",Screen.GRN);
	        	 final GRN entity=selectionModelMyObj.getLastSelectedObject();
	        	 final GRNForm form = GRNPresenter.initalize();
	        	  
	        	 form.showWaitSymbol();
	        	 Timer timer=new Timer() {
	 				
	        	@Override
	 			public void run() {
	        		form.hideWaitSymbol();
					form.updateView(entity);
				    form.setToViewState();
	 			}
	 		};
	        timer.schedule(5000); 
	     }
	  };
	  // Add the handler to the selection model
	  selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	  // Add the selection model to the table
	  form.grnTable.getTable().setSelectionModel(selectionModelMyObj);
	}
	public void setTableSelectionOnMrn()
	{
		 final NoSelectionModel<MaterialRequestNote> selectionModelMyObj = new NoSelectionModel<MaterialRequestNote>();
		 
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	    	 @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	    		 /**** Date 31-01-2019 by Vijay for NBHC Inventory management Product Details popup  *****/
	        	 if(MrnTableProxy.flag==false){
	    		 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Material Request Note",Screen.MATERIALREQUESTNOTE);
	        	 final MaterialRequestNote entity=selectionModelMyObj.getLastSelectedObject();
	        	 final MaterialRequestNoteForm form = MaterialRequestNotePresenter.initialize();
	        	  
	        	 form.showWaitSymbol();
	        	 Timer timer=new Timer() {
	 				
	        	@Override
	 			public void run() {
	        		form.hideWaitSymbol();
					form.updateView(entity);
				    form.setToViewState();
	 			}
		 		};
		        timer.schedule(5000); 
		        
	         }
	        else{
	        	MrnTableProxy.flag = false;
	        }
	     }
	  };
	  // Add the handler to the selection model
	  selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	  // Add the selection model to the table
	  form.mrnTable.getTable().setSelectionModel(selectionModelMyObj);
	}
	 
	public void setTableSelectionOnMin()
	{
		 final NoSelectionModel<MaterialIssueNote> selectionModelMyObj = new NoSelectionModel<MaterialIssueNote>();
		 
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	    	 @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	    		 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Material Issue Note",Screen.MATERIALISSUENOTE);
	        	 final MaterialIssueNote entity=selectionModelMyObj.getLastSelectedObject();
	        	 final MaterialIssueNoteForm form = MaterialIssueNotePresenter.initialize();
	        	  
	        	 form.showWaitSymbol();
	        	 Timer timer=new Timer() {
	 				
	        	@Override
	 			public void run() {
	        		form.hideWaitSymbol();
					form.updateView(entity);
				    form.setToViewState();
	 			}
	 		};
	        timer.schedule(5000); 
	     }
	  };
	  // Add the handler to the selection model
	  selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	  // Add the selection model to the table
	  form.minTable.getTable().setSelectionModel(selectionModelMyObj);
	}

	public void setTableSelectionOnMmn()
	{
		 final NoSelectionModel<MaterialMovementNote> selectionModelMyObj = new NoSelectionModel<MaterialMovementNote>();
		 
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	    	 @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	    		 /**** Date 31-01-2019 by Vijay added flag condition for NBHC Inventory management Product Details popup  *****/
	        	 if(MmnTableProxy.flag==false){
	    		 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Material Movement Note",Screen.MATERIALMOVEMENTNOTE);
	        	 final MaterialMovementNote entity=selectionModelMyObj.getLastSelectedObject();
	        	 final MaterialMovementNoteForm form = MaterialMovementNotePresenter.initialize();
	        	  
	        	 form.showWaitSymbol();
	        	 Timer timer=new Timer() {
	 				
	        	@Override
	 			public void run() {
	        		form.hideWaitSymbol();
					form.updateView(entity);
				    form.setToViewState();
	 			}
		 		};
		        timer.schedule(5000); 
		        
	        	}
	        	else{
	        		MmnTableProxy.flag = false;
	        	}
	     }
	  };
	  // Add the handler to the selection model
	  selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	  // Add the selection model to the table
	  form.mmntable.getTable().setSelectionModel(selectionModelMyObj);
	}


	public void setTableSelectionOnInspection()
	{
		 final NoSelectionModel<Inspection> selectionModelMyObj = new NoSelectionModel<Inspection>();
		 
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	    	 @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	    		 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Inspection",Screen.INSPECTION);
	        	 final Inspection entity=selectionModelMyObj.getLastSelectedObject();
	        	 final InspectionForm form = InspectionPresenter.initalize();
	        	  
	        	 form.showWaitSymbol();
	        	 Timer timer=new Timer() {
	 				
	        	@Override
	 			public void run() {
	        		form.hideWaitSymbol();
					form.updateView(entity);
				    form.setToViewState();
	 			}
	 		};
	        timer.schedule(5000); 
	     }
	  };
	  // Add the handler to the selection model
	  selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	  // Add the selection model to the table
	  form.inspectionTable.getTable().setSelectionModel(selectionModelMyObj);
	}


	private void searchByFromToDate(){
		
		
		System.out.println("Hiiiiiiiiiiii........");
		
		Long compId=UserConfiguration.getCompanyId();
	    System.out.println("Company Id :: "+compId);
		
		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		List<String> statusList;
		
		////////////////////////////////////Create Querry For GRN ///////////////////////////// 
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(GRN.CREATED);
		statusList.add(GRN.REQUESTED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate >=");
		filter.setDateValue(form.dbFromDate.getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate <=");
		filter.setDateValue(form.dbToDate.getValue());
		filtervec.add(filter);
		}
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("inside lead***********");
				filter = new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(form.getOlbbranch().getValue());
				filtervec.add(filter);
				
	    }
		

		querry=new MyQuerry();
		querry.setQuerryObject(new GRN());
		querry.setFilters(filtervec);
		this.form.grnTable.connectToLocal();
		retriveTable(querry,form.getGrnTable());
		
		//////////////////////////////////// Create Querry For MRN ///////////////////////////// 
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(MaterialRequestNote.CREATED);
		statusList.add(MaterialRequestNote.REQUESTED);
//		statusList.add(MaterialRequestNote.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("mrnDate >=");
		filter.setDateValue(form.dbFromDate.getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("mrnDate <=");
		filter.setDateValue(form.dbToDate.getValue());
		filtervec.add(filter);
		}

		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("inside lead***********");
				filter = new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(form.getOlbbranch().getValue());
				filtervec.add(filter);
				
	    }
		
		querry=new MyQuerry();
		querry.setQuerryObject(new MaterialRequestNote());
		querry.setFilters(filtervec);
		this.form.mrnTable.connectToLocal();
		retriveTable(querry,form.getMrnTable());
		
		////////////////////////////////////Create Querry For MIN ///////////////////////////// 
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(MaterialIssueNote.CREATED);
		statusList.add(MaterialIssueNote.REQUESTED);
//		statusList.add(MaterialIssueNote.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("minDate >=");
		filter.setDateValue(form.dbFromDate.getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("minDate <=");
		filter.setDateValue(form.dbToDate.getValue());
		filtervec.add(filter);
		}
		
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("inside lead***********");
				filter = new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(form.getOlbbranch().getValue());
				filtervec.add(filter);
				
	    }
		
		querry=new MyQuerry();
		querry.setQuerryObject(new MaterialIssueNote());
		querry.setFilters(filtervec);
		this.form.minTable.connectToLocal();
		retriveTable(querry,form.getMinTable());
		
		////////////////////////// Create Querry For MMN ////////////////////////
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(MaterialMovementNote.CREATED);
		statusList.add(MaterialMovementNote.REQUESTED);
//		statusList.add(MaterialMovementNote.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("mmnDate >=");
		filter.setDateValue(form.dbFromDate.getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("mmnDate <=");
		filter.setDateValue(form.dbToDate.getValue());
		filtervec.add(filter);
		}
		
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("inside lead***********");
				filter = new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(form.getOlbbranch().getValue());
				filtervec.add(filter);
				
	    }
		
		querry=new MyQuerry();
		querry.setQuerryObject(new MaterialMovementNote());
		querry.setFilters(filtervec);
		this.form.mmntable.connectToLocal();
		retriveTable(querry,form.getMmntable());
		
		
		//////////////////////////Create Query For Inspection ////////////////////////
			
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(Inspection.CREATED);
		statusList.add(Inspection.REQUESTED);
//		statusList.add(Inspection.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate >=");
		filter.setDateValue(form.dbFromDate.getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate <=");
		filter.setDateValue(form.dbToDate.getValue());
		filtervec.add(filter);
		}
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("inside lead***********");
				filter = new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(form.getOlbbranch().getValue());
				filtervec.add(filter);
				
	    }
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Inspection());
		querry.setFilters(filtervec);
		retriveTable(querry,form.getInspectionTable());
	}




	 public <T> void retriveTable(MyQuerry querry,final SuperTable<T>table)
	 {
		table.connectToLocal();
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for(SuperModel model:result)
				{
					table.getListDataProvider().getList().add((T) model);
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		}); 
	 }}
