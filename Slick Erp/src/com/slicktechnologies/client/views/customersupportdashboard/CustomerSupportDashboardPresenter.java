package com.slicktechnologies.client.views.customersupportdashboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.complain.ComplainPresenterSearchProxy;
import com.slicktechnologies.client.views.customersupport.CustomerSupportPopup;
import com.slicktechnologies.client.views.dashboard.leaddashboard.CommunicationSmartPoup;
import com.slicktechnologies.client.views.dashboard.leaddashboard.LeadDashboardForm;
import com.slicktechnologies.client.views.dashboard.leaddashboard.LeadDashboardPresenter;
import com.slicktechnologies.client.views.dashboard.leaddashboard.LeadDashboardSearchForm;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;

public class CustomerSupportDashboardPresenter implements ClickHandler, CellClickHandler {
	


	CustomerSupportDashboardForm form;
	LeadDashboardSearchForm searchForm;
	
	GenricServiceAsync service=GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	ArrayList<Complain> globalComplainList=new ArrayList<Complain>();

	CommunicationSmartPoup commSmrtPopup=new CommunicationSmartPoup();
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	
	Complain complainEntity=null;
	ListGridRecord selectedRecord=null;
	int position;
	
	protected AppMemory mem;
	
	CustomerSupportPopup otherComplaintpopup = null;

	public CustomerSupportDashboardPresenter(CustomerSupportDashboardForm homeForm,LeadDashboardSearchForm searchForm2) {
		this.form=homeForm;
		this.searchForm=searchForm2;
		form.grid.addCellClickHandler(this);
		mem=AppMemory.getAppMemory();
		setEventHandeling();
//		retrieveComplainData(); //Ashwini Patil Date:23-08-2024 to stop auto loading
		
		
	}
	
	private void setEventHandeling() {
		form.getBtnGo().addClickHandler(this);
		searchForm.golbl.addClickHandler(this);
		
		commSmrtPopup.getBtnOk().addClickHandler(this);
		commSmrtPopup.getBtnCancel().addClickHandler(this);
		commSmrtPopup.getBtnAdd().addClickHandler(this);
		
		
		InlineLabel label[] = AppMemory.getAppMemory().skeleton.getMenuLabels();
		for (int k = 0; k < label.length; k++) {
			if (AppMemory.getAppMemory().skeleton.registration[k] != null)
				AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
		}
		for (int k = 0; k < label.length; k++) {
			AppMemory.getAppMemory().skeleton.registration[k] = label[k].addClickHandler(this);
		}
	}

	public static void initialize(){
		System.out.println("INSIDE CHART INITIALIZATION");
		CustomerSupportDashboardForm homeForm = new CustomerSupportDashboardForm();
		AppMemory.getAppMemory().currentScreen=Screen.HOME;
		AppMemory.getAppMemory().skeleton.getProcessName().setText(AppConstants.SERVICEMODULE+"/"+"Customer Support Dashboard");
		
		LeadDashboardSearchForm searchForm=new LeadDashboardSearchForm();
		CustomerSupportDashboardPresenter presenter= new CustomerSupportDashboardPresenter(homeForm,searchForm);
		
		AppMemory.getAppMemory().stickPnel(homeForm);	
	}
	
	public void retrieveComplainData(){
		Date date=new Date();
		System.out.println("Today Date ::::::::: "+date);
		Console.log("Today Date ::::::::: "+date);
		CalendarUtil.addDaysToDate(date,1);
//	    System.out.println("Changed Date + 7 ::::::::: "+date);
//	    Console.log("Changed Date + 7 ::::::::: "+date);
	    
		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		filtervec=new Vector<Filter>();
//		List<String> statusList=new ArrayList<String>();
//		statusList=AppUtility.getConfigValue(6);
//		System.out.println("STATUS LIST SIZE:  "+statusList.size());
//		
//		for(int i=0;i<statusList.size();i++){
//			if(statusList.get(i).trim().equalsIgnoreCase(AppConstants.Sucessful)
//					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Successful)
//					||statusList.get(i).trim().equalsIgnoreCase("successfull")
//					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Unsucessful)
//					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CANCELLED)
//					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CANCEL)
//					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.REJECTED)
//					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CLOSED)
//					||statusList.get(i).trim().equalsIgnoreCase("Completed")
//					||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Successful)
//					){
//				statusList.remove(i);
//				i--;
//			}
//		}
//		System.out.println("AFTER STATUS LIST SIZE:  "+statusList.size());
		
		filter = new Filter();
		filter.setQuerryString("compStatus");
		filter.setStringValue("Created");
		filtervec.add(filter);
				
		List<String> branchList ;
		branchList= new ArrayList<String>();
		for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
			branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
		}
	
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("complainDate <=");
		filter.setDateValue(date);
		filtervec.add(filter);
		
		querry=new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Complain());
		this.retriveTable(querry);
		
	}
	
	
	public void retriveTable(MyQuerry querry){
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				globalComplainList=new ArrayList<>();
				
				if(result.size()==0){
					SC.say("No result found.");
				}
				for(SuperModel model:result){
					Complain complainEntity =(Complain) model;
					globalComplainList.add(complainEntity);
				}
				form.setDataToListGrid(globalComplainList);
			}
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		}); 
	 }
	

	@Override
	public void onClick(ClickEvent event) {
		System.out.println("CLICK EVENT..!!!");
		if(event.getSource() instanceof InlineLabel){
			InlineLabel lbl= (InlineLabel) event.getSource();
			if(lbl.getText().equals("Download")){
				reactOnDownload();
			}
			if(lbl.getText().equals("Search")){
				System.out.println("ON CLICK OF SEARCH..!!!");
				searchForm.showPopUp();
			}
			if(lbl.getText().equals("Go")){
				reactOnGo();
			}
			
			if(otherComplaintpopup!=null){
				if(lbl.getText().equals("MarkComplete")){
					System.out.println("lblmarkComplete");
					if(!otherComplaintpopup.ticketNo.getValue().equals("")){
						reactonMarkComplete(otherComplaintpopup.ticketNo.getValue());
					}
					else{
//						showDialogMessage("Ticket Id not found!");
					}
				}
			}
			
			
		}
		
		if (event.getSource().equals(form.getBtnGo())) {
			System.out.println("BTN GO CLICK");
			if(LoginPresenter.branchRestrictionFlag==true&&form.getOlbbranch().getSelectedIndex()==0){
				SC.say("Please select branch.");
				return;
			}
			System.out.println("FRM DT "+form.getDbFromDate().getValue());
			if (form.getOlbbranch().getSelectedIndex() == 0
					&& form.olbassignTo.getSelectedIndex() == 0
					&& form.getDbFromDate().getValue()==null
					&& form.getDbToDate().getValue()==null
					&& form.olbStatus.getSelectedIndex() == 0) {		
				SC.say("Please select atleast one filter.");
				return;
			}
			else{
				searchByFromToDate();
			}
		}
		
		
		
		
	}
	
	private void reactonMarkComplete(String ticketId) {
		GeneralServiceAsync generalasync = GWT.create(GeneralService.class);

		int complaintId = Integer.parseInt(ticketId);
		Company comp = new Company();
		System.out.println("Comp =="+comp.getCompanyId());
		generalasync.markCompleteComplaint(comp.getCompanyId(), complaintId, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(String result) {
				if(result.equals("Success")){
					selectedRecord.setAttribute("status","Completed");
					SC.say("Complaint Completed Successfully");
					form.grid.redraw();
					otherComplaintpopup.hidePopUp();
				}
				else{
					SC.say("Failed! Please try again");

				}
			}
		});
	}
	
	
	private void searchByFromToDate() {
		System.out.println("FROM AND DATE ---");
		Long compId = UserConfiguration.getCompanyId();
		Vector<Filter> filtervec = null;
		Filter filter = null;
		MyQuerry querry;

		filtervec = new Vector<Filter>();

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		
		
		if(form.olbStatus.getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("compStatus");
			filter.setStringValue(form.olbStatus.getValue(form.olbStatus.getSelectedIndex()));
			filtervec.add(filter);
		} 
//		else {
//			filter = new Filter();
//			filter.setQuerryString("compStatus");
//			filter.setStringValue("Created");
//			filtervec.add(filter);
//		}

		if (form.getDbFromDate().getValue() != null) {
			filter = new Filter();
			filter.setQuerryString("complainDate >=");
			filter.setDateValue(form.getDbFromDate().getValue());
			filtervec.add(filter);
		}

		if (form.getDbToDate().getValue() != null) {

			filter = new Filter();
			filter.setQuerryString("complainDate <=");
			filter.setDateValue(form.getDbToDate().getValue());
			filtervec.add(filter);
		}

		if (form.getOlbbranch().getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);

		}

		if (form.olbassignTo.getSelectedIndex() != 0) {
			filter = new Filter();
			filter.setQuerryString("assignto");
			filter.setStringValue(form.olbassignTo.getValue());
			filtervec.add(filter);
		}

		querry = new MyQuerry();
		querry.setQuerryObject(new Complain());
		querry.setFilters(filtervec);
		this.retriveTable(querry);

	}
	
	
	public void reactOnGo() {

		if (!searchForm.personInfo.getId().getValue().equals("")
				&& !searchForm.personInfo.getName().getValue().equals("")
				&& !searchForm.personInfo.getPhone().getValue().equals("")) {
			searchForm.hidePopUp();
			MyQuerry querry = createComplainFilter();
			retriveTable(querry);
			
		}

		if (searchForm.personInfo.getId().getValue().equals("")
				&& searchForm.personInfo.getName().getValue().equals("")
				&& searchForm.personInfo.getPhone().getValue().equals("")) {
			SC.say("Please enter customer information");
		}

	}

	private MyQuerry createComplainFilter() {
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter temp;

		if (searchForm.personInfo.getId().getText().trim().equals("") == false) {
			temp = new Filter();
			temp.setLongValue(Long.parseLong(searchForm.personInfo.getId().getValue()));
			temp.setQuerryString("personinfo.count");
			filterVec.add(temp);
		}

		if ((searchForm.personInfo.getName().getText()).trim().equals("") == false) {
			temp = new Filter();
			temp.setStringValue(searchForm.personInfo.getName().getValue());
			temp.setQuerryString("personinfo.fullName");
			filterVec.add(temp);
		}

		if (searchForm.personInfo.getPhone().getText().trim().equals("") == false) {
			temp = new Filter();
			temp.setLongValue(Long.parseLong(searchForm.personInfo.getPhone().getValue()));
			temp.setQuerryString("personinfo.cellNumber");
			filterVec.add(temp);
		}

		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Complain());
		querry.setFilters(filterVec);

		return querry;
	}

	private void reactOnDownload() {
		ArrayList<Complain> complainList = new ArrayList<Complain>();
		ListGridRecord[] allRecords = form.grid.getRecords();
        if (allRecords != null) {
        	List<ListGridRecord> allRecordsList = Arrays.asList(allRecords);
	   		for(int i=0;i<allRecordsList.size();i++){
	   			 final Complain model=getComplainDetails(Integer.parseInt(allRecordsList.get(i).getAttribute("TicketNo").trim()));
	   			 if(model!=null){
	   				complainList.add(model);
	   			 }
	   		}
        }
		
		csvservice.setComplainlist(complainList, new AsyncCallback<Void>(){
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "CustomerSupportReport")){
					final String url = gwt + "csvservlet" + "?type=" + 126;
					Window.open(url, "test", "enabled");
				}else{
					final String url = gwt + "csvservlet" + "?type=" + 95;
					Window.open(url, "test", "enabled");
				}
			}
		});
		
		 
	}

	private Complain getComplainDetails(int complainId) {
		for(Complain object:globalComplainList){
			if(object.getCount()==complainId){
				return object;
			}
		}
		return null;
	}

	@Override
	public void onCellClick(CellClickEvent event) {
		
		ListGridRecord record = event.getRecord();
		int colNum = event.getColNum();
		ListGridField field = form.grid.getField(colNum);
		String fieldName = form.grid.getFieldName(colNum);
		String fieldTitle = field.getTitle();
		position=event.getRowNum();
		System.out.println("Column No=="+event.getColNum());
		System.out.println("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Lead Id :: "+ record.getAttribute("leadId")+"POSITION "+position);
		complainEntity = getComplainDetails((Integer.parseInt(record.getAttribute("TicketNo").trim())));
		selectedRecord = record;
		
		/*** Date 14-08-2019 by vijay if condition for Service complaints  and else for Other than service complaints ***/
		if(complainEntity.getServicedetails()!=null && complainEntity.getServicedetails().size()!=0){
			CustomerSupportPopup complaintpopup = new CustomerSupportPopup(complainEntity);
			mapToScreen(complainEntity,complaintpopup,selectedRecord);
		}
		else{
			otherComplaintpopup = new CustomerSupportPopup(complainEntity,true);
			otherComplaintpopup.getLblmarkComplete().addClickHandler(this);
			mapToScreen(complainEntity,otherComplaintpopup,selectedRecord);
		}

		
		
	}
	
	private void mapToScreen(final Complain complainEntity, final CustomerSupportPopup complaintpopup, ListGridRecord selectedRecord2) {
		complaintpopup.getLblUpdate().getElement().setId("addbutton");
		complaintpopup.getLblbtnSave().getElement().setId("addbutton");
		complaintpopup.getLblbtnCancel().getElement().setId("addbutton");
		complaintpopup.getHorizontal().add(complaintpopup.getLblUpdate());
		complaintpopup.getHorizontal().add(complaintpopup.getLblbtnSave());
		complaintpopup.getHorizontal().add(complaintpopup.getLblbtnCancel());

		complaintpopup.getLblOk().setVisible(false);
		complaintpopup.getLblCancel().setVisible(false);
		
		complaintpopup.getLblbtnSave().setVisible(false);
		if(complainEntity.getCompStatus().equals("Completed")){
			complaintpopup.getLblUpdate().setVisible(false);
		}
		
		complaintpopup.showPopUp();
		if(selectedRecord2!=null){
			String status = selectedRecord2.getAttribute("status");
			System.out.println("status ============="+status);
			if(status.equals("Completed")){
				complaintpopup.getLblbtnSave().setVisible(false);
				complaintpopup.getLblUpdate().setVisible(false);
				complaintpopup.getLblmarkComplete().setVisible(false);
			}
		}

		complaintpopup.getPopup().getElement().addClassName("customerSupportPopup");
		AppUtility.makeCustomerBranchLive(complainEntity.getPersoninfo(),complaintpopup.getOlbcustomerBranch(),null);
		complaintpopup.setEnable(false);
		Timer timer = new Timer() {
			
			@Override
			public void run() {
				complaintpopup.updateView(complainEntity);
				
			}
		};
		timer.schedule(2000);
		
    }





}
