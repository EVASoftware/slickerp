package com.slicktechnologies.client.views.customersupportdashboard;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.AutoFitWidthApproach;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class CustomerSupportDashboardForm extends ViewContainer{




	ListGrid grid;
	DataSource ds;
	
	ObjectListBox<Branch> olbbranch;
	
	ObjectListBox<Employee> olbassignTo;
	DateBox dbFromDate;
	DateBox dbToDate;
	Button btnGo;
	
	FlexForm form;
	public ListBox olbStatus;
	
	HorizontalPanel horizontalpanel;
	VerticalPanel verticalpanel;
	
	public CustomerSupportDashboardForm() {
		super();
		createGui();
	}
	
	@Override
	protected void createGui() {
		System.out.println("CREATE GUI");
		toggleAppHeaderBarMenu();
		
		olbbranch =new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbranch);
		
		olbassignTo =new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbassignTo);
		
		olbStatus= new ListBox();
		AppUtility.setStatusListBox(olbStatus, Complain.getStatusList());
		olbStatus.getElement().getStyle().setHeight(26, Unit.PX);
		/** Ends **/
		
		olbbranch.getElement().getStyle().setHeight(26, Unit.PX);
		olbassignTo.getElement().getStyle().setHeight(26, Unit.PX);
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		dbFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		dbToDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		btnGo=new Button("GO");
		
		
		horizontalpanel = new HorizontalPanel();
		
		
		VerticalPanel vpanel1=new VerticalPanel();
		VerticalPanel vpanel2=new VerticalPanel();
		VerticalPanel vpanel3=new VerticalPanel();
		VerticalPanel vpanel4=new VerticalPanel();
		VerticalPanel vpanel5=new VerticalPanel();
		VerticalPanel vpanel6=new VerticalPanel();
		
		InlineLabel lablel1=new InlineLabel("Branch");
		InlineLabel lablel2=new InlineLabel("Assigned To");
		InlineLabel lablel3=new InlineLabel("From Date (Ticket Date)");
		InlineLabel lablel4=new InlineLabel("To Date (Ticket Date)");
		InlineLabel lable16=new InlineLabel("Status");
		
		InlineLabel lablel5=new InlineLabel(". ");
		
		vpanel1.add(lablel1);
		vpanel1.add(olbbranch);
		
		vpanel2.add(lablel2);
		vpanel2.add(olbassignTo);
		
		vpanel3.add(lablel3);
		vpanel3.add(dbFromDate);
		
		vpanel4.add(lablel4);
		vpanel4.add(dbToDate);
		vpanel6.add(lable16);
		vpanel6.add(olbStatus);
		
		vpanel5.add(lablel5);
		vpanel5.add(btnGo);
		
		horizontalpanel.add(vpanel1);
		horizontalpanel.add(vpanel2);
		horizontalpanel.add(vpanel3);
		horizontalpanel.add(vpanel4);
		horizontalpanel.add(vpanel6);
		horizontalpanel.add(vpanel5);

		horizontalpanel.setSpacing(10);
		horizontalpanel.getElement().addClassName("technicianscheduleButtons");
		
		
		verticalpanel = new VerticalPanel();
				
		
		grid = new ListGrid();
//		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX); // vijay
		grid.setStyleName("GridList");
		setGridProperty(grid);
		grid.setFields(getGridFields());
		
		ds= new DataSource();
		ds.setClientOnly(true);
		setDataSourceField(ds);
		grid.setDataSource(ds);
		
		verticalpanel.getElement().addClassName("varticalGridPanel");
		grid.getElement().addClassName("technicianSchedule");
		verticalpanel.add(grid);
		
		content.add(horizontalpanel);
		content.add(verticalpanel);
		
	}
	
	public void setDataToListGrid(ArrayList<Complain> list){
		System.out.println("FORM SETTING DATA....");
		
		ds.setTestData(getGridData(list));
		grid.setDataSource(ds);
		grid.setFields(getGridFields());
		grid.fetchData();
		
	}

	
	
	//grid property
	private static void setGridProperty(ListGrid grid) {
	   grid.setWidth("83%");
	   grid.setHeight("70%");
	   grid.setAutoFetchData(true);
	   grid.setAlternateRecordStyles(true);
	   grid.setShowAllRecords(true);
	   grid.setShowFilterEditor(true);
	   grid.setFilterOnKeypress(true);
	   
	   grid.setWrapCells(true);	
	   grid.setFixedRecordHeights(false);
	   grid.setAutoFitData(Autofit.HORIZONTAL);	
	   grid.setAutoFitWidthApproach(AutoFitWidthApproach.BOTH);	
	   grid.setCanResizeFields(true);
	   
	   /**
	    * @author Anil
	    * @since 31-12-2020
	    * for resizing smartgrid
	    */
	   if(AppMemory.getAppMemory().enableMenuBar){
		   grid.setWidth("100%");
	   }
	}
	
	//fields
	private static ListGridField[] getGridFields() {
	
		
	   ListGridField field1 = new ListGridField("TicketNo","Ticket No");
	   field1.setWrap(true);
	   field1.setAlign(Alignment.CENTER);
//	   ListGridField field2 = new ListGridField("followupButton","Follow Up");
//	   field2.setWrap(true);
//	   field2.setAlign(Alignment.CENTER);
//	   field2.setType(ListGridFieldType.IMAGE);
//	   field2.setDefaultValue("followup.png");
//	   field2.setCanEdit(false);
//	   field2.setImageSize(80);
//	   field2.setImageHeight(30);
	   
	   ListGridField field2 = new ListGridField("TicketDate","Ticket Date");
	   field2.setWrap(true);
	   field2.setAlign(Alignment.CENTER);
	   
	   ListGridField field3 = new ListGridField("DueDate","Due Date");
	   field3.setWrap(true);
	   field3.setAlign(Alignment.CENTER);
	   
	   ListGridField field4 = new ListGridField("ServiceDate","Service Date");
	   field4.setWrap(true);
	   field4.setAlign(Alignment.CENTER);
	   
	   ListGridField field5 = new ListGridField("customerId","Customer Id");
	   field5.setWrap(true);
	   field5.setAlign(Alignment.CENTER);
	   
	   ListGridField field6 = new ListGridField("customerName","Customer Name");
	   field6.setWrap(true);
	   field6.setAlign(Alignment.CENTER);
	   
	   ListGridField field7 = new ListGridField("customerCell","Customer Cell");
	   field7.setWrap(true);
	   field7.setAlign(Alignment.CENTER);
	   
	   ListGridField field8 = new ListGridField("AssignedTo","Assigned To");
	   field8.setWrap(true);
	   field8.setAlign(Alignment.CENTER);
	   
	   ListGridField field9 = new ListGridField("Branch","Branch");
	   field9.setWrap(true);
	   field9.setAlign(Alignment.CENTER);
	   
	   ListGridField field10 = new ListGridField("CreationDate","Creation Date"); 
	   field10.setWrap(true);
	   field10.setAlign(Alignment.CENTER);
	   
	   
	   ListGridField field12 = new ListGridField("status","Status");
	   field12.setWrap(true);
	   field12.setAlign(Alignment.CENTER);
	   
	   ListGridField field14 = new ListGridField("ServiceLocation","Service Location");
	   field12.setWrap(true);
	   field12.setAlign(Alignment.CENTER);
	   
	   ListGridField field20 =new ListGridField("Service Id","Service Id");
	   field20.setWrap(true);
	   field20.setAlign(Alignment.CENTER);
	   
	   ListGridField field21 =new ListGridField("Assesment Id","Assesment Id");
	   field21.setWrap(true);
	   field21.setAlign(Alignment.CENTER);
	   
	   return new ListGridField[] { 
			   field1,field20,field21,field2 ,field3,field4,field5,field6,field7,field8,field9,
			   field10,field12,field14,
			   };
	}
	
	//data source field
	protected static void setDataSourceField(DataSource dataSource) {
		DataSourceIntegerField pkField = new DataSourceIntegerField("pk");  
        pkField.setHidden(true);  
        pkField.setPrimaryKey(true);
		
	   DataSourceField field1 = new DataSourceField("TicketNo", FieldType.TEXT);
	   DataSourceDateField field2 = new DataSourceDateField("TicketDate");
	   DataSourceDateField field14 = new DataSourceDateField("DueDate");
	   DataSourceDateField field3 = new DataSourceDateField("ServiceDate");
	   
	   DataSourceField field5 = new DataSourceField("customerId", FieldType.TEXT);
	   DataSourceField field6 = new DataSourceField("customerName", FieldType.TEXT);
	   DataSourceField field7 = new DataSourceField("customerCell", FieldType.TEXT);
	   DataSourceField field8 = new DataSourceField("AssignedTo", FieldType.TEXT);
	   DataSourceField field9 = new DataSourceField("Branch", FieldType.TEXT);
	   DataSourceDateField field13 = new DataSourceDateField("CreationDate");
	   DataSourceField field12 = new DataSourceField("status", FieldType.TEXT);
	   DataSourceField field15 = new DataSourceField("ServiceLocation", FieldType.TEXT);
	   DataSourceField field20 =new DataSourceField("Service Id",FieldType.TEXT);
	   DataSourceField field21 = new DataSourceField("Assesment Id",FieldType.TEXT);
	  
	   
	   dataSource.setFields(pkField,field1,field20,field21, field2 ,field14,field3,field5,field6,field7,field8,field9,field13,field12,field15);
	}
	
	
	 private static ListGridRecord[] getGridData(ArrayList<Complain> complaintList) {
			DateTimeFormat dateformat = DateTimeFormat.getFormat("dd/MM/yyyy");

		 ListGridRecord[] records = null; 
		 records = new ListGridRecord[complaintList.size()];
		 for(int i=0;i<complaintList.size();i++){
			 ListGridRecord record = new ListGridRecord();
			 Complain complaint=complaintList.get(i);
			 AssesmentReport assrep=new AssesmentReport();
			 record.setAttribute("TicketNo", complaint.getCount());
			 
			 
			 if(complaint.getServiceId()!=null){
				 record.setAttribute("Service Id", complaint.getServiceId());
			 }
			 else{
				 record.setAttribute("Service Id", "");
			 }
			 if(complaint.getAssesmentId()!=null){
				 record.setAttribute("Assesment Id", complaint.getAssesmentId());
			 }
			 else{
				 record.setAttribute("Assesment Id", "");
			 }
			 
			 if(complaint.getComplainDate()!=null){
				 record.setAttribute("TicketDate", dateformat.format(complaint.getComplainDate()));
			 }
			 else{
				 record.setAttribute("TicketDate", dateformat.format(complaint.getComplainDate()));
			 }
			
			 if(complaint.getDueDate()!=null){
				 record.setAttribute("DueDate", dateformat.format(complaint.getDueDate()));
			 }else{
				 record.setAttribute("DueDate", "");
			 }
			 if(complaint.getNewServiceDate()!=null){
				 record.setAttribute("ServiceDate", dateformat.format(complaint.getNewServiceDate()));
			 }else{
				 record.setAttribute("ServiceDate", "");
			 }
			 record.setAttribute("customerId", complaint.getPersoninfo().getCount());
			 record.setAttribute("customerName",complaint.getPersoninfo().getFullName());
			 record.setAttribute("customerCell", complaint.getPersoninfo().getCellNumber());
			 record.setAttribute("AssignedTo", complaint.getAssignto());
			 record.setAttribute("Branch", complaint.getBranch());
			 record.setAttribute("CreationDate", dateformat.format(complaint.getDate()));
			 record.setAttribute("status", complaint.getCompStatus());
			 record.setAttribute("ServiceLocation", complaint.getCustomerBranch());
			 records[i]=record;
		 }
		 System.out.println("RECORDS LENGTH : "+records.length);
		 
		 return records;
	}
	 
	 /**
		 * Toggles the app header bar menus as per screen state
		 */
		public void toggleAppHeaderBarMenu() {
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download")||text.equals("Search"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		  		
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download"))
						menus[k].setVisible(true); 
					else
						menus[k].setVisible(false);  		   
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download"))
						menus[k].setVisible(true); 
					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.CUSTOMERSUPPORTDASHBOARD,LoginPresenter.currentModule.trim());
		}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	public void showMessage(String msg){
		Window alert=new Window();
		
		alert.setWidth("25%");
        alert.setHeight("20%"); 
        alert.setTitle("Alert");
        alert.setShowMinimizeButton(false);
        alert.setIsModal(true);
        alert.setShowModalMask(true);
        alert.centerInPage();
        alert.setMargin(4);
        
		Label label = new Label();
		label.setContents(msg);
		label.setAlign(Alignment.CENTER);
		label.setPadding(5);
		label.setHeight100();
		
		alert.addItem(label);
		
		
		alert.show();
	}
	
	
	

	public ListGrid getGrid() {
		return grid;
	}

	public void setGrid(ListGrid grid) {
		this.grid = grid;
	}

	public DataSource getDs() {
		return ds;
	}

	public void setDs(DataSource ds) {
		this.ds = ds;
	}

	public ObjectListBox<Branch> getOlbbranch() {
		return olbbranch;
	}

	public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
		this.olbbranch = olbbranch;
	}


	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}

	public Button getBtnGo() {
		return btnGo;
	}

	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}

	public FlexForm getForm() {
		return form;
	}

	public void setForm(FlexForm form) {
		this.form = form;
	}

	public HorizontalPanel getHorizontalpanel() {
		return horizontalpanel;
	}

	public void setHorizontalpanel(HorizontalPanel horizontalpanel) {
		this.horizontalpanel = horizontalpanel;
	}

	public VerticalPanel getVerticalpanel() {
		return verticalpanel;
	}

	public void setVerticalpanel(VerticalPanel verticalpanel) {
		this.verticalpanel = verticalpanel;
	}
	
	
	
	
	


}
