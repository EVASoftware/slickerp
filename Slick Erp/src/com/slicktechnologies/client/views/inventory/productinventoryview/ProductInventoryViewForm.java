package com.slicktechnologies.client.views.inventory.productinventoryview;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.poi.ss.formula.ptg.TblPtg;

import com.gargoylesoftware.htmlunit.javascript.host.Text;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.shippingpackingprocess.InspectionMethod;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;

public class ProductInventoryViewForm extends FormScreen<ProductInventoryView>implements ClickHandler,ChangeHandler{
	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	
	ProductInfoComposite productInfoComposite;
	
	ObjectListBox<WareHouse> oblwarehouse;
	ListBox lsStorageLoc;
	ListBox lsStoragebin;
	ListBox lbinspectionReqList;
	ObjectListBox<InspectionMethod> olbInspectionMethod;
	FormField fgroupingProductInformation;
	Button addBtn;
	InvetoryViewDetailsTable table;
	ArrayList<String> storageList=new ArrayList<String>();
	ProductInventoryView productinentoryviewobj;
	
	/*
	 * Date:20-12-2018
	 * @author Ashwini
	 * Des:To add descripation coloum
	 */
	
	TextArea  taComment;

	/*
	 * end by Ashwini
	 */
	
	/**  this flag is used for capturing transactions when available qty gets change  
	 * This is done by Anil on 7/9/2016 **/
	boolean editFlag=false;
	
	
	/**
	 * This list contains wareHouseList. Which is used for branch level
	 * restrictions and created by Rahul Verma on 15-09-2016
	 */
	ArrayList<WareHouse> wareHouseList;
	
	public ProductInventoryViewForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		olbInspectionMethod.setEnabled(false);
		editFlag=false;
	}

	public ProductInventoryViewForm() {
		super();
		createGui();
		table.connectToLocal();
		olbInspectionMethod.setEnabled(false);
		editFlag=false;
	}

	private void initalizeWidget() {
		productInfoComposite = AppUtility.initiateSalesProductComposite(new SuperProduct());
		
		lbinspectionReqList=new ListBox();
		lbinspectionReqList.addItem(AppConstants.NO);
		lbinspectionReqList.addItem(AppConstants.YES);
		lbinspectionReqList.addChangeHandler(this);
		
		oblwarehouse = new ObjectListBox<WareHouse>();
		oblwarehouse.addChangeHandler(this);
		addBtn = new Button("Add");
		addBtn.addClickHandler(this);
		table = new InvetoryViewDetailsTable();
		
		olbInspectionMethod=new ObjectListBox<InspectionMethod>();
		InspectionMethod.initializeInspectionName(olbInspectionMethod);
		
		lsStorageLoc=new ListBox();
		lsStorageLoc.addChangeHandler(this);
		lsStorageLoc.addItem("--SELECT--");
		lsStoragebin=new ListBox();
		lsStoragebin.addItem("--SELECT--");
		
		/*
		 * Date:20-12-2018
		 * @author Ashwini
		 * Des:To add product descripation
		 */
		
		taComment = new TextArea();
		
		
		AppUtility.initializeWarehouse(oblwarehouse);
	}
	

	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames = new String[] { "New" };
		// ////////////////////////// Form Field
		// Initialization////////////////////////////////////////////////////
		
		String mainScreenLabel="Product Inventory View";
		//fgroupingCompanyInformation.setLabel(mainScreenLabel);
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingProductInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();		


//		FormFieldBuilder fbuilder;
//		fbuilder = new FormFieldBuilder();
//		FormField Product Information = fbuilder.setlabel("Product Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingInventoryInformation = fbuilder.setlabel("Inventroy Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Warehouse", oblwarehouse);
		FormField fwarehouse = fbuilder.setMandatory(false).setMandatoryMsg("WareHouse Name Is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Storage Location", lsStorageLoc);
		FormField flocation = fbuilder.setMandatory(false).setMandatoryMsg("Storage Location Is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Storage Bin", lsStoragebin);
		FormField fbin = fbuilder.setMandatory(false).setMandatoryMsg("Storage Bin Is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", addBtn);
		FormField fadd = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", table.getTable());
		FormField ftable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Inspection Required", lbinspectionReqList);
		FormField flbInsreq = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Inspection Method", olbInspectionMethod);
		FormField foblInsme = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		/* 
		 * Date:20-12-2018
		 * @author Ashwini
		 */
		
		fbuilder = new FormFieldBuilder();
		FormField  fbgroupComment =  fbuilder.setlabel("Description").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Description (Max 500 characters) ",taComment);
		FormField fbComment1 = fbuilder.setMandatory(false).setColSpan(4).build();
		
		/*
		 * end by Ashwini
		 */
		// //////////////////////////////Lay Out Making
		// Code///////////////////////////////////////////////////////////////

		FormField[][] formfield = { 
//				{ fgroupingProductInformation }, 
//				{ fproductInfoComposite },
//				{flbInsreq,foblInsme},
//				{ fwarehouse, flocation, fbin, fadd },
//				{fgroupingInventoryInformation},
//				{ ftable },
//				{fbgroupComment},  //Addeded by Ashwini
//				{fbComment1}};  //Added by Ashwini
				/**-
				 * Added by Priyanka -08-02-2021
				 * Des-Simplification of Product Inventory View Req. raise by Nitin Sir.
				 */
				{ fgroupingProductInformation }, 
				{ fproductInfoComposite, },
				{fwarehouse,flocation,fbin},
				{ flbInsreq,foblInsme,fadd},
				{fgroupingInventoryInformation},
				{ ftable },
				{fbgroupComment},  //Addeded by Ashwini
				{fbComment1}};  //Added by Ashwini
		
				/**
				 * End
				 */

		this.fields = formfield;
	}

	/**************************** method template to update the model with token entity name **********************************/
	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(ProductInventoryView model) {
		model.setCount(Integer.parseInt(productInfoComposite.getProdID().getValue()));
		if(lbinspectionReqList.getSelectedIndex()!=0){
			model.setInspectionRequired(lbinspectionReqList.getValue(lbinspectionReqList.getSelectedIndex()));
		}
		if(olbInspectionMethod.getSelectedIndex()!=0){
			model.setInspectionMethod(olbInspectionMethod.getValue());
		}
		if(productInfoComposite.getProdID()!=null)
			model.setProdID(Integer.parseInt(productInfoComposite.getProdID().getValue()));
		if(productInfoComposite.getProdCode()!=null)
			model.setProductCode(productInfoComposite.getProdCode().getValue());
		if(productInfoComposite.getProdCategory()!=null)
			model.setProductCategory(productInfoComposite.getProdCategory().getValue());
		if(productInfoComposite.getProdName()!=null)
			model.setProductName(productInfoComposite.getProdName().getValue());
		if(productInfoComposite.getProdPrice().getValue()!=null)
			model.setProductPrice(productInfoComposite.getProdPrice().getValue());
		if(productInfoComposite.getUnitOfmeasuerment()!=null)
			model.setProductUOM(productInfoComposite.getUnitOfmeasuerment().getValue());
		if (table.getValue() != null){
				model.setDetails(table.getValue());
		}
		
		/*
		 * Date:20-12-2018
		 * @author Ashwini
		 * Des:To update value of descripation 
		 */
		
		if(taComment.getValue() != null){
			 model.setComment(taComment.getValue());
		}
		
		/*
		 * end by Ashwini
		 */
		
		System.out.println("UPDATE MODEL EDIT FLAG :: "+editFlag);
		if(editFlag==true){
			model.setEditFlag(editFlag);
		}
		productinentoryviewobj=model;
		presenter.setModel(model);
		
		editFlag = true;
	}

	/************************ method template to update the view with token entity name ***************************************/
	
	@SuppressWarnings("unchecked")
	@Override
	public void updateView(ProductInventoryView view) {
		
		productinentoryviewobj=view;
		if (view.getProdID() != 0){
			productInfoComposite.getProdID().setValue(String.valueOf(view.getProdID()));
		}
		if (view.getProductCode() != null){
			productInfoComposite.getProdCode().setValue(view.getProductCode());
		}
		if (view.getProductName() != null){
			productInfoComposite.getProdName().setValue(view.getProductName());
		}
		
		if (view.getProductPrice() != 0){
			productInfoComposite.getProdPrice().setValue(view.getProductPrice());
		}
		if (view.getProductCategory() != null){
			productInfoComposite.getProdCategory().setValue(view.getProductCategory());
		}
		if (view.getProductUOM() != null){
			productInfoComposite.getUnitOfmeasuerment().setValue(view.getProductUOM());
		}
		if (view.getDetails() != null){
			table.setValue(view.getDetails());
		}
		
		/*
		 * Date:20-12-2018
		 * @author Ashwini
		 */
		
		if(view.getComment() !=null){
			taComment.setValue(view.getComment());
		}
		
		/*
		 * End by Ashwini
		 */
		if(view.getInspectionRequired()!=null){
			for(int i=0;i<lbinspectionReqList.getItemCount();i++){
				String data=lbinspectionReqList.getItemText(i);
				if(data.equals(view.getInspectionRequired()))
				{
					lbinspectionReqList.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getInspectionMethod()!=null){
			olbInspectionMethod.setValue(view.getInspectionMethod());
		}
		editFlag=true;
		System.out.println("UPDATE VIEW EDIT FLAG :: "+editFlag);
		presenter.setModel(view);
	}

	/************************** Toggles the app header bar menus as per screen state ****************************************/

	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   
				}
			}
	
			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
						menus[k].setVisible(true); 
	
					else
						menus[k].setVisible(false);  		   
				}
			}
	
			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
						menus[k].setVisible(true); 
	
					else
						menus[k].setVisible(false);  		   
				}
			}
			
			AuthorizationHelper.setAsPerAuthorization(Screen.PRODUCTINVENTORYVIEW,LoginPresenter.currentModule.trim());
	}
	
//	public void toggleAppHeaderBarMenu() {
//		
//		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.equals("Save")||text.equals("Discard")||text.equals("Search"))
//				{
//					menus[k].setVisible(true); 
//				}
//				else
//					menus[k].setVisible(false);  		   
//			}
//		}
//
//		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.equals("Save")||text.equals("Discard"))
//					menus[k].setVisible(true); 
//
//				else
//					menus[k].setVisible(false);  		   
//			}
//		}
//
//		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search"))
//					menus[k].setVisible(true); 
//
//				else
//					menus[k].setVisible(false);  		   
//			}
//		}
//		
//		AuthorizationHelper.setAsPerAuthorization(Screen.PRODUCTINVENTORYVIEW);
//	}
	
	
	/********************************************* OnClick Method *******************************************************/
	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource().equals(addBtn)) {
			if(!productInfoComposite.getProdID().getValue().isEmpty()&&!productInfoComposite.getProdName().getValue().isEmpty()){
				if(oblwarehouse.getSelectedIndex()!=0&&lsStorageLoc.getSelectedIndex()!=0&&lsStoragebin.getSelectedIndex()!=0){
	//********************validate product ie we can not add 2 diff product at a time 				
					if(validateDifferentProduct()==true){
					if(validateProduct()==true){
						ProductInventoryViewDetails details = new ProductInventoryViewDetails();
						details.setProdid(Integer.parseInt(productInfoComposite.getProdID().getValue()));
						details.setProdname(productInfoComposite.getProdName().getValue());
						details.setWarehousename(oblwarehouse.getValue());
						details.setStoragelocation(lsStorageLoc.getValue(lsStorageLoc.getSelectedIndex()));
						details.setStoragebin(lsStoragebin.getValue(lsStoragebin.getSelectedIndex()));
						if(productInfoComposite.getUnitOfmeasuerment().getText()!=null) //Ashwini Patil Date:23-01-2023
							details.setProdUOM(productInfoComposite.getUnitOfmeasuerment().getText());
						/**
						 * Date 29-01-2019 by Vijay NBHC Inventory Management 
						 * Des :- added Warehouse type for differentiate cluster and warehouse
						 */
						WareHouse warehouse = oblwarehouse.getSelectedItem();
						if(warehouse!=null && warehouse.getWarehouseType()!=null){
							details.setWarehouseType(warehouse.getWarehouseType());
						}
						/**
						 * ends here
						 */
						System.out.println("ProdCode"+productInfoComposite.getProdCode().getValue());
						System.out.println("Prod"+productInfoComposite.getProdCodeValue());
						if(productInfoComposite.getProdCode().getValue()!=null){
							details.setProdcode(productInfoComposite.getProdCode().getValue());
						}
						table.getDataprovider().getList().add(details);
						oblwarehouse.setSelectedIndex(0);
						lsStorageLoc.setSelectedIndex(0);
						lsStoragebin.setSelectedIndex(0);
					}
					else{
						this.showDialogMessage("Can not add Duplicate product");
					}
					
					}
				else{
					this.showDialogMessage("You can not add different product");
				}
				}
				else{
					showDialogMessage("Select ListBox Items");
				}
			}
			else{
				showDialogMessage("Enter Product Id");
			}
			
		}
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}
	
	/*********************************************Clear Method****************************************************/
	@Override
	public void clear() {
		super.clear();
		table.connectToLocal();
	}
	/***************************************************Validation Method ****************************************/
	public boolean validate() {
		boolean b = super.validate();
		boolean descBox1Limit=true;
		if (b == false)
			return false;
		if (table.getDataprovider().getList().size() == 0) {
			this.showDialogMessage("Please add atleast one Product");
			return false;
		}
		
		/*
		 * Date:21-12-2018
		 * @author Ashwini
		 * Dea:TO make validation for descripation coloumn.ie character in that should not greater than 500
		 */
		 int commentBoxLen = taComment.getValue().length();
		 System.out.println("common box length::"+commentBoxLen);
		if(commentBoxLen>=500){
			this.showDialogMessage("Description 1 text exceeds 500 characters");
			descBox1Limit = false;
		}
		return descBox1Limit && true;
//		return true; //commented by Ashwini
	}
	
	
	public boolean validateProduct() {
		String name = oblwarehouse.getValue();
		String storLoc=lsStorageLoc.getValue(lsStorageLoc.getSelectedIndex());
		String storBin=lsStoragebin.getValue(lsStoragebin.getSelectedIndex());
		List<ProductInventoryViewDetails> list = table.getDataprovider().getList();
		for (ProductInventoryViewDetails temp : list) {
			if (name.equals(temp.getWarehousename())&&storLoc.equals(temp.getStoragelocation())&&storBin.equals(temp.getStoragebin())) {
				return false;
			}
		}
		return true;
	}
	
	
	public boolean validateDifferentProduct(){
		
		int prodId=Integer.parseInt(productInfoComposite.getProdID().getValue().trim());
		System.out.println("prod id"+prodId);
		String prodName=productInfoComposite.getProdName().getValue().trim();
		System.out.println("prod name"+prodName);
		List<ProductInventoryViewDetails> prodList= table.getDataprovider().getList();
		System.out.println("prod list size"+prodList.size());
		
		
		if(prodList.size()==0){
			return true;
		}
		else{
		for (int i=0;i<prodList.size();i++) {
			System.out.println("prod id from list"+prodList.get(i).getProdid());
			System.out.println("prod code from list"+prodList.get(i).getProdname());
			if (prodId==prodList.get(i).getProdid()&&prodName.equals(prodList.get(i).getProdname())){
				System.out.println("in side condition");
				return true;
			}
					
		}
		return false;
		}
		
	}
	
	
	/*************************************************Set Enable Method *********************************************/
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		table.setEnable(state);
		olbInspectionMethod.setEnabled(false);
	}

	/******************************************** Initializing ObjectList Elements *************************************/

	public void initializeWarehounse(final ObjectListBox<WareHouse> oblwarehouse2) {
		
		GenricServiceAsync async = GWT.create(GenricService.class);
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("Branch");
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {

			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();
				if (result.size() == 0) {
					MyQuerry querry = new MyQuerry(new Vector<Filter>(), new WareHouse());
					oblwarehouse2.MakeLive(querry);
				} else {
					for (SuperModel model : result) {
						ProcessConfiguration processConfig = (ProcessConfiguration) model;
						processList.addAll(processConfig.getProcessList());
					}

					for (int i = 0; i < processList.size(); i++) {
						if (processList.get(i).getProcessType().trim().equalsIgnoreCase("BranchLevelRestriction")
								&& processList.get(i).isStatus() == true) {
							
							System.out.println("user role "+LoginPresenter.myUserEntity.getRole().getRoleName().trim());
							if (LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")) {
								MyQuerry myquerry = new MyQuerry(new Vector<Filter>(),new WareHouse());
								oblwarehouse2.MakeLive(myquerry);
							
							} else {
								
								wareHouseList = new ArrayList<WareHouse>();
								ArrayList<EmployeeBranch> branchesOfEmployee = new ArrayList<EmployeeBranch>();
								// String fullname =
								// LoginPresenter.loggedInUser;
								for (Employee emp : LoginPresenter.globalEmployee) {
									if (emp.getCount() == UserConfiguration.getInfo().getEmpCount()) {
										if (emp.getEmpBranchList() != null&& emp.getEmpBranchList().size() != 0) {
											branchesOfEmployee.addAll(emp.getEmpBranchList());
											break;
										} else {
											EmployeeBranch emplBranch = new EmployeeBranch();
											emplBranch.setBranchName(emp.getBranchName());
											branchesOfEmployee.add(emplBranch);
											break;
										}
									}
								}
								System.out.println("branchesOfEmployee:::::::::::"+ branchesOfEmployee.size());
								ArrayList<String> branchNameList = new ArrayList<String>();
								branchNameList = getBranchNameList(branchesOfEmployee);
								if (getBranchWiseWareHouse(branchNameList)) {
									Timer timer = new Timer() {
										@Override
										public void run() {
											System.out.println("Warehouse List"+ wareHouseList.size());
											oblwarehouse2.setListItems(wareHouseList);
										}
									};
									timer.schedule(3000);
								}
							}
						}else{
							MyQuerry myquerry = new MyQuerry(new Vector<Filter>(),new WareHouse());
							oblwarehouse2.MakeLive(myquerry);
						}
					}
				}
			}
		});
	}

	

	/**
	 * 
	 * Description :@param branches assigned to Employee
	 * 
	 * @return branches Name Date: 14-Sept-2016 Project: EVA Erp Required for :
	 *         Everyone.(This has been developed for branch level restriction)
	 *         Created by: Rahul Verma.
	 */
	public ArrayList<String> getBranchNameList(ArrayList<EmployeeBranch> branchesOfEmployee) {
		ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < branchesOfEmployee.size(); i++) {
			System.out.println("Branch Name::::::"+ branchesOfEmployee.get(i).getBranchName());
			list.add(branchesOfEmployee.get(i).getBranchName().trim());
		}
		return list;
	}

	/**
	 * END
	 */

	/**
	 * 
	 * Description : @param branches of Employee who is Logged In.
	 *
	 * @return This will return boolean value(if executed without exception)
	 *         Creation Date: 14-Sept-2016 Project : EVA ERP Required for:
	 *         Everyone.(This is created for branch level restriction. Created
	 *         by : Rahul Verma
	 */
	public boolean getBranchWiseWareHouse(ArrayList<String> branchNameList) {
		// TODO Auto-generated method stub

		GenricServiceAsync async = GWT.create(GenricService.class);

		// for (int i = 0; i < branchesOfEmployee.size(); i++) {
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("branchdetails.branchName IN");
		filter.setList(branchNameList);
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new WareHouse());
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {

			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("Result Size:::::::::::::"+ result.size());
				if (result.size() != 0) {
					for (SuperModel model : result) {
						System.out.println("Inside On Success loop");
						WareHouse warehouseEntity = (WareHouse) model;
						wareHouseList.add(warehouseEntity);
						System.out.println("warehouselist after adding::::"+ wareHouseList.size());
					}
				}
			}
		});
		return true;
	}

	/**
	 * END
	 */
	
	
	
	
//	public void initializeLocation() {
//		MyQuerry querry = new MyQuerry(new Vector<Filter>(),new StorageLocation());
//		lss.MakeLive(querry);
//	}
//
//	public void initializeBin() {
//		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Storagebin());
//		oblbin.MakeLive(querry);
//	}

	/******************************************* Checking Product Data From DataBase **********************************/
	public void retriveProduct() {
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = new Filter();
		
		filter.setQuerryString("details.prodid");
		filter.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
		filtervec.add(filter);
		
//		filter.setQuerryString("count");
//		filter.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
//		filtervec.add(filter);
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProductInventoryView());

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							showDialogMessage("Product Already Exist!");
							productInfoComposite.clear();
						}
					}
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("No Product Found");
					}
				});
	}

	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(oblwarehouse)){
			System.out.println("Inside Change Event.......");
				retriveStorageLocation();
		}
		if(event.getSource().equals(lsStorageLoc)){
				System.out.println("Inside Change Event.......");
				retriveStorageBin();
		}
		
		if(event.getSource().equals(lbinspectionReqList))
		{
			if(lbinspectionReqList.getSelectedIndex()!=0){
				olbInspectionMethod.setEnabled(true);
			}
			if(lbinspectionReqList.getSelectedIndex()==0){
				olbInspectionMethod.setEnabled(false);
				olbInspectionMethod.setSelectedIndex(0);
			}
		}		
	}
	
	// ******************************* Retrieving Storage location from ****************************************
	
	private void retriveStorageLocation(){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("warehouseName");
		filter.setStringValue(oblwarehouse.getValue());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new StorageLocation());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				lsStorageLoc.clear();
				lsStoragebin.clear();
				lsStorageLoc.addItem("--SELECT--");
				lsStoragebin.addItem("--SELECT--");
				for (SuperModel model : result) {
					StorageLocation entity = (StorageLocation) model;
					storageList.add(entity.getBusinessUnitName());
					lsStorageLoc.addItem(entity.getBusinessUnitName());
					System.out.println("Location :"+entity.getBusinessUnitName());
				}
				
			}
		});
	}
	
	// ****************************** Retrieving Storage Bin From Storage location **********************************
	
	private void retriveStorageBin(){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("storagelocation");
		filter.setStringValue(lsStorageLoc.getValue(lsStorageLoc.getSelectedIndex()));
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				lsStoragebin.clear();
				lsStoragebin.addItem("--SELECT--");
				for (SuperModel model : result) {
					Storagebin entity = (Storagebin) model;
					lsStoragebin.addItem(entity.getBinName());
					System.out.println("BIN :"+entity.getBinName());
				}
			}
		});
	}

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		table.getTable().redraw();
	}

	
	
	
}
