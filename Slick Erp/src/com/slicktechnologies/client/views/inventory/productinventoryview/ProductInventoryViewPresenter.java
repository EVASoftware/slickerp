package com.slicktechnologies.client.views.inventory.productinventoryview;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.customer.CustomerForm;
import com.slicktechnologies.client.views.customer.CustomerPresenterSearchProxy;
import com.slicktechnologies.client.views.customer.CustomerPresenter.CustomerPresenterSearch;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ProductInventoryViewPresenter extends FormScreenPresenter<ProductInventoryView> implements SelectionHandler<Suggestion> {
	ProductInventoryViewForm form;

//	public ProductInventoryViewPresenter(FormTableScreen<ProductInventoryView> view,ProductInventoryView model) {
//		super(view, model);
////		form = (ProductInventoryViewForm) view;
////		form.getSupertable().connectToLocal();
////		form.retriveTable(getLocationQuery());
//		form.productInfoComposite.getProdID().addSelectionHandler(this);
//		form.setPresenter(this);
//	}
	
	public ProductInventoryViewPresenter  (FormScreen<ProductInventoryView> view, ProductInventoryView model) {
		super(view, model);
		form=(ProductInventoryViewForm) view;
		form.productInfoComposite.getProdID().addSelectionHandler(this);
		form.productInfoComposite.getProdName().addSelectionHandler(this);
		form.productInfoComposite.getProdCode().addSelectionHandler(this);
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PRODUCTINVENTORYVIEW,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	public MyQuerry getLocationQuery() {
		MyQuerry quer = new MyQuerry(new Vector<Filter>(),new ProductInventoryView());
		return quer;
	}

	public void setModel(ProductInventoryView entity) {
		model = entity;
	}

	public static ProductInventoryViewForm initalize() {
		ProductInventoryViewTable gentableScreen = new ProductInventoryViewTable();
		ProductInventoryViewForm form = new ProductInventoryViewForm();
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		
		ProductInventoryViewSearchProxy.staticSuperTable=gentableScreen;
		ProductInventoryViewSearchProxy searchpopup=new ProductInventoryViewSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		ProductInventoryViewPresenter presenter = new ProductInventoryViewPresenter(form, new ProductInventoryView());
		AppMemory.getAppMemory().stickPnel(form);
		return form;

	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		if (text.equals(AppConstants.NEW))
			reactToNew();
	}

	private void reactToNew() {
		form.setToNewState();
		initalize();
	}

	@Override
	public void reactOnPrint() {

	}

	@Override
	public void reactOnEmail() {

	}
	
	@Override
	public void reactOnDownload() {
		CsvServiceAsync async = GWT.create(CsvService.class);
		ArrayList<ProductInventoryView> PRArray = new ArrayList<ProductInventoryView>();
		List<ProductInventoryView> list = form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		PRArray.addAll(list);
		async.setProductInventoryView(PRArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 72;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	@Override
	protected void makeNewModel() {
		model = new ProductInventoryView();
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if (event.getSource().equals(form.productInfoComposite.getProdID())||event.getSource().equals(form.productInfoComposite.getProdName())||event.getSource().equals(form.productInfoComposite.getProdCode())) {
			form.retriveProduct();
		}
	}

}
