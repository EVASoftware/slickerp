package com.slicktechnologies.client.views.inventory.productinventoryview;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;

public class ProductInventoryViewTable extends SuperTable<ProductInventoryView>{

	TextColumn<ProductInventoryView> prodIdColumn;
	TextColumn<ProductInventoryView> prodCodeColumn;
	TextColumn<ProductInventoryView> prodNameColumn;
	TextColumn<ProductInventoryView> prodPriceColumn;
	TextColumn<ProductInventoryView> prodCategoryColumn;
	TextColumn<ProductInventoryView> prodUOMColumn;
	TextColumn<ProductInventoryView> warehouseNameColumn;
	TextColumn<ProductInventoryView> storageLocationColumn;
	TextColumn<ProductInventoryView> storageBinColumn;
	
	public ProductInventoryViewTable() {
		super();
		setHeight("800px");
	}
	@Override
	public void createTable() {
		
		createColumnProdId();
		createColumnProdCode();
		createColumnProdName();
		createColumnProdCategory();
		/**
		 * @author Anil , Date : 27-03-2020
		 * As discussed with Nitin sir and Neha , no need to show price column here
		 */
//		createColumnProdPrice();
		createColumnProdUOM();
	}
	
	public void addColumnSorting() {
		createColumnSortingProdId();
		createColumnSortingProdCode();
		createColumnSortingProdName();
		createColumnSortingProdCategory();
		/**
		 * @author Anil , Date : 27-03-2020
		 * As discussed with Nitin sir and Neha , no need to show price column here
		 */
//		createColumnSortingProdPrice();
		createColumnSortingProdUOM();
		
	}
	

	private void createColumnSortingProdId() {
		List<ProductInventoryView> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryView>(list);
		columnSort.setComparator(prodIdColumn,new Comparator<ProductInventoryView>() {
			@Override
			public int compare(ProductInventoryView e1,ProductInventoryView e2) {
				if (e1 != null && e2 != null) {
					if (e1.getProdID() == e2.getProdID()) {
						return 0;
					}
					if (e1.getProdID() > e2.getProdID()) {
						return 1;
					} else {
						return -1;
					}
					} else {
						return 0;
					}
				}
			});
		table.addColumnSortHandler(columnSort);
	}
	private void createColumnSortingProdCode() {
		List<ProductInventoryView> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryView>(list);
		columnSort.setComparator(prodCodeColumn,new Comparator<ProductInventoryView>() {
			@Override
			public int compare(ProductInventoryView e1,ProductInventoryView e2) {
				if (e1 != null && e2 != null) {
					if (e1.getProductCode() != null && e2.getProductCode() != null) {
						return e1.getProductCode().compareTo(e2.getProductCode());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void createColumnSortingProdName() {
		List<ProductInventoryView> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryView>(list);
		columnSort.setComparator(prodNameColumn,new Comparator<ProductInventoryView>() {
			@Override
			public int compare(ProductInventoryView e1,ProductInventoryView e2) {
				if (e1 != null && e2 != null) {
					if (e1.getProductName() != null && e2.getProductName() != null) {
						return e1.getProductName().compareTo(e2.getProductName());
					}
				} 
				else 
				{
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	private void createColumnSortingProdCategory() {
		List<ProductInventoryView> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryView>(list);
		columnSort.setComparator(prodCategoryColumn,new Comparator<ProductInventoryView>() {
			@Override
			public int compare(ProductInventoryView e1,ProductInventoryView e2) {
				if (e1 != null && e2 != null) {
					if (e1.getProductCategory() != null && e2.getProductCategory() != null) {
						return e1.getProductCategory().compareTo(e2.getProductCategory());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void createColumnSortingProdPrice() {
		List<ProductInventoryView> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryView>(list);
		columnSort.setComparator(prodPriceColumn,new Comparator<ProductInventoryView>() {
					@Override
					public int compare(ProductInventoryView e1,ProductInventoryView e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProdID() == e2.getProdID()) {
								return 0;
							}
							if (e1.getProdID() > e2.getProdID()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	private void createColumnSortingProdUOM() {
		List<ProductInventoryView> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryView>(list);
		columnSort.setComparator(prodUOMColumn,new Comparator<ProductInventoryView>() {
			@Override
			public int compare(ProductInventoryView e1,ProductInventoryView e2) {
				if (e1 != null && e2 != null) {
					if (e1.getProductUOM() != null && e2.getProductUOM() != null) {
						return e1.getProductUOM().compareTo(e2.getProductUOM());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	// ****************************************** Table View ***********************************************
	
	public void createColumnProdId(){
		prodIdColumn=new TextColumn<ProductInventoryView>() {
			@Override
			public String getValue(ProductInventoryView object) {
				return object.getProdID()+"";
			}
		};
		table.addColumn(prodIdColumn,"ID");
		prodIdColumn.setSortable(true);
	}
	
	public void createColumnProdName()
	{
		prodNameColumn=new TextColumn<ProductInventoryView>() {
			@Override
			public String getValue(ProductInventoryView object) {
				return object.getProductName();
			}
		};
		table.addColumn(prodNameColumn,"Name");
		prodNameColumn.setSortable(true);
	}
	
	
	private void createColumnProdCode() {
		prodCodeColumn=new TextColumn<ProductInventoryView>() {
			@Override
			public String getValue(ProductInventoryView object) {
				return object.getProductCode();
			}
		};
		table.addColumn(prodCodeColumn,"Code");
		prodCodeColumn.setSortable(true);
	}


	private void createColumnProdCategory() {
		prodCategoryColumn=new TextColumn<ProductInventoryView>() {
			@Override
			public String getValue(ProductInventoryView object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(prodCategoryColumn,"Category");
		prodCategoryColumn.setSortable(true);
	}


	private void createColumnProdPrice() {
		prodPriceColumn=new TextColumn<ProductInventoryView>() {
			@Override
			public String getValue(ProductInventoryView object) {
				return object.getProductPrice()+"";
			}
		};
		table.addColumn(prodPriceColumn,"Price");
		prodPriceColumn.setSortable(true);
	}


	private void createColumnProdUOM() {
		prodUOMColumn=new TextColumn<ProductInventoryView>() {
			@Override
			public String getValue(ProductInventoryView object) {
				return object.getProductUOM();
			}
		};
		table.addColumn(prodUOMColumn,"UOM");
		prodUOMColumn.setSortable(true);
	}

	
	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
