package com.slicktechnologies.client.views.inventory.productinventoryview;

import java.util.ArrayList;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.popups.ProductSerailNumberPopup;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;

public class InvetoryViewDetailsTable extends SuperTable<ProductInventoryViewDetails> implements ClickHandler{
	NumberFormat nf=NumberFormat.getFormat("0.00");
	TextColumn<ProductInventoryViewDetails> prodIdColumn;
	TextColumn<ProductInventoryViewDetails> prodNameColumn;
	TextColumn<ProductInventoryViewDetails> prodWarehouseColumn;
	TextColumn<ProductInventoryViewDetails> prodStorageLocColumn;
	TextColumn<ProductInventoryViewDetails> prodStorageBinColumn;
	
	TextColumn<ProductInventoryViewDetails> prodViewMinColumn;
	TextColumn<ProductInventoryViewDetails> prodViewMaxColumn;
	TextColumn<ProductInventoryViewDetails> prodViewNormalLevelColumn;
	TextColumn<ProductInventoryViewDetails> prodViewReorderLevelColumn;
	TextColumn<ProductInventoryViewDetails> prodViewReorderQtyColumn;
	TextColumn<ProductInventoryViewDetails> prodViewAvailableQtyColumn;
	
	/**@Sheetal:19-03-2022
	 * Des : Adding UOM Column in product inventory view**/
	ArrayList<String> UOM = new ArrayList<String>();
	Column<ProductInventoryViewDetails, String> UOMcolumn;
	TextColumn<ProductInventoryViewDetails> viewUOMcolumn;
	
	Column<ProductInventoryViewDetails,String> prodMinQty;
	Column<ProductInventoryViewDetails,String> prodMaxQty;
	Column<ProductInventoryViewDetails,String> prodNormalLevel;
	Column<ProductInventoryViewDetails,String> prodReorderLevel;
	Column<ProductInventoryViewDetails,String> prodReorderQty;
	Column<ProductInventoryViewDetails,String> prodAvailableQty;
	Column<ProductInventoryViewDetails,String> prodDeleteColumn;
	
	
	/**
	 * nidhi
	 * 21-08-2018
	 * for map serial number
	 */
	Column<ProductInventoryViewDetails, String> addProSerialNo;
	Column<ProductInventoryViewDetails, String> viewProSerialNo;
	boolean tableState = false;
	ProductSerailNumberPopup prodSerialPopup = new ProductSerailNumberPopup(true,false);
	
	
	/**
	 * @author Vijay Chougule
	 * Date :- 11-Dec-2018
	 * Des :- For NBHC Inventory Management for Automatic PR Order Qty 
	 */
	Column<ProductInventoryViewDetails, String> avgConsumption;
	Column<ProductInventoryViewDetails, String> leadDays;
	TextColumn<ProductInventoryViewDetails> safetyStockLevel;
	Column<ProductInventoryViewDetails, String> minOrderQty;
	Column<ProductInventoryViewDetails, String> foreCastDemand;
	TextColumn<ProductInventoryViewDetails> orderQty;
	TextColumn<ProductInventoryViewDetails> expectedStock;
	TextColumn<ProductInventoryViewDetails> furtherCosmnStockReceived;
	TextColumn<ProductInventoryViewDetails> viewAvgConsumption;
	TextColumn<ProductInventoryViewDetails> viewLeadDays;
	TextColumn<ProductInventoryViewDetails> viewMinOrderQty;
	TextColumn<ProductInventoryViewDetails> viewForecastDemand;
	TextColumn<ProductInventoryViewDetails> reorderLevel;
	TextColumn<ProductInventoryViewDetails> warehouseType;
	/**
	 * ends here
	 */
	
	
	public InvetoryViewDetailsTable(){
		/**
		 * nidhi
		 * 22-08-2018
		 */
		prodSerialPopup.getLblOk().addClickHandler(this);
		prodSerialPopup.getLblCancel().addClickHandler(this);
	}
	
	@Override
	public void createTable() {
		createColumnProdId();
		createColumnProdName();
		createColumnProdWarehouse();
		createColumnProdStorageLoc();
		createColumnProdStorageBin();
		/**
		 * @author Vijay Chougule
		 * Date :- 11-Dec-2018
		 * Des :- For NBHC Inventory Management for Warehouse Type
		 */
		if(LoginPresenter.automaticPROrderQtyFlag){
			createcolumnWarehouseType();
		}
		createColumnProdMinQty();
		createColumnProdMaxQty();
		createColumnProdNormalLevle();
		createColumnProdReorderLevel();
		
		if(!LoginPresenter.automaticPROrderQtyFlag){
			createColumnProdReorderQty();
		}

		createColumnProdAvailableQty();
		createColumnProdUOM();
		/**
		 * nidhi
		 * 21-08-2018
		 * for add serial number
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createViewColumnAddProSerialNoColumn();
		}
		
		/**
		 * @author Vijay Chougule
		 * Date :- 11-Dec-2018
		 * Des :- For NBHC Inventory Management for Automatic PR Order Qty 
		 */
		if(LoginPresenter.automaticPROrderQtyFlag){
			createColumnAvgConsumption();
			createColumnLeadDays();
			createColumnStockLevel();
			createColumnMinOrderQty();
			createColumnForecastDemand();
			createColumnOrderQty();
			createColumnExpectedStock();
//			createColumnFurtherConsumption();
		}
		/**
		 * ends here
		 */
		
		createColumnProdDelete();
		addFieldUpdater();
	}
	
	public void addEditColumn(){
		createColumnProdId();
		createColumnProdName();
		createColumnProdWarehouse();
		createColumnProdStorageLoc();
		createColumnProdStorageBin();
		if(LoginPresenter.automaticPROrderQtyFlag){
			createcolumnWarehouseType();
		}	
		createColumnProdMinQty();
		createColumnProdMaxQty();
		createColumnProdNormalLevle();
		if(LoginPresenter.automaticPROrderQtyFlag){
			createColumnProdViewReorderLevel();
		}else{
			createColumnProdReorderLevel();
		}
		if(!LoginPresenter.automaticPROrderQtyFlag){
			createColumnProdReorderQty();
		}
		createColumnProdAvailableQty();
		createColumnProdUOM();
		/**
		 * nidhi
		 * 21-08-2018
		 * for add serial number
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createViewColumnAddProSerialNoColumn();
		}
		
		/**
		 * @author Vijay Chougule
		 * Date :- 11-Dec-2018
		 * Des :- For NBHC Inventory Management for Automatic PR Order Qty 
		 */
		if(LoginPresenter.automaticPROrderQtyFlag){
			createColumnAvgConsumption();
			createColumnLeadDays();
			createColumnStockLevel();
			createColumnMinOrderQty();
			createColumnForecastDemand();
			createColumnOrderQty();
			createColumnExpectedStock();
//			createColumnFurtherConsumption();
		}
		/**
		 * ends here
		 */
		
		createColumnProdDelete();
		addFieldUpdater();
	}
	
	public void addViewColumn(){
		createColumnProdId();
		createColumnProdName();
		createColumnProdWarehouse();
		createColumnProdStorageLoc();
		createColumnProdStorageBin();
		if(LoginPresenter.automaticPROrderQtyFlag){
			createcolumnWarehouseType();
		}	
		createColumnProdViewMinQty();
		createColumnProdViewMaxQty();
		createColumnProdViewNormalLevle();
		createColumnProdViewReorderLevel();
		createColumnProdViewReorderQty();
		createColumnProdViewAvailableQty();
		createColumnProdViewUOM();
		/**
		 * nidhi
		 * 21-08-2018
		 * for add serial number
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createViewColumnAddProSerialNoColumn();
		}
		
		/**
		 * @author Vijay Chougule
		 * Date :- 11-Dec-2018
		 * Des :- For NBHC Inventory Management for Automatic PR Order Qty 
		 */
		if(LoginPresenter.automaticPROrderQtyFlag){
			createColumnViewAvgConsumption();
			createColumnViewLeadDays();
			createColumnStockLevel();
			createColumnViewMinOrderQty();
			createColumnViewForecastDemand();
			createColumnOrderQty();
			createColumnExpectedStock();
//			createColumnFurtherConsumption();
		}
		/**
		 * ends here
		 */
		addFieldUpdater();
	}
	
	@Override
	public void addFieldUpdater() {
		updateColumnProdMinQty();
		updateColumnProdMaxQty();
		updateColumnProdNormalLevle();
		updateColumnProdReorderLevel();
		/**
		 * @author Vijay Chougule
		 * Date :- 10-09-2019
		 * Des :- For NBHC Inventory Management Reorder Qty should not display
		 */
		if(!LoginPresenter.automaticPROrderQtyFlag){
			updateColumnProdReorderQty();
		}
		updateColumnProdAvailableQty();
		updateColumProdUOM();
		/**
		 * nidhi
		 * 4-10-2018
		 * for map serial number
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createFieldUpdaterViewSerialNo();
		}
		/**
		 * @author Vijay Chougule
		 * Date :- 11-Dec-2018
		 * Des :- For NBHC Inventory Management for Automatic PR Order Qty 
		 */
		if(LoginPresenter.automaticPROrderQtyFlag){
			updateColumnAvgConsumption();
			updateColumnLeadDays();
			updateColumnMinOrderQty();
			updateColumnForecastDemand();
		}
		updateOnDeleteColumn();
	}
	

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addEditColumn();
		if (state == false)
			addViewColumn();
	}
	
	/***************************************************** Column View *****************************************************/
	
	public void createColumnProdId()
	{
		prodIdColumn=new TextColumn<ProductInventoryViewDetails>() {
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getProdid()+"";
			}
		};table.addColumn(prodIdColumn,"Product ID");
		table.setColumnWidth(prodIdColumn, 100,Unit.PX );
	}
	
	public void createColumnProdName()
	{
		prodNameColumn=new TextColumn<ProductInventoryViewDetails>() {
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getProdname();
			}
		};table.addColumn(prodNameColumn,"Product Name");
		table.setColumnWidth(prodNameColumn, 100,Unit.PX );
	}
	
	public void createColumnProdWarehouse()
	{
		prodWarehouseColumn=new TextColumn<ProductInventoryViewDetails>() {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getWarehousename();
			}
		};table.addColumn(prodWarehouseColumn,"Warehouse");
		table.setColumnWidth(prodWarehouseColumn, 100,Unit.PX );
	}
	
	public void createColumnProdStorageLoc()
	{
		prodStorageLocColumn=new TextColumn<ProductInventoryViewDetails>() {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getStoragelocation();
			}
		};table.addColumn(prodStorageLocColumn,"Storage Loc.");
		table.setColumnWidth(prodStorageLocColumn, 100,Unit.PX );
	}

	public void createColumnProdStorageBin(){
		prodStorageBinColumn=new TextColumn<ProductInventoryViewDetails>() {
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getStoragebin();
			}
		};table.addColumn(prodStorageBinColumn,"Storage Bin");
		table.setColumnWidth(prodStorageBinColumn, 100,Unit.PX );
	}
	
	public void createColumnProdMinQty(){
		EditTextCell editCell=new EditTextCell();
		prodMinQty=new Column<ProductInventoryViewDetails, String>(editCell) {
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return nf.format(object.getMinqty());
			}
		};table.addColumn(prodMinQty,"#Min Qty");
		table.setColumnWidth(prodMinQty, 80,Unit.PX );
	}
	
	public void createColumnProdMaxQty(){
		EditTextCell editCell=new EditTextCell();
		prodMaxQty=new Column<ProductInventoryViewDetails, String>(editCell) {
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return nf.format(object.getMaxqty());
			}
		};table.addColumn(prodMaxQty,"#Max Qty");
		table.setColumnWidth(prodMaxQty, 80,Unit.PX );
	}
	
	public void createColumnProdNormalLevle()
	{
		EditTextCell editCell=new EditTextCell();
		prodNormalLevel=new Column<ProductInventoryViewDetails, String>(editCell) {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return nf.format(object.getNormallevel());
			}
		};table.addColumn(prodNormalLevel,"#Normal level");
		table.setColumnWidth(prodNormalLevel, 90,Unit.PX );
	}
	
	public void createColumnProdReorderLevel()
	{
		EditTextCell editCell=new EditTextCell();
		prodReorderLevel=new Column<ProductInventoryViewDetails, String>(editCell) {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return nf.format(object.getReorderlevel());
			}
		};table.addColumn(prodReorderLevel,"#Reorder level");
		table.setColumnWidth(prodReorderLevel, 90,Unit.PX );

	}
	
	public void createColumnProdReorderQty()
	{
		EditTextCell editCell=new EditTextCell();
		prodReorderQty=new Column<ProductInventoryViewDetails, String>(editCell) {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return nf.format(object.getReorderqty());
			}
		};table.addColumn(prodReorderQty,"#Reorder Qty");
		table.setColumnWidth(prodReorderQty, 90,Unit.PX );
	}
	
	public void createColumnProdAvailableQty(){
		EditTextCell editCell=new EditTextCell();
		prodAvailableQty=new Column<ProductInventoryViewDetails, String>(editCell) {
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return nf.format(object.getAvailableqty());
			}
		};table.addColumn(prodAvailableQty,"#Available Qty");
		table.setColumnWidth(prodAvailableQty, 90,Unit.PX );
	}
	
	private void createColumnProdViewMinQty() {
		prodViewMinColumn=new TextColumn<ProductInventoryViewDetails>() {
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return nf.format(object.getMinqty());
			}
		};
		table.addColumn(prodViewMinColumn,"#Min Qty");
		table.setColumnWidth(prodViewMinColumn, 90,Unit.PX );
	}


	private void createColumnProdViewMaxQty() {
		prodViewMaxColumn=new TextColumn<ProductInventoryViewDetails>() {
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return nf.format(object.getMaxqty());
			}
		};
		table.addColumn(prodViewMaxColumn,"#Max Qty");
		table.setColumnWidth(prodViewMaxColumn, 90,Unit.PX );
	}


	private void createColumnProdViewNormalLevle() {
		prodViewNormalLevelColumn=new TextColumn<ProductInventoryViewDetails>() {
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return nf.format(object.getNormallevel());
			}
		};
		table.addColumn(prodViewNormalLevelColumn,"#Normal level");
		table.setColumnWidth(prodViewNormalLevelColumn, 90,Unit.PX );
	}


	private void createColumnProdViewReorderLevel() {
		prodViewReorderLevelColumn=new TextColumn<ProductInventoryViewDetails>() {
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return nf.format(object.getReorderlevel());
			}
		};
		table.addColumn(prodViewReorderLevelColumn,"#Reorder level");
		table.setColumnWidth(prodViewReorderLevelColumn, 90,Unit.PX );
	}


	private void createColumnProdViewReorderQty() {
		prodViewReorderQtyColumn=new TextColumn<ProductInventoryViewDetails>() {
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return nf.format(object.getReorderqty());
			}
		};
		table.addColumn(prodViewReorderQtyColumn,"#Reorder Qty");
		table.setColumnWidth(prodViewReorderQtyColumn, 90,Unit.PX );
	}


	private void createColumnProdViewAvailableQty() {
		prodViewAvailableQtyColumn=new TextColumn<ProductInventoryViewDetails>() {
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return nf.format(object.getAvailableqty());
			}
		};
		table.addColumn(prodViewAvailableQtyColumn,"#Available Qty");
		table.setColumnWidth(prodViewAvailableQtyColumn, 90,Unit.PX );

	}

	private void createColumnProdViewUOM() {
		viewUOMcolumn=new  TextColumn<ProductInventoryViewDetails>(){

			@Override
			public String getValue(ProductInventoryViewDetails object) 
			    {
				return object.getProdUOM();
				}
			};
			table.addColumn(viewUOMcolumn,"#UOM");
			table.setColumnWidth(viewUOMcolumn, 100,Unit.PX);
   }
		

	private void createColumnProdUOM() {
		if(UOM != null && UOM.size()>0)
			UOM.clear();
		else{
			UOM = new ArrayList<String>();
		}
		
		for(Config cfg : LoginPresenter.unitForProduct){
			UOM.add(cfg.getName());
		}
		SelectionCell UOMselection = new SelectionCell(UOM);
		UOMcolumn = new Column<ProductInventoryViewDetails, String>(UOMselection){

			@Override
			public String getValue(ProductInventoryViewDetails object) {
				if(object.getProdUOM()!=null&&!object.getProdUOM().equals("")){
					return object.getProdUOM();
				}else{
					return "NA";
				}
			}
		};
		table.addColumn(UOMcolumn, "#UOM");
		table.setColumnWidth(UOMcolumn, 90, Unit.PX);
	}

	
	private void createColumnProdDelete() {
		ButtonCell btnCell = new ButtonCell();
		prodDeleteColumn = new Column<ProductInventoryViewDetails, String>(btnCell) {
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return "Delete";
			}
		};
		table.addColumn(prodDeleteColumn, "Delete");
		table.setColumnWidth(prodDeleteColumn, 90,Unit.PX );

	}
	
/********************************************* Update Column View *************************************************/
	
	private void updateColumnProdMinQty() {
		prodMinQty.setFieldUpdater(new FieldUpdater<ProductInventoryViewDetails, String>() {
			@Override
			public void update(int index, ProductInventoryViewDetails object,String value) {
				try {
					double val1;
					val1 = Double.parseDouble(value.trim());
					object.setMinqty(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	private void updateColumnProdMaxQty() {
		prodMaxQty.setFieldUpdater(new FieldUpdater<ProductInventoryViewDetails, String>() {
			@Override
			public void update(int index, ProductInventoryViewDetails object,String value) {
				try {
					double val1;
					val1 = Double.parseDouble(value.trim());
					object.setMaxqty(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	private void updateColumnProdNormalLevle() {
		prodNormalLevel.setFieldUpdater(new FieldUpdater<ProductInventoryViewDetails, String>() {
			@Override
			public void update(int index, ProductInventoryViewDetails object,String value) {
				try {
					double val1;
					val1 = Double.parseDouble(value.trim());
					object.setNormallevel(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	private void updateColumnProdReorderLevel() {
		prodReorderLevel.setFieldUpdater(new FieldUpdater<ProductInventoryViewDetails, String>() {
			@Override
			public void update(int index, ProductInventoryViewDetails object,String value) {
				try {
					double val1;
					val1 = Double.parseDouble(value.trim());
					object.setReorderlevel(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	private void updateColumnProdReorderQty() {
		prodReorderQty.setFieldUpdater(new FieldUpdater<ProductInventoryViewDetails, String>() {
			@Override
			public void update(int index, ProductInventoryViewDetails object,String value) {
				try {
					double val1;
					val1 = Double.parseDouble(value.trim());
					object.setReorderqty(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	private void updateColumnProdAvailableQty() {
		prodAvailableQty.setFieldUpdater(new FieldUpdater<ProductInventoryViewDetails, String>() {
			@Override
			public void update(int index, ProductInventoryViewDetails object,String value) {
				try {
					double val1;
					val1 = Double.parseDouble(value.trim());
					object.setAvailableqty(val1);
					
					/**
					 * Date 13-Dec-2018 By Vijay for NBHC Inventory Managment
					 */
					if(LoginPresenter.automaticPROrderQtyFlag){
					 object.setOrderQty(AppUtility.getOrderQty(object.getAvailableqty(), object.getForecastDemand(), 
								object.getConsumptiontillStockReceived(), object.getReorderlevel(), 
								object.getMinOrderQty(),object.getSafetyStockLevel()));

					 object.setExpectedStock(getExpectedStock(object.getAvailableqty(),object.getOrderQty(),object.getConsumptiontillStockReceived()));
					 table.redraw();
					}
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	private void updateColumProdUOM() {
		UOMcolumn.setFieldUpdater(new FieldUpdater<ProductInventoryViewDetails, String>()
				{

					@Override
					public void update(int index, ProductInventoryViewDetails object, String value) {
						try{
							String val1 =  (value.trim());
							object.setProdUOM(val1);
						}catch(Exception e){
							
						}
						table.redrawRow(index);
					}
				});
		
	}

	private void updateOnDeleteColumn() {
		prodDeleteColumn.setFieldUpdater(new FieldUpdater<ProductInventoryViewDetails, String>() {
			@Override
			public void update(int index, ProductInventoryViewDetails object, String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void applyStyle() {
		
	}
	
	public void setVisible()
	{
		
	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource().equals(prodSerialPopup.getLblOk())){
			prodSerialPopup.hidePopUp();
		}else{
			prodSerialPopup.hidePopUp();
		}
	}
	public void createViewColumnAddProSerialNoColumn() {
		ButtonCell btnCell = new ButtonCell();
		viewProSerialNo = new Column<ProductInventoryViewDetails, String>(btnCell) {
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return "View Product Serial Nos";
			}
		};
		table.addColumn(viewProSerialNo, "View Details");
		table.setColumnWidth(viewProSerialNo,180,Unit.PX);

	}
	public void createFieldUpdaterViewSerialNo() {
		viewProSerialNo.setFieldUpdater(new FieldUpdater<ProductInventoryViewDetails, String>() {
			@Override
			public void update(int index, ProductInventoryViewDetails object, String value) {
				prodSerialPopup.setProSerialenble(false,false);
				prodSerialPopup.getPopup().setSize("50%", "40%");
				prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
				if(object.getProSerialNoDetails().size()>0){
					ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
//						for(int i = 0 ; i < object.getProSerialNoDetails().get(0).size() ; i ++ ){
//							ProductSerialNoMapping pro = new ProductSerialNoMapping();
					System.out.println("size  -- " + serNo.size());
							serNo.addAll(object.getProSerialNoDetails().get(0));
//						}
							prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
						prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
						prodSerialPopup.getProSerNoTable().getTable().redraw();
					}
				prodSerialPopup.showPopUp();
			
			}
		});
	}

	
	
	/**
	 * @author Vijay Chougule
	 * Date :- 11-Dec-2018
	 * Des :- For NBHC Inventory Management for Automatic PR Order Qty 
	 */
	
//	private void createColumnFurtherConsumption() {
//		furtherCosmnStockReceived = new TextColumn<ProductInventoryViewDetails>() {
//			
//			@Override
//			public String getValue(ProductInventoryViewDetails object) {
//				return object.getConsumptiontillStockReceived()+"";
//			}
//		};
//		table.addColumn(furtherCosmnStockReceived,"Further Stock Received");
//		table.setColumnWidth(furtherCosmnStockReceived, 100, Unit.PX);
//	}

	private void createColumnExpectedStock() {
		expectedStock = new TextColumn<ProductInventoryViewDetails>() {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getExpectedStock()+"";
			}
		};
		table.addColumn(expectedStock,"Expected Stock");
		table.setColumnWidth(expectedStock, 90,Unit.PX);
	}

	private void createColumnOrderQty() {
		orderQty = new TextColumn<ProductInventoryViewDetails>() {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getOrderQty()+"";
			}
		};
		table.addColumn(orderQty,"Order Qty");
		table.setColumnWidth(orderQty, 90,Unit.PX);
	}

	private void createColumnForecastDemand() {
		EditTextCell editCell = new EditTextCell();
		foreCastDemand = new Column<ProductInventoryViewDetails, String>(editCell) {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getForecastDemand()+"";
			}
		};
		table.addColumn(foreCastDemand,"#Forecast/Demand");
		table.setColumnWidth(foreCastDemand, 100,Unit.PX);
	}

	private void createColumnMinOrderQty() {
		EditTextCell editCell = new EditTextCell();
		minOrderQty = new Column<ProductInventoryViewDetails, String>(editCell) {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getMinOrderQty()+"";
			}
		};
		table.addColumn(minOrderQty,"#Min Order Qty");
		table.setColumnWidth(minOrderQty, 100,Unit.PX);
	}

	private void createColumnStockLevel() {
		safetyStockLevel = new TextColumn<ProductInventoryViewDetails>() {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getSafetyStockLevel()+"";
			}
		};
		table.addColumn(safetyStockLevel,"Safety Stock Level");
		table.setColumnWidth(safetyStockLevel, 90,Unit.PX);
	}

	private void createColumnLeadDays() {
		EditTextCell editCell = new EditTextCell();
		leadDays = new Column<ProductInventoryViewDetails, String>(editCell) {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getLeadDays()+"";
			}
		};
		table.addColumn(leadDays,"#Lead Days");
		table.setColumnWidth(leadDays, 90,Unit.PX);
	}

	private void createColumnAvgConsumption() {
		EditTextCell editCell = new EditTextCell();
		avgConsumption = new Column<ProductInventoryViewDetails, String>(editCell) {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getAvgConsumption()+"";
			}
		};
		table.addColumn(avgConsumption,"# Per Monthly Avg Consumption");
		table.setColumnWidth(avgConsumption, 120,Unit.PX);
	}

	
	private void createColumnViewForecastDemand() {
		viewForecastDemand = new TextColumn<ProductInventoryViewDetails>() {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getForecastDemand()+"";
			}
		};
		table.addColumn(viewForecastDemand,"#Forecast/ Demand");
		table.setColumnWidth(viewForecastDemand, 90,Unit.PX);
	}

	private void createColumnViewMinOrderQty() {
		viewMinOrderQty = new TextColumn<ProductInventoryViewDetails>() {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getMinOrderQty()+"";
			}
		};
		table.addColumn(viewMinOrderQty,"#Min Order Qty");
		table.setColumnWidth(viewMinOrderQty, 90,Unit.PX);
		
	}

	private void createColumnViewLeadDays() {
		viewLeadDays = new TextColumn<ProductInventoryViewDetails>() {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getLeadDays()+"";
			}
		};
		table.addColumn(viewLeadDays,"#Lead Days");
		table.setColumnWidth(viewLeadDays, 90,Unit.PX);
	}

	private void createColumnViewAvgConsumption() {
		viewAvgConsumption = new TextColumn<ProductInventoryViewDetails>() {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return object.getAvgConsumption()+"";
			}
		};
		table.addColumn(viewAvgConsumption,"# Per Monthly Avg Consumption");
		table.setColumnWidth(viewAvgConsumption, 120,Unit.PX);
	}
	
	
	
	private void updateColumnAvgConsumption() {
		avgConsumption.setFieldUpdater(new FieldUpdater<ProductInventoryViewDetails, String>() {
			
			@Override
			public void update(int index, ProductInventoryViewDetails object,
					String value) {
				double avgConsumptionValue =  Double.parseDouble(value);
				object.setAvgConsumption(avgConsumptionValue);
				double safetyStockLevel = (avgConsumptionValue*110/100);
				object.setSafetyStockLevel(safetyStockLevel);
				
				object.setConsumptiontillStockReceived(getfurtherConsumptionStockReceived(avgConsumptionValue,object.getLeadDays()));

				double reorderLevel = getReorderLevel(avgConsumptionValue,object.getLeadDays(),object.getSafetyStockLevel());
				object.setReorderlevel(reorderLevel);
				
				object.setOrderQty(AppUtility.getOrderQty(object.getAvailableqty(), object.getForecastDemand(), 
						object.getConsumptiontillStockReceived(), object.getReorderlevel(), 
						object.getMinOrderQty(),object.getSafetyStockLevel()));
				
				object.setExpectedStock(getExpectedStock(object.getAvailableqty(),object.getOrderQty(),object.getConsumptiontillStockReceived()));

				table.redraw();
			}

			

			
		});
	}

	private void updateColumnLeadDays() {
		leadDays.setFieldUpdater(new FieldUpdater<ProductInventoryViewDetails, String>() {
			
			@Override
			public void update(int index, ProductInventoryViewDetails object,
					String value) {
				int leadDaysValue = Integer.parseInt(value);
				object.setLeadDays(leadDaysValue);
				object.setConsumptiontillStockReceived(getfurtherConsumptionStockReceived(object.getAvgConsumption(),object.getLeadDays()));
				
				object.setExpectedStock(getExpectedStock(object.getAvailableqty(),object.getOrderQty(),object.getConsumptiontillStockReceived()));

				double reorderLevel = getReorderLevel(object.getAvgConsumption(),object.getLeadDays(),object.getSafetyStockLevel());
				object.setReorderlevel(reorderLevel);
				table.redraw();
			}
		});
	}

	private void updateColumnMinOrderQty() {
		minOrderQty.setFieldUpdater(new FieldUpdater<ProductInventoryViewDetails, String>() {
			
			@Override
			public void update(int index, ProductInventoryViewDetails object,
					String value) {
				double minOrderQty = Double.parseDouble(value);
				object.setMinOrderQty(minOrderQty);
				object.setOrderQty(AppUtility.getOrderQty(object.getAvailableqty(), object.getForecastDemand(), 
						object.getConsumptiontillStockReceived(), object.getReorderlevel(), 
						object.getMinOrderQty(),object.getSafetyStockLevel()));
				table.redraw();
			}
		});
	}

	private void updateColumnForecastDemand() {
		foreCastDemand.setFieldUpdater(new FieldUpdater<ProductInventoryViewDetails, String>() {
			
			@Override
			public void update(int index, ProductInventoryViewDetails object,
					String value) {
				double forecastDemandValue = Double.parseDouble(value);
				object.setForecastDemand(forecastDemandValue);
				double orderQty = AppUtility.getOrderQty(object.getAvailableqty(), forecastDemandValue, 
						object.getConsumptiontillStockReceived(), object.getReorderlevel(), 
						object.getMinOrderQty(),object.getSafetyStockLevel());
				object.setOrderQty(orderQty);
				object.setExpectedStock(getExpectedStock(object.getAvailableqty(),object.getOrderQty(),object.getConsumptiontillStockReceived()));
				table.redraw();
			}
		});
	}
	
/**
 * Date 28-01-2019 by Vijay
 * Des :- As per Yash and kaastuk sir request they not required this calculation so below code commented
 * Requirements :- NBHC Inventory Management	
 * @param avgConsumptionValue
 * @param leadDays
 * @param safetyStockLevel
 * @return
 */
	private double getfurtherConsumptionStockReceived(double avgConsumptionValue, int leadDays) {
		 double furtherConsumptiontillStockReceived = Math.round(avgConsumptionValue*leadDays/30);
		return furtherConsumptiontillStockReceived;
	}
	
	private double getReorderLevel(double avgConsumptionValue,
			int leadDays, double safetyStockLevel) {
		double reOrderLevel = (((avgConsumptionValue/30)*leadDays)+safetyStockLevel);
		reOrderLevel = AppUtility.getRoundUpNumber(reOrderLevel);
		return reOrderLevel;
	}
	
	private double getExpectedStock(double availableqty,
			double orderQty, double consumptiontillStockReceived) {
		return orderQty+availableqty-consumptiontillStockReceived;
//		return orderQty+availableqty;

	}
	
	
	private void createcolumnWarehouseType() {
		warehouseType = new TextColumn<ProductInventoryViewDetails>() {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				if(object.getWarehouseType()!=null)
					return object.getWarehouseType();
				return "";
			}
		};
		table.addColumn(warehouseType,"Warehouse Type");
		table.setColumnWidth(warehouseType, 100,Unit.PX);

	}
	
	/**
	 * ends here
	 */
	
	
	
}
