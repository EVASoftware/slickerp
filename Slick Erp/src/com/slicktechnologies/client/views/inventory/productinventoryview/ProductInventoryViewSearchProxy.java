package com.slicktechnologies.client.views.inventory.productinventoryview;

import java.util.Vector;

import com.google.gwt.dom.client.Style.Unit;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class ProductInventoryViewSearchProxy extends SearchPopUpScreen<ProductInventoryView> {
	ProductInfoComposite productInfoComposite;
	
	/**
	 * @author Anil , Date : 30-07-2019
	 * Adding product category search
	 */
	ObjectListBox<Category> olbcProductCategory;
	
	public ProductInventoryViewSearchProxy() {
		super();
		createGui();
	}
	
	private void initializeWidget(){
		productInfoComposite = AppUtility.initiateSalesProductComposite(new SuperProduct());
//		productInfoComposite.getProdCategory().setVisible(false);
//		productInfoComposite.getProdPrice().setVisible(false);
//		productInfoComposite.getUnitOfmeasuerment().setVisible(false);
//		productInfoComposite.getProdID().setText("");
//		productInfoComposite.getProdPrice().setText("");
//		productInfoComposite.getUnitOfmeasuerment().setText("");
		
		olbcProductCategory=new ObjectListBox<Category>();
		olbcProductCategory.getElement().getStyle().setWidth(350, Unit.PX);
		initalizeCategoryComboBox();
	}
	
	public void initalizeCategoryComboBox()
	{
	   MyQuerry querry=new MyQuerry();
	   querry.setQuerryObject(new Category());
	   Filter categoryFilter = new Filter();
	   categoryFilter.setQuerryString("isSellCategory");
	   categoryFilter.setBooleanvalue(true);
	   Filter statusfilter=new Filter();
	   statusfilter.setQuerryString("status");
	   statusfilter.setBooleanvalue(true);
	   querry.getFilters().add(categoryFilter);
	   querry.getFilters().add(statusfilter);
	   olbcProductCategory.MakeLive(querry);
	}
	
	@Override
	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		
		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		

		fbuilder = new FormFieldBuilder("Product Category",olbcProductCategory);
		FormField folbcProductCategory= fbuilder.setMandatory(false).setMandatoryMsg("Product Category is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = { 
				{ fproductInfoComposite },
				{ folbcProductCategory}
				};

		this.fields = formfield;
	}
	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;
		
		if (!productInfoComposite.getProdName().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(productInfoComposite.getProdName().getValue());
			temp.setQuerryString("productinfo.productName");
			filtervec.add(temp);
		}
		if (!productInfoComposite.getProdCode().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(productInfoComposite.getProdCode().getValue());
			temp.setQuerryString("productinfo.productCode");
			filtervec.add(temp);
		}

		if (!productInfoComposite.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
			temp.setQuerryString("productinfo.prodID");
			filtervec.add(temp);
		}
		
		if (olbcProductCategory.getValue()!=null) {
			temp = new Filter();
			temp.setStringValue(olbcProductCategory.getValue());
			temp.setQuerryString("productinfo.productCategory");
			filtervec.add(temp);
		}

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProductInventoryView());
		return querry;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

}
