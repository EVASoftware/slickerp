package com.slicktechnologies.client.views.inventory.inspectionlist;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.inspection.InspectionForm;
import com.slicktechnologies.client.views.inventory.inspection.InspectionPresenter;
import com.slicktechnologies.client.views.inventory.inspection.InspectionPresenterSearchProxy;
import com.slicktechnologies.client.views.inventory.inspection.InspectionPresenterTableProxy;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class InspectionListPresenter extends TableScreenPresenter<Inspection>{

	TableScreen<Inspection>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	public InspectionListPresenter(TableScreen<Inspection> view, Inspection model) {
		super(view, model);
		form=view;
		form.getSearchpopupscreen().getDwnload().setVisible(false);
//		view.retriveTable(getProductTransactionListQuery());
		setTableSelectionOnProducts();
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.INSPECTIONLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	private MyQuerry getProductTransactionListQuery() {
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new Inspection());
		return quer;
	}

	public InspectionListPresenter(TableScreen<Inspection> view,Inspection model,MyQuerry querry) {
		super(view, model);
		form=view;
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		view.retriveTable(querry);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.INSPECTIONLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}

	}
	
	public static void initalize()
	{
		InspectionPresenterTableProxy table=new InspectionPresenterTableProxy();
		InspectionListForm form=new InspectionListForm(table);
		
		InspectionPresenterSearchProxy.staticSuperTable=table;
		InspectionPresenterSearchProxy searchPopUp=new InspectionPresenterSearchProxy(false);
		form.setSearchpopupscreen(searchPopUp);
		
		InspectionListPresenter presenter=new InspectionListPresenter(form,new Inspection());
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	public static void initalize(MyQuerry querry)
	{
		InspectionPresenterTableProxy table=new InspectionPresenterTableProxy();
		InspectionListForm form=new InspectionListForm(table);
		
		InspectionPresenterSearchProxy.staticSuperTable=table;
		InspectionPresenterSearchProxy searchPopUp=new InspectionPresenterSearchProxy(false);
		form.setSearchpopupscreen(searchPopUp);
		
		InspectionListPresenter presenter=new InspectionListPresenter(form,new Inspection(),querry);
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}

	
	@Override
	protected void makeNewModel() {
		model=new Inspection();
	}
	
	@Override
	public void reactOnDownload() {
		ArrayList<Inspection> custarray = new ArrayList<Inspection>();
		List<Inspection> list = (List<Inspection>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		custarray.addAll(list);
		csvservice.setInspectionDetails(custarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 83;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	
	private void setTableSelectionOnProducts() {
		final NoSelectionModel<Inspection> selectionModelMyObj = new NoSelectionModel<Inspection>();
		 
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	    	 @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	    		 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Inspection",Screen.INSPECTION);
	        	 final Inspection entity=selectionModelMyObj.getLastSelectedObject();
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 final InspectionForm form = InspectionPresenter.initalize();
	        	  
	        	 form.showWaitSymbol();
	        	 Timer timer=new Timer() {
	 				
	        	@Override
	 			public void run() {
	        		form.hideWaitSymbol();
					form.updateView(entity);
				    form.setToViewState();
	 			}
	 		};
	        timer.schedule(3000); 
	     }
	  };
	  // Add the handler to the selection model
	  selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	  // Add the selection model to the table
	  form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
	}
	
	
	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}
}
