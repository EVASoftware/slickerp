package com.slicktechnologies.client.views.inventory.recievingnote;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.resources.css.MergeRulesByContentVisitor;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.DataMigrationService;
import com.slicktechnologies.client.services.DataMigrationServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.cnc.CNCForm;
import com.slicktechnologies.client.views.cnc.CNCPresenter;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.popups.PartialGRNCancellationPopup;
import com.slicktechnologies.client.views.project.tool.UploadAssetPopup;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoForm;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoPresenter;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.InspectionDetails;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class GRNPresenter extends ApprovableFormScreenPresenter<GRN> {
	public static GRNForm form;
	final static GenricServiceAsync async = GWT.create(GenricService.class);
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	EmailServiceAsync emailService = GWT.create(EmailService.class);
	UpdateServiceAsync updateService=GWT.create(UpdateService.class);
	static ArrayList<ProductQty> poList;
	static ArrayList<ProductQty> grnList;
	
	DocumentCancellationPopUp popup = new DocumentCancellationPopUp();
	PopupPanel panel ;
	ArrayList<InspectionDetails>inspectionList;
	static int poNo=0;
	int count=0;
	int rowIndex=0;
	
	/**
	 * Added by RahulVerma on 19 June 2017
	 * This was in reactToProcessBarEvents
	 * Making it global
	 */
	String text;
	
	/**
	 * this variable stores the warehouse name for which grn is raised and it is used to validate
	 * warehouse name while selecting warehouse at product level.
	 * Date : 13-10-2016 By Anil
	 * Release : 30 SEPT 2016
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */
	public static String warehouseName="";
	
	/**
	 * End
	 */
	
	/**
	 * This fields are used to create large no. of assets
	 * In upload popup you can download the format and 
	 * upload asset excel to create assetts in the system
	 * Date : 22-10-2016 By ANil
	 * Release : 30 Sept 2016
	 * Project : Purchase Modification(NBHC)
	 *
	 */
	UploadAssetPopup uploadPopup=new UploadAssetPopup();
	DataMigrationServiceAsync datamigAsync = GWT.create(DataMigrationService.class);
	/**
	 * End
	 */
	
	/**
	 * Date 13-04-2019 by Vijay
	 * Des :- This is to select specific record and cancel the specific material (Partial GRN cancellation)
	 */
	PartialGRNCancellationPopup partialGRNCancellation = new PartialGRNCancellationPopup();
	
	public GRNPresenter(ApprovableFormScreen<GRN> view, GRN model) {
		super(view, model);
		form = (GRNForm) view;
		form.getTable().connectToLocal();
		
		uploadPopup.getBtnOk().addClickHandler(this);
		uploadPopup.getBtnCancel().addClickHandler(this);
		uploadPopup.getDownloadFormat().addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.GRN,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
		
		partialGRNCancellation.getLblCancel().addClickHandler(this);
		partialGRNCancellation.getLblOk().addClickHandler(this);
	}

	public static void showMessage(String msg){
		form.showDialogMessage(msg);
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		text = label.getText().trim();

		if (text.equals(AppConstants.CANCELGRN)) {
			if(!model.getStatus().equals(GRN.REQUESTED)){
				popup.getPaymentDate().setValue(new Date()+"");
				popup.getPaymentID().setValue(model.getCount()+"");
				popup.getPaymentStatus().setValue(model.getStatus());
				panel=new PopupPanel(true);
				panel.add(popup);
				panel.show();
				panel.center();
			}else{
				form.showDialogMessage("Cancel approval request to cancel the GRN.");
			}
		}
		if (text.equals(AppConstants.NEW)){
			reactToNew();
		}
		if (text.equals("DownLoad")){
			reactOnDownload();
		}
		if (text.equals("Email")){
			reactOnEmail();
		}
		
		if (text.equals(AppConstants.REQUESTINSPECTION)){
			if(form.validateWarehouse()){
				updateInspectionDetails();
			}
			else{
				form.showDialogMessage("Please Select Warehouse Details.");
			}
		}
		
		if(text.equals(ManageApprovals.APPROVALREQUEST))
		{
			if(form.validateWarehouse()){
				if(isExcelUploade()){/*
					form.getManageapproval().reactToRequestForApproval();
				*/

					/**
					 * Date 07-07-2018 By Vijay
					 * Des :- NBHC CCPM As per vaishali mam at this level this validation not required we can send request for approval any moment so below code commented
					 */  
					
					/**
//					 *  nidhi
//					 *  15-05-2018
//					 *  for GRN Approval Validation
//					 */
//					if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
//						if(form.validateGRNDate())	
//						{
//							form.getManageapproval().reactToRequestForApproval();
//						}
//					}else{
						form.getManageapproval().reactToRequestForApproval();
//					}
					/**
					 *  end
					 */
					
				
				}else{
					form.showDialogMessage("Please upload asset excel.");
				}
			}else{
				form.showDialogMessage("Please Select Warehouse Details.");
			}
		}
		if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
		{
			cancelApprovalLogic();
		}
		
		if(text.equals(AppConstants.UPLOADASSET)){
			uploadPopup.clear();
			panel=new PopupPanel(true);
			panel.add(uploadPopup);
			panel.show();
			panel.center();
		}
		if(text.equals(AppConstants.SUBMIT)){
			/** date 29.07.2019 added by komal to add warehouse validation while submitting  GRN**/
			Console.log("submit :");
			if(form.validateWarehouse()){
				Console.log("submit  1:");
				if(isExcelUploade()){
					Console.log("submit  2:");
					/*
							//			/**
			//			 *  nidhi
			//			 *  15-05-2018
			//			 *  for GRN Approval Validation
			//			 */
			//			if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
			//				if(form.validateGRNDate())	
			//				{
								form.getManageapproval().reactToSubmit();
			//				}
			//			}
						}else{
							form.showDialogMessage("Please upload asset excel.");
						}
					}else{
						form.showDialogMessage("Please Select Warehouse Details.");
					}
		}
		if(text.equals(AppConstants.PARTIALCANCEL)){
			if(!model.getStatus().equals(GRN.REQUESTED)){
//				popup.getPaymentDate().setValue(new Date()+"");
//				popup.getPaymentID().setValue(model.getCount()+"");
//				popup.getPaymentStatus().setValue(model.getStatus());
//				panel=new PopupPanel(true);
//				panel.add(popup);
//				panel.show();
//				panel.center();
				partialGRNCancellation.showPopUp();
				partialGRNCancellation.getTable().getDataprovider().setList(model.getInventoryProductItem());
				partialGRNCancellation.getTable().setEnable(false);
				
			}else{
				form.showDialogMessage("Cancel approval request to cancel the GRN.");
			}
		}
		/** date 3.10.2018 added by komal to create sales order from grn**/
		if(text.equals(AppConstants.CREATESALESORDER)){
			reactOnCreateSalesOrder();
		}
		if (text.equals(AppConstants.VIEWORDER)) {
			viewOrderDocuments();
		}
		
		if (text.equals(AppConstants.VIEWBILL)) {
			viewBillDocuments();
		}
		
		
		
	}
	
	
	
private void viewBillDocuments() {
	final MyQuerry querry=new MyQuerry();
	Vector<Filter> temp=new Vector<Filter>();
	Filter filter=null;
	
	filter=new Filter();
	filter.setQuerryString("contractCount");
	filter.setIntValue(model.getPoNo());
	temp.add(filter);
	
	/**Date 30-1-2020 by Amol added a Type Of Order Querry**/
	filter=new Filter();
	filter.setQuerryString("typeOfOrder");
	filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
	temp.add(filter);
	
	
	querry.setFilters(temp);
	querry.setQuerryObject(new BillingDocument());
	form.showWaitSymbol();
	service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		@Override
		public void onFailure(Throwable caught) {
			form.hideWaitSymbol();
		}

		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			form.hideWaitSymbol();
			if(result.size()==0){
				form.showDialogMessage("No billing document found.");
				return;
			}
			if(result.size()==1){
				final BillingDocument billDocument=(BillingDocument) result.get(0);
				final BillingDetailsForm form=BillingDetailsPresenter.initalize();
				Timer timer=new Timer() {
					@Override
					public void run() {
						form.updateView(billDocument);
						form.setToViewState();
					}
				};
				timer.schedule(1000);
			}else{
				BillingListPresenter.initalize(querry);
			}
			
		}
	});
	
}

/**Date 23-1-2019 
 * @author Amol added for view order
 */
	private void viewOrderDocuments() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
	
		filter.setIntValue(model.getPoNo());
		temp.add(filter);
	//	querry.setFilters(temp);
//		filter=new Filter();
//		filter.setQuerryString("typeOfOrder");
//		filter.setStringValue(AppConstants.ORDERTYPEFORPURCHASE);
//		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new PurchaseOrder());
	
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No order document found.");
					return;
				} else {
					final PurchaseOrder purchaseorder=(PurchaseOrder) result.get(0);
					final PurchaseOrderForm form=PurchaseOrderPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(purchaseorder);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
			}
			}
		});
	}

	@Override
	public void reactOnPrint() {
		boolean grnUpdate = AppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", "GRNUpdated");

		if(grnUpdate){
			final String url = GWT.getModuleBaseURL()+ "grn" + "?Id=" + model.getId()+"&type=GRNUpdated";
			Window.open(url, "test", "enabled");
		}else{
			final String url = GWT.getModuleBaseURL()+ "grn" + "?Id=" + model.getId();
			Window.open(url, "test", "enabled");
		}
	}

	@Override
	public void reactOnDownload() {
		ArrayList<GRN> custarray = new ArrayList<GRN>();
		List<GRN> list = (List<GRN>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		custarray.addAll(list);

		csvservice.setGRN(custarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {/*
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 48;
				Window.open(url, "test", "enabled");
			*/
			
				/**
				 * nidhi
				 * 16-4-2018
				 * for grn custom report download using process configration
				 * 
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", "GRNReport")){
					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url = gwt + "csvservlet" + "?type=" + 138;
					Window.open(url, "test", "enabled");
				}else{
				
					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url = gwt + "csvservlet" + "?type=" + 48;
					Window.open(url, "test", "enabled");
				}
				/**
				 * end
				 */
				
			}
		});
	}
	
	
	
	
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		if(event.getSource().equals(popup.getBtnOk())){
			panel.hide();
//			createNewMMNForSelectedGRN();
			if(model.getStatus().equals(GRN.CREATED)){
				changeStatus();
			}
//			else if(model.getStatus().equals(GRN.APPROVED)){
//				cancelGRN();
//			}
			else if(model.getStatus().equals(GRN.APPROVED)&&!text.equals(AppConstants.PARTIALCANCEL)){
				cancelGRN();
			}else if(model.getStatus().equals(GRN.APPROVED)&&text.equals(AppConstants.PARTIALCANCEL)){
				System.out.println("Inside Partial Cancel!!!!!");
				cancelGRN();
			}
		}
		else if(event.getSource()==popup.getBtnCancel()){
			panel.hide();
			popup.remark.setValue("");
		}
		
		if(event.getSource().equals(uploadPopup.getDownloadFormat())){
			
			System.out.println("MODEL COUNT "+model.getCount());
			
//			GRNDetails object=getAssetProductDetails();
			csvservice.getAssetUploadFormat(model, new AsyncCallback<Void>() {
				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+113;
					Window.open(url, "test", "enabled");
				}
				
				@Override
				public void onFailure(Throwable caught) {
					
				}
			});
			panel.hide();
		}
		if(event.getSource().equals(uploadPopup.getBtnOk())){
			form.showWaitSymbol();
			datamigAsync.savedRecordsDeatils(UserConfiguration.getCompanyId(), "Company Asset", false,LoginPresenter.loggedInUser,new AsyncCallback<ArrayList<Integer>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
					panel.hide();	
				}

				@Override
				public void onSuccess(ArrayList<Integer> result) {
					if (result.contains(-1)) {
						form.hideWaitSymbol();
						form.showDialogMessage("Invalid Excel File");
					}else if (result.contains(-16)) {
						form.hideWaitSymbol();
						form.showDialogMessage("Error: Asset Name Already Exists!!");  
					} else if (result.contains(-17)) {
						form.hideWaitSymbol();
						form.showDialogMessage("Error: Please set date format to date column!!");  
					}else if (result.contains(-18)) {
						form.hideWaitSymbol();
						form.showDialogMessage("Error: Please upload .xls format file!!");  
					}else {
						
						model.setAssetExcelUplod(uploadPopup.getUpload().getValue());
						
						service.save(model, new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
								panel.hide();
							}

							@Override
							public void onSuccess(ReturnFromServer result) {
								form.hideWaitSymbol();
								form.showDialogMessage("Sucessfully");
								form.assetExcelLink=model.getAssetExcelUplod();
								form.hyperlink.setText(model.getAssetExcelUplod().getName());
								form.hyperlink.setVisible(true);
								panel.hide();
								
							}
						});
						
						
						
					}
				}
			});
			
				
		}
		if(event.getSource().equals(uploadPopup.getBtnCancel())){
			panel.hide();
		}
		/**
		 *  Date 13-04-2019 by Vijay
		 *  Des :- for NBHC CCPM Partial GRN cancellation
		 */
		if(event.getSource().equals(partialGRNCancellation.getLblOk())){
			reactonPartialGRNCancellation();
			
		}
		if(event.getSource().equals(partialGRNCancellation.getLblCancel())){
			partialGRNCancellation.hidePopUp();
		}
	}
	
	
	private void reactonPartialGRNCancellation() {
		
		ArrayList<GRNDetails> grnMaterialList = getSelectedRecords(partialGRNCancellation.getTable().getDataprovider().getList());
		if(grnMaterialList.size()==0){
			form.showDialogMessage("Please select at least one material!");
			return;
		}
		
		updateGRN(model,grnMaterialList);
	}


	private ArrayList<GRNDetails> getSelectedRecords(List<GRNDetails> list) {
		ArrayList<GRNDetails> grnMaterialList = new ArrayList<GRNDetails>();
		for(GRNDetails grndetails : list){
			if(grndetails.isRecordSelect()){
				grnMaterialList.add(grndetails);
			}
		}
		return grnMaterialList;
	}

	/**
	 * This method return true if products in grn are not of asset category
	 * if category is asset then it checks whether asset excel is uploaded or not
	 * Date : 22-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : Purchase Modification (NBHC)
	 */
	public boolean isExcelUploade(){
		if(form.isAsset()){
			if(model.getAssetExcelUplod()!=null&&!model.getAssetExcelUplod().getName().equals("")){
//					&&!model.getAssetExcelUplod().getUrl().equals("")){
				return true;
			}
		}else{
			return true;
		}
		return false;
	}

	/**
	 * This method returns the asset product details which will be used as sample data
	 * when user download the asset format
	 * Date : 22-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : Purchase Modification
	 */
	public GRNDetails getAssetProductDetails(){
		for(GRNDetails obj:model.getInventoryProductItem()){
			if(obj.getProductCategory().equals("ASSET")){
				return obj;
			}
		}
		return null;
	}
	
	

	public void reactOnEmail() {
		
		boolean conf = Window.confirm("Do you really want to send email?");
		if (conf == true) {
			emailService.initiateGRNEmail((GRN)model, new AsyncCallback<Void>() {
				@Override
				public void onSuccess(Void result) {
					Window.alert("Email Sent Sucessfully !");
				}
				
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Resource Quota Ended");
					caught.printStackTrace();
				}
			});
		}
	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model = new GRN();
	}

	public static GRNForm initalize() {
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("GRN",Screen.GRN);
		GRNForm form = new GRNForm();

		GRNSearchTable gentable = new GRNSearchTable();
		gentable.setView(form);
		gentable.applySelectionModle();
		GRNSearchForm.staticSuperTable = gentable;
		GRNSearchForm searchpopup = new GRNSearchForm();
		form.setSearchpopupscreen(searchpopup);

		GRNPresenter presenter = new GRNPresenter(form, new GRN());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	public static GRNForm initalize(MyQuerry querry) {
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("GRN",Screen.GRN);
		GRNForm form = new GRNForm();

		GRNSearchTable gentable = new GRNSearchTable();
		gentable.setView(form);
		gentable.applySelectionModle();
		GRNSearchForm.staticSuperTable = gentable;
		GRNSearchForm searchpopup = new GRNSearchForm();
		form.setSearchpopupscreen(searchpopup);

		GRNPresenter presenter = new GRNPresenter(form, new GRN());
		AppMemory.getAppMemory().stickPnel(form);

		return form;
	}

	private void reactToCancel() {
		model.setStatus(GRN.CANCELLED);
		changeStatus("Failed To Cancel GRN!", "GRN Cancelled !");
	}

	private void reactToNew() {
		form.setToNewState();
		initalize();
	}

	private void changeStatus(final String failureMessage,final String successMessage) {
		GenricServiceAsync async = GWT.create(GenricService.class);
		form.showWaitSymbol();
		async.save(model, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onSuccess(ReturnFromServer result) {
				form.setAppHeaderBarAsPerStatus();
				form.setMenuAsPerStatus();
				form.showDialogMessage(successMessage);
				form.getTbStatus().setText(GRN.CANCELLED);
				form.setToViewState();
				form.hideWaitSymbol();
			}

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage(failureMessage);
				form.hideWaitSymbol();
			}
		});
	}
	public static void checkQuantity(final int poId,final int grnId){
		System.out.println("Inside search .............................");
		
		
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(poId);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new PurchaseOrder());
		
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				poList=new ArrayList<ProductQty>();
				ArrayList<ProductQty> mergedQty=new ArrayList<ProductQty>();
				HashSet<Integer> prodQtySet=new HashSet<Integer>();
				for (SuperModel model : result) {
					if(result.size()!=0){
						PurchaseOrder entity = (PurchaseOrder) model;
						for(int i=0;i<entity.getProductDetails().size();i++){
							if(warehouseName.trim().equals(entity.getProductDetails().get(i).getWarehouseName().trim())){
								System.out.println("WAREHOUSE NAME : "+warehouseName+" --- "+entity.getProductDetails().get(i).getWarehouseName());
								prodQtySet.add(entity.getProductDetails().get(i).getProductID());
								ProductQty pq=new ProductQty();
								pq.setProdId(entity.getProductDetails().get(i).getProductID());
								pq.setProdQty(entity.getProductDetails().get(i).getProductQuantity());
								mergedQty.add(pq);
	//							poList.add(pq);
							}
						}
					}
				}
				System.out.println("HS WH prod lis SIZE "+prodQtySet.size());
				System.out.println("PO prod lis Wh wise Size "+mergedQty.size());
				if(prodQtySet.size()<mergedQty.size()){
					for(Integer obj:prodQtySet){
						double sum=0;
						for(ProductQty qtyObj:mergedQty){
							if(qtyObj.getProdId()==obj){
								sum=sum+qtyObj.getProdQty();
							}
						}
						ProductQty pq=new ProductQty();
						pq.setProdId(obj);
						pq.setProdQty(sum);
						poList.add(pq);
					}
					System.out.println("MODIFIES PO LIST SIZE "+poList.size());
				}else{
					poList=mergedQty;
					System.out.println("UNCHANGED PO LIS SIZE "+poList.size());
				}
				
				
				MyQuerry querey1 = new MyQuerry();
				Company c=new Company();
				
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter filter = null;
				
				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("poNo");
				filter.setIntValue(poId);
				filtervec.add(filter);
				
				if(!warehouseName.equals("")){
					System.out.println("WH NOT NULL QUERRY");
					filter = new Filter();
					filter.setQuerryString("warehouseName");
					filter.setStringValue(warehouseName.trim());
					filtervec.add(filter);
				}
				
				querey1.setFilters(filtervec);
				querey1.setQuerryObject(new GRN());
				
				async.getSearchResult(querey1,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("On Failure !!!");
					}
		
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("GRN RESULT SIZE : "+result.size());
						grnList=new ArrayList<ProductQty>();
						double qty=0;
						boolean flage=false;
						for (SuperModel model1 : result) {
							if(result.size()!=0){
								GRN entity = (GRN) model1;
								if(entity.getCount()!=grnId){
								for(int i=0;i<entity.getInventoryProductItem().size();i++){
									if(!grnList.isEmpty()){
										for(int j=0;j<grnList.size();j++){
											if(grnList.get(j).getProdId()==entity.getInventoryProductItem().get(i).getProductID()
													&& entity.getInventoryProductItem().get(i).getStatus()==null){
												qty=grnList.get(j).getProdQty();
												qty=qty+entity.getInventoryProductItem().get(i).getProductQuantity();
												grnList.get(j).setProdQty(qty);
												flage=true;
											}
										}
										if(flage==false){
											if(entity.getInventoryProductItem().get(i).getStatus()==null){
											ProductQty pq=new ProductQty();
											pq.setProdId(entity.getInventoryProductItem().get(i).getProductID());
											pq.setProdQty(entity.getInventoryProductItem().get(i).getProductQuantity());
											grnList.add(pq);
											}
										}
									}
									else {
										if(entity.getInventoryProductItem().get(i).getStatus()==null){
											ProductQty pq=new ProductQty();
											pq.setProdId(entity.getInventoryProductItem().get(i).getProductID());
											pq.setProdQty(entity.getInventoryProductItem().get(i).getProductQuantity());
											grnList.add(pq);
										}
									
									}
								  }
								}
							}
							else{
								System.out.println("No Result Found !!!!");
							}
						}
						
						form.validateGrnQty(poList,grnList);
					}
				});
			}
		});
	}

	
	public void createGRN(List<GRNDetails> updList)
	{
		System.out.println("Entity Enter Here");
		GRN grnEntity=new GRN();
		grnEntity.setCompanyId(model.getCompanyId());
		if(model.getPoNo()!=0){
			grnEntity.setPoNo(model.getPoNo());
		}
		if(model.getPoTitle()!=null){
			grnEntity.setPoTitle(model.getPoTitle());
		}
		if(model.getPoCreationDate()!=null){
			grnEntity.setPoCreationDate(model.getPoCreationDate());
		}
		if(model.getProJectName()!=null){
			grnEntity.setProJectName(model.getProJectName());
		}
		if(model.getTitle()!=null){
			grnEntity.setTitle(model.getTitle());
		}
		if(model.getEmployee()!=null){
			grnEntity.setEmployee(model.getEmployee());
		}
		if(model.getGrnCreationDate()!=null){
			grnEntity.setGrnCreationDate(model.getGrnCreationDate());
		}
		grnEntity.setStatus(ConcreteBusinessProcess.CREATED);
		if(model.getBranch()!=null){
			grnEntity.setBranch(model.getBranch());
		}
		if(model.getGrnCategory()!=null){
			grnEntity.setGrnCategory(model.getGrnCategory());
		}
		if(model.getGrnType()!=null){
			grnEntity.setGrnType(model.getGrnType());
		}
		if(model.getGrnGroup()!=null){
			grnEntity.setGrnGroup(model.getGrnGroup());
		}
		if(model.getApproverName()!=null){
			grnEntity.setApproverName(model.getApproverName());
		}
		if(model.getRefNo()!=null){
			grnEntity.setRefNo(model.getRefNo());
		}
		if(model.getGrnReferenceDate()!=null){
			grnEntity.setGrnReferenceDate(model.getGrnReferenceDate());
		}
		if(model.getRemark()!=null){
			grnEntity.setRemark(model.getRemark());
		}
		if(model.getInventoryProductItem().size()!=0){
			grnEntity.setInventoryProductItem(updList);
		}
		if(model.getVendorInfo()!=null){
			grnEntity.setVendorInfo(model.getVendorInfo());
		}
		if(model.getInspectionRequired()!=null){
			grnEntity.setInspectionRequired(model.getInspectionRequired());
		}
		////////////////////
		if(model.getWarehouseName()!=null){
			grnEntity.setWarehouseName(model.getWarehouseName());
		}
		
		async.save(grnEntity,new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An unexpected error occurred");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
			}
		});
		
	}
	
	
	int mmnIndex=0;
	private void createNewMMNForSelectedGRN()
	{

		final List<GRNDetails> grnlist= new ArrayList<GRNDetails>(); 
		grnlist.addAll(model.getInventoryProductItem());
		
		for(int i=0;i<grnlist.size();i++)
		{
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("warehousename");
			filter.setStringValue(grnlist.get(i).getWarehouseLocation());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("storagelocation");
			filter.setStringValue(grnlist.get(i).getStorageLoc());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("storagebin");
			filter.setStringValue(grnlist.get(i).getStorageBin());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("prodid");
			filter.setIntValue(grnlist.get(i).getProductID());
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ProductInventoryViewDetails());
			
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					double avlQty = 0;
					if(mmnIndex!=0){
						mmnIndex=mmnIndex+1;
					}
					else{
						mmnIndex=0;
					}
					
					for(SuperModel model:result){
					
						ProductInventoryViewDetails prodInvViewEntity=(ProductInventoryViewDetails)model;
						avlQty=prodInvViewEntity.getAvailableqty();
					}
					
					final MaterialMovementNote mmn=new MaterialMovementNote();
					
					mmn.setMmnTitle("MMN OUT for GRN");
					mmn.setMmnDate(new Date());
					mmn.setMmnTransactionType("scrap");
					mmn.setTransDirection("OUT");
				    mmn.setMmnCategory("");
					mmn.setMmnType("");
					
					if(model.getBranch()!=null){
						mmn.setBranch(model.getBranch());
					}
					if(model.getEmployee()!=null){
						mmn.setMmnPersonResposible(model.getEmployee());
					}
					if(model.getApproverName()!=null){
						mmn.setApproverName(model.getApproverName());
					}
					if(model.getEmployee()!=null){
						mmn.setEmployee(model.getEmployee());
					}
					
					mmn.setStatus(MaterialMovementNote.CREATED);
					if(model.getDescription()!=null){
						mmn.setMmnDescription("Ref GRN -: "+model.getCount()+"\n"+model.getDescription());
					}
					List<MaterialProduct> mmnlis= new ArrayList<MaterialProduct>();
						
						MaterialProduct mmnentity=new MaterialProduct();
						
						mmnentity.setMaterialProductId(grnlist.get(mmnIndex).getProductID());
						mmnentity.setMaterialProductCode(grnlist.get(mmnIndex).getProductCode());
						mmnentity.setMaterialProductName(grnlist.get(mmnIndex).getProductName());
						mmnentity.setMaterialProductRequiredQuantity(grnlist.get(mmnIndex).getProductQuantity());
						mmnentity.setMaterialProductAvailableQuantity(avlQty);
						mmnentity.setMaterialProductUOM(grnlist.get(mmnIndex).getUnitOfmeasurement());
						mmnentity.setMaterialProductRemarks("");
						mmnentity.setMaterialProductWarehouse(grnlist.get(mmnIndex).getWarehouseLocation());
						mmnentity.setMaterialProductStorageLocation(grnlist.get(mmnIndex).getStorageLoc());
						mmnentity.setMaterialProductStorageBin(grnlist.get(mmnIndex).getStorageBin());
						mmnlis.add(mmnentity);
						mmn.setSubProductTableMmn(mmnlis);
						mmn.setCompanyId(model.getCompanyId());
				
						async.save(mmn,new AsyncCallback<ReturnFromServer>() {
							
						@Override
						public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
						
						}
						
						@Override
						public void onSuccess(ReturnFromServer result) {
							
			    		}
					});
			
				}
			});
	}
	}
	
	
	
	private void changeStatus() 
	{
		 model.setStatus(GRN.CANCELLED);
		 model.setDescription("GRN ID ="+model.getCount()+" "+"GRN status ="+form.getTbStatus().getValue().trim()+"\n"
					+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark."+"\n"
					+"Remark ="+popup.getRemark().getValue().trim()+" "+"Cancellation Date ="+new Date());
    	  async.save(model,new AsyncCallback<ReturnFromServer>() {
				
				@Override
				public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
				
				}
				
				@Override
				public void onSuccess(ReturnFromServer result) {
					form.getstatustextbox().setValue(model.getStatus());
					form.getTaDescription().setValue(model.getDescription());
					form.setAppHeaderBarAsPerStatus();
					form.setToViewState();
					checkInspectionStatus(model.getCount());
					/**
					 * Date 20-12-2018 By Vijay
					 * Des :- NBHC Inventory Management changing PR Status to closed
					 */
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "EnableAutomaticPROrderQty")){
						 updatePRStatusToClosed(model);
					}
					/**
					  * ends here
					  */
	    		}
			});
	}
	
	
	/**
	 * Date 20-12-2018 By Vijay
	 * Des :- NBHC Inventory Management changing PR Status to closed
	 */
	private void updatePRStatusToClosed(GRN model) {
		GeneralServiceAsync genService = GWT.create(GeneralService.class);
		genService.updatePRStatus(model.getPrNumber(), new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	 /**
	  * ends here
	  */
	
	/**
	 * This is used to cancel GRN in approved status and revert stock
	 * Date : 13/02/2017
	 */
	
	
	private void cancelGRN() {
//		/*
//		 * Did changes by Rahul Verma on 19 June 2017
//		 * 
//		 */
//		String cancelText="has been cancelled by ";
//		if(text.equals(AppConstants.PARTIALCANCEL)){
//			model.setPartialCancellationFlag(true);
//			cancelText="has been partially cancelled by ";
//		}else{
//			model.setPartialCancellationFlag(false);
//		}
//		model.setStatus(MaterialIssueNote.CANCELLED);
//		model.setDescription("GRN ID ="+model.getCount()+" "+"GRN status ="+form.getTbStatus().getValue().trim()+"\n"
//				+cancelText+LoginPresenter.loggedInUser+" with remark "+"\n"
//				+"Remark ="+popup.getRemark().getValue().trim()+" "+"Cancellation Date ="+AppUtility.parseDate(new Date()));
//		form.showWaitSymbol();
//		updateService.cancelDocument(model, new AsyncCallback<String>() {
//			@Override
//			public void onFailure(Throwable caught) {
//				form.hideWaitSymbol();
//				form.showDialogMessage("An Unexpected Error occured !");
//			}
//			@Override
//			public void onSuccess(String result) {
//				form.hideWaitSymbol();
//				if(result.equalsIgnoreCase("GRN Cancelled Successfully.")){
//					form.showDialogMessage(result);
//					form.getTbStatus().setText(model.getStatus());
//					form.getTaDescription().setValue(model.getDescription());
//				}else{
//					form.showDialogMessage(result);
//					model.setStatus(MaterialIssueNote.APPROVED);
//				}
//				form.setToViewState();
//			}
//		});
//		
		

		/*
		 * Did changes by Rahul Verma on 19 June 2017
		 * 
		 */
		String cancelText="has been cancelled by ";
		if(text.equals(AppConstants.PARTIALCANCEL)){
			model.setPartialCancellationFlag(true);
			cancelText="has been partially cancelled by ";
		}else{
			model.setPartialCancellationFlag(false);
		}
		model.setStatus(MaterialIssueNote.CANCELLED);
		model.setDescription("GRN ID ="+model.getCount()+" "+"GRN status ="+form.getTbStatus().getValue().trim()+"\n"
				+cancelText+LoginPresenter.loggedInUser+" with remark "+"\n"
				+"Remark ="+popup.getRemark().getValue().trim()+" "+"Cancellation Date ="+AppUtility.parseDate(new Date()));
		form.showWaitSymbol();
		updateService.cancelDocument(model, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("An Unexpected Error occured !");
			}
			@Override
			public void onSuccess(String result) {
				form.hideWaitSymbol();
				if(result.equalsIgnoreCase("GRN Cancelled Successfully.")){
					form.showDialogMessage(result);
					form.getTbStatus().setText(model.getStatus());
					form.getTaDescription().setValue(model.getDescription());
					/** Date 13-04-2019 by Vijay partial grn changed with new logic updated so below code commented ***/
//					if(!text.equals(AppConstants.PARTIALCANCEL)){
						form.setToViewState();
//					}else{
//						createNewGrn(model);
//					}
				}else{
					form.showDialogMessage(result);
					model.setStatus(MaterialIssueNote.APPROVED);
				}
				
				
			}
		});
		
	
	}
	
	/**
	 * Rahul Added this for NBHC partial cancellation
	 * This will create new GRN form against same PO.
	 * @param model
	 * @param grnMaterialList 
	 */
	protected void createNewGrn(final GRN model, ArrayList<GRNDetails> grnMaterialList) {

		System.out.println("Creating new GRN!!!!");
		GRN grnEntity=new GRN();
		grnEntity.setCompanyId(model.getCompanyId());
		grnEntity.setPartialCancellationFlag(false);
		if(model.getPoNo()!=0){
			grnEntity.setPoNo(model.getPoNo());
		}
		if(model.getPoTitle()!=null){
			grnEntity.setPoTitle(model.getPoTitle());
		}
		if(model.getPoCreationDate()!=null){
			grnEntity.setPoCreationDate(model.getPoCreationDate());
		}
		if(model.getProJectName()!=null){
			grnEntity.setProJectName(model.getProJectName());
		}
		if(model.getTitle()!=null){
			grnEntity.setTitle(model.getTitle());
		}
		if(model.getEmployee()!=null){
			grnEntity.setEmployee(model.getEmployee());
		}
		if(model.getGrnCreationDate()!=null){
			grnEntity.setGrnCreationDate(model.getGrnCreationDate());
		}
		grnEntity.setStatus(ConcreteBusinessProcess.CREATED);
		if(model.getBranch()!=null){
			grnEntity.setBranch(model.getBranch());
		}
		if(model.getGrnCategory()!=null){
			grnEntity.setGrnCategory(model.getGrnCategory());
		}
		if(model.getGrnType()!=null){
			grnEntity.setGrnType(model.getGrnType());
		}
		if(model.getGrnGroup()!=null){
			grnEntity.setGrnGroup(model.getGrnGroup());
		}
		if(model.getApproverName()!=null){
			grnEntity.setApproverName(model.getApproverName());
		}
		if(model.getRefNo()!=null){
			grnEntity.setRefNo(model.getRefNo());
		}
		if(model.getGrnReferenceDate()!=null){
			grnEntity.setGrnReferenceDate(model.getGrnReferenceDate());
		}
		if(model.getRemark()!=null){
			grnEntity.setRemark(model.getRemark());
		}
//		if(model.getInventoryProductItem().size()!=0){
//			grnEntity.setInventoryProductItem(model.getInventoryProductItem());
//		}
		if(model.getVendorInfo()!=null){
			grnEntity.setVendorInfo(model.getVendorInfo());
		}
		if(model.getInspectionRequired()!=null){
			grnEntity.setInspectionRequired(model.getInspectionRequired());
		}
		////////////////////
		if(model.getWarehouseName()!=null){
			grnEntity.setWarehouseName(model.getWarehouseName());
		}
		
		if(model.getRefNo2()!=null){
			grnEntity.setRefNo2(model.getRefNo2());
		}
		grnEntity.setPartialCancellationFlag(true);
		
		for(GRNDetails grndetails : grnMaterialList){
			grndetails.setRecordSelect(false);
			grndetails.setStatus("");
		}
		grnEntity.setInventoryProductItem(grnMaterialList);
		
		
		async.save(grnEntity,new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				
//				final String refNo2=result.count+"";
//				System.out.println("Count Got of GRN:::::::::"+refNo2);
				
//				model.setRefNo2(refNo2);
//				async.save(model,new AsyncCallback<ReturnFromServer>() {
//					
//					@Override
//					public void onFailure(Throwable caught) {
//						form.showDialogMessage("An Unexpected Error occured !");
//					}
//					@Override
//					public void onSuccess(ReturnFromServer result) {
////						form.tbrefNo2.setValue(refNo2);
//						form.setToViewState();
//					}
//				});
			}
		});
	
	}
	
	private void checkInspectionStatus(int cnt)
	{
		
		final int grnId =cnt;
		final String grnStatus=model.getStatus();
		
	MyQuerry querry = new MyQuerry();
  	Company c = new Company();
  	Vector<Filter> filtervec=new Vector<Filter>();
  	Filter filter = null;
  	filter = new Filter();
  	filter.setQuerryString("companyId");
	filter.setLongValue(c.getCompanyId());
	filtervec.add(filter);
	filter = new Filter();
	filter.setQuerryString("inspectionGrnId");
	filter.setIntValue(cnt);
	filtervec.add(filter);
	querry.setFilters(filtervec);
	querry.setQuerryObject(new Inspection());
	
	async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		
		@Override
		public void onFailure(Throwable caught) {
		}

		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			for(SuperModel model:result)
			{
				

					final Inspection inspectionEntity = (Inspection)model;
					
					if(inspectionEntity.getStatus().trim().equals(Inspection.CREATED)){
					
					inspectionEntity.setStatus(Inspection.CANCELLED);
					inspectionEntity.setInspectionDescription("GRN ID ="+grnId+" "+"GRN status ="+grnStatus+"\n"+
			   "has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+popup.getRemark().getValue().trim());
				
				async.save(inspectionEntity,new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
					}
					@Override
					public void onSuccess(ReturnFromServer result) {
					}
				});
				  }
					
					if(inspectionEntity.getStatus().trim().equals(Inspection.REJECTED)){
  					
  					inspectionEntity.setStatus(Inspection.CANCELLED);
  					inspectionEntity.setInspectionDescription("GRN ID ="+grnId+" "+"GRN status ="+grnStatus+"\n"+
  						   "has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+popup.getRemark().getValue().trim());
					
					async.save(inspectionEntity,new AsyncCallback<ReturnFromServer>() {
						
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
						}
					});
  				  }
					
					if(inspectionEntity.getStatus().trim().equals(Inspection.APPROVED)){
  					
  					inspectionEntity.setStatus(Inspection.CANCELLED);
  					inspectionEntity.setInspectionDescription("GRN ID ="+grnId+" "+"GRN status ="+grnStatus+"\n"+
  						   "has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+popup.getRemark().getValue().trim());
					
					async.save(inspectionEntity,new AsyncCallback<ReturnFromServer>() {
						
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
						}
					});
  				  }
					
					if(inspectionEntity.getStatus().trim().equals(Inspection.REQUESTED)){
  					
  					inspectionEntity.setStatus(Inspection.CANCELLED);
  					inspectionEntity.setInspectionDescription("GRN ID ="+grnId+" "+"GRN status ="+grnStatus+"\n"+
  						   "has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+popup.getRemark().getValue().trim());
					
					async.save(inspectionEntity,new AsyncCallback<ReturnFromServer>() {
						
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
						}
					});
  				  
					MyQuerry querry = new MyQuerry();
				  	Company c = new Company();
				  	Vector<Filter> filtervec=new Vector<Filter>();
				  	Filter filter = null;
				  	filter = new Filter();
				  	filter.setQuerryString("companyId");
					filter.setLongValue(c.getCompanyId());
					filtervec.add(filter);
					filter = new Filter();
					filter.setQuerryString("businessprocessId");
					filter.setIntValue(inspectionEntity.getCount());
					filtervec.add(filter);
					filter = new Filter();
					filter.setQuerryString("businessprocesstype");
					filter.setStringValue(ApproverFactory.INSPECTION);
					filtervec.add(filter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Approvals());
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			  			
			  			@Override
			  			public void onFailure(Throwable caught) {
			  			}

			  			@Override
						public void onSuccess(ArrayList<SuperModel> result) {
			  				
			  				for(SuperModel smodel:result)
							{
			  					Approvals approval = (Approvals)smodel;
			  					
			  					approval.setStatus(Approvals.CANCELLED);
			  					inspectionEntity.setInspectionDescription("GRN ID ="+grnId+" "+"GRN status ="+grnStatus+"\n"+
			  						   "has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+popup.getRemark().getValue().trim());
								
								async.save(approval,new AsyncCallback<ReturnFromServer>() {
									
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
									}
								});
							}
			  			}
			  			});
					
					}
					
					
			
				
	
			}
		}
	});
	}

	
	private void cancelApprovalLogic()
	{
		form.showWaitSymbol(); 
		ApproverFactory approverfactory=new ApproverFactory();
		
		  Filter filter=new Filter();
		  filter.setStringValue(approverfactory.getBuisnessProcessType());
		  filter.setQuerryString("businessprocesstype");
		  
		  Filter filterOnid=new Filter();
		  filterOnid.setIntValue(model.getCount());
		  filterOnid.setQuerryString("businessprocessId");
		  
		  Filter filterOnStatus=new Filter();
		  filterOnStatus.setStringValue(Approvals.PENDING);
		  filterOnStatus.setQuerryString("status");
		  
		  
		  Vector<Filter> vecfilter=new Vector<Filter>();
		  vecfilter.add(filter);
		  vecfilter.add(filterOnid);
		  vecfilter.add(filterOnStatus);
		  
		  MyQuerry querry=new MyQuerry(vecfilter, new Approvals());
		  
		  final ArrayList<SuperModel> array = new ArrayList<SuperModel>();
		  
		  service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("CANCEL APPROVAL RESULT SIZE :: "+result.size());
				array.addAll(result);
				Approvals approval=(Approvals) array.get(0);
				approval.setStatus(Approvals.CANCELLED);
				 
				saveApprovalReq(approval, ConcreteBusinessProcess.CREATED, "Approval Request Cancelled!");
			}
		  });
	}
	
	private void saveApprovalReq(Approvals approval,final String status,final String dialogmsg)
	{
		service.save(approval, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) 
			{
				
			//  added by vijay  for when grn cancelled request for approval then its status is coming in created and search same record then
							// its showing its status inspection approved  to solve this this issue added if else condition
							
			if(model.getInspectionRequired().trim().equals(AppConstants.YES))
			{
				model.setStatus(GRN.INSPECTIONAPPROVED);
			}
			else
			{
				model.setStatus(GRN.CREATED);
			}
							
//				model.setStatus(GRN.INSPECTIONAPPROVED);
				service.save(model, new AsyncCallback<ReturnFromServer>() {

					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(ReturnFromServer result) {
						form.getTbStatus().setValue(status);
						form.setToViewState();
						
//						Timer t = new Timer() {
//							@Override
//							public void run() {
								form.showDialogMessage(dialogmsg);
								form.hideWaitSymbol();
//							}
//						};
//						t.schedule(1500);
						
						
						if(model.getInspectionRequired().trim().equals(AppConstants.YES))
						{
							form.getTbStatus().setValue(GRN.INSPECTIONAPPROVED);
						}
						else
						{
							form.getTbStatus().setValue(GRN.CREATED);
						}
						form.setMenuAsPerStatus();
					}
				});
			}
			
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("Request Failed! Try Again!");
				form.hideWaitSymbol();
			}
		});
	}
	
	
	private void updateInspectionDetails() {
		
		form.showWaitSymbol();
		inspectionList=new ArrayList<InspectionDetails>();
		final GenricServiceAsync genasync = GWT.create(GenricService.class);
		
		for(int i=0;i<model.getInventoryProductItem().size();i++){
			
			final int prodId=model.getInventoryProductItem().get(i).getProductID();
			final MyQuerry querry = new MyQuerry();
			Company c=new Company();
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			filter = new Filter();
			filter.setQuerryString("productinfo.prodID");
			filter.setIntValue(prodId);
			filtervec.add(filter);
			
			filter=new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			querry.setFilters(filtervec);
//			form.showWaitSymbol();
			querry.setQuerryObject(new ProductInventoryView());
				genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						form.showDialogMessage("An GRN Presenter Unexpected error occurred!");
					}
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for (SuperModel model1 : result) {
							ProductInventoryView piv = (ProductInventoryView) model1;
							
							if(!piv.getInspectionRequired().equals("")&&piv.getInspectionRequired()!=null){
									for(int i=0;i<model.getInventoryProductItem().size();i++){
										if(model.getInventoryProductItem().get(i).getProductID()==prodId){
											System.out.println("11111");
											InspectionDetails poproducts=new InspectionDetails();
											
											if(model.getInventoryProductItem().get(i).getProductQuantity()!=0){
												poproducts.setProductID(model.getInventoryProductItem().get(i).getProductID());
												poproducts.setProductCategory(model.getInventoryProductItem().get(i).getProductCategory());
												poproducts.setProductCode(model.getInventoryProductItem().get(i).getProductCode());
												poproducts.setProductName(model.getInventoryProductItem().get(i).getProductName());
												poproducts.setUnitOfmeasurement(model.getInventoryProductItem().get(i).getUnitOfmeasurement());
												poproducts.setProdPrice(model.getInventoryProductItem().get(i).getProdPrice());
												poproducts.setTax(model.getInventoryProductItem().get(i).getTax());
												poproducts.setVat(model.getInventoryProductItem().get(i).getVat());
												poproducts.setAcceptedQty(model.getInventoryProductItem().get(i).getProductQuantity());
												poproducts.setProductQuantity(model.getInventoryProductItem().get(i).getProductQuantity());
												poproducts.setWarehouseLocation(model.getInventoryProductItem().get(i).getWarehouseLocation());
												poproducts.setStorageLoc(model.getInventoryProductItem().get(i).getStorageLoc());
												poproducts.setStorageBin(model.getInventoryProductItem().get(i).getStorageBin());
												poproducts.setInspectionRequired(piv.getInspectionRequired());
												poproducts.setReasonForReject("");
												inspectionList.add(poproducts);
											}
										}
									}
							}
							else{
									for(int i=0;i<model.getInventoryProductItem().size();i++){
										if(model.getInventoryProductItem().get(i).getProductID()==prodId){
											InspectionDetails poproducts=new InspectionDetails();
											if(model.getInventoryProductItem().get(i).getProductQuantity()!=0){
												System.out.println("null 2222");
												poproducts.setProductID(model.getInventoryProductItem().get(i).getProductID());
												poproducts.setProductCategory(model.getInventoryProductItem().get(i).getProductCategory());
												poproducts.setProductCode(model.getInventoryProductItem().get(i).getProductCode());
												poproducts.setProductName(model.getInventoryProductItem().get(i).getProductName());
												poproducts.setUnitOfmeasurement(model.getInventoryProductItem().get(i).getUnitOfmeasurement());
												poproducts.setProdPrice(model.getInventoryProductItem().get(i).getProdPrice());
												poproducts.setTax(model.getInventoryProductItem().get(i).getTax());
												poproducts.setVat(model.getInventoryProductItem().get(i).getVat());
												poproducts.setAcceptedQty(model.getInventoryProductItem().get(i).getProductQuantity());
												poproducts.setProductQuantity(model.getInventoryProductItem().get(i).getProductQuantity());
												poproducts.setWarehouseLocation(model.getInventoryProductItem().get(i).getWarehouseLocation());
												poproducts.setStorageLoc(model.getInventoryProductItem().get(i).getStorageLoc());
												poproducts.setStorageBin(model.getInventoryProductItem().get(i).getStorageBin());
												poproducts.setInspectionRequired(AppConstants.NO);
												poproducts.setReasonForReject("");
												inspectionList.add(poproducts);
											}
										}
									}
							}
							
						}
						rowIndex=rowIndex+1;
						if(rowIndex==model.getInventoryProductItem().size()){
//							form.hideWaitSymbol();
							createInspection(inspectionList);
						}
						form.hideWaitSymbol();
					}
				});
		}
	}
	
	
	public void createInspection(ArrayList<InspectionDetails> list){
		System.out.println("Entity Enter Here");
//		form.hideWaitSymbol();
		Inspection inspectionEntity=new Inspection();
		
		inspectionEntity.setCompanyId(model.getCompanyId());
		if(model.getCount()!=0){
			inspectionEntity.setInspectionGrnId(model.getCount());
		}
		if(model.getTitle()!=null){
			inspectionEntity.setInspectionGrnTitle(model.getTitle());
		}
		if(model.getCreationDate()!=null){
			inspectionEntity.setInspectionGrnDate(model.getCreationDate());
		}
		if(model.getProJectName()!=null){
			inspectionEntity.setInspectionProject(model.getProJectName());
		}
		if(model.getRefNo()!=null){
			inspectionEntity.setInspectionRefNo(model.getRefNo());
		}
		if(model.getGrnReferenceDate()!=null){
			inspectionEntity.setInspectionRefDate(model.getGrnReferenceDate());
		}
		if(model.getPoNo()!=0){
			inspectionEntity.setInspectionPOid(model.getPoNo());
		}
		if(model.getTitle()!=null){
			inspectionEntity.setInspectionTitle(model.getTitle());
		}
		if(model.getCreationDate()!=null){
			inspectionEntity.setCreationDate(new Date());
		}
		if(model.getBranch()!=null){
			inspectionEntity.setBranch(model.getBranch());
		}
		if(model.getEmployee()!=null){
			inspectionEntity.setEmployee(model.getEmployee());
		}
		if(model.getApproverName()!=null){
			inspectionEntity.setApproverName(model.getApproverName());
		}
		inspectionEntity.setStatus(ConcreteBusinessProcess.CREATED);
		
		if(model.getRemark()!=null){
			inspectionEntity.setRemark(model.getRemark());
		}
		
		
		if(model.getInventoryProductItem().size()!=0){
			inspectionEntity.setItems(list);
		}
		if(model.getVendorInfo()!=null){
			inspectionEntity.setVendorInfo(model.getVendorInfo());
		}
		async.save(inspectionEntity,new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected error occurred!");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				model.setInspectionId(result.count);
				model.setStatus(GRN.INSPECTIONREQUESTED);
				async.save(model,new AsyncCallback<ReturnFromServer>() {
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						form.showDialogMessage("An Unexpected error occurred!");
					}
					@Override
					public void onSuccess(ReturnFromServer result) {
						form.showDialogMessage("Inspection Requested.");
						form.hideWaitSymbol();
						form.tbStatus.setValue(model.getStatus());
						form.tbinspectionId.setValue(model.getInspectionId()+"");
						form.setMenuAsPerStatus();
					}
				});
			}
		});
		
//		form.hideWaitSymbol();
	}
	
	/** date 3.10.2018 added by komal**/
	private void reactOnCreateSalesOrder(){
		MyQuerry querry = new MyQuerry();
		Vector<Filter> temp = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("grnId");
		filter.setIntValue(model.getCount());
		temp.add(filter);

		querry.setFilters(temp);
		querry.setQuerryObject(new SalesOrder());
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(final ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Sales Order",Screen.SALESORDER);
					final SalesOrderForm form=SalesOrderPresenter.initalize();
					form.showWaitSymbol();
					Timer t = new Timer() {
					      @Override
					      public void run() {
					    	  if(result.size() == 0){
					    	  form.setToNewState();
					    	  form.hideWaitSymbol();
					    	  if(model.getPoNo()!=0){
					    		  form.getTbPOId().setValue(model.getPoNo()+"");
					    	  }
					    	  form.getTbGRNId().setValue(model.getCount()+"");
					    	  form.getOlbbBranch().setValue(model.getBranch());
					    	  form.getOlbApproverName().setValue(model.getApproverName());
					    	  form.getDbsalesorderdate().setValue(new Date());
					    	  MyQuerry quer = getProductQuerry();
					    	  async.getSearchResult(quer, new AsyncCallback<ArrayList<SuperModel>>() {

								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									
								}

								@Override
								public void onSuccess(
										ArrayList<SuperModel> result) {
									// TODO Auto-generated method stub
									Map<Integer ,ItemProduct> productMap = new HashMap<Integer , ItemProduct>();
									for(SuperModel model : result){
										ItemProduct product = (ItemProduct) model;
										productMap.put(product.getCount(), product);
									}
									 List<SalesLineItem> productList = getProductForSalesOrder(productMap);
							    	  if(productList.size() > 0){
							    		  form.getSalesorderlineitemtable().getDataprovider().getList().addAll(productList);

							    	  }								
								}
							});
					    }else{
					    		  SalesOrder order = (SalesOrder) result.get(0);
					    		  form.updateView(order);
					    		  form.hideWaitSymbol();
					    	  }
					      }
					    };
					    t.schedule(2000);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub

			}
		});

	}
	private List<SalesLineItem> getProductForSalesOrder(Map<Integer ,ItemProduct> prodcutMap){
		List<SalesLineItem> itemList = new ArrayList<SalesLineItem>();
		SalesLineItem salesLineItem = null;
		for(GRNDetails grnDetails : form.getTable().getDataprovider().getList()){
			salesLineItem = new SalesLineItem();
			ItemProduct product = prodcutMap.get(grnDetails.getProductID());
			if(product !=null){
				salesLineItem.setPrduct(product);
				salesLineItem.setVatTax(product.getVatTax());
				salesLineItem.setServiceTax(product.getServiceTax());
				salesLineItem.setPrice(product.getPrice());
				salesLineItem.setTotalAmount(grnDetails.getProductQuantity() * product.getPrice());
			
			salesLineItem.setProductCode(grnDetails.getProductCode());
			salesLineItem.setProductName(grnDetails.getProductName());
			salesLineItem.setQuantity(grnDetails.getProductQuantity());
			salesLineItem.setProductCategory(grnDetails.getProductCategory());
			if(grnDetails.getUnitOfmeasurement()!=null){
				salesLineItem.setUnitOfMeasurement(grnDetails.getUnitOfmeasurement());
			}
			
			itemList.add(salesLineItem);
			}
		}
		
		return itemList;
	}
	private MyQuerry getProductQuerry(){
		List<String> prodIdList = new ArrayList<String>();
		for(GRNDetails grnDetails : form.getTable().getDataprovider().getList()){
			prodIdList.add(grnDetails.getProductCode());
		}
	
		MyQuerry querry = new MyQuerry();
		Vector<Filter> temp = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("productCode IN");
		filter.setList(prodIdList);
		temp.add(filter);

		querry.setFilters(temp);
		querry.setQuerryObject(new ItemProduct());
		return querry;
	}

	
	private void updateGRN(final GRN model, final ArrayList<GRNDetails> grnMaterialList) {
			form.showWaitSymbol();
			for(GRNDetails grnMaterial : model.getInventoryProductItem()){
				for(GRNDetails selectedgrnMaterial :grnMaterialList){
					if(grnMaterial.getProductID()==selectedgrnMaterial.getProductID()){
						grnMaterial.setStatus(GRN.CANCELLED);
						String cancellationRemark = "Product ID ="+grnMaterial.getProductID()+""+"has been partially cancelled by"+
								LoginPresenter.loggedInUser+"Cancellation Date ="+AppUtility.parseDate(new Date());
						grnMaterial.setPartialCancellationRemark(cancellationRemark);
						System.out.println("Selected record");
					}
				}
			}
			
			async.save(model,new AsyncCallback<ReturnFromServer>() {
				
				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("An Unexpected Error occured !");
					form.hideWaitSymbol();
				}
				@Override
				public void onSuccess(ReturnFromServer result) {
					form.setToViewState();
					form.showDialogMessage("Partial GRN Cancelled Sucessfully");
					form.hideWaitSymbol();
					partialGRNCancellation.hidePopUp();
					form.getTable().getTable().redraw();
					partialGRNCancellation(model,grnMaterialList);
					
				}
			});
			
		
			
		
	}

	protected void partialGRNCancellation(final GRN model, final ArrayList<GRNDetails> grnMaterialList) {
		
		updateService.partialGRNCancellation(model, new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				createNewGrn(model,grnMaterialList);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
}
