package com.slicktechnologies.client.views.inventory.recievingnote;

public class ProductWarehouse {
	protected int prodId;
	protected String warehouseName;
	
	
	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	public String getWarehouseName() {
		return warehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}
	

}
