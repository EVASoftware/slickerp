package com.slicktechnologies.client.views.inventory.recievingnote;

public class ProductQty {
	
	protected int prodId;
	protected double prodQty;
	protected String status;
	
	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	public double getProdQty() {
		return prodQty;
	}
	public void setProdQty(double prodQty) {
		this.prodQty = prodQty;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
