package com.slicktechnologies.client.views.inventory.recievingnote;

import java.util.Date;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.deliverynote.ShippingSteps;

public class InspectionStepsTable extends SuperTable<ShippingSteps> {
	
	TextColumn<ShippingSteps> getstepDetailsNumberColumn;
	TextColumn<ShippingSteps> getStepDetailsCodeColumn;
	TextColumn<ShippingSteps> getStepDetailsNameColumn;
	TextColumn<ShippingSteps> getStepDetailsMandatoryColumn;
	private Column<ShippingSteps, Boolean> completionStatus;
	TextColumn<ShippingSteps> getCompStatusColumn;
	Column<ShippingSteps,String> deleteColumn;
	
	public InspectionStepsTable() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetStepNumber();
		addColumngetStepsCode();
		addColumngetStepName();
		addColumngetStepMandatory();
		addColumnCheckBox();
		createColumndeleteColumn();
		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	
	protected void addColumngetStepNumber()
	{
		getstepDetailsNumberColumn=new TextColumn<ShippingSteps>()
				{
			@Override
			public String getValue(ShippingSteps object)
			{
				if( object.getShippingStepNumber()==0)
					return "";
				else return object.getShippingStepNumber()+"";
			}
				};
		table.addColumn(getstepDetailsNumberColumn,"Step Number");
	}
	
	protected void addColumngetStepsCode()
	{
		getStepDetailsCodeColumn=new TextColumn<ShippingSteps>()
				{
			@Override
			public String getValue(ShippingSteps object)
			{
				return object.getShippingStepCode();
			}
				};
		table.addColumn(getStepDetailsCodeColumn,"Step Code");
	}
	
	protected void addColumngetStepName()
	{
		getStepDetailsNameColumn=new TextColumn<ShippingSteps>()
				{
			@Override
			public String getValue(ShippingSteps object)
			{
				return object.getShippingStepName();
			}
				};
		table.addColumn(getStepDetailsNameColumn,"Step Name");
	}
	
	protected void addColumngetStepMandatory()
	{
		getStepDetailsMandatoryColumn=new TextColumn<ShippingSteps>()
				{
			@Override
			public String getValue(ShippingSteps object)
			{
				return object.getShippingStepMandatory();
			}
				};
		table.addColumn(getStepDetailsMandatoryColumn,"Is Mandatory");
	}
	
	private void addColumnCheckBox()
	{
		CheckboxCell checkCell= new CheckboxCell();
		completionStatus = new Column<ShippingSteps, Boolean>(checkCell) {

			@Override
			public Boolean getValue(ShippingSteps object) {
				
				return object.isCompletionStatus();
			}
		};
		table.addColumn(completionStatus,"Completion Status");
	}

	private void setFieldUpdaterOnCheckBox()
	{
		completionStatus.setFieldUpdater(new FieldUpdater<ShippingSteps, Boolean>() {
			
			@Override
			public void update(int index, ShippingSteps object, Boolean value) {
				try{
					Boolean val1=(value);
					object.setCompletionStatus(val1);
				}
				catch(Exception e)
				{
				}
				table.redrawRow(index);
			
			}
		});
	}
	
	protected void addColumngetViewCompStatus()
	{
		getCompStatusColumn=new TextColumn<ShippingSteps>()
				{
			@Override
			public String getValue(ShippingSteps object)
			{
				if(object.isCompletionStatus()==true){
					return "YES";
				}
				else{
					return "NO";
				}
				
			}
				};
		table.addColumn(getCompStatusColumn,"Completion Status");
	}
	
	
	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<ShippingSteps,String>(btnCell)
				{
			@Override
			public String getValue(ShippingSteps object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
	}
	
	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<ShippingSteps,String>()
				{
			@Override
			public void update(int index,ShippingSteps object,String value)
			{
				getDataprovider().getList().remove(object);

				table.redrawRow(index);
			}
				});
	}
	

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<ShippingSteps>() {
			@Override
			public Object getKey(ShippingSteps item) {
				if(item==null)
					return null;
				else
					return new Date().getTime();
			}
		};
	
	}

	@Override
	public void addFieldUpdater() {
		setFieldUpdaterOnCheckBox();
		createFieldUpdaterdeleteColumn();
	}


	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setEnable(boolean state) {
		 for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
	        	  
	      
	          if(state==true)
	          {
	        	addColumngetStepNumber();
	      		addColumngetStepsCode();
	      		addColumngetStepName();
	      		addColumngetStepMandatory();
	      		addColumnCheckBox();
	      		createColumndeleteColumn();
	      		addFieldUpdater();
	      	 }
	          else
	          {
	        	  addColumngetStepNumber();
		      		addColumngetStepsCode();
		      		addColumngetStepName();
		      		addColumngetStepMandatory();
	        	  addColumngetViewCompStatus();
	          }
	}



}
