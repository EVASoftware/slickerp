package com.slicktechnologies.client.views.inventory.recievingnote;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.RowCountChangeEvent;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Vector;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.google.gwt.cell.client.FieldUpdater;
import java.util.List;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextInputCell;
import com.google.gwt.core.client.GWT;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;

public class GRNDetailsTable extends SuperTable<GRNDetails> {
	
	TextColumn<GRNDetails> getPoQtyColumn;
    TextColumn<GRNDetails> getReceivedQtyColumn;
    TextColumn<GRNDetails> getRejectedQtyColumn;
	TextColumn<GRNDetails> getTotalColumn;
	TextColumn<GRNDetails> getLBTColumn;
	TextColumn<GRNDetails> getVATColumn;
	TextColumn<GRNDetails> getWareHouseLocationNameColumn;
	TextColumn<GRNDetails> getProductCategoryColumn;
	TextColumn<GRNDetails> getProductCodeColumn;
	TextColumn<GRNDetails> getProductNameColumn;
	TextColumn<GRNDetails> getUnitOfMeasurmentColumn;
	TextColumn<GRNDetails> getPriceColumn;
	TextColumn<GRNDetails> getCountColumn;
	
    
	Column<GRNDetails,String> editPoQtyColumn;
    Column<GRNDetails,String> editReceivedQtyColumn;
	Column<GRNDetails,String> editPriceColumn;
	Column<GRNDetails,String> deleteColumn;
	public ArrayList<String> binList;
	private Column<GRNDetails, String> storagebin;
	public ArrayList<String> storagelocList;
	private Column<GRNDetails, String> storageLoc;
	

	
	public GRNDetailsTable()
	{
		super();
		binList=new ArrayList<String>();
	}
	@Override public void createTable() {
		
		addColumngetCount();
		addColumngetWareHouseLocationName();
		addColumngetProductCategory();
		addColumngetProductCode();
		addColumngetProductName();
		addColumngetUnitOfMeasurment();
		addColumnEditPrice();
		addColumngetLBT();
		addColumngetVAT();
		addColumnEditPoQty();
		addColumnEditRecievedQty();
		addColumngetRejectedQty();
		addColumngetTotal();
		retriveStorageBin();
	//	retriveStorageLoc();
	//	createColumndeleteColumn();
	//	addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);	
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<GRNDetails>()
				{
			@Override
			public Object getKey(GRNDetails item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
		};
	}
	
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetWareHouseLocationName();
		addSortinggetProductCategory();
		addSortinggetProductCode();
		addSortinggetProductName();
		addSortinggetUnitOfMeasurment();
		if(this.getPriceColumn!=null)
		   addSortinggetPrice();
		addSortinggetLBT();
		addSortinggetVAT();
		if(this.getPoQtyColumn!=null)
		    addSortinggetPoQty();
		if(this.getReceivedQtyColumn!=null)
		           addSortinggetReceivedQty();
		
		addSortinggetEditPoQty();
		addSortinggetEditRecievedQty();
		addSortinggetEditPrice();
		
	}
	
	
	
	
	/**************storage location*********************************/
	
	public void retriveStorageLoc()
	{
		final GenricServiceAsync service=GWT.create(GenricService.class);
		Vector<Filter>filtervalue=new Vector<Filter>(); 
		MyQuerry query=new MyQuerry();
		query.setFilters(filtervalue);
		query.setQuerryObject(new StorageLocation());
		System.out.println("service"+service);
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			
		@Override
		public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				for(SuperModel model:result)
				{
				 StorageLocation entity=(StorageLocation) model;
				 storagelocList.add(entity.getBusinessUnitName());
				}
				
				addEditColumnStorageLocation();
				createColumndeleteColumn();
				addFieldUpdater();
				
			}
		});
		

	}
	
	
	protected void createFieldUpdaterstorageLocation()
	{
		storageLoc.setFieldUpdater(new FieldUpdater<GRNDetails, String>()
				{
			@Override

			public void update(int index,GRNDetails object,String value)
			{

				try{
					String val1=(value.trim());
					
					object.setStorageLoc(val1);
				}
				catch (NumberFormatException e)
				{

				}


				table.redrawRow(index);
			}
				});
	}

	
	public void addEditColumnStorageLocation()
	{
		SelectionCell employeeselection=new SelectionCell(binList);
		storageLoc=new Column<GRNDetails, String>(employeeselection) {

			@Override
			public String getValue(GRNDetails object) {	
				
				if(object.getStorageLoc()!=null){
				return object.getStorageLoc();}
				else
					return "N/A";
			}
		};table.addColumn(storagebin,"Storage Location");
		
	}


	
	
	/************storage bin list box*******************/
	
	public void retriveStorageBin()
	{
		final GenricServiceAsync service=GWT.create(GenricService.class);
		Vector<Filter>filtervalue=new Vector<Filter>(); 
		MyQuerry query=new MyQuerry();
		query.setFilters(filtervalue);
		query.setQuerryObject(new Storagebin());
		System.out.println("service"+service);
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			
		@Override
		public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				for(SuperModel model:result)
				{
				 Storagebin entity=(Storagebin) model;
				 binList.add(entity.getBinName());
				}
				addEditColumnEmployee();
				retriveStorageLoc();
			//	createColumndeleteColumn();
				//addeditcomment();
				//addcolumndocument();
			//	addColumnStatus();
			//	addFieldUpdater();
				
			}
		});
		

	}
	
	protected void createFieldUpdaterEmployee()
	{
		storagebin.setFieldUpdater(new FieldUpdater<GRNDetails, String>()
				{
			@Override

			public void update(int index,GRNDetails object,String value)
			{

				try{
					String val1=(value.trim());
					
					object.setStorageBin(val1);
					 
						
					
				}
				catch (NumberFormatException e)
				{

				}


				table.redrawRow(index);
			}
				});
	}


	
	public void addEditColumnEmployee()
	{
		SelectionCell employeeselection=new SelectionCell(binList);
		storagebin=new Column<GRNDetails, String>(employeeselection) {

			@Override
			public String getValue(GRNDetails object) {	
				
				if(object.getStorageBin()!=null){
				return object.getStorageBin();}
				else
					return "N/A";
			}
		};table.addColumn(storagebin,"Storage Bin");
		
	}

	
	/****************************************/
	@Override public void addFieldUpdater() 
	{
		if(editPoQtyColumn!=null)
			addFieldUpdaterEditPoQty();
		if(editPriceColumn!=null)
			addFieldUpdaterPrice();
		if(editReceivedQtyColumn!=null)
			addFieldUpdaterRecievedQuantity();
		if(deleteColumn!=null)
			createFieldUpdaterdeleteColumn();
		createFieldUpdaterEmployee();
		createFieldUpdaterstorageLocation();
	}
	protected void addSortinggetCount()
	{
		List<GRNDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<GRNDetails>(list);
		columnSort.setComparator(getCountColumn, new Comparator<GRNDetails>()
				{
			@Override
			public int compare(GRNDetails e1,GRNDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<GRNDetails>()
				{
			@Override
			public String getValue(GRNDetails object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"Id");
				getCountColumn.setSortable(true);
	}
	protected void addSortinggetWareHouseLocationName()
	{
		List<GRNDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<GRNDetails>(list);
		columnSort.setComparator(getWareHouseLocationNameColumn, new Comparator<GRNDetails>()
				{
			@Override
			public int compare(GRNDetails e1,GRNDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getWarehouseLocation()!=null && e2.getWarehouseLocation()!=null){
						return e1.getWarehouseLocation().compareTo(e2.getWarehouseLocation());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetWareHouseLocationName()
	{
		getWareHouseLocationNameColumn=new TextColumn<GRNDetails>()
				{
			@Override
			public String getValue(GRNDetails object)
			{
				return object.getWarehouseLocation()+"";
			}
				};
				table.addColumn(getWareHouseLocationNameColumn,"Storage Location");
				getWareHouseLocationNameColumn.setSortable(true);
	}
	protected void addSortinggetProductCategory()
	{
		List<GRNDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<GRNDetails>(list);
		columnSort.setComparator(getProductCategoryColumn, new Comparator<GRNDetails>()
				{
			@Override
			public int compare(GRNDetails e1,GRNDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProductCategory()!=null && e2.getProductCategory()!=null){
						return e1.getProductCategory().compareTo(e2.getProductCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetProductCategory()
	{
		getProductCategoryColumn=new TextColumn<GRNDetails>()
				{
			@Override
			public String getValue(GRNDetails object)
			{
				return object.getProductCategory()+"";
			}
				};
				table.addColumn(getProductCategoryColumn,"Product Category");
				getProductCategoryColumn.setSortable(true);
	}
	protected void addSortinggetProductCode()
	{
		List<GRNDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<GRNDetails>(list);
		columnSort.setComparator(getProductCodeColumn, new Comparator<GRNDetails>()
				{
			@Override
			public int compare(GRNDetails e1,GRNDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProductCode()!=null && e2.getProductCode()!=null){
						return e1.getProductCode().compareTo(e2.getProductCode());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetProductCode()
	{
		getProductCodeColumn=new TextColumn<GRNDetails>()
				{
			@Override
			public String getValue(GRNDetails object)
			{
				return object.getProductCode()+"";
			}
				};
				table.addColumn(getProductCodeColumn,"Product Code");
				getProductCodeColumn.setSortable(true);
	}
	protected void addSortinggetProductName()
	{
		List<GRNDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<GRNDetails>(list);
		columnSort.setComparator(getProductNameColumn, new Comparator<GRNDetails>()
				{
			@Override
			public int compare(GRNDetails e1,GRNDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProductName()!=null && e2.getProductName()!=null){
						return e1.getProductName().compareTo(e2.getProductName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetProductName()
	{
		getProductNameColumn=new TextColumn<GRNDetails>()
				{
			@Override
			public String getValue(GRNDetails object)
			{
				return object.getProductName()+"";
			}
				};
				table.addColumn(getProductNameColumn,"Product");
				getProductNameColumn.setSortable(true);
	}
	protected void addSortinggetUnitOfMeasurment()
	{
		List<GRNDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<GRNDetails>(list);
		columnSort.setComparator(getUnitOfMeasurmentColumn, new Comparator<GRNDetails>()
				{
			@Override
			public int compare(GRNDetails e1,GRNDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getUnitOfmeasurement()!=null && e2.getUnitOfmeasurement()!=null){
						return e1.getUnitOfmeasurement().compareTo(e2.getUnitOfmeasurement());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetUnitOfMeasurment()
	{
		getUnitOfMeasurmentColumn=new TextColumn<GRNDetails>()
				{
			@Override
			public String getValue(GRNDetails object)
			{
				return object.getUnitOfmeasurement()+"";
			}
				};
				table.addColumn(getUnitOfMeasurmentColumn,"Unit Of Measurement");
				getUnitOfMeasurmentColumn.setSortable(true);
	}
	protected void addSortinggetPrice()
	{
		List<GRNDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<GRNDetails>(list);
		columnSort.setComparator(getPriceColumn, new Comparator<GRNDetails>()
				{
			@Override
			public int compare(GRNDetails e1,GRNDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getProdPrice()== e2.getProdPrice()){
						return 0;}
					if(e1.getProdPrice()> e2.getProdPrice()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetPrice()
	{
		getPriceColumn=new TextColumn<GRNDetails>()
				{
			@Override
			public String getValue(GRNDetails object)
			{
				return object.getProdPrice()+"";
			}
				};
				table.addColumn(getPriceColumn,"Price");
				getPriceColumn.setSortable(true);
	}
	protected void addSortinggetLBT()
	{
		List<GRNDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<GRNDetails>(list);
		columnSort.setComparator(getLBTColumn, new Comparator<GRNDetails>()
				{
			@Override
			public int compare(GRNDetails e1,GRNDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getLBT()== e2.getLBT()){
						return 0;}
					if(e1.getLBT().getPercentage()> e2.getLBT().getPercentage()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetLBT()
	{
		getLBTColumn=new TextColumn<GRNDetails>()
				{
			@Override
			public String getValue(GRNDetails object)
			{
				if(object.getLBT()==null)
					return 0.0+"";
				else
				  return object.getLBT().getPercentage()+"";
			}
				};
				table.addColumn(getLBTColumn,"LBT Tax");
				getLBTColumn.setSortable(true);
	}
	protected void addSortinggetVAT()
	{
		List<GRNDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<GRNDetails>(list);
		columnSort.setComparator(getVATColumn, new Comparator<GRNDetails>()
				{
			@Override
			public int compare(GRNDetails e1,GRNDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getVAT()== e2.getVAT()){
						return 0;}
					if(e1.getVAT().getPercentage()> e2.getVAT().getPercentage()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetVAT()
	{
		getVATColumn=new TextColumn<GRNDetails>()
				{
			@Override
			public String getValue(GRNDetails object)
			{
				if(object.getVAT()==null)
					return 0.0+"";
				return object.getVAT().getPercentage()+"";
			}
				};
				table.addColumn(getVATColumn,"VAT Tax");
				getVATColumn.setSortable(true);
	}
	protected void addSortinggetPoQty()
	{
		List<GRNDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<GRNDetails>(list);
		columnSort.setComparator(getPoQtyColumn, new Comparator<GRNDetails>()
				{
			@Override
			public int compare(GRNDetails e1,GRNDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPoQty()== e2.getPoQty()){
						return 0;}
					if(e1.getPoQty()> e2.getPoQty()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetPoQty()
	{
		getPoQtyColumn=new TextColumn<GRNDetails>()
				{
			@Override
			public String getValue(GRNDetails object)
			{
				return object.getPoQty()+"";
			}
				};
				table.addColumn(getPoQtyColumn,"PO Qty");
				getPoQtyColumn.setSortable(true);
	}
	protected void addSortinggetReceivedQty()
	{
		List<GRNDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<GRNDetails>(list);
		columnSort.setComparator(getReceivedQtyColumn, new Comparator<GRNDetails>()
				{
			@Override
			public int compare(GRNDetails e1,GRNDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getReceivedQty()== e2.getReceivedQty()){
						return 0;}
					if(e1.getReceivedQty()> e2.getReceivedQty()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetReceivedQty()
	{
		getReceivedQtyColumn=new TextColumn<GRNDetails>()
				{
			@Override
			public String getValue(GRNDetails object)
			{
				return object.getReceivedQty()+"";
			}
				};
				table.addColumn(getReceivedQtyColumn,"Recieved Qty");
				getReceivedQtyColumn.setSortable(true);
	}
	
	protected void addSortinggetEditPoQty()
	{
		List<GRNDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<GRNDetails>(list);
		columnSort.setComparator(editPoQtyColumn, new Comparator<GRNDetails>()
				{
			@Override
			public int compare(GRNDetails e1,GRNDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPoQty()== e2.getPoQty()){
						return 0;}
					if(e1.getPoQty()> e2.getPoQty()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumnEditPoQty()
	{
		TextInputCell cell=new TextInputCell();
		editPoQtyColumn=new Column<GRNDetails,String>(cell)
				{
			@Override
			public String getValue(GRNDetails object)
			{
				return object.getReceivedQty()+"";
			}
				};
				table.addColumn(editPoQtyColumn,"Po Qty");
				editPoQtyColumn.setSortable(true);
	}
	
	protected void addFieldUpdaterEditPoQty()
	{
		
		editPoQtyColumn.setFieldUpdater(new FieldUpdater<GRNDetails, String>() {

			@Override
			public void update(final int index, GRNDetails object, String value) {
				try{
					System.out.println("Value"+value);
					Double d=Double.parseDouble(value.trim());
					
					
					if(d>0)
					{
					 AppUtility.ApplyWhiteColoronRow(GRNDetailsTable.this, index);
					 object.setPoQty(d);
					// RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
					}
					
					else
					{
						 object.setPoQty(-1d);
						 AppUtility.ApplyRedColoronRow(GRNDetailsTable.this, index);
					}
					
					
				}
				catch(Exception e)
				{
					
					 object.setPoQty(-1d);
					AppUtility.ApplyRedColoronRow(GRNDetailsTable.this, index);
				}
				table.redrawRow(index);
				
			}
		});
			
	}
	
	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<GRNDetails,String>()
				{
			@Override
			public void update(int index,GRNDetails object,String value)
			{
				AppUtility.ApplyWhiteColoronRow(GRNDetailsTable.this, index);
				getDataprovider().getList().remove(object);

				table.redrawRow(index);
			}
				});
	}
	
	
	
	
	protected void addColumnEditRecievedQty()
	{
		TextInputCell cell=new TextInputCell();
		editReceivedQtyColumn=new Column<GRNDetails,String>(cell)
				{
			@Override
			public String getValue(GRNDetails object)
			{
				return object.getReceivedQty()+"";
				
			}
				};
				table.addColumn(editReceivedQtyColumn,"Recieved Qty");
				editReceivedQtyColumn.setSortable(true);
	}
	
	protected void addFieldUpdaterRecievedQuantity()
	{
		
		editReceivedQtyColumn.setFieldUpdater(new FieldUpdater<GRNDetails, String>() {

			@Override
			public void update(final int index, GRNDetails object, String value) {
				try{
					Double d=Double.parseDouble(value.trim());
					if(d>=0&& d<=object.getPoQty())
					{
					AppUtility.ApplyWhiteColoronRow(GRNDetailsTable.this, index);
					 object.setReceivedQty(d);
					 RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
					
					}
					
					else
					{
						 object.setReceivedQty(-1d);
						AppUtility.ApplyRedColoronRow(GRNDetailsTable.this, index);
					}
					
					
					
				}
				catch(Exception e)
				{
					 object.setReceivedQty(-1d);
					 AppUtility.ApplyRedColoronRow(GRNDetailsTable.this, index);
				}
				table.redrawRow(index);
				
			}
		});
			
	}
	
	
	
	protected void addSortinggetEditRecievedQty()
	{
		List<GRNDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<GRNDetails>(list);
		columnSort.setComparator(editReceivedQtyColumn, new Comparator<GRNDetails>()
				{
			@Override
			public int compare(GRNDetails e1,GRNDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getReceivedQty()== e2.getReceivedQty()){
						return 0;}
					if(e1.getReceivedQty()> e2.getReceivedQty()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggetEditPrice()
	{
		List<GRNDetails> list=getDataprovider().getList();
		columnSort=new ListHandler<GRNDetails>(list);
		columnSort.setComparator(editPriceColumn, new Comparator<GRNDetails>()
				{
			@Override
			public int compare(GRNDetails e1,GRNDetails e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getProdPrice()== e2.getProdPrice()){
						return 0;}
					if(e1.getProdPrice()> e2.getProdPrice()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumnEditPrice()
	{
		TextInputCell cell=new TextInputCell();
		editPriceColumn=new Column<GRNDetails,String>(cell)
				{
			@Override
			public String getValue(GRNDetails object)
			{
				return object.getProdPrice()+"";
			}
				};
				table.addColumn(editPriceColumn,"Price");
				editPriceColumn.setSortable(true);
	}
	
	protected void addFieldUpdaterPrice()
	{
		
		editPriceColumn.setFieldUpdater(new FieldUpdater<GRNDetails, String>() {

			@Override
			public void update(final int index, GRNDetails object, String value) {
				try{
					Double d=Double.parseDouble(value.trim());
					object.setProdPrice(d);
					if(d>=0)
					{
					
						AppUtility.ApplyWhiteColoronRow(GRNDetailsTable.this, index);
					
					}
					else
					{
						AppUtility.ApplyRedColoronRow(GRNDetailsTable.this, index);
						object.setProdPrice(-1d);
					}
					 
					
				}
				catch(Exception e)
				{
					object.setProdPrice(-1d);
					AppUtility.ApplyRedColoronRow(GRNDetailsTable.this, index);
					
				}
				
				table.redrawRow(index);
				
			}
		});
			
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	protected void addColumngetRejectedQty()
	{
		getRejectedQtyColumn=new TextColumn<GRNDetails>()
				{
			@Override
			public String getValue(GRNDetails object)
			{
				return object.getPoQty()-object.getReceivedQty()+"";
			}
				};
				table.addColumn(getRejectedQtyColumn,"Rejected Qty");
				getRejectedQtyColumn.setSortable(true);
	}
	
	protected void addColumngetTotal()
	{
		getTotalColumn=new TextColumn<GRNDetails>()
				{
			@Override
			public String getValue(GRNDetails object)
			{
				System.out.println("*************"+object.getProdPrice()+" "+object.getReceivedQty());
				double total=object.getReceivedQty()*object.getProdPrice();
				return total+"";
			}
				};
				table.addColumn(getTotalColumn,"Total");
				getTotalColumn.setSortable(true);
	}
	
	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<GRNDetails,String>(btnCell)
				{
			@Override
			public String getValue(GRNDetails object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
	}
	
	@Override
	public void setEnable(boolean state)
	{
          
		
        for(int i=table.getColumnCount()-1;i>-1;i--)
    	  table.removeColumn(i); 
        	  
      
          if(state==true)
          {
        	  addColumngetCount();
      		addColumngetWareHouseLocationName();
      		addColumngetProductCategory();
      		addColumngetProductCode();
      		addColumngetProductName();
      		addColumngetUnitOfMeasurment();
      		addColumnEditPrice();
      		addColumngetLBT();
      		addColumngetVAT();
      		addColumnEditPoQty();
      		addColumnEditRecievedQty();
      		addColumngetRejectedQty();
      		addColumngetTotal();
      		createColumndeleteColumn();
      		
      		addFieldUpdater();
      		addColumnSorting();
      		}
 
          else
          {
        	  addColumngetCount();
      		addColumngetWareHouseLocationName();
      		addColumngetProductCategory();
      		addColumngetProductCode();
      		addColumngetProductName();
      		addColumngetUnitOfMeasurment();
      		addColumngetPrice();
      		addColumngetLBT();
      		addColumngetVAT();
      		addColumngetPoQty();
      		addColumngetReceivedQty();
      		addColumngetRejectedQty();
      		addColumngetTotal();
      		addFieldUpdater();
      		addColumnSorting();
      	
          }
	}
	
	
	public boolean validatePrice()
	{
		List<GRNDetails>details=this.getDataprovider().getList();
		for(GRNDetails temp:details)
		{
			if(temp.getProdPrice()<0)
			{
				return false;
			}
		}
		
		return true;
	}
	
	
	public boolean validatePoQty()
	{
		List<GRNDetails>details=this.getDataprovider().getList();
		for(GRNDetails temp:details)
		{
			if(temp.getPoQty()<0)
			{
				return false;
			}
		}
		
		return true;
	}
	
	
	public boolean validateRecievedQty()
	{
		List<GRNDetails>details=this.getDataprovider().getList();
		for(GRNDetails temp:details)
		{
			if(temp.getReceivedQty()<0||temp.getReceivedQty()>temp.getPoQty())
			{
				return false;
			}
		}
		
		return true;
	}
	
	
	
}
