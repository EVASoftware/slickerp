package com.slicktechnologies.client.views.inventory.recievingnote;

import java.util.ArrayList;
import java.util.HashMap;

import com.slicktechnologies.shared.ProductSerialNoMapping;

public class StorageLocationBin {
	
	protected String storageLocation;
	protected String storageBin;
	protected String warehouseName;
	protected int prodId;
	
	protected double availableQty;
	protected double reOrderLvlQty;
	
	/**
	 * nidhi
	 * 23-08-2018
	 */
	protected HashMap<Integer, ArrayList<ProductSerialNoMapping>> prodSerialNoList = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();

	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	public String getWarehouseName() {
		return warehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}
	
	public String getStorageLocation() {
		return storageLocation;
	}
	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}
	public String getStorageBin() {
		return storageBin;
	}
	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}
	public double getAvailableQty() {
		return availableQty;
	}
	public void setAvailableQty(double availableQty) {
		this.availableQty = availableQty;
	}
	public double getReOrderLvlQty() {
		return reOrderLvlQty;
	}
	public void setReOrderLvlQty(double reOrderLvlQty) {
		this.reOrderLvlQty = reOrderLvlQty;
	}
	public HashMap<Integer, ArrayList<ProductSerialNoMapping>> getProdSerialNoList() {
		return prodSerialNoList;
	}
	public void setProdSerialNoList(
			HashMap<Integer, ArrayList<ProductSerialNoMapping>> prodSerialNoList) {
		this.prodSerialNoList = prodSerialNoList;
	}
	
	
	
}
