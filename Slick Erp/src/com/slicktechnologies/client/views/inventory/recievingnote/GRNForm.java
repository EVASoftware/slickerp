package com.slicktechnologies.client.views.inventory.recievingnote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class GRNForm extends ApprovableFormScreen<GRN> implements ClickHandler, SelectionHandler<Suggestion>, ChangeHandler{

	// ***********************************Variable Declaration **********************************//
	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	
	InventoryLocationPopUp invLoc=new InventoryLocationPopUp();
	
	
	TextBox ibPoNo;
	TextBox tbPoTitle;
	DateBox dbPOdate;
	TextBox tbRefNo;
	
	TextBox ibgrnId;
	TextBox tbgrnTitle;
	DateBox dbgrnDate;
	DateBox dbgrnRefDate;
	
	ObjectListBox<Config> oblGrnGroup;
	ObjectListBox<ConfigCategory> oblGrnCat;
	ObjectListBox<Type> oblGrntype;
	
	ObjectListBox<Branch> olbBranch;
	ObjectListBox<Employee> olbEmployee;
	ObjectListBox<Employee> olbApprover;
	ObjectListBox<HrProject> olbProject;
	
	TextBox tbStatus;
	TextBox tbMrnCommentOnStatusChange;
	TextArea taDescription;
	UploadComposite uploadDocument;
	FormField fgroupingGRNInformation;
	public PersonInfoComposite personInfoComposite;
	public ProductInfoComposite productInfoComposite;
	Button btnAdd;
	public ProductTableGRN table;
	
	
	TextBox tbinspectionId;
	ListBox lbinspectionReqList;
	
	ArrayList<ProductQty>compList=new ArrayList<ProductQty>();
	GRN grnobj;
	
	
	/**
	 * Hyperlink for uploaded asset excel
	 * Date : 22-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : Purchase Modification (NBHC)
	 */
	Hyperlink hyperlink;
	public DocumentUpload assetExcelLink;
	
	/*
	 * Rahul added this for Vendor Ref id on 1st April 2017
	 */
	TextBox tbVendorRefId;
	/**
	 * Added by Rahul Verma on 19 June 2017
	 * For Partial Cancellation we will start saving in it.
	 */
	TextBox tbrefNo2;
	
	// ***********************************Variable Declaration ***********************************//

	public GRNForm() {
		super();
		createGui();
		this.ibgrnId.setEnabled(false);
		this.tbStatus.setEnabled(false);
		this.tbStatus.setText(GRN.CREATED);
		// this.paymentComposite.setWidth("200px");
		// this.paymentComposite.getElement().addClassName("footer-grid");
		// this.addressComposite.setEnable(false);
		this.tbMrnCommentOnStatusChange.setEnabled(false);
//		prodQtyList =new ArrayList<Double>();
		invLoc.getAddButton().addClickHandler(this);
		this.tbinspectionId.setEnabled(false);
		this.tbVendorRefId.setEnabled(false);
		
//		hyperlink.setVisible(false);
	}

	public GRNForm(String[] processlevel, FormField[][] fields,FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
		invLoc.getAddButton().addClickHandler(this);
	}

	// ***********************************Variable Initialization********************************//

	/**
	 * Method template to initialize the declared variables.
	 */
	@SuppressWarnings("unused")
	private void initalizeWidget() {
		ibPoNo = new TextBox();
		ibPoNo.setEnabled(false);
		tbPoTitle = new TextBox();
		tbPoTitle.setEnabled(false);
		dbPOdate = new DateBoxWithYearSelector();
		dbPOdate.setEnabled(false);
		tbRefNo = new TextBox();
		olbProject = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(olbProject);
		
		ibgrnId = new TextBox();
		tbgrnTitle = new TextBox();
		dbgrnDate = new DateBoxWithYearSelector();
		dbgrnRefDate=new DateBoxWithYearSelector();
		
		oblGrnGroup = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblGrnGroup, Screen.GRNGROUP);
		
		oblGrnCat= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblGrnCat, Screen.GRNCATEGORY);
		oblGrnCat.addChangeHandler(this);
		oblGrntype = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(oblGrntype, Screen.GRNTYPE);
		
		olbBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		olbBranch.addClickHandler(this);
		
		olbEmployee = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		olbEmployee.makeEmployeeLive(AppConstants.INVENTORYMODULE, AppConstants.GRN, "Purchase Engineer");
		
		olbApprover = new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApprover, "GRN");
		
		tbStatus = new TextBox();
		taDescription = new TextArea();
		uploadDocument=new UploadComposite();
//		personInfoComposite = new PersonInfoComposite(new MyQuerry(new Vector<Filter>(), new Vendor()));
		personInfoComposite = new PersonInfoComposite(new MyQuerry(new Vector<Filter>(), new Vendor()),false);
		
		/*
		 * commented by Ashwini
		 */
		
//		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Vendor ID");
//		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Vendor Name");
//		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Vendor Cell");
		
		/*
		 * Date:25/09/2018
		 * Des:To make vendor information not mandatory
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", "VENDORINFONOTMANDATORY")){
			
		personInfoComposite.getCustomerId().getHeaderLabel().setText(" Vendor ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText(" Vendor Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText(" Vendor Cell");
		}else{
			personInfoComposite.getCustomerId().getHeaderLabel().setText("* Vendor ID");
			personInfoComposite.getCustomerName().getHeaderLabel().setText("* Vendor Name");
			personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Vendor Cell");
		}
		/*
		 * end by Ashwini
		 */
		
		personInfoComposite.getId().addSelectionHandler(this);
		
		productInfoComposite = AppUtility.initiateSalesProductComposite(new SuperProduct());
		
		tbinspectionId=new TextBox();
		lbinspectionReqList=new ListBox();
		lbinspectionReqList.addItem("SELECT");
		lbinspectionReqList.addItem(AppConstants.YES);
		lbinspectionReqList.addItem(AppConstants.NO);
		
//		productInfoComposite = AppUtility.ReactOnAddGRNProductComposite(new SuperProduct());
		btnAdd = new Button("Add");
		btnAdd.addClickHandler(this);
		
		table = new ProductTableGRN();
		tbMrnCommentOnStatusChange=new TextBox();
		
		hyperlink=new Hyperlink();
		hyperlink.addClickHandler(this);
		hyperlink.setVisible(false);
		
		tbVendorRefId=new TextBox();
		tbrefNo2=new TextBox();
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", "MonthwiseNumberGeneration")){
			tbRefNo.setEnabled(false);
		}
	}

	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen() {
		initalizeWidget();
		// Token to initialize the processlevel menus.
		/****30-1-2019 added by amol "viewbill"****/
		this.processlevelBarNames = new String[] {
				ManageApprovals.APPROVALREQUEST,
				ManageApprovals.CANCELAPPROVALREQUEST, AppConstants.CANCELGRN,
				AppConstants.NEW,AppConstants.REQUESTINSPECTION ,AppConstants.UPLOADASSET,AppConstants.PARTIALCANCEL
				,AppConstants.CREATESALESORDER,AppConstants.SUBMIT,AppConstants.VIEWORDER,AppConstants.VIEWBILL};/** date 3.10.2018 added by komal for sales order creation **/

		// ////////////////////////// Form Field
		// Initialization////////////////////////////////////////////////////
		// Token to initialize formfield
		
		
		
		/** date 12.10.2018 added by komal for header change **/
		String refNo = "Ref No";
		String refNo1 = "Reference Id";
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "POPDFV1")){
			refNo = "Receipt Note No.";
			refNo1 = "Supplier's Ref.";
		}//fgroupingGRNInformation
		
		String mainScreenLabel="GRN Details";
		if(grnobj!=null&&grnobj.getCount()!=0){
			mainScreenLabel=grnobj.getCount()+" "+"/"+" "+grnobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(grnobj.getCreationDate());
		}
		
		boolean referenceDateFlag=false;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", AppConstants.PC_ENABLEREFERENCEDATEMANDATORY)){
			referenceDateFlag=true;
		}
		
		boolean vendorRefIdFlag=false;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", AppConstants.PC_ENABLEVENDORREFERENCEIDMANDATORY)){
			vendorRefIdFlag=true;
		}
		
		//fgroupingCustomerInformation.setLabel(mainScreenLabel);
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingGRNInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		
		fbuilder = new FormFieldBuilder("PO No.", ibPoNo);
		FormField fibPoNo = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PO Title", tbPoTitle);
		FormField ftitle = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("PO Date", dbPOdate);
		FormField fpodate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project", olbProject);
		FormField folbProject = fbuilder.setRowSpan(0).setColSpan(0).build();

		//FormField fprgrouping = fbuilder.setlabel("Purchase Order Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fgroupingIssueNoteInformation = fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		FormField fgroupingAttachInformation = fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("GRN ID", ibgrnId);
		FormField fId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("GRN Title", tbgrnTitle);
		FormField fgrntitle = fbuilder.setMandatory(false).setMandatoryMsg("GRN Title is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* GRN Date", dbgrnDate);
		FormField fdbgrnDate = fbuilder.setMandatory(true).setMandatoryMsg("GRN date is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(refNo, tbRefNo);
		FormField ftbRefId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		String referenceDateLabel = "Reference Date";
		if(referenceDateFlag){
			referenceDateLabel = "* Reference Date";
		}
		fbuilder = new FormFieldBuilder(referenceDateLabel, dbgrnRefDate);
		FormField ftbRefDate = fbuilder.setMandatory(referenceDateFlag).setMandatoryMsg("Reference Date is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("GRN Group", oblGrnGroup);
		FormField fgrngroup = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Inspection Id", tbinspectionId);
		FormField ftbInsId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Inspection Required", lbinspectionReqList);
		FormField flbinsls = fbuilder.setMandatory(true).setMandatoryMsg("Inspection Required is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("GRN Category", oblGrnCat);
		FormField fgrncate = fbuilder.setMandatory(false)
				.setMandatoryMsg("GRN Category is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("GRN Type", oblGrntype);
		FormField fgrntype = fbuilder.setMandatory(false)
				.setMandatoryMsg("GRN Type is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		

//		fbuilder = new FormFieldBuilder("* Branch", olbBranch);
//		FormField folbBranch = fbuilder.setMandatory(true)
//				.setMandatoryMsg("Branch is Manadatory !").setRowSpan(0)
//				.setColSpan(0).build();
		
		FormField folbBranch=null;
		if(AppUtility.isBranchNonMandatory("GRN")){
			fbuilder = new FormFieldBuilder("Branch", olbBranch);
			folbBranch = fbuilder.setMandatory(false).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("* Branch", olbBranch);
			folbBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}
		
		
		
		fbuilder = new FormFieldBuilder("* Purchase Engineer", olbEmployee);
		FormField foblpurchaseengg = fbuilder.setMandatory(true)
				.setMandatoryMsg("Purchase engineer is mandatory")
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Approver", olbApprover);
		FormField folbApprover = fbuilder.setMandatory(true)
				.setMandatoryMsg("Approver is Manadatory !").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Status", tbStatus);
		FormField ftbStatus = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Description(Max 500 characters)", taDescription);
		FormField ftaDescription = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();

		
		FormField fgroupingCountSheetInformation = fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("", personInfoComposite);
		FormField fpersonInfoComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(4).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingItemInformation = fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fprodcompo = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fAdd = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", table.getTable());
		FormField fitemTable = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Remark", tbMrnCommentOnStatusChange);
		FormField ftbMrnCommentOnStatusChange = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Document",uploadDocument);
		FormField fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("", hyperlink);
		FormField fhyperlink = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		String vendorreferenceIdLabel = "Vendor Reference Id";
		if(vendorRefIdFlag){
			vendorreferenceIdLabel = "* Vendor Reference Id";
		}
		
		fbuilder = new FormFieldBuilder(vendorreferenceIdLabel,tbVendorRefId);
		FormField ftbRefNo= fbuilder.setMandatory(vendorRefIdFlag).setMandatoryMsg("Vendor Reference Id is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(refNo1,tbrefNo2);
		FormField ftbrefNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// //////////////////////////////Lay Out Making
		// Code///////////////////////////////////////////////////////////////

//		FormField[][] formfield = { 
//				{ fprgrouping },
//				{ fibPoNo, ftitle, fpodate,folbProject,  },
//				{ fgroupingIssueNoteInformation },
//				{ fId, fgrntitle, fdbgrnDate,ftbRefId},
//				{ ftbRefDate,fgrngroup,fgrncate, fgrntype,  },
//				{ folbBranch,foblpurchaseengg, folbApprover,ftbStatus},
//				{ ftbMrnCommentOnStatusChange,flbinsls,ftbInsId,ftbrefNo2},
//				{ fucUploadTAndCs,fhyperlink},
//				{ ftaDescription },
//				{ fgroupingCountSheetInformation },
//				{ fpersonInfoComposite },
//				{ftbRefNo},
//				{ fgroupingItemInformation },
//				{ fprodcompo,fAdd},
//				{ fitemTable }, 
////				{ flbtotal }, { ftotal }
//		};
//		this.fields = formfield;
		
		FormField[][] formfield = { 
				/**GRN Details **/
				{fgroupingGRNInformation},
				{ fpersonInfoComposite },
				{fgrntitle,fdbgrnDate,folbBranch,foblpurchaseengg},
				{folbApprover,ftbMrnCommentOnStatusChange},
				/**Product Order Details**/
				{ fgroupingItemInformation },
				{ fprodcompo,fAdd},
				{ fitemTable },
				/**Classification**/
				{ fgroupingIssueNoteInformation},
				{ fgrngroup,fgrncate, fgrntype,flbinsls},
				{ ftbInsId},
				/**Reference/Genrl**/
				{ fgroupingCountSheetInformation },
				{ fibPoNo, ftitle, fpodate,ftbrefNo2},
				{ ftbRefId ,ftbRefDate, ftbRefNo,},
				{ ftaDescription },
				/**Attachment**/
				{fgroupingAttachInformation},
				{ fucUploadTAndCs,fhyperlink}
				
		};
		this.fields = formfield;
	}

	/**
	 * method template to update the model with token entity name
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(GRN model) {
		
		if (!ibPoNo.getValue().equals(""))
			model.setPoNo(Integer.parseInt(ibPoNo.getValue()));
		if (tbPoTitle.getValue() != null)
			model.setPoTitle(tbPoTitle.getValue());
		if (dbPOdate.getValue() != null)
			model.setPoCreationDate(dbPOdate.getValue());
		if (olbProject.getValue() != null)
			model.setProJectName(olbProject.getValue());
		
		if (tbgrnTitle.getValue() != null)
			model.setTitle(tbgrnTitle.getValue());
		if (dbgrnDate.getValue() != null)
			model.setCreationDate(dbgrnDate.getValue());
		if (tbRefNo.getValue() != null)
			model.setRefNo(tbRefNo.getValue());
		if (dbgrnRefDate.getValue() != null)
			model.setGrnReferenceDate(dbgrnRefDate.getValue());
		
		if (oblGrnCat.getValue() != null)
			model.setGrnCategory(oblGrnCat.getValue());
		if (oblGrnGroup.getValue() != null)
			model.setGrnGroup(oblGrnGroup.getValue());
		if (oblGrntype.getValue() != null)
			model.setGrnType(oblGrntype.getValue(oblGrntype.getSelectedIndex()));
		
		if (olbBranch.getValue() != null)
			model.setBranch(olbBranch.getValue());
		if (olbApprover.getValue() != null)
			model.setApproverName(olbApprover.getValue());
		if (olbEmployee.getValue() != null)
			model.setEmployee(olbEmployee.getValue());

		model.setUpload(uploadDocument.getValue());
		if(!tbinspectionId.getValue().equals("")){
			model.setInspectionId(Integer.parseInt(tbinspectionId.getValue()));
		}
		if(lbinspectionReqList.getSelectedIndex()!=0){
			model.setInspectionRequired(lbinspectionReqList.getValue(lbinspectionReqList.getSelectedIndex()));
		}
		
		if (taDescription.getValue() != null)
			model.setDescription(taDescription.getValue());
		if (tbStatus.getValue() != null)
			model.setStatus(tbStatus.getValue());
		if (personInfoComposite.getValue() != null)
			model.setVendorInfo(personInfoComposite.getValue());
		
		if (table.getValue() != null)
			model.setInventoryProductItem(table.getValue());
		
		
//		if (!ibPoNo.getValue().equals("")){
//			GRNPresenter.checkQuantity(Integer.parseInt(ibPoNo.getValue()),Integer.parseInt(ibgrnId.getValue()));
//			callOnSave();
//			
//		}
		
		if(tbVendorRefId.getValue()!=null){
			model.setVendorReferenceId(tbVendorRefId.getValue());
		}
		
		if (tbrefNo2.getValue() != null){
			model.setRefNo2(tbrefNo2.getValue().trim());
		}
		grnobj=model;
		presenter.setModel(model);

	}

	/**
	 * method template to update the view with token entity name
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void updateView(GRN view) {
		
		
		grnobj=view;
		if(view.getPoNo()!=0){
			ibPoNo.setValue(view.getPoNo()+"");
			if(view.getWarehouseName()!=null){
				GRNPresenter.warehouseName=view.getWarehouseName();
			}
			GRNPresenter.poNo=view.getPoNo();
			GRNPresenter.checkQuantity(view.getPoNo(),view.getCount());
		}
		
		
		if (view.getPoTitle() != null)
			tbPoTitle.setValue(view.getPoTitle());
		if (view.getPoCreationDate() != null)
			dbPOdate.setValue(view.getPoCreationDate());
		if (view.getProJectName() != null)
			olbProject.setValue(view.getProJectName());
		
		ibgrnId.setValue(view.getCount()+"");
		
		if (view.getTitle() != null)
			tbgrnTitle.setValue(view.getTitle());
		if (view.getCreationDate() != null)
			dbgrnDate.setValue(view.getCreationDate());
		if (view.getGrnReferenceDate() != null)
			dbgrnRefDate.setValue(view.getGrnReferenceDate());
		
		tbRefNo.setValue(view.getRefNo());
		
		if (view.getGrnCategory() != null)
			oblGrnCat.setValue(view.getGrnCategory());
		if (view.getGrnGroup() != null)
			oblGrnGroup.setValue(view.getGrnGroup());
		if (view.getGrnType() != null)
			oblGrntype.setValue(view.getGrnType());
		
		if (view.getBranch() != null)
			olbBranch.setValue(view.getBranch());
		if (view.getEmployee() != null)
			olbEmployee.setValue(view.getEmployee());
		if (view.getApproverName() != null)
			olbApprover.setValue(view.getApproverName());
		
		if (view.getVendorInfo() != null)
			personInfoComposite.setValue(view.getVendorInfo());
		
		if (view.getInventoryProductItem() != null){
			table.setValue(view.getInventoryProductItem());
		}
		
		if (view.getStatus() != null)
			tbStatus.setValue(view.getStatus());
		if (view.getDescription() != null)
			taDescription.setValue(view.getDescription());
		if (view.getRemark()!= null)
			tbMrnCommentOnStatusChange.setValue(view.getRemark());
		
		if(view.getInspectionId()!=0)
			tbinspectionId.setValue(view.getInspectionId()+"");
		
		if(view.getInspectionRequired()!=null){
			for(int i=0;i<lbinspectionReqList.getItemCount();i++){
				String data=lbinspectionReqList.getItemText(i);
				if(data.equals(view.getInspectionRequired()))
				{
					lbinspectionReqList.setSelectedIndex(i);
					break;
				}
			}
		}
		
		if(view.getUpload()!=null)
			uploadDocument.setValue(view.getUpload());
		
		if(view.getAssetExcelUplod()!=null&&!view.getAssetExcelUplod().getName().equals("")){
			hyperlink.setText(view.getAssetExcelUplod().getName());
			assetExcelLink=view.getAssetExcelUplod();
			hyperlink.setVisible(true);
		}else{
			hyperlink.setVisible(false);
		}
		
		if(view.getVendorReferenceId()!=null){
			tbVendorRefId.setValue(view.getVendorReferenceId().trim());
		}
		
		if (view.getRefNo2().trim() != null&&!view.getRefNo2().trim().equals("")){
			tbrefNo2.setValue(view.getRefNo2().trim());
		}
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */

	}
	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")
						|| text.contains("Search")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
//					.getMenuLabels();
			
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")|| text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
//					.getMenuLabels();
			
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Edit") || text.contains("Search")|| text.contains(AppConstants.NAVIGATION) )
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.GRN,LoginPresenter.currentModule.trim());
	}
	
	
	/**
	 * This method checks Goods received product's category is ASSET or not 
	 * if asset in that case user need to upload asset excel file 
	 * which will be use for creating GRN at the time of GRN Approval
	 * 
	 * Date : 22-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */
	public boolean isAsset(){
		List<GRNDetails> itemList=table.getDataprovider().getList();
		
		for(GRNDetails obj:itemList){
			if(obj.getProductCategory().equals("ASSET")){
				return true;
			}
		}
		return false;
	}
	
	public void toggleProcessLevelMenu() {	
		GRN entity = (GRN) presenter.getModel();
		String status = entity.getStatus();
		String inspectionRequired=null;
		int inspectionId=0;
		if(entity.getInspectionRequired()!=null){
			inspectionRequired=entity.getInspectionRequired();
		}
		if(entity.getInspectionId()!=0){
			inspectionId=entity.getInspectionId();
		}
		
	

	for (int i = 0; i < getProcesslevelBarNames().length; i++) {
		InlineLabel label = getProcessLevelBar().btnLabels[i];
		String text = label.getText().trim();
		if ((inspectionRequired.equals(AppConstants.YES)&&status.equals(GRN.CREATED)&&inspectionId!=0)) {
			if (text.equals(AppConstants.REQUESTINSPECTION))
				label.setVisible(true);
			
			if(isPopUpAppMenubar()){
				if(text.equals(AppConstants.NEW)){
					label.setVisible(false);
				}
			}else{
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
			}
			
			if (text.equals(ManageApprovals.APPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(AppConstants.CANCELGRN))
				label.setVisible(false);
			if (text.equals(AppConstants.UPLOADASSET))
				label.setVisible(false);
			/**23-1-2019 added by amol for vieworder***/
			if (text.equals(AppConstants.VIEWORDER))
				label.setVisible(true);
			if(text.equals(AppConstants.VIEWBILL))
				label.setVisible(false);
			/**** Date 13-04-2019 by Vijay for NBHC CCPM partial GRN Cancellation ***/
			if(text.equals(AppConstants.PARTIALCANCEL))
				label.setVisible(false);
			
			
		}
		
		if ((inspectionRequired.equals(AppConstants.YES)&&status.equals(GRN.CREATED)&&inspectionId==0)) {
			if (text.equals(AppConstants.REQUESTINSPECTION))
				label.setVisible(true);
			if(isPopUpAppMenubar()){
				if(text.equals(AppConstants.NEW)){
					label.setVisible(false);
				}
			}else{
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
			}
			if (text.equals(ManageApprovals.APPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(AppConstants.CANCELGRN))
				label.setVisible(false);
			if (text.equals(AppConstants.UPLOADASSET))
				label.setVisible(false);
			/**** Date 13-04-2019 by Vijay for NBHC CCPM partial GRN Cancellation ***/
			if(text.equals(AppConstants.PARTIALCANCEL))
				label.setVisible(false);
		}
		if ((inspectionRequired.equals(AppConstants.NO)&&status.equals(GRN.CREATED))) {
			if(text.equals(AppConstants.VIEWBILL))
				label.setVisible(false);
			if (text.equals(AppConstants.REQUESTINSPECTION))
				label.setVisible(false);
			if(isPopUpAppMenubar()){
				if(text.equals(AppConstants.NEW)){
					label.setVisible(false);
				}
			}else{
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
			}
//			if (text.equals(ManageApprovals.APPROVALREQUEST))
//				label.setVisible(true);
			
			/**
			 *  nidhi
			 *  15-05-2018
			 *  self approval option
			 *  
			 */
			if(getManageapproval().isSelfApproval()){
				/**
				 * Ajinkya added this code 
				 * Date:1/08/2017
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(true);
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				/**
				 * End here
				 */
			}else{
				/**
				 * Ajinkya added this code 
				 * Date:1/08/2017
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(true);
				/**
				 * End here
				 */
			}
			
			if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(AppConstants.CANCELGRN))
				label.setVisible(false);
			if (text.equals(AppConstants.UPLOADASSET))
				label.setVisible(false);
			
			/**** Date 13-04-2019 by Vijay for NBHC CCPM partial GRN Cancellation ***/
			if(text.equals(AppConstants.PARTIALCANCEL))
				label.setVisible(false);
		}
		if (status.equals(GRN.REJECTED)) {
			if (text.equals(AppConstants.NEW))
				label.setVisible(true);
			else
				label.setVisible(false);
			if (text.equals(AppConstants.CANCELGRN))
				label.setVisible(false);
			if (text.equals(AppConstants.UPLOADASSET))
				label.setVisible(false);
			if(text.equals(AppConstants.SUBMIT))
				label.setVisible(false);
			/**23-1-2019 added by amol for vieworder***/
			if (text.equals(AppConstants.VIEWORDER))
				label.setVisible(true);
			if(text.equals(AppConstants.VIEWBILL))
				label.setVisible(false);
			/**** Date 13-04-2019 by Vijay for NBHC CCPM partial GRN Cancellation ***/
			if(text.equals(AppConstants.PARTIALCANCEL))
				label.setVisible(false);
		}
		
		if (status.equals(GRN.CANCELLED)) {
			if (text.equals(AppConstants.NEW))
				label.setVisible(true);
			else
				label.setVisible(false);
			
			if (text.equals(AppConstants.CANCELGRN))
				label.setVisible(false);
			if (text.equals(AppConstants.UPLOADASSET))
				label.setVisible(false);
			if(text.equals(AppConstants.SUBMIT))
				label.setVisible(false);
			/**23-1-2019 added by amol for vieworder***/
			if (text.equals(AppConstants.VIEWORDER))
				label.setVisible(true);
			/**1-2-2019 added by AMOL***/
			if(text.equals(AppConstants.VIEWBILL))
				label.setVisible(true);
			/**** Date 13-04-2019 by Vijay for NBHC CCPM partial GRN Cancellation ***/
			if(text.equals(AppConstants.PARTIALCANCEL))
				label.setVisible(false);
		}
		
		if(status.equals(GRN.INSPECTIONREQUESTED)){
			if (text.equals(AppConstants.REQUESTINSPECTION))
				label.setVisible(false);
			if(isPopUpAppMenubar()){
				if(text.equals(AppConstants.NEW)){
					label.setVisible(false);
				}
			}else{
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
			}
			if(text.equals(AppConstants.SUBMIT))
				label.setVisible(false);
			if (text.equals(ManageApprovals.APPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(AppConstants.CANCELGRN))
				label.setVisible(false);
			if (text.equals(AppConstants.UPLOADASSET))
				label.setVisible(false);
			/**** Date 13-04-2019 by Vijay for NBHC CCPM partial GRN Cancellation ***/
			if(text.equals(AppConstants.PARTIALCANCEL))
				label.setVisible(false);
		}
		if(status.equals(GRN.INSPECTIONAPPROVED)){
			if (text.equals(AppConstants.REQUESTINSPECTION))
				label.setVisible(false);
			if(isPopUpAppMenubar()){
				if(text.equals(AppConstants.NEW)){
					label.setVisible(false);
				}
			}else{
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
			}
			if (text.equals(ManageApprovals.APPROVALREQUEST))
				label.setVisible(true);
			if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(AppConstants.CANCELGRN))
				label.setVisible(false);
			if (text.equals(AppConstants.UPLOADASSET))
				label.setVisible(false);
			/**** Date 13-04-2019 by Vijay for NBHC CCPM partial GRN Cancellation ***/
			if(text.equals(AppConstants.PARTIALCANCEL))
				label.setVisible(false);
		}
		if(status.equals(GRN.REQUESTED)){
			if (text.equals(AppConstants.REQUESTINSPECTION))
				label.setVisible(false);
			if(isPopUpAppMenubar()){
				if(text.equals(AppConstants.NEW)){
					label.setVisible(false);
				}
			}else{
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
			}
			if (text.equals(ManageApprovals.APPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
				label.setVisible(true);
			if (text.equals(AppConstants.CANCELGRN))
				label.setVisible(false);
			if (text.equals(AppConstants.UPLOADASSET))
				label.setVisible(false);
			if(text.equals(AppConstants.SUBMIT))
				label.setVisible(false);
			/**23-1-2019 added by amol for vieworder***/
			if (text.equals(AppConstants.VIEWORDER))
				label.setVisible(true);
			/**1-2-2019 added by AMOL***/
			if(text.equals(AppConstants.VIEWBILL))
				label.setVisible(false);
			/**** Date 13-04-2019 by Vijay for NBHC CCPM partial GRN Cancellation ***/
			if(text.equals(AppConstants.PARTIALCANCEL))
				label.setVisible(false);
			
		}
		if(status.equals(GRN.APPROVED)){
			/**23-1-2019 added by amol for vieworder***/
			if (text.equals(AppConstants.VIEWORDER))
				label.setVisible(true);
			/**1-2-2019 added by AMOL***/
			if(text.equals(AppConstants.VIEWBILL))
				label.setVisible(true);
			
			
			if (text.equals(AppConstants.REQUESTINSPECTION))
				label.setVisible(false);
			if(isPopUpAppMenubar()){
				if(text.equals(AppConstants.NEW)){
					label.setVisible(false);
				}
			}else{
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
			}
			if (text.equals(ManageApprovals.APPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(AppConstants.CANCELGRN))
				label.setVisible(true);
			if (text.equals(AppConstants.UPLOADASSET))
				label.setVisible(false);
			if(text.equals(AppConstants.SUBMIT))
				label.setVisible(false);
			/**** Date 13-04-2019 by Vijay for NBHC CCPM partial GRN Cancellation ***/
			if(text.equals(AppConstants.PARTIALCANCEL))
				label.setVisible(true);
		}
		
		if(status.equals(GRN.CLOSED)){
			/**1-2-2019 added by AMOL***/
			if(text.equals(AppConstants.VIEWBILL))
				label.setVisible(false);
			if (text.equals(AppConstants.REQUESTINSPECTION))
				label.setVisible(false);
			if(text.equals(AppConstants.VIEWBILL))
				label.setVisible(false);
			if(isPopUpAppMenubar()){
				if(text.equals(AppConstants.NEW)){
					label.setVisible(false);
				}
			}else{
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
			}
			if (text.equals(ManageApprovals.APPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(AppConstants.CANCEL))
				label.setVisible(false);
			if (text.equals(AppConstants.UPLOADASSET))
				label.setVisible(false);
			
			if(text.equals(AppConstants.SUBMIT))
				label.setVisible(false);
			/**** Date 13-04-2019 by Vijay for NBHC CCPM partial GRN Cancellation ***/
			if(text.equals(AppConstants.PARTIALCANCEL))
				label.setVisible(true);
		}
		
		if ((inspectionRequired.equals(AppConstants.NO)&&status.equals(GRN.CREATED)&&isAsset())) {
			if (text.equals(AppConstants.REQUESTINSPECTION))
				label.setVisible(false);
			if(isPopUpAppMenubar()){
				if(text.equals(AppConstants.NEW)){
					label.setVisible(false);
				}
			}else{
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
			}
			if (text.equals(ManageApprovals.APPROVALREQUEST))
				label.setVisible(true);
			if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(AppConstants.CANCELGRN))
				label.setVisible(false);
			if (text.equals(AppConstants.UPLOADASSET))
				label.setVisible(true);
			if(text.equals(AppConstants.SUBMIT))
				label.setVisible(false);
			/**** Date 13-04-2019 by Vijay for NBHC CCPM partial GRN Cancellation ***/
			if(text.equals(AppConstants.PARTIALCANCEL))
				label.setVisible(false);
		}
		
		if(status.equals(GRN.INSPECTIONAPPROVED)&&isAsset()){
			if (text.equals(AppConstants.REQUESTINSPECTION))
				label.setVisible(false);
			if(isPopUpAppMenubar()){
				if(text.equals(AppConstants.NEW)){
					label.setVisible(false);
				}
			}else{
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
			}
			if (text.equals(ManageApprovals.APPROVALREQUEST))
				label.setVisible(true);
			if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
				label.setVisible(false);
			if (text.equals(AppConstants.CANCELGRN))
				label.setVisible(false);
			if (text.equals(AppConstants.UPLOADASSET))
				label.setVisible(true);
			if(text.equals(AppConstants.SUBMIT))
				label.setVisible(false);
			/**** Date 13-04-2019 by Vijay for NBHC CCPM partial GRN Cancellation ***/
			if(text.equals(AppConstants.PARTIALCANCEL))
				label.setVisible(false);
		}
		

		//Ashwini Patil Date:20-09-2022
		if(isHideNavigationButtons()) {
			Console.log("in changeProcessLevel isHideNavigationButtons");
			if(text.equalsIgnoreCase(AppConstants.VIEWBILL)||text.equalsIgnoreCase(AppConstants.VIEWORDER)||text.equalsIgnoreCase(AppConstants.CREATESALESORDER)||text.equalsIgnoreCase(AppConstants.NEW))
				label.setVisible(false);	
		}
	}
	}

	public void setAppHeaderBarAsPerStatus() {
		GRN entity = (GRN) presenter.getModel();
		String status = entity.getStatus();
		int inspectionId=0;
		if(entity.getInspectionId()!=0){
			inspectionId=entity.getInspectionId();
		}
		if (status.equals(GRN.APPROVED) || status.equals(GRN.CANCELLED)) {
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search") || text.contains("Email")|| text.contains(AppConstants.NAVIGATION) || text.contains("Print")) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		if (status.equals(GRN.INSPECTIONREQUESTED)||status.equals(GRN.INSPECTIONAPPROVED)) {
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		
		if (status.equals(GRN.CREATED)&&inspectionId!=0) {
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains(AppConstants.NAVIGATION) || text.contains("Print")) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		
		if (status.equals(GRN.REJECTED)) {
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
	}
	
	public void setMenuAsPerStatus() {
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
	}

	public boolean validate() {
		boolean superValidate = super.validate();
		if (superValidate == false)
			return false;
		if (table.getDataprovider().getList().size() == 0) {
			showDialogMessage("Please Select at least one Product !");
			return false;
		}
		if(ibPoNo.getValue()!=null&&tbPoTitle.getValue()!=null){
			if(validateProdQtyAgainstGrn()==false){
//				showDialogMessage("GRN Quantity exceeds PO Quantity.");
				return false;
			}
		}
		if(validateWarehouse()==false){
			showDialogMessage("Please Select Warehouse Details.");
			return false;
		}
		
		if(!validateGrnProdQty()){
			showDialogMessage("Please enter appropriate quantity required!");
			return false;
		}
		
		//vijay
				Date todayDate = new Date();
				CalendarUtil.addDaysToDate(todayDate,1); //Ashwini Patil Date:4-07-2023 Since GRN was not getting saved with current date added 1 day to it
				Console.log("calculated date="+todayDate+" grn date="+dbgrnDate.getValue());
				if(dbgrnDate.getValue().after(todayDate)){
					System.out.println("vijay");
					showDialogMessage("GRN Date Cant be future date");
					return false;
				}
				/**
				 * nidhi
				 * 15-05-2018
				 * for grn validate
				 */
				if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))
				{	if(!validateGRNDate()){	
						return false;
					}
				}
				/*
				 * end
				 */
				
				/**
				 * nidhi
				 * 20-09-2018
				 */
				if(LoginPresenter.mapModelSerialNoFlag ){
					if(!validateSerailNo()){
						showDialogMessage("Please verify Product Qty and serial number selection.");
						return false;
					}
				}
				
				/**
				 * Date 03-07-2019 by Vijay for IM and standard
				 */
				if(dbgrnDate.getValue().after(new Date())){
					showDialogMessage("GRN Date should not be future Date!");
					return false;
				}
		return true;
	}
	boolean validateSerailNo(){
		boolean flag = true;
		

		for(int i=0;i<table.getDataprovider().getList().size();i++){
			int qty = (int) table.getDataprovider().getList().get(i).getProductQuantity();
			int serqty = 0;
			if(table.getDataprovider().getList().get(i).getProSerialNoDetails()!=null  
					&& table.getDataprovider().getList().get(i).getProSerialNoDetails().size()>0
					&& table.getDataprovider().getList().get(i).getProSerialNoDetails().containsKey(0)){
				for(int j = 0;j<table.getDataprovider().getList().get(i).getProSerialNoDetails().get(0).size() ; j++){
					if (table.getDataprovider().getList().get(i).getProSerialNoDetails().get(0).get(j).isStatus()){
						serqty++;
					}
				}
				if(serqty>0 && serqty==qty){
					flag = true;
				}else{
					/** date 2.3.2019 added by komal to allow user for saving GRN without adding any serial number**/
					if(serqty == 0){
						flag = true;
					}else{
						flag = false;
						break;
					}
				}
			}
		}
		
		return flag; 
				
	}
	
	boolean validateGRNDate(){
		boolean flag = true;
		DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
		String diffDays = AppUtility.getForProcessConfigurartionIsActiveOrNot("GRNDateRestriction");
		
		if(diffDays==null){
			flag = true;
		}else{
		
			int days = Integer.parseInt(diffDays);
		
			if(days>0){/*
				Date currentDate = new Date();
				
				Date validateDate = new Date();
	
				currentDate=format.parse(format.format(currentDate));
				validateDate.setDate(days);
				
				Date firstDate = new Date();
				firstDate.setDate(1);
				if(dbgrnDate.getValue().before(firstDate) && currentDate.after(validateDate)){
					showDialogMessage("GRN of previous month is not allowed..!");
				}
			*/
				
	
				Date preFirstDate = new Date();
				Date preLastDate = new Date();
				
				CalendarUtil.addMonthsToDate(preFirstDate, -1);
				preFirstDate.setDate(1);
				
				preLastDate.setDate(1);
				CalendarUtil.addDaysToDate(preLastDate, -1);
				
				
				Date currentDate = new Date();
				
				Date validateDate = new Date();
				
				
				
				currentDate=format.parse(format.format(currentDate));
				
				validateDate=format.parse(format.format(validateDate));
				preLastDate=format.parse(format.format(preLastDate));
				preFirstDate=format.parse(format.format(preFirstDate));
				
				validateDate.setDate(days);
				
				Date firstDate = new Date();
				firstDate.setDate(1);
				if(dbgrnDate.getValue().before(firstDate) ){
					if((dbgrnDate.getValue().after(preFirstDate) && dbgrnDate.getValue().before(preLastDate)) || 
							dbgrnDate.getValue().equals(preLastDate) || dbgrnDate.getValue().equals(preFirstDate) ){
						if(currentDate.after(validateDate) || dbgrnDate.getValue().before(firstDate)){
							showDialogMessage("GRN of previous month is not allowed..!");
							flag = false;
						}
					}else{
						if(currentDate.after(validateDate) || dbgrnDate.getValue().before(firstDate)){
							showDialogMessage("GRN of previous month is not allowed..!");
							flag = false;
						}
					}
				}
			}
		}
		
		return flag;
	}
	
	
	
	// ************************************ Validate Warehouse *********************************************
			public boolean validateWarehouse(){
				int ctr=0;
				for(int i=0;i<table.getDataprovider().getList().size();i++){
					if(!table.getDataprovider().getList().get(i).getWarehouseLocation().equals("")
							&&!table.getDataprovider().getList().get(i).getStorageLoc().equals("")
							&&!table.getDataprovider().getList().get(i).getStorageBin().equals("")){
						ctr++;
					}
				}
				if(ctr==table.getDataprovider().getList().size()){
					return true;
				}else{
					return false;
				}
			}
	
	// ************************************** Validate Product ***********************************************
	
			public boolean validateGrnProdQty()
			{
				List<GRNDetails> grnlis=table.getDataprovider().getList();
				int ctr=0;
				if(!ibPoNo.getValue().equals("")){
					for(int i=0;i<grnlis.size();i++)
					{
						if(grnlis.get(i).getProductQuantity()==0)
						{
							ctr++;
						}
						if(grnlis.get(i).getProductQuantity()<0)
						{
							return false;
						}
					}
					
					if(ctr==grnlis.size()){
						return false;
					}
					else{
						return true;
					}
				}
				else{
					for(int i=0;i<grnlis.size();i++)
					{
						if(grnlis.get(i).getProductQuantity()<=0)
						{
							ctr=ctr+1;
						}
					}
					
					if(ctr==0){
						return true;
					}
					else{
						return false;
					}
				}
			}
			
			
		public boolean validateProduct() {
			int id=Integer.parseInt(productInfoComposite.getProdID().getValue());
			String name = productInfoComposite.getProdNameValue();
			
			System.out.println("Product Id :: "+id);
			
			System.out.println("Product name is :"+name);
			List<GRNDetails> list = table.getDataprovider().getList();
			for (GRNDetails temp : list) {
				if (id==temp.getProductID()) {
					this.showDialogMessage("Can not add Duplicate product");
					return false;
				}
			}
			return true;
		}
		
		// **************** Validate Product Qty Against ****************************
	public boolean validateProdQtyAgainstGrn(){
		boolean flage=true;
		for(int i=0;i<table.getDataprovider().getList().size();i++){
			
			if(!compList.isEmpty()){
				System.out.println("INSIDE Not null Condn.........");
				for(int j=0;j<compList.size();j++){
					if(table.getDataprovider().getList().get(i).getProductID()== compList.get(j).getProdId()){
						if(table.getDataprovider().getList().get(i).getProductQuantity()>compList.get(j).getProdQty()){
							double exceedQty=table.getDataprovider().getList().get(i).getProductQuantity()-compList.get(j).getProdQty();
							flage=false; /**Date 18-04-2019 by Vijay for this validation only for warning msg only dont restric so commented this flag.**/
							showDialogMessage("Product  "+table.getDataprovider().getList().get(i).getProductName()+" exceeds quantity in PO by "+exceedQty+".");
							break;
						}
					}
				}
			}
		}
		return flage;
	}
	
	public void validateGrnQty(ArrayList<ProductQty> poList,ArrayList<ProductQty> grnList){
		compList=new ArrayList<ProductQty>();
		double qty=0;
		if(!grnList.isEmpty()){
			for(int i=0;i<poList.size();i++){
				for(int j=0;j<grnList.size();j++){
					if(poList.get(i).getProdId()==grnList.get(j).getProdId() ){
							qty=poList.get(i).getProdQty()-grnList.get(j).getProdQty();
							ProductQty pq=new ProductQty();
							pq.setProdId(poList.get(i).getProdId());
							pq.setProdQty(qty);
							System.out.println("QTY CHEKING");
							compList.add(pq);
					}
				}
			}
		}
		else{
			System.out.println("hiiiiiiiii");
			compList.addAll(poList);
		}
	}

	/**
	 * sets the id textbox with the passed count value.
	 */
	@Override
	public void setCount(int count) {
		ibgrnId.setValue(count+"");
	}

	@Override
	public void clear() {
		super.clear();
		tbStatus.setText(GRN.CREATED);
		table.clear();

	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbStatus.setEnabled(false);
		this.ibgrnId.setEnabled(false);
		table.setEnable(state);
		tbMrnCommentOnStatusChange.setEnabled(false);
		ibPoNo.setEnabled(false);
		tbPoTitle.setEnabled(false);
		dbPOdate.setEnabled(false);
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", "MonthwiseNumberGeneration")){
			tbRefNo.setEnabled(false);
		}
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		setMenuAsPerStatus();
		

		SuperModel model=new GRN();
		model=grnobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.INVENTORYMODULE,AppConstants.GRN, grnobj.getCount(), null,null,null, false, model, null);
		/** Added by Priyanka**/
		String mainScreenLabel="GRN Details";
		if(grnobj!=null&&grnobj.getCount()!=0){
			mainScreenLabel=grnobj.getCount()+" "+"/"+" "+grnobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(grnobj.getCreationDate());
		}
		fgroupingGRNInformation.getHeaderLabel().setText(mainScreenLabel);
	
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		if(tbStatus.getValue().equals(GRN.REJECTED)){
			tbStatus.setValue(GRN.CREATED);
		}
		if(!ibPoNo.getValue().equals("")){
			personInfoComposite.setEnabled(false);
			productInfoComposite.setEnabled(false);
			getIbPoNo().setEnabled(false);
			getTbPoTitle().setEnabled(false);
			if(getOlbBranch().getValue()!=null){
				getOlbBranch().setEnabled(false);
			}
			getDbPOdate().setEnabled(false);
			if(getOlbProject().getValue()!=null){
				getOlbProject().setEnabled(false);
			}
			/**
			 * @author Vijay Date 08-02-2022
			 * Pecopp wants it should be editable
			 */
//			getOlbApprover().setEnabled(false);
			getOlbEmployee().setEnabled(false);
			tbinspectionId.setEnabled(false);
		}
		if(ibPoNo.getValue().equals("")){
			getIbPoNo().setEnabled(false);
			getTbPoTitle().setEnabled(false);
			getDbPOdate().setEnabled(false);
			tbinspectionId.setEnabled(false);
		}
		setMenuAsPerStatus();
		
		this.processLevelBar.setVisibleFalse(false);
		
		/** Added by Priyanka **/
		String mainScreenLabel="GRN Details";
		if(grnobj!=null&&grnobj.getCount()!=0){
			mainScreenLabel=grnobj.getCount()+" "+"/"+" "+grnobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(grnobj.getCreationDate());
		}
		fgroupingGRNInformation.getHeaderLabel().setText(mainScreenLabel);
	}

	/*************************** on click ***********************************/
	@Override
	public void onClick(ClickEvent event) {
		
		/*
		 * commented by Ashwini
		 */
//		if (event.getSource().equals(btnAdd)) {
//			if(validateVendor()){
//				setproductTable();
//			}
//			else{
//				showDialogMessage("Please select vendor details.");
//			}
//		}
		
		/*
		 * end by Ashwini
		 */
		
		/*
		 * Date:25/09/2018
		 * Des:To remove vendor dependancy
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", "VENDORINFONOTMANDATORY")){
			if(event.getSource().equals(btnAdd)){
				System.out.println("product addition button:::::");
			   setproductTable();
				
			}
		}else{
			if (event.getSource().equals(btnAdd)) {
				if(validateVendor()){
					setproductTable();
				}
				else{
				showDialogMessage("Please select vendor details.");
				}
			}
			
		}
			
		
		
		/*
		 * End by Ashwini
		 */
		
		
		if (event.getSource().equals(hyperlink)) {
			System.out.println("Called-----");
		    String urls=assetExcelLink.getUrl()+"&"+"filename="+hyperlink.getHTML();
		    Window.open(urls, "", "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=no,toolbar=true, width=" 
		    + Window.getClientWidth()+ ",height=" + Window.getClientHeight());
		    System.out.println("Called-----");
		}
	}
	
	public boolean validateVendor(){
		if(!personInfoComposite.getId().getValue().equals("")&&!personInfoComposite.getName().getValue().equals("")&&!personInfoComposite.getPhone().getValue().equals("")){
			return true;
		}
		return false;
	}
	

	/*********************** set table on click **************************/

	/*
	 * commented by Ashwini
	 */
//	public void setproductTable() {
//		if(validateProduct()){
//		  if (productInfoComposite.getValue() != null) {
//			  
//			  prodctType(Integer.parseInt(productInfoComposite.getProdID().getValue())) ;
////			  productInfoComposite.clear();
//		  }else{
//				showDialogMessage("Enter Product Details.");
//			}
//		}
//	}
	

	/*
	 * Date:27/09//2018
	 * Developer:Ashwini
	 * Des:To make vendor info and vendor price list unmandatory
	 */
	public void setproductTable() {
		if(validateProduct()){
		  if (productInfoComposite.getValue() != null) {
			  System.out.println("product::::"+productInfoComposite.getValue());
	          System.out.println(productInfoComposite.getProdName());
	          if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", "VENDORINFONOTMANDATORY")){
	        	  prodctType1(Integer.parseInt(productInfoComposite.getProdID().getValue())) ;
//			  productInfoComposite.clear();
	          }else{
	        	  prodctType(Integer.parseInt(productInfoComposite.getProdID().getValue()));
	          }
		  }else{
				showDialogMessage("Enter Product Details.");
			}
		}
	}
	
	
	
	public void prodctType1(int prodIdCount) {
		final GenricServiceAsync genasync = GWT.create(GenricService.class);
		final MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(prodIdCount);
		filtervec.add(filter);
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		
		querry.setQuerryObject(new SuperProduct());
		showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {
				genasync.getSearchResult(querry,
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								showDialogMessage("An Unexpected error occurred!");
							}
							GRNDetails lis ;
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								for (SuperModel model : result) {
									SuperProduct superProdEntity = (SuperProduct) model;
									 lis = AppUtility.ReactOnAddGRNProductComposite(superProdEntity);
								table.getDataprovider().getList().add(lis);
								}
								
								/*
								 * Comment by Ashwini
								 */
								
//								MyQuerry querry1=new MyQuerry();
//								Vector<Filter> filtervec=new Vector<Filter>();
//								Filter filter1 = new Filter();
//								filter1.setQuerryString("prodCode");
//								filter1.setStringValue(productInfoComposite.getProdCode().getValue().trim());
//								filtervec.add(filter1);
//								filter1=new Filter();
//								filter1.setQuerryString("prodID");
//								filter1.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue().trim()));
//								filtervec.add(filter1);
//								querry1.setFilters(filtervec);
//								querry1.setQuerryObject(new PriceList());
//										
//								async.getSearchResult(querry1, new AsyncCallback<ArrayList<SuperModel>>() {
//									@Override
//									public void onFailure(Throwable caught) {
//									}
//									
//									@Override
//									public void onSuccess(ArrayList<SuperModel> vresult) {
//										boolean flage=false;
//										if (vresult.size() != 0) {
//											final ArrayList<PriceListDetails> list=new ArrayList<PriceListDetails>();
//											ArrayList<VendorDetails> vendorDetailList = new ArrayList<VendorDetails>();
//											for(SuperModel smodel:vresult){
//												PriceList pentity=(PriceList) smodel;
//												list.addAll(pentity.getPriceInfo());
//											}
//											for (PriceListDetails temp : list) {
//												VendorDetails ve = new VendorDetails();
//												ve.setVendorEmailId(temp.getEmail());
//												ve.setVendorId(temp.getVendorID());
//												ve.setVendorName(temp.getFullName());
//												ve.setVendorPhone(temp.getCellNumber());
//												vendorDetailList.add(ve);
//											}
//											
//											for(int f=0;f<list.size();f++)
//											{
//												lis.setProdPrice(list.get(f).getProductPrice());
//											}
//											
//											for(int i=0;i<vendorDetailList.size();i++){
//												if(vendorDetailList.get(i).getVendorId()==Integer.parseInt(personInfoComposite.getId().getValue())){
//													flage=true;
//												}
//											}
//											if(flage==true){
//												table.getDataprovider().getList().add(lis);
//												productInfoComposite.clear();
//											}
//											if(flage==false){
//												showDialogMessage("Selected vendor does not supply this product.");
//												productInfoComposite.clear();
//											}
//										}
//										else{
//											showDialogMessage("Vendor Price List does not exit for product.");
//											productInfoComposite.clear();
//										}
//									}
//								});
							}
						});
				hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}
	/*
	 * End by Ashwini
	 */
	
	
	public void prodctType(int prodIdCount) {
		final GenricServiceAsync genasync = GWT.create(GenricService.class);
		final MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(prodIdCount);
		filtervec.add(filter);
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
		showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {
				genasync.getSearchResult(querry,
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								showDialogMessage("An Unexpected error occurred!");
							}
							GRNDetails lis ;
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								for (SuperModel model : result) {
									SuperProduct superProdEntity = (SuperProduct) model;
									 lis = AppUtility.ReactOnAddGRNProductComposite(superProdEntity);
//									table.getDataprovider().getList().add(lis);
								}
								
								MyQuerry querry1=new MyQuerry();
								Vector<Filter> filtervec=new Vector<Filter>();
								Filter filter1 = new Filter();
								filter1.setQuerryString("prodCode");
								filter1.setStringValue(productInfoComposite.getProdCode().getValue().trim());
								filtervec.add(filter1);
								filter1=new Filter();
								filter1.setQuerryString("prodID");
								filter1.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue().trim()));
								filtervec.add(filter1);
								querry1.setFilters(filtervec);
								querry1.setQuerryObject(new PriceList());
										
								async.getSearchResult(querry1, new AsyncCallback<ArrayList<SuperModel>>() {
									@Override
									public void onFailure(Throwable caught) {
									}
									
									@Override
									public void onSuccess(ArrayList<SuperModel> vresult) {
										boolean flage=false;
										if (vresult.size() != 0) {
											final ArrayList<PriceListDetails> list=new ArrayList<PriceListDetails>();
											ArrayList<VendorDetails> vendorDetailList = new ArrayList<VendorDetails>();
											for(SuperModel smodel:vresult){
												PriceList pentity=(PriceList) smodel;
												list.addAll(pentity.getPriceInfo());
											}
											for (PriceListDetails temp : list) {
												VendorDetails ve = new VendorDetails();
												ve.setVendorEmailId(temp.getEmail());
												ve.setVendorId(temp.getVendorID());
												ve.setVendorName(temp.getFullName());
												ve.setVendorPhone(temp.getCellNumber());
												vendorDetailList.add(ve);
											}
											
											for(int f=0;f<list.size();f++)
											{
												lis.setProdPrice(list.get(f).getProductPrice());
											}
											
											for(int i=0;i<vendorDetailList.size();i++){
												if(vendorDetailList.get(i).getVendorId()==Integer.parseInt(personInfoComposite.getId().getValue())){
													flage=true;
												}
											}
											if(flage==true){
												table.getDataprovider().getList().add(lis);
												productInfoComposite.clear();
											}
											if(flage==false){
												showDialogMessage("Selected vendor does not supply this product.");
												productInfoComposite.clear();
											}
										}
										else{
											showDialogMessage("Vendor Price List does not exit for product.");
											productInfoComposite.clear();
										}
									}
								});
							}
						});
				hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}
	
	
	
	/*******************************************Type Drop Down Logic**************************************/
	
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(oblGrnCat))
		{
			if(oblGrnCat.getSelectedIndex()!=0){
				ConfigCategory cat=oblGrnCat.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(oblGrntype, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
		
	}
	
	
	/***********************************************GRN QTY LOGIC*****************************************/
	
	public void callOnSave()
	{
	int counter=0;
	List<ProductQty> updatedQtyLis=new ArrayList<ProductQty>();
	double qty=0;
	for(int i=0;i<table.getDataprovider().getList().size();i++){
		for(int j=0;j<compList.size();j++){
			if(table.getDataprovider().getList().get(i).getProductID()== compList.get(j).getProdId()){
						
				if(table.getDataprovider().getList().get(i).getProductQuantity()==compList.get(j).getProdQty()){
					counter++;
				}
				else{
					qty=compList.get(j).getProdQty()-table.getDataprovider().getList().get(i).getProductQuantity();
						
					ProductQty pq=new ProductQty();
					pq.setProdId(table.getDataprovider().getList().get(i).getProductID());
					pq.setProdQty(qty);
					updatedQtyLis.add(pq);
				}
			}
		}
	}
		
	if(counter!=table.getDataprovider().getList().size()){
		GRNPresenter pres=(GRNPresenter)presenter;
		pres.createGRN(getUpdatedList(updatedQtyLis));
	}
		
	}
	
	private List<GRNDetails> getUpdatedList(List<ProductQty> qtyLis)
	{
		ArrayList<GRNDetails> arrGrn=new ArrayList<GRNDetails>();
		
		for(int i=0;i<table.getDataprovider().getList().size();i++)
		{
			for(int j=0;j<qtyLis.size();j++){
				if(table.getDataprovider().getList().get(i).getProductID()==qtyLis.get(j).getProdId()){
					GRNDetails dli=new GRNDetails();
					dli.setProductID(table.getDataprovider().getList().get(i).getProductID());
					dli.setProductCategory(table.getDataprovider().getList().get(i).getProductCategory());
					dli.setProductCode(table.getDataprovider().getList().get(i).getProductCode());
					dli.setProductName(table.getDataprovider().getList().get(i).getProductName());
					dli.setProductQuantity(qtyLis.get(j).getProdQty());
					dli.setProdPrice(table.getDataprovider().getList().get(i).getProdPrice());
					dli.setUnitOfmeasurement(table.getDataprovider().getList().get(i).getUnitOfmeasurement());
					dli.setVat(table.getDataprovider().getList().get(i).getVat());
					dli.setTax(table.getDataprovider().getList().get(i).getTax());
					dli.setDiscount(table.getDataprovider().getList().get(i).getDiscount());
					dli.setTotal(table.getDataprovider().getList().get(i).getTotal());
					dli.setWarehouseLocation(table.getDataprovider().getList().get(i).getWarehouseLocation());
					dli.setStorageLoc(table.getDataprovider().getList().get(i).getStorageLoc());
					dli.setStorageBin(table.getDataprovider().getList().get(i).getStorageBin());
					arrGrn.add(dli);
				}
			}
		}
		return arrGrn;
	}
	
	
	
	/**********************************************************************************/

	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	public ObjectListBox<Employee> getOlbEmployee() {
		return olbEmployee;
	}

	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}

	public ObjectListBox<Employee> getOlbApprover() {
		return olbApprover;
	}

	public void setOlbApprover(ObjectListBox<Employee> olbApprover) {
		this.olbApprover = olbApprover;
	}

	public TextBox getIbgrnId() {
		return ibgrnId;
	}

	public void setIbgrnId(TextBox ibgrnId) {
		this.ibgrnId = ibgrnId;
	}

	public ObjectListBox<HrProject> getOlbProject() {
		return olbProject;
	}

	public void setOlbProject(ObjectListBox<HrProject> olbProject) {
		this.olbProject = olbProject;
	}
	
	public TextBox getTbRefNo() {
		return tbRefNo;
	}

	public void setTbRefNo(TextBox tbRefNo) {
		this.tbRefNo = tbRefNo;
	}

	public ProductTableGRN getTable() {
		return table;
	}

	public void setTable(ProductTableGRN table) {
		this.table = table;
	}

	public TextBox getTbStatus() {
		return tbStatus;
	}

	public void setTbStatus(TextBox tbStatus) {
		this.tbStatus = tbStatus;
	}

	public TextArea getTaDescription() {
		return taDescription;
	}

	public void setTaDescription(TextArea taDescription) {
		this.taDescription = taDescription;
	}

	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}

	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}
	
	public Button getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(Button btnAdd) {
		this.btnAdd = btnAdd;
	}
	
	public TextBox getIbPoNo() {
		return ibPoNo;
	}

	public void setIbPoNo(TextBox ibPoNo) {
		this.ibPoNo = ibPoNo;
	}

	public TextBox getTbPoTitle() {
		return tbPoTitle;
	}

	public void setTbPoTitle(TextBox tbPoTitle) {
		this.tbPoTitle = tbPoTitle;
	}

	public DateBox getDbPOdate() {
		return dbPOdate;
	}

	public void setDbPOdate(DateBox dbPOdate) {
		this.dbPOdate = dbPOdate;
	}

	public ProductInfoComposite getProductInfoComposite() {
		return productInfoComposite;
	}

	public void setProductInfoComposite(ProductInfoComposite productInfoComposite) {
		this.productInfoComposite = productInfoComposite;
	}

	@Override
	public TextBox getstatustextbox() {
		return tbStatus;
	}
	
	/******************************************************************/

	public void setEnableFalse() {
		int row = this.fields.length;
		for (int k = 0; k < row; k++) {
			int column = fields[k].length;
			for (int i = 0; i < column; i++) {
				Widget widg = fields[k][i].getWidget();
				if (widg instanceof TextBox) {
					TextBox sb = (TextBox) widg;

					// sb.setText("");
					if (!sb.getText().equals(""))
						sb.setEnabled(false);

				}
				if (widg instanceof IntegerBox) {
					IntegerBox sb = (IntegerBox) widg;

					// sb.setText("");
					if (sb.getValue() != 0)
						sb.setEnabled(false);

				}
				if (widg instanceof DateBox) {
					DateBox sb = (DateBox) widg;
					// sb.setText("");
					if (sb.getValue() != null)
						sb.setEnabled(false);

				}
				if (widg instanceof ListBox) {
					ListBox sb = (ListBox) widg;

					// sb.setText("");
					if (sb.getSelectedIndex() != 0)
						sb.setEnabled(false);

				}
				if (widg instanceof DoubleBox) {
					DoubleBox sb = (DoubleBox) widg;

					// sb.setText("");
					if (sb.getValue() != 0)
						sb.setEnabled(false);

				}

			}
		}
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if(event.getSource().equals(personInfoComposite.getId())||event.getSource().equals(personInfoComposite.getName())||event.getSource().equals(personInfoComposite.getPhone())){
			if(table.getDataprovider().getList().size()!=0){
				table.clear();
			}
		}
	}
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		table.getTable().redraw();
	}

}
