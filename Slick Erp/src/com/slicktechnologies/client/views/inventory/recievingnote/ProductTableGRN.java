package com.slicktechnologies.client.views.inventory.recievingnote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.views.popups.ProductSerailNumberPopup;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class ProductTableGRN extends SuperTable<GRNDetails> implements ClickHandler {
	
	InventoryLocationPopUp invLoc=new InventoryLocationPopUp();
	PopupPanel panel=new PopupPanel(true);
	
	int rowIndex;
	NumberFormat nf = NumberFormat.getFormat("0.00");
	
	TextColumn<GRNDetails> columnProductID;
	TextColumn<GRNDetails> columnProductName;
	TextColumn<GRNDetails> columnProductCode;
	TextColumn<GRNDetails> columnProductCategory;
	TextColumn<GRNDetails> productQuantity;
	
	Column<GRNDetails, String> columnProductQuantity;
	
	TextColumn<GRNDetails> columnunitOfmeasurement;
	
	public  ArrayList<String> warehouseList;
	public  ArrayList<String> binList;
	public  ArrayList<String> storagelocList;
	public  ArrayList<Storagebin> storagelocList1;
	TextColumn<GRNDetails> viewcolumnProductPrice;
	Column<GRNDetails, String> warehouseColumn;
	TextColumn<GRNDetails> warehouseViewColumn;
	
	Column<GRNDetails, String> storagebin;
	Column<GRNDetails, String> columnviewBin;
	
	Column<GRNDetails, String> storageLoc;
	TextColumn<GRNDetails> columnviewLoc;

	Column<GRNDetails, String> deleteColumn;
	Column<GRNDetails, String> addColumn;

//	Column<GRNDetails, String> columnTotal;
	Column<GRNDetails, String> columnTax;
	TextColumn<GRNDetails> columnVat;
	TextColumn<GRNDetails> columnviewDiscount;
	String branch=null;
	
	
	/**
	 * Date 26-04-2017 By Vijay
	 * Total Column And Warehouse Address column
	 * Project : Deadstock (NBHC)
	 */
	TextColumn<GRNDetails> columnTotal;
	TextColumn<GRNDetails> warehouseAddressColumn; 
	public static String warehouseAddress;
	GWTCGlassPanel glassPanel;
	/**
	 * ends here
	 */
	
	/**
	 * nidhi
	 * 21-08-2018
	 * for map serial number
	 */
	Column<GRNDetails, String> addProSerialNo;
	Column<GRNDetails, String> viewProSerialNo;
	boolean tableState = false;
	ProductSerailNumberPopup prodSerialPopup = new ProductSerailNumberPopup(true,false);
	
	
	/*** Date 05-04-2019 by Vijay for NBHC CCPM ***/
	TextColumn<GRNDetails> getcolumnReferenceNo;
	TextColumn<GRNDetails> getcolumnReferenceNo2;

	/**** Date 13-04-2019 by Vijay NBHC CCPM for Partial Cancellation ***/
	Column<GRNDetails, Boolean> checkColumn;
	Header<Boolean> selectAllHeader;
	public static boolean flag = false;
	TextColumn<GRNDetails> getcolumnStatus;
	/**** End here ****/
	
	public ProductTableGRN() {
		
		table.setWidth("100%");
		table.setHeight("400px");
		invLoc.getAddButton().addClickHandler(this);
		invLoc.getCancelButton().addClickHandler(this);
		/**
		 * nidhi
		 * 22-08-2018
		 */
		prodSerialPopup.getLblOk().addClickHandler(this);
		prodSerialPopup.getLblCancel().addClickHandler(this);
	}

	public ProductTableGRN(boolean chkflag) {
		flag = chkflag;
		invLoc.getAddButton().addClickHandler(this);
		invLoc.getCancelButton().addClickHandler(this);
		/**
		 * nidhi
		 * 22-08-2018
		 */
		prodSerialPopup.getLblOk().addClickHandler(this);
		prodSerialPopup.getLblCancel().addClickHandler(this);
	}
	
	@Override
	public void createTable() {
		addCheckBoxCell();
		
		addColumnProductName();
		addColumnUnit();
		
		addColumnProductPrice();
		addColumnProductQuantity();
		
		addColumnTotal();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		createColumnAddColumn();
//		addColumnServiceTax();
//		addColumnVat();
//		addColumnTotal();
		
		addColumnWarehouseAddress();
		/**
		 * nidhi
		 * 21-08-2018
		 * for add serial number
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createColumnAddProSerialNoColumn();
		}
		
		/*** Date 05-04-2019 by Vijay for NBHC CCPM SAP Id mapped here ****/
		createColumnReferenceNo();
		createColumnReferenceNo2();
		createColumnStatus();
		addColumnProductCategory();
		addColumnProductCode();
		addColumnProductID();
		createColumndeleteColumn();
		
		addFieldUpdater();
	}

	

	public void addeditColumn() {
		addCheckBoxCell();
		addColumnProductID();
		addColumnProductName();
		addColumnProductCode();
		addColumnProductCategory();
		addColumnProductPrice();
		addColumnProductQuantity();
		addColumnUnit();
		addColumnTotal();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		createColumnAddColumn();
		addColumnWarehouseAddress();
		/**
		 * nidhi
		 * 21-08-2018
		 * for add serial number
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createColumnAddProSerialNoColumn();
		}
		/*** Date 05-04-2019 by Vijay for NBHC CCPM ****/
		createColumnReferenceNo();
		createColumnReferenceNo2();
		createColumnStatus();

		
		if(GRNPresenter.poNo==0){
			createColumndeleteColumn();
		}
		addFieldUpdater();
	}

	public void addViewColumn() {
		addCheckBoxCell();
		addColumnProductID();
		addColumnProductName();
		addColumnProductCode();
		addColumnProductCategory();
		addColumnProductPrice();
		viewColumnProductQuantity();
		addColumnUnit();
		addColumnTotal();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		addColumnWarehouseAddress();
		/**
		 * nidhi
		 * 21-08-2018
		 * for add serial number
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createViewColumnAddProSerialNoColumn();
		}

		/*** Date 05-04-2019 by Vijay ****/
		createColumnReferenceNo();
		createColumnReferenceNo2();
		createColumnStatus();
		
		addFieldUpdater();
//		addColumnServiceTax();
//		addColumnVat();
//		addColumnTotal();
	}

	@Override
	public void addFieldUpdater() {
//		invLoc.getAddButton().addClickHandler(this);
//		invLoc.getCancelButton().addClickHandler(this);
		createFieldUpdaterprodQtyColumn();
		createFieldUpdaterAdd();
		createFieldUpdaterDelete();
		/**
		 * nidhi
		 * 21-08-2018
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			if(addProSerialNo!=null)
			createFieldUpdaterAddSerialNo();
			if(viewProSerialNo!=null)
			createFieldUpdaterViewSerialNo();
		}
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();
	}
	
	/**
	 * Date 26-04-2017 By Vijay
	 * Warehouse Address column
	 * Project : Deadstock (NBHC)
	 */
	private void addColumnWarehouseAddress() {
		// TODO Auto-generated method stub
		warehouseAddressColumn = new TextColumn<GRNDetails>() {
			
			@Override
			public String getValue(GRNDetails object) {
				// TODO Auto-generated method stub
				return object.getWarehouseAddress();
			}
		};
		table.addColumn(warehouseAddressColumn,"Warehouse Address");
		table.setColumnWidth(warehouseAddressColumn, 200,Unit.PX);
	}
	
	
	
	/******************************************** Warehouse *****************************************************/
	
	
	
	public void retriveWarehouse() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		final MyQuerry query = new MyQuerry();
		query.setQuerryObject(new WareHouse());
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				warehouseList=new ArrayList<String>();
				warehouseList.add("SELECT");
				for (SuperModel model : result) {
					WareHouse entity = (WareHouse) model;
					warehouseList.add(entity.getBusinessUnitName());
					System.out.println("size" + warehouseList.size());
				}
			}
		});
	}


	/************************* storage location ***************************************/

	public void retriveStorageLoc() {
		
		System.out.println("in retrive storage loc");
		final GenricServiceAsync service = GWT.create(GenricService.class);
		final MyQuerry query = new MyQuerry();
		
		Vector<Filter> filtervalue = new Vector<Filter>();
		
		query.setFilters(filtervalue);
		query.setQuerryObject(new StorageLocation());
		System.out.println("service" + service);
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				storagelocList=new ArrayList<String>();
				storagelocList.add("SELECT");
				for (SuperModel model : result) {
					StorageLocation entity = (StorageLocation) model;
					storagelocList.add(entity.getBusinessUnitName());
					System.out.println("size" + storagelocList.size());
				}
			}
		});
	}
	
	/*************************** storage bin ******************************************/

	public void retriveStorageBin() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				binList=new ArrayList<String>();
				storagelocList1=new ArrayList<Storagebin>();
				binList.add("SELECT");
				for (SuperModel model : result) {
					Storagebin entity = (Storagebin) model;
					storagelocList1.add(entity);
					binList.add(entity.getBinName());
				}
				addEditColumnWarehouse();
				addEditColumnStorageLocation();
				addEditColumnStorageBin();
				createColumnAddColumn();
				createColumndeleteColumn();
				addFieldUpdater();
			}
		});
	}
	/*******************************************************************************************/
	// Editable Warehouse
		public void addEditColumnWarehouse() {
			SelectionCell locationselection = new SelectionCell(warehouseList);
			warehouseColumn = new Column<GRNDetails, String>(locationselection) {
				@Override
				public String getValue(GRNDetails object) {
					System.out.println("before==" + object.getWarehouseLocation());
					if (object.getWarehouseLocation() != null) {
						System.out.println("loc==" + object.getWarehouseLocation());
						return object.getWarehouseLocation();
					} else
						return "N.A";
				}
			};
			table.addColumn(warehouseColumn, "Warehouse");
			table.setColumnWidth(warehouseColumn,180,Unit.PX);
		}
	// Editable Storage Location
	public void addEditColumnStorageLocation() {
		System.out.println("in edit column storage loc");
		SelectionCell locationselection = new SelectionCell(storagelocList);
		storageLoc = new Column<GRNDetails, String>(locationselection) {
			@Override
			public String getValue(GRNDetails object) {
				System.out.println("before==" + object.getStorageLoc());
				if (object.getStorageLoc() != null) {
					System.out.println("loc==" + object.getStorageLoc());
				//	storageLocation=object.getStorageLoc();
					return object.getStorageLoc();
				} else
					return "N.A";
			}
		};
		table.addColumn(storageLoc, "Storage Loc");
		table.setColumnWidth(storageLoc,153,Unit.PX);
	}

	// Storage Bin Selection List Column
		public void addEditColumnStorageBin() {
			SelectionCell employeeselection = new SelectionCell(binList);
			storagebin = new Column<GRNDetails, String>(employeeselection) {
				@Override
				public String getValue(GRNDetails object) {
					if (object.getStorageBin() != null) {
						return object.getStorageBin();
					} else
						return "N/A";
				}
			};
			table.addColumn(storagebin, "Storage Bin");
			table.setColumnWidth(storagebin,153,Unit.PX);
		}
		
		
		/*****************************************************************/
		
		// Warehouse Update Method
				protected void createFieldUpdaterWarehouse() {
					warehouseColumn.setFieldUpdater(new FieldUpdater<GRNDetails, String>() {
						@Override
						public void update(int index, GRNDetails object, String value) {
							try {
								String val1 = (value.trim());
								System.out.println("Warehouse   ====" + val1+ " ===" + value);
								object.setWarehouseLocation(val1);
								RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
							} catch (NumberFormatException e) {
							}
							table.redrawRow(index);
						}
					});
				}
				
		// Storage Location Update Method
		protected void createFieldUpdaterstorageLocation() {
			storageLoc.setFieldUpdater(new FieldUpdater<GRNDetails, String>() {
				@Override
				public void update(int index, GRNDetails object, String value) {
					try {
						String val1 = (value.trim());
						System.out.println("STORAGE LOCATION   ====" + val1+ " ===" + value);
						
						if(val1!=null){
							System.out.println("lenovo   "+storagelocList1.size());
						for(int i=0;i<storagelocList1.size();i++){
							System.out.println("lenovo 5  ");
							binList.clear();
							if(storagelocList1.get(i).getStoragelocation().trim().equals(val1.trim())){
								System.out.println("lenovo 9  ");
								binList.add(storagelocList1.get(i).getBinName().trim());
							}
						}
						}
						object.setStorageLoc(val1);
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					} catch (NumberFormatException e) {
					}
					table.redrawRow(index);
				}
			});
		}

		// Storage Bin Update Method
		protected void createFieldUpdaterstorageBin() {
			storagebin.setFieldUpdater(new FieldUpdater<GRNDetails, String>() {
				@Override
				public void update(int index, GRNDetails object, String value) {
					try {
						String val1 = (value.trim());
						object.setStorageBin(val1);
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
						
					} catch (NumberFormatException e) {
					}
					table.redrawRow(index);
				}
			});
		}
		

	/*************************************** Column View ***********************************************/

	public void addColumnProductID() {
		columnProductID = new TextColumn<GRNDetails>() {
			@Override
			public String getValue(GRNDetails object) {
				return object.getProductID() + "";
			}
		};
		table.addColumn(columnProductID, "ID");
		table.setColumnWidth(columnProductID,90,Unit.PX);
	}

	public void addColumnProductCode() {
		columnProductCode = new TextColumn<GRNDetails>() {
			@Override
			public String getValue(GRNDetails object) {
				return object.getProductCode();
			}
		};
		table.addColumn(columnProductCode, "Code");
		table.setColumnWidth(columnProductCode,90,Unit.PX);
	}

	public void addColumnProductName() {
		columnProductName = new TextColumn<GRNDetails>() {
			@Override
			public String getValue(GRNDetails object) {
				return object.getProductName();
			}
		};
		table.addColumn(columnProductName, "Name");
		table.setColumnWidth(columnProductName,120,Unit.PX);
	}

	public void addColumnProductCategory() {
		columnProductCategory = new TextColumn<GRNDetails>() {
			@Override
			public String getValue(GRNDetails object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(columnProductCategory, "Category");
		table.setColumnWidth(columnProductCategory,120,Unit.PX);
	}

	public void addColumnProductQuantity() {
		EditTextCell editCell = new EditTextCell();
		columnProductQuantity = new Column<GRNDetails, String>(editCell) {
			@Override
			public String getValue(GRNDetails object) {
				return object.getProductQuantity() + "";
			}
		};
		table.addColumn(columnProductQuantity, "#Qty.");
		table.setColumnWidth(columnProductQuantity,90,Unit.PX);
	}

	// Read Only Product Quantity
	private void viewColumnProductQuantity() {
		productQuantity = new TextColumn<GRNDetails>() {
			@Override
			public String getValue(GRNDetails object) {
				return object.getProductQuantity() + "";
			}
		};
		table.addColumn(productQuantity, "#Qty.");
		table.setColumnWidth(productQuantity,90,Unit.PX);
	}

	public void addColumnUnit() {
		columnunitOfmeasurement = new TextColumn<GRNDetails>() {
			@Override
			public String getValue(GRNDetails object) {
				return object.getUnitOfmeasurement();
			}
		};
		table.addColumn(columnunitOfmeasurement, "UOM");
		table.setColumnWidth(columnunitOfmeasurement,81,Unit.PX);
	}

	public void addColumnProductPrice() {
		viewcolumnProductPrice = new TextColumn<GRNDetails>() {
			@Override
			public String getValue(GRNDetails object) {
				return object.getProdPrice() + "";
			}
		};
		table.addColumn(viewcolumnProductPrice, "Price");
		table.setColumnWidth(viewcolumnProductPrice,90,Unit.PX);
	}

	public void addColumnviewDiscount() {
		columnviewDiscount = new TextColumn<GRNDetails>() {
			@Override
			public String getValue(GRNDetails object) {
				return object.getDiscount() + "";
			}
		};
		table.addColumn(columnviewDiscount, "% Dis.");
		table.setColumnWidth(columnviewDiscount,45,Unit.PX);
	}

	public void addColumnServiceTax() {
		EditTextCell cell = new EditTextCell();
		columnTax = new Column<GRNDetails, String>(cell) {
			@Override
			public String getValue(GRNDetails object) {
				return object.getTax() + "";
			}
		};
		table.addColumn(columnTax, "Ser.Tax");
		table.setColumnWidth(columnTax,81,Unit.PX);
	}

	public void addColumnVat() {
		columnVat = new TextColumn<GRNDetails>() {
			@Override
			public String getValue(GRNDetails object) {
				if (object.getVat() != null) {
					return object.getVat() + "";
				} else {
					return "N.A";
				}
			}
		};
		table.addColumn(columnVat, "VAT");
		table.setColumnWidth(columnVat,45,Unit.PX);
	}

//	public void addColumnTotal() {
//		TextCell editCell = new TextCell();
//		columnTotal = new Column<GRNDetails, String>(editCell) {
//			@Override
//			public String getValue(GRNDetails object) {
//				double total = 0;
//				SuperProduct product = object.getPrduct();
//				double tax = removeAllTaxes(product);
//				double origPrice = object.getProdPrice() - tax;
//				if (object.getDiscount() == null) {
//					total = origPrice * object.getProductQuantity();
//				}
//				if (object.getDiscount() != null) {
//					total = origPrice- (origPrice * object.getDiscount() / 100);
//					total = total * object.getProductQuantity();
//					// total=Math.round(total);
//				}
//				return nf.format(total);
//			}
//		};
//		table.addColumn(columnTotal, "Total");
//		table.setColumnWidth(columnTotal,90,Unit.PX);
//	}

	
	/**
	 * Date 26-04-2017 By Vijay
	 * Total Column And Warehouse Address column
	 * Project : Deadstock (NBHC)
	 */
	
	public void addColumnTotal() {
		columnTotal = new TextColumn<GRNDetails>() {
			@Override
			public String getValue(GRNDetails object) {
				double total = 0;
				SuperProduct product = object.getPrduct();
				double tax = removeAllTaxes(product);
				double origPrice = object.getProdPrice() - tax;
				if (object.getDiscount() == null) {
					total = origPrice * object.getProductQuantity();
				}
				if (object.getDiscount() != null) {
					total = origPrice- (origPrice * object.getDiscount() / 100);
					total = total * object.getProductQuantity();
				}
				object.setTotal(total);
				return nf.format(object.getTotal());
			}
		};
		table.addColumn(columnTotal, "Total");
		table.setColumnWidth(columnTotal,90,Unit.PX);
	}
	public void viewstorageBin() {
		columnviewBin = new TextColumn<GRNDetails>() {
			@Override
			public String getValue(GRNDetails object) {
				return object.getStorageBin();
			}
		};
		table.addColumn(columnviewBin, "Storage Bin");
		table.setColumnWidth(columnviewBin,153,Unit.PX);
	}

	public void viewWarehouse() {
		warehouseViewColumn = new TextColumn<GRNDetails>() {
			@Override
			public String getValue(GRNDetails object) {
				return object.getWarehouseLocation();
			}
		};
		table.addColumn(warehouseViewColumn, "Warehouse");
		table.setColumnWidth(warehouseViewColumn,180,Unit.PX);
	}
	
	public void viewstarageLoc() {
		columnviewLoc = new TextColumn<GRNDetails>() {
			@Override
			public String getValue(GRNDetails object) {
				return object.getStorageLoc();
			}
		};
		table.addColumn(columnviewLoc, "Storage Loc.");
		table.setColumnWidth(columnviewLoc,153,Unit.PX);
	}

	public void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<GRNDetails, String>(btnCell) {
			@Override
			public String getValue(GRNDetails object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn,81,Unit.PX);

	}
	
	public void createColumnAddColumn() {
		ButtonCell btnCell = new ButtonCell();
		addColumn = new Column<GRNDetails, String>(btnCell) {
			@Override
			public String getValue(GRNDetails object) {
				return "Select Warehouse";
			}
		};
		table.addColumn(addColumn, "");
		table.setColumnWidth(addColumn,180,Unit.PX);

	}

	/****************************************** Column Update methods ******************************************/
	// Product Quantity Update Method
	protected void createFieldUpdaterprodQtyColumn() {
		columnProductQuantity.setFieldUpdater(new FieldUpdater<GRNDetails, String>() {
					@Override
					public void update(int index, GRNDetails object,String value) {
						try {
							double val1;
//							if (Double.parseDouble(value.trim()) == 0) {
//								val1 = 1;
//								object.setProductQuantity(val1);
//								System.out.println("in 0" + val1);
//								table.redrawRow(index);
//							} else {
								val1 = Double.parseDouble(value.trim());
								object.setProductQuantity(val1);
								
								//Date 26-04-2017 added by vijay for setting total amount NBHC Deadstock
								object.setTotal(val1*object.getProdPrice());
//							}
//							RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
						} catch (NumberFormatException e) {
						}
						table.redrawRow(index);
					}
				});
	}

	

	protected void createFieldUpdaterTotalColumn() {
		columnTotal.setFieldUpdater(new FieldUpdater<GRNDetails, String>() {
			@Override
			public void update(int index, GRNDetails object, String value) {
				try {
					Integer val1 = Integer.parseInt(value.trim());
					object.setTotal(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					System.out.println("Field Updater Called");
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	// Delete Update Method
	public void createFieldUpdaterDelete() {
		deleteColumn.setFieldUpdater(new FieldUpdater<GRNDetails, String>() {
			@Override
			public void update(int index, GRNDetails object, String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	// Delete Update Method
		public void createFieldUpdaterAdd() {
			addColumn.setFieldUpdater(new FieldUpdater<GRNDetails, String>() {
				@Override
				public void update(int index, GRNDetails object, String value) {
					
					panel=new PopupPanel(true);
					panel.add(invLoc);
					
					InventoryLocationPopUp.initialzeWarehouse(object.getProductID());
					
					
					if(object.getWarehouseLocation()!=null&&object.getStorageLoc()!=null&&object.getStorageBin()!=null){
						System.out.println("Inside table not null list settt...........");
						for(int i=0;i<invLoc.getLsWarehouse().getItemCount();i++)
						{
							String data=invLoc.lsWarehouse.getItemText(i);
							if(data.equals(object.getWarehouseLocation()))
							{
								invLoc.lsWarehouse.setSelectedIndex(i);
								break;
							}
						}
						
						invLoc.getLsStorageLoc().clear();
						invLoc.lsStorageLoc.addItem("--SELECT--");
						invLoc.lsStorageLoc.addItem(object.getStorageLoc());
						
						
						invLoc.getLsStoragebin().clear();
						invLoc.lsStoragebin.addItem("--SELECT--");
						
						for(int i=0;i<invLoc.arraylist2.size();i++){
							if(invLoc.arraylist2.get(i).getWarehouseName().equals(object.getWarehouseLocation())
									&&invLoc.arraylist2.get(i).getStorageLocation().equals(object.getStorageLoc())){
								invLoc.lsStoragebin.addItem(invLoc.arraylist2.get(i).getStorageBin());
							}
						}
						
						for(int i=0;i<invLoc.getLsStorageLoc().getItemCount();i++)
						{
							String data=invLoc.lsStorageLoc.getItemText(i);
							if(data.equals(object.getStorageLoc()))
							{
								invLoc.lsStorageLoc.setSelectedIndex(i);
								break;
							}
						}
						for(int i=0;i<invLoc.getLsStoragebin().getItemCount();i++)
						{
							String data=invLoc.lsStoragebin.getItemText(i);
							if(data.equals(object.getStorageBin()))
							{
								invLoc.lsStoragebin.setSelectedIndex(i);
								break;
							}
						}
					}
					invLoc.formView();
					panel.show();
					
					
					panel.center();
					rowIndex=index;
				}
			});
		}

		@Override
		public void onClick(ClickEvent event) {
			if(event.getSource()==invLoc.getAddButton())
			{	
				if(!GRNPresenter.warehouseName.equals("")){
					if(invLoc.getLsWarehouse().getSelectedIndex()!=0){
						if(!invLoc.getLsWarehouse().getValue(invLoc.getLsWarehouse().getSelectedIndex()).trim().equals(GRNPresenter.warehouseName.trim())){
							GRNPresenter.showMessage("Warehouse should be "+GRNPresenter.warehouseName+".");
							return;
						}
					}
				}
				if(invLoc.getLsWarehouse().getSelectedIndex()!=0&&invLoc.getLsStorageLoc().getSelectedIndex()!=0&&invLoc.getLsStoragebin().getSelectedIndex()!=0){
					
					/** Date 27-04-2017 added by vijay for deadstock NBHC
					 * for warehouse Address and old code added into timer
					 */
					String warehouseName = invLoc.getLsWarehouse().getValue(invLoc.getLsWarehouse().getSelectedIndex());
					getWarehouseAddress(warehouseName);
					glassPanel = new GWTCGlassPanel();
					glassPanel.show();
					Timer time = new Timer() {
						
						@Override
						public void run() {
							
						
					ArrayList<GRNDetails> list=new ArrayList<GRNDetails>();
					if(getDataprovider().getList().size()!=0){
						list.addAll(getDataprovider().getList());
						for( int i=rowIndex;i<getDataprovider().getList().size();i++){
							list.get(rowIndex).setWarehouseLocation(invLoc.getLsWarehouse().getValue(invLoc.getLsWarehouse().getSelectedIndex()));
							list.get(rowIndex).setStorageLoc(invLoc.getLsStorageLoc().getValue(invLoc.getLsStorageLoc().getSelectedIndex()));
							list.get(rowIndex).setStorageBin(invLoc.getLsStoragebin().getValue(invLoc.getLsStoragebin().getSelectedIndex()));
							
							// warehouse address added by vijay on 27-04-2017 NBHC Deadstock
							if(warehouseAddress!=null)
							list.get(rowIndex).setWarehouseAddress(warehouseAddress);
							
							getDataprovider().getList().clear();
							getDataprovider().getList().addAll(list);
							glassPanel.hide();
						}
					}
				   }
						
					};
					time.schedule(2000);
					
					panel.hide();
				}
				if(invLoc.getLsWarehouse().getSelectedIndex()==0&&invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
					GRNPresenter.showMessage("Please Select Warehouse.");
				}
				else if(invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
					GRNPresenter.showMessage("Please Select Storage Location.");
				}
				else if(invLoc.getLsStoragebin().getSelectedIndex()==0){
					GRNPresenter.showMessage("Please Select Storage Bin.");
				}
				
			}
			if(event.getSource()==invLoc.getCancelButton())
			{	
				invLoc.clear();
				panel.hide();
			}
			

			/**
			 * nidhi
			 * 22-08-2018
			 */
			if(event.getSource() == prodSerialPopup.getLblOk()){
				ArrayList<ProductSerialNoMapping> proList = new ArrayList<ProductSerialNoMapping>();
				List<ProductSerialNoMapping> mainproList = prodSerialPopup.getProSerNoTable().getDataprovider().getList();
				
				
				ArrayList<GRNDetails> list=new ArrayList<GRNDetails>();
				list.addAll(getDataprovider().getList());
				/*if(mainproList != null  && mainproList.size()>0 && mainproList.size()!= (int) list.get(rowIndex).getProductQuantity())
				{
					final GWTCAlert alert = new GWTCAlert(); 
				     alert.alert("Please Add all Serial no.");
				     return ; 
				}*/
				int count = 0;
				for(int i =0 ; i <mainproList.size();i++){
					System.out.println(mainproList.get(i).getProSerialNo() + "  --- get serial number -- ");
					if(mainproList.get(i).getProSerialNo().trim().length()>0){
						proList.add(mainproList.get(i));
						if(mainproList.get(i).isStatus()){
							count++;
						}
					}
				}
				
				if(list.get(rowIndex).getProSerialNoDetails()!=null){
					list.get(rowIndex).getProSerialNoDetails().clear();
					list.get(rowIndex).getProSerialNoDetails().put(0, proList);
					list.get(rowIndex).setProductQuantity(count);
					getDataprovider().getList().clear();
					getDataprovider().getList().addAll(list);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}else{
					HashMap<Integer, ArrayList<ProductSerialNoMapping>> pro = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
					pro.put(0, proList);
						list.get(rowIndex).setProSerialNoDetails(pro);
						list.get(rowIndex).setProductQuantity(count);
						getDataprovider().getList().clear();
						getDataprovider().getList().addAll(list);
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				prodSerialPopup.hidePopUp();
			}
			if(event.getSource()== prodSerialPopup.getLblCancel()){
				prodSerialPopup.hidePopUp();
			}
		}
		
		
		
		/**
		 * Date 27-04-2017 added by vijay for getting warehouse address
		 * @param warehouseName
		 * NBHC Deadstock
		 */
		
			private void getWarehouseAddress(String warehouseName) {

				final GenricServiceAsync service = GWT.create(GenricService.class);
				MyQuerry query = new MyQuerry();
				Company c=new Company();
				System.out.println("Company Id :: "+c.getCompanyId());
				
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter filter = null;
				
				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("buisnessUnitName");
				filter.setStringValue(warehouseName);
				filtervec.add(filter);
				
				query.setFilters(filtervec);
				query.setQuerryObject(new WareHouse());
				
				service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						
						for(int i=0;i<result.size();i++){
							if(i==0){
								WareHouse warehouse = (WareHouse) result.get(i);
								warehouseAddress = warehouse.getAddress().getCompleteAddress();
								System.out.println("on success warehouse Address"+warehouse.getAddress().getCompleteAddress());
							}
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
			}

	
	
/****************************************************************************************************************************/
	
	public double calculateTotal() {
		List<GRNDetails> list = getDataprovider().getList();
		double total = 0;
		for (GRNDetails object : list) {
			SuperProduct product = object.getPrduct();
			double tax = removeAllTaxes(product);
			double discount = object.getProdPrice()* (object.getDiscount() / 100);
			total = object.getProdPrice() * object.getProductQuantity() +tax+ total- discount;
		}
		return total;
	}

	public double removeAllTaxes(SuperProduct entity) {
		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;
		if (entity instanceof ServiceProduct) {
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
		}

		if (entity instanceof ItemProduct) {
			ItemProduct prod = (ItemProduct) entity;
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			// retrVat=Math.round(entity.getPrice()/(1+(vat/100)));
			retrVat = (entity.getPrice() / (1 + (vat / 100)));
			retrVat = entity.getPrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			// retrServ=Math.round(entity.getPrice()/(1+service/100));
			retrServ = (entity.getPrice() / (1 + service / 100));
			retrServ = entity.getPrice() - retrServ;
		}
		if (service != 0 && vat != 0) {

			// Here if both are inclusive then first remove service tax and then
			// on that amount
			// calculate vat.
			double removeServiceTax = (entity.getPrice() / (1 + service / 100));

			// double taxPerc=service+vat;
			// retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed
			// below
			retrServ = (removeServiceTax / (1 + vat / 100));
			retrServ = entity.getPrice() - retrServ;
		}
		tax = retrVat + retrServ;
		return tax;
	}

	@Override
	protected void initializekeyprovider() {

	}

	@Override
	public void applyStyle() {

	}



	public String getBranch() {
		return branch;
	}



	public void setBranch(String branch) {
		this.branch = branch;
	}


	public void createColumnAddProSerialNoColumn() {
		ButtonCell btnCell = new ButtonCell();
		addProSerialNo = new Column<GRNDetails, String>(btnCell) {
			@Override
			public String getValue(GRNDetails object) {
				return "Product Serial Nos";
			}
		};
		table.addColumn(addProSerialNo, "");
		table.setColumnWidth(addProSerialNo,180,Unit.PX);

	}
	public void createFieldUpdaterAddSerialNo() {
		addProSerialNo.setFieldUpdater(new FieldUpdater<GRNDetails, String>() {
			@Override
			public void update(int index, GRNDetails object, String value) {
				rowIndex = index;
				prodSerialPopup.setProSerialenble(true,true);
				prodSerialPopup.getPopup().setSize("50%", "40%");
				if(object.getProSerialNoDetails()!=null && object.getProSerialNoDetails().size()>0){
					ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
					if(object.getProSerialNoDetails().containsKey(0)){
						serNo.addAll(object.getProSerialNoDetails().get(0));
					}
					if(serNo.size() < object.getProductQuantity()){
						int count = (int) (object.getProductQuantity()- serNo.size());
						for(int i = 0 ; i < count ; i ++ ){
							ProductSerialNoMapping pro = new ProductSerialNoMapping();
							serNo.add(pro);
						}
					}
					prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
					prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
					prodSerialPopup.getProSerNoTable().getTable().redraw();
				}else{

					ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
						for(int i = 0 ; i < object.getProductQuantity() ; i ++ ){
							ProductSerialNoMapping pro = new ProductSerialNoMapping();
							pro.setProSerialNo("");
							serNo.add(pro);
						}
						prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
						prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
						prodSerialPopup.getProSerNoTable().getTable().redraw();
				}
				prodSerialPopup.showPopUp();
			
			}
		});
	}
	
	public void createFieldUpdaterViewSerialNo() {
		viewProSerialNo.setFieldUpdater(new FieldUpdater<GRNDetails, String>() {
			@Override
			public void update(int index, GRNDetails object, String value) {
				prodSerialPopup.setProSerialenble(false,false);
				prodSerialPopup.getPopup().setSize("50%", "40%");
				if(object.getProSerialNoDetails().size()>0){
					ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
				System.out.println("size  -- " + serNo.size());
					if(object.getProSerialNoDetails().containsKey(0)){
						serNo.addAll(object.getProSerialNoDetails().get(0));
					}
//					}
						prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
					prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
					prodSerialPopup.getProSerNoTable().getTable().redraw();
				}
				prodSerialPopup.showPopUp();
			
			}
		});
	}
	
	public void createViewColumnAddProSerialNoColumn() {
		ButtonCell btnCell = new ButtonCell();
		viewProSerialNo = new Column<GRNDetails, String>(btnCell) {
			@Override
			public String getValue(GRNDetails object) {
				return "View Product Serial Nos";
			}
		};
		table.addColumn(viewProSerialNo, "");
		table.setColumnWidth(viewProSerialNo,180,Unit.PX);

	}
	
	
	private void createColumnReferenceNo() {
		getcolumnReferenceNo = new TextColumn<GRNDetails>() {
			
			@Override
			public String getValue(GRNDetails object) {
				if(object.getReferenceNo()!=null)
					return object.getReferenceNo();
				else
					return "";
			}
		};
		table.addColumn(getcolumnReferenceNo,"Ref No");
		table.setColumnWidth(getcolumnReferenceNo,90,Unit.PX);

	}

	private void createColumnReferenceNo2() {
		getcolumnReferenceNo2 = new TextColumn<GRNDetails>() {
			
			@Override
			public String getValue(GRNDetails object) {
				if(object.getReferenceNo2()!=null)
					return object.getReferenceNo2();
				else
					return "";
			}
		};
		table.addColumn(getcolumnReferenceNo2,"Ref No2");
		table.setColumnWidth(getcolumnReferenceNo2,90,Unit.PX);


	}

	private void addCheckBoxCell() {
		
		checkColumn=new Column<GRNDetails, Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue(GRNDetails object) {
				return object.isRecordSelect();
			}
		};
		
		checkColumn.setFieldUpdater(new FieldUpdater<GRNDetails, Boolean>() {
			@Override
			public void update(int index, GRNDetails object, Boolean value) {
				System.out.println("Check Column ....");
				object.setRecordSelect(value);
				if(object.getStatus()!=null && object.getStatus().equals(GRN.CANCELLED)){
					GWTCAlert alert = new GWTCAlert(); 
				     alert.alert("This Material Already Cancelled!");
				     object.setRecordSelect(false);
				}
				table.redrawRow(index);
				selectAllHeader.getValue();
				table.redrawHeaders();
				
				
			
			}
		});
		
		
		selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue() {
				if(getDataprovider().getList().size()!=0){
					
				}
				return false;
			}
		};
		
		
		selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
			@Override
			public void update(Boolean value) {
				List<GRNDetails> list=getDataprovider().getList();
				for(GRNDetails object:list){
					object.setRecordSelect(value);
				}
				getDataprovider().setList(list);
				table.redraw();
				addColumnSorting();
			}
		});
		table.addColumn(checkColumn,selectAllHeader);
		table.setColumnWidth(checkColumn,40,Unit.PX);

	}
	
	private void createColumnStatus() {
		getcolumnStatus = new TextColumn<GRNDetails>() {
			
			@Override
			public String getValue(GRNDetails object) {
				if(object.getStatus()!=null)
					return object.getStatus();
				else
					return "";
			}
		};
		table.addColumn(getcolumnStatus,"Status");
		table.setColumnWidth(getcolumnStatus,90,Unit.PX);
			
	}
}
