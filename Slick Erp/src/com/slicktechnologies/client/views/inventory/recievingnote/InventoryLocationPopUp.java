package com.slicktechnologies.client.views.inventory.recievingnote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;

public class InventoryLocationPopUp extends AbsolutePanel implements CompositeInterface,ChangeHandler {
	final static GenricServiceAsync async = GWT.create(GenricService.class);
	
	ObjectListBox<WareHouse> oblwarehouse;
	public static ListBox lsStorageLoc;
	public static ListBox lsStoragebin;
	public static ListBox lsWarehouse;
	private Button addButton;
	private Button cancelButton;
	public static int prodId;
	static double availableQty;
	static double reorderQty;
	
  public static ArrayList<StorageLocationBin> strLocStrBin; 
	static ArrayList<StorageLocationBin>compList;
	public static ArrayList<StorageLocationBin> arraylist2; 
	
	
	static ArrayList<ProductWarehouse>prodWare;
	static ArrayList<WarehouseStorageLoc>wareStr;
	
	static ArrayList<String> warehouseList=new ArrayList<String>();
	
	public InventoryLocationPopUp(){
		initialize();
		prodId=0;
		
		strLocStrBin=new ArrayList<StorageLocationBin>();
		compList=new ArrayList<StorageLocationBin>();
		arraylist2=new ArrayList<StorageLocationBin>();
		
		prodWare=new ArrayList<ProductWarehouse>();
		wareStr=new ArrayList<WarehouseStorageLoc>();
		
		addButton = new Button("Select");
		addButton.setWidth("60px");
		add(addButton, 150, 100);
		
		cancelButton = new Button("Cancel");
		addButton.setWidth("60px");
		add(cancelButton, 250, 100);
		
		setSize("430px","200px");
		this.getElement().setId("form");
		this.getElement().addClassName("newindex"); //Ashwini Patil Date:30-01-2023
		
	}
	
	public void formView(){
		InlineLabel warehouseList = new InlineLabel("Warehouse");
		add(warehouseList, 10, 28);
		lsWarehouse.getElement().getStyle().setWidth(160,Unit.PX);
		add(lsWarehouse, 10, 60);
		
		InlineLabel storageLocationList = new InlineLabel("Storage Location");
		add(storageLocationList, 180, 28);
		lsStorageLoc.getElement().getStyle().setWidth(110,Unit.PX);
		add(lsStorageLoc, 180, 60);
		
		InlineLabel storageBinList = new InlineLabel("Storage Bin");
		add(storageBinList, 310, 28);
		lsStoragebin.getElement().getStyle().setWidth(110,Unit.PX);
		add(lsStoragebin, 310, 60);
	}
	
	protected void initialize(){
		
		lsWarehouse=new ListBox();
		lsWarehouse.addChangeHandler(this);
		lsWarehouse.addItem("--SELECT--");
		
		lsStorageLoc=new ListBox();
		lsStorageLoc.addChangeHandler(this);
		lsStorageLoc.addItem("--SELECT--");
		lsStoragebin=new ListBox();
		lsStoragebin.addItem("--SELECT--");
		lsStoragebin.addChangeHandler(this);
	}
	
	@Override
	public void setEnable(boolean status) {
		
	}

	@Override
	public boolean validate() {
		return false;
		
	}

	public void clear() {
		lsWarehouse.setSelectedIndex(0);
		lsStorageLoc.setSelectedIndex(0);
		lsStoragebin.setSelectedIndex(0);
	}
	

	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(lsWarehouse)){
			System.out.println("in side on change of warehouse event ");
			retriveStorageLocation();
		}
		if(event.getSource().equals(lsStorageLoc)){
			System.out.println("in side on change of storage location event ");
			retriveStorageBin();
		}
		if(event.getSource().equals(lsStoragebin)){
			retriveAvailableQty();
			retriveReorderQty();
		}
	}
	
	// ******************************* Retrieving Warehouse from ****************************************

	
	
	public static void initialzeWarehouse(final int prodId){
		InventoryLocationPopUp.prodId=prodId;
		boolean flage=false;
		boolean arrayListFlage=false;
		
//		if(!prodWare.isEmpty()&&prodWare!=null&&!wareStr.isEmpty()&&wareStr!=null&&!compList.isEmpty()&&compList!=null){
		if(!strLocStrBin.isEmpty()&&strLocStrBin!=null){	
			System.out.println("inside list not empty condition.............");
			
			lsWarehouse.clear();
			lsStorageLoc.clear();
			lsStoragebin.clear();
			
			lsWarehouse.addItem("--SELECT--");
			lsStorageLoc.addItem("--SELECT--");
			lsStoragebin.addItem("--SELECT--");
			
			System.out.println("PRODUCT ID ::: "+prodId);
			arraylist2.clear();
			for(int j=0;j<strLocStrBin.size();j++){
				
				if(prodId==strLocStrBin.get(j).getProdId()){
					System.out.println("PRODUCT ID ::: "+strLocStrBin.get(j).getProdId());
					System.out.println("inside Match Found condition.............");
					StorageLocationBin slb=new StorageLocationBin();
					slb.setProdId(strLocStrBin.get(j).getProdId());
					slb.setWarehouseName(strLocStrBin.get(j).getWarehouseName());
					slb.setStorageLocation(strLocStrBin.get(j).getStorageLocation());
					slb.setStorageBin(strLocStrBin.get(j).getStorageBin());
					slb.setAvailableQty(strLocStrBin.get(j).getAvailableQty());
					slb.setReOrderLvlQty(strLocStrBin.get(j).getReOrderLvlQty());
					/**
					 * nidhi
					 * 23-08-2018
					 * for map serial numbera
					 */
					slb.setProdSerialNoList(strLocStrBin.get(j).getProdSerialNoList());
					arraylist2.add(slb);
					
					flage=true;
				}
			}
			
//			if(!arraylist2.isEmpty()){
//				for(int k=0;k<arraylist2.size();k++){
//					if(arraylist2.get(k).getProdId()==prodId){
//						arrayListFlage=true;
//					}
//				}
//				if(arrayListFlage==false){
//					StorageLocationBin slb=new StorageLocationBin();
//					slb.setProdId(strLocStrBin.get(j).getProdId());
//					slb.setWarehouseName(strLocStrBin.get(j).getWarehouseName());
//					slb.setStorageLocation(strLocStrBin.get(j).getStorageLocation());
//					slb.setStorageBin(strLocStrBin.get(j).getStorageBin());
//					slb.setAvailableQty(strLocStrBin.get(j).getAvailableQty());
//					arraylist2.add(slb);
//					
//					flage=true;
//				}
//			}
//			else{
//				StorageLocationBin slb=new StorageLocationBin();
//				slb.setProdId(strLocStrBin.get(j).getProdId());
//				slb.setWarehouseName(strLocStrBin.get(j).getWarehouseName());
//				slb.setStorageLocation(strLocStrBin.get(j).getStorageLocation());
//				slb.setStorageBin(strLocStrBin.get(j).getStorageBin());
//				slb.setAvailableQty(strLocStrBin.get(j).getAvailableQty());
//				arraylist2.add(slb);
//			
//				flage=true;
//			}
			
			if(flage==false){
				getWarehouseName(prodId);
			}
			
			if(flage==true){
				
				HashSet<String> hset1=new HashSet<String>();
				ArrayList<String> wh=new ArrayList<String>();
				
//				HashSet<String> hset2=new HashSet<String>();
//				ArrayList<String> sl=new ArrayList<String>();
//				
//				HashSet<String> hset3=new HashSet<String>();
//				ArrayList<String> sb=new ArrayList<String>();
				
				for(int i=0;i<arraylist2.size();i++){
					hset1.add(arraylist2.get(i).getWarehouseName());
					
//					for(int j=0;j<compList.size();j++){
//						if(arraylist2.get(i).getProdId()==compList.get(j).getProdId()
//								&&arraylist2.get(i).getWarehouseName().equals(compList.get(j).getWarehouseName())
//								&&arraylist2.get(i).getStorageLocation().equals(compList.get(j).getStorageLocation())){
//							hset2.add(arraylist2.get(i).getStorageLocation());
//						}
//						if(arraylist2.get(i).getProdId()==compList.get(j).getProdId()
//								&&arraylist2.get(i).getWarehouseName().equals(compList.get(j).getWarehouseName())
//								&&arraylist2.get(i).getStorageLocation().equals(compList.get(j).getStorageLocation())
//								&&arraylist2.get(i).getStorageBin().equals(compList.get(j).getStorageBin())){
//							hset3.add(arraylist2.get(i).getStorageBin());
//						}
//					}
				}
				wh.addAll(hset1);
//				sl.addAll(hset2);
//				sb.addAll(hset3);
				
				
				
//				for(int i=0;i<wh.size();i++){
//					lsWarehouse.addItem(wh.get(i));
//				}
//				for(int i=0;i<sl.size();i++){
//					lsStorageLoc.addItem(sl.get(i));
//				}
//				for(int i=0;i<sb.size();i++){
//					lsStoragebin.addItem(sb.get(i));
//				}
				
				
				/**
				 * Date : 06-02-2017 by Anil
				 */
				if(AppUtility.isBranchRestricted()&&!LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")){
					for(String wh1:wh){
						if(warehouseList.contains(wh1)){
							lsWarehouse.addItem(wh1);
						}
					}	
				}else{
					for(int i=0;i<wh.size();i++){
						lsWarehouse.addItem(wh.get(i));
					}
				}
				
				/**
				 * End
				 */
				
				
			}
			
		}
		else{
			getWarehouseName(prodId);
		}
	}
	
	// *************************************** Retrieving Warehouse **************************************
	
		private static void getWarehouseName(final int prodId){
//			prodWare.clear();
			System.out.println("Inside search .............................");
			MyQuerry query = new MyQuerry();
			Company c=new Company();
			System.out.println("Company Id :: "+c.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("productinfo.prodID");
			filter.setIntValue(prodId);
			filtervec.add(filter);
			
			query.setFilters(filtervec);
			query.setQuerryObject(new ProductInventoryView());
			
			async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					lsWarehouse.clear();
					lsWarehouse.addItem("--SELECT--");
					lsStorageLoc.clear();
					lsStorageLoc.addItem("--SELECT--");
					lsStoragebin.clear();
					lsStoragebin.addItem("--SELECT--");
					HashSet<String> hset=new HashSet<String>();
					
					
					for (SuperModel model : result) {
						ProductInventoryView entity = (ProductInventoryView) model;
						for(int i=0;i<entity.getDetails().size();i++){
							
							StorageLocationBin slb=new StorageLocationBin();
							slb.setProdId(entity.getDetails().get(i).getProdid());
							slb.setWarehouseName(entity.getDetails().get(i).getWarehousename());
							slb.setStorageLocation(entity.getDetails().get(i).getStoragelocation());
							slb.setStorageBin(entity.getDetails().get(i).getStoragebin());
							slb.setAvailableQty(entity.getDetails().get(i).getAvailableqty());
							slb.setReOrderLvlQty(entity.getDetails().get(i).getReorderlevel());
							/**
							 * Nidhi 
							 * 23-08-2018
							 * 
							 */
							slb.setProdSerialNoList(entity.getDetails().get(i).getProSerialNoDetails());
							strLocStrBin.add(slb);
							
						}
					}
					/** Date 18-06-2019
					 *  By Vijay for NBHC IM old code added in else block 
					 *  If stock available then only show warehouses with process config
				     */  
					boolean flag = false;
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Warehouse", "EnableLoadExpiryWarehouseStockInHand")){
						flag = true;
					}
					for(int i=0;i<strLocStrBin.size();i++){
						if(flag && strLocStrBin.get(i).getAvailableQty()>0){
							hset.add(strLocStrBin.get(i).getWarehouseName());
						}
						else{
							hset.add(strLocStrBin.get(i).getWarehouseName());
						}
					}
					ArrayList<String>list=new ArrayList<String>();
					list.addAll(hset);
//					for(int i=0;i<list.size();i++){
//						lsWarehouse.addItem(list.get(i));
//					}
					
//					if(AppUtility.isBranchRestricted()){
//						getWarehouseName(list);
//					}else{
//						for(int i=0;i<list.size();i++){
//							lsWarehouse.addItem(list.get(i));
//						}
//					}
					
					
					/**
					 * Date : 06-02-2017 by Anil
					 */
					if(AppUtility.isBranchRestricted()
							&&!LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")){
						getWarehouseName(list);
					}else{
						for(int i=0;i<list.size();i++){
							lsWarehouse.addItem(list.get(i));
						}
					}
					/**
					 * End
					 */
					
				}
			});

			
		}
	
		private static void getWarehouseName(final ArrayList<String>list1) {
			
			MyQuerry query = new MyQuerry();
			System.out.println("Company Id :: "+UserConfiguration.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(true);
			filtervec.add(filter);
			
			if(AppUtility.isBranchRestricted()){
				if (!LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")) {
					List<String> list=new ArrayList<String>();
					for(Branch branch:LoginPresenter.globalBranch){
						list.add(branch.getBusinessUnitName());
					}
					
					 filter=new Filter();
	                 filter.setQuerryString("branchdetails.branchName IN");
	                 filter.setList(list);
	                 filtervec.add(filter);
				}
			}
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(UserConfiguration.getCompanyId());
			filtervec.add(filter);
			
			query.setFilters(filtervec);
			query.setQuerryObject(new WareHouse());
			
			async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					ArrayList<String> warehouseList=new ArrayList<String>();
					if(result.size()!=0){
						for(SuperModel model:result){
							WareHouse entity=(WareHouse) model;
							warehouseList.add(entity.getBusinessUnitName());
						}
						Collections.sort(warehouseList);
					}
					
						for(String wh:list1){
							if(warehouseList.contains(wh)){
								lsWarehouse.addItem(wh);
							}
						}

					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					
				}
			});
		}
		
		
	// ******************************* Retrieving Storage location from ****************************************
	
	private void retriveStorageLocation(){
		System.out.println("Inside Warehouse Change......");
		Console.log("inside warehouse change event");
		lsStorageLoc.clear();
		lsStorageLoc.addItem("--SELECT--");
		lsStoragebin.clear();
		lsStoragebin.addItem("--SELECT--");
		
		Console.log("inside warehouse change storage bin list size "+strLocStrBin.size());
		System.out.println("");
		HashSet<String> hset=new HashSet<String>();
		for(int i=0;i<strLocStrBin.size();i++){
			
			Console.log("inside for loop");
			
			
			Console.log("inside for loop prodId"+prodId);
			Console.log("inside for loop strLocStrBin.get(i).getProdId()"+strLocStrBin.get(i).getProdId());
			Console.log("inside for loop loc 1"+lsWarehouse.getValue(lsWarehouse.getSelectedIndex()).trim());
			Console.log("inside for loop loc"+strLocStrBin.get(i).getWarehouseName().trim());
			Console.log("inside for loop");
			Console.log("inside for loop");
			
			if(prodId==strLocStrBin.get(i).getProdId()
					&&lsWarehouse.getValue(lsWarehouse.getSelectedIndex()).trim().equals(strLocStrBin.get(i).getWarehouseName().trim())){
			
				Console.log("inside for condition");
				
				hset.add(strLocStrBin.get(i).getStorageLocation());
			}
		}
		
		Console.log("hash set size "+hset.size());
		
		ArrayList<String>list=new ArrayList<String>();
		list.addAll(hset);
		
		Console.log("list size "+list.size());
		
		for(int i=0;i<list.size();i++){
			lsStorageLoc.addItem(list.get(i));
		}
		
		
//		boolean check=false;
//		for(int i=0;i<list.size();i++){
//			if(!wareStr.isEmpty())
//			{
//				for(int j=0;j<wareStr.size();j++){
//					if(prodId==wareStr.get(j).getProdId()
//					&&lsWarehouse.getValue(lsWarehouse.getSelectedIndex()).trim().equals(wareStr.get(j).getWarehouseName())){
//						check=true;
//					}
//				}
//				if(check==false){
//					
//				}
//			}
//			else{
//				WarehouseStorageLoc wsl=new WarehouseStorageLoc();
//				wsl.setProdId(prodId);
//				wsl.setWarehouseName(lsWarehouse.getValue(lsWarehouse.getSelectedIndex()));
//				wsl.setStorageLocation(list.get(i));
//				wareStr.add(wsl);
//			}
//		}
//		for(int i=0;i<wareStr.size();i++){
//			System.out.println("+++++++++++++++++++++++ 222 +++++++++++++++++++++++++");
//			System.out.println(wareStr.get(i).getProdId());
//			System.out.println(wareStr.get(i).getStorageLocation());
//			System.out.println(wareStr.get(i).getWarehouseName());
//		}
		
	}
	
	
	
	// ****************************** Retrieving Storage Bin From Storage location **********************************
	/**
	 *  Added By Priyanka - 28-08-2021
	 *  Des : Pecopp - if storage bin status is inactive then is should not in drop down selection raised by ashwini
	 */
	
	private void retriveStorageBin(){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("storagelocation");
		filter.setStringValue(lsStorageLoc.getValue(lsStorageLoc.getSelectedIndex()));
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				lsStoragebin.clear();
				lsStoragebin.addItem("--SELECT--");
				for (SuperModel model : result) {
					Storagebin entity = (Storagebin) model;
					lsStoragebin.addItem(entity.getBinName());
					System.out.println("BIN :"+entity.getBinName());
				}
			}
		});
	}
	/**
	 *  End By Priyanka
	 */
	
//	private void retriveStorageBin(){
//		System.out.println("Inside Storage Location Change......");
//		lsStoragebin.clear();
//		lsStoragebin.addItem("--SELECT--");
//		HashSet<String> hset=new HashSet<String>();
//		for(int i=0;i<strLocStrBin.size();i++){
//			if(prodId==strLocStrBin.get(i).getProdId()
//					&&lsWarehouse.getValue(lsWarehouse.getSelectedIndex()).trim().equals(strLocStrBin.get(i).getWarehouseName().trim())
//					&&lsStorageLoc.getValue(lsStorageLoc.getSelectedIndex()).trim().equals(strLocStrBin.get(i).getStorageLocation().trim())){
//				//if(strLocStrBin.get(i).)
//				Console.log("inside condition for stoage bin");
//				
//				hset.add(strLocStrBin.get(i).getStorageBin());
//			}
//		}
//		
//		
//		Console.log("inside condition for stoage bin hash set size "+hset.size());
//		ArrayList<String>list=new ArrayList<String>();
//		list.addAll(hset);
//		
//		Console.log("inside condition for stoage bin list size"+list.size());
//		
//		for(int i=0;i<list.size();i++){
//			lsStoragebin.addItem(list.get(i));
//		}
//		
////		boolean check=false;
////		for(int i=0;i<list.size();i++){
////			
////			if(!compList.isEmpty())
////			{
////				System.out.println("INSIDE NON EMPTY >>>");
////				for(int j=0;j<compList.size();j++){
////					if(prodId==compList.get(j).getProdId()
////					&&lsWarehouse.getValue(lsWarehouse.getSelectedIndex()).trim().equals(compList.get(j).getWarehouseName())
////					&&lsStorageLoc.getValue(lsStorageLoc.getSelectedIndex()).trim().equals(compList.get(j).getStorageLocation())
////					&&lsStoragebin.getValue(lsStoragebin.getSelectedIndex()).trim().equals(compList.get(j).getStorageBin())){
////						check=true;
////						System.out.println("INSIDE NON EMPTY TRUE >>>");
////					}
////				}
////				if(check==false){
////					System.out.println("INSIDE NON EMPTY False >>>");
////					StorageLocationBin slb=new StorageLocationBin();
////					slb.setProdId(prodId);
////					slb.setWarehouseName(lsWarehouse.getValue(lsWarehouse.getSelectedIndex()));
////					slb.setStorageLocation(lsStorageLoc.getValue(lsStorageLoc.getSelectedIndex()));
////					slb.setStorageBin(list.get(i));
////					
////					compList.add(slb);
////				}
////			}
////			else{
////			
////			
////			StorageLocationBin slb=new StorageLocationBin();
////			slb.setProdId(prodId);
////			slb.setWarehouseName(lsWarehouse.getValue(lsWarehouse.getSelectedIndex()));
////			slb.setStorageLocation(lsStorageLoc.getValue(lsStorageLoc.getSelectedIndex()));
////			slb.setStorageBin(list.get(i));
////			
////			compList.add(slb);
////			}
////		}
////		for(int i=0;i<compList.size();i++){
////			System.out.println("++++++++++++++++++++++ 333 ++++++++++++++++++++++++++");
////			System.out.println(compList.get(i).getProdId());
////			System.out.println(compList.get(i).getWarehouseName());
////			System.out.println(compList.get(i).getStorageLocation());
////			System.out.println(compList.get(i).getStorageBin());
////		}
//		
//	}
	
	// ****************************** Retrieving Available Quantity From Storage Bin **********************************
	
	private void retriveAvailableQty() {
		
		
		for(int i=0;i<strLocStrBin.size();i++){
			if(prodId==strLocStrBin.get(i).getProdId()
					&&lsWarehouse.getValue(lsWarehouse.getSelectedIndex()).trim().equals(strLocStrBin.get(i).getWarehouseName().trim())
					&&lsStorageLoc.getValue(lsStorageLoc.getSelectedIndex()).trim().equals(strLocStrBin.get(i).getStorageLocation().trim())
					&&lsStoragebin.getValue(lsStoragebin.getSelectedIndex()).trim().equals(strLocStrBin.get(i).getStorageBin().trim())){
				availableQty=strLocStrBin.get(i).getAvailableQty();
			}
		}
	}
	
	
private void retriveReorderQty() {
		
		for(int i=0;i<strLocStrBin.size();i++){
			if(prodId==strLocStrBin.get(i).getProdId()
					&&lsWarehouse.getValue(lsWarehouse.getSelectedIndex()).trim().equals(strLocStrBin.get(i).getWarehouseName().trim())
					&&lsStorageLoc.getValue(lsStorageLoc.getSelectedIndex()).trim().equals(strLocStrBin.get(i).getStorageLocation().trim())
					&&lsStoragebin.getValue(lsStoragebin.getSelectedIndex()).trim().equals(strLocStrBin.get(i).getStorageBin().trim())){
				reorderQty=strLocStrBin.get(i).getReOrderLvlQty();
			}
		}
	}
	
	
	
	
	
	// *********************************** Getter and Setter *********************************************
	
	
	public static ListBox getLsWarehouse() {
		return lsWarehouse;
	}

	public static void setLsWarehouse(ListBox lsWarehouse) {
		InventoryLocationPopUp.lsWarehouse = lsWarehouse;
	}

	public void initializeWarehounse() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new WareHouse());
		oblwarehouse.MakeLive(querry);
	}

	public ObjectListBox<WareHouse> getOblwarehouse() {
		return oblwarehouse;
	}

	public void setOblwarehouse(ObjectListBox<WareHouse> oblwarehouse) {
		this.oblwarehouse = oblwarehouse;
	}

	public ListBox getLsStorageLoc() {
		return lsStorageLoc;
	}

	public void setLsStorageLoc(ListBox lsStorageLoc) {
		this.lsStorageLoc = lsStorageLoc;
	}

	public ListBox getLsStoragebin() {
		return lsStoragebin;
	}

	public void setLsStoragebin(ListBox lsStoragebin) {
		this.lsStoragebin = lsStoragebin;
	}

	public Button getAddButton() {
		return addButton;
	}

	public void setAddButton(Button addButton) {
		this.addButton = addButton;
	}

	public Button getCancelButton() {
		return cancelButton;
	}

	public void setCancelButton(Button cancelButton) {
		this.cancelButton = cancelButton;
	}

	public static double getAvailableQty() {
		return availableQty;
	}

	public static void setAvailableQty(double availableQty) {
		InventoryLocationPopUp.availableQty = availableQty;
	}

	public static double getReorderQty() {
		return reorderQty;
	}

	public static void setReorderQty(double reorderQty) {
		InventoryLocationPopUp.reorderQty = reorderQty;
	}



}
