package com.slicktechnologies.client.views.inventory.recievingnote;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;

import java.util.Vector;

import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;

public class GRNSearchForm extends SearchPopUpScreen<GRN> {

	public IntegerBox ibPOId;
	public IntegerBox ibGRNId;
	public DateComparator dateComparator;
	ObjectListBox<Config> oblGrnGroup;
	ObjectListBox<ConfigCategory> oblGrnCat;
	ObjectListBox<Type> oblGrntype;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Employee> olbEmployee;
	public ObjectListBox<Employee> tbApproverName;
	public ListBox olbStatus;
	public TextBox tbgrntitle;
	public TextBox tbrefId;
	public PersonInfoComposite personInfo;
	ProductInfoComposite pic;

	public GRNSearchForm() {
		super();
		createGui();
		pic.getProdPrice().setVisible(false);
		pic.getUnitOfmeasuerment().setVisible(false);
		tbrefId.setValue("");
	}
	
	public GRNSearchForm(boolean b) {
		super(b);
		createGui();
		pic.getProdPrice().setVisible(false);
		pic.getUnitOfmeasuerment().setVisible(false);
	}

	public void initWidget() {
		ibPOId = new IntegerBox();
		ibGRNId = new IntegerBox();

		dateComparator = new DateComparator("creationDate", new GRN());

		oblGrnGroup = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblGrnGroup, Screen.GRNGROUP);

		oblGrnCat = new ObjectListBox<ConfigCategory>();
		oblGrntype = new ObjectListBox<Type>();
		CategoryTypeFactory.initiateOneManyFunctionality(oblGrnCat, oblGrntype);
		AppUtility.makeTypeListBoxLive(oblGrntype, Screen.GRNCATEGORY);

		olbBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);

		olbEmployee = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		olbEmployee.makeEmployeeLive(AppConstants.INVENTORYMODULE, AppConstants.GRN, "Purchase Engineer");
		
		tbApproverName = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(tbApproverName);
		
		tbgrntitle=new TextBox();

		olbStatus = new ListBox();
		AppUtility.setStatusListBox(olbStatus, GRN.getStatusList());

		personInfo = AppUtility.vendorInfoComposite(new Vendor());

		personInfo.getCustomerId().getHeaderLabel().setText("Vendor Id");
		personInfo.getCustomerName().getHeaderLabel().setText("Vendor Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Vendor Cell");
		
		pic = AppUtility.initiateSalesProductComposite(new SuperProduct());
		
		tbrefId=new TextBox();
	}

	public void createScreen() {
		initWidget();
		FormFieldBuilder builder;

		builder = new FormFieldBuilder("PO ID", ibPOId);
		FormField fibPOId = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("GRN ID", ibGRNId);
		FormField ftbContractId = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("GRN From Date",dateComparator.getFromDate());
		FormField fdateComparator = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("GRN To Date",dateComparator.getToDate());
		FormField fTodateComparator = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("GRN Group", oblGrnGroup);
		FormField foblGrnGroup = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		builder = new FormFieldBuilder("GRN Title", tbgrntitle);
		FormField ftbgrntitle = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		builder = new FormFieldBuilder("GRN Category", oblGrnCat);
		FormField foblGrnCategory = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		builder = new FormFieldBuilder("GRN Type", oblGrntype);
		FormField foblGrnType = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Branch", olbBranch);
		FormField folbBranch = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		builder = new FormFieldBuilder("Purchase Engineer", olbEmployee);
		FormField folbEmployee = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		builder = new FormFieldBuilder("Status", olbStatus);
		FormField folbStatus = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Approver Name", tbApproverName);
		FormField ftbApproverName = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("", personInfo);
		FormField fpersonInfo = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();
		
		builder = new FormFieldBuilder("", pic);
		FormField fpic = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();

		
		builder = new FormFieldBuilder("Ref Id", tbrefId);
		FormField ftbrefId = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		this.fields = new FormField[][] {
				{ ftbContractId,ftbgrntitle, fdateComparator, fTodateComparator },
				{ foblGrnGroup, foblGrnCategory, foblGrnType, folbBranch },
				{ folbEmployee, ftbApproverName, folbStatus, fibPOId }, { fpersonInfo ,ftbrefId},{fpic} };
	}

	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		if (dateComparator.getValue() != null) {
			filtervec.addAll(dateComparator.getValue());
		}

		if (personInfo.getIdValue() != -1) {
			temp = new Filter();
			temp.setIntValue(personInfo.getIdValue());
			temp.setQuerryString("vendorInfo.count");
			filtervec.add(temp);
		}
		if (!(personInfo.getFullNameValue().equals(""))) {
			temp = new Filter();
			temp.setStringValue(personInfo.getFullNameValue());
			temp.setQuerryString("vendorInfo.fullName");
			filtervec.add(temp);
		}
		if (personInfo.getCellValue() != -1l) {
			temp = new Filter();
			temp.setLongValue(personInfo.getCellValue());
			temp.setQuerryString("vendorInfo.cellNumber");
			filtervec.add(temp);
		}

		if (olbBranch.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if (olbEmployee.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbEmployee.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		
		if(!tbrefId.getValue().equals("")){
			temp=new Filter();
			temp.setStringValue(tbrefId.getValue());
			temp.setQuerryString("refNo");
			filtervec.add(temp);
		}
		
		if (tbApproverName.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(tbApproverName.getValue().trim());
			temp.setQuerryString("approverName");
			filtervec.add(temp);
		}
		if (ibPOId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibPOId.getValue());
			temp.setQuerryString("poNo");
			filtervec.add(temp);
		}
		if (olbStatus.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbStatus
					.getValue(olbStatus.getSelectedIndex()).trim());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		if (ibGRNId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibGRNId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if (!tbgrntitle.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(tbgrntitle.getValue());
			temp.setQuerryString("Title");
			filtervec.add(temp);
		}
		
		
		if (oblGrnCat.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblGrnCat.getValue());
			temp.setQuerryString("grnCategory");
			filtervec.add(temp);
		}
		if (oblGrnGroup.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblGrnGroup.getValue());
			temp.setQuerryString("grnGroup");
			filtervec.add(temp);
		}
		if (oblGrntype.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblGrntype.getValue());
			temp.setQuerryString("grnType");
			filtervec.add(temp);
		}
		if (!pic.getProdCode().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(pic.getProdCode().getValue());
			temp.setQuerryString("items.productdetails.productCode");
			filtervec.add(temp);
		}

		if (!pic.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(pic.getProdID().getValue()));
			temp.setQuerryString("items.productdetails.productID");
			filtervec.add(temp);
		}

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new GRN());
		return querry;
	}

	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}
}
