package com.slicktechnologies.client.views.inventory.productinventorytransaction;

import com.simplesoftwares.client.library.appstructure.SuperModel;

public class ClosingReport extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5709472156252139543L;

	String branch;
	String warehouseName;
	String storageLocation;
	String storageBin;
	int productId;
	String productCode;
	String productName;
	
	double openingStock;
	double totalGrnQty;
	double totalMinQty;
	double totalMmnInQty;
	double totalMmnOutQty;
	double totalMmnTransferInQty;
	double totalMmnTransferOutQty;
	double totalPhysicalTransferQty;
	


	double closingStock;
	
	
	public ClosingReport() {
		// TODO Auto-generated constructor stub
	}


	public String getBranch() {
		return branch;
	}


	public void setBranch(String branch) {
		this.branch = branch;
	}


	public String getWarehouseName() {
		return warehouseName;
	}


	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}


	public String getStorageLocation() {
		return storageLocation;
	}


	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}


	public String getStorageBin() {
		return storageBin;
	}


	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}


	public int getProductId() {
		return productId;
	}


	public void setProductId(int productId) {
		this.productId = productId;
	}


	public String getProductCode() {
		return productCode;
	}


	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public double getOpeningStock() {
		return openingStock;
	}


	public void setOpeningStock(double openingStock) {
		this.openingStock = openingStock;
	}


	public double getTotalGrnQty() {
		return totalGrnQty;
	}


	public void setTotalGrnQty(double totalGrnQty) {
		this.totalGrnQty = totalGrnQty;
	}


	public double getTotalMinQty() {
		return totalMinQty;
	}


	public void setTotalMinQty(double totalMinQty) {
		this.totalMinQty = totalMinQty;
	}


	public double getTotalMmnInQty() {
		return totalMmnInQty;
	}


	public void setTotalMmnInQty(double totalMmnInQty) {
		this.totalMmnInQty = totalMmnInQty;
	}


	public double getTotalMmnOutQty() {
		return totalMmnOutQty;
	}


	public void setTotalMmnOutQty(double totalMmnOutQty) {
		this.totalMmnOutQty = totalMmnOutQty;
	}


	public double getTotalMmnTransferInQty() {
		return totalMmnTransferInQty;
	}


	public void setTotalMmnTransferInQty(double totalMmnTransferInQty) {
		this.totalMmnTransferInQty = totalMmnTransferInQty;
	}


	public double getTotalMmnTransferOutQty() {
		return totalMmnTransferOutQty;
	}


	public void setTotalMmnTransferOutQty(double totalMmnTransferOutQty) {
		this.totalMmnTransferOutQty = totalMmnTransferOutQty;
	}


	public double getClosingStock() {
		return closingStock;
	}


	public void setClosingStock(double closingStock) {
		this.closingStock = closingStock;
	}


	public double getTotalPhysicalTransferQty() {
		return totalPhysicalTransferQty;
	}


	public void setTotalPhysicalTransferQty(double totalPhysicalTransferQty) {
		this.totalPhysicalTransferQty = totalPhysicalTransferQty;
	}
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	

}
