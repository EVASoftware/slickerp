package com.slicktechnologies.client.views.inventory.productinventorytransaction;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.inventory.MaterialMovementType;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class InventoryProductTransactionSearchProxy  extends SearchPopUpScreen<ProductInventoryTransaction> implements ChangeHandler{
	
	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	ProductInfoComposite productInfoComposite;
	
	IntegerBox ibMinId;
	ListBox documnetTypeCol;
	ObjectListBox<WareHouse> oblWarehouse;
	ListBox oblStorageLocation;
	ListBox oblStorageBin;
	
	/**
	 * Date : 18-01-2017 By Anil
	 * Added Branch Search
	 */
	ObjectListBox<Branch> olbBranch;
	
	/**
	 * Date : 01-03-2017 By ANIL
	 * Adding Sub type Search
	 */
	ObjectListBox<Config> olbDocumnetSubType;
	
	
	/**
	 * Date : 02-03-2017 By ANIl
	 * Added date search
	 */
	DateComparator dateComparator;
	
	
	public InventoryProductTransactionSearchProxy() {
		super();
		createGui();
	}
	public InventoryProductTransactionSearchProxy(boolean b) {
		super(b);
		createGui();
	}
	
	private void initializeWidget(){
		productInfoComposite = AppUtility.initiateSalesProductComposite(new SuperProduct());
		
		oblWarehouse = new ObjectListBox<WareHouse>();
		AppUtility.initializeWarehouse(oblWarehouse);
		oblWarehouse.addChangeHandler(this);
		
		oblStorageLocation = new ListBox();
		oblStorageLocation.addItem("--SELECT--");
		oblStorageLocation.addChangeHandler(this);
		
		oblStorageBin = new ListBox();
		oblStorageBin.addItem("--SELECT--");
		
		ibMinId = new IntegerBox();
		
		documnetTypeCol=new ListBox();
		AppUtility.setStatusListBox(documnetTypeCol, Approvals.getDocTypeList());
		
		olbBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		olbDocumnetSubType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbDocumnetSubType, Screen.MMNDIRECTION);
		
		dateComparator= new DateComparator("inventoryTransactionDateTime",new ProductInventoryTransaction());
	}
	
	@Override
	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("Warehouse", oblWarehouse);
		FormField fwarehouse = fbuilder.setMandatory(false).setMandatoryMsg("WareHouse Name Is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Storage Location", oblStorageLocation);
		FormField flocation = fbuilder.setMandatory(false).setMandatoryMsg("Storage Location Is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Storage Bin", oblStorageBin);
		FormField fbin = fbuilder.setMandatory(false).setMandatoryMsg("Storage Bin Is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Document ID", ibMinId);
		FormField fibMinId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Document Type",documnetTypeCol);
		FormField fdocumnetTypeCol =fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Branch", olbBranch);
		FormField folbBranch = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Sub Type", olbDocumnetSubType);
		FormField foblSubType = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("From Date (Transaction Date)",dateComparator.getFromDate());
		FormField fdateComparator= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date (Transaction Date)",dateComparator.getToDate());
		FormField fdateComparator1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = { 
				{ fproductInfoComposite },
				{ fibMinId,fdocumnetTypeCol,foblSubType,folbBranch},
				{fwarehouse ,flocation, fbin },
				{fdateComparator,fdateComparator1}
		};
		this.fields = formfield;
	}
	
	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;
		
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}


		if (!productInfoComposite.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
			temp.setQuerryString("productId");
			filtervec.add(temp);
		}
		
		
		if (ibMinId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibMinId.getValue());
			temp.setQuerryString("documentId");
			filtervec.add(temp);
		}
		
		if(documnetTypeCol.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(documnetTypeCol.getValue(documnetTypeCol.getSelectedIndex()).trim());
			temp.setQuerryString("documentType");
			filtervec.add(temp);
		}
		
		
		if (oblWarehouse.getSelectedIndex()!=0) {
			temp = new Filter();
			temp.setStringValue(oblWarehouse.getValue(oblWarehouse.getSelectedIndex()).trim());
			temp.setQuerryString("warehouseName");
			filtervec.add(temp);
		}
		
		if (oblStorageLocation.getSelectedIndex()!=0) {
			temp = new Filter();
			temp.setStringValue(oblStorageLocation.getValue(oblStorageLocation.getSelectedIndex()).trim());
			temp.setQuerryString("storageLocation");
			filtervec.add(temp);
		}
		
		if (oblStorageBin.getSelectedIndex()!=0) {
			temp = new Filter();
			temp.setStringValue(oblStorageBin.getValue(oblStorageBin.getSelectedIndex()).trim());
			temp.setQuerryString("storageBin");
			filtervec.add(temp);
		}
		
		if (olbBranch.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbBranch.getValue(olbBranch.getSelectedIndex()).trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		
		if (olbDocumnetSubType.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbDocumnetSubType.getValue(olbDocumnetSubType.getSelectedIndex()).trim());
			temp.setQuerryString("documentSubType");
			filtervec.add(temp);
		}

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProductInventoryTransaction());
		return querry;
	}
	@Override
	public boolean validate() {
		
//		if (LoginPresenter.branchRestrictionFlag) {
//			if (olbBranch.getSelectedIndex() == 0) {
//				showDialogMessage("Select Branch");
//				return false;
//			}
//		}
		
		
		//Ashwini Patil Date:25-01-2024 	
		
		
		boolean filterAppliedFlag=false;
		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null||
				olbBranch.getSelectedIndex()!=0 ||documnetTypeCol.getSelectedIndex()!=0 ||
				oblWarehouse.getSelectedIndex()!=0 ||oblStorageLocation.getSelectedIndex()!=0 ||olbDocumnetSubType.getSelectedIndex()!=0 || 
			    oblStorageBin.getSelectedIndex()!=0 ||productInfoComposite.getValue()!=null|| ibMinId.getValue()!= null){
			
							filterAppliedFlag=true;
		}
						
		if(!filterAppliedFlag){
				showDialogMessage("Please apply at least one filter!");
				return false;
		}
					
		if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)){
				int diffdays = AppUtility.getDifferenceDays(dateComparator.getFromDate().getValue(), dateComparator.getToDate().getValue());
				Console.log("diffdays "+diffdays);
				if(diffdays>31){
						showDialogMessage("Please select from date and to date range within 30 days");
						return false;
				}
		}

		
		
		return true;
	}
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(oblWarehouse)){
			retriveStorageLocation();
	}
	if(event.getSource().equals(oblStorageLocation)){
			retriveStorageBin();
	}
	}

	
	private void retriveStorageLocation(){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("warehouseName");
		filter.setStringValue(oblWarehouse.getValue(oblWarehouse.getSelectedIndex()));
		filtervec.add(filter);
		
		
		/**
		 * nidhi
		 * 22-05-201
		 * for load only active status location
		 */
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		/**
		 * end
		 */
		
		query.setFilters(filtervec);
		query.setQuerryObject(new StorageLocation());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				oblStorageLocation.clear();
				oblStorageBin.clear();
				oblStorageLocation.addItem("--SELECT--");
				oblStorageBin.addItem("--SELECT--");
				for (SuperModel model : result) {
					StorageLocation entity = (StorageLocation) model;
//					storageList.add(entity.getBusinessUnitName());
					oblStorageLocation.addItem(entity.getBusinessUnitName());
					System.out.println("Location :"+entity.getBusinessUnitName());
				}
				
			}
		});
	}
	
	// ****************************** Retrieving Storage Bin From Storage location **********************************
	
	private void retriveStorageBin(){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("storagelocation");
		filter.setStringValue(oblStorageLocation.getValue(oblStorageLocation.getSelectedIndex()));
		filtervec.add(filter);
		
		/**
		 * nidhi
		 * 22-05-201
		 * for load only active status location
		 */
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		/**
		 * end
		 */
		
		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				oblStorageBin.clear();
				oblStorageBin.addItem("--SELECT--");
				for (SuperModel model : result) {
					Storagebin entity = (Storagebin) model;
					oblStorageBin.addItem(entity.getBinName());
					System.out.println("BIN :"+entity.getBinName());
				}
				
			}
		});
	}
	
}
