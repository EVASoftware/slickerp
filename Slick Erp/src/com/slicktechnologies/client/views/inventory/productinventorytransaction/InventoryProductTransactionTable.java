package com.slicktechnologies.client.views.inventory.productinventorytransaction;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.popups.ProductSerailNumberPopup;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class InventoryProductTransactionTable extends SuperTable<ProductInventoryTransaction> implements ClickHandler {
	
	TextColumn<ProductInventoryTransaction> getColumnInvTranId;
	TextColumn<ProductInventoryTransaction> getColumnInvTranProdId;
	TextColumn<ProductInventoryTransaction> getColumnInvTranProdName;
	TextColumn<ProductInventoryTransaction> getColumnInvTranWarehousName;
	TextColumn<ProductInventoryTransaction> getColumnInvTranStorageLocation;
	TextColumn<ProductInventoryTransaction> getColumnInvTranStorageBin;
	TextColumn<ProductInventoryTransaction> getColumnInvTranprodUOM;
	TextColumn<ProductInventoryTransaction> getColumnInvTranProdQty;
	TextColumn<ProductInventoryTransaction> getColumnInvTranProdPrice;
	TextColumn<ProductInventoryTransaction> getColumnInvTranDocType;
	TextColumn<ProductInventoryTransaction> getColumnInvTranDocId;
	TextColumn<ProductInventoryTransaction> getColumnInvTranDocTitle;
	TextColumn<ProductInventoryTransaction> getColumnInvTranDocDate;
	TextColumn<ProductInventoryTransaction> getColumnInvTranOperation;
	TextColumn<ProductInventoryTransaction> getColumnInvTranOpeningStock;
	TextColumn<ProductInventoryTransaction> getColumnInvTranClosingStock;
	TextColumn<ProductInventoryTransaction> getColumnInvTranDate;
	
	/**
	 * Date : 18-01-2017 By Anil
	 * Added branch Column
	 */
	TextColumn<ProductInventoryTransaction> getColumnInvBranch;
	
	/**
	 * Date: 01-03-2017 By ANIL
	 * MMN Sub Type Column
	 */
	TextColumn<ProductInventoryTransaction> getColumnSubType;
	
	
	/**
	 * nidhi
	 * 21-08-2018
	 * for map serial number
	 */
	Column<ProductInventoryTransaction, String> addProSerialNo;
	Column<ProductInventoryTransaction, String> viewProSerialNo;
	boolean tableState = false;
	ProductSerailNumberPopup prodSerialPopup = new ProductSerailNumberPopup(true,false);
	
	/**
	 * nidhi ||*|| 29-12-2018
	 */
	TextColumn<ProductInventoryTransaction> getViewColumnProductPlannedQty;
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	/** Date 29-11-2019 By Vijay for Lock Seal serial Number ***/
	TextColumn<ProductInventoryTransaction> getcolumnSerialNumber;
	
	TextColumn<ProductInventoryTransaction> getColumnServiceId; //Ashwini Patil Date:28-06-2024 for Rex
	
	
	public InventoryProductTransactionTable() {
		super();
		setHeight("1600px");
		/**
		 * nidhi
		 * 22-08-2018
		 */
		prodSerialPopup.getLblOk().addClickHandler(this);
		prodSerialPopup.getLblCancel().addClickHandler(this);
	}

	@Override
	public void createTable() {
		createColumnInvTranId();
		createColumnInvTranProdId();
		createColumnInvTranProdName();
		createColumnInvTranWarehouseName();
		createColumnInvTranStorageLocation();
		createColumnInvTranStorageBin();
		
		getColumnInvBranch();
		
//		createColumnInvTranProdPrice();
		createColumnInvTranDocType();
		getColumnSubType();
		
		createColumnInvTranDocId();
		createColumnInvTranDocTitle();
		createColumnInvTranDocDate();
		createColumnServiceId();
		createColumnInvTranProdUOM();
		/**
		 *nidhi ||*|| 29-12-2018 for display planned qty
		 */
		if(LoginPresenter.billofMaterialActive){
			addColumnProductViewPlannedQty();
		}
		createColumnInvTranOpeningStock();
		createColumnInvTranOperation();
		createColumnInvTranProdQty();
		createColumnInvTranClosingStock();
		createColumnInvTranDate();
		/**
		 * nidhi
		 * 21-08-2018
		 * for add serial number
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createViewColumnAddProSerialNoColumn();
			createFieldUpdaterViewSerialNo();
		}
		createcolumnSerialNumber();
	}

	
	

	public void addColumnSorting() {
		createColumnSortingInvTranId();
		createColumnSortingInvTranProdId();
		createColumnSortingInvTranProdName();
		createColumnSortingInvTranWarehouseName();
		createColumnSortingInvTranStorageLocation();
		createColumnSortingInvTranStorageBin();
		createColumnSortingInvTranProdPrice();
		createColumnSortingInvTranDocId();
		createColumnSortingInvTranDocTitle();
		createColumnSortingInvTranDocType();
		createColumnSortingOnSubType();
		createColumnSortingInvTranDocDate();
		createColumnSortingInvTranProdUOM();
		createColumnSortingInvTranOpeningStock();
		createColumnSortingInvTranOperation();
		/**
		 *nidhi ||*|| 29-12-2018 for display planned qty
		 */
		if(LoginPresenter.billofMaterialActive){
			createColumnSortingInvTranPlannedProdQty();
		}
		createColumnSortingInvTranProdQty();
		createColumnSortingInvTranClosingStock();
		createColumnSortingInvTranDate();
		createColumnSortingInvBranch();
		
		createcolumnSortingSerialNumber();

	}
	
	
	

	public void createColumnSortingInvTranId() {
		
		List<ProductInventoryTransaction> list=getDataprovider().getList();
		columnSort=new ListHandler<ProductInventoryTransaction>(list);
		Comparator<ProductInventoryTransaction>quote= new Comparator<ProductInventoryTransaction>()
				{

					@Override
					public int compare(ProductInventoryTransaction e1, ProductInventoryTransaction e2) 
					{
						if(e1!=null && e2!=null)
						{
							if(e1.getCount()==e2.getCount())
								return 0;
							else if(e1.getCount()<e2.getCount())
							return 1;
							else
								return -1;
						}
						else
							return 0;
					}

				};
			columnSort.setComparator(getColumnInvTranId, quote);
			table.addColumnSortHandler(columnSort);
	}
	
	private void createColumnSortingInvTranProdId() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranProdId,new Comparator<ProductInventoryTransaction>() {
					@Override
					public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProductId() == e2.getProductId()) {
								return 0;
							}
							if (e1.getProductId() > e2.getProductId()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingInvTranProdName() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranProdName,new Comparator<ProductInventoryTransaction>() {
			@Override
			public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getProductName() != null && e2.getProductName() != null) {
						return e1.getProductName().compareTo(e2.getProductName());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	private void createColumnSortingInvTranWarehouseName() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranWarehousName,new Comparator<ProductInventoryTransaction>() {
			@Override
			public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getWarehouseName() != null && e2.getWarehouseName() != null) {
						return e1.getWarehouseName().compareTo(e2.getWarehouseName());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingInvTranStorageLocation() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranStorageLocation,new Comparator<ProductInventoryTransaction>() {
			@Override
			public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStorageLocation() != null && e2.getStorageLocation() != null) {
						return e1.getStorageLocation().compareTo(e2.getStorageLocation());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingInvTranStorageBin() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranStorageBin,new Comparator<ProductInventoryTransaction>() {
			@Override
			public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStorageBin() != null && e2.getStorageBin() != null) {
						return e1.getStorageBin().compareTo(e2.getStorageBin());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingInvTranProdPrice() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranProdPrice,new Comparator<ProductInventoryTransaction>() {
					@Override
					public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProductPrice() == e2.getProductPrice()) {
								return 0;
							}
							if (e1.getProductPrice() > e2.getProductPrice()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingInvTranDocId() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranDocId,new Comparator<ProductInventoryTransaction>() {
					@Override
					public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
						if (e1 != null && e2 != null) {
							if (e1.getDocumentId() == e2.getDocumentId()) {
								return 0;
							}
							if (e1.getDocumentId() > e2.getDocumentId()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingInvTranDocTitle() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranDocTitle,new Comparator<ProductInventoryTransaction>() {
			@Override
			public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getDocumentTitle() != null && e2.getDocumentTitle() != null) {
						return e1.getDocumentTitle().compareTo(e2.getDocumentTitle());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingInvTranDocType() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranDocType,new Comparator<ProductInventoryTransaction>() {
			@Override
			public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getDocumentType() != null && e2.getDocumentType() != null) {
						return e1.getDocumentType().compareTo(e2.getDocumentType());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void createColumnSortingOnSubType() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnSubType,new Comparator<ProductInventoryTransaction>() {
			@Override
			public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getDocumentSubType() != null&& e2.getDocumentSubType() != null) {
						return e1.getDocumentSubType().compareTo(e2.getDocumentSubType());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}
////////////////////////////
	private void createColumnSortingInvTranDocDate() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranDocDate,new Comparator<ProductInventoryTransaction>() {
			@Override
			public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getDocumentDate() != null&& e2.getDocumentDate() != null) {
						return e1.getDocumentDate().compareTo(e2.getDocumentDate());
					}
				} 
				else {
							return 0;
					}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingInvTranProdUOM() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranprodUOM,new Comparator<ProductInventoryTransaction>() {
			@Override
			public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getProductUOM() != null && e2.getProductUOM() != null) {
						return e1.getProductUOM().compareTo(e2.getProductUOM());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingInvTranOpeningStock() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranOpeningStock,new Comparator<ProductInventoryTransaction>() {
					@Override
					public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
						if (e1 != null && e2 != null) {
							if (e1.getOpeningStock() == e2.getOpeningStock()) {
								return 0;
							}
							if (e1.getOpeningStock() > e2.getOpeningStock()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingInvTranOperation() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranOperation,new Comparator<ProductInventoryTransaction>() {
			@Override
			public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getOperation() != null && e2.getOperation() != null) {
						return e1.getOperation().compareTo(e2.getOperation());
					}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingInvTranProdQty() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranProdQty,new Comparator<ProductInventoryTransaction>() {
					@Override
					public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProductQty() == e2.getProductQty()) {
								return 0;
							}
							if (e1.getProductQty() > e2.getProductQty()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingInvTranClosingStock() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranClosingStock,new Comparator<ProductInventoryTransaction>() {
					@Override
					public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
						if (e1 != null && e2 != null) {
							if (e1.getClosingStock() == e2.getClosingStock()) {
								return 0;
							}
							if (e1.getClosingStock() > e2.getClosingStock()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void createColumnSortingInvTranDate() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranDate,new Comparator<ProductInventoryTransaction>() {
			@Override
			public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCreationDate() != null&& e2.getCreationDate() != null) {
						return e1.getCreationDate().compareTo(e2.getCreationDate());
					}
				} 
				else {
							return 0;
					}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	private void createColumnSortingInvBranch() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvBranch,new Comparator<ProductInventoryTransaction>() {
			@Override
			public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null&& e2.getBranch() != null) {
						return e1.getBranch().compareTo(e2.getBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	
	
	/*********************************************************************************************************************/
	
	private void createColumnInvTranId() {
		getColumnInvTranId=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getColumnInvTranId,"ID");
		table.setColumnWidth(getColumnInvTranId,100,Unit.PX);
		getColumnInvTranId.setSortable(true);
	}

	private void createColumnInvTranProdId() {
		getColumnInvTranProdId=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return object.getProductId()+"";
			}
		};
		table.addColumn(getColumnInvTranProdId,"Prod.ID");
		table.setColumnWidth(getColumnInvTranProdId,100,Unit.PX);
		getColumnInvTranProdId.setSortable(true);
	}

	private void createColumnInvTranProdName() {
		getColumnInvTranProdName=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return object.getProductName();
			}
		};
		table.addColumn(getColumnInvTranProdName,"Name");
		table.setColumnWidth(getColumnInvTranProdName,110,Unit.PX);
		getColumnInvTranProdName.setSortable(true);
	}
	private void createColumnInvTranWarehouseName() {
		getColumnInvTranWarehousName=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return object.getWarehouseName();
			}
		};
		table.addColumn(getColumnInvTranWarehousName,"Warehouse");
		table.setColumnWidth(getColumnInvTranWarehousName,110,Unit.PX);
		getColumnInvTranWarehousName.setSortable(true);
	}

	private void createColumnInvTranStorageLocation() {
		getColumnInvTranStorageLocation=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return object.getStorageLocation();
			}
		};
		table.addColumn(getColumnInvTranStorageLocation,"Str. Loc.");
		table.setColumnWidth(getColumnInvTranStorageLocation,110,Unit.PX);
		getColumnInvTranStorageLocation.setSortable(true);
	}

	private void createColumnInvTranStorageBin() {
		getColumnInvTranStorageBin=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return object.getStorageBin();
			}
		};
		table.addColumn(getColumnInvTranStorageBin,"Str. Bin");
		table.setColumnWidth(getColumnInvTranStorageBin,110,Unit.PX);
		getColumnInvTranStorageBin.setSortable(true);
	}

	private void createColumnInvTranProdUOM() {
		getColumnInvTranprodUOM=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return object.getProductUOM();
			}
		};
		table.addColumn(getColumnInvTranprodUOM,"UOM");
		table.setColumnWidth(getColumnInvTranprodUOM,100,Unit.PX);
		getColumnInvTranprodUOM.setSortable(true);
	}

	private void createColumnInvTranProdQty() {
		getColumnInvTranProdQty=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return object.getProductQty()+"";
			}
		};
		table.addColumn(getColumnInvTranProdQty,"Qty.");
		table.setColumnWidth(getColumnInvTranProdQty,90,Unit.PX);
		getColumnInvTranProdQty.setSortable(true);
	}

	private void createColumnInvTranProdPrice() {
		getColumnInvTranProdPrice=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return object.getProductPrice()+"";
			}
		};
		table.addColumn(getColumnInvTranProdPrice,"Price");
		table.setColumnWidth(getColumnInvTranProdPrice,100,Unit.PX);
		getColumnInvTranProdPrice.setSortable(true);
	}

	private void createColumnInvTranDocType() {
		getColumnInvTranDocType=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return object.getDocumentType();
			}
		};
		table.addColumn(getColumnInvTranDocType,"Doc.Type");
		table.setColumnWidth(getColumnInvTranDocType,110,Unit.PX);
		getColumnInvTranDocType.setSortable(true);
	}
	
	private void getColumnSubType() {
		getColumnSubType=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				if(object.getDocumentSubType()!=null){
					return object.getDocumentSubType();
				}
				return "";
			}
		};
		table.addColumn(getColumnSubType,"Sub Type");
		table.setColumnWidth(getColumnSubType,110,Unit.PX);
		getColumnSubType.setSortable(true);
	}

	private void getColumnInvBranch() {
		getColumnInvBranch=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				if(object.getBranch()!=null){
					return object.getBranch();
				}
				return "";
			}
		};
		table.addColumn(getColumnInvBranch,"Branch");
		table.setColumnWidth(getColumnInvBranch,110,Unit.PX);
		getColumnInvBranch.setSortable(true);
	}
	
	private void createColumnInvTranDocId() {
		getColumnInvTranDocId=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return object.getDocumentId()+"";
			}
		};
		table.addColumn(getColumnInvTranDocId,"Doc.ID");
		table.setColumnWidth(getColumnInvTranDocId,100,Unit.PX);
		getColumnInvTranDocId.setSortable(true);
	}

	private void createColumnInvTranDocTitle() {
		getColumnInvTranDocTitle=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return object.getDocumentTitle();
			}
		};
		table.addColumn(getColumnInvTranDocTitle,"Doc.Title");
		table.setColumnWidth(getColumnInvTranDocTitle,110,Unit.PX);
		getColumnInvTranDocTitle.setSortable(true);
	}

	private void createColumnInvTranDocDate() {
		getColumnInvTranDocDate=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				 String serDate=AppUtility.parseDate(object.getDocumentDate());
				 return serDate;
			}
		};
		table.addColumn(getColumnInvTranDocDate,"Doc.Date");
		table.setColumnWidth(getColumnInvTranDocDate,100,Unit.PX);
		getColumnInvTranDocDate.setSortable(true);
	}

	private void createColumnInvTranOperation() {
		getColumnInvTranOperation=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				if(object.getOperation().equals("Add"))
					return "+";
				else
					return "-";
			}
		};
		table.addColumn(getColumnInvTranOperation,"Action");
		table.setColumnWidth(getColumnInvTranOperation,100,Unit.PX);
		getColumnInvTranOperation.setSortable(true);
	}

	private void createColumnInvTranOpeningStock() {
		getColumnInvTranOpeningStock=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return object.getOpeningStock()+"";
			}
		};
		table.addColumn(getColumnInvTranOpeningStock,"Opn.Stock");
		table.setColumnWidth(getColumnInvTranOpeningStock,100,Unit.PX);
		getColumnInvTranOpeningStock.setSortable(true);
	}

	private void createColumnInvTranClosingStock() {
		getColumnInvTranClosingStock=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return object.getClosingStock()+"";
			}
		};
		table.addColumn(getColumnInvTranClosingStock,"Cls.Stock");
		table.setColumnWidth(getColumnInvTranClosingStock,100,Unit.PX);
		getColumnInvTranClosingStock.setSortable(true);
	}

	private void createColumnInvTranDate() {
		getColumnInvTranDate=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				String serDate=AppUtility.parseDate(object.getCreationDate());
				 return serDate;
				
			}
		};
		table.addColumn(getColumnInvTranDate,"Tran.Date");
		table.setColumnWidth(getColumnInvTranDate,100,Unit.PX);
		getColumnInvTranDate.setSortable(true);
	}

	/*********************************************************************************************************************/
	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}


	public void createFieldUpdaterViewSerialNo() {

		viewProSerialNo.setFieldUpdater(new FieldUpdater<ProductInventoryTransaction, String>() {
			
			@Override
			public void update(int index, ProductInventoryTransaction object,
					String value) {
				

				prodSerialPopup.setProSerialenble(false,false);
				prodSerialPopup.getPopup().setSize("50%", "40%");
				if(object.getProSerialNoDetails().size()>0){
					ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
//					for(int i = 0 ; i < object.getProSerialNoDetails().get(0).size() ; i ++ ){
//						ProductSerialNoMapping pro = new ProductSerialNoMapping();
				System.out.println("size  -- " + serNo.size());
						serNo.addAll(object.getProSerialNoDetails().get(0));
//					}
						prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
					prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
					prodSerialPopup.getProSerNoTable().getTable().redraw();
				}
				prodSerialPopup.showPopUp();
			
			
			}
		});
	
	}
	
	public void createViewColumnAddProSerialNoColumn() {
		ButtonCell btnCell = new ButtonCell();
		viewProSerialNo = new Column<ProductInventoryTransaction, String>(btnCell) {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return "View Product Serial Nos";
			}
		};
		table.addColumn(viewProSerialNo, "");
		table.setColumnWidth(viewProSerialNo,180,Unit.PX);

	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub	
		/**
		 * nidhi
		 * 22-08-2018
		 */
		if(event.getSource() == prodSerialPopup.getLblOk()){
			prodSerialPopup.hidePopUp();
		}
		if(event.getSource()== prodSerialPopup.getLblCancel()){
			prodSerialPopup.hidePopUp();
		}
		
	}
	

	/**
	 * nidhi ||*||
	 */
	
	
	private void addColumnProductViewPlannedQty() {
		getViewColumnProductPlannedQty=new TextColumn<ProductInventoryTransaction>() {
			
			@Override
			public String getValue(ProductInventoryTransaction object) {
				return nf.format(object.getPlannedQty());
			}
		};
		table.addColumn(getViewColumnProductPlannedQty,"Planned Quantity");
		table.setColumnWidth(getViewColumnProductPlannedQty,81,Unit.PX);
	}
	
	private void createColumnSortingInvTranPlannedProdQty() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getColumnInvTranProdQty,new Comparator<ProductInventoryTransaction>() {
					@Override
					public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
						if (e1 != null && e2 != null) {
							if (e1.getProductQty() == e2.getProductQty()) {
								return 0;
							}
							if (e1.getProductQty() > e2.getProductQty()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	private void createcolumnSerialNumber() {

		getcolumnSerialNumber = new TextColumn<ProductInventoryTransaction>() {
			
			@Override
			public String getValue(ProductInventoryTransaction object) {
				if(object.getSerialNumber()!=null){
					return object.getSerialNumber();
				}
				return "NA";
			}
		};
		table.addColumn(getcolumnSerialNumber, "");
		table.setColumnWidth(getcolumnSerialNumber,100,Unit.PX);
	}
	
	private void createcolumnSortingSerialNumber() {
		List<ProductInventoryTransaction> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryTransaction>(list);
		columnSort.setComparator(getcolumnSerialNumber,new Comparator<ProductInventoryTransaction>() {
			@Override
			public int compare(ProductInventoryTransaction e1,ProductInventoryTransaction e2) {
				if (e1 != null && e2 != null) {
					if (e1.getSerialNumber() != null&& e2.getSerialNumber() != null) {
						return e1.getSerialNumber().compareTo(e2.getSerialNumber());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void createColumnServiceId() {
		getColumnServiceId=new TextColumn<ProductInventoryTransaction>() {
			@Override
			public String getValue(ProductInventoryTransaction object) {
				if(object.getServiceId()!=null&&!object.getServiceId().equals("")&&!object.getServiceId().equals("0"))
					return object.getServiceId()+"";
				else 
					return "";
			}
		};
		table.addColumn(getColumnServiceId,"Service ID");
		table.setColumnWidth(getColumnServiceId,100,Unit.PX);
	}
	
}