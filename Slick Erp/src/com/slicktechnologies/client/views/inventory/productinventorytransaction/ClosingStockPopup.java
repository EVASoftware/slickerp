package com.slicktechnologies.client.views.inventory.productinventorytransaction;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.inventory.WareHouse;

public class ClosingStockPopup extends AbsolutePanel{

	public Button btnOne;
	public Button btnTwo;
	public DateBox fromDate;
	public DateBox toDate;
	
	ObjectListBox<WareHouse> olbWarehouse;
	ObjectListBox<Branch> olbBranch;
	
	public ClosingStockPopup() {
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy"); 
		
		btnOne=new Button("Ok");
		btnOne.getElement().getStyle().setWidth(60, Unit.PX);
		btnTwo=new Button("Cancel");
		
		InlineLabel displayMessage = new InlineLabel("From Date");
		add(displayMessage,10,10);
		fromDate=new DateBoxWithYearSelector();
		add(fromDate,10,30);
		fromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		InlineLabel displayMessage1 = new InlineLabel("To Date");
		add(displayMessage1,200,10);
		toDate=new DateBoxWithYearSelector();
		add(toDate,200,30);
		toDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		
		
		InlineLabel displayMessage2 = new InlineLabel("Branch");
		add(displayMessage2,10,70);
		olbBranch=new ObjectListBox<Branch>();
		/**
		 * Date 10-07-2018 by vijay
		 * Des :- for branch level restriction added for branch
		 */
//		AppUtility.makeCustomerBranchListBoxLive(olbBranch);
		AppUtility.makeBranchListBoxLive(olbBranch);
		/**
		 * ends here
		 */
		add(olbBranch,10,90);
		olbBranch.getElement().getStyle().setWidth(175, Unit.PX);
		olbBranch.getElement().getStyle().setHeight(25, Unit.PX);
		
		InlineLabel displayMessage3 = new InlineLabel("Warehouse");
		add(displayMessage3,200,70);
		olbWarehouse=new ObjectListBox<WareHouse>();
		AppUtility.initializeWarehouse(olbWarehouse);
		add(olbWarehouse,200,90);
		olbWarehouse.getElement().getStyle().setWidth(175, Unit.PX);
		olbWarehouse.getElement().getStyle().setHeight(25, Unit.PX);
		
		add(btnOne,120,130);
		add(btnTwo,220,130); 
		setSize("380px", "170px");
		this.getElement().setId("form");
		
	}
	
	public void clear(){
		fromDate.setValue(null);
		toDate.setValue(null);
		olbBranch.setSelectedIndex(0);
		olbWarehouse.setSelectedIndex(0);
	}

	//**************getters and setters ********************
	
	
	
	public Button getBtnOne() {
		return btnOne;
	}

	public ObjectListBox<WareHouse> getOlbWarehouse() {
		return olbWarehouse;
	}

	public void setOlbWarehouse(ObjectListBox<WareHouse> olbWarehouse) {
		this.olbWarehouse = olbWarehouse;
	}

	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}

	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}

	public void setBtnOne(Button btnOne) {
		this.btnOne = btnOne;
	}

	public Button getBtnTwo() {
		return btnTwo;
	}

	public void setBtnTwo(Button btnTwo) {
		this.btnTwo = btnTwo;
	}

	public DateBox getFromDate() {
		return fromDate;
	}

	public void setFromDate(DateBox fromDate) {
		this.fromDate = fromDate;
	}

	public DateBox getToDate() {
		return toDate;
	}

	public void setToDate(DateBox toDate) {
		this.toDate = toDate;
	}
}
