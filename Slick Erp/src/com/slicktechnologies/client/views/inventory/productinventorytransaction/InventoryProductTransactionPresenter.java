package com.slicktechnologies.client.views.inventory.productinventorytransaction;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class InventoryProductTransactionPresenter extends TableScreenPresenter<ProductInventoryTransaction> {
	
	TableScreen<ProductInventoryTransaction>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	GeneralServiceAsync genservice=GWT.create(GeneralService.class);
	
	/**
	 * Date : 01-03-2017 By Anil
	 * 
	 */
	ClosingStockPopup datePopup = new ClosingStockPopup();
	PopupPanel panel=new PopupPanel(true);
	UpdateServiceAsync updateService=GWT.create(UpdateService.class);
	
	public InventoryProductTransactionPresenter(TableScreen<ProductInventoryTransaction> view,ProductInventoryTransaction model) {
		super(view, model);
		form=view;
		form.getSearchpopupscreen().getDwnload().setVisible(false);
//		view.retriveTable(getProductTransactionListQuery());
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PRODUCTINVENTORYTRANSACTION,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
		datePopup.getBtnOne().addClickHandler(this);
		datePopup.getBtnTwo().addClickHandler(this);
	}
	
	public InventoryProductTransactionPresenter(TableScreen<ProductInventoryTransaction> view,ProductInventoryTransaction model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PRODUCTINVENTORYTRANSACTION,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}

	}

	public static void initalize()
	{
		InventoryProductTransactionTable table=new InventoryProductTransactionTable();
		InventoryProductTransactionListForm form=new InventoryProductTransactionListForm(table);
		
		InventoryProductTransactionSearchProxy.staticSuperTable=table;
		InventoryProductTransactionSearchProxy searchPopUp=new InventoryProductTransactionSearchProxy(false);
		form.setSearchpopupscreen(searchPopUp);
		
		InventoryProductTransactionPresenter presenter=new InventoryProductTransactionPresenter(form, new ProductInventoryTransaction());
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	public static void initalize(MyQuerry querry)
	{
		InventoryProductTransactionTable table=new InventoryProductTransactionTable();
		InventoryProductTransactionListForm form=new InventoryProductTransactionListForm(table);
		
		InventoryProductTransactionSearchProxy.staticSuperTable=table;
		InventoryProductTransactionSearchProxy searchPopUp=new InventoryProductTransactionSearchProxy(false);
		form.setSearchpopupscreen(searchPopUp);
		
		InventoryProductTransactionPresenter presenter=new InventoryProductTransactionPresenter(form, new ProductInventoryTransaction(),querry);
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	
	
	public MyQuerry getProductTransactionListQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new ProductInventoryTransaction());
		return quer;
	}
	
	@Override
	protected void makeNewModel() {
		model=new ProductInventoryTransaction();
	}
	
	@Override
	public void reactOnDownload() {
		ArrayList<ProductInventoryTransaction> prodInvTranList=new ArrayList<ProductInventoryTransaction>();
		if(form.getSuperTable().getDataprovider().getList().size()==0)
		{
			form.showDialogMessage("No record in Transaction list to download");
		}
		else
		{
			System.out.println("Super Table List Size :: "+form.getSuperTable().getListDataProvider().getList().size());
			
			List<ProductInventoryTransaction> listProdInvTranList=form.getSuperTable().getListDataProvider().getList();
			System.out.println("product List Size :: "+listProdInvTranList.size());
//			HashSet<ProductInventoryTransaction> hashSet=new HashSet<ProductInventoryTransaction>(listProdInvTranList);
			prodInvTranList.addAll(listProdInvTranList); 
			System.out.println("product ArrayList Size :: "+prodInvTranList.size());
		}
		csvservice.setProInvTranList(prodInvTranList, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}
			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+70;
				Window.open(url, "test", "enabled");
			}
		});
		
	}
	
	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("Closing stock report")){
			panel=new PopupPanel(true);
			datePopup.clear();
			panel.add(datePopup);
			panel.center();
			panel.show();
		}
		if(text.equals("Update")){
			form.showWaitSymbol();
			updateService.updateProductInventoryListIndex(UserConfiguration.getCompanyId(), new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
					form.showDialogMessage("Failed!!!");
				}

				@Override
				public void onSuccess(Void result) {
					form.hideWaitSymbol();
					form.showDialogMessage("Updated Successfully!!!");
				}
			});
		}
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		if(event.getSource().equals(datePopup.getBtnOne())){
			if(datePopup.getFromDate().getValue()==null){
				form.showDialogMessage("Please select from date!");
				return;
			}
			if(datePopup.getToDate().getValue()==null){
				form.showDialogMessage("Please select to date!");
				return;
			}
			
			if(LoginPresenter.branchRestrictionFlag==true&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
				if(datePopup.getOlbWarehouse().getSelectedIndex()==0){
					form.showDialogMessage("Please select Warehouse!");
					return;
				}
			}
			
			String branch=null;
			String warehouse=null;
			if(datePopup.getOlbBranch().getSelectedIndex()!=0){
				branch=datePopup.getOlbBranch().getValue();
			}
			if(datePopup.getOlbWarehouse().getSelectedIndex()!=0){
				warehouse=datePopup.getOlbWarehouse().getValue();
			}
			
			getClosingReport(datePopup.getFromDate().getValue(),datePopup.getToDate().getValue(),branch,warehouse);
			panel.hide();
		}
		if(event.getSource().equals(datePopup.getBtnTwo())){
			panel.hide();
		}
	}

	private void getClosingReport(Date fromDate, Date toDate, final String branch,final  String warehouse) {
		
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		MyQuerry querry=new MyQuerry();
		
//		filter=new Filter();
//		filter.setQuerryString("inventoryTransactionDateTime >=");
//		filter.setDateValue(fromDate);
//		temp.add(filter);
//		
//		filter=new Filter();
//		filter.setQuerryString("inventoryTransactionDateTime <=");
//		filter.setDateValue(toDate);
//		temp.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("creationDate >=");
		filter.setDateValue(fromDate);
		temp.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("creationDate <=");
		filter.setDateValue(toDate);
		temp.add(filter);
		
		if(branch!=null){
			filter=new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(branch);
			temp.add(filter);
		}
		
		if(warehouse!=null){
			filter=new Filter();
			filter.setQuerryString("warehouseName");
			filter.setStringValue(warehouse);
			temp.add(filter);
		}
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new ProductInventoryTransaction());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();	
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				ArrayList<ProductInventoryTransaction> transactionList=new ArrayList<ProductInventoryTransaction>();
				if(result.size()!=0){
					for(SuperModel model:result){
						ProductInventoryTransaction pit=(ProductInventoryTransaction) model;
						transactionList.add(pit);
					}
					Console.log("Transaction List Size : "+transactionList.size());
					final ArrayList<ClosingReport> closingStockReport=getClosingStockReport(transactionList);
					Console.log("Closing Stock List Size : "+closingStockReport.size());
                   /**  date 01-02-2018 added by komal for nbhc closing report requirement **/
					genservice.getProductInventoryViewDetails(UserConfiguration.getCompanyId(), branch, warehouse, new AsyncCallback<ArrayList<ProductInventoryViewDetails>>() {
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							Console.log("falied" + caught);
						}
						@Override
						public void onSuccess(ArrayList<ProductInventoryViewDetails> result) {
							
							ArrayList<ClosingReport> closingStockReport1 = new ArrayList<ClosingReport>();
							if(result.size()!=0){
							ArrayList<ProductInventoryViewDetails> list = new ArrayList<ProductInventoryViewDetails>();
							list.addAll(result);
							for(ProductInventoryViewDetails wh : result){
								for(ClosingReport c : closingStockReport) {
									
							if(c.getWarehouseName().equalsIgnoreCase(wh.getWarehousename()) &&
									c.getStorageLocation().equalsIgnoreCase(wh.getStoragelocation()) &&
									c.getStorageBin().equalsIgnoreCase(wh.getStoragebin()) &&
									c.getProductId() == wh.getProdid()){
								list.remove(wh);						
							  }
							 }
							}
							
							if(list.size()!=0){
								closingStockReport1 = generateClosingReportForOtherProducts(list ,branch);
							}
							if(closingStockReport1.size()!=0){
								closingStockReport.addAll(closingStockReport1);
							}
							}
							if(closingStockReport.size()!=0 || closingStockReport1.size()!=0){
								downloadClosingStockReport(closingStockReport);
							}else{
								form.showDialogMessage("No records found");
							}
							
						}
					});				
					
				}else{
					//form.showDialogMessage("No records found for given duration.");
					genservice.getProductInventoryViewDetails(UserConfiguration.getCompanyId(), branch, warehouse, new AsyncCallback<ArrayList<ProductInventoryViewDetails>>() {
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
						@Override
						public void onSuccess(ArrayList<ProductInventoryViewDetails> result) {
							// TODO Auto-generated method stub
							ArrayList<ProductInventoryViewDetails> list = new ArrayList<ProductInventoryViewDetails>();
							list.addAll(result);
							ArrayList<ClosingReport> closingStockReport1 = new ArrayList<ClosingReport>();
							if(list.size()!=0){
								closingStockReport1 = generateClosingReportForOtherProducts(list ,branch);
							}
							if(closingStockReport1.size()!=0){
								downloadClosingStockReport(closingStockReport1);
							}else{
								form.showDialogMessage("No records found");
							}
							
						}
					});					
				}
			}
		});
		
		
		
	}

	protected void downloadClosingStockReport(ArrayList<ClosingReport> closingStockReport) {
		csvservice.setClosingStockReport(closingStockReport, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}
			@Override
			public void onSuccess(Void result) {
				
				Timer t = new Timer() {
					@Override
					public void run() {
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet"+ "?type=" + 122;
						Window.open(url, "test", "enabled");
					}
				};
				t.schedule(2000);
			}
		});
		
	}

	protected ArrayList<ClosingReport> getClosingStockReport(ArrayList<ProductInventoryTransaction> transactionList) {
		HashSet<Integer> productHS=new HashSet<Integer>();
		HashSet<String> branchHS=new HashSet<String>();
		HashSet<WarehouseInfo> whHS=new HashSet<WarehouseInfo>();
		ArrayList<ClosingReport> closingStockReport=new ArrayList<ClosingReport>();
		
		HashSet<String> warehouseHS=new HashSet<String>();
		
		
		for(int i=0;i<transactionList.size();i++){
			if(transactionList.get(i).getDocumentType().equals("Product Inventory View")){//||transactionList.get(i).getDocumentType().equals(AppConstants.PHYSICALINVENTORY)
				transactionList.remove(i);
				i--;
			}
		}
		Console.log("Updated Transaction List Size : "+transactionList.size());
		
		Comparator<ProductInventoryTransaction> comparator = new Comparator<ProductInventoryTransaction>() {  
			@Override  
			public int compare(ProductInventoryTransaction o1, ProductInventoryTransaction o2) {
				Integer prodId1=o1.getProductId();
				Integer prodId2=o2.getProductId();
				return prodId1.compareTo(prodId2);  
			}  
		};  
		Collections.sort(transactionList,comparator);
		
		for(ProductInventoryTransaction pit:transactionList){
			productHS.add(pit.getProductId());
			branchHS.add(pit.getBranch());
			
			String warehouseInfo=pit.getWarehouseName()+"$"+pit.getStorageLocation()+"$"+pit.getStorageBin();
			warehouseHS.add(warehouseInfo);
//			WarehouseInfo wh=new WarehouseInfo();
//			wh.setWarehouseName(pit.getWarehouseName());
//			wh.setStorageLocation(pit.getStorageLocation());
//			wh.setStorageBin(pit.getStorageBin());
//			whHS.add(wh);
			
				
		}
		ArrayList<Integer> productList=new ArrayList<Integer>(productHS);
		Console.log("No. of Unique Product "+productList.size());
		ArrayList<String> branchList=new ArrayList<String>(branchHS);
		Console.log("No. of Unique Branch "+branchList.size());
		
		ArrayList<String> warehouseList=new ArrayList<String>(warehouseHS);
		Console.log("No. of Unique Warehouse 1 - "+warehouseList.size());
		ArrayList<WarehouseInfo> whList=new ArrayList<WarehouseInfo>();
		for(String warehouse:warehouseList){
			String[] warehouseArr = warehouse.split("[$]");
			
			WarehouseInfo wh=new WarehouseInfo();
			wh.setWarehouseName(warehouseArr[0]);
			wh.setStorageLocation(warehouseArr[1]);
			wh.setStorageBin(warehouseArr[2]);
			whList.add(wh);
		}
//		ArrayList<WarehouseInfo> whList=new ArrayList<WarehouseInfo>(whHS);
		Console.log("No. of Unique Warehouse "+whList.size());
		
		for(String branch:branchList){
			for(WarehouseInfo wh:whList){
				for(Integer productId:productList){
					ArrayList<ProductInventoryTransaction> prodWiseTranList=getProductWiseTransactionList(branch,wh,productId,transactionList);
//					Console.log("###Branch : "+branch+" WH : "+wh.getWarehouseName()+"-"+wh.getStorageLocation()+"-"+wh.getStorageBin()+" "+"Product: "+productId+"--"+prodWiseTranList.size());
					
					if(prodWiseTranList.size()!=0){
						ClosingReport report=generateClosingReport(prodWiseTranList);
						closingStockReport.add(report);
					}
				}
			}
		}
		Console.log("Closing Stock Report Size : "+closingStockReport.size());
		
		return closingStockReport;
	}

	private ClosingReport generateClosingReport(ArrayList<ProductInventoryTransaction> prodWiseTranList) {
		double openingStock=0;
		double totalGrnQty=0;
		double totalMinQty=0;
		double totalMmnInQty=0;
		double totalMmnOutQty=0;
		double totalMmnTransferInQty=0;
		double totalMmnTransferOutQty=0;		
		double closingStock=0;
		double totalPhysicalQty=0;//Ashwini Patil Date:11-07-2023
		
		
		Comparator<ProductInventoryTransaction> comparator = new Comparator<ProductInventoryTransaction>() {  
			@Override  
			public int compare(ProductInventoryTransaction o1, ProductInventoryTransaction o2) {
				Integer count1=o1.getCount();
				Integer count2=o2.getCount();
				
				return count1.compareTo(count2);  
			}  
		};  
		Collections.sort(prodWiseTranList,comparator);
		
		openingStock=prodWiseTranList.get(0).getOpeningStock();
		closingStock=prodWiseTranList.get(prodWiseTranList.size()-1).getClosingStock();
		
		for(ProductInventoryTransaction trans:prodWiseTranList){
			if(trans.getDocumentType().equals(AppConstants.GRN)){
				totalGrnQty=totalGrnQty+trans.getProductQty();
			}else if(trans.getDocumentType().equals(AppConstants.MIN)){
				totalMinQty=totalMinQty+trans.getProductQty();
			}else if(trans.getDocumentType().equals(AppConstants.MMN)){
				if(trans.getDocumentSubType()!=null&&!trans.getDocumentSubType().equals("")){
					if(trans.getDocumentSubType().equals("IN")){
						totalMmnInQty=totalMmnInQty+trans.getProductQty();
					}else if(trans.getDocumentSubType().equals("OUT")){
						totalMmnOutQty=totalMmnOutQty+trans.getProductQty();
					}else if(trans.getDocumentSubType().equals("TRANSFERIN")){
						totalMmnTransferInQty=totalMmnTransferInQty+trans.getProductQty();
					}else if(trans.getDocumentSubType().equals("TRANSFEROUT")){
						totalMmnTransferOutQty=totalMmnTransferOutQty+trans.getProductQty();
					}
				}
			}else if(trans.getDocumentType().equals(AppConstants.PHYSICALINVENTORY)) {
				Console.log("in PHYSICALINVENTORY condition");
				if(trans.getOperation().equals("Add")) {
					totalPhysicalQty=totalPhysicalQty+trans.getProductQty();
				}else {
					totalPhysicalQty=totalPhysicalQty-trans.getProductQty();
				}
									
			}
		}
		
		ClosingReport object=new ClosingReport();
		object.setBranch(prodWiseTranList.get(0).getBranch());
		object.setWarehouseName(prodWiseTranList.get(0).getWarehouseName());
		object.setStorageLocation(prodWiseTranList.get(0).getStorageLocation());
		object.setStorageBin(prodWiseTranList.get(0).getStorageBin());
		object.setProductId(prodWiseTranList.get(0).getProductId());
		object.setProductCode(prodWiseTranList.get(0).getProductCode());
		object.setProductName(prodWiseTranList.get(0).getProductName());
		object.setProductName(prodWiseTranList.get(0).getProductName());
		object.setOpeningStock(openingStock);
		object.setTotalGrnQty(totalGrnQty);
		object.setTotalMinQty(totalMinQty);
		object.setTotalMmnInQty(totalMmnInQty);
		object.setTotalMmnOutQty(totalMmnOutQty);
		object.setTotalMmnTransferInQty(totalMmnTransferInQty);
		object.setTotalMmnTransferOutQty(totalMmnTransferOutQty);
		object.setTotalPhysicalTransferQty(totalPhysicalQty);
		object.setClosingStock(closingStock);
		
		return object;
	}

	private ArrayList<ProductInventoryTransaction> getProductWiseTransactionList(String branch, WarehouseInfo wh, Integer productId,ArrayList<ProductInventoryTransaction> transactionList) {
		ArrayList<ProductInventoryTransaction> prodWiseTranList=new ArrayList<ProductInventoryTransaction>();
		
		for(ProductInventoryTransaction pit:transactionList){
			if(branch.equals(pit.getBranch())
				&&wh.getWarehouseName().equals(pit.getWarehouseName())
				&&wh.getStorageLocation().equals(pit.getStorageLocation())
				&&wh.getStorageBin().equals(pit.getStorageBin())
				&&productId==pit.getProductId()){
				
				prodWiseTranList.add(pit);
			}
		}
		
		Comparator<ProductInventoryTransaction> comparator = new Comparator<ProductInventoryTransaction>() {  
			@Override  
			public int compare(ProductInventoryTransaction o1, ProductInventoryTransaction o2) {
				return o1.getCreationDate().compareTo(o2.getCreationDate());  
			}  
		};  
		Collections.sort(prodWiseTranList,comparator);
		
		return prodWiseTranList;
	}
	
//	date 01-02-2018 added by komal for nbhc 
	private ArrayList<ClosingReport>  generateClosingReportForOtherProducts(ArrayList<ProductInventoryViewDetails> prodViewList ,String branch) {
		ArrayList<ClosingReport> list = new ArrayList<ClosingReport>();
		ClosingReport object;
		for(ProductInventoryViewDetails prodView : prodViewList){
		object=new ClosingReport();
		if(branch == null){
			object.setBranch(prodView.getCreatedBy());
		}else{
			object.setBranch(branch);
		}
		
		object.setWarehouseName(prodView.getWarehousename());
		object.setStorageLocation(prodView.getStoragelocation());
		object.setStorageBin(prodView.getStoragebin());
		object.setProductId(prodView.getProdid());
		object.setProductCode(prodView.getProdcode());
		object.setProductName(prodView.getProdname());
		object.setOpeningStock(prodView.getAvailableqty());
		object.setTotalGrnQty(0);
		object.setTotalMinQty(0);
		object.setTotalMmnInQty(0);
		object.setTotalMmnOutQty(0);
		object.setTotalMmnTransferInQty(0);
		object.setTotalMmnTransferOutQty(0);
		object.setTotalPhysicalTransferQty(0);
		object.setClosingStock(prodView.getAvailableqty());
		list.add(object);
		}
		return list;
	}
	
	

}
