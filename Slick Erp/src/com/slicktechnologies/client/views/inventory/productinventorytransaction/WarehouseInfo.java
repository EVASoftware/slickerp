package com.slicktechnologies.client.views.inventory.productinventorytransaction;

public class WarehouseInfo {

	String warehouseName;
	String storageLocation;
	String storageBin;
	
	public WarehouseInfo() {
		// TODO Auto-generated constructor stub
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public String getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	public String getStorageBin() {
		return storageBin;
	}

	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}
}
