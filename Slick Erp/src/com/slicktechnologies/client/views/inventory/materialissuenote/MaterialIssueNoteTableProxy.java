package com.slicktechnologies.client.views.inventory.materialissuenote;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;

public class MaterialIssueNoteTableProxy extends SuperTable<MaterialIssueNote>{
	
	TextColumn<MaterialIssueNote> getColumnMinId;
	TextColumn<MaterialIssueNote> getColumnMinDate;
	TextColumn<MaterialIssueNote> getColumnMinReqDate;
	TextColumn<MaterialIssueNote> getColumnMinTitle;
	TextColumn<MaterialIssueNote> getColumnMinCategory;
	TextColumn<MaterialIssueNote> getColumnMinType;
	TextColumn<MaterialIssueNote> getColumnMinMrnId;
	TextColumn<MaterialIssueNote> getColumnMinSoId;
	
	/**
	 * Date : 17/01/2017
	 * Rohan added this service id in table for NBHC
	 */
	
	TextColumn<MaterialIssueNote> getColumnMinSerivceId;
	
	TextColumn<MaterialIssueNote> getCustomerCellNumberColumn;
	TextColumn<MaterialIssueNote> getCustomerFullNameColumn;
	TextColumn<MaterialIssueNote> getCustomerIdColumn;
	
	/**
	 * ends here 
	 */
	
	TextColumn<MaterialIssueNote> getColumnMinWoId;
	TextColumn<MaterialIssueNote> getColumnMinProject;
	TextColumn<MaterialIssueNote> getColumnMinBranch;
	TextColumn<MaterialIssueNote> getColumnMinSalesPerson;
	TextColumn<MaterialIssueNote> getColumnMinWarehouse;
	TextColumn<MaterialIssueNote> getColumnMinIssuedBy;
	TextColumn<MaterialIssueNote> getColumnMinApprover;
	TextColumn<MaterialIssueNote> getColumnMinStatus;
	
	public MaterialIssueNoteTableProxy() {
		super();
	}
	
	@Override
	public void createTable() {
		addColumnMinId();
		addColumnMinTitle();
		addColumnMinDate();
		addColumnMinMrnId();
		addColumnMinWoId();
		addColumnMinSoId();
		addColumnMinServiceId();
		addColumngetCustomerId();
		addColumngetCustomerFullName();
		addColumngetCustomerCellNumber();
		addColumnMinCategory();
		addColumnMinType();
		addColumnMinIssuedBy();
		addColumnMinApprover();
		addColumnMinProject();
		addColumnMinBranch();
		addColumnMinStatus();
	
		
	}

/**********************************************************************************************************************/
	private void addColumnMinId() {
		getColumnMinId = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getColumnMinId, "MIN ID");
		table.setColumnWidth(getColumnMinId,100,Unit.PX);
		getColumnMinId.setSortable(true);
	}

	private void addColumnMinDate() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
//		DatePickerCell date = new DatePickerCell(fmt);
		getColumnMinDate = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getMinDate()!= null) {
					return fmt.format(object.getMinDate());
				}
				return null;
			}
		};
		table.addColumn(getColumnMinDate, "MIN Date");
		table.setColumnWidth(getColumnMinDate,120,Unit.PX);
		getColumnMinDate.setSortable(true);
	}

	private void addColumnMinTitle() {
		getColumnMinTitle = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getMinTitle()!=null){
					return object.getMinTitle();
				}
				return null;
			}
		};
		table.addColumn(getColumnMinTitle, "MIN Title");
		table.setColumnWidth(getColumnMinTitle,110,Unit.PX);
		getColumnMinTitle.setSortable(true);
	}

	private void addColumnMinCategory() {
		getColumnMinCategory = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getMinCategory()!=null){
					return object.getMinCategory();
				}
				return null;
			}
		};
		table.addColumn(getColumnMinCategory, "MIN Category");
		table.setColumnWidth(getColumnMinCategory,130,Unit.PX);
		getColumnMinCategory.setSortable(true);
	}

	private void addColumnMinType() {
		getColumnMinType = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getMinType()!=null){
					return object.getMinType();
				}
				return null;
			}
		};
		table.addColumn(getColumnMinType, "MIN Type");
		table.setColumnWidth(getColumnMinType,130,Unit.PX);
		getColumnMinType.setSortable(true);
	}

	private void addColumnMinProject() {
		getColumnMinProject = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getMinProject()!=null){
					return object.getMinProject();
				}
				return null;
			}
		};
		table.addColumn(getColumnMinProject, "Project");
		table.setColumnWidth(getColumnMinProject,120,Unit.PX);
		getColumnMinProject.setSortable(true);
	}

	private void addColumnMinBranch() {
		getColumnMinBranch = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getBranch()!=null){
					return object.getBranch();
				}
				return null;
			}
		};
		table.addColumn(getColumnMinBranch, "Branch");
		table.setColumnWidth(getColumnMinBranch,90,Unit.PX);
		getColumnMinBranch.setSortable(true);
	}

	private void addColumnMinSalesPerson() {
		getColumnMinSalesPerson = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getMinSalesPerson()!=null){
					return object.getMinSalesPerson();
				}
				return null;
			}
		};
		table.addColumn(getColumnMinSalesPerson, "Sales Person");
		table.setColumnWidth(getColumnMinSalesPerson,120,Unit.PX);
		getColumnMinSalesPerson.setSortable(true);
	}

	private void addColumnMinIssuedBy() {
		getColumnMinIssuedBy = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getEmployee()!=null){
					return object.getEmployee();
				}
				return null;
			}
		};
		table.addColumn(getColumnMinIssuedBy, "Issued By");
		table.setColumnWidth(getColumnMinIssuedBy,120,Unit.PX);
		getColumnMinIssuedBy.setSortable(true);
	}

	private void addColumnMinApprover() {
		getColumnMinApprover = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getApproverName()!=null){
					return object.getApproverName();
				}
				return null;
			}
		};
		table.addColumn(getColumnMinApprover, "Approver");
		table.setColumnWidth(getColumnMinApprover,90,Unit.PX);
		getColumnMinApprover.setSortable(true);
	}

	private void addColumnMinMrnId() {
		getColumnMinMrnId = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getMinMrnId() == -1)
					return "N.A";
				else
					return object.getMinMrnId() + "";
			}
		};
		table.addColumn(getColumnMinMrnId, "MRN Id");
		table.setColumnWidth(getColumnMinMrnId,120,Unit.PX);
		getColumnMinMrnId.setSortable(true);
	}
	private void addColumnMinSoId() {
		getColumnMinSoId = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getMinSoId() == -1)
					return "";
				else
					return object.getMinSoId() + "";
			}
		};
		table.addColumn(getColumnMinSoId, "Order ID");
		table.setColumnWidth(getColumnMinSoId,135,Unit.PX);
		getColumnMinSoId.setSortable(true);
	}

	private void addColumnMinServiceId() {
		getColumnMinSerivceId = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getServiceId() == -1)
					return "";
				else
					return object.getServiceId() + "";
			}
		};
		table.addColumn(getColumnMinSerivceId, "Service ID");
		table.setColumnWidth(getColumnMinSerivceId,135,Unit.PX);
		getColumnMinSerivceId.setSortable(true);
	}
	
	
	

	private void addColumnMinWoId() {
		getColumnMinWoId = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getMinWoId() == -1)
					return "";
				else
					return object.getMinWoId() + "";
			}
		};
		table.addColumn(getColumnMinWoId, "Work Order ID");
		table.setColumnWidth(getColumnMinWoId,150,Unit.PX);
		getColumnMinWoId.setSortable(true);
	}

	private void addColumnMinStatus() {
		getColumnMinStatus = new TextColumn<MaterialIssueNote>() {
			@Override
			public String getValue(MaterialIssueNote object) {
				if (object.getStatus()!=null){
					return object.getStatus();
				}
				return null;
			}
		};
		table.addColumn(getColumnMinStatus, "Status");
		table.setColumnWidth(getColumnMinStatus,90,Unit.PX);
		getColumnMinStatus.setSortable(true);
	}


	
	
	/***********************************************************************************************************/
	
	
	
	public void addColumnSorting() {
		addColumnSortingMinId();
		addColumnSortingMinDate();
		addColumnSortingMinTitle();
		addColumnSortingMinCategory();
		addColumnSortingMinType();
		addColumnSortingMinProject();
		addColumnSortingMinBranch();
		addColumnSortingMinSalesPerson();
		addColumnSortingMinIssuedBy();
		addColumnSortingMinApprover();
		addColumnSortingMinWareHouse();
		addColumnSortingMinMrnId();
		addColumnSortingMinSoId();
		addColumnSortingMinWoId();
		addColumnSortingMinStatus();
		
		addColumnSortingMinServiceId();
	}	
	

	/***********************************************Column Sorting***************************************/	
	private void addColumnSortingMinId() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinId, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMinDate() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinDate, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMinDate() != null && e2.getMinDate() != null) {
						return e1.getMinDate().compareTo(e2.getMinDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMinMrnId() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinMrnId, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMinSoId() == e2.getMinSoId()) {
						return 0;
					}
					if (e1.getMinSoId() > e2.getMinSoId()) {
						return 1;
					} 
					else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMinSoId() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinSoId, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMinSoId() == e2.getMinSoId()) {
						return 0;
					}
					if (e1.getMinSoId() > e2.getMinSoId()) {
						return 1;
					} 
					else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	
	private void addColumnSortingMinServiceId() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinSerivceId, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceId() == e2.getServiceId()) {
						return 0;
					}
					if (e1.getServiceId() > e2.getServiceId()) {
						return 1;
					} 
					else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	
	
	private void addColumnSortingMinWoId() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinWoId, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMinWoId() == e2.getMinWoId()) {
						return 0;
					}
					if (e1.getMinWoId() > e2.getMinWoId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMinTitle() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinTitle, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMinTitle() != null && e2.getMinTitle() != null) {
						return e1.getMinTitle().compareTo(e2.getMinTitle());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMinCategory() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinCategory, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMinCategory() != null && e2.getMinCategory() != null) {
						return e1.getMinCategory().compareTo(e2.getMinCategory());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMinType() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinType, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMinType() != null && e2.getMinType() != null) {
						return e1.getMinType().compareTo(e2.getMinType());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnSortingMinProject() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinProject, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMinProject() != null && e2.getMinProject() != null) {
						return e1.getMinProject().compareTo(e2.getMinProject());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMinBranch() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinBranch, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null && e2.getBranch() != null) {
						return e1.getBranch().compareTo(e2.getBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMinSalesPerson() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinSalesPerson, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMinSalesPerson() != null && e2.getMinSalesPerson() != null) {
						return e1.getMinSalesPerson().compareTo(e2.getMinSalesPerson());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMinIssuedBy() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinIssuedBy, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployee() != null && e2.getEmployee() != null) {
						return e1.getEmployee().compareTo(e2.getEmployee());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMinApprover() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinApprover, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getApproverName() != null && e2.getApproverName() != null) {
						return e1.getApproverName().compareTo(e2.getApproverName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnSortingMinWareHouse() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinWarehouse, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMinWarehouse() != null && e2.getMinWarehouse() != null) {
						return e1.getMinWarehouse().compareTo(e2.getMinWarehouse());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMinStatus() {
		List<MaterialIssueNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialIssueNote>(list);
		columnSort.setComparator(getColumnMinStatus, new Comparator<MaterialIssueNote>() {
			@Override
			public int compare(MaterialIssueNote e1, MaterialIssueNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null && e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}


protected void addSortinggetCustomerId()
{
	List<MaterialIssueNote> list=getDataprovider().getList();
	columnSort=new ListHandler<MaterialIssueNote>(list);
	columnSort.setComparator(getCustomerIdColumn, new Comparator<MaterialIssueNote>()
			{
		@Override
		public int compare(MaterialIssueNote e1,MaterialIssueNote e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getCinfo().getCount()== e2.getCinfo().getCount()){
					return 0;}
				if(e1.getCinfo().getCount()> e2.getCinfo().getCount()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCustomerId()
{
	getCustomerIdColumn=new TextColumn<MaterialIssueNote>()
			{
		@Override
		public String getValue(MaterialIssueNote object)
		{
			if( object.getCinfo().getCount()!=0){
				 return object.getCinfo().getCount()+"";
			}
			else
			{
				return "";
			}
		}
			};
			table.addColumn(getCustomerIdColumn,"Customer ID");
			table.setColumnWidth(getCustomerIdColumn,110,Unit.PX);
			getCustomerIdColumn.setSortable(true);
}


protected void addSortinggetCustomerFullName()
{
	List<MaterialIssueNote> list=getDataprovider().getList();
	columnSort=new ListHandler<MaterialIssueNote>(list);
	columnSort.setComparator(getCustomerFullNameColumn, new Comparator<MaterialIssueNote>()
			{
		@Override
		public int compare(MaterialIssueNote e1,MaterialIssueNote e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getCinfo().getFullName()!=null && e2.getCinfo().getFullName()!=null){
					return e1.getCinfo().getFullName().compareTo(e2.getCinfo().getFullName());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);

}
protected void addColumngetCustomerFullName()
{
	getCustomerFullNameColumn=new TextColumn<MaterialIssueNote>()
			{
		@Override
		public String getValue(MaterialIssueNote object)
		{
			if(object.getCinfo().getFullName()!= null)
			{
				return object.getCinfo().getFullName();
			}
			else
			{
				return "";
			}
			
		}
			};
			table.addColumn(getCustomerFullNameColumn,"Customer Name");
			table.setColumnWidth(getCustomerFullNameColumn,130,Unit.PX);
			getCustomerFullNameColumn.setSortable(true);
}
protected void addSortinggetCustomerCellNumber()
{
	List<MaterialIssueNote> list=getDataprovider().getList();
	columnSort=new ListHandler<MaterialIssueNote>(list);
	columnSort.setComparator(getCustomerCellNumberColumn, new Comparator<MaterialIssueNote>()
			{
		@Override
		public int compare(MaterialIssueNote e1,MaterialIssueNote e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getCinfo().getCellNumber()== e2.getCinfo().getCellNumber()){
					return 0;}
				if(e1.getCinfo().getCellNumber()> e2.getCinfo().getCellNumber()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetCustomerCellNumber()
{
	getCustomerCellNumberColumn=new TextColumn<MaterialIssueNote>()
			{
		@Override
		public String getValue(MaterialIssueNote object)
		{
			if(object.getCinfo().getCellNumber()!= null)
			{
				return object.getCinfo().getCellNumber()+"";
			}
			else
			{
				return "";
			}
			
		}
			};
			table.addColumn(getCustomerCellNumberColumn,"Customer Cell");
			table.setColumnWidth(getCustomerCellNumberColumn,120,Unit.PX);
			getCustomerCellNumberColumn.setSortable(true);
}

}
