package com.slicktechnologies.client.views.inventory.materialissuenote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNotePresenter;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNoteForm;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;

public class MaterialIssueNotePresenter extends ApprovableFormScreenPresenter<MaterialIssueNote> implements 
		ClickHandler,ChangeHandler,RowCountChangeEvent.Handler  {

	public static  MaterialIssueNoteForm form;
	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	UpdateServiceAsync updateService=GWT.create(UpdateService.class);
	DocumentCancellationPopUp cancelPopup = new DocumentCancellationPopUp();
	PopupPanel cancelpanel ;
	
	double qty;
	int prodID;
	String wareHouse;
	String storageLocation;
	String strbin;
	int cnt =0;
	int i=0;
	
	public MaterialIssueNotePresenter(FormScreen<MaterialIssueNote> view,MaterialIssueNote model) {
		super(view, model);
		form = (MaterialIssueNoteForm) view;
		form.getIbMinSoId().addChangeHandler(this);
		form.getIbMinMrnId().addChangeHandler(this);
		
		cancelPopup.getBtnOk().addClickHandler(this);
		cancelPopup.getBtnCancel().addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.MATERIALISSUENOTE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		if (text.equals(AppConstants.NEW))
			reactToNew();
		if (text.equals("DownLoad"))
			reactOnDownload();
		if (text.equals("Email"))
			reactOnEmail();
		if (text.equals("Create MMN")) {
			reactToMMN();
		}
		if(text.equals(ManageApprovals.APPROVALREQUEST)){
			if(form.validate()==true){/*
				validateRequiredQuantityAtRequestFroApproval(ManageApprovals.APPROVALREQUEST);
			*/

				/**
				 * Date 07-07-2018 By Vijay
				 * Des :- NBHC CCPM As per vaishali mam at this level this validation not required we can send request for approval any moment so below code commented
				 */  
				
//				/**
//				 *  nidhi
//				 *  15-05-2018
//				 *  for GRN Approval Validation
//				 */
//				if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
//					if(form.validateMINDate())	
//					{
//						validateRequiredQuantityAtRequestFroApproval(ManageApprovals.APPROVALREQUEST);
//					}
//				}else{
					validateRequiredQuantityAtRequestFroApproval(ManageApprovals.APPROVALREQUEST);
//				}
				
			}
			else{
				form.showDialogMessage("Please fill mandatory fields.");
			}
		}
		
		if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST)){
			form.getManageapproval().reactToApprovalRequestCancel();
		}
		
		if (text.equals(AppConstants.CANCELMIN)) {
			
			
			if(!model.getStatus().equals(MaterialIssueNote.REQUESTED)){
				cancelPopup.getPaymentDate().setValue(new Date()+"");
				cancelPopup.getPaymentID().setValue(model.getCount()+"");
				cancelPopup.getPaymentStatus().setValue(model.getStatus());
				cancelpanel=new PopupPanel(true);
				cancelpanel.add(cancelPopup);
				cancelpanel.show();
				cancelpanel.center();
			}else{
				form.showDialogMessage("Cancel approval request to cancel the MIN.");
			}
		}
	
		if(text.equals(ManageApprovals.SUBMIT)){
			if(form.validate()==true){
//				form.getManageapproval().reactToSubmit();
				validateRequiredQuantityAtRequestFroApproval(ManageApprovals.SUBMIT);
			}
			else{
				form.showDialogMessage("Please select mandatory fields.");
			}
		}
	}
	
	
	
	public static MaterialIssueNoteForm initialize()
	{
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Material Issue Note",Screen.MATERIALISSUENOTE); 
		MaterialIssueNoteForm form=new  MaterialIssueNoteForm();

		MaterialIssueNoteTableProxy gentable=new MaterialIssueNoteTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		
		MaterialIssueNoteSerachProxy.staticSuperTable=gentable;
		MaterialIssueNoteSerachProxy searchpopup=new MaterialIssueNoteSerachProxy();
		form.setSearchpopupscreen(searchpopup);

	    MaterialIssueNotePresenter  presenter=new MaterialIssueNotePresenter(form,new MaterialIssueNote());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	public static MaterialIssueNoteForm initialize(MyQuerry querry)
	{
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Material Issue Note",Screen.MATERIALISSUENOTE); 
		MaterialIssueNoteForm form=new  MaterialIssueNoteForm();

		MaterialIssueNoteTableProxy gentable=new MaterialIssueNoteTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		
		MaterialIssueNoteSerachProxy.staticSuperTable=gentable;
		MaterialIssueNoteSerachProxy searchpopup=new MaterialIssueNoteSerachProxy();
		form.setSearchpopupscreen(searchpopup);

	    MaterialIssueNotePresenter  presenter=new MaterialIssueNotePresenter(form,new MaterialIssueNote());
		AppMemory.getAppMemory().stickPnel(form);
		return form;

	}

	
	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
		boolean conf = Window.confirm("Do you really want to send email?");
		if (conf == true) {
			emailService.initiateMINEmail((MaterialIssueNote) model,new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Resource Quota Ended ");
					caught.printStackTrace();
				}

				@Override
				public void onSuccess(Void result) {
					Window.alert("Email Sent Sucessfully !");
				}
			});
		}
	}

	@Override
	protected void makeNewModel() {
		model = new MaterialIssueNote();
	}
	
	private void reactToNew() {
		form.setToNewState();
		initialize();
	}
	
	@Override
	public void reactOnDownload() {
		ArrayList<MaterialIssueNote> minArray = new ArrayList<MaterialIssueNote>();
		List<MaterialIssueNote> list = (List<MaterialIssueNote>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		minArray.addAll(list);
		csvservice.setMaterialIssueNote(minArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {

				/**
				 * nidhi
				 * 16-4-2018
				 * for min custom report download using process configration
				 * 
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MIN", "MINReport")){
					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url = gwt + "csvservlet" + "?type=" + 139;
					Window.open(url, "test", "enabled");
				}else{
					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url = gwt + "csvservlet" + "?type=" + 69;
					Window.open(url, "test", "enabled");
				}
				
			
			}
		});
	}
	
	
	@Override
	public void onClick(ClickEvent event) {
			super.onClick(event);
			if(event.getSource()==cancelPopup.getBtnOk()){
				cancelpanel.hide();
				if(model.getStatus().equals(MaterialIssueNote.CREATED)){
					changeStatus(MaterialIssueNote.CANCELLED);
				}else if(model.getStatus().equals(MaterialIssueNote.APPROVED)){
					cancelMin();
				}
			}
			
			if(event.getSource()==cancelPopup.getBtnCancel()){
					cancelpanel.hide();
					cancelPopup.remark.setValue("");
				}
		}
	
	/**
	 * This is used to cancel MIN in approved status and revert stock
	 * Date : 20/1/2017
	 */
	
	
	private void cancelMin() {
		model.setStatus(MaterialIssueNote.CANCELLED);
		model.setMinDescription("MIN ID ="+model.getCount()+" "+"MIN status ="+form.getTbMinStatus().getValue().trim()+"\n"
				+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"
				+"Remark ="+cancelPopup.getRemark().getValue().trim()+" "+"Cancellation Date ="+AppUtility.parseDate(new Date()));
		form.showWaitSymbol();
		updateService.cancelMIN(model, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("An Unexpected Error occured !");
			}
			@Override
			public void onSuccess(String result) {
				form.hideWaitSymbol();
				if(result.equalsIgnoreCase("MIN Cancelled Successfully.")){
					form.showDialogMessage(result);
					form.tbMinStatus.setText(model.getStatus());
					form.getTaMinDescription().setValue(model.getMinDescription());
				}else{
					form.showDialogMessage(result);
					model.setStatus(MaterialIssueNote.APPROVED);
				}
				form.setToViewState();
			}
		});
		
	}
	
	
	public void changeStatus(final String status)
	{
		model.setStatus(status);
		model.setMinDescription("MIN ID ="+model.getCount()+" "+"MIN status ="+form.getTbMinStatus().getValue().trim()+"\n"
				+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"
				+"Remark ="+cancelPopup.getRemark().getValue().trim()+" "+"Cancellation Date ="+AppUtility.parseDate(new Date()));
		
		form.showWaitSymbol();
		async.save(model,new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
				form.hideWaitSymbol();
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				form.hideWaitSymbol();
				form.showDialogMessage("MIN Cancelled Successfully.");
				form.tbMinStatus.setText(model.getStatus());
				form.getTaMinDescription().setValue(model.getMinDescription());
				form.setToViewState();
				
			}
		});
	}
	

	@Override
	public void onChange(ChangeEvent event) {
		if (event.getSource() == form.getIbMinSoId()) {
			if (form.getIbMinSoId().getValue() != null) {
				setDataFromSo();
			}
		}
		if (event.getSource() == form.getIbMinMrnId()) {
			if (form.getIbMinMrnId().getValue() != null) {
				setDataFromMRN();
			}
		}
	}
	
	private void setDataFromMRN() {
		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(form.getIbMinMrnId().getValue()));
		querry.getFilters().add(filter);
		querry.setQuerryObject(new MaterialRequestNote());
		form.showWaitSymbol();
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							MaterialRequestNote mrn=(MaterialRequestNote) result.get(0);;
							if(mrn.getStatus().equals("Approved"))
							{
								
								if(mrn.getProductTableMrn()!=null){
								form.getProductTableMin().getDataprovider().setList(mrn.getProductTableMrn());
								}
								if(mrn.getSubProductTableMrn()!=null){
									form.getSubProductTableMin().getDataprovider().setList(mrn.getSubProductTableMrn());
								}
								form.productInfoComposite.setEnable(false);
								form.getdbMinMrnDate().setValue(mrn.getMrnDate());
								
								if(mrn.getMrnSoId()!=0){
								form.getIbMinSoId().setValue(mrn.getMrnSoId()+"");
								}
								if(mrn.getMrnSoDate()!=null){
								form.getDbMinSoDate().setValue(mrn.getMrnSoDate());
								}
								if(mrn.getMrnDeliveryDate()!=null){
								form.getDbMinDeliveryDate().setValue(mrn.getMrnDeliveryDate());
								}
								if(mrn.getBranch()!=null){
									form.getOblMinBranch().setValue(mrn.getBranch());
									form.getOblMinBranch().setEnabled(false);
								}
								if(mrn.getMrnSalesPerson()!=null){
									form.getOblMinSalesPerson().setValue(mrn.getMrnSalesPerson());
									form.getOblMinSalesPerson().setEnabled(false);
								}
								if(mrn.getApproverName()!=null){
									form.getOblMinApproverName().setValue(mrn.getApproverName());
									form.getOblMinApproverName().setEnabled(false);
								}
								if(mrn.getEmployee()!=null){
									form.getOblMinRequestedBy().setValue(mrn.getEmployee());
									form.getOblMinRequestedBy().setEnabled(false);
								}
								
								form.checkMrnQuantity(Integer.parseInt(form.getIbMinMrnId().getValue()));
							}
							else{
								form.showDialogMessage("MRN ID is Not Approved");
								form.setToNewState();
								form.getProductTableMin().clear();
								form.getSubProductTableMin().clear();
							}
						}else
						{
							form.showDialogMessage("No MRN ID found");
							form.setToNewState();
							form.getProductTableMin().clear();
							form.getSubProductTableMin().clear();
						}
					}

					@Override
					public void onFailure(Throwable caught) {

					}
				});
		form.hideWaitSymbol();
	}


	public void setDataFromSo(){
		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(form.getIbMinSoId().getValue()));
		querry.getFilters().add(filter);
		querry.setQuerryObject(new SalesOrder());
		form.showWaitSymbol();
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							SalesOrder so=(SalesOrder) result.get(0);;
							if(so.getStatus().equals("Approved"))
							{
								form.getIbMinId().setValue(null);
								form.getDbMinDate().setValue(null);
								
								form.getProductTableMin().getDataprovider().setList(so.getItems());

								form.getDbMinSoDate().setValue(so.getCreationDate());
								form.getDbMinDeliveryDate().setValue(so.getDeliveryDate());

								form.getOblMinBranch().setValue(so.getBranch());
								form.getOblMinBranch().setEnabled(false);
								
								if(so.getEmployee()!=null){
								form.getOblMinSalesPerson().setValue(so.getEmployee());
									form.getOblMinSalesPerson().setEnabled(false);
								}
								if(so.getApproverName()!=null){
									form.getOblMinApproverName().setValue(so.getApproverName());
									form.getOblMinApproverName().setEnabled(false);
								}
							}
							else{
								form.showDialogMessage("SO ID is Not Approved");
								form.setToNewState();
								form.getProductTableMin().clear();
								form.getSubProductTableMin().clear();
							}
						}else
						{
							form.showDialogMessage("Sales Order with this number doesnt exist, Please recheck.");
							form.setToNewState();
							form.getProductTableMin().clear();
							form.getSubProductTableMin().clear();
						}
					}

					@Override
					public void onFailure(Throwable caught) {

					}
				});
		form.hideWaitSymbol();
	}



	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		
	}

	
	/******************************************** react to MMN ***********************************************************/

	public void reactToMMN() {
		final MaterialMovementNoteForm form = MaterialMovementNotePresenter.initialize();
		final MaterialMovementNote mmn = new MaterialMovementNote();
		
		mmn.setMmnMinId(model.getCount());
		mmn.setMmnMinDate(model.getMinDate());
		mmn.setMmnWoId(model.getMinWoId());
		mmn.setMmnWoDate(model.getMinWoDate());
		
		mmn.setBranch(model.getBranch());
		mmn.setEmployee(model.getEmployee());
		mmn.setApproverName(model.getApproverName());
		
		
		ArrayList<MaterialProduct> materialList = model.getSubProductTablemin();
		mmn.setSubProductTableMmn(materialList);
		form.checkMinQuantity(model.getCount());

		Timer t = new Timer() {
			@Override
			public void run() {
				form.setToNewState();
				form.updateView(mmn);
				form.getProductInfoComposite().setEnabled(false);
			}
		};
		t.schedule(5000);
	}
	/**
	 * Created by Rohan
	 * Modified BY Rahul Verma on 16 June 2016
	 * Desc: This will validate quantity of product available in Product Inventory View.
	 * @param takeAction : ApprovalRequest or Sumit
	 */
	public void validateRequiredQuantityAtRequestFroApproval(final String takeAction) {
		cnt=0;
		final List<MaterialProduct> list = form.subProductTableMin.getDataprovider().getList();
		for (final MaterialProduct temp : list) {
				System.out.println("list size::::::::::"+list.size());
				prodID=temp.getMaterialProductId();
				

		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(prodID);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProductInventoryView());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
				}
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					qty=temp.getMaterialProductRequiredQuantity();
					wareHouse=temp.getMaterialProductWarehouse();
					storageLocation=temp.getMaterialProductStorageLocation();
					strbin=temp.getMaterialProductStorageBin();
					i=i+1;
					for(SuperModel model:result)
					{
						
						ProductInventoryView sup=(ProductInventoryView)model;
						
										for(int i=0;i<sup.getDetails().size();i++){
											
											if(wareHouse.equals(sup.getDetails().get(i).getWarehousename())){
												if(storageLocation.equals(sup.getDetails().get(i).getStoragelocation())){
											if(strbin.equals(sup.getDetails().get(i).getStoragebin())){
											
											 
											 if(qty >sup.getDetails().get(i).getAvailableqty()){
												form.showDialogMessage("Required quantity is greatter than available quantity."+" Availabe Quantity="+sup.getDetails().get(i).getAvailableqty());
												cnt=cnt+1;
											}
											}}}
										}
				           	}
					if(list.size()==1&&cnt==0){
						if(takeAction.trim().equalsIgnoreCase(ManageApprovals.APPROVALREQUEST)){
							form.getManageapproval().reactToRequestForApproval();
						}else if(takeAction.trim().equalsIgnoreCase(ManageApprovals.SUBMIT)){
							form.getManageapproval().reactToSubmit();
						}
					}
					else if(i==list.size()-1&& cnt==0){
						if(takeAction.trim().equalsIgnoreCase(ManageApprovals.APPROVALREQUEST)){
							form.getManageapproval().reactToRequestForApproval();
						}else if(takeAction.trim().equalsIgnoreCase(ManageApprovals.SUBMIT)){
							form.getManageapproval().reactToSubmit();
						}
					}
				     }
				});
	}	
	}
	
	
}
