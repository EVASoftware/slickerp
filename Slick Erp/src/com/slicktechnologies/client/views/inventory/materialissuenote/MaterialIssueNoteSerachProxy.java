package com.slicktechnologies.client.views.inventory.materialissuenote;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class MaterialIssueNoteSerachProxy extends SearchPopUpScreen<MaterialIssueNote>{
	
	IntegerBox ibMinMrnId;
	IntegerBox ibMinSoId;
	IntegerBox ibMinWoId;
	public DateComparator dateComparator;
	IntegerBox ibMinId;
	TextBox tbMinTitle;
	
	ObjectListBox<ConfigCategory> oblMinCategory;
	ObjectListBox<Type> oblMinType;
	
	ObjectListBox<Branch> oblMinBranch;
	ObjectListBox<HrProject> oblMinProject;
	ObjectListBox<Employee> oblMinSalesPerson;
	ObjectListBox<Employee> oblMinApproverName;
	ObjectListBox<Employee> oblMinIssuedBy;
//	ObjectListBox<WareHouse> oblMinWarehouse;
	
	//  rohan added this for NBHC 
	IntegerBox tbServiceId;
	public PersonInfoComposite personInfo;
	
	ListBox tbMinStatus;
	ProductInfoComposite productInfoComposite;
	
	public MaterialIssueNoteSerachProxy() {
		super();
		createGui();
	}		
	
	private void initializeWidget(){
		ibMinMrnId=new IntegerBox();
		ibMinSoId=new IntegerBox();
		ibMinWoId=new IntegerBox();
		ibMinId=new IntegerBox();
		
		tbMinTitle=new TextBox();
		dateComparator = new DateComparator("minDate", new MaterialIssueNote());	
		
		oblMinCategory= new ObjectListBox<ConfigCategory>();
		oblMinType = new ObjectListBox<Type>();
		CategoryTypeFactory.initiateOneManyFunctionality(oblMinCategory,oblMinType);
		AppUtility.makeTypeListBoxLive(oblMinType,Screen.MINCATEGORY);	
		
		oblMinBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(oblMinBranch);
		
		oblMinSalesPerson = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblMinSalesPerson);
		oblMinSalesPerson.makeEmployeeLive(AppConstants.INVENTORYMODULE, "Material Issue Note", "Requested By");
		
		oblMinApproverName = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(oblMinApproverName);
		
		oblMinIssuedBy = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblMinIssuedBy);
		oblMinIssuedBy.makeEmployeeLive(AppConstants.INVENTORYMODULE, "Material Issue Note", "Issued By");
		
		oblMinProject = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(oblMinProject);
		
//		oblMinWarehouse=new ObjectListBox<WareHouse>();
//		initializeWareHouse();
		
		
		
		tbMinStatus=new ListBox();
		AppUtility.setStatusListBox(tbMinStatus,MaterialIssueNote.getStatusList());
		
		productInfoComposite = AppUtility.initiateMrnProductComposite(new SuperProduct());
		
		tbServiceId=new IntegerBox();
		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
	}

//	private void initializeWareHouse() {
//		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new WareHouse());
//		oblMinWarehouse.MakeLive(querry);
//	}

	@Override
	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder("MRN ID", ibMinMrnId);
		FormField fibMinMrnId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Work Order ID", ibMinWoId);
		FormField fibMinWoId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Order ID", ibMinSoId);
		FormField fibMinSoId = fbuilder.setMandatory(false).setMandatoryMsg("Sales Order Id is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project", oblMinProject);
		FormField foblMinProject = fbuilder.setMandatory(false).setMandatoryMsg("Project is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Sales Person",oblMinSalesPerson );
		FormField foblMinSalesPerson = fbuilder.setMandatory(false).setMandatoryMsg("Sales Person is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MIN ID", ibMinId);
		FormField fibMinId= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MIN Title", tbMinTitle);
		FormField ftbMinTitle = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("From Date", dateComparator.getFromDate());
		FormField fdbMinFrmDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date", dateComparator.getToDate());
		FormField fdbMinToDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MIN Category", oblMinCategory);
		FormField foblMinCategory = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MIN Type", oblMinType);
		FormField foblMinType = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Branch", oblMinBranch);
		FormField foblMinBranch = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Approver", oblMinApproverName);
		FormField foblMinApproverName = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Issued By", oblMinIssuedBy);
		FormField foblMinIssuedBy = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("Warehouse", oblMinWarehouse);
//		FormField foblMinWarehouse= fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", tbMinStatus);
		FormField ftbMinStatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();
		
		
		fbuilder = new FormFieldBuilder("Service ID", tbServiceId);
		FormField ftbServiceId = fbuilder.setMandatory(false).setMandatoryMsg("Service ID is Mandatory!").setRowSpan(0)
		.setColSpan(0).build();
		
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		FormField[][] formfield = { 
				{ fibMinId,ftbMinTitle,fdbMinFrmDate,fdbMinToDate},
				{ foblMinCategory,foblMinType,foblMinBranch,foblMinIssuedBy},
				{foblMinApproverName,foblMinProject,ftbMinStatus},
				{fibMinMrnId,fibMinWoId, fibMinSoId,ftbServiceId},
				{ fproductInfoComposite},
				{fpersonInfo}
		};
		this.fields = formfield;
	}
	
	
	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		if (dateComparator.getValue() != null) {
			filtervec.addAll(dateComparator.getValue());
		}

		if (oblMinBranch.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMinBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if (oblMinCategory.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMinCategory.getValue().trim());
			temp.setQuerryString("minCategory");
			filtervec.add(temp);
		}
		if (oblMinType.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMinType.getValue().trim());
			temp.setQuerryString("minType");
			filtervec.add(temp);
		}
		
		if (oblMinProject.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMinProject.getValue().trim());
			temp.setQuerryString("minProject");
			filtervec.add(temp);
		}
		if (oblMinApproverName.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMinApproverName.getValue().trim());
			temp.setQuerryString("approverName");
			filtervec.add(temp);
		}
		if (oblMinIssuedBy.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMinIssuedBy.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
//		if (oblMinWarehouse.getSelectedIndex() != 0) {
//			temp = new Filter();
//			temp.setStringValue(oblMinWarehouse.getValue().trim());
//			temp.setQuerryString("minWarehouse");
//			filtervec.add(temp);
//		}
		if (ibMinId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibMinId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		if (ibMinMrnId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibMinMrnId.getValue());
			temp.setQuerryString("minMrnId");
			filtervec.add(temp);
		}
		if (ibMinSoId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibMinSoId.getValue());
			temp.setQuerryString("minSoId");
			filtervec.add(temp);
		}
		
		if (tbServiceId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(tbServiceId.getValue());
			temp.setQuerryString("serviceId");
			filtervec.add(temp);
		}
		
		if (ibMinWoId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibMinWoId.getValue());
			temp.setQuerryString("minWoId");
			filtervec.add(temp);
		}
		if (tbMinStatus.getSelectedIndex()!=0) {
			temp = new Filter();
			temp.setStringValue(tbMinStatus.getValue(tbMinStatus.getSelectedIndex()));
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		if (!tbMinTitle.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(tbMinTitle.getValue());
			temp.setQuerryString("minTitle");
			filtervec.add(temp);
		}
		
		if (!productInfoComposite.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
			temp.setQuerryString("subProductTablemin.materialProductId");
			filtervec.add(temp);
		}
		
		
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("cinfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("cinfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("cinfo.cellNumber");
		  filtervec.add(temp);
		  }
		

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new MaterialIssueNote());
		return querry;
	}

	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(oblMinBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}
}
