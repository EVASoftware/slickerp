package com.slicktechnologies.client.views.inventory.materialissuenote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.gargoylesoftware.htmlunit.javascript.host.Console;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.materialreuestnote.ProductTableMrn;
import com.slicktechnologies.client.views.inventory.materialreuestnote.SubProductTableMrn;
import com.slicktechnologies.client.views.inventory.recievingnote.ProductQty;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class MaterialIssueNoteForm extends ApprovableFormScreen<MaterialIssueNote> implements ClickHandler, ChangeHandler {
	final GenricServiceAsync async = GWT.create(GenricService.class);
	ArrayList<ProductQty> mrnList;
	ArrayList<ProductQty> minList;
	ArrayList<ProductQty> compList;
	TextBox ibMinMrnId;
	DateBox dbMinMrnDate;
	TextBox tbServiceId;
	TextBox ibMinSoId;
	DateBox dbMinSoDate;
	TextBox ibMinWoId;
	DateBox dbMinWoDate;
	DateBox dbMinDeliveryDate;
	
	TextBox ibMinId;
	DateBox dbMinDate;
	
	TextBox tbMinTitle;
	
	ObjectListBox<ConfigCategory> oblMinCategory;
	ObjectListBox<Type> oblMinType;
	
	ObjectListBox<Branch> oblMinBranch;
	ObjectListBox<HrProject> oblMinProject;
	ObjectListBox<Employee> oblMinSalesPerson;
	ObjectListBox<Employee> oblMinApproverName;
	ObjectListBox<Employee> oblMinRequestedBy;
	ObjectListBox<Employee> oblMinIssuedBy;
	
	TextBox tbMinStatus;
	TextBox tbMinCommentOnStatusChange;
	
	UploadComposite upload;
	TextArea taMinDescription;
	FormField fMinRefereceDetailGrouping;
	ProductTableMrn productTableMin;			//
	ProductInfoComposite productInfoComposite;
	Button btnAdd;
	SubProductTableMrn subProductTableMin;	
	
	MaterialIssueNote minobj;
	
	//  rohan aadeed this code for NBHC to download customer in MIN download
	PersonInfoComposite personInfoComposite;
	
	public MaterialIssueNoteForm() {
		super();
		createGui();
		
		this.ibMinId.setEnabled(false);
		this.tbMinStatus.setEnabled(false);
		this.tbMinStatus.setText(MaterialIssueNote.CREATED);		
		this.dbMinSoDate.setEnabled(false);
		this.dbMinWoDate.setEnabled(false);
		this.dbMinDeliveryDate.setEnabled(false);
		this.tbMinCommentOnStatusChange.setEnabled(false);
		this.dbMinMrnDate.setEnabled(false);
		subProductTableMin.connectToLocal();
		productTableMin.connectToLocal();
		
		Timer timer=new Timer() {
			@Override
			public void run() {
				setDefaultDataOfLoggedInUser();
			}
		};
		timer.schedule(4000);
	}
	
	
	protected void setDefaultDataOfLoggedInUser() {
		oblMinRequestedBy.setValue(LoginPresenter.loggedInUser);
//		oblMinSalesPerson.setValue(LoginPresenter.loggedInUser);
		oblMinApproverName.setValue(LoginPresenter.loggedInUser);
		oblMinIssuedBy.setValue(LoginPresenter.loggedInUser);
		
		if(oblMinRequestedBy.getValue()!=null){
			Employee emp=oblMinRequestedBy.getSelectedItem();
			oblMinBranch.setValue(emp.getBranchName());
		}
		
		/**
		 * Date 11-07-2018 By Vijay
		 * Des :- For NBCH CCPM not allow to change by default requested approver issued by Name
		 * it must be looged in user name
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialIssueNote", "OnlyForNBHC") && 
				!LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
			oblMinRequestedBy.setEnabled(false);
			oblMinApproverName.setEnabled(false);
			oblMinIssuedBy.setEnabled(false);
		}
		/**
		 * ends here
		 */
	}
	
	protected boolean getProductInfoVisibilityStatus(){
		if(LoginPresenter.globalProcessConfig.size()!=0){
			for(ProcessConfiguration proConf:LoginPresenter.globalProcessConfig){
				if(proConf.getProcessName().equals("MaterialIssueNote")&&proConf.isConfigStatus()==true){
					for(ProcessTypeDetails proTypDet:proConf.getProcessList()){
						if(proTypDet.getProcessType().equalsIgnoreCase("HIDEPRODUCTINFO")&&proTypDet.isStatus()==true){
							return false;
						}
					}
				}
			}
		}
		return true;
	}
	
	
	public MaterialIssueNoteForm(String[] processlevel, FormField[][] fields,FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
	}
	
	private void initializeWidget(){
		ibMinMrnId=new TextBox();
		ibMinMrnId.setEnabled(false);
		dbMinMrnDate=new DateBoxWithYearSelector();
		ibMinSoId=new TextBox();
		ibMinSoId.setEnabled(false);
		dbMinSoDate=new DateBoxWithYearSelector();
		ibMinWoId=new TextBox();
		ibMinWoId.setEnabled(false);
		dbMinWoDate=new DateBoxWithYearSelector();
		dbMinDeliveryDate=new DateBoxWithYearSelector();
		
		ibMinId=new TextBox();
		dbMinDate=new DateBoxWithYearSelector();
		
		tbMinTitle=new TextBox();
		
		oblMinCategory= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblMinCategory, Screen.MINCATEGORY);
		oblMinCategory.addChangeHandler(this);
		oblMinType = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(oblMinType,Screen.MINTYPE);			
		oblMinBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(oblMinBranch);
		
		oblMinSalesPerson = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblMinSalesPerson);
		oblMinSalesPerson.makeEmployeeLive(AppConstants.INVENTORYMODULE, "Material Issue Note", "Sales Person");
		
		oblMinApproverName = new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(oblMinApproverName, "MIN");
		
		oblMinIssuedBy = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblMinIssuedBy);
		oblMinIssuedBy.makeEmployeeLive(AppConstants.INVENTORYMODULE, "Material Issue Note", "Issued By");
		
		oblMinRequestedBy = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblMinRequestedBy);
		oblMinRequestedBy.makeEmployeeLive(AppConstants.INVENTORYMODULE, "Material Issue Note", "Requested By");
		
		
		oblMinProject = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(oblMinProject);
		
		tbMinStatus=new TextBox();
		tbMinCommentOnStatusChange=new TextBox();
		
		upload=new UploadComposite();
		taMinDescription=new TextArea();
		
		productInfoComposite = AppUtility.initiateMrnProductComposite(new SuperProduct());
		productTableMin=new ProductTableMrn();
		
		btnAdd=new Button("ADD");
		btnAdd.addClickHandler(this);
		
		subProductTableMin=new SubProductTableMrn();
		tbServiceId=new TextBox();
		tbServiceId.setEnabled(false);
		
		MyQuerry custquerry1 = new MyQuerry();
		custquerry1.setQuerryObject(new Customer());
		personInfoComposite = new PersonInfoComposite(custquerry1, false);
		personInfoComposite.setEnable(false);
		
		personInfoComposite.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("Customer Cell");
	}

	@Override
	public void createScreen() {
		initializeWidget();
		
		this.processlevelBarNames = new String[] {ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,ManageApprovals.SUBMIT,AppConstants.CANCELMIN,AppConstants.NEW };
		
		String mainScreenLabel="MATERIAL ISSUE NOTE";
		if(minobj!=null&&minobj.getCount()!=0){
			mainScreenLabel=minobj.getCount()+" "+"/"+" "+minobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(minobj.getCreationDate());
		}
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fMinRefereceDetailGrouping=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		
		fbuilder = new FormFieldBuilder("MRN ID", ibMinMrnId);
		FormField  fibMinMrnId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("MRN Date", dbMinMrnDate);
		FormField fdbMinMrnDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Work Order ID", ibMinWoId);
		FormField fibMinWoId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Work Order Date", dbMinWoDate);
		FormField fdbMinWoDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Order ID", ibMinSoId);
		FormField fibMinSoId = fbuilder.setMandatory(false).setMandatoryMsg("SO ID is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Order Date", dbMinSoDate);
		FormField fdbMinSoDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Delivery Date", dbMinDeliveryDate);
		FormField fdbMinDeliveryDate = fbuilder.setMandatory(false).setMandatoryMsg("Delivery Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project", oblMinProject);
		FormField foblMinProject = fbuilder.setMandatory(false).setMandatoryMsg("Project is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Sales Person",oblMinSalesPerson );
		FormField foblMinSalesPerson = fbuilder.setMandatory(false).setMandatoryMsg("Sales Person is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		FormField fMinMinInfoGrouping = fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fMinMinInfoGrouping1 = fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("MIN ID", ibMinId);
		FormField fibMinId= fbuilder.setMandatory(false).setMandatoryMsg("MIN ID is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MIN Title", tbMinTitle);
		FormField ftbMinTitle = fbuilder.setMandatory(false).setMandatoryMsg("MIN Title is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* MIN Date", dbMinDate);
		FormField fdbMinDate = fbuilder.setMandatory(true).setMandatoryMsg("MIN Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MIN Category", oblMinCategory);
		FormField foblMinCategory = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MIN Type", oblMinType);
		FormField foblMinType = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField foblMinBranch=null;
		if(AppUtility.isBranchNonMandatory("MaterialIssueNote")){
			fbuilder = new FormFieldBuilder("Branch", oblMinBranch);
			foblMinBranch = fbuilder.setMandatory(false).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("* Branch", oblMinBranch);
			foblMinBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}
		
		fbuilder = new FormFieldBuilder("* Approver", oblMinApproverName);
		FormField foblMinApproverName = fbuilder.setMandatory(true).setMandatoryMsg("Approver is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		/**
		 *  Added By : Priyanka
		 *  Des : PECOPP ON MRN,MIN,MMN replace label "Requested By" to "Requested To" Raised By Ashwini.
		 */
		
		//fbuilder = new FormFieldBuilder("* Requested By", oblMinRequestedBy);
		fbuilder = new FormFieldBuilder("* Requested To", oblMinRequestedBy);
		FormField foblMinRequestedBy = fbuilder.setMandatory(true).setMandatoryMsg("Requested By is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Issued By", oblMinIssuedBy);
		FormField foblMinIssuedBy = fbuilder.setMandatory(true).setMandatoryMsg("Issued By is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", tbMinStatus);
		FormField ftbMinStatus = fbuilder.setMandatory(true).setMandatoryMsg("Status is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Comment", tbMinCommentOnStatusChange);
		FormField ftbMinCommentOnStatusChange = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Document", upload);
		FormField fupload = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)", taMinDescription);
		FormField ftaMinDescription = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField fMinProductGrouping = fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", productTableMin.getTable());
		FormField fproductTableMin= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField fMinSubProductGrouping = fbuilder.setlabel("MIN Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fGenrelInfoGrouping = fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fbtnAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", subProductTableMin.getTable());
		FormField fsubProductTableMin= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		//  rohan added service id and make mandetory for NBHC only 
		
		FormField ftbServiceId;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialIssueNote","MakeServiceIDMandetoryInMIN"))
		{
			fbuilder = new FormFieldBuilder("* Service ID", tbServiceId);
			ftbServiceId = fbuilder.setMandatory(true).setMandatoryMsg("Service ID is Mandatory!").setRowSpan(0)
			.setColSpan(0).build();
			
			
		}
		else
		{
			fbuilder = new FormFieldBuilder("Service ID", tbServiceId);
			ftbServiceId = fbuilder.setMandatory(false).setMandatoryMsg("Service ID is Mandatory!").setRowSpan(0)
			.setColSpan(0).build();
		}
		
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		if(getProductInfoVisibilityStatus()){
			
			FormField[][] formfield = { 
//					{ fMinRefereceDetailGrouping },
//					{ fibMinMrnId,fdbMinMrnDate,fibMinWoId, fdbMinWoDate},
//					{ fibMinSoId,fdbMinSoDate,fdbMinDeliveryDate,foblMinProject},
//					{ foblMinSalesPerson,ftbServiceId},
//					{fpersonInfoComposite},
//					{ fMinMinInfoGrouping},
//					{ fibMinId,ftbMinTitle, fdbMinDate,foblMinCategory},
//					{ foblMinType,foblMinBranch,foblMinIssuedBy,foblMinApproverName},
//					{ foblMinRequestedBy,ftbMinStatus, ftbMinCommentOnStatusChange},
//					{ fupload},
//					{ ftaMinDescription},
//					
//					{ fMinProductGrouping},
//					{ fproductTableMin},
//					
//					{ fMinSubProductGrouping},
//					{ fproductInfoComposite,fbtnAdd},
//					{ fsubProductTableMin}
					/** material issue note **/
					{ fMinRefereceDetailGrouping },
					{ftbMinTitle,fdbMinDate,foblMinBranch,foblMinIssuedBy},
					{foblMinRequestedBy,foblMinApproverName,ftbMinCommentOnStatusChange},
					/**Product Information**/
					{ fMinProductGrouping},
					{ fproductTableMin},
					/** MIN Information **/
					{ fMinSubProductGrouping},
					{ fproductInfoComposite,fbtnAdd},
					{ fsubProductTableMin},
					/**Classification Information**/
					{ fMinMinInfoGrouping},
					{ foblMinCategory,foblMinType},
					/**Reference**/
					{ fGenrelInfoGrouping},
					{ fpersonInfoComposite},
					{ fibMinMrnId,fdbMinMrnDate,fibMinWoId,fdbMinWoDate},
					{ fibMinSoId,fdbMinSoDate,fdbMinDeliveryDate,foblMinProject},
					{ foblMinSalesPerson,ftbServiceId},
					{ ftaMinDescription},
					//Attachment
					{fMinMinInfoGrouping1},
					{ fupload}
			};
			this.fields = formfield;
		}else{
			FormField[][] formfield = { 
//					{ fMinRefereceDetailGrouping },
//					{ fibMinMrnId,fdbMinMrnDate,fibMinWoId, fdbMinWoDate},
//					{ fibMinSoId,fdbMinSoDate,fdbMinDeliveryDate,foblMinProject},
//					{ foblMinSalesPerson,ftbServiceId},
//					{fpersonInfoComposite},
//					{ fMinMinInfoGrouping},
//					{ fibMinId,ftbMinTitle, fdbMinDate,foblMinCategory},
//					{ foblMinType,foblMinBranch,foblMinIssuedBy,foblMinApproverName},
//					{ foblMinRequestedBy,ftbMinStatus, ftbMinCommentOnStatusChange},
//					{ fupload},
//					{ ftaMinDescription},
//					
//					{ fMinSubProductGrouping},
//					{ fproductInfoComposite,fbtnAdd},
//					{ fsubProductTableMin}
					
					/** material issue note **/
					{ fMinRefereceDetailGrouping },
					{fdbMinDate,foblMinBranch,foblMinIssuedBy,foblMinRequestedBy},
					{foblMinApproverName,ftbMinStatus,ftbServiceId},
					/** MIN Information **/
					{ fMinSubProductGrouping},
					{ fproductInfoComposite,fbtnAdd},
					{ fsubProductTableMin},
					/**Product Information**/
					//{ fMinProductGrouping},
					//{ fproductTableMin},
					/**Classification Information**/
					{ fMinMinInfoGrouping},
					{ ftbMinTitle,foblMinCategory,foblMinType},
					/**Reference/General**/
					{fGenrelInfoGrouping},
					{fpersonInfoComposite},
					{fibMinMrnId,fdbMinMrnDate,fibMinWoId,fdbMinWoDate},
					{ fibMinSoId,fdbMinSoDate,fdbMinDeliveryDate,foblMinProject},
					{ foblMinSalesPerson,ftbMinCommentOnStatusChange},
					{ fupload},
					{ ftaMinDescription}
			};
			this.fields = formfield;	
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(MaterialIssueNote model) {
		if (tbMinTitle.getValue() != null)
			model.setMinTitle(tbMinTitle.getValue());
		if (dbMinDate.getValue() != null)
			model.setMinDate(dbMinDate.getValue());
		if (!ibMinMrnId.getValue().equals(""))
			model.setMinMrnId(Integer.parseInt(ibMinMrnId.getValue()));
		if (dbMinMrnDate.getValue() != null)
			model.setMinMrnDate(dbMinMrnDate.getValue());
		if (!ibMinWoId.getValue().equals(""))
			model.setMinWoId(Integer.parseInt(ibMinWoId.getValue()));
		if (dbMinWoDate.getValue() != null)
			model.setMinWoDate(dbMinWoDate.getValue());
		if (!ibMinSoId.getValue().equals(""))
			model.setMinSoId(Integer.parseInt(ibMinSoId.getValue()));
		if (dbMinSoDate.getValue() != null)
			model.setMinSoDate(dbMinSoDate.getValue());
		if (dbMinDeliveryDate.getValue() != null)
			model.setMinSoDeliveryDate(dbMinDeliveryDate.getValue());
		if (oblMinProject.getValue() != null)
			model.setMinProject(oblMinProject.getValue());
		if(!tbServiceId.getValue().equals(""))
			model.setServiceId(Integer.parseInt(tbServiceId.getValue()));
		if (oblMinCategory.getValue() != null)
			model.setMinCategory(oblMinCategory.getValue());
		if (oblMinType.getValue() != null)
			model.setMinType(oblMinType.getValue(oblMinType.getSelectedIndex()));
		
		if (oblMinBranch.getValue() != null)
			model.setBranch(oblMinBranch.getValue());
		if (oblMinSalesPerson.getValue() != null)
			model.setMinSalesPerson(oblMinSalesPerson.getValue());		
		if (oblMinRequestedBy.getValue() != null)
			model.setEmployee(oblMinRequestedBy.getValue());
		if (oblMinIssuedBy.getValue() != null)
			model.setMinIssuedBy(oblMinIssuedBy.getValue());
		if (oblMinApproverName.getValue() != null)
			model.setApproverName(oblMinApproverName.getValue());
		
		if (tbMinStatus.getValue() != null)
			model.setStatus(tbMinStatus.getValue());
//		if (tbMinCommentOnStatusChange.getValue() != null)
//			model.setMinCommentOnStatusChange(tbMinCommentOnStatusChange.getValue());
		
		if (taMinDescription.getValue() != null)
			model.setMinDescription(taMinDescription.getValue());
		if (upload.getValue() != null)
			model.setUpload(upload.getValue());
		
		
		if (productTableMin.getValue() != null)
			model.setProductTablemin(productTableMin.getValue());
		if (subProductTableMin.getValue() != null)
			model.setSubProductTablemin(subProductTableMin.getValue());
		
		if(personInfoComposite.getValue()!=null){
			model.setCinfo(personInfoComposite.getValue());
		}
		
		minobj=model;
		
		presenter.setModel(model);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateView(MaterialIssueNote view) {
		
		
		minobj=view;
		
		ibMinId.setValue(view.getCount()+"");
		if (view.getMinTitle() != null)
			tbMinTitle.setValue(view.getMinTitle());
		if (view.getMinDate() != null)
			dbMinDate.setValue(view.getMinDate());
		
		if (view.getMinMrnId() != 0){
			ibMinMrnId.setValue(view.getMinMrnId()+"");
			checkMrnQuantity(view.getMinMrnId());
		}
		if (view.getMinMrnDate() != null)
			dbMinMrnDate.setValue(view.getMinMrnDate());
		
		if (view.getMinWoId() != 0)
			ibMinWoId.setValue(view.getMinWoId()+"");
		if (view.getMinWoDate() != null)
			dbMinWoDate.setValue(view.getMinWoDate());
		if (view.getMinSoId() != 0)
			ibMinSoId.setValue(view.getMinSoId()+"");
		if (view.getMinSoDate() != null)
			dbMinSoDate.setValue(view.getMinSoDate());
		
		if (view.getMinSoDeliveryDate() != null)
			dbMinDeliveryDate.setValue(view.getMinSoDeliveryDate());
		
		if (view.getMinCategory() != null)
			oblMinCategory.setValue(view.getMinCategory());
		if (view.getMinType()!= null)
			oblMinType.setValue(view.getMinType());
		
		if (view.getMinProject() != null)
			oblMinProject.setValue(view.getMinProject());
		if (view.getBranch() != null)
			oblMinBranch.setValue(view.getBranch());
		if (view.getMinSalesPerson() != null)
			oblMinSalesPerson.setValue(view.getMinSalesPerson());
		if (view.getEmployee() != null)
			oblMinRequestedBy.setValue(view.getEmployee());
		if (view.getMinIssuedBy() != null)
			oblMinIssuedBy.setValue(view.getMinIssuedBy());
		if (view.getApproverName()!= null)
			oblMinApproverName.setValue(view.getApproverName());
		
		if (view.getStatus() != null)
			tbMinStatus.setValue(view.getStatus());
		if (view.getRemark() != null){
			tbMinCommentOnStatusChange.setValue(view.getRemark());
		}
		if(view.getServiceId()!=0)
			tbServiceId.setValue(view.getServiceId()+"");
		if (view.getMinDescription() != null)
			taMinDescription.setValue(view.getMinDescription());
		if (view.getUpload() != null)
			upload.setValue(view.getUpload());
		
		if (view.getProductTablemin() != null)
			productTableMin.setValue(view.getProductTablemin());
		if (view.getSubProductTablemin() != null)
			subProductTableMin.setValue(view.getSubProductTablemin());
		
		
		if(view.getCinfo().getCellNumber()!=null){
			personInfoComposite.setValue(view.getCinfo());
		}
		
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")
						|| text.contains("Search")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")|| text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Edit") || text.contains("Discard")|| text.contains("Search")|| text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.MATERIALISSUENOTE,LoginPresenter.currentModule.trim());
	}
	
	public void toggleProcessLevelMenu() {
		MaterialIssueNote entity = (MaterialIssueNote) presenter.getModel();
		String status = entity.getStatus();

		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();
			if ((status.equals(MaterialIssueNote.APPROVED))) {
				if (text.equals(AppConstants.NEW)){
					label.setVisible(true);
				}
//				if (text.equals(AppConstants.CANCELMIN)){
//					label.setVisible(true);
//				}
				
				/**
				 * rohan added this code for NBHC Changes ADMIN role should  
				 */
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
					if (text.equals(AppConstants.CANCELMIN)){
						label.setVisible(true);
					}
				}
				else
				{
					if (text.equals(AppConstants.CANCELMIN)){
						label.setVisible(false);
					}
				}
				
				
				if (text.equals(ManageApprovals.SUBMIT)){
					label.setVisible(false);
				}
			}
			
			if ((status.equals(MaterialIssueNote.CREATED))) {
				if (text.equals(AppConstants.NEW)){
					label.setVisible(true);
				}
//				if (text.equals(AppConstants.CANCELMIN)){
//					label.setVisible(true);
//				}
				
								
				/**
				 * rohan added this code for NBHC Changes ADMIN role should  
				 */
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
					if (text.equals(AppConstants.CANCELMIN)){
						label.setVisible(true);
					}
				}
				else
				{
					if (text.equals(AppConstants.CANCELMIN)){
						label.setVisible(false);
					}
				}
				
				
			}
			
			if ((status.equals(MaterialIssueNote.REQUESTED))) {
				if (text.equals(AppConstants.NEW)){
					label.setVisible(true);
				}
				if (text.equals(AppConstants.NEW)){
					label.setVisible(false);
				}
				if (text.equals(AppConstants.CANCELMIN)){
					label.setVisible(false);
				}
				if (text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST)){
					label.setVisible(true);
				}
			}
			
			
			if ((status.equals(MaterialIssueNote.REJECTED) || status.equals(MaterialIssueNote.CANCELLED))) {
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);
				
				if (text.equals(AppConstants.CANCELMIN)){
					label.setVisible(false);
				}
			}
		}

	}
	
	public void setAppHeaderBarAsPerStatus() {
		MaterialIssueNote entity = (MaterialIssueNote) presenter.getModel();
		String status = entity.getStatus();

		if (status.equals(MaterialIssueNote.APPROVED) || status.equals(MaterialIssueNote.CANCELLED)) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")||text.contains("Email")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		
		if (status.equals(MaterialIssueNote.CANCELLED)) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
	}
	
	public void setMenuAsPerStatus() {
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
	}
	
	@Override
	public void setCount(int count) {
		ibMinId.setValue(count+"");
	}

	@Override
	public void clear() {
		super.clear();
		tbMinStatus.setText(MaterialIssueNote.CREATED);
		productTableMin.clear();
		subProductTableMin.clear();

	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbMinStatus.setEnabled(false);
		tbMinCommentOnStatusChange.setEnabled(false);
		ibMinId.setEnabled(false);
		productTableMin.setEnable(state);
		subProductTableMin.setEnable(state);
		dbMinDeliveryDate.setEnabled(false);
		dbMinSoDate.setEnabled(false);
		dbMinWoDate.setEnabled(false);
		dbMinMrnDate.setEnabled(false);
		ibMinWoId.setEnabled(false);
		ibMinSoId.setEnabled(false);
		ibMinMrnId.setEnabled(false);
		tbServiceId.setEnabled(false);
		personInfoComposite.setEnable(false);
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		setMenuAsPerStatus();
		
		SuperModel model=new MaterialIssueNote();
		model=minobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.INVENTORYMODULE,AppConstants.MATERIALISSUENOTE, minobj.getCount(), null,null,null, false, model, null);
		
		String mainScreenLabel="MATERIAL ISSUE NOTE";
		if(minobj!=null&&minobj.getCount()!=0){
			mainScreenLabel=minobj.getCount()+" "+"/"+" "+minobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(minobj.getCreationDate());
		}
		fMinRefereceDetailGrouping.getHeaderLabel().setText(mainScreenLabel);
	
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		
		if(tbMinStatus.getValue().equals(MaterialIssueNote.REJECTED)){
			tbMinStatus.setValue(MaterialIssueNote.CREATED);
		}
		if(!ibMinMrnId.getValue().equals("")){
			productInfoComposite.setEnabled(false);
		}
		setMenuAsPerStatus();
		
		this.processLevelBar.setVisibleFalse(false);
		
		String mainScreenLabel="MATERIAL ISSUE NOTE";
		if(minobj!=null&&minobj.getCount()!=0){
			mainScreenLabel=minobj.getCount()+" "+"/"+" "+minobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(minobj.getCreationDate());
		}
		fMinRefereceDetailGrouping.getHeaderLabel().setText(mainScreenLabel);
	}

	@Override
	public TextBox getstatustextbox() {
		return tbMinStatus;
	}

	@Override
	public boolean validate() {
		boolean superValidate = super.validate();
		if (superValidate == false)
			return false;
		if(subProductTableMin.getDataprovider().getList().size()== 0){
			showDialogMessage("Please add material details.");
			return false;
		}
		if(validateWarehouse()==false){
			showDialogMessage("Please Select Warehouse Details.");
			return false;
		}
		if(validateRequiredQuantity()==false){
			return false;
		}
		
		//   rohan added this validation with process configuration for NBHC
		
		//   
		
		if(!ibMinMrnId.getValue().equals("")){
			if(validateProdQtyAgainstMrn()==false){
				return false;
			}
		}
		/**
		 * nidhi
		 * 15-05-2018
		 * for min date validation
		 * 
		 */
		if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
			if(validateMINDate())	
			{
				
			}else{
				return false;
			}
		}
		/**
		 * end
		 */
		return true;
	}
	
	public boolean validateMINDate(){

		boolean flag = true;
		DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
		String diffDays = AppUtility.getForProcessConfigurartionIsActiveOrNot("MINDateRestriction");
		
		if(diffDays==null){
			flag = true;
		}else{
			if(diffDays==null){
				flag = true;
			}else{
				int days = Integer.parseInt(diffDays);
		
			if(days>0){/*
				Date currentDate = new Date();
				
				Date validateDate = new Date();
	
				currentDate=format.parse(format.format(currentDate));
				validateDate.setDate(days);
				
				Date firstDate = new Date();
				firstDate.setDate(1);
				if(dbMinDate.getValue().before(firstDate) && currentDate.after(validateDate)){
					showDialogMessage("MIN of previous month is not allowed..!");
				}
			*/
	
				Date preFirstDate = new Date();
				Date preLastDate = new Date();
				
				CalendarUtil.addMonthsToDate(preFirstDate, -1);
				preFirstDate.setDate(1);
				
				preLastDate.setDate(1);
				CalendarUtil.addDaysToDate(preLastDate, -1);
				
				
				Date currentDate = new Date();
				
				Date validateDate = new Date();
				
				currentDate=format.parse(format.format(currentDate));
				
				validateDate=format.parse(format.format(validateDate));
				preLastDate=format.parse(format.format(preLastDate));
				preFirstDate=format.parse(format.format(preFirstDate));
				
				validateDate.setDate(days);
				
				Date firstDate = new Date();
				firstDate.setDate(1);
				
				com.slicktechnologies.client.utils.Console.log("MIN Date validation"+(currentDate.after(validateDate) || dbMinDate.getValue().before(firstDate)));
				com.slicktechnologies.client.utils.Console.log("current date"+currentDate);
				com.slicktechnologies.client.utils.Console.log("validateDate date"+validateDate);
				com.slicktechnologies.client.utils.Console.log("firstDate date"+firstDate);

				com.slicktechnologies.client.utils.Console.log("preFirstDate date"+preFirstDate);
				com.slicktechnologies.client.utils.Console.log("preLastDate date"+preLastDate);
				com.slicktechnologies.client.utils.Console.log("Min date"+dbMinDate.getValue());


				if(dbMinDate.getValue().before(firstDate) ){
					if((dbMinDate.getValue().after(preFirstDate) && dbMinDate.getValue().before(preLastDate)) || 
							dbMinDate.getValue().equals(preLastDate) || dbMinDate.getValue().equals(preFirstDate) ){
						if(currentDate.after(validateDate) || dbMinDate.getValue().before(firstDate)){
							showDialogMessage("MIN of previous month is not allowed..!");
							flag = false;
						}
					}else{
						if(currentDate.after(validateDate) || dbMinDate.getValue().before(firstDate)){
							showDialogMessage("MIN of previous month is not allowed..!");
							flag = false;
						}
					}
				}
			}
			}
		}
		
		return flag;
	
	}
	
	public boolean validateProduct() {
		String name = productInfoComposite.getProdNameValue();
		List<MaterialProduct> list = subProductTableMin.getDataprovider().getList();
		for (MaterialProduct temp : list) {
			if (name.equals(temp.getMaterialProductName())) {
				this.showDialogMessage("Can not add Duplicate product");
				return false;
			}
		}
		return true;
	}
	
	public boolean validateRequiredQuantity() {
		List<MaterialProduct> list = subProductTableMin.getDataprovider().getList();
		for (MaterialProduct temp : list) {
			if (temp.getMaterialProductRequiredQuantity()==0) {
				this.showDialogMessage("Issued Quantity Can Not Be Zero");
				return false;
			}
			if(temp.getMaterialProductAvailableQuantity()<temp.getMaterialProductRequiredQuantity()){
				this.showDialogMessage("Issued Quantity Should Not Be Greater Than Available Quantity");
				return false;
			}
		}
		return true;
	}
	
	// ************************************ Validate Warehouse *********************************************
	public boolean validateWarehouse(){
		int ctr=0;
		for(int i=0;i<subProductTableMin.getDataprovider().getList().size();i++){
			if(!subProductTableMin.getDataprovider().getList().get(i).getMaterialProductWarehouse().equals("")
					&&!subProductTableMin.getDataprovider().getList().get(i).getMaterialProductStorageBin().equals("")){
				ctr++;
			}
		}
		if(ctr==subProductTableMin.getDataprovider().getList().size()){
			return true;
		}else{
			return false;
		}
	}
			
			
	// **************** Validate Product Qty Against ****************************
			
	public boolean validateProdQtyAgainstMrn(){
		boolean flage=true;
		for(int i=0;i<subProductTableMin.getDataprovider().getList().size();i++){
					
			if(!compList.isEmpty()){
				for(int j=0;j<compList.size();j++){
					if(subProductTableMin.getDataprovider().getList().get(i).getMaterialProductId()== compList.get(j).getProdId()){
						if(subProductTableMin.getDataprovider().getList().get(i).getMaterialProductRequiredQuantity()>compList.get(j).getProdQty()){
							double exceedQty=subProductTableMin.getDataprovider().getList().get(i).getMaterialProductRequiredQuantity()-compList.get(j).getProdQty();
							flage=false;
								showDialogMessage("Material Id "+subProductTableMin.getDataprovider().getList().get(i).getMaterialProductId()+" exceeds quantity in MRN by "+exceedQty+".");
							break;
						}
					}
				}
			}
		}
		return flage;
	}
			
	public void validateMinQty(ArrayList<ProductQty> mrnList,ArrayList<ProductQty> minList){
		compList=new ArrayList<ProductQty>();
		double qty=0;
		if(!minList.isEmpty()){
			for(int i=0;i<mrnList.size();i++){
				for(int j=0;j<minList.size();j++){
					if(mrnList.get(i).getProdId()==minList.get(j).getProdId()){
						qty=mrnList.get(i).getProdQty()-minList.get(j).getProdQty();
							
						ProductQty pq=new ProductQty();
						pq.setProdId(mrnList.get(i).getProdId());
						pq.setProdQty(qty);
						
						compList.add(pq);
					}
				}
			}
		}
		else{
			compList.addAll(mrnList);
		}
	}
			
			

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource().equals(btnAdd)) 
		{
			setSubProductTable();
			
		}
	}
	
	public void setSubProductTable(){
		if(validateProduct()){
		if (productInfoComposite.getValue() != null) {
			
			//  rohan commented this old code for science utsav requirement
//			MaterialProduct prod = new MaterialProduct();
//			prod.setMaterialProductId(Integer.parseInt(productInfoComposite.getProdID().getValue()));
//			prod.setMaterialProductCode(productInfoComposite.getProdCode().getValue());
//			prod.setMaterialProductCategory(productInfoComposite.getProdCategory().getValue());
//			prod.setMaterialProductName(productInfoComposite.getProdName().getValue());
//			prod.setMaterialProductAvailableQuantity(0);
//			prod.setMaterialProductUOM(productInfoComposite.getUnitOfmeasuerment().getValue());
//			prod.setMaterialProductRequiredQuantity(0);
//			prod.setMaterialProductRemarks("");
//			prod.setMaterialProductWarehouse("");
//			prod.setMaterialProductStorageLocation("");
//			prod.setMaterialProductStorageBin("");
//			subProductTableMin.getDataprovider().getList().add(prod);
//			productInfoComposite.clear();
			
			
			
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			filter = new Filter();
			filter.setQuerryString("productinfo.prodID");
			filter.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
			filtervec.add(filter);
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ProductInventoryView());
			
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					if(result.size()!=0){
					
					for(SuperModel model:result)
					{
						ProductInventoryView product=(ProductInventoryView)model;
						
						if(product.getDetails().size()== 1 ){
						for (int j = 0; j < product.getDetails().size(); j++) 
						{
							{
								MaterialProduct prod = new MaterialProduct();
								prod.setMaterialProductId(Integer.parseInt(productInfoComposite.getProdID().getValue()));
								prod.setMaterialProductCode(productInfoComposite.getProdCode().getValue());
								prod.setMaterialProductCategory(productInfoComposite.getProdCategory().getValue());
								prod.setMaterialProductName(productInfoComposite.getProdName().getValue());
								prod.setMaterialProductAvailableQuantity(product.getDetails().get(j).getAvailableqty());
								prod.setMatReqReOdrLvlQty(product.getDetails().get(j).getReorderlevel());
								prod.setMaterialProductUOM(productInfoComposite.getUnitOfmeasuerment().getValue());
								prod.setMaterialProductRequiredQuantity(0);
								prod.setMaterialProductRemarks("");
								prod.setMaterialProductWarehouse(product.getDetails().get(j).getWarehousename());
								prod.setMaterialProductStorageLocation(product.getDetails().get(j).getStoragelocation());
								prod.setMaterialProductStorageBin(product.getDetails().get(j).getStoragebin());
								subProductTableMin.getDataprovider().getList().add(prod);
								productInfoComposite.clear();
							}
						}
						
						}
						else
						{
							MaterialProduct prod = new MaterialProduct();
							prod.setMaterialProductId(Integer.parseInt(productInfoComposite.getProdID().getValue()));
							prod.setMaterialProductCode(productInfoComposite.getProdCode().getValue());
							prod.setMaterialProductCategory(productInfoComposite.getProdCategory().getValue());
							prod.setMaterialProductName(productInfoComposite.getProdName().getValue());
							prod.setMaterialProductAvailableQuantity(0);
							prod.setMaterialProductUOM(productInfoComposite.getUnitOfmeasuerment().getValue());
							prod.setMaterialProductRequiredQuantity(0);
							prod.setMaterialProductRemarks("");
							prod.setMaterialProductWarehouse("");
							prod.setMaterialProductStorageLocation("");
							prod.setMaterialProductStorageBin("");
							subProductTableMin.getDataprovider().getList().add(prod);
							productInfoComposite.clear();
						}
					
				}
				}
					else
					{
						showDialogMessage("Please Define Warehouse For This Product..!");
					}
				}
			});
		}
	  
			
			
		}
	  }
	
	
	// *********************************** Validation method for MIN QTY against MRN QTY **********************************
	
	public void checkMrnQuantity(final int mrnId){
		System.out.println("Inside search .............................");
		
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(mrnId);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new MaterialRequestNote());
		
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				mrnList=new ArrayList<ProductQty>();
				for (SuperModel model : result) {
					if(result.size()!=0){
						MaterialRequestNote entity = (MaterialRequestNote) model;
						for(int i=0;i<entity.getSubProductTableMrn().size();i++){
							
							ProductQty pq=new ProductQty();
							pq.setProdId(entity.getSubProductTableMrn().get(i).getMaterialProductId());
							pq.setProdQty(entity.getSubProductTableMrn().get(i).getMaterialProductRequiredQuantity());
							
							mrnList.add(pq);
						}
					}
				}
				
				MyQuerry querey1 = new MyQuerry();
				Company c=new Company();
				System.out.println("Company Id :: "+c.getCompanyId());
				
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter filter = null;
				
				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("minMrnId");
				filter.setIntValue(mrnId);
				filtervec.add(filter);
				
				querey1.setFilters(filtervec);
				querey1.setQuerryObject(new MaterialIssueNote());
				
				async.getSearchResult(querey1,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("On Failure !!!");
					}
		
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						int minId=0;
						if(!ibMinId.getValue().equals("")){
							minId=Integer.parseInt(ibMinId.getValue());
						}
						minList=new ArrayList<ProductQty>();
						double qty=0;
						boolean flage=false;
						for (SuperModel model1 : result) {
							if(result.size()!=0){
								MaterialIssueNote entity = (MaterialIssueNote) model1;
								if(entity.getCount()!=minId){
									
								for(int i=0;i<entity.getSubProductTablemin().size();i++){
									if(!minList.isEmpty()){
										for(int j=0;j<minList.size();j++){
											if(minList.get(j).getProdId()==entity.getSubProductTablemin().get(i).getMaterialProductId()){
												qty=minList.get(j).getProdQty();
												qty=qty+entity.getSubProductTablemin().get(i).getMaterialProductRequiredQuantity();
												minList.get(j).setProdQty(qty);
												flage=true;
											}
										}
										if(flage==false){
											ProductQty pq=new ProductQty();
											pq.setProdId(entity.getSubProductTablemin().get(i).getMaterialProductId());
											pq.setProdQty(entity.getSubProductTablemin().get(i).getMaterialProductRequiredQuantity());
										
											minList.add(pq);
										}
									}
									else {
										ProductQty pq=new ProductQty();
										pq.setProdId(entity.getSubProductTablemin().get(i).getMaterialProductId());
										pq.setProdQty(entity.getSubProductTablemin().get(i).getMaterialProductRequiredQuantity());
									
										minList.add(pq);
									}
								  }
								}
							}
							else{
								System.out.println("No Result Found !!!!");
							}
						}
						
						for(int i=0;i<minList.size();i++){
							System.out.println("------------------------ Result ----------------------------  ");
							System.out.println("MIN List PO ID : "+minList.get(i).getProdId());
							System.out.println("MIN List QTY : "+minList.get(i).getProdQty());
						}
						validateMinQty(mrnList,minList);
					}
				});
			}
		});
	}
	
	
	
	/*******************************************Type Drop Down Logic**************************************/
	
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(oblMinCategory))
		{
			if(oblMinCategory.getSelectedIndex()!=0){
				ConfigCategory cat=oblMinCategory.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(oblMinType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
		
	}
	
	
	/**
	 * Develpoed By : Rohan Bhagde
	 * Date : 16/11/2016
	 * Reason: This is used to lock all the fields in table when we issue material from project. 
	 */
	
		public void setColumnSelectWareHouseEditable() {
			
			System.out.println("in side method");
			subProductTableMin.setEnableOnlySelectWarehouseButton();
		}

		
	/**
	 * ends here 
	 */
	

	/************************************* Getter And Setter For Form Fields ********************************************/
	
	public TextBox getIbMinMrnId() {
		return ibMinMrnId;
	}

	public void setIbMinMrnId(TextBox ibMinMrnId) {
		this.ibMinMrnId = ibMinMrnId;
	}

	public DateBox getdbMinMrnDate() {
		return dbMinMrnDate;
	}

	public void setdbMinMrnDate(DateBox dbMinMrnDate) {
		this.dbMinMrnDate = dbMinMrnDate;
	}

	
	public TextBox getIbMinSoId() {
		return ibMinSoId;
	}

	public void setIbMinSoId(TextBox ibMinSoId) {
		this.ibMinSoId = ibMinSoId;
	}

	public DateBox getDbMinSoDate() {
		return dbMinSoDate;
	}

	public void setDbMinSoDate(DateBox dbMinSoDate) {
		this.dbMinSoDate = dbMinSoDate;
	}

	public TextBox getIbMinWoId() {
		return ibMinWoId;
	}

	public void setIbMinWoId(TextBox ibMinWoId) {
		this.ibMinWoId = ibMinWoId;
	}

	public DateBox getDbMinWoDate() {
		return dbMinWoDate;
	}

	public void setDbMinWoDate(DateBox dbMinWoDate) {
		this.dbMinWoDate = dbMinWoDate;
	}

	public DateBox getDbMinDeliveryDate() {
		return dbMinDeliveryDate;
	}

	public void setDbMinDeliveryDate(DateBox dbMinDeliveryDate) {
		this.dbMinDeliveryDate = dbMinDeliveryDate;
	}

	public TextBox getIbMinId() {
		return ibMinId;
	}

	public void setIbMinId(TextBox ibMinId) {
		this.ibMinId = ibMinId;
	}

	public DateBox getDbMinDate() {
		return dbMinDate;
	}

	public void setDbMinDate(DateBox dbMinDate) {
		this.dbMinDate = dbMinDate;
	}

	public TextBox getTbMinTitle() {
		return tbMinTitle;
	}

	public void setTbMinTitle(TextBox tbMinTitle) {
		this.tbMinTitle = tbMinTitle;
	}

	public ObjectListBox<ConfigCategory> getOblMinCategory() {
		return oblMinCategory;
	}

	public void setOblMinCategory(ObjectListBox<ConfigCategory> oblMinCategory) {
		this.oblMinCategory = oblMinCategory;
	}

	public ObjectListBox<Type> getOblMinType() {
		return oblMinType;
	}

	public void setOblMinType(ObjectListBox<Type> oblMinType) {
		this.oblMinType = oblMinType;
	}

	public ObjectListBox<Branch> getOblMinBranch() {
		return oblMinBranch;
	}

	public void setOblMinBranch(ObjectListBox<Branch> oblMinBranch) {
		this.oblMinBranch = oblMinBranch;
	}

	public ObjectListBox<HrProject> getOblMinProject() {
		return oblMinProject;
	}

	public void setOblMinProject(ObjectListBox<HrProject> oblMinProject) {
		this.oblMinProject = oblMinProject;
	}

	

	public TextBox getTbMinStatus() {
		return tbMinStatus;
	}

	public void setTbMinStatus(TextBox tbMinStatus) {
		this.tbMinStatus = tbMinStatus;
	}

	public TextBox getTbMinCommentOnStatusChange() {
		return tbMinCommentOnStatusChange;
	}

	public void setTbMinCommentOnStatusChange(TextBox tbMinCommentOnStatusChange) {
		this.tbMinCommentOnStatusChange = tbMinCommentOnStatusChange;
	}

	public UploadComposite getUpload() {
		return upload;
	}

	public void setUpload(UploadComposite upload) {
		this.upload = upload;
	}

	public TextArea getTaMinDescription() {
		return taMinDescription;
	}

	public void setTaMinDescription(TextArea taMinDescription) {
		this.taMinDescription = taMinDescription;
	}

	public ProductTableMrn getProductTableMin() {
		return productTableMin;
	}

	public void setProductTableMin(ProductTableMrn productTableMin) {
		this.productTableMin = productTableMin;
	}

	public ProductInfoComposite getProductInfoComposite() {
		return productInfoComposite;
	}

	public void setProductInfoComposite(ProductInfoComposite productInfoComposite) {
		this.productInfoComposite = productInfoComposite;
	}

	public Button getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(Button btnAdd) {
		this.btnAdd = btnAdd;
	}

	public SubProductTableMrn getSubProductTableMin() {
		return subProductTableMin;
	}

	public void setSubProductTableMin(SubProductTableMrn subProductTableMin) {
		this.subProductTableMin = subProductTableMin;
	}

	public ObjectListBox<Employee> getOblMinSalesPerson() {
		return oblMinSalesPerson;
	}

	public void setOblMinSalesPerson(ObjectListBox<Employee> oblMinSalesPerson) {
		this.oblMinSalesPerson = oblMinSalesPerson;
	}

	public ObjectListBox<Employee> getOblMinApproverName() {
		return oblMinApproverName;
	}

	public void setOblMinApproverName(ObjectListBox<Employee> oblMinApproverName) {
		this.oblMinApproverName = oblMinApproverName;
	}

	public ObjectListBox<Employee> getOblMinRequestedBy() {
		return oblMinRequestedBy;
	}

	public void setOblMinRequestedBy(ObjectListBox<Employee> oblMinRequestedBy) {
		this.oblMinRequestedBy = oblMinRequestedBy;
	}

	public ObjectListBox<Employee> getOblMinIssuedBy() {
		return oblMinIssuedBy;
	}

	public void setOblMinIssuedBy(ObjectListBox<Employee> oblMinIssuedBy) {
		this.oblMinIssuedBy = oblMinIssuedBy;
	}

	public TextBox getTbServiceId() {
		return tbServiceId;
	}

	public void setTbServiceId(TextBox tbServiceId) {
		this.tbServiceId = tbServiceId;
	}


	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}


	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		subProductTableMin.getTable().redraw();
		productTableMin.getTable().redraw();
	}
	
	
	
}
