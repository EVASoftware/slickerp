package com.slicktechnologies.client.views.inventory.warehousedetails;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.poi.ss.formula.ptg.TblPtg;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.inventory.productinventoryview.ProductInventoryViewForm;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.BranchDetails;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryProducts;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class WareHouseDetailsForm extends ApprovableFormScreen<PhysicalInventoryMaintaince>implements ClickHandler,ChangeHandler{
	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	
	TextBox physicalid;
	TextBox title;
	DateBox date;
	ProductInfoComposite productInfoComposite;
	ObjectListBox<WareHouse> oblwarehouse;
	ListBox lsStorageLoc;
	ListBox lsStoragebin;
	Button norBtn,maxBtn,minBtn,searBtn;
	WareHouseDetailsTable table;
	ArrayList<String> storageList=new ArrayList<String>();
	FormField physicaldetails;
	PhysicalInventoryMaintaince physicalinventoryobj;
	
	/**
	 * Developed by : Rohan Bhagde 
	 * Used for :used calling public method present in ProductInventoryViewForm for initializing warehouse
	 * Method name :initializeWarehounse()
	 * Dated : 20/09/2016
	 */
	
		ProductInventoryViewForm productInventory;
		
	/**
	 * ends here 
	 */
	
	public	boolean actualQtyProcess = false;
	ObjectListBox<Employee> olbeSalesPerson,olbApproverName;
	FormField folbApproverName,ftbStatus;
	FormFieldBuilder	fbuilderAp ,fbuilderStatus;
	TextBox txtStatus;
	TextBox txtCreatedBy;
	public WareHouseDetailsForm() {
		super();
		createGui();
		this.txtStatus.setText(Contract.CREATED);
		this.physicalid.setEnabled(false);
		Console.log("Get flag process at form init -- " + this.actualQtyProcess);
		table.actualQtyProcess = this.actualQtyProcess;
		table.connectToLocal();
	}
	
	
	public WareHouseDetailsForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		this.physicalid.setEnabled(false);
		this.txtStatus.setText(Contract.CREATED);
		table.connectToLocal();
	}

	private void initalizeWidget() {
		productInfoComposite = AppUtility.initiateSalesProductComposite(new SuperProduct());
		
		physicalid=new TextBox();
		title=new TextBox();
//		date=new DateBox();
		date = new DateBoxWithYearSelector();

		oblwarehouse = new ObjectListBox<WareHouse>();
		oblwarehouse.addChangeHandler(this);
		norBtn = new Button("Normal Quantity");
		norBtn.addClickHandler(this);
		maxBtn = new Button("Max Quantity");
		maxBtn.addClickHandler(this);
		minBtn = new Button("Min Quantity");
		minBtn.addClickHandler(this);
		searBtn = new Button("Search");
		searBtn.addClickHandler(this);
		this.actualQtyProcess =AppUtility.checkForProcessConfigurartionIsActiveOrNot("PhysicalInventoryProducts", "ActualProductQtyMapActive");
		Console.log("Get flag process -- " + this.actualQtyProcess);
//		this.actualQtyProcess = true;
		table = new WareHouseDetailsTable(this.actualQtyProcess);
		table.actualQtyProcess = this.actualQtyProcess;
//		 table.setEnable(true);
		
		lsStorageLoc=new ListBox();
		lsStorageLoc.addChangeHandler(this);
		lsStorageLoc.addItem("--SELECT--");
		lsStoragebin=new ListBox();
		lsStoragebin.addItem("--SELECT--");
		
		//  rohan commented only this 
//		initializeWarehouse();
		
		//  new code 
		/**
		 * Developed by : Rohan Bhagde 
		 * Used for :used calling public method present in ProductInventoryViewForm for initializing warehouse
		 * Method name :initializeWarehounse()
		 * Dated : 20/09/2016
		 */
		
//			productInventory =new ProductInventoryViewForm();
//			productInventory.initializeWarehounse(oblwarehouse);
		/**
		 * ends here 
		 */
		
		
		
		/**
		 * Update by Anil On 30-12-2016 For NBHC CCPM
		 */
		
		AppUtility.initializeWarehouse(oblwarehouse);
		/**
		 * End
		 */
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApproverName,"Physical Inventory");
		olbApproverName.setVisible(actualQtyProcess);
		table.actualQtyProcess = this.actualQtyProcess;
		if(AppMemory.getAppMemory().currentState == ScreeenState.NEW || AppMemory.getAppMemory().currentState == ScreeenState.EDIT){
			table.setEnable(true);
		}else{
			table.setEnable(false);
		}
		
		txtStatus = new TextBox();
		txtStatus.setEnabled(false);
		txtCreatedBy = new TextBox();
		this.txtStatus.setText(Contract.CREATED);
		txtCreatedBy.setEnabled(false);
		txtCreatedBy.setValue(UserConfiguration.getUserconfig().getUser().getEmployeeName().trim());	
		
//		initializeLocation();
//		initializeBin();
	}
	


	@Override
	public void createScreen() {
		initalizeWidget();
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PhysicalInventoryProducts", "ActualProductQtyMapActive")){
		    this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,
				 AppConstants.NEW,ManageApprovals.SUBMIT};
		}else{
		    	this.processlevelBarNames = new String[] {"New", };
		}
	
		
		// ////////////////////////// Form Field
		// Initialization////////////////////////////////////////////////////

		String mainScreenLabel="Physical Inventory Details";
		if(physicalinventoryobj!=null&&physicalinventoryobj.getCount()!=0){
			mainScreenLabel=physicalinventoryobj.getCount()+" "+"/"+" "+physicalinventoryobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(physicalinventoryobj.getCreationDate());
		}
		//fgroupingCompanyInformation.setLabel(mainScreenLabel);
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		physicaldetails=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();		
		

		fbuilder = new FormFieldBuilder();
		FormField fgroupingProductInformation = fbuilder.setlabel("Product Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingInventoryInformation = fbuilder.setlabel("Inventory Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Warehouse", oblwarehouse);
		FormField fwarehouse = fbuilder.setMandatory(false).setMandatoryMsg("WareHouse Name Is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Storage Location", lsStorageLoc);
		FormField flocation = fbuilder.setMandatory(false).setMandatoryMsg("Storage Location Is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Storage Bin", lsStoragebin);
		FormField fbin = fbuilder.setMandatory(false).setMandatoryMsg("Storage Bin Is Mandatory").setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("Storage Location", lsStorageLoc);
//		FormField flsStoloc = fbuilder.setMandatory(false).setMandatoryMsg("Storage Location Is Mandatory").setRowSpan(0).setColSpan(0).build();
//
//		fbuilder = new FormFieldBuilder("Storage Bin", lsStoragebin);
//		FormField flsStobin = fbuilder.setMandatory(false).setMandatoryMsg("Storage Bin Is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", norBtn);
		FormField fadd = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", maxBtn);
		FormField fadd1 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", minBtn);
		FormField fadd2 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", searBtn);
		FormField fadd3 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", table.getTable());
		FormField ftable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
//		fbuilder = new FormFieldBuilder();
//		FormField physicaldetails = fbuilder.setlabel("Physical Inventory Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Date",date);
		FormField date = fbuilder.setMandatory(true).setMandatoryMsg("Date is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Title",title);
		FormField title = fbuilder.setMandatory(true).setMandatoryMsg("Title is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Id",physicalid );
		FormField phyid = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilderAp = new FormFieldBuilder("* Approver Name",olbApproverName);
		 folbApproverName= fbuilderAp.setMandatory(actualQtyProcess).setMandatoryMsg("Approver Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		 fbuilderStatus = new FormFieldBuilder("Status",txtStatus);
		 ftbStatus= fbuilderStatus.setMandatory(false).setMandatoryMsg("Approver Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		 fbuilder = new FormFieldBuilder("Created By",txtCreatedBy);
		 FormField ftbCreatedBy= fbuilder.setMandatory(false).setMandatoryMsg("Approver Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		// //////////////////////////////Lay Out Making
		// Code///////////////////////////////////////////////////////////////

		FormField[][] formfield = {
//				{physicaldetails},
//				{phyid,date,title,folbApproverName},
//				{ftbStatus,ftbCreatedBy},
//				{ fgroupingProductInformation }, 
//				{ fwarehouse, flocation, fbin},
//				{ fproductInfoComposite },
//				{fadd,fadd1,fadd2,fadd3},
//				{fgroupingInventoryInformation},
//				{ ftable } };
				/**-
				 * Added by Priyanka -08-02-2021
				 * Des-Simplification of Physical Inventory View Req. raise by Nitin Sir.
				 */
				{physicaldetails},
				{phyid,date,title,folbApproverName},
				{ftbStatus,ftbCreatedBy},
			//	{ title,folbApproverName,ftbCreatedBy}, //ftbStatus
				{ fgroupingProductInformation }, 
				{ fproductInfoComposite},
				{ fwarehouse,flocation,fbin},
				{ fadd,fadd1,fadd2,fadd3},
				{ fgroupingInventoryInformation},
				{ ftable } 
				};
		


		this.fields = formfield;
		Timer timer = new Timer() {
			@Override
			public void run() {
					folbApproverName.getHeaderLabel().setVisible(actualQtyProcess);
					olbApproverName.setVisible(actualQtyProcess);
//					showDialogMessage("get set method called -- " +actualQtyProcess);
			}
		};
		timer.schedule(2000);
		table.actualQtyProcess = this.actualQtyProcess;
		
	}
	
	

	/**************************** method template to update the model with token entity name **********************************/
	@Override
	public void updateModel(PhysicalInventoryMaintaince model) {
		
				List<PhysicalInventoryProducts> prodlist=this.table.getDataprovider().getList();
				ArrayList<PhysicalInventoryProducts> prodlistArr=new ArrayList<PhysicalInventoryProducts>();
				
				
				if (date.getValue() != null)
					model.setDate(date.getValue());
				if (title.getValue() != null)
					model.setTitle(title.getValue());
				
				
				for(int i=0;i<prodlist.size();i++)
				{
					Console.log("Ashwini in updatemodel prodlist size="+prodlist.size());
					
					//do nothing if varaince is zero...
					if(prodlist.get(i).getVariance()!=0){
						
						PhysicalInventoryProducts pip=new PhysicalInventoryProducts();
						
						
						
						pip.setProductid(prodlist.get(i).getProductid());
						pip.setProductname(prodlist.get(i).getProductname());
						pip.setUnitofmeorment(prodlist.get(i).getUnitofmeorment());
						pip.setWarehousename(prodlist.get(i).getWarehousename());
						pip.setStoragelocation(prodlist.get(i).getStoragelocation());
						pip.setStoragebin(prodlist.get(i).getStoragebin());
						if(prodlist.get(i).getBranch()!=null)
							pip.setBranch(prodlist.get(i).getBranch());//Ashwini Patil Date:14-06-2023
						pip.setMaxqty(prodlist.get(i).getMaxqty());
						pip.setMinqty(prodlist.get(i).getMinqty());
						pip.setNormallevel(prodlist.get(i).getNormallevel());
						pip.setReorderlevel(prodlist.get(i).getReorderlevel());
						pip.setReorderqty(prodlist.get(i).getReorderqty());
						pip.setAvailableqty(prodlist.get(i).getAvailableqty());
						/**
						 * nidhi 3-12-2018
						 */
						pip.setActualQty(prodlist.get(i).getActualQty());
						pip.setVariance(prodlist.get(i).getVariance());
						pip.setRemark(prodlist.get(i).getRemark());
						
						/**
						 * @author Anil , Date : 30-04-2019
						 */
						if(prodlist.get(i).getProSerialNoDetails()!=null){
							pip.setProSerialNoDetails(prodlist.get(i).getProSerialNoDetails());
						}
						
						prodlistArr.add(pip);
					}
				}
				Console.log("Ashwini in updatemodel prodlistArr size="+prodlistArr.size());
				if(actualQtyProcess){
					model.setApproverName(olbApproverName.getValue(olbApproverName.getSelectedIndex()));
					
					if(olbApproverName.getSelectedIndex()!=0)
						model.setApproverName(olbApproverName.getValue(olbApproverName.getSelectedIndex()));
					model.setStatus(txtStatus.getValue());
				}
				
				model.setCreatedBy(txtCreatedBy.getValue());
				physicalinventoryobj=model;
				model.setProductsLis(prodlistArr);

			presenter.setModel(model);
	}

	/************************ method template to update the view with token entity name ***************************************/
	
	@Override
	public void updateView(PhysicalInventoryMaintaince view) {
		
		physicalinventoryobj=view;
		if(view.getTitle()!=null){
			title.setValue(view.getTitle());
		}
		if(view.getDate()!=null){
			date.setValue(view.getDate());
		}
			table.setValue(view.getProductsLis());
			
		if(actualQtyProcess){
			if(view.getApproverName()!=null)
				olbApproverName.setValue(view.getApproverName());
			txtStatus.setValue(view.getStatus());
			table.actualQtyProcess = this.actualQtyProcess;
			table.setEnable(false);
		}
		txtCreatedBy.setValue(view.getCreatedBy());
		List<PhysicalInventoryProducts> retrievedProductLis= view.getProductsLis();
		Console.log("retrievedProductLis size="+retrievedProductLis);
		List<PhysicalInventoryProducts> updatedProductLis=new ArrayList<PhysicalInventoryProducts>();
		 
		for(int i=0;i<retrievedProductLis.size();i++)
		{
			List<String> branchlist=getBranchlistByWarehouseName(retrievedProductLis.get(i).getWarehousename());
			PhysicalInventoryProducts product=retrievedProductLis.get(i);
			product.setBranchlist(branchlist);
			updatedProductLis.add(product);
		}
		Console.log("updatedProductLis size="+updatedProductLis);
		view.setProductsLis(updatedProductLis);
		presenter.setModel(view);
	}

	/************************** Toggles the app header bar menus as per screen state ****************************************/

	/*	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.contains("Search")||text.contains("Download")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.contains("Search")||text.contains("Download")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		
		AuthorizationHelper.setAsPerAuthorization(Screen.WAREHOUSEDETAILS,LoginPresenter.currentModule.trim());
	}
	
	*/
	
	 public void setCount(int count) {
		 physicalid.setValue(count+"");
	 };
	
 
 
 
	@Override
	public boolean validate() {
		boolean superValidate=super.validate();
		if(superValidate==false){
			return false;
		}
		
		if(table.getDataprovider().getList().size() == 0){
			showDialogMessage("Add Product Details.");
			return false;
		}
//		boolean inValidFlag=false;
//		String alertMsg="";
		if(table.getDataprovider().getList().size() != 0){
			List<PhysicalInventoryProducts> prodlist=this.table.getDataprovider().getList();
			boolean flag = false;
			for(int i=0;i<prodlist.size();i++){
				if((prodlist.get(i).getVariance()!=0 )
						&&( prodlist.get(i).getRemark()==null || prodlist.get(i).getRemark().trim().length()==0)){
					showDialogMessage("Please add remark for product Variace.");
					return false;
				}
				//Ashwini Patil Date:24-07-2023
				if((prodlist.get(i).getBranch()==null || prodlist.get(i).getBranch().equals(""))){
					showDialogMessage("Please add branch for product");
					return false;
				}
			}
			
			/**
			 * @author Vijay Date :- 02-04-2021
			 * Des :- added if condition if serial no process config is active then below validation should come 
			 */
			if(LoginPresenter.mapModelSerialNoFlag){
				for(PhysicalInventoryProducts object:prodlist){
					System.out.println("INSIDE NEGATIVE VARIANCE..");
					boolean serNumFlag=false;
					int variance=0;
					if(object.getVariance()<0){
						variance=(int) (object.getVariance()*(-1));
					}
					int counter=0;
					ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
					if(object.getProSerialNoDetails().containsKey(0)){
						serNumFlag=true;
						serNo.addAll(object.getProSerialNoDetails().get(0));
						for(ProductSerialNoMapping obj:serNo){
							if(obj.isStatus()==true){
								counter++;
							}
						}
					}
					if(serNumFlag&&variance!=counter){
						showDialogMessage("Please select the serial number as per the variance entered.");
//						showDialogMessage("Please enter serial number as per the product available quantity and variance.");
						return false;
					}
				}
			}
			
			
			/**
			 * @author Vijay Date :- 02-04-2021
			 * Des :- added if condition if serial no process config is active then below validation should come 
			 */
			if(LoginPresenter.mapModelSerialNoFlag){
				/**
				 * @author Anil , Date : 24-05-2019
				 * checking serial number validation as per variance
				 */
				for(PhysicalInventoryProducts object:prodlist){
					System.out.println("INSIDE POSITIVE VARIANCE..");
					boolean serNumFlag=false;
//					int variance=0;
					int maxQty=(int) object.getAvailableqty();
					if(object.getVariance()<0){
//						variance=(int) (object.getVariance()*(-1));
					}else{
						maxQty=(int) (maxQty+object.getVariance());
					}
					int counter=0;
					ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
					if(object.getProSerialNoDetails().containsKey(0)){
						serNumFlag=true;
						serNo.addAll(object.getProSerialNoDetails().get(0));
						for(ProductSerialNoMapping obj:serNo){
//							if(obj.isStatus()==true){
								counter++;
//							}
						}
					}
					if(serNumFlag&&maxQty!=counter){
						showDialogMessage("Serial number must be equal to sum of available quantity and variance.");
//						showDialogMessage("Please enter serial number as per the product available quantity and variance.");
						return false;
					}
				}
			}

		
		}
		
		return true;
	}

	/********************************************* OnClick Method *******************************************************/
	@Override
	public void onClick(ClickEvent event) {
		
	}
	
	/*********************************************Clear Method****************************************************/
	@Override
	public void clear() {
		super.clear();
		table.connectToLocal();
		this.txtStatus.setText(PhysicalInventoryMaintaince.CREATED);
	}
	
	/*************************************************Set Enable Method *********************************************/
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		physicalid.setEnabled(false);
		txtCreatedBy.setEnabled(false);
		txtStatus.setEnabled(false);
		Console.log("get call in set enabled method" + this.actualQtyProcess);
		table.actualQtyProcess = this.actualQtyProcess;
		table.setEnable(state);
	}

	/******************************************** Initializing ObjectList Elements *************************************/

	public void initializeWarehouse() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new WareHouse());
		oblwarehouse.MakeLive(querry);
	}

	/******************************************* Checking Product Data From DataBase **********************************/
	public void retriveProduct() {
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Company c=new Company();
		Filter filter =null;
		
		filter= new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
		filtervec.add(filter);
		
		filter= new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new PhysicalInventoryMaintaince());

		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							showDialogMessage("Product Already Exist!");
							productInfoComposite.clear();
						}
//						else
//						{
//							showDialogMessage("No Product ID found");
//							table.clear();
//						}
					}
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("No Product Found");
					}
				});
	}

	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(oblwarehouse)){
				retriveStorageLocation();
		}
		if(event.getSource().equals(lsStorageLoc)){
				retriveStorageBin();
		}
	}
	
	
	
	// ******************************* Retrieving Storage location from ****************************************//
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.setEnable(true);
//		setAppHeaderBarAsPerStatus();
		this.processLevelBar.setVisibleFalse(false);
		
		/**-
		 * Added by Priyanka -08-02-2021
		 * Des-Simplification of Physical Inventory View Req. raise by Nitin Sir.
		 */
		String mainScreenLabel="Physical Inventory Details";
		if(physicalinventoryobj!=null&&physicalinventoryobj.getCount()!=0){
			mainScreenLabel=physicalinventoryobj.getCount()+" "+"/"+" "+physicalinventoryobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(physicalinventoryobj.getCreationDate());
		}
		physicaldetails.setLabel(mainScreenLabel);

	}


	private void retriveStorageLocation(){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("warehouseName");
		filter.setStringValue(oblwarehouse.getValue());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new StorageLocation());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				lsStorageLoc.clear();
				lsStoragebin.clear();
				lsStorageLoc.addItem("--SELECT--");
				lsStoragebin.addItem("--SELECT--");
				for (SuperModel model : result) {
					StorageLocation entity = (StorageLocation) model;
					storageList.add(entity.getBusinessUnitName());
					lsStorageLoc.addItem(entity.getBusinessUnitName());
					System.out.println("Location :"+entity.getBusinessUnitName());
				}
				
			}
		});
	}
	
	// ****************************** Retrieving Storage Bin From Storage location **********************************
	
	private void retriveStorageBin(){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("storagelocation");
		filter.setStringValue(lsStorageLoc.getValue(lsStorageLoc.getSelectedIndex()));
		filtervec.add(filter);
		
		/**
		 *  Added by Priyanka - 28-08-2021
		 *  Des :Pecopp - if storage bin status is inactive then is should not in drop down selection raised by Ashwini
		 */
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				lsStoragebin.clear();
				lsStoragebin.addItem("--SELECT--");
				for (SuperModel model : result) {
					Storagebin entity = (Storagebin) model;
					lsStoragebin.addItem(entity.getBinName());
					System.out.println("BIN :"+entity.getBinName());
				}
				
			}
		});
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		setEnable(false);
		setAppHeaderBarAsPerStatus();
		SuperModel model=new PhysicalInventoryMaintaince();
		model=physicalinventoryobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.INVENTORYMODULE,AppConstants.PHYSICALINVENTORY, physicalinventoryobj.getCount(), null,null,null, false, model, null);
	
		/**-
		 * Added by Priyanka -08-02-2021
		 * Des-Simplification of Physical Inventory View Req. raise by Nitin Sir.
		 */
		String mainScreenLabel="Physical Inventory Details";
		if(physicalinventoryobj!=null&&physicalinventoryobj.getCount()!=0){
			mainScreenLabel=physicalinventoryobj.getCount()+" "+"/"+" "+physicalinventoryobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(physicalinventoryobj.getCreationDate());
		}
		physicaldetails.setLabel(mainScreenLabel);
		
	}
	
	@Override
	public void setToNewState(){
		super.setToNewState();
		Console.log("get call" + this.actualQtyProcess);
		setEnable(true);
	}
	
	public void setAppHeaderBarAsPerStatus()
	{
		toggleProcessLevelMenu();
		setAppHeaderBarAsPerStatusForView();
	}
	
	public void setAppHeaderBarAsPerStatusForView(){


		PhysicalInventoryMaintaince entity=(PhysicalInventoryMaintaince) presenter.getModel();
		String status=entity.getStatus();
		
		
		/**
		 * Ashwini Patil 
		 * Date:16-06-2023
		 * Removed edit button as there is no submit on this screen. Everytime we edit and save stock gets updated ie. new transaction entry gets created in inventory transaction list
		 */
		if(status.equals(PhysicalInventoryMaintaince.CREATED)||status.equals(PhysicalInventoryMaintaince.REJECTED)||status.equals(PhysicalInventoryMaintaince.APPROVED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.equals(AppConstants.NAVIGATION))//text.contains("Edit")||
				{
					menus[k].setVisible(true); 
					System.out.println("Value of text is "+menus[k].getText());
				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}

		if(status.equals(PhysicalInventoryMaintaince.APPROVED))
		{
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
					if (text.contains("Discard") || text.contains("Search")
							|| text.contains("Email") || text.contains("Print")
							|| text.equals(AppConstants.NAVIGATION)) {
						menus[k].setVisible(true);
					} else {
						menus[k].setVisible(false);
					}
			}
		}
		
		if(status.equals(PhysicalInventoryMaintaince.REQUESTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}
		
		if(status.equals(PhysicalInventoryMaintaince.REJECTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains("Edit")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}
	}
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION)|| text.contains(AppConstants.COMMUNICATIONLOG))//text.contains("Edit")||
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.WAREHOUSEDETAILS,LoginPresenter.currentModule.trim());
		
	}

	public void toggleProcessLevelMenu()
	{
		PhysicalInventoryMaintaince entity=(PhysicalInventoryMaintaince) presenter.getModel();
		
		String status=entity.getStatus();
		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();

			if(status.equals(PhysicalInventoryMaintaince.CREATED))
			{    
				if(getManageapproval().isSelfApproval()){
					/**
					 * Ajinkya added this code 
					 * Date:1/08/2017
					 */
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(true);
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					/**
					 * End here
					 */
				}else{
					/**
					 * Ajinkya added this code 
					 * Date:1/08/2017
					 */
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(true);
					/**
					 * End here
					 */
				}
				if(text.equals(manageapproval.CANCELAPPROVALREQUEST)){
					label.setVisible(false);
				}
			}else if(status.equals(PhysicalInventoryMaintaince.REJECTED)){
				if(text.equals(AppConstants.NEW))
					label.setVisible(true);
				if(text.equals(ManageApprovals.SUBMIT)){
					label.setVisible(false);
				}
				if(text.equals(manageapproval.CANCELAPPROVALREQUEST)){
					label.setVisible(false);
				}
			}else if(status.equals(PhysicalInventoryMaintaince.REQUESTED)){
				if(text.equals(ManageApprovals.SUBMIT)){
					label.setVisible(false);
				}
				if(text.equals(ManageApprovals.APPROVALREQUEST)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.NEW))
					label.setVisible(true);
				if(text.equals(manageapproval.CANCELAPPROVALREQUEST)){
					label.setVisible(true);
				}
			}else if(status.equals(PhysicalInventoryMaintaince.APPROVED)){
				if(text.equals(AppConstants.NEW))
					label.setVisible(true);
				if(text.equals(ManageApprovals.APPROVALREQUEST)){
					label.setVisible(false);
				}
				if(text.equals(ManageApprovals.SUBMIT)){
					label.setVisible(false);
				}
				if(text.equals(manageapproval.CANCELAPPROVALREQUEST)){
					label.setVisible(false);
				}
			}
		}
	}
	/*************************************************Getters And Setters*******************************************/
	
	public TextBox getPhysicalid() {
		return physicalid;
	}


	public void setPhysicalid(TextBox physicalid) {
		this.physicalid = physicalid;
	}


	public TextBox getTitle() {
		return title;
	}

	public void setTitle(TextBox title) {
		this.title = title;
	}

	public DateBox getDate() {
		return date;
	}

	public void setDate(DateBox date) {
		this.date = date;
	}

	public ProductInfoComposite getProductInfoComposite() {
		return productInfoComposite;
	}

	public void setProductInfoComposite(ProductInfoComposite productInfoComposite) {
		this.productInfoComposite = productInfoComposite;
	}

	public ObjectListBox<WareHouse> getOblwarehouse() {
		return oblwarehouse;
	}

	public void setOblwarehouse(ObjectListBox<WareHouse> oblwarehouse) {
		this.oblwarehouse = oblwarehouse;
	}

	public ListBox getLsStorageLoc() {
		return lsStorageLoc;
	}

	public void setLsStorageLoc(ListBox lsStorageLoc) {
		this.lsStorageLoc = lsStorageLoc;
	}

	public ListBox getLsStoragebin() {
		return lsStoragebin;
	}

	public void setLsStoragebin(ListBox lsStoragebin) {
		this.lsStoragebin = lsStoragebin;
	}

	public Button getNorBtn() {
		return norBtn;
	}

	public void setNorBtn(Button norBtn) {
		this.norBtn = norBtn;
	}

	public Button getMaxBtn() {
		return maxBtn;
	}

	public void setMaxBtn(Button maxBtn) {
		this.maxBtn = maxBtn;
	}

	public Button getMinBtn() {
		return minBtn;
	}

	public void setMinBtn(Button minBtn) {
		this.minBtn = minBtn;
	}

	public Button getSearBtn() {
		return searBtn;
	}

	public void setSearBtn(Button searBtn) {
		this.searBtn = searBtn;
	}

	public WareHouseDetailsTable getTable() {
		return table;
	}

	public void setTable(WareHouseDetailsTable table) {
		this.table = table;
	}

	public ArrayList<String> getStorageList() {
		return storageList;
	}

	public void setStorageList(ArrayList<String> storageList) {
		this.storageList = storageList;
	}
	

	@Override
	public TextBox getstatustextbox() {
		
		return this.txtStatus;
	}
	
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		table.getTable().redraw();
	}
	//Ashwini Patil Date:14-06-2023
	public List<String> getBranchlistByWarehouseName(String wname){
				List<WareHouse> warehouselist=oblwarehouse.getItems();
				if(wname!=null&&!wname.equals("")&&warehouselist!=null){
					Console.log("warehouselist size="+warehouselist.size());
					 for(WareHouse w:warehouselist){
						 if(w.getBusinessUnitName().equals(wname)){
							 List<String> branchlist=new ArrayList<String>();
							 for(BranchDetails detail: w.getBranchdetails()){
								 branchlist.add(detail.getBranchName());
							 }
							 if(branchlist!=null)
								 Console.log("branchlist size="+branchlist.size());
							 return branchlist;
						 }
							
					 }
				}
				return null;
	}
}
