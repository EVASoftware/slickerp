package com.slicktechnologies.client.views.inventory.warehousedetails;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;

public class WareHouseDetailsViewSearchProxy extends SearchPopUpScreen<PhysicalInventoryMaintaince> {
	
	
	IntegerBox ibphysicalCount;
	DateComparator dateComparator;
	
	
	public WareHouseDetailsViewSearchProxy() {
		super();
		createGui();
	}
	
	private void initializeWidget(){
		ibphysicalCount=new IntegerBox();
		dateComparator= new DateComparator("date",new PhysicalInventoryMaintaince());
	}
	
	@Override
	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("Physical Inventory ID", ibphysicalCount);
		FormField fibphysicalCount = fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("From Date (Physical Inventory Date)", dateComparator.getFromDate());
		FormField ffromdate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("To Date (Physical Inventory Date)", dateComparator.getToDate());
		FormField ftodate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = { 
				{ fibphysicalCount,ffromdate,ftodate },
				};

		this.fields = formfield;
	}
	
	
	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;
		
		
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		
		if(ibphysicalCount.getValue()!=null)
		{
			temp=new Filter();
			temp.setIntValue(ibphysicalCount.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new PhysicalInventoryMaintaince());
		return querry;
	}

	@Override
	public boolean validate() {
		return true;
	}

}
