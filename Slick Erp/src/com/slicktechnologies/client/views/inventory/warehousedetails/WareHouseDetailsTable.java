package com.slicktechnologies.client.views.inventory.warehousedetails;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.inventory.recievingnote.InventoryLocationPopUp;
import com.slicktechnologies.client.views.popups.ProductSerailNumberPopup;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryProducts;

public class WareHouseDetailsTable extends SuperTable<PhysicalInventoryProducts> implements ClickHandler{
	int rowIndex;
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	TextColumn<PhysicalInventoryProducts> prodIdColumn;
	TextColumn<PhysicalInventoryProducts> prodNameColumn;
	TextColumn<PhysicalInventoryProducts> prodUOMentColumn;
	TextColumn<PhysicalInventoryProducts> prodWarehouseColumn;
	TextColumn<PhysicalInventoryProducts> prodStorageLocColumn;
	TextColumn<PhysicalInventoryProducts> prodStorageBinColumn;
	
	TextColumn<PhysicalInventoryProducts> prodViewMinColumn;
	TextColumn<PhysicalInventoryProducts> prodViewMaxColumn;
	TextColumn<PhysicalInventoryProducts> prodViewNormalLevelColumn;
	TextColumn<PhysicalInventoryProducts> prodViewReorderLevelColumn;
	TextColumn<PhysicalInventoryProducts> prodViewReorderQtyColumn; 
	
	TextColumn<PhysicalInventoryProducts> prodViewAvailableQtyColumn;
	TextColumn<PhysicalInventoryProducts> prodViewVarianceQtyColumn;
	TextColumn<PhysicalInventoryProducts> prodViewRemark;//non Editable
	
	Column<PhysicalInventoryProducts,String> prodRemark; 
	Column<PhysicalInventoryProducts,String> prodVariance; //Editable
	
	/**
	 * nidhi
	 * 30-11-2018
	 * actualQty
	 */
	Column<PhysicalInventoryProducts,String> prodActalQty; 
	TextColumn<PhysicalInventoryProducts> prodViewActalQtyColumn;
	
	
	
	
	public boolean actualQtyProcess = false;
	WareHouseDetailsForm form;
	
	
	/**
	 * 
	 * 
	 */
	ProductSerailNumberPopup prodSerialPopup = new ProductSerailNumberPopup(true,false);
	Column<PhysicalInventoryProducts, String> viewProSerialNo;
	Column<PhysicalInventoryProducts, String> addProSerialNo;
	boolean tableState = false;
	
	Column<PhysicalInventoryProducts,String> editBranchColumn;//Ashwini Patil Date:14-06-2023
	TextColumn<PhysicalInventoryProducts> viewBranchColumn;//Ashwini Patil Date:14-06-2023
	BranchSelectionPopup branchpopup=new BranchSelectionPopup();
	PopupPanel panel=new PopupPanel(true);
	WareHouseDetailsTable(boolean actualQtyProcess){
		super();
		this.actualQtyProcess = actualQtyProcess;
		prodSerialPopup.getLblOk().addClickHandler(this);
		prodSerialPopup.getLblCancel().addClickHandler(this);
		branchpopup.getAddButton().addClickHandler(this);
		branchpopup.getCancelButton().addClickHandler(this);
	}
	@Override
	public void createTable() {
		createColumnProdId();
		createColumnProdName();
		createColumnProdUOMent();
		createColumnProdWarehouse();
		createColumnProdStorageLoc();
		createColumnProdStorageBin();
		createColumnViewBranch();
		
		createColumnProdMinQty();
		createColumnProdMaxQty();
		createColumnProdNormalLevle();
		createColumnProdReorderLevel();
		createColumnProdReorderQty(); 
		createColumnProdAvailableQty();
		/**
		 * nidhi 30-11-018
		 * for actual qty 
		 */
		if(actualQtyProcess){
			createColumnEditProdActualQty();
		}
		createColumnProdVarianceQty();
		createColumnProdRemarks();
		
		/**
		 * nidhi
		 * 21-08-2018
		 * for add serial number
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createColumnAddProSerialNoColumn();
		}
		
		addFieldUpdater();
	}
	
	public void createColumnAddProSerialNoColumn() {
		ButtonCell btnCell = new ButtonCell();
		addProSerialNo = new Column<PhysicalInventoryProducts, String>(btnCell) {
			@Override
			public String getValue(PhysicalInventoryProducts object) {
				return "Product Serial Nos";
			}
		};
		table.addColumn(addProSerialNo, "");
		table.setColumnWidth(addProSerialNo,180,Unit.PX);
		
		addProSerialNo.setFieldUpdater(new FieldUpdater<PhysicalInventoryProducts, String>() {
			@Override
			public void update(int index, PhysicalInventoryProducts object, String value) {
				System.out.println("ADD MODEL NUMBER BUTTON CLICKED");
				rowIndex = index;
				prodSerialPopup.setProSerialenble(true,true);
				prodSerialPopup.getPopup().setSize("50%", "40%");
				/**
				 * @author Anil, Date : 30-04-2019
				 */
				int variance=(int) object.getVariance();
				if(variance<0){
					variance=0;
				}
				
				if(object.getProSerialNoDetails()!=null && object.getProSerialNoDetails().size()>0){
					System.out.println("SERIAL DATA NOT NULL");
					ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
					if(object.getProSerialNoDetails().containsKey(0)){
						serNo.addAll(object.getProSerialNoDetails().get(0));
					}
					
					if(serNo.size() < (object.getAvailableqty()+variance)){
						int count = (int) ((object.getAvailableqty()+variance)- serNo.size());
						for(int i = 0 ; i < count ; i ++ ){
							ProductSerialNoMapping pro = new ProductSerialNoMapping();
							serNo.add(pro);
						}
					}
					prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
					prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
					prodSerialPopup.getProSerNoTable().getTable().redraw();
				}else{
					System.out.println("SERIAL DATA NULL");
					ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
						for(int i = 0 ; i < (object.getAvailableqty()+variance) ; i ++ ){
							ProductSerialNoMapping pro = new ProductSerialNoMapping();
							pro.setProSerialNo("");
							serNo.add(pro);
						}
						prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
						prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
						prodSerialPopup.getProSerNoTable().getTable().redraw();
				}
				prodSerialPopup.showPopUp();
			
			}
		});

	}
	
	public void createViewColumnAddProSerialNoColumn() {
		ButtonCell btnCell = new ButtonCell();
		viewProSerialNo = new Column<PhysicalInventoryProducts, String>(btnCell) {
			@Override
			public String getValue(PhysicalInventoryProducts object) {
				return "View Product Serial Nos";
			}
		};
		table.addColumn(viewProSerialNo, "View Details");
		table.setColumnWidth(viewProSerialNo,180,Unit.PX);
		
		viewProSerialNo.setFieldUpdater(new FieldUpdater<PhysicalInventoryProducts, String>() {
			@Override
			public void update(int index, PhysicalInventoryProducts object, String value) {
				prodSerialPopup.setProSerialenble(false,false);
				prodSerialPopup.getPopup().setSize("50%", "40%");
				prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
					if (object.getProSerialNoDetails().size() > 0) {
						ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
						System.out.println("size  -- " + serNo.size());
						serNo.addAll(object.getProSerialNoDetails().get(0));
						prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
						prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
						prodSerialPopup.getProSerNoTable().getTable().redraw();
					}
				prodSerialPopup.showPopUp();
			
			}
		});

	}
	

	public void addEditColumn(){
		createColumnProdId();
		createColumnProdName();
		createColumnProdUOMent();
		createColumnProdWarehouse();
		createColumnProdStorageLoc();
		createColumnProdStorageBin();
		createColumnViewBranch();
		addEditColumnBranch();
		createColumnProdMinQty();
		createColumnProdMaxQty();
		createColumnProdNormalLevle();
		createColumnProdReorderLevel();
		createColumnProdReorderQty(); 
		
		createColumnProdAvailableQty();     
		/**
		 * nidhi 30-11-018
		 * for actual qty 
		 */
		if(actualQtyProcess){
			createColumnEditProdActualQty();
		}
		createColumnProdVarianceQty();      //editable
		createColumnProdRemarks();
		/**
		 * nidhi
		 * 21-08-2018
		 * for add serial number
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createColumnAddProSerialNoColumn();
		}
		addFieldUpdater();
	}

	public void addViewColumn(){
		createColumnProdId();
		createColumnProdName();
		createColumnProdUOMent();
		createColumnProdWarehouse();
		createColumnProdStorageLoc();
		createColumnProdStorageBin();
		createColumnViewBranch();
		createColumnProdMinQty();
		createColumnProdMaxQty();
		createColumnProdNormalLevle();
		createColumnProdReorderLevel();
		createColumnProdReorderQty(); 
		
		createColumnProdAvailableQty();
		/**
		 * nidhi 30-11-018
		 * for actual qty 
		 */
		if(actualQtyProcess){
			createColumnProdActualQty();
		}
		createColumnViewProdVarianceQty();
		/**
		 * nidhi
		 * 21-08-2018
		 * for add serial number
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createViewColumnAddProSerialNoColumn();
		}
		createColumnViewProdRemarks();
	}
	
	@Override
	public void addFieldUpdater() {
		if(actualQtyProcess){
			updateColumnProdActualQty();
		}
		updateColumnProdVarianceQty();
		updateColumnProdRemarks();
	}
	

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addEditColumn();
		if (state == false)
			addViewColumn();
	}
	
	/***************************************************** Column View *****************************************************/
	
	public void createColumnProdId()
	{
		prodIdColumn=new TextColumn<PhysicalInventoryProducts>() {
			@Override
			public String getValue(PhysicalInventoryProducts object) {
				return object.getProductid()+"";
			}
		};
		table.addColumn(prodIdColumn,"Prod. ID");
		table.setColumnWidth(prodIdColumn,100,Unit.PX);
	}
	
	
	public void createColumnProdName()
	{
		prodNameColumn=new TextColumn<PhysicalInventoryProducts>() {
			@Override
			public String getValue(PhysicalInventoryProducts object) {
				return object.getProductname();
			}
		};
		table.addColumn(prodNameColumn,"Product Name");
		table.setColumnWidth(prodNameColumn,125,Unit.PX);
	}
	
	private void createColumnProdUOMent() {

		prodUOMentColumn=new TextColumn<PhysicalInventoryProducts>() {
			
			@Override
			public String getValue(PhysicalInventoryProducts object) {
				return object.getUnitofmeorment();
			}
		};
		table.addColumn(prodUOMentColumn,"UOM");
		table.setColumnWidth(prodUOMentColumn,65,Unit.PX);
	}
	
	public void createColumnProdWarehouse()
	{
		prodWarehouseColumn=new TextColumn<PhysicalInventoryProducts>() {
			
			@Override
			public String getValue(PhysicalInventoryProducts object) {
				return object.getWarehousename();
			}
		};table.addColumn(prodWarehouseColumn,"Warehouse");
		table.setColumnWidth(prodWarehouseColumn,120,Unit.PX);
	}
	
	public void createColumnProdStorageLoc()
	{
		prodStorageLocColumn=new TextColumn<PhysicalInventoryProducts>() {
			
			@Override
			public String getValue(PhysicalInventoryProducts object) {
				return object.getStoragelocation();
			}
		};table.addColumn(prodStorageLocColumn,"Storage Location");
		table.setColumnWidth(prodStorageLocColumn,125,Unit.PX);
	}

	public void createColumnProdStorageBin(){
		prodStorageBinColumn=new TextColumn<PhysicalInventoryProducts>() {
			@Override
			public String getValue(PhysicalInventoryProducts object) {
				return object.getStoragebin();
			}
		};table.addColumn(prodStorageBinColumn,"Storage Bin");
		table.setColumnWidth(prodStorageBinColumn,105,Unit.PX);
	}

	public void createColumnProdAvailableQty(){
		prodViewAvailableQtyColumn=new TextColumn<PhysicalInventoryProducts>() {
			@Override
			public String getValue(PhysicalInventoryProducts object) {
				return nf.format(object.getAvailableqty());
			}
		};
		table.addColumn(prodViewAvailableQtyColumn,"Available Qty");
		table.setColumnWidth(prodViewAvailableQtyColumn,105,Unit.PX);
	}
	
	
	// Product Physical Quantity ReadOnly
	
		private void createColumnViewProdVarianceQty() {
			prodViewVarianceQtyColumn=new TextColumn<PhysicalInventoryProducts>() {
				@Override
				public String getValue(PhysicalInventoryProducts object) {
					return nf.format(object.getVariance());
				}
				@Override
				public void onBrowserEvent(Context context, Element elem,
						PhysicalInventoryProducts object, NativeEvent event) {
					// TODO Auto-generated method stub
					if(actualQtyProcess){
						
					}else{
						super.onBrowserEvent(context, elem, object, event);
					}
					
				}
			};
			table.addColumn(prodViewVarianceQtyColumn,"#Variance");
			table.setColumnWidth(prodViewVarianceQtyColumn,105,Unit.PX);
		}
		
		// Product Physical Quantity Editable
		
		private void createColumnProdVarianceQty() {
			EditTextCell editCell=new EditTextCell();
			prodVariance=new Column<PhysicalInventoryProducts, String>(editCell) {
				@Override
				public String getValue(PhysicalInventoryProducts object) {
					return nf.format(object.getVariance());
				}
			};
			table.addColumn(prodVariance,"#Variance");
			table.setColumnWidth(prodVariance,105,Unit.PX);
		}
	
		// Product Remark Read Only
		private void createColumnViewProdRemarks() {
			prodViewRemark=new TextColumn<PhysicalInventoryProducts>() {
				@Override
				public String getValue(PhysicalInventoryProducts object) {
					if(object.getRemark()!=null){
					return object.getRemark();
					}
					else{
						return "";
					}
				}
			};
			table.addColumn(prodViewRemark,"#Remark");
			table.setColumnWidth(prodViewRemark,105,Unit.PX);
		}
		
		// Product Remark  Editable
		private void createColumnProdRemarks() {
			EditTextCell editCell=new EditTextCell();
			prodRemark=new Column<PhysicalInventoryProducts, String>(editCell) {
				@Override
				public String getValue(PhysicalInventoryProducts object) {
					if(object.getRemark()!=null){
						return object.getRemark();
						}
						else{
							return "";
						}
				}
			};
			table.addColumn(prodRemark,"#Remark");
			table.setColumnWidth(prodRemark,105,Unit.PX);
		}
		
		private void createColumnProdReorderQty() {

			prodViewReorderQtyColumn=new TextColumn<PhysicalInventoryProducts>() {
				@Override
				public String getValue(PhysicalInventoryProducts object) {
					return nf.format(object.getReorderqty());
				}
			};
			table.addColumn(prodViewReorderQtyColumn,"Reorder Qty");
			table.setColumnWidth(prodViewReorderQtyColumn,105,Unit.PX);
			
		}

		private void createColumnProdReorderLevel() {

			prodViewReorderLevelColumn=new TextColumn<PhysicalInventoryProducts>() {
				@Override
				public String getValue(PhysicalInventoryProducts object) {
					return nf.format(object.getReorderlevel());
				}
			};
			table.addColumn(prodViewReorderLevelColumn,"Reorder Level");
			table.setColumnWidth(prodViewReorderLevelColumn,105,Unit.PX);
		}

		private void createColumnProdNormalLevle() {

			prodViewNormalLevelColumn=new TextColumn<PhysicalInventoryProducts>() {
				@Override
				public String getValue(PhysicalInventoryProducts object) {
					return nf.format(object.getNormallevel());
				}
			};
			table.addColumn(prodViewNormalLevelColumn,"Normal Level");
			table.setColumnWidth(prodViewNormalLevelColumn,105,Unit.PX);
		}

		private void createColumnProdMaxQty() {

			prodViewMaxColumn=new TextColumn<PhysicalInventoryProducts>() {
				@Override
				public String getValue(PhysicalInventoryProducts object) {
					return nf.format(object.getMaxqty());
				}
			};
			table.addColumn(prodViewMaxColumn,"Max Qty");
			table.setColumnWidth(prodViewMaxColumn,105,Unit.PX);
		}

		private void createColumnProdMinQty() {

			prodViewMinColumn=new TextColumn<PhysicalInventoryProducts>() {
				@Override
				public String getValue(PhysicalInventoryProducts object) {
					return nf.format(object.getMinqty());
				}
			};
			table.addColumn(prodViewMinColumn,"Min Qty");
			table.setColumnWidth(prodViewMinColumn,105,Unit.PX);
		}

		
	
/********************************************* Update Column View *************************************************/
	
	
	private void updateColumnProdVarianceQty() {

		prodVariance.setFieldUpdater(new FieldUpdater<PhysicalInventoryProducts, String>() {
			@Override
			public void update(int index, PhysicalInventoryProducts object,String value) {
				try {
					double val1;
					val1 = Double.parseDouble(value.trim());
					object.setVariance(val1);
					
					if(object.getVariance()>=0){
						ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
						if(object.getProSerialNoDetails().containsKey(0)){
							serNo.addAll(object.getProSerialNoDetails().get(0));
							for(ProductSerialNoMapping obj:serNo){
								obj.setStatus(false);
							}
						}
					}
				} 
				catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	private void updateColumnProdRemarks() {

		prodRemark.setFieldUpdater(new FieldUpdater<PhysicalInventoryProducts, String>() {
			@Override
			public void update(int index, PhysicalInventoryProducts object,String value) {
				try {
					object.setRemark(value);
				} 
				catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void applyStyle() {
		
	}
	
	public void setVisible()
	{
		
	}

	public void createColumnProdActualQty(){
		prodViewActalQtyColumn=new TextColumn<PhysicalInventoryProducts>() {
			@Override
			public String getValue(PhysicalInventoryProducts object) {
				return nf.format(object.getActualQty());
			}
		};
		table.addColumn(prodViewActalQtyColumn,"Actual Qty");
		table.setColumnWidth(prodViewActalQtyColumn,105,Unit.PX);
	}
	
	private void createColumnEditProdActualQty() {
		EditTextCell editCell=new EditTextCell();
		prodActalQty=new Column<PhysicalInventoryProducts, String>(editCell) {
			@Override
			public String getValue(PhysicalInventoryProducts object) {
				return object.getActualQty()+"";
			}
		};
		table.addColumn(prodActalQty,"#Actual Qty");
		table.setColumnWidth(prodActalQty,105,Unit.PX);
	}
	
	private void updateColumnProdActualQty() {

		prodActalQty.setFieldUpdater(new FieldUpdater<PhysicalInventoryProducts, String>() {
			@Override
			public void update(int index, PhysicalInventoryProducts object,String value) {
				try {
//					DecimalFormat df=new DecimalFormat("0.00");
					if(value.trim().length()>0 && value.trim().matches("[0-9.]*")){
						double val1;
						val1 = Double.parseDouble(value.trim());
						object.setActualQty(val1);
//						if(object.getActualQty()!=0){
							double availableQty = object.getAvailableqty();
							double variance = val1- availableQty;
							
							object.setVariance(variance);
//						}
					}else{
						if(value.trim().length()>0){
							final GWTCAlert alert = new GWTCAlert(); 
						     alert.alert("Please enter numeric values onlys.");	
						}else{
							object.setActualQty(0.00);
							object.setVariance(0.00);
						}
						
					}
					
				} 
				catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource() == prodSerialPopup.getLblOk()){
			System.out.println("ONCLICK MODEL SERIAL");
			ArrayList<ProductSerialNoMapping> proList = new ArrayList<ProductSerialNoMapping>();
			List<ProductSerialNoMapping> mainproList = prodSerialPopup.getProSerNoTable().getDataprovider().getList();
			
			ArrayList<PhysicalInventoryProducts> list=new ArrayList<PhysicalInventoryProducts>();
			list.addAll(getDataprovider().getList());
			int variance=0;
			for(int i =0 ; i <mainproList.size();i++){
				System.out.println(mainproList.get(i).getProSerialNo() + "  --- get serial number -- ");
				if(mainproList.get(i).getProSerialNo().trim().length()>0){
					proList.add(mainproList.get(i));
					if(mainproList.get(i).isStatus()==true){
						variance++;
					}
				}
			}
			if(variance>0){
				variance=variance*-1;
			}
			
			if(list.get(rowIndex).getProSerialNoDetails()!=null){
				System.out.println("UPDATE SERIAL DATA NOT NULL");
				list.get(rowIndex).getProSerialNoDetails().clear();
				list.get(rowIndex).getProSerialNoDetails().put(0, proList);
				if(variance<0){
					list.get(rowIndex).setVariance(variance);
				}
//				list.get(rowIndex).setProductQuantity(count);
				getDataprovider().getList().clear();
				getDataprovider().getList().addAll(list);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			}else{
				System.out.println("UPDATE SERIAL DATA NULL");
				HashMap<Integer, ArrayList<ProductSerialNoMapping>> pro = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
				pro.put(0, proList);
				list.get(rowIndex).setProSerialNoDetails(pro);
				if(variance<0){
					list.get(rowIndex).setVariance(variance);
				}
//				list.get(rowIndex).setProductQuantity(count);
				getDataprovider().getList().clear();
				getDataprovider().getList().addAll(list);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			}
			table.redraw();
			prodSerialPopup.hidePopUp();
		}
		if(event.getSource()== prodSerialPopup.getLblCancel()){
			prodSerialPopup.hidePopUp();
		}
		if(event.getSource()==branchpopup.getAddButton())
		{
			Console.log("branchpopup add button clicked");
			Console.log("branch selected in branchpopup="+branchpopup.getlsBranch().getItemText(branchpopup.getlsBranch().getSelectedIndex()));
			ArrayList<PhysicalInventoryProducts> list=new ArrayList<PhysicalInventoryProducts>();
			if(getDataprovider().getList().size()!=0){
				list.addAll(getDataprovider().getList());
				for( int i=rowIndex;i<getDataprovider().getList().size();i++){
					list.get(rowIndex).setBranch(branchpopup.getlsBranch().getValue(branchpopup.getlsBranch().getSelectedIndex()));					
				}
				getDataprovider().getList().clear();
				getDataprovider().getList().addAll(list);
			}
			Console.log("branchpopup add button clicked list size="+getDataprovider().getList().size());
			panel.hide();
		}
		if(event.getSource()==branchpopup.getCancelButton())
		{	Console.log("branchpopup cancel button clicked");
			panel.hide();
		}
	}
	
	private void createColumnViewBranch() {
		viewBranchColumn=new TextColumn<PhysicalInventoryProducts>() {
			@Override
			public String getValue(PhysicalInventoryProducts object) {
				return object.getBranch();
			}

		};
		table.addColumn(viewBranchColumn,"#Branch");
		table.setColumnWidth(viewBranchColumn,105,Unit.PX);
	}
	public void addEditColumnBranch() {
		ButtonCell btnCell = new ButtonCell();
		editBranchColumn = new Column<PhysicalInventoryProducts, String>(btnCell) {
			@Override
			public String getValue(PhysicalInventoryProducts object) {
				return "Select Branch";
			}
		};
		table.addColumn(editBranchColumn, "");
		table.setColumnWidth(editBranchColumn,180,Unit.PX);
		
		editBranchColumn.setFieldUpdater(new FieldUpdater<PhysicalInventoryProducts, String>() {
			
			@Override
			public void update(int index, PhysicalInventoryProducts object, String value) {
				// TODO Auto-generated method stub
				panel=new PopupPanel(true);
				panel.add(branchpopup);
				branchpopup.initialzeBranches(object.getBranchlist());
				branchpopup.formView();
				panel.show();
				panel.center();
				rowIndex=index;
			}
		});

	}
	
	
}
