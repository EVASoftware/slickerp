package com.slicktechnologies.client.views.inventory.warehousedetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;

public class BranchSelectionPopup extends AbsolutePanel implements CompositeInterface {
	final static GenricServiceAsync async = GWT.create(GenricService.class);
	
	ObjectListBox<WareHouse> oblwarehouse;
	
	public static ListBox lsBranch;
	private Button addButton;
	private Button cancelButton;
	
  
	static ArrayList<String> warehouseList=new ArrayList<String>();
	
	public BranchSelectionPopup(){
		initialize();
		
		addButton = new Button("OK");
		addButton.setWidth("60px");
		add(addButton, 150, 100);
		
		cancelButton = new Button("Cancel");
		addButton.setWidth("60px");
		add(cancelButton, 250, 100);
		
		setSize("430px","200px");
		this.getElement().setId("form");
		this.getElement().addClassName("newindex"); //Ashwini Patil Date:30-01-2023
		
	}
	
	public void formView(){
		InlineLabel warehouseList = new InlineLabel("Warehouse");
		add(warehouseList, 10, 28);
		lsBranch.getElement().getStyle().setWidth(160,Unit.PX);
		add(lsBranch, 10, 60);
		
	}
	
	protected void initialize(){		
		lsBranch=new ListBox();
		lsBranch.addItem("--SELECT--");			
	}
	
	@Override
	public void setEnable(boolean status) {
		
	}

	@Override
	public boolean validate() {
		return false;
		
	}

	public void clear() {
		lsBranch.setSelectedIndex(0);
	}
	
	public static void initialzeBranches(List<String> branchlist){
		lsBranch.clear();		
		lsBranch.addItem("--SELECT--");			
		if(branchlist!=null){
			for(String s:branchlist){
				lsBranch.addItem(s);
			}
		}
		
	}	
	
	public static ListBox getlsBranch() {
		return lsBranch;
	}

	public static void setlsBranch(ListBox lsBranch) {
		BranchSelectionPopup.lsBranch = lsBranch;
	}

	public Button getAddButton() {
		return addButton;
	}

	public void setAddButton(Button addButton) {
		this.addButton = addButton;
	}

	public Button getCancelButton() {
		return cancelButton;
	}

	public void setCancelButton(Button cancelButton) {
		this.cancelButton = cancelButton;
	}




}
