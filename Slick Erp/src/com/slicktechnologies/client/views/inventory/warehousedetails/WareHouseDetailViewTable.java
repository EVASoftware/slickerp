package com.slicktechnologies.client.views.inventory.warehousedetails;

import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;

public class WareHouseDetailViewTable extends SuperTable<PhysicalInventoryMaintaince>{

	TextColumn<PhysicalInventoryMaintaince> countColumn;
	TextColumn<PhysicalInventoryMaintaince> physicalInvenDate;
	
	public WareHouseDetailViewTable() {
		super();
	}
	
	@Override
	public void createTable() {
		
		createColumnCount();
		createColumnDate();
	}

	public void createColumnCount(){
		countColumn=new TextColumn<PhysicalInventoryMaintaince>() {
			@Override
			public String getValue(PhysicalInventoryMaintaince object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(countColumn,"ID");
	}
	
	public void createColumnDate()
	{
		physicalInvenDate=new TextColumn<PhysicalInventoryMaintaince>() {
			@Override
			public String getValue(PhysicalInventoryMaintaince object) {
				return AppUtility.parseDate(object.getDate());
			}
		};table.addColumn(physicalInvenDate,"Inventory Date");
	}
	
	
	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
