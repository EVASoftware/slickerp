package com.slicktechnologies.client.views.inventory.warehousedetails;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.inventory.BranchDetails;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryProducts;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class WareHouseDetailsPresenter extends FormScreenPresenter<PhysicalInventoryMaintaince> implements ClickHandler {
	WareHouseDetailsForm form;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	final CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	public WareHouseDetailsPresenter  (FormScreen<PhysicalInventoryMaintaince> view, PhysicalInventoryMaintaince model) {
		
		super(view,model);
		
		
		form=(WareHouseDetailsForm) view;
		/**
		 * nidhi
		 * 30-11-2018
		 */
		form.actualQtyProcess = AppUtility.checkForProcessConfigurartionIsActiveOrNot("PhysicalInventoryProducts", "ActualProductQtyMapActive");
		form.searBtn.addClickHandler(this);
		form.norBtn.addClickHandler(this);
		form.minBtn.addClickHandler(this);
		form.maxBtn.addClickHandler(this);
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.WAREHOUSEDETAILS,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		if (text.equals(AppConstants.NEW))
			reactToNew();
			/**
			*nidhi */
		else 	if (text.equals(ManageApprovals.APPROVALREQUEST)){
			form.getManageapproval().reactToRequestForApproval();
		}else if(text.equals(ManageApprovals.SUBMIT)){
			form.getManageapproval().reactToSubmit();
		}else if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST)){
			form.getManageapproval().reactToApprovalRequestCancel();
		}
	}

	private void reactToNew() {
		form.setToNewState();
		initalize();
	}

	@Override
	public void reactOnPrint() {

		if(form.getTable().getDataprovider().getList().size()==0)
		{
			form.showDialogMessage("No record to download");
		}
		else
		{
			ArrayList<PhysicalInventoryMaintaince> custarray=new ArrayList<PhysicalInventoryMaintaince>();
			List<PhysicalInventoryMaintaince> list= new ArrayList<PhysicalInventoryMaintaince>();
			custarray.add((PhysicalInventoryMaintaince)model); 
			csvservice.setPhysicalInventoryList(custarray, new AsyncCallback<Void>() {
	
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}
	
				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+88;
					Window.open(url, "test", "enabled");
				}
			});
		}
	}

	@Override
	public void reactOnEmail() {

	}
	
	@Override
	public void reactOnDownload() {
		List<PhysicalInventoryMaintaince> list = (List<PhysicalInventoryMaintaince>) form.getSearchpopupscreen()
				.getSupertable().getDataprovider().getList();
		

		ArrayList<PhysicalInventoryMaintaince> custarray=new ArrayList<PhysicalInventoryMaintaince>();
		custarray.addAll(list); 
		csvservice.setPhysicalInventoryList(custarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+88;
				Window.open(url, "test", "enabled");
			}
		});
	
		
	}

	@Override
	protected void makeNewModel() {
		model = new PhysicalInventoryMaintaince();
	}
	
	public MyQuerry getLocationQuery() {
		MyQuerry quer = new MyQuerry(new Vector<Filter>(),new PhysicalInventoryMaintaince());
		return quer;
	}

	public void setModel(PhysicalInventoryMaintaince entity) {
		model = entity;
	}

	public static WareHouseDetailsForm initalize() {
		

		WareHouseDetailsForm form = new WareHouseDetailsForm();
		WareHouseDetailViewTable genTable=new WareHouseDetailViewTable();
		genTable.setView(form);
		genTable.applySelectionModle();

		WareHouseDetailsViewSearchProxy.staticSuperTable=genTable;
		WareHouseDetailsViewSearchProxy searchpopup=new WareHouseDetailsViewSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		WareHouseDetailsPresenter presenter = new WareHouseDetailsPresenter(form, new PhysicalInventoryMaintaince());
		AppMemory.getAppMemory().stickPnel(form);
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Physical Inventory",Screen.WAREHOUSEDETAILS);
		/**
		 * nidhi
		 * 30-11-2018
		 */
		form.actualQtyProcess = AppUtility.checkForProcessConfigurartionIsActiveOrNot("PhysicalInventoryProducts", "ActualProductQtyMapActive");
//		form.actualQtyProcess =true;
		return form;
		
	}
	
	
	  @Override
	  public void onClick(ClickEvent event) {
			   super.onClick(event);
				
			   if(event.getSource()==form.searBtn){
					
					if(form.oblwarehouse.getSelectedIndex()==0&&form.lsStoragebin.getSelectedIndex()==0&&form.lsStorageLoc.getSelectedIndex()==0&&form.productInfoComposite.getProdID().getValue().equals("")){
						form.showDialogMessage("Please select atleast one search criteria!");
					}
					
					if(form.oblwarehouse.getSelectedIndex()!=0||form.lsStoragebin.getSelectedIndex()!=0||form.lsStorageLoc.getSelectedIndex()!=0||!form.productInfoComposite.getProdID().getValue().equals("")){
						searchOnButton();
					}
				}

				
			//// For Normal Button.............	
				
				if(event.getSource()==form.norBtn){
					if(form.oblwarehouse.getSelectedIndex()==0&&form.lsStoragebin.getSelectedIndex()==0&&form.lsStorageLoc.getSelectedIndex()==0&&form.productInfoComposite.getProdID().getValue().equals("")){
						form.showDialogMessage("Please select atleast one search criteria!");
					}
					if(form.oblwarehouse.getSelectedIndex()!=0||form.lsStoragebin.getSelectedIndex()!=0||form.lsStorageLoc.getSelectedIndex()!=0||!form.productInfoComposite.getProdID().getValue().equals("")){
					normalOnButton();
				}}
				
				/// For Min Button.......
				
				if(event.getSource()==form.minBtn){
					if(form.oblwarehouse.getSelectedIndex()==0&&form.lsStoragebin.getSelectedIndex()==0&&form.lsStorageLoc.getSelectedIndex()==0&&form.productInfoComposite.getProdID().getValue().equals("")){
						form.showDialogMessage("Please select atleast one search criteria!");
					}
					
					if(form.oblwarehouse.getSelectedIndex()!=0||form.lsStoragebin.getSelectedIndex()!=0||form.lsStorageLoc.getSelectedIndex()!=0||!form.productInfoComposite.getProdID().getValue().equals("")){
					minOnButton();
					}
				}
				
				/// for Max Button.......
				
				if(event.getSource()==form.maxBtn){
					if(form.oblwarehouse.getSelectedIndex()==0&&form.lsStoragebin.getSelectedIndex()==0&&form.lsStorageLoc.getSelectedIndex()==0&&form.productInfoComposite.getProdID().getValue().equals("")){
						System.out.println("All Null");
						form.showDialogMessage("Please select atleast one search criteria!");
					}
					
					if(form.oblwarehouse.getSelectedIndex()!=0||form.lsStoragebin.getSelectedIndex()!=0||form.lsStorageLoc.getSelectedIndex()!=0||!form.productInfoComposite.getProdID().getValue().equals("")){
						maxOnButton();
					}
					
				}
				
	  }
	  
	/*************************************************Normal Button************************************************///

	private void normalOnButton() {
		
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;
		
		form.table.connectToLocal();
		
		if(form.oblwarehouse.getSelectedIndex()!=0)
			  {
				  temp=new Filter();
				  temp.setStringValue(form.oblwarehouse.getValue().trim());
				  temp.setQuerryString("details.warehousename");
				  filtervec.add(temp);
			  }
			
			 if(form.lsStorageLoc.getSelectedIndex()!=0)
			  {
				  temp=new Filter();
				  temp.setStringValue(form.lsStorageLoc.getValue(form.lsStorageLoc.getSelectedIndex()).trim());
				  temp.setQuerryString("details.storagelocation");
				  filtervec.add(temp);
			  }
			
			 if(form.lsStoragebin.getSelectedIndex()!=0)
			  {
				  temp=new Filter();
				  temp.setStringValue(form.lsStoragebin.getValue(form.lsStoragebin.getSelectedIndex()).trim());
				  temp.setQuerryString("details.storagebin");
				  filtervec.add(temp);
			  }
			
		if (!form.productInfoComposite.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(form.productInfoComposite.getProdID().getValue()));
			temp.setQuerryString("productinfo.prodID");
			filtervec.add(temp);
		}
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProductInventoryView());
		form.showWaitSymbol();
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						String unitofmesor=null;
						
						ArrayList<ProductInventoryViewDetails> filteredlist=new ArrayList<ProductInventoryViewDetails>();
						ArrayList<ProductInventoryViewDetails> prodDetails=new ArrayList<ProductInventoryViewDetails>();
						
						if (result.size() != 0) {
							
							for(int i=0;i<result.size();i++){
								ProductInventoryView piv=(ProductInventoryView) result.get(i);
								prodDetails.addAll(piv.getDetails());
								unitofmesor=piv.getProductUOM();
							}
							filteredlist= normal(prodDetails , 1);
							
							
							setDataToTable(filteredlist,unitofmesor);
						}
						else{
							form.showDialogMessage("No Record found");
						}
					
						
					}
					@Override
					public void onFailure(Throwable caught) {

					}
				});
		form.hideWaitSymbol();
  

		  
	  }  
	
	/****************************************search button.............***************************************************////
	public void searchOnButton(){
		
		searchAll();

	  }
	
	
/************************************************8Max Button*****************************************************/
	
	  private void maxOnButton() {

		  Vector<Filter> filtervec = new Vector<Filter>();
			Filter temp = null;
			
			form.table.connectToLocal();
			
			if (!form.productInfoComposite.getProdID().getValue().equals("")) {
				temp = new Filter();
				temp.setIntValue(Integer.parseInt(form.productInfoComposite.getProdID().getValue()));
				temp.setQuerryString("productinfo.prodID");
				filtervec.add(temp);
			}
			
			MyQuerry querry = new MyQuerry();
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ProductInventoryView());
			System.out.println("Querry Stared");
			form.showWaitSymbol();
			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							String unitofmesor=null;
							System.out.println("On Success Method");
							System.out.println("Query Executed "+ result.size());
							
							ArrayList<ProductInventoryViewDetails> filteredlist=new ArrayList<ProductInventoryViewDetails>();
							ArrayList<ProductInventoryViewDetails> prodDetails=new ArrayList<ProductInventoryViewDetails>();
							
							if (result.size() != 0) {
								System.out.println("Greater than 0 ");
								
								for(int i=0;i<result.size();i++){
									ProductInventoryView piv=(ProductInventoryView) result.get(i);
									prodDetails.addAll(piv.getDetails());
									unitofmesor=piv.getProductUOM();
								}
								System.out.println("Productlist size ::"+prodDetails.size());
								filteredlist= max(prodDetails , 2);
								
								setDataToTable(filteredlist,unitofmesor);
								
							}
							else{
								form.showDialogMessage("No Record found");
							}
						
							
						}
						@Override
						public void onFailure(Throwable caught) {

						}
					});
			form.hideWaitSymbol();
	  

			
		}

	  /*******************************************************Min Button********************************************************////
	  
		private void minOnButton() {
			

			  Vector<Filter> filtervec = new Vector<Filter>();
				Filter temp = null;
				form.table.connectToLocal();
				
				if (!form.productInfoComposite.getProdID().getValue().equals("")) {
					temp = new Filter();
					temp.setIntValue(Integer.parseInt(form.productInfoComposite.getProdID().getValue()));
					temp.setQuerryString("productinfo.prodID");
					filtervec.add(temp);
				}
				
				MyQuerry querry = new MyQuerry();
				querry.setFilters(filtervec);
				querry.setQuerryObject(new ProductInventoryView());
				System.out.println("Querry Stared");
				form.showWaitSymbol();
				async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
					
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								String unitofmesor=null;
								System.out.println("On Success Method");
								System.out.println("Query Executed "+ result.size());
								
								ArrayList<ProductInventoryViewDetails> filteredlist=new ArrayList<ProductInventoryViewDetails>();
								ArrayList<ProductInventoryViewDetails> prodDetails=new ArrayList<ProductInventoryViewDetails>();
								
								if (result.size() != 0) {
									System.out.println("Greater than 0 ");
									
									for(int i=0;i<result.size();i++){
										ProductInventoryView piv=(ProductInventoryView) result.get(i);
										prodDetails.addAll(piv.getDetails());
										unitofmesor=piv.getProductUOM();
									}
									System.out.println("Productlist size ::"+prodDetails.size());
									filteredlist= min(prodDetails , 3);
									
									
									setDataToTable(filteredlist,unitofmesor);
									
								}
								else{
									form.showDialogMessage("No Record found");
								}
							
								
							}
							@Override
							public void onFailure(Throwable caught) {

							}
						});
				form.hideWaitSymbol();
	  

			
		}
		
		
		public ArrayList<ProductInventoryViewDetails> min(ArrayList<ProductInventoryViewDetails> list, int typeOfQuantity ) {
			
			ArrayList<ProductInventoryViewDetails> filterlist=new ArrayList<ProductInventoryViewDetails>();
			
			System.out.println("Min Method Started ");
			
			boolean flage=false;
			
			for(int i=0;i<list.size();i++){
				if(list.get(i).getAvailableqty()<list.get(i).getMinqty()){
					
					filterlist.add(list.get(i));
					flage=true;
				}
			}
			
			if(flage==false){
				form.showDialogMessage("No Record Found!!!");
			}
			
			
			return filterlist;
		
		}
		
		
		
		public ArrayList<ProductInventoryViewDetails> max(ArrayList<ProductInventoryViewDetails> list, int typeOfQuantity ) {
			
			ArrayList<ProductInventoryViewDetails> filterlist=new ArrayList<ProductInventoryViewDetails>();
			
			boolean flage =false;
			for(int i=0;i<list.size();i++){
				
				
				if(list.get(i).getAvailableqty()>list.get(i).getMaxqty()){
					
					filterlist.add(list.get(i));
					flage =true;
				}
			}
			
			if(flage==false){
				form.showDialogMessage("No Record Found!!!");
			}
			
			return filterlist;
		
		}
	
		
		
		public ArrayList<ProductInventoryViewDetails> normal(ArrayList<ProductInventoryViewDetails> list, int typeOfQuantity ) {
		
			ArrayList<ProductInventoryViewDetails> filterlist=new ArrayList<ProductInventoryViewDetails>();
			
			System.out.println("Normal Method Started ");
			
			boolean flage=false;
			for(int i=0;i<list.size();i++){
				
				if(list.get(i).getAvailableqty()<list.get(i).getNormallevel()){
					
					filterlist.add(list.get(i));
					flage=true;
				}
			}
			
			if(flage==false){
				form.showDialogMessage("No Record Found!!!");
			}
			return filterlist;
		}
		
		
		
			
		
		public void searchAll() 
		{
			  Vector<Filter> filtervec = new Vector<Filter>();
			  Filter temp = null;
			 
				form.table.connectToLocal();
				
				if(form.oblwarehouse.getSelectedIndex()!=0)
					  {
						  temp=new Filter();
						  temp.setStringValue(form.oblwarehouse.getValue().trim());
						  temp.setQuerryString("details.warehousename");
						  filtervec.add(temp);
					  }
					
					 if(form.lsStorageLoc.getSelectedIndex()!=0)
					  {
						  temp=new Filter();
						  temp.setStringValue(form.lsStorageLoc.getValue(form.lsStorageLoc.getSelectedIndex()).trim());
						  temp.setQuerryString("details.storagelocation");
						  filtervec.add(temp);
					  }
					
					 if(form.lsStoragebin.getSelectedIndex()!=0)
					  {
						  temp=new Filter();
						  temp.setStringValue(form.lsStoragebin.getValue(form.lsStoragebin.getSelectedIndex()).trim());
						  temp.setQuerryString("details.storagebin");
						  filtervec.add(temp);
					  }
					
				
				
				if (!form.productInfoComposite.getProdID().getValue().equals("")) {
					temp = new Filter();
					temp.setIntValue(Integer.parseInt(form.productInfoComposite.getProdID().getValue()));
					temp.setQuerryString("productinfo.prodID");
					filtervec.add(temp);
				}
				
				MyQuerry querry = new MyQuerry();
				querry.setFilters(filtervec);
				querry.setQuerryObject(new ProductInventoryView());
				async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
					
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								 String unitofmesor=null;
								System.out.println("Query Executed "+ result.size());
								ArrayList<ProductInventoryViewDetails> prodDetails=new ArrayList<ProductInventoryViewDetails>();
								if (result.size() != 0) {
									
									for(int i=0;i<result.size();i++){
										
										ProductInventoryView piv=(ProductInventoryView) result.get(i);
										prodDetails.addAll(piv.getDetails());
										unitofmesor=piv.getProductUOM();
									}
									
									
									//calling here the common method for setting data to table.
									
									setDataToTable(prodDetails,unitofmesor);
										
								} 
								else
								{
									System.out.println("No record found");
									form.showDialogMessage("Record not Found");
								}
							}
							@Override
							public void onFailure(Throwable caught) {

							}
						});
		  
		}
		
		
		/*****************************************************************************************************/
		
		private void setDataToTable(List<ProductInventoryViewDetails> retrievedProductLis,String unitofmesor)
		{
			
			
			System.out.println(retrievedProductLis.size()+" product list ");
			
			for(int i=0;i<retrievedProductLis.size();i++)
			{
				List<String> branchlist=form.getBranchlistByWarehouseName(retrievedProductLis.get(i).getWarehousename());
				
				if(form.oblwarehouse.getSelectedIndex()==0&&form.lsStoragebin.getSelectedIndex()==0&&form.lsStorageLoc.getSelectedIndex()==0&&!form.productInfoComposite.getProdID().getValue().equals("")){
					PhysicalInventoryProducts pim=new PhysicalInventoryProducts();
					pim.setProductid(retrievedProductLis.get(i).getProdid());
					pim.setProductname(retrievedProductLis.get(i).getProdname());
					pim.setUnitofmeorment(unitofmesor);
					pim.setWarehousename(retrievedProductLis.get(i).getWarehousename());
					pim.setStoragebin(retrievedProductLis.get(i).getStoragebin());
					pim.setStoragelocation(retrievedProductLis.get(i).getStoragelocation());
					pim.setAvailableqty(retrievedProductLis.get(i).getAvailableqty());
					if(branchlist!=null)
						pim.setBranchlist(branchlist);					
					pim.setMinqty(retrievedProductLis.get(i).getMinqty());
					pim.setMaxqty(retrievedProductLis.get(i).getMaxqty());
					pim.setNormallevel(retrievedProductLis.get(i).getNormallevel());
					pim.setReorderlevel(retrievedProductLis.get(i).getReorderlevel());
					pim.setReorderqty(retrievedProductLis.get(i).getReorderqty());
					
					/**
					 * @author Anil ,Date : 29-04-2019
					 * setting products sr. no.
					 */
					if(retrievedProductLis.get(i).getProSerialNoDetails()!=null){
						pim.setProSerialNoDetails(retrievedProductLis.get(i).getProSerialNoDetails());
					}
					
					form.table.getDataprovider().getList().add(pim);
					
					
				}
				
				//loc and bin as 0
				
				else if(form.oblwarehouse.getSelectedIndex()!=0&&form.lsStoragebin.getSelectedIndex()==0&&form.lsStorageLoc.getSelectedIndex()==0){
					
					if(form.oblwarehouse.getValue().trim().equals(retrievedProductLis.get(i).getWarehousename().trim()))
					{
						PhysicalInventoryProducts pim=new PhysicalInventoryProducts();
						pim.setProductid(retrievedProductLis.get(i).getProdid());
						pim.setProductname(retrievedProductLis.get(i).getProdname());
						pim.setUnitofmeorment(unitofmesor);
						pim.setWarehousename(retrievedProductLis.get(i).getWarehousename());
						pim.setStoragebin(retrievedProductLis.get(i).getStoragebin());
						pim.setStoragelocation(retrievedProductLis.get(i).getStoragelocation());
						pim.setAvailableqty(retrievedProductLis.get(i).getAvailableqty());
						if(branchlist!=null)
							pim.setBranchlist(branchlist);	
						pim.setMinqty(retrievedProductLis.get(i).getMinqty());
						pim.setMaxqty(retrievedProductLis.get(i).getMaxqty());
						pim.setNormallevel(retrievedProductLis.get(i).getNormallevel());
						pim.setReorderlevel(retrievedProductLis.get(i).getReorderlevel());
						pim.setReorderqty(retrievedProductLis.get(i).getReorderqty());
						
						/**
						 * @author Anil ,Date : 29-04-2019
						 * setting products sr. no.
						 */
						if(retrievedProductLis.get(i).getProSerialNoDetails()!=null){
							pim.setProSerialNoDetails(retrievedProductLis.get(i).getProSerialNoDetails());
						}
						
						form.table.getDataprovider().getList().add(pim);
					}
				
				}
				
				//bin and loc not 0 means all not 0
				
				else if(form.oblwarehouse.getSelectedIndex()!=0&&form.lsStoragebin.getSelectedIndex()!=0&&form.lsStorageLoc.getSelectedIndex()!=0){
					
					if(form.lsStorageLoc.getItemText(form.lsStorageLoc.getSelectedIndex()).trim().equals(retrievedProductLis.get(i).getStoragelocation().trim())&&form.oblwarehouse.getValue().trim().equals(retrievedProductLis.get(i).getWarehousename().trim())&&form.lsStoragebin.getItemText(form.lsStoragebin.getSelectedIndex()).trim().equals(retrievedProductLis.get(i).getStoragebin().trim()))
					{
						PhysicalInventoryProducts pim=new PhysicalInventoryProducts();
						pim.setProductid(retrievedProductLis.get(i).getProdid());
						pim.setProductname(retrievedProductLis.get(i).getProdname());
						pim.setUnitofmeorment(unitofmesor);
						pim.setWarehousename(retrievedProductLis.get(i).getWarehousename());
						pim.setStoragebin(retrievedProductLis.get(i).getStoragebin());
						pim.setStoragelocation(retrievedProductLis.get(i).getStoragelocation());
						pim.setAvailableqty(retrievedProductLis.get(i).getAvailableqty());
						if(branchlist!=null)
							pim.setBranchlist(branchlist);	
						pim.setMinqty(retrievedProductLis.get(i).getMinqty());
						pim.setMaxqty(retrievedProductLis.get(i).getMaxqty());
						pim.setNormallevel(retrievedProductLis.get(i).getNormallevel());
						pim.setReorderlevel(retrievedProductLis.get(i).getReorderlevel());
						pim.setReorderqty(retrievedProductLis.get(i).getReorderqty());
						
						/**
						 * @author Anil ,Date : 29-04-2019
						 * setting products sr. no.
						 */
						if(retrievedProductLis.get(i).getProSerialNoDetails()!=null){
							pim.setProSerialNoDetails(retrievedProductLis.get(i).getProSerialNoDetails());
						}
						
						form.table.getDataprovider().getList().add(pim);
					}
				
				}
				
				//bin as 0
				
				else if(form.oblwarehouse.getSelectedIndex()!=0&&form.lsStoragebin.getSelectedIndex()==0&&form.lsStorageLoc.getSelectedIndex()!=0){
					
					if(form.lsStorageLoc.getItemText(form.lsStorageLoc.getSelectedIndex()).trim().equals(retrievedProductLis.get(i).getStoragelocation().trim())&&form.oblwarehouse.getValue().trim().equals(retrievedProductLis.get(i).getWarehousename().trim()))
					{
						PhysicalInventoryProducts pim=new PhysicalInventoryProducts();
						pim.setProductid(retrievedProductLis.get(i).getProdid());
						pim.setProductname(retrievedProductLis.get(i).getProdname());
						pim.setUnitofmeorment(unitofmesor);
						pim.setWarehousename(retrievedProductLis.get(i).getWarehousename());
						pim.setStoragebin(retrievedProductLis.get(i).getStoragebin());
						pim.setStoragelocation(retrievedProductLis.get(i).getStoragelocation());
						if(branchlist!=null)
							pim.setBranchlist(branchlist);	
						pim.setAvailableqty(retrievedProductLis.get(i).getAvailableqty());
						pim.setMinqty(retrievedProductLis.get(i).getMinqty());
						pim.setMaxqty(retrievedProductLis.get(i).getMaxqty());
						pim.setNormallevel(retrievedProductLis.get(i).getNormallevel());
						pim.setReorderlevel(retrievedProductLis.get(i).getReorderlevel());
						pim.setReorderqty(retrievedProductLis.get(i).getReorderqty());
						
						/**
						 * @author Anil ,Date : 29-04-2019
						 * setting products sr. no.
						 */
						if(retrievedProductLis.get(i).getProSerialNoDetails()!=null){
							pim.setProSerialNoDetails(retrievedProductLis.get(i).getProSerialNoDetails());
						}
						
						form.table.getDataprovider().getList().add(pim);
					}
			}
			
		}

		
		}
	
	
}
