package com.slicktechnologies.client.views.inventory.inventoryinhand;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.search.*;

import java.util.*;

import com.simplesoftwares.client.library.appstructure.tablescreen.*;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.InventoryInHand;

/**
 * TableScreen presenter template.
 */
public class InventoryInHandPresenter extends TableScreenPresenter<InventoryInHand> {
	TableScreen<InventoryInHand>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	public InventoryInHandPresenter(TableScreen<InventoryInHand> view,
			InventoryInHand model) {
		super(view, model);
		form=view;
		view.retriveTable(getServiceQuery());
		
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
	}
	
	public InventoryInHandPresenter(TableScreen<InventoryInHand> view,
			InventoryInHand model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals(""))
  reactTo();

		
	}

	public void reactOnPrint()
	{
		
		
			
	
}
	
	/*@Override
	public void reactOnDownload()
	{
		
		ArrayList<InventoryInHand> custarray=new ArrayList<InventoryInHand>();
		List<InventoryInHand> list=(List<InventoryInHand>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		custarray.addAll(list);
		
		csvservice.setinventoryinhandlist(custarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+48;
				Window.open(url, "test", "enabled");
			}
		});

	}*/


	@Override
	public void reactOnEmail() {
		
		
	}

	/*
	 * Method template to make a new model
	 */
	@Override
	protected void makeNewModel() {
		
		;
	}
	
	public static void initalize()
	{
		InventoryInHandTable table=new InventoryInHandTable();
		InventoryInHandForm form=new InventoryInHandForm(table);
		InventoryInHandSearchPopUp.staticSuperTable=table;
		
		
		
		InventoryInHandSearchPopUp searchPopUp=new InventoryInHandSearchPopUp(false);
	    form.setSearchpopupscreen(searchPopUp);
	   InventoryInHandPresenter presenter=new InventoryInHandPresenter(form, new InventoryInHand());
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	public static void initalize(MyQuerry querry)
	{
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Service List",Screen.SERVICE);
		InventoryInHandTable table=new InventoryInHandTable();
		InventoryInHandForm form=new InventoryInHandForm(table);
		InventoryInHandSearchPopUp.staticSuperTable=table;
		
		
		
		InventoryInHandSearchPopUp searchPopUp=new InventoryInHandSearchPopUp(false);
	    form.setSearchpopupscreen(searchPopUp);
	   InventoryInHandPresenter presenter=new InventoryInHandPresenter(form, new InventoryInHand(),querry);
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	/*
	 * Method template to set the query object in specific to search criteria and return the same
	 * @return MyQuerry object
	 */
	public MyQuerry getServiceQuery()
	{
		MyQuerry quer=new MyQuerry();
		quer.setQuerryObject(new InventoryInHand());
		return quer;
	}
	
	  private void reactTo()
  {
}
	  
		     
			 
			 
		 }

