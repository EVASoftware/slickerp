package com.slicktechnologies.client.views.inventory.inventoryinhand;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;

import java.util.Vector;

import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;
import com.slicktechnologies.shared.common.relationalLayer.LocationRelation;
import com.slicktechnologies.shared.common.salesprocess.ProductLineItem;
import com.slicktechnologies.shared.common.inventory.InventoryInHand;
import com.slicktechnologies.shared.common.inventory.InventoryProductDetail;

import com.simplesoftwares.client.library.composite.CommonSearchComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.client.utility.AppUtility;
import com.google.gwt.user.client.ui.CheckBox;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utility.AppConstants;

public class InventoryInHandSearchPopUp extends SearchPopUpScreen<InventoryInHand> {
  public ObjectListBox<LocationRelation> olbLocation;
  public IntegerBox ibProductId;
  public ObjectListBox<Category> olbProductCategory;
  public TextBox tbProductCode;
  public ObjectListBox<InventoryProductDetail> olbProductName;
  
  public InventoryInHandSearchPopUp()
  {
  super();
  createGui();
  }
  public InventoryInHandSearchPopUp(boolean b) {
		super(b);
		createGui();
	}
  public void initWidget()
  {
  olbLocation= new ObjectListBox<LocationRelation>();
  LocationRelation.makeObjectListBoxLive(olbLocation);
  ibProductId= new IntegerBox();
  olbProductCategory= new ObjectListBox<Category>();
  Category.makeItemCategoryListBoxLive(olbProductCategory);
  tbProductCode= new TextBox();
  olbProductName= new ObjectListBox<InventoryProductDetail>();
  InventoryProductDetail.makeInventoryProductDetailListBoxLive(olbProductName);
  }
  public void createScreen()
  {
  initWidget();
  FormFieldBuilder builder;
  builder = new FormFieldBuilder("Location",olbLocation);
  FormField folbLocation= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
  builder = new FormFieldBuilder("Product Id",ibProductId);
  FormField fibProductId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
  builder = new FormFieldBuilder("Product Category",olbProductCategory);
  FormField folbProductCategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
  builder = new FormFieldBuilder("Product Code",tbProductCode);
  FormField ftbProductCode= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
  builder = new FormFieldBuilder("Product Name",olbProductName);
  FormField folbProductName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
  this.fields=new FormField[][]{
  {folbLocation,fibProductId,folbProductCategory},
  {ftbProductCode,folbProductName,}
  };
  }
  public MyQuerry getQuerry()
  {
  Vector<Filter> filtervec=new Vector<Filter>();
  Filter temp=null;
  if(olbLocation.getSelectedIndex()!=0){
  temp=new Filter();temp.setStringValue(olbLocation.getValue().trim());
  temp.setQuerryString("item.warehouseLocation");
  filtervec.add(temp);
  }
  if(ibProductId.getValue()!=null){
  temp=new Filter();temp.setIntValue(ibProductId.getValue());
  temp.setQuerryString("item.productdetails.productID");
  filtervec.add(temp);
  }
  if(olbProductCategory.getSelectedIndex()!=0){
  temp=new Filter();temp.setStringValue(olbProductCategory.getValue().trim());
  temp.setQuerryString("item.productdetails.productCategory");
  filtervec.add(temp);
  }
  if(!(tbProductCode.getValue().trim().equals(""))){
  temp=new Filter();temp.setStringValue(tbProductCode.getValue().trim());
  temp.setQuerryString("item.productdetails.productCode");
  filtervec.add(temp);
  }
  if(olbProductName.getSelectedIndex()!=0){
  temp=new Filter();temp.setStringValue(olbProductName.getValue().trim());
  temp.setQuerryString("item.productdetails.productName");
  filtervec.add(temp);
  }
  MyQuerry querry= new MyQuerry();
  querry.setFilters(filtervec);
  querry.setQuerryObject(new InventoryInHand());
  return querry;
  }
@Override
public boolean validate() {
	return true;
}
}
