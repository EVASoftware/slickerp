package com.slicktechnologies.client.views.inventory.inventoryinhand;
import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.TableSelection;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.inventory.IncomingInventory;
import com.slicktechnologies.shared.common.inventory.InventoryInHand;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
/**
 * Table screen template
 */
public class InventoryInHandForm extends TableScreen<InventoryInHand>   implements ClickHandler{

	//Token to add variable declaration
	//
	
	public InventoryInHandForm  (SuperTable<InventoryInHand> superTable) {
		super(superTable);
		createGui();
	}
	
	
	@Override
	public void createScreen() 
	{
		//Token to initialize the processlevel menus.
		// <ReplasbleProcesslevelDeclarationLine>	
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(InventoryInHand model)
	{
		//<ReplasableUpdateModelFromViewCode>
	}

	@SuppressWarnings("unused")
	private void initalizeWidget()
	{
		//<ReplasbleVariableInitalizerBlock>
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(InventoryInHand view) 
	{
		//<ReplasableUpdateViewFromModelCode>
	}
	
	
	@Override
	public void onClick(ClickEvent event) {
		
	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Print")||text.equals("Search")||text.equals("Download"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		  		
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Download"))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Print")||text.equals("Search")||text.equals("Download")
						)
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.INVENTORYINHAND,LoginPresenter.currentModule.trim());
	}


	@Override
	public void retriveTable(MyQuerry querry) {
		superTable.connectToLocal();
		
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for(SuperModel model:result)
					superTable.getListDataProvider().getList().add((InventoryInHand) model);
				superTable.getTable().setVisibleRange(0,result.size());
				
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
			caught.printStackTrace();
				
			}
		});
		
		
	}




	

}
