package com.slicktechnologies.client.views.inventory.inventoryinhand;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.inventory.InventoryInHand;
import com.slicktechnologies.client.utility.Screen;

public class InventoryInHandTable extends SuperTable<InventoryInHand> {
	TextColumn<InventoryInHand> getQtyColumn;
	TextColumn<InventoryInHand> getCostOfInventoryColumn;
	TextColumn<InventoryInHand> getProductIdColumn;
	TextColumn<InventoryInHand> getProductCategoryColumn;
	TextColumn<InventoryInHand> getProductCodeColumn;
	TextColumn<InventoryInHand> getProductNameColumn;
	TextColumn<InventoryInHand> getUnitOfMeasurmentColumn;
	TextColumn<InventoryInHand> setLocationColumn;
	TextColumn<InventoryInHand> setStoragebinColumn;
	TextColumn<InventoryInHand> setStoragelocationcolumn;

	public InventoryInHandTable()
	{
		super();
	}
	@Override public void createTable() {
		addColumngetProductId();
		addColumngetProductCategory();
		addColumngetProductCode();
		addColumngetProductName();
		addColumngetUnitOfMeasurment();
		addColumnsetLocation();
		setStoragelocationcolumn();
		addColumnsetBin();
		addColumngetQty();
		addColumngetCostOfInventory();
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<InventoryInHand>()
				{
			@Override
			public Object getKey(InventoryInHand item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return 900L;
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetProductCategory();
		addSortinggetProductCode();
		addSortinggetProductId();
		addSortinggetProductName();
		addSortinggetUnitOfMeasurment();
		addSortinggetQty();
		addSortingsetLocation();
		addSortinggetCostOfInventory();
	}
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetProductCategory()
	{
		List<InventoryInHand> list=getDataprovider().getList();
		columnSort=new ListHandler<InventoryInHand>(list);
		columnSort.setComparator(getProductCategoryColumn, new Comparator<InventoryInHand>()
				{
			@Override
			public int compare(InventoryInHand e1,InventoryInHand e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProductCategory()!=null && e2.getProductCategory()!=null){
						return e1.getProductCategory().compareTo(e2.getProductCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetProductCategory()
	{
		getProductCategoryColumn=new TextColumn<InventoryInHand>()
				{
			@Override
			public String getValue(InventoryInHand object)
			{
				return object.getProductCategory()+"";
			}
				};
				table.addColumn(getProductCategoryColumn,"Category");
				getProductCategoryColumn.setSortable(true);
	}
	protected void addSortinggetProductCode()
	{
		List<InventoryInHand> list=getDataprovider().getList();
		columnSort=new ListHandler<InventoryInHand>(list);
		columnSort.setComparator(getProductCodeColumn, new Comparator<InventoryInHand>()
				{
			@Override
			public int compare(InventoryInHand e1,InventoryInHand e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProductCode()!=null && e2.getProductCode()!=null){
						return e1.getProductCode().compareTo(e2.getProductCode());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetProductCode()
	{
		getProductCodeColumn=new TextColumn<InventoryInHand>()
				{
			@Override
			public String getValue(InventoryInHand object)
			{
				return object.getProductCode()+"";
			}
				};
				table.addColumn(getProductCodeColumn,"Product Code");
				getProductCodeColumn.setSortable(true);
	}
	protected void addSortinggetProductId()
	{
		List<InventoryInHand> list=getDataprovider().getList();
		columnSort=new ListHandler<InventoryInHand>(list);
		columnSort.setComparator(getProductIdColumn, new Comparator<InventoryInHand>()
				{
			@Override
			public int compare(InventoryInHand e1,InventoryInHand e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getProductId()== e2.getProductId()){
						return 0;}
					if(e1.getProductId()> e2.getProductId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetProductId()
	{
		getProductIdColumn=new TextColumn<InventoryInHand>()
				{
			@Override
			public String getValue(InventoryInHand object)
			{
				if( object.getProductId()==-1)
					return "N.A";
				else return object.getProductId()+"";
			}
				};
				table.addColumn(getProductIdColumn,"Product Id");
				getProductIdColumn.setSortable(true);
	}
	protected void addSortinggetProductName()
	{
		List<InventoryInHand> list=getDataprovider().getList();
		columnSort=new ListHandler<InventoryInHand>(list);
		columnSort.setComparator(getProductNameColumn, new Comparator<InventoryInHand>()
				{
			@Override
			public int compare(InventoryInHand e1,InventoryInHand e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProductName()!=null && e2.getProductName()!=null){
						return e1.getProductName().compareTo(e2.getProductName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void setStoragelocationcolumn()
	{
		setStoragelocationcolumn=new TextColumn<InventoryInHand>() {
			
			@Override
			public String getValue(InventoryInHand object) {
				// TODO Auto-generated method stub
				return object.getStorageLoc();
			}
		};table.addColumn(setStoragelocationcolumn,"Storage Location");
	}
	
	protected void addColumngetProductName()
	{
		getProductNameColumn=new TextColumn<InventoryInHand>()
				{
			@Override
			public String getValue(InventoryInHand object)
			{
				return object.getProductName()+"";
			}
				};
				table.addColumn(getProductNameColumn,"Product Name");
				getProductNameColumn.setSortable(true);
	}
	protected void addSortinggetUnitOfMeasurment()
	{
		List<InventoryInHand> list=getDataprovider().getList();
		columnSort=new ListHandler<InventoryInHand>(list);
		columnSort.setComparator(getUnitOfMeasurmentColumn, new Comparator<InventoryInHand>()
				{
			@Override
			public int compare(InventoryInHand e1,InventoryInHand e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getUnitOfMeasurment()!=null && e2.getUnitOfMeasurment()!=null){
						return e1.getUnitOfMeasurment().compareTo(e2.getUnitOfMeasurment());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetUnitOfMeasurment()
	{
		getUnitOfMeasurmentColumn=new TextColumn<InventoryInHand>()
				{
			@Override
			public String getValue(InventoryInHand object)
			{
				return object.getUnitOfMeasurment()+"";
			}
				};
				table.addColumn(getUnitOfMeasurmentColumn,"Unit of Measurement");
				getUnitOfMeasurmentColumn.setSortable(true);
	}
	protected void addSortinggetQty()
	{
		List<InventoryInHand> list=getDataprovider().getList();
		columnSort=new ListHandler<InventoryInHand>(list);
		columnSort.setComparator(getQtyColumn, new Comparator<InventoryInHand>()
				{
			@Override
			public int compare(InventoryInHand e1,InventoryInHand e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getQty()== e2.getQty()){
						return 0;}
					if(e1.getQty()> e2.getQty()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetQty()
	{
		getQtyColumn=new TextColumn<InventoryInHand>()
				{
			@Override
			public String getValue(InventoryInHand object)
			{
				return object.getQty()+"";
			}
				};
				table.addColumn(getQtyColumn,"Qty in Hand");
				getQtyColumn.setSortable(true);
	}
	protected void addSortingsetLocation()
	{
		List<InventoryInHand> list=getDataprovider().getList();
		columnSort=new ListHandler<InventoryInHand>(list);
		columnSort.setComparator(setLocationColumn, new Comparator<InventoryInHand>()
				{
			@Override
			public int compare(InventoryInHand e1,InventoryInHand e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getWareHouseLocationName()!=null&&e2.getWareHouseLocationName()!=null)
						return e1.getWareHouseLocationName().compareTo(e2.getWareHouseLocationName());
				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumnsetLocation()
	{
		setLocationColumn=new TextColumn<InventoryInHand>()
				{
			@Override
			public String getValue(InventoryInHand object)
			{
				return object.getWareHouseLocationName();
			}
				};
				table.addColumn(setLocationColumn,"Warehouse");
				setLocationColumn.setSortable(true);
	}
	protected void addSortinggetCostOfInventory()
	{
		List<InventoryInHand> list=getDataprovider().getList();
		columnSort=new ListHandler<InventoryInHand>(list);
		columnSort.setComparator(getCostOfInventoryColumn, new Comparator<InventoryInHand>()
				{
			@Override
			public int compare(InventoryInHand e1,InventoryInHand e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCostOfInventory()== e2.getCostOfInventory()){
						return 0;}
					if(e1.getCostOfInventory()> e2.getCostOfInventory()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCostOfInventory()
	{
		getCostOfInventoryColumn=new TextColumn<InventoryInHand>()
				{
			@Override
			public String getValue(InventoryInHand object)
			{
				return object.getCostOfInventory()+"";
			}
				};
				table.addColumn(getCostOfInventoryColumn,"Cost of Inventory");
				getCostOfInventoryColumn.setSortable(true);
	}
	public void addColumnsetBin()
	{
		setStoragebinColumn=new TextColumn<InventoryInHand>() {
			
			@Override
			public String getValue(InventoryInHand object) {
				// TODO Auto-generated method stub
				return object.getStorageBin();
			}
		};table.addColumn(setStoragebinColumn,"Storage Bin");
	}
}
