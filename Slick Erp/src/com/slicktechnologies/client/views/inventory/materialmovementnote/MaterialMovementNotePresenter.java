package com.slicktechnologies.client.views.inventory.materialmovementnote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNoteForm;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNotePresenter;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNoteSerachProxy;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNoteTableProxy;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementType;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;

public class MaterialMovementNotePresenter extends ApprovableFormScreenPresenter<MaterialMovementNote> implements 
		ClickHandler,ChangeHandler,RowCountChangeEvent.Handler{

	public static  MaterialMovementNoteForm form;
	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	UpdateServiceAsync updateService=GWT.create(UpdateService.class);
	DocumentCancellationPopUp cancelPopup = new DocumentCancellationPopUp();
	PopupPanel cancelpanel ;
	
	double qty;
	int prodID;
	String wareHouse;
	String storageLocation;
	String strbin;
	int cnt =0;
	int i=0;
	
	public static MaterialMovementType matMovTyp=null;
//	double checkWithAvlbQty=checkTransaction();
	
	public MaterialMovementNotePresenter(FormScreen<MaterialMovementNote> view,MaterialMovementNote model) {
		super(view, model);
		form = (MaterialMovementNoteForm) view;
		form.getIbMmnMinId().addChangeHandler(this);
		form.getStrWarehouse().addChangeHandler(this);
		form.getStrStrLoc().addChangeHandler(this);
		form.getStrStrBin().addChangeHandler(this);
		
		cancelPopup.getBtnOk().addClickHandler(this);
		cancelPopup.getBtnCancel().addClickHandler(this);
		
		form.getOblMmnTransactionType().addChangeHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.MATERIALMOVEMENTNOTE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	public static MaterialMovementNoteForm initialize(){
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Material Movement Note",Screen.MATERIALMOVEMENTNOTE);
		MaterialMovementNoteForm form=new  MaterialMovementNoteForm();
		MaterialMovementNoteTableProxy gentable=new MaterialMovementNoteTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		
		MaterialMovementNoteSearchProxy.staticSuperTable=gentable;
		MaterialMovementNoteSearchProxy searchpopup=new MaterialMovementNoteSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		MaterialMovementNotePresenter  presenter=new MaterialMovementNotePresenter(form,new MaterialMovementNote());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		if (text.equals(AppConstants.CANCELMMN)) {
			if(!model.getStatus().equals(MaterialMovementNote.REQUESTED)){
				cancelPopup.getPaymentDate().setValue(new Date()+"");
				cancelPopup.getPaymentID().setValue(model.getCount()+"");
				cancelPopup.getPaymentStatus().setValue(model.getStatus());
				cancelpanel=new PopupPanel(true);
				cancelpanel.add(cancelPopup);
				cancelpanel.show();
				cancelpanel.center();
			}else{
				form.showDialogMessage("Cancel approval request to cancel the MMN.");
			}
		}
		if (text.equals(AppConstants.NEW))
			reactToNew();
		if (text.equals("DownLoad"))
			reactOnDownload();
		if (text.equals("Email"))
			reactOnEmail();
		if(text.equals(ManageApprovals.APPROVALREQUEST)){
			validateRequiredQuantityAtRequestFroApproval();
		}
		
		if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST)){
			form.getManageapproval().reactToApprovalRequestCancel();
		}
		
		if(text.equals(ManageApprovals.SUBMIT)){
			if(form.validate()==true){
				form.getManageapproval().reactToSubmit();
			}
			else{
				form.showDialogMessage("Please select mandatory fields.");
			}
		}
		
		/**
		 * Date : 17 -03-2017 
		 * added by rohan
		 * NBHC CCPM
		 */
		if(text.equals("Gate Pass")){
			if(form.getOblMmnTransactionType().getSelectedIndex()!=0){
				if(form.getOblMmnTransactionType().getValue().equalsIgnoreCase("TransferOUT")){
					reactOnPrintGatePass();
				}else{
					form.showDialogMessage("This is applicable for TransferOut Only !");
				}
			}
		}
	}
	
	private void reactOnPrintGatePass() {
		
		final String url = GWT.getModuleBaseURL()+"pdfprintservice"+"?Id="+model.getId()
				+"&"+"type="+"Gatepass";
		Window.open(url, "test", "enabled");
	}
	
	
	private void reactToNew() {
		form.setToNewState();
		initialize();
	}
	
	@Override
	public void reactOnDownload() {
		ArrayList<MaterialMovementNote> minArray = new ArrayList<MaterialMovementNote>();
		List<MaterialMovementNote> list = (List<MaterialMovementNote>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		minArray.addAll(list);
		csvservice.setMaterialMovementNote(minArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 71;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
		boolean conf = Window.confirm("Do you really want to send email?");
		if (conf == true) {
			emailService.initiateMMNEmail((MaterialMovementNote) model,new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Resource Quota Ended ");
					caught.printStackTrace();
				}

				@Override
				public void onSuccess(Void result) {
					Window.alert("Email Sent Sucessfully !");
				}
			});
		}
	}

	@Override
	protected void makeNewModel() {
		model=new MaterialMovementNote();
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		
	}

	@Override
	public void onChange(ChangeEvent event) {
		if (event.getSource() == form.getIbMmnMinId()) {
			if (form.getIbMmnMinId().getValue() != null) {
				setDataFromMIN();
			}
		}
		
		
		if (event.getSource().equals(form.getOblMmnTransactionType())){
//			MaterialMovementType mmtType=null;
			matMovTyp=null;
			if(form.getOblMmnTransactionType().getSelectedIndex()!=0){
				matMovTyp = form.getOblMmnTransactionType().getSelectedItem();
				form.getTransDirection().setValue(matMovTyp.getMmtType());
			}
			
			
			if(form.getOblMmnTransactionType().getValue().equalsIgnoreCase("TransferIN")){
				
				form.showDialogMessage("TransferIN is not applicable");
				form.getOblMmnTransactionType().setSelectedIndex(0);
			}
			
			else if(form.getOblMmnTransactionType().getValue().equalsIgnoreCase("TransferOUT")){
				form.getStrStrBin().setVisible(true);
				form.getStrWarehouse().setVisible(true);
				form.getStrStrLoc().setVisible(true);
				
				
			}else{
			form.getStrWarehouse().setSelectedIndex(0);
			form.getStrStrBin().setSelectedIndex(0);
			form.getStrStrLoc().setSelectedIndex(0);
			form.getStrStrBin().setVisible(false);
			form.getStrWarehouse().setVisible(false);
			form.getStrStrLoc().setVisible(false);
			
		}
		
		if(form.getOblMmnTransactionType().getSelectedIndex()==0)
		{
			
			form.getStrStrBin().setVisible(false);
			form.getStrWarehouse().setVisible(false);
			form.getStrStrLoc().setVisible(false);
			form.getTransDirection().setValue("");	
			form.getStrStrBin().setSelectedIndex(0);
			form.getStrStrLoc().setSelectedIndex(0);
			form.getStrWarehouse().setValue("");
		}
			
		}
			
		if(event.getSource().equals(form.strWarehouse)){
			retriveStorageLocation();
			form.getSubProductTableMmn().connectToLocal();
		}
		if(event.getSource().equals(form.strStrLoc)){
			
			retriveStorageBin();
			form.getSubProductTableMmn().connectToLocal();
		}
		if(event.getSource().equals(form.strStrBin)){
			form.getSubProductTableMmn().connectToLocal();
		}
	}
	
	/********************************************************************************************************************/

	
	
private void retriveStorageLocation(){
		
		form.showWaitSymbol();
		form.getStrStrLoc().clear();
		form.getStrStrLoc().addItem("SELECT");
		Timer timer=new Timer() 
	     {
			@Override
			public void run() 
			{
				
				MyQuerry querry = new MyQuerry();
				Company c=new Company();
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter filter = null;
				
				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("warehouseName");
				filter.setStringValue(form.getStrWarehouse().getValue());
				filtervec.add(filter);
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new StorageLocation());
				
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						form.showDialogMessage("An Unexpected error occurred!");
					}
		
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for(SuperModel model:result)
						{
							StorageLocation locEntity=(StorageLocation)model;
							form.getStrStrLoc().addItem(locEntity.getBusinessUnitName());
						}
						}
					 });
		
		 form.hideWaitSymbol();
			}
	     };
    	 timer.schedule(2000);
    	
	}

	private void retriveStorageBin(){
		
		form.showWaitSymbol();
		form.getStrStrBin().clear();
		form.getStrStrBin().addItem("SELECT");
		Timer timer=new Timer() 
	     {
			@Override
			public void run() 
			{
		
			MyQuerry querry = new MyQuerry();
			Company c=new Company();
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("storagelocation");
			filter.setStringValue(form.getStrStrLoc().getItemText(form.getStrStrLoc().getSelectedIndex()).trim());
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Storagebin());
			
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
					form.showDialogMessage("An Unexpected error occurred!");
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel model:result)
					{
						Storagebin binEntity=(Storagebin)model;
						if(binEntity.getStatus()) //Ashwini Patil Date:5-09-2023
							form.getStrStrBin().addItem(binEntity.getBinName());
					}
					}
				 });
		
		
		
		 form.hideWaitSymbol();
			}
	     };
    	 timer.schedule(2000);
    	
	}

	
	@Override
	public void onClick(ClickEvent event) {
			super.onClick(event);
			
			
			if(event.getSource()==cancelPopup.getBtnOk()){
				cancelpanel.hide();
				
				if(model.getStatus().equals(MaterialMovementNote.CREATED)){
					changeStatus(MaterialMovementNote.CANCELLED);
				}else if(model.getStatus().equals(MaterialMovementNote.APPROVED)){
					cancelMMN();
				}
			}
			
			if(event.getSource()==cancelPopup.getBtnCancel()){
					cancelpanel.hide();
					cancelPopup.remark.setValue("");
				}
			
		}
	
	
	
	/**
	 * This is used to cancel MMN in approved status and revert stock
	 * Date : 13/02/2017
	 */
	
	
	private void cancelMMN() {
		model.setStatus(MaterialIssueNote.CANCELLED);
		model.setMmnDescription("MMN ID ="+model.getCount()+" "+"MMN status ="+form.getTbMmnStatus().getValue().trim()+"\n"
				+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"
				+"Remark ="+cancelPopup.getRemark().getValue().trim()+" "+"Cancellation Date ="+AppUtility.parseDate(new Date()));
		form.showWaitSymbol();
		updateService.cancelDocument(model, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("An Unexpected Error occured !");
			}
			@Override
			public void onSuccess(String result) {
				form.hideWaitSymbol();
				if(result.equalsIgnoreCase("MMN Cancelled Successfully.")){
					form.showDialogMessage(result);
					form.getTbMmnStatus().setText(model.getStatus());
					form.getTaMmnDescription().setValue(model.getMmnDescription());
				}else{
					form.showDialogMessage(result);
					model.setStatus(MaterialIssueNote.APPROVED);
				}
				form.setToViewState();
			}
		});
		
	}
	
	
	public void changeStatus(final String status)
	{
		model.setStatus(status);
		model.setMmnDescription("MMN ID ="+model.getCount()+" "+"MMN status ="+form.getTbMmnStatus().getValue().trim()+"\n"
				+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"
				+"Remark ="+cancelPopup.getRemark().getValue().trim()+" "+"Cancellation Date ="+new Date());
		async.save(model,new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				
				form.tbMmnStatus.setText(status);
				form.getTaMmnDescription().setValue(model.getMmnDescription());
				form.setToViewState();
			}
		});
	}
	
	
	
	
	
	
	
	
//	private void setDataFromMRN() {
//		MyQuerry querry = new MyQuerry();
//		Filter filter = new Filter();
//		filter.setQuerryString("count");
//		filter.setIntValue(form.getIbMmnMrnId().getValue());
//		querry.getFilters().add(filter);
//		querry.setQuerryObject(new MaterialRequestNote());
//		form.showWaitSymbol();
//		async.getSearchResult(querry,
//				new AsyncCallback<ArrayList<SuperModel>>() {
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						if (result.size() != 0) {
//							MaterialRequestNote mrn=(MaterialRequestNote) result.get(0);;
//							if(mrn.getStatus().equals("Approved"))
//							{
//								form.getIbMmnMinId().setValue(null);
//								form.getDbMmnMinDate().setValue(null);
//								
//								if(mrn.getSubProductTableMrn()!=null){
//									form.getSubProductTableMmn().getDataprovider().setList(mrn.getSubProductTableMrn());
//								}
//								form.getDbMmnMrnDate().setValue(mrn.getMrnDate());
//								form.productInfoComposite.setEnable(false);
//								form.getOblMmnBranch().setValue(mrn.getBranch());
//								form.getOblMmnBranch().setEnabled(false);
//								
//
//								form.getOblMmnApproverName().setValue(mrn.getApproverName());
//								if (form.getOblMmnApproverName().getValue() != null) {
//									form.getOblMmnApproverName().setEnabled(false);
//								}
//								
//								form.getOblMmnRequestedBy().setValue(mrn.getEmployee());
//								if (form.getOblMmnRequestedBy().getValue() != null) {
//									form.getOblMmnRequestedBy().setEnabled(false);
//								}
//							}
//							else{
//								form.showDialogMessage("MRN ID is Not Approved");
//								form.setToNewState();
//								form.getSubProductTableMmn().clear();
//							}
//						}else
//						{
//							form.showDialogMessage("No MRN ID found");
//							form.setToNewState();
//							form.getSubProductTableMmn().clear();
//						}
//					}
//
//					@Override
//					public void onFailure(Throwable caught) {
//
//					}
//				});
//		form.hideWaitSymbol();
//	}


	public void setDataFromMIN(){
		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(form.getIbMmnMinId().getValue()));
		querry.getFilters().add(filter);
		querry.setQuerryObject(new MaterialIssueNote());
		form.showWaitSymbol();
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							MaterialIssueNote min=(MaterialIssueNote) result.get(0);;
							if(min.getStatus().equals("Approved"))
							{
								if(min.getSubProductTablemin()!=null){
									form.getSubProductTableMmn().getDataprovider().setList(min.getSubProductTablemin());
								}
								form.getDbMmnMinDate().setValue(min.getMinDate());
								form.productInfoComposite.setEnable(false);
								
//								form.getIbMmnMrnId().setValue(min.getMinMrnId());
//								form.getDbMmnMrnDate().setValue(min.getMinMrnDate());
//								if (form.getDbMmnMrnDate().getValue() != null) {
//									form.getDbMmnMrnDate().setEnabled(false);
//								}

								form.getOblMmnBranch().setValue(min.getBranch());
								if(min.getMinSoId()!=0){
									form.getOrderId().setValue(min.getMinSoId()+"");
									form.getOrderId().setEnabled(false);
								}
								if(min.getMinWoId()!=0){
									form.getIbMmnWoId().setValue(min.getMinWoId()+"");
									form.getIbMmnWoId().setEnabled(false);
								}
								if(min.getMinWoDate()!=null){
									form.getDbMmnWoDate().setValue(min.getMinWoDate());
									form.getDbMmnWoDate().setEnabled(false);
								}

								form.getOblMmnApproverName().setValue(min.getApproverName());
								if (form.getOblMmnApproverName().getValue() != null) {
								}
								
								form.getOblMmnRequestedBy().setValue(min.getEmployee());
								if (form.getOblMmnRequestedBy().getValue() != null) {
								}
								form.checkMinQuantity(Integer.parseInt(form.getIbMmnMinId().getValue()));
							}
							else{
								form.showDialogMessage("MIN ID is Not Approved");
								form.setToNewState();
								form.getSubProductTableMmn().clear();
							}
						}else
						{
							form.showDialogMessage("No MIN ID found");
							form.setToNewState();
							form.getSubProductTableMmn().clear();
						}
					}

					@Override
					public void onFailure(Throwable caught) {

					}
				});
		form.hideWaitSymbol();
	}
	
//	public double checkTransaction(){
//		MyQuerry query = new MyQuerry();
//		Company c=new Company();
//		System.out.println("Company Id :: "+c.getCompanyId());
//		
//		Vector<Filter> filtervec=new Vector<Filter>();
//		Filter filter = null;
//		
//		filter = new Filter();
//		filter.setQuerryString("companyId");
//		filter.setLongValue(c.getCompanyId());
//		filtervec.add(filter);
//		
//		filter = new Filter();
//		filter.setQuerryString("mmtName");
//		filter.setStringValue(form.getOblMmnTransactionType().getValue());
//		filtervec.add(filter);
//		
//		query.setFilters(filtervec);
//		query.setQuerryObject(new MaterialMovementType());
//		
//		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
//			@Override
//			public void onFailure(Throwable caught) {
//				
//			}
//
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				for (SuperModel model : result) {
//					MaterialMovementType entity = (MaterialMovementType) model;
//					
//				}
//			}
//		});
//		return 0;
//	}
/********************************************************************************************************************/	
	
	
	
	
	public void validateRequiredQuantityAtRequestFroApproval() {
		
		if(form.getOblMmnTransactionType().getValue().equals("TransferOUT")
				||form.getOblMmnTransactionType().getValue().equals("Scrap")){
		
		final List<MaterialProduct> list = form.subProductTableMmn.getDataprovider().getList();
		for (final MaterialProduct temp : list) {
				System.out.println("list size::::::::::"+list.size());
				prodID=temp.getMaterialProductId();
				

		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(prodID);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProductInventoryView());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
				}
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					qty=temp.getMaterialProductRequiredQuantity();
					wareHouse=temp.getMaterialProductWarehouse().trim();
					storageLocation=temp.getMaterialProductStorageLocation().trim();
					strbin=temp.getMaterialProductStorageBin().trim();
					i=i+1;
					for(SuperModel model:result)
					{
						
						ProductInventoryView sup=(ProductInventoryView)model;
						
										for(int i=0;i<sup.getDetails().size();i++){
											
											if(wareHouse.equals(sup.getDetails().get(i).getWarehousename())){
												if(storageLocation.equals(sup.getDetails().get(i).getStoragelocation())){
											if(strbin.equals(sup.getDetails().get(i).getStoragebin())){
											 if(qty >sup.getDetails().get(i).getAvailableqty()){
												form.showDialogMessage("Required quantity is greatter than available quantity."+" Availabe Quantity="+sup.getDetails().get(i).getAvailableqty());
												cnt=cnt+1;
											}}}
											}
										}
				           	}
					if(list.size()==1&&cnt==0){
						form.getManageapproval().reactToRequestForApproval();
					}
					else if(i==list.size()-1&& cnt==0){
						form.getManageapproval().reactToRequestForApproval();
					}
				     }
				});
	}	
		}else{form.getManageapproval().reactToRequestForApproval();}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
