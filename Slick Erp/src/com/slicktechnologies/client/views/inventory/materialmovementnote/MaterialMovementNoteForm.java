package com.slicktechnologies.client.views.inventory.materialmovementnote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.inventory.materialreuestnote.SubProductTableMrn;
import com.slicktechnologies.client.views.inventory.productinventoryview.ProductInventoryViewForm;
import com.slicktechnologies.client.views.inventory.recievingnote.ProductQty;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementType;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class MaterialMovementNoteForm extends ApprovableFormScreen<MaterialMovementNote> implements ClickHandler, ChangeHandler{
	final GenricServiceAsync async = GWT.create(GenricService.class);
	ArrayList<ProductQty> minList;
	ArrayList<ProductQty> mmnList;
	ArrayList<ProductQty> compList;
//	IntegerBox ibMmnMrnId;
	TextBox ibMmnMinId;
//	DateBox dbMmnMrnDate;
	DateBox dbMmnMinDate;
	TextBox tbserviceId;
	TextBox ibMmnWoId;
	DateBox dbMmnWoDate;
	
	TextBox ibMmnId;
	DateBox dbMmnDate;
	
	TextBox tbMmnTitle;
	
	ObjectListBox<ConfigCategory> oblMmnCategory;
	ObjectListBox<Type> oblMmnType;
	ObjectListBox<MaterialMovementType> oblMmnTransactionType;
	
	ObjectListBox<Branch> oblMmnBranch;
	ObjectListBox<Employee> oblMmnApproverName;
	ObjectListBox<Employee> oblMmnRequestedBy;
	ObjectListBox<Employee> oblMmnPersonResponsible;
	TextBox orderId;
	FormField fMmnMmnInfoGrouping ;
	TextBox tbMmnStatus;
	TextBox tbMmnCommentOnStatusChange;
	
	UploadComposite upload;
	TextArea taMmnDescription;
	
	ProductInfoComposite productInfoComposite;
	Button btnAdd;
//	SubProductTableMrn subProductTableMmn;	
	SubProductTableMMN subProductTableMmn;
	
	
	/**
	 * Developed by : Rohan Bhagde.
	 * Date : 15/11/2016
	 * Reason : This branch field is used for transfer to approver branch used for branch lelev restriction  
	 */
	
	ObjectListBox<Branch> oblMmnTransferToBranch;
	
	/**
	 * ends here 
	 */
	/************************************Transfer WareHosue**********************************************/
	
	ObjectListBox<WareHouse>strWarehouse;
	ListBox strStrLoc;
	ListBox strStrBin;
	
	
	
	TextBox transDirection;
	double qty;
	int prodID;
	String wareHouse;
	String strbin;
	MaterialMovementNote mmnobj;
	
	
	/**
	 * Developed by : Rohan Bhagde 
	 * Used for :used calling public method present in ProductInventoryViewForm for initializing warehouse
	 * Method name :initializeWarehounse()
	 * Dated : 20/09/2016
	 */
	ProductInventoryViewForm productInventory;
	/**
	 * ends here 
	 */
	
	public MaterialMovementNoteForm(){
		super();
		createGui();
		this.ibMmnId.setEnabled(false);
		this.tbMmnStatus.setEnabled(false);
		this.tbMmnStatus.setText(MaterialIssueNote.CREATED);		
		this.dbMmnWoDate.setEnabled(false);
		this.tbMmnCommentOnStatusChange.setEnabled(false);
//		this.dbMmnMrnDate.setEnabled(false);
		this.dbMmnMinDate.setEnabled(false);
		subProductTableMmn.connectToLocal();
		
		Timer timer=new Timer() {
			@Override
			public void run() {
				setDefaultDataOfLoggedInUser();
			}
		};
		timer.schedule(4000);
	}
	
	protected void setDefaultDataOfLoggedInUser() {
		oblMmnRequestedBy.setValue(LoginPresenter.loggedInUser);
		oblMmnApproverName.setValue(LoginPresenter.loggedInUser);
		oblMmnPersonResponsible.setValue(LoginPresenter.loggedInUser);
		
		if(oblMmnRequestedBy.getValue()!=null){
			Employee emp=oblMmnRequestedBy.getSelectedItem();
			oblMmnBranch.setValue(emp.getBranchName());
		}
	}

	public MaterialMovementNoteForm(String[] processlevel, FormField[][] fields,FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
	}
	
	private void initializeWidget(){
//		ibMmnMrnId=new IntegerBox();
//		dbMmnMrnDate=new DateBoxWithYearSelector();
		ibMmnMinId=new TextBox();
		dbMmnMinDate=new DateBoxWithYearSelector();
		
		ibMmnWoId=new TextBox();
		dbMmnWoDate=new DateBoxWithYearSelector();
		
		ibMmnId=new TextBox();
		dbMmnDate=new DateBoxWithYearSelector();
		tbMmnTitle=new TextBox();
		
		oblMmnTransactionType=new ObjectListBox<MaterialMovementType>();
		initalizTransactionName();
		
		
		oblMmnCategory= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblMmnCategory, Screen.MMNCATEGORY);
		oblMmnCategory.addChangeHandler(this);
		oblMmnType = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(oblMmnType,Screen.MMNTYPE);
		oblMmnBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(oblMmnBranch);
		
		oblMmnApproverName = new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(oblMmnApproverName, "MMN");
		
		oblMmnPersonResponsible = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblMmnPersonResponsible);
		oblMmnPersonResponsible.makeEmployeeLive(AppConstants.INVENTORYMODULE, "Material Movement Note", "Person Responsible");
		
		oblMmnRequestedBy = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblMmnRequestedBy);
		oblMmnRequestedBy.makeEmployeeLive(AppConstants.INVENTORYMODULE,"Material Movement Note", "Requested By");
		
		tbMmnStatus=new TextBox();
		tbMmnCommentOnStatusChange=new TextBox();
		
		upload=new UploadComposite();
		taMmnDescription=new TextArea();
		
		productInfoComposite = AppUtility.initiateMrnProductComposite(new SuperProduct());
		
		btnAdd=new Button("ADD");
		btnAdd.addClickHandler(this);
		subProductTableMmn=new SubProductTableMMN();
		
		
		/**
		 * rohan does not modify this code because NBHC requires that all warehouse should be loaded in warehouse 
		 * where they want to transfer
		 * 
		 */
		
		strWarehouse = new ObjectListBox<WareHouse>();
		/**
		 * Date 18-06-2019 By Vijay
		 * For NBHC IM allow to load all cluster in Transfer To DropDown with process Config. even if branch level restriction
		 * and Should not show expiry warehouses
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialMovementNote", "EnableLoadAllClusterInTransferTo")){
			strWarehouse.MakeLiveWarehouse();
		}
		else{
			strWarehouse.MakeLive(new MyQuerry(new Vector<Filter>(),new WareHouse()));
		}
		
//		/**
//		 * Developed by : Rohan Bhagde 
//		 * Used for :used calling public method present in ProductInventoryViewForm for initializing warehouse
//		 * Method name :initializeWarehounse()
//		 * Dated : 20/09/2016
//		 */
//		
//			productInventory =new ProductInventoryViewForm();
//			productInventory.initializeWarehounse(strWarehouse);
//		/**
//		 * ends here 
//		 */
		
		

		
		strStrLoc=new ListBox();
		strStrLoc.addItem("SELECT");
		strStrBin=new ListBox();
		strStrBin.addItem("SELECT");
		tbserviceId=new TextBox();
		transDirection=new TextBox();
		
		orderId=new TextBox();
		
		
//		oblMmnTransferToBranch = new ObjectListBox<Branch>();
//		oblMmnTransferToBranch.MakeLive(query);
		
	}

	@Override
	public void createScreen() {
		initializeWidget();
		
		
		//Ashwini Patil Date:27-03-2023 commenting this code as we want to give Gate pass as standard option. without process configuration.
//		/**
//		 * Date : 17-03-2017 
//		 * added by Rohan
//		 * NBHC CCPM
//		 */
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialMovementNote","OnlyForNBHC"))
//		{
//			System.out.println("!!!!!!!!!!!!!!!!!!!!! ONLY FOR NBHC");
//			this.processlevelBarNames = new String[] {
//					ManageApprovals.APPROVALREQUEST,
//					ManageApprovals.CANCELAPPROVALREQUEST,ManageApprovals.SUBMIT, AppConstants.CANCELMMN,
//					AppConstants.NEW , "Gate Pass"};
//		}
//		else{
			this.processlevelBarNames = new String[] {
					ManageApprovals.APPROVALREQUEST,
					ManageApprovals.CANCELAPPROVALREQUEST,ManageApprovals.SUBMIT, AppConstants.CANCELMMN,
					AppConstants.NEW , "Gate Pass"};
//		}
		
		String mainScreenLabel="MATERIAL MOVEMENT NOTE";
		if(mmnobj!=null&&mmnobj.getCount()!=0){
			mainScreenLabel=mmnobj.getCount()+" "+"/"+" "+mmnobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(mmnobj.getCreationDate());
		}
		//fMmnMmnInfoGrouping.getHeaderLabel().setText(mainScreenLabel);
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fMmnMmnInfoGrouping=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		

		
		fbuilder = new FormFieldBuilder("MIN ID", ibMmnMinId);
		FormField fibMmnMinId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("MIN Date", dbMmnMinDate);
		FormField fdbMmnMinDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		
//		fbuilder = new FormFieldBuilder("MRN ID", ibMmnMrnId);
//		FormField fibMmnMrnId = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//
//		fbuilder = new FormFieldBuilder("MRN Date", dbMmnMrnDate);
//		FormField fdbMmnMrnDate = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Work Order Nr.", ibMmnWoId);
		FormField fibMmnWoId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Work Order Date", dbMmnWoDate);
		FormField fdbMmnWoDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		
		
		fbuilder = new FormFieldBuilder("MMN ID", ibMmnId);
		FormField fibMmnId= fbuilder.setMandatory(false).setMandatoryMsg("MMN ID is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MMN Title", tbMmnTitle);
		FormField ftbMmnTitle = fbuilder.setMandatory(false).setMandatoryMsg("MMN Title is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* MMN Date", dbMmnDate);
		FormField fdbMmnDate = fbuilder.setMandatory(true).setMandatoryMsg("MMN Date is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MMN Category", oblMmnCategory);
		FormField foblMmnCategory = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MMN Type", oblMmnType);
		FormField foblMmnType = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Transaction Type", oblMmnTransactionType);
		FormField foblMmnTransactionType = fbuilder.setMandatory(true).setMandatoryMsg("Transaction Type is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		

		FormField foblMmnBranch=null;
		if(AppUtility.isBranchNonMandatory("MaterialMovementNote")){
			fbuilder = new FormFieldBuilder("Branch", oblMmnBranch);
			foblMmnBranch = fbuilder.setMandatory(false).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("* Branch", oblMmnBranch);
			foblMmnBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}
		
		fbuilder = new FormFieldBuilder("* Approver", oblMmnApproverName);
		FormField foblMmnApproverName = fbuilder.setMandatory(true).setMandatoryMsg("Approver is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		/**
		 *  Added By : Priyanka
		 *  Des : PECOPP ON MRN,MIN,MMN replace label "Requested By" to "Requested To" Raised By Ashwini.
		 */
		//fbuilder = new FormFieldBuilder("* Requested By", oblMrnRequestedBy);
		fbuilder = new FormFieldBuilder("* Requested To", oblMmnRequestedBy);
		FormField foblMmnRequestedBy = fbuilder.setMandatory(true).setMandatoryMsg("Requested By is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Person Responsible", oblMmnPersonResponsible);
		FormField foblMmnPersonResponsible = fbuilder.setMandatory(true).setMandatoryMsg("Issued By is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", tbMmnStatus);
		FormField ftbMmnStatus = fbuilder.setMandatory(true).setMandatoryMsg("Status is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Comment", tbMmnCommentOnStatusChange);
		FormField ftbMmnCommentOnStatusChange = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Document", upload);
		FormField fupload = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Description(Max 500 characters)", taMmnDescription);
		FormField ftaMmnDescription = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		FormField fMmnSubProductGrouping = fbuilder.setlabel("Product")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fbtnAdd= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", subProductTableMmn.getTable());
		FormField fsubProductTableMmn= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Order ID",orderId);
		FormField forderId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		FormField fMmnRefereceDetailGrouping = fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fMmnclassificationDetailGrouping = fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fMmnAttachmentGrouping = fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		/*********************************Transfer Warehouse**********************************/
		
		fbuilder = new FormFieldBuilder("Transfer to - Warehouse",strWarehouse);
		FormField fstrWarehouse = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" Transfer to - Storage Bin",strStrBin);
		FormField fstrStrBin = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Transfer To - Storage Location",strStrLoc);
		FormField fstrStrLoc = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Trans Direction",transDirection);
		FormField ftransDitection = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		
		FormField[][] formfield = { 
//				{ fMmnRefereceDetailGrouping },
//				{ fibMmnMinId,fdbMmnMinDate,fibMmnWoId,fdbMmnWoDate},
//				{forderId},
//				{ fMmnMmnInfoGrouping},
//				{ fibMmnId,ftbMmnTitle, fdbMmnDate,foblMmnTransactionType},
//				{ foblMmnCategory,foblMmnType,foblMmnBranch,foblMmnPersonResponsible},
//				{ foblMmnApproverName,foblMmnRequestedBy,ftbMmnStatus,ftbMmnCommentOnStatusChange},
//				{ fstrWarehouse,fstrStrLoc,fstrStrBin},
//				{ fupload},
//				{ ftaMmnDescription},
//				{ fMmnSubProductGrouping},
//				{ fproductInfoComposite,fbtnAdd},
//				{ fsubProductTableMmn}
			
				
				
				{ fMmnMmnInfoGrouping},
				{ ftbMmnTitle, fdbMmnDate,foblMmnTransactionType,foblMmnBranch,},
				{ foblMmnRequestedBy,foblMmnPersonResponsible,foblMmnApproverName,ftbMmnCommentOnStatusChange},
				{ fstrWarehouse,fstrStrLoc,fstrStrBin},
				/**Product details**/
				{ fMmnSubProductGrouping},
				{ fproductInfoComposite,fbtnAdd},
				{ fsubProductTableMmn},
				/**Classification**/
				{fMmnclassificationDetailGrouping},
				{ foblMmnCategory,foblMmnType},
				/**MMN details**/
				{ fMmnRefereceDetailGrouping },
				{ fibMmnMinId,fdbMmnMinDate,fibMmnWoId,fdbMmnWoDate},
				{ forderId},
				{ ftaMmnDescription},
				/**Attachment**/
				{fMmnAttachmentGrouping},
				{ fupload}
				
				
		};
		this.fields = formfield;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(MaterialMovementNote model) {
		if (tbMmnTitle.getValue() != null)
			model.setMmnTitle(tbMmnTitle.getValue());
		if (dbMmnDate.getValue() != null)
			model.setMmnDate(dbMmnDate.getValue());
		
		if (!ibMmnWoId.getValue().equals(""))
			model.setMmnWoId(Integer.parseInt(ibMmnWoId.getValue()));
		if (dbMmnWoDate.getValue() != null)
			model.setMmnWoDate(dbMmnWoDate.getValue());
		
		if (!ibMmnMinId.getValue().equals(""))
			model.setMmnMinId(Integer.parseInt(ibMmnMinId.getValue()));
		if (dbMmnMinDate.getValue() != null)
			model.setMmnMinDate(dbMmnMinDate.getValue());
		
//		if (ibMmnMrnId.getValue() != null)
//			model.setMmnMrnId(ibMmnMrnId.getValue());
//		if (dbMmnMrnDate.getValue() != null)
//			model.setMmnMrnDate(dbMmnMrnDate.getValue());
		
		if (oblMmnCategory.getValue() != null)
			model.setMmnCategory(oblMmnCategory.getValue());
		if (oblMmnType.getValue() != null)
			model.setMmnType(oblMmnType.getValue(oblMmnType.getSelectedIndex()));
		
		if (oblMmnTransactionType.getValue() != null)
			model.setMmnTransactionType(oblMmnTransactionType.getValue());
		
		if (oblMmnBranch.getValue() != null)
			model.setBranch(oblMmnBranch.getValue());
		if (oblMmnRequestedBy.getValue() != null)
			model.setEmployee(oblMmnRequestedBy.getValue());
		if (oblMmnPersonResponsible.getValue() != null)
			model.setMmnPersonResposible(oblMmnPersonResponsible.getValue());
		if (oblMmnApproverName.getValue() != null)
			model.setApproverName(oblMmnApproverName.getValue());
		if(!tbserviceId.getValue().equals(""))
			model.setServiceId(Integer.parseInt(tbserviceId.getValue()));
		if (tbMmnStatus.getValue() != null)
			model.setStatus(tbMmnStatus.getValue());
//		if (tbMmnCommentOnStatusChange.getValue() != null)
//			model.setMmnCommentOnStatusChange(tbMmnCommentOnStatusChange.getValue());
		
		if (taMmnDescription.getValue() != null)
			model.setMmnDescription(taMmnDescription.getValue());
		if (upload.getValue() != null)
			model.setUpload(upload.getValue());
		
		if(strWarehouse.getValue()!=null)
			model.setTransToWareHouse(strWarehouse.getValue());
		
		if(strStrLoc.getSelectedIndex()!=0)
			model.setTransToStorLoc(strStrLoc.getItemText(strStrLoc.getSelectedIndex()));
		
		if(strStrBin.getSelectedIndex()!=0)
			model.setTransToStorBin(strStrBin.getItemText(strStrBin.getSelectedIndex()));
		
		if(transDirection.getValue()!=null){
			model.setTransDirection(transDirection.getValue());
		}
		
		if(orderId.getValue()!=null){
			model.setOrderID(orderId.getValue());
		}
		
		if (subProductTableMmn.getValue() != null)
			model.setSubProductTableMmn(subProductTableMmn.getValue());
		
		mmnobj=model;
		
		presenter.setModel(model);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public void updateView(MaterialMovementNote view) {
		
		mmnobj=view;
		
		ibMmnId.setValue(view.getCount()+"");
		if (view.getMmnTitle() != null)
			tbMmnTitle.setValue(view.getMmnTitle());
		if (view.getMmnDate() != null)
			dbMmnDate.setValue(view.getMmnDate());
		
		if (view.getMmnMinId() != 0){
			ibMmnMinId.setValue(view.getMmnMinId()+"");
			checkMinQuantity(view.getMmnMinId());
		}
		if (view.getMmnMinDate() != null)
			dbMmnMinDate.setValue(view.getMmnMinDate());
		
//		if (view.getMmnMrnId() != 0)
//			ibMmnMrnId.setValue(view.getMmnMrnId());
//		if (view.getMmnMrnDate() != null)
//			dbMmnMrnDate.setValue(view.getMmnMrnDate());
		
		if (view.getMmnWoId() != 0)
			ibMmnWoId.setValue(view.getMmnWoId()+"");
		if (view.getMmnWoDate() != null)
			dbMmnWoDate.setValue(view.getMmnWoDate());
		
		if (view.getMmnCategory() != null)
			oblMmnCategory.setValue(view.getMmnCategory());
		if (view.getMmnType()!= null)
			oblMmnType.setValue(view.getMmnType());
		
		if (view.getMmnTransactionType() != null)
			oblMmnTransactionType.setValue(view.getMmnTransactionType());
		if(view.getServiceId()!=0)
			tbserviceId.setValue(view.getServiceId()+"");
		if (view.getBranch() != null)
			oblMmnBranch.setValue(view.getBranch());
		if (view.getEmployee() != null)
			oblMmnRequestedBy.setValue(view.getEmployee());
		if (view.getMmnPersonResposible() != null)
			oblMmnPersonResponsible.setValue(view.getMmnPersonResposible());
		if (view.getApproverName()!= null)
			oblMmnApproverName.setValue(view.getApproverName());
		
		if (view.getStatus() != null)
			tbMmnStatus.setValue(view.getStatus());
		if (view.getRemark()!= null)
			tbMmnCommentOnStatusChange.setValue(view.getRemark());
		
		if (view.getMmnDescription() != null)
			taMmnDescription.setValue(view.getMmnDescription());
		if (view.getUpload() != null)
			upload.setValue(view.getUpload());
		
		if(view.getTransToWareHouse()!=null)
			strWarehouse.setValue(view.getTransToWareHouse());
		
		if(view.getOrderID()!=null){
			orderId.setValue(view.getOrderID());
		}
		
		if(view.getTransToStorLoc()!=null)
		{
			/**
			 * @author Anil @since 26-03-2021
			 * Storage location is cleared and added new item
			 */
			strStrLoc.clear();
			strStrLoc.addItem("--SELECT--");
			strStrLoc.addItem(view.getTransToStorLoc());
			strStrLoc.setSelectedIndex(1);
		}
		
		if(view.getTransToStorBin()!=null)
		{
			/**
			 * @author Anil @since 26-03-2021
			 * Storage bin is cleared and added new item
			 */
			strStrBin.clear();
			strStrBin.addItem("--SELECT--");
			strStrBin.addItem(view.getTransToStorBin());
			strStrBin.setSelectedIndex(1);
		}
		
		if(view.getTransDirection()!=null){
			transDirection.setValue(view.getTransDirection());
		}
		
		
		if (view.getSubProductTableMmn() != null)
			subProductTableMmn.setValue(view.getSubProductTableMmn());
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */
	}
	
	

	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")
						|| text.contains("Search")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")|| text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Edit") || text.contains("Discard")|| text.contains("Search")|| text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.MATERIALMOVEMENTNOTE,LoginPresenter.currentModule.trim());
	}
	
	public void toggleProcessLevelMenu() {
		MaterialMovementNote entity = (MaterialMovementNote) presenter.getModel();
		String status = entity.getStatus();

		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();
			if ((status.equals(MaterialMovementNote.APPROVED))) {
				
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);
				if (text.equals(ManageApprovals.SUBMIT)){
					label.setVisible(false);
				}
				
//				if (text.equals(AppConstants.CANCELMMN))
//					label.setVisible(true);

				/**
				 * rohan added this code for NBHC Changes ADMIN role should  
				 */
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
					if (text.equals(AppConstants.CANCELMMN))
						label.setVisible(true);
				}
				else
				{
					if (text.equals(AppConstants.CANCELMMN))
						label.setVisible(false);
				}
				
				/**
				 * Date : 17-03-2017 By ANIL
				 * NBHC CCPM
				 */
				if (text.equals("Gate Pass")){
					label.setVisible(true);
				}
			
			}
			
			if ((status.equals(MaterialMovementNote.CREATED))) {
				
				/**
				 * rohan added this code for NBHC Changes ADMIN role should cancel document 
				 */
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
					if (text.equals(AppConstants.CANCELMMN))
						label.setVisible(true);
				}
				else
				{
					if (text.equals(AppConstants.CANCELMMN))
						label.setVisible(false);
				}
			}
			if ((status.equals(MaterialMovementNote.REQUESTED))) {
				
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(true);
				if (text.equals(AppConstants.CANCELMMN))
					label.setVisible(false);
				if (text.equals(ManageApprovals.SUBMIT)){
					label.setVisible(false);
				}
			}
			
			if ((status.equals(MaterialMovementNote.REJECTED) || status.equals(MaterialMovementNote.CANCELLED))) {
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);
				
				if (text.equals(AppConstants.CANCELMMN))
					label.setVisible(false);
			}
		}

	}
	
	public void setAppHeaderBarAsPerStatus() {
		MaterialMovementNote entity = (MaterialMovementNote) presenter.getModel();
		String status = entity.getStatus();

		if (status.equals(MaterialMovementNote.CANCELLED)) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		
		if (status.equals(MaterialMovementNote.APPROVED)) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")||text.contains("Email")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
	}
	
	public void setMenuAsPerStatus() {
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
	}
	
	@Override
	public void setCount(int count) {
		ibMmnId.setValue(count+"");
	}

	@Override
	public void clear() {
		super.clear();
		tbMmnStatus.setText(MaterialMovementNote.CREATED);
		subProductTableMmn.clear();

	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbMmnStatus.setEnabled(false);
		tbMmnCommentOnStatusChange.setEnabled(false);
		ibMmnId.setEnabled(false);
		subProductTableMmn.setEnable(state);
		dbMmnWoDate.setEnabled(false);
//		dbMmnMrnDate.setEnabled(false);
		dbMmnMinDate.setEnabled(false);
		tbserviceId.setEnabled(false);

	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		setMenuAsPerStatus();
		
		SuperModel model=new MaterialIssueNote();
		model=mmnobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.INVENTORYMODULE,AppConstants.MATERIALMOVEMENTNOTE, mmnobj.getCount(), null,null,null, false, model, null);
	
		String mainScreenLabel="MATERIAL MOVEMENT NOTE";
		if(mmnobj!=null&&mmnobj.getCount()!=0){
			mainScreenLabel=mmnobj.getCount()+" "+"/"+" "+mmnobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(mmnobj.getCreationDate());
		}
		fMmnMmnInfoGrouping.getHeaderLabel().setText(mainScreenLabel);
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		
		if(tbMmnStatus.getValue().equals(MaterialMovementNote.REJECTED)){
			tbMmnStatus.setValue(MaterialMovementNote.CREATED);
		}
		if(!ibMmnMinId.getValue().equals("")){
			productInfoComposite.setEnabled(false);
		}
		if(this.getOblMmnTransactionType().getValue().equals("TransferIN")){
			setFieldsEnable();
		}
		setMenuAsPerStatus();
		
		this.processLevelBar.setVisibleFalse(false);
		
		String mainScreenLabel="MATERIAL MOVEMENT NOTE";
		if(mmnobj!=null&&mmnobj.getCount()!=0){
			mainScreenLabel=mmnobj.getCount()+" "+"/"+" "+mmnobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(mmnobj.getCreationDate());
		}
		fMmnMmnInfoGrouping.getHeaderLabel().setText(mainScreenLabel);
	}

	@Override
	public TextBox getstatustextbox() {
		return tbMmnStatus;
	}

	@Override
	public boolean validate() {
		boolean superValidate = super.validate();
		if (superValidate == false)
			return false;
		if(subProductTableMmn.getDataprovider().getList().size()== 0){
			showDialogMessage("Please add material details.");
			return false;
		}
		if(validateWarehouse()==false){
			showDialogMessage("Please Select Warehouse Details.");
			return false;
		}
		if(validateRequiredQuantity()==false){
			return false;
		}
		if(!ibMmnMinId.getValue().equals("")){
			if(validateProdQtyAgainstMin()==false){
				return false;
			}
		}
		
		/**
		 * @author Vijay Date 02-04-2021 Updated validation with time as 00
		 */
		Date todayDate = new Date();
		CalendarUtil.resetTime(todayDate);
		Date mmnDate = dbMmnDate.getValue();
		CalendarUtil.resetTime(mmnDate);
		if(mmnDate.after(todayDate)){
			showDialogMessage("MMN Date should not be future Date!");
			return false;
		}
		
		if(this.getOblMmnTransactionType().getValue().equals("TransferOUT"))
		{
		if(validateForSelectedWareHouse()==false){
			showDialogMessage("You can't transfer to same warehouse");
			return false;
		}
		}
		else{
			return true;
		}
		
		
		
		return true;
	}
	
	public boolean validateProduct() {
		String name = productInfoComposite.getProdNameValue();
		System.out.println("Product name is :"+name);
		List<MaterialProduct> list = subProductTableMmn.getDataprovider().getList();
		for (MaterialProduct temp : list) {
			if (name.equals(temp.getMaterialProductName())) {
				this.showDialogMessage("Can not add Duplicate product");
				return false;
			}
		}
		return true;
	}
	
	public boolean validateRequiredQuantity() {
		List<MaterialProduct> list = subProductTableMmn.getDataprovider().getList();
		for (MaterialProduct temp : list) {
			if (temp.getMaterialProductRequiredQuantity()==0) {
				this.showDialogMessage("Quantity Can Not Be Zero");
				return false;
			}
			
			MaterialMovementType mmt=oblMmnTransactionType.getSelectedItem();
			if(mmt.getMmtType().equals("OUT")||mmt.getMmtType().equals("TRANSFEROUT")){
				if(temp.getMaterialProductAvailableQuantity()<temp.getMaterialProductRequiredQuantity()){
					this.showDialogMessage("Quantity Should Not Be Greater Than Available Quantity");
					return false;
				}
				/** Date 17-06-2019 by Vijay - value should  not allow ***/
				if(temp.getMaterialProductRequiredQuantity()<=0){
					showDialogMessage("Quantity Should Not Be in Negative");
					return false;

				}
				
				/*** Date 12-02-2020 Deepak Salve added this code for Normal flow ***/
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialMovementNote", "EnableLockSealTransferFromSameWarehouse")){
					/*** Date 03-07-2019 by Vijay TarnsferTo and Transfer From should not be same Validation ****/
				if(strWarehouse.getSelectedIndex()!=0){
					if(temp.getMaterialProductWarehouse().trim().equals(strWarehouse.getValue().trim())){
						showDialogMessage("TRANSFEROUT transaction sholud not applicable for same TransferFrom and TransferTo warehouses");
						return false;
					}
				}
			  }
				/***End***/
			}
			
			/**
			 * Date 18-05-2019 by Vijay for NBHC IM 
			 * Des :- for Lock Seal Start and End Serial Number Mandatory
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialMovementNote", "EnableLockSealStartEndSerialMandatory")){ 
				if(temp.getMaterialProductCode().trim().equals("P100000065") && temp.getProSerialNoDetails().size()==0){
					showDialogMessage("Start Serial And End Serial Number Mandatory!");
					return false;
				}
				
			}
			/**
			 * ends here
			 */
			/**
			 * Date 18-06-2019  by Vijay 
			 * Des :- for NBHC IM MMN allow only to Cluster To cluster and Warehouse To cluster
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialMovementNote", "ValidateMMN")){
				boolean Flag = true;
				if(this.strWarehouse.getSelectedIndex()!=0){
					WareHouse transferToWarehouse = this.strWarehouse.getSelectedItem();
					System.out.println("Global Size =="+LoginPresenter.allWarehouselist.size());
					for(WareHouse fromwarehouseinfo : LoginPresenter.allWarehouselist){
						if(fromwarehouseinfo.getBusinessUnitName().trim().equals(temp.getMaterialProductWarehouse().trim())){
							if(fromwarehouseinfo.getParentWarehouse().equals(transferToWarehouse.getBusinessUnitName().trim()) 
									|| (fromwarehouseinfo.getParentWarehouse().trim().equals("NBHC HO") &&
											transferToWarehouse.getParentWarehouse().trim().equals("NBHC HO"))
									|| (fromwarehouseinfo.getParentWarehouse().trim().equals("NBHC HO") &&
											transferToWarehouse.getParentWarehouse().trim().equals(fromwarehouseinfo.getBusinessUnitName().trim()))){
								Flag = false;
								break;
							}
						}
					}
					if(Flag){
						showDialogMessage("Material Transfer Only Applicable To Cluster To Warehouse & Warehouse To Cluster & Cluster To Cluster");
						return false;
					}
				}
			}
		
			
			
		}
		return true;
	}
	
	// ************************************ Validate Warehouse *********************************************
			public boolean validateWarehouse(){
				int ctr=0;
				for(int i=0;i<subProductTableMmn.getDataprovider().getList().size();i++){
					if(!subProductTableMmn.getDataprovider().getList().get(i).getMaterialProductWarehouse().equals("")
							&&!subProductTableMmn.getDataprovider().getList().get(i).getMaterialProductStorageBin().equals("")){
						ctr++;
					}
				}
				if(ctr==subProductTableMmn.getDataprovider().getList().size()){
					return true;
				}else{
					return false;
				}
			}
			
			
			// **************** Validate Product Qty Against ****************************
			
			public boolean validateProdQtyAgainstMin(){
				System.out.println("EOne");
				boolean flage=true;
				for(int i=0;i<subProductTableMmn.getDataprovider().getList().size();i++){
							
					if(!compList.isEmpty()){
						for(int j=0;j<compList.size();j++){
							if(subProductTableMmn.getDataprovider().getList().get(i).getMaterialProductId()== compList.get(j).getProdId()){
								if(subProductTableMmn.getDataprovider().getList().get(i).getMaterialProductRequiredQuantity()>compList.get(j).getProdQty()){
									double exceedQty=subProductTableMmn.getDataprovider().getList().get(i).getMaterialProductRequiredQuantity()-compList.get(j).getProdQty();
									flage=false;
									showDialogMessage("Material Id "+subProductTableMmn.getDataprovider().getList().get(i).getMaterialProductId()+" exceeds quantity in MIN by "+exceedQty+".");
									break;
								}
							}
						}
					}
				}
				return flage;
			}
					
			public void validateMmnQty(ArrayList<ProductQty> minList,ArrayList<ProductQty> mmnList){
				compList=new ArrayList<ProductQty>();
				double qty=0;
				if(!mmnList.isEmpty()){
					for(int i=0;i<minList.size();i++){
						for(int j=0;j<mmnList.size();j++){
							if(minList.get(i).getProdId()==mmnList.get(j).getProdId()){
								qty=minList.get(i).getProdQty()-mmnList.get(j).getProdQty();
									
								ProductQty pq=new ProductQty();
								pq.setProdId(minList.get(i).getProdId());
								pq.setProdQty(qty);
								
								compList.add(pq);
							}
						}
					}
				}
				else{
					compList.addAll(minList);
				}
						
			}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource().equals(btnAdd)) {
				
			if(this.getOblMmnTransactionType().getValue().equals("TransferOUT")){
				if((this.getStrWarehouse().getSelectedIndex()!=0) && (this.getStrStrLoc().getSelectedIndex()!=0) 
						&&(this.getStrStrBin().getSelectedIndex()!=0)){
					
					validationForProductCheck();
				}
				else{
					showDialogMessage("Select Warehouse Details for Transfer");
				}
			}
			else{
				setSubProductTable();
			}
		}
	}
	
	public void setSubProductTable(){
		if(validateProduct()){
		if (productInfoComposite.getValue() != null) {
			MaterialProduct prod = new MaterialProduct();
			prod.setMaterialProductId(Integer.parseInt(productInfoComposite.getProdID().getValue()));
			prod.setMaterialProductCode(productInfoComposite.getProdCode().getValue());
			prod.setMaterialProductCategory(productInfoComposite.getProdCategory().getValue());
			prod.setMaterialProductName(productInfoComposite.getProdName().getValue());
			prod.setMaterialProductAvailableQuantity(0);
			prod.setMaterialProductUOM(productInfoComposite.getUnitOfmeasuerment().getValue());
			prod.setMaterialProductRequiredQuantity(0);
			prod.setMaterialProductRemarks("");
			prod.setMaterialProductWarehouse("");
			prod.setMaterialProductStorageLocation("");
			prod.setMaterialProductStorageBin("");
			/*** Date 03-07-2019 by Vijay set GRN Qty 1 by default ****/
			prod.setMaterialProductRequiredQuantity(1);
			subProductTableMmn.getDataprovider().getList().add(prod);
			productInfoComposite.clear();
		}}
	}
	
	
	private void setFieldsEnable() {
		
		this.getOblMmnTransactionType().setEnabled(false);
		this.getStrStrBin().setEnabled(false);
		this.getStrStrLoc().setEnabled(false);
		this.getStrWarehouse().setEnabled(false);
		this.getSubProductTableMmn().setEnable(false);
		this.getBtnAdd().setEnabled(false);
}


		public boolean validateForSelectedWareHouse(){
			
			List<MaterialProduct> formProdList=subProductTableMmn.getDataprovider().getList();
			int ctr=0;
			for(int i=0;i<formProdList.size();i++)
			{
				if(formProdList.get(i).getMaterialProductWarehouse().trim().equals(strWarehouse.getValue().trim())
				   &&(formProdList.get(i).getMaterialProductStorageLocation().trim().equals(strStrLoc.getItemText(strStrLoc.getSelectedIndex()).trim()))
				   &&(formProdList.get(i).getMaterialProductStorageBin().trim().equals(strStrBin.getItemText(strStrBin.getSelectedIndex()).trim())))
				{
					ctr=ctr+1;
				}
				else{
				}
			}
			if(ctr>0){
				return false;
				
			}
			else{
				return true;
			}
		}
		
		public  void validationForProductCheck(){
			
			final String code=this.getProductInfoComposite().getProdCodeValue();
			
				MyQuerry querry = new MyQuerry();
			  	Company c = new Company();
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
			  	filter = new Filter();
			  	filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("details.warehousename");
				filter.setStringValue(this.getStrWarehouse().getValue());
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("details.storagelocation");
				filter.setStringValue(this.getStrStrLoc().getItemText(this.getStrStrLoc().getSelectedIndex()));
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("details.storagebin");
				filter.setStringValue(this.getStrStrBin().getItemText(this.getStrStrBin().getSelectedIndex()));
				filtervec.add(filter);
				
				/**
				 * @author Anil , Date : 28-04-2020
				 * Load only stock master for selected from product
				 */
				if(productInfoComposite.getValue()!=null) {
					filter = new Filter();
					filter.setQuerryString("details.prodid");
					filter.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
					filtervec.add(filter);
				}
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new ProductInventoryView());
				
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("RS"+result.size());
						int cnt=0;
						ArrayList<String> productsList = new ArrayList<String>();
						for(SuperModel model:result){
							
								ProductInventoryView sup=(ProductInventoryView)model;
								productsList.add(sup.getProductCode());
//								productsList.add(sup.getProdID()+"");
						}
						
						
						for(int i=0;i<productsList.size();i++){
							
							if(code.equals(productsList.get(i))){
								
								cnt=cnt+1;
							}
							else{
								
							System.out.println("count is zero");
							}
						}
						
						if(cnt > 0){
							
							setSubProductTable();
						}
						else{
							showDialogMessage("This product is not present in the selected transfer-to-warehouse");
						}
						
					}
				});
		
		}
	
	/************************************ Initializing WareHouse ObjectList ******************************************/
	
	protected void initalizTransactionName() {
//		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new MaterialMovementType());
//		oblMmnTransactionType.MakeLive(querry);
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		filter=new Filter();
		filter.setQuerryString("mmtStatus");
		filter.setBooleanvalue(true);
		temp.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		temp.add(filter);
		
		MyQuerry querry = new MyQuerry(temp, new MaterialMovementType());
		oblMmnTransactionType.MakeLive(querry);
	}
	
	// *********************************** Validation method for MIN QTY against MRN QTY **********************************
	
		public void checkMinQuantity(final int mrnId){
			System.out.println("Inside search .............................");
			
			MyQuerry querry = new MyQuerry();
			Company c=new Company();
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(mrnId);
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new MaterialIssueNote());
			
			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					minList=new ArrayList<ProductQty>();
					for (SuperModel model : result) {
						if(result.size()!=0){
							MaterialIssueNote entity = (MaterialIssueNote) model;
							for(int i=0;i<entity.getSubProductTablemin().size();i++){
								
								ProductQty pq=new ProductQty();
								pq.setProdId(entity.getSubProductTablemin().get(i).getMaterialProductId());
								pq.setProdQty(entity.getSubProductTablemin().get(i).getMaterialProductRequiredQuantity());
								
								minList.add(pq);
							}
						}
					}
					
					MyQuerry querey1 = new MyQuerry();
					Company c=new Company();
					
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter filter = null;
					
					filter = new Filter();
					filter.setQuerryString("companyId");
					filter.setLongValue(c.getCompanyId());
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("mmnMinId");
					filter.setIntValue(mrnId);
					filtervec.add(filter);
					
					querey1.setFilters(filtervec);
					querey1.setQuerryObject(new MaterialMovementNote());
					
					async.getSearchResult(querey1,new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onFailure(Throwable caught) {
							System.out.println("On Failure !!!");
						}
			
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							int mmnId=0;
							if(!ibMmnId.getValue().equals("")){
								mmnId=Integer.parseInt(ibMmnId.getValue());
							}
							mmnList=new ArrayList<ProductQty>();
							double qty=0;
							boolean flage=false;
							for (SuperModel model1 : result) {
								if(result.size()!=0){
									MaterialMovementNote entity = (MaterialMovementNote) model1;
									if(entity.getCount()!=mmnId){
										
									for(int i=0;i<entity.getSubProductTableMmn().size();i++){
										if(!mmnList.isEmpty()){
											for(int j=0;j<mmnList.size();j++){
												if(mmnList.get(j).getProdId()==entity.getSubProductTableMmn().get(i).getMaterialProductId()){
													qty=mmnList.get(j).getProdQty();
													qty=qty+entity.getSubProductTableMmn().get(i).getMaterialProductRequiredQuantity();
													mmnList.get(j).setProdQty(qty);
													flage=true;
												}
											}
											if(flage==false){
												ProductQty pq=new ProductQty();
												pq.setProdId(entity.getSubProductTableMmn().get(i).getMaterialProductId());
												pq.setProdQty(entity.getSubProductTableMmn().get(i).getMaterialProductRequiredQuantity());
											
												mmnList.add(pq);
											}
										}
										else {
											ProductQty pq=new ProductQty();
											pq.setProdId(entity.getSubProductTableMmn().get(i).getMaterialProductId());
											pq.setProdQty(entity.getSubProductTableMmn().get(i).getMaterialProductRequiredQuantity());
										
											mmnList.add(pq);
										}
									  }
									}
								}
								else{
									System.out.println("No Result Found !!!!");
								}
							}
							
							for(int i=0;i<mmnList.size();i++){
								System.out.println("------------------------ Result ----------------------------  ");
								System.out.println("MMN List prod ID : "+mmnList.get(i).getProdId());
								System.out.println("MMN List QTY : "+mmnList.get(i).getProdQty());
							}
							validateMmnQty(minList,mmnList);
						}
					});
				}
			});
		}
		
		
		
/*******************************************Type Drop Down Logic**************************************/
		
		@Override
		public void onChange(ChangeEvent event) {
			if(event.getSource().equals(oblMmnCategory))
			{
				if(oblMmnCategory.getSelectedIndex()!=0){
					ConfigCategory cat=oblMmnCategory.getSelectedItem();
					if(cat!=null){
						AppUtility.makeLiveTypeDropDown(oblMmnType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
					}
				}
			}
			
		}
	
	
	/******************************************************************************************************************/

//	public IntegerBox getIbMmnMrnId() {
//		return ibMmnMrnId;
//	}
//
//	public void setIbMmnMrnId(IntegerBox ibMmnMrnId) {
//		this.ibMmnMrnId = ibMmnMrnId;
//	}

		public TextBox getIbMmnMinId() {
			return ibMmnMinId;
		}

		public void setIbMmnMinId(TextBox ibMmnMinId) {
			this.ibMmnMinId = ibMmnMinId;
		}

//	public DateBox getDbMmnMrnDate() {
//		return dbMmnMrnDate;
//	}
//
//	public void setDbMmnMrnDate(DateBox dbMmnMrnDate) {
//		this.dbMmnMrnDate = dbMmnMrnDate;
//	}

	public DateBox getDbMmnMinDate() {
		return dbMmnMinDate;
	}

	

	public void setDbMmnMinDate(DateBox dbMmnMinDate) {
		this.dbMmnMinDate = dbMmnMinDate;
	}

	public TextBox getIbMmnWoId() {
		return ibMmnWoId;
	}

	public void setIbMmnWoId(TextBox ibMmnWoId) {
		this.ibMmnWoId = ibMmnWoId;
	}

	public DateBox getDbMmnWoDate() {
		return dbMmnWoDate;
	}

	public void setDbMmnWoDate(DateBox dbMmnWoDate) {
		this.dbMmnWoDate = dbMmnWoDate;
	}

	public TextBox getIbMmnId() {
		return ibMmnId;
	}

	public void setIbMmnId(TextBox ibMmnId) {
		this.ibMmnId = ibMmnId;
	}

	public DateBox getDbMmnDate() {
		return dbMmnDate;
	}

	public void setDbMmnDate(DateBox dbMmnDate) {
		this.dbMmnDate = dbMmnDate;
	}

	public TextBox getTbMmnTitle() {
		return tbMmnTitle;
	}

	public void setTbMmnTitle(TextBox tbMmnTitle) {
		this.tbMmnTitle = tbMmnTitle;
	}

	public ObjectListBox<ConfigCategory> getOblMmnCategory() {
		return oblMmnCategory;
	}

	public void setOblMmnCategory(ObjectListBox<ConfigCategory> oblMmnCategory) {
		this.oblMmnCategory = oblMmnCategory;
	}

	public ObjectListBox<Type> getOblMmnType() {
		return oblMmnType;
	}

	public void setOblMmnType(ObjectListBox<Type> oblMmnType) {
		this.oblMmnType = oblMmnType;
	}

	public ObjectListBox<MaterialMovementType> getOblMmnTransactionType() {
		return oblMmnTransactionType;
	}

	public void setOblMmnTransactionType(ObjectListBox<MaterialMovementType> oblMmnTransactionType) {
		this.oblMmnTransactionType = oblMmnTransactionType;
	}

	public ObjectListBox<Branch> getOblMmnBranch() {
		return oblMmnBranch;
	}

	public void setOblMmnBranch(ObjectListBox<Branch> oblMmnBranch) {
		this.oblMmnBranch = oblMmnBranch;
	}

	public ObjectListBox<Employee> getOblMmnApproverName() {
		return oblMmnApproverName;
	}

	public void setOblMmnApproverName(ObjectListBox<Employee> oblMmnApproverName) {
		this.oblMmnApproverName = oblMmnApproverName;
	}

	public ObjectListBox<Employee> getOblMmnRequestedBy() {
		return oblMmnRequestedBy;
	}

	public void setOblMmnRequestedBy(ObjectListBox<Employee> oblMmnRequestedBy) {
		this.oblMmnRequestedBy = oblMmnRequestedBy;
	}

	public ObjectListBox<Employee> getOblMmnPersonResponsible() {
		return oblMmnPersonResponsible;
	}

	public void setOblMmnPersonResponsible(
			ObjectListBox<Employee> oblMmnPersonResponsible) {
		this.oblMmnPersonResponsible = oblMmnPersonResponsible;
	}

	public TextBox getTbMmnStatus() {
		return tbMmnStatus;
	}

	public void setTbMmnStatus(TextBox tbMmnStatus) {
		this.tbMmnStatus = tbMmnStatus;
	}

	public TextBox getTbMmnCommentOnStatusChange() {
		return tbMmnCommentOnStatusChange;
	}

	public void setTbMmnCommentOnStatusChange(TextBox tbMmnCommentOnStatusChange) {
		this.tbMmnCommentOnStatusChange = tbMmnCommentOnStatusChange;
	}

	public UploadComposite getUpload() {
		return upload;
	}

	public void setUpload(UploadComposite upload) {
		this.upload = upload;
	}

	public TextArea getTaMmnDescription() {
		return taMmnDescription;
	}

	public void setTaMmnDescription(TextArea taMmnDescription) {
		this.taMmnDescription = taMmnDescription;
	}

	public ProductInfoComposite getProductInfoComposite() {
		return productInfoComposite;
	}

	public void setProductInfoComposite(ProductInfoComposite productInfoComposite) {
		this.productInfoComposite = productInfoComposite;
	}

	public Button getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(Button btnAdd) {
		this.btnAdd = btnAdd;
	}

	
	public ObjectListBox<WareHouse> getStrWarehouse() {
		return strWarehouse;
	}

	public void setStrWarehouse(ObjectListBox<WareHouse> strWarehouse) {
		this.strWarehouse = strWarehouse;
	}

	public ListBox getStrStrLoc() {
		return strStrLoc;
	}

	public void setStrStrLoc(ListBox strStrLoc) {
		this.strStrLoc = strStrLoc;
	}

	public ListBox getStrStrBin() {
		return strStrBin;
	}

	public void setStrStrBin(ListBox strStrBin) {
		this.strStrBin = strStrBin;
	}

	public TextBox getTransDirection() {
		return transDirection;
	}

	public void setTransDirection(TextBox transDirection) {
		this.transDirection = transDirection;
	}

	public TextBox getOrderId() {
		return orderId;
	}

	public void setOrderId(TextBox orderId) {
		this.orderId = orderId;
	}

	public TextBox getTbserviceId() {
		return tbserviceId;
	}

	public void setTbserviceId(TextBox tbserviceId) {
		this.tbserviceId = tbserviceId;
	}

	public SubProductTableMMN getSubProductTableMmn() {
		return subProductTableMmn;
	}

	public void setSubProductTableMmn(SubProductTableMMN subProductTableMmn) {
		this.subProductTableMmn = subProductTableMmn;
	}
	
	
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		subProductTableMmn.getTable().redraw();
	}
	
	
	
}
