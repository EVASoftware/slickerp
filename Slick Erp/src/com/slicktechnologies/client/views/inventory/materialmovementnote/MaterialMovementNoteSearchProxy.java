package com.slicktechnologies.client.views.inventory.materialmovementnote;

import java.util.Vector;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.materialreuestnote.SubProductTableMrn;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementType;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class MaterialMovementNoteSearchProxy extends SearchPopUpScreen<MaterialMovementNote> {

	IntegerBox ibMmnMrnId,ibMmnMinId;
	
	IntegerBox ibMmnWoId;
	TextBox ibOrderId;
	
	IntegerBox ibMmnId;
	public DateComparator dateComparator;
	
	TextBox tbMmnTitle;
	
	ObjectListBox<ConfigCategory> oblMmnCategory;
	ObjectListBox<Type> oblMmnType;
	
	ObjectListBox<MaterialMovementType> oblMmnTransactionType;
	
	ObjectListBox<Branch> oblMmnBranch;
	ObjectListBox<Employee> oblMmnApproverName;
	ObjectListBox<Employee> oblMmnRequestedBy;
	ObjectListBox<Employee> oblMmnPersonResponsible;
//	ObjectListBox<WareHouse> oblMmnWarehouse;
	
	ListBox tbMmnStatus;
	ProductInfoComposite productInfoComposite;
	
	public MaterialMovementNoteSearchProxy() {
		super();
		createGui();
	}	
	
	private void initializeWidget(){
		ibMmnMrnId=new IntegerBox();
		ibMmnMinId=new IntegerBox();
		
		ibMmnWoId=new IntegerBox();
		
		ibMmnId=new IntegerBox();
		dateComparator = new DateComparator("mmnDate", new MaterialMovementNote());
		tbMmnTitle=new TextBox();
		
		oblMmnTransactionType=new ObjectListBox<MaterialMovementType>();
		initalizTransactionName();
//		oblMmnDirection=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(oblMmnDirection, Screen.MMNDIRECTION);
		
		oblMmnCategory= new ObjectListBox<ConfigCategory>();
		oblMmnType = new ObjectListBox<Type>();
		CategoryTypeFactory.initiateOneManyFunctionality(oblMmnCategory,oblMmnType);
		AppUtility.makeTypeListBoxLive(oblMmnType,Screen.MMNCATEGORY);	
		oblMmnBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(oblMmnBranch);
		
		oblMmnApproverName = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(oblMmnApproverName);
		
		oblMmnPersonResponsible = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblMmnPersonResponsible);
		oblMmnPersonResponsible.makeEmployeeLive(AppConstants.INVENTORYMODULE, "Material Movement Note", "Person Responsible");
		
		oblMmnRequestedBy = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblMmnRequestedBy);
		oblMmnRequestedBy.makeEmployeeLive(AppConstants.INVENTORYMODULE, "Material Movement Note", "Requested By");
		
		ibOrderId=new TextBox();
		
//		oblMmnWarehouse = new ObjectListBox<WareHouse>();
//		initalizWarehouseName();
		
		tbMmnStatus=new ListBox();
		AppUtility.setStatusListBox(tbMmnStatus,MaterialMovementNote.getStatusList());
		
		productInfoComposite = AppUtility.initiateMrnProductComposite(new SuperProduct());
		
	}
	
	
	@Override
	public void createScreen() {
		initializeWidget();
		
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("MIN ID", ibMmnMinId);
		FormField fibMmnMinId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MRN ID", ibMmnMrnId);
		FormField fibMmnMrnId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Work Order Nr.", ibMmnWoId);
		FormField fibMmnWoId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MMN ID", ibMmnId);
		FormField fibMmnId= fbuilder.setMandatory(false).setMandatoryMsg("MMN ID is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MMN Title", tbMmnTitle);
		FormField ftbMmnTitle = fbuilder.setMandatory(true).setMandatoryMsg("MMN Title is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MMN From Date", dateComparator.getFromDate());
		FormField fdbMinFrmDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MMN To Date", dateComparator.getToDate());
		FormField fdbMinToDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MMN Category", oblMmnCategory);
		FormField foblMmnCategory = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MMN Type", oblMmnType);
		FormField foblMmnType = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Order ID", ibOrderId);
		FormField fibOrderId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Transaction Type", oblMmnTransactionType);
		FormField foblMmnTransactionType = fbuilder.setMandatory(true).setMandatoryMsg("Transaction Type is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Branch", oblMmnBranch);
		FormField foblMmnBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Approver", oblMmnApproverName);
		FormField foblMmnApproverName = fbuilder.setMandatory(true).setMandatoryMsg("Approver is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Requested By", oblMmnRequestedBy);
		FormField foblMmnRequestedBy = fbuilder.setMandatory(true).setMandatoryMsg("Requested By is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Person Responsible", oblMmnPersonResponsible);
		FormField foblMmnPersonResponsible = fbuilder.setMandatory(true).setMandatoryMsg("Issued By is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("WareHouse", oblMmnWarehouse);
//		FormField foblMmnWarehouse = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();
//		
		fbuilder = new FormFieldBuilder("Status", tbMmnStatus);
		FormField ftbMmnStatus = fbuilder.setMandatory(true).setMandatoryMsg("Status is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();
		
		
		FormField[][] formfield = { 
				{ fibMmnId,ftbMmnTitle,fdbMinFrmDate,fdbMinToDate},
				{foblMmnTransactionType,foblMmnCategory,foblMmnType,foblMmnBranch},
				{ foblMmnPersonResponsible,foblMmnApproverName,foblMmnRequestedBy,ftbMmnStatus},
//				{ foblMmnWarehouse},
				{ fibMmnMinId,fibMmnMrnId,fibMmnWoId,fibOrderId},
				{ fproductInfoComposite},
		};
		this.fields = formfield;
	}
	
	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		if (dateComparator.getValue() != null) {
			filtervec.addAll(dateComparator.getValue());
		}

		if (oblMmnBranch.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMmnBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if (oblMmnCategory.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMmnCategory.getValue().trim());
			temp.setQuerryString("mmnCategory");
			filtervec.add(temp);
		}
		if (oblMmnType.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMmnType.getValue().trim());
			temp.setQuerryString("mmnType");
			filtervec.add(temp);
		}
		if (oblMmnApproverName.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMmnApproverName.getValue().trim());
			temp.setQuerryString("approverName");
			filtervec.add(temp);
		}
		if (oblMmnTransactionType.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMmnTransactionType.getValue().trim());
			temp.setQuerryString("mmnTransactionType");
			filtervec.add(temp);
		}
		if (oblMmnPersonResponsible.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMmnPersonResponsible.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
//		if (oblMmnWarehouse.getSelectedIndex() != 0) {
//			temp = new Filter();
//			temp.setStringValue(oblMmnWarehouse.getValue().trim());
//			temp.setQuerryString("mmnWarehouse");
//			filtervec.add(temp);
//		}
		if (ibMmnId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibMmnId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		if (ibMmnMrnId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibMmnMrnId.getValue());
			temp.setQuerryString("mmnMrnId");
			filtervec.add(temp);
		}
		if (ibMmnMinId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibMmnMinId.getValue());
			temp.setQuerryString("mmnMinId");
			filtervec.add(temp);
		}
		
		if (!ibOrderId.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(ibOrderId.getValue());
			temp.setQuerryString("orderID");
			filtervec.add(temp);
		}
		
		if (ibMmnWoId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibMmnWoId.getValue());
			temp.setQuerryString("mmnWoId");
			filtervec.add(temp);
		}
		if (tbMmnStatus.getSelectedIndex()!=0) {
			temp = new Filter();
			temp.setStringValue(tbMmnStatus.getValue(tbMmnStatus.getSelectedIndex()));
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		if (!tbMmnTitle.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(tbMmnTitle.getValue());
			temp.setQuerryString("mmnTitle");
			filtervec.add(temp);
		}
		
		if (!productInfoComposite.getProdCode().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(productInfoComposite.getProdCode().getValue());
			temp.setQuerryString("subProductTableMmn.materialProductCode");
			filtervec.add(temp);
		}

		if (!productInfoComposite.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
			temp.setQuerryString("subProductTableMmn.materialProductId");
			filtervec.add(temp);
		}

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new MaterialMovementNote());
		return querry;
	}
	
/************************************ Initializing WareHouse ObjectList ******************************************/
	
//	protected void initalizWarehouseName() {
//		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new WareHouse());
//		oblMmnWarehouse.MakeLive(querry);
//	}
	
	protected void initalizTransactionName() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new MaterialMovementType());
		oblMmnTransactionType.MakeLive(querry);
	}

@Override
public boolean validate() {
	if(LoginPresenter.branchRestrictionFlag)
	{
	if(oblMmnBranch.getSelectedIndex()==0)
	{
		showDialogMessage("Select Branch");
		return false;
	}
	}
	return true;
}
}
