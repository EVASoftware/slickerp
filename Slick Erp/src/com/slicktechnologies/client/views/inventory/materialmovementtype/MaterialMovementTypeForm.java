package com.slicktechnologies.client.views.inventory.materialmovementtype;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.inventory.MaterialMovementType;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class MaterialMovementTypeForm extends FormTableScreen<MaterialMovementType>{

	TextBox tbMmtName;
	ObjectListBox<Config> oblMmtDirection;
	CheckBox cbMmtStatus;
	
	public MaterialMovementTypeForm  (SuperTable<MaterialMovementType> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();

	}
	private void initalizeWidget()
	{
		tbMmtName=new TextBox();
		oblMmtDirection=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblMmtDirection, Screen.MMNDIRECTION);
		
		cbMmtStatus=new CheckBox();
		cbMmtStatus.setValue(true);
	}
	
	@Override
	public void createScreen() {
		
		initalizeWidget();
		this.processlevelBarNames=new String[]{"New"};
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder("Name",tbMmtName);
		FormField ftbMmtName= fbuilder.setMandatory(true).setMandatoryMsg("Direction  is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Direction ",oblMmtDirection);
		FormField ftbMmtType= fbuilder.setMandatory(true).setMandatoryMsg("Direction  is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbMmtStatus);
		FormField fcbMmtStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		FormField[][] formfield = {   
				{ftbMmtName,ftbMmtType,fcbMmtStatus},
		};
		this.fields=formfield;	
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(MaterialMovementType model) {
		
		if(tbMmtName.getValue()!=null)
			model.setMmtName(tbMmtName.getValue());
		if(oblMmtDirection.getValue()!=null)
			model.setMmtType(oblMmtDirection.getValue());
		if(cbMmtStatus.getValue()!=null)
			model.setMmtStatus(cbMmtStatus.getValue());
		presenter.setModel(model);
	}
	@Override
	public void updateView(MaterialMovementType view) {
		if(view.getMmtName()!=null)
			tbMmtName.setValue(view.getMmtName());
		if(view.getMmtType()!=null)
			oblMmtDirection.setValue(view.getMmtType());
			
		cbMmtStatus.setValue(view.isMmtStatus());
	}
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.MMNTRANSACTIONTYPE,LoginPresenter.currentModule.trim());
	}

}
