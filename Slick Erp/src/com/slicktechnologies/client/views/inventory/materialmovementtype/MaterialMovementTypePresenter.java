package com.slicktechnologies.client.views.inventory.materialmovementtype;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.inventory.MaterialMovementType;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class MaterialMovementTypePresenter extends FormTableScreenPresenter<MaterialMovementType>{
	
	MaterialMovementTypeForm form;
	
	public MaterialMovementTypePresenter(FormTableScreen<MaterialMovementType> view,MaterialMovementType model) {
		super(view, model);
		form= (MaterialMovementTypeForm) view;
//		form.getSupertable().connectToLocal();
		form.retriveTable(getMaterialMovementTypeQuery());
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.MMNTRANSACTIONTYPE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	public MaterialMovementTypePresenter(FormTableScreen<MaterialMovementType> view,MaterialMovementType model,MyQuerry querry) {
		super(view, model);
		form=(MaterialMovementTypeForm) view;
		view.retriveTable(querry);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.MMNTRANSACTIONTYPE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	/**
	 * Method template to set the processBar events
	 */
	@SuppressWarnings("static-access")
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
    {
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
	 }
	
	public static void initalize()
	{
		MaterialMovementTypeTableProxy gentableScreen=new MaterialMovementTypeTableProxy();
		
		MaterialMovementTypeForm  form=new  MaterialMovementTypeForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
			
		MaterialMovementTypePresenter  presenter=new  MaterialMovementTypePresenter(form,new MaterialMovementType());
		AppMemory.getAppMemory().stickPnel(form);
	}

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}

	@Override
	protected void makeNewModel() {
		model= new MaterialMovementType();
	}
	
	public MyQuerry getMaterialMovementTypeQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new MaterialMovementType());
		return quer;
	}

}
