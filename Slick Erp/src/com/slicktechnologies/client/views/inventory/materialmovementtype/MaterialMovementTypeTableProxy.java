package com.slicktechnologies.client.views.inventory.materialmovementtype;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.inventory.MaterialMovementType;

public class MaterialMovementTypeTableProxy extends SuperTable<MaterialMovementType>{
	
	TextColumn<MaterialMovementType> getMmtNameColumn;
	TextColumn<MaterialMovementType> getMmtTypeColumn;
	TextColumn<MaterialMovementType> getMmtStatusColumn;
	
	public MaterialMovementTypeTableProxy()
	{
		super();
	}
	
	@Override
	public void createTable() {
		createColumnMmtName();
		createColumnMmtType();
		createColumnMmtStatus();
	}

	private void createColumnMmtStatus() {
		getMmtStatusColumn=new TextColumn<MaterialMovementType>() {
			
			@Override
			public String getValue(MaterialMovementType object) {
				if(object.isMmtStatus()==true)
					return "Active";
				else
					return "In Active";
			}
		};
		table.addColumn(getMmtStatusColumn, "Status");
		getMmtStatusColumn.setSortable(true);
	}

	private void createColumnMmtType() {
		getMmtTypeColumn=new TextColumn<MaterialMovementType>() {
			
			@Override
			public String getValue(MaterialMovementType object) {
				return object.getMmtType();
			}
		};
		table.addColumn(getMmtTypeColumn, "Direction");
		getMmtTypeColumn.setSortable(true);
	}

	private void createColumnMmtName() {
		getMmtNameColumn=new TextColumn<MaterialMovementType>() {
			
			@Override
			public String getValue(MaterialMovementType object) {
				return object.getMmtName();
			}
		};
		table.addColumn(getMmtNameColumn, "Name");
		getMmtNameColumn.setSortable(true);
	}

	public void addColumnSorting(){
		addSortingMmtName();
		addSortingMmtType();
		addSortingMmtStatus();
	}
	
	
	private void addSortingMmtName() {
		List<MaterialMovementType> list=getDataprovider().getList();
		columnSort=new ListHandler<MaterialMovementType>(list);
		columnSort.setComparator(getMmtNameColumn, new Comparator<MaterialMovementType>()
		{
			@Override
			public int compare(MaterialMovementType e1,MaterialMovementType e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getMmtName()!=null && e2.getMmtName()!=null){
						return e1.getMmtName().compareTo(e2.getMmtName());}
				}
				else
				{
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingMmtType() {
		List<MaterialMovementType> list=getDataprovider().getList();
		columnSort=new ListHandler<MaterialMovementType>(list);
		columnSort.setComparator(getMmtTypeColumn, new Comparator<MaterialMovementType>()
		{
			@Override
			public int compare(MaterialMovementType e1,MaterialMovementType e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getMmtType()!=null && e2.getMmtType()!=null){
						return e1.getMmtType().compareTo(e2.getMmtType());}
				}
				else
				{
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingMmtStatus() {
		List<MaterialMovementType> list=getDataprovider().getList();
		columnSort=new ListHandler<MaterialMovementType>(list);
		columnSort.setComparator(getMmtStatusColumn, new Comparator<MaterialMovementType>()
		{
			@Override
			public int compare(MaterialMovementType e1,MaterialMovementType e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.isMmtStatus()== e2.isMmtStatus()){
						return 0;
					}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	
	

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
