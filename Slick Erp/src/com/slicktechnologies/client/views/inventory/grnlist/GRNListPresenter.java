package com.slicktechnologies.client.views.inventory.grnlist;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNForm;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNPresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNSearchForm;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNSearchTable;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class GRNListPresenter extends TableScreenPresenter<GRN> {
	
	TableScreen<GRN>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	public GRNListPresenter(TableScreen<GRN> view, GRN model) {
		super(view, model);
		form=view;
		form.getSearchpopupscreen().getDwnload().setVisible(false);
//		view.retriveTable(getProductTransactionListQuery());
		setTableSelectionOnProducts();
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.GRNLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	private MyQuerry getProductTransactionListQuery() {
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new GRN());
		return quer;
	}

	public GRNListPresenter(TableScreen<GRN> view,GRN model,MyQuerry querry) {
		super(view, model);
		form=view;
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		view.retriveTable(querry);
		setTableSelectionOnProducts();
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.GRNLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}

	}
	
	public static void initalize()
	{
		GRNSearchTable table=new GRNSearchTable();
		GRNListForm form=new GRNListForm(table);
		
		GRNSearchForm.staticSuperTable=table;
		GRNSearchForm searchPopUp=new GRNSearchForm(false);
		form.setSearchpopupscreen(searchPopUp);
		
		GRNListPresenter presenter=new GRNListPresenter(form,new GRN());
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	public static void initalize(MyQuerry querry)
	{
		GRNSearchTable table=new GRNSearchTable();
		GRNListForm form=new GRNListForm(table);
		
		GRNSearchForm.staticSuperTable=table;
		GRNSearchForm searchPopUp=new GRNSearchForm(false);
		form.setSearchpopupscreen(searchPopUp);
		
		GRNListPresenter presenter=new GRNListPresenter(form,new GRN(),querry);
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}

	
	@Override
	protected void makeNewModel() {
		model=new GRN();
	}
	
	@Override
	public void reactOnDownload() {
		ArrayList<GRN> custarray = new ArrayList<GRN>();
		List<GRN> list = (List<GRN>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		custarray.addAll(list);
		csvservice.setGRN(custarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
					
				/**
				 * nidhi
				 * 16-4-2018
				 * for grn custom report download using process configration
				 * 
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", "GRNReport")){
					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url = gwt + "csvservlet" + "?type=" + 138;
					Window.open(url, "test", "enabled");
				}else{
				
					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url = gwt + "csvservlet" + "?type=" + 48;
					Window.open(url, "test", "enabled");
				}
			
			
			}
		});
	}
	
	private void setTableSelectionOnProducts() {
		final NoSelectionModel<GRN> selectionModelMyObj = new NoSelectionModel<GRN>();
		 
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	    	 @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	    		 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/GRN",Screen.GRN);
	        	 final GRN entity=selectionModelMyObj.getLastSelectedObject();
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 final GRNForm form = GRNPresenter.initalize();
	        	  
	        	 form.showWaitSymbol();
	        	 Timer timer=new Timer() {
	 				
	        	@Override
	 			public void run() {
	        		form.hideWaitSymbol();
					form.updateView(entity);
				    form.setToViewState();
	 			}
	 		};
	        timer.schedule(3000); 
	     }
	  };
	  // Add the handler to the selection model
	  selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	  // Add the selection model to the table
	  form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
	}
	
	
	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}

}
