package com.slicktechnologies.client.views.inventory.storagebin;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class StoragebinPresenter extends FormScreenPresenter<Storagebin>
		implements ValueChangeHandler<String> {

	StoragebinForm form;    
	final static GenricServiceAsync async = GWT.create(GenricService.class);

	public StoragebinPresenter(FormScreen<Storagebin> view, Storagebin model) {
		super(view, model);
		form = (StoragebinForm) view;
		form.gettbStorageBinName().addValueChangeHandler(this);

		boolean isDownload = AuthorizationHelper.getDownloadAuthorization(
				Screen.STORAGEBIN, LoginPresenter.currentModule.trim());
		if (isDownload == false) {
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel lbl = (InlineLabel) e.getSource();
		if (lbl.getText().contains("New")) {
			form.setToNewState();
			this.initalize();
		}

	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void makeNewModel() {
		model = new Storagebin();

	}

	public MyQuerry getstorageBinQuery() {
		MyQuerry quer = new MyQuerry(new Vector<Filter>(), new Storagebin());
		return quer;
	}

	public void setModel(Storagebin entity) {
		model = entity;
	}

	public static void initalize() {

		StoragebinForm form = new StoragebinForm();

		StoragebinPresenterTable gentable = new StoragebinPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		StoragebinPresenterSearch.staticSuperTable = gentable;
		StoragebinPresenterSearch searchpopup = new StoragebinPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		StoragebinPresenter presenter = new StoragebinPresenter(form,
				new Storagebin());
		AppMemory.getAppMemory().stickPnel(form);

	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.inventory.Storagebin")
	public static class StoragebinPresenterTable extends SuperTable<Storagebin>
			implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub

		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub

		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub

		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub

		}
	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.inventory.Storagebin")
	public static class StoragebinPresenterSearch extends
			SearchPopUpScreen<Storagebin> {

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
	};

	@Override
	public void reactOnDownload() {

		// super.reactOnDownload();
		CsvServiceAsync async = GWT.create(CsvService.class);
		ArrayList<Storagebin> whArray = new ArrayList<Storagebin>();
		List<Storagebin> list = form.getSearchpopupscreen().getSupertable()
				.getDataprovider().getList(); // 1 no
		whArray.addAll(list);
		async.setStorageBinList(whArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 116;
				Window.open(url, "test", "enabled");
			}
		});
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {

		ScreeenState scr = AppMemory.getAppMemory().currentState;

		if (scr.equals(ScreeenState.NEW)) {
			if (event.getSource().equals(form.gettbStorageBinName())) {
				validateStorageBin();
			}
		}

	}

	private void validateStorageBin() {

		System.out.println("in side condition ");
		String storageWarHouseNameVal = form.olbWarehouse.getValue();
		String storageLocationNameVal = form.oblstorageLocation.getValue();

		if (!storageWarHouseNameVal.equals("")) {
			
			if (!storageLocationNameVal.equals("")) {  
				
				if (!form.gettbStorageBinName().getValue().equals("")) {
					
					String storageBinNameVal = form.gettbStorageBinName().getValue().trim();

					if (storageBinNameVal != null)
					   {

						System.out.println("warehouse ="+storageWarHouseNameVal);
						System.out.println("StorageLocation ="+storageLocationNameVal);
						System.out.println("StorageBin ="+form.gettbStorageBinName().getValue().trim());
						
						final MyQuerry querry = new MyQuerry();
						Vector<Filter> filtervec = new Vector<Filter>();
						Filter temp = null;

						temp = new Filter();
						temp.setQuerryString("companyId");
						temp.setLongValue(model.getCompanyId());
						filtervec.add(temp);

						temp = new Filter();
						temp.setQuerryString("binName");
						temp.setStringValue(form.gettbStorageBinName().getValue().trim());
						filtervec.add(temp);
						
						temp = new Filter();
						temp.setQuerryString("storagelocation");
						temp.setStringValue(storageLocationNameVal);
						filtervec.add(temp);
						
						temp = new Filter();
						temp.setQuerryString("warehouseName");
						temp.setStringValue(storageWarHouseNameVal);
						filtervec.add(temp);

						querry.setFilters(filtervec);
						querry.setQuerryObject(new Storagebin());
						
						async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");

									}

									@Override
									public void onSuccess(ArrayList<SuperModel> result) {
										System.out.println("Success"+ result.size());
										if (result.size() != 0) {
											form.showDialogMessage("Storage Bin Name Already Exists");
											form.gettbStorageBinName().setValue("");

										}
										if (result.size() == 0) {

										}
									}
								});
					}
				}
			}
		}

	}

}
