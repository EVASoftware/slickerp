package com.slicktechnologies.client.views.inventory.storagebin;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.views.inventory.storagebin.StoragebinPresenter.StoragebinPresenterTable;
import com.slicktechnologies.shared.common.inventory.Storagebin;

public class StoragebinPresenterTableProxy extends StoragebinPresenterTable {

	TextColumn<Storagebin> getStatusColumn;
	TextColumn<Storagebin> getStoragebinNameColumn; // added by Ajinkya
	TextColumn<Storagebin> getCountColumn;
	TextColumn<Storagebin> getStorageLocationNameColumn;
	TextColumn<Storagebin> getWareHouseNameColumn;

	public Object getVarRef(String varName) {

		if (varName.equals("getStatusColumn"))
			return this.getStatusColumn;

		if (varName.equals("getStoragebinNameColumn")) // added by Ajinkya
			return this.getStoragebinNameColumn;

		if (varName.equals("getStorageLocationNameColumn")) // added by Ajinkya
			return this.getStorageLocationNameColumn;

		if (varName.equals("getWareHouseNameColumn")) // added by Ajinkya
			return this.getWareHouseNameColumn;

		if (varName.equals("getCountColumn"))
			return this.getCountColumn;
		return null;
	}

	public StoragebinPresenterTableProxy() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetWareHouseName();
		addColumngetStorageLocationName();
		addColumngetStoragebinName(); // added by Ajinkya
		addColumngetStatus();

	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<Storagebin>() {
			@Override
			public Object getKey(Storagebin item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetWareHouseName();
		addSortinggetStorageLocationName();
		addSortinggetStoragebinName(); // added by Ajinkya
		addSortinggetStatus();

	}

	@Override
	public void addFieldUpdater() {

	}

	protected void addSortinggetCount() {
		List<Storagebin> list = getDataprovider().getList();
		columnSort = new ListHandler<Storagebin>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Storagebin>() {
			@Override
			public int compare(Storagebin e1, Storagebin e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<Storagebin>() {
			@Override
			public String getValue(Storagebin object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		getCountColumn.setSortable(true);
	}

	protected void addSortinggetStatus() // added by Ajinkya
	{
		List<Storagebin> list = getDataprovider().getList();
		columnSort = new ListHandler<Storagebin>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Storagebin>() {
			@Override
			public int compare(Storagebin e1, Storagebin e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null && e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<Storagebin>() {
			@Override
			public String getValue(Storagebin object) {
				if (object.getStatus() == true)
					return "Active";
				else
					return "In Active";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
	}

	protected void addSortinggetStoragebinName() // added by Ajinkya
	{
		List<Storagebin> list = getDataprovider().getList();
		columnSort = new ListHandler<Storagebin>(list);
		columnSort.setComparator(getStoragebinNameColumn,
				new Comparator<Storagebin>() {
					@Override
					public int compare(Storagebin e1, Storagebin e2) {
						if (e1 != null && e2 != null) {
							if (e1.getBinName() != null
									&& e2.getBinName() != null) {
								return e1.getBinName().compareTo(
										e2.getBinName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	protected void addColumngetStoragebinName() // added by Ajinkya
	{

		getStoragebinNameColumn = new TextColumn<Storagebin>() {
			@Override
			public String getValue(Storagebin object) {
				return object.getBinName() + "";
			}
		};
		table.addColumn(getStoragebinNameColumn, "Storage Bin");
		getStoragebinNameColumn.setSortable(true);
	}

	protected void addSortinggetStorageLocationName() // added by Ajinkya
	{
		List<Storagebin> list = getDataprovider().getList();
		columnSort = new ListHandler<Storagebin>(list);
		columnSort.setComparator(getStorageLocationNameColumn,
				new Comparator<Storagebin>() {
					@Override
					public int compare(Storagebin e1, Storagebin e2) {
						if (e1 != null && e2 != null) {
							if (e1.getStoragelocation() != null && e2.getStoragelocation() != null) 
							{
								return e1.getStoragelocation().compareTo(e2.getStoragelocation());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumngetStorageLocationName() {
		getStorageLocationNameColumn = new TextColumn<Storagebin>() {
			@Override
			public String getValue(Storagebin object) {
				return object.getStoragelocation() + "";
			}
		};
		table.addColumn(getStorageLocationNameColumn, "Storage Location");
		getStorageLocationNameColumn.setSortable(true);
	}

	private void addSortinggetWareHouseName() {

		List<Storagebin> list = getDataprovider().getList();
		columnSort = new ListHandler<Storagebin>(list);
		
		columnSort.setComparator(getWareHouseNameColumn, new Comparator <Storagebin>()
				{
					@Override
					public int compare(Storagebin e1, Storagebin e2)
					{
						if (e1 != null && e2 != null)
						{
							if (e1.getWarehouseName() != null && e2.getWarehouseName() != null)
							{
								return e1.getWarehouseName().compareTo(e2.getWarehouseName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	private void addColumngetWareHouseName() {
		
		getWareHouseNameColumn = new TextColumn<Storagebin>()
				{
			@Override
			
			public String getValue(Storagebin object)
			{
				return object.getWarehouseName()+"";
			}  
				};
				table.addColumn(getWareHouseNameColumn,"Warehouse Name");
				getWareHouseNameColumn.setSortable(true);
	       }

}
