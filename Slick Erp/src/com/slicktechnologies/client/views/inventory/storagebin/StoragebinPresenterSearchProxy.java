package com.slicktechnologies.client.views.inventory.storagebin;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.inventory.storagebin.StoragebinPresenter.StoragebinPresenterSearch;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;

public class StoragebinPresenterSearchProxy extends StoragebinPresenterSearch
		implements ChangeHandler {

	final GenricServiceAsync async = GWT.create(GenricService.class);
	    /////////////////////////////// added by Ajinkya  ////////////////////////////
	public ListBox lbStoragebinStatus; 
	public ObjectListBox<WareHouse> olbWareHouseName; 
	public ListBox olbStorageLocation; 
	public ListBox olbStoragebin;
       
	
	
	public Object getVarRef(String varName) {
		if (varName.equals("olbWareHouseName"))
			return true;

		if (varName.equals("olbStorageLocationName"))
			return true;

		if (varName.equals("olbStoragebin"))
			return true;

		if (varName.equals("lbStorageLocationStatus"))
			return true;

		return null;
	}

         ////////////////////////////////////////  End Here  //////////////////////////////////////
	public StoragebinPresenterSearchProxy() {
		super();
		createGui();
	}

	public void initWidget() {
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Storagebin());

		olbWareHouseName = new ObjectListBox<WareHouse>();
		AppUtility.initializeWarehouse(olbWareHouseName);
		olbWareHouseName.addChangeHandler(this);

		olbStorageLocation = new ListBox();
		olbStorageLocation.addItem("--SELECT--");
		olbStorageLocation.addChangeHandler(this);

		olbStoragebin = new ListBox();
		olbStoragebin.addItem("--SELECT--");
		olbStoragebin.addChangeHandler(this);

		lbStoragebinStatus = new ListBox(); // added by Ajinkya
		lbStoragebinStatus.addItem("--SELECT--");
		lbStoragebinStatus.addItem(AppConstants.ACTIVE);
		lbStoragebinStatus.addItem(AppConstants.INACTIVE);

	}

	public void createScreen() {
		initWidget();
		FormFieldBuilder builder;

		// /////////////////////////////// added by Ajinkya // /////////////////////////////////////

		builder = new FormFieldBuilder("Storage Location", olbStorageLocation);
		FormField folbStorageLocation = builder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder(" Warehouse ", olbWareHouseName);
		FormField folbWareHouse = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Status",
				lbStoragebinStatus);
		FormField flbstatus = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		builder = new FormFieldBuilder("Storage Bin", olbStoragebin);
		FormField folbStoragebin = builder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		this.fields = new FormField[][] {

		{ folbWareHouse, folbStorageLocation, folbStoragebin, flbstatus },    // added  by  Ajinkya

		};
	}

	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		// added by Ajinkya

		if (lbStoragebinStatus.getSelectedIndex() != 0) {
			temp = new Filter();
			int index = lbStoragebinStatus.getSelectedIndex();
			String selectedItem = lbStoragebinStatus.getItemText(index);
			temp.setQuerryString("status");

			if (selectedItem.equals(AppConstants.ACTIVE)) {
				temp.setBooleanvalue(true);
			}
			if (selectedItem.equals(AppConstants.INACTIVE)) {
				temp.setBooleanvalue(false);

			}
			 filtervec.add(temp);
		}

		if (olbWareHouseName.getSelectedIndex() != 0) {
			
			temp = new Filter();
			temp.setStringValue(olbWareHouseName.getValue(olbWareHouseName.getSelectedIndex()).trim());
			temp.setQuerryString("warehouseName");
			filtervec.add(temp);

		}

		if (olbStorageLocation.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbStorageLocation.getValue(
					olbStorageLocation.getSelectedIndex()).trim());  
			temp.setQuerryString("storagelocation");
			filtervec.add(temp);
		}

		if (olbStoragebin.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbStoragebin.getValue(  
					olbStoragebin.getSelectedIndex()).trim());
			temp.setQuerryString("binName");
			filtervec.add(temp);
		}

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Storagebin());
		return querry;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void onChange(ChangeEvent event) {

		if (event.getSource().equals(olbWareHouseName)) {
			retriveStorageLocation();
		}
		if (event.getSource().equals(olbStorageLocation)) {
			retriveStorageBin();
		}

	}

	private void retriveStorageBin() {

		MyQuerry query = new MyQuerry();
		Company c = new Company();
		System.out.println("Company Id :: " + c.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("storagelocation");
		filter.setStringValue(olbStorageLocation.getValue(olbStorageLocation.getSelectedIndex()));
		filtervec.add(filter);

		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());

		async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						olbStoragebin.clear();
						olbStoragebin.addItem("--SELECT--");
						for (SuperModel model : result) {
							Storagebin entity = (Storagebin) model;
							olbStoragebin.addItem(entity.getBinName());
							System.out.println("BIN :" + entity.getBinName());
						}

					}
				});

	}

	private void retriveStorageLocation() {

		MyQuerry query = new MyQuerry();
		Company c = new Company();
		System.out.println("Company Id :: " + c.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("warehouseName");
		filter.setStringValue(olbWareHouseName.getValue(olbWareHouseName.getSelectedIndex()));
		filtervec.add(filter);

		query.setFilters(filtervec);
		query.setQuerryObject(new StorageLocation());

		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						olbStorageLocation.clear();
						olbStoragebin.clear();
						olbStorageLocation.addItem("--SELECT--");
						olbStoragebin.addItem("--SELECT--");
						for (SuperModel model : result) {
							StorageLocation entity = (StorageLocation) model;
							// storageList.add(entity.getBusinessUnitName());
							olbStorageLocation.addItem(entity.getBusinessUnitName());
							System.out.println("Location :"+ entity.getBusinessUnitName());
						}

					}
				});

	}

}
