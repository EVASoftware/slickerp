package com.slicktechnologies.client.views.inventory.storagebin;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.inventory.Storagebin;

public class StoragebinTable extends SuperTable<Storagebin>{

	TextColumn<Storagebin> columnName;
	TextColumn<Storagebin> columnID;
	TextColumn<Storagebin> columnXcord;
	TextColumn<Storagebin> columnYcord;
	TextColumn<Storagebin> columnstatus;
	TextColumn<Storagebin> columnstorageloc;
	TextColumn<Storagebin> columndescription;
	
	TextColumn<Storagebin> columnWarehouseName;
	
	
	
	@Override
	public void createTable() {
		
		addcolumnId();
		addColumnWarehouseName();
		addcolumnstorageloc();
		addcolumnName();
		addcolumnxcord();
		addcolumnycord();
		addcolumndescription();
		addcolumnstatus();
		
	}
	
	

	public void addcolumnId()
	{
		columnID=new TextColumn<Storagebin>() {
			
			@Override
			public String getValue(Storagebin object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(columnID,"ID");
		columnID.setSortable(true);
		table.setColumnWidth(columnID, 100, Unit.PX);
	}
	public void addcolumnxcord()
	{
		columnXcord=new TextColumn<Storagebin>() {
			
			@Override
			public String getValue(Storagebin object) {
				return object.getXcoordinate();
			}
		};
		table.addColumn(columnXcord,"X Coordinate");
		columnXcord.setSortable(true);
		table.setColumnWidth(columnXcord, 80, Unit.PX);
	}
	
	public void addcolumnycord()
	{
		columnYcord=new TextColumn<Storagebin>() {
			
			@Override
			public String getValue(Storagebin object) {
				return object.getYcoordinate();
			}
		};
		table.addColumn(columnYcord,"Y Coordinate");
		columnYcord.setSortable(true);
		table.setColumnWidth(columnYcord, 80, Unit.PX);
	}
	
	public void addcolumndescription()
	{
		columndescription=new TextColumn<Storagebin>() {
			
			@Override
			public String getValue(Storagebin object) {
				return object.getDescription();
			}
		};
		table.addColumn(columndescription,"Description");
		columndescription.setSortable(true);
		table.setColumnWidth(columndescription, 110, Unit.PX);
	}
	
	public void addcolumnName()
	{
		columnName=new TextColumn<Storagebin>() {
			
			@Override
			public String getValue(Storagebin object) {
				return object.getBinName();
			}
		};
		table.addColumn(columnName,"Name");
		columnName.setSortable(true);
		table.setColumnWidth(columnName, 110, Unit.PX);
		
	}
	
	
	
	public void addcolumnstorageloc()
	{
		columnstorageloc=new TextColumn<Storagebin>() {
			
			@Override
			public String getValue(Storagebin object) {
				return object.getStoragelocation();
			}
		};
		table.addColumn(columnstorageloc,"Storage Location");
		columnstorageloc.setSortable(true);
		table.setColumnWidth(columnstorageloc, 110, Unit.PX);
	}
	
	public void addcolumnstatus()
	{
		columnstatus=new TextColumn<Storagebin>() {
			
			@Override
			public String getValue(Storagebin object) {
				if(object.getStatus()==true)
				return "Active";
				else return "Inactive";
			}
		};
		table.addColumn(columnstatus,"Status");
		columnstatus.setSortable(true);
		table.setColumnWidth(columnstatus, 100, Unit.PX);
	}

	
	private void addColumnWarehouseName() {
		columnWarehouseName=new TextColumn<Storagebin>() {
			
			@Override
			public String getValue(Storagebin object) {
				if(object.getWarehouseName()!=null){
					return object.getWarehouseName();
				}
				return "";
			}
		};
		table.addColumn(columnWarehouseName,"Warehouse Name");
		columnWarehouseName.setSortable(true);
		table.setColumnWidth(columnWarehouseName, 110, Unit.PX);
		
	}
	
	
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetName();
		addSortingXCord();
		addSortinggetYCord();
		addSortinggetStoLoc();
		addSortinggetDesc();
		addSortinggetstatus();
		
		addSortingToWarehouseColumn();
	}
	
	private void addSortingToWarehouseColumn() {
		List<Storagebin> list=getDataprovider().getList();
		columnSort=new ListHandler<Storagebin>(list);
		columnSort.setComparator(columnWarehouseName, new Comparator<Storagebin>(){
			@Override
			public int compare(Storagebin e1, Storagebin e2) {
				if (e1 != null && e2 != null) {
					if (e1.getWarehouseName() != null&& e2.getWarehouseName() != null) {
						return e1.getWarehouseName().compareTo(e2.getWarehouseName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);	
	}



	protected void addSortinggetCount()
	{
		List<Storagebin> list=getDataprovider().getList();
		columnSort=new ListHandler<Storagebin>(list);
		columnSort.setComparator(columnID, new Comparator<Storagebin>()
				{
			@Override
			public int compare(Storagebin e1,Storagebin e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetName()
	{
		List<Storagebin> list=getDataprovider().getList();
		columnSort=new ListHandler<Storagebin>(list);
		columnSort.setComparator(columnName, new Comparator<Storagebin>()
				{
			@Override
			public int compare(Storagebin e1,Storagebin e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBinName()!=null && e2.getBinName()!=null){
						return e1.getBinName().compareTo(e2.getBinName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingXCord()
	{
		List<Storagebin> list=getDataprovider().getList();
		columnSort=new ListHandler<Storagebin>(list);
		columnSort.setComparator(columnXcord, new Comparator<Storagebin>()
				{
			@Override
			public int compare(Storagebin e1,Storagebin e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getXcoordinate()!=null && e2.getXcoordinate()!=null){
						return e1.getXcoordinate().compareTo(e2.getXcoordinate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetYCord()
	{
		List<Storagebin> list=getDataprovider().getList();
		columnSort=new ListHandler<Storagebin>(list);
		columnSort.setComparator(columnYcord, new Comparator<Storagebin>()
				{
			@Override
			public int compare(Storagebin e1,Storagebin e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getYcoordinate()!=null && e2.getYcoordinate()!=null){
						return e1.getYcoordinate().compareTo(e2.getYcoordinate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggetStoLoc()
	{
		List<Storagebin> list=getDataprovider().getList();
		columnSort=new ListHandler<Storagebin>(list);
		columnSort.setComparator(columnstorageloc, new Comparator<Storagebin>()
				{
			@Override
			public int compare(Storagebin e1,Storagebin e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStoragelocation()!=null && e2.getStoragelocation()!=null){
						return e1.getStoragelocation().compareTo(e2.getStoragelocation());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addSortinggetDesc()
	{
		List<Storagebin> list=getDataprovider().getList();
		columnSort=new ListHandler<Storagebin>(list);
		columnSort.setComparator(columndescription, new Comparator<Storagebin>()
				{
			@Override
			public int compare(Storagebin e1,Storagebin e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDescription()!=null && e2.getDescription()!=null){
						return e1.getDescription().compareTo(e2.getDescription());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetstatus()
	{
		List<Storagebin> list=getDataprovider().getList();
		columnSort=new ListHandler<Storagebin>(list);
		columnSort.setComparator(columnstatus, new Comparator<Storagebin>()
				{
			@Override
			public int compare(Storagebin e1,Storagebin e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	
	
	
	
	
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
