package com.slicktechnologies.client.views.inventory.storagebin;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class StoragebinForm extends FormScreen<Storagebin> implements ChangeHandler{
	TextBox ibBinId;
	TextBox tbBinName,tbxcordinate,tbycordinate;
	TextArea tadescription;
	
	                       /////////////// added By Ajinkya  ///////////////////////
	
	ObjectListBox<StorageLocation> oblstorageLocation;     
	
	                    
	                       //////////////////// end Here /////////////////////// 
	
	
	CheckBox status;
	
	
	/**
	 * Date : 23-11-2016 By Anil
	 */
	ObjectListBox<WareHouse> olbWarehouse;
	/** date 10.09.2019 added by komal to store main branch warehouse bin as per nitin sir's instruction**/
	CheckBox cbIsMainBranchWareHouse;
	
	public StoragebinForm  () {
		super();
		createGui();
	}
	
	public StoragebinForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);   // added by Ajinkya for form screen 
		createGui();
		ibBinId.setEnabled(false);
		

	}

	private void initalizeWidget()
	{
		ibBinId=new TextBox();
		ibBinId.setEnabled(false);
		tbBinName=new TextBox();
		tbxcordinate=new TextBox();
		tbycordinate=new TextBox();
		tadescription=new TextArea();
		status=new CheckBox();
		status.setValue(true);
		
		oblstorageLocation = new ObjectListBox<StorageLocation>();    // added by Ajinkya
		
		olbWarehouse=new ObjectListBox<WareHouse>();
		AppUtility.initializeWarehouse(olbWarehouse);
		olbWarehouse.addChangeHandler(this);
		cbIsMainBranchWareHouse = new CheckBox();
	}
	
	public void initializeWarehouse()
	{
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new WareHouse());
		olbWarehouse.MakeLive(querry);
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames=new String[]{"New"};
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCountryInformation=fbuilder.setlabel("Storage Bin Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Storage Bin Id",ibBinId);
		FormField fbinid= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Name",tbBinName);
		FormField fname= fbuilder.setMandatory(true).setMandatoryMsg(" Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* X-coordinate",tbxcordinate);
		FormField xcord= fbuilder.setMandatory(true).setMandatoryMsg("Y-Coordinate is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Y-coordinate",tbycordinate);
		FormField ycord= fbuilder.setMandatory(true).setMandatoryMsg("Y-Coordinate is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Description",tadescription);
		FormField ftadescription= fbuilder.setMandatory(false).setMandatoryMsg("Description is mandatory!").setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Storage Location",oblstorageLocation);
		FormField fstorageloc= fbuilder.setMandatory(true).setMandatoryMsg("Storage location is mandatory!").setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Status",status);
		FormField fstatus= fbuilder.setMandatory(false).setMandatoryMsg("Storage location is mandatory!").setRowSpan(0).setColSpan(0).build();
	
		
		fbuilder = new FormFieldBuilder("* Warehouse Name",olbWarehouse);
		FormField folbWarehouse= fbuilder.setMandatory(true).setMandatoryMsg("Warehouse Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Branch Warehouse",cbIsMainBranchWareHouse);
		FormField fcbIsMainBranchWareHouse = fbuilder.setMandatory(false).setMandatoryMsg("").setRowSpan(0).setColSpan(0).build();
	
		FormField[][] formfield = {  
			{fgroupingCountryInformation},
			{fbinid,folbWarehouse,fstorageloc,fname},
			{xcord,ycord,fstatus,fcbIsMainBranchWareHouse},
			{ftadescription}
		};
		this.fields=formfield;		
	}

		/**
		 * method template to update the model with token entity name
		 */
		@Override
		public void updateModel(Storagebin model) 
		{
			if(tbBinName.getValue()!=null)
				model.setBinName(tbBinName.getValue().trim());
			if(tbxcordinate.getValue()!=null)
				model.setXcoordinate(tbxcordinate.getValue());
			if(tbycordinate.getValue()!=null)
				model.setYcoordinate(tbycordinate.getValue());
			if(tadescription.getValue()!=null)
				model.setDescription(tadescription.getValue());
			if(olbWarehouse.getValue()!=null){
				model.setWarehouseName(olbWarehouse.getValue());
			}
			if(oblstorageLocation.getValue()!=null)
				model.setStoragelocation(oblstorageLocation.getValue());
			model.setStatus(status.getValue());
			model.setMainWareHouseBin(cbIsMainBranchWareHouse.getValue());
			presenter.setModel(model);

		}

		/**
		 * method template to update the view with token entity name
		 */
		@Override
		public void updateView(Storagebin view) 
		{
			if(view.getBinName()!=null)
				tbBinName.setValue(view.getBinName());
			if(view.getDescription()!=null)
				tadescription.setValue(view.getDescription());
			if(view.getXcoordinate()!=null)
				tbxcordinate.setValue(view.getXcoordinate());
			if(view.getYcoordinate()!=null)
				tbycordinate.setValue(view.getYcoordinate());
			if(view.getWarehouseName()!=null){
				olbWarehouse.setValue(view.getWarehouseName());
				initializeAndSetStorageLocation(view.getWarehouseName(), view.getStoragelocation());
			}
//			if(view.getStoragelocation()!=null)
//				oblstoragebin.setValue(view.getStoragelocation());
			ibBinId.setValue(view.getCount()+"");
			status.setValue(view.getStatus());
			cbIsMainBranchWareHouse.setValue(view.isMainWareHouseBin());
			presenter.setModel(view);
		}

		/**
		 * Toggles the app header bar menus as per screen state
		 */

		public void toggleAppHeaderBarMenu() {    // added by Ajinkya for search screen 
			

			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   

				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
						menus[k].setVisible(true); 

					else
						menus[k].setVisible(false);  		   
				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
						menus[k].setVisible(true); 

					else  
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.STORAGEBIN,LoginPresenter.currentModule.trim());
		}

		/**
		 * sets the id textbox with the passed count value. 
		 */
		@Override
		public void setCount(int count)
		{
		ibBinId.setValue(count+"");	
		}


		@Override
		public void setEnable(boolean state) {
			super.setEnable(state);
			ibBinId.setEnabled(false);

		}

		@Override
		public void setToEditState() {
			super.setToEditState();
			this.processLevelBar.setVisibleFalse(false);
		}

		protected void initalizeStorageloction()
		{
			MyQuerry querry = new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(c.getCompanyId());
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new StorageLocation());

			
			oblstorageLocation.MakeLive(querry);

		}

		@Override
		public void onChange(ChangeEvent event) {
			if(event.getSource().equals(olbWarehouse)){
				if(olbWarehouse.getSelectedIndex()!=0){
					
					MyQuerry querry = new MyQuerry();
					Company c=new Company();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter temp=null;
					
					temp=new Filter();
					temp.setQuerryString("status");
					temp.setBooleanvalue(true);
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("companyId");
					temp.setLongValue(c.getCompanyId());
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("warehouseName");
					temp.setStringValue(olbWarehouse.getValue().trim());
					filtervec.add(temp);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new StorageLocation());

					
					oblstorageLocation.MakeLive(querry);
					
					
				}else{
					oblstorageLocation.setSelectedIndex(0);
				}
			}
		}
		
		public void initializeAndSetStorageLocation(String warehouseName,final String storageLocation){
			GenricServiceAsync service=GWT.create(GenricService.class);
			 
			MyQuerry querry = new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(c.getCompanyId());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("warehouseName");
			temp.setStringValue(warehouseName.trim());
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new StorageLocation());
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					List<StorageLocation> list=new ArrayList<StorageLocation>();
					for(SuperModel model:result){
						StorageLocation entity=(StorageLocation) model;
						list.add(entity);
					}
					oblstorageLocation.setListItems(list);
					oblstorageLocation.setValue(storageLocation);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					
				}
			});
		}

		
		public TextBox gettbStorageBinName(){
			return tbBinName ; 
		}
		
		public void setTbStorageBinName(TextBox tbBinName) 
		{
			this.tbBinName = tbBinName;
		}
		
	

}
