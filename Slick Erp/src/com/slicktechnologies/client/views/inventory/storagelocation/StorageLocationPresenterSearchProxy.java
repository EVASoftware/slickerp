package com.slicktechnologies.client.views.inventory.storagelocation;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.inventory.storagelocation.StorageLocationPresenter.StorageLocationPresenterSearch;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.WareHouse;

public class StorageLocationPresenterSearchProxy extends StorageLocationPresenterSearch implements ChangeHandler {
	
	  final GenricServiceAsync async = GWT.create(GenricService.class);
	  public ListBox lbStorageLocationStatus;        // added by Ajinkya
	  public ObjectListBox<WareHouse> olbWareHouseName;
	  public ListBox olbStorageLocationName;
	  
	  
		public Object getVarRef(String varName)
	  	{
	     
	  		if(varName.equals("olbWareHouseName"))
	  			return true ;
	  		
	  		if(varName.equals("olbStorageLocationName"))
	  			return true ;
	  		
	  		if(varName.equals("lbStorageLocationStatus"))
	  			return true ;
	  		
	  		return null ;
	  	
	  	}    
	         
	public StorageLocationPresenterSearchProxy()
	{
		super();
		createGui();
	}
	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new StorageLocation());
		       ///////////////////////////////// added by Ajinkya   //////////////////////////////////
		  olbWareHouseName =new ObjectListBox<WareHouse>();
		  AppUtility.initializeWarehouse(olbWareHouseName);
		  olbWareHouseName.addChangeHandler(this);
		  
//		  olbStorageLocationName=new ObjectListBox<StorageLocation>();
		  olbStorageLocationName = new ListBox();
		  olbStorageLocationName.addItem("--SELECT--");
		  olbStorageLocationName.addChangeHandler(this);
		  
		  lbStorageLocationStatus=new ListBox();    
		  lbStorageLocationStatus.addItem("--SELECT--");
		  lbStorageLocationStatus.addItem(AppConstants.ACTIVE);
		  lbStorageLocationStatus.addItem(AppConstants.INACTIVE);
			
		  
		  
		          ////////////////////////////    end Here  /////////////////////////////////////
		  
		  
		  
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		  
		  builder = new FormFieldBuilder("Status",lbStorageLocationStatus);   // added by Ajinkya
		  FormField flbstatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		  
		
			builder = new FormFieldBuilder("Warehouse Name",olbWareHouseName);    
			FormField ftbWareHouseName= builder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
			
			builder = new FormFieldBuilder("Storage Location Name",olbStorageLocationName);    
			FormField folbStorageLocationName= builder.setMandatory(true).setRowSpan(0).setColSpan(0).build();;
		  
		this.fields=new FormField[][]
				{
				
				{ftbWareHouseName,folbStorageLocationName,flbstatus},      // added by Ajinkya
		
				};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		  
		
		// added by Ajinkya
			  
			  if(lbStorageLocationStatus.getSelectedIndex()!=0){
				  temp=new Filter();
				  int index=lbStorageLocationStatus.getSelectedIndex();
				  String selectedItem=lbStorageLocationStatus.getItemText(index);
				  temp.setQuerryString("status");
				  if(selectedItem.equals(AppConstants.ACTIVE))
				  {
					  temp.setBooleanvalue(true);
				  }
				  if(selectedItem.equals(AppConstants.INACTIVE))
				  {
					  temp.setBooleanvalue(false);
					  
				  } 
				     filtervec.add(temp);
				  }
			  
			  if( olbWareHouseName.getSelectedIndex()!=0)
			  {
			  temp=new Filter();
			  
			  temp.setStringValue(olbWareHouseName.getValue(olbWareHouseName.getSelectedIndex()).trim());
			  temp.setQuerryString("warehouseName");
			  filtervec.add(temp);
			  
			  }  
			  
			  if (olbStorageLocationName.getSelectedIndex()!=0) {
					temp = new Filter();
					temp.setStringValue(olbStorageLocationName.getValue(olbStorageLocationName.getSelectedIndex()).trim());
					temp.setQuerryString("buisnessUnitName");
					filtervec.add(temp);
				}
			  
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new StorageLocation());
		return querry;
	}
	
	@Override
	public boolean validate() {
		
		return true ;
	
	}

	@Override
	public void onChange(ChangeEvent event) {

		if(event.getSource().equals(olbWareHouseName)){
			retriveStorageLocation();
			
	}
	
	}

	private void retriveStorageLocation() {
		

		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("warehouseName");
		filter.setStringValue(olbWareHouseName.getValue(olbWareHouseName.getSelectedIndex()));
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new StorageLocation());
		
		
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				olbStorageLocationName.clear();
				olbStorageLocationName.addItem("--SELECT--");
				for (SuperModel model : result) {
					StorageLocation entity = (StorageLocation) model;
					olbStorageLocationName.addItem(entity.getBusinessUnitName());
					System.out.println("Location :"+entity.getBusinessUnitName());
				}
				
			}
		});
	
		
		
		
	}


}
