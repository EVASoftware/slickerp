package com.slicktechnologies.client.views.inventory.storagelocation;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TextBoxBase;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.productinventoryview.ProductInventoryViewForm;
import com.slicktechnologies.client.views.inventory.warehouse.WareHouseForm;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class StorageLocationForm extends FormScreen<StorageLocation>  implements ClickHandler,ChangeHandler  {

	//Token to add the varialble declarations
	EmailTextBox etbEmail,etbPointOfContactEmail;
	AddressComposite addressComposite;
	CheckBox checkBoxStatus;
	TextBox tbBranchName,tbPointOfContactName;
	PhoneNumberBox pnbLandlinePhone,pnbCellPhoneNo1,pnbCellPhoneNo2,pnbFaxNo,pnbPointOfContactLandline,pnbPointOfContactCell;
	ObjectListBox<WareHouse> warehouse;
	FormField fgroupingCompanyInformation;
	TextBox tbStorageLocationId;   // added by Ajinkya
	
	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;

	Button copyCompAddrs;	
	
	/**
	 * Developed by : Rohan Bhagde 
	 * Used for :used calling public method present in ProductInventoryViewForm for initializing warehouse
	 * Method name :initializeWarehounse()
	 * Dated : 20/09/2016
	 */
	ProductInventoryViewForm productInventory;
	/**
	 * ends here 
	 */

	public StorageLocationForm  () {
		super();
		createGui();
		tbStorageLocationId.setEnabled(false);
	}
	

	public StorageLocationForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);    /// Added by ajinkya for form screen
		createGui();
		tbStorageLocationId.setEnabled(false);
		

	}

	private void initalizeWidget()
	{

		etbEmail=new EmailTextBox();

		etbPointOfContactEmail=new EmailTextBox();

		addressComposite=new AddressComposite();

		checkBoxStatus=new CheckBox();
		checkBoxStatus.setValue(true); 
		tbBranchName=new TextBox();

		tbPointOfContactName=new TextBox();

		pnbLandlinePhone=new PhoneNumberBox();

		pnbCellPhoneNo1=new PhoneNumberBox();

		pnbCellPhoneNo2=new PhoneNumberBox();

		pnbFaxNo=new PhoneNumberBox();

		pnbPointOfContactLandline=new PhoneNumberBox();

		pnbPointOfContactCell=new PhoneNumberBox();
		
		//  Anil has commented this for NBHC Date - 07/09/2016
		/** new Code **/
		copyCompAddrs = new Button("Copy Warehouse Address");
		copyCompAddrs.addClickHandler(this);
		
		warehouse=new ObjectListBox<WareHouse>();
		
		/** 
		 * Date 03-02-2017 By ANIL
		 * 
		 */
		AppUtility.initializeWarehouse(warehouse);
		

		/**
		 * Ajinkya Added id for  11/02/2017 
		 */
		      tbStorageLocationId = new TextBox();
		      
		/**
		 * ends here 
		 */
	
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.

	    initalizeWidget();
		
		this.processlevelBarNames=new String[]{"New"};
		

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		String mainScreenLabel="Warehouse Details";
		//fgroupingCompanyInformation.setLabel(mainScreenLabel);
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingCompanyInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();		

//		FormFieldBuilder fbuilder;
//		fbuilder = new FormFieldBuilder();
//		FormField fgroupingCompanyInformation=fbuilder.setlabel("Warehouse Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
//		
		fbuilder = new FormFieldBuilder("* Storage Location  Name",tbBranchName);
		FormField ftbBranchName= fbuilder.setMandatory(true).setMandatoryMsg("Storage Location Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("Storage Location Id ",tbStorageLocationId);    // added by Ajinkya
//		FormField ftbStorageLocationId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Storage Location  Email",etbEmail);
		FormField fetbEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",checkBoxStatus);
		FormField fcheckBoxStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Landline Phone",pnbLandlinePhone);
		FormField fpnbLandlinePhone= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cell Phone No 1",pnbCellPhoneNo1);
		FormField fpnbCellPhoneNo1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cell Phone No 2",pnbCellPhoneNo2);
		FormField fpnbCellPhoneNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Fax No",pnbFaxNo);
		FormField fpnbFaxNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPointOfContactInformation=fbuilder.setlabel("Contact Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Point Of Contact Name",tbPointOfContactName);
		FormField ftbPointOfContactName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Point Of Contact Landline",pnbPointOfContactLandline);
		FormField fpnbPointOfContactLandline= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Point Of Contact Cell",pnbPointOfContactCell);
		FormField fpnbPointOfContactCell= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Point Of Contact Email",etbPointOfContactEmail);
		FormField fetbPointOfContactEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAddressInformation=fbuilder.setlabel("Address Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",addressComposite);
		FormField faddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Warehouse",warehouse);
		FormField fwarehouse= fbuilder.setMandatory(true).setMandatoryMsg("Ware House  Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",copyCompAddrs);
		FormField fcopyCompAddrs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();



		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield ={ 
//		  {fgroupingCompanyInformation},
//		  {fwarehouse,ftbBranchName,fetbEmail,fcheckBoxStatus},   // id added by Ajinkya
//		  {fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2,fpnbFaxNo},
//		  {fgroupingPointOfContactInformation},
//		  {ftbPointOfContactName,fpnbPointOfContactLandline,fpnbPointOfContactCell,fetbPointOfContactEmail},
//		  {fgroupingAddressInformation},
//		  {fcopyCompAddrs},
//		  {faddressComposite},
//		  };
//			this.fields=formfield;	
		  /**
		   	* Added by Priyanka -08-02-2021
			* Des-Simplification of Storage location screen  Req. raise by Nitin Sir.
			*/
			{fgroupingCompanyInformation},
			{fwarehouse,ftbBranchName,fcheckBoxStatus},   
			/**Address Info**/
			{fgroupingAddressInformation},
			{fcopyCompAddrs},
			{faddressComposite},
			/**Contact Details**/
		    {fgroupingPointOfContactInformation},
		    {fetbEmail,fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2},
		    {fpnbFaxNo},
			{ftbPointOfContactName,fpnbPointOfContactLandline,fpnbPointOfContactCell,fetbPointOfContactEmail},
			
		    };
				this.fields=formfield;
		}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(StorageLocation model) 
	{

		if(tbBranchName.getValue()!=null)
			model.setBusinessUnitName(tbBranchName.getValue());
		if(etbEmail.getValue()!=null)
			model.setEmail(etbEmail.getValue());
		if(checkBoxStatus.getValue()!=null)
			model.setstatus(checkBoxStatus.getValue());
		if(pnbLandlinePhone.getValue()!=null)
			model.setLandline(pnbLandlinePhone.getValue());
		if(pnbCellPhoneNo1.getValue()!=null)
			model.setCellNumber1(pnbCellPhoneNo1.getValue());
		if(pnbCellPhoneNo2.getValue()!=null)
			model.setCellNumber2(pnbCellPhoneNo2.getValue());
		if(pnbFaxNo.getValue()!=null)
			model.setFaxNumber(pnbFaxNo.getValue());
		if(tbPointOfContactName.getValue()!=null)
			model.setPocName(tbPointOfContactName.getValue());
		if(pnbPointOfContactLandline.getValue()!=null)
			model.setPocLandline(pnbPointOfContactLandline.getValue());
		if(pnbPointOfContactCell.getValue()!=null)
			model.setPocCell(pnbPointOfContactCell.getValue());
		if(etbPointOfContactEmail.getValue()!=null)
			model.setPocEmail(etbPointOfContactEmail.getValue());
		if(addressComposite.getValue()!=null)
			model.setAddress(addressComposite.getValue());
		if(warehouse.getValue()!=null)
			model.setWarehouseName(warehouse.getValue());
		

		presenter.setModel(model);


	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(StorageLocation view) 
	{
		tbStorageLocationId.setValue(view.getCount()+" ");

		/**
		 * Date 20-04-2018 By vijay for orion pest locality load  
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
			setAddressDropDown();
		}
		/**
		 * ends here
		 */
		
		if(view.getBusinessUnitName()!=null)
			tbBranchName.setValue(view.getBusinessUnitName());
		if(view.getEmail()!=null)
			etbEmail.setValue(view.getEmail());
		if(view.getstatus()!=null)
			checkBoxStatus.setValue(view.getstatus());
		if(view.getLandline()!=null)
			pnbLandlinePhone.setValue(view.getLandline());
		if(view.getCellNumber1()!=null)
			pnbCellPhoneNo1.setValue(view.getCellNumber1());
		if(view.getCellNumber2()!=null)
			pnbCellPhoneNo2.setValue(view.getCellNumber2());
		if(view.getFaxNumber()!=null)
			pnbFaxNo.setValue(view.getFaxNumber());
		if(view.getPocName()!=null)
			tbPointOfContactName.setValue(view.getPocName());
		if(view.getPocLandline()!=null)
			pnbPointOfContactLandline.setValue(view.getPocLandline());
		if(view.getPocCell()!=null)
			pnbPointOfContactCell.setValue(view.getPocCell());
		if(view.getPocEmail()!=null)
			etbPointOfContactEmail.setValue(view.getPocEmail());
		if(view.getAddress()!=null)
			addressComposite.setValue(view.getAddress());
		if(view.getWarehouseName()!=null)
			warehouse.setValue(view.getWarehouseName());
		
		
		presenter.setModel(view);

	}

	 /**
		 * Date 20-04-2018 By vijay
		 * here i am refreshing address composite data if global list size greater than drop down list size
		 * because we are adding locality and city from an entity in view state to global list if doest not exist in global list 
		 */
		private void setAddressDropDown() {
			if(LoginPresenter.globalLocality.size()>addressComposite.locality.getItems().size()	){
				addressComposite.locality.setListItems(LoginPresenter.globalLocality);
			}
		}
		/**
		 * ends here 
		 */
		
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		   // search 
		

				if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
				{
					InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
					for(int k=0;k<menus.length;k++)
					{
						String text=menus[k].getText();
						if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
						{
							menus[k].setVisible(true); 
						}
						else
							menus[k].setVisible(false);  		   

					}
				}

				else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
				{
					InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
					for(int k=0;k<menus.length;k++)
					{
						String text=menus[k].getText();
						if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
							menus[k].setVisible(true); 

						else
							menus[k].setVisible(false);  		   
					}
				}

				else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
				{
					InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
					for(int k=0;k<menus.length;k++)
					{
						String text=menus[k].getText();
						if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
							menus[k].setVisible(true); 

						else  
							menus[k].setVisible(false);  		   
					}
				}
				
				AuthorizationHelper.setAsPerAuthorization(Screen.STORAGELOCATION,LoginPresenter.currentModule.trim());
			
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		tbStorageLocationId.setValue(count+""); 
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

	@Override
	public void onClick(ClickEvent event) { 
		
		
		if(event.getSource().equals(copyCompAddrs))
		{
			
			if(warehouse.getValue()!=null){
				WareHouse warhse=warehouse.getSelectedItem();
				addressComposite.clear();
				addressComposite.setValue(warhse.getAddress());
				
			}else{
				showDialogMessage("Please select warehouse!");
			}
			
		
		}
	}

	@Override
	public void clear() {
		super.clear();
		checkBoxStatus.setValue(true);
	}
	
	public void initializeWarehouse()
	{
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		temp=new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new WareHouse());
		
		
		warehouse.MakeLive(querry);

	}


	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		
	}


	public TextBox gettbStorageLocationName(){
		return tbBranchName ; 
	}
	
	public void setTbStorageLocationName(TextBox tbBranchName) 
	{
		this.tbBranchName = tbBranchName;
	}

}
