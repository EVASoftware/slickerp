package com.slicktechnologies.client.views.inventory.storagelocation;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.views.inventory.storagelocation.StorageLocationPresenter.StorageLocationPresenterTable;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
public class StorageLocationPresenterTableProxy extends StorageLocationPresenterTable {
	

	TextColumn<StorageLocation> getCountColumn;
	TextColumn<StorageLocation> getWareHouseNameColumn;
	TextColumn<StorageLocation> getStorageLocationNameColumn;  // added by Ajinkya
	TextColumn<StorageLocation> getStatusColumn;              
	
	public Object getVarRef(String varName)
	{    
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		if(varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if(varName.equals("getWareHouseNameColumn"))
			return this.getWareHouseNameColumn;
		if(varName.equals("getStorageLocationNameColumn"))  // added by Ajinkya
			return this.getStorageLocationNameColumn;      
		
		return null ;
	}
	public StorageLocationPresenterTableProxy()
	{
		super();
	}
	@Override public void createTable() {
		addColumngetCount();
		addColumngetWareHouseName();
		addColumngetStorageLocationName();
		addColumngetStatus();
		
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<StorageLocation>()
				{
			@Override
			public Object getKey(StorageLocation item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)  
	{
	}
	@Override
	public void applyStyle()
	{       
	}
	public void addColumnSorting()
	{
		addSortinggetCount();
		addSortinggetWareHouseName();
		addSortinggetStorageLocationName();
		addSortinggetStatus();
			
	}
	
	@Override public void addFieldUpdater() 
	{
	}
	
	protected void addSortinggetCount()
	{
		List<StorageLocation> list=getDataprovider().getList();
		columnSort=new ListHandler<StorageLocation>(list);
		columnSort.setComparator(getCountColumn, new Comparator<StorageLocation>()
				{
			@Override
			public int compare(StorageLocation e1,StorageLocation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		
		table.addColumnSortHandler(columnSort);
	
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<StorageLocation>()
				{
			@Override
			public String getValue(StorageLocation object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"Id");
				getCountColumn.setSortable(true);
	}
	
	
	
//	
	protected void addSortinggetStatus()
	{
		List<StorageLocation> list=getDataprovider().getList();
		columnSort=new ListHandler<StorageLocation>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<StorageLocation>()
				{
			@Override
			public int compare(StorageLocation e1,StorageLocation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getstatus()!=null && e2.getstatus()!=null){
						return e1.getstatus().compareTo(e2.getstatus());}
				}
				else{
					return 0;
					}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<StorageLocation>()  // added by Ajinkya
				{       
			@Override
			public String getValue(StorageLocation object)
			{
				if(object.getstatus())    
					return "Active";
				else
					return "In Active";
			}  
				};
				table.addColumn(getStatusColumn,"Status");
				getStatusColumn.setSortable(true);
	}
	
	protected void addSortinggetStorageLocationName()   // added by Ajinkya
	{
		List<StorageLocation> list=getDataprovider().getList();
		columnSort=new ListHandler<StorageLocation>(list);
		columnSort.setComparator(getStorageLocationNameColumn, new Comparator<StorageLocation>()
				{
			@Override
			public int compare(StorageLocation e1,StorageLocation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBusinessUnitName()!=null && e2.getBusinessUnitName()!=null){
						return e1.getBusinessUnitName().compareTo(e2.getBusinessUnitName());  //  getBussinessUnitName
						}
				}
				else{
					return 0;
					}
				return 0;  
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	               ////////////// // added by Ajinkya //  ///////////////////////////
	
	protected void addColumngetStorageLocationName()    
	{
		getStorageLocationNameColumn = new TextColumn<StorageLocation>()
				{
			@Override
			public String getValue(StorageLocation object)
			{
				return object.getBusinessUnitName()+"";
			}
				};
				table.addColumn(getStorageLocationNameColumn,"Storage Location");
				getStorageLocationNameColumn.setSortable(true);
	}
	
	private void addSortinggetWareHouseName() {

		List<StorageLocation> list=getDataprovider().getList();
		columnSort=new ListHandler<StorageLocation>(list);
		columnSort.setComparator(getWareHouseNameColumn, new Comparator<StorageLocation>()
				{
			@Override
			public int compare(StorageLocation e1,StorageLocation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getWarehouseName()!=null && e2.getWarehouseName()!=null)
					{
						return e1.getWarehouseName().compareTo(e2.getWarehouseName());
						}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
			
	}
	
	private void addColumngetWareHouseName() {
		
		getWareHouseNameColumn = new TextColumn<StorageLocation>()
				{
			@Override
			public String getValue(StorageLocation object)
			{
				return object.getWarehouseName()+"";
			}  
				};
				table.addColumn(getWareHouseNameColumn,"Warehouse Name");
				getWareHouseNameColumn.setSortable(true);
	
		
	}
	
  ////////////////////////////// end here /////////////////////////// 
}
