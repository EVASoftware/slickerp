package com.slicktechnologies.client.views.inventory.storagelocation;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import java.util.List;
import com.google.gwt.dom.client.Style.Unit;
import com.slicktechnologies.shared.common.inventory.StorageLocation;

public class StorageLocationTable extends SuperTable<StorageLocation> {
	TextColumn<StorageLocation> getstatusColumn;
	TextColumn<StorageLocation> getBusinessUnitNameColumn;
	TextColumn<StorageLocation> getCellNumber1Column;
	TextColumn<StorageLocation> getPocNameColumn;
	TextColumn<StorageLocation> getWareHouseNameColumn;
	TextColumn<StorageLocation> getPocCellColumn;
	TextColumn<StorageLocation> getCountColumn;

	public StorageLocationTable() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumnWarehouseName();
		addColumngetBusinessUnitName();
		addColumngetCellNumber1();
		addColumngetPocName();
		addColumngetPocCell();
		addColumngetstatus();
	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<StorageLocation>() {
			@Override
			public Object getKey(StorageLocation item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetBusinessUnitName();
		addSortingWareHouseName();
		addSortinggetCellNumber1();
		addSortinggetPocName();
		addSortinggetPocCell();
		addSortinggetstatus();
	}

	@Override
	public void addFieldUpdater() {
	}

	protected void addSortinggetCount() {
		List<StorageLocation> list = getDataprovider().getList();
		columnSort = new ListHandler<StorageLocation>(list);
		columnSort.setComparator(getCountColumn,
				new Comparator<StorageLocation>() {
					@Override
					public int compare(StorageLocation e1, StorageLocation e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCount() == e2.getCount()) {
								return 0;
							}
							if (e1.getCount() > e2.getCount()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<StorageLocation>() {
			@Override
			public String getValue(StorageLocation object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "ID");
		getCountColumn.setSortable(true);
		table.setColumnWidth(getCountColumn, 100, Unit.PX);
	}

	protected void addSortinggetBusinessUnitName() {
		List<StorageLocation> list = getDataprovider().getList();
		columnSort = new ListHandler<StorageLocation>(list);
		columnSort.setComparator(getBusinessUnitNameColumn,
				new Comparator<StorageLocation>() {
					@Override
					public int compare(StorageLocation e1, StorageLocation e2) {
						if (e1 != null && e2 != null) {
							if (e1.getBusinessUnitName() != null
									&& e2.getBusinessUnitName() != null) {
								return e1.getBusinessUnitName().compareTo(
										e2.getBusinessUnitName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetBusinessUnitName() {
		getBusinessUnitNameColumn = new TextColumn<StorageLocation>() {
			@Override
			public String getValue(StorageLocation object) {
				return object.getBusinessUnitName() + "";
			}
		};
		table.addColumn(getBusinessUnitNameColumn, "Name");
		getBusinessUnitNameColumn.setSortable(true);
		table.setColumnWidth(getBusinessUnitNameColumn, 100, Unit.PX);
	}

	protected void addSortingWareHouseName() {
		List<StorageLocation> list = getDataprovider().getList();
		columnSort = new ListHandler<StorageLocation>(list);
		columnSort.setComparator(getWareHouseNameColumn,
				new Comparator<StorageLocation>() {
					@Override
					public int compare(StorageLocation e1, StorageLocation e2) {
						if (e1 != null && e2 != null) {
							if (e1.getWarehouseName() != null
									&& e2.getWarehouseName() != null) {
								return e1.getWarehouseName().compareTo(
										e2.getWarehouseName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumnWarehouseName() {
		getWareHouseNameColumn = new TextColumn<StorageLocation>() {
			@Override
			public String getValue(StorageLocation object) {
				return object.getWarehouseName() + "";
			}
		};
		table.addColumn(getWareHouseNameColumn, "Warehouse Name");
		getWareHouseNameColumn.setSortable(true);
		table.setColumnWidth(getWareHouseNameColumn, 120, Unit.PX);
	}

	protected void addSortinggetCellNumber1() {
		List<StorageLocation> list = getDataprovider().getList();
		columnSort = new ListHandler<StorageLocation>(list);
		columnSort.setComparator(getCellNumber1Column,
				new Comparator<StorageLocation>() {
					@Override
					public int compare(StorageLocation e1, StorageLocation e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCellNumber1() == e2.getCellNumber1()) {
								return 0;
							}
							if (e1.getCellNumber1() > e2.getCellNumber1()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCellNumber1() {
		getCellNumber1Column = new TextColumn<StorageLocation>() {
			@Override
			public String getValue(StorageLocation object) {
				return object.getCellNumber1() + "";
			}
		};
		table.addColumn(getCellNumber1Column, "Phone");
		getCellNumber1Column.setSortable(true);
		table.setColumnWidth(getCellNumber1Column, 100, Unit.PX);
	}

	protected void addSortinggetPocName() {
		List<StorageLocation> list = getDataprovider().getList();
		columnSort = new ListHandler<StorageLocation>(list);
		columnSort.setComparator(getPocNameColumn,
				new Comparator<StorageLocation>() {
					@Override
					public int compare(StorageLocation e1, StorageLocation e2) {
						if (e1 != null && e2 != null) {
							if (e1.getPocName() != null
									&& e2.getPocName() != null) {
								return e1.getPocName().compareTo(
										e2.getPocName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetPocName() {
		getPocNameColumn = new TextColumn<StorageLocation>() {
			@Override
			public String getValue(StorageLocation object) {
				return object.getPocName() + "";
			}
		};
		table.addColumn(getPocNameColumn, "Contact Name");
		getPocNameColumn.setSortable(true);
		table.setColumnWidth(getPocNameColumn, 110, Unit.PX);
	}

	protected void addSortinggetPocCell() {
		List<StorageLocation> list = getDataprovider().getList();
		columnSort = new ListHandler<StorageLocation>(list);
		columnSort.setComparator(getPocCellColumn,
				new Comparator<StorageLocation>() {
					@Override
					public int compare(StorageLocation e1, StorageLocation e2) {
						if (e1 != null && e2 != null) {
							if (e1.getPocCell() == e2.getPocCell()) {
								return 0;
							}
							if (e1.getPocCell() > e2.getPocCell()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetPocCell() {
		getPocCellColumn = new TextColumn<StorageLocation>() {
			@Override
			public String getValue(StorageLocation object) {
				return object.getPocCell() + "";
			}
		};
		table.addColumn(getPocCellColumn, "Contact Phone");
		getPocCellColumn.setSortable(true);
		table.setColumnWidth(getPocCellColumn, 100, Unit.PX);
	}

	protected void addSortinggetstatus() {
		List<StorageLocation> list = getDataprovider().getList();
		columnSort = new ListHandler<StorageLocation>(list);
		columnSort.setComparator(getstatusColumn,
				new Comparator<StorageLocation>() {
					@Override
					public int compare(StorageLocation e1, StorageLocation e2) {
						if (e1 != null && e2 != null) {
							if (e1.getstatus() == e2.getstatus()) {
								return 0;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetstatus() {
		getstatusColumn = new TextColumn<StorageLocation>() {
			@Override
			public String getValue(StorageLocation object) {
				if (object.getstatus() == true)
					return "Active";
				else
					return "In Active";
			}
		};
		table.addColumn(getstatusColumn, "Status");
		getstatusColumn.setSortable(true);
		table.setColumnWidth(getstatusColumn, 100, Unit.PX);
	}

}
