package com.slicktechnologies.client.views.inventory.productinventoryviewlist;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.popups.StockUpdationPopup;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;

public class ProductInventoryViewListTable extends SuperTable<ProductInventoryViewDetails> implements ClickHandler {
	
	
	TextColumn<ProductInventoryViewDetails> getProductIdColumn;
	TextColumn<ProductInventoryViewDetails> getProdNameColumn;
	TextColumn<ProductInventoryViewDetails> getProdCodeColumn;
	TextColumn<ProductInventoryViewDetails> getWarehouseColumn;
	TextColumn<ProductInventoryViewDetails> getStorageLocationColumn;
	TextColumn<ProductInventoryViewDetails> getStorageBinColumn;
	TextColumn<ProductInventoryViewDetails> getAvailableQuantityColumn;
	TextColumn<ProductInventoryViewDetails> getMinQuantityColumn;
	TextColumn<ProductInventoryViewDetails> getMaxQuantityColumn;
	TextColumn<ProductInventoryViewDetails> getNormalLevelColumn;
	TextColumn<ProductInventoryViewDetails> getReorderLevelColumn;
	TextColumn<ProductInventoryViewDetails> getReorderQuantityColumn;
	
	/** date 22/02/2018 added by komal for product purchaseprice in hvac **/
	TextColumn<ProductInventoryViewDetails> getPurchasePriceColumn;
	TextColumn<ProductInventoryViewDetails> getSalesPriceColumn;
	TextColumn<ProductInventoryViewDetails> getProductCategory;
	ArrayList<ProductInfo> list =ProductInfoComposite.globalItemProdArray;	
	
	/**
	 * @author Vijay Chougule
	 * Des :- NBHC IM all stock maintained in ProductInventoryViewDetails
	 * so for Admin allow to update the stock.
	 */
	Column<ProductInventoryViewDetails,String> getupdateAvailableQty;
	StockUpdationPopup stockUpdationpopup = new StockUpdationPopup();
	int rowIndex;
	protected GWTCGlassPanel glassPanel;
	GenricServiceAsync gensync=GWT.create(GenricService.class);
	/**
	 * ends here
	 */
	public ProductInventoryViewListTable()
	{
		
		super();
		table.setWidth("100%");
		
		
		stockUpdationpopup.getLblOk().addClickHandler(this);
	}
	
	@Override public void createTable() {
		addColumngetProductId();
		addColumngetProdName();
		addColumngetProdCodeColumn();
		addColumngetWarehouse();
		addColumngetStorageLocation();
		addColumngetStorageBin();
		addColumngetAvailableQuantity();
		addColumngetMinQuantityColumn();
		addColumngetMaxQuantityColumn();
		addColumngetNormalLevelColumn();
		addColumngetReorderLevelColumn();
		addColumngetReorderQuantityColumn();
		/** date 22/02/2018 added by komal for product purchaseprice in hvac **/
		addColumngetSalesPriceColumn();
		addColumngetPurchasePriceColumn();
		addColumngetProductCategoryColumn();
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryViewDetails", "EnableShowStockUpdation")
				&& LoginPresenter.myUserEntity.getRole().getRoleName().trim().equals("ADMIN")){
			addColumngetStockUpdationBtnColumn();
		}

		}
	
	

	private void addColumngetProductCategoryColumn() {
		getProductCategory=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				if( object.getProductCategory()==null){
					return "";
				}
				else{
					return object.getProductCategory();
				}
			}
			};
		table.addColumn(getProductCategory,"Product Category");
		table.setColumnWidth(getProductCategory, 100, Unit.PX);
		getProductCategory.setSortable(true);


	}

	protected void addColumngetProductId()
	{
		getProductIdColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				if( object.getProdid()==0){
					return "";
				}
				else{
					return object.getProdid()+"";
				}
			}
			};
		table.addColumn(getProductIdColumn,"Product ID");
		table.setColumnWidth(getProductIdColumn, 100, Unit.PX);
		getProductIdColumn.setSortable(true);


	}
	
	protected void addColumngetProdName()
	{
		getProdNameColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getProdname();
			}
				};
		table.addColumn(getProdNameColumn,"Product Name");
		table.setColumnWidth(getProdNameColumn, 110, Unit.PX);
		getProdNameColumn.setSortable(true);

	}
	
	protected void addColumngetProdCodeColumn()
	{
		getProdCodeColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getProdcode();
			}
				};
		table.addColumn(getProdCodeColumn,"Product Code");
		table.setColumnWidth(getProdCodeColumn, 100, Unit.PX);
		getProdCodeColumn.setSortable(true);

		
	}
	
	protected void addColumngetWarehouse()
	{
		getWarehouseColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getWarehousename();
			}
				};
		table.addColumn(getWarehouseColumn,"Warehouse");
		table.setColumnWidth(getWarehouseColumn, 100, Unit.PX);
		getWarehouseColumn.setSortable(true);

	}
	
	protected void addColumngetStorageLocation()
	{
		getStorageLocationColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object. getStoragelocation();
			}
				};
		table.addColumn(getStorageLocationColumn,"Storage Location");
		table.setColumnWidth(getStorageLocationColumn, 120, Unit.PX);
		getStorageLocationColumn.setSortable(true);

	}
	
	protected void addColumngetStorageBin()
	{
		getStorageBinColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getStoragebin();
			}
				};
		table.addColumn(getStorageBinColumn,"Storage Bin");
		table.setColumnWidth(getStorageBinColumn, 100, Unit.PX);
		getStorageBinColumn.setSortable(true);

	}

	protected void addColumngetAvailableQuantity()
	{
		getAvailableQuantityColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getAvailableqty()+"";
			}
				};
		table.addColumn(getAvailableQuantityColumn,"Avl Qty");
		table.setColumnWidth(getAvailableQuantityColumn, 90, Unit.PX);
		getAvailableQuantityColumn.setSortable(true);

	}
	
	protected void addColumngetMinQuantityColumn()
	{
		getMinQuantityColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getMinqty()+"";
			}
				};
		table.addColumn(getMinQuantityColumn,"Min Qty");
		table.setColumnWidth(getMinQuantityColumn, 90, Unit.PX);
		getMinQuantityColumn.setSortable(true);

	}
	
	protected void addColumngetMaxQuantityColumn()
	{
		getMaxQuantityColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getMaxqty()+"";
			}
				};
		table.addColumn(getMaxQuantityColumn,"Max Qty");
		table.setColumnWidth(getMaxQuantityColumn, 90, Unit.PX);
		getMaxQuantityColumn.setSortable(true);

	}
	
	protected void addColumngetNormalLevelColumn()
	{
		getNormalLevelColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getNormallevel()+"";
			}
				};
		table.addColumn(getNormalLevelColumn,"Normal Lvl");
		table.setColumnWidth(getNormalLevelColumn, 90, Unit.PX);
		getNormalLevelColumn.setSortable(true);


	}
	
	
	protected void addColumngetReorderLevelColumn()
	{
		getReorderLevelColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getReorderlevel()+"";
			}
				};
		table.addColumn(getReorderLevelColumn,"Reorder Lvl");
		table.setColumnWidth(getReorderLevelColumn, 88, Unit.PX);
		getReorderLevelColumn.setSortable(true);


	}
	
	protected void addColumngetReorderQuantityColumn()
	{
		getReorderQuantityColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getReorderqty()+"";
			}
				};
		table.addColumn(getReorderQuantityColumn,"Reorder Qty");
		table.setColumnWidth(getReorderQuantityColumn, 88, Unit.PX);
		getReorderQuantityColumn.setSortable(true);

	}
	
	
	
	public void addColumnSorting()
	{
		addProductIdSorting();
		addProdNameSorting();
		addProdCodeSorting();
		addWarehouseSorting();
		addStorageLocationSorting();
		addStorageBinSorting();
		addAvailableQuantitySorting();
		addMinQuantitySorting();
		addMaxQuantitySorting();
		addNormalLevelSorting();
		addReorderLevelSorting();
		addReorderQuantitySorting();
		/** date 22/02/2018 added by komal for product purchaseprice in hvac **/
		getSalesPriceColumnSorting();
		getPurchasePriceColumnSorting();
		getProductCategoryColumnSorting();
	}

	private void getProductCategoryColumnSorting() {
		List<ProductInventoryViewDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryViewDetails>(list);
		Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

			@Override
			public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getProductCategory()!=null && e2.getProductCategory()!=null){
						return e1.getProductCategory().compareTo(e2.getProductCategory());}
				}
				  else{
				  return 0;}
				  return 0;
				}
		};
		columnSort.setComparator(getProductCategory, com);
		table.addColumnSortHandler(columnSort);
	}

	private void addProductIdSorting() {
		
		List<ProductInventoryViewDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryViewDetails>(list);
		Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

			@Override
			public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

				if(e1!=null && e2!=null){
					if(e1.getProdid()== e2.getProdid())
					{
						return 0;
					}
						if(e1.getProdid()> e2.getProdid())	
					{
						return 1;
					}
						else
					{
						return -1;
					}}else					
				
						return 0;
					}
			};
		columnSort.setComparator(getProductIdColumn, com);
		table.addColumnSortHandler(columnSort);
	}	
	
	private void addProdNameSorting() {
		List<ProductInventoryViewDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryViewDetails>(list);
		Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

			@Override
			public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getProdname()!=null && e2.getProdname()!=null){
						return e1.getProdname().compareTo(e2.getProdname());}
				}
				  else{
				  return 0;}
				  return 0;
				}
		};
		columnSort.setComparator(getProdNameColumn, com);
		table.addColumnSortHandler(columnSort);
	}

	
	private void addProdCodeSorting() {
		
		List<ProductInventoryViewDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryViewDetails>(list);
		Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

			@Override
			public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getProdcode()!=null && e2.getProdcode()!=null)
					{
					return e1.getProdcode().compareTo(e2.getProdcode());
					}
				}
				  else
				  {
				  return 0;
				  }
				  return 0;
				}
				};
		columnSort.setComparator(getProdCodeColumn,com);
		table.addColumnSortHandler(columnSort);
	}

	private void addWarehouseSorting() {
		List<ProductInventoryViewDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryViewDetails>(list);
		Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

			@Override
			public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getWarehousename()!=null && e2.getWarehousename()!=null){
						return e1.getWarehousename().compareTo(e2.getWarehousename());}
				}
				  else{
				  return 0;}
				  return 0;
				}
		};
		columnSort.setComparator(getWarehouseColumn, com);
		table.addColumnSortHandler(columnSort);
	}
	
	private void addStorageLocationSorting() {
		List<ProductInventoryViewDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryViewDetails>(list);
		Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

			@Override
			public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getStoragelocation()!=null && e2.getStoragelocation()!=null){
						return e1.getStoragelocation().compareTo(e2.getStoragelocation());}
				}
				  else{
				  return 0;}
				  return 0;
				}
		};
		columnSort.setComparator(getStorageLocationColumn, com);
		table.addColumnSortHandler(columnSort);
	}


private void addStorageBinSorting() {
	List<ProductInventoryViewDetails> list = getDataprovider().getList();
	columnSort = new ListHandler<ProductInventoryViewDetails>(list);
	Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

		@Override
		public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

			if(e1!=null && e2!=null)
			{
				if( e1.getStoragebin()!=null && e2.getStoragebin()!=null){
					return e1.getStoragebin().compareTo(e2.getStoragebin());}
			}
			  else{
			  return 0;}
			  return 0;
			}
	};
	columnSort.setComparator(getStorageBinColumn, com);
	table.addColumnSortHandler(columnSort);
}

private void addAvailableQuantitySorting() {
	
	List<ProductInventoryViewDetails> list = getDataprovider().getList();
	columnSort = new ListHandler<ProductInventoryViewDetails>(list);
	Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

		@Override
		public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

			if(e1!=null && e2!=null){
				if(e1.getAvailableqty()== e2.getAvailableqty())
				{
					return 0;
				}
					if(e1.getAvailableqty()> e2.getAvailableqty())	
				{
					return 1;
				}
					else
				{
					return -1;
				}}else					
			
			return 0;
		
			}
	};
	columnSort.setComparator(getAvailableQuantityColumn, com);
	table.addColumnSortHandler(columnSort);
}
	
private void addMinQuantitySorting() {
	
	List<ProductInventoryViewDetails> list = getDataprovider().getList();
	columnSort = new ListHandler<ProductInventoryViewDetails>(list);
	Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

		@Override
		public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

			if(e1!=null && e2!=null){
				if(e1.getMinqty()== e2.getMinqty())
				{
					return 0;
				}
					if(e1.getMinqty()> e2.getMinqty())	
				{
					return 1;
				}
					else
				{
					return -1;
				}}else					
			
			return 0;
		
			}
	};
	columnSort.setComparator(getMinQuantityColumn, com);
	table.addColumnSortHandler(columnSort);
}
  
private void addMaxQuantitySorting() {
	
	List<ProductInventoryViewDetails> list = getDataprovider().getList();
	columnSort = new ListHandler<ProductInventoryViewDetails>(list);
	Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

		@Override
		public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

			if(e1!=null && e2!=null){
				if(e1.getMaxqty()== e2.getMaxqty())
				{
					return 0;
				}
					if(e1.getMaxqty()> e2.getMaxqty())	
				{
					return 1;
				}
					else
				{
					return -1;
				}}else					
			
			return 0;
		
			}
	};
	columnSort.setComparator(getMaxQuantityColumn, com);
	table.addColumnSortHandler(columnSort);
}
 
private void addNormalLevelSorting() {
	
	List<ProductInventoryViewDetails> list = getDataprovider().getList();
	columnSort = new ListHandler<ProductInventoryViewDetails>(list);
	Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

		@Override
		public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

			if(e1!=null && e2!=null){
				if(e1.getNormallevel()== e2.getNormallevel())
				{
					return 0;
				}
					if(e1.getNormallevel()> e2.getNormallevel())	
				{
					return 1;
				}
					else
				{
					return -1;
				}}else					
			
			return 0;
		
			}
	};
	columnSort.setComparator(getNormalLevelColumn, com);
	table.addColumnSortHandler(columnSort);
}

private void addReorderLevelSorting() {
	
	List<ProductInventoryViewDetails> list = getDataprovider().getList();
	columnSort = new ListHandler<ProductInventoryViewDetails>(list);
	Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

		@Override
		public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

			if(e1!=null && e2!=null){
				if(e1.getReorderlevel()== e2.getReorderlevel())
				{
					return 0;
				}
					if(e1.getReorderlevel()> e2.getReorderlevel())	
				{
					return 1;
				}
					else
				{
					return -1;
				}}else					
			
			return 0;
		
			}
	};
	columnSort.setComparator(getReorderLevelColumn, com);
	table.addColumnSortHandler(columnSort);
}

private void addReorderQuantitySorting() {
	
	List<ProductInventoryViewDetails> list = getDataprovider().getList();
	columnSort = new ListHandler<ProductInventoryViewDetails>(list);
	Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

		@Override
		public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

			if(e1!=null && e2!=null){
				if(e1.getReorderqty()== e2.getReorderqty())
				{
					return 0;
				}
					if(e1.getReorderqty()> e2.getReorderqty())	
				{
					return 1;
				}
					else
				{
					return -1;
				}}else					
			
			return 0;
		
			}
	};
	columnSort.setComparator(getReorderQuantityColumn, com);
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetSalesPriceColumn()
{
	getSalesPriceColumn=new TextColumn<ProductInventoryViewDetails>()
		{
		@Override
		public String getValue(ProductInventoryViewDetails object)
		{   if(list.size()>0){
			   for(ProductInfo p : list){
				   if(p.getProductCode().equals(object.getProdcode())){
					   System.out.println("purchase price : " + p.getPurchasePrice());
					   object.setPrice(p.getProductPrice());
					   return object.getPrice()+"";
				   }
			   }
		     }
		return 0+"";
		}
		};
	table.addColumn(getSalesPriceColumn,"Product Price");
	table.setColumnWidth(getSalesPriceColumn, 100, Unit.PX);
	getSalesPriceColumn.setSortable(true);

	
}
protected void addColumngetPurchasePriceColumn()
{
	getPurchasePriceColumn=new TextColumn<ProductInventoryViewDetails>()
		{
		@Override
		public String getValue(ProductInventoryViewDetails object)
		{   if(list.size()>0){
			   for(ProductInfo p : list){
				   if(p.getProductCode().equals(object.getProdcode())){
					   object.setPurchasePrice(p.getPurchasePrice());
					   return object.getPurchasePrice()+"";
				   }
			   }
		     }
		return 0+"";
		}
			};
	table.addColumn(getPurchasePriceColumn,"Product Purchase Price");
	table.setColumnWidth(getPurchasePriceColumn, 100, Unit.PX);
	getPurchasePriceColumn.setSortable(true);

	
}
private void getSalesPriceColumnSorting() {
	
	List<ProductInventoryViewDetails> list = getDataprovider().getList();
	columnSort = new ListHandler<ProductInventoryViewDetails>(list);
	Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

		@Override
		public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

			if(e1!=null && e2!=null){
				if(e1.getPrice()== e2.getPrice())
				{
					return 0;
				}
					if(e1.getPrice()> e2.getPrice())	
				{
					return 1;
				}
					else
				{
					return -1;
				}}else					
			
			return 0;
		
			}
	};
	columnSort.setComparator(getSalesPriceColumn, com);
	table.addColumnSortHandler(columnSort);
}
private void getPurchasePriceColumnSorting() {
	
	List<ProductInventoryViewDetails> list = getDataprovider().getList();
	columnSort = new ListHandler<ProductInventoryViewDetails>(list);
	Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

		@Override
		public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

			if(e1!=null && e2!=null){
				if(e1.getPurchasePrice()== e2.getPurchasePrice())
				{
					return 0;
				}
					if(e1.getPurchasePrice()> e2.getPurchasePrice())	
				{
					return 1;
				}
					else
				{
					return -1;
				}}else					
			
			return 0;
		
			}
	};
	columnSort.setComparator(getPurchasePriceColumn, com);
	table.addColumnSortHandler(columnSort);
}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	private void addColumngetStockUpdationBtnColumn() {
		ButtonCell btncell = new ButtonCell();
		getupdateAvailableQty = new Column<ProductInventoryViewDetails, String>(btncell) {
			
			@Override
			public String getValue(ProductInventoryViewDetails object) {
				return "Update Available Qty";
			}
		};
		table.addColumn(getupdateAvailableQty,"");
		table.setColumnWidth(getupdateAvailableQty, 100, Unit.PX);
	
		getupdateAvailableQty.setFieldUpdater(new FieldUpdater<ProductInventoryViewDetails, String>() {
			
			@Override
			public void update(int index, ProductInventoryViewDetails object, String value) {
				rowIndex=index;
				stockUpdationpopup.showPopUp();
			}
		});
	}

	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource()==stockUpdationpopup.getLblOk()){
			getDataprovider().getList().get(rowIndex).setAvailableqty(stockUpdationpopup.getDbAvailableStockQty().getValue());
			ProductInventoryViewDetails entity=getDataprovider().getList().get(rowIndex);
			table.redraw();
			showWaitSymbol();
			gensync.save(entity, new AsyncCallback<ReturnFromServer>() {
				@Override
				public void onSuccess(ReturnFromServer result) {
					hideWaitSymbol();
					stockUpdationpopup.hidePopUp();
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					hideWaitSymbol();
					stockUpdationpopup.hidePopUp();
				}
			});
		}
	}
	
	public void showWaitSymbol(){
		glassPanel=new GWTCGlassPanel();
		glassPanel.show();
	}
	
	public void hideWaitSymbol(){
		glassPanel.hide();
	}
}
