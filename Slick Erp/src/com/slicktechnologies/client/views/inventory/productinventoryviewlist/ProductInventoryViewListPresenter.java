package com.slicktechnologies.client.views.inventory.productinventoryviewlist;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.productinventoryview.ProductInventoryViewForm;
import com.slicktechnologies.client.views.inventory.productinventoryview.ProductInventoryViewPresenter;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ProductInventoryViewListPresenter extends TableScreenPresenter<ProductInventoryViewDetails> {
	
	TableScreen<ProductInventoryViewDetails>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final GenricServiceAsync async=GWT.create(GenricService.class);
	
	public ProductInventoryViewListPresenter(TableScreen<ProductInventoryViewDetails> view,ProductInventoryViewDetails model) {
		super(view, model);
		form=view;
		if(UserConfiguration.getUserconfig().getUser().getRoleName().equalsIgnoreCase("ADMIN") && 
				!AppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryViewDetails", "EnableShowStockUpdation")){
			setTableSelectionOnService();
		}
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PRODUCTINVENTORYVIEWLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
	}
	
	public ProductInventoryViewListPresenter(TableScreen<ProductInventoryViewDetails> view,
			ProductInventoryViewDetails model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		if(UserConfiguration.getUserconfig().getUser().getRoleName().equalsIgnoreCase("ADMIN") &&
				!AppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryViewDetails", "EnableShowStockUpdation")){
			setTableSelectionOnService();
		}
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PRODUCTINVENTORYVIEWLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("")){
			reactTo();
		}
	}
	

	@Override
	public void reactOnDownload() {

		
		ArrayList<ProductInventoryViewDetails> invArray = new ArrayList<ProductInventoryViewDetails>();
		List<ProductInventoryViewDetails> list = (List<ProductInventoryViewDetails>)
				form.getSearchpopupscreen().getSupertable().getDataprovider().getList();

		invArray.addAll(list);

		csvservice.setInventoryListDetails(invArray, new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed" + caught);

					}

					@Override
					public void onSuccess(Void result) {

						String gwt = com.google.gwt.core.client.GWT
								.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 80;
						Window.open(url, "test", "enabled");

					}
				});
	
	}

	@Override
	public void reactOnEmail() {
		
		
	}

	/*
	 * Method template to make a new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new ProductInventoryViewDetails();
	}
	
	public static void initalize()
	{
		ProductInventoryViewListTable table=new ProductInventoryViewListTable();
		ProductInventoryViewListForm form=new ProductInventoryViewListForm(table);
		ProductInventoryViewSearchPopUp.staticSuperTable=table;
		
		ProductInventoryViewSearchPopUp searchPopUp=new ProductInventoryViewSearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		ProductInventoryViewListPresenter presenter=new ProductInventoryViewListPresenter(form, new ProductInventoryViewDetails());
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	public static void initalize(MyQuerry querry)
	{
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory View List",Screen.PRODUCTINVENTORYVIEWLIST);
		ProductInventoryViewListTable table=new ProductInventoryViewListTable();
		ProductInventoryViewListForm form=new ProductInventoryViewListForm(table);
		ProductInventoryViewSearchPopUp.staticSuperTable=table;
		ProductInventoryViewSearchPopUp searchPopUp=new ProductInventoryViewSearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		ProductInventoryViewListPresenter presenter=new ProductInventoryViewListPresenter(form, new ProductInventoryViewDetails(),querry);
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	/*
	 * Method template to set the query object in specific to search criteria and return the same
	 * @return MyQuerry object
	 */
	public MyQuerry getServiceQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new ProductInventoryViewDetails());
		return quer;
	}
	
	  private void reactTo()
	  {
	  }
	  
	  public void setTableSelectionOnService()
		 {
			 final NoSelectionModel<ProductInventoryViewDetails> selectionModelMyObj = new NoSelectionModel<ProductInventoryViewDetails>();
		     
		     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
		     {
		         @Override
		         public void onSelectionChange(SelectionChangeEvent event) 
		         {
		        	 //setContractRedirection();
		        	 final ProductInventoryViewDetails entity=selectionModelMyObj.getLastSelectedObject();
		        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
		        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Product Inventory View",Screen.PRODUCTINVENTORYVIEW);
		        	 final ProductInventoryViewForm form = ProductInventoryViewPresenter.initalize();
		        	 form.setToViewState();
		        	 AppMemory.getAppMemory().stickPnel(form);

		        	 
		        	 final MyQuerry querry=new MyQuerry();
		        	 Vector<Filter> filtervec=new Vector<Filter>();
		        	 Filter temp=null;
		        	 
		        	 temp=new Filter();
		        	 temp.setLongValue(entity.getCompanyId());
		        	 temp.setQuerryString("companyId");
		        	 filtervec.add(temp);
		        	 

		        	 temp=new Filter();
		        	 temp.setIntValue(entity.getProdInvViewCount());
		        	 temp.setQuerryString("count");
		        	 filtervec.add(temp);
		        	 
		        	 querry.setFilters(filtervec);
		        	 querry.setQuerryObject(new ProductInventoryView());
		        	 
		        	 
		        	 form.showWaitSymbol();
						
		        	 Timer timer=new Timer() 
		        	 {
		 				@Override
		 				public void run() {
		 					
		 					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

		 						@Override
		 						public void onFailure(Throwable caught) {
		 							form.showDialogMessage("An Unexpected Error occured !");
		 							form.hideWaitSymbol();
		 						}
		 						@Override
		 						public void onSuccess(ArrayList<SuperModel> result) {
		 							
		 							
		 							for(SuperModel pmodel:result)
		 							{
		 								ProductInventoryView pgi=(ProductInventoryView)pmodel;
		 								form.updateView(pgi);
		 							}
		 						}
		 					});
		 					 form.hideWaitSymbol();
		 				}
		 			};
		             timer.schedule(3000); 
		          }
		     };
		     // Add the handler to the selection model
		     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
		     // Add the selection model to the table
		     form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
		     
			 
			 
		 }

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}
	  

}
