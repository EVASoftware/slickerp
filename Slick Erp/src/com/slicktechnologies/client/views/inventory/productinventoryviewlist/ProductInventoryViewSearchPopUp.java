package com.slicktechnologies.client.views.inventory.productinventoryviewlist;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.inventory.productinventoryview.ProductInventoryViewForm;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class ProductInventoryViewSearchPopUp extends SearchPopUpScreen<ProductInventoryViewDetails> implements ChangeHandler {
	
	ProductInfoComposite productInfoComposite;

	ObjectListBox<WareHouse> oblWarehouse;
	ListBox oblStorageLocation;
	ListBox oblStorageBin;
	
	/**
	 * @author Amol , Date : 16-08-2019
	 * Adding product category search
	 */
	ObjectListBox<Category> olbcProductCategory;
	
	
	
	
	ProductInventoryViewForm form;
	ArrayList<WareHouse> wareHouseList;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	public ProductInventoryViewSearchPopUp() {
		super();
		createGui();
	}
	
	public ProductInventoryViewSearchPopUp(boolean b) {
		super(b);
		createGui();
	}
	
	private void initializeWidget(){
		
		productInfoComposite = AppUtility.initiateSalesProductComposite(new SuperProduct());
		oblWarehouse= new ObjectListBox<WareHouse>();
		AppUtility.initializeWarehouse(oblWarehouse);
		oblWarehouse.addChangeHandler(this);
		
		oblStorageLocation = new ListBox();
		oblStorageLocation.addItem("--SELECT--");
		oblStorageLocation.addChangeHandler(this);
		
		oblStorageBin = new ListBox();
		oblStorageBin.addItem("--SELECT--");
		
//		/**
//		 * Rahul wrote this method for loading warehouse in both with branch and without 
//		 * branch level restriction 
//		 * 	Method loadWareHouseConfig() returns warehouse list 
//		 * date : 19
//		 */
//		loadWareHouseConfig();
		
		olbcProductCategory=new ObjectListBox<Category>();
		Category.makeItemCategoryListBoxLive(olbcProductCategory);
		
	
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
//	/**
//	 * Description : @parameter:null
//	 * 				 @return : null
//	 *				 This method will load warehouse according to branches assigned to employee and will load all branches in case of admin
//	 *Creation Date: 14-Sept-2016 
//	 *Project : EVA ERP
//	 *Required for : Everyone(This was initially created for NBHC)
//	 *Created by : Rahul Verma
//	 *
//	 */
//	private void loadWareHouseConfig() {
//		
//		wareHouseList = new ArrayList<WareHouse>();
//		form = new ProductInventoryViewForm();
//		GenricServiceAsync async = GWT.create(GenricService.class);
//
//		MyQuerry querry = new MyQuerry();
//		Company c = new Company();
//		Vector<Filter> filtervec = new Vector<Filter>();
//		Filter filter = null;
//		filter = new Filter();
//		filter.setQuerryString("companyId");
//		filter.setLongValue(c.getCompanyId());
//		filtervec.add(filter);
//		filter = new Filter();
//		filter.setQuerryString("processName");
//		filter.setStringValue("Branch");
//		filtervec.add(filter);
//
//		filter = new Filter();
//		filter.setQuerryString("processList.status");
//		filter.setBooleanvalue(true);
//		filtervec.add(filter);
//
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new ProcessConfiguration());
//
//		async.getSearchResult(querry,
//				new AsyncCallback<ArrayList<SuperModel>>() {
//
//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//
//					}
//
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						// TODO Auto-generated method stub
//						List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();
//						if (result.size() == 0) {
//							oblWarehouse.MakeLive(new MyQuerry(
//									new Vector<Filter>(), new WareHouse()));
//						} else {
//							for (SuperModel model : result) {
//								ProcessConfiguration processConfig = (ProcessConfiguration) model;
//								processList.addAll(processConfig
//										.getProcessList());
//							}
//							for (int i = 0; i < processList.size(); i++) {
//								if (processList
//										.get(i)
//										.getProcessType()
//										.trim()
//										.equalsIgnoreCase(
//												"BranchLevelRestriction")
//										&& processList.get(i).isStatus() == true) {
//									if (LoginPresenter.myUserEntity.getRole()
//											.equals("ADMIN")) {
//										oblWarehouse.MakeLive(new MyQuerry(
//													new Vector<Filter>(), new WareHouse()));
//										} else {
//										// wareHouseList = new
//										// ArrayList<WareHouse>();
//										ArrayList<EmployeeBranch> branchesOfEmployee = new ArrayList<EmployeeBranch>();
//										// String fullname =
//										// LoginPresenter.loggedInUser;
//										for (Employee emp : LoginPresenter.globalEmployee) {
//											if (emp.getCount() == UserConfiguration
//													.getInfo().getEmpCount()) {
//												if (emp.getEmpBranchList() != null
//														&& emp.getEmpBranchList()
//																.size() != 0) {
//													branchesOfEmployee.addAll(emp
//															.getEmpBranchList());
//													break;
//												} else {
//													EmployeeBranch emplBranch = new EmployeeBranch();
//													emplBranch.setBranchName(emp
//															.getBranchName());
//													branchesOfEmployee
//															.add(emplBranch);
//													break;
//												}
//											}
//										}
//										System.out
//												.println("branchesOfEmployee:::::::::::"
//														+ branchesOfEmployee
//																.size());
//										ArrayList<String> branchNameList = new ArrayList<String>();
//										branchNameList = form
//												.getBranchNameList(branchesOfEmployee);
//										System.out
//										.println("branchNameList:::::::::::"
//												+ branchNameList
//														.size());
//										if (getBranchWiseWareHouse(branchNameList)) {
//											Timer timer = new Timer() {
//
//												@Override
//												public void run() {
//													// TODO Auto-generated
//													// method stub
//													System.out.println("Warehouse List"
//															+ wareHouseList
//																	.size());
//													oblWarehouse
//															.setListItems(wareHouseList);
//												}
//											};
//											timer.schedule(3000);
//										}
//									}
//								} else {
//									oblWarehouse.MakeLive(new MyQuerry(
//											new Vector<Filter>(), new WareHouse()));
//								}
//							}
//						}
//
//					}
//				});
//	}
	
	
//	/**
//	 * Ends here 
//	 */
//	

	/**
	 * 
	 * Description : @param branches of Employee who is Logged In.
	 *
	 * @return This will return boolean value(if executed without exception)
	 *         Creation Date: 14-Sept-2016 Project : EVA ERP Required for:
	 *         Everyone.(This is created for branch level restriction. Created
	 *         by : Rahul Verma
	 */
	public boolean getBranchWiseWareHouse(ArrayList<String> branchNameList) {
		// TODO Auto-generated method stub

		GenricServiceAsync async = GWT.create(GenricService.class);

		// for (int i = 0; i < branchesOfEmployee.size(); i++) {
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("branchdetails.branchName IN");
		filter.setList(branchNameList);
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new WareHouse());
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						System.out.println("Result Size:::::::::::::"
								+ result.size());
						if (result.size() != 0) {
							for (SuperModel model : result) {
								System.out.println("Inside On Success loop");
								WareHouse warehouseEntity = (WareHouse) model;
								wareHouseList.add(warehouseEntity);
								System.out
										.println("warehouselist after adding::::"
												+ wareHouseList.size());
								// System.out.println("BIN :" +
								// entity.getBinName());
							}
						}
					}
				});
		// }
		// System.out.println("Warehouselist::::::::::"+wareHouseList.size());
		return true;
	}

	/**
	 * END
	 */
	
	

	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder builder;

		builder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite = builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
					builder = new FormFieldBuilder("Warehouse",oblWarehouse);
					FormField InvWarehouse= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
					builder = new FormFieldBuilder("Storage Location",oblStorageLocation);
					FormField StorageLoc= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
					builder = new FormFieldBuilder("Storage Bin",oblStorageBin);
					FormField StorageBin= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
					
					builder = new FormFieldBuilder("Product Category",olbcProductCategory);
					FormField folbcProductCategory= builder.setMandatory(false).setMandatoryMsg("Product Category is Mandatory!").setRowSpan(0).setColSpan(0).build();
					
				FormField[][] formfield = { 
						{ fproductInfoComposite },
						{InvWarehouse,StorageLoc,StorageBin,folbcProductCategory}
				};

		this.fields = formfield;
	}
	
	
	
	
	@Override
	public MyQuerry getQuerry() {
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if (!productInfoComposite.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
			temp.setQuerryString("prodid");
			filtervec.add(temp);
		}
		
		if(oblWarehouse.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(oblWarehouse.getValue().trim());
			temp.setQuerryString("warehousename");
			filtervec.add(temp);
		}
		if (oblStorageLocation.getSelectedIndex()!=0) {
			temp = new Filter();
			temp.setStringValue(oblStorageLocation.getValue(oblStorageLocation.getSelectedIndex()).trim());
			temp.setQuerryString("storagelocation");
			filtervec.add(temp);
		}
		
		if (oblStorageBin.getSelectedIndex()!=0) {
			temp = new Filter();
			temp.setStringValue(oblStorageBin.getValue(oblStorageBin.getSelectedIndex()).trim());
			temp.setQuerryString("storagebin");
			filtervec.add(temp);
		}
		
		if (olbcProductCategory.getSelectedIndex()!=0) {
			temp = new Filter();
			temp.setStringValue(olbcProductCategory.getValue().trim());
			temp.setQuerryString("productCategory");
			filtervec.add(temp);
		}
		
		
		
		
		
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProductInventoryViewDetails());
		return querry;

		
	}

	@Override
	public boolean validate() {
		
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(oblWarehouse.getSelectedIndex()==0)
		{
			showDialogMessage("Select WareHouse");
			return false;
		}
		}
			return true;	
	}

	@Override
	public void onChange(ChangeEvent event) {
		
		if(event.getSource().equals(oblWarehouse)){
			retriveStorageLocation();
		}
		if(event.getSource().equals(oblStorageLocation)){
			retriveStorageBin();
		}
	}
	

	private void retriveStorageLocation(){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("warehouseName");
		filter.setStringValue(oblWarehouse.getValue(oblWarehouse.getSelectedIndex()));
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new StorageLocation());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				oblStorageLocation.clear();
				oblStorageBin.clear();
				oblStorageLocation.addItem("--SELECT--");
				oblStorageBin.addItem("--SELECT--");
				for (SuperModel model : result) {
					StorageLocation entity = (StorageLocation) model;
//					storageList.add(entity.getBusinessUnitName());
					oblStorageLocation.addItem(entity.getBusinessUnitName());
					System.out.println("Location :"+entity.getBusinessUnitName());
				}
				
			}
		});
	}
	
	// ****************************** Retrieving Storage Bin From Storage location **********************************
	
	private void retriveStorageBin(){
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("storagelocation");
		filter.setStringValue(oblStorageLocation.getValue(oblStorageLocation.getSelectedIndex()));
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				oblStorageBin.clear();
				oblStorageBin.addItem("--SELECT--");
				for (SuperModel model : result) {
					Storagebin entity = (Storagebin) model;
					oblStorageBin.addItem(entity.getBinName());
					System.out.println("BIN :"+entity.getBinName());
				}
				
			}
		});
	}
	

}
