package com.slicktechnologies.client.views.inventory.productinventoryviewlist;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ProductInventoryViewListForm extends TableScreen<ProductInventoryViewDetails> implements ClickHandler {
	
	
	
	public ProductInventoryViewListForm  (SuperTable<ProductInventoryViewDetails> superTable) {
		super(superTable);
		createGui();
	}
	
	
	@Override
	public void createScreen() 
	{
		//Token to initialize the processlevel menus.
		// <ReplasbleProcesslevelDeclarationLine>	
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(ProductInventoryViewDetails model)
	{
		//<ReplasableUpdateModelFromViewCode>
	}

	@SuppressWarnings("unused")
	private void initalizeWidget()
	{
		//<ReplasbleVariableInitalizerBlock>
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(ProductInventoryViewDetails view) 
	{
		//<ReplasableUpdateViewFromModelCode>
	}
	
	
	

	/**
	 * Toggles the app header bar menus as per screen state
	 */
	public void toggleAppHeaderBarMenu() {
		if(superTable!=null&&superTable.getListDataProvider().getList()!=null){
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Rows:")){
					menus[k].setText("Rows:"+superTable.getListDataProvider().getList().size());	
					menus[k].getElement().setId("addlabel2");
				}
			}
		}
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Search")||text.equals("Download")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		  		
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Download")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Search")||text.equals("Download")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:")
						)
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PRODUCTINVENTORYVIEWLIST,LoginPresenter.currentModule.trim());
	}


	@Override
	public void retriveTable(MyQuerry querry) {
		superTable.connectToLocal();
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for(SuperModel model:result)
					superTable.getListDataProvider().getList().add((ProductInventoryViewDetails) model);
				superTable.getTable().setVisibleRange(0,result.size());
				
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
			caught.printStackTrace();
				
			}
		});
		
	}


	@Override
	public void onClick(ClickEvent event) {
		
	}



}
