package com.slicktechnologies.client.views.inventory.inspection;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.views.inventory.recievingnote.InventoryLocationPopUp;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.deliverynote.ShippingSteps;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.InspectionDetails;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryProducts;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;

public class InspectionProductTable extends SuperTable<InspectionDetails> implements ClickHandler {
	final static GenricServiceAsync async = GWT.create(GenricService.class);
	InventoryLocationPopUp invLoc=new InventoryLocationPopUp();
	InspectionPopUp insPopUp=new InspectionPopUp();
	PopupPanel panel=new PopupPanel(true);
	double rejectedQty=0;
	
	int rowIndex;
	NumberFormat nf = NumberFormat.getFormat("0.00");
	
	TextColumn<InspectionDetails> columnProductID;
	TextColumn<InspectionDetails> columnProductName;
	TextColumn<InspectionDetails> columnProductCode;
	TextColumn<InspectionDetails> columnProductCategory;
	TextColumn<InspectionDetails> productQuantity;
	TextColumn<InspectionDetails> rejectedQuantity;
	TextColumn<InspectionDetails> grnQuantity;
	
	Column<InspectionDetails, String> columnProductQuantity;
	Column<InspectionDetails, String> columnProductPrice;
	
	TextColumn<InspectionDetails> columnunitOfmeasurement;
	
	public  ArrayList<String> warehouseList;
	public  ArrayList<String> binList;
	public  ArrayList<String> storagelocList;
	public  ArrayList<Storagebin> storagelocList1;
	
	Column<InspectionDetails, String> warehouseColumn;
	TextColumn<InspectionDetails> warehouseViewColumn;
	
	Column<InspectionDetails, String> storagebin;
	Column<InspectionDetails, String> columnviewBin;
	
	Column<InspectionDetails, String> storageLoc;
	TextColumn<InspectionDetails> columnviewLoc;
	
	TextColumn<InspectionDetails> reasonForRejectView;
	Column<InspectionDetails, String> reasonForReject;

	Column<InspectionDetails, String> deleteColumn;
	Column<InspectionDetails, String> addColumn;
	Column<InspectionDetails, String> inspectColumn;

	
	public InspectionProductTable() {
		invLoc.getAddButton().addClickHandler(this);
		invLoc.getCancelButton().addClickHandler(this);
		insPopUp.getAddInspection().addClickHandler(this);
		insPopUp.getBtnOk().addClickHandler(this);
		insPopUp.getBtnCancel().addClickHandler(this);
	}

	
	
	@Override
	public void createTable() {
		
		addColumnProductName();
		addColumnUnit();
		addColumnProductGRNQuantity();
		addColumnProductPrice();
		addColumnProductQuantity();
		addColumnProductRejectedQuantity();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		addColumnProductCategory();
		addColumnProductCode();
		addColumnProductID();
		addColumnRemark();
		createColumnAddColumn();
		createColumnInspectColumn();
		createColumndeleteColumn();
		addFieldUpdater();
	}

	public void addeditColumn() {
		addColumnProductID();
		addColumnProductName();
		addColumnProductCode();
		addColumnProductCategory();
		addColumnProductPrice();
		addColumnProductGRNQuantity();
		addColumnProductQuantity();
		addColumnProductRejectedQuantity();
		addColumnUnit();
		createColumnInspectColumn();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		addColumnRemark();
		if(InspectionPresenter.grnId==0){
			createColumnAddColumn();
			createColumndeleteColumn();
		}
		
		
		addFieldUpdater();
	}

	public void addViewColumn() {
		addColumnProductID();
		addColumnProductName();
		addColumnProductCode();
		addColumnProductCategory();
		addColumnProductPrice();
		addColumnProductGRNQuantity();
		viewColumnProductQuantity();
		addColumnProductRejectedQuantity();
		addColumnUnit();
		createColumnInspectColumn();
		createFieldUpdaterInspect();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		viewRemark();
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterprodQtyColumn();
		updateColumnProdRemarks();
		createFieldUpdaterAdd();
		createFieldUpdaterInspect();
		createFieldUpdaterDelete();
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true){
			addeditColumn();
			insPopUp.getBtnOk().setEnabled(true);
			insPopUp.getAddInspection().setEnabled(true);
			insPopUp.getInspectionListTable().setEnable(true);
			insPopUp.getOlbInspectionMethod().setEnabled(true);
			insPopUp.getOlbinspectionchecklist().setEnabled(true);
		}
		if (state == false){
			addViewColumn();
			insPopUp.getBtnOk().setEnabled(false);
			insPopUp.getAddInspection().setEnabled(false);
			insPopUp.getInspectionListTable().setEnable(false);
			insPopUp.getOlbInspectionMethod().setEnabled(false);
			insPopUp.getOlbinspectionchecklist().setEnabled(false);
		}
	}
	
	/******************************************** Warehouse *****************************************************/
	public void retriveWarehouse() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		final MyQuerry query = new MyQuerry();
		query.setQuerryObject(new WareHouse());
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				warehouseList=new ArrayList<String>();
				warehouseList.add("SELECT");
				for (SuperModel model : result) {
					WareHouse entity = (WareHouse) model;
					warehouseList.add(entity.getBusinessUnitName());
					System.out.println("size" + warehouseList.size());
				}
			}
		});
	}


	/************************* storage location ***************************************/

	public void retriveStorageLoc() {
		
		System.out.println("in retrive storage loc");
		final GenricServiceAsync service = GWT.create(GenricService.class);
		final MyQuerry query = new MyQuerry();
		
		Vector<Filter> filtervalue = new Vector<Filter>();
		
		query.setFilters(filtervalue);
		query.setQuerryObject(new StorageLocation());
		System.out.println("service" + service);
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				storagelocList=new ArrayList<String>();
				storagelocList.add("SELECT");
				for (SuperModel model : result) {
					StorageLocation entity = (StorageLocation) model;
					storagelocList.add(entity.getBusinessUnitName());
					System.out.println("size" + storagelocList.size());
				}
			}
		});
	}
	
	/*************************** storage bin ******************************************/

	public void retriveStorageBin() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				binList=new ArrayList<String>();
				storagelocList1=new ArrayList<Storagebin>();
				binList.add("SELECT");
				for (SuperModel model : result) {
					Storagebin entity = (Storagebin) model;
					storagelocList1.add(entity);
					binList.add(entity.getBinName());
				}
				addEditColumnWarehouse();
				addEditColumnStorageLocation();
				addEditColumnStorageBin();
				createColumnAddColumn();
				createColumndeleteColumn();
				addFieldUpdater();
			}
		});
	}
	/*******************************************************************************************/
	// Editable Warehouse
		public void addEditColumnWarehouse() {
			SelectionCell locationselection = new SelectionCell(warehouseList);
			warehouseColumn = new Column<InspectionDetails, String>(locationselection) {
				@Override
				public String getValue(InspectionDetails object) {
					System.out.println("before==" + object.getWarehouseLocation());
					if (object.getWarehouseLocation() != null) {
						System.out.println("loc==" + object.getWarehouseLocation());
						return object.getWarehouseLocation();
					} else
						return "N.A";
				}
			};
			table.addColumn(warehouseColumn, "Warehouse");
			table.setColumnWidth(warehouseColumn,180,Unit.PX);
		}
	// Editable Storage Location
	public void addEditColumnStorageLocation() {
		System.out.println("in edit column storage loc");
		SelectionCell locationselection = new SelectionCell(storagelocList);
		storageLoc = new Column<InspectionDetails, String>(locationselection) {
			@Override
			public String getValue(InspectionDetails object) {
				System.out.println("before==" + object.getStorageLoc());
				if (object.getStorageLoc() != null) {
					System.out.println("loc==" + object.getStorageLoc());
				//	storageLocation=object.getStorageLoc();
					return object.getStorageLoc();
				} else
					return "N.A";
			}
		};
		table.addColumn(storageLoc, "Storage Loc");
		table.setColumnWidth(storageLoc,153,Unit.PX);
	}

	// Storage Bin Selection List Column
		public void addEditColumnStorageBin() {
			SelectionCell employeeselection = new SelectionCell(binList);
			storagebin = new Column<InspectionDetails, String>(employeeselection) {
				@Override
				public String getValue(InspectionDetails object) {
					if (object.getStorageBin() != null) {
						return object.getStorageBin();
					} else
						return "N/A";
				}
			};
			table.addColumn(storagebin, "Storage Bin");
			table.setColumnWidth(storagebin,153,Unit.PX);
		}
		
		
		/*****************************************************************/
		
		// Warehouse Update Method
				protected void createFieldUpdaterWarehouse() {
					warehouseColumn.setFieldUpdater(new FieldUpdater<InspectionDetails, String>() {
						@Override
						public void update(int index, InspectionDetails object, String value) {
							try {
								String val1 = (value.trim());
								System.out.println("Warehouse   ====" + val1+ " ===" + value);
								object.setWarehouseLocation(val1);
								RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
							} catch (NumberFormatException e) {
							}
							table.redrawRow(index);
						}
					});
				}
				
		// Storage Location Update Method
		protected void createFieldUpdaterstorageLocation() {
			storageLoc.setFieldUpdater(new FieldUpdater<InspectionDetails, String>() {
				@Override
				public void update(int index, InspectionDetails object, String value) {
					try {
						String val1 = (value.trim());
						System.out.println("STORAGE LOCATION   ====" + val1+ " ===" + value);
						
						if(val1!=null){
							System.out.println("lenovo   "+storagelocList1.size());
						for(int i=0;i<storagelocList1.size();i++){
							System.out.println("lenovo 5  ");
							binList.clear();
							if(storagelocList1.get(i).getStoragelocation().trim().equals(val1.trim())){
								System.out.println("lenovo 9  ");
								binList.add(storagelocList1.get(i).getBinName().trim());
							}
						}
						}
						object.setStorageLoc(val1);
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					} catch (NumberFormatException e) {
					}
					table.redrawRow(index);
				}
			});
		}

		// Storage Bin Update Method
		protected void createFieldUpdaterstorageBin() {
			storagebin.setFieldUpdater(new FieldUpdater<InspectionDetails, String>() {
				@Override
				public void update(int index, InspectionDetails object, String value) {
					try {
						String val1 = (value.trim());
						object.setStorageBin(val1);
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
						
					} catch (NumberFormatException e) {
					}
					table.redrawRow(index);
				}
			});
		}
		
		
		private void viewRemark() {
			reasonForRejectView=new TextColumn<InspectionDetails>() {
				@Override
				public String getValue(InspectionDetails object) {
					if(object.getReasonForReject()!=null){
					return object.getReasonForReject();
					}
					else{
						return "";
					}
				}
			};
			table.addColumn(reasonForRejectView,"#Remark");
			table.setColumnWidth(reasonForRejectView,180,Unit.PX);
		}
		
		// Product Remark  Editable
		private void addColumnRemark() {
			EditTextCell editCell=new EditTextCell();
			reasonForReject=new Column<InspectionDetails,String>(editCell) {
				@Override
				public String getValue(InspectionDetails object) {
					if(object.getReasonForReject()!=null){
						return object.getReasonForReject();
						}
						else{
							return "";
						}
				}
			};
			table.addColumn(reasonForReject,"#Remark");
			table.setColumnWidth(reasonForReject,180,Unit.PX);
		}
		
		
		private void updateColumnProdRemarks() {

			reasonForReject.setFieldUpdater(new FieldUpdater<InspectionDetails, String>() {
				@Override
				public void update(int index, InspectionDetails object,String value) {
					try {
						object.setReasonForReject(value);
					} 
					catch (NumberFormatException e) {
					}
					table.redrawRow(index);
				}
			});
		}

	/*************************************** Column View ***********************************************/

	public void addColumnProductID() {
		columnProductID = new TextColumn<InspectionDetails>() {
			@Override
			public String getValue(InspectionDetails object) {
				return object.getProductID() + "";
			}
		};
		table.addColumn(columnProductID, "ID");
		table.setColumnWidth(columnProductID,90,Unit.PX);
	}

	public void addColumnProductCode() {
		columnProductCode = new TextColumn<InspectionDetails>() {
			@Override
			public String getValue(InspectionDetails object) {
				return object.getProductCode();
			}
		};
		table.addColumn(columnProductCode, "Code");
		table.setColumnWidth(columnProductCode,90,Unit.PX);
	}

	public void addColumnProductName() {
		columnProductName = new TextColumn<InspectionDetails>() {
			@Override
			public String getValue(InspectionDetails object) {
				return object.getProductName();
			}
		};
		table.addColumn(columnProductName, "Name");
		table.setColumnWidth(columnProductName,120,Unit.PX);
	}

	public void addColumnProductCategory() {
		columnProductCategory = new TextColumn<InspectionDetails>() {
			@Override
			public String getValue(InspectionDetails object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(columnProductCategory, "Category");
		table.setColumnWidth(columnProductCategory,120,Unit.PX);
	}

	public void addColumnProductQuantity() {
		EditTextCell editCell = new EditTextCell();
		columnProductQuantity = new Column<InspectionDetails, String>(editCell) {
			@Override
			public String getValue(InspectionDetails object) {
				return object.getAcceptedQty() + "";
			}
		};
		table.addColumn(columnProductQuantity, "#Acpt.Qty.");
		table.setColumnWidth(columnProductQuantity,90,Unit.PX);
	}

	// Read Only Product Quantity
	private void viewColumnProductQuantity() {
		productQuantity = new TextColumn<InspectionDetails>() {
			@Override
			public String getValue(InspectionDetails object) {
				return object.getAcceptedQty() + "";
			}
		};
		table.addColumn(productQuantity, "#Acpt.Qty.");
		table.setColumnWidth(productQuantity,90,Unit.PX);
	}
	
	// Read Only Product Quantity
	private void addColumnProductGRNQuantity() {
		grnQuantity = new TextColumn<InspectionDetails>() {
			@Override
			public String getValue(InspectionDetails object) {
				return object.getProductQuantity() + "";
			}
		};
		table.addColumn(grnQuantity, "GRN Quantity");
		table.setColumnWidth(grnQuantity,110,Unit.PX);
	}

	private void addColumnProductRejectedQuantity() {
		rejectedQuantity = new TextColumn<InspectionDetails>() {
			@Override
			public String getValue(InspectionDetails object) {
				return object.getRejectedQty() + "";
			}
		};
		table.addColumn(rejectedQuantity, "Rejected Qty.");
		table.setColumnWidth(rejectedQuantity,117,Unit.PX);
	}
	
	public void addColumnUnit() {
		columnunitOfmeasurement = new TextColumn<InspectionDetails>() {
			@Override
			public String getValue(InspectionDetails object) {
				return object.getUnitOfmeasurement();
			}
		};
		table.addColumn(columnunitOfmeasurement, "UOM");
		table.setColumnWidth(columnunitOfmeasurement,81,Unit.PX);
	}

	public void addColumnProductPrice() {
		EditTextCell prodprice = new EditTextCell();
		columnProductPrice = new Column<InspectionDetails, String>(prodprice) {
			@Override
			public String getValue(InspectionDetails object) {
				return object.getProdPrice() + "";
			}
		};
		table.addColumn(columnProductPrice, "Price");
		table.setColumnWidth(columnProductPrice,90,Unit.PX);
	}

	public void viewstorageBin() {
		columnviewBin = new TextColumn<InspectionDetails>() {
			@Override
			public String getValue(InspectionDetails object) {
				return object.getStorageBin();
			}
		};
		table.addColumn(columnviewBin, "Storage Bin");
		table.setColumnWidth(columnviewBin,153,Unit.PX);
	}

	public void viewWarehouse() {
		warehouseViewColumn = new TextColumn<InspectionDetails>() {
			@Override
			public String getValue(InspectionDetails object) {
				return object.getWarehouseLocation();
			}
		};
		table.addColumn(warehouseViewColumn, "Warehouse");
		table.setColumnWidth(warehouseViewColumn,180,Unit.PX);
	}
	
	public void viewstarageLoc() {
		columnviewLoc = new TextColumn<InspectionDetails>() {
			@Override
			public String getValue(InspectionDetails object) {
				return object.getStorageLoc();
			}
		};
		table.addColumn(columnviewLoc, "Storage Loc.");
		table.setColumnWidth(columnviewLoc,153,Unit.PX);
	}

	public void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<InspectionDetails, String>(btnCell) {
			@Override
			public String getValue(InspectionDetails object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn,81,Unit.PX);

	}
	
	public void createColumnAddColumn() {
		ButtonCell btnCell = new ButtonCell();
		addColumn = new Column<InspectionDetails, String>(btnCell) {
			@Override
			public String getValue(InspectionDetails object) {
				return "Select Warehouse";
			}
		};
		table.addColumn(addColumn, "");
		table.setColumnWidth(addColumn,180,Unit.PX);

	}
	
	public void createColumnInspectColumn() {
		ButtonCell btnCell = new ButtonCell();
		inspectColumn = new Column<InspectionDetails, String>(btnCell) {
			@Override
			public String getValue(InspectionDetails object) {
				return "Inspect";
			}
		};
		table.addColumn(inspectColumn, "");
		table.setColumnWidth(inspectColumn,100,Unit.PX);

	}
	

	/****************************************** Column Update methods ******************************************/
	// Product Quantity Update Method
	protected void createFieldUpdaterprodQtyColumn() {
		columnProductQuantity.setFieldUpdater(new FieldUpdater<InspectionDetails, String>() {
					@Override
					public void update(final int index,final InspectionDetails object,String value) {
						try {
							final double val1;
							val1 = Double.parseDouble(value.trim());
							final int prodId=object.getProductID();
							final double qty=val1;
							
							if(InspectionPresenter.grnId!=0){
								if(qty<object.getProductQuantity()){
									if(qty<0){
										rejectedQty=0;
									}else{
										rejectedQty=object.getProductQuantity()-qty;
										System.out.println("Rejected Qty :::: "+rejectedQty);
									}
									
								}
								else if(qty==object.getProductQuantity()){
									rejectedQty=0;
								}
								else{
									rejectedQty=0;
								}
								
								object.setAcceptedQty(val1);
								object.setRejectedQty(rejectedQty);
								table.redrawRow(index);
							}
							else{
								object.setAcceptedQty(val1);
								object.setRejectedQty(rejectedQty);
								table.redrawRow(index);
							}
						} catch (NumberFormatException e) {
						}
					}
					
				});
		
		
	}
	// Delete Update Method
	public void createFieldUpdaterDelete() {
		deleteColumn.setFieldUpdater(new FieldUpdater<InspectionDetails, String>() {
			@Override
			public void update(int index, InspectionDetails object, String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	// Inspect Update Method
		public void createFieldUpdaterInspect() {
			inspectColumn.setFieldUpdater(new FieldUpdater<InspectionDetails, String>() {
				@Override
				public void update(int index, InspectionDetails object, String value) {
					
					panel=new PopupPanel(true);
					panel.add(insPopUp);
					insPopUp.clear();
					if(object.getInspectionStepsLis().isEmpty()){
						insPopUp.initialzeInspectionMethod(object.getProductID());
					}
					if(!object.getInspectionStepsLis().isEmpty()){
						System.out.println("Inside List Not Null......");
						insPopUp.getInspectionListTable().getDataprovider().setList(object.getInspectionStepsLis());
					}
					panel.show();
					panel.center();
					rowIndex=index;
				}
			});
		}
		
		// Select Warehouse Update Method
				public void createFieldUpdaterAdd() {
					addColumn.setFieldUpdater(new FieldUpdater<InspectionDetails, String>() {
						@SuppressWarnings("static-access")
						@Override
						public void update(int index, InspectionDetails object, String value) {
								panel=new PopupPanel(true);
								panel.add(invLoc);
								
								InventoryLocationPopUp.initialzeWarehouse(object.getProductID());
								
								
								if(object.getWarehouseLocation()!=null&&object.getStorageLoc()!=null&&object.getStorageBin()!=null){
									System.out.println("Inside table not null list settt...........");
									for(int i=0;i<invLoc.getLsWarehouse().getItemCount();i++)
									{
										String data=invLoc.lsWarehouse.getItemText(i);
										if(data.equals(object.getWarehouseLocation()))
										{
											invLoc.lsWarehouse.setSelectedIndex(i);
											break;
										}
									}
									
									invLoc.getLsStorageLoc().clear();
									invLoc.lsStorageLoc.addItem("--SELECT--");
									invLoc.lsStorageLoc.addItem(object.getStorageLoc());
									
									
									invLoc.getLsStoragebin().clear();
									invLoc.lsStoragebin.addItem("--SELECT--");
									
									for(int i=0;i<invLoc.arraylist2.size();i++){
										if(invLoc.arraylist2.get(i).getWarehouseName().equals(object.getWarehouseLocation())
												&&invLoc.arraylist2.get(i).getStorageLocation().equals(object.getStorageLoc())){
											invLoc.lsStoragebin.addItem(invLoc.arraylist2.get(i).getStorageBin());
										}
									}
									
									for(int i=0;i<invLoc.getLsStorageLoc().getItemCount();i++)
									{
										String data=invLoc.lsStorageLoc.getItemText(i);
										if(data.equals(object.getStorageLoc()))
										{
											invLoc.lsStorageLoc.setSelectedIndex(i);
											break;
										}
									}
									for(int i=0;i<invLoc.getLsStoragebin().getItemCount();i++)
									{
										String data=invLoc.lsStoragebin.getItemText(i);
										if(data.equals(object.getStorageBin()))
										{
											invLoc.lsStoragebin.setSelectedIndex(i);
											break;
										}
									}
								}
								invLoc.formView();
								panel.show();
								
								
								panel.center();
								rowIndex=index;
						}
				});
			}
				

	/***************************************************************************************************************************/				
		@Override
		public void onClick(ClickEvent event) {
			if(event.getSource()==invLoc.getAddButton())
			{	
				if(invLoc.getLsWarehouse().getSelectedIndex()!=0&&invLoc.getLsStorageLoc().getSelectedIndex()!=0&&invLoc.getLsStoragebin().getSelectedIndex()!=0){
					ArrayList<InspectionDetails> list=new ArrayList<InspectionDetails>();
					if(getDataprovider().getList().size()!=0){
						list.addAll(getDataprovider().getList());
						for( int i=rowIndex;i<getDataprovider().getList().size();i++){
							list.get(rowIndex).setWarehouseLocation(invLoc.getLsWarehouse().getValue(invLoc.getLsWarehouse().getSelectedIndex()));
							list.get(rowIndex).setStorageLoc(invLoc.getLsStorageLoc().getValue(invLoc.getLsStorageLoc().getSelectedIndex()));
							list.get(rowIndex).setStorageBin(invLoc.getLsStoragebin().getValue(invLoc.getLsStoragebin().getSelectedIndex()));
							getDataprovider().getList().clear();
							getDataprovider().getList().addAll(list);
						}
					}
					panel.hide();
				}
				if(invLoc.getLsWarehouse().getSelectedIndex()==0&&invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
					InspectionPresenter.showMessage("Please Select Warehouse.");
				}
				else if(invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
					InspectionPresenter.showMessage("Please Select Storage Location.");
				}
				else if(invLoc.getLsStoragebin().getSelectedIndex()==0){
					InspectionPresenter.showMessage("Please Select Storage Bin.");
				}
				
			}
			if(event.getSource()==invLoc.getCancelButton())
			{	
				invLoc.clear();
				panel.hide();
			}
			
			
			
			
			if(event.getSource()==insPopUp.getAddInspection())
			{
				System.out.println("Inside onClick Condition.............");
				if(insPopUp.getOlbInspectionMethod().getSelectedIndex()==0)
				{
					InspectionPresenter.showMessage("Please select Inspection Method!");
				}
				if(insPopUp.getOlbinspectionchecklist().getSelectedIndex()==0&&insPopUp.getOlbInspectionMethod().getSelectedIndex()!=0)
				{
					InspectionPresenter.showMessage("Please select Inspection Checklist!");
				}
				
				List<ShippingSteps> shipStepLis=insPopUp.getInspectionListTable().getDataprovider().getList();
				System.out.println("Ship Steps Size"+shipStepLis.size());
				
				if(shipStepLis.size()>0)
				{
					InspectionPresenter.showMessage("Inspection Steps Already Added!");
				}
				
				if(insPopUp.getOlbinspectionchecklist().getSelectedIndex()!=0&&insPopUp.getOlbInspectionMethod().getSelectedIndex()!=0&&shipStepLis.size()==0)
				{
					MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter temp=null;
					
					temp=new Filter();
					temp.setStringValue(insPopUp.getOlbinspectionchecklist().getItemText(insPopUp.getOlbinspectionchecklist().getSelectedIndex()).trim());
					temp.setQuerryString("checkListName");
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("status");
					temp.setBooleanvalue(true);
					filtervec.add(temp);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new CheckListType());
					
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onFailure(Throwable caught) {
							InspectionPresenter.showMessage("An Unexpected error occurred!");
						}
			
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							
							if(result.size()>0)
							{
								for(SuperModel model:result){
									CheckListType chkType=(CheckListType)model;
									
									for(int i=0;i<chkType.getStepsInfo().size();i++)
									{
										ShippingSteps shipSteps=new ShippingSteps();
										shipSteps.setShippingStepNumber(chkType.getStepsInfo().get(i).getStepDetailsNumber());
										shipSteps.setShippingStepCode(chkType.getStepsInfo().get(i).getStepDetailsCode());
										shipSteps.setShippingStepName(chkType.getStepsInfo().get(i).getStepDetailsName());
										shipSteps.setShippingStepMandatory(chkType.getStepsInfo().get(i).getStepDetailsMandatory());
										
										insPopUp.getInspectionListTable().getDataprovider().getList().add(shipSteps);
									}
								}
							}
							
							}
						 });
				}
			}
			
			if(event.getSource()==insPopUp.getBtnOk())
			{	
				ArrayList<InspectionDetails> list=new ArrayList<InspectionDetails>();
				if(getDataprovider().getList().size()!=0){
					list.addAll(getDataprovider().getList());
					for( int i=rowIndex;i<getDataprovider().getList().size();i++){
						list.get(rowIndex).setInspectionStepsLis(insPopUp.getInspectionListTable().getDataprovider().getList());
						getDataprovider().getList().clear();
						getDataprovider().getList().addAll(list);
					}
				}
				insPopUp.clear();
				panel.hide();
			}
			
			if(event.getSource()==insPopUp.getBtnCancel())
			{	
				insPopUp.clear();
				panel.hide();
			}
			
			
		}
		
		
	
	
/****************************************************************************************************************************/
	
	@Override
	protected void initializekeyprovider() {

	}

	@Override
	public void applyStyle() {

	}

}
