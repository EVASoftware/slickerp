package com.slicktechnologies.client.views.inventory.inspection;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class InspectionPresenterSearchProxy extends SearchPopUpScreen<Inspection> {

	IntegerBox ibGrnNo;
//	TextBox tbGrnTitle;
	
	ObjectListBox<HrProject> olbProject;
	TextBox ibPOid;
	
	TextBox ibInspectionId;
	TextBox tbInspectionTitle;
	public DateComparator dateComparator;
	
	ObjectListBox<Config> oblInspectionGroup;
	ObjectListBox<ConfigCategory> oblInspectionCat;
	ObjectListBox<Type> oblInspectiontype;
	
	ObjectListBox<Branch> olbBranch;
	ObjectListBox<Employee> olbEmployee;
	ObjectListBox<Employee> olbApprover;
	
	public ListBox olbStatus;
	
	public PersonInfoComposite personInfoComposite;
	public ProductInfoComposite productInfoComposite;
	
	public InspectionPresenterSearchProxy() {
		super();
		createGui();
		productInfoComposite.getProdPrice().setVisible(false);
		productInfoComposite.getUnitOfmeasuerment().setVisible(false);
	}
	
	public InspectionPresenterSearchProxy(boolean b) {
		super(b);
		createGui();
		productInfoComposite.getProdPrice().setVisible(false);
		productInfoComposite.getUnitOfmeasuerment().setVisible(false);
	}

	private void initalizeWidget() {
		ibGrnNo=new IntegerBox();
//		tbGrnTitle=new TextBox();
		olbProject = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(olbProject);
		ibPOid=new TextBox();
		
		ibInspectionId=new TextBox();
		tbInspectionTitle=new TextBox();
		dateComparator = new DateComparator("creationDate", new Inspection());
		oblInspectionGroup = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblInspectionGroup, Screen.INSPECTIONGROUP);
		oblInspectionCat= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblInspectionCat, Screen.INSPECTIONCATEGORY);
		oblInspectiontype = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(oblInspectiontype, Screen.INSPECTIONTYPE);
		
		olbBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		olbEmployee = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		olbEmployee.makeEmployeeLive(AppConstants.INVENTORYMODULE, AppConstants.INSPECTION, "Purchase Engineer");
		olbApprover = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbApprover);
		
		personInfoComposite = new PersonInfoComposite(new MyQuerry(new Vector<Filter>(), new Vendor()));
		personInfoComposite.getCustomerId().getHeaderLabel().setText("Vendor ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("Vendor Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("Vendor Cell");
		
		productInfoComposite = AppUtility.product(new SuperProduct(), false);
		
		olbStatus = new ListBox();
		AppUtility.setStatusListBox(olbStatus, Inspection.getStatusList());
	}
	
	@Override
	public void createScreen() {
		initalizeWidget();
		
		// ////////////////////////// Form Field
		// Initialization////////////////////////////////////////////////////
		// Token to initialize formfield
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder("GRN ID", ibGrnNo);
		FormField fibGrnNo = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

//		fbuilder = new FormFieldBuilder("GRN Title", tbGrnTitle);
//		FormField fgrntitle = fbuilder.setMandatory(false).setRowSpan(0)
//				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Project", olbProject);
		FormField folbProject = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Order ID", ibPOid);
		FormField ftbPOid = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Inspection ID", ibInspectionId);
		FormField finspectionId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Inspection Title", tbInspectionTitle);
		FormField finspectiontitle = fbuilder.setMandatory(false)
				.setMandatoryMsg("Inspection Title is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("From Inspection Date", dateComparator.getFromDate());
		FormField fdbinspectionDate = fbuilder.setMandatory(false)
				.setMandatoryMsg("Inspection date is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Inspection Date", dateComparator.getToDate());
		FormField fdbtoinspectionDate = fbuilder.setMandatory(false)
				.setMandatoryMsg("Inspection date is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		
		
		fbuilder = new FormFieldBuilder("Inspection Group", oblInspectionGroup);
		FormField fgrngroup = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Inspection Category", oblInspectionCat);
		FormField fgrncate = fbuilder.setMandatory(false)
				.setMandatoryMsg("GRN Category is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Inspection Type", oblInspectiontype);
		FormField fgrntype = fbuilder.setMandatory(false)
				.setMandatoryMsg("GRN Type is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		

		fbuilder = new FormFieldBuilder("Branch", olbBranch);
		FormField folbBranch = fbuilder.setMandatory(false)
				.setMandatoryMsg("Branch is Manadatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Purchase Engineer", olbEmployee);
		FormField foblpurchaseengg = fbuilder.setMandatory(false)
				.setMandatoryMsg("Purchase engineer is mandatory")
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Approver", olbApprover);
		FormField folbApprover = fbuilder.setMandatory(false)
				.setMandatoryMsg("Approver is Manadatory !").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Status", olbStatus);
		FormField ftbStatus = fbuilder.setRowSpan(0).setColSpan(0).build();
		

		fbuilder = new FormFieldBuilder("", personInfoComposite);
		FormField fpersonInfoComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fprodcompo = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();
		

		// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// //////////////////////////////Lay Out Making
		// Code///////////////////////////////////////////////////////////////

		FormField[][] formfield = { 
				{ finspectionId, finspectiontitle, fdbinspectionDate,fdbtoinspectionDate},
				{ fgrngroup,fgrncate, fgrntype,folbBranch  },
				{ foblpurchaseengg,folbApprover,folbProject,ftbStatus},
				{ fibGrnNo,ftbPOid},
				{ fpersonInfoComposite },
				{ fprodcompo},
		};
		this.fields = formfield;
	}
	
	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		if (dateComparator.getValue() != null) {
			filtervec.addAll(dateComparator.getValue());
		}

		if (personInfoComposite.getIdValue() != -1) {
			temp = new Filter();
			temp.setIntValue(personInfoComposite.getIdValue());
			temp.setQuerryString("vendorInfo.count");
			filtervec.add(temp);
		}
		if (!(personInfoComposite.getFullNameValue().equals(""))) {
			temp = new Filter();
			temp.setStringValue(personInfoComposite.getFullNameValue());
			temp.setQuerryString("vendorInfo.fullName");
			filtervec.add(temp);
		}
		if (personInfoComposite.getCellValue() != -1l) {
			temp = new Filter();
			temp.setLongValue(personInfoComposite.getCellValue());
			temp.setQuerryString("vendorInfo.cellNumber");
			filtervec.add(temp);
		}

		if (olbBranch.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if (olbEmployee.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbEmployee.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		if (olbApprover.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbApprover.getValue().trim());
			temp.setQuerryString("approverName");
			filtervec.add(temp);
		}
		if (olbProject.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbProject.getValue().trim());
			temp.setQuerryString("inspectionProject");
			filtervec.add(temp);
		}
		if (ibGrnNo.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibGrnNo.getValue());
			temp.setQuerryString("inspectionGrnId");
			filtervec.add(temp);
		}
		if (olbStatus.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbStatus.getValue(olbStatus.getSelectedIndex()).trim());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		if (!ibPOid.getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(ibPOid.getValue()));
			temp.setQuerryString("inspectionPOid");
			filtervec.add(temp);
		}
		if (!ibInspectionId.getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(ibInspectionId.getValue()));
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		if (!tbInspectionTitle.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(tbInspectionTitle.getValue());
			temp.setQuerryString("inspectionTitle");
			filtervec.add(temp);
		}
		if (oblInspectionCat.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblInspectionCat.getValue());
			temp.setQuerryString("category");
			filtervec.add(temp);
		}
		if (oblInspectionGroup.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblInspectionGroup.getValue());
			temp.setQuerryString("group");
			filtervec.add(temp);
		}
		if (oblInspectiontype.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblInspectiontype.getValue());
			temp.setQuerryString("type");
			filtervec.add(temp);
		}
		if (!productInfoComposite.getProdCode().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(productInfoComposite.getProdCode().getValue());
			temp.setQuerryString("items.productdetails.productCode");
			filtervec.add(temp);
		}

		if (!productInfoComposite.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
			temp.setQuerryString("items.productdetails.productID");
			filtervec.add(temp);
		}

		if (!productInfoComposite.getProdName().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(productInfoComposite.getProdName().getValue());
			temp.setQuerryString("items.productdetails.productName");
			filtervec.add(temp);
		}
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Inspection());
		return querry;
	}

	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}

}
