package com.slicktechnologies.client.views.inventory.inspection;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.views.inventory.recievingnote.InspectionStepsTable;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.shippingpackingprocess.InspectionMethod;

public class InspectionPopUp extends AbsolutePanel implements ChangeHandler{
	
	final static GenricServiceAsync async = GWT.create(GenricService.class);
	ObjectListBox<InspectionMethod> olbInspectionMethod;
	ListBox olbinspectionchecklist;
	InspectionStepsTable inspectionListTable;
	private Button addInspection;
	private Button btnCancel,btnOk;
	
	public InspectionPopUp() {
		olbInspectionMethod=new ObjectListBox<InspectionMethod>();
		InspectionMethod.initializeInspectionName(olbInspectionMethod);
		olbInspectionMethod.addChangeHandler(this);
		olbinspectionchecklist=new ListBox();
		
		olbinspectionchecklist.setEnabled(false);
		olbinspectionchecklist.addItem("SELECT");
		
		inspectionListTable=new InspectionStepsTable();
		addInspection=new Button("ADD");
		
		
		
		InlineLabel inspectionDetails=new InlineLabel("Inspection Details");
		inspectionDetails.getElement().getStyle().setFontSize(20, Unit.PX);
		add(inspectionDetails,320,10);
		
		InlineLabel inspectionMethod=new InlineLabel("Inspection Method");
		add(inspectionMethod, 10, 50);
		add(olbInspectionMethod, 10, 70);
		olbInspectionMethod.setSize("140px", "25px");
		
		InlineLabel inspectionCheckList=new InlineLabel("Inspection CheckList");
		add(inspectionCheckList, 160, 50);
		add(olbinspectionchecklist, 160, 70);
		olbinspectionchecklist.setSize("140px", "25px");
		
		add(addInspection, 320, 70);
		
		
		inspectionListTable.connectToLocal();
		add(inspectionListTable.getTable(),10,110);
		
		btnCancel = new Button("Cancel");
		btnOk=new Button("Ok");
		
		add(btnOk, 300, 465);
		add(btnCancel,450,465);
		
		setSize("800px","500px");
		this.getElement().setId("form");
		
	}

	
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(getOlbInspectionMethod()))
		{
			if(getOlbInspectionMethod().getSelectedIndex()!=0){
				getOlbinspectionchecklist().clear();
				getOlbinspectionchecklist().addItem("SELECT");
				InspectionMethod shipEntity=getOlbInspectionMethod().getSelectedItem();
				if(shipEntity.getCheckListName()!=null){
					getOlbinspectionchecklist().setEnabled(true);
					getOlbinspectionchecklist().addItem(shipEntity.getCheckListName());
				}
			}
			if(getOlbInspectionMethod().getSelectedIndex()==0){
				getOlbinspectionchecklist().setEnabled(false);
				getOlbinspectionchecklist().setSelectedIndex(0);
			}
		}
	}
	
	
	
	
	@Override
	public void clear() {
		olbinspectionchecklist.setSelectedIndex(0);
		olbInspectionMethod.setSelectedIndex(0);
		inspectionListTable.connectToLocal();
	}
	
	public void initialzeInspectionMethod(final int prodId){
		System.out.println("inside Inspection popoup........");
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("productinfo.prodID");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new ProductInventoryView());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for (SuperModel model : result) {
					ProductInventoryView entity = (ProductInventoryView) model;
					System.out.println("INspection Method 111 ::: "+entity.getInspectionMethod());
					if(entity.getInspectionMethod()!=null){
						System.out.println("INspection Method ::: "+entity.getInspectionMethod());
						getOlbInspectionMethod().setValue(entity.getInspectionMethod());
						if(olbInspectionMethod.getSelectedIndex()!=0){
							getOlbinspectionchecklist().clear();
							getOlbinspectionchecklist().addItem("SELECT");
							InspectionMethod shipEntity=getOlbInspectionMethod().getSelectedItem();
							if(shipEntity.getCheckListName()!=null){
								getOlbinspectionchecklist().setEnabled(true);
								getOlbinspectionchecklist().addItem(shipEntity.getCheckListName());
							}
						}
					}
				}
				
			}
		});
	}
	
	


	/**************************** Getter/Setter ***********************************/
	
	
	public ObjectListBox<InspectionMethod> getOlbInspectionMethod() {
		return olbInspectionMethod;
	}

	public void setOlbInspectionMethod(
			ObjectListBox<InspectionMethod> olbInspectionMethod) {
		this.olbInspectionMethod = olbInspectionMethod;
	}

	public ListBox getOlbinspectionchecklist() {
		return olbinspectionchecklist;
	}

	public void setOlbinspectionchecklist(ListBox olbinspectionchecklist) {
		this.olbinspectionchecklist = olbinspectionchecklist;
	}

	public InspectionStepsTable getInspectionListTable() {
		return inspectionListTable;
	}

	public void setInspectionListTable(InspectionStepsTable inspectionListTable) {
		this.inspectionListTable = inspectionListTable;
	}

	public Button getAddInspection() {
		return addInspection;
	}

	public void setAddInspection(Button addInspection) {
		this.addInspection = addInspection;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}


	

	
}
