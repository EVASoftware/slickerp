package com.slicktechnologies.client.views.inventory.inspection;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.recievingnote.ProductQty;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.deliverynote.ShippingSteps;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.InspectionDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;


public class InspectionPresenter extends ApprovableFormScreenPresenter<Inspection>{

	public static InspectionForm form;
	final static GenricServiceAsync async = GWT.create(GenricService.class);
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	EmailServiceAsync emailService = GWT.create(EmailService.class);
	InspectionPopUp insPopUp=new InspectionPopUp();
	static ArrayList<ProductQty> grnList=new ArrayList<ProductQty>();
	/**
	 * this variable is used to query into grn while updating inspection qty.
	 */
	static int grnId=0;
	
	public InspectionPresenter(ApprovableFormScreen<Inspection> view, Inspection model) {
		super(view, model);
		form = (InspectionForm) view;
		insPopUp.getAddInspection().addClickHandler(this);
		insPopUp.getBtnOk().addClickHandler(this);
		insPopUp.getBtnCancel().addClickHandler(this);
		
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.INSPECTION,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
	}
	public static void showMessage(String msg){
		form.showDialogMessage(msg);
	}
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		if (text.equals(AppConstants.CANCEL)){
			reactToCancel();
		}
		if (text.equals(AppConstants.NEW)){
			reactToNew();
		}
		if (text.equals("DownLoad")){
			reactOnDownload();
		}
		if (text.equals("Email")){
			reactOnEmail();
		}
		if(text.equals(ManageApprovals.APPROVALREQUEST))
		{
			reactOnApprovalSendRequest();
		}
		if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
		{
			form.getManageapproval().reactToApprovalRequestCancel();
		}
	}
	
	
	private void reactOnApprovalSendRequest()
	{
		if(form.validate())
		{
			if(!validateInspectionTable()){
				form.showDialogMessage("Please select all mandatory steps in inspection table!");
			}
			
			if(validateInspectionTable())
			{
				form.getManageapproval().reactToRequestForApproval();
			}
		}
		else
		{
			form.showDialogMessage("Plesae fill mandatory fields!");
		}
	}
	
	
	private boolean validateInspectionTable()
	{
		List<ShippingSteps> shiplis=new ArrayList<ShippingSteps>();
		List<InspectionDetails>insplis=form.getTable().getDataprovider().getList();
		for(int i=0;i<insplis.size();i++)
		{	
			if(insplis.get(i).getInspectionRequired().equals(AppConstants.YES)){
				shiplis.addAll(insplis.get(i).getInspectionStepsLis());
			}
		}
		if(shiplis.isEmpty()){
			return true;
		}else{
			for(int i=0;i<shiplis.size();i++){
				if(shiplis.get(i).getShippingStepMandatory().trim().equals(AppConstants.YES)&&shiplis.get(i).isCompletionStatus()==false){
					return false;
				}
			}
		}
		
		return true;
	}
	
	
	@Override
	public void reactOnDownload() {
		ArrayList<Inspection> custarray = new ArrayList<Inspection>();
		List<Inspection> list = (List<Inspection>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		custarray.addAll(list);
		csvservice.setInspectionDetails(custarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 83;
				Window.open(url, "test", "enabled");
			}
		});
	}

	@Override
	public void reactOnPrint() {
		final String url = GWT.getModuleBaseURL() + "pdfinspection" + "?Id="+ model.getId();
		Window.open(url, "test", "enabled");
	}
	
	@Override
	public void reactOnEmail() {

		boolean conf = Window.confirm("Do you really want to send email?");
		if (conf == true) {
			emailService.initiateInspectionEmail(model, new AsyncCallback<Void>() {
				@Override
				public void onSuccess(Void result) {
					Window.alert("Email Sent Sucessfully !");
				}
				
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Resource Quota Ended");
					caught.printStackTrace();
				}
			});
		}
	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model = new Inspection();
	}

	public static InspectionForm initalize() {
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inspection",Screen.INSPECTION);
		InspectionForm form = new InspectionForm();

		InspectionPresenterTableProxy gentable = new InspectionPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		InspectionPresenterSearchProxy.staticSuperTable = gentable;
		InspectionPresenterSearchProxy searchpopup = new InspectionPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		InspectionPresenter presenter = new InspectionPresenter(form, new Inspection());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	private void reactToCancel() {
		model.setStatus(Inspection.CANCELLED);
		changeStatus("Failed To Cancel Inspection!", "Inspection Cancelled !");
	}

	private void reactToNew() {
		form.setToNewState();
		initalize();
	}

	private void changeStatus(final String failureMessage,final String successMessage) {
		GenricServiceAsync async = GWT.create(GenricService.class);
		form.showWaitSymbol();
		async.save(model, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onSuccess(ReturnFromServer result) {
				form.setAppHeaderBarAsPerStatus();
				form.setMenuAsPerStatus();
				form.showDialogMessage(successMessage);
				form.getTbStatus().setText(Inspection.CANCELLED);
				form.setToViewState();
				form.hideWaitSymbol();
			}
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage(failureMessage);
				form.hideWaitSymbol();
			}
		});
	}

	public static void checkQuantity(final int grnId,final int inspectionId){
		System.out.println("Inside search .............................");
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(grnId);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new GRN());
		
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				grnList=new ArrayList<ProductQty>();
				for (SuperModel model : result) {
					if(result.size()!=0){
						GRN entity = (GRN) model;
						for(int i=0;i<entity.getProductDetails().size();i++){
							
							ProductQty pq=new ProductQty();
							pq.setProdId(entity.getProductDetails().get(i).getProductID());
							pq.setProdQty(entity.getProductDetails().get(i).getProductQuantity());
							
							grnList.add(pq);
						}
					}
				}
				
				for(int i=0;i<grnList.size();i++){
					System.out.println("------------------------ Result ----------------------------  ");
					System.out.println("GRN List GRN ID : "+grnList.get(i).getProdId());
					System.out.println("GRN List QTY : "+grnList.get(i).getProdQty());
				}
			}
		});
	}
	
	
	
	
}
