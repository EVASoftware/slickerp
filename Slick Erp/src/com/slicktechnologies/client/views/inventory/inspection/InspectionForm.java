package com.slicktechnologies.client.views.inventory.inspection;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.InspectionDetails;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class InspectionForm extends ApprovableFormScreen<Inspection> implements ClickHandler,ChangeHandler, SelectionHandler<Suggestion> {
	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	
	TextBox ibGrnNo;
	TextBox tbGrnTitle;
	DateBox dbGrndate;
	
	TextBox tbRefNo;
	DateBox dbRefDate;
	ObjectListBox<HrProject> olbProject;
	TextBox ibPOid;
	
	TextBox ibInspectionId;
	TextBox tbInspectionTitle;
	DateBox dbInspectionDate;
	
	ObjectListBox<Config> oblInspectionGroup;
	ObjectListBox<ConfigCategory> oblInspectionCat;
	ObjectListBox<Type> oblInspectiontype;
	
	ObjectListBox<Branch> olbBranch;
	ObjectListBox<Employee> olbEmployee;
	ObjectListBox<Employee> olbApprover;
	
	TextBox tbStatus;
	TextBox tbInspectionRemark;
	TextArea taDescription;
	
	public PersonInfoComposite personInfoComposite;
	public ProductInfoComposite productInfoComposite;
	Button btnAdd;
	
	UploadComposite uploadDocument;
	
	public InspectionProductTable table;
	FormField fgrngrouping;
	Inspection inspectionobj;

	public InspectionForm() {
		super();
		createGui();
		this.ibInspectionId.setEnabled(false);
		this.tbStatus.setEnabled(false);
		this.tbStatus.setText(Inspection.CREATED);
		this.tbInspectionRemark.setEnabled(false);
		this.ibGrnNo.setEnabled(false);
		this.tbGrnTitle.setEnabled(false);
		this.dbGrndate.setEnabled(false);
		this.ibPOid.setEnabled(false);
	}

	public InspectionForm(String[] processlevel, FormField[][] fields,FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
		this.ibInspectionId.setEnabled(false);
		this.tbStatus.setEnabled(false);
		this.tbStatus.setText(Inspection.CREATED);
		this.tbInspectionRemark.setEnabled(false);
		this.ibGrnNo.setEnabled(false);
		this.tbGrnTitle.setEnabled(false);
		this.dbGrndate.setEnabled(false);
		this.ibPOid.setEnabled(false);
		
	}
	
	private void initalizeWidget() {
		ibGrnNo=new TextBox();
		tbGrnTitle=new TextBox();
//		dbGrndate=new DateBox();
		dbGrndate = new DateBoxWithYearSelector();

		olbProject = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(olbProject);
		
		tbRefNo=new TextBox();
		dbRefDate=new DateBoxWithYearSelector();
		ibPOid=new TextBox();
		
		ibInspectionId=new TextBox();
		tbInspectionTitle=new TextBox();
		dbInspectionDate=new DateBoxWithYearSelector();
		
		oblInspectionGroup = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblInspectionGroup, Screen.INSPECTIONGROUP);
		oblInspectionCat= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblInspectionCat, Screen.INSPECTIONCATEGORY);
		oblInspectiontype = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(oblInspectiontype, Screen.INSPECTIONTYPE);
		
		olbBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		olbEmployee = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		olbEmployee.makeEmployeeLive(AppConstants.INVENTORYMODULE, AppConstants.INSPECTION, "Purchase Engineer");
		
		olbApprover = new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApprover,"Inspection");
		
		uploadDocument=new UploadComposite();
		
		personInfoComposite = new PersonInfoComposite(new MyQuerry(new Vector<Filter>(), new Vendor()));
		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Vendor ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Vendor Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Vendor Cell");
		personInfoComposite.getId().addSelectionHandler(this);
		personInfoComposite.getName().addSelectionHandler(this);
		personInfoComposite.getPhone().addSelectionHandler(this);
		
		productInfoComposite = AppUtility.initiateSalesProductComposite(new SuperProduct());
		
		btnAdd = new Button("Add");
		btnAdd.addClickHandler(this);
		
		table = new InspectionProductTable();
		tbInspectionRemark=new TextBox();
		
		tbStatus = new TextBox();
		taDescription = new TextArea();
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")
						|| text.contains("Search")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")|| text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Edit") || text.contains("Search") || text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.INSPECTION,LoginPresenter.currentModule.trim());
	}

	@Override
	public void createScreen() {
		initalizeWidget();
		// Token to initialize the processlevel menus.
		this.processlevelBarNames = new String[] {
				ManageApprovals.APPROVALREQUEST,
				ManageApprovals.CANCELAPPROVALREQUEST, AppConstants.CANCEL,
				AppConstants.NEW };

		// ////////////////////////// Form Field
		// Initialization////////////////////////////////////////////////////
		// Token to initialize formfield
		
		String mainScreenLabel="INSPECTION";
		if(inspectionobj!=null&&inspectionobj.getCount()!=0){
			mainScreenLabel=inspectionobj.getCount()+" "+"/"+" "+inspectionobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(inspectionobj.getCreationDate());
		}
		//fgrngrouping.getHeaderLabel().setText(mainScreenLabel);
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgrngrouping=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		
		//FormField fgrngrouping = fbuilder.setlabel("Reference Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("GRN ID", ibGrnNo);
		FormField fibGrnNo = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("GRN Title", tbGrnTitle);
		FormField fgrntitle = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("GRN Date", dbGrndate);
		FormField fgrndate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project", olbProject);
		FormField folbProject = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Reference Id", tbRefNo);
		FormField ftbRefId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Reference Date", dbRefDate);
		FormField ftbRefDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Order Id", ibPOid);
		FormField ftbPOid = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		
		FormField fgroupinginspection = fbuilder
				.setlabel("Vendor").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Inspection ID", ibInspectionId);
		FormField finspectionId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Inspection Title", tbInspectionTitle);
		FormField finspectiontitle = fbuilder.setMandatory(true)
				.setMandatoryMsg("Inspection Title is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Inspection Date", dbInspectionDate);
		FormField fdbinspectionDate = fbuilder.setMandatory(true)
				.setMandatoryMsg("Inspection date is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		
		
		fbuilder = new FormFieldBuilder("Inspection Group", oblInspectionGroup);
		FormField fgrngroup = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Inspection Category", oblInspectionCat);
		FormField fgrncate = fbuilder.setMandatory(false)
				.setMandatoryMsg("Category is mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Inspection Type", oblInspectiontype);
		FormField fgrntype = fbuilder.setMandatory(false)
				.setMandatoryMsg("Type is mandatory").setRowSpan(0)
				.setColSpan(0).build();
		

		fbuilder = new FormFieldBuilder("* Branch", olbBranch);
		FormField folbBranch = fbuilder.setMandatory(true)
				.setMandatoryMsg("Branch is Manadatory !").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Purchase Engineer", olbEmployee);
		FormField foblpurchaseengg = fbuilder.setMandatory(true)
				.setMandatoryMsg("Purchase engineer is mandatory")
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Approver", olbApprover);
		FormField folbApprover = fbuilder.setMandatory(true)
				.setMandatoryMsg("Approver is Manadatory !").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Status", tbStatus);
		FormField ftbStatus = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Comment", tbInspectionRemark);
		FormField ftbRemark = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)", taDescription);
		FormField ftaDescription = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Upload Document",uploadDocument);
		FormField fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingGRNInfo = fbuilder
				.setlabel("GRN Details").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();
		
		FormField fgroupingVendorInfo = fbuilder
				.setlabel("Classification").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("", personInfoComposite);
		FormField fpersonInfoComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(4).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingItemInformation = fbuilder
				.setlabel("Product").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fprodcompo = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fAdd = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", table.getTable());
		FormField fitemTable = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		FormField fgroupingAttachInfo = fbuilder
				.setlabel("Attachment").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();

		// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// //////////////////////////////Lay Out Making
		// Code///////////////////////////////////////////////////////////////

		FormField[][] formfield = { 
//				{ fgrngrouping },
//				{ fibGrnNo, fgrntitle, fgrndate,folbProject},
//				{ ftbRefId,ftbRefDate,ftbPOid},
//				{ fgroupinginspection },
//				{ finspectionId, finspectiontitle, fdbinspectionDate,fgrngroup},
//				{ fgrncate, fgrntype,folbBranch,foblpurchaseengg  },
//				{ folbApprover,ftbStatus,ftbRemark},
//				{ fucUploadTAndCs },
//				{ ftaDescription },
//				{ fgroupingVendorInfo },
//				{ fpersonInfoComposite },
//				{ fgroupingItemInformation },
//				{ fprodcompo,fAdd},
//				{ fitemTable }, 
				/**INSPECTION**/
				{ fgrngrouping },
				{ finspectiontitle,fdbinspectionDate,folbBranch,foblpurchaseengg},
				{ folbApprover,ftbRemark},
				/**Vendor Information**/
				{ fgroupinginspection },
				{ fpersonInfoComposite },
				/**Product Information**/
				{ fgroupingItemInformation },
				{ fprodcompo,fAdd},
				{ fitemTable }, 
				/**GRN Details**/
				{fgroupingGRNInfo},
				{ fibGrnNo, fgrntitle, fgrndate,folbProject},
				{ ftbPOid,ftbRefId,ftbRefDate},
				/**Product Information**/
				{ fgroupingVendorInfo },
				{ fgrngroup,fgrncate,fgrntype,},
				{ ftaDescription },
				/**Attachment**/
				{fgroupingAttachInfo},
				{ fucUploadTAndCs }
		
				
				
				
		};
		this.fields = formfield;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(Inspection model) {
		if(!ibGrnNo.getValue().equals("")){
			model.setInspectionGrnId(Integer.parseInt(ibGrnNo.getValue()));
		}
		if (tbGrnTitle.getValue() != null)
			model.setInspectionGrnTitle(tbGrnTitle.getValue());
		if (dbGrndate.getValue() != null)
			model.setInspectionGrnDate(dbGrndate.getValue());
		if (olbProject.getValue() != null)
			model.setInspectionProject(olbProject.getValue());
		if (tbRefNo.getValue() != null)
			model.setInspectionRefNo(tbRefNo.getValue());
		if (dbRefDate.getValue() != null)
			model.setInspectionRefDate(dbRefDate.getValue());
		if (!ibPOid.getValue().equals(""))
			model.setInspectionPOid(Integer.parseInt(ibPOid.getValue()));
		
		if (tbInspectionTitle.getValue() != null)
			model.setInspectionTitle(tbInspectionTitle.getValue());
		if (dbInspectionDate.getValue() != null)
			model.setCreationDate(dbInspectionDate.getValue());
		
		
		if (oblInspectionCat.getValue() != null)
			model.setCategory(oblInspectionCat.getValue());
		if (oblInspectiontype.getValue() != null)
			model.setType(oblInspectiontype.getValue(oblInspectiontype.getSelectedIndex()));
		if (oblInspectionGroup.getValue() != null)
			model.setGroup(oblInspectionGroup.getValue());
		
		if (olbBranch.getValue() != null)
			model.setBranch(olbBranch.getValue());
		if (olbApprover.getValue() != null)
			model.setApproverName(olbApprover.getValue());
		if (olbEmployee.getValue() != null)
			model.setEmployee(olbEmployee.getValue());
		if (olbApprover.getValue() != null)
			model.setApproverName(olbApprover.getValue());

		model.setUpload(uploadDocument.getValue());
		
		if (taDescription.getValue() != null)
			model.setInspectionDescription(taDescription.getValue());
		if (tbStatus.getValue() != null)
			model.setStatus(tbStatus.getValue());
		if(tbInspectionRemark.getValue()!=null)
			model.setRemark(tbInspectionRemark.getValue());
		if (personInfoComposite.getValue() != null)
			model.setVendorInfo(personInfoComposite.getValue());
		
		if (table.getValue() != null)
			model.setItems(table.getValue());
		
		inspectionobj=model;
		presenter.setModel(model);
	}

	@Override
	public void updateView(Inspection view) {
		
		inspectionobj=view;
		
		if(view.getInspectionGrnId()!=0){
			ibGrnNo.setValue(view.getInspectionGrnId()+"");
			InspectionPresenter.grnId=view.getInspectionGrnId();
			System.out.println("GRN ID ::::: "+InspectionPresenter.grnId);
			InspectionPresenter.checkQuantity(view.getInspectionGrnId(),view.getCount());
		}
		if(view.getInspectionGrnTitle()!=null)
			tbGrnTitle.setValue(view.getInspectionGrnTitle());
		if(view.getInspectionGrnDate()!=null)
			dbGrndate.setValue(view.getInspectionGrnDate());
		if(view.getInspectionProject()!=null)
			olbProject.setValue(view.getInspectionProject());
		if(view.getInspectionRefNo()!=null)
			tbRefNo.setValue(view.getInspectionRefNo());
		if(view.getInspectionRefDate()!=null)
			dbRefDate.setValue(view.getInspectionRefDate());
		if(view.getInspectionPOid()!=0)
			ibPOid.setValue(view.getInspectionPOid()+"");
		
		if(view.getCount()!=0)
			ibInspectionId.setValue(view.getCount()+"");
		if(view.getInspectionTitle()!=null)
			tbInspectionTitle.setValue(view.getInspectionTitle());
		if(view.getCreationDate()!=null)
			dbInspectionDate.setValue(view.getCreationDate());
		
		if(view.getCategory()!=null)
			oblInspectionCat.setValue(view.getCategory());
		if(view.getType()!=null)
			oblInspectiontype.setValue(view.getType());
		if(view.getGroup()!=null)
			oblInspectionGroup.setValue(view.getGroup());
		if(view.getBranch()!=null)
			olbBranch.setValue(view.getBranch());
		if(view.getEmployee()!=null)
			olbEmployee.setValue(view.getEmployee());
		if(view.getApproverName()!=null)
			olbApprover.setValue(view.getApproverName());
		if(view.getStatus()!=null)
			tbStatus.setValue(view.getStatus());
		if(view.getRemark()!=null)
			tbInspectionRemark.setValue(view.getRemark());
		if(view.getInspectionDescription()!=null)
			taDescription.setValue(view.getInspectionDescription());
		if(view.getVendorInfo()!=null)
			personInfoComposite.setValue(view.getVendorInfo());
		if(view.getItems()!=null)
			table.setValue(view.getItems());
		if(view.getUpload()!=null)
			uploadDocument.setValue(view.getUpload());
		
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */
		
	}

	
	
	
	
	public void toggleProcessLevelMenu() {
		Inspection entity = (Inspection) presenter.getModel();
		String status = entity.getStatus();

		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();
			if ((status.equals(Inspection.APPROVED))) {
				if (text.equals("Create Inspection"))
					label.setVisible(true);
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);
			}
			if ((status.equals(Inspection.REJECTED) || status.equals(Inspection.CANCELLED))) {
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);
			}
		}

	}

	public void setAppHeaderBarAsPerStatus() {
		Inspection entity = (Inspection) presenter.getModel();
		String status = entity.getStatus();

		if (status.equals(Inspection.APPROVED) || status.equals(Inspection.CANCELLED)) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains("Print")|| text.contains("Email")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		if (status.equals(Inspection.REJECTED)) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains("Print")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
	}
	
	public void setMenuAsPerStatus() {
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
	}
	@Override
	public TextBox getstatustextbox() {
		return this.tbStatus;
	}
	public boolean validate() {
		boolean superValidate = super.validate();
		if (superValidate == false)
			return false;
		if (table.getDataprovider().getList().size() == 0) {
			showDialogMessage("Please Select at least one Product !");
			return false;
		}
		if(ibGrnNo.getValue()!=null&&tbGrnTitle.getValue()!=null){
			if(validateProdQtyAgainstGrn()==false){
				return false;
			}
		}
		if(validateWarehouse()==false){
			showDialogMessage("Please Select Warehouse Details.");
			return false;
		}

		if(!validateShipingMethod()){
//			showDialogMessage("Please Add Inspection Details!");
			return false;
		}
		if(!validateRemarkForRejectQty()){
			showDialogMessage("Please Add Remark For Rejection!");
			return false;
		}
		if(!validateProdQty()){
			showDialogMessage("Please select appropriate quantity.");
			return false;
		}

		return true;
	}
	
	public boolean validateProdQty()
	{
		int ctr=0;
		List<InspectionDetails> insplist=table.getDataprovider().getList();
		if(!ibGrnNo.getValue().equals("")){
			for(int i=0;i<insplist.size();i++)
			{
				if(insplist.get(i).getAcceptedQty()<0)
				{
					return false;
				}
			}
		}
		else{
			for(int i=0;i<insplist.size();i++)
			{
				if(insplist.get(i).getAcceptedQty()<=0)
				{
					ctr=ctr+1;
				}
			}
			
			if(ctr==0){
				return true;
			}
			else{
				return false;
			}
		}
			return true;
	}
	
	// *********************************** Validate shiping method ***********************************
	public boolean validateShipingMethod(){
		boolean flage=true;
		if(table.getDataprovider().getList().size()!=0){
			for(int i=0;i<table.getDataprovider().getList().size();i++){
				if(table.getDataprovider().getList().get(i).getInspectionRequired().equals(AppConstants.YES)){
					if(table.getDataprovider().getList().get(i).getInspectionStepsLis().isEmpty()){
						flage=false;
						showDialogMessage("Please Add Inspection Details For Product "+table.getDataprovider().getList().get(i).getProductName()+"!");
					}
				}
			}
		}
		return flage;
	}
	
	public boolean validateRemarkForRejectQty(){
		if(table.getDataprovider().getList().size()!=0){
			for(int i=0;i<table.getDataprovider().getList().size();i++){
				if(table.getDataprovider().getList().get(i).getRejectedQty()!=0&&table.getDataprovider().getList().get(i).getReasonForReject().equals("")){
					return false;
				}
			}
		}
		return true;
	}
	
	// ************************************ Validate Warehouse *********************************************
			public boolean validateWarehouse(){
				int ctr=0;
				for(int i=0;i<table.getDataprovider().getList().size();i++){
					if(!table.getDataprovider().getList().get(i).getWarehouseLocation().equals("")
							&&!table.getDataprovider().getList().get(i).getStorageBin().equals("")){
						ctr++;
					}
				}
				if(ctr==table.getDataprovider().getList().size()){
					return true;
				}else{
					return false;
				}
			}
	
	// ************************************** Validate Product ***********************************************
	
		public boolean validateInspectionProdQty()
		{
			List<InspectionDetails> grnlis=table.getDataprovider().getList();
			int ctr=0;
			for(int i=0;i<grnlis.size();i++)
			{
				if(grnlis.get(i).getAcceptedQty()==0)
				{
					ctr=ctr+1;
				}
			}
			
			if(ctr==0){
				return true;
			}
			else{
				return false;
			}
		}
			
			
		public boolean validateProduct() {
			int id=Integer.parseInt(productInfoComposite.getProdID().getValue());
			String name = productInfoComposite.getProdNameValue();
			System.out.println("Product Id :: "+id);
			System.out.println("Product name is :"+name);
			List<InspectionDetails> list = table.getDataprovider().getList();
			for (InspectionDetails temp : list) {
				if (id==temp.getProductID()) {
					this.showDialogMessage("Can not add Duplicate product");
					return false;
				}
			}
			return true;
		}
		// **************** Validate Product Qty Against GRN ****************************
		
		public boolean validateProdQtyAgainstGrn(){
			boolean flage=true;
			for(int i=0;i<table.getDataprovider().getList().size();i++){
				
				if(!InspectionPresenter.grnList.isEmpty()){
					for(int j=0;j<InspectionPresenter.grnList.size();j++){
						if(table.getDataprovider().getList().get(i).getProductID()== InspectionPresenter.grnList.get(j).getProdId()){
							if(table.getDataprovider().getList().get(i).getAcceptedQty()>InspectionPresenter.grnList.get(j).getProdQty()){
								double exceedQty=table.getDataprovider().getList().get(i).getAcceptedQty()-InspectionPresenter.grnList.get(j).getProdQty();
								flage=false;
								showDialogMessage("Product  "+table.getDataprovider().getList().get(i).getProductName()+" exceeds quantity in GRN by "+exceedQty+".");
								break;
							}
						}
					}
				}
			}
			return flage;
		}


	/**
	 * sets the id textbox with the passed count value.
	 */
	@Override
	public void setCount(int count) {
		ibInspectionId.setValue(count+"");
	}

	@Override
	public void clear() {
		super.clear();
		tbStatus.setText(Inspection.CREATED);
		table.clear();

	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		this.tbStatus.setEnabled(false);
		this.ibInspectionId.setEnabled(false);
		table.setEnable(state);
		this.tbStatus.setEnabled(false);
		this.tbInspectionRemark.setEnabled(false);
		this.ibGrnNo.setEnabled(false);
		this.tbGrnTitle.setEnabled(false);
		this.dbGrndate.setEnabled(false);
		this.ibPOid.setEnabled(false);
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		setMenuAsPerStatus();
		
		SuperModel model=new Inspection();
		model=inspectionobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.INVENTORYMODULE,AppConstants.INSPECTION, inspectionobj.getCount(), null,null,null, false, model, null);
	
		String mainScreenLabel="INSPECTION";
		if(inspectionobj!=null&&inspectionobj.getCount()!=0){
			mainScreenLabel=inspectionobj.getCount()+" "+"/"+" "+inspectionobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(inspectionobj.getCreationDate());
		}
		fgrngrouping.getHeaderLabel().setText(mainScreenLabel);
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		if(tbStatus.getValue().equals(Inspection.REJECTED)){
			tbStatus.setValue(Inspection.CREATED);
		}
		if(ibGrnNo.getValue()!=null){
			personInfoComposite.setEnabled(false);
			productInfoComposite.setEnabled(false);
			getIbGrnNo().setEnabled(false);
			getTbGrnTitle().setEnabled(false);
			getOlbBranch().setEnabled(false);
			getDbGrndate().setEnabled(false);
			if(getOlbProject().getValue()!=null){
				getOlbProject().setEnabled(false);
			}
			if(!getTbRefNo().getValue().equals("")){
				getTbRefNo().setEnabled(false);
			}
			if(dbRefDate.getValue()!=null){
				dbRefDate.setEnabled(false);
			}
			getOlbApprover().setEnabled(false);
			getOlbEmployee().setEnabled(false);
		}
		setMenuAsPerStatus();
		
		this.processLevelBar.setVisibleFalse(false);
		
		String mainScreenLabel="INSPECTION";
		if(inspectionobj!=null&&inspectionobj.getCount()!=0){
			mainScreenLabel=inspectionobj.getCount()+" "+"/"+" "+inspectionobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(inspectionobj.getCreationDate());
		}
		fgrngrouping.getHeaderLabel().setText(mainScreenLabel);
	}

	/*************************** on click ***********************************/

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource().equals(btnAdd)) {
			if(validateVendor()){
				setproductTable();
			}
			else{
				showDialogMessage("Please select vendor details.");
			}
		}
	}
	
	public boolean validateVendor(){
		if(!personInfoComposite.getId().getValue().equals("")&&!personInfoComposite.getName().getValue().equals("")&&!personInfoComposite.getPhone().getValue().equals("")){
			return true;
		}
		return false;
	}
	
	
	
	
	/*********************** set table on click **************************/

	public void setproductTable() {
		if(validateProduct()){
		  if (productInfoComposite.getValue() != null) {
			 prodctType(productInfoComposite.getProdCode().getValue(),productInfoComposite.getProdID().getValue()) ;
		  }
		  else{
			showDialogMessage("Enter Product Details.");
		  }
		}
	}
	
	/**
	 * 
	 * This Method Takes Product Code as parameter and get product from SuperProduct 
	 * which has Price list in PriceList
	 */
	
	
	public void prodctType(String pcode,String prodId) {
		final GenricServiceAsync genasync = GWT.create(GenricService.class);
		final MyQuerry querry = new MyQuerry();
		Company c=new Company();
		int productId=Integer.parseInt(prodId);
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("productCode");
		filter.setStringValue(pcode.trim());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(productId);
		filtervec.add(filter);
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
		showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								hideWaitSymbol();
								showDialogMessage("An Unexpected error occurred!");
							}
							InspectionDetails lis;
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								for (SuperModel model : result) {
									SuperProduct superProdEntity = (SuperProduct) model;
									 lis = AppUtility.ReactOnAddInspectionProductComposite(superProdEntity);
								}
								
								MyQuerry querry1=new MyQuerry();
								Vector<Filter> filtervec=new Vector<Filter>();
								Filter filter1 = new Filter();
								filter1.setQuerryString("prodCode");
								filter1.setStringValue(productInfoComposite.getProdCode().getValue().trim());
								filtervec.add(filter1);
								filter1=new Filter();
								filter1.setQuerryString("prodID");
								filter1.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue().trim()));
								filtervec.add(filter1);
								
								querry1.setFilters(filtervec);
								querry1.setQuerryObject(new PriceList());
									hideWaitSymbol();
									showWaitSymbol();
								async.getSearchResult(querry1, new AsyncCallback<ArrayList<SuperModel>>() {
									@Override
									public void onFailure(Throwable caught) {
										hideWaitSymbol();
									}
									
									@Override
									public void onSuccess(ArrayList<SuperModel> vresult) {
										boolean flage=false;
										if (vresult.size() != 0) {
											final ArrayList<PriceListDetails> list=new ArrayList<PriceListDetails>();
											ArrayList<VendorDetails> vendorDetailList = new ArrayList<VendorDetails>();
											for(SuperModel smodel:vresult){
												PriceList pentity=(PriceList) smodel;
												list.addAll(pentity.getPriceInfo());
											}
											for (PriceListDetails temp : list) {
												VendorDetails ve = new VendorDetails();
												ve.setVendorEmailId(temp.getEmail());
												ve.setVendorId(temp.getVendorID());
												ve.setVendorName(temp.getFullName());
												ve.setVendorPhone(temp.getCellNumber());
												vendorDetailList.add(ve);
											}
											
											for(int f=0;f<list.size();f++)
											{
												lis.setProdPrice(list.get(f).getProductPrice());
											}
											
											for(int i=0;i<vendorDetailList.size();i++){
												if(vendorDetailList.get(i).getVendorId()==Integer.parseInt(personInfoComposite.getId().getValue())){
													flage=true;
												}
											}
											if(flage==true){
												updateInspectionDetails(lis,Integer.parseInt(productInfoComposite.getProdID().getValue().trim()));
											}
											if(flage==false){
												showDialogMessage("Selected vendor does not supply this product.");
												productInfoComposite.clear();
											}
										}
										else{
											showDialogMessage("Vendor Price List does not exit for product.");
											productInfoComposite.clear();
										}
										hideWaitSymbol();
									}
								});
							}
						});
				
			}
		};
		timer.schedule(3000);
	}
	
	
	private InspectionDetails updateInspectionDetails(final InspectionDetails insdet,int prodId){
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("productinfo.prodID");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		
		querry.setQuerryObject(new ProductInventoryView());
		
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("An Unexpected error occurred!");
				}
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					for (SuperModel model1 : result) {
						ProductInventoryView piv = (ProductInventoryView) model1;
						
						if(!piv.getInspectionRequired().equals("")&&piv.getInspectionRequired()!=null){
							insdet.setInspectionRequired(piv.getInspectionRequired());
							table.getDataprovider().getList().add(insdet);
							productInfoComposite.clear();
						}
						else{
							insdet.setInspectionRequired(AppConstants.NO);
							table.getDataprovider().getList().add(insdet);
							productInfoComposite.clear();
						}
					}
				}
			});
		return insdet;
	}
	
	
	
	
	

/*******************************************Type Drop Down Logic**************************************/
	
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(oblInspectionCat))
		{
			if(oblInspectionCat.getSelectedIndex()!=0){
				ConfigCategory cat=oblInspectionCat.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(oblInspectiontype, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
		
	}
	
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if(event.getSource().equals(personInfoComposite.getId())||event.getSource().equals(personInfoComposite.getName())||event.getSource().equals(personInfoComposite.getPhone())){
			if(table.getDataprovider().getList().size()!=0){
				table.clear();
			}
		}
	}
	
	
	/******************************* Getter/Setter ******************************/
	
	
	
	public TextBox getIbGrnNo() {
		return ibGrnNo;
	}

	public void setIbGrnNo(TextBox ibGrnNo) {
		this.ibGrnNo = ibGrnNo;
	}

	public TextBox getTbGrnTitle() {
		return tbGrnTitle;
	}

	public void setTbGrnTitle(TextBox tbGrnTitle) {
		this.tbGrnTitle = tbGrnTitle;
	}

	public DateBox getDbGrndate() {
		return dbGrndate;
	}

	public void setDbGrndate(DateBox dbGrndate) {
		this.dbGrndate = dbGrndate;
	}

	public TextBox getTbRefNo() {
		return tbRefNo;
	}

	public void setTbRefNo(TextBox tbRefNo) {
		this.tbRefNo = tbRefNo;
	}

	public DateBox getDbRefDate() {
		return dbRefDate;
	}

	public void setDbRefDate(DateBox dbRefDate) {
		this.dbRefDate = dbRefDate;
	}

	public ObjectListBox<HrProject> getOlbProject() {
		return olbProject;
	}

	public void setOlbProject(ObjectListBox<HrProject> olbProject) {
		this.olbProject = olbProject;
	}

	public TextBox getIbInspectionId() {
		return ibInspectionId;
	}

	public void setIbInspectionId(TextBox ibInspectionId) {
		this.ibInspectionId = ibInspectionId;
	}

	public TextBox getTbInspectionTitle() {
		return tbInspectionTitle;
	}

	public void setTbInspectionTitle(TextBox tbInspectionTitle) {
		this.tbInspectionTitle = tbInspectionTitle;
	}

	public DateBox getDbInspectionDate() {
		return dbInspectionDate;
	}

	public void setDbInspectionDate(DateBox dbInspectionDate) {
		this.dbInspectionDate = dbInspectionDate;
	}

	public ObjectListBox<Config> getOblInspectionGroup() {
		return oblInspectionGroup;
	}

	public void setOblInspectionGroup(ObjectListBox<Config> oblInspectionGroup) {
		this.oblInspectionGroup = oblInspectionGroup;
	}

	public ObjectListBox<ConfigCategory> getOblInspectionCat() {
		return oblInspectionCat;
	}

	public void setOblInspectionCat(ObjectListBox<ConfigCategory> oblInspectionCat) {
		this.oblInspectionCat = oblInspectionCat;
	}

	public ObjectListBox<Type> getOblInspectiontype() {
		return oblInspectiontype;
	}

	public void setOblInspectiontype(ObjectListBox<Type> oblInspectiontype) {
		this.oblInspectiontype = oblInspectiontype;
	}

	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}

	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}

	public ObjectListBox<Employee> getOlbEmployee() {
		return olbEmployee;
	}

	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}

	public ObjectListBox<Employee> getOlbApprover() {
		return olbApprover;
	}

	public void setOlbApprover(ObjectListBox<Employee> olbApprover) {
		this.olbApprover = olbApprover;
	}

	public TextBox getTbStatus() {
		return tbStatus;
	}

	public void setTbStatus(TextBox tbStatus) {
		this.tbStatus = tbStatus;
	}

	public TextBox getTbInspectionRemark() {
		return tbInspectionRemark;
	}

	public void setTbInspectionRemark(TextBox tbInspectionRemark) {
		this.tbInspectionRemark = tbInspectionRemark;
	}

	public TextArea getTaDescription() {
		return taDescription;
	}

	public void setTaDescription(TextArea taDescription) {
		this.taDescription = taDescription;
	}

	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	public ProductInfoComposite getProductInfoComposite() {
		return productInfoComposite;
	}

	public void setProductInfoComposite(ProductInfoComposite productInfoComposite) {
		this.productInfoComposite = productInfoComposite;
	}

	public Button getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(Button btnAdd) {
		this.btnAdd = btnAdd;
	}

	public UploadComposite getUploadDocument() {
		return uploadDocument;
	}

	public void setUploadDocument(UploadComposite uploadDocument) {
		this.uploadDocument = uploadDocument;
	}

	public InspectionProductTable getTable() {
		return table;
	}

	public void setTable(InspectionProductTable table) {
		this.table = table;
	}
	
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		table.getTable().redraw();
	}
	
}
