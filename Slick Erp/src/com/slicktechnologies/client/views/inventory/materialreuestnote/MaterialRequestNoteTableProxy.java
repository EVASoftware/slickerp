package com.slicktechnologies.client.views.inventory.materialreuestnote;

import java.util.Comparator;
import java.util.List;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;

public class MaterialRequestNoteTableProxy extends SuperTable<MaterialRequestNote>{

	TextColumn<MaterialRequestNote> getColumnMrnId;
	TextColumn<MaterialRequestNote> getColumnMrnDate;
	TextColumn<MaterialRequestNote> getColumnMrnReqDate;
	TextColumn<MaterialRequestNote> getColumnMrnTitle;
	TextColumn<MaterialRequestNote> getColumnMrnCategory;
	TextColumn<MaterialRequestNote> getColumnMrnType;
	TextColumn<MaterialRequestNote> getColumnMrnSoId;
	TextColumn<MaterialRequestNote> getColumnMrnWoId;
	TextColumn<MaterialRequestNote> getColumnMrnProject;
	TextColumn<MaterialRequestNote> getColumnMrnBranch;
	TextColumn<MaterialRequestNote> getColumnMrnSalesPerson;
	TextColumn<MaterialRequestNote> getColumnMrnRequestedBy;
	TextColumn<MaterialRequestNote> getColumnMrnApprover;
	TextColumn<MaterialRequestNote> getColumnMrnStatus;
	
	public MaterialRequestNoteTableProxy() {
		super();
	}
	
	@Override
	public void createTable() {
		addColumnMrnId();
		addColumnMrnTitle();
		addColumnMrnDate();
		addColumnMrnRequiredDate();
		addColumnMrnWoId();
		addColumnMrnSoId();
		addColumnMrnCategory();
		addColumnMrnType();
		addColumnMrnSalesPerson();
		addColumnMrnRequestedBy();
		addColumnMrnApprover();
		addColumnMrnProject();
		addColumnMrnBranch();
		addColumnMrnStatus();
		
	}
	
	private void addColumnMrnId() {
		getColumnMrnId = new TextColumn<MaterialRequestNote>() {
			@Override
			public String getValue(MaterialRequestNote object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getColumnMrnId, "MRN ID");
		table.setColumnWidth(getColumnMrnId,90,Unit.PX);
		getColumnMrnId.setSortable(true);
	}

	private void addColumnMrnDate() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
//		DatePickerCell date = new DatePickerCell(fmt);
		getColumnMrnDate = new TextColumn<MaterialRequestNote>() {
			@Override
			public String getValue(MaterialRequestNote object) {
				if (object.getMrnDate()!= null) {
					return fmt.format(object.getMrnDate());
				}
				return null;
			}
		};
		table.addColumn(getColumnMrnDate, "MRN Date");
		table.setColumnWidth(getColumnMrnDate,120,Unit.PX);
		getColumnMrnDate.setSortable(true);
	}
	
	private void addColumnMrnRequiredDate() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
//		DatePickerCell date = new DatePickerCell(fmt);
		getColumnMrnReqDate = new TextColumn<MaterialRequestNote>() {
			@Override
			public String getValue(MaterialRequestNote object) {
				if (object.getMrnRequiredDate()!= null) {
					return fmt.format(object.getMrnRequiredDate());
				}
				return null;
			}
		};
		table.addColumn(getColumnMrnReqDate, "Material Required Date");
		table.setColumnWidth(getColumnMrnReqDate,200,Unit.PX);
		getColumnMrnReqDate.setSortable(true);
	}
	
	private void addColumnMrnTitle() {
		getColumnMrnTitle = new TextColumn<MaterialRequestNote>() {
			@Override
			public String getValue(MaterialRequestNote object) {
				if (object.getMrnTitle()!=null){
					return object.getMrnTitle();
				}
				return null;
			}
		};
		table.addColumn(getColumnMrnTitle, "MRN Title");
		table.setColumnWidth(getColumnMrnTitle,110,Unit.PX);
		getColumnMrnTitle.setSortable(true);
	}

	private void addColumnMrnCategory() {
		getColumnMrnCategory = new TextColumn<MaterialRequestNote>() {
			@Override
			public String getValue(MaterialRequestNote object) {
				if (object.getMrnCategory()!=null){
					return object.getMrnCategory();
				}
				return null;
			}
		};
		table.addColumn(getColumnMrnCategory, "MRN Category");
		table.setColumnWidth(getColumnMrnCategory,130,Unit.PX);
		getColumnMrnCategory.setSortable(true);
	}

	private void addColumnMrnType() {
		getColumnMrnType = new TextColumn<MaterialRequestNote>() {
			@Override
			public String getValue(MaterialRequestNote object) {
				if (object.getMrnType()!=null){
					return object.getMrnType();
				}
				return null;
			}
		};
		table.addColumn(getColumnMrnType, "MRN Type");
		table.setColumnWidth(getColumnMrnType,130,Unit.PX);
		getColumnMrnType.setSortable(true);
	}

	private void addColumnMrnSoId() {
		getColumnMrnSoId = new TextColumn<MaterialRequestNote>() {
			@Override
			public String getValue(MaterialRequestNote object) {
				if (object.getMrnSoId() == -1)
					return "";
				else
					return object.getMrnSoId() + "";
			}
		};
		table.addColumn(getColumnMrnSoId, "Order ID");
		table.setColumnWidth(getColumnMrnSoId,135,Unit.PX);
		getColumnMrnSoId.setSortable(true);
	}

	private void addColumnMrnWoId() {
		getColumnMrnWoId = new TextColumn<MaterialRequestNote>() {
			@Override
			public String getValue(MaterialRequestNote object) {
				if (object.getMrnWoId() == -1)
					return "";
				else
					return object.getMrnWoId() + "";
			}
		};
		table.addColumn(getColumnMrnWoId, "Work Order ID");
		table.setColumnWidth(getColumnMrnWoId,135,Unit.PX);
		getColumnMrnWoId.setSortable(true);
	}

	private void addColumnMrnProject() {
		getColumnMrnProject = new TextColumn<MaterialRequestNote>() {
			@Override
			public String getValue(MaterialRequestNote object) {
				if (object.getMrnProject()!=null){
					return object.getMrnProject();
				}
				return null;
			}
		};
		table.addColumn(getColumnMrnProject, "Project");
		table.setColumnWidth(getColumnMrnProject,90,Unit.PX);
		getColumnMrnProject.setSortable(true);
	}

	private void addColumnMrnBranch() {
		getColumnMrnBranch = new TextColumn<MaterialRequestNote>() {
			@Override
			public String getValue(MaterialRequestNote object) {
				if (object.getBranch()!=null){
					return object.getBranch();
				}
				return null;
			}
		};
		table.addColumn(getColumnMrnBranch, "Branch");
		table.setColumnWidth(getColumnMrnBranch,90,Unit.PX);
		getColumnMrnBranch.setSortable(true);
	}

	private void addColumnMrnSalesPerson() {
		getColumnMrnSalesPerson = new TextColumn<MaterialRequestNote>() {
			@Override
			public String getValue(MaterialRequestNote object) {
				if (object.getMrnSalesPerson()!=null){
					return object.getMrnSalesPerson();
				}
				return null;
			}
		};
		table.addColumn(getColumnMrnSalesPerson, "Sales Person");
		table.setColumnWidth(getColumnMrnSalesPerson,117,Unit.PX);
		getColumnMrnSalesPerson.setSortable(true);
	}

	private void addColumnMrnRequestedBy() {
		getColumnMrnRequestedBy = new TextColumn<MaterialRequestNote>() {
			@Override
			public String getValue(MaterialRequestNote object) {
				if (object.getEmployee()!=null){
					return object.getEmployee();
				}
				return null;
			}
		};
		table.addColumn(getColumnMrnRequestedBy, "Requested By");
		table.setColumnWidth(getColumnMrnRequestedBy,117,Unit.PX);
		getColumnMrnRequestedBy.setSortable(true);
	}

	private void addColumnMrnApprover() {
		getColumnMrnApprover = new TextColumn<MaterialRequestNote>() {
			@Override
			public String getValue(MaterialRequestNote object) {
				if (object.getApproverName()!=null){
					return object.getApproverName();
				}
				return null;
			}
		};
		table.addColumn(getColumnMrnApprover, "Approver");
		table.setColumnWidth(getColumnMrnApprover,90,Unit.PX);
		getColumnMrnApprover.setSortable(true);
	}

	private void addColumnMrnStatus() {
		getColumnMrnStatus = new TextColumn<MaterialRequestNote>() {
			@Override
			public String getValue(MaterialRequestNote object) {
				if (object.getStatus()!=null){
					return object.getStatus();
				}
				return null;
			}
		};
		table.addColumn(getColumnMrnStatus, "Status");
		table.setColumnWidth(getColumnMrnStatus,90,Unit.PX);
		getColumnMrnStatus.setSortable(true);
	}

	public void addColumnSorting() {
		addColumnSortingMrnId();
		addColumnSortingMrnDate();
		addColumnSortingMrnReqDate();
		addColumnSortingMrnTitle();
		addColumnSortingMrnCategory();
		addColumnSortingMrnType();
		addColumnSortingMrnProject();
		addColumnSortingMrnBranch();
		addColumnSortingMrnSalesPerson();
		addColumnSortingMrnRequestedBy();
		addColumnSortingMrnApprover();
		addColumnSortingMrnSoId();
		addColumnSortingMrnWoId();
		addColumnSortingMrnStatus();
	}	

	/***********************************************Column Sorting***************************************/	
	private void addColumnSortingMrnId() {
		List<MaterialRequestNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialRequestNote>(list);
		columnSort.setComparator(getColumnMrnId, new Comparator<MaterialRequestNote>() {
			@Override
			public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMrnDate() {
		List<MaterialRequestNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialRequestNote>(list);
		columnSort.setComparator(getColumnMrnDate, new Comparator<MaterialRequestNote>() {
			@Override
			public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMrnDate() != null && e2.getMrnDate() != null) {
						return e1.getMrnDate().compareTo(e2.getMrnDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnSortingMrnReqDate() {
		List<MaterialRequestNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialRequestNote>(list);
		columnSort.setComparator(getColumnMrnReqDate, new Comparator<MaterialRequestNote>() {
			@Override
			public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMrnRequiredDate() != null && e2.getMrnRequiredDate() != null) {
						return e1.getMrnRequiredDate().compareTo(e2.getMrnRequiredDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	private void addColumnSortingMrnSoId() {
		List<MaterialRequestNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialRequestNote>(list);
		columnSort.setComparator(getColumnMrnSoId, new Comparator<MaterialRequestNote>() {
			@Override
			public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMrnSoId() == e2.getMrnSoId()) {
						return 0;
					}
					if (e1.getMrnSoId() > e2.getMrnSoId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMrnWoId() {
		List<MaterialRequestNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialRequestNote>(list);
		columnSort.setComparator(getColumnMrnWoId, new Comparator<MaterialRequestNote>() {
			@Override
			public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMrnWoId() == e2.getMrnWoId()) {
						return 0;
					}
					if (e1.getMrnWoId() > e2.getMrnWoId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMrnTitle() {
		List<MaterialRequestNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialRequestNote>(list);
		columnSort.setComparator(getColumnMrnTitle, new Comparator<MaterialRequestNote>() {
			@Override
			public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMrnTitle() != null && e2.getMrnTitle() != null) {
						return e1.getMrnTitle().compareTo(e2.getMrnTitle());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMrnCategory() {
		List<MaterialRequestNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialRequestNote>(list);
		columnSort.setComparator(getColumnMrnCategory, new Comparator<MaterialRequestNote>() {
			@Override
			public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMrnCategory() != null && e2.getMrnCategory() != null) {
						return e1.getMrnCategory().compareTo(e2.getMrnCategory());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMrnType() {
		List<MaterialRequestNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialRequestNote>(list);
		columnSort.setComparator(getColumnMrnType, new Comparator<MaterialRequestNote>() {
			@Override
			public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMrnType() != null && e2.getMrnType() != null) {
						return e1.getMrnType().compareTo(e2.getMrnType());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnSortingMrnProject() {
		List<MaterialRequestNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialRequestNote>(list);
		columnSort.setComparator(getColumnMrnProject, new Comparator<MaterialRequestNote>() {
			@Override
			public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMrnProject() != null && e2.getMrnProject() != null) {
						return e1.getMrnProject().compareTo(e2.getMrnProject());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMrnBranch() {
		List<MaterialRequestNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialRequestNote>(list);
		columnSort.setComparator(getColumnMrnBranch, new Comparator<MaterialRequestNote>() {
			@Override
			public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null && e2.getBranch() != null) {
						return e1.getBranch().compareTo(e2.getBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMrnSalesPerson() {
		List<MaterialRequestNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialRequestNote>(list);
		columnSort.setComparator(getColumnMrnSalesPerson, new Comparator<MaterialRequestNote>() {
			@Override
			public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getMrnSalesPerson() != null && e2.getMrnSalesPerson() != null) {
						return e1.getMrnSalesPerson().compareTo(e2.getMrnSalesPerson());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMrnRequestedBy() {
		List<MaterialRequestNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialRequestNote>(list);
		columnSort.setComparator(getColumnMrnRequestedBy, new Comparator<MaterialRequestNote>() {
			@Override
			public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployee() != null && e2.getEmployee() != null) {
						return e1.getEmployee().compareTo(e2.getEmployee());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMrnApprover() {
		List<MaterialRequestNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialRequestNote>(list);
		columnSort.setComparator(getColumnMrnApprover, new Comparator<MaterialRequestNote>() {
			@Override
			public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getApproverName() != null && e2.getApproverName() != null) {
						return e1.getApproverName().compareTo(e2.getApproverName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingMrnStatus() {
		List<MaterialRequestNote> list = getDataprovider().getList();
		columnSort = new ListHandler<MaterialRequestNote>(list);
		columnSort.setComparator(getColumnMrnStatus, new Comparator<MaterialRequestNote>() {
			@Override
			public int compare(MaterialRequestNote e1, MaterialRequestNote e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null && e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}
}
