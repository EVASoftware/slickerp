package com.slicktechnologies.client.views.inventory.materialreuestnote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNPresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.InventoryLocationPopUp;
import com.slicktechnologies.client.views.popups.ProductSerailNumberPopup;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class SubProductTableMrn extends SuperTable<MaterialProduct> implements ClickHandler {
	static double availableQty;
	static double availableQuantity;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	InventoryLocationPopUp invLoc=new InventoryLocationPopUp();
	PopupPanel panel=new PopupPanel(true);
	int rowIndex;
	
	TextColumn<MaterialProduct> getColumnProductId;
	TextColumn<MaterialProduct> getColumnProductCode;
	TextColumn<MaterialProduct> getColumnProductName;
	TextColumn<MaterialProduct> getColumnProductAvailableQty;
	TextColumn<MaterialProduct> getViewColumnProductRequiredQty;
	Column<MaterialProduct, String> getColumnProductRequiredQty;
	TextColumn<MaterialProduct> getColumnProductUom;
	Column<MaterialProduct, String> getColumnProductRemarks;
	TextColumn<MaterialProduct> getColumnProductViewRemarks;
	Column<MaterialProduct, String> getColumnProductDeleteButton;
	Column<MaterialProduct, String> addColumn;
	
	public  ArrayList<String> warehouseList;
	public  ArrayList<String> storageBinList;
	public  ArrayList<String> storagelocList;
	
	public  ArrayList<Storagebin> storagelocList1;
	
	Column<MaterialProduct, String> warehouseColumn;
	TextColumn<MaterialProduct> warehouseViewColumn;
	
	Column<MaterialProduct, String> storagebin;
	Column<MaterialProduct, String> columnviewBin;
	
	Column<MaterialProduct, String> storageLoc;
	TextColumn<MaterialProduct> columnviewLoc;
	/**
	 * nidhi ||*|| 29-12-2018
	 */
	TextColumn<MaterialProduct> getViewColumnProductPlannedQty;
	
	/**
	 * Date 23-04-2019 by Vijay for NBHC IM
	 * Adding serial number
	 */
	Column<MaterialProduct, String> addProSerialNo;
	Column<MaterialProduct, String> viewProSerialNo;
	boolean tableState = false;
	ProductSerailNumberPopup prodSerialPopup = new ProductSerailNumberPopup(true,false);
	/**
	 * ends here
	 */
	
	public SubProductTableMrn() {
		invLoc.getAddButton().addClickHandler(this);
		invLoc.getCancelButton().addClickHandler(this);
		
		/**
		 * Date 23-04-2019 by Vijay for NBHC IM
		 * Adding serial number
		 */
		prodSerialPopup.getLblOk().addClickHandler(this);
		prodSerialPopup.getLblCancel().addClickHandler(this);
	}
	
	
	@Override
	public void createTable() {
		
		addColumnProductName();
		addColumnProductUom();
		addColumnProductAvailableQty();
		/**
		 *nidhi ||*|| 29-12-2018 for display planned qty
		 */
		if(LoginPresenter.billofMaterialActive){
			addColumnProductViewPlannedQty();
		}
		addColumnProductRequiredQty();
		
		addColumnProductRemarks();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		createColumnAddColumn();
		
		/**
		 * Date 23-04-2019 by Vijay NBHC IM
		 * for add serial number
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createColumnAddProSerialNoColumn();
		}
		/** ends here ***/
		
		addColumnProductCode();
		addColumnProductId();
		addColumnProductDeleteButton();
		addFieldUpdater();
		
	}

	public void addViewColumn(){
		addColumnProductId();
		addColumnProductCode();
		addColumnProductName();
		/**
		 *nidhi ||*|| 29-12-2018 for display planned qty
		 */
		if(LoginPresenter.billofMaterialActive){
			addColumnProductViewPlannedQty();
		}
		addColumnProductViewRequiredQty();
		addColumnProductAvailableQty();
		addColumnProductUom();
		addColumnProductViewRemarks();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		
		/**
		 * Date 23-04-2019 by Vijay NBHC IM
		 * added serial number functionality
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createViewColumnAddProSerialNoColumn();
		}
		addFieldUpdater();

	}

	public void addEditColumn(){
		addColumnProductId();
		addColumnProductCode();
		addColumnProductName();
		/**
		 *nidhi ||*|| 29-12-2018 for display planned qty
		 */
		if(LoginPresenter.billofMaterialActive){
			addColumnProductViewPlannedQty();
		}
		addColumnProductRequiredQty();
		addColumnProductAvailableQty();
		addColumnProductUom();
		addColumnProductRemarks();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		createColumnAddColumn();
		
		/**
		 * Date 23-04-2019 by Vijay NBHC IM
		 * added serial number functionality
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createColumnAddProSerialNoColumn();
		}
		
		addColumnProductDeleteButton();
		addFieldUpdater();
	}
	
	@Override
	public void addFieldUpdater() {
		addColumnDeleteUpdater();
		addColumnProductQtyUpdater();
		createFieldUpdaterAdd();
		addColumnRemarkUpdater();
		
		/**
		 * Date 23-04-2019 by Vijay NBHC IM
		 * added serial number functionality
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			if(addProSerialNo!=null)
			createFieldUpdaterAddSerialNo();
			if(viewProSerialNo!=null)
			createFieldUpdaterViewSerialNo();
		}
	}

	
	/**
	 * Develpoed By : Rohan Bhagde
	 * Date : 16/11/2016
	 * Reason: This is used to lock all the fields in table when we issue material from project. 
	 */
	

	
	public void setEnableOnlySelectWarehouseButton() {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
		table.removeColumn(i);
		
		addColumnProductId();
		addColumnProductCode();
		addColumnProductName();
		/**
		 *nidhi ||*|| 29-12-2018 for display planned qty
		 */
		if(LoginPresenter.billofMaterialActive){
			addColumnProductViewPlannedQty();
		}
		addColumnProductViewRequiredQty();
		addColumnProductAvailableQty();
		addColumnProductUom();
		addColumnProductViewRemarks();
		viewWarehouse();
		viewstarageLoc();
		viewstorageBin();
		createColumnAddColumn();
		addFieldUpdater();
	}


	/**
	 * ends here 
	 */

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addEditColumn();
		if (state == false)
			addViewColumn();
	}
	
	
	public void createColumnAddColumn() {
		ButtonCell btnCell = new ButtonCell();
		addColumn = new Column<MaterialProduct, String>(btnCell) {
			@Override
			public String getValue(MaterialProduct object) {
				return "Select Warehouse";
			}
		};
		table.addColumn(addColumn, "");
		table.setColumnWidth(addColumn,180,Unit.PX);

	}
	
	
		
	private void addColumnProductQtyUpdater() {
		getColumnProductRequiredQty.setFieldUpdater(new FieldUpdater<MaterialProduct, String>() {
			@Override
			public void update(int index, MaterialProduct object,String value) {
				try {
					double val1 = Double.parseDouble(value.trim());
					object.setMaterialProductRequiredQuantity(val1);
					/*** Date 16-09-2019 by Vijay For NBHC IM ****/
					object.setMaterialProductBalanceQty(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
				
				/////////////////////
				
			if(object.getMaterialProductAvailableQuantity()!=0){
				double reqQty=object.getMaterialProductRequiredQuantity();
				double avlQty=object.getMaterialProductAvailableQuantity();
				double reOdrQty=object.getMatReqReOdrLvlQty();
				double diff=avlQty-reqQty;
				String location=object.getMaterialProductWarehouse()+"/"
						+object.getMaterialProductStorageLocation()+"/"
						+object.getMaterialProductStorageBin();
							
				System.out.println("Reorder QTy in OTY UPD : "+reOdrQty);
				if(reOdrQty!=0){
					System.out.println("Reorder QTy in OTY UPD 1 : "+reOdrQty);
					if(avlQty<=reOdrQty){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Available quantity in warehouse "+location+" is less than reorder level!");
					}else if(diff<=reOdrQty){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Available quantity in warehouse "+location+" is less than reorder level!");
					}
				}
			}
						
			//////////////
			}
		});
	}
	
	private void addColumnRemarkUpdater() {
		getColumnProductRemarks.setFieldUpdater(new FieldUpdater<MaterialProduct, String>() {
			@Override
			public void update(int index, MaterialProduct object, String value) {
				object.setMaterialProductRemarks(value);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}

	private void addColumnDeleteUpdater() {
		getColumnProductDeleteButton.setFieldUpdater(new FieldUpdater<MaterialProduct, String>() {
			@Override
			public void update(int index, MaterialProduct object,String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	/****************************************** Editable Column  ***********************************************/
	private void addColumnProductDeleteButton() {
		ButtonCell btnCell = new ButtonCell();
		getColumnProductDeleteButton = new Column<MaterialProduct, String>(btnCell) {
			@Override
			public String getValue(MaterialProduct object) {
				return "Delete";
			}
		};
		table.addColumn(getColumnProductDeleteButton, "Delete");
		table.setColumnWidth(getColumnProductDeleteButton,60,Unit.PX);
	}
	
	private void addColumnProductRequiredQty() {
		EditTextCell editCell = new EditTextCell();
		getColumnProductRequiredQty=new Column<MaterialProduct, String>(editCell) {
			@Override
			public String getValue(MaterialProduct object) {
				return nf.format(object.getMaterialProductRequiredQuantity());
			}
		};
		table.addColumn(getColumnProductRequiredQty, "#Quantity");
		table.setColumnWidth(getColumnProductRequiredQty,81,Unit.PX);
	}

	private void addColumnProductRemarks() {
		EditTextCell editCell = new EditTextCell();
		getColumnProductRemarks=new Column<MaterialProduct,String>(editCell) {
			@Override
			public String getValue(MaterialProduct object) {
				return object.getMaterialProductRemarks();
			}
		};
		table.addColumn(getColumnProductRemarks, "#Remarks");
		table.setColumnWidth(getColumnProductRemarks,90,Unit.PX);
	}
	
	/***************************************** View Column **********************************************************/
	
	private void addColumnProductViewRemarks() {
		getColumnProductViewRemarks=new TextColumn<MaterialProduct>() {
			@Override
			public String getValue(MaterialProduct object) {
				return object.getMaterialProductRemarks();
			}
		};
		table.addColumn(getColumnProductViewRemarks, "#Remarks");
		table.setColumnWidth(getColumnProductViewRemarks,90,Unit.PX);
	}
	
	public void viewWarehouse() {
		warehouseViewColumn = new TextColumn<MaterialProduct>() {
			@Override
			public String getValue(MaterialProduct object) {
				return object.getMaterialProductWarehouse();
			}
		};
		table.addColumn(warehouseViewColumn, "Warehouse");
		table.setColumnWidth(warehouseViewColumn,153,Unit.PX);
	}
	
	public void viewstarageLoc() {
		columnviewLoc = new TextColumn<MaterialProduct>() {
			@Override
			public String getValue(MaterialProduct object) {
				return object.getMaterialProductStorageLocation();
			}
		};
		table.addColumn(columnviewLoc, "Storage Location");
		table.setColumnWidth(columnviewLoc,153,Unit.PX);
	}
	public void viewstorageBin() {
		columnviewBin = new TextColumn<MaterialProduct>() {
			@Override
			public String getValue(MaterialProduct object) {
				return object.getMaterialProductStorageBin();
			}
		};
		table.addColumn(columnviewBin, "Storage Bin");
		table.setColumnWidth(columnviewBin,153,Unit.PX);
	}
	
	private void addColumnProductUom() {
		getColumnProductUom=new TextColumn<MaterialProduct>() {
			@Override
			public String getValue(MaterialProduct object) {
				return object.getMaterialProductUOM();
			}
		};
		table.addColumn(getColumnProductUom, "UOM");
		table.setColumnWidth(getColumnProductUom,45,Unit.PX);
	}

	private void addColumnProductAvailableQty() {
		getColumnProductAvailableQty=new TextColumn<MaterialProduct>() {
			@Override
			public String getValue(MaterialProduct object) {
				return nf.format(object.getMaterialProductAvailableQuantity());
			}
		};
		table.addColumn(getColumnProductAvailableQty, "Stock");
		table.setColumnWidth(getColumnProductAvailableQty,180,Unit.PX);
	}

	private void addColumnProductViewRequiredQty() {
		getViewColumnProductRequiredQty=new TextColumn<MaterialProduct>() {
			
			@Override
			public String getValue(MaterialProduct object) {
				return nf.format(object.getMaterialProductRequiredQuantity());
			}
		};
		table.addColumn(getViewColumnProductRequiredQty,"#Quantity");
		table.setColumnWidth(getViewColumnProductRequiredQty,81,Unit.PX);
	}

	private void addColumnProductName() {
		getColumnProductName=new TextColumn<MaterialProduct>() {
			@Override
			public String getValue(MaterialProduct object) {
				return object.getMaterialProductName();
			}
		};
		table.addColumn(getColumnProductName, "Name");
		table.setColumnWidth(getColumnProductName,153,Unit.PX);
	}

	private void addColumnProductCode() {
		getColumnProductCode=new TextColumn<MaterialProduct>() {
			@Override
			public String getValue(MaterialProduct object) {
				return object.getMaterialProductCode();
			}
		};
		table.addColumn(getColumnProductCode, "Code");
		table.setColumnWidth(getColumnProductCode,90,Unit.PX);
	}

	private void addColumnProductId() {
		getColumnProductId=new TextColumn<MaterialProduct>() {
			@Override
			public String getValue(MaterialProduct object) {
				return object.getMaterialProductId()+"";
			}
		};
		table.addColumn(getColumnProductId, "ID");
		table.setColumnWidth(getColumnProductId,100,Unit.PX);
	}
	
	
	
			public void createFieldUpdaterAdd() {
				addColumn.setFieldUpdater(new FieldUpdater<MaterialProduct, String>() {
					@Override
					public void update(int index, MaterialProduct object, String value) {
						
						panel=new PopupPanel(true);
						panel.add(invLoc);
						InventoryLocationPopUp.initialzeWarehouse(object.getMaterialProductId());
						
						if(object.getMaterialProductWarehouse()!=null&&object.getMaterialProductStorageLocation()!=null
								&&object.getMaterialProductStorageBin()!=null){
							System.out.println("Inside table not null list settt...........");
							for(int i=0;i<InventoryLocationPopUp.getLsWarehouse().getItemCount();i++)
							{
								String data=InventoryLocationPopUp.lsWarehouse.getItemText(i);
								if(data.equals(object.getMaterialProductWarehouse()))
								{
									InventoryLocationPopUp.lsWarehouse.setSelectedIndex(i);
									break;
								}
							}
							
							invLoc.getLsStorageLoc().clear();
							invLoc.lsStorageLoc.addItem("--SELECT--");
							invLoc.lsStorageLoc.addItem(object.getMaterialProductStorageLocation());
							
							
							invLoc.getLsStoragebin().clear();
							invLoc.lsStoragebin.addItem("--SELECT--");
							
							for(int i=0;i<invLoc.arraylist2.size();i++){
								if(invLoc.arraylist2.get(i).getWarehouseName().equals(object.getMaterialProductWarehouse())
										&&invLoc.arraylist2.get(i).getStorageLocation().equals(object.getMaterialProductStorageLocation())){
									invLoc.lsStoragebin.addItem(invLoc.arraylist2.get(i).getStorageBin());
								}
							}
							
							for(int i=0;i<invLoc.getLsStorageLoc().getItemCount();i++)
							{
								String data=invLoc.lsStorageLoc.getItemText(i);
								if(data.equals(object.getMaterialProductStorageLocation()))
								{
									invLoc.lsStorageLoc.setSelectedIndex(i);
									break;
								}
							}
							for(int i=0;i<invLoc.getLsStoragebin().getItemCount();i++)
							{
								String data=invLoc.lsStoragebin.getItemText(i);
								if(data.equals(object.getMaterialProductStorageBin()))
								{
									invLoc.lsStoragebin.setSelectedIndex(i);
									break;
								}
							}
						}
						
						
						
						invLoc.formView();
						panel.show();
						panel.center();
						rowIndex=index;
					}
				});
			}

			@Override
			public void onClick(ClickEvent event) {
				if(event.getSource()==invLoc.getAddButton())
				{	
					if(invLoc.getLsWarehouse().getSelectedIndex()!=0&&invLoc.getLsStorageLoc().getSelectedIndex()!=0&&invLoc.getLsStoragebin().getSelectedIndex()!=0){
						ArrayList<MaterialProduct> list=new ArrayList<MaterialProduct>();
						if(getDataprovider().getList().size()!=0){
							list.addAll(getDataprovider().getList());
							for( int i=rowIndex;i<getDataprovider().getList().size();i++){
								list.get(rowIndex).setMaterialProductWarehouse(invLoc.getLsWarehouse().getValue(invLoc.getLsWarehouse().getSelectedIndex()));
								list.get(rowIndex).setMaterialProductStorageLocation(invLoc.getLsStorageLoc().getValue(invLoc.getLsStorageLoc().getSelectedIndex()));
								list.get(rowIndex).setMaterialProductStorageBin(invLoc.getLsStoragebin().getValue(invLoc.getLsStoragebin().getSelectedIndex()));
								list.get(rowIndex).setMaterialProductAvailableQuantity(invLoc.getAvailableQty());
								

								/**
								 * Added by Anil On 26-08-2016
								 * If available qty is below reorder level then generate alert msg. 
								 */
								
								list.get(rowIndex).setMatReqReOdrLvlQty(invLoc.getReorderQty());
								getDataprovider().getList().clear();
								getDataprovider().getList().addAll(list);
								
								/////////////////////
								
								double reqQty=list.get(rowIndex).getMaterialProductRequiredQuantity();
								double avlQty=list.get(rowIndex).getMaterialProductAvailableQuantity();
								double reOdrQty=list.get(rowIndex).getMatReqReOdrLvlQty();
								double diff=avlQty-reqQty;
								String location=list.get(rowIndex).getMaterialProductWarehouse()+"/"
										+list.get(rowIndex).getMaterialProductStorageLocation()+"/"
										+list.get(rowIndex).getMaterialProductStorageBin();
								
								System.out.println("REorder Qty in Select : "+reOdrQty);
								
								if(reOdrQty!=0){
									System.out.println("REorder Qty in Select1 : "+reOdrQty);
									if(avlQty<=reOdrQty){
										GWTCAlert alert=new GWTCAlert();
										alert.alert("Available quantity in warehouse "+location+" is less than reorder level!");
									}else if(diff<=reOdrQty){
										GWTCAlert alert=new GWTCAlert();
										alert.alert("Available quantity in warehouse "+location+" is less than reorder level!");
									}
								}
							}
						}
						panel.hide();
					}
					if(invLoc.getLsWarehouse().getSelectedIndex()==0&&invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
						MaterialRequestNotePresenter.showMessage("Please Select Warehouse.");
					}
					else if(invLoc.getLsStorageLoc().getSelectedIndex()==0&&invLoc.getLsStoragebin().getSelectedIndex()==0){
						MaterialRequestNotePresenter.showMessage("Please Select Storage Location.");
					}
					else if(invLoc.getLsStoragebin().getSelectedIndex()==0){
						MaterialRequestNotePresenter.showMessage("Please Select Storage Bin.");
					}
					
				}
				if(event.getSource()==invLoc.getCancelButton())
				{	
					panel.hide();
				}
				
				/**
				 * Date 25-04-2019 by Vijay
				 * For Serial number functionality
				 */
				if(event.getSource() == prodSerialPopup.getLblOk()){
					ArrayList<ProductSerialNoMapping> proList = new ArrayList<ProductSerialNoMapping>();
					List<ProductSerialNoMapping> mainproList = prodSerialPopup.getProSerNoTable().getDataprovider().getList();
					
					
					ArrayList<MaterialProduct> list=new ArrayList<MaterialProduct>();
					list.addAll(getDataprovider().getList());
					/*if(mainproList != null  && mainproList.size()>0 && mainproList.size()!= (int) list.get(rowIndex).getProductQuantity())
					{
						final GWTCAlert alert = new GWTCAlert(); 
					     alert.alert("Please Add all Serial no.");
					     return ; 
					}*/
					int count = 0;
					for(int i =0 ; i <mainproList.size();i++){
						System.out.println(mainproList.get(i).getProSerialNo() + "  --- get serial number -- ");
						if(mainproList.get(i).getProSerialNo().trim().length()>0){
							proList.add(mainproList.get(i));
							if(mainproList.get(i).isStatus()){
								count++;
							}
						}
					}
					
					if(list.get(rowIndex).getProSerialNoDetails()!=null){
						list.get(rowIndex).getProSerialNoDetails().clear();
						list.get(rowIndex).getProSerialNoDetails().put(0, proList);
						list.get(rowIndex).setMaterialProductRequiredQuantity(count);
						getDataprovider().getList().clear();
						getDataprovider().getList().addAll(list);
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					}else{
						HashMap<Integer, ArrayList<ProductSerialNoMapping>> pro = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
						pro.put(0, proList);
							list.get(rowIndex).setProSerialNoDetails(pro);
							list.get(rowIndex).setMaterialProductRequiredQuantity(count);
							getDataprovider().getList().clear();
							getDataprovider().getList().addAll(list);
							RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					}
					prodSerialPopup.hidePopUp();
				}
				if(event.getSource()== prodSerialPopup.getLblCancel()){
					prodSerialPopup.hidePopUp();
				}
				/**
				 * ends here
				 */
			}
	
	/*****************************************************************************************************************/

	@Override
	public void applyStyle() {
		
	}
	

	@Override
	protected void initializekeyprovider() {
		
	}
	
	/**
	 * nidhi ||*||
	 */
	
	
	private void addColumnProductViewPlannedQty() {
		getViewColumnProductPlannedQty=new TextColumn<MaterialProduct>() {
			
			@Override
			public String getValue(MaterialProduct object) {
				return nf.format(object.getMaterialProductPlannedQuantity());
			}
		};
		table.addColumn(getViewColumnProductPlannedQty,"Planned Quantity");
		table.setColumnWidth(getViewColumnProductPlannedQty,81,Unit.PX);
	}
	
	
	/**
	 * Date 23-04-2019 by Vijay
	 * Des :- Serial number functionality added
	 */
	public void createColumnAddProSerialNoColumn() {
		ButtonCell btnCell = new ButtonCell();
		addProSerialNo = new Column<MaterialProduct, String>(btnCell) {
			@Override
			public String getValue(MaterialProduct object) {
				return "Product Serial Nos";
			}
		};
		table.addColumn(addProSerialNo, "");
		table.setColumnWidth(addProSerialNo,180,Unit.PX);

	}
	public void createFieldUpdaterAddSerialNo() {
		addProSerialNo.setFieldUpdater(new FieldUpdater<MaterialProduct, String>() {
			@Override
			public void update(int index, MaterialProduct object, String value) {
				rowIndex = index;
				prodSerialPopup.setProSerialenble(true,true);
				prodSerialPopup.getPopup().setSize("50%", "40%");
				if(object.getProSerialNoDetails()!=null && object.getProSerialNoDetails().size()>0){
					ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
					if(object.getProSerialNoDetails().containsKey(0)){
						serNo.addAll(object.getProSerialNoDetails().get(0));
					}
					if(serNo.size() < object.getMaterialProductRequiredQuantity()){
						int count = (int) (object.getMaterialProductRequiredQuantity()- serNo.size());
						for(int i = 0 ; i < count ; i ++ ){
							ProductSerialNoMapping pro = new ProductSerialNoMapping();
							serNo.add(pro);
						}
					}
					prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
					prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
					prodSerialPopup.getProSerNoTable().getTable().redraw();
				}else{

					ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
						for(int i = 0 ; i < object.getMaterialProductRequiredQuantity() ; i ++ ){
							ProductSerialNoMapping pro = new ProductSerialNoMapping();
							pro.setProSerialNo("");
							serNo.add(pro);
						}
						prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
						prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
						prodSerialPopup.getProSerNoTable().getTable().redraw();
				}
				prodSerialPopup.showPopUp();
			
			}
		});
	}
	
	public void createFieldUpdaterViewSerialNo() {
		viewProSerialNo.setFieldUpdater(new FieldUpdater<MaterialProduct, String>() {
			@Override
			public void update(int index, MaterialProduct object, String value) {
				prodSerialPopup.setProSerialenble(false,false);
				prodSerialPopup.getPopup().setSize("50%", "40%");
				if(object.getProSerialNoDetails().size()>0){
					ArrayList<ProductSerialNoMapping> serNo = new ArrayList<ProductSerialNoMapping>();
				System.out.println("size  -- " + serNo.size());
					if(object.getProSerialNoDetails().containsKey(0)){
						serNo.addAll(object.getProSerialNoDetails().get(0));
					}
						prodSerialPopup.getProSerNoTable().getDataprovider().getList().clear();
					prodSerialPopup.getProSerNoTable().getDataprovider().getList().addAll(serNo);
					prodSerialPopup.getProSerNoTable().getTable().redraw();
				}
				prodSerialPopup.showPopUp();
			
			}
		});
	}
	
	public void createViewColumnAddProSerialNoColumn() {
		ButtonCell btnCell = new ButtonCell();
		viewProSerialNo = new Column<MaterialProduct, String>(btnCell) {
			@Override
			public String getValue(MaterialProduct object) {
				return "View Product Serial Nos";
			}
		};
		table.addColumn(viewProSerialNo, "");
		table.setColumnWidth(viewProSerialNo,180,Unit.PX);
	}
	/**
	 * ends here
	 */
}
