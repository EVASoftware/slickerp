package com.slicktechnologies.client.views.inventory.materialreuestnote;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class MaterialRequestNoteSearchProxy extends SearchPopUpScreen<MaterialRequestNote> {

	IntegerBox ibMrnId;
	public DateComparator dateComparator;
	
	TextBox tbMrnTitle;
	
	ObjectListBox<ConfigCategory> oblMrnCategory;
	ObjectListBox<Type> oblMrnType;
	
	ObjectListBox<Branch> oblMrnBranch;
	ObjectListBox<HrProject> oblMrnProject;
	ObjectListBox<Employee> oblMrnSalesPerson;
	ObjectListBox<Employee> oblMrnApproverName;
	ObjectListBox<Employee> oblMrnRequestedBy;
	
	ListBox tbMrnStatus;
	TextBox ibMrnSoId;
	TextBox ibMrnWoId;
	
	ProductInfoComposite productInfoComposite;
	
	public MaterialRequestNoteSearchProxy() {
		super();
		createGui();
	}
	
	private void initializeWidget(){
		ibMrnId=new IntegerBox();
		dateComparator = new DateComparator("mrnDate", new MaterialRequestNote());
		
		tbMrnTitle=new TextBox();
		
		oblMrnCategory= new ObjectListBox<ConfigCategory>();
		oblMrnType = new ObjectListBox<Type>();
		CategoryTypeFactory.initiateOneManyFunctionality(oblMrnCategory,oblMrnType);
		AppUtility.makeTypeListBoxLive(oblMrnType,Screen.MRNCATEGORY);
		
		oblMrnBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(oblMrnBranch);
		
		oblMrnSalesPerson = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblMrnSalesPerson);
		oblMrnSalesPerson.makeEmployeeLive(AppConstants.INVENTORYMODULE, "Material Request Note", "Requested By");
		
		oblMrnApproverName = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(oblMrnApproverName);
		
		oblMrnRequestedBy = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(oblMrnRequestedBy);
		
		oblMrnProject = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(oblMrnProject);
		
		tbMrnStatus=new ListBox();
		AppUtility.setStatusListBox(tbMrnStatus,MaterialRequestNote.getStatusList());
		ibMrnSoId=new TextBox();
		ibMrnWoId=new TextBox();
		
	}
	
	@Override
	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder("MRN ID", ibMrnId);
		FormField fibMrnId= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MRN Title", tbMrnTitle);
		FormField ftbMrnTitle = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MRN Category", oblMrnCategory);
		FormField foblMrnCategory = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MRN Type", oblMrnType);
		FormField foblMrnType = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MRN From Date", dateComparator.getFromDate());
		FormField fdbMrnFrmDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MRN To Date", dateComparator.getToDate());
		FormField fdbMrnToDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Sales Person",oblMrnSalesPerson );
		FormField foblMrnSalesPerson = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Branch", oblMrnBranch);
		FormField foblMrnBranch = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project", oblMrnProject);
		FormField foblMrnProject = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Approver", oblMrnApproverName);
		FormField foblMrnApproverName = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Requested By", oblMrnRequestedBy);
		FormField foblMrnRequestedBy = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", tbMrnStatus);
		FormField ftbMrnStatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Work Order ID", ibMrnWoId);
		FormField fibMrnWoId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Order ID.", ibMrnSoId);
		FormField fibMrnSoId = fbuilder.setMandatory(false).setMandatoryMsg("SO ID is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();
		
		
		FormField[][] formfield = { 
				{ fibMrnId,ftbMrnTitle,fdbMrnFrmDate,fdbMrnToDate},
				{ foblMrnCategory,foblMrnType,foblMrnProject,foblMrnBranch},
				{ foblMrnRequestedBy,foblMrnApproverName,ftbMrnStatus},
				{	fibMrnWoId,fibMrnSoId,foblMrnSalesPerson},
				{ fproductInfoComposite},
		};
		this.fields = formfield;
		
	}
	
	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		if (dateComparator.getValue() != null) {
			filtervec.addAll(dateComparator.getValue());
		}

		if (oblMrnBranch.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMrnBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if (oblMrnCategory.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMrnCategory.getValue().trim());
			temp.setQuerryString("mrnCategory");
			filtervec.add(temp);
		}
		if (oblMrnType.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMrnType.getValue().trim());
			temp.setQuerryString("mrnType");
			filtervec.add(temp);
		}
		
		if (oblMrnProject.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMrnProject.getValue().trim());
			temp.setQuerryString("mrnProject");
			filtervec.add(temp);
		}
		if (oblMrnApproverName.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMrnApproverName.getValue().trim());
			temp.setQuerryString("approverName");
			filtervec.add(temp);
		}
		if (oblMrnSalesPerson.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMrnSalesPerson.getValue().trim());
			temp.setQuerryString("mrnSalesPerson");
			filtervec.add(temp);
		}
		if (oblMrnRequestedBy.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblMrnRequestedBy.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		if (ibMrnId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibMrnId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if (tbMrnStatus.getSelectedIndex()!=0) {
			temp = new Filter();
			temp.setStringValue(tbMrnStatus.getValue(tbMrnStatus.getSelectedIndex()));
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		if (!tbMrnTitle.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(tbMrnTitle.getValue());
			temp.setQuerryString("mrnTitle");
			filtervec.add(temp);
		}
		
		if (!ibMrnWoId.getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(ibMrnWoId.getValue()));
			temp.setQuerryString("mrnWoId");
			filtervec.add(temp);
		}
		if (!ibMrnSoId.getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(ibMrnSoId.getValue()));
			temp.setQuerryString("mrnSoId");
			filtervec.add(temp);
		}
		
//		if (!productInfoComposite.getProdCode().getValue().equals("")) {
//			temp = new Filter();
//			temp.setStringValue(productInfoComposite.getProdCode().getValue());
//			temp.setQuerryString("items.productdetails.productCode");
//			filtervec.add(temp);
//		}
//
//		if (!productInfoComposite.getProdID().getValue().equals("")) {
//			temp = new Filter();
//			temp.setIntValue(Integer.parseInt(productInfoComposite.getProdID().getValue()));
//			temp.setQuerryString("items.productdetails.productID");
//			filtervec.add(temp);
//		}

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new MaterialRequestNote());
		return querry;
	}

	@Override
	public boolean validate() {
		
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(oblMrnBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}

}
