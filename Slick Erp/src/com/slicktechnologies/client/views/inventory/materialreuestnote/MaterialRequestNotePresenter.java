package com.slicktechnologies.client.views.inventory.materialreuestnote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNoteForm;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNotePresenter;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNoteForm;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNotePresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationForm;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationPresenter;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class MaterialRequestNotePresenter extends ApprovableFormScreenPresenter<MaterialRequestNote> 
			implements ClickHandler,ChangeHandler,RowCountChangeEvent.Handler {
	static double availableQty;
	public  static MaterialRequestNoteForm form;
	final static  GenricServiceAsync async = GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	
	DocumentCancellationPopUp cancelPopup = new DocumentCancellationPopUp();
	PopupPanel cancelpanel ;
	
	public MaterialRequestNotePresenter(ApprovableFormScreen<MaterialRequestNote> view, MaterialRequestNote model) {
		super(view, model);
		form = (MaterialRequestNoteForm) view;
		form.getIbMrnSoId().addChangeHandler(this);
		form.getSubProductTableMrn().getTable().addRowCountChangeHandler(this);
		
		cancelPopup.getBtnOk().addClickHandler(this);
		cancelPopup.getBtnCancel().addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.MATERIALREQUESTNOTE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	public static void showMessage(String msg){
		form.showDialogMessage(msg);
	}

	public static MaterialRequestNoteForm initialize()
	{
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Material Request Note",Screen.MATERIALREQUESTNOTE);
		MaterialRequestNoteForm form=new  MaterialRequestNoteForm();

		MaterialRequestNoteTableProxy gentable=new MaterialRequestNoteTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		
		MaterialRequestNoteSearchProxy.staticSuperTable=gentable;
		MaterialRequestNoteSearchProxy searchpopup=new MaterialRequestNoteSearchProxy();
		form.setSearchpopupscreen(searchpopup);

	    MaterialRequestNotePresenter  presenter=new MaterialRequestNotePresenter(form,new MaterialRequestNote());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	public static MaterialRequestNoteForm initialize(MyQuerry querry)
	{
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Material Request Note",Screen.MATERIALREQUESTNOTE);		
		MaterialRequestNoteForm form=new  MaterialRequestNoteForm();

		MaterialRequestNoteTableProxy gentable=new MaterialRequestNoteTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		
		MaterialRequestNoteSearchProxy.staticSuperTable=gentable;
		MaterialRequestNoteSearchProxy searchpopup=new MaterialRequestNoteSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		MaterialRequestNotePresenter  presenter=new MaterialRequestNotePresenter(form,new MaterialRequestNote());
		AppMemory.getAppMemory().stickPnel(form);

		return form;
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
//		super.reactToProcessBarEvents(e);
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		if (text.equals(AppConstants.NEW))
			reactToNew();
		if (text.equals("DownLoad"))
			reactOnDownload();
		if (text.equals("Email"))
			reactOnEmail();
		if (text.equals("Create MIN")) {
			reactToMIN();
		}
		if (text.equals(AppConstants.CANCELMRN)) {
			
			cancelPopup.getPaymentDate().setValue(new Date()+"");
			cancelPopup.getPaymentID().setValue(model.getCount()+"");
			cancelPopup.getPaymentStatus().setValue(model.getStatus());
			cancelpanel=new PopupPanel(true);
			cancelpanel.add(cancelPopup);
			cancelpanel.show();
			cancelpanel.center();
		}
		if(text.equals(ManageApprovals.APPROVALREQUEST)){
			if(form.validate()==true){
				form.getManageapproval().reactToRequestForApproval();
			}
			else{
				form.showDialogMessage("Please select mandatory fields.");
			}
		}
		if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST)){
			form.getManageapproval().reactToApprovalRequestCancel();
		}
		
		if(text.equals(ManageApprovals.SUBMIT)){
			if(form.validate()==true){
				form.getManageapproval().reactToSubmit();
			}
			else{
				form.showDialogMessage("Please select mandatory fields.");
			}
		}
		/**
		 * Date 06-02-2019 By Vijay 
		 * Requirements :- NBHC Inventory management
		 */
		if(text.equals(AppConstants.VIEWMMN)){
			reactOnViewMMN();
		}
	}

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
		boolean conf = Window.confirm("Do you really want to send email?");
		if (conf == true) {
			/**
			 * @author Anil @since 12-05-2021
			 */
			MaterialRequestNote mrn=(MaterialRequestNote) model;
			if(mrn.getMrnDate()==null){
				mrn.setMrnDate(mrn.getCreationDate());
			}
			emailService.initiateMRNEmail((MaterialRequestNote) model,new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Resource Quota Ended ");
					caught.printStackTrace();
				}

				@Override
				public void onSuccess(Void result) {
					Window.alert("Email Sent Sucessfully !");
				}
			});
		}
	}

	@Override
	protected void makeNewModel() {
		model = new MaterialRequestNote();
	}
	
	private void reactToNew() {
		form.setToNewState();
		initialize();
	}
	
	@Override
	public void reactOnDownload() {
		ArrayList<MaterialRequestNote> mrnArray = new ArrayList<MaterialRequestNote>();
		List<MaterialRequestNote> list = (List<MaterialRequestNote>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		mrnArray.addAll(list);
		csvservice.setMaterialRequestNote(mrnArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 68;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	
	@Override
	public void onClick(ClickEvent event) {
			super.onClick(event);
			
			if(event.getSource()==cancelPopup.getBtnOk()){
					cancelpanel.hide();
					changeStatus(MaterialRequestNote.CANCELLED);
				}
			
			if(event.getSource()==cancelPopup.getBtnCancel()){
					cancelpanel.hide();
					cancelPopup.remark.setValue("");
			}
		}
	
	public void changeStatus(final String status)
	{
		model.setStatus(status);
		model.setMrnDescription("MRN ID ="+model.getCount()+" "+"MRN status ="+form.getTbMrnStatus().getValue().trim()+"\n"
				+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"
				+"Remark ="+cancelPopup.getRemark().getValue().trim()+" "+"Cancellation Date ="+new Date());
		async.save(model,new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				
				form.tbMrnStatus.setText(status);
				form.getTaMrnDescription().setValue(model.getMrnDescription());
				form.setToViewState();
			}
		});
	}

	
	@Override
	public void onChange(ChangeEvent event) {
		if (event.getSource() == form.getIbMrnSoId()) {
			if (form.getIbMrnSoId().getValue() != null) {
				setDataFromSo();
			}
		}
	}
	
	public void setDataFromSo(){
		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(form.getIbMrnSoId().getValue()));
		querry.getFilters().add(filter);
		querry.setQuerryObject(new SalesOrder());
		form.showWaitSymbol();
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							SalesOrder so=(SalesOrder) result.get(0);;
							if(so.getStatus().equals("Approved"))
							{
								form.getProductTableMrn().getDataprovider().setList(so.getItems());

								form.getDbMrnSoDate().setValue(so.getCreationDate());
								form.getDbMrnDeliveryDate().setValue(so.getDeliveryDate());

								form.getOblMrnBranch().setValue(so.getBranch());
								form.getOblMrnBranch().setEnabled(false);
								
								form.getOblMrnSalesPerson().setValue(so.getEmployee());
								if (form.getOblMrnSalesPerson().getValue() != null) {
									form.getOblMrnSalesPerson().setEnabled(false);
								}

								form.getOblMrnApproverName().setValue(so.getApproverName());
								if (form.getOblMrnApproverName().getValue() != null) {
									form.getOblMrnApproverName().setEnabled(false);
								}
								
							}
							else{
								form.showDialogMessage("SO ID is Not Approved");
								form.setToNewState();
								form.getProductTableMrn().clear();
								form.getSubProductTableMrn().clear();
							}
						}else
						{
							form.showDialogMessage("Sales Order with this number doesnt exist, Please recheck.");
							form.setToNewState();
							form.getProductTableMrn().clear();
							form.getSubProductTableMrn().clear();
						}
					}

					@Override
					public void onFailure(Throwable caught) {

					}
				});
		form.hideWaitSymbol();
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		NumberFormat nf=NumberFormat.getFormat("0.00");
		if(event.getSource()==form.getSubProductTableMrn().getTable()){
			
			
		
			
	    }
	}
	
	/******************************************** react to MIN ***********************************************************/

	public void reactToMIN() {
		final MaterialIssueNoteForm form = MaterialIssueNotePresenter.initialize();
		final MaterialIssueNote min = new MaterialIssueNote();
		
		min.setMinMrnId(model.getCount());
		min.setMinMrnDate(model.getMrnDate());
		min.setMinWoId(model.getMrnWoId());
		min.setMinWoDate(model.getMrnWoDate());
		min.setMinSoId(model.getMrnSoId());
		min.setMinSoDate(model.getMrnSoDate());
		min.setMinSoDeliveryDate(model.getMrnDeliveryDate());
		min.setMinSalesPerson(model.getMrnSalesPerson());
		min.setMinProject(model.getMrnProject());
		
		min.setBranch(model.getBranch());
		min.setEmployee(model.getEmployee());
		min.setApproverName(model.getApproverName());
		
		ArrayList<SalesLineItem> productList = model.getProductTableMrn();
		min.setProductTablemin(productList);
		
		ArrayList<MaterialProduct> materialList = model.getSubProductTableMrn();
		min.setSubProductTablemin(materialList);
		form.checkMrnQuantity(model.getCount());

		Timer t = new Timer() {
			@Override
			public void run() {
				form.setToNewState();
				form.updateView(min);
				form.getProductInfoComposite().setEnabled(false);
				form.getIbMinMrnId().setEnabled(false);
			}
		};
		t.schedule(5000);
	}

	/**
	 * Date 06-02-2019 by Vijay to show MMN of specific MRN
	 */
	private void reactOnViewMMN() {
		
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("mmnMrnId");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new MaterialMovementNote());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				if(result.size()==0){
					form.showDialogMessage("No MMN document found.");
					form.hideWaitSymbol();
					return;
				}
				if(result.size()>=1){
					final MaterialMovementNote mmn=(MaterialMovementNote) result.get(0);
					final MaterialMovementNoteForm mmnform=MaterialMovementNotePresenter.initialize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							mmnform.updateView(mmn);
							mmnform.setToViewState();
							form.hideWaitSymbol();
						}
					};
					timer.schedule(9000);
					
				}
				
			}
		});
	}
	/**
	 * ends here
	 */
}
