package com.slicktechnologies.client.views.inventory.materialreuestnote;

import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.SalesLineItem;

public class ProductTableMrn extends SuperTable<SalesLineItem> {

	TextColumn<SalesLineItem> getColumnProductId;
	TextColumn<SalesLineItem> getColumnProductCode;
	TextColumn<SalesLineItem> getColumnProductName;
	TextColumn<SalesLineItem> getColumnProductCategory;
	TextColumn<SalesLineItem> getColumnProductQty;
	TextColumn<SalesLineItem> getColumnProductUom;
	
	public ProductTableMrn() {
	}
	
	@Override
	public void createTable() {
//		addColumnProductId();
		addColumnProductName();
		addColumnProductUom();
		addColumnProductQty();
		addColumnProductCategory();
		addColumnProductCode();
	}

	private void addColumnProductUom() {
		getColumnProductUom=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getUnitOfMeasurement();
			}
		};
		table.addColumn(getColumnProductUom, "UOM");
	}

	private void addColumnProductQty() {
		getColumnProductQty=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getQty()+"";
			}
		};
		table.addColumn(getColumnProductQty, "Quantity");
	}

	private void addColumnProductCategory() {
		getColumnProductCategory=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(getColumnProductCategory, "Category");
	}

	private void addColumnProductName() {
		getColumnProductName=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductName();
			}
		};
		table.addColumn(getColumnProductName, "Name");
	}

	private void addColumnProductCode() {
		getColumnProductCode=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductCode();
			}
		};
		table.addColumn(getColumnProductCode, "Code");
	}

//	private void addColumnProductId() {
//		getColumnProductId=new TextColumn<SalesLineItem>() {
//			@Override
//			public String getValue(SalesLineItem object) {
//				return object.get+"";
//			}
//		};
//		table.addColumn(getColumnProductId, "ID");
//	}



	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}
	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
