package com.slicktechnologies.client.views.inventory.materialreuestnote;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class MaterialRequestNoteForm extends ApprovableFormScreen<MaterialRequestNote> implements ClickHandler,ChangeHandler{
	final GenricServiceAsync async = GWT.create(GenricService.class);
	TextBox ibMrnSoId;
	DateBox dbMrnSoDate;
	TextBox ibMrnWoId;
	DateBox dbMrnWoDate;
	DateBox dbMrnDeliveryDate;
	
	TextBox ibMrnId;
	DateBox dbMrnDate;
	
	TextBox tbMrnTitle;
	DateBox dbMrnRequiredDate;
	
	ObjectListBox<ConfigCategory> oblMrnCategory;
	ObjectListBox<Type> oblMrnType;
	
	TextBox tbMrnRefNum;
	DateBox dbMrnRefDate;
	
	ObjectListBox<Branch> oblMrnBranch;
	ObjectListBox<HrProject> oblMrnProject;
	ObjectListBox<Employee> oblMrnSalesPerson;
	ObjectListBox<Employee> oblMrnApproverName;
	ObjectListBox<Employee> oblMrnRequestedBy;
	
//	ObjectListBox<WareHouse> oblwarehouse;
//	ListBox lsStorageLoc;
//	ListBox lsStoragebin;
	
	TextBox tbMrnStatus;
	TextBox tbMrnCommentOnStatusChange;
	FormField fgroupingMRNInformation;
	UploadComposite upload;
	TextArea taMrnDescription;
	
	ProductTableMrn productTableMrn;
	ProductInfoComposite productInfoComposite;
	Button btnAdd;
	SubProductTableMrn subProductTableMrn;
	double availableQty;
	MaterialRequestNote mrnobj;
	
	boolean flage1=false;
	boolean flage2=false;
	public MaterialRequestNoteForm() {
		super();
		createGui();
		
		this.ibMrnId.setEnabled(false);
		this.tbMrnStatus.setEnabled(false);
		this.tbMrnStatus.setText(MaterialRequestNote.CREATED);
		this.dbMrnSoDate.setEnabled(false);
		this.dbMrnWoDate.setEnabled(false);
		this.dbMrnDeliveryDate.setEnabled(false);
		this.tbMrnCommentOnStatusChange.setEnabled(false);
		this.dbMrnRefDate.setEnabled(false);
		this.tbMrnRefNum.setEnabled(false);
		subProductTableMrn.connectToLocal();
		
		Timer timer=new Timer() {
			@Override
			public void run() {
				setDefaultDataOfLoggedInUser();
			}
		};
		timer.schedule(4000);
		
	}
	
	protected void setDefaultDataOfLoggedInUser() {
		oblMrnRequestedBy.setValue(LoginPresenter.loggedInUser);
//		oblMrnSalesPerson.setValue(LoginPresenter.loggedInUser);
		oblMrnApproverName.setValue(LoginPresenter.loggedInUser);
		
		if(oblMrnRequestedBy.getValue()!=null){
			Employee emp=oblMrnRequestedBy.getSelectedItem();
			oblMrnBranch.setValue(emp.getBranchName());
		}
	}
	
	
	protected boolean getProductInfoVisibilityStatus(){
		if(LoginPresenter.globalProcessConfig.size()!=0){
			for(ProcessConfiguration proConf:LoginPresenter.globalProcessConfig){
				if(proConf.getProcessName().equals("MaterialRequestNote")&&proConf.isConfigStatus()==true){
					for(ProcessTypeDetails proTypDet:proConf.getProcessList()){
						if(proTypDet.getProcessType().equalsIgnoreCase("HIDEPRODUCTINFO")&&proTypDet.isStatus()==true){
							return false;
						}
					}
				}
			}
		}
		return true;
	}
	

	public MaterialRequestNoteForm(String[] processlevel, FormField[][] fields,FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
	}
	
	private void initializeWidget(){
		ibMrnSoId=new TextBox();
		ibMrnSoId.setEnabled(false);
		dbMrnSoDate=new DateBoxWithYearSelector();
		ibMrnWoId=new TextBox();
		ibMrnWoId.setEnabled(false);
		dbMrnWoDate=new DateBoxWithYearSelector();
		dbMrnDeliveryDate=new DateBoxWithYearSelector();
		tbMrnRefNum=new TextBox();
		dbMrnRefDate=new DateBoxWithYearSelector();
		ibMrnId=new TextBox();
		dbMrnDate=new DateBoxWithYearSelector();
		
		tbMrnTitle=new TextBox();
		dbMrnRequiredDate=new DateBoxWithYearSelector();
		
		oblMrnCategory= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblMrnCategory, Screen.MRNCATEGORY);
		oblMrnCategory.addChangeHandler(this);
		oblMrnType = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(oblMrnType,Screen.MRNTYPE);
		
		oblMrnBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(oblMrnBranch);
		
		oblMrnSalesPerson = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblMrnSalesPerson);
		oblMrnSalesPerson.makeEmployeeLive(AppConstants.INVENTORYMODULE, "Material Request Note", "Sales Person");
		
		oblMrnApproverName = new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(oblMrnApproverName, "MRN");
		
		oblMrnRequestedBy = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblMrnRequestedBy);
		oblMrnRequestedBy.makeEmployeeLive(AppConstants.INVENTORYMODULE, "Material Request Note", "Requested By");
		
		oblMrnProject = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(oblMrnProject);
		
		tbMrnStatus=new TextBox();
		tbMrnCommentOnStatusChange=new TextBox();
		
		upload=new UploadComposite();
		taMrnDescription=new TextArea();
		
		productInfoComposite = AppUtility.initiateMrnProductComposite(new SuperProduct());
		
		productTableMrn=new ProductTableMrn();
		
		btnAdd=new Button("ADD");
		btnAdd.addClickHandler(this);
		
		subProductTableMrn=new SubProductTableMrn();
	}
	
	@Override
	public void createScreen() {
		initializeWidget();
		
		this.processlevelBarNames = new String[] {
				ManageApprovals.APPROVALREQUEST,
				ManageApprovals.CANCELAPPROVALREQUEST,ManageApprovals.SUBMIT,AppConstants.CANCELMRN,
				AppConstants.NEW,AppConstants.VIEWMMN };
		
		String mainScreenLabel="MATERIAL REQUEST NOTE";
		if(mrnobj!=null&&mrnobj.getCount()!=0){
			mainScreenLabel=mrnobj.getCount()+" "+"/"+" "+mrnobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(mrnobj.getCreationDate());
		}
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingMRNInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		
		fbuilder = new FormFieldBuilder("Work Order ID", ibMrnWoId);
		FormField fibMrnWoId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Work Order Date", dbMrnWoDate);
		FormField fdbMrnWoDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Reference Number", tbMrnRefNum);
		FormField fibMrnRefId = fbuilder.setMandatory(false).setMandatoryMsg("Order Id is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Reference Date", dbMrnRefDate);
		FormField fdbMrnRefDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Order ID", ibMrnSoId);
		FormField fibMrnSoId = fbuilder.setMandatory(false).setMandatoryMsg("SO ID is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Sales Order Date", dbMrnSoDate);
		FormField fdbMrnSoDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Sales Order Delivery Date", dbMrnDeliveryDate);
		FormField fdbMrnDeliveryDate = fbuilder.setMandatory(false).setMandatoryMsg("Delivery Date is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project", oblMrnProject);
		FormField foblMrnProject = fbuilder.setMandatory(false).setMandatoryMsg("Project is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Sales Person",oblMrnSalesPerson );
		FormField foblMrnSalesPerson = fbuilder.setMandatory(false).setMandatoryMsg("Sales Person is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		FormField fmrnMrnInfoGrouping = fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("MRN ID", ibMrnId);
		FormField fibMrnId= fbuilder.setMandatory(false).setMandatoryMsg("MRN ID is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MRN Title", tbMrnTitle);
		FormField ftbMrnTitle = fbuilder.setMandatory(false).setMandatoryMsg("MRN Title is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* MRN Date", dbMrnDate);
		FormField fdbMrnDate = fbuilder.setMandatory(true).setMandatoryMsg("MRN Date is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* MRN Required Date", dbMrnRequiredDate);
		FormField fdbMrnRequiredDate = fbuilder.setMandatory(true).setMandatoryMsg("MRN Required Date is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MRN Category", oblMrnCategory);
		FormField foblMrnCategory = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("MRN Type", oblMrnType);
		FormField foblMrnType = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		FormField foblMrnBranch=null;
		if(AppUtility.isBranchNonMandatory("MaterialRequestNote")){
			fbuilder = new FormFieldBuilder("Branch", oblMrnBranch);
			foblMrnBranch = fbuilder.setMandatory(false).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("* Branch", oblMrnBranch);
			foblMrnBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
					.setColSpan(0).build();
		}
		
		fbuilder = new FormFieldBuilder("* Approver", oblMrnApproverName);
		FormField foblMrnApproverName = fbuilder.setMandatory(true).setMandatoryMsg("Approver is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		/**
		 *  Added By : Priyanka
		 *  Des : PECOPP ON MRN,MIN,MMN replace label "Requested By" to "Requested To" Raised By Ashwini.
		 */
		
		//fbuilder = new FormFieldBuilder("* Requested By", oblMrnRequestedBy);
		fbuilder = new FormFieldBuilder("* Requested To", oblMrnRequestedBy);
		FormField foblMrnRequestedBy = fbuilder.setMandatory(true).setMandatoryMsg("Requested By is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", tbMrnStatus);
		FormField ftbMrnStatus = fbuilder.setMandatory(true).setMandatoryMsg("Status is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Comment", tbMrnCommentOnStatusChange);
		FormField ftbMrnCommentOnStatusChange = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Document", upload);
		FormField fupload = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)", taMrnDescription);
		FormField ftaMrnDescription = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		FormField fmrnProductGrouping = fbuilder.setlabel("Product")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", productTableMrn.getTable());
		FormField fproductTableMrn= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField fmrnRefereceDetailGrouping = fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fmrnSubProductGrouping = fbuilder.setlabel("MRN Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fbtnAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", subProductTableMrn.getTable());
		FormField fsubProductTableMrn= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		if(getProductInfoVisibilityStatus()){
//			FormField[][] formfield = { 
//					{ fmrnRefereceDetailGrouping },
//					{ fibMrnWoId, fdbMrnWoDate, fibMrnSoId,fdbMrnSoDate},
//					{ fdbMrnDeliveryDate,fibMrnRefId,fdbMrnRefDate,foblMrnProject},
//					{foblMrnSalesPerson},
//					{ fmrnMrnInfoGrouping},
//					{ fibMrnId,ftbMrnTitle, fdbMrnDate,fdbMrnRequiredDate},
//					{ foblMrnCategory,foblMrnType,foblMrnBranch,foblMrnRequestedBy},
//					{ foblMrnApproverName,ftbMrnStatus, ftbMrnCommentOnStatusChange},
//					{ fupload},
//					{ ftaMrnDescription},
//					
//					{ fmrnProductGrouping},
//					{ fproductTableMrn},
//					
//					{ fmrnSubProductGrouping},
//					{ fproductInfoComposite,fbtnAdd},
//					{ fsubProductTableMrn}
//			};
//			this.fields = formfield;
			
			FormField[][] formfield = { 
					/**MATERIAL REQUEST NOTE **/
					{fgroupingMRNInformation},
					{ftbMrnTitle,foblMrnBranch,foblMrnRequestedBy,foblMrnSalesPerson},
					{foblMrnApproverName,ftbMrnCommentOnStatusChange},
					/**Product Details **/
					{ fmrnProductGrouping},
					{ fproductTableMrn},
					/**MRN Details **/
					{ fmrnSubProductGrouping},
					{ fproductInfoComposite,fbtnAdd},
					{ fsubProductTableMrn},
					/**Classification**/
					{ fmrnRefereceDetailGrouping },
					{ foblMrnCategory,foblMrnType},
					/**Reference/General**/
					{ fmrnMrnInfoGrouping},
					{ fibMrnWoId, fdbMrnWoDate, fibMrnSoId,fdbMrnSoDate},
					{ fdbMrnDeliveryDate,fibMrnRefId,fdbMrnRefDate,foblMrnProject},
					{ ftaMrnDescription}
					
			};
			this.fields = formfield;
		}else{
//			FormField[][] formfield = { 
//					{ fmrnRefereceDetailGrouping },
//					{ fibMrnWoId, fdbMrnWoDate, fibMrnSoId,fdbMrnSoDate},
//					{ fdbMrnDeliveryDate,fibMrnRefId,fdbMrnRefDate,foblMrnProject},
//					{foblMrnSalesPerson},
//					{ fmrnMrnInfoGrouping},
//					{ fibMrnId,ftbMrnTitle, fdbMrnDate,fdbMrnRequiredDate},
//					{ foblMrnCategory,foblMrnType,foblMrnBranch,foblMrnRequestedBy},
//					{ foblMrnApproverName,ftbMrnStatus, ftbMrnCommentOnStatusChange},
//					{ fupload},
//					{ ftaMrnDescription},
//					
//					{ fmrnSubProductGrouping},
//					{ fproductInfoComposite,fbtnAdd},
//					{ fsubProductTableMrn}
//			};
//			this.fields = formfield;
			
			FormField[][] formfield = { 
					/**MATERIAL REQUEST NOTE **/
					{fgroupingMRNInformation},
					{ftbMrnTitle,foblMrnBranch,foblMrnRequestedBy,ftbMrnCommentOnStatusChange},
					{foblMrnApproverName},
					/**MRN Details **/
					{ fmrnSubProductGrouping},
					{ fproductInfoComposite,fbtnAdd},
					{ fsubProductTableMrn},
					/**Product Details **/
					//{ fmrnProductGrouping},
					//{ fproductTableMrn},
					/**Classification**/
					{ fmrnRefereceDetailGrouping },
					{ foblMrnCategory,foblMrnType},
					/**Reference/General**/
					{ fmrnMrnInfoGrouping},
					{ fibMrnWoId, fdbMrnWoDate, fibMrnSoId,fdbMrnSoDate},
					{ fdbMrnDeliveryDate,fibMrnRefId,fdbMrnRefDate,foblMrnProject},
					{foblMrnSalesPerson},
					{ ftaMrnDescription}
					
			};
			this.fields = formfield;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(MaterialRequestNote model) {
		if (tbMrnTitle.getValue() != null)
			model.setMrnTitle(tbMrnTitle.getValue());
		if (dbMrnDate.getValue() != null)
			model.setMrnDate(dbMrnDate.getValue());
		if (dbMrnRequiredDate.getValue() != null)
			model.setMrnRequiredDate(dbMrnRequiredDate.getValue());
		if (!ibMrnWoId.getValue().equals(""))
			model.setMrnWoId(Integer.parseInt(ibMrnWoId.getValue()));
		if (dbMrnWoDate.getValue() != null)
			model.setMrnWoDate(dbMrnWoDate.getValue());
		if (!ibMrnSoId.getValue().equals(""))
			model.setMrnSoId(Integer.parseInt(ibMrnSoId.getValue()));
		if (!tbMrnRefNum.getValue().equals(""))
			model.setMrnRefId(tbMrnRefNum.getValue());
		if (dbMrnRefDate.getValue() != null)
			model.setMrnRefDate(dbMrnRefDate.getValue());
		if (dbMrnSoDate.getValue() != null)
			model.setMrnSoDate(dbMrnSoDate.getValue());
		if (dbMrnDeliveryDate.getValue() != null)
			model.setMrnDeliveryDate(dbMrnDeliveryDate.getValue());
		if (oblMrnProject.getValue() != null)
			model.setMrnProject(oblMrnProject.getValue());
		
		if (oblMrnCategory.getValue() != null)
			model.setMrnCategory(oblMrnCategory.getValue());
		if (oblMrnType.getValue() != null)
			model.setMrnType(oblMrnType.getValue(oblMrnType.getSelectedIndex()));
		
		if (oblMrnBranch.getValue() != null)
			model.setBranch(oblMrnBranch.getValue());
		if (oblMrnSalesPerson.getValue() != null)
			model.setMrnSalesPerson(oblMrnSalesPerson.getValue());
		if (oblMrnRequestedBy.getValue() != null)
			model.setEmployee(oblMrnRequestedBy.getValue());
		if (oblMrnApproverName.getValue() != null)
			model.setApproverName(oblMrnApproverName.getValue());
		
		if (tbMrnStatus.getValue() != null)
			model.setStatus(tbMrnStatus.getValue());
//		if (tbMrnCommentOnStatusChange.getValue() != null)
//			model.setRemark(tbMrnCommentOnStatusChange.getValue());
		
		if (taMrnDescription.getValue() != null)
			model.setMrnDescription(taMrnDescription.getValue());
		if (upload.getValue() != null)
			model.setUpload(upload.getValue());
		
		
		if (productTableMrn.getValue() != null)
			model.setProductTableMrn(productTableMrn.getValue());
		if (subProductTableMrn.getValue() != null)
			model.setSubProductTableMrn(subProductTableMrn.getValue());
		
		mrnobj=model;
		
		presenter.setModel(model);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateView(MaterialRequestNote view) {
		
		mrnobj=view;
		
		ibMrnId.setValue(view.getCount()+"");
		if (view.getMrnTitle() != null)
			tbMrnTitle.setValue(view.getMrnTitle());
		if (view.getMrnDate() != null)
			dbMrnDate.setValue(view.getMrnDate());
		if (view.getMrnRequiredDate() != null)
			dbMrnRequiredDate.setValue(view.getMrnRequiredDate());
		if (view.getMrnWoId() != 0)
			ibMrnWoId.setValue(view.getMrnWoId()+"");
		if (view.getMrnWoDate() != null)
			dbMrnWoDate.setValue(view.getMrnWoDate());
		if (view.getMrnSoId() != 0)
			ibMrnSoId.setValue(view.getMrnSoId()+"");
		if (view.getMrnSoDate() != null)
			dbMrnSoDate.setValue(view.getMrnSoDate());
		
		if (view.getMrnDeliveryDate() != null)
			dbMrnDeliveryDate.setValue(view.getMrnDeliveryDate());
		
		if (view.getMrnCategory() != null)
			oblMrnCategory.setValue(view.getMrnCategory());
		if (view.getMrnType()!= null)
			oblMrnType.setValue(view.getMrnType());
		if (view.getMrnRefId() != null)
			tbMrnRefNum.setValue(view.getMrnRefId());
		if (view.getMrnRefDate() != null)
			dbMrnRefDate.setValue(view.getMrnRefDate());
		if (view.getMrnProject() != null)
			oblMrnProject.setValue(view.getMrnProject());
		if (view.getBranch() != null)
			oblMrnBranch.setValue(view.getBranch());
		if (view.getMrnSalesPerson() != null)
			oblMrnSalesPerson.setValue(view.getMrnSalesPerson());
		if (view.getEmployee() != null)
			oblMrnRequestedBy.setValue(view.getEmployee());
		if (view.getApproverName()!= null)
			oblMrnApproverName.setValue(view.getApproverName());
		
		if (view.getStatus() != null)
			tbMrnStatus.setValue(view.getStatus());
		if (view.getRemark()!= null){
			tbMrnCommentOnStatusChange.setValue(view.getRemark());
		}
		
		if (view.getMrnDescription() != null)
			taMrnDescription.setValue(view.getMrnDescription());
		if (view.getUpload() != null)
			upload.setValue(view.getUpload());
		
		if (view.getProductTableMrn() != null)
			productTableMrn.setValue(view.getProductTableMrn());
		if (view.getSubProductTableMrn() != null)
			subProductTableMrn.setValue(view.getSubProductTableMrn());
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */
	}
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")
						|| text.contains("Search")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")|| text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Edit") || text.contains("Discard")|| text.contains("Search")|| text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.MATERIALREQUESTNOTE,LoginPresenter.currentModule.trim());
	}
	
	public void toggleProcessLevelMenu() {
		MaterialRequestNote entity = (MaterialRequestNote) presenter.getModel();
		String status = entity.getStatus();

		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();
			if ((status.equals(MaterialRequestNote.APPROVED))) {
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				if (text.equals(AppConstants.CREATEMIN))
					label.setVisible(true);
				else
					label.setVisible(false);
				
				if (text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);;
				
//				if (text.equals(AppConstants.CANCELMRN))
//					label.setVisible(true);
					
					/**
					 * rohan added this code for NBHC Changes ADMIN role should  
					 */
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
						if (text.equals(AppConstants.CANCELMRN))
							label.setVisible(true);
					}
					else
					{
						if (text.equals(AppConstants.CANCELMRN))
						label.setVisible(false);;
					}	
					
					/**** Date 07-02-2019 by Vijay for View MMN ***/
					if (text.equals(AppConstants.VIEWMMN))
						label.setVisible(true);
					
			}
			if (status.equals(MaterialRequestNote.CREATED)) {
				/**
				 * rohan added this code for NBHC Changes ADMIN role should  
				 */
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
					if (text.equals(AppConstants.CANCELMRN))
						label.setVisible(true);
				}
				else
				{
					if (text.equals(AppConstants.CANCELMRN))
					label.setVisible(false);;
				}	
				/**** Date 07-02-2019 by Vijay for View MMN ***/
				if (text.equals(AppConstants.VIEWMMN))
					label.setVisible(false);
			}
			
			if ((status.equals(MaterialRequestNote.REQUESTED))) {
				
				if (text.equals(AppConstants.CANCELMRN))
					label.setVisible(false);
				
				if (text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				
				if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(true);
				
				/**** Date 07-02-2019 by Vijay for View MMN ***/
				if (text.equals(AppConstants.VIEWMMN))
					label.setVisible(false);
			}
			
			
			
			if ((status.equals(MaterialRequestNote.REJECTED) || status.equals(MaterialRequestNote.CANCELLED))) {
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);
				
				if (text.equals(AppConstants.CANCELMRN))
					label.setVisible(false);
				/**** Date 07-02-2019 by Vijay for View MMN ***/
				if (text.equals(AppConstants.VIEWMMN))
					label.setVisible(false);
			}
			
			/**
			 * Date 12-01-2019 by Vijay For NBHC Inventory Management closed MRN managing process bar buttons 
			 */
			if ((status.equals(MaterialRequestNote.CLOSED))) {
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);
				
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
					if (text.equals(AppConstants.CANCELMRN))
						label.setVisible(true);
				}
				
				/**** Date 07-02-2019 by Vijay for View MMN ***/
				if (text.equals(AppConstants.VIEWMMN))
					label.setVisible(true);
			}
			/**
			 * ends here
			 */
		}

	}
	
	public void setAppHeaderBarAsPerStatus() {
		MaterialRequestNote entity = (MaterialRequestNote) presenter.getModel();
		String status = entity.getStatus();

		if (status.equals(MaterialRequestNote.APPROVED)) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")||text.contains("Email")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		
		if (status.equals(MaterialRequestNote.CANCELLED)) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
	}
	
	public void setMenuAsPerStatus() {
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
	}
	
	@Override
	public void setCount(int count) {
		ibMrnId.setValue(count+"");
	}

	@Override
	public void clear() {
		super.clear();
		tbMrnStatus.setText(MaterialRequestNote.CREATED);
		productTableMrn.clear();
		subProductTableMrn.clear();

	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbMrnStatus.setEnabled(false);
		tbMrnCommentOnStatusChange.setEnabled(false);
		ibMrnId.setEnabled(false);
		productTableMrn.setEnable(state);
		subProductTableMrn.setEnable(state);
		dbMrnDeliveryDate.setEnabled(false);
		dbMrnSoDate.setEnabled(false);
		dbMrnWoDate.setEnabled(false);
		ibMrnWoId.setEnabled(false);
		ibMrnSoId.setEnabled(false);
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		setMenuAsPerStatus();
		
		SuperModel model=new MaterialRequestNote();
		model=mrnobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.INVENTORYMODULE,AppConstants.MATERIALREQUESTNOTE, mrnobj.getCount(), null,null,null, false, model, null);
		
		
		String mainScreenLabel="MATERIAL REQUEST NOTE";
		if(mrnobj!=null&&mrnobj.getCount()!=0){
			mainScreenLabel=mrnobj.getCount()+" "+"/"+" "+mrnobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(mrnobj.getCreationDate());
		}
		fgroupingMRNInformation.getHeaderLabel().setText(mainScreenLabel);
		
	
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		
		if(tbMrnStatus.getValue().equals(MaterialRequestNote.REJECTED)){
			tbMrnStatus.setValue(MaterialRequestNote.CREATED);
		}
		setMenuAsPerStatus();
		this.processLevelBar.setVisibleFalse(false);
		/**
		 * Date 02-09-2019 by Vijay
		 * Des :- NBHC Inventory Management Auto MRN Requested by should be by default logged in user
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialRequestNote", "EnableAutoMRNRequestedByLoggedInUser")){
			MaterialRequestNote mrn = (MaterialRequestNote) this.getPresenter().getModel();
			if(mrn.isAutoMRN()){
				setDefaultDataOfLoggedInUser();		
			}
		}
		/**
		 * ends here
		 */
		
		String mainScreenLabel="MATERIAL REQUEST NOTE";
		if(mrnobj!=null&&mrnobj.getCount()!=0){
			mainScreenLabel=mrnobj.getCount()+" "+"/"+" "+mrnobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(mrnobj.getCreationDate());
		}
		fgroupingMRNInformation.getHeaderLabel().setText(mainScreenLabel);
		
	}

	@Override
	public TextBox getstatustextbox() {
		return tbMrnStatus;
	}
	
	// ************************************** Validation Method ******************************************
	
	@Override
	public boolean validate() {
		boolean superValidate = super.validate();
		if (superValidate == false)
			return false;
		if(validateRequiredQuantity()==false){
			return false;
		}
		if(subProductTableMrn.getDataprovider().getList().size()== 0){
			showDialogMessage("Please add material details.");
			return false;
		}
//		if(validateWarehouse()==false){
//			showDialogMessage("Please Select Warehouse Details.");
//			return false;
//		}
		return true;
	}
	
	
	
	
	// ************************************** Validate Product ***********************************************
	
	public boolean validateProduct() {
		String name = productInfoComposite.getProdNameValue();
		System.out.println("Product name is :"+name);
		List<MaterialProduct> list = subProductTableMrn.getDataprovider().getList();
		for (MaterialProduct temp : list) {
			if (name.equals(temp.getMaterialProductName())) {
				this.showDialogMessage("Can not add Duplicate product");
				return false;
			}
		}
		return true;
	}
	
	// ********************************* Validate Required Quantity *********************************************
	
	public boolean validateRequiredQuantity() {
		List<MaterialProduct> list = subProductTableMrn.getDataprovider().getList();
		for (MaterialProduct temp : list) {
			if (temp.getMaterialProductRequiredQuantity()==0) {
				this.showDialogMessage("Required Quantity Can Not Be Zero");
				return false;
			}
		}
		return true;
	}
	
	// ************************************ Validate Warehouse *********************************************
		public boolean validateWarehouse(){
			int ctr=0;
			for(int i=0;i<subProductTableMrn.getDataprovider().getList().size();i++){
				if(!subProductTableMrn.getDataprovider().getList().get(i).getMaterialProductWarehouse().equals("")
						&&!subProductTableMrn.getDataprovider().getList().get(i).getMaterialProductStorageBin().equals("")){
					ctr++;
				}
			}
			if(ctr==subProductTableMrn.getDataprovider().getList().size()){
				return true;
			}else{
				return false;
			}
		}

	// *************************************** On Click Method ***************************************************
	
	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource().equals(btnAdd)) {
			setSubProductTable();
		}
	}
	
	// ***************************** Set Material Product On Button Click ******************************************
	
	public void setSubProductTable(){
		
		if(validateProduct()){
		if (productInfoComposite.getValue() != null) {
//			if(oblwarehouse.getSelectedIndex()!=0&&lsStorageLoc.getSelectedIndex()!=0&&lsStoragebin.getSelectedIndex()!=0){
			MaterialProduct prod = new MaterialProduct();
			prod.setMaterialProductId(Integer.parseInt(productInfoComposite.getProdID().getValue()));
			prod.setMaterialProductCode(productInfoComposite.getProdCode().getValue());
			prod.setMaterialProductCategory(productInfoComposite.getProdCategory().getValue());
			prod.setMaterialProductName(productInfoComposite.getProdName().getValue());
			prod.setMaterialProductAvailableQuantity(0);
			prod.setMaterialProductUOM(productInfoComposite.getUnitOfmeasuerment().getValue());
			prod.setMaterialProductRequiredQuantity(0);
			prod.setMaterialProductRemarks("");
			prod.setMaterialProductWarehouse("");
			prod.setMaterialProductStorageLocation("");
			prod.setMaterialProductStorageBin("");
			subProductTableMrn.getDataprovider().getList().add(prod);
			productInfoComposite.clear();
		}
		else{
			showDialogMessage("Enter Product Id");
		}
		}
	}
	
	
	/*******************************************Type Drop Down Logic**************************************/
	
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(oblMrnCategory))
		{
			if(oblMrnCategory.getSelectedIndex()!=0){
				ConfigCategory cat=oblMrnCategory.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(oblMrnType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
		
	}
	
	
	
	// *********************************** Getter and Setter **************************************************
	
	public DateBox getDbMrnDate() {
		return dbMrnDate;
	}

	public TextBox getIbMrnId() {
		return ibMrnId;
	}

	public void setIbMrnId(TextBox ibMrnId) {
		this.ibMrnId = ibMrnId;
	}

	public void setDbMrnDate(DateBox dbMrnDate) {
		this.dbMrnDate = dbMrnDate;
	}

	public TextBox getIbMrnSoId() {
		return ibMrnSoId;
	}

	public void setIbMrnSoId(TextBox ibMrnSoId) {
		this.ibMrnSoId = ibMrnSoId;
	}

	public DateBox getDbMrnSoDate() {
		return dbMrnSoDate;
	}

	public void setDbMrnSoDate(DateBox dbMrnSoDate) {
		this.dbMrnSoDate = dbMrnSoDate;
	}

	public TextBox getIbMrnWoId() {
		return ibMrnWoId;
	}

	public void setIbMrnWoId(TextBox ibMrnWoId) {
		this.ibMrnWoId = ibMrnWoId;
	}

	public DateBox getDbMrnWoDate() {
		return dbMrnWoDate;
	}

	public void setDbMrnWoDate(DateBox dbMrnWoDate) {
		this.dbMrnWoDate = dbMrnWoDate;
	}

	public DateBox getDbMrnDeliveryDate() {
		return dbMrnDeliveryDate;
	}

	public void setDbMrnDeliveryDate(DateBox dbMrnDeliveryDate) {
		this.dbMrnDeliveryDate = dbMrnDeliveryDate;
	}

	public ObjectListBox<Branch> getOblMrnBranch() {
		return oblMrnBranch;
	}

	public void setOblMrnBranch(ObjectListBox<Branch> oblMrnBranch) {
		this.oblMrnBranch = oblMrnBranch;
	}

	public ObjectListBox<HrProject> getOblMrnProject() {
		return oblMrnProject;
	}

	public void setOblMrnProject(ObjectListBox<HrProject> oblMrnProject) {
		this.oblMrnProject = oblMrnProject;
	}

	public ObjectListBox<Employee> getOblMrnSalesPerson() {
		return oblMrnSalesPerson;
	}

	public void setOblMrnSalesPerson(ObjectListBox<Employee> oblMrnSalesPerson) {
		this.oblMrnSalesPerson = oblMrnSalesPerson;
	}

	public ObjectListBox<Employee> getOblMrnApproverName() {
		return oblMrnApproverName;
	}

	public void setOblMrnApproverName(ObjectListBox<Employee> oblMrnApproverName) {
		this.oblMrnApproverName = oblMrnApproverName;
	}

	public ObjectListBox<Employee> getOblMrnRequestedBy() {
		return oblMrnRequestedBy;
	}

	public void setOblMrnRequestedBy(ObjectListBox<Employee> oblMrnRequestedBy) {
		this.oblMrnRequestedBy = oblMrnRequestedBy;
	}

	public TextBox getTbMrnStatus() {
		return tbMrnStatus;
	}

	public void setTbMrnStatus(TextBox tbMrnStatus) {
		this.tbMrnStatus = tbMrnStatus;
	}

	public TextBox getTbMrnCommentOnStatusChange() {
		return tbMrnCommentOnStatusChange;
	}

	public void setTbMrnCommentOnStatusChange(TextBox tbMrnCommentOnStatusChange) {
		this.tbMrnCommentOnStatusChange = tbMrnCommentOnStatusChange;
	}

	public ProductTableMrn getProductTableMrn() {
		return productTableMrn;
	}

	public void setProductTableMrn(ProductTableMrn productTableMrn) {
		this.productTableMrn = productTableMrn;
	}

	public ProductInfoComposite getProductInfoComposite() {
		return productInfoComposite;
	}

	public void setProductInfoComposite(ProductInfoComposite productInfoComposite) {
		this.productInfoComposite = productInfoComposite;
	}

	public Button getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(Button btnAdd) {
		this.btnAdd = btnAdd;
	}

	public SubProductTableMrn getSubProductTableMrn() {
		return subProductTableMrn;
	}

	public void setSubProductTableMrn(SubProductTableMrn subProductTableMrn) {
		this.subProductTableMrn = subProductTableMrn;
	}

	public TextBox getTbMrnTitle() {
		return tbMrnTitle;
	}

	public void setTbMrnTitle(TextBox tbMrnTitle) {
		this.tbMrnTitle = tbMrnTitle;
	}

	public DateBox getDbMrnRequiredDate() {
		return dbMrnRequiredDate;
	}

	public void setDbMrnRequiredDate(DateBox dbMrnRequiredDate) {
		this.dbMrnRequiredDate = dbMrnRequiredDate;
	}

	public ObjectListBox<ConfigCategory> getOblMrnCategory() {
		return oblMrnCategory;
	}

	public void setOblMrnCategory(ObjectListBox<ConfigCategory> oblMrnCategory) {
		this.oblMrnCategory = oblMrnCategory;
	}

	public ObjectListBox<Type> getOblMrnType() {
		return oblMrnType;
	}

	public void setOblMrnType(ObjectListBox<Type> oblMrnType) {
		this.oblMrnType = oblMrnType;
	}

	public UploadComposite getUpload() {
		return upload;
	}

	public void setUpload(UploadComposite upload) {
		this.upload = upload;
	}

	public TextArea getTaMrnDescription() {
		return taMrnDescription;
	}

	public void setTaMrnDescription(TextArea taMrnDescription) {
		this.taMrnDescription = taMrnDescription;
	}

	public TextBox getTbMrnRefNum() {
		return tbMrnRefNum;
	}

	public void setTbMrnRefNum(TextBox tbMrnRefNum) {
		this.tbMrnRefNum = tbMrnRefNum;
	}

	public DateBox getDbMrnRefDate() {
		return dbMrnRefDate;
	}

	public void setDbMrnRefDate(DateBox dbMrnRefDate) {
		this.dbMrnRefDate = dbMrnRefDate;
	}

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		subProductTableMrn.getTable().redraw();
		productTableMrn.getTable().redraw();
	}
	
	
	
	
	

}
