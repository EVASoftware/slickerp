package com.slicktechnologies.client.views.inventory.billofmaterial;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.commons.io.output.ProxyWriter;


import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.billofmaterial.BillOfMaterialTable;
import com.slicktechnologies.client.views.project.concproject.ToolTable;
import com.slicktechnologies.client.views.project.toolGroup.ToolGroupTable;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.server.printing.Printer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.BillProductDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;

public class BillOfMaterialForm extends FormScreen<BillOfMaterial> implements ClickHandler{
	
	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	TextBox tbTitle,tbCode,tbName;
	IntegerBox ibProductGroupId,ibProductId;
	TextBox tbCount1;
	CheckBox status;
	public TextBox tbassetName;
    public TextBox tbBrand;
    public TextBox tbModelNo;
    public TextBox tbSrNo;
    public DateBox dbDateofManufacture;
    public DateBox dbdateofInstallation;
	Button addSteps;
	Button btnAddTool;
    Button btnAdd1;
    ToolTable tooltable;
    ToolGroupTable toolgrouptable;
    ObjectListBox<ToolGroup> olbToolGroup;
    ObjectListBox<CompanyAsset> olbTool;
	ProductInfoComposite pic;
	BillOfMaterialTable bomtable;
	boolean flag=true;

	/**
	 * nidhi
	 * 11-09-2018
	 */
	TextBox tbAreaUnit ;
	DoubleBox dbArea;
	public BillOfMaterialForm(){
		
		super();
	       createGui();
	       this.tbCount1.setEnabled(false);
	       bomtable.connectToLocal();
	}
	public BillOfMaterialForm(String[] processlevel,FormField[][] fields,FormStyle formstyle){
		
		super(processlevel,fields,formstyle);
		createGui();
	}	

	private void initalizeWidget(){
	
		ibProductId=new IntegerBox();
		tbCode=new TextBox();
		tbName=new TextBox();
		addSteps=new Button("Add");
		addSteps.addClickHandler(this);
		tbCount1=new TextBox();
		status= new CheckBox();
		status.setValue(true);
		pic=AppUtility.initiateServiceProductComposite(new SuperProduct());
		
		 olbToolGroup = new ObjectListBox<ToolGroup>();
	     AppUtility.makeToolGroupListBoxLive(olbToolGroup);
	     olbTool = new ObjectListBox<CompanyAsset>();
	     AppUtility.makeCompanyAssetListBoxLive(olbTool);
	     
	 	 tbassetName = new TextBox();
	     tbBrand = new TextBox();
	     tbModelNo = new TextBox();
	     tbSrNo = new TextBox();
	     dbDateofManufacture = new DateBoxWithYearSelector();
	     dbdateofInstallation = new DateBoxWithYearSelector();
	 	
	     tooltable = new ToolTable();
	     tooltable.connectToLocal();
	     toolgrouptable = new ToolGroupTable();
	     toolgrouptable.connectToLocal();
	     btnAddTool = new Button("Add Tool");
	     btnAddTool.addClickHandler(this);
	     btnAdd1 = new Button("Add ToolSet");
	     btnAdd1.addClickHandler(this);
	     
	     bomtable=new BillOfMaterialTable();
	    
	     tbAreaUnit = new TextBox();
	     dbArea = new DoubleBox();
	}

	@Override
	public void createScreen() {

		initalizeWidget();	
		
		this.processlevelBarNames = new String[]{"New"};
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fMaterialGroup=fbuilder.setlabel("Material Group").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("Bill Of Material ID",tbCount1);
    	FormField ftbCount1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
    	
    	fbuilder = new FormFieldBuilder("Status",status);
		FormField fstatus=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",addSteps);
		FormField faddStep= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", bomtable.getTable());
	    FormField fbomtable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(6).build();
		
		FormField fgroupingToolAssetInfo = fbuilder.setlabel("Tool Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(6).build();
	        fbuilder = new FormFieldBuilder("Tool", olbTool);
	        FormField folbTool = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	        fbuilder = new FormFieldBuilder("", btnAddTool);
	        FormField fbtnAdd = fbuilder.build();
	        fbuilder = new FormFieldBuilder("Tool Set", olbToolGroup);
	        FormField folbToolGroup = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	        fbuilder = new FormFieldBuilder("", btnAdd1);
	        FormField fbtnAdd1 = fbuilder.build();
	        fbuilder = new FormFieldBuilder("", tooltable.getTable());
	        FormField ftooltable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	        fbuilder = new FormFieldBuilder("", toolgrouptable.getTable());
	        FormField ftoolgrouptable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",pic);
		FormField fcom =fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		FormField [][] formFields ={{fMaterialGroup},
				                    {ftbCount1,fstatus},
				                    {fcom,faddStep},
				                    {fbomtable},
				                    {fgroupingToolAssetInfo},
				                    {folbToolGroup, fbtnAdd1, folbTool, fbtnAdd}, 
				                    {ftooltable}
		                            };
		this.fields=formFields;
	}

	
	@Override
	public void updateModel(BillOfMaterial model) {
		
		if(!pic.getProdID().getValue().equals(""))
		{
			model.setProduct_id(Integer.parseInt(pic.getProdID().getValue()));
		}
		
		if(!pic.getProdCode().getValue().equals(""))
		{
			model.setCode(pic.getProdCode().getValue());
		}
		
		if(!pic.getProdName().getValue().equals(""))
		{
			model.setName(pic.getProdName().getValue());
		}
		
		if(status.getValue()!=null)
			model.setStatus(status.getValue());
	
		model.setBillProdItems(bomtable.getValue());
		model.setToolItems(tooltable.getValue());
	}
		  
	@Override
	public void updateView(BillOfMaterial view) {
		
		tbCount1.setValue(view.getCount()+"");
		bomtable.setValue(view.getBillProdItems());
		tooltable.setValue(view.getToolItems());
	}

	
	 
		@Override
		public void toggleAppHeaderBarMenu() {
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Save")||text.equals("Discard")||text.equals("Search"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   
				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Save")||text.equals("Discard"))
						menus[k].setVisible(true); 

					else
						menus[k].setVisible(false);  		   
				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Discard")||text.equals("Edit")||text.equals("Search"))
						menus[k].setVisible(true); 

					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.BILLOFMATERIAL,LoginPresenter.currentModule.trim());
		}
		
	
	
	
	
	@Override
		public void setToEditState() {
			super.setToEditState();
			this.processLevelBar.setVisibleFalse(false);
		}
	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource().equals(addSteps))
		 {
			
			ScreeenState screenState=AppMemory.getAppMemory().currentState;
			String currentStateScreen=screenState.toString();
			System.out.println("screenState"+currentStateScreen);
			
			if(pic.getValue()!=null)
			{
				if(currentStateScreen.trim().equals("NEW"))
				{
					System.out.println("If");
					bomtable.connectToLocal();
					addingNoOfServ();
					CheckBOMExist();
				}
				
				if(currentStateScreen.trim().equals("EDIT"))
				{
					int prodId=bomtable.getDataprovider().getList().get(0).getProdId();
					System.out.println("Id of Product"+prodId);
					
					if(Integer.parseInt(pic.getProdID().getValue().trim())==prodId)
					{
						bomtable.connectToLocal();
						addingNoOfServ();
					}
					if(Integer.parseInt(pic.getProdID().getValue().trim())!=prodId)
					{
						showDialogMessage("Please use old product only!");
					}
				}
			}	
			else
			{
			showDialogMessage("Add atleast one Product ");	
			}
		}
				
		 if(event.getSource().equals(btnAddTool))
	        {
	            if(this.olbTool.getValue()!=null)
	            {
	        	  reactOnAddTool();
	            }
	        	  else
	        	{
	        		showDialogMessage("Select atlest one Tool from Tool List");
	        	}
	        }
		 
		 if(event.getSource().equals(btnAdd1))
	        {
	        	if(this.olbToolGroup.getValue()!=null)
	        	{
	        		reactOnToolSet();
	        	}
	        	else
	        	{
	        		showDialogMessage("Select atleast one Tool Set from Tool Set List");
	        	}
	        }
		}
	
	private void CheckBOMExist() {

		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("product_id");
		filter.setIntValue(Integer.parseInt(pic.getProdID().getValue()));
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillOfMaterial());
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {

			showDialogMessage("An Unexpected Error occured !");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for(SuperModel model:result)
				{
					BillOfMaterial bom=(BillOfMaterial)model;
					if(!pic.getProdID().getValue().equals("")){
						int id=Integer.parseInt(pic.getProdID().getValue().trim());
						if(id==bom.getProduct_id()){
							flag=false;
						}
					}
				}
			}		
		});
	}		
	
	public void setCount(int count)
	{
		tbCount1.setValue(count+"");
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		this.bomtable.setEnable(state);
		this.tooltable.setEnable(state);
		status.setValue(true);
		tbCount1.setEnabled(false);
		
	}
	
	protected void addingNoOfServ(){
		
		final MyQuerry querry = new MyQuerry();
		final Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(pic.getProdID().getValue()));
		filtervec.add(filter);
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ServiceProduct());
		
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					ServiceProduct sup=(ServiceProduct) result.get(0);
					
					final int noOfSer=sup.getNumberOfServices();
								
								ArrayList<BillProductDetails> list=new ArrayList<BillProductDetails>();
							
								for(int i=1;i<=noOfSer;i++){
									
									BillProductDetails bill=new BillProductDetails();
									
										bill.setServiceNo(i);
										bill.setProdId(Integer.parseInt(pic.getProdID().getValue()));
										bill.setProdName(pic.getProdName().getValue());
										bill.setProdCode(pic.getProdCode().getValue());
										list.add(bill);
								}
								bomtable.getDataprovider().getList().addAll(list);	
							}
							}
				@Override
				public void onFailure(Throwable caught) {
						
				}
					});
	}
	
	
	private void reactOnAddTool()
	    {
	        CompanyAsset asset = olbTool.getSelectedItem();
	        List<CompanyAsset> list = tooltable.getDataprovider().getList();
	        if(list != null)
	        {
	            if(list.contains(asset)==false)
	        	   list.add(asset);
	            olbTool.setSelectedIndex(0);
	        }
	        olbTool.setSelectedIndex(0);
	    }
	  
	    private void reactOnToolSet()
	    {
	        ToolGroup toolgrp = olbToolGroup.getSelectedItem();
	        List<CompanyAsset> array = toolgrp.getArraylistTool();
	        List<CompanyAsset>compast= tooltable.getDataprovider().getList();
	        if(array != null)
	        {
	        	for(CompanyAsset ast:array)
	        	{
	        	    if(compast.contains(ast)==false)
	        		   tooltable.getDataprovider().getList().add(ast);
	        	}  
	        }
	        olbToolGroup.setSelectedIndex(0);
	    }

	
	@Override
	public boolean validate() {
		boolean superres = super.validate();
		boolean tableval=true;
		boolean productval=true;
		if(tooltable.getDataprovider().getList().size()==0 && (bomtable.getDataprovider().getList().size()==0))	
		{
			showDialogMessage("Add Material Required and Tools Required");
			tableval= false;
		}
		if(flag==false){
			showDialogMessage("Bill Of Material for this is already exist....Please Edit");
			productval=false;
		}
		return superres&&tableval&&productval;
	}
}
