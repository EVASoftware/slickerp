package com.slicktechnologies.client.views.inventory.billofmaterial;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;

public class BillOfMaterialTableProxy extends SuperTable<BillOfMaterial>{

	TextColumn<BillOfMaterial>getProductIdcolumn;
	TextColumn<BillOfMaterial>getCodecolumn;
	TextColumn<BillOfMaterial>getNamecolumn;
	
	
	@Override
	public void createTable() {
		addColumngetProductId();
		addColumngetCode();
		addColumngetName();
	}

	private void addColumngetCode() {
		
		getCodecolumn=new TextColumn<BillOfMaterial>() {
			public String getValue(BillOfMaterial object)
			{
				return object.getCode();
			}
		};
		table.addColumn(getCodecolumn,"Code");
		getCodecolumn.setSortable(true);
	}

	private void addColumngetName() {
		
		getNamecolumn=new TextColumn<BillOfMaterial>() {
			public String getValue(BillOfMaterial object)
			{
				return object.getName();
			}
		};
		table.addColumn(getNamecolumn,"Name");
		getNamecolumn.setSortable(true);
	}

	private void addColumngetProductId() {
		
		getProductIdcolumn=new TextColumn<BillOfMaterial>() {
			public String getValue(BillOfMaterial object)
			{
				return object.getProduct_id()+"";
			}
		};
		table.addColumn(getProductIdcolumn,"ProductId");
		getProductIdcolumn.setSortable(true);
	}
	
	
	public void addColumnSorting(){
		
		addColumnProductIdSorting();
		addColumnCodeSorting();
		addColumnNameSorting();
	}

	
	private void addColumnProductIdSorting() {
		
		List<BillOfMaterial> list = getDataprovider().getList();
		columnSort = new ListHandler<BillOfMaterial>(list);
		Comparator<BillOfMaterial> com = new Comparator<BillOfMaterial>() {

			@Override
			public int compare(BillOfMaterial e1,BillOfMaterial e2) {

				if(e1!=null && e2!=null){
					if(e1.getProduct_id()== e2.getProduct_id())
					{
						return 0;
					}
						if(e1.getProduct_id()> e2.getProduct_id())	
					{
						return 1;
					}
						else
					{
						return -1;
					}}else					
				
						return 0;
					}
			};
		columnSort.setComparator(getProductIdcolumn, com);
		table.addColumnSortHandler(columnSort);
	}

	
	private void addColumnNameSorting() {
		
		List<BillOfMaterial> list = getDataprovider().getList();
		columnSort = new ListHandler<BillOfMaterial>(list);
		Comparator<BillOfMaterial> com = new Comparator<BillOfMaterial>() {

			@Override
			public int compare(BillOfMaterial e1,BillOfMaterial e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getName()!=null && e2.getName()!=null){
						return e1.getName().compareTo(e2.getName());}
				}
				  else{
				  return 0;}
				  return 0;
				}
		};
		columnSort.setComparator(getNamecolumn, com);
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnCodeSorting() {
		List<BillOfMaterial> list = getDataprovider().getList();
		columnSort = new ListHandler<BillOfMaterial>(list);
		Comparator<BillOfMaterial> com = new Comparator<BillOfMaterial>() {

			@Override
			public int compare(BillOfMaterial e1,BillOfMaterial e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getCode()!=null && e2.getCode()!=null){
						return e1.getCode().compareTo(e2.getCode());}
				}
				  else{
				  return 0;}
				  return 0;
				}
		};
		columnSort.setComparator(getCodecolumn, com);
		table.addColumnSortHandler(columnSort);
		}

	

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}
	/*****************************Getters and Setters***********************/

	public TextColumn<BillOfMaterial> getGetProductIdcolumn() {
		return getProductIdcolumn;
	}

	public void setGetProductIdcolumn(TextColumn<BillOfMaterial> getProductIdcolumn) {
		this.getProductIdcolumn = getProductIdcolumn;
	}

	public TextColumn<BillOfMaterial> getGetNamecolumn() {
		return getNamecolumn;
	}

	public void setGetNamecolumn(TextColumn<BillOfMaterial> getNamecolumn) {
		this.getNamecolumn = getNamecolumn;
	}

	public TextColumn<BillOfMaterial> getGetCodecolumn() {
		return getCodecolumn;
	}

	public void setGetCodecolumn(TextColumn<BillOfMaterial> getCodecolumn) {
		this.getCodecolumn = getCodecolumn;
	}

}
