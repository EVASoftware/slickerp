package com.slicktechnologies.client.views.inventory.billofmaterial;

import java.util.Vector;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class BillOfMaterialSearchProxy extends SearchPopUpScreen<BillOfMaterial>{

	public IntegerBox tbProductId;
	public TextBox tbCode,tbName;
	ProductInfoComposite pic;
	
	public BillOfMaterialSearchProxy(){
		
		super();
	    createGui();
	} 
	
	public BillOfMaterialSearchProxy(boolean b){
		
		super(b);
	    createGui();
	}
	public void initWidget(){
		

		tbProductId = new IntegerBox();
		tbCode = new TextBox();
		tbName = new TextBox();
		pic=AppUtility.initiateServiceProductComposite(new SuperProduct());
	}
	
	public void createScreen(){	
		initWidget();
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder("",pic);
		FormField fpic =builder.setMandatory(false).setRowSpan(0).setColSpan(5).build();



		this.fields= new FormField[][]{
				{},
				{fpic},
	    };
	}

	@Override
	public MyQuerry getQuerry() {
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
	
			if (!pic.getProdCode().getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(pic.getProdCode().getValue().trim());
				temp.setQuerryString("billProdItems.prodCode");
				filtervec.add(temp);
			}

			if (!pic.getProdName().getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(pic.getProdName().getValue().trim());
				temp.setQuerryString("billProdItems.prodName");
				filtervec.add(temp);
			}
			
			if (pic.getIdValue()!=-1){
				temp=new Filter();
				temp.setIntValue(pic.getIdValue());
				temp.setQuerryString("billProdItems.prodId");
				filtervec.add(temp);
			}
			
			temp =new Filter();
			temp.setBooleanvalue(true);
			temp.setQuerryString("status");
			filtervec.add(temp);


				
			MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillOfMaterial());
		return querry;
	}

	public InlineLabel getDwnload() {
		return dwnload;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

		
}
