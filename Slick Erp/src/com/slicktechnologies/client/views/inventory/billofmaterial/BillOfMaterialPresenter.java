package com.slicktechnologies.client.views.inventory.billofmaterial;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class BillOfMaterialPresenter extends FormScreenPresenter<BillOfMaterial>{
	
	BillOfMaterialForm form;
	
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final GenricServiceAsync async=GWT.create(GenricService.class);
	
	public BillOfMaterialPresenter(UiScreen<BillOfMaterial> view,
			BillOfMaterial model) {
		super(view, model);
		form =(BillOfMaterialForm)view;
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.BILLOFMATERIAL,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}


	@Override
	public void reactToProcessBarEvents(ClickEvent e) {

		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initialize();
		}
	}
	
	@Override
	public void reactOnDownload() {
	CsvServiceAsync async=GWT.create(CsvService.class);
	ArrayList<BillOfMaterial> custarray=new ArrayList<BillOfMaterial>();
	List<BillOfMaterial>list=form.getSearchpopupscreen().getSupertable().getListDataProvider().getList();
	custarray.addAll(list); 
	
	async.setBillOfMaterial(custarray, new AsyncCallback<Void>() {

		@Override
		public void onFailure(Throwable caught) {
			System.out.println("RPC call Failed"+caught);
			
		}

		@Override
		public void onSuccess(Void result) {
			String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
			final String url=gwt + "csvservlet"+"?type="+66;
			Window.open(url, "test", "enabled");
		}
	});
	}
	
	@Override
	public void reactOnPrint() {
	}

	@Override
	public void reactOnEmail() {
		
	}

	@Override
	protected void makeNewModel() {
		model= new BillOfMaterial();
	}
	
	public static BillOfMaterialForm initialize(){
		
		BillOfMaterialForm form = new BillOfMaterialForm();
		BillOfMaterialTableProxy gentable=new BillOfMaterialTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		BillOfMaterialSearchProxy.staticSuperTable=gentable;
		BillOfMaterialSearchProxy searchpopup=new BillOfMaterialSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		BillOfMaterialPresenter presenter=new BillOfMaterialPresenter(form, new BillOfMaterial());		
		AppMemory.getAppMemory().stickPnel(form);
		return form;		 
		
		
	}
	

}
