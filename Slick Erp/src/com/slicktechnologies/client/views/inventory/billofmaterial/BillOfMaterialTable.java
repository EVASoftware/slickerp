package com.slicktechnologies.client.views.inventory.billofmaterial;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.BillProductDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;


public class BillOfMaterialTable extends SuperTable<BillProductDetails>implements ClickHandler{
	
	TextColumn<BillProductDetails>getServiceNocolumn;
	TextColumn<BillProductDetails>getProductIdcolumn;
	TextColumn<BillProductDetails>getNamecolumn;
	TextColumn<BillProductDetails>getCodecolumn;
	TextColumn<BillProductDetails>gettitlecolumn;
	TextColumn<BillProductDetails>getProductGroupIdcolumn;
	Column<BillProductDetails, String>ProdGroupTitle;
	TextColumn<BillProductDetails> viewProdGroupTitleColumn;
	public  ArrayList<String> binList;
	public  ArrayList<ProductGroupDetails> groupList;
	
	
	public BillOfMaterialTable(){
		super();
		
	}
	

	@Override
	public void createTable() {

		addColumngetServiceNocolumn();
		addColumngetProductId();
		addColumngetName();
		addColumngetCode();
//		gettitlecolumn();
		addColumngetProductGroupId();
		retriveProdGroupTitle();
		
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	
	private void retriveProdGroupTitle() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		
		query.setFilters(filtervec);
		query.setQuerryObject(new ProductGroupDetails());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

		
			public void onSuccess(ArrayList<SuperModel> result) {
				binList=new ArrayList<String>();
				groupList=new ArrayList<ProductGroupDetails>();
				binList.add("SELECT");
				for (SuperModel model : result) {
					ProductGroupDetails entity = (ProductGroupDetails) model;
					binList.add(entity.getTitle());
					groupList.add(entity);
				}
				addEditColumnTitle();
				addFieldUpdater();
			}
		});
	}

//	Project Group Title Selection List Column
	public void addEditColumnTitle() {
		SelectionCell employeeselection= new SelectionCell(binList);
		ProdGroupTitle = new Column<BillProductDetails, String>(employeeselection) {
			@Override
			public String getValue(BillProductDetails object) {
				if (object.getProdGroupId() != 0) {
					return object.getProdGroupTitle();
				} else
					return "N/A";
			}

			
		};
		table.addColumn(ProdGroupTitle, "Product Group Name");
		table.setColumnWidth(ProdGroupTitle,153,Unit.PX);
	}
	
	
//	Update Method for Project Group Name(ie Title)
	
	protected void createFieldUpdaterstorageProductGroupTitle() {

		ProdGroupTitle.setFieldUpdater(new FieldUpdater<BillProductDetails, String>() {
			
			@Override
			public void update(int index, BillProductDetails object, String value) {
				try {
					String val1 = (value.trim());
					object.setProdGroupTitle(val1);
					int pgid=0;
					for(int i=0;i<groupList.size();i++)
					{
						if(val1.trim().equals(groupList.get(i).getTitle().trim())){
							pgid=groupList.get(i).getCount();
							
						}
					}
					object.setProdGroupId(pgid);
					
					
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	private void addColumngetServiceNocolumn() {
		getServiceNocolumn=new TextColumn<BillProductDetails>() {
			public String getValue(BillProductDetails object)
			{
				return object.getServiceNo()+"";
			}
		};
		table.addColumn(getServiceNocolumn,"Service No" );
	}
	
	private void gettitlecolumn() {
		gettitlecolumn=new TextColumn<BillProductDetails>() {
			public String getValue(BillProductDetails object)
			{
				return object.getProdGroupTitle();
			}
		};
		table.addColumn(gettitlecolumn,"Title" );
	}

	protected void createViewTitleColumn(){
		
		viewProdGroupTitleColumn=new TextColumn<BillProductDetails>() {
			public String getValue(BillProductDetails object)
			{
				return object.getProdGroupTitle();
			}
		};
		table.addColumn(viewProdGroupTitleColumn,"Title" );
	}
		
	private void addColumngetName() {
		getNamecolumn=new TextColumn<BillProductDetails>() {
			public String getValue(BillProductDetails object)
			{
				return object.getProdName();
			}
		};
		table.addColumn(getNamecolumn,"Name" );
		getNamecolumn.setSortable(true);
	}


	private void addColumngetCode() {
		getCodecolumn=new TextColumn<BillProductDetails>() {
			public String getValue(BillProductDetails object)
			{
				return object.getProdCode();
			}
		};
		table.addColumn(getCodecolumn,"Code" );
		getCodecolumn.setSortable(true);
	}


	private void addColumngetProductId() {
		
		getProductIdcolumn=new TextColumn<BillProductDetails>()
				{
			@Override
			public String getValue(BillProductDetails object)
			{
				return object.getProdId()+"";
			}
				};
				table.addColumn(getProductIdcolumn,"Product ID");
//				getProductIdcolumn.setSortable(true);
	}
	


	private void addColumngetProductGroupId() {
		getProductGroupIdcolumn=new TextColumn<BillProductDetails>() {
			@Override
			public String getValue(BillProductDetails object)
			{
				return object.getProdGroupId()+"";
			}
		};		
		table.addColumn(getProductGroupIdcolumn,"Product Group ID");
//		getProductGroupIdcolumn.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		
	}


	@Override
	public void addFieldUpdater() {
		createFieldUpdaterstorageProductGroupTitle();
		
	}


	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void onClick(ClickEvent event) {
		
	}


	@Override
	public void setEnable(boolean state) {
		for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
	        	  
	      
	          if(state==true){
	        	  
	        	addColumngetServiceNocolumn();
	      		addColumngetProductId();
	      		addColumngetName();
	      		addColumngetCode();
	      		addColumngetProductGroupId();
	      		retriveProdGroupTitle();
	      		addFieldUpdater();
	          }
	          else{
	        	  
	        	  addColumngetServiceNocolumn();
		      		addColumngetProductId();
		      		addColumngetName();
		      		addColumngetCode();
		      		addColumngetProductGroupId();
		      	    createViewTitleColumn();
	        	   addFieldUpdater();
	          }
		
	}	

	
	
}
