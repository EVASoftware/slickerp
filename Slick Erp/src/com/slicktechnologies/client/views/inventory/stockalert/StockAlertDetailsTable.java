package com.slicktechnologies.client.views.inventory.stockalert;

import java.util.Comparator;
import java.util.List;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.cell.client.Cell.Context;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;

public class StockAlertDetailsTable extends SuperTable<ProductInventoryViewDetails> {
	
	TextColumn<ProductInventoryViewDetails> getProductIdColumn;
	TextColumn<ProductInventoryViewDetails> getProdNameColumn;
	TextColumn<ProductInventoryViewDetails> getProdCodeColumn;
	TextColumn<ProductInventoryViewDetails> getWarehouseColumn;
	TextColumn<ProductInventoryViewDetails> getStorageLocationColumn;
	TextColumn<ProductInventoryViewDetails> getStorageBinColumn;
	TextColumn<ProductInventoryViewDetails> getAvailableQuantityColumn;
	TextColumn<ProductInventoryViewDetails> getMinQuantityColumn;
	TextColumn<ProductInventoryViewDetails> getMaxQuantityColumn;
	TextColumn<ProductInventoryViewDetails> getNormalLevelColumn;
	TextColumn<ProductInventoryViewDetails> getReorderLevelColumn;
	TextColumn<ProductInventoryViewDetails> getReorderQuantityColumn;
	
	
	public StockAlertDetailsTable()
	{
		super();
		setHeight("400px");
	}

	@Override
	public void createTable() {
		
		addColumngetWarehouse();
		addColumngetStorageLocation();
		addColumngetStorageBin();
		addColumngetProductId();
		addColumngetProdCodeColumn();
		addColumngetProdName();
		addColumngetMinQuantityColumn();
		addColumngetReorderLevelColumn();
		addColumngetAvailableQuantity();
		addColumngetMaxQuantityColumn();
		addColumngetNormalLevelColumn();
		addColumngetReorderQuantityColumn();
	}
	
	
	protected void addColumngetProductId()
	{
		getProductIdColumn=new TextColumn<ProductInventoryViewDetails>()
		{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				if( object.getProdid()==0){
					return "";
				}
				else{
					return object.getProdid()+"";
				}
			}
		};
		table.addColumn(getProductIdColumn,"Product ID");
		table.setColumnWidth(getProductIdColumn, 90, Unit.PX);
		getProductIdColumn.setSortable(true);
	}
	
	protected void addColumngetProdName()
	{
		getProdNameColumn=new TextColumn<ProductInventoryViewDetails>()
		{
			@Override
			public String getCellStyleNames(Context context, ProductInventoryViewDetails object) {
				return "orange";
			}
			
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getProdname();
			}
		};
		table.addColumn(getProdNameColumn,"Product Name");
		table.setColumnWidth(getProdNameColumn, 110, Unit.PX);
		getProdNameColumn.setSortable(true);
	}
	
	protected void addColumngetProdCodeColumn()
	{
		getProdCodeColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getProdcode();
			}
				};
		table.addColumn(getProdCodeColumn,"Product Code");
		table.setColumnWidth(getProdCodeColumn, 100, Unit.PX);
		getProdCodeColumn.setSortable(true);
	}
	
	protected void addColumngetWarehouse()
	{
		getWarehouseColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getWarehousename();
			}
				};
		table.addColumn(getWarehouseColumn,"Warehouse");
		table.setColumnWidth(getWarehouseColumn, 100, Unit.PX);
		getWarehouseColumn.setSortable(true);
	}
	
	protected void addColumngetStorageLocation()
	{
		getStorageLocationColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object. getStoragelocation();
			}
				};
		table.addColumn(getStorageLocationColumn,"Storage Location");
		table.setColumnWidth(getStorageLocationColumn, 120, Unit.PX);
		getStorageLocationColumn.setSortable(true);
	}
	
	protected void addColumngetStorageBin()
	{
		getStorageBinColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getStoragebin();
			}
				};
		table.addColumn(getStorageBinColumn,"Storage Bin");
		table.setColumnWidth(getStorageBinColumn, 120, Unit.PX);
		getStorageBinColumn.setSortable(true);
	}

	protected void addColumngetAvailableQuantity()
	{
		getAvailableQuantityColumn=new TextColumn<ProductInventoryViewDetails>()
				{
					@Override
					public String getCellStyleNames(Context context, ProductInventoryViewDetails object) {
						return "orange";
					}
			
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getAvailableqty()+"";
			}
				};
		table.addColumn(getAvailableQuantityColumn,"Avl Qty");
		table.setColumnWidth(getAvailableQuantityColumn, 90, Unit.PX);
		getAvailableQuantityColumn.setSortable(true);
	}
	
	protected void addColumngetMinQuantityColumn()
	{
		getMinQuantityColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getMinqty()+"";
			}
				};
		table.addColumn(getMinQuantityColumn,"Min Qty");
		table.setColumnWidth(getMinQuantityColumn, 85, Unit.PX);
		getMinQuantityColumn.setSortable(true);
	}
	
	protected void addColumngetMaxQuantityColumn()
	{
		getMaxQuantityColumn=new TextColumn<ProductInventoryViewDetails>()
		{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getMaxqty()+"";
			}
		};
		table.addColumn(getMaxQuantityColumn,"Max Qty");
		table.setColumnWidth(getMaxQuantityColumn, 90, Unit.PX);
		getMaxQuantityColumn.setSortable(true);
	}
	
	protected void addColumngetNormalLevelColumn()
	{
		getNormalLevelColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getNormallevel()+"";
			}
				};
		table.addColumn(getNormalLevelColumn,"Normal Lvl");
		table.setColumnWidth(getNormalLevelColumn, 100, Unit.PX);
		getNormalLevelColumn.setSortable(true);
	}
	
	protected void addColumngetReorderLevelColumn()
	{
		getReorderLevelColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getReorderlevel()+"";
			}
				};
		table.addColumn(getReorderLevelColumn,"Reorder Lvl");
		table.setColumnWidth(getReorderLevelColumn, 90, Unit.PX);
		getReorderLevelColumn.setSortable(true);
	}
	
	protected void addColumngetReorderQuantityColumn()
	{
		getReorderQuantityColumn=new TextColumn<ProductInventoryViewDetails>()
				{
			@Override
			public String getValue(ProductInventoryViewDetails object)
			{
				return object.getReorderqty()+"";
			}
				};
		table.addColumn(getReorderQuantityColumn,"Reorder Qty");
		table.setColumnWidth(getReorderQuantityColumn, 90, Unit.PX);
		getReorderQuantityColumn.setSortable(true);
	}
	
	
	
	public void addColumnSorting()
	{
		addProductIdSorting();
		addProdNameSorting();
		addProdCodeSorting();
		addWarehouseSorting();
		addStorageLocationSorting();
		addStorageBinSorting();
		addAvailableQuantitySorting();
		addMinQuantitySorting();
		addMaxQuantitySorting();
		addNormalLevelSorting();
		addReorderLevelSorting();
		addReorderQuantitySorting();
		
	}
	
	
	private void addProductIdSorting() {
		
		List<ProductInventoryViewDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryViewDetails>(list);
		Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

			@Override
			public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

				if(e1!=null && e2!=null){
					if(e1.getProdid()== e2.getProdid())
					{
						return 0;
					}
						if(e1.getProdid()> e2.getProdid())	
					{
						return 1;
					}
						else
					{
						return -1;
					}}else					
				
						return 0;
					}
			};
		columnSort.setComparator(getProductIdColumn, com);
		table.addColumnSortHandler(columnSort);
	}	
	
	private void addProdNameSorting() {
		List<ProductInventoryViewDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryViewDetails>(list);
		Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

			@Override
			public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getProdname()!=null && e2.getProdname()!=null){
						return e1.getProdname().compareTo(e2.getProdname());}
				}
				  else{
				  return 0;}
				  return 0;
				}
		};
		columnSort.setComparator(getProdNameColumn, com);
		table.addColumnSortHandler(columnSort);
	}

	
	private void addProdCodeSorting() {
		
		List<ProductInventoryViewDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryViewDetails>(list);
		Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

			@Override
			public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getProdcode()!=null && e2.getProdcode()!=null)
					{
					return e1.getProdcode().compareTo(e2.getProdcode());
					}
				}
				  else
				  {
				  return 0;
				  }
				  return 0;
				}
				};
		columnSort.setComparator(getProdCodeColumn,com);
		table.addColumnSortHandler(columnSort);
	}

	private void addWarehouseSorting() {
		List<ProductInventoryViewDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryViewDetails>(list);
		Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

			@Override
			public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getWarehousename()!=null && e2.getWarehousename()!=null){
						return e1.getWarehousename().compareTo(e2.getWarehousename());}
				}
				  else{
				  return 0;}
				  return 0;
				}
		};
		columnSort.setComparator(getWarehouseColumn, com);
		table.addColumnSortHandler(columnSort);
	}
	
	private void addStorageLocationSorting() {
		List<ProductInventoryViewDetails> list = getDataprovider().getList();
		columnSort = new ListHandler<ProductInventoryViewDetails>(list);
		Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {

			@Override
			public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {

				if(e1!=null && e2!=null)
				{
					if( e1.getStoragelocation()!=null && e2.getStoragelocation()!=null){
						return e1.getStoragelocation().compareTo(e2.getStoragelocation());}
				}
				  else{
				  return 0;}
				  return 0;
				}
		};
		columnSort.setComparator(getStorageLocationColumn, com);
		table.addColumnSortHandler(columnSort);
	}

		
		private void addStorageBinSorting() {
			List<ProductInventoryViewDetails> list = getDataprovider().getList();
			columnSort = new ListHandler<ProductInventoryViewDetails>(list);
			Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {
		
				@Override
				public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {
		
					if(e1!=null && e2!=null)
					{
						if( e1.getStoragebin()!=null && e2.getStoragebin()!=null){
							return e1.getStoragebin().compareTo(e2.getStoragebin());}
					}
					  else{
					  return 0;}
					  return 0;
					}
			};
			columnSort.setComparator(getStorageBinColumn, com);
			table.addColumnSortHandler(columnSort);
		}
		
		private void addAvailableQuantitySorting() {
			
			List<ProductInventoryViewDetails> list = getDataprovider().getList();
			columnSort = new ListHandler<ProductInventoryViewDetails>(list);
			Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {
		
				@Override
				public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {
		
					if(e1!=null && e2!=null){
						if(e1.getAvailableqty()== e2.getAvailableqty())
						{
							return 0;
						}
							if(e1.getAvailableqty()> e2.getAvailableqty())	
						{
							return 1;
						}
							else
						{
							return -1;
						}}else					
					
					return 0;
				
					}
			};
			columnSort.setComparator(getAvailableQuantityColumn, com);
			table.addColumnSortHandler(columnSort);
		}
			
		private void addMinQuantitySorting() {
			
			List<ProductInventoryViewDetails> list = getDataprovider().getList();
			columnSort = new ListHandler<ProductInventoryViewDetails>(list);
			Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {
		
				@Override
				public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {
		
					if(e1!=null && e2!=null){
						if(e1.getMinqty()== e2.getMinqty())
						{
							return 0;
						}
							if(e1.getMinqty()> e2.getMinqty())	
						{
							return 1;
						}
							else
						{
							return -1;
						}}else					
					
					return 0;
				
					}
			};
			columnSort.setComparator(getMinQuantityColumn, com);
			table.addColumnSortHandler(columnSort);
		}
		  
		private void addMaxQuantitySorting() {
			
			List<ProductInventoryViewDetails> list = getDataprovider().getList();
			columnSort = new ListHandler<ProductInventoryViewDetails>(list);
			Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {
		
				@Override
				public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {
		
					if(e1!=null && e2!=null){
						if(e1.getMaxqty()== e2.getMaxqty())
						{
							return 0;
						}
							if(e1.getMaxqty()> e2.getMaxqty())	
						{
							return 1;
						}
							else
						{
							return -1;
						}}else					
					
					return 0;
				
					}
			};
			columnSort.setComparator(getMaxQuantityColumn, com);
			table.addColumnSortHandler(columnSort);
		}
		 
		private void addNormalLevelSorting() {
			
			List<ProductInventoryViewDetails> list = getDataprovider().getList();
			columnSort = new ListHandler<ProductInventoryViewDetails>(list);
			Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {
		
				@Override
				public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {
		
					if(e1!=null && e2!=null){
						if(e1.getNormallevel()== e2.getNormallevel())
						{
							return 0;
						}
							if(e1.getNormallevel()> e2.getNormallevel())	
						{
							return 1;
						}
							else
						{
							return -1;
						}}else					
					
					return 0;
				
					}
			};
			columnSort.setComparator(getNormalLevelColumn, com);
			table.addColumnSortHandler(columnSort);
		}
		
		private void addReorderLevelSorting() {
			
			List<ProductInventoryViewDetails> list = getDataprovider().getList();
			columnSort = new ListHandler<ProductInventoryViewDetails>(list);
			Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {
		
				@Override
				public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {
		
					if(e1!=null && e2!=null){
						if(e1.getReorderlevel()== e2.getReorderlevel())
						{
							return 0;
						}
							if(e1.getReorderlevel()> e2.getReorderlevel())	
						{
							return 1;
						}
							else
						{
							return -1;
						}}else					
					
					return 0;
				
					}
			};
			columnSort.setComparator(getReorderLevelColumn, com);
			table.addColumnSortHandler(columnSort);
		}
		
		private void addReorderQuantitySorting() {
			
			List<ProductInventoryViewDetails> list = getDataprovider().getList();
			columnSort = new ListHandler<ProductInventoryViewDetails>(list);
			Comparator<ProductInventoryViewDetails> com = new Comparator<ProductInventoryViewDetails>() {
		
				@Override
				public int compare(ProductInventoryViewDetails e1,ProductInventoryViewDetails e2) {
		
					if(e1!=null && e2!=null){
						if(e1.getReorderqty()== e2.getReorderqty())
						{
							return 0;
						}
							if(e1.getReorderqty()> e2.getReorderqty())	
						{
							return 1;
						}
							else
						{
							return -1;
						}}else					
					
					return 0;
				
					}
			};
			columnSort.setComparator(getReorderQuantityColumn, com);
			table.addColumnSortHandler(columnSort);
		}
		
		
		
		@Override
		protected void initializekeyprovider() {
		}
		
		@Override
		public void addFieldUpdater() {
		}
		
		@Override
		public void setEnable(boolean state) {
		}
		
		@Override
		public void applyStyle() {
		}
			
			
			
	
	
	
	
	

}
