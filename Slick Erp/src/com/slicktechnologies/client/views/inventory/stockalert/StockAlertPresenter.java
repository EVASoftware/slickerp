package com.slicktechnologies.client.views.inventory.stockalert;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;

public class StockAlertPresenter extends FormScreenPresenter<ProductInventoryViewDetails>  {
	
	StockAlertForm form;
	final GenricServiceAsync async=GWT.create(GenricService.class);
	final CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	public StockAlertPresenter(FormScreen<ProductInventoryViewDetails> view, ProductInventoryViewDetails model) {
		
		super(view,model);
		form=(StockAlertForm) view;
		form.reorderBtn.addClickHandler(this);
		form.minimumBtn.addClickHandler(this);
		form.setPresenter(this);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
	}

	@Override
	public void reactOnDownload() {
		
		
		ArrayList<ProductInventoryViewDetails> custarray=new ArrayList<ProductInventoryViewDetails>();
		List<ProductInventoryViewDetails> list=form.getTable().getListDataProvider().getList();
		
		custarray.addAll(list); 
		
		csvservice.setStockAlertList(custarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+89;
				Window.open(url, "test", "enabled");
			}
		});
	}

	
	public static void initalize() {
		AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		StockAlertForm form = new StockAlertForm();
//		form.setToViewState();
		StockAlertPresenter presenter = new StockAlertPresenter(form, new ProductInventoryViewDetails());
		AppMemory.getAppMemory().stickPnel(form);
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		if(event.getSource()==form.reorderBtn){
			if(form.oblwarehouse.getSelectedIndex()==0&&form.lststoragebin.getSelectedIndex()==0&&form.lststoragelocation.getSelectedIndex()==0&&form.productInfoComposite.getProdID().getValue().equals("")){
				form.showDialogMessage("Please select atleast one search criteria!");
			}
			
			if(form.oblwarehouse.getSelectedIndex()!=0||form.lststoragebin.getSelectedIndex()!=0||form.lststoragelocation.getSelectedIndex()!=0||!form.productInfoComposite.getProdID().getValue().equals("")){
				reorderLevelButton();
			}
		}
		
		if(event.getSource()==form.minimumBtn){
			if(form.oblwarehouse.getSelectedIndex()==0&&form.lststoragebin.getSelectedIndex()==0&&form.lststoragelocation.getSelectedIndex()==0&&form.productInfoComposite.getProdID().getValue().equals("")){
				form.showDialogMessage("Please select atleast one search criteria!");
			}
			
			if(form.oblwarehouse.getSelectedIndex()!=0||form.lststoragebin.getSelectedIndex()!=0||form.lststoragelocation.getSelectedIndex()!=0||!form.productInfoComposite.getProdID().getValue().equals("")){
				minimumLevelButton();
			}
		}	
		
	}
		
	
	private void minimumLevelButton() {
		
		form.getTable().connectToLocal();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filtr = null;
		
		System.out.println("Company id :: "+model.getCompanyId());
		filtr = new Filter();
		filtr.setLongValue(model.getCompanyId());
		filtr.setQuerryString("companyId");
		filtervec.add(filtr);
		
		if (!form.productInfoComposite.getProdID().getValue().equals("")) {
			filtr = new Filter();
			filtr.setIntValue(Integer.parseInt(form.productInfoComposite.getProdID().getValue()));
			filtr.setQuerryString("prodid");
			filtervec.add(filtr);
		}
		
		if(form.oblwarehouse.getSelectedIndex()!=0){
			filtr=new Filter();
			filtr.setStringValue(form.oblwarehouse.getValue().trim());
			filtr.setQuerryString("warehousename");
			filtervec.add(filtr);
		}
		
		 if(form.lststoragelocation.getSelectedIndex()!=0)
		  {
			 filtr=new Filter();
			 filtr.setStringValue(form.lststoragelocation.getValue(form.lststoragelocation.getSelectedIndex()).trim());
			 filtr.setQuerryString("storagelocation");
			 filtervec.add(filtr);
		  }
		 
		 if(form.lststoragebin.getSelectedIndex()!=0)
		  {
			 filtr=new Filter();
			 filtr.setStringValue(form.lststoragebin.getValue(form.lststoragebin.getSelectedIndex()).trim());
			 filtr.setQuerryString("storagebin");
			 filtervec.add(filtr);
		  }
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProductInventoryViewDetails());
		form.showWaitSymbol();
		
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						ArrayList<ProductInventoryViewDetails> prodDetails=new ArrayList<ProductInventoryViewDetails>();
						ArrayList<ProductInventoryViewDetails> filteredlist=new ArrayList<ProductInventoryViewDetails>();
						if (result.size() != 0) {
							for(SuperModel model:result){
								ProductInventoryViewDetails piv=(ProductInventoryViewDetails) model;
								prodDetails.add(piv);
							}
							
							filteredlist= minimumLevel(prodDetails);
							if(filteredlist.size()!=0){
								form.getTable().getDataprovider().setList(filteredlist);
							}
							else{
								form.showDialogMessage("No Record found");
							}
							
						}
						else{
							form.showDialogMessage("No Record found");
						}
					}
					@Override
					public void onFailure(Throwable caught) {

					}
				});
		
		form.hideWaitSymbol();
		
	}
	
	public ArrayList<ProductInventoryViewDetails> minimumLevel(ArrayList<ProductInventoryViewDetails> list ) {
			
			ArrayList<ProductInventoryViewDetails> filterlist=new ArrayList<ProductInventoryViewDetails>();
			for(int i=0;i<list.size();i++){
				
				if(list.get(i).getAvailableqty()<=list.get(i).getMinqty()){
					
					filterlist.add(list.get(i));
				}
			}
			
			
			return filterlist;
		}
	
	

	private void reorderLevelButton() {
		
			form.getTable().connectToLocal();
			
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filtr = null;
			filtr = new Filter();
			filtr.setLongValue(model.getCompanyId());
			filtr.setQuerryString("companyId");
			filtervec.add(filtr);
			
			if (!form.productInfoComposite.getProdID().getValue().equals("")) {
				filtr = new Filter();
				filtr.setIntValue(Integer.parseInt(form.productInfoComposite.getProdID().getValue()));
				filtr.setQuerryString("prodid");
				filtervec.add(filtr);
			}
			
			if(form.oblwarehouse.getSelectedIndex()!=0){
				filtr=new Filter();
				filtr.setStringValue(form.oblwarehouse.getValue().trim());
				filtr.setQuerryString("warehousename");
				filtervec.add(filtr);
			}
			
			 if(form.lststoragelocation.getSelectedIndex()!=0)
			  {
				 filtr=new Filter();
				 filtr.setStringValue(form.lststoragelocation.getValue(form.lststoragelocation.getSelectedIndex()).trim());
				 filtr.setQuerryString("storagelocation");
				 filtervec.add(filtr);
			  }
			 
			 if(form.lststoragebin.getSelectedIndex()!=0)
			  {
				 filtr=new Filter();
				 filtr.setStringValue(form.lststoragebin.getValue(form.lststoragebin.getSelectedIndex()).trim());
				 filtr.setQuerryString("storagebin");
				 filtervec.add(filtr);
			  }
			
			MyQuerry querry = new MyQuerry();
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ProductInventoryViewDetails());
			form.showWaitSymbol();
			
			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							ArrayList<ProductInventoryViewDetails> prodDetails=new ArrayList<ProductInventoryViewDetails>();
							ArrayList<ProductInventoryViewDetails> filteredlist=new ArrayList<ProductInventoryViewDetails>();
							if (result.size() != 0) {
								for(SuperModel model:result){
									ProductInventoryViewDetails piv=(ProductInventoryViewDetails) model;
									prodDetails.add(piv);
								}
								
								filteredlist= ReorderLevel(prodDetails);
								if(filteredlist.size()!=0){
									form.getTable().getDataprovider().setList(filteredlist);
								}
								else{
									form.showDialogMessage("No Record found");
								}
							}
							else{
								form.showDialogMessage("No Record found");
							}
						}
						@Override
						public void onFailure(Throwable caught) {

						}
					});
			
			form.hideWaitSymbol();	
	}
	
	public ArrayList<ProductInventoryViewDetails> ReorderLevel(ArrayList<ProductInventoryViewDetails> list ) {
		ArrayList<ProductInventoryViewDetails> filterlist=new ArrayList<ProductInventoryViewDetails>();
		for(int i=0;i<list.size();i++){
			
			if(list.get(i).getAvailableqty()<=list.get(i).getReorderlevel()&&list.get(i).getAvailableqty()>list.get(i).getMinqty()){
				
				filterlist.add(list.get(i));
			}
		}
		return filterlist;
	}

	@Override
	public void reactOnPrint() {
	}

	@Override
	public void reactOnEmail() {
	}

	@Override
	protected void makeNewModel() {
	}

	

}
