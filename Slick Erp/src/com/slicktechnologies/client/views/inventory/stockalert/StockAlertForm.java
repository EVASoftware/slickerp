package com.slicktechnologies.client.views.inventory.stockalert;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.productinventoryview.ProductInventoryViewForm;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class StockAlertForm extends FormScreen<ProductInventoryViewDetails> implements ChangeHandler {
	

	ProductInfoComposite productInfoComposite;
	ObjectListBox<WareHouse> oblwarehouse;
	ListBox lststoragelocation;
	ListBox lststoragebin;
	Button minimumBtn, reorderBtn;
	StockAlertDetailsTable table;
	ArrayList<String> storageList=new ArrayList<String>();
	final GenricServiceAsync async = GWT.create(GenricService.class);
	
	
	public StockAlertForm() {
		super(FormStyle.DEFAULT);
//		super();
		createGui();
		table.connectToLocal();
	}
	
	public StockAlertForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}
	
	
	private void initializeWidget() {
		productInfoComposite = AppUtility.initiateSalesProductComposite(new SuperProduct());
		
		oblwarehouse = new ObjectListBox<WareHouse>();
		oblwarehouse.addChangeHandler(this);
		AppUtility.initializeWarehouse(oblwarehouse);
		
		lststoragelocation = new ListBox();
		lststoragelocation.addChangeHandler(this);
		lststoragelocation.addItem("--SELECT--");		
		
		lststoragebin = new ListBox();
		lststoragebin.addItem("--SELECT--");		
		
		reorderBtn = new Button("Reorder Level");
//		reorderBtn.addClickHandler(this);
		
		minimumBtn = new Button("Minimum Level");
//		minimumBtn.addClickHandler(this);
		
		table = new StockAlertDetailsTable();
		
	
	}
	


	@Override
	public void createScreen() {
		
		initializeWidget();
		
		FormFieldBuilder builder;
		builder = new FormFieldBuilder();
		FormField fgroupingProductInformation = builder.setlabel("Product Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		builder = new FormFieldBuilder();
		FormField fgroupingInventoryInformation = builder.setlabel("Inventory Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		builder = new FormFieldBuilder("", reorderBtn);
		FormField fadd = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("", minimumBtn);
		FormField fadd2 = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite = builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		builder = new FormFieldBuilder("Warehouse",oblwarehouse);
		FormField InvWarehouse= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Storage Location",lststoragelocation);
		FormField StorageLoc= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Storage Bin",lststoragebin);
		FormField StorageBin= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("", table.getTable());
		FormField ftable = builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
					
				FormField[][] formfield = { 
						{fgroupingProductInformation},
						{ fproductInfoComposite },
						{fgroupingInventoryInformation},
						{InvWarehouse,StorageLoc,StorageBin},
						{fadd,fadd2},
						{ftable}
				};

		this.fields = formfield;
	}
	
	@Override
	public void updateModel(ProductInventoryViewDetails model) {
		
		
	}

	@Override
	public void updateView(ProductInventoryViewDetails model) {
	}

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Download"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Download"))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.STOCKALERT,LoginPresenter.currentModule.trim());
	}
	
	
	private void initializeWarehouse() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new WareHouse());
		oblwarehouse.MakeLive(querry);
		
	}

	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(oblwarehouse))
		{
			retrieveStorogeLocation();
		}
		if(event.getSource().equals(lststoragelocation))
		{
			retrieveStorageBin();
		}
	}
	
	
	private void retrieveStorogeLocation() {
		
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("warehouseName");
		filter.setStringValue(oblwarehouse.getValue());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new StorageLocation());
		
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				lststoragelocation.clear();
				lststoragebin.clear();
				lststoragelocation.addItem("--SELECT--");
				lststoragebin.addItem("--SELECT--");
				for (SuperModel model : result) {
					StorageLocation entity = (StorageLocation) model;
					storageList.add(entity.getBusinessUnitName());
					lststoragelocation.addItem(entity.getBusinessUnitName());
				}
			}
		});
	}

		/****************************************  retrieving Storage Bin    **********************************************/

	private void retrieveStorageBin() {
		
		MyQuerry query = new MyQuerry();
		Company c = new Company();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("storagelocation");
		filter.setStringValue(lststoragelocation.getValue(lststoragelocation.getSelectedIndex()));
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());
		
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				lststoragebin.clear();
				lststoragebin.addItem("--SELECT--");
				for (SuperModel model : result) {
					Storagebin entity = (Storagebin) model;
					lststoragebin.addItem(entity.getBinName());
				}
			}
		});
		
	}
	
	/***************************************Getters And Setters******************************************/
	


	public ProductInfoComposite getProductInfoComposite() {
		return productInfoComposite;
	}

	public void setProductInfoComposite(ProductInfoComposite productInfoComposite) {
		this.productInfoComposite = productInfoComposite;
	}

	public ObjectListBox<WareHouse> getOblwarehouse() {
		return oblwarehouse;
	}

	public void setOblwarehouse(ObjectListBox<WareHouse> oblwarehouse) {
		this.oblwarehouse = oblwarehouse;
	}

	public ListBox getLststoragelocation() {
		return lststoragelocation;
	}

	public void setLststoragelocation(ListBox lststoragelocation) {
		this.lststoragelocation = lststoragelocation;
	}

	public ListBox getLststoragebin() {
		return lststoragebin;
	}

	public void setLststoragebin(ListBox lststoragebin) {
		this.lststoragebin = lststoragebin;
	}

	public Button getMinimumBtn() {
		return minimumBtn;
	}

	public void setMinimumBtn(Button minimumBtn) {
		this.minimumBtn = minimumBtn;
	}

	public Button getReorderBtn() {
		return reorderBtn;
	}

	public void setReorderBtn(Button reorderBtn) {
		this.reorderBtn = reorderBtn;
	}

	public StockAlertDetailsTable getTable() {
		return table;
	}

	public void setTable(StockAlertDetailsTable table) {
		this.table = table;
	}

	public ArrayList<String> getStorageList() {
		return storageList;
	}

	public void setStorageList(ArrayList<String> storageList) {
		this.storageList = storageList;
	}	


	
		


}
