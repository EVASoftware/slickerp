package com.slicktechnologies.client.views.inventory.warehouse;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueBoxBase;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.inventory.BranchDetails;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class WareHouseForm extends FormScreen<WareHouse>  implements ClickHandler,ChangeHandler  {

	
	EmailTextBox etbEmail,etbPointOfContactEmail;
	AddressComposite addressComposite;
	CheckBox checkBoxStatus;
	public  TextBox tbBranchName;
	WareHouse wareHouseObj;
	TextBox tbPointOfContactName;
	TextBox tbWareHouseId;   //added by Ajinkya 
	PhoneNumberBox pnbLandlinePhone,pnbCellPhoneNo1,pnbCellPhoneNo2,pnbFaxNo,pnbPointOfContactLandline,pnbPointOfContactCell;
	ObjectListBox<Branch> oblBranch;
	BranchDetailsTable table;
	Button addBtn,copyCompAddrs;
	FormField fgroupingCompanyInformation;
	
	/**
	 * This fields stores warehouse code and warehouse expiry date
	 * Date 06-10-2016 By Anil
	 * Release 30 Sept 2016 
	 * Project : Inventory Modification (NBHC)
	 **/
	  TextBox tbWarehouseCode;
	  DateBox dbWarehouseExpiryDate;
	/**
	 * END
	 */
	
	/**
	 * This fields are used to maintain the hierarchy relation between warehouses
	 * warehouse type stores value like HO,Branch,Cluster,Hub,Warehouse
	 * Date : 21-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	ObjectListBox<WareHouse> olbParentWarehouse;
	ObjectListBox<Config> olbWarehouseType;
	
	/**
	 * end	
	 */
	 
	final static GenricServiceAsync async = GWT.create(GenricService.class);


	public WareHouseForm  () {
		super();
		createGui();
		tbWareHouseId.setEnabled(false);
	}
	
	public WareHouseForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		tbWareHouseId.setEnabled(false);
		

	}

	private void initalizeWidget()
	{

		etbEmail=new EmailTextBox();

		etbPointOfContactEmail=new EmailTextBox();

		addressComposite=new AddressComposite();

		checkBoxStatus=new CheckBox();
		checkBoxStatus.setValue(true); 
		checkBoxStatus.addClickHandler(this);

		tbBranchName = new TextBox();

		tbPointOfContactName=new TextBox();

		pnbLandlinePhone=new PhoneNumberBox();

		pnbCellPhoneNo1=new PhoneNumberBox();

		pnbCellPhoneNo2=new PhoneNumberBox();

		pnbFaxNo=new PhoneNumberBox();

		pnbPointOfContactLandline=new PhoneNumberBox();

		pnbPointOfContactCell=new PhoneNumberBox();
		
		oblBranch=new ObjectListBox<Branch>();

		table=new BranchDetailsTable();
		
		addBtn=new Button("Add Branch");
		addBtn.addClickHandler(this);
		
		
		copyCompAddrs = new Button("Copy Company Address");
		copyCompAddrs.addClickHandler(this);
		
		initializeBranch();
		
		
		tbWarehouseCode=new TextBox();
		dbWarehouseExpiryDate=new DateBoxWithYearSelector();
		
		tbWareHouseId=new TextBox();  // Added by Ajinkya  
		
		
		olbParentWarehouse=new ObjectListBox<WareHouse>();
		AppUtility.initializeWarehouse(olbParentWarehouse);
		
		olbWarehouseType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbWarehouseType, Screen.WAREHOUSETYPE);
	}

	/*
	 * Method template to create the form screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.

	    initalizeWidget();
		
		this.processlevelBarNames=new String[]{"New"};
		

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		
		String mainScreenLabel="Warehouse Details";
		if(wareHouseObj!=null&&wareHouseObj.getCount()!=0){
			mainScreenLabel=wareHouseObj.getCount()+" "+"/"+" "+wareHouseObj.getstatus();
		}
		//fgroupingCompanyInformation.setLabel(mainScreenLabel);
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingCompanyInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();		
		
//		FormFieldBuilder fbuilder;
//		fbuilder = new FormFieldBuilder();
//		FormField fgroupingCompanyInformation=fbuilder.setlabel("Warehouse Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Warehouse Name",tbBranchName);
		FormField ftbBranchName= fbuilder.setMandatory(true).setMandatoryMsg("Ware House  Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Warehouse  Email",etbEmail);
		FormField fetbEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",checkBoxStatus);
		FormField fcheckBoxStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Landline Phone",pnbLandlinePhone);
		FormField fpnbLandlinePhone= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cell Phone No 1",pnbCellPhoneNo1);
		FormField fpnbCellPhoneNo1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cell Phone No 2",pnbCellPhoneNo2);
		FormField fpnbCellPhoneNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Fax No",pnbFaxNo);
		FormField fpnbFaxNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		
		FormField fgroupingPointOfContactInformation=fbuilder.setlabel("Contact Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Point Of Contact Name",tbPointOfContactName);
		FormField ftbPointOfContactName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Point Of Contact Landline",pnbPointOfContactLandline);
		FormField fpnbPointOfContactLandline= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Point Of Contact Cell",pnbPointOfContactCell);
		FormField fpnbPointOfContactCell= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Point Of Contact Email",etbPointOfContactEmail);
		FormField fetbPointOfContactEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAddressInformation=fbuilder.setlabel("Address Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",addressComposite);
		FormField faddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",table.getTable());
		FormField ftable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Branch",oblBranch);
		FormField fbranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",addBtn);
		FormField faddbtn= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("",copyCompAddrs);
		FormField fcopyCompAddrs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("Warehouse Code",tbWarehouseCode);
		FormField ftbWarehouseCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Expiry Date",dbWarehouseExpiryDate);
		FormField fdbWarehouseExpiryDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Warehouse Type",olbWarehouseType);
		FormField folbWarehouseType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Parent Warehouse",olbParentWarehouse);
		FormField folbParentWarehouse= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		// added by Ajinkya 09/02/2017
		
		 fbuilder = new FormFieldBuilder(" Warehouse Id",tbWareHouseId);
		 FormField ftbWareHouseId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
				
		 
		 fbuilder = new FormFieldBuilder();
		FormField fgroupingBranchDetails=fbuilder.setlabel("Branch Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		//changes end here
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


//		FormField[][] formfield = {   
//			  {fgroupingCompanyInformation},
//			  {ftbWareHouseId,ftbBranchName,ftbWarehouseCode,folbWarehouseType}, // added by Ajinkya 09/02/2017
//			  {folbParentWarehouse,fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2},
//			  {fetbEmail,fpnbFaxNo,fdbWarehouseExpiryDate,fcheckBoxStatus},
//			  {fgroupingPointOfContactInformation},
//			  {ftbPointOfContactName,fpnbPointOfContactLandline,fpnbPointOfContactCell,fetbPointOfContactEmail},
//			  {fgroupingAddressInformation},
//			  {fcopyCompAddrs},
//			  {faddressComposite},
//			  {fbranch,faddbtn},
//			  {ftable}
//		};
//		this.fields=formfield;	
			/**-
			 * Added by Priyanka -08-02-2021
			 * Des-Simplification of warehouse screen  Req. raise by Nitin Sir.
			 */
		 FormField[][] formfield = { 
				  /**Warehouse Details**/
				  {fgroupingCompanyInformation},
				  {ftbWareHouseId,ftbBranchName,ftbWarehouseCode,folbWarehouseType},
				  {folbParentWarehouse,fdbWarehouseExpiryDate,fcheckBoxStatus},
				  /**Address Info**/
				  {fgroupingAddressInformation},
				  {fcopyCompAddrs},
				  {faddressComposite},
				  /**Branch Details**/
				  {fgroupingBranchDetails},
				  {fbranch,faddbtn},
				  {ftable},
				  /**Contact Information**/
				  {fgroupingPointOfContactInformation},
				  {fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2,fetbEmail},
				  {fpnbFaxNo},
				  {ftbPointOfContactName,fpnbPointOfContactLandline,fpnbPointOfContactCell,fetbPointOfContactEmail}
				  
			 };
			this.fields=formfield;	

	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(WareHouse model) 
	{

		if(tbBranchName.getValue()!=null)
			model.setBusinessUnitName(tbBranchName.getValue().trim());
		if(etbEmail.getValue()!=null)
			model.setEmail(etbEmail.getValue());
		if(checkBoxStatus.getValue()!=null)
			model.setstatus(checkBoxStatus.getValue());
		if(pnbLandlinePhone.getValue()!=null)
			model.setLandline(pnbLandlinePhone.getValue());
		if(pnbCellPhoneNo1.getValue()!=null)
			model.setCellNumber1(pnbCellPhoneNo1.getValue());
		if(pnbCellPhoneNo2.getValue()!=null)
			model.setCellNumber2(pnbCellPhoneNo2.getValue());
		if(pnbFaxNo.getValue()!=null)
			model.setFaxNumber(pnbFaxNo.getValue());
		if(tbPointOfContactName.getValue()!=null)
			model.setPocName(tbPointOfContactName.getValue());
		if(pnbPointOfContactLandline.getValue()!=null)
			model.setPocLandline(pnbPointOfContactLandline.getValue());
		if(pnbPointOfContactCell.getValue()!=null)
			model.setPocCell(pnbPointOfContactCell.getValue());
		if(etbPointOfContactEmail.getValue()!=null)
			model.setPocEmail(etbPointOfContactEmail.getValue());
		if(addressComposite.getValue()!=null)
			model.setAddress(addressComposite.getValue());

		if(table.getValue()!=null)
			model.setBranchdetails(table.getValue());
		
		if(tbWarehouseCode.getValue()!=null){
			model.setWarehouseCode(tbWarehouseCode.getValue());
		}
		if(dbWarehouseExpiryDate.getValue()!=null){
			model.setWarehouseExpiryDate(dbWarehouseExpiryDate.getValue());
		}
		
		if(olbWarehouseType.getValue()!=null){
			model.setWarehouseType(olbWarehouseType.getValue());
		}
		if(olbParentWarehouse.getValue()!=null){
			model.setParentWarehouse(olbParentWarehouse.getValue());
		}
		wareHouseObj=model;
		presenter.setModel(model);

	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(WareHouse view)    
	{
		wareHouseObj=view;
		tbWareHouseId.setValue(view.getCount()+" ");  //added by Ajinkya
		
		/**
		 * Date 20-04-2018 By vijay for orion pest locality load  
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
			setAddressDropDown();
		}
		/**
		 * ends here
		 */
		
		if(view.getBusinessUnitName()!=null)
			tbBranchName.setValue(view.getBusinessUnitName());
		if(view.getEmail()!=null)
			etbEmail.setValue(view.getEmail());
		if(view.getstatus()!=null)
			checkBoxStatus.setValue(view.getstatus());
		if(view.getLandline()!=null)
			pnbLandlinePhone.setValue(view.getLandline());
		if(view.getCellNumber1()!=null)
			pnbCellPhoneNo1.setValue(view.getCellNumber1());
		if(view.getCellNumber2()!=null)
			pnbCellPhoneNo2.setValue(view.getCellNumber2());
		if(view.getFaxNumber()!=null)
			pnbFaxNo.setValue(view.getFaxNumber());
		if(view.getPocName()!=null)
			tbPointOfContactName.setValue(view.getPocName());
		if(view.getPocLandline()!=null)
			pnbPointOfContactLandline.setValue(view.getPocLandline());
		if(view.getPocCell()!=null)
			pnbPointOfContactCell.setValue(view.getPocCell());
		if(view.getPocEmail()!=null)
			etbPointOfContactEmail.setValue(view.getPocEmail());
		if(view.getAddress()!=null)
			addressComposite.setValue(view.getAddress());
		/*if(view.getCalendarName()!=null)
			calendar.setValue(view.getCalendarName());*/
		if(view.getBranchdetails()!=null)
			table.setValue(view.getBranchdetails());
		
		if(view.getWarehouseCode()!=null){
			tbWarehouseCode.setValue(view.getWarehouseCode());
		}
		
		if(view.getWarehouseExpiryDate()!=null){
			dbWarehouseExpiryDate.setValue(view.getWarehouseExpiryDate());
		}
		
		if(view.getWarehouseType()!=null){
			olbWarehouseType.setValue(view.getWarehouseType());
		}
		if(view.getParentWarehouse()!=null){
			olbParentWarehouse.setValue(view.getParentWarehouse());
		}
		
		presenter.setModel(view);

	}

	/**
	 * Date 20-04-2018 By vijay
	 * here i am refreshing address composite data if global list size greater than drop down list size
	 * because we are adding locality and city from an entity in view state to global list if doest not exist in global list 
	 */
	private void setAddressDropDown() {
		if(LoginPresenter.globalLocality.size()>addressComposite.locality.getItems().size()	){
			addressComposite.locality.setListItems(LoginPresenter.globalLocality);
		}
	}
	/**
	 * ends here 
	 */
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		
		 
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else  
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.WAREHOUSE,LoginPresenter.currentModule.trim());
	
	}
                             ///////////////// end Here //////////////////

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		
		tbWareHouseId.setValue(count+"");    // Addded by Ajinkya    09/02/2017
	}
	
	
	/**-
	 * Added by Priyanka -08-02-2021
	 * Des-Simplification of warehouse screen Warehouse Id and status shown on the top of the screen Req. raise by Nitin Sir.
	 */
	@Override
	public void setToViewState() {
		// TODO Auto-generated method stub
		super.setToViewState();
	
		String mainScreenLabel="Warehouse Details";
		if(wareHouseObj!=null&&wareHouseObj.getCount()!=0){
			mainScreenLabel=wareHouseObj.getCount()+" "+"/"+" "+wareHouseObj.getstatus();
		}
		fgroupingCompanyInformation.setLabel(mainScreenLabel);
		
		
	}
	/**
	 * END
	 */

	@Override
	public void setToEditState()
	{
		 Console.log("inside edit state warehouse");
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		 if(wareHouseObj.isFreez()){
			 Console.log("warehouse  name in edit state disable");
			this.tbBranchName.setEnabled(false);	
		}
			/**-
			 * Added by Priyanka -08-02-2021
			 * Des-Simplification of warehouse screen Warehouse Id and status shown on the top of the screen Req. raise by Nitin Sir.
			 */
			String mainScreenLabel="Warehouse Details";
			if(wareHouseObj!=null&&wareHouseObj.getCount()!=0){
				mainScreenLabel=wareHouseObj.getCount()+" "+"/"+" "+wareHouseObj.getstatus();
			}
			fgroupingCompanyInformation.setLabel(mainScreenLabel);
			/**
			 * END
			 */
			
			tbBranchName.setEnabled(false);

	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource().equals(addBtn)&&oblBranch.getSelectedIndex()!=0)
		{
			Branch branch=oblBranch.getSelectedItem();
			ArrayList<BranchDetails> brancharr=new ArrayList<BranchDetails>();
			BranchDetails bdet=new BranchDetails();
			bdet.setBranchName(branch.getBusinessUnitName());
			bdet.setBranchID(branch.getCount());
			bdet.setBranchAdds(branch.getAddress().getCity());
			branch.getBusinessUnitName();
			
			brancharr.add(bdet);
			table.getDataprovider().getList().add(bdet);
		}
		
		if(event.getSource().equals(copyCompAddrs))
		{
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Company());
			
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override    
				public void onSuccess(ArrayList<SuperModel> result) {
					System.out.println(" result set size +++++++"+result.size());
					
					for(SuperModel model:result)
					{
						Company comp=(Company)model;
						addressComposite.clear();
						addressComposite.setValue(comp.getAddress());
				}
				}
			});
		}
		
		/**
		 * Date 04-05-2017 added by vijay for Deadstock NBHC
		 * for dont allow warehouse inactive if warehouse having some stock
		 */
		if(event.getSource().equals(checkBoxStatus)){
			System.out.println("checkbox on click");
			System.out.println("value =="+this.checkBoxStatus.getValue());
			if(this.checkBoxStatus.getValue()==false){
				this.showWaitSymbol();
				MyQuerry querry = new MyQuerry();
			  	Company c = new Company();
			  	
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
			  	
			  	filter = new Filter();
			  	filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				
				filter = new Filter();
			  	filter.setQuerryString("warehousename");
				filter.setStringValue(this.tbBranchName.getValue());
				filtervec.add(filter);
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new ProductInventoryViewDetails());
				
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						double sum=0;
						for(SuperModel model:result){
							
							ProductInventoryViewDetails entity = (ProductInventoryViewDetails) model;
							sum +=entity.getAvailableqty();
						}
						if(sum>0){
							showDialogMessage("you can not make warehouse Inactive because this warehouse having stock. Total available quantity:"+sum);
							checkBoxStatus.setValue(true);
						}
						hideWaitSymbol();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
					}
				});

			}
		}
		/**
		 * ends here
		 */
	}

	@Override
	public void clear() {
		super.clear();
		checkBoxStatus.setValue(true);
		table.connectToLocal();
	}
	
	public void initializeBranch()
	{
		MyQuerry querry = new MyQuerry(new Vector<Filter>(),new Branch());

		oblBranch.MakeLive(querry);
	}
	
	public boolean validate()
	{
		boolean b=super.validate();
		if(b==false)
			return false;
		
		//  Anil has commented this for NBHC Date - 07/09/2016
//		if(table.getDataprovider().getList().size()==0)
//		{
//			this.showDialogMessage("Please add atleast one Branch");
//			return false;
//		}
		return true;
	}
	
	public TextBox gettbBranchName(){
		return tbBranchName ; 
	}
	
	public void setTbBranchName(TextBox tbBranchName) 
	{
		this.tbBranchName = tbBranchName;
	}
	
	@Override
	public void setEnable(boolean state)
	{
		super.setEnable(state);
		table.setEnable(state);
	}

	
	
	
	public AddressComposite getAddressComposite() {
		return addressComposite;
	}

	public void setAddressComposite(AddressComposite addressComposite) {
		this.addressComposite = addressComposite;
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

	



}
