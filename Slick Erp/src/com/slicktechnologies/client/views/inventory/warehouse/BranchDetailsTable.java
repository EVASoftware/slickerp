package com.slicktechnologies.client.views.inventory.warehouse;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.inventory.BranchDetails;

public class BranchDetailsTable extends SuperTable<BranchDetails>{

	TextColumn<BranchDetails> columnname;
	TextColumn<BranchDetails> columnID;
	TextColumn<BranchDetails> columnAdd;
	Column<BranchDetails,String> deleteColumn;
	
	@Override
	public void createTable() {
		createColumnID();
		createColumnName();
		createColumnAdd();
		createColumndeleteColumn();
		addFieldUpdater();
		// TODO Auto-generated method stub
		
	}
	
	public void addeditColumn()
	{
		createColumnID();
		createColumnName();
		createColumnAdd();
		createColumndeleteColumn();
		addFieldUpdater();

	}
	
	public void addViewColumn()
	{
		createColumnID();
		createColumnName();
		createColumnAdd();
	//	createColumndeleteColumn();

	}
	
	public void createColumnID()
	{
		columnID=new TextColumn<BranchDetails>() {
			
			@Override
			public String getValue(BranchDetails object) {
				// TODO Auto-generated method stub
				return object.getBranchID()+"";
			}
		};table.addColumn(columnID,"Branch ID");
	}
	
	public void createColumnName()
	{
		columnname=new TextColumn<BranchDetails>() {
			
			@Override
			public String getValue(BranchDetails object) {
				// TODO Auto-generated method stub
				return object.getBranchName();
			}
		};table.addColumn(columnname,"Branch Name");
	}
	
	public void createColumnAdd()
	{
		columnAdd=new TextColumn<BranchDetails>() {
			
			@Override
			public String getValue(BranchDetails object) {
				// TODO Auto-generated method stub
				return object.getBranchAdds();
			}
		};table.addColumn(columnAdd,"City");
	}

	public void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<BranchDetails,String>(btnCell)
				{
			@Override
			public String getValue(BranchDetails object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");

	}

	public void createFieldUpdaterDelete()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<BranchDetails,String>()
				{
			@Override
			public void update(int index,BranchDetails object,String value)
			{
				getDataprovider().getList().remove(object);

				table.redrawRow(index);
			}
				});

	}
	

	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterDelete();
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
		
		
	}
	

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
