package com.slicktechnologies.client.views.inventory.warehouse;

import java.util.Vector;

import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.warehouse.WareHousePresenter.WareHousePresenterSearch;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.inventory.WareHouse;

public class WareHousePresenterSearchProxy extends WareHousePresenterSearch {
    

	
	//public DateComparator dateComparator; 
	      public ObjectListBox<WareHouse> olbWareHouseName;
	      public ListBox lbWareHouseStatus ;                     // added by Ajinkya
	      public ObjectListBox<Branch> oblBranch;
	      ObjectListBox<WareHouse> olbParentWarehouse;
	      ObjectListBox<Config> olbWarehouseType;
	      ObjectListBox<WareHouse> olbWarehouseCode;

	  	public Object getVarRef(String varName)
	  	{
	  		
	  	//	if(varName.equals("dateComparator"))
	  		//	  return this.dateComparator;
	  		if(varName.equals("oblBranch"))
	  			return true ;
	  	
	     
	  		if(varName.equals("olbWareHouseName"))
	  			return true ;
	  		
	  		
	  		if(varName.equals("olbParentWarehouse"))
	  			return true ;
	  		
	  		
	  		if(varName.equals("olbWarehouseType"))
	  			return true ;
	  		
	  		if(varName.equals("olbWarehouseCode"))
	  			return true ;
	  		
	  		if(varName.equals("lbWareHouseStatus"))// chnge for test ajinkya
	  			return true ;
	  		
	  		
	  		return null ;
	  	
	  	}    
	      
	
	public WareHousePresenterSearchProxy()
	{
		super();
		createGui();        
	}
	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new WareHouse());
		
		  oblBranch=new ObjectListBox<Branch>();
		  AppUtility.makeBranchListBoxLive(oblBranch);
		  
		  olbWareHouseName = new ObjectListBox <WareHouse>();
		  AppUtility.initializeWarehouse(olbWareHouseName);
		  
		  olbParentWarehouse = new ObjectListBox <WareHouse>();
		  AppUtility.initializeWarehouse(olbParentWarehouse);
		  
		  olbWarehouseType=new ObjectListBox<Config>();
		  AppUtility.MakeLiveConfig(olbWarehouseType, Screen.WAREHOUSETYPE);
		  
		  lbWareHouseStatus =new ListBox();
		  lbWareHouseStatus.addItem("--Select--");
		  lbWareHouseStatus.addItem(AppConstants.ACTIVE);
		  lbWareHouseStatus.addItem(AppConstants.INACTIVE);
		  
		  //For giving type category functionality
		     
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder("Warehouse Name",olbWareHouseName);    
		FormField ftbWareHouseName= builder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		
	    builder = new FormFieldBuilder("Status",lbWareHouseStatus);
		FormField flbstatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Branch",oblBranch);
		FormField fbranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Parent Warehouse",olbParentWarehouse);    
		FormField folbParentWarehouse= builder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Warehouse Type",olbWarehouseType);    
		FormField folbWarehouseType= builder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		  
		this.fields=new FormField[][]
				{
				 {folbWarehouseType,ftbWareHouseName,folbParentWarehouse},
				 {fbranch,flbstatus},
				
		};
	} 
	
	
 public MyQuerry getQuerry()
 {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		if (olbWareHouseName.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbWareHouseName.getValue().trim());
			temp.setQuerryString("buisnessUnitName");
			filtervec.add(temp);

		}

//		if (oblBranch.getSelectedIndex() != 0) {
//			temp = new Filter();
//
//			temp.setStringValue(oblBranch.getValue().trim());
//			temp.setQuerryString("branchdetails.branchName");
//			filtervec.add(temp);
//
//		}
		
		/**
		 * @author Vijay Date :- 12-01-2022
		 */
		if(LoginPresenter.branchRestrictionFlag==true && oblBranch.getSelectedIndex()==0)
		{
			temp=new Filter();
			temp.setList(AppUtility.getbranchlist());
			temp.setQuerryString("branchdetails.branchName IN");
			filtervec.add(temp);
		}
		else{
			if (oblBranch.getSelectedIndex() != 0) {
				temp = new Filter();
				temp.setStringValue(oblBranch.getValue().trim());
				temp.setQuerryString("branchdetails.branchName");
				filtervec.add(temp);

			}
		}
		/**
		 * ends here
		 */
		

		if (olbParentWarehouse.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbParentWarehouse.getValue().trim());
			temp.setQuerryString("parentWarehouse");
			filtervec.add(temp);

		}

		if (olbWarehouseType.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(olbWarehouseType.getValue().trim());
			temp.setQuerryString("warehouseType");
			filtervec.add(temp);

		}


		if (this.lbWareHouseStatus.getSelectedIndex() != 0) {
			temp = new Filter();
			int index = lbWareHouseStatus.getSelectedIndex();
			String selectedItem = lbWareHouseStatus.getItemText(index);
			temp.setQuerryString("status");
			if (selectedItem.equals(AppConstants.ACTIVE)) {
				temp.setBooleanvalue(true);
			}
			if (selectedItem.equals(AppConstants.INACTIVE)) {
				temp.setBooleanvalue(false);
			}
			filtervec.add(temp);
		}

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new WareHouse());
		return querry;
	}
	
	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

	

}
