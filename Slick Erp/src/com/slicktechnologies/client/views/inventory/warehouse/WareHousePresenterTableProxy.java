package com.slicktechnologies.client.views.inventory.warehouse;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.views.inventory.warehouse.WareHousePresenter.WareHousePresenterTable;
import com.slicktechnologies.shared.common.inventory.BranchDetails;
import com.slicktechnologies.shared.common.inventory.WareHouse;

public class WareHousePresenterTableProxy extends WareHousePresenterTable {

	TextColumn<WareHouse> getBranchColumn;
	TextColumn<WareHouse> getStatusColumn;
	TextColumn<WareHouse> getWareHouseNameColumn;
	TextColumn<WareHouse> getParentWareHouseNameColumn;
	TextColumn<WareHouse> getCountColumn;
	TextColumn<WareHouse> getWareHouseTypeColumn;

	public Object getVarRef(String varName) 
	{
		if (varName.equals("getBranchColumn"))
			return this.getBranchColumn;
		if (varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if (varName.equals("getWareHouseNameColumn"))
			return this.getWareHouseNameColumn;
		if (varName.equals("getParentWareHouseNameColumn"))
			return this.getParentWareHouseNameColumn;
		if (varName.equals("getWareHouseTypeColumn"))
			return this.getWareHouseTypeColumn;
		if (varName.equals("getCountColumn"))
			return this.getCountColumn;
		return null;
	}

	public WareHousePresenterTableProxy() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetWareHouseType();
		addColumngetWareHouseName();
		addColumngetParentWareHouseName();
		addColumngetBranch();
		addColumngetStatus();

	}
  
	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<WareHouse>() {
			@Override
			public Object getKey(WareHouse item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {

	}

	@Override
	public void applyStyle() {

	}

	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetWareHouseType();
		addSortinggetWareHouseName();
		addSortinggetParentWareHouseName();
		 addSortinggetBranch();
		addSortinggetStatus();
		
	}

	@Override
	public void addFieldUpdater() {

	}

	protected void addSortinggetCount() {
		List<WareHouse> list = getDataprovider().getList();
		columnSort = new ListHandler<WareHouse>(list);
		columnSort.setComparator(getCountColumn, new Comparator<WareHouse>() {
			@Override
			public int compare(WareHouse e1, WareHouse e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<WareHouse>() {
			@Override
			public String getValue(WareHouse object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		getCountColumn.setSortable(true);
	}
  
	
	protected void addSortinggetStatus() {
		List<WareHouse> list = getDataprovider().getList();
		columnSort = new ListHandler<WareHouse>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<WareHouse>() {
			@Override
			public int compare(WareHouse e1, WareHouse e2) {
				if (e1 != null && e2 != null) {
					 if( e1.getstatus()!=null && e2.getstatus()!=null){
					 return e1.getstatus().compareTo(e2.getstatus());}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<WareHouse>() {
			@Override
			public String getValue(WareHouse object) {
				if(object.getstatus()!=null){
					if (object.getstatus() == true)
						return "Active";
					else
						return "In Active";
				}
				return "";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
	}

	protected void addSortinggetWareHouseName() {
		List<WareHouse> list = getDataprovider().getList();
		columnSort = new ListHandler<WareHouse>(list);
		columnSort.setComparator(getWareHouseNameColumn,
				new Comparator<WareHouse>() {
					@Override
					public int compare(WareHouse e1, WareHouse e2) {
						if (e1 != null && e2 != null) {
							if (e1.getBusinessUnitName() != null
									&& e2.getBusinessUnitName() != null) {
								return e1.getBusinessUnitName().compareTo(
										e2.getBusinessUnitName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetWareHouseName() {
		getWareHouseNameColumn = new TextColumn<WareHouse>() {
			@Override
			public String getValue(WareHouse object) {
				if(object.getBusinessUnitName()!=null){
				return object.getBusinessUnitName() + "";
				}
				return "";
			}

		};

		table.addColumn(getWareHouseNameColumn, "Warehouse");
		getWareHouseNameColumn.setSortable(true);

	}

	// /////////////////////////////////////// added by Ajinkya 21/02/2017 //////////////////////////////////////////////
	protected void addSortinggetParentWareHouseName() {
		List<WareHouse> list = getDataprovider().getList();
		columnSort = new ListHandler<WareHouse>(list);
		columnSort.setComparator(getParentWareHouseNameColumn,
				new Comparator<WareHouse>() {
					@Override
					public int compare(WareHouse e1, WareHouse e2) {
						if (e1 != null && e2 != null) {
							if (e1.getParentWarehouse() != null
									&& e2.getParentWarehouse() != null) {

								return e1.getParentWarehouse().compareTo(
										e2.getParentWarehouse());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetParentWareHouseName() {
		getParentWareHouseNameColumn = new TextColumn<WareHouse>() {
			@Override
			public String getValue(WareHouse object) {
				if(object.getParentWarehouse()!=null){
				return object.getParentWarehouse() + "";
				}
				return "";
			}
		};

		table.addColumn(getParentWareHouseNameColumn, "Parent Warehouse");
		getParentWareHouseNameColumn.setSortable(true);

	}

	private void addSortinggetWareHouseType() {

		List<WareHouse> list = getDataprovider().getList();
		columnSort = new ListHandler<WareHouse>(list);
		columnSort.setComparator(getWareHouseTypeColumn,
				new Comparator<WareHouse>() {
					@Override
					public int compare(WareHouse e1, WareHouse e2) {
						if (e1 != null && e2 != null) {
							if (e1.getWarehouseType() != null
									&& e2.getWarehouseType() != null) {
								return e1.getWarehouseType().compareTo(
										e2.getWarehouseType());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});

		table.addColumnSortHandler(columnSort);

	}

	private void addColumngetWareHouseType() 
	{
		getWareHouseTypeColumn = new TextColumn<WareHouse>() {
			@Override
			public String getValue(WareHouse object) {
				
				if(object.getWarehouseType()!=null){
					return object.getWarehouseType() + "";
				}
				return "";
				
			}
		};

		table.addColumn(getWareHouseTypeColumn, "Warehouse Type");
		getWareHouseTypeColumn.setSortable(true);
		
	}
     
	protected void addSortinggetBranch() 
	 {
		List<WareHouse> list = getDataprovider().getList();
		columnSort = new ListHandler<WareHouse>(list);
		columnSort.setComparator(getWareHouseTypeColumn,new Comparator<WareHouse>()
				{
			
			@Override
			public int compare(WareHouse e1, WareHouse e2)
			{
				String branchName1 = "" ;
				String branchName2 = "" ;
				
				for(BranchDetails obj:e1.getBranchdetails())
				{
					branchName1 = branchName1+obj.getBranchName()+" " ;
				}
				for(BranchDetails obj:e2.getBranchdetails())
				{
					branchName2 = branchName2+obj.getBranchName()+" " ;
				}
				
				return branchName1.compareTo(branchName2) ;
				
			    }
		  });
		table.addColumnSortHandler(columnSort);
		}
	
	protected void addColumngetBranch() {
		getBranchColumn = new TextColumn<WareHouse>() {
			@Override
			public String getValue(WareHouse object) {
				String branchName = "";

				if (object.getBranchdetails() != null) {
					for (BranchDetails obj : object.getBranchdetails()) {
						branchName = branchName + obj.getBranchName() + " ";
					}
				}
				return branchName;
			}
		};
		table.addColumn(getBranchColumn, "Branch");
		getBranchColumn.setSortable(true);
	}

	
	// /////////////////////////////////////// End Here // /////////////////////////////////////////////
}
