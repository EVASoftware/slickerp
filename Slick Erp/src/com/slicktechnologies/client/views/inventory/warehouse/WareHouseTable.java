package com.slicktechnologies.client.views.inventory.warehouse;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.client.utility.Screen;

public class WareHouseTable extends SuperTable<WareHouse> {
	TextColumn<WareHouse> getstatusColumn;
	TextColumn<WareHouse> getBusinessUnitNameColumn;
	TextColumn<WareHouse> getCellNumber1Column;
	TextColumn<WareHouse> getPocNameColumn;
	TextColumn<WareHouse> getPocCellColumn;
	TextColumn<WareHouse> getCountColumn;

	public WareHouseTable()
	{
		super();
	}
	@Override public void createTable() {
		addColumngetCount();
		addColumngetBusinessUnitName();
		addColumngetCellNumber1();
		addColumngetPocName();
		addColumngetPocCell();
		addColumngetstatus();
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<WareHouse>()
				{
			@Override
			public Object getKey(WareHouse item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetBusinessUnitName();
		addSortinggetCellNumber1();
		addSortinggetPocName();
		addSortinggetPocCell();
		addSortinggetstatus();
	}
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetCount()
	{
		List<WareHouse> list=getDataprovider().getList();
		columnSort=new ListHandler<WareHouse>(list);
		columnSort.setComparator(getCountColumn, new Comparator<WareHouse>()
				{
			@Override
			public int compare(WareHouse e1,WareHouse e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<WareHouse>()
				{
			@Override
			public String getValue(WareHouse object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				getCountColumn.setSortable(true);
				table.setColumnWidth(getCountColumn, 100, Unit.PX);
	}
	
	protected void addSortinggetBusinessUnitName()
	{
		List<WareHouse> list=getDataprovider().getList();
		columnSort=new ListHandler<WareHouse>(list);
		columnSort.setComparator(getBusinessUnitNameColumn, new Comparator<WareHouse>()
				{
			@Override
			public int compare(WareHouse e1,WareHouse e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBusinessUnitName()!=null && e2.getBusinessUnitName()!=null){
						return e1.getBusinessUnitName().compareTo(e2.getBusinessUnitName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetBusinessUnitName()
	{
		getBusinessUnitNameColumn=new TextColumn<WareHouse>()
				{
			@Override
			public String getValue(WareHouse object)
			{
				return object.getBusinessUnitName()+"";
			}
				};
				table.addColumn(getBusinessUnitNameColumn,"Name");
				getBusinessUnitNameColumn.setSortable(true);
				table.setColumnWidth(getBusinessUnitNameColumn, 100, Unit.PX);
	}
	
	protected void addSortinggetCellNumber1()
	{
		List<WareHouse> list=getDataprovider().getList();
		columnSort=new ListHandler<WareHouse>(list);
		columnSort.setComparator(getCellNumber1Column, new Comparator<WareHouse>()
				{
			@Override
			public int compare(WareHouse e1,WareHouse e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCellNumber1()== e2.getCellNumber1()){
						return 0;}
					if(e1.getCellNumber1()> e2.getCellNumber1()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetCellNumber1()
	{
		getCellNumber1Column=new TextColumn<WareHouse>()
				{
			@Override
			public String getValue(WareHouse object)
			{
				return object.getCellNumber1()+"";
			}
				};
				table.addColumn(getCellNumber1Column,"Phone");
				getCellNumber1Column.setSortable(true);
				table.setColumnWidth(getCellNumber1Column, 110, Unit.PX);
	}
	
	protected void addSortinggetPocName()
	{
		List<WareHouse> list=getDataprovider().getList();
		columnSort=new ListHandler<WareHouse>(list);
		columnSort.setComparator(getPocNameColumn, new Comparator<WareHouse>()
				{
			@Override
			public int compare(WareHouse e1,WareHouse e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPocName()!=null && e2.getPocName()!=null){
						return e1.getPocName().compareTo(e2.getPocName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetPocName()
	{
		getPocNameColumn=new TextColumn<WareHouse>()
				{
			@Override
			public String getValue(WareHouse object)
			{
				return object.getPocName()+"";
			}
				};
				table.addColumn(getPocNameColumn,"Contact Name");
				getPocNameColumn.setSortable(true);
				table.setColumnWidth(getPocNameColumn, 110, Unit.PX);
	}
	
	protected void addSortinggetPocCell()
	{
		List<WareHouse> list=getDataprovider().getList();
		columnSort=new ListHandler<WareHouse>(list);
		columnSort.setComparator(getPocCellColumn, new Comparator<WareHouse>()
				{
			@Override
			public int compare(WareHouse e1,WareHouse e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPocCell()== e2.getPocCell()){
						return 0;}
					if(e1.getPocCell()> e2.getPocCell()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetPocCell()
	{
		getPocCellColumn=new TextColumn<WareHouse>()
				{
			@Override
			public String getValue(WareHouse object)
			{
				return object.getPocCell()+"";
			}
				};
				table.addColumn(getPocCellColumn,"Contact Phone");
				getPocCellColumn.setSortable(true);
				table.setColumnWidth(getPocCellColumn, 110, Unit.PX);
	}
	protected void addSortinggetstatus()
	{
		List<WareHouse> list=getDataprovider().getList();
		columnSort=new ListHandler<WareHouse>(list);
		columnSort.setComparator(getstatusColumn, new Comparator<WareHouse>()
				{
			@Override
			public int compare(WareHouse e1,WareHouse e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getstatus()== e2.getstatus()){
						return 0;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetstatus()
	{
		getstatusColumn=new TextColumn<WareHouse>()
				{
			@Override
			public String getValue(WareHouse object)
			{
				if( object.getstatus()==true)
					return "Active";
				else 
					return "In Active";
			}
				};
				table.addColumn(getstatusColumn,"Status");
				getstatusColumn.setSortable(true);
				table.setColumnWidth(getstatusColumn, 100, Unit.PX);
	}
	
}
