package com.slicktechnologies.client.views.inventory.warehouse;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class WareHousePresenter extends FormScreenPresenter<WareHouse> implements ValueChangeHandler<String> {

	// Token to set the concrete form
	WareHouseForm form;
	final static GenricServiceAsync async = GWT.create(GenricService.class);

	public WareHousePresenter(FormScreen<WareHouse> view, WareHouse model) {
		super(view, model);
		form = (WareHouseForm) view;
		form.setPresenter(this);
		form.gettbBranchName().addValueChangeHandler(this); // add for validate

		boolean isDownload = AuthorizationHelper.getDownloadAuthorization(
				Screen.WAREHOUSE, LoginPresenter.currentModule.trim());
		if (isDownload == false) {
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals("New")) {
			form.setToNewState();
			initalize();
		}

	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub

	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {

		model = new WareHouse();
	}

	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getLocationQuery() {
		MyQuerry quer = new MyQuerry(new Vector<Filter>(), new WareHouse());
		return quer;
	}

	public void setModel(WareHouse entity) {
		model = entity;
	}

	public static void initalize() {

		// //////////////// // addded by ajinkya 10/02/2017
		// ///////////////////////////////////
		WareHouseForm form = new WareHouseForm();

		WareHousePresenterTable gentable = new WareHousePresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();

		WareHousePresenterSearch.staticSuperTable = gentable;
		WareHousePresenterSearch searchpopup = new WareHousePresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		WareHousePresenter presenter = new WareHousePresenter(form,
				new WareHouse());
		AppMemory.getAppMemory().stickPnel(form);

	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.inventory.WareHouse")
	public static class WareHousePresenterTable extends SuperTable<WareHouse>
			implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub

		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub

		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub

		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub

		}
	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.inventory.WareHouse")
	public static class WareHousePresenterSearch extends SearchPopUpScreen<WareHouse> {

		@Override
		public MyQuerry getQuerry()
		{
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() 
		{
			// TODO Auto-generated method stub
			return true;
		}
	};

	@Override
	public void onClick(ClickEvent event)
	{
		super.onClick(event);
		InlineLabel label = (InlineLabel) event.getSource();
//		if (label.getText().equals("Selenium Test"))
//			reactOnSilleniumTest();

	};

//	@Override
//	public void reactOnSilleniumTest() 
//	{
//
//	}

	@Override
	public void reactOnDownload() {
		// super.reactOnDownload();
		CsvServiceAsync async = GWT.create(CsvService.class);
		ArrayList<WareHouse> whArray = new ArrayList<WareHouse>();
		List<WareHouse> list = form.getSearchpopupscreen().getSupertable()
				.getDataprovider().getList(); // 1 no
		whArray.addAll(list);
		async.setWarehouseList(whArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 114;
				Window.open(url, "test", "enabled");
			}
		});
	}

	private void validateWareHouse() {

		if (!form.gettbBranchName().getValue().equals("")) {
			String wareHouseNameVal = form.gettbBranchName().getValue();

			if (wareHouseNameVal != null) {

				final MyQuerry querry = new MyQuerry();
				Vector<Filter> filtervec = new Vector<Filter>();
				Filter temp = null;

				temp = new Filter();
				temp.setQuerryString("companyId");
				temp.setLongValue(model.getCompanyId());
				filtervec.add(temp);

				temp = new Filter();
				temp.setQuerryString("buisnessUnitName");
				temp.setStringValue(wareHouseNameVal);
				filtervec.add(temp);

				querry.setFilters(filtervec);
				querry.setQuerryObject(new WareHouse());
				async.getSearchResult(querry,
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");

							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								System.out.println("Success" + result.size());
								if (result.size() != 0) {
									form.showDialogMessage("WareHouse Name Already Exists");
									form.gettbBranchName().setValue("");

								}
								if (result.size() == 0) {

								}
							}
						});
			}
		}
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {

		ScreeenState scr = AppMemory.getAppMemory().currentState;

		if (scr.equals(ScreeenState.NEW)) {
			if (event.getSource().equals(form.gettbBranchName())) {
				validateWareHouse();
			}
		}

	}

}
