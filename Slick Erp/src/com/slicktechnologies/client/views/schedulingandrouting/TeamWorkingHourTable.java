package com.slicktechnologies.client.views.schedulingandrouting;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.scheduling.TeamWorkingHours;

public class TeamWorkingHourTable extends SuperTable<TeamWorkingHours> {

	TextColumn<TeamWorkingHours> getTeamNameColumn;
	TextColumn<TeamWorkingHours> getFromTimeColumn;
	TextColumn<TeamWorkingHours> getToTimeColumn;
	TextColumn<TeamWorkingHours> getPerDayWorkingHrsColumn;
	TextColumn<TeamWorkingHours> getWorkingHrsColumn;
	TextColumn<TeamWorkingHours> getTotalWorkingHrsColumn;
	
	Column<TeamWorkingHours,String> getReservedTimeColumn;
	Column<TeamWorkingHours,String> getOverTimeColumn;
	Column<TeamWorkingHours, String> delete;
	
	public TeamWorkingHourTable() {
		super();
	}
	
	@Override
	public void createTable() {
		getTeamNameColumn();
		getFromTimeColumn();
		getToTimeColumn();
//		getPerDayWorkingHrsColumn();
		getWorkingHrsColumn();
		getReservedTimeColumn();
		getOverTimeColumn();
		getTotalWorkingHrsColumn();
		addColumnDelete();
		addFieldUpdater();
		
	}

	

	

	private void getTeamNameColumn() {
		getTeamNameColumn = new TextColumn<TeamWorkingHours>() {
			@Override
			public String getValue(TeamWorkingHours object) {
				return object.getTeamName();
			}
		};
		table.addColumn(getTeamNameColumn,"Team Name");
	}

	private void getFromTimeColumn() {
		getFromTimeColumn = new TextColumn<TeamWorkingHours>() {
			@Override
			public String getValue(TeamWorkingHours object) {
				return object.getFromTime()+"";
			}
		};
		table.addColumn(getFromTimeColumn,"From Time");
	}

	private void getToTimeColumn() {
		getToTimeColumn = new TextColumn<TeamWorkingHours>() {
			@Override
			public String getValue(TeamWorkingHours object) {
				return object.getToTime()+"";
			}
		};
		table.addColumn(getToTimeColumn,"To Time");
	}

	private void getPerDayWorkingHrsColumn() {
		getPerDayWorkingHrsColumn = new TextColumn<TeamWorkingHours>() {
			@Override
			public String getValue(TeamWorkingHours object) {
				return object.getPerDayWorkingHrs()+"";
			}
		};
		table.addColumn(getPerDayWorkingHrsColumn,"Working Hrs/Day");
		
	}

	private void getWorkingHrsColumn() {
		getWorkingHrsColumn = new TextColumn<TeamWorkingHours>() {
			@Override
			public String getValue(TeamWorkingHours object) {
				return object.getTeamWorkingHrs()+"";
			}
		};
		table.addColumn(getWorkingHrsColumn,"Team Working Hrs");
	}
	
	
	
	private void getOverTimeColumn() {
		EditTextCell editCell = new EditTextCell();
		getOverTimeColumn = new Column<TeamWorkingHours, String>(editCell) {
			@Override
			public String getValue(TeamWorkingHours object) {
//				System.out.println("OVERTIME :: "+object.getOverTime());
				return object.getOverTime()+"";
			}
		};
		table.addColumn(getOverTimeColumn,"#Overtime(%)");
	}
	
	

	private void getReservedTimeColumn() {
		EditTextCell editCell = new EditTextCell();
		getReservedTimeColumn = new Column<TeamWorkingHours, String>(editCell) {
			@Override
			public String getValue(TeamWorkingHours object) {
//				System.out.println("RESERVED TIME :: "+object.getReservedTime());
				return object.getReservedTime()+"";
			}
		};
		table.addColumn(getReservedTimeColumn,"#Reserved Time(%)");
	}
	
	
	private void getTotalWorkingHrsColumn() {
		getTotalWorkingHrsColumn = new TextColumn<TeamWorkingHours>() {
			@Override
			public String getValue(TeamWorkingHours object) {
				return object.getTotalWorkingHrs()+"";
			}
		};
		table.addColumn(getTotalWorkingHrsColumn,"Total Working Hours");
	}

	
	
	
	
	
	
	private void addColumnDelete() {
		ButtonCell btnCell= new ButtonCell();
		delete = new Column<TeamWorkingHours, String>(btnCell) {
			@Override
			public String getValue(TeamWorkingHours object) {
				return "Delete";
			}
		};
		table.addColumn(delete,"Delete");
	}

	private void setFieldUpdaterOnDelete() {
		delete.setFieldUpdater(new FieldUpdater<TeamWorkingHours, String>() {
			@Override
			public void update(int index, TeamWorkingHours object, String value) {
				getDataprovider().getList().remove(object);
			}
		});
	}
	
	
	private void setFieldUpdaterOnReservedTime() {
		getReservedTimeColumn.setFieldUpdater(new FieldUpdater<TeamWorkingHours, String>() {
			@Override
			public void update(int index, TeamWorkingHours object, String value) {
				try {
					double reservedtimePer = Double.parseDouble(value.trim());
//					System.out.println("RESERVED TIME :::::: "+reservedtimePer);
					double perdayreservedtime=(object.getTeamWorkingHrs()/100)*reservedtimePer;
					double totalWorkingHrs=object.getTeamWorkingHrs()-perdayreservedtime;
					object.setTotalWorkingHrs(totalWorkingHrs);
					object.setReservedTime(reservedtimePer);
					
					object.setOverTime(0.0);
				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			}
		});
	}
	
	private void setFieldUpdaterOnOverTime() {
		getOverTimeColumn.setFieldUpdater(new FieldUpdater<TeamWorkingHours, String>() {
			@Override
			public void update(int index, TeamWorkingHours object, String value) {
				try {
					double overtimePer = Double.parseDouble(value.trim());
					double perdayovertime=(object.getTeamWorkingHrs()/100)*overtimePer;
					double totalWorkingHrs=object.getTeamWorkingHrs()+perdayovertime;
					object.setTotalWorkingHrs(totalWorkingHrs);
					object.setOverTime(overtimePer);
					
					object.setReservedTime(0.0);
					
				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			}
		});
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		setFieldUpdaterOnReservedTime();
		setFieldUpdaterOnOverTime();
		setFieldUpdaterOnDelete();
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
