package com.slicktechnologies.client.views.schedulingandrouting;

import java.util.Date;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.HTMLTable.CellFormatter;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.timereport.TimeSheetLineItem;
import com.slicktechnologies.shared.common.scheduling.SchedulingAndRouting;

public class SchedulingUiComposite extends Composite implements CompositeInterface, ClickHandler, ValueChangeHandler<Double>,HasValue<List<SchedulingAndRouting>> {

	
	/** Flex Table for Setting User Interface Elements. */
	FlexTable table;

	/** To Provide Vertical and Horizontal Functionality. */
	ScrollPanel panel;
	
	/** Date corresponding to first column. */
	private Date fromDate;

	/** Date corresponding to End column. */
	private Date toDate;
	
	public SchedulingUiComposite() {
		super();

		table = new FlexTable();
		table.addClickHandler(this);
		panel = new ScrollPanel();
		panel.add(table);
		initWidget(panel);
		applyStyle();
	}

	public void applyStyle() {
		panel.setHeight("250px");
		panel.setWidth("100%");
	
		table.setWidth("100%");
		
		table.getColumnFormatter().addStyleName(0, "firstcolumn");
//		 Window.enableScrolling(true);
		
		// apply other column style
//		for (int i = 1; i < 10; i++) {
//			table.getColumnFormatter().addStyleName(i, "othercolumn");
//		}
	}
	
	
	public void initializeSchedulingUiComposite(Date startDate, Date endDate) {
		this.fromDate = startDate;
		this.toDate = endDate;
//		if (table.getRowCount() != 0) {
//			changePeriod(startDate, endDate);
//			clear();
//
//		} else {
//			Console.log("Table Row zero.");
			setHeaderRow(fromDate, toDate);
//			generateShiftAndMachineRow();
//			generateTotalRow();
//		}
	}
	
	
	/**
	 * Sets the Header Date Column on Table.
	 * @param fromDate First Column Date
	 * @param toDate Last Column Date
	 *            
	 */
	private void setHeaderRow(Date fromDate, Date toDate) {
		
		int noDays = CalendarUtil.getDaysBetween(fromDate, toDate);
		CellFormatter formater = table.getCellFormatter();
		
		InlineLabel projectName = new InlineLabel("Date/Time(24 Hours)");
		table.setWidget(0, 0, projectName);
		
		formater.setHorizontalAlignment(0, 0, HasAlignment.ALIGN_LEFT);
		
		for (int i = 1; i <= noDays + 1; i++) {
			DateTimeFormat fmt = DateTimeFormat.getFormat("EEE dd MM");
			String day = fmt.format(fromDate);
			table.setWidget(0, i, new InlineLabel(day));
//			table.setWidth("100%");
			CalendarUtil.addDaysToDate(fromDate, 1);
		}
		
//		 Window.enableScrolling(true);
//		table.setWidget(0, 8, new InlineLabel("Total"));

		for (int i = 1; i < table.getCellCount(0); i++) {
			formater.setHorizontalAlignment(0, i, HasAlignment.ALIGN_CENTER);
		}
	}
	
	
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<List<SchedulingAndRouting>> handler) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SchedulingAndRouting> getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setValue(List<SchedulingAndRouting> value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setValue(List<SchedulingAndRouting> value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onValueChange(ValueChangeEvent<Double> event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean status) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return false;
	}

}
