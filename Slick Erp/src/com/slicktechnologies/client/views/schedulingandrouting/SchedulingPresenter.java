package com.slicktechnologies.client.views.schedulingandrouting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.calendar.CalendarPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.scheduling.HolidayDates;
import com.slicktechnologies.shared.common.scheduling.SchedulingAndRouting;
import com.slicktechnologies.shared.common.scheduling.SchedulingUi;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.slicktechnologies.shared.common.scheduling.TeamWorkingHours;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

public class SchedulingPresenter extends FormScreenPresenter<SchedulingAndRouting> implements ClickHandler, Handler, ValueChangeHandler<Date>, ChangeHandler {
	static ScheduliingForm form;
	static SchedulingServiceAsync schedulingService = GWT.create(SchedulingService.class);
	
	final static  GenricServiceAsync async = GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	ArrayList<SalesLineItem> serviceItem;
	ArrayList<Service> schedLIst=new ArrayList<Service>();
	ArrayList<Service> unSchedLIst=new ArrayList<Service>();
	int totalWorkingDays;
	
	FromAndToDateBoxPopup frmAndToPop=new FromAndToDateBoxPopup();
	PopupPanel panel ;
	ArrayList<String> teamList;
	
	public SchedulingPresenter(UiScreen<SchedulingAndRouting> view,SchedulingAndRouting model) {
		super(view, model);
		form = (ScheduliingForm) view;
		form.btnGo.addClickHandler(this);
		form.btnAdd.addClickHandler(this);
		form.teamWorkingHrTable.getTable().addRowCountChangeHandler(this);
		form.fromdate.addValueChangeHandler(this);
		form.todate.addValueChangeHandler(this);
		
		form.lbTeam.addChangeHandler(this);
		form.btnDownload.addClickHandler(this);
		form.btnDownload1.addClickHandler(this);
//		form.uitable.getTable().addRowCountChangeHandler(this);
		
		
		frmAndToPop.btnOne.addClickHandler(this);
		frmAndToPop.btnTwo.addClickHandler(this);
		
		
	}

	public static void initalize() {
		ScheduliingForm form = new ScheduliingForm();
		SchedulingPresenter presenter = new SchedulingPresenter(form,new SchedulingAndRouting());
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		
		if (text.equals("Update Services")){
			
			panel = new PopupPanel(true);
			panel.add(frmAndToPop);
			panel.show();
			panel.center();
			
		}
		
		if (text.equals("Update Customer Locality")){
			updateCustomerEntity();
		}
		
		if (text.equals("Update Service Product")){
			updateServiceProductEntity();
		}
		
		if (text.equals("Update Calendar")){
			saveServices();
		}
		
		if(text.equals("Download(Google Calendar Format)")){
			if(form.processedtable.getDataprovider().getList().size()!=0){
				teamList=new ArrayList<String>();
				teamList=getTeamList();	
				sendEmailTeamWise();
			}else{
				form.showDialogMessage("No scheduled services !");
			}
			
		}
	}

	
	private void sendEmailTeamWise() {
		System.out.println("Team List Size :: " + teamList.size());
		System.out.println("Team Name :: " + teamList.get(0));
		ArrayList<Service> list = getServicesTeamWise(teamList.get(0));
		teamList.remove(0);
		System.out.println("Team Services :: " + list.size());
		reactOnDownloadGoogleCalendarFormat(list);
	}
	
	
	private ArrayList<Service> getServicesTeamWise(String teamName){
		ArrayList<Service> servlist=new ArrayList<Service>();
		for(int i=0;i<form.processedtable.getDataprovider().getList().size();i++){
			if(teamName.equals(form.processedtable.getDataprovider().getList().get(i).getTeam())){
				servlist.add(form.processedtable.getDataprovider().getList().get(i));
			}
		}
		
		return servlist;
	}
	
	private ArrayList<String> getTeamList(){
		
		ArrayList<String> teamList=new ArrayList<String>();
		HashSet<String> hset=new HashSet<String>();
		for(int i=0;i<form.processedtable.getDataprovider().getList().size();i++){
			hset.add(form.processedtable.getDataprovider().getList().get(i).getTeam());
		}
		
		teamList.addAll(hset);
		return teamList;
	}
	
	
	private void reactOnDownloadGoogleCalendarFormat(ArrayList<Service> serviceList) {
		CsvServiceAsync csvser = GWT.create(CsvService.class);
		System.out.println("INSIDE SUCCESS !!!");
		System.out.println("DOWNLOAD OF  :: "+serviceList.get(0).getTeam());
		csvser.setScheduledServicelist(serviceList,new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 107;
				Window.open(url, "test", "enabled");
				
				if(teamList.size()!=0){
					System.out.println("Team List Size :: "+teamList.size());
					System.out.println("Team Name :: "+teamList.get(0));
					ArrayList<Service> list=getServicesTeamWise(teamList.get(0));
					teamList.remove(0);
					System.out.println("Team Services :: "+list.size());
					reactOnDownloadGoogleCalendarFormat(list);
				}
			}
		});
	}

	private void saveServices() {
		
		if(form.processedtable.getDataprovider().getList().size()!=0){
			List<Service> list=form.processedtable.getDataprovider().getList();
			ArrayList<Service> servlist =new ArrayList<Service>();
			servlist.addAll(list);
			
			form.showWaitSymbol();
			schedulingService.saveServices(servlist, new AsyncCallback<ReturnFromServer>(){
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
					form.showDialogMessage("Unexpected Error!");
					form.table.connectToLocal();
					form.processedtable.connectToLocal();
				}
				@Override
				public void onSuccess(ReturnFromServer result) {
					form.hideWaitSymbol();
					form.table.connectToLocal();
					form.processedtable.connectToLocal();
					form.showDialogMessage("Updated Successfully !");
				}
			});
			
			
		}else{
			form.showDialogMessage("No schedule services!");
		}
		
	}

	private void updateServiceProductEntity() {
		Vector<Filter> filtervec=new Vector<Filter>();
		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ServiceProduct());
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("ServiceProduct Result Size ::: "+result.size());
				if (result.size() != 0) {
					int count=1;
					for(SuperModel model:result){
						ServiceProduct con=(ServiceProduct) model;
							
						if(count==1){
							count++;
							con.setServiceTime(1d);
						}
						if(count==2){
							count++;
							con.setServiceTime(2d);
						}
						if(count==3){
							count++;
							con.setServiceTime(3d);
						}
						if(count==4){
							count++;
							con.setServiceTime(4d);
						}
						if(count==5){
							count++;
							con.setServiceTime(5d);
						}
						if(count==6){
							count=1;
							con.setServiceTime(6d);
						}
						
//						if(custListWthOutLoc.size()!=0){
						
						async.save(con,new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected error occurred!");
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								form.showDialogMessage("ENTITY UPDATED SUCCESSFULLY!");
							}
						});
//						}
					}
				}
				
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		System.out.println("INSIDE PRESENTER CLICK");
		if(event.getSource().equals(form.btnGo)){
			if(validateDate()){
				if(form.teamWorkingHrTable.getDataprovider().getList().size()!=0){
					ArrayList<Filter> serviceFilter = getServiceFilter();
					form.showWaitSymbol();
					schedulingService.getServicesForScheduling(serviceFilter, new AsyncCallback<ArrayList<Service>>(){
						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
							form.showDialogMessage("Unexpected Error!");
							form.table.connectToLocal();
							form.processedtable.connectToLocal();
						}
						@Override
						public void onSuccess(ArrayList<Service> result) {
							form.hideWaitSymbol();
							form.table.connectToLocal();
							form.processedtable.connectToLocal();
							if(result.size()!=0){
								List<Service> unprocessedList=new ArrayList<Service>();
								for(Service renres:result){
									unprocessedList.add(renres);
								}
								double totalServicingHrs=0;
								for(int i=0;i<unprocessedList.size();i++){
									if(unprocessedList.get(i).getServicingTime()!=null){
										totalServicingHrs=totalServicingHrs+unprocessedList.get(i).getServicingTime();
									}
								}
								double totalNoOfServices=result.size();
								System.out.println("TOTAL NOS OF SERVICES  :: "+totalNoOfServices);
								System.out.println("TOTAL SERVICING HRS :: "+totalServicingHrs);
								
								form.dbTotalServicingHrs.setValue(totalServicingHrs);
								form.dbTotalNoOfServices.setValue(totalNoOfServices);
								
								if(form.dbTotalServicingHrs.getValue()>form.dbTotalTeamHrs.getValue()){
									
									form.dbTotalServicingHrs.addStyleName("red");
									
									boolean conf = Window.confirm("Servicing hours is greater than total team hours,Do you want to continue?");
									if (conf == true) {
										shedulingServices(unprocessedList,form.teamWorkingHrTable.getDataprovider().getList());
									}else{
										System.out.println("CANCELLED.................");
									}
								}else{
									shedulingServices(unprocessedList,form.teamWorkingHrTable.getDataprovider().getList());
								}
							}
						}
					});
					
					
					
					form.lbTeam.clear();
					form.lbTeam.addItem("--SELECT--");
					form.lbTeam.addItem("All");
					for(int i=0;i<form.teamWorkingHrTable.getDataprovider().getList().size();i++){
						form.lbTeam.addItem(form.teamWorkingHrTable.getDataprovider().getList().get(i).getTeamName());
					}
					
					
					}else{
						form.showDialogMessage("Please add team!");
					}
			}
		}
		
		if(event.getSource().equals(form.btnAdd)){
			if(form.olbTeam.getValue()!=null){
				if(validateDuplicateTeam()){
				TeamManagement team=form.olbTeam.getSelectedItem();
				
				TeamWorkingHours teamWkhr=new TeamWorkingHours();
				teamWkhr.setCompanyId(UserConfiguration.getCompanyId());
				teamWkhr.setTeamName(team.getTeamName());
				teamWkhr.setFromTime(team.getFromTime());
				teamWkhr.setToTime(team.getToTime());
				teamWkhr.setPerDayWorkingHrs(team.getLeaveCalendar().getWorkingHours());
				teamWkhr.setTeamWorkingHrs(team.getTotalWorkingHrs());
				teamWkhr.setReservedTime(0);
				teamWkhr.setOverTime(0);
				teamWkhr.setTotalWorkingHrs(team.getTotalWorkingHrs());
				
				
				
				form.teamWorkingHrTable.getDataprovider().getList().add(teamWkhr);
				}
				
			}else{
				form.showDialogMessage("Please select team !");
			}
		}
		
		
		if(event.getSource().equals(form.btnDownload)){
			if(form.processedtable.getDataprovider().getList().size()!=0){
				reactOnDownload();
			}else{
				form.showDialogMessage("No schedule services !");
			}
		}
		
		if(event.getSource().equals(form.btnDownload1)){
			if(form.table.getDataprovider().getList().size()!=0){
				reactOnDownload1();
			}else{
				form.showDialogMessage("No unschedule services !");
			}
		}
		
		
		
		if(event.getSource()==frmAndToPop.btnOne){
			loadServiceProduct();
			panel.hide();
		}
		
		if(event.getSource()==frmAndToPop.btnTwo){
			panel.hide();
		}
		
		
	}
	
	private void shedulingServices(List<Service> unprocessedList,List<TeamWorkingHours> list) {
		ArrayList<HolidayDates> holidayList=getHolidayList();
		
		ArrayList<Service>serviceList=new ArrayList<Service>();
		serviceList.addAll(unprocessedList);
		ArrayList<TeamWorkingHours>teamList=new ArrayList<TeamWorkingHours>();
		teamList.addAll(list);
		
//		System.out.println("@@@ FROM DATE :: "+form.fromdate.getValue());
//		System.out.println("@@@ TO DATE :: "+form.todate.getValue());
		
		Date date =form.fromdate.getValue();
//		date.se
		schedulingService.schedulingServices(form.fromdate.getValue(),form.todate.getValue(),serviceList, teamList,holidayList, new AsyncCallback<ArrayList<Service>>(){
			@Override
			public void onFailure(Throwable caught) {
				
			}
					
			@Override
			public void onSuccess(ArrayList<Service> result) {
				List<Service> unscheduledList=new ArrayList<Service>();
				List<Service> scheduledLIst=new ArrayList<Service>();
				schedLIst=new ArrayList<Service>();
				unSchedLIst=new ArrayList<>();
				Comparator<Service> dateComparator = new Comparator<Service>() {  
					@Override  
					public int compare(Service o1, Service o2) {  
						return o1.getServiceDate().compareTo(o2.getServiceDate());  
					}  
				};
				Collections.sort(result,dateComparator);	
				
				
				System.out.println(" !!!!!!!! RESULT SIZE ::: "+result.size());
				
				
				for(Service serv:result){
					if(serv.getScheduled()){
						scheduledLIst.add(serv);
					}else{
						unscheduledList.add(serv);
					}
				}
				schedLIst.addAll(scheduledLIst);
				unSchedLIst.addAll(unscheduledList);
				
				double totalSchServicingHrs=0;
				for(int i=0;i<scheduledLIst.size();i++){
					totalSchServicingHrs=totalSchServicingHrs+scheduledLIst.get(i).getServicingTime();
				}
				double totalNoOfSchServices=scheduledLIst.size();
				
				System.out.println("TOTAL NOS OF SCHEDULE SERVICES  :: "+totalNoOfSchServices);
				System.out.println("TOTAL SCHEDULE SERVICING HRS :: "+totalSchServicingHrs);
				form.dbTotalNoOfScheduledServices.setValue(totalNoOfSchServices);
				form.dbTotalScheduledHrs.setValue(totalSchServicingHrs);
				
				
				double totalUschServicingHrs=0;
				for(int i=0;i<unscheduledList.size();i++){
					totalUschServicingHrs=totalUschServicingHrs+unscheduledList.get(i).getServicingTime();
				}
				double totalNoOfUschServices=unscheduledList.size();
				System.out.println("TOTAL NOS OF UNSCHEDULE SERVICES  :: "+totalNoOfUschServices);
				System.out.println("TOTAL UNSCHEDULE SERVICING HRS :: "+totalUschServicingHrs);
				form.dbTotalUncheduledHrs.setValue(totalUschServicingHrs);
				form.dbTotalNoOfUncheduledServices.setValue(totalNoOfUschServices);
				
				
				
				form.processedtable.getDataprovider().setList(scheduledLIst);
				form.table.getDataprovider().setList(unscheduledList);
				
				Comparator<Service> idComparator = new Comparator<Service>() {  
					@Override  
					public int compare(Service o1, Service o2) {  
						
						Integer configName1=o1.getCount();
						Integer configName2=o2.getCount();
						return configName1.compareTo(configName2);  
					}  
				};
				Collections.sort(scheduledLIst,idComparator);	
				Collections.sort(unscheduledList,idComparator);
				System.out.println();
				System.out.println("SCHEDULE LIST SIZE :: "+scheduledLIst.size());
				System.out.println();
				System.out.println();
				for(int i=0;i<scheduledLIst.size();i++){
					System.out.println(i+" - "+scheduledLIst.get(i).getCount());
				}
				System.out.println();
				System.out.println("UNSCHEDULE LIST SIZE :: "+unscheduledList.size());
				System.out.println();
				System.out.println();
				for(int i=0;i<unscheduledList.size();i++){
					System.out.println(i+" - "+unscheduledList.get(i).getCount());
				}
				
			}
		});
	}
	
	public boolean validateDuplicateTeam(){
		if(form.teamWorkingHrTable.getDataprovider().getList().size()!=0){
			for(int i=0;i<form.teamWorkingHrTable.getDataprovider().getList().size();i++){
				if(form.olbTeam.getValue().equals(form.teamWorkingHrTable.getDataprovider().getList().get(i).getTeamName())){
					form.showDialogMessage("Can't add duplicate team!");
					return false;
				}
			}
		}
		return true;
	}
	
	public boolean validateDate(){
		Date formDate;
		Date toDate;
		if(form.fromdate.getValue()!=null&&form.todate.getValue()!=null){
			formDate=form.fromdate.getValue();
			toDate=form.todate.getValue();
			if(toDate.equals(formDate)||toDate.after(formDate)){
				return true;
			}else{
				form.showDialogMessage("To Date should be greater than From Date.");
				return false;
			}
		}
		else{
			form.showDialogMessage("Please select From Date and To Date!");
			return false;
		}
	}
	
	
	
	public ArrayList<Filter>  getServiceFilter()
	{
//		System.out.println("INSIDE FILTERS ");
		ArrayList<Filter> filtervec=new ArrayList<Filter>();
		Filter temp=null;
		
		
		
		if(form.lbStatus.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(form.lbStatus.getValue(form.lbStatus.getSelectedIndex()).trim());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}else{
			ArrayList<String> statusList=new ArrayList<String>();
			statusList.add(Service.SERVICESTATUSRESCHEDULE);
			statusList.add(Service.SERVICESTATUSSCHEDULE);
			
			temp = new Filter();
			temp.setQuerryString("status IN");
			temp.setList(statusList);
			filtervec.add(temp);
		}
		
		if(form.olbBranch.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(form.olbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		
		if(form.fromdate.getValue()!=null){
			temp = new Filter();
			temp.setQuerryString("serviceDate >=");
			temp.setDateValue(form.fromdate.getValue());
			filtervec.add(temp);
		}
			
		if(form.todate.getValue()!=null){
			temp = new Filter();
			temp.setQuerryString("serviceDate <=");
			temp.setDateValue(form.todate.getValue());
			filtervec.add(temp);
		}
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		if(UserConfiguration.getCompanyId()!=null)
			temp.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(temp);
		
		return filtervec;	
	}
	
	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}

	@Override
	protected void makeNewModel() {
		
	}
	
	
	private void updateCustomerEntity() {
		Vector<Filter> filtervec=new Vector<Filter>();
		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("Customer Result Size ::: "+result.size());
				ArrayList<Service> custListWthLoc=new ArrayList<Service>(); 
				ArrayList<Service> custListWthOutLoc=new ArrayList<Service>(); 
				if (result.size() != 0) {
					int count=1;
					int cnt=1;
					for(SuperModel model:result){
						Service con=(Service) model;

						if(!con.getLocality().equals("")){
							custListWthLoc.add(con);
						}else{
							custListWthOutLoc.add(con);
							
							if(count==1){
								count++;
								con.setLocality("Vikhroli");
							}
							if(count==2){
								count++;
								con.setLocality("Pimpri Tagore Nagar");
							}
							if(count==3){
								count++;
								con.setLocality("Ghansoli Sector 4");
							}
							if(count==4){
								count=1;
								con.setLocality("Kopar Khairane Sector 12");
							}
						}
						
						if(custListWthOutLoc.size()!=0){
						
						async.save(con,new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected error occurred!");
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								form.showDialogMessage("ENTITY UPDATED SUCCESSFULLY!");
							}
						});
						}
					}
				}
				
				System.out.println("Customers With Locality :: "+custListWthLoc.size());
				System.out.println("Customers With Out Locality :: "+custListWthOutLoc.size());
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}
	
	
	
	private void updateServiceEntity() {
		Vector<Filter> filtervec=new Vector<Filter>();
		MyQuerry querry = new MyQuerry();
		
		Filter filter =null;
		//******************rohan added filter for record update
		
		filter = new Filter();
		filter.setQuerryString("serviceDate >=");
		filter.setDateValue(frmAndToPop.getFromDate().getValue());
		filtervec.add(filter);
		
		
		filter = new Filter();
		filter.setQuerryString("serviceDate <=");
		filter.setDateValue(frmAndToPop.getToDate().getValue());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
		form.showWaitSymbol();
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				System.out.println("aaaaaaaaa result size"+result.size());
				
				if (result.size() != 0) {
					form.hideWaitSymbol();
					for(SuperModel model:result){
						Service con=(Service) model;

						if(con.getProduct()!=null){
							for(int i=0;i<serviceItem.size();i++){
								if(con.getProduct().getCount()==serviceItem.get(i).getPrduct().getCount()){
									System.out.println("Found ::: "+con.getProduct().getCount()+" --- "+serviceItem.get(i).getPrduct().getCount());
									con.setServicingTime(serviceItem.get(i).getServicingTIme());
								}
							}
						}
						
						async.save(con,new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
								form.showDialogMessage("An Unexpected error occurred!");
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								form.hideWaitSymbol();
								form.showDialogMessage("ENTITY UPDATED SUCCESSFULLY!");
							}
						});
					}
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
		});
		
	}
	
	
	
	public void loadServiceProduct(){
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new SuperProduct());
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				serviceItem=new ArrayList<SalesLineItem>();
				SalesLineItem lis=null;
				for(SuperModel model:result)
				{
					SuperProduct superProdEntity = (SuperProduct)model;
					lis=AppUtility.ReactOnAddProductComposite(superProdEntity);
					serviceItem.add(lis);
				}
				
				System.out.println("PRODUCT LIST SIZE :: "+serviceItem.size());
				updateServiceEntity();
			}
		 });
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
//		System.out.println("ROW COUNT CHANGE METHOD");
		if(event.getSource()==form.teamWorkingHrTable.getTable()){
			double totalWorkingHours=0;
			if(form.teamWorkingHrTable.getDataprovider().getList().size()!=0){
				for(int i=0;i<form.teamWorkingHrTable.getDataprovider().getList().size();i++){
					totalWorkingHours=totalWorkingHours+form.teamWorkingHrTable.getDataprovider().getList().get(i).getTotalWorkingHrs();
				}
				if(form.fromdate.getValue()!=null&&form.todate.getValue()!=null){
					totalWorkingDays=getTotalWorkingDays();
//					System.out.println("TOTAL WORKING DAYS :: "+totalWorkingDays);
					
					double teamHrs=totalWorkingDays*totalWorkingHours;
//					System.out.println("TOTAL TEAM HRS  :: "+teamHrs);
					form.dbTotalTeamHrs.setValue(teamHrs);
				}else{
					form.dbTotalTeamHrs.setValue(totalWorkingHours);
				}
			}else{
				form.dbTotalTeamHrs.setValue(totalWorkingHours);
			}
		}
	}

	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		if(event.getSource().equals(form.fromdate)){
			if(form.fromdate.getValue()!=null&&form.todate.getValue()!=null){
				double totalWorkingHours=0;
				if(form.teamWorkingHrTable.getDataprovider().getList().size()!=0){
					for(int i=0;i<form.teamWorkingHrTable.getDataprovider().getList().size();i++){
						totalWorkingHours=totalWorkingHours+form.teamWorkingHrTable.getDataprovider().getList().get(i).getTotalWorkingHrs();
					}
					totalWorkingDays=getTotalWorkingDays();
//					System.out.println("TOTAL WORKING DAYS :: "+totalWorkingDays);
					double teamHrs=totalWorkingDays*totalWorkingHours;
//					System.out.println("TOTAL TEAM HRS  :: "+teamHrs);
					form.dbTotalTeamHrs.setValue(teamHrs);
				}else{
					form.dbTotalTeamHrs.setValue(totalWorkingHours);
				}
				
				
			}
		}
		
		if(event.getSource().equals(form.todate)){
			if(form.fromdate.getValue()!=null&&form.todate.getValue()!=null){
				double totalWorkingHours=0;
				if(form.teamWorkingHrTable.getDataprovider().getList().size()!=0){
					for(int i=0;i<form.teamWorkingHrTable.getDataprovider().getList().size();i++){
						totalWorkingHours=totalWorkingHours+form.teamWorkingHrTable.getDataprovider().getList().get(i).getTotalWorkingHrs();
					}
					totalWorkingDays=getTotalWorkingDays();
//					System.out.println("TOTAL WORKING DAYS :: "+totalWorkingDays);
					double teamHrs=totalWorkingDays*totalWorkingHours;
//					System.out.println("TOTAL TEAM HRS  :: "+teamHrs);
					form.dbTotalTeamHrs.setValue(teamHrs);
				}else{
					form.dbTotalTeamHrs.setValue(totalWorkingHours);
				}
			}
		}
	}
	
	public int getTotalWorkingDays(){
		int days=0;
		Date levFrmDt=form.fromdate.getValue();
		Date levToDt=form.todate.getValue();
		
		int dayduration = CalendarUtil.getDaysBetween(levFrmDt, levToDt) + 1;
		System.out.println("Duration is " + dayduration);
		
		int weekend=isWeeklyOff();
//		System.out.println("WO :: "+weekend);
		int holiday=isHoliday();
//		System.out.println("HOLIDAY :: "+holiday);
		
		days=dayduration-(weekend+holiday);
		
		
		return days;
	}
	
	
	public int isWeeklyOff(){
		TeamManagement info=form.olbTeam.getItems().get(0);
		Date levFrmDt=form.fromdate.getValue();
		Date levToDt=form.todate.getValue();
		int weekend=0;
		
//		int dayduration = CalendarUtil.getDaysBetween(levFrmDt, levToDt) + 1;
//		System.out.println("Duration is " + dayduration);
		
		Date d1fromdate = CalendarUtil.copyDate(levFrmDt);
		Date d2toate = CalendarUtil.copyDate(levToDt);
		
		while (d1fromdate.before(d2toate) || d1fromdate.equals(d2toate)) {
			boolean weeklyOff = CalendarPresenter.isWeekleyOff(info.getLeaveCalendar().getWeeklyoff(),d1fromdate);
			if (weeklyOff) {
				weekend++;
			}
			CalendarUtil.addDaysToDate(d1fromdate, 1);
		}
		
//		System.out.println("Weekend V: "+weekend);
		return weekend;
	}

	public int isHoliday(){
		TeamManagement info=form.olbTeam.getItems().get(0);
		Date levFrmDt=form.fromdate.getValue();
		Date levToDt=form.todate.getValue();
		int day=0;
		
		Date d3fromdate = CalendarUtil.copyDate(levFrmDt);
		Date d4toate = CalendarUtil.copyDate(levToDt);
		
		while (d3fromdate.before(d4toate) || d3fromdate.equals(d4toate)) {
			boolean isHoliday = info.getLeaveCalendar().isHolidayDate(d3fromdate);
			if(isHoliday){
				day++;
			}
			CalendarUtil.addDaysToDate(d3fromdate, 1);
		}
		
		return day;
	}
	
	public ArrayList<HolidayDates> getHolidayList(){
		ArrayList <HolidayDates> holidayList=new ArrayList<HolidayDates>();
		
		TeamManagement info=form.olbTeam.getItems().get(0);
		
		Date wFrmDt=form.fromdate.getValue();
		Date wToDt=form.todate.getValue();
		
		Date hFrmDt=form.fromdate.getValue();
		Date hToDt=form.todate.getValue();
		
		Date d1fromdate = CalendarUtil.copyDate(wFrmDt);
		Date d2toate = CalendarUtil.copyDate(wToDt);
		
		while (d1fromdate.before(d2toate) || d1fromdate.equals(d2toate)) {
			boolean weeklyOff = CalendarPresenter.isWeekleyOff(info.getLeaveCalendar().getWeeklyoff(),d1fromdate);
			if (weeklyOff) {
				HolidayDates dates=new HolidayDates();
				Date date1=CalendarUtil.copyDate(d1fromdate);
				dates.setHolidayName("WO");
				dates.setHolidayDate(date1);
				holidayList.add(dates);
			}
			CalendarUtil.addDaysToDate(d1fromdate, 1);
		}
		
		
		Date d3fromdate = CalendarUtil.copyDate(hFrmDt);
		Date d4toate = CalendarUtil.copyDate(hToDt);
		
		while (d3fromdate.before(d4toate) || d3fromdate.equals(d4toate)) {
			boolean isHoliday = info.getLeaveCalendar().isHolidayDate(d3fromdate);
			if(isHoliday){
				HolidayDates dates=new HolidayDates();
				Date date1=CalendarUtil.copyDate(d3fromdate);
				dates.setHolidayName("HOLIDAY");
				dates.setHolidayDate(date1);
				holidayList.add(dates);
			}
			CalendarUtil.addDaysToDate(d3fromdate, 1);
		}
		return holidayList;
	}

	@Override
	public void onChange(ChangeEvent event) {

		if(event.getSource().equals(form.lbTeam)){
			if(form.processedtable.getDataprovider().getList().size()!=0){
				if(form.lbTeam.getSelectedIndex()!=0){
					form.processedtable.connectToLocal();
					if(form.lbTeam.getValue(form.lbTeam.getSelectedIndex()).equals("All")){
						form.processedtable.getDataprovider().setList(schedLIst);
					}else{
						ArrayList<Service> list=new ArrayList<Service>();
						for(int i=0;i<schedLIst.size();i++){
							if(form.lbTeam.getValue(form.lbTeam.getSelectedIndex()).equals(schedLIst.get(i).getTeam())){
								list.add(schedLIst.get(i));
							}
						}
						form.processedtable.getDataprovider().setList(list);
					}
				}
			}
		}
	}
	
	
	@Override
	public void reactOnDownload() {
		ArrayList<Service> serviceList = new ArrayList<Service>();
		List<Service> list = (List<Service>) form.processedtable.getDataprovider().getList();
		serviceList.addAll(list);
		csvservice.setScheduledServicelist(serviceList, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 103;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	public void reactOnDownload1() {
		ArrayList<Service> serviceList = new ArrayList<Service>();
		List<Service> list = (List<Service>) form.table.getDataprovider().getList();
		serviceList.addAll(list);
		csvservice.setScheduledServicelist(serviceList, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 104;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	

}
