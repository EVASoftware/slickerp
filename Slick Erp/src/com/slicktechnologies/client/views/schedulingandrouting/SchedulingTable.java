package com.slicktechnologies.client.views.schedulingandrouting;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.shared.Service;

public class SchedulingTable extends SuperTable<Service> {

	Column<Service, Boolean> getcheckRecord;
	TextColumn<Service>getserviceId;
	TextColumn<Service>getserviceNumber;
	TextColumn<Service>getserviceDate;
	TextColumn<Service>getserviceTime;
	TextColumn<Service>getserviceDay;
	TextColumn<Service>getTotalServicingTime;
	TextColumn<Service>getteam;
	TextColumn<Service>getLocalityColumn;
	TextColumn<Service>getserviceBranch;
	TextColumn<Service>getProduct;
	TextColumn<Service>getcontractId;
	TextColumn<Service>getcustomerId;
	TextColumn<Service>getcustomerName;
	TextColumn<Service>getcustomerPhone;
	TextColumn<Service>getBranch;
	TextColumn<Service>getstatus;
	TextColumn<Service>getReason;
	
	
	@Override
	public void createTable() {
		getcheckRecord();
		getserviceId();
		getserviceNumber();
		getserviceDate();
		getserviceTime();
		getserviceDay();
		getTotalServicingTime();
		getteam();
		getLocalityColumn();
		getserviceBranch();
		getProduct();
		
		getcontractId();
		getcustomerId();
		getcustomerName();
		getcustomerPhone();
		
		
		getBranch();
		getstatus();
		getReason();
	}

	private void getReason() {
		getReason=new TextColumn<Service>() {
			
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}

			@Override
			public String getValue(Service object) {
				if(object.getReason()!=null){
					return object.getReason()+"";
				}
				return "";
			}
		};
		table.addColumn(getReason,"Remark");
		table.setColumnWidth(getReason,100,Unit.PX);
		getReason.setSortable(true);
		
	}

	private void getLocalityColumn() {
		getLocalityColumn=new TextColumn<Service>() {
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}
			
			@Override
			public String getValue(Service object) {
				return object.getLocality()+"";
			}
		};
		table.addColumn(getLocalityColumn,"Locality");
		table.setColumnWidth(getLocalityColumn,100,Unit.PX);
		getLocalityColumn.setSortable(true);
	}

	private void getTotalServicingTime() {
		getTotalServicingTime=new TextColumn<Service>() {
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}

			@Override
			public String getValue(Service object) {
				return object.getServicingTime()+"";
			}
		};
		
		table.addColumn(getTotalServicingTime,"Servicing Time(Hrs)");
		table.setColumnWidth(getTotalServicingTime,160,Unit.PX);
		getTotalServicingTime.setSortable(true);
	}

	private void getserviceBranch() {
		getserviceBranch=new TextColumn<Service>() {
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}

			@Override
			public String getValue(Service object) {
				return object.getServiceBranch();
			}
		};
		
		table.addColumn(getserviceBranch,"Service Branch");
		table.setColumnWidth(getserviceBranch,120,Unit.PX);
		getserviceBranch.setSortable(true);
		
	}

	private void getProduct() {
		getProduct=new TextColumn<Service>() {
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}

			@Override
			public String getValue(Service object) {
				return object.getProductName();
			}
		};
		
		table.addColumn(getProduct,"Product");
		table.setColumnWidth(getProduct,120,Unit.PX);
		getProduct.setSortable(true);
		
	}

	private void getBranch() {
		getBranch=new TextColumn<Service>() {
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}

			@Override
			public String getValue(Service object) {
				return object.getBranch();
			}
		};
		
		table.addColumn(getBranch,"Branch");
		table.setColumnWidth(getBranch,90,Unit.PX);
		getBranch.setSortable(true);
		
	}

	private void getstatus() {
		getstatus=new TextColumn<Service>() {
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}

			@Override
			public String getValue(Service object) {
			
				return object.getStatus();
			}
		};
		
		table.addColumn(getstatus,"Status");
		table.setColumnWidth(getstatus,100,Unit.PX);
		getstatus.setSortable(true);
		
	}

	private void getcheckRecord() {
		CheckboxCell checkCell= new CheckboxCell();
		getcheckRecord = new Column<Service, Boolean>(checkCell) {

			@Override
			public Boolean getValue(Service object) {
				
//				return object.getRecordSelect();
				return false;
			}
		};
		table.addColumn(getcheckRecord,"Select");
		table.setColumnWidth(getcheckRecord, 60, Unit.PX);
	}

	private void getserviceId() {
		getserviceId=new TextColumn<Service>() {
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}

			@Override
			public String getValue(Service object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getserviceId,"Service ID");
		table.setColumnWidth(getserviceId,"100px");
		getserviceId.setSortable(true);
	}

	private void getserviceNumber() {
		getserviceNumber=new TextColumn<Service>() {
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}

			@Override
			public String getValue(Service object) {
				return object.getServiceSerialNo()+"";
			}
		};
		
		table.addColumn(getserviceNumber,"Sr No");
		table.setColumnWidth(getserviceNumber, 80, Unit.PX);
		getserviceNumber.setSortable(true);	
	}

	private void getserviceDate() {
		getserviceDate=new TextColumn<Service>() {
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}
			@Override
			public String getValue(Service object) {
				if(object.getServiceDate()!=null)
				{
				    String serDate=AppUtility.parseDate(object.getServiceDate());
				    return serDate;
				}
				return "Not Set";
			}
		};
		
		table.addColumn(getserviceDate,"Service Date");
        table.setColumnWidth(getserviceDate,120,Unit.PX);
        getserviceDate.setSortable(true);
	}

	private void getserviceTime() {
		getserviceTime=new TextColumn<Service>() {
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}

			@Override
			public String getValue(Service object) {
				return object.getServiceTime();
			}
		};
		
		table.addColumn(getserviceTime,"Service Time");
		table.setColumnWidth(getserviceTime,120,Unit.PX);
		getserviceTime.setSortable(true);	
	}

	private void getserviceDay() {
		getserviceDay=new TextColumn<Service>() {
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}

			@Override
			public String getValue(Service object) {
				if(!object.getServiceDay().equals("Not Select")){
					return object.getServiceDay();
				}
				return "";
			}
		};
		
		table.addColumn(getserviceDay,"Service Day");
		table.setColumnWidth(getserviceDay,120,Unit.PX);
		getserviceDay.setSortable(true);
	}

	private void getcontractId() {
		getcontractId=new TextColumn<Service>() {
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}
			@Override
			public String getValue(Service object) {
				return object.getContractCount()+"";
			}
		};
		table.addColumn(getcontractId,"Contract ID");
		table.setColumnWidth(getcontractId,100,Unit.PX);
		getcontractId.setSortable(true);	
	}

	private void getcustomerId() {
		getcustomerId=new TextColumn<Service>() {
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}

			@Override
			public String getValue(Service object) {
			
				return object.getCustomerId()+"";
			}
		};
		
		table.addColumn(getcustomerId,"Customer ID");
		table.setColumnWidth(getcustomerId,110,Unit.PX);
		getcustomerId.setSortable(true);
	}

	private void getcustomerName() {
		getcustomerName=new TextColumn<Service>() {
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}

			@Override
			public String getValue(Service object) {
				return object.getName();
			}
		};
		
		table.addColumn(getcustomerName,"Customer Name");
		table.setColumnWidth(getcustomerName,150,Unit.PX);
		getcustomerName.setSortable(true);
	}

	private void getcustomerPhone() {
		getcustomerPhone=new TextColumn<Service>() {
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}
			@Override
			public String getValue(Service object) {
				return object.getCellNumber()+"";
			}
		};
		table.addColumn(getcustomerPhone,"Phone");
		table.setColumnWidth(getcustomerPhone,130,Unit.PX);
		getcustomerPhone.setSortable(true);
	}

	private void getteam() {
		getteam=new TextColumn<Service>() {
			
			@Override
			public String getCellStyleNames(Context context, Service object) {
				if(object.getSpecificServFlag()){
					return "blue";
				}
				return "black";
			}

			@Override
			public String getValue(Service object) {
				if(object.getTeam()!=null){
					return object.getTeam();
				}
				return "";
			}
		};
		
		table.addColumn(getteam,"Team");
		table.setColumnWidth(getteam,120,Unit.PX);
		getteam.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
