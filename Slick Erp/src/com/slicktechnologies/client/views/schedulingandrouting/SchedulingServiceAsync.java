package com.slicktechnologies.client.views.schedulingandrouting;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.scheduling.HolidayDates;
import com.slicktechnologies.shared.common.scheduling.TeamWorkingHours;

public interface SchedulingServiceAsync {
	void getServicesForScheduling(ArrayList<Filter>filter,AsyncCallback<ArrayList<Service>> callback);
	void schedulingServices(Date fromDate,Date toDate,ArrayList<Service>services,ArrayList<TeamWorkingHours>teamList,ArrayList<HolidayDates>holidayList,AsyncCallback<ArrayList<Service>> callback);
	void saveServices(ArrayList<Service>services,AsyncCallback callback);
	
	 void validateDateFormat(String userName,AsyncCallback<Boolean> callback);
}
