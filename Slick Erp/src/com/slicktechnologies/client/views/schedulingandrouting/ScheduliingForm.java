package com.slicktechnologies.client.views.schedulingandrouting;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contractrenewal.ContractRenewalTable;
import com.slicktechnologies.client.views.humanresource.timereport.TimeReportEntryComposite;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.scheduling.SchedulingAndRouting;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;

public class ScheduliingForm extends FormScreen<SchedulingAndRouting>implements ClickHandler{

	DateBox fromdate, todate;
	ObjectListBox<Branch> olbBranch;
	ListBox lbStatus;
	Button btnGo;
	
	ObjectListBox<TeamManagement> olbTeam;
	Button btnAdd;
	
	TeamWorkingHourTable teamWorkingHrTable;
	
	DoubleBox dbTotalTeamHrs;
	DoubleBox dbTotalServicingHrs;
	
	
	SchedulingTable table;
	SchedulingTable processedtable;
	
	ListBox lbTeam;	
	
	Button btnDownload;
	Button btnDownload1;
	TextBox tbblanck;
	TextBox tbblanck1;
	
	DoubleBox dbTotalNoOfServices;
	DoubleBox dbTotalNoOfScheduledServices;
	DoubleBox dbTotalNoOfUncheduledServices;
	DoubleBox dbTotalScheduledHrs;
	DoubleBox dbTotalUncheduledHrs;
	
//	SchedulingUiComposite composite;
//	SchedulingUiTable uitable;
	
	public ScheduliingForm() {
		super(FormStyle.DEFAULT);
//		super();
		createGui();
		processLevelBar.btnLabels[0].setVisible(true);
		processLevelBar.btnLabels[1].setVisible(true);
		processLevelBar.btnLabels[2].setVisible(true);
		processLevelBar.btnLabels[3].setVisible(true);
		processLevelBar.btnLabels[4].setVisible(true);
	}
	
	private void initalizeWidget() {

		table=new SchedulingTable();
		processedtable=new SchedulingTable();
		
		fromdate = new DateBoxWithYearSelector();
		todate = new DateBoxWithYearSelector();
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		btnGo=new Button("SCHEDULE");
		
		lbStatus=new ListBox();
		AppUtility.setStatusListBox(lbStatus, Service.getStatusList());
		
		olbTeam = new ObjectListBox<TeamManagement>();
		this.initalizeTeamListBox();
		
		btnAdd=new Button("Add");
		
		teamWorkingHrTable=new TeamWorkingHourTable();
		
		dbTotalServicingHrs=new DoubleBox();
		dbTotalServicingHrs.setEnabled(false);
		
		dbTotalTeamHrs=new DoubleBox();
		dbTotalTeamHrs.setEnabled(false);
		
		lbTeam=new ListBox();
		lbTeam.addItem("--SELECT--");
		btnDownload=new Button("Download");
		btnDownload1=new Button("Download");
		
		tbblanck = new TextBox();
		tbblanck.setVisible(false);
		
		tbblanck1 = new TextBox();
		tbblanck1.setVisible(false);
		
		
		dbTotalNoOfScheduledServices=new DoubleBox();
		dbTotalNoOfScheduledServices.setEnabled(false);
		dbTotalNoOfServices=new DoubleBox();
		dbTotalNoOfServices.setEnabled(false);
		dbTotalNoOfUncheduledServices=new DoubleBox();
		dbTotalNoOfUncheduledServices.setEnabled(false);
		dbTotalScheduledHrs=new DoubleBox();
		dbTotalScheduledHrs.setEnabled(false);
		dbTotalUncheduledHrs=new DoubleBox();
		dbTotalUncheduledHrs.setEnabled(false);
		
//		composite=new SchedulingUiComposite();
		
//		uitable=new SchedulingUiTable();
	}
	
	
	private void initalizeTeamListBox() {
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new TeamManagement());
		olbTeam.MakeLive(querry);
	}

	@Override
	public void createScreen() {
		
		initalizeWidget();

		processlevelBarNames=new String[]{"Update Service Product","Update Services","Update Customer Locality","Update Calendar","Download(Google Calendar Format)"};

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSearchFilter=fbuilder.setlabel("Search Filters").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		
		fbuilder = new FormFieldBuilder("Branch",olbBranch);
		FormField olbBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Working From",fromdate);
		FormField fromdate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Working Till",todate);
		FormField todate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Status",lbStatus);
		FormField lbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnGo);
		FormField btnGo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Team",olbTeam);
		FormField folbTeam= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnAdd);
		FormField fbtnAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",teamWorkingHrTable.getTable());
		FormField teamWorkingHrTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		
		fbuilder = new FormFieldBuilder("Total Team Hours",dbTotalTeamHrs);
		FormField dbTotalTeamHrs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Servicing Hours",dbTotalServicingHrs);
		FormField dbTotalServicingHrs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Scheduled Hours",dbTotalScheduledHrs);
		FormField dbTotalScheduledHrs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Unscheduled Hours",dbTotalUncheduledHrs);
		FormField dbTotalUncheduledHrs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total No. Of Services",dbTotalNoOfServices);
		FormField dbTotalNoOfServices= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total No. Of Scheduled Services",dbTotalNoOfScheduledServices);
		FormField dbTotalNoOfScheduledServices= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total No. Of Unscheduled Services",dbTotalNoOfUncheduledServices);
		FormField dbTotalNoOfUncheduledServices= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSearchResult=fbuilder.setlabel("Unscheduled Servies").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("",table.getTable());
		FormField table= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSearchProcessedResult=fbuilder.setlabel("Scheduled Servies").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Team Filter",lbTeam);
		FormField lbTeam= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbblanck);
		FormField tbblanck= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		
		fbuilder = new FormFieldBuilder("",tbblanck1);
		FormField tbblanck1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",btnDownload);
		FormField btnDownload= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnDownload1);
		FormField btnDownload1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("",processedtable.getTable());
		FormField processedtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
//		fbuilder = new FormFieldBuilder("",composite);
//		FormField fComposite= fbuilder.setMandatory(true).setMandatoryMsg("Please Enter at least one row !").setRowSpan(0).setColSpan(5).build();
		
//		fbuilder = new FormFieldBuilder("",uitable.getTable());
//		FormField uitable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		FormField[][] formfield = {   
				{fgroupingSearchFilter},
				{olbBranch,lbStatus,fromdate,todate},
				{folbTeam,fbtnAdd},
				{teamWorkingHrTable},
				{dbTotalTeamHrs,dbTotalServicingHrs,dbTotalScheduledHrs,dbTotalUncheduledHrs},
				{dbTotalNoOfServices,dbTotalNoOfScheduledServices,dbTotalNoOfUncheduledServices,btnGo},
				{fgroupingSearchResult},
				{tbblanck,tbblanck1,btnDownload1},
				{table},
				{fgroupingSearchProcessedResult},
				{lbTeam,tbblanck,btnDownload},
				{processedtable},
//				{fComposite}
//				{uitable}
		};
		this.fields=formfield;	
	}
	
	@Override
	public void updateModel(SchedulingAndRouting model) {
	}

	@Override
	public void updateView(SchedulingAndRouting model) {
	}
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard")) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard"))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard"))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SCHEDULINGANDROUTING,LoginPresenter.currentModule.trim());
	}

	@Override
	public void onClick(ClickEvent event) {
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		
		dbTotalServicingHrs.setEnabled(false);
		dbTotalTeamHrs.setEnabled(false);
	}

	
	

}
