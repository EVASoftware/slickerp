package com.slicktechnologies.client.views.schedulingandrouting;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.scheduling.HolidayDates;
import com.slicktechnologies.shared.common.scheduling.TeamWorkingHours;

@RemoteServiceRelativePath("serviceshceduling")
public interface SchedulingService extends RemoteService {
	public ArrayList<Service> getServicesForScheduling(ArrayList<Filter>filter);
	public ArrayList<Service> schedulingServices(Date fromDate,Date toDate,ArrayList<Service>services,ArrayList<TeamWorkingHours>teamList,ArrayList<HolidayDates>holidayList);
	public void saveServices(ArrayList<Service>services);
	
	Boolean validateDateFormat(String userName);
}
