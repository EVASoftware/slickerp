package com.slicktechnologies.client.views.schedulingandrouting;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.scheduling.SchedulingUi;

public class SchedulingUiTable extends SuperTable<SchedulingUi> {

	TextColumn<SchedulingUi> getTimeColoumn;
	Column<SchedulingUi,String> getDay1Coloumn;
	Column<SchedulingUi,String> getDay2Coloumn;
	Column<SchedulingUi,String> getDay3Coloumn;
	Column<SchedulingUi,String> getDay4Coloumn;
	Column<SchedulingUi,String> getDay5Coloumn;
	Column<SchedulingUi,String> getDay6Coloumn;
	Column<SchedulingUi,String> getDay7Coloumn;
	
	
	
	public SchedulingUiTable() {
//		getDay2Coloumn.onBrowserEvent(context, elem, object, event);
	}
	
	
	@Override
	public void createTable() {
		getTimeColoumn();
		
		getDay1Coloumn();
		getDay2Coloumn();
		getDay3Coloumn();
		getDay4Coloumn();
		getDay5Coloumn();
		getDay6Coloumn();
		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);	
		
	}

	private void getTimeColoumn() {
		getTimeColoumn = new TextColumn<SchedulingUi>() {
			@Override
			public String getValue(SchedulingUi object) {
				return object.getTime()+"";
			}
		};
		table.addColumn(getTimeColoumn, "Time(In 24 Hours)");
		table.setColumnWidth(getTimeColoumn, "135px");
		
	}

	private void getDay1Coloumn() {
		EditTextCell editCell = new EditTextCell();
		getDay1Coloumn = new Column<SchedulingUi, String>(editCell) {
			@Override
			public String getValue(SchedulingUi object) {
				if (object.getDay1() != null) {
					return object.getDay1();
				} 
				return " ";
			}
		};
		table.addColumn(getDay1Coloumn, "Day1");
		table.setColumnWidth(getDay1Coloumn, "100px");
		
	}

	private void getDay2Coloumn() {
		TextCell editCell = new TextCell();
		getDay2Coloumn = new Column<SchedulingUi, String>(editCell) {
			@Override
			public String getValue(SchedulingUi object) {
				if (object.getDay2() != null) {
					return object.getDay2();
				} 
				return " ";
			}
			
			public void onBrowserEvent(final Event event) {
	            // TODO Auto-generated method stub
//	            super.onBrowserEvent(event);
	            if (DOM.eventGetType(event) == Event.ONCLICK) {
	                    System.out.println("event type -->> " + event.getType());
	            }

	        }

//			@Override
//			public void onBrowserEvent(Context context, Element elem,SchedulingUi object, NativeEvent event) {
//				// TODO Auto-generated method stub
//				super.onBrowserEvent(context, elem, object, event);
//				if ("click".equals(event.getType())) {
//	                //call your click event handler here
//					System.out.println("You clicked Day 2 Coloumn....!!!!");
//					
//	            }
////				if (DOM.eventGetType(event) == Event.ONCLICK) {
////                    System.out.println("event type -->> " + event.getType());
////				}
//			}
			
			
//			@Override
//	        public void onBrowserEvent(Context context, Element elem,SchedulingUi object, NativeEvent event) {
//	            super.onBrowserEvent(context, elem, object, event); 
//	            if ("click".equals(event.getType())) {
//	                //call your click event handler here
//	            }
//	        }
			
//			boolean isClick = "click".equals(event.getNativeEvent().getType());
		};
		table.addColumn(getDay2Coloumn, "Day2");
		table.setColumnWidth(getDay2Coloumn, "100px");
	}

	private void getDay3Coloumn() {
		TextCell editCell = new TextCell();
		getDay3Coloumn = new Column<SchedulingUi, String>(editCell) {
			@Override
			public String getValue(SchedulingUi object) {
				if (object.getDay3() != null) {
					return object.getDay3();
				} 
				return " ";
			}
		};
		table.addColumn(getDay3Coloumn, "Day3");
		table.setColumnWidth(getDay3Coloumn, "100px");
		
	}

	private void getDay4Coloumn() {
		TextCell editCell = new TextCell();
		getDay4Coloumn = new Column<SchedulingUi, String>(editCell) {
			@Override
			public String getValue(SchedulingUi object) {
				if (object.getDay4() != null) {
					return object.getDay4();
				} 
				return " ";
			}
		};
		table.addColumn(getDay4Coloumn, "Day4");
		table.setColumnWidth(getDay4Coloumn, "100px");
		
	}

	private void getDay5Coloumn() {
		TextCell editCell = new TextCell();
		getDay5Coloumn = new Column<SchedulingUi, String>(editCell) {
			@Override
			public String getValue(SchedulingUi object) {
				if (object.getDay5() != null) {
					return object.getDay5();
				} 
				return " ";
			}
		};
		table.addColumn(getDay5Coloumn, "Day5");
		table.setColumnWidth(getDay5Coloumn, "100px");
		
	}

	private void getDay6Coloumn() {
		TextCell editCell = new TextCell();
		getDay6Coloumn = new Column<SchedulingUi, String>(editCell) {
			@Override
			public String getValue(SchedulingUi object) {
				if (object.getDay6() != null) {
					return object.getDay6();
				} 
				return " ";
			}
		};
		table.addColumn(getDay6Coloumn, "Day6");
		table.setColumnWidth(getDay6Coloumn, "100px");
		
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	protected void createFieldUpdaterDay1Column() {
		getDay1Coloumn.setFieldUpdater(new FieldUpdater<SchedulingUi, String>() {
			@Override
			public void update(int index, SchedulingUi object,String value) {
				System.out.println("INSIDE UPDATOR METHOD..........");
				value="Day1";
				object.setDay1(value);
				table.redrawRow(index);
			}
		});
	}
	
	
	
	
	
	
	
	
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterDay1Column();
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
