package com.slicktechnologies.client.views.workorder;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.views.inventory.inspection.InspectionPresenter;
import com.slicktechnologies.shared.common.workorder.WorkOrderProductDetails;

public class WorkOrderTable extends SuperTable<WorkOrderProductDetails> implements ClickHandler {
	NumberFormat nf=NumberFormat.getFormat("0.00");
	TextColumn<WorkOrderProductDetails> compProdId;
	TextColumn<WorkOrderProductDetails> prodId;
	TextColumn<WorkOrderProductDetails> prodCode;
	TextColumn<WorkOrderProductDetails> prodName;
	TextColumn<WorkOrderProductDetails> prodCategory;
	TextColumn<WorkOrderProductDetails> prodQty;
	Column<WorkOrderProductDetails, String> prodQtyEdit;
	TextColumn<WorkOrderProductDetails> prodUOM;
	TextColumn<WorkOrderProductDetails> prodPrice;
	Column<WorkOrderProductDetails, String> deleteColumn;
	
	
	/**
	 * Date 22-01-2018 By vijay for product description poupup
	 */
	Column<WorkOrderProductDetails, String> btnProdDescription;
	TextColumn<WorkOrderProductDetails> getColumnProdDescription;
	ProductDescriptionPopUp prodDescripPopup = new ProductDescriptionPopUp();
	int rowindex=0;
	/**
	 * ends here
	 */
	
	public WorkOrderTable(){
		prodDescripPopup.getLblOk().addClickHandler(this);
		prodDescripPopup.getLblCancel().addClickHandler(this);
	}
	
	
	
	@Override
	public void createTable() {
		addColumnCompProdID();
		addColumnprodname();
		addColumnUnitOfMeasurement();
		addColumnQty();
		addColumnprodPrice();
		addColumnprodcategory();
		addColumnprodcode();
		addColumnprodID();
		addProdDescription();
		addProdDescriptionbtn();
		
		createColumndeleteColumn();
		addFieldUpdater();

	}

	public void addeditColumn() {
		addColumnCompProdID();
		addColumnprodID();
		addColumnprodcode();
		addColumnprodname();
		addColumnprodcategory();
		addColumnQty();
		addColumnUnitOfMeasurement();
		addColumnprodPrice();
		
		addProdDescription();
		addProdDescriptionbtn();
		
		createColumndeleteColumn();
		addFieldUpdater();

	}

	public void addViewColumn() {
		addColumnCompProdID();
		addColumnprodID();
		addColumnprodcode();
		addColumnprodname();
		addColumnprodcategory();
		addColumnviewQty();
		addColumnprodPrice();
		addColumnUnitOfMeasurement();
		addProdDescription();
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterprodQtyColumn();
		createFieldUpdaterDelete();
		createFieldUpdaterProdDescription();
		
	}
	
	/**
	 * Date 22-01-2018 By vijay for product description column and button column
	 */
	
	private void addProdDescription() {

		
		getColumnProdDescription = new TextColumn<WorkOrderProductDetails>() {
			
			@Override
			public String getValue(WorkOrderProductDetails object) {
				// TODO Auto-generated method stub
				if(object.getProdDescription()!=null)
				return object.getProdDescription();
				return "";
			}
		};
		table.addColumn(getColumnProdDescription, "Description");
	}

	private void addProdDescriptionbtn() {
		ButtonCell buttoncell = new ButtonCell();
		btnProdDescription = new Column<WorkOrderProductDetails, String>(buttoncell) {
			
			@Override
			public String getValue(WorkOrderProductDetails object) {
				// TODO Auto-generated method stub
				return "Description";
			}
		};
		table.addColumn(btnProdDescription," ");
	}
	
	private void createFieldUpdaterProdDescription() {

		btnProdDescription.setFieldUpdater(new FieldUpdater<WorkOrderProductDetails, String>() {
			
			@Override
			public void update(int index, WorkOrderProductDetails object, String value) {
				// TODO Auto-generated method stub
				prodDescripPopup.showPopUp();
				if(object.getProdDescription()!=null)
				prodDescripPopup.getTaProdDescription().setValue(object.getProdDescription());
				rowindex = index;
			}
		});
	}

	/**
	 * ends here
	 */
	
	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();
	}
	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void applyStyle() {
		
	}
	
/*********************************************Product View Column ********************************************/
	
	public void addColumnCompProdID() {
		compProdId = new TextColumn<WorkOrderProductDetails>() {

			@Override
			public String getValue(WorkOrderProductDetails object) {
				return object.getCompProdId() + "";
			}
		};
		table.addColumn(compProdId, "Composite Id");
	}
	
	public void addColumnprodID() {
		prodId = new TextColumn<WorkOrderProductDetails>() {

			@Override
			public String getValue(WorkOrderProductDetails object) {
				return object.getProductId() + "";
			}
		};
		table.addColumn(prodId, "ID");
	}
	
	public void addColumnprodcode() {
		prodCode = new TextColumn<WorkOrderProductDetails>() {

			@Override
			public String getValue(WorkOrderProductDetails object) {
				return object.getProductCode();
			}
		};
		table.addColumn(prodCode, "Code");
	}

	public void addColumnprodname() {
		prodName = new TextColumn<WorkOrderProductDetails>() {

			@Override
			public String getValue(WorkOrderProductDetails object) {
				return object.getProductName();
			}
		};
		table.addColumn(prodName, "Name");
	}
	
	public void addColumnprodcategory() {
		prodCategory = new TextColumn<WorkOrderProductDetails>() {

			@Override
			public String getValue(WorkOrderProductDetails object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(prodCategory, "Category");
	}
	
	
	//Read Only Quantity Column
	public void addColumnviewQty() {
		prodQty = new TextColumn<WorkOrderProductDetails>() {

			@Override
			public String getValue(WorkOrderProductDetails object) {
				return nf.format(object.getProductQty());
			}
		};
		table.addColumn(prodQty, "#Quantity");
	}
	
	//Editable Quantity Column
	public void addColumnQty() {
		EditTextCell editCell = new EditTextCell();
		prodQtyEdit = new Column<WorkOrderProductDetails, String>(editCell) {

			@Override
			public String getValue(WorkOrderProductDetails object) {
				return nf.format(object.getProductQty());
			}
		};
		table.addColumn(prodQtyEdit, "#Quantity");
	}
	
	protected void addColumnUnitOfMeasurement() {
		prodUOM = new TextColumn<WorkOrderProductDetails>() {

			@Override
			public String getValue(WorkOrderProductDetails object) {
				if (object.getProductUOM().equals(""))
					return "N/A";
				else
					return object.getProductUOM();
			}
		};
		table.addColumn(prodUOM, "UOM");
	}
	
	protected void addColumnprodPrice() {
		prodPrice = new TextColumn<WorkOrderProductDetails>() {

			@Override
			public String getValue(WorkOrderProductDetails object) {
					return object.getProductPrice()+"";
			}
		};
		table.addColumn(prodPrice, "Price");
	}

	public void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<WorkOrderProductDetails, String>(btnCell) {
			@Override
			public String getValue(WorkOrderProductDetails object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");

	}
	
	
	public void createFieldUpdaterDelete() {
		deleteColumn
				.setFieldUpdater(new FieldUpdater<WorkOrderProductDetails, String>() {
					@Override
					public void update(int index, WorkOrderProductDetails object,
							String value) {
						getDataprovider().getList().remove(object);

						table.redrawRow(index);
					}
				});

	}

	protected void createFieldUpdaterprodQtyColumn() {
		prodQtyEdit.setFieldUpdater(new FieldUpdater<WorkOrderProductDetails, String>() {
			@Override
			public void update(int index, WorkOrderProductDetails object, String value) {

				try {
					double val1 = Double.parseDouble(value.trim());
					object.setProductQty(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {

				}

				table.redrawRow(index);
			}
		});
	}
	

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource() == prodDescripPopup.getLblOk()){
			System.out.println("Text area value =="+prodDescripPopup.getTaProdDescription().getValue());
			if(!prodDescripPopup.getTaProdDescription().getValue().equals("")){
				this.getDataprovider().getList().get(rowindex).setProdDescription(prodDescripPopup.getTaProdDescription().getValue());
				this.table.redraw();
				prodDescripPopup.hidePopUp();
			}else {
				prodDescripPopup.showDialogMessage("Product Description can not be blank!");
			}
		}
		if(event.getSource() == prodDescripPopup.getLblCancel()){
			prodDescripPopup.hidePopUp();
		}
	}

}
