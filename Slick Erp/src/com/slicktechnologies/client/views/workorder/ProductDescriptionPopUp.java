package com.slicktechnologies.client.views.workorder;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class ProductDescriptionPopUp extends PopupScreen{

	TextArea taProdDescription;
	

	public ProductDescriptionPopUp(){
		super();
		createGui();
	}
	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
		taProdDescription = new TextArea();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingProductInformation=fbuilder.setlabel("Product Description Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Product Description",taProdDescription);
		FormField fprodDescription = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		FormField [][] formfield = {
				{fgroupingProductInformation},
				{fprodDescription}
		};
		
		this.fields = formfield;
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public TextArea getTaProdDescription() {
		return taProdDescription;
	}

	public void setTaProdDescription(TextArea taProdDescription) {
		this.taProdDescription = taProdDescription;
	}
	
	
	
	
}
