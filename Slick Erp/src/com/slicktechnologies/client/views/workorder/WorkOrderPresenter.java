package com.slicktechnologies.client.views.workorder;


import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.commons.io.input.BOMInputStream;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.complain.ServiceDateBoxPopup;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterial;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterialDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.workorder.WorkOrder;
import com.slicktechnologies.shared.common.workorder.WorkOrderProductDetails;

public class WorkOrderPresenter extends ApprovableFormScreenPresenter<WorkOrder> implements SelectionHandler<Suggestion>, ValueChangeHandler<String>, Handler,ClickHandler  {

	public  static WorkOrderForm form;
	final static  GenricServiceAsync async = GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	ArrayList<WorkOrderProductDetails> list;	
	ArrayList<WorkOrderProductDetails> materialList;
	
	/*************** vijay *************/
	ServiceDateBoxPopup completiondatepopup = new ServiceDateBoxPopup("Completion Date", "Remark");
	PopupPanel markpanel=new PopupPanel(true);
	
	ConditionDialogBox conditionPopup=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
	PopupPanel preprintPanel;
	
	public WorkOrderPresenter(FormScreen<WorkOrder> view, WorkOrder model) {
		super(view, model);
		form = (WorkOrderForm) view;
		form.prodInfoComp.getProdID().addSelectionHandler(this);
		form.prodInfoComp.getProdName().addSelectionHandler(this);
		form.prodInfoComp.getProdCode().addSelectionHandler(this);
		form.getIbOrderId().addValueChangeHandler(this);
		form.getBomTable().getTable().addRowCountChangeHandler(this);
		
		completiondatepopup.getBtnOne().addClickHandler(this);
		completiondatepopup.getBtnTwo().addClickHandler(this);
		
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.WORKORDER,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	public static WorkOrderForm initialize() {
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Work Order",Screen.WORKORDER);
		WorkOrderForm form=new  WorkOrderForm();

		WorkOrderTableProxy gentable=new WorkOrderTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		
		WorkOrderSearchProxy.staticSuperTable=gentable;
		WorkOrderSearchProxy searchpopup=new WorkOrderSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		WorkOrderPresenter  presenter=new WorkOrderPresenter(form,new WorkOrder());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		super.reactToProcessBarEvents(e);
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		if (text.equals(AppConstants.CANCEL))
			reactToCancel();
		if (text.equals(AppConstants.NEW))
			if(form.isPopUpAppMenubar()){
				Console.log("Work Order POPUP : New clicked!");
				return;
			}
			reactToNew();
		if (text.equals("DownLoad"))
			reactOnDownload();
		if (text.equals("Email"))
			reactOnEmail();
		if (text.equals("Print"))
			reactOnPrint();
		
		if (text.equals("Mark Completed")) {
			markpanel = new PopupPanel(true);
			markpanel.add(completiondatepopup);
			markpanel.center();
		}
		
		/**
		 * Date :- 15-01-2018 By Vijay
		 * for delivery note print
		 * Client :- Versatile
		 */
		if(text.equals(AppConstants.PRINTDELIVERYNOTE)){
			ReactOnPrintDeliveryNote();
		}
		/**
		 * ends here
		 */
	}
	
	
	/**
	 * Date :- 15-01-2018 By Vijay
	 * for delivery note print based on work order
	 * Client :- Versatile
	 */
	private void ReactOnPrintDeliveryNote() {

		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("WorkOrder", "CompanyAsLetterHead")){
			preprintPanel = new PopupPanel();
			preprintPanel.add(conditionPopup);
			preprintPanel.setGlassEnabled(true);
			preprintPanel.show();
			preprintPanel.center();
		}else{
			final String url = GWT.getModuleBaseURL() + "deliverynotepdf"+"?Id="+model.getId()+"&"+"preprint="+"plane"+"&"+"type=workOrderDeliveryNote";
			Window.open(url, "test", "enabled");
		}
	}
	/**
	 * ends here
	 */
	
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource()==completiondatepopup.getBtnOne()){
			if(!completiondatepopup.getTaDesc().getValue().equals("")){
				form.tbRemark.setValue(completiondatepopup.getTaDesc().getValue());
			}
			if(completiondatepopup.getServiceDate().getValue()!=null){
				form.dbWoCompletionDateOne.setValue(completiondatepopup.getServiceDate().getValue());
			}
			
			reactOnComplete();
			markpanel.hide();
		}
		
		if(event.getSource()==completiondatepopup.getBtnTwo()){
			markpanel.hide();
		}
		

		/**
		 * Date :- 15-01-2018 By Vijay
		 * for delivery note print based on work order
		 * Client Name :- Versatile
		 */
		if(event.getSource() == conditionPopup.getBtnOne()){

			final String url = GWT.getModuleBaseURL() + "deliverynotepdf"+"?Id="+model.getId()+"&"+"preprint="+"yes"+"&"+"type=workOrderDeliveryNote";
			Window.open(url, "test", "enabled");
			
			preprintPanel.hide();
		}
		if(event.getSource() == conditionPopup.getBtnTwo()){
			
			final String url = GWT.getModuleBaseURL() + "deliverynotepdf"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"type=workOrderDeliveryNote";
			Window.open(url, "test", "enabled");
		    
			preprintPanel.hide();
		}
		/**
		 * ends here
		 */
			
			
	}
	
	private void reactOnComplete() {
		// TODO Auto-generated method stub
		
		model.setStatus("Completed");
		
		if(!completiondatepopup.getTaDesc().getValue().equals("")){
			model.setRemark(completiondatepopup.getTaDesc().getValue());
		}
		
		if(completiondatepopup.getServiceDate().getValue()!=null){
			model.setCompletionDateone(completiondatepopup.getServiceDate().getValue());
		}
		
		form.showWaitSymbol();
		
		async.save(model,new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				
				form.hideWaitSymbol();
				form.showDialogMessage("Failed To Mark Complete !");
				
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				form.hideWaitSymbol();
				form.showDialogMessage("Marked Completed !");
				form.tbWoStatus.setValue("Completed");
				form.setMenuAsPerStatus();
			}
		});		
		
	}

	@Override
	public void reactOnPrint() {
		
		System.out.println("in side reacton print ");
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("WorkOrder");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}			

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				int cnt=0;
				System.out.println(" result set size +++++++"+result.size());
				
				List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
				
				if(result.size()==0){
					
					final String url = GWT.getModuleBaseURL() + "pdfworkorder" + "?Id="+ model.getId();
					Window.open(url, "test", "enabled");
				}
				else{
				
					for(SuperModel model:result)
					{
						ProcessConfiguration processConfig=(ProcessConfiguration)model;
						processList.addAll(processConfig.getProcessList());
						
					}
//				
				for(int k=0;k<processList.size();k++){	
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("InstallationPdf")&&processList.get(k).isStatus()==true){
					
					cnt=cnt+1;
				
				}
				}
				}
				
				if(cnt >0)
				{
					System.out.println("in side cnt >0");
					final String url = GWT.getModuleBaseURL() + "pdfrep" + "?Id="+ model.getId();
					Window.open(url, "test", "enabled");
				}
				else
				{
					System.out.println("in side else ");
					final String url = GWT.getModuleBaseURL() + "pdfworkorder" + "?Id="+ model.getId();
					Window.open(url, "test", "enabled");
				}
				
				
			}
		});
	}

	@Override
	public void reactOnEmail() {
		boolean conf = Window.confirm("Do you really want to send email?");
		if (conf == true) {
			emailService.initiateWorkOrderEmail((WorkOrder) model,new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Resource Quota Ended ");
					caught.printStackTrace();
				}

				@Override
				public void onSuccess(Void result) {
					Window.alert("Email Sent Sucessfully !");
				}
			});
		}
	}

	@Override
	protected void makeNewModel() {
		model = new WorkOrder();
	}
	
	private void reactToNew() {
		form.setToNewState();
		initialize();
	}
	
	@Override
	public void reactOnDownload() {
		ArrayList<WorkOrder> woArray = new ArrayList<WorkOrder>();
		List<WorkOrder> list = (List<WorkOrder>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		woArray.addAll(list);
		csvservice.setWorkOrderDetails(woArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 86;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	private void reactToCancel() {
		model.setStatus(WorkOrder.CANCELLED);
		changeStatus("Failed To Cancel Work Order!", "Work Order Cancelled !");
	}

	private void changeStatus(final String failureMessage,final String successMessage) {
		GenricServiceAsync async = GWT.create(GenricService.class);
		form.showWaitSymbol();
		async.save(model, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onSuccess(ReturnFromServer result) {
				form.setAppHeaderBarAsPerStatus();
				form.setMenuAsPerStatus();
				form.showDialogMessage(successMessage);
				form.getTbWoStatus().setText(WorkOrder.CANCELLED);
				form.hideWaitSymbol();
			}

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage(failureMessage);
				form.hideWaitSymbol();
			}
		});
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
//		if(form.getTable().getDataprovider().getList().size()!=0){
//			form.getTable().connectToLocal();
//		}
	}
	
	public void setDataFromSo(){
		form.getDbOrderDate().setValue(null);
		form.getTbRefNumId().setValue(null);
		form.getDbRefDate().setValue(null);
		form.getOblProject().setSelectedIndex(0);
		form.getOblSalesPerson().setSelectedIndex(0);
		
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		
		Filter filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(form.getIbOrderId().getValue()));
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SalesOrder());
		
		form.showWaitSymbol();
		
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							SalesOrder so=(SalesOrder) result.get(0);
							if(so.getStatus().equals("Approved"))
							{
								form.bomTable.getDataprovider().setList(so.getItems());
								form.getProdInfoComp().setEnable(false);
								form.getDbOrderDate().setValue(so.getCreationDate());
								form.getDbOrderDeliveryDate().setValue(so.getDeliveryDate());

								if(so.getRefNo()!=null){
									form.getTbRefNumId().setValue(so.getRefNo()+"");
									form.getTbRefNumId().setEnabled(false);
								}
								if(so.getReferenceDate()!=null){
									form.getDbRefDate().setValue(so.getReferenceDate());
									form.getDbRefDate().setEnabled(false);
								}
								
								if(so.getBranch()!=null){
									form.getOblWoBranch().setValue(so.getBranch());
									form.getOblWoBranch().setEnabled(false);
								}
								
								if(so.getEmployee()!=null){
									form.getOblSalesPerson().setValue(so.getEmployee());
									form.getOblSalesPerson().setEnabled(false);
								}
								
								if (so.getApproverName()!= null) {
									form.getOblWoApproverName().setValue(so.getApproverName());
									form.getOblWoApproverName().setEnabled(false);
								}
								form.getLbComProdId().clear();
								form.getLbComProdId().addItem("--SELECT--");
								for(int i=0;i<so.getItems().size();i++){
									form.flage=true;
									form.getBillOfProductMaterial(so.getItems().get(i).getPrduct().getCount(),so.getItems().get(i).getProductName(),so.getItems().get(i).getQty(),false);
									form.getLbComProdId().addItem(so.getItems().get(i).getPrduct().getCount()+"");
								}
								
							}
							else{
								form.showDialogMessage("SO ID is Not Approved");
								form.setToNewState();
								form.getBomTable().clear();
								form.getTable().clear();
							}
							form.hideWaitSymbol();
						}
						else
						{
//							form.showDialogMessage("Sales Order with this number doesnt exist, Please recheck.");
//							form.setToNewState();
							form.getDbOrderDate().setValue(null);
							form.getTbRefNumId().setValue(null);
							form.getDbRefDate().setValue(null);
							form.getOblProject().setSelectedIndex(0);
							form.getOblSalesPerson().setSelectedIndex(0);
							form.getBomTable().clear();
							form.getTable().clear();
							form.hideWaitSymbol();
							setDataFromContract();
						}
					}

					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
	}

	
	public void setDataFromContract(){
		
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		
		Filter filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(form.getIbOrderId().getValue()));
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
//		form.setToNewState();
		form.showWaitSymbol();
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					Contract so=(Contract) result.get(0);
					if(so.getStatus().equals("Approved"))
					{
						form.bomTable.getDataprovider().setList(so.getItems());
						form.getProdInfoComp().setEnable(false);
						form.getDbOrderDate().setValue(so.getCreationDate());

						if(so.getRefNo()!=null){
							form.getTbRefNumId().setValue(so.getRefNo());
							form.getTbRefNumId().setEnabled(false);
						}
						if(so.getRefDate()!=null){
							form.getDbRefDate().setValue(so.getRefDate());
							form.getDbRefDate().setEnabled(false);
						}
								
						if(so.getBranch()!=null){
							form.getOblWoBranch().setValue(so.getBranch());
							form.getOblWoBranch().setEnabled(false);
						}
								
						if(so.getEmployee()!=null){
							form.getOblSalesPerson().setValue(so.getEmployee());
							form.getOblSalesPerson().setEnabled(false);
						}
								
						if (so.getApproverName()!= null) {
							form.getOblWoApproverName().setValue(so.getApproverName());
							form.getOblWoApproverName().setEnabled(false);
						}
								
						form.getLbComProdId().clear();
						form.getLbComProdId().addItem("--SELECT--");
						for(int i=0;i<so.getItems().size();i++){
							form.flage=true;
							form.getBillOfProductMaterial(so.getItems().get(i).getPrduct().getCount(),so.getItems().get(i).getProductName(),so.getItems().get(i).getQty(),false);
							form.getLbComProdId().addItem(so.getItems().get(i).getPrduct().getCount()+"");
						}
								
					}
					else{
						form.showDialogMessage("Contract ID is Not Approved");
						form.setToNewState();
						form.getBomTable().clear();
						form.getTable().clear();
					}
					form.hideWaitSymbol();
				}
				else
				{
					form.showDialogMessage("Contract with this number doesnt exist, Please recheck.");
					form.setToNewState();
					form.getBomTable().clear();
					form.getTable().clear();
					form.hideWaitSymbol();
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
		});
	}
	
	
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		if(event.getSource().equals(form.getIbOrderId()))
			setDataFromSo();
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		if(event.getSource()==form.getBomTable().getTable()){
			if(form.bomTable.getDataprovider().getList().size()==0){
				form.table.connectToLocal();
				form.lbComProdId.clear();
				form.lbComProdId.addItem("--SELECT--");
				form.flage=false;
			}
			if(form.bomTable.getDataprovider().getList().size()!=0){
				if(form.flage!=true){
					System.out.println("Inside rw chng flage false");
					list=new ArrayList<WorkOrderProductDetails>();
					materialList=new ArrayList<WorkOrderProductDetails>();
					materialList.addAll(form.table.getDataprovider().getList());
					form.lbComProdId.clear();
					form.lbComProdId.addItem("--SELECT--");
					form.table.connectToLocal();
					for(int i=0;i<form.bomTable.getDataprovider().getList().size();i++){
						
						updateBillOfMaterialProduct(form.bomTable.getDataprovider().getList().get(i).getPrduct().getCount(),form.bomTable.getDataprovider().getList().get(i).getQty());
					}
					for(int i=0;i<form.bomTable.getDataprovider().getList().size();i++){
						form.lbComProdId.addItem(form.bomTable.getDataprovider().getList().get(i).getPrduct().getCount()+"");
					}
				}
				form.flage=false;
			}
		}
	}
	
	private void updateBillOfMaterialProduct(final int prodId,final double prodQty){
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = new Filter();
		filter.setQuerryString("bomProductId");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillOfProductMaterial());
		view.showWaitSymbol();
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						if (result.size() != 0) {
//							form.table.connectToLocal();
							for (SuperModel model : result) {
								BillOfProductMaterial entity = (BillOfProductMaterial) model;
								list=woUpdatedProductList(entity.getBomProductList(), prodId, prodQty);
							}
	
//							form.table.getDataprovider().setList(list);
						}else{
							System.out.println("ProdId :: "+prodId +" Prod Qty :: "+prodQty);
							System.out.println("Material List Size :: "+materialList.size());
							for(int i=0;i<materialList.size();i++){
								if(prodId==materialList.get(i).getCompProdId()){
									WorkOrderProductDetails wopd=new WorkOrderProductDetails();
									wopd.setCompProdId(materialList.get(i).getCompProdId());
									wopd.setProductId(materialList.get(i).getProductId());
									wopd.setProductCode(materialList.get(i).getProductCode());
									wopd.setProductName(materialList.get(i).getProductName());
									wopd.setProductCategory(materialList.get(i).getProductCategory());
									wopd.setProductQty(1*prodQty);
									wopd.setProductPrice(materialList.get(i).getProductPrice());
									wopd.setProductUOM(materialList.get(i).getProductUOM());
//									list.add(wopd);
									form.table.getDataprovider().getList().add(wopd);
								}
							}
								
						}
						
					}
					@Override
					public void onFailure(Throwable caught) {
						view.hideWaitSymbol();
					}
		});
		view.hideWaitSymbol();
	}
		
	
	
	public ArrayList<WorkOrderProductDetails> woUpdatedProductList(List<BillOfProductMaterialDetails> bomList,int prodId,double prodQty){
		
		ArrayList<WorkOrderProductDetails> updatedWoList=new ArrayList<WorkOrderProductDetails>();
		ArrayList<WorkOrderProductDetails> woList=new ArrayList<WorkOrderProductDetails>();
		
		for(int i=0;i<materialList.size();i++){
			if(prodId==materialList.get(i).getCompProdId()){
				woList.add(materialList.get(i));
			}
		}
		if(bomList.size()==woList.size()){
			for(int i=0;i<bomList.size();i++){
			
				WorkOrderProductDetails wopd=new WorkOrderProductDetails();
				wopd.setCompProdId(bomList.get(i).getCompProdId());
				wopd.setProductId(bomList.get(i).getProductId());
				wopd.setProductCode(bomList.get(i).getProductCode());
				wopd.setProductName(bomList.get(i).getProductName());
				wopd.setProductCategory(bomList.get(i).getProductCategory());
				wopd.setProductQty(bomList.get(i).getProductQty()*prodQty);
				wopd.setProductPrice(bomList.get(i).getProductPrice());
				wopd.setProductUOM(bomList.get(i).getProductUOM());
				form.table.getDataprovider().getList().add(wopd);
				updatedWoList.add(wopd);
			}
			
		}else if(woList.size()<bomList.size()){
			for(int i=0;i<woList.size();i++){
				for(int j=0;j<bomList.size();j++){
					if(woList.get(i).getProductId()==bomList.get(j).getProductId()){
						WorkOrderProductDetails wopd=new WorkOrderProductDetails();
						wopd.setCompProdId(bomList.get(j).getCompProdId());
						wopd.setProductId(bomList.get(j).getProductId());
						wopd.setProductCode(bomList.get(j).getProductCode());
						wopd.setProductName(bomList.get(j).getProductName());
						wopd.setProductCategory(bomList.get(j).getProductCategory());
						wopd.setProductQty(bomList.get(j).getProductQty()*prodQty);
						wopd.setProductPrice(bomList.get(j).getProductPrice());
						wopd.setProductUOM(bomList.get(j).getProductUOM());
						form.table.getDataprovider().getList().add(wopd);
						updatedWoList.add(wopd);
					}
				}
			}
		}else{
			ArrayList<Integer> woProdList =new ArrayList<Integer>();
			for(int i=0;i<woList.size();i++){
				woProdList.add(woList.get(i).getProductId());
			}
			
			ArrayList<Integer> bomProdList =new ArrayList<Integer>();
			for(int i=0;i<bomList.size();i++){
				bomProdList.add(bomList.get(i).getProductId());
			}

			ArrayList<Integer> uncommonProdList=new ArrayList<Integer>();
			
			for(Integer id:woProdList){
				if(!bomProdList.contains(id)){
					uncommonProdList.add(id);
				}
			}
			
			for(Integer id:bomProdList){
				if(!woProdList.contains(id)){
					uncommonProdList.add(id);
				}
			}
			
			System.out.println("Uncommon List Size ::: "+uncommonProdList.size());
			
			for(int i=0;i<woList.size();i++){
				for(int j=0;j<bomList.size();j++){
					if(woList.get(i).getProductId()==bomList.get(j).getProductId()){
						WorkOrderProductDetails wopd=new WorkOrderProductDetails();
						wopd.setCompProdId(bomList.get(j).getCompProdId());
						wopd.setProductId(bomList.get(j).getProductId());
						wopd.setProductCode(bomList.get(j).getProductCode());
						wopd.setProductName(bomList.get(j).getProductName());
						wopd.setProductCategory(bomList.get(j).getProductCategory());
						wopd.setProductQty(bomList.get(j).getProductQty()*prodQty);
						wopd.setProductPrice(bomList.get(j).getProductPrice());
						wopd.setProductUOM(bomList.get(j).getProductUOM());
						form.table.getDataprovider().getList().add(wopd);
						updatedWoList.add(wopd);
					}
				}
			}
			
			
			for(int i=0;i<uncommonProdList.size();i++){
				for(int j=0;j<woList.size();j++){
					if(uncommonProdList.get(i)==woList.get(j).getProductId()){
						WorkOrderProductDetails wopd=new WorkOrderProductDetails();
						wopd.setCompProdId(woList.get(j).getCompProdId());
						wopd.setProductId(woList.get(j).getProductId());
						wopd.setProductCode(woList.get(j).getProductCode());
						wopd.setProductName(woList.get(j).getProductName());
						wopd.setProductCategory(woList.get(j).getProductCategory());
						wopd.setProductQty(1*prodQty);
						wopd.setProductPrice(woList.get(j).getProductPrice());
						wopd.setProductUOM(woList.get(j).getProductUOM());
						form.table.getDataprovider().getList().add(wopd);
						updatedWoList.add(wopd);
					}
				}
			}
		}
		
		return updatedWoList;
	}

	
	

}
