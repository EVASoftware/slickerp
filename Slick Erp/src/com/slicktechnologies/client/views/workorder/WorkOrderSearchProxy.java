package com.slicktechnologies.client.views.workorder;

import java.util.Vector;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

public class WorkOrderSearchProxy extends SearchPopUpScreen<WorkOrder>{

	TextBox ibOrderId;
	ObjectListBox<HrProject> oblProject;
	ObjectListBox<Employee> oblSalesPerson;
	public DateComparator dateComparator;
	TextBox ibWoId;
	TextBox tbWoTitle;
	
	ObjectListBox<Config> oblWoGroup;
	ObjectListBox<ConfigCategory> oblWoCategory;
	ObjectListBox<Type> oblWoType;
	
	ObjectListBox<Branch> oblWoBranch;
	ObjectListBox<Employee> oblWoApproverName;
	ObjectListBox<Employee> oblWoPersonRes;
	
	ListBox tbWoStatus;
	ProductInfoComposite prodInfoComp;
	
	public WorkOrderSearchProxy() {
		super();
		createGui();
	}
	
	private void initializeWidget(){
		ibOrderId=new TextBox();
		ibWoId=new TextBox();
		tbWoTitle=new TextBox();
		dateComparator = new DateComparator(new MaterialRequestNote());
		oblWoGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblWoGroup,Screen.WORKORDERGROUP);
		oblWoCategory= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblWoCategory, Screen.WORKORDERCATEGORY);
		oblWoType = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(oblWoType,Screen.WORKORDERTYPE);
		
		oblWoBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(oblWoBranch);
		
		oblSalesPerson = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblSalesPerson);
		oblSalesPerson.makeEmployeeLive(AppConstants.SALESMODULE, AppConstants.WORK_ORDER, "Sales Person");
		
		oblWoApproverName = new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(oblWoApproverName);
		
		oblWoPersonRes = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblWoPersonRes);
		oblWoPersonRes.makeEmployeeLive(AppConstants.SALESMODULE, AppConstants.WORK_ORDER, "Person Responsible");
		
		oblProject = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(oblProject);
		
		tbWoStatus=new ListBox();
		AppUtility.setStatusListBox(tbWoStatus,WorkOrder.getStatusList());

		prodInfoComp = AppUtility.initiatePurchaseProductComposite(new SuperProduct());
//		prodInfoComp = AppUtility.initiateSalesProductComposite(new SuperProduct());
	}
	
	@Override
	public void createScreen() {
		initializeWidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder("Order Id", ibOrderId);
		FormField fibOrderIdId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project", oblProject);
		FormField foblWoProject = fbuilder.setMandatory(false).setMandatoryMsg("Project is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Sales Person",oblSalesPerson );
		FormField foblWoSalesPerson = fbuilder.setMandatory(false).setMandatoryMsg("Sales Person is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Work Order ID", ibWoId);
		FormField fibWoId= fbuilder.setMandatory(false).setMandatoryMsg("Work Order ID is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Work Order Title", tbWoTitle);
		FormField ftbWoTitle = fbuilder.setMandatory(false).setMandatoryMsg("Work Order Title is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Work Order From Date", dateComparator.getFromDate());
		FormField fdbWoDate = fbuilder.setMandatory(false).setMandatoryMsg("Work Order Date is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Work Order To Date", dateComparator.getToDate());
		FormField fdbtoWoDate = fbuilder.setMandatory(false).setMandatoryMsg("Work Order Date is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Work Order Group", oblWoGroup);
		FormField foblWoGroup = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Work Order Category", oblWoCategory);
		FormField foblWoCategory = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Work Order Type", oblWoType);
		FormField foblWoType = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Branch", oblWoBranch);
		FormField foblWoBranch = fbuilder.setMandatory(false).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Approver", oblWoApproverName);
		FormField foblWoApproverName = fbuilder.setMandatory(false).setMandatoryMsg("Approver is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Person Responsible", oblWoPersonRes);
		FormField foblWoPersonResp = fbuilder.setMandatory(false).setMandatoryMsg("Person Responsible is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", tbWoStatus);
		FormField ftbWoStatus = fbuilder.setMandatory(false).setMandatoryMsg("Status is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", prodInfoComp);
		FormField fprodInfoComp= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();
		
		
		FormField[][] formfield = { 
				{ fibWoId,ftbWoTitle, fdbWoDate,fdbtoWoDate},
				{ foblWoGroup,foblWoCategory,foblWoType,foblWoBranch},
				{ foblWoPersonResp,foblWoApproverName,foblWoProject,ftbWoStatus},
				{ fibOrderIdId,foblWoSalesPerson},
				{ fprodInfoComp},
		};
		this.fields = formfield;
		
	}
	
	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		if (dateComparator.getValue() != null) {
			filtervec.addAll(dateComparator.getValue());
		}

		if (oblWoBranch.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblWoBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		if (oblWoCategory.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblWoCategory.getValue().trim());
			temp.setQuerryString("category");
			filtervec.add(temp);
		}
		if (oblWoType.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblWoType.getValue().trim());
			temp.setQuerryString("type");
			filtervec.add(temp);
		}
		
		if (oblProject.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblProject.getValue().trim());
			temp.setQuerryString("woProject");
			filtervec.add(temp);
		}
		if (oblWoApproverName.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblWoApproverName.getValue().trim());
			temp.setQuerryString("approverName");
			filtervec.add(temp);
		}
		if (oblSalesPerson.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblSalesPerson.getValue().trim());
			temp.setQuerryString("salesPerson");
			filtervec.add(temp);
		}
		if (oblWoPersonRes.getSelectedIndex() != 0) {
			temp = new Filter();
			temp.setStringValue(oblWoPersonRes.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		if (!ibOrderId.getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(ibOrderId.getValue()));
			temp.setQuerryString("orderId");
			filtervec.add(temp);
		}
		if (!ibWoId.getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(ibWoId.getValue()));
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if (tbWoStatus.getSelectedIndex()!=0) {
			temp = new Filter();
			temp.setStringValue(tbWoStatus.getValue(tbWoStatus.getSelectedIndex()));
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		if (!tbWoTitle.getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(tbWoTitle.getValue());
			temp.setQuerryString("woTitle");
			filtervec.add(temp);
		}
		
		if (!prodInfoComp.getProdCode().getValue().equals("")) {
			temp = new Filter();
			temp.setStringValue(prodInfoComp.getProdCode().getValue());
			temp.setQuerryString("bomTable.itemProduct.productCode");
			filtervec.add(temp);
		}

		if (!prodInfoComp.getProdID().getValue().equals("")) {
			temp = new Filter();
			temp.setIntValue(Integer.parseInt(prodInfoComp.getProdID().getValue()));
			temp.setQuerryString("bomTable.itemProduct.count");
			filtervec.add(temp);
		}

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new WorkOrder());
		return querry;
	}

	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(oblWoBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}

}
