package com.slicktechnologies.client.views.workorder;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

public class WorkOrderTableProxy extends SuperTable<WorkOrder> {

	TextColumn<WorkOrder> getColumnWoId;
	Column<WorkOrder, Date> getColumnWoDate;
	TextColumn<WorkOrder> getColumnWoTitle;
	TextColumn<WorkOrder> getColumnWoCategory;
	TextColumn<WorkOrder> getColumnWoGroup;
	TextColumn<WorkOrder> getColumnWoType;
	TextColumn<WorkOrder> getColumnOrderId;
	TextColumn<WorkOrder> getColumnWoProject;
	TextColumn<WorkOrder> getColumnWoBranch;
	TextColumn<WorkOrder> getColumnWoSalesPerson;
	TextColumn<WorkOrder> getColumnWoPersonResponsible;
	TextColumn<WorkOrder> getColumnWoApprover;
	TextColumn<WorkOrder> getColumnWoStatus;
	
	public WorkOrderTableProxy() {
		super();
	}
	
	@Override
	public void createTable() {
		addColumnWoId();
		addColumnWoTitle();
		addColumnWoDate();
		addColumnOrderId();
		addColumnWoGroup();
		addColumnWoCategory();
		addColumnWoType();
		addColumnWoPersonResponsible();
		addColumnWoApprover();
		addColumnWoBranch();
		addColumnWoStatus();
		addColumnWoProject();
		addColumnWoSalesPerson();
	}
	
	private void addColumnWoId() {
		getColumnWoId = new TextColumn<WorkOrder>() {
			@Override
			public String getValue(WorkOrder object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getColumnWoId, "Id");
		table.setColumnWidth(getColumnWoId,90,Unit.PX);
		getColumnWoId.setSortable(true);
	}

	private void addColumnWoDate() {
		DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		getColumnWoDate = new Column<WorkOrder, Date>(date) {
			@Override
			public Date getValue(WorkOrder object) {
				if (object.getCreationDate()!= null) {
					return object.getCreationDate();
				}
				return null;
			}
		};
		table.addColumn(getColumnWoDate, "Date");
		table.setColumnWidth(getColumnWoDate,120,Unit.PX);
		getColumnWoDate.setSortable(true);
	}
	
	private void addColumnWoTitle() {
		getColumnWoTitle = new TextColumn<WorkOrder>() {
			@Override
			public String getValue(WorkOrder object) {
				if (object.getWoTitle()!=null){
					return object.getWoTitle();
				}
				return null;
			}
		};
		table.addColumn(getColumnWoTitle, "Title");
		table.setColumnWidth(getColumnWoTitle,90,Unit.PX);
		getColumnWoTitle.setSortable(true);
	}
	
	private void addColumnWoGroup() {
		getColumnWoGroup = new TextColumn<WorkOrder>() {
			@Override
			public String getValue(WorkOrder object) {
					return object.getGroup();
			}
		};
		table.addColumn(getColumnWoGroup, "Group");
		table.setColumnWidth(getColumnWoGroup,90,Unit.PX);
		getColumnWoGroup.setSortable(true);
	}

	private void addColumnWoCategory() {
		getColumnWoCategory = new TextColumn<WorkOrder>() {
			@Override
			public String getValue(WorkOrder object) {
					return object.getCategory();
			}
		};
		table.addColumn(getColumnWoCategory, "Category");
		table.setColumnWidth(getColumnWoCategory,90,Unit.PX);
		getColumnWoCategory.setSortable(true);
	}

	private void addColumnWoType() {
		getColumnWoType = new TextColumn<WorkOrder>() {
			@Override
			public String getValue(WorkOrder object) {
				if (object.getType()!=null){
					return object.getType();
				}
				return null;
			}
		};
		table.addColumn(getColumnWoType, "Type");
		table.setColumnWidth(getColumnWoType,90,Unit.PX);
		getColumnWoType.setSortable(true);
	}

	
	private void addColumnOrderId() {
		getColumnOrderId = new TextColumn<WorkOrder>() {
			@Override
			public String getValue(WorkOrder object) {
				if (object.getOrderId() == -1)
					return "N.A";
				else
					return object.getOrderId() + "";
			}
		};
		table.addColumn(getColumnOrderId, "Order ID");
		table.setColumnWidth(getColumnOrderId,90,Unit.PX);
		getColumnOrderId.setSortable(true);
	}

	private void addColumnWoProject() {
		getColumnWoProject = new TextColumn<WorkOrder>() {
			@Override
			public String getValue(WorkOrder object) {
				if (object.getWoProject()!=null){
					return object.getWoProject();
				}
				return null;
			}
		};
		table.addColumn(getColumnWoProject, "Project");
		table.setColumnWidth(getColumnWoProject,90,Unit.PX);
		getColumnWoProject.setSortable(true);
	}

	private void addColumnWoBranch() {
		getColumnWoBranch = new TextColumn<WorkOrder>() {
			@Override
			public String getValue(WorkOrder object) {
				if (object.getBranch()!=null){
					return object.getBranch();
				}
				return null;
			}
		};
		table.addColumn(getColumnWoBranch, "Branch");
		table.setColumnWidth(getColumnWoBranch,90,Unit.PX);
		getColumnWoBranch.setSortable(true);
	}

	private void addColumnWoSalesPerson() {
		getColumnWoSalesPerson = new TextColumn<WorkOrder>() {
			@Override
			public String getValue(WorkOrder object) {
				if (object.getSalesPerson()!=null){
					return object.getSalesPerson();
				}
				return null;
			}
		};
		table.addColumn(getColumnWoSalesPerson, "Sales Person");
		table.setColumnWidth(getColumnWoSalesPerson,117,Unit.PX);
		getColumnWoSalesPerson.setSortable(true);
	}

	private void addColumnWoPersonResponsible() {
		getColumnWoPersonResponsible = new TextColumn<WorkOrder>() {
			@Override
			public String getValue(WorkOrder object) {
				if (object.getEmployee()!=null){
					return object.getEmployee();
				}
				return null;
			}
		};
		table.addColumn(getColumnWoPersonResponsible, "Person Responsible");
		table.setColumnWidth(getColumnWoPersonResponsible,140,Unit.PX);
		getColumnWoPersonResponsible.setSortable(true);
	}

	private void addColumnWoApprover() {
		getColumnWoApprover = new TextColumn<WorkOrder>() {
			@Override
			public String getValue(WorkOrder object) {
				if (object.getApproverName()!=null){
					return object.getApproverName();
				}
				return null;
			}
		};
		table.addColumn(getColumnWoApprover, "Approver");
		table.setColumnWidth(getColumnWoApprover,90,Unit.PX);
		getColumnWoApprover.setSortable(true);
	}

	private void addColumnWoStatus() {
		getColumnWoStatus = new TextColumn<WorkOrder>() {
			@Override
			public String getValue(WorkOrder object) {
				if (object.getStatus()!=null){
					return object.getStatus();
				}
				return null;
			}
		};
		table.addColumn(getColumnWoStatus, "Status");
		table.setColumnWidth(getColumnWoStatus,90,Unit.PX);
		getColumnWoStatus.setSortable(true);
	}

	public void addColumnSorting() {
		addColumnSortingWoId();
		addColumnSortingWoDate();
		addColumnSortingWoTitle();
		addColumnSortingWoGroup();
		addColumnSortingWoCategory();
		addColumnSortingWoType();
		addColumnSortingWoProject();
		addColumnSortingWoBranch();
		addColumnSortingWoSalesPerson();
		addColumnSortingWoRequestedBy();
		addColumnSortingWoApprover();
		addColumnSortingOrderId();
		addColumnSortingWoStatus();
	}	

	/***********************************************Column Sorting***************************************/	
	private void addColumnSortingWoId() {
		List<WorkOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<WorkOrder>(list);
		columnSort.setComparator(getColumnWoId, new Comparator<WorkOrder>() {
			@Override
			public int compare(WorkOrder e1, WorkOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingWoDate() {
		List<WorkOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<WorkOrder>(list);
		columnSort.setComparator(getColumnWoDate, new Comparator<WorkOrder>() {
			@Override
			public int compare(WorkOrder e1, WorkOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCreationDate() != null && e2.getCreationDate() != null) {
						return e1.getCreationDate().compareTo(e2.getCreationDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnSortingOrderId() {
		List<WorkOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<WorkOrder>(list);
		columnSort.setComparator(getColumnOrderId, new Comparator<WorkOrder>() {
			@Override
			public int compare(WorkOrder e1, WorkOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getOrderId() == e2.getOrderId()) {
						return 0;
					}
					if (e1.getOrderId() > e2.getOrderId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingWoTitle() {
		List<WorkOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<WorkOrder>(list);
		columnSort.setComparator(getColumnWoTitle, new Comparator<WorkOrder>() {
			@Override
			public int compare(WorkOrder e1, WorkOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getWoTitle() != null && e2.getWoTitle() != null) {
						return e1.getWoTitle().compareTo(e2.getWoTitle());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingWoGroup() {
		List<WorkOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<WorkOrder>(list);
		columnSort.setComparator(getColumnWoGroup, new Comparator<WorkOrder>() {
			@Override
			public int compare(WorkOrder e1, WorkOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getGroup() != null && e2.getGroup() != null) {
						return e1.getGroup().compareTo(e2.getGroup());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnSortingWoCategory() {
		List<WorkOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<WorkOrder>(list);
		columnSort.setComparator(getColumnWoCategory, new Comparator<WorkOrder>() {
			@Override
			public int compare(WorkOrder e1, WorkOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCategory() != null && e2.getCategory() != null) {
						return e1.getCategory().compareTo(e2.getCategory());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingWoType() {
		List<WorkOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<WorkOrder>(list);
		columnSort.setComparator(getColumnWoType, new Comparator<WorkOrder>() {
			@Override
			public int compare(WorkOrder e1, WorkOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getType() != null && e2.getType() != null) {
						return e1.getType().compareTo(e2.getType());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnSortingWoProject() {
		List<WorkOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<WorkOrder>(list);
		columnSort.setComparator(getColumnWoProject, new Comparator<WorkOrder>() {
			@Override
			public int compare(WorkOrder e1, WorkOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getWoProject() != null && e2.getWoProject() != null) {
						return e1.getWoProject().compareTo(e2.getWoProject());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingWoBranch() {
		List<WorkOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<WorkOrder>(list);
		columnSort.setComparator(getColumnWoBranch, new Comparator<WorkOrder>() {
			@Override
			public int compare(WorkOrder e1, WorkOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null && e2.getBranch() != null) {
						return e1.getBranch().compareTo(e2.getBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingWoSalesPerson() {
		List<WorkOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<WorkOrder>(list);
		columnSort.setComparator(getColumnWoSalesPerson, new Comparator<WorkOrder>() {
			@Override
			public int compare(WorkOrder e1, WorkOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getSalesPerson() != null && e2.getSalesPerson() != null) {
						return e1.getSalesPerson().compareTo(e2.getSalesPerson());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingWoRequestedBy() {
		List<WorkOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<WorkOrder>(list);
		columnSort.setComparator(getColumnWoPersonResponsible, new Comparator<WorkOrder>() {
			@Override
			public int compare(WorkOrder e1, WorkOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployee() != null && e2.getEmployee() != null) {
						return e1.getEmployee().compareTo(e2.getEmployee());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingWoApprover() {
		List<WorkOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<WorkOrder>(list);
		columnSort.setComparator(getColumnWoApprover, new Comparator<WorkOrder>() {
			@Override
			public int compare(WorkOrder e1, WorkOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getApproverName() != null && e2.getApproverName() != null) {
						return e1.getApproverName().compareTo(e2.getApproverName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingWoStatus() {
		List<WorkOrder> list = getDataprovider().getList();
		columnSort = new ListHandler<WorkOrder>(list);
		columnSort.setComparator(getColumnWoStatus, new Comparator<WorkOrder>() {
			@Override
			public int compare(WorkOrder e1, WorkOrder e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null && e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
