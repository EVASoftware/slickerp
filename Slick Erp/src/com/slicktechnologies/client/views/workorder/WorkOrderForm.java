package com.slicktechnologies.client.views.workorder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import javax.swing.text.TabExpander;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterial;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterialDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.workorder.WorkOrder;
import com.slicktechnologies.shared.common.workorder.WorkOrderProductDetails;

public class WorkOrderForm extends ApprovableFormScreen<WorkOrder> implements ClickHandler{

	final GenricServiceAsync async = GWT.create(GenricService.class);
	
	TextBox ibOrderId;
	DateBox dbOrderDate;
	TextBox tbRefNumId;
	DateBox dbRefDate;
	DateBox dbOrderDeliveryDate;
	ObjectListBox<HrProject> oblProject;
	ObjectListBox<Employee> oblSalesPerson;
	
	
	TextBox ibWoId;
	TextBox tbWoTitle;
	DateBox dbWoDate;
	DateBox dbWoCompletionDate;
	
	ObjectListBox<Config> oblWoGroup;
	ObjectListBox<ConfigCategory> oblWoCategory;
	ObjectListBox<Type> oblWoType;
	
	ObjectListBox<Branch> oblWoBranch;
	ObjectListBox<Employee> oblWoApproverName;
	ObjectListBox<Employee> oblWoPersonRes;
	
	TextBox tbWoStatus;
	TextBox tbWoComment;
	
	UploadComposite upload;
	TextArea taWoDescription;
	public boolean flage=false;
	
	WorkOrderBomTable bomTable;
	WorkOrderTable table;
	
	ProductInfoComposite prodInfoComp;
	Button btnAdd1;
	ProductInfoComposite productInfoComposite;
	Button btnAdd;
	
	ListBox lbComProdId;
	
	ArrayList<WorkOrderProductDetails> list;
	
	TextBox tbusertrainingOne;
	TextBox tbusertrainingTwo;
	CheckBox chkcablingdonegenesis;
	Button btncopytoMatrialtable;
	DateBox dbWoCompletionDateOne;
	TextBox tbRemark;
	
	WorkOrder workorderobj;
	FormField fmrnWoInfoGrouping;
	public WorkOrderForm() {
		super();
		createGui();
		
		this.ibWoId.setEnabled(false);
		this.tbWoStatus.setEnabled(false);
		this.tbWoStatus.setText(WorkOrder.CREATED);
		this.dbOrderDate.setEnabled(false);
		this.tbWoComment.setEnabled(false);
		this.dbOrderDeliveryDate.setEnabled(false);
		list=new ArrayList<WorkOrderProductDetails>();
	}

	public WorkOrderForm(String[] processlevel, FormField[][] fields,FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
		list=new ArrayList<WorkOrderProductDetails>();
	}
	
	private void initializeWidget(){
		ibOrderId=new TextBox();
		dbOrderDate=new DateBoxWithYearSelector();
		tbRefNumId=new TextBox();
		dbRefDate=new DateBoxWithYearSelector();
		dbOrderDeliveryDate=new DateBoxWithYearSelector();
		ibWoId=new TextBox();
		dbWoDate=new DateBoxWithYearSelector();
		tbWoTitle=new TextBox();
		
		dbWoCompletionDate=new DateBoxWithYearSelector();
		oblWoGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblWoGroup,Screen.WORKORDERGROUP);
		oblWoCategory= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblWoCategory, Screen.WORKORDERCATEGORY);
		oblWoType = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(oblWoType,Screen.WORKORDERTYPE);
		
		oblWoBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(oblWoBranch);
		
		oblSalesPerson = new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(oblSalesPerson);
		oblSalesPerson.makeEmployeeLive(AppConstants.SALESMODULE, AppConstants.WORK_ORDER, "Sales Person");
		
		oblWoApproverName = new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(oblWoApproverName, "Work Order");
		
		
		oblWoPersonRes = new ObjectListBox<Employee>();
		oblWoPersonRes.makeEmployeeLive(AppConstants.SALESMODULE, AppConstants.WORK_ORDER, "Person Responsible");
//		AppUtility.makeSalesPersonListBoxLive(oblWoPersonRes);
		
		oblProject = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(oblProject);
		
		
		tbWoStatus=new TextBox();
		tbWoComment=new TextBox();
		
		upload=new UploadComposite();
		taWoDescription=new TextArea();
		
		prodInfoComp = AppUtility.initiatePurchaseProductComposite(new SuperProduct());
		bomTable=new WorkOrderBomTable();
		btnAdd1=new Button("ADD");
		btnAdd1.addClickHandler(this);
		
		/**
		 * Date : 23-11-2017 BY ANIL
		 * Changed entity from SuperProduct To ItemProduct to load only item product on product composite
		 */
		productInfoComposite = AppUtility.initiatePurchaseProductComposite(new ItemProduct());
		lbComProdId=new ListBox();
		lbComProdId.addItem("--SELECT--");
		
		table=new WorkOrderTable();
		btnAdd=new Button("ADD");
		btnAdd.addClickHandler(this);
		
		
		tbusertrainingOne = new TextBox();
		tbusertrainingTwo = new TextBox();
		chkcablingdonegenesis = new CheckBox();
		chkcablingdonegenesis.setValue(false);
		btncopytoMatrialtable = new Button("Copy Product");
		btncopytoMatrialtable.addClickHandler(this);
		
		dbWoCompletionDateOne=new DateBoxWithYearSelector();
		tbRemark = new TextBox();
	}
	
	@Override
	public void createScreen() {
		initializeWidget();
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("WorkOrder","DeliveryNoteFromWorkOrder")){
			
			/**
			 * Ajinkya added  submit button on Date 01/08/2017
			 */
			this.processlevelBarNames = new String[] {"Mark Completed",
					ManageApprovals.APPROVALREQUEST,
					ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCEL,AppConstants.NEW ,manageapproval.SUBMIT,AppConstants.PRINTDELIVERYNOTE};
			
		}else{
			
			/**
			 * Ajinkya added  submit button on Date 01/08/2017
			 */
			this.processlevelBarNames = new String[] {"Mark Completed",
					ManageApprovals.APPROVALREQUEST,
					ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCEL,AppConstants.NEW ,manageapproval.SUBMIT};
			
		}
		
		String mainScreenLabel="Work Order";
		if(workorderobj!=null&&workorderobj.getCount()!=0){
			mainScreenLabel=workorderobj.getCount()+"/"+workorderobj.getStatus()+"/"+AppUtility.parseDate(workorderobj.getCreationDate());
		}
		//fmrnWoInfoGrouping.getHeaderLabel().setText(mainScreenLabel);
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fmrnWoInfoGrouping=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
				
		//FormField fmrnWoInfoGrouping = fbuilder.setlabel("Work Order Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("Order Id", ibOrderId);
		FormField fibOrderIdId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Order Date", dbOrderDate);
		FormField fdbOrderDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Order Delivery Date", dbOrderDeliveryDate);
		FormField fdbOrderDeliveryDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Reference Number", tbRefNumId);
		FormField fibRefId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Reference Date", dbRefDate);
		FormField fdbRefDate = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project", oblProject);
		FormField foblWoProject = fbuilder.setMandatory(false).setMandatoryMsg("Project is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Saleperson",oblSalesPerson );
		FormField foblWoSalesPerson = fbuilder.setMandatory(false).setMandatoryMsg("Sales Person is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
			
		fbuilder = new FormFieldBuilder("Work Order ID", ibWoId);
		FormField fibWoId= fbuilder.setMandatory(false).setMandatoryMsg("Work Order ID is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Work Order Title", tbWoTitle);
		FormField ftbWoTitle = fbuilder.setMandatory(true).setMandatoryMsg("Work Order Title is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Work Order Date", dbWoDate);
		FormField fdbWoDate = fbuilder.setMandatory(true).setMandatoryMsg("Work Order Date is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Work Order Due Date", dbWoCompletionDate);
		FormField fdbWoCompDate = fbuilder.setMandatory(false).setMandatoryMsg("Work Order Date is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Work Order Group", oblWoGroup);
		FormField foblWoGroup = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Work Order Category", oblWoCategory);
		FormField foblWoCategory = fbuilder.setMandatory(false).setMandatoryMsg("Work Order Category is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Work Order Type", oblWoType);
		FormField foblWoType = fbuilder.setMandatory(false).setMandatoryMsg("Work Order Type is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Branch", oblWoBranch);
		FormField foblWoBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Approver", oblWoApproverName);
		FormField foblWoApproverName = fbuilder.setMandatory(true).setMandatoryMsg("Approver is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Person Responsible", oblWoPersonRes);
		FormField foblWoPersonResp = fbuilder.setMandatory(true).setMandatoryMsg("Person Responsible is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", tbWoStatus);
		FormField ftbWoStatus = fbuilder.setMandatory(true).setMandatoryMsg("Status is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Comment", tbWoComment);
		FormField ftbWoCommentOnStatusChange = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Document", upload);
		FormField fupload = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)", taWoDescription);
		FormField ftaWoDescription = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		FormField fmrnProductGrouping = fbuilder.setlabel("Product")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", prodInfoComp);
		FormField fprodInfoComp= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("", btnAdd1);
		FormField fbtnAdd1= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", bomTable.getTable());
		FormField fproductTableBom= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		   
		
		FormField fmrnClassiGrouping = fbuilder.setlabel("Classification")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		FormField fmrnRefereceDetailGrouping = fbuilder.setlabel("Reference/General")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		FormField fmrnAttachGrouping = fbuilder.setlabel("Attachment")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		FormField fmrnSubProductGrouping = fbuilder.setlabel("Material")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", productInfoComposite);
		FormField fproductInfoComposite= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("Composite Id", lbComProdId);
		FormField flbcomprodId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fbtnAdd= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", table.getTable());
		FormField fproductTableWo= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		/************************* vijay  ***********************/
		
		fbuilder = new FormFieldBuilder("User Training One", tbusertrainingOne);
		FormField ftbusertrainingOne = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("User Training Two", tbusertrainingTwo);
		FormField ftbusertrainingTwo = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Executed By", chkcablingdonegenesis);
		FormField fchkcablingdonegenesis = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", btncopytoMatrialtable);
		FormField fbtncopytoMatrialtable= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Completion Date", dbWoCompletionDateOne);
		FormField fdbWoCompletionDateOne = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Remark", tbRemark);
		FormField ftbRemark = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		/***************************************************************/
		
		
		FormField[][] formfield = { 
//				{ fmrnRefereceDetailGrouping },
//				{ fibOrderIdId,fdbOrderDate,fibRefId, fdbRefDate},
//				{ fdbOrderDeliveryDate,foblWoProject,foblWoSalesPerson},
//				{ fmrnWoInfoGrouping},
//				{ fibWoId,ftbWoTitle, fdbWoDate,fdbWoCompDate},
//				{ foblWoGroup,foblWoCategory,foblWoType,foblWoBranch},
//				{ foblWoPersonResp,foblWoApproverName,ftbWoStatus, ftbWoCommentOnStatusChange},
//				{ ftbusertrainingOne,ftbusertrainingTwo,fdbWoCompletionDateOne,ftbRemark},
//				{ fchkcablingdonegenesis},
//				{ fupload},
//				{ ftaWoDescription},
//				{ fmrnProductGrouping},
//				{ fprodInfoComp,fbtnAdd1},
//				{ fproductTableBom},
//				{ fmrnSubProductGrouping},
//				{ fproductInfoComposite},
//				{ flbcomprodId,fbtnAdd,fbtncopytoMatrialtable},
//				{ fproductTableWo}
				/**Main Screen**/
				{ fmrnWoInfoGrouping},
				{ ftbWoTitle, fdbWoDate,fibOrderIdId,fdbOrderDate},
				{ foblWoBranch,foblWoPersonResp,foblWoApproverName,ftbRemark},
				{ fchkcablingdonegenesis},
				/**Product**/
				{ fmrnProductGrouping},
				{ fprodInfoComp,fbtnAdd1},
				{ fproductTableBom},
				/**Material Required**/
				{ fmrnSubProductGrouping},
				{ flbcomprodId,fbtncopytoMatrialtable},
				{ fproductInfoComposite,fbtnAdd},
				{ fproductTableWo},
				/**Classification**/
				{ fmrnClassiGrouping},   
				{ foblWoGroup,foblWoCategory,foblWoType},
				/**Reference and Genral**/
				{ fmrnRefereceDetailGrouping },
				{ fibRefId, fdbRefDate,fdbOrderDeliveryDate,foblWoSalesPerson},
				{ fdbWoCompletionDateOne,foblWoProject,ftbusertrainingOne,ftbusertrainingTwo,},
				{ fdbWoCompDate,ftbWoCommentOnStatusChange},
				{ ftaWoDescription},
				/**Attachment**/
				{ fmrnAttachGrouping},
				{ fupload },
				
				
				
				
				
		};
		this.fields = formfield;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(WorkOrder model) {
		
		if (!ibOrderId.getValue().equals(""))
			model.setOrderId(Integer.parseInt(ibOrderId.getValue()));
		if (dbOrderDate.getValue() != null)
			model.setOrderDate(dbOrderDate.getValue());
		if (!tbRefNumId.getValue().equals(""))
			model.setRefNum(tbRefNumId.getValue());
		if (dbRefDate.getValue() != null)
			model.setRefDate(dbRefDate.getValue());
		if (dbOrderDeliveryDate.getValue() != null)
			model.setOrderDeliveryDate(dbOrderDeliveryDate.getValue());
		
		if (oblProject.getValue() != null)
			model.setWoProject(oblProject.getValue());
		
		if (tbWoTitle.getValue() != null)
			model.setWoTitle(tbWoTitle.getValue());
		if (dbWoDate.getValue() != null)
			model.setCreationDate(dbWoDate.getValue());
		if (dbWoCompletionDate.getValue() != null)
			model.setCompletionDate(dbWoCompletionDate.getValue());
		
		
		if (oblWoGroup.getValue() != null)
			model.setGroup(oblWoGroup.getValue());
		if (oblWoCategory.getValue() != null)
			model.setCategory(oblWoCategory.getValue());
		if (oblWoType.getValue() != null)
			model.setType(oblWoType.getValue());
		
		if (oblWoBranch.getValue() != null)
			model.setBranch(oblWoBranch.getValue());
		if (oblSalesPerson.getValue() != null)
			model.setSalesPerson(oblSalesPerson.getValue());
		if (oblWoPersonRes.getValue() != null)
			model.setEmployee(oblWoPersonRes.getValue());
		if (oblWoApproverName.getValue() != null)
			model.setApproverName(oblWoApproverName.getValue());
		
		if (tbWoStatus.getValue() != null)
			model.setStatus(tbWoStatus.getValue());
		if (tbWoComment.getValue() != null)
			model.setRemark(tbWoComment.getValue());
		
		if (taWoDescription.getValue() != null)
			model.setWoDescription(taWoDescription.getValue());
		if (upload.getValue() != null)
			model.setUpload(upload.getValue());
		if (bomTable.getValue() != null)
			model.setBomTable(bomTable.getValue());
		if (table.getValue() != null)
			model.setWoTable(table.getValue());
		
		if(tbusertrainingOne.getValue()!=null){
			model.setUserTrainingOne(tbusertrainingOne.getValue());
		}
		
		if(tbusertrainingTwo.getValue()!=null){
			model.setUserTrainingTwo(tbusertrainingTwo.getValue());
		}

		if (dbWoCompletionDateOne.getValue() != null)
			model.setCompletionDateone(dbWoCompletionDateOne.getValue());
		
		if(tbRemark.getValue()!=null){
			model.setRemark(tbRemark.getValue());
		}
		
		
		if(chkcablingdonegenesis.getValue()!=null){
			model.setCablingByGenesis(chkcablingdonegenesis.getValue());	
		}
		
	
		workorderobj=model;
		presenter.setModel(model);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateView(WorkOrder view) {
		
		workorderobj=view;
		
		ibWoId.setValue(view.getCount()+"");
		if (view.getWoTitle() != null)
			tbWoTitle.setValue(view.getWoTitle());
		if (view.getCompletionDate() != null)
			dbWoCompletionDate.setValue(view.getCompletionDate());
		if (view.getCreationDate() != null)
			dbWoDate.setValue(view.getCreationDate());
		
		if (view.getRefNum() != null)
			tbRefNumId.setValue(view.getRefNum());
		if (view.getRefDate() != null)
			dbRefDate.setValue(view.getRefDate());
		if (view.getOrderId() != 0){
			ibOrderId.setValue(view.getOrderId()+"");
		}
		if(view.getOrderDeliveryDate()!=null){
			dbOrderDeliveryDate.setValue(view.getOrderDeliveryDate());
		}
		if (view.getOrderDate() != null)
			dbOrderDate.setValue(view.getOrderDate());
		if (view.getWoProject() != null)
			oblProject.setValue(view.getWoProject());
		if (view.getSalesPerson() != null)
			oblSalesPerson.setValue(view.getSalesPerson());
		
		if (view.getGroup()!= null)
			oblWoGroup.setValue(view.getGroup());
		if (view.getCategory() != null)
			oblWoCategory.setValue(view.getCategory());
		if (view.getType()!= null)
			oblWoType.setValue(view.getType());
		
		if (view.getBranch() != null)
			oblWoBranch.setValue(view.getBranch());
		if (view.getEmployee() != null)
			oblWoPersonRes.setValue(view.getEmployee());
		if (view.getApproverName()!= null)
			oblWoApproverName.setValue(view.getApproverName());
		if (view.getStatus() != null)
			tbWoStatus.setValue(view.getStatus());
		if (view.getRemark()!= null){
			tbWoComment.setValue(view.getRemark());
		}
		if (view.getWoDescription() != null)
			taWoDescription.setValue(view.getWoDescription());
		if (view.getUpload() != null)
			upload.setValue(view.getUpload());
		
		if (view.getBomTable() != null){
			bomTable.setValue(view.getBomTable());
			lbComProdId.clear();
			lbComProdId.addItem("--SELECT--");
			for(int i=0;i<view.getBomTable().size();i++){
				lbComProdId.addItem(view.getBomTable().get(i).getPrduct().getCount()+"");
			}
		}
		
		if (view.getWoTable() != null){
			table.setValue(view.getWoTable());
		}
		
		if(view.getUserTrainingOne()!=null){
			tbusertrainingOne.setValue(view.getUserTrainingTwo());
		}
		
		if(view.getUserTrainingTwo()!=null){
			tbusertrainingTwo.setValue(view.getUserTrainingTwo());
		}
		if (view.getCompletionDateone() != null)
			dbWoCompletionDateOne.setValue(view.getCompletionDateone());
												
		if(view.getRemark()!=null){
			tbRemark.setValue(view.getRemark());
		}
		
		chkcablingdonegenesis.setValue(view.isCablingByGenesis());
		
		
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")
						|| text.contains("Search")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")|| text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Edit") || text.contains("Discard")|| text.contains("Search")|| text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.WORKORDER,LoginPresenter.currentModule.trim());
	}
	
	public void toggleProcessLevelMenu() {
		WorkOrder entity = (WorkOrder) presenter.getModel();
		String status = entity.getStatus();

		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();
			if ((status.equals(WorkOrder.APPROVED))) {
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);
			}
			if ((status.equals(WorkOrder.REJECTED) || status.equals(WorkOrder.CANCELLED))) {
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);
			}
		}
	}
	
	public void setAppHeaderBarAsPerStatus() {
		WorkOrder entity = (WorkOrder) presenter.getModel();
		String status = entity.getStatus();

		if (status.equals(WorkOrder.APPROVED)) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")||text.contains("Email")||text.contains("Print")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		
		//*************vaishnavi****************
				if (status.equals(WorkOrder.CREATED)) {
					InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
					for (int k = 0; k < menus.length; k++) {
						String text = menus[k].getText();
						if (text.contains("Discard") || text.contains("Search")||text.contains("Edit")||text.contains("Print")|| text.contains(AppConstants.NAVIGATION))
						{
							menus[k].setVisible(true);
						} else {
							menus[k].setVisible(false);
						}
					}
				}
		
		
		if (status.equals(WorkOrder.CANCELLED)||status.equals(WorkOrder.REJECTED)||status.equals(WorkOrder.REQUESTED)) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")||text.contains("Print")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
	}
	
	public void setMenuAsPerStatus() 
	{    
		this.changeProcessLevel();                    //Ajinkya Added this on date : 01/08/2017  
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
	}
	
	 //////////////////// Ajinkya added this for self Approval      ////////////////////////
	
//	public void setMenus()
//	{
//		this.changeProcessLevel();
//		this.setAppHeaderBarAsPerStatus();
//	} 
	
	public void changeProcessLevel()
	{  
		
		WorkOrder entity=(WorkOrder) presenter.getModel();
		String status=entity.getStatus();
	
//		boolean renewFlagStatus=entity.isRenewFlag();
		
		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();
			if(status.equals(WorkOrder.CREATED))
			{
				System.out.println("inside Created WorkOrder ");
				/**
				 * Date : 23-05-2017 By ANIL
				 * Checking self approval           updated here by Ajinkya for contract 
				 */
				if(getManageapproval().isSelfApproval())
				{  
					System.out.println("inside getManageapproval method ");
					
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(true);
				}
				else
				{  
					System.out.println("inside else getManageapproval method ");
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(true);
					if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
				}
				
				
			}
			if(status.equals(WorkOrder.REQUESTED))
			{  
				System.out.println("inside  WorkOrder REQUESTED ");
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(true);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			}
			
			if(status.equals(WorkOrder.REJECTED))
			{
				System.out.println("inside  WorkOrder REJECTED ");
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(true);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			}
			
			if(status.equals(WorkOrder.APPROVED))
			{
				System.out.println("inside  WorkOrder APPROVED ");
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			}
			
			
			if(status.equals(WorkOrder.CANCELLED))
			{  
				System.out.println("inside  WorkOrder CANCELLED ");
				
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
			}
			
			
			
			//Ashwini Patil Date:20-09-2022
			if(isHideNavigationButtons()) {
				Console.log("in changeProcessLevel isHideNavigationButtons");
				if(text.equalsIgnoreCase(AppConstants.NEW ))
					label.setVisible(false);	
			}
		}
		
	} 
	
	                  //////////////////////////// End Here //////////////////////////
	

	
	
	
	@Override
	public void setCount(int count) {
		ibWoId.setValue(count+"");
	}

	@Override
	public void clear() {
		super.clear();
		tbWoStatus.setText(WorkOrder.CREATED);
		table.clear();

	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbWoStatus.setEnabled(false);
		tbWoComment.setEnabled(false);
		ibWoId.setEnabled(false);
		table.setEnable(state);
		dbOrderDate.setEnabled(false);
		table.setEnable(state);
		bomTable.setEnable(state);

	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		
		setMenuAsPerStatus();
		
		SuperModel model=new WorkOrder();
		model=workorderobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SALESMODULE,AppConstants.WORK_ORDER, workorderobj.getCount(), null,null,null, false, model, null);
		
		String mainScreenLabel="Work Order";
		if(workorderobj!=null&&workorderobj.getCount()!=0){
			mainScreenLabel=workorderobj.getCount()+"/"+workorderobj.getStatus()+"/"+AppUtility.parseDate(workorderobj.getCreationDate());
		}
			fmrnWoInfoGrouping.getHeaderLabel().setText(mainScreenLabel);
		
	}

	@Override
	public void setToEditState() {
		
		setMenuAsPerStatus();
		if(tbWoStatus.getValue().equals(WorkOrder.REJECTED)){
			tbWoStatus.setValue(WorkOrder.CREATED);
		}
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		
		String mainScreenLabel="Work Order";
		if(workorderobj!=null&&workorderobj.getCount()!=0){
			mainScreenLabel=workorderobj.getCount()+"/"+workorderobj.getStatus()+"/"+AppUtility.parseDate(workorderobj.getCreationDate());
		}
			fmrnWoInfoGrouping.getHeaderLabel().setText(mainScreenLabel);
	}

	@Override
	public TextBox getstatustextbox() {
		return tbWoStatus;
	}

	@Override
	public void onClick(ClickEvent event) {
		// Button 1
		if(event.getSource().equals(btnAdd)){
			addProductToTable();
		}
		//Button 2
		if(event.getSource().equals(btnAdd1)){
			if(prodInfoComp.getValue()!=null){
				if(validateCompProduct()){
					flage=true;
					getBillOfProductMaterial(Integer.parseInt(prodInfoComp.getProdID().getValue()),prodInfoComp.getProdName().getValue(),1.0,true);
				}
				else{
					showDialogMessage("Can not add Duplicate Product!");
				}
			}else{
				showDialogMessage("Please Enter Product Details!");
			}
		}
		
		if(event.getSource().equals(btncopytoMatrialtable)){
			System.out.println("Clickd");
			copyProductToTable();
		}
	}
	
	
	private void copyProductToTable() {
		System.out.println("In method");
		
		if(table.getDataprovider().getList().size() > 0)
		{
			List<WorkOrderProductDetails> tableList = new ArrayList<WorkOrderProductDetails>();
			List<WorkOrderProductDetails> setTotableList = new ArrayList<WorkOrderProductDetails>();
			tableList.addAll(table.getDataprovider().getList());
			setTotableList.addAll(table.getDataprovider().getList());
			
			
				if(validateProcuct(tableList)== false)
				{
					for(int i=0;i<bomTable.getDataprovider().getList().size();i++){
					System.out.println("In method for loop");
					WorkOrderProductDetails bopm=new WorkOrderProductDetails();
					
					bopm.setCompProdId(bomTable.getDataprovider().getList().get(i).getPrduct().getCount());
					bopm.setProductId(bomTable.getDataprovider().getList().get(i).getPrduct().getCount());
					bopm.setProductCode(bomTable.getDataprovider().getList().get(i).getProductCode());
					bopm.setProductName(bomTable.getDataprovider().getList().get(i).getProductName());
					bopm.setProductCategory(bomTable.getDataprovider().getList().get(i).getProductCategory());
					bopm.setProductQty(bomTable.getDataprovider().getList().get(i).getQty());
					bopm.setProductPrice(bomTable.getDataprovider().getList().get(i).getPrice());
					bopm.setProductUOM(bomTable.getDataprovider().getList().get(i).getUnitOfMeasurement());
					
					setTotableList.add(bopm);
				}
					
					table.getDataprovider().setList(setTotableList);
			}
			else
			{
				this.showDialogMessage("Duplicate Product Please Check..!");
			}
		}
		else
		{
		table.connectToLocal();
		for(int i=0;i<bomTable.getDataprovider().getList().size();i++){
			System.out.println("In method for loop");
			WorkOrderProductDetails bopm=new WorkOrderProductDetails();
			
			bopm.setCompProdId(bomTable.getDataprovider().getList().get(i).getPrduct().getCount());
			bopm.setProductId(bomTable.getDataprovider().getList().get(i).getPrduct().getCount());
			bopm.setProductCode(bomTable.getDataprovider().getList().get(i).getProductCode());
			bopm.setProductName(bomTable.getDataprovider().getList().get(i).getProductName());
			bopm.setProductCategory(bomTable.getDataprovider().getList().get(i).getProductCategory());
			bopm.setProductQty(bomTable.getDataprovider().getList().get(i).getQty());
			bopm.setProductPrice(bomTable.getDataprovider().getList().get(i).getPrice());
			bopm.setProductUOM(bomTable.getDataprovider().getList().get(i).getUnitOfMeasurement());
			
			table.getDataprovider().getList().add(bopm);
		}
		}
	}
		
			private boolean validateProcuct(List<WorkOrderProductDetails> tableList) 
			{
				int cnt = 0;
				for (int j = 0; j < bomTable.getDataprovider().getList().size(); j++) {
					
					for (int i = 0; i < tableList.size(); i++) {
					
						 if(bomTable.getDataprovider().getList().get(j).getPrduct().getProductCode().
								 equals(tableList.get(i).getProductCode()))
								 {
							 		cnt = cnt+1;
								 }
					}
				}
				
				if(cnt > 0)
					return true;
				else
					return false;
			}

/**
 * 
 * @param prodId
 * @param prodName
 * @param prodQty
 * @param isMsg
 * 
 * This method is used to get Bill of material from entity BillOfProductMaterial and this method is also called form 
 * SalesOrderPresenter.
 * 
 * this method is commonly used for both Item Product and Service product.
 * if it didnt get bill of material for item product then it checks for service Bill of Material.
 * 
 */
	
	public void getBillOfProductMaterial(final int prodId,final String prodName,final double prodQty,final boolean isMsg) {
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = new Filter();
		filter.setQuerryString("bomProductId");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillOfProductMaterial());
		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						if (result.size() != 0) {
							
							for (SuperModel model : result) {
							
								BillOfProductMaterial entity = (BillOfProductMaterial) model;
								if(entity.isBomStatus()==true){
									if(isMsg==true){
										boolean conf = Window.confirm("Do you want to add assembly products for "+prodName+" ?");
										if (conf == true) {
											for(int i=0;i<entity.getBomProductList().size();i++){
												WorkOrderProductDetails wopd=new WorkOrderProductDetails();
												wopd.setCompProdId(entity.getBomProductList().get(i).getCompProdId());
												wopd.setProductId(entity.getBomProductList().get(i).getProductId());
												wopd.setProductCode(entity.getBomProductList().get(i).getProductCode());
												wopd.setProductName(entity.getBomProductList().get(i).getProductName());
												wopd.setProductCategory(entity.getBomProductList().get(i).getProductCategory());
												wopd.setProductQty(entity.getBomProductList().get(i).getProductQty()*prodQty);
												wopd.setProductPrice(entity.getBomProductList().get(i).getProductPrice());
												wopd.setProductUOM(entity.getBomProductList().get(i).getProductUOM());
												table.getDataprovider().getList().add(wopd);
//												list.add(wopd);
											}
//											table.getDataprovider().setList(list);
											ProdctType(prodInfoComp.getProdCode().getValue(), prodInfoComp.getProdID().getValue());
										}
										else{
											ProdctType(prodInfoComp.getProdCode().getValue(), prodInfoComp.getProdID().getValue());
										}
									}else{
											for(int i=0;i<entity.getBomProductList().size();i++){
												WorkOrderProductDetails wopd=new WorkOrderProductDetails();
												wopd.setCompProdId(entity.getBomProductList().get(i).getCompProdId());
												wopd.setProductId(entity.getBomProductList().get(i).getProductId());
												wopd.setProductCode(entity.getBomProductList().get(i).getProductCode());
												wopd.setProductName(entity.getBomProductList().get(i).getProductName());
												wopd.setProductCategory(entity.getBomProductList().get(i).getProductCategory());
												wopd.setProductQty(entity.getBomProductList().get(i).getProductQty()*prodQty);
												wopd.setProductPrice(entity.getBomProductList().get(i).getProductPrice());
												wopd.setProductUOM(entity.getBomProductList().get(i).getProductUOM());
												table.getDataprovider().getList().add(wopd);
//												list.add(wopd);
											}
//											table.getDataprovider().setList(list);
									}
								}else{
									if(isMsg==true){
										showDialogMessage("Please check whether product status is active or not!");
									}
								}
							
							
							}
						}
						else{
							if(isMsg==true){
							ProdctType(prodInfoComp.getProdCode().getValue(), prodInfoComp.getProdID().getValue());
							}
						}
//						flage=false;
					}
					
					@Override
					public void onFailure(Throwable caught) {
						showDialogMessage("Unexpected Error occured!");
					}
		});
	}

	/*************************************************************************************************************/
	
	
	private void addProductToTable() {
		if(productInfoComposite.getValue()!=null){
			if(validateBOMProduct()){
				if(validateProduct()){
					WorkOrderProductDetails bopm=new WorkOrderProductDetails();
					bopm.setCompProdId(Integer.parseInt(lbComProdId.getValue(lbComProdId.getSelectedIndex())));
					bopm.setProductId(Integer.parseInt(productInfoComposite.getProdID().getValue()));
					bopm.setProductCode(productInfoComposite.getProdCode().getValue());
					bopm.setProductName(productInfoComposite.getProdName().getValue());
					bopm.setProductCategory(productInfoComposite.getProdCategory().getValue());
					bopm.setProductQty(0);
					bopm.setProductPrice(productInfoComposite.getProdPrice().getValue());
					bopm.setProductUOM(productInfoComposite.getUnitOfmeasuerment().getValue());
					
					table.getDataprovider().getList().add(bopm);
				}
				else{
					showDialogMessage("Can not add Duplicate Product!");
				}
			}
			else{
				showDialogMessage("Can not add Bill of Material Product!");
			}
		}else{
			showDialogMessage("Please Enter Product Details!");
		}
	}
	

	/********************************** Adding BOM product to table *******************************************/
	
	public void ProdctType(String pcode,String productId)
	{
		final GenricServiceAsync genasync=GWT.create(GenricService.class);
		final MyQuerry querry=new MyQuerry();
		
		int prodId=Integer.parseInt(productId);
		
		Vector<Filter> filtervec=new Vector<Filter>();
		
		Filter filter=new Filter();
		filter.setQuerryString("productCode");
		filter.setStringValue(pcode.trim());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
		showWaitSymbol();
		 Timer timer=new Timer() 
    	 {
				@Override
				public void run() {
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					hideWaitSymbol();
					showDialogMessage("An Unexpected error occurred!");
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					if(result.size()==0)
					{
						showDialogMessage("Please check whether product status is active or not!");
					}
					
					
					for(SuperModel model:result)
					{
						SuperProduct superProdEntity = (SuperProduct)model;
						SalesLineItem lis=AppUtility.ReactOnAddProductComposite(superProdEntity);
					    bomTable.getDataprovider().getList().add(lis);
					}
					lbComProdId.clear();
					lbComProdId.addItem("--SELECT--");
					for(int i=0;i<bomTable.getDataprovider().getList().size();i++){
						lbComProdId.addItem(bomTable.getDataprovider().getList().get(i).getPrduct().getCount()+"");
					}
					
					
					}
				 });
				hideWaitSymbol();
 				}
	 	  };
          timer.schedule(3000); 
	}
	private boolean validateCompProduct(){
		if(bomTable.getDataprovider().getList().size()!=0){
			for(int i=0;i<bomTable.getDataprovider().getList().size();i++){
				if(bomTable.getDataprovider().getList().get(i).getPrduct().getCount()==Integer.parseInt(prodInfoComp.getProdID().getValue())){
					return false;
				}
			}
		}
		else{
			return true;
		}
		return true;
	}
	/***********************************************************************************************************/
	private boolean validateProduct(){
		if(table.getDataprovider().getList().size()!=0){
			if(lbComProdId.getSelectedIndex()!=0){
				for(int i=0;i<table.getDataprovider().getList().size();i++){
					if(table.getDataprovider().getList().get(i).getCompProdId()==Integer.parseInt(lbComProdId.getValue(lbComProdId.getSelectedIndex()))&&table.getDataprovider().getList().get(i).getProductId()==Integer.parseInt(productInfoComposite.getProdID().getValue())){
						return false;
					}
				}
			}else{
				this.showDialogMessage("Please select composite id.");
			}
		}
		else{
			return true;
		}
		return true;
	}
	
	// ************************************** Validate Product Quantity ***********************************************
	
	public boolean validateProdQty()
	{
		int ctr=0;
		for(int i=0;i<table.getDataprovider().getList().size();i++)
		{
			if(table.getDataprovider().getList().get(i).getProductQty()<=0)
			{
				ctr=ctr+1;
			}
		}
		if(ctr==0){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	public boolean validateBomProdQty()
	{
		int ctr=0;
		for(int i=0;i<bomTable.getDataprovider().getList().size();i++)
		{
			if(bomTable.getDataprovider().getList().get(i).getQty()<=0)
			{
				ctr=ctr+1;
			}
		}
		if(ctr==0){
			return true;
		}
		else{
			return false;
		}
	}
	
	/***********************************************************************************************/
	
	private boolean validateBOMProduct(){
		if(productInfoComposite.getValue()!=null){
			for(int i=0;i<bomTable.getDataprovider().getList().size();i++){
				if(Integer.parseInt(productInfoComposite.getProdID().getValue())==bomTable.getDataprovider().getList().get(i).getPrduct().getCount()){
					return false;
				}
			}
		}
		else{
			return true;
		}
		return true;
	}
	
	/*******************************************Validate******************************************************/
	
	@Override
	public boolean validate() {
		boolean superValidate = super.validate();;
		if (superValidate == false)
			return false;
		if(!validateProdQty()){
			showDialogMessage("Please select appropriate quantity!");
			return false;
		}
		
		if(!validateBomProdQty()){
			showDialogMessage("Please select appropriate quantity!");
			return false;
		}
		
		if(bomTable.getDataprovider().getList().size()==0)
		{
			showDialogMessage("Please add atleast one product!");
			return false;
		}
		
		if(table.getDataprovider().getList().size()==0)
		{
			showDialogMessage("Please add material required!");
			return false;
		}
		
		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("WorkOrder", "DoNotMaterialRequiredValidation")){
			if(!validateBomMaterialRequired()){
				showDialogMessage("Please select Material required!");
				return false;
			}
		}

			
		
		
		return true;
	}
	
	
	public boolean validateBomMaterialRequired(){
		List<Integer> productList =new ArrayList<Integer>();
		for(int i=0;i<table.getDataprovider().getList().size();i++){
			productList.add(table.getDataprovider().getList().get(i).getCompProdId());
		}
		List<Integer> mergelist=new ArrayList<Integer>(new HashSet<Integer>(productList));
		
		if(mergelist.size()==bomTable.getDataprovider().getList().size()){
			return true;
		}else{
			return false;
		}
	}
	
	
	
	/************************************** Getter and Setter ***************************************/

	public TextBox getIbOrderId() {
		return ibOrderId;
	}

	public void setIbOrderId(TextBox ibOrderId) {
		this.ibOrderId = ibOrderId;
	}

	public DateBox getDbOrderDate() {
		return dbOrderDate;
	}

	public void setDbOrderDate(DateBox dbOrderDate) {
		this.dbOrderDate = dbOrderDate;
	}

	public TextBox getTbRefNumId() {
		return tbRefNumId;
	}

	public void setTbRefNumId(TextBox tbRefNumId) {
		this.tbRefNumId = tbRefNumId;
	}

	public DateBox getDbRefDate() {
		return dbRefDate;
	}

	public void setDbRefDate(DateBox dbRefDate) {
		this.dbRefDate = dbRefDate;
	}

	public ObjectListBox<HrProject> getOblProject() {
		return oblProject;
	}

	public void setOblProject(ObjectListBox<HrProject> oblProject) {
		this.oblProject = oblProject;
	}

	public ObjectListBox<Employee> getOblSalesPerson() {
		return oblSalesPerson;
	}

	public void setOblSalesPerson(ObjectListBox<Employee> oblSalesPerson) {
		this.oblSalesPerson = oblSalesPerson;
	}

	public TextBox getIbWoId() {
		return ibWoId;
	}

	public void setIbWoId(TextBox ibWoId) {
		this.ibWoId = ibWoId;
	}

	public TextBox getTbWoTitle() {
		return tbWoTitle;
	}

	public void setTbWoTitle(TextBox tbWoTitle) {
		this.tbWoTitle = tbWoTitle;
	}

	public DateBox getDbWoDate() {
		return dbWoDate;
	}

	public void setDbWoDate(DateBox dbWoDate) {
		this.dbWoDate = dbWoDate;
	}

	public ObjectListBox<Config> getOblWoGroup() {
		return oblWoGroup;
	}

	public void setOblWoGroup(ObjectListBox<Config> oblWoGroup) {
		this.oblWoGroup = oblWoGroup;
	}

	public ObjectListBox<ConfigCategory> getOblWoCategory() {
		return oblWoCategory;
	}

	public void setOblWoCategory(ObjectListBox<ConfigCategory> oblWoCategory) {
		this.oblWoCategory = oblWoCategory;
	}

	public ObjectListBox<Type> getOblWoType() {
		return oblWoType;
	}

	public void setOblWoType(ObjectListBox<Type> oblWoType) {
		this.oblWoType = oblWoType;
	}

	public ObjectListBox<Branch> getOblWoBranch() {
		return oblWoBranch;
	}

	public void setOblWoBranch(ObjectListBox<Branch> oblWoBranch) {
		this.oblWoBranch = oblWoBranch;
	}

	public ObjectListBox<Employee> getOblWoApproverName() {
		return oblWoApproverName;
	}

	public void setOblWoApproverName(ObjectListBox<Employee> oblWoApproverName) {
		this.oblWoApproverName = oblWoApproverName;
	}

	public ObjectListBox<Employee> getOblWoPersonRes() {
		return oblWoPersonRes;
	}

	public void setOblWoPersonRes(ObjectListBox<Employee> oblWoPersonRes) {
		this.oblWoPersonRes = oblWoPersonRes;
	}

	public TextBox getTbWoStatus() {
		return tbWoStatus;
	}

	public void setTbWoStatus(TextBox tbWoStatus) {
		this.tbWoStatus = tbWoStatus;
	}

	public TextBox getTbWoComment() {
		return tbWoComment;
	}

	public void setTbWoComment(TextBox tbWoComment) {
		this.tbWoComment = tbWoComment;
	}

	public UploadComposite getUpload() {
		return upload;
	}

	public void setUpload(UploadComposite upload) {
		this.upload = upload;
	}

	public TextArea getTaWoDescription() {
		return taWoDescription;
	}

	public void setTaWoDescription(TextArea taWoDescription) {
		this.taWoDescription = taWoDescription;
	}

	public WorkOrderTable getTable() {
		return table;
	}

	public void setTable(WorkOrderTable table) {
		this.table = table;
	}

	public ProductInfoComposite getProdInfoComp() {
		return prodInfoComp;
	}

	public void setProdInfoComp(ProductInfoComposite prodInfoComp) {
		this.prodInfoComp = prodInfoComp;
	}

	public Button getBtnAdd1() {
		return btnAdd1;
	}

	public void setBtnAdd1(Button btnAdd1) {
		this.btnAdd1 = btnAdd1;
	}

	public ProductInfoComposite getProductInfoComposite() {
		return productInfoComposite;
	}

	public void setProductInfoComposite(ProductInfoComposite productInfoComposite) {
		this.productInfoComposite = productInfoComposite;
	}

	public Button getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(Button btnAdd) {
		this.btnAdd = btnAdd;
	}

	public DateBox getDbOrderDeliveryDate() {
		return dbOrderDeliveryDate;
	}

	public void setDbOrderDeliveryDate(DateBox dbOrderDeliveryDate) {
		this.dbOrderDeliveryDate = dbOrderDeliveryDate;
	}

	public WorkOrderBomTable getBomTable() {
		return bomTable;
	}

	public void setBomTable(WorkOrderBomTable bomTable) {
		this.bomTable = bomTable;
	}

	public DateBox getDbWoCompletionDate() {
		return dbWoCompletionDate;
	}

	public void setDbWoCompletionDate(DateBox dbWoCompletionDate) {
		this.dbWoCompletionDate = dbWoCompletionDate;
	}

	public boolean isFlage() {
		return flage;
	}

	public void setFlage(boolean flage) {
		this.flage = flage;
	}

	public ListBox getLbComProdId() {
		return lbComProdId;
	}

	public void setLbComProdId(ListBox lbComProdId) {
		this.lbComProdId = lbComProdId;
	}

	public ArrayList<WorkOrderProductDetails> getList() {
		return list;
	}

	public void setList(ArrayList<WorkOrderProductDetails> list) {
		this.list = list;
	}
	
	
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		bomTable.getTable().redraw();
		table.getTable().redraw();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
