package com.slicktechnologies.client.views.workorder;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.workorder.WorkOrderProductDetails;

public class WorkOrderBomTable extends SuperTable<SalesLineItem>{
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	TextColumn<SalesLineItem> getColumnProductId;
	TextColumn<SalesLineItem> getColumnProductCode;
	TextColumn<SalesLineItem> getColumnProductName;
	TextColumn<SalesLineItem> getColumnProductCategory;
	TextColumn<SalesLineItem> getColumnProductQty;
	TextColumn<SalesLineItem> getColumnProductUom;
	Column<SalesLineItem, String> prodQtyEdit;
	Column<SalesLineItem, String> deleteColumn;
	
	public WorkOrderBomTable() {
	}
	
	@Override
	public void createTable() {
		
		
		addColumnProductName();
		addColumnProductUom();
		addColumnQty();
		addColumnProductCategory();
		addColumnProductCode();
		addColumnProductId();
		createColumndeleteColumn();
		addFieldUpdater();
		
	}
	

	public void addeditColumn() {
		addColumnProductId();
		addColumnProductCode();
		addColumnProductName();
		addColumnProductCategory();
		addColumnQty();
		addColumnProductUom();
		createColumndeleteColumn();
		addFieldUpdater();

	}

	public void addViewColumn() {
		addColumnProductId();
		addColumnProductCode();
		addColumnProductName();
		addColumnProductCategory();
		addColumnProductQty();
		addColumnProductUom();
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterprodQtyColumn();
//		if(WorkOrderPresenter.orderId==0)
			createFieldUpdaterDelete();
		
	}
	
	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();
	}
	
	private void addColumnProductId() {
		getColumnProductId=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getPrduct().getCount()!=0){
					return object.getPrduct().getCount()+"";
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getColumnProductId, "Id");
	}

	private void addColumnProductUom() {
		getColumnProductUom=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getUnitOfMeasurement();
			}
		};
		table.addColumn(getColumnProductUom, "UOM");
	}

	private void addColumnProductQty() {
		getColumnProductQty=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return nf.format(object.getQty());
			}
		};
		table.addColumn(getColumnProductQty, "#Quantity");
	}
	//Editable Quantity Column
		public void addColumnQty() {
			EditTextCell editCell = new EditTextCell();
			prodQtyEdit = new Column<SalesLineItem, String>(editCell) {

				@Override
				public String getValue(SalesLineItem object) {
					return nf.format(object.getQty());
				}
			};
			table.addColumn(prodQtyEdit, "#Quantity");
		}
	private void addColumnProductCategory() {
		getColumnProductCategory=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(getColumnProductCategory, "Category");
	}

	private void addColumnProductName() {
		getColumnProductName=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductName();
			}
		};
		table.addColumn(getColumnProductName, "Name");
	}

	private void addColumnProductCode() {
		getColumnProductCode=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductCode();
			}
		};
		table.addColumn(getColumnProductCode, "Code");
	}
	public void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<SalesLineItem, String>(btnCell) {
			@Override
			public String getValue(SalesLineItem object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");

	}
	
	public void createFieldUpdaterDelete() {
		deleteColumn
				.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, SalesLineItem object,
							String value) {
						getDataprovider().getList().remove(object);

						table.redrawRow(index);
					}
				});

	}

	protected void createFieldUpdaterprodQtyColumn() {
		prodQtyEdit.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object, String value) {

				try {
					double val1 = Double.parseDouble(value.trim());
					object.setQuantity(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {

				}

				table.redrawRow(index);
			}
		});
	}

	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
