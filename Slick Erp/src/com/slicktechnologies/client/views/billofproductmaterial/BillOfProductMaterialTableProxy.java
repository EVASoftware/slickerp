package com.slicktechnologies.client.views.billofproductmaterial;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterial;

public class BillOfProductMaterialTableProxy extends SuperTable<BillOfProductMaterial>{

	TextColumn<BillOfProductMaterial>getIdcolumn;
	TextColumn<BillOfProductMaterial>getTitlecolumn;
	TextColumn<BillOfProductMaterial>getProductIdcolumn;
	TextColumn<BillOfProductMaterial>getCodecolumn;
	TextColumn<BillOfProductMaterial>getNamecolumn;
	
	@Override
	public void createTable() {
		addColumngetId();
		addColumngetTitle();
		addColumngetProductId();
		addColumngetCode();
		addColumngetName();
	}

	private void addColumngetCode() {
		
		getCodecolumn=new TextColumn<BillOfProductMaterial>() {
			public String getValue(BillOfProductMaterial object)
			{
				return object.getBomProductCode();
			}
		};
		table.addColumn(getCodecolumn,"Code");
		getCodecolumn.setSortable(true);
	}

	private void addColumngetName() {
		
		getNamecolumn=new TextColumn<BillOfProductMaterial>() {
			public String getValue(BillOfProductMaterial object)
			{
				return object.getBomProductName();
			}
		};
		table.addColumn(getNamecolumn,"Name");
		getNamecolumn.setSortable(true);
	}

	private void addColumngetProductId() {
		
		getProductIdcolumn=new TextColumn<BillOfProductMaterial>() {
			public String getValue(BillOfProductMaterial object)
			{
				return object.getBomProductId()+"";
			}
		};
		table.addColumn(getProductIdcolumn,"Product Id");
		getProductIdcolumn.setSortable(true);
	}
	
	private void addColumngetTitle() {
		
		getTitlecolumn=new TextColumn<BillOfProductMaterial>() {
			public String getValue(BillOfProductMaterial object)
			{
				return object.getBomTitle();
			}
		};
		table.addColumn(getTitlecolumn,"Title");
		getTitlecolumn.setSortable(true);
	}

	private void addColumngetId() {
		
		getIdcolumn=new TextColumn<BillOfProductMaterial>() {
			public String getValue(BillOfProductMaterial object)
			{
				return object.getCount()+"";
			}
		};
		table.addColumn(getIdcolumn,"Id");
		getIdcolumn.setSortable(true);
	}
	
	public void addColumnSorting(){
		addColumngetIdSorting();
		addColumngetTitleSorting();
		addColumnProductIdSorting();
		addColumnCodeSorting();
		addColumnNameSorting();
	}

	
	private void addColumngetIdSorting() {
		List<BillOfProductMaterial> list = getDataprovider().getList();
		columnSort = new ListHandler<BillOfProductMaterial>(list);
		Comparator<BillOfProductMaterial> com = new Comparator<BillOfProductMaterial>() {
			@Override
			public int compare(BillOfProductMaterial e1,BillOfProductMaterial e2) {

				if(e1!=null && e2!=null){
					if(e1.getCount()== e2.getCount())
					{
						return 0;
					}
						if(e1.getCount()> e2.getCount())	
					{
						return 1;
					}
						else
					{
						return -1;
					}}else					
				
						return 0;
					}
			};
		columnSort.setComparator(getIdcolumn, com);
		table.addColumnSortHandler(columnSort);
	}

	
	private void addColumngetTitleSorting() {
		List<BillOfProductMaterial> list = getDataprovider().getList();
		columnSort = new ListHandler<BillOfProductMaterial>(list);
		Comparator<BillOfProductMaterial> com = new Comparator<BillOfProductMaterial>() {
			@Override
			public int compare(BillOfProductMaterial e1,BillOfProductMaterial e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getBomTitle()!=null && e2.getBomTitle()!=null){
						return e1.getBomTitle().compareTo(e2.getBomTitle());}
				}
				else{
				  return 0;
				}
				return 0;
			}
		};
		columnSort.setComparator(getTitlecolumn, com);
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnProductIdSorting() {
		List<BillOfProductMaterial> list = getDataprovider().getList();
		columnSort = new ListHandler<BillOfProductMaterial>(list);
		Comparator<BillOfProductMaterial> com = new Comparator<BillOfProductMaterial>() {
			@Override
			public int compare(BillOfProductMaterial e1,BillOfProductMaterial e2) {

				if(e1!=null && e2!=null){
					if(e1.getBomProductId()== e2.getBomProductId())
					{
						return 0;
					}
						if(e1.getBomProductId()> e2.getBomProductId())	
					{
						return 1;
					}
						else
					{
						return -1;
					}}else					
				
						return 0;
					}
			};
		columnSort.setComparator(getProductIdcolumn, com);
		table.addColumnSortHandler(columnSort);
	}

	
	private void addColumnNameSorting() {
		List<BillOfProductMaterial> list = getDataprovider().getList();
		columnSort = new ListHandler<BillOfProductMaterial>(list);
		Comparator<BillOfProductMaterial> com = new Comparator<BillOfProductMaterial>() {
			@Override
			public int compare(BillOfProductMaterial e1,BillOfProductMaterial e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getBomProductName()!=null && e2.getBomProductName()!=null){
						return e1.getBomProductName().compareTo(e2.getBomProductName());}
				}
				else{
				  return 0;
				}
				return 0;
			}
		};
		columnSort.setComparator(getNamecolumn, com);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnCodeSorting() {
		List<BillOfProductMaterial> list = getDataprovider().getList();
		columnSort = new ListHandler<BillOfProductMaterial>(list);
		Comparator<BillOfProductMaterial> com = new Comparator<BillOfProductMaterial>() {
			@Override
			public int compare(BillOfProductMaterial e1,BillOfProductMaterial e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getBomProductCode()!=null && e2.getBomProductCode()!=null){
						return e1.getBomProductCode().compareTo(e2.getBomProductCode());}
				}
				else{
				  return 0;
				}
				return 0;
			}
		};
		columnSort.setComparator(getCodecolumn, com);
		table.addColumnSortHandler(columnSort);
	}
	
	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
