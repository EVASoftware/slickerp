package com.slicktechnologies.client.views.billofproductmaterial;

import java.util.Vector;

import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterial;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class BillOfProductMaterialSearchProxy extends SearchPopUpScreen<BillOfProductMaterial>{

	ProductInfoComposite pic;
	
	public BillOfProductMaterialSearchProxy(){
		
		super();
	    createGui();
	} 
	
	public BillOfProductMaterialSearchProxy(boolean b){
		
		super(b);
	    createGui();
	}
	public void initWidget(){
		pic=AppUtility.initiatePurchaseProductComposite(new SuperProduct());
	}
	
	public void createScreen(){	
		initWidget();
		
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",pic);
		FormField fpic =builder.setMandatory(false).setRowSpan(0).setColSpan(5).build();

		this.fields= new FormField[][]{
				{},
				{fpic},
	    };
	}
	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
	
			if (!pic.getProdCode().getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(pic.getProdCode().getValue().trim());
				temp.setQuerryString("bomProductCode");
				filtervec.add(temp);
			}

			if (!pic.getProdName().getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(pic.getProdName().getValue().trim());
				temp.setQuerryString("bomProductName");
				filtervec.add(temp);
			}
			if (pic.getIdValue()!=-1){
				temp=new Filter();
				temp.setIntValue(pic.getIdValue());
				temp.setQuerryString("bomProductId");
				filtervec.add(temp);
			}
			
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillOfProductMaterial());
		return querry;
	}

	@Override
	public boolean validate() {
		return true;
	}

}
