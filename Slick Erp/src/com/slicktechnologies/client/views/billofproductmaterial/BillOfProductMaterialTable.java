package com.slicktechnologies.client.views.billofproductmaterial;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterialDetails;

public class BillOfProductMaterialTable extends SuperTable<BillOfProductMaterialDetails>{
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	TextColumn<BillOfProductMaterialDetails> prodId;
	TextColumn<BillOfProductMaterialDetails> prodCode;
	TextColumn<BillOfProductMaterialDetails> prodName;
	TextColumn<BillOfProductMaterialDetails> prodCategory;
	TextColumn<BillOfProductMaterialDetails> prodQty;
	Column<BillOfProductMaterialDetails, String> prodQtyEdit;
	TextColumn<BillOfProductMaterialDetails> prodUOM;
	Column<BillOfProductMaterialDetails, String> deleteColumn;
	
	
	@Override
	public void createTable() {
		addColumnprodID();
		addColumnprodcode();
		addColumnprodname();
		addColumnprodcategory();
		addColumnQty();
		addColumnUnitOfMeasurement();
		createColumndeleteColumn();
		addFieldUpdater();

	}

	public void addeditColumn() {
		addColumnprodID();
		addColumnprodcode();
		addColumnprodname();
		addColumnprodcategory();
		addColumnQty();
		addColumnUnitOfMeasurement();
		createColumndeleteColumn();
		addFieldUpdater();

	}

	public void addViewColumn() {
		addColumnprodID();
		addColumnprodcode();
		addColumnprodname();
		addColumnprodcategory();
		addColumnviewQty();
		addColumnUnitOfMeasurement();
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterprodQtyColumn();
		createFieldUpdaterDelete();
		
	}
	
	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();
	}
	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void applyStyle() {
		
	}
	
/*********************************************Product View Column ********************************************/
	
	public void addColumnprodID() {
		prodId = new TextColumn<BillOfProductMaterialDetails>() {

			@Override
			public String getValue(BillOfProductMaterialDetails object) {
				return object.getProductId() + "";
			}
		};
		table.addColumn(prodId, "ID");
	}
	
	public void addColumnprodcode() {
		prodCode = new TextColumn<BillOfProductMaterialDetails>() {

			@Override
			public String getValue(BillOfProductMaterialDetails object) {
				return object.getProductCode();
			}
		};
		table.addColumn(prodCode, "Code");
	}

	public void addColumnprodname() {
		prodName = new TextColumn<BillOfProductMaterialDetails>() {

			@Override
			public String getValue(BillOfProductMaterialDetails object) {
				return object.getProductName();
			}
		};
		table.addColumn(prodName, "Name");
	}
	
	public void addColumnprodcategory() {
		prodCategory = new TextColumn<BillOfProductMaterialDetails>() {

			@Override
			public String getValue(BillOfProductMaterialDetails object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(prodCategory, "Category");
	}
	
	
	//Read Only Quantity Column
	public void addColumnviewQty() {
		prodQty = new TextColumn<BillOfProductMaterialDetails>() {

			@Override
			public String getValue(BillOfProductMaterialDetails object) {
				return nf.format(object.getProductQty());
			}
		};
		table.addColumn(prodQty, "#Quantity");
	}
	
	//Editable Quantity Column
	public void addColumnQty() {
		EditTextCell editCell = new EditTextCell();
		prodQtyEdit = new Column<BillOfProductMaterialDetails, String>(editCell) {

			@Override
			public String getValue(BillOfProductMaterialDetails object) {
				return nf.format(object.getProductQty());
			}
		};
		table.addColumn(prodQtyEdit, "#Quantity");
	}
	
	protected void addColumnUnitOfMeasurement() {
		prodUOM = new TextColumn<BillOfProductMaterialDetails>() {

			@Override
			public String getValue(BillOfProductMaterialDetails object) {
				if (object.getProductUOM().equals(""))
					return "N/A";
				else
					return object.getProductUOM();
			}
		};
		table.addColumn(prodUOM, "UOM");
	}

	public void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<BillOfProductMaterialDetails, String>(btnCell) {
			@Override
			public String getValue(BillOfProductMaterialDetails object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");

	}
	
	
	public void createFieldUpdaterDelete() {
		deleteColumn
				.setFieldUpdater(new FieldUpdater<BillOfProductMaterialDetails, String>() {
					@Override
					public void update(int index, BillOfProductMaterialDetails object,
							String value) {
						getDataprovider().getList().remove(object);

						table.redrawRow(index);
					}
				});

	}

	protected void createFieldUpdaterprodQtyColumn() {
		prodQtyEdit.setFieldUpdater(new FieldUpdater<BillOfProductMaterialDetails, String>() {
			@Override
			public void update(int index, BillOfProductMaterialDetails object, String value) {

				try {
					double val1 = Double.parseDouble(value.trim());
					object.setProductQty(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {

				}

				table.redrawRow(index);
			}
		});
	}
	
	
}
