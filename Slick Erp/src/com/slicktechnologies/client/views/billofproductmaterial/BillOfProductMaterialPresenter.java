package com.slicktechnologies.client.views.billofproductmaterial;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterial;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class BillOfProductMaterialPresenter extends FormScreenPresenter<BillOfProductMaterial> implements SelectionHandler<Suggestion>{
	
	BillOfProductMaterialForm form;
	
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final GenricServiceAsync async=GWT.create(GenricService.class);
	public BillOfProductMaterialPresenter(UiScreen<BillOfProductMaterial> view,BillOfProductMaterial model) {
		super(view, model);
		form =(BillOfProductMaterialForm)view;
		form.pic.getProdID().addSelectionHandler(this);
		form.pic.getProdName().addSelectionHandler(this);
		form.pic.getProdCode().addSelectionHandler(this);
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.BILLOFPRODUCTMATERIAL,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	public static BillOfProductMaterialForm initialize(){
		
		BillOfProductMaterialForm form = new BillOfProductMaterialForm();
		BillOfProductMaterialTableProxy gentable=new BillOfProductMaterialTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		BillOfProductMaterialSearchProxy.staticSuperTable=gentable;
		BillOfProductMaterialSearchProxy searchpopup=new BillOfProductMaterialSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		BillOfProductMaterialPresenter presenter=new BillOfProductMaterialPresenter(form, new BillOfProductMaterial());		
		AppMemory.getAppMemory().stickPnel(form);
		return form;		 
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initialize();
		}
	}

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}

	@Override
	public void reactOnDownload() {
	CsvServiceAsync async=GWT.create(CsvService.class);
	ArrayList<BillOfProductMaterial> custarray=new ArrayList<BillOfProductMaterial>();
	List<BillOfProductMaterial>list=form.getSearchpopupscreen().getSupertable().getListDataProvider().getList();
	custarray.addAll(list); 
		async.setBillOfProductMaterial(custarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+84;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	@Override
	protected void makeNewModel() {
		model= new BillOfProductMaterial();
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if (event.getSource().equals(form.pic.getProdID())||event.getSource().equals(form.pic.getProdName())||event.getSource().equals(form.pic.getProdCode())) {
			form.retriveProduct();
		}
		
		if(form.table.getDataprovider().getList().size()!=0){
			form.table.connectToLocal();
			/**
			 * Date : 19-05-2017 By ANIL
			 */
			form.manPowerCostTbl.connectToLocal();
			/**
			 * ENd
			 */
		}
	}

}
