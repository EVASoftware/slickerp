package com.slicktechnologies.client.views.billofproductmaterial;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.quotation.coststructure.ManPowerCostStructureTable;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterial;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterialDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.coststructure.EmployeeLevel;
import com.slicktechnologies.shared.common.coststructure.ManPowerCostStructure;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class BillOfProductMaterialForm extends FormScreen<BillOfProductMaterial> implements ClickHandler{
	final GenricServiceAsync async = GWT.create(GenricService.class);
	TextBox ibBOMid;
	TextBox tbBOMtitle;
	ProductInfoComposite pic;
	ProductInfoComposite pic1;
	Button btnAdd;
	CheckBox status;
	BillOfProductMaterialTable table;
	
	/**
	 * Date : 19-05-2017 BY ANIL
	 */
	
	ObjectListBox<EmployeeLevel> olbEmployeeLevel;
	Button btnAddEmpLvl;
	ManPowerCostStructureTable manPowerCostTbl;

	/**
	 * End
	 */
	
	BillOfProductMaterial billofprodMaterialObj;
	
	public BillOfProductMaterialForm() {
		super();
	    createGui();
	    this.ibBOMid.setEnabled(false);
	    table.connectToLocal();
	}
	
	public BillOfProductMaterialForm(String[] processlevel,FormField[][] fields,FormStyle formstyle){
		
		super(processlevel,fields,formstyle);
		createGui();
	}	
	
	private void initalizeWidget(){
		ibBOMid=new TextBox();
		tbBOMtitle=new TextBox();
		status= new CheckBox();
		status.setValue(true);
		pic=AppUtility.initiatePurchaseProductComposite(new SuperProduct());
		pic1=AppUtility.initiatePurchaseProductComposite(new SuperProduct());
		btnAdd = new Button("Add");
		btnAdd.addClickHandler(this);
	     
		table=new BillOfProductMaterialTable();
		
		/**
		 * Date : 19-05-2017 By ANIL
		 */
		olbEmployeeLevel=new ObjectListBox<EmployeeLevel>();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		AppUtility.makeObjectListBoxLive(olbEmployeeLevel, new EmployeeLevel(), filter);
		
		manPowerCostTbl=new ManPowerCostStructureTable();
		btnAddEmpLvl=new Button("Add");
		btnAddEmpLvl.addClickHandler(this);
		
		/**
		 * End
		 */
	}
	

	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.BILLOFPRODUCTMATERIAL,LoginPresenter.currentModule.trim());
	}

	@Override
	public void createScreen() {
		initalizeWidget();	
		
		this.processlevelBarNames = new String[]{"New"};
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fBMaterialGroup=fbuilder.setlabel("Bill Of Material details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Bill Of Material Id",ibBOMid);
    	FormField ftbCount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
    	
    	fbuilder = new FormFieldBuilder("* Bill Of Material Title",tbBOMtitle);
    	FormField ftbtitle= fbuilder.setMandatory(true).setMandatoryMsg("Bill Of Material Title is Mandatory!").setRowSpan(0).setColSpan(1).build();
    	
    	fbuilder = new FormFieldBuilder("",pic);
		FormField fcom =fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
    	
    	fbuilder = new FormFieldBuilder("Status",status);
		FormField fstatus=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		FormField fgroupingMaterialInfo = fbuilder.setlabel("Material Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",pic1);
		FormField fcom1 =fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",btnAdd);
		FormField faddStep= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", table.getTable());
	    FormField fbomtable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	    
	    /**
	     * Date : 19-05-2017 by ANIL
	     */
	    fbuilder = new FormFieldBuilder();
		FormField fManPowerGrouping=fbuilder.setlabel("Man Power Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Employee Level Name",olbEmployeeLevel);
		FormField folbEmployeeLevel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",btnAddEmpLvl);
		FormField fbtnAddEmpLvl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", manPowerCostTbl.getTable());
	    FormField fmanPowerCostTbl = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	    
	    /**
	     * End
	     */
	    
		
		FormField [][] formFields ={{fBMaterialGroup},
	                {ftbCount,ftbtitle,fstatus},
	                {fcom},
	                {fgroupingMaterialInfo},
	                {fcom1,faddStep},
	                {fbomtable},
	                {fManPowerGrouping},
	                {folbEmployeeLevel,fbtnAddEmpLvl},
	                {fmanPowerCostTbl}
	                };
		this.fields=formFields;
	}

	@Override
	public void updateModel(BillOfProductMaterial model) {
		
		if(tbBOMtitle.getValue()!=null){
			model.setBomTitle(tbBOMtitle.getValue());
		}
		if(!pic.getProdID().getValue().equals(""))
		{
			model.setBomProductId(Integer.parseInt(pic.getProdID().getValue()));
		}
		
		if(!pic.getProdCode().getValue().equals(""))
		{
			model.setBomProductCode(pic.getProdCode().getValue());
		}
		
		if(!pic.getProdName().getValue().equals(""))
		{
			model.setBomProductName(pic.getProdName().getValue());
		}
		if(status.getValue()!=null){
			model.setBomStatus(status.getValue());
		}
		if(table.getValue()!=null){
			model.setBomProductList(table.getValue());
		}
		
		/**
		 * Date : 19-05-2017 By ANIL
		 */
		if(manPowerCostTbl.getValue()!=null){
			model.setManPowerCostStructureList(manPowerCostTbl.getValue());
		}
		
		/**
		 * ENd
		 */
		
		billofprodMaterialObj=model;
	}

	@Override
	public void updateView(BillOfProductMaterial view) {
		
		billofprodMaterialObj=view;
		
		ibBOMid.setValue(view.getCount()+"");
		if (view.getBomProductId() != 0){
			pic.getProdID().setValue(String.valueOf(view.getBomProductId()));
		}
		if (view.getBomProductCode() != null){
			pic.getProdCode().setValue(view.getBomProductCode());
		}
		if (view.getBomProductName() != null){
			pic.getProdName().setValue(view.getBomProductName());
		}
		if(view.getBomTitle()!=null)
			tbBOMtitle.setValue(view.getBomTitle());
		status.setValue(view.isBomStatus());
		table.setValue(view.getBomProductList());
		
		if(view.getManPowerCostStructureList()!=null){
			manPowerCostTbl.setValue(view.getManPowerCostStructureList());
		}
	}

	public void setCount(int count)
	{
		ibBOMid.setValue(count+"");
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		this.table.setEnable(state);
		this.pic.setEnable(false);
		this.ibBOMid.setEnabled(false);
		manPowerCostTbl.setEnable(state);
	}
	
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.pic.setEnable(false);
		this.ibBOMid.setEnabled(false);
		this.processLevelBar.setVisibleFalse(false);
	}

	
	@Override
	public void setToViewState() {
		super.setToViewState();
		
		SuperModel model=new BillOfProductMaterial();
		model=billofprodMaterialObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.PRODUCTMODULE,AppConstants.BILLOFMATERIAL, billofprodMaterialObj.getCount(), null,null,null, false, model, null);
	
	}
	
	@Override
	public boolean validate() {
		boolean b = super.validate();
		if (b == false)
			return false;
		if (table.getDataprovider().getList().size() == 0) {
			this.showDialogMessage("Please add atleast one Product");
			return false;
		}
		if(!validateProdQty()){
			showDialogMessage("Please enter appropriate quantity required!");
			return false;
		}
//		if(flag==false){
//			showDialogMessage("Bill Of Material for this is already exist....Please Edit");
//			productval=false;
//		}
		
		/**
		 * Date : 19-05-2017 By ANIL
		 */
		
		if(manPowerCostTbl.getDataprovider().getList().size()!=0){
			boolean oprFlag=false;
			boolean costFlag=false;
			String message="";
			for(ManPowerCostStructure object:manPowerCostTbl.getDataprovider().getList()){
				if(object.getNoOfOprator()==0){
					oprFlag=true;
					message=message+"No. of operator can not be zero.\n";
					break;
				}
			}
			for(ManPowerCostStructure object:manPowerCostTbl.getDataprovider().getList()){
				if(object.getManCostPerHour()==0){
					costFlag=true;
					message=message+"Man hour cost can not be zero.";
					break;
				}
			}
			
			if(oprFlag||costFlag){
				showDialogMessage(message);
				return false;
			}
		}
		
		/**
		 * End
		 */
		
		return true;
	}
	
	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource().equals(btnAdd)){
			addProductToTable();
			pic1.clear();
		}
		
		/**
		 * Date : 19-05-2017 BY ANIL
		 */
		
		if(event.getSource().equals(btnAddEmpLvl)){
			if(pic.getValue()==null){
				showDialogMessage("Please select product first!");
				return;
			}
			if(olbEmployeeLevel.getSelectedIndex()==0){
				showDialogMessage("Please select employee level!");
				return;
			}
			EmployeeLevel employeeLevel=olbEmployeeLevel.getSelectedItem();
			if(manPowerCostTbl.getDataprovider().getList().size()!=0){
				for(ManPowerCostStructure object:manPowerCostTbl.getDataprovider().getList()){
					if(object.getEmployeeLevel().equals(olbEmployeeLevel.getValue())){
						showDialogMessage(olbEmployeeLevel.getValue()+" already added.");
						return;
					}
				}
			}
			
			ManPowerCostStructure manPowCostObj=new ManPowerCostStructure();
			manPowCostObj.setServiceId(Integer.parseInt(pic.getProdID().getValue()));
			manPowCostObj.setServiceName(pic.getProdName().getValue());
			manPowCostObj.setServiceCode(pic.getProdCodeValue());
			manPowCostObj.setEmployeeLevel(employeeLevel.getEmpLvlName());
			manPowCostObj.setNoOfOprator(1);
			manPowCostObj.setManCostPerHour(employeeLevel.getAvgHourlyRate());
			manPowCostObj.setTotalCost(employeeLevel.getAvgHourlyRate());
			manPowerCostTbl.getDataprovider().getList().add(manPowCostObj);
		}
		/**
		 * End
		 */
	}

	private void addProductToTable() {
		if(pic1.getValue()!=null&&pic.getValue()!=null){
			if(validateBOMProduct()){
				if(validateProduct()){
					BillOfProductMaterialDetails bopm=new BillOfProductMaterialDetails();
					bopm.setCompProdId(Integer.parseInt(pic.getProdID().getValue()));
					bopm.setProductId(Integer.parseInt(pic1.getProdID().getValue()));
					bopm.setProductCode(pic1.getProdCode().getValue());
					bopm.setProductName(pic1.getProdName().getValue());
					bopm.setProductCategory(pic1.getProdCategory().getValue());
					bopm.setProductQty(0);
					bopm.setProductPrice(pic1.getProdPrice().getValue());
					bopm.setProductUOM(pic1.getUnitOfmeasuerment().getValue());
					
					table.getDataprovider().getList().add(bopm);
				}
				else{
					showDialogMessage("Can not add Duplicate Product!");
				}
			}
			else{
				showDialogMessage("Can not add Bill of Material Product!");
			}
		}else{
			showDialogMessage("Please Enter Product Details!");
		}
	}

	private boolean validateProduct(){
		if(table.getDataprovider().getList().size()!=0){
			for(int i=0;i<table.getDataprovider().getList().size();i++){
				if(table.getDataprovider().getList().get(i).getProductId()==Integer.parseInt(pic1.getProdID().getValue())){
					return false;
				}
			}
		}
		else{
			return true;
		}
		return true;
	}
	
	
	private boolean validateBOMProduct(){
		if(pic1.getValue()!=null){
				if(Integer.parseInt(pic1.getProdID().getValue())==Integer.parseInt(pic.getProdID().getValue())){
					return false;
				}
		}
		else{
			return true;
		}
		return true;
	}
	
	// ************************************** Validate Product Quantity ***********************************************
	
	public boolean validateProdQty()
	{
		int ctr=0;
		for(int i=0;i<table.getDataprovider().getList().size();i++)
		{
			if(table.getDataprovider().getList().get(i).getProductQty()<=0)
			{
				ctr=ctr+1;
			}
		}
		if(ctr==0){
			return true;
		}
		else{
			return false;
		}
	}
	
	/******************************************* Checking Product Data From DataBase **********************************/
	public void retriveProduct() {
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = new Filter();
		filter.setQuerryString("bomProductId");
		filter.setIntValue(Integer.parseInt(pic.getProdID().getValue()));
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillOfProductMaterial());

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							showDialogMessage("Product Already Exist!");
							pic.clear();
						}
					}
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("No Product Found");
					}
				});
	}
	
	
	
	
}
