package com.slicktechnologies.client.views.accountsdashboardar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogPopUp;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter.PaymentDetailsPresenterTable;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

public class PaymentPresenterTableProxy extends PaymentDetailsPresenterTable implements ClickHandler {TextColumn<CustomerPayment> getpaymentIdColumn;
TextColumn<CustomerPayment> getpaymentDateColumn;
TextColumn<CustomerPayment> getcustomerIdColumn;
TextColumn<CustomerPayment> getcustomerNameColumn;
TextColumn<CustomerPayment> getcustomerCellColumn;
TextColumn<CustomerPayment> getcontractIdColumn;
TextColumn<CustomerPayment> getSalesAmtColumn;
TextColumn<CustomerPayment> getinvoiceAmtColumn;
TextColumn<CustomerPayment> getstatusColumn;
TextColumn<CustomerPayment> getbrahchColumn;

TextColumn<CustomerPayment> getbalanceColumn;

/**
 * Date 09-05-2018
 * Developer : Vijay
 * Des : Follow date button and Communication log
 */
TextColumn<CustomerPayment> getFollowUpDateColumn;
Column<CustomerPayment , String> followUpDateButton;
PopupPanel communicationPanel;
CommunicationLogPopUp communicationLogPopUp = new CommunicationLogPopUp();
CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
int rowIndex ;
CustomerPayment customerPayment;
final GWTCAlert alert = new GWTCAlert(); 
final GWTCGlassPanel glassPanel = new GWTCGlassPanel();
final GenricServiceAsync service = GWT.create(GenricService.class);
static boolean followUpFlag =  false;

/**
 * ends here
 */


public Object getVarRef(String varName)
{
	if(varName.equals("getpaymentIdColumn"))
		return this.getpaymentIdColumn;
	if(varName.equals("getpaymentDateColumn"))
		return this.getpaymentDateColumn;
	if(varName.equals("getcustomerIdColumn"))
		return this.getcustomerIdColumn;
	if(varName.equals("getcustomerNameColumn"))
		return this.getcustomerNameColumn;
	if(varName.equals("getcustomerCellColumn"))
		return this.getcustomerCellColumn;
	if(varName.equals("getcontractIdColumn"))
		return this.getcontractIdColumn;
	if(varName.equals("getSalesAmtColumn"))
		return this.getSalesAmtColumn;
	if(varName.equals("getinvoiceAmtColumn"))
		return this.getinvoiceAmtColumn;
	if(varName.equals("getstatusColumn"))
		return this.getstatusColumn;
	if(varName.equals("getbrahchColumn"))
		return this.getbrahchColumn;
	if(varName.equals("getbalanceColumn"))
		return this.getbalanceColumn;
	/** date 09/05/2018 added by Vijay for followup date**/
	if(varName.equals("getFollowUpDateColumn"))
		return this.getFollowUpDateColumn;
	return null ;
}
public PaymentPresenterTableProxy()
{
	super();
	/** 09/05/2018 added by Vijay for communication log popup **/
	communicationLogPopUp.getBtnOk().addClickHandler(this);
	communicationLogPopUp.getBtnCancel().addClickHandler(this);
	communicationLogPopUp.getBtnAdd().addClickHandler(this);
}

@Override 
public void createTable() {
	addColumngetCount();
	
	/** Date 09/05/2018 added by Vijay for follow-up date button **/
	createColumnFollowupDate();
	updateColumnFollowupDate();
	addColumngetFollowUpDate();
//	addColumngetPaymentDate();
	addColumngetCustomerId();
	addColumngetCustomerName();
	addColumngetCustomerCell();
	addColumngetContractCount();
	addColumngetSalesAmount();
	addColumngetInvoiceAmount();
	addColumngetBalanceAmt();
	addColumngetStatus();
	addColumngetBranch();
	addColumnSorting();
	
}

/**
* Date 09-05-2018
* Developer : Vijay
* Des : Follow date added 
*/

private void addColumngetFollowUpDate() {

	final Date todaysDate=new Date();
	CalendarUtil.addDaysToDate(todaysDate,-1);
	todaysDate.setHours(23);
	todaysDate.setMinutes(59);
	todaysDate.setSeconds(59);
	System.out.println("Todays Date Converted ::: "+todaysDate);
	
	getFollowUpDateColumn=new TextColumn<CustomerPayment>()
	{
		@Override
		public String getCellStyleNames(Context context, CustomerPayment object) {
			if(object.getFollowUpDate()!=null&&(object.getFollowUpDate().before(todaysDate))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Approved")||object.getStatus().equals("Closed")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
		
		@Override
		public String getValue(CustomerPayment object)
		{
			if(object.getFollowUpDate()!=null){
				return AppUtility.parseDate(object.getFollowUpDate());}
			else{
				//object.setFollowUpDate(null);
				return "N.A.";
			}
		}
	};
	table.addColumn(getFollowUpDateColumn,"Follow-up Date");
	getFollowUpDateColumn.setSortable(true);
}


protected void createColumnFollowupDate()
{
	ButtonCell btnCell = new ButtonCell();
  followUpDateButton = new Column<CustomerPayment, String>(btnCell) {
		
		@Override
		public String getValue(CustomerPayment object) {
			// TODO Auto-generated method stub
			return "Follow-up Date";
		}
	};                 
	
	table.addColumn(followUpDateButton, "");
	table.setColumnWidth(followUpDateButton,130,Unit.PX);
	
}


protected void updateColumnFollowupDate()
{
	// TODO Auto-generated method stub
	followUpDateButton.setFieldUpdater(new FieldUpdater<CustomerPayment, String>() {
		
				@Override
				public void update(int index, final CustomerPayment object, String value) {
					
					
					rowIndex=index;
					followUpFlag = true;
					customerPayment= object;
					getLogDetails(object);
				}
			});
	
}

private void getLogDetails(CustomerPayment customerPayment)
{
	glassPanel.show();
	MyQuerry querry = AppUtility.communicationLogQuerry(customerPayment.getCount(), customerPayment.getCompanyId(),AppConstants.ACCOUNTMODULE,AppConstants.PAYMENTDETAILS);
	
	service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		
		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			
			ArrayList<InteractionType> list = new ArrayList<InteractionType>();
			
			for(SuperModel model : result){
				
				InteractionType interactionType = (InteractionType) model;
				list.add(interactionType);
				
			}
			Comparator<InteractionType> comp=new Comparator<InteractionType>() {
				@Override
				public int compare(InteractionType e1, InteractionType e2) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				}
			};
			Collections.sort(list,comp);
           
			glassPanel.hide();
			communicationLogPopUp.getRemark().setValue("");
			communicationLogPopUp.getDueDate().setValue(null);
			communicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
			communicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
			communicationPanel = new PopupPanel(true);
			communicationPanel.add(communicationLogPopUp);
			
			communicationPanel.center();
			communicationPanel.show();
				
		}
		
		@Override
		public void onFailure(Throwable caught) {
			glassPanel.hide();
		}
	});
	
}

/**
* ends here
*/


	private void addColumngetBalanceAmt() {
		getbalanceColumn = new TextColumn<CustomerPayment>() {
			@Override
			public String getValue(CustomerPayment object) {
				return object.getBalanceAmount() + "";
			}
		};
		table.addColumn(getbalanceColumn, "Bal Amt");
		getbalanceColumn.setSortable(true);
	}
	
@Override
protected void initializekeyprovider() {
	keyProvider= new ProvidesKey<CustomerPayment>()
			{
		@Override
		public Object getKey(CustomerPayment item)
		{
			if(item==null)
			{
				return null;
			}
			else
				return item.getId();
		}
			};
}

@Override
public void setEnable(boolean state)
{
}

@Override
public void applyStyle()
{
}

public void addColumnSorting(){
	addSortinggetCount();
	addSortinggetPaymentDate();
	addSortinggetCustomerId();
	addSortinggetCustomerName();
	addSortinggetCustomerCell();
	addSortinggetContractCount();
	addSortinggetSalesAmount();
	addSortinggetInvoiceAmount();
	addSortinggetStatus();
	
	/** date 09/05/2018 added by Vijay for followup date**/
	addSortinggetFollowUpDate();
}
@Override public void addFieldUpdater() {
}



/** date 09/05/2018 added by Vijay for followup date**/
protected void addSortinggetFollowUpDate()
{
	List<CustomerPayment> list=getDataprovider().getList();
	columnSort=new ListHandler<CustomerPayment>(list);
	columnSort.setComparator(getFollowUpDateColumn, new Comparator<CustomerPayment>()
	{
	@Override
	public int compare(CustomerPayment e1,CustomerPayment e2)
	{
	if(e1!=null && e2!=null)
	{
		 if(e1.getFollowUpDate() == null && e2.getFollowUpDate()==null){
		        return 0;
		    }
		    if(e1.getFollowUpDate() == null) return 1;
		    if(e2.getFollowUpDate() == null) return -1;
		    
		    return e1.getFollowUpDate().compareTo(e2.getFollowUpDate());
	}
	return 0;
	}
	});
	table.addColumnSortHandler(columnSort);
}

/**
 * ends here
 */

protected void addColumngetCount()
{
	getpaymentIdColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getCount()+"";
		}
			};
			table.addColumn(getpaymentIdColumn,"ID");
			getpaymentIdColumn.setSortable(true);
}

protected void addSortinggetCount()
  {
  List<CustomerPayment> list=getDataprovider().getList();
  columnSort=new ListHandler<CustomerPayment>(list);
  columnSort.setComparator(getpaymentIdColumn, new Comparator<CustomerPayment>()
  {
  @Override
  public int compare(CustomerPayment e1,CustomerPayment e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCount()== e2.getCount()){
  return 0;}
  if(e1.getCount()> e2.getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }

protected void addColumngetPaymentDate()
{
	final Date todaysDate=new Date();
	CalendarUtil.addDaysToDate(todaysDate,-1);
	todaysDate.setHours(23);
	todaysDate.setMinutes(59);
	todaysDate.setSeconds(59);
	System.out.println("Todays Date Converted ::: "+todaysDate);
	
	getpaymentDateColumn=new TextColumn<CustomerPayment>()
	{
		@Override
		public String getCellStyleNames(Context context, CustomerPayment object) {
			if(object.getPaymentDate()!=null&&(object.getPaymentDate().before(todaysDate))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Approved")||object.getStatus().equals("Closed")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
		
		@Override
		public String getValue(CustomerPayment object)
		{
			return AppUtility.parseDate(object.getPaymentDate());
		}
	};
	table.addColumn(getpaymentDateColumn,"Payment Date");
	getpaymentDateColumn.setSortable(true);
}

protected void addSortinggetPaymentDate()
{
	List<CustomerPayment> list=getDataprovider().getList();
	columnSort=new ListHandler<CustomerPayment>(list);
	columnSort.setComparator(getpaymentDateColumn, new Comparator<CustomerPayment>()
			{
		@Override
		public int compare(CustomerPayment e1,CustomerPayment e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getPaymentDate()!=null && e2.getPaymentDate()!=null){
					return e1.getPaymentDate().compareTo(e2.getPaymentDate());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}


protected void addColumngetCustomerId()
{
	getcustomerIdColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getPersonInfo().getCount()+"";
		}
			};
			table.addColumn(getcustomerIdColumn,"Customer ID");
			getcustomerIdColumn.setSortable(true);
}

protected void addSortinggetCustomerId()
  {
  List<CustomerPayment> list=getDataprovider().getList();
  columnSort=new ListHandler<CustomerPayment>(list);
  columnSort.setComparator(getcustomerIdColumn, new Comparator<CustomerPayment>()
  {
  @Override
  public int compare(CustomerPayment e1,CustomerPayment e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
  return 0;}
  if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }


protected void addColumngetCustomerName()
{
	getcustomerNameColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getPersonInfo().getFullName()+"";
		}
			};
			table.addColumn(getcustomerNameColumn,"Customer Name");
			getcustomerNameColumn.setSortable(true);
}

protected void addSortinggetCustomerName()
{
	List<CustomerPayment> list=getDataprovider().getList();
	columnSort=new ListHandler<CustomerPayment>(list);
	columnSort.setComparator(getcustomerNameColumn, new Comparator<CustomerPayment>()
			{
		@Override
		public int compare(CustomerPayment e1,CustomerPayment e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
					return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCustomerCell()
{
	
	getcustomerCellColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			if(object.getPersonInfo().getCellNumber()==0){
				return 0+"";}
			else{
				return object.getPersonInfo().getCellNumber()+"";}
		}
			};
			table.addColumn(getcustomerCellColumn,"Customer Cell");
			getcustomerCellColumn.setSortable(true);
}

protected void addSortinggetCustomerCell()
{
	List<CustomerPayment> list=getDataprovider().getList();
	columnSort=new ListHandler<CustomerPayment>(list);
	columnSort.setComparator(getcustomerCellColumn, new Comparator<CustomerPayment>()
			{
		@Override
		public int compare(CustomerPayment e1,CustomerPayment e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getPersonInfo().getCellNumber()== e2.getPersonInfo().getCellNumber()){
					return 0;}
				if(e1.getPersonInfo().getCellNumber()> e2.getPersonInfo().getCellNumber()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetContractCount()
{
	getcontractIdColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getContractCount()+"";
		}
			};
			table.addColumn(getcontractIdColumn,"Contract ID");
			getcontractIdColumn.setSortable(true);
}

protected void addSortinggetContractCount()
  {
  List<CustomerPayment> list=getDataprovider().getList();
  columnSort=new ListHandler<CustomerPayment>(list);
  columnSort.setComparator(getcontractIdColumn, new Comparator<CustomerPayment>()
  {
  @Override
  public int compare(CustomerPayment e1,CustomerPayment e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getContractCount()== e2.getContractCount()){
  return 0;}
  if(e1.getContractCount()> e2.getContractCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }


protected void addColumngetSalesAmount()
{
	getSalesAmtColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getTotalSalesAmount()+"";
		}
			};
			table.addColumn(getSalesAmtColumn,"Sales Amount");
			getSalesAmtColumn.setSortable(true);
}

protected void addSortinggetSalesAmount()
  {
  List<CustomerPayment> list=getDataprovider().getList();
  columnSort=new ListHandler<CustomerPayment>(list);
  columnSort.setComparator(getSalesAmtColumn, new Comparator<CustomerPayment>()
  {
  @Override
  public int compare(CustomerPayment e1,CustomerPayment e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getTotalSalesAmount()== e2.getTotalSalesAmount()){
  return 0;}
  if(e1.getTotalSalesAmount()> e2.getTotalSalesAmount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }

protected void addColumngetInvoiceAmount()
{
	getinvoiceAmtColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getInvoiceAmount()+"";
		}
			};
			table.addColumn(getinvoiceAmtColumn,"Invoice Amount");
			getinvoiceAmtColumn.setSortable(true);
}

protected void addSortinggetInvoiceAmount()
  {
  List<CustomerPayment> list=getDataprovider().getList();
  columnSort=new ListHandler<CustomerPayment>(list);
  columnSort.setComparator(getinvoiceAmtColumn, new Comparator<CustomerPayment>()
  {
  @Override
  public int compare(CustomerPayment e1,CustomerPayment e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getTotalBillingAmount()== e2.getTotalBillingAmount()){
  return 0;}
  if(e1.getTotalBillingAmount()> e2.getTotalBillingAmount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }

protected void addColumngetStatus()
{
	getstatusColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getStatus()+"";
		}
			};
			table.addColumn(getstatusColumn,"Status");
			getstatusColumn.setSortable(true);
}


protected void addSortinggetStatus()
{
	List<CustomerPayment> list=getDataprovider().getList();
	columnSort=new ListHandler<CustomerPayment>(list);
	columnSort.setComparator(getstatusColumn, new Comparator<CustomerPayment>()
			{
		@Override
		public int compare(CustomerPayment e1,CustomerPayment e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getStatus()!=null && e2.getStatus()!=null){
					return e1.getStatus().compareTo(e2.getStatus());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}


protected void addColumngetBranch()
{
	getbrahchColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getBranch();
		}
			};
			table.addColumn(getbrahchColumn,"Branch");
			getbrahchColumn.setSortable(true);
}

protected void addSortinggetBranch()
{
	List<CustomerPayment> list=getDataprovider().getList();
	columnSort=new ListHandler<CustomerPayment>(list);
	columnSort.setComparator(getbrahchColumn, new Comparator<CustomerPayment>()
			{
		@Override
		public int compare(CustomerPayment e1,CustomerPayment e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getBranch()!=null && e2.getBranch()!=null){
					return e1.getBranch().compareTo(e2.getBranch());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}

/**
 * Date 09-05-2018
 * Developer : Vijay
 * Des : Follow date added 
 */

@Override
public void onClick(ClickEvent event) {

	if(event.getSource() == communicationLogPopUp.getBtnOk()){
		
		glassPanel.show();
	    List<InteractionType> list = communicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
	   
	    ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
	    interactionlist.addAll(list);
	    
	    boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);
	    
	    if(checkNewInteraction==false){
	    	alert.alert("Please add new interaction details");
	    	glassPanel.hide();
	    }	
	    else{	
	    	
	    	final Date followUpDate = interactionlist.get(interactionlist.size()-1).getInteractionDueDate();
	    	 communicationService.saveCommunicationLog(interactionlist, new AsyncCallback<Void>() {
	    		 
					@Override
					public void onFailure(Throwable caught) {
						alert.alert("Unexpected Error");
						glassPanel.hide();
						LoginPresenter.communicationLogPanel.hide();
					}

					@Override
					public void onSuccess(Void result) {
						alert.alert("Data Save Successfully");
						List<CustomerPayment> list = getDataprovider().getList();
						list.get(rowIndex).setFollowUpDate(followUpDate);
						CustomerPayment CustomerPaymentObj = list.get(rowIndex);
						saveCustomerPayment(CustomerPaymentObj);
						followUpFlag = false;
						table.redraw();
						glassPanel.hide();
						communicationPanel.hide();
					} 
				});
	    }
	   
	}
	if(event.getSource() == communicationLogPopUp.getBtnCancel()){
		followUpFlag = false;
		communicationPanel.hide();
	}
	if(event.getSource() == communicationLogPopUp.getBtnAdd()){
			glassPanel.show();
			String remark = communicationLogPopUp.getRemark().getValue();
			Date dueDate = communicationLogPopUp.getDueDate().getValue();
			String interactiongGroup =null;
			if(communicationLogPopUp.getOblinteractionGroup().getSelectedIndex()!=0)
				interactiongGroup=communicationLogPopUp.getOblinteractionGroup().getValue(communicationLogPopUp.getOblinteractionGroup().getSelectedIndex());
			boolean validationFlag = AppUtility.validateCommunicationlog(remark,dueDate);
			
			List<InteractionType> list = communicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
			/**
			 * Date : 08-11-2017 BY Komal
			 */
			Comparator<InteractionType> comp=new Comparator<InteractionType>() {
				@Override
				public int compare(InteractionType e1, InteractionType e2) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				}
			};
			Collections.sort(list,comp);
            
			
			if(validationFlag){
				InteractionType communicationLog =  AppUtility.getCommunicationLog(AppConstants.ACCOUNTMODULE,AppConstants.PAYMENTDETAILS,customerPayment.getCount(),customerPayment.getEmployee(),remark,dueDate,customerPayment.getPersonInfo(),null,interactiongGroup, customerPayment.getBranch(),"");
				list.add(communicationLog);
				communicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);;
				communicationLogPopUp.getRemark().setValue("");
				communicationLogPopUp.getDueDate().setValue(null);
				communicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
			}
			glassPanel.hide();
	}
}
	
	private void saveCustomerPayment(CustomerPayment CustomerPaymentObj) {
		service.save(CustomerPaymentObj,new AsyncCallback<ReturnFromServer>() {
		@Override
		public void onFailure(Throwable caught) {
			glassPanel.hide();
			
		     alert.alert("An Unexpected error occurred!");
		}
		@Override
		public void onSuccess(ReturnFromServer result) {
			glassPanel.hide();
			
		     alert.alert("Saved successfully!");
		}
	});
	
}
	/**
	 * end here
	 */

}
