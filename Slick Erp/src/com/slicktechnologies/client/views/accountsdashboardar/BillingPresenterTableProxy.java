package com.slicktechnologies.client.views.accountsdashboardar;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter.BillingDetailsPresenterTable;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class BillingPresenterTableProxy extends BillingDetailsPresenterTable {TextColumn<BillingDocument> getbillingIdColumn;
TextColumn<BillingDocument> getbillingDateColumn;
TextColumn<BillingDocument> getcustomerIdColumn;
TextColumn<BillingDocument> getcustomerNameColumn;
TextColumn<BillingDocument> getcustomerCellColumn;
TextColumn<BillingDocument> getcontractIdColumn;
TextColumn<BillingDocument> getSalesAmtColumn;
TextColumn<BillingDocument> getbillingAmtColumn;
TextColumn<BillingDocument> getstatusColumn;
TextColumn<BillingDocument> getbranchColumn;


public Object getVarRef(String varName)
{
	if(varName.equals("getbillingIdColumn"))
		return this.getbillingIdColumn;
	if(varName.equals("getbillingDateColumn"))
		return this.getbillingDateColumn;
	if(varName.equals("getcustomerIdColumn"))
		return this.getcustomerIdColumn;
	if(varName.equals("getcustomerNameColumn"))
		return this.getcustomerNameColumn;
	if(varName.equals("getcustomerCellColumn"))
		return this.getcustomerCellColumn;
	if(varName.equals("getcontractIdColumn"))
		return this.getcontractIdColumn;
	if(varName.equals("getSalesAmtColumn"))
		return this.getSalesAmtColumn;
	if(varName.equals("getbillingAmtColumn"))
		return this.getbillingAmtColumn;
	if(varName.equals("getstatusColumn"))
		return this.getstatusColumn;
	if(varName.equals("getbranchColumn"))
		return this.getbranchColumn;
	
	return null ;
}
public BillingPresenterTableProxy()
{
	super();
}

@Override 
public void createTable() {
	addColumngetCount();
	addColumngetBillingDate();
	addColumngetCustomerId();
	addColumngetCustomerName();
	addColumngetCustomerCell();
	addColumngetContractCount();
	addColumngetSalesAmount();
	addColumngetBillingAmount();
	addColumngetStatus();
	addColumnbranch();
	addColumnSorting();
	
}

@Override
protected void initializekeyprovider() {
	keyProvider= new ProvidesKey<BillingDocument>()
			{
		@Override
		public Object getKey(BillingDocument item)
		{
			if(item==null)
			{
				return null;
			}
			else
				return item.getId();
		}
			};
}

@Override
public void setEnable(boolean state)
{
}

@Override
public void applyStyle()
{
}

public void addColumnSorting(){
	addSortinggetCount();
	addSortinggetBillingDate();
	addSortinggetCustomerId();
	addSortinggetCustomerName();
	addSortinggetCustomerCell();
	addSortinggetContractCount();
	addSortinggetSalesAmount();
	addSortinggetBillingAmount();
	addSortinggetStatus();
}
@Override public void addFieldUpdater() {
}



protected void addColumngetCount()
{
	getbillingIdColumn=new TextColumn<BillingDocument>()
			{
		@Override
		public String getValue(BillingDocument object)
		{
			return object.getCount()+"";
		}
			};
			table.addColumn(getbillingIdColumn,"ID");
			getbillingIdColumn.setSortable(true);
}

protected void addSortinggetCount()
  {
  List<BillingDocument> list=getDataprovider().getList();
  columnSort=new ListHandler<BillingDocument>(list);
  columnSort.setComparator(getbillingIdColumn, new Comparator<BillingDocument>()
  {
  @Override
  public int compare(BillingDocument e1,BillingDocument e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCount()== e2.getCount()){
  return 0;}
  if(e1.getCount()> e2.getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }

protected void addColumngetBillingDate()
{
	final Date todaysDate=new Date();
	CalendarUtil.addDaysToDate(todaysDate,-1);
	todaysDate.setHours(23);
	todaysDate.setMinutes(59);
	todaysDate.setSeconds(59);
	System.out.println("Todays Date Converted ::: "+todaysDate);
	
	getbillingDateColumn=new TextColumn<BillingDocument>()
	{
		@Override
		public String getCellStyleNames(Context context, BillingDocument object) {
			if(object.getBillingDate()!=null&&(object.getBillingDate().before(todaysDate))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Approved")||object.getStatus().equals("Invoiced")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
		
		@Override
		public String getValue(BillingDocument object)
		{
			if(object.getBillingDate()!=null){
				return AppUtility.parseDate(object.getBillingDate());
			}
			return null;
		}
	};
	table.addColumn(getbillingDateColumn,"Billing Date");
	getbillingDateColumn.setSortable(true);
}

protected void addSortinggetBillingDate()
{
	List<BillingDocument> list=getDataprovider().getList();
	columnSort=new ListHandler<BillingDocument>(list);
	columnSort.setComparator(getbillingDateColumn, new Comparator<BillingDocument>()
			{
		@Override
		public int compare(BillingDocument e1,BillingDocument e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getBillingDate()!=null && e2.getBillingDate()!=null){
					return e1.getBillingDate().compareTo(e2.getBillingDate());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}


protected void addColumngetCustomerId()
{
	getcustomerIdColumn=new TextColumn<BillingDocument>()
			{
		@Override
		public String getValue(BillingDocument object)
		{
			return object.getPersonInfo().getCount()+"";
		}
			};
			table.addColumn(getcustomerIdColumn,"Customer ID");
			getcustomerIdColumn.setSortable(true);
}

protected void addSortinggetCustomerId()
  {
  List<BillingDocument> list=getDataprovider().getList();
  columnSort=new ListHandler<BillingDocument>(list);
  columnSort.setComparator(getcustomerIdColumn, new Comparator<BillingDocument>()
  {
  @Override
  public int compare(BillingDocument e1,BillingDocument e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
  return 0;}
  if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }


protected void addColumngetCustomerName()
{
	getcustomerNameColumn=new TextColumn<BillingDocument>()
			{
		@Override
		public String getValue(BillingDocument object)
		{
			return object.getPersonInfo().getFullName()+"";
		}
			};
			table.addColumn(getcustomerNameColumn,"Customer Name");
			getcustomerNameColumn.setSortable(true);
}

protected void addSortinggetCustomerName()
{
	List<BillingDocument> list=getDataprovider().getList();
	columnSort=new ListHandler<BillingDocument>(list);
	columnSort.setComparator(getcustomerNameColumn, new Comparator<BillingDocument>()
			{
		@Override
		public int compare(BillingDocument e1,BillingDocument e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
					return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCustomerCell()
{
	
	getcustomerCellColumn=new TextColumn<BillingDocument>()
			{
		@Override
		public String getValue(BillingDocument object)
		{
			if(object.getPersonInfo().getCellNumber()==0){
				return 0+"";}
			else{
				return object.getPersonInfo().getCellNumber()+"";}
		}
			};
			table.addColumn(getcustomerCellColumn,"Customer Cell");
			getcustomerCellColumn.setSortable(true);
}

protected void addSortinggetCustomerCell()
{
	List<BillingDocument> list=getDataprovider().getList();
	columnSort=new ListHandler<BillingDocument>(list);
	columnSort.setComparator(getcustomerCellColumn, new Comparator<BillingDocument>()
			{
		@Override
		public int compare(BillingDocument e1,BillingDocument e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getPersonInfo().getCellNumber()== e2.getPersonInfo().getCellNumber()){
					return 0;}
				if(e1.getPersonInfo().getCellNumber()> e2.getPersonInfo().getCellNumber()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetContractCount()
{
	getcontractIdColumn=new TextColumn<BillingDocument>()
			{
		@Override
		public String getValue(BillingDocument object)
		{
			return object.getContractCount()+"";
		}
			};
			table.addColumn(getcontractIdColumn,"Contract ID");
			getcontractIdColumn.setSortable(true);
}

protected void addSortinggetContractCount()
  {
  List<BillingDocument> list=getDataprovider().getList();
  columnSort=new ListHandler<BillingDocument>(list);
  columnSort.setComparator(getcontractIdColumn, new Comparator<BillingDocument>()
  {
  @Override
  public int compare(BillingDocument e1,BillingDocument e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getContractCount()== e2.getContractCount()){
  return 0;}
  if(e1.getContractCount()> e2.getContractCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }


protected void addColumngetSalesAmount()
{
	getSalesAmtColumn=new TextColumn<BillingDocument>()
			{
		@Override
		public String getValue(BillingDocument object)
		{
			return object.getTotalSalesAmount()+"";
		}
			};
			table.addColumn(getSalesAmtColumn,"Sales Amount");
			getSalesAmtColumn.setSortable(true);
}

protected void addSortinggetSalesAmount()
  {
  List<BillingDocument> list=getDataprovider().getList();
  columnSort=new ListHandler<BillingDocument>(list);
  columnSort.setComparator(getSalesAmtColumn, new Comparator<BillingDocument>()
  {
  @Override
  public int compare(BillingDocument e1,BillingDocument e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getTotalSalesAmount()== e2.getTotalSalesAmount()){
  return 0;}
  if(e1.getTotalSalesAmount()> e2.getTotalSalesAmount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }

protected void addColumngetBillingAmount()
{
	getbillingAmtColumn=new TextColumn<BillingDocument>()
			{
		@Override
		public String getValue(BillingDocument object)
		{
			return object.getTotalBillingAmount()+"";
		}
			};
			table.addColumn(getbillingAmtColumn,"Billing Amount");
			getbillingAmtColumn.setSortable(true);
}

protected void addSortinggetBillingAmount()
  {
  List<BillingDocument> list=getDataprovider().getList();
  columnSort=new ListHandler<BillingDocument>(list);
  columnSort.setComparator(getbillingAmtColumn, new Comparator<BillingDocument>()
  {
  @Override
  public int compare(BillingDocument e1,BillingDocument e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getTotalBillingAmount()== e2.getTotalBillingAmount()){
  return 0;}
  if(e1.getTotalBillingAmount()> e2.getTotalBillingAmount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }


protected void addColumnbranch() {
	getbranchColumn=new TextColumn<BillingDocument>()
			{
		@Override
		public String getValue(BillingDocument object)
		{
			return object.getBranch();
		}
			};
			table.addColumn(getbranchColumn,"Branch");
			getbranchColumn.setSortable(true);
}

protected void addSortinggetBranch()
{
	List<BillingDocument> list=getDataprovider().getList();
	columnSort=new ListHandler<BillingDocument>(list);
	columnSort.setComparator(getbranchColumn, new Comparator<BillingDocument>()
			{
		@Override
		public int compare(BillingDocument e1,BillingDocument e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getBranch()!=null && e2.getBranch()!=null){
					return e1.getBranch().compareTo(e2.getBranch());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}



protected void addColumngetStatus()
{
	getstatusColumn=new TextColumn<BillingDocument>()
			{
		@Override
		public String getValue(BillingDocument object)
		{
			return object.getStatus()+"";
		}
			};
			table.addColumn(getstatusColumn,"Status");
			getstatusColumn.setSortable(true);
}


protected void addSortinggetStatus()
{
	List<BillingDocument> list=getDataprovider().getList();
	columnSort=new ListHandler<BillingDocument>(list);
	columnSort.setComparator(getstatusColumn, new Comparator<BillingDocument>()
			{
		@Override
		public int compare(BillingDocument e1,BillingDocument e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getStatus()!=null && e2.getStatus()!=null){
					return e1.getStatus().compareTo(e2.getStatus());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}}
