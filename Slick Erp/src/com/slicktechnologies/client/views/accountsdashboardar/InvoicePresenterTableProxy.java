package com.slicktechnologies.client.views.accountsdashboardar;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter.InvoiceDetailsPresenterTable;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class InvoicePresenterTableProxy extends InvoiceDetailsPresenterTable {TextColumn<Invoice> getinvoiceIdColumn;
TextColumn<Invoice> getinvoiceDateColumn;
TextColumn<Invoice> getinvoiceTypeColumn;
TextColumn<Invoice> getcustomerIdColumn;
TextColumn<Invoice> getcustomerNameColumn;
TextColumn<Invoice> getcustomerCellColumn;
TextColumn<Invoice> getcontractIdColumn;
TextColumn<Invoice> getSalesAmtColumn;
TextColumn<Invoice> getinvoiceAmtColumn;
TextColumn<Invoice> getstatusColumn;
TextColumn<Invoice> getbranchColumn;


public Object getVarRef(String varName)
{
	if(varName.equals("getinvoiceIdColumn"))
		return this.getinvoiceIdColumn;
	if(varName.equals("getinvoiceDateColumn"))
		return this.getinvoiceDateColumn;
	if(varName.equals("getinvoiceTypeColumn"))
		return this.getinvoiceTypeColumn;
	if(varName.equals("getcustomerIdColumn"))
		return this.getcustomerIdColumn;
	if(varName.equals("getcustomerNameColumn"))
		return this.getcustomerNameColumn;
	if(varName.equals("getcustomerCellColumn"))
		return this.getcustomerCellColumn;
	if(varName.equals("getcontractIdColumn"))
		return this.getcontractIdColumn;
	if(varName.equals("getSalesAmtColumn"))
		return this.getSalesAmtColumn;
	if(varName.equals("getinvoiceAmtColumn"))
		return this.getinvoiceAmtColumn;
	if(varName.equals("getstatusColumn"))
		return this.getstatusColumn;
	if(varName.equals("getbranchColumn"))
		return this.getbranchColumn;
	
	return null ;
}
public InvoicePresenterTableProxy()
{
	super();
}

@Override 
public void createTable() {
	addColumngetCount();
	addColumngetInvoiceDate();
	addColumngetInvoiceType();
	addColumngetCustomerId();
	addColumngetCustomerName();
	addColumngetCustomerCell();
	addColumngetContractCount();
	addColumngetSalesAmount();
	addColumngetInvoiceAmount();
	addColumngetStatus();
	addColumngetBranch();
	addColumnSorting();
}



@Override
public void setEnable(boolean state)
{
}

@Override
public void applyStyle()
{
}

public void addColumnSorting(){
	addSortinggetCount();
	addSortinggetInvoiceDate();
	addSortinggetInvoiceType();
	addSortinggetCustomerId();
	addSortinggetCustomerName();
	addSortinggetCustomerCell();
	addSortinggetContractCount();
	addSortinggetSalesAmount();
	addSortinggetInvoiceAmount();
	addSortinggetStatus();
	addSortinggetBranch();
	
}

@Override
protected void initializekeyprovider() {
	keyProvider= new ProvidesKey<Invoice>()
			{
		@Override
		public Object getKey(Invoice item)
		{
			if(item==null)
			{
				return null;
			}
			else
				return item.getId();
		}
			};
}

@Override public void addFieldUpdater() {
}

protected void addColumngetCount()
{
	getinvoiceIdColumn=new TextColumn<Invoice>()
			{
		@Override
		public String getValue(Invoice object)
		{
			return object.getCount()+"";
		}
			};
			table.addColumn(getinvoiceIdColumn,"ID");
			getinvoiceIdColumn.setSortable(true);
}

protected void addSortinggetCount()
  {
  List<Invoice> list=getDataprovider().getList();
  columnSort=new ListHandler<Invoice>(list);
  columnSort.setComparator(getinvoiceIdColumn, new Comparator<Invoice>()
  {
  @Override
  public int compare(Invoice e1,Invoice e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCount()== e2.getCount()){
  return 0;}
  if(e1.getCount()> e2.getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }

protected void addColumngetInvoiceDate()
{
	final Date todaysDate=new Date();
	CalendarUtil.addDaysToDate(todaysDate,-1);
	todaysDate.setHours(23);
	todaysDate.setMinutes(59);
	todaysDate.setSeconds(59);
	System.out.println("Todays Date Converted ::: "+todaysDate);
	getinvoiceDateColumn=new TextColumn<Invoice>()
	{
		@Override
		public String getCellStyleNames(Context context, Invoice object) {
			if(object.getInvoiceDate()!=null&&(object.getInvoiceDate().before(todaysDate))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Approved")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
		
		@Override
		public String getValue(Invoice object)
		{
			return AppUtility.parseDate(object.getInvoiceDate());
		}
	};
	table.addColumn(getinvoiceDateColumn,"Invoice Date");
	getinvoiceDateColumn.setSortable(true);
}

protected void addSortinggetInvoiceDate()
{
	List<Invoice> list=getDataprovider().getList();
	columnSort=new ListHandler<Invoice>(list);
	columnSort.setComparator(getinvoiceDateColumn, new Comparator<Invoice>()
			{
		@Override
		public int compare(Invoice e1,Invoice e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getInvoiceDate()!=null && e2.getInvoiceDate()!=null){
					return e1.getInvoiceDate().compareTo(e2.getInvoiceDate());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetInvoiceType()
{
	getinvoiceTypeColumn=new TextColumn<Invoice>()
			{
		@Override
		public String getValue(Invoice object)
		{
			return object.getInvoiceType();
		}
			};
			table.addColumn(getinvoiceTypeColumn,"Invoice Type");
			getinvoiceTypeColumn.setSortable(true);
}

protected void addSortinggetInvoiceType()
{
	List<Invoice> list=getDataprovider().getList();
	columnSort=new ListHandler<Invoice>(list);
	columnSort.setComparator(getinvoiceTypeColumn, new Comparator<Invoice>()
			{
		@Override
		public int compare(Invoice e1,Invoice e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getInvoiceType()!=null && e2.getInvoiceType()!=null){
					return e1.getInvoiceType().compareTo(e2.getInvoiceType());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCustomerId()
{
	getcustomerIdColumn=new TextColumn<Invoice>()
			{
		@Override
		public String getValue(Invoice object)
		{
			return object.getPersonInfo().getCount()+"";
		}
			};
			table.addColumn(getcustomerIdColumn,"Customer ID");
			getcustomerIdColumn.setSortable(true);
}

protected void addSortinggetCustomerId()
  {
  List<Invoice> list=getDataprovider().getList();
  columnSort=new ListHandler<Invoice>(list);
  columnSort.setComparator(getcustomerIdColumn, new Comparator<Invoice>()
  {
  @Override
  public int compare(Invoice e1,Invoice e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
  return 0;}
  if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }


protected void addColumngetCustomerName()
{
	getcustomerNameColumn=new TextColumn<Invoice>()
			{
		@Override
		public String getValue(Invoice object)
		{
			return object.getPersonInfo().getFullName()+"";
		}
			};
			table.addColumn(getcustomerNameColumn,"Customer Name");
			getcustomerNameColumn.setSortable(true);
}

protected void addSortinggetCustomerName()
{
	List<Invoice> list=getDataprovider().getList();
	columnSort=new ListHandler<Invoice>(list);
	columnSort.setComparator(getcustomerNameColumn, new Comparator<Invoice>()
			{
		@Override
		public int compare(Invoice e1,Invoice e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
					return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCustomerCell()
{
	
	getcustomerCellColumn=new TextColumn<Invoice>()
			{
		@Override
		public String getValue(Invoice object)
		{
			if(object.getPersonInfo().getCellNumber()==0){
				return 0+"";}
			else{
				return object.getPersonInfo().getCellNumber()+"";}
		}
			};
			table.addColumn(getcustomerCellColumn,"Customer Cell");
			getcustomerCellColumn.setSortable(true);
}

protected void addSortinggetCustomerCell()
{
	List<Invoice> list=getDataprovider().getList();
	columnSort=new ListHandler<Invoice>(list);
	columnSort.setComparator(getcustomerCellColumn, new Comparator<Invoice>()
			{
		@Override
		public int compare(Invoice e1,Invoice e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getPersonInfo().getCellNumber()== e2.getPersonInfo().getCellNumber()){
					return 0;}
				if(e1.getPersonInfo().getCellNumber()> e2.getPersonInfo().getCellNumber()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetContractCount()
{
	getcontractIdColumn=new TextColumn<Invoice>()
			{
		@Override
		public String getValue(Invoice object)
		{
			return object.getContractCount()+"";
		}
			};
			table.addColumn(getcontractIdColumn,"Contract ID");
			getcontractIdColumn.setSortable(true);
}

protected void addSortinggetContractCount()
  {
  List<Invoice> list=getDataprovider().getList();
  columnSort=new ListHandler<Invoice>(list);
  columnSort.setComparator(getcontractIdColumn, new Comparator<Invoice>()
  {
  @Override
  public int compare(Invoice e1,Invoice e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getContractCount()== e2.getContractCount()){
  return 0;}
  if(e1.getContractCount()> e2.getContractCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }


protected void addColumngetSalesAmount()
{
	getSalesAmtColumn=new TextColumn<Invoice>()
			{
		@Override
		public String getValue(Invoice object)
		{
			return object.getTotalSalesAmount()+"";
		}
			};
			table.addColumn(getSalesAmtColumn,"Sales Amount");
			getSalesAmtColumn.setSortable(true);
}

protected void addSortinggetSalesAmount()
  {
  List<Invoice> list=getDataprovider().getList();
  columnSort=new ListHandler<Invoice>(list);
  columnSort.setComparator(getSalesAmtColumn, new Comparator<Invoice>()
  {
  @Override
  public int compare(Invoice e1,Invoice e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getTotalSalesAmount()== e2.getTotalSalesAmount()){
  return 0;}
  if(e1.getTotalSalesAmount()> e2.getTotalSalesAmount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }

protected void addColumngetInvoiceAmount()
{
	getinvoiceAmtColumn=new TextColumn<Invoice>()
			{
		@Override
		public String getValue(Invoice object)
		{
			return object.getInvoiceAmount()+"";
		}
			};
			table.addColumn(getinvoiceAmtColumn,"Invoice Amount");
			getinvoiceAmtColumn.setSortable(true);
}

protected void addSortinggetInvoiceAmount()
  {
  List<Invoice> list=getDataprovider().getList();
  columnSort=new ListHandler<Invoice>(list);
  columnSort.setComparator(getinvoiceAmtColumn, new Comparator<Invoice>()
  {
  @Override
  public int compare(Invoice e1,Invoice e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getInvoiceAmount()== e2.getInvoiceAmount()){
  return 0;}
  if(e1.getInvoiceAmount()> e2.getInvoiceAmount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }


protected void addColumngetBranch()
{
	getbranchColumn=new TextColumn<Invoice>()
			{
		@Override
		public String getValue(Invoice object)
		{
			return object.getBranch();
		}
			};
			table.addColumn(getbranchColumn,"Branch");
			getbranchColumn.setSortable(true);
}


protected void addSortinggetBranch()
{
	List<Invoice> list=getDataprovider().getList();
	columnSort=new ListHandler<Invoice>(list);
	columnSort.setComparator(getbranchColumn, new Comparator<Invoice>()
			{
		@Override
		public int compare(Invoice e1,Invoice e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getBranch()!=null && e2.getBranch()!=null){
					return e1.getBranch().compareTo(e2.getBranch());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}


protected void addColumngetStatus()
{
	getstatusColumn=new TextColumn<Invoice>()
			{
		@Override
		public String getValue(Invoice object)
		{
			return object.getStatus()+"";
		}
			};
			table.addColumn(getstatusColumn,"Status");
			getstatusColumn.setSortable(true);
}


protected void addSortinggetStatus()
{
	List<Invoice> list=getDataprovider().getList();
	columnSort=new ListHandler<Invoice>(list);
	columnSort.setComparator(getstatusColumn, new Comparator<Invoice>()
			{
		@Override
		public int compare(Invoice e1,Invoice e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getStatus()!=null && e2.getStatus()!=null){
					return e1.getStatus().compareTo(e2.getStatus());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}}
