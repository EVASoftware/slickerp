package com.slicktechnologies.client.views.customerDetails;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Quotation;

public class QuotationTableProxy extends SuperTable<ContractTableValueProxy>{

	
	TextColumn<ContractTableValueProxy> getCountColumn;
	TextColumn<ContractTableValueProxy> getCreationDateColumn;
	TextColumn<ContractTableValueProxy> getValidUntilDateColumn;
	TextColumn<ContractTableValueProxy> getStatusColumn;
	@Override
	public void createTable() {
		
		addColumngetCount();
		addColumngetCreationDate();
//		addColumngetValiduntil();
		addColumngetStatus();
	}

	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<ContractTableValueProxy>()
				{
			@Override
			public String getValue(ContractTableValueProxy object)
			{
				if( object.getQuotationId()==-1)
					return "N.A";
				else return object.getQuotationId()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				table.setColumnWidth(getCountColumn,100,Unit.PX);
				getCountColumn.setSortable(true);
	}
	
	protected void addColumngetCreationDate()
	{
		final Date sub7Days=new Date();
		System.out.println("Todays Date ::: "+sub7Days);
		CalendarUtil.addDaysToDate(sub7Days,-7);
		System.out.println("Date - 7 Days ::: "+sub7Days);
		sub7Days.setHours(23);
		sub7Days.setMinutes(59);
		sub7Days.setSeconds(59);
		System.out.println("Converted Date ::: "+sub7Days);
		
		getCreationDateColumn=new TextColumn<ContractTableValueProxy>()
		{
			@Override
			public String getCellStyleNames(Context context, ContractTableValueProxy object) {
				 if(object.getQuotationDate().before(sub7Days)&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
					 return "red";
				 }
				 else if(object.getStatus().equals("Successful")||object.getStatus().equals("UnSuccessful")||object.getStatus().equals("Approved")){
					 return "green";
				 }
				 else{
					 return "black";
				 }
			}
			@Override
			public String getValue(ContractTableValueProxy object)
			{
				if(object.getQuotationDate()!=null)
				     return  AppUtility.parseDate(object.getQuotationDate());
				  else 
					  return "N.A";
				
			}
		};
		table.addColumn(getCreationDateColumn,"Creation Date");
		table.setColumnWidth(getCreationDateColumn,120,Unit.PX);
		getCreationDateColumn.setSortable(true);
	}
	
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<ContractTableValueProxy>()
				{
			@Override
			public String getValue(ContractTableValueProxy object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				table.setColumnWidth(getStatusColumn,90,Unit.PX);
				getStatusColumn.setSortable(true);
	}
	
	
	protected void addColumngetValiduntil()
	{
		getValidUntilDateColumn=new TextColumn<ContractTableValueProxy>()
				{
			@Override
			public String getValue(ContractTableValueProxy object)
			{
				if(object.getValidUntil()!=null)
				     return  AppUtility.parseDate(object.getValidUntil());
				  else 
					  return " ";
				
			}
				};
				table.addColumn(getValidUntilDateColumn,"Valid Until");
				table.setColumnWidth(getValidUntilDateColumn,120,Unit.PX);
				getValidUntilDateColumn.setSortable(true);
	}
	
	
	@Override
	protected void initializekeyprovider(){
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}

	/**
	 * Date 05-08-2017 BY ANIL
	 */
	@Override
	public void addColumnSorting() {
		// TODO Auto-generated method stub
		super.addColumnSorting();
		
		addColumnSortingCount();
		addColumnSortingCreationDate();
		addColumnSortingStatus();
	}

	private void addColumnSortingCount() {
		List<ContractTableValueProxy> list = getDataprovider().getList();
		columnSort = new ListHandler<ContractTableValueProxy>(list);
		columnSort.setComparator(getCountColumn, new Comparator<ContractTableValueProxy>() {
			@Override
			public int compare(ContractTableValueProxy e1, ContractTableValueProxy e2) {
				if (e1 != null && e2 != null) {
					if (e1.getQuotationId() == e2.getQuotationId()) {
						return 0;
					}
					if (e1.getQuotationId() > e2.getQuotationId()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingCreationDate() {
		List<ContractTableValueProxy> list = getDataprovider().getList();
		columnSort = new ListHandler<ContractTableValueProxy>(list);
		columnSort.setComparator(getCreationDateColumn, new Comparator<ContractTableValueProxy>() {
			@Override
			public int compare(ContractTableValueProxy e1, ContractTableValueProxy e2) {
				if (e1 != null && e2 != null) {
					if (e1.getQuotationDate() != null&& e2.getQuotationDate() != null) {
						return e1.getQuotationDate().compareTo(e2.getQuotationDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingStatus() {
		List<ContractTableValueProxy> list = getDataprovider().getList();
		columnSort = new ListHandler<ContractTableValueProxy>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<ContractTableValueProxy>() {
			@Override
			public int compare(ContractTableValueProxy e1, ContractTableValueProxy e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null&& e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}
	
	/**
	 * ENd
	 */
	
	
	

}
