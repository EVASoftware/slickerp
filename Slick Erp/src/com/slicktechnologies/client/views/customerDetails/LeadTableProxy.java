package com.slicktechnologies.client.views.customerDetails;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;

public class LeadTableProxy extends SuperTable<ContractTableValueProxy>{

	
	TextColumn<ContractTableValueProxy> getLeadIdColumn;
	TextColumn<ContractTableValueProxy> getLeadCreationDateColumn;
	TextColumn<ContractTableValueProxy> getLeadTitleColumn;
	TextColumn<ContractTableValueProxy> getstatusColumn;
	
	@Override
	public void createTable(){
		
		addColumngetCount();
		addColumngetCreationDate();
		addColumngetTitle();
		
	}
	
	protected void addColumngetCount()
	{
		getLeadIdColumn=new TextColumn<ContractTableValueProxy>()
				{
			@Override
			public String getValue(ContractTableValueProxy object)
			{
				return object.getLeadID()+"";
			}
				};
				table.addColumn(getLeadIdColumn,"ID");
				table.setColumnWidth(getLeadIdColumn, 100,Unit.PX);
				getLeadIdColumn.setSortable(true);
	}
	
	
	protected void addColumngetTitle()
	{
		getLeadTitleColumn=new TextColumn<ContractTableValueProxy>()
				{
			@Override
			public String getValue(ContractTableValueProxy object)
			{
				return object.getLeadTitle();
			}
				};
				table.addColumn(getLeadTitleColumn,"Lead Title");
				table.setColumnWidth(getLeadTitleColumn, 120,Unit.PX);
				getLeadTitleColumn.setSortable(true);
	}
	
	
	protected void addColumngetStatus()
	{
		getstatusColumn=new TextColumn<ContractTableValueProxy>()
				{
			@Override
			public String getValue(ContractTableValueProxy object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(getstatusColumn,"Status");
				table.setColumnWidth(getstatusColumn, 90,Unit.PX);
				getstatusColumn.setSortable(true);
	}
	
	protected void addColumngetCreationDate()
	{
		DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
		DatePickerCell date= new DatePickerCell(fmt);
		
		final Date sub7Days=new Date();
		System.out.println("Todays Date ::: "+sub7Days);
		CalendarUtil.addDaysToDate(sub7Days,-7);
		System.out.println("Date - 7 Days ::: "+sub7Days);
		sub7Days.setHours(23);
		sub7Days.setMinutes(59);
		sub7Days.setSeconds(59);
		System.out.println("Converted Date ::: "+sub7Days);
		
		getLeadCreationDateColumn=new TextColumn<ContractTableValueProxy>()
		{
			@Override
			public String getCellStyleNames(Context context, ContractTableValueProxy object) {
				 if(object.getLeadDate().before(sub7Days)&&object.getStatus().equals("Created")){
					 return "red";
				 }
				 else if(object.getStatus().equals("Closed")||object.getStatus().equals("Successful")||object.getStatus().equals("Unsuccessful")){
					 return "green";
				 }
				 else{
					 return "black";
				 }
			}
			
			@Override
			public String getValue(ContractTableValueProxy object)
			{
				if(object.getLeadDate()!=null){
					return AppUtility.parseDate(object.getLeadDate());}
				else{
					object.setLeadDate(new Date());
					return " ";
				}
			}
		};
		table.addColumn(getLeadCreationDateColumn,"Creation Date");
		table.setColumnWidth(getLeadCreationDateColumn, 120,Unit.PX);
		getLeadCreationDateColumn.setSortable(true);
	}

	@Override
	protected void initializekeyprovider(){
		
	}

	@Override
	public void addFieldUpdater(){
		
	}

	@Override
	public void setEnable(boolean state){
		
	}

	@Override
	public void applyStyle(){
		
	}

	/**
	 * Date : 05-08-2017 By ANIL
	 */
	
	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		
		addColumnSortingCount();
		addColumnSortingCreationDate();
		addColumnSortingTitle();
	}

	private void addColumnSortingCount() {
		List<ContractTableValueProxy> list = getDataprovider().getList();
		columnSort = new ListHandler<ContractTableValueProxy>(list);
		columnSort.setComparator(getLeadIdColumn, new Comparator<ContractTableValueProxy>() {
			@Override
			public int compare(ContractTableValueProxy e1, ContractTableValueProxy e2) {
				if (e1 != null && e2 != null) {
					if (e1.getLeadID() == e2.getLeadID()) {
						return 0;
					}
					if (e1.getLeadID() > e2.getLeadID()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortingCreationDate() {
		List<ContractTableValueProxy> list = getDataprovider().getList();
		columnSort = new ListHandler<ContractTableValueProxy>(list);
		columnSort.setComparator(getLeadCreationDateColumn, new Comparator<ContractTableValueProxy>() {
			@Override
			public int compare(ContractTableValueProxy e1, ContractTableValueProxy e2) {
				if (e1 != null && e2 != null) {
					if (e1.getLeadDate() != null&& e2.getLeadDate() != null) {
						return e1.getLeadDate().compareTo(e2.getLeadDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingTitle() {
		List<ContractTableValueProxy> list = getDataprovider().getList();
		columnSort = new ListHandler<ContractTableValueProxy>(list);
		columnSort.setComparator(getLeadTitleColumn, new Comparator<ContractTableValueProxy>() {
			@Override
			public int compare(ContractTableValueProxy e1, ContractTableValueProxy e2) {
				if (e1 != null && e2 != null) {
					if (e1.getLeadTitle() != null&& e2.getLeadTitle() != null) {
						return e1.getLeadTitle().compareTo(e2.getLeadTitle());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}
	
	
	/**
	 * End
	 */

	
}
