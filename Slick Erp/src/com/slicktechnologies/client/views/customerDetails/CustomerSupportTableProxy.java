package com.slicktechnologies.client.views.customerDetails;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.complain.Complain;

public class CustomerSupportTableProxy extends SuperTable<Complain>{
	
	TextColumn<Complain> getContractId;
	TextColumn<Complain> getServiceID;
	TextColumn<Complain> getTicketNumber;
	TextColumn<Complain> getTicketDate;
	TextColumn<Complain> getTitle;
	TextColumn<Complain> getStatus;
	
	@Override
	public void createTable() {
		
		addColumngetContractId();
		addColumngetServiceID();
		addColumngetTicketNumber();
		addColumngetTicketDate();
		addColumngetTitle();
		addColumngetStatus();
	}

	private void addColumngetContractId() {
		
		getContractId = new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				if(object.getContrtactId()!=null)
					return object.getContrtactId()+"";
				else
					return "";
			}
		};
		table.addColumn(getContractId, "Contract ID");
		getContractId.setSortable(true);
		table.setColumnWidth(getContractId, 80, Unit.PX);
	}

	private void addColumngetServiceID() {
		
		getServiceID = new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				if(object.getServiceId()!=null)
				return object.getServiceId()+"";
				else
					return "";
			}
		};
		table.addColumn(getServiceID, "Service ID");
		getServiceID.setSortable(true);
		table.setColumnWidth(getServiceID, 80, Unit.PX);
	}

	private void addColumngetTicketNumber() {
		
		getTicketNumber = new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getTicketNumber, "Ticket No");
		getTicketNumber.setSortable(true);
		table.setColumnWidth(getTicketNumber, 83, Unit.PX);
	}

	private void addColumngetTicketDate() {
		
		getTicketDate = new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				if(object.getDate()!=null)
					return AppUtility.parseDate(object.getDate());
				else
					return "";
			}
		};
		table.addColumn(getTicketDate, "Ticket Dt");
		getTicketDate.setSortable(true);
		table.setColumnWidth(getTicketDate, 80, Unit.PX);
	}

	private void addColumngetTitle() {
		
		getTitle = new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				return object.getComplainTitle();
			}
		};
		table.addColumn(getTitle, "Title");
		getTitle.setSortable(true);
		table.setColumnWidth(getTitle, 80, Unit.PX);
	}

	private void addColumngetStatus() {

		getStatus = new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				return object.getCompStatus();
			}
		};
		table.addColumn(getStatus, "Status");
		getStatus.setSortable(true);
		table.setColumnWidth(getStatus, 80, Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	
	
	/**
	 * Date :05-08-2017 By Anil
	 */
	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		
		addColumnSortingContractId();
		addColumnSortingServiceID();
		addColumnSortingTicketNumber();
		addColumnSortingTicketDate();
		addColumnSortingTitle();
		addColumnSortingStatus();
	}

	private void addColumnSortingContractId() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
					if(e1.getContrtactId()== e2.getContrtactId()){
					  return 0;}
				  if(e1.getContrtactId()> e2.getContrtactId()){
					  return 1;}
				  else{
					  return -1;}} else
					return 0;
			}
		};
		columnSort.setComparator(getContractId, quote);
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingServiceID() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
					if(e1.getServiceId()== e2.getServiceId()){
					  return 0;}
				  if(e1.getServiceId()> e2.getServiceId()){
					  return 1;}
				  else{
					  return -1;}} else
					return 0;
			}
		};
		columnSort.setComparator(getServiceID, quote);
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingTicketNumber() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
					if(e1.getCount()== e2.getCount()){
					  return 0;}
				  if(e1.getCount()> e2.getCount()){
					  return 1;}
				  else{
					  return -1;}} else
					return 0;
			}
		};
		columnSort.setComparator(getTicketNumber, quote);
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingTicketDate() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
					if (e1.getDate() != null&& e2.getDate() != null)
						return e1.getDate().compareTo(e2.getDate());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getTicketDate, quote);
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingTitle() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
					if (e1.getComplainTitle() != null&& e2.getComplainTitle() != null)
						return e1.getComplainTitle().compareTo(e2.getComplainTitle());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getTitle, quote);
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingStatus() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCompStatus() != null&& e2.getCompStatus() != null)
						return e1.getCompStatus().compareTo(e2.getCompStatus());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getStatus, quote);
		table.addColumnSortHandler(columnSort);
		
	}
	
	/**
	 * End
	 */
	
}
