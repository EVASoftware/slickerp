package com.slicktechnologies.client.views.customerDetails;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;

public class ServiceTableProxy extends SuperTable<Service>{
	
	TextColumn<Service> getContractCountColumn;
	TextColumn<Service> getServiceCountColumn;
	TextColumn<Service> getCreationDateColumn;
	TextColumn<Service> getCompletionDateColumn;
	TextColumn<Service> getStatusColumn;
	
	TextColumn<Service> getServiceAddressColumn;
	
	@Override
	public void createTable(){

		addColumngetServiceCount();
		addColumngetContractCount();
		addColumngetCreationDate();
		addColumngetCompletionDate();
		addColumngetStatus();
		addColumngetServiceAddress();
	}
	
	/**
	 * Date 11 April 2017
	 * added by vijay for Service Address
	 * requirement from PCAAMB
	 */
	private void addColumngetServiceAddress() {

		getServiceAddressColumn = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getAddress()!=null){
					return object.getAddress().getCompleteAddress();
				}else{
					return "";
				}
			}
		};
		table.addColumn(getServiceAddressColumn,"Service Address");
		table.setColumnWidth(getServiceAddressColumn, 300,Unit.PX);
		getServiceAddressColumn.setCellStyleNames("");
	}
	
	/**
	 * ends here
	 */

	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				return object.getStatus();
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
		table.setColumnWidth(getStatusColumn, 90, Unit.PX);

	}
	
	protected void addColumngetCreationDate() {
		

		getCreationDateColumn = new TextColumn<Service>() {
			@Override
			public String getCellStyleNames(Context context, Service object) {
				
				
				Date todaysDate=CalendarUtil.copyDate(new Date());
				
				CalendarUtil.addDaysToDate(todaysDate,-7);
				todaysDate.setHours(23);
				todaysDate.setMinutes(59);
				todaysDate.setSeconds(59);
				
				if((object.getServiceDate().before(new Date()))&&(object.getStatus().equals("Scheduled"))){
					return "red";
				 }
				
				 else if((object.getServiceDate().after(todaysDate)&&object.getServiceDate().before(new Date()))&&(object.getStatus().equals("Scheduled")))
				 {
					 return "orange";
				 }
				 else if(object.getStatus().equals("Completed")){
					 return "green";
				 }
				 else if(object.getStatus().trim().equals("Rescheduled")){
					 return "blue";
				 }
				 else
				{
					return "black";
				}
				
			}

			@Override
			public String getValue(Service object) {
				if (object.getServiceDate() != null)
					return AppUtility.parseDate(object.getServiceDate());
				else
					return " ";
			}
		};
		table.addColumn(getCreationDateColumn, "Service Date");
		table.setColumnWidth(getCreationDateColumn, 120, Unit.PX);
		getCreationDateColumn.setSortable(true);
	}
	
	
	
			protected void addColumngetCompletionDate() {

					getCompletionDateColumn = new TextColumn<Service>() {
							@Override
							public String getValue(Service object) {
								if (object.getServiceCompletionDate() != null)
									return AppUtility.parseDate(object.getServiceCompletionDate());
								else
									return " ";
							}
						};
						table.addColumn(getCompletionDateColumn, "Completion Date");
						table.setColumnWidth(getCompletionDateColumn, 120, Unit.PX);
						getCompletionDateColumn.setSortable(true);
	}
	
	
	protected void addColumngetContractCount()
	 {
		 getContractCountColumn=new TextColumn<Service>()
		 {
			 @Override
			 public String getValue(Service object)
			 {
				 if( object.getContractCount()==-1)
					 return "  ";
				 else return object.getContractCount()+"";
			 }
		 };
		 table.addColumn(getContractCountColumn,"Contract Id");
		 table.setColumnWidth(getContractCountColumn,90,Unit.PX);
		 getContractCountColumn.setSortable(true);
	 }
	
	protected void addColumngetServiceCount() {
		getServiceCountColumn = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				if (object.getCount()== -1)
					return " ";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getServiceCountColumn, "Service ID");

		table.setColumnWidth(getServiceCountColumn, 100,Unit.PX);
		getServiceCountColumn.setSortable(true);
	}

	
	
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Date : 05-08-2017 BY ANIL
	 */
	
	@Override
	public void addColumnSorting() {
		// TODO Auto-generated method stub
		super.addColumnSorting();
		
		addColumnSortingServiceCount();
		addColumnSortingContractCount();
		addColumnSortingCreationDate();
		addColumnSortingCompletionDate();
		addColumnSortingStatus();
		addColumnSortingServiceAddress();
	}

	private void addColumnSortingServiceCount() {
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(getServiceCountColumn, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingContractCount() {
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(getContractCountColumn, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);	
		
	}

	private void addColumnSortingCreationDate() {
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(getCreationDateColumn, new Comparator<Service>() {
			
			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getServiceDate()!=null && e2.getServiceDate()!=null){
						return e1.getServiceDate().compareTo(e2.getServiceDate());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingCompletionDate() {
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(getCompletionDateColumn, new Comparator<Service>() {
			
			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getServiceDate()!=null && e2.getServiceDate()!=null){
						return e1.getServiceDate().compareTo(e2.getServiceDate());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingStatus() {
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Service>() {
			
			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingServiceAddress() {
		List<Service> list=getDataprovider().getList();
		columnSort=new ListHandler<Service>(list);
		columnSort.setComparator(getServiceAddressColumn, new Comparator<Service>() {
			
			@Override
			public int compare(Service e1, Service e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getAddress().getCompleteAddress()!=null && e2.getAddress().getCompleteAddress()!=null){
						return e1.getAddress().getCompleteAddress().compareTo(e2.getAddress().getCompleteAddress());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}
	
	/**
	 * ENd
	 */
	

}
