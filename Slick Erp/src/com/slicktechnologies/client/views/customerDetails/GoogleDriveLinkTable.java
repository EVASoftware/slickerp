package com.slicktechnologies.client.views.customerDetails;
import java.util.Comparator;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.GoogleDriveLink;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
public class GoogleDriveLinkTable extends SuperTable<GoogleDriveLink>{


	TextColumn<GoogleDriveLink> getDateColumn;
	TextColumn<GoogleDriveLink> getDocumentTypeColumn;
	TextColumn<GoogleDriveLink> getGoogleDriveLinkColumn;
	
	TextColumn<GoogleDriveLink> getRemarkColumn;
	TextColumn<GoogleDriveLink> getCreationDateColumn;
	TextColumn<GoogleDriveLink> getCreatedByColumn;
	
	Column<GoogleDriveLink, String> deleteColumn;
	Column<GoogleDriveLink, String> viewDocumentColumn;
	protected GWTCGlassPanel glassPanel;
	public final GenricServiceAsync service=GWT.create(GenricService.class);
	
	@Override
	public void createTable() {
		getDateColumn();
		getDocumentTypeColumn();
		getGoogleDriveLinkColumn();
		getRemarkColumn();
		getCreationDateColumn();
		getCreatedByColumn();
		viewDocumentColumn();
		
		if(LoginPresenter.loggedInCustomer==false){
		createColumndeleteColumn();
		createFieldUpdaterDelete();
		}
		createFieldUpdaterViewDocument();
	}

	private void createFieldUpdaterViewDocument() {
		viewDocumentColumn.setFieldUpdater(new FieldUpdater<GoogleDriveLink, String>() {
			@Override
			public void update(int index, GoogleDriveLink object, String value) {
				 final String url = object.getGoogleDriveLink();
		 			Window.open(url, "test", "enabled");
			}

			
		});
	}

	private void viewDocumentColumn() {
		ButtonCell btnCell = new ButtonCell();
		viewDocumentColumn = new Column<GoogleDriveLink, String>(btnCell) {
			@Override
			public String getValue(GoogleDriveLink object) {
				return "View Document";
			}
		};
		table.addColumn(viewDocumentColumn, "View Document");
		table.setColumnWidth(viewDocumentColumn,120,Unit.PX);
		
	}

	private void createFieldUpdaterDelete() {
		deleteColumn.setFieldUpdater(new FieldUpdater<GoogleDriveLink, String>() {
			@Override
			public void update(int index, GoogleDriveLink object, String value) {
//				getDataprovider().getList().remove(object);
//				table.redrawRow(index);
				deleteGoogleDriveLink(object,index);
			}
		});
	}
	
	private void deleteGoogleDriveLink(final GoogleDriveLink object,final int index) {
		for(int i=0;i<CustomerDetailsPresenter.customer.getGoogleDriveLinkList().size();i++){
			if(object.getGoogleDriveLink().equals(CustomerDetailsPresenter.customer.getGoogleDriveLinkList().get(i).getGoogleDriveLink())){
				CustomerDetailsPresenter.customer.getGoogleDriveLinkList().remove(i);
				glassPanel=new GWTCGlassPanel();
				glassPanel.show();
				service.save(CustomerDetailsPresenter.customer,new AsyncCallback<ReturnFromServer>() {
					@Override
					public void onFailure(Throwable caught) {
						glassPanel.hide();
						final GWTCAlert alert = new GWTCAlert(); 
					     alert.alert("An Unexpected error occurred!");
					}
					@Override
					public void onSuccess(ReturnFromServer result) {
						glassPanel.hide();
						
						getDataprovider().setList(CustomerDetailsPresenter.customer.getGoogleDriveLinkList());
//						final GWTCAlert alert = new GWTCAlert(); 
//					     alert.alert("Saved successfully!");
						
					}
				});
				
				
				
				break;
			}
		}
	}

	private void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<GoogleDriveLink, String>(btnCell) {
			@Override
			public String getValue(GoogleDriveLink object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn,81,Unit.PX);
		
	}

	private void getRemarkColumn() {
		getRemarkColumn=new TextColumn<GoogleDriveLink>() {
			@Override
			public String getValue(GoogleDriveLink object) {
				if(object.getRemark()!=null){
					return object.getRemark();
				}else{
					return "";
				}
					
			}
		};
		table.addColumn(getRemarkColumn, "Remark");
		table.setColumnWidth(getRemarkColumn, 180, Unit.PX);
		getRemarkColumn.setSortable(true);
		
	}

	private void getCreationDateColumn() {
		getCreationDateColumn=new TextColumn<GoogleDriveLink>() {
			@Override
			public String getValue(GoogleDriveLink object) {
					return AppUtility.parseDate(object.getCreationDate());
			}
		};
		table.addColumn(getCreationDateColumn, "Creation Date");
		table.setColumnWidth(getCreationDateColumn, 140, Unit.PX);
		getCreationDateColumn.setSortable(true);
		
	}

	private void getCreatedByColumn() {
		getCreatedByColumn=new TextColumn<GoogleDriveLink>() {
			@Override
			public String getValue(GoogleDriveLink object) {
					return object.getCreatedBy();
			}
		};
		table.addColumn(getCreatedByColumn, "Created By");
		table.setColumnWidth(getCreatedByColumn, 90, Unit.PX);
		getCreatedByColumn.setSortable(true);
		
	}

	private void getDateColumn() {
		getDateColumn=new TextColumn<GoogleDriveLink>() {
			@Override
			public String getValue(GoogleDriveLink object) {
				if (object.getDate() != null)
					return AppUtility.parseDate(object.getDate());
				else
					return " ";
			}
		};
		table.addColumn(getDateColumn, "Date");
		table.setColumnWidth(getDateColumn, 100, Unit.PX);
		getDateColumn.setSortable(true);
	}

	private void getDocumentTypeColumn() {
		getDocumentTypeColumn=new TextColumn<GoogleDriveLink>() {
			@Override
			public String getValue(GoogleDriveLink object) {
				if (object.getDocumentType() != null)
					return object.getDocumentType() ;
				else
					return " ";
			}
		};
		table.addColumn(getDocumentTypeColumn, "Document Type");
		table.setColumnWidth(getDocumentTypeColumn, 120, Unit.PX);
		getDocumentTypeColumn.setSortable(true);
	}

	private void getGoogleDriveLinkColumn() {
		getGoogleDriveLinkColumn=new TextColumn<GoogleDriveLink>() {
			@Override
			public String getValue(GoogleDriveLink object) {
				if (object.getGoogleDriveLink() != null)
					return object.getGoogleDriveLink() ;
				else
					return " ";
			}
		};
		table.addColumn(getGoogleDriveLinkColumn, "Googl Drive Link");
		table.setColumnWidth(getGoogleDriveLinkColumn, 200, Unit.PX);
		getGoogleDriveLinkColumn.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Date : 05-08-2017 By ANIL
	 */

	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		
		getSortingDateColumn();
		getSortingDocumentTypeColumn();
		getSortingGoogleDriveLinkColumn();
		getSortingRemarkColumn();
		getSortingCreationDateColumn();
		getSortingCreatedByColumn();
	}

	private void getSortingDateColumn() {
		List<GoogleDriveLink> list=getDataprovider().getList();
		columnSort=new ListHandler<GoogleDriveLink>(list);
		columnSort.setComparator(getDateColumn, new Comparator<GoogleDriveLink>()
				{
			@Override
			public int compare(GoogleDriveLink e1,GoogleDriveLink e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDate()!=null && e2.getDate()!=null){
						return e1.getDate().compareTo(e2.getDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void getSortingDocumentTypeColumn() {
		List<GoogleDriveLink> list=getDataprovider().getList();
		columnSort=new ListHandler<GoogleDriveLink>(list);
		columnSort.setComparator(getDocumentTypeColumn, new Comparator<GoogleDriveLink>()
				{
			@Override
			public int compare(GoogleDriveLink e1,GoogleDriveLink e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDocumentType()!=null && e2.getDocumentType()!=null){
						return e1.getDocumentType().compareTo(e2.getDocumentType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void getSortingGoogleDriveLinkColumn() {
		List<GoogleDriveLink> list=getDataprovider().getList();
		columnSort=new ListHandler<GoogleDriveLink>(list);
		columnSort.setComparator(getGoogleDriveLinkColumn, new Comparator<GoogleDriveLink>()
				{
			@Override
			public int compare(GoogleDriveLink e1,GoogleDriveLink e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getGoogleDriveLink()!=null && e2.getGoogleDriveLink()!=null){
						return e1.getGoogleDriveLink().compareTo(e2.getGoogleDriveLink());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void getSortingRemarkColumn() {
		List<GoogleDriveLink> list=getDataprovider().getList();
		columnSort=new ListHandler<GoogleDriveLink>(list);
		columnSort.setComparator(getRemarkColumn, new Comparator<GoogleDriveLink>()
				{
			@Override
			public int compare(GoogleDriveLink e1,GoogleDriveLink e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getRemark()!=null && e2.getRemark()!=null){
						return e1.getRemark().compareTo(e2.getRemark());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void getSortingCreationDateColumn() {
		List<GoogleDriveLink> list=getDataprovider().getList();
		columnSort=new ListHandler<GoogleDriveLink>(list);
		columnSort.setComparator(getCreationDateColumn, new Comparator<GoogleDriveLink>()
				{
			@Override
			public int compare(GoogleDriveLink e1,GoogleDriveLink e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
						return e1.getCreationDate().compareTo(e2.getCreationDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void getSortingCreatedByColumn() {
		List<GoogleDriveLink> list=getDataprovider().getList();
		columnSort=new ListHandler<GoogleDriveLink>(list);
		columnSort.setComparator(getCreatedByColumn, new Comparator<GoogleDriveLink>()
				{
			@Override
			public int compare(GoogleDriveLink e1,GoogleDriveLink e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreatedBy()!=null && e2.getCreatedBy()!=null){
						return e1.getCreatedBy().compareTo(e2.getCreatedBy());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}
	
	/**
	 * End
	 */
	
	
	
}
