package com.slicktechnologies.client.views.customerDetails;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

public class PaymentTableProxy  extends SuperTable<CustomerPayment>{

	TextColumn<CustomerPayment> getcontractIdColumn;
	TextColumn<CustomerPayment> getpaymentIdColumn;
	TextColumn<CustomerPayment> getpaymentDateColumn;
	TextColumn<CustomerPayment> getpayOrderAmtColumn;
	TextColumn<CustomerPayment> getBalanceAmtColumn;
	TextColumn<CustomerPayment> getstatusColumn;
	TextColumn<CustomerPayment> getamountrecivedColumn;
	
    /** date 13/11/2017 added by komal for invoice amount **/
	TextColumn<CustomerPayment> getColumnInvoiceAmtColumn;
	
	 /** date 9-4-2018  
	  * added by jayshree for payment method **/
		TextColumn<CustomerPayment> getPaymentMethodColumn;
	@Override
	public void createTable() {
		
		addColumngetContractCount();
		addColumngetCount();
		addColumngetPaymentDate();
		/** date 9-4-2018  
		  * added by jayshree for payment method **/
		addColumngetPaymentmethod();
		//addColumngetPayOrderAmt();
		addColumnInvoiceAmount();
		addColumngetamountrecived();
		addColumngetBalanceAmt();
		addColumngetStatus();
	}
	
	private void addColumngetPaymentmethod() {
		
		getPaymentMethodColumn=new TextColumn<CustomerPayment>()
				{
			@Override
			public String getValue(CustomerPayment object)
			{
				return object.getPaymentMethod()+"";
			}
				};
				table.addColumn(getPaymentMethodColumn,"Payment Method");
				getPaymentMethodColumn.setSortable(true);
	}

	private void addColumngetPayOrderAmt() {
		
		getpayOrderAmtColumn=new TextColumn<CustomerPayment>()
				{
			@Override
			public String getValue(CustomerPayment object)
			{
				return object.getTotalSalesAmount()+"";
			}
				};
				table.addColumn(getpayOrderAmtColumn,"Order Amt");
				getpayOrderAmtColumn.setSortable(true);
	}



	private void addColumngetBalanceAmt() {
		
		getBalanceAmtColumn=new TextColumn<CustomerPayment>()
				{
			@Override
			public String getValue(CustomerPayment object)
			{
				return object.getBalanceAmount()+"";
			}
				};
				table.addColumn(getBalanceAmtColumn,"Bal Amt");
				getBalanceAmtColumn.setSortable(true);
	}



	protected void addColumngetContractCount()
	{
		getcontractIdColumn=new TextColumn<CustomerPayment>()
				{
			@Override
			public String getValue(CustomerPayment object)
			{
				return object.getContractCount()+"";
			}
				};
				table.addColumn(getcontractIdColumn,"Contract ID");
				getcontractIdColumn.setSortable(true);
	}
	
	protected void addColumngetCount()
	{
		getpaymentIdColumn=new TextColumn<CustomerPayment>()
				{
			@Override
			public String getValue(CustomerPayment object)
			{
				return object.getCount()+"";
			}
				};
				table.addColumn(getpaymentIdColumn,"ID");
				getpaymentIdColumn.setSortable(true);
	}
	
	
	
	protected void addColumngetPaymentDate()
	{
				
		getpaymentDateColumn=new TextColumn<CustomerPayment>()
		{
			@Override
			public String getCellStyleNames(Context context, CustomerPayment object) {
				
//				Date todaysDate=CalendarUtil.copyDate(object.getPaymentDate());
//				CalendarUtil.addDaysToDate(todaysDate,1);
//				todaysDate.setHours(23);
//				todaysDate.setMinutes(59);
//				todaysDate.setSeconds(59);
//				
//				Date dates=CalendarUtil.copyDate(object.getPaymentDate());
//				CalendarUtil.addDaysToDate(dates,-7);
//				dates.setHours(23);
//				dates.setMinutes(59);
//				dates.setSeconds(59);
//				
//				
//				final Date yestDate=CalendarUtil.copyDate(new Date());
//				CalendarUtil.addDaysToDate(yestDate,-1);
//				yestDate.setHours(23);
//				yestDate.setMinutes(59);
//				yestDate.setSeconds(59);
//				System.out.println("Todays Date Converted ::: "+yestDate);
//				
//				if((new Date().after(todaysDate))&&(object.getStatus().equals("Created"))){
//					return "red";
//				 }
//				
//				 else if((new Date().after(dates)&&object.getPaymentDate().before(yestDate))&&(object.getStatus().equals("Created")))
//				 {
//					 return "orange";
//				 }
//				 else if(object.getStatus().equals("Closed")){
//					 return "green";
//				 }
//				 else
//				 {
//					 return "black";	 
//				 }
				
				
				Date todaysDate=CalendarUtil.copyDate(new Date());
				
				CalendarUtil.addDaysToDate(todaysDate,-7);
				todaysDate.setHours(23);
				todaysDate.setMinutes(59);
				todaysDate.setSeconds(59);
				
				if((object.getPaymentDate().before(new Date()))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
					return "red";
				 }
				
				 else if((object.getPaymentDate().after(todaysDate)&&object.getPaymentDate().before(new Date()))&&(object.getStatus().equals("Requested")))
				 {
					 return "orange";
				 }
				 else if(object.getStatus().equals("Approved")){
					 return "green";
				 }
				 else
					 {
						 return "black";	 
					 }
				
			}
			
			@Override
			public String getValue(CustomerPayment object)
			{
				return AppUtility.parseDate(object.getPaymentDate());
			}
		};
		table.addColumn(getpaymentDateColumn,"Payment Dt");
		getpaymentDateColumn.setSortable(true);
	}
	
	
	protected void addColumngetStatus()
	{
		getstatusColumn=new TextColumn<CustomerPayment>()
				{
			@Override
			public String getValue(CustomerPayment object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(getstatusColumn,"Status");
				getstatusColumn.setSortable(true);
	}

/**
 * Date 04/09/2017 By jayshree changes make to add 
 * the Amount Received column in the customer help desk payment table  
 */
	private void addColumngetamountrecived() 
	{
		getamountrecivedColumn=new TextColumn<CustomerPayment>()
				{
			@Override
			public String getValue(CustomerPayment object)
			{
				return object.getPaymentReceived()+"";
			}
				};
				table.addColumn(getamountrecivedColumn,"Amount Received");
				getamountrecivedColumn.setSortable(true);
		
	}
	
	///*************************************************************
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		addSortingOnContractCount();
		addSortingOnCount();
		addSortingOnPaymentDate();
		/** date 9-4-2018  
		  * added by jayshree for payment method **/
		addSortingOnPaymentMethod();
		addSortingOnPayOrderAmt();
		addSortingOnBalanceAmt();
		/** **/
		addSortingOnInvoiceAmt();
		addSortingOnStatus();
	}



	private void addSortingOnPaymentMethod() {
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getPaymentMethodColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPaymentMethod()!=null && e2.getPaymentMethod()!=null){
						return e1.getPaymentMethod().compareTo(e2.getPaymentMethod());
					}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnContractCount() {
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getcontractIdColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}



	private void addSortingOnCount() {
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getpaymentIdColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnPaymentDate() {
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getpaymentDateColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPaymentDate()!=null && e2.getPaymentDate()!=null){
						return e1.getPaymentDate().compareTo(e2.getPaymentDate());
					}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnPayOrderAmt() {
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getpayOrderAmtColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getTotalSalesAmount()== e2.getTotalSalesAmount()){
						return 0;}
					if(e1.getTotalSalesAmount()> e2.getTotalSalesAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnBalanceAmt() {
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getBalanceAmtColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPaymentAmt()== e2.getPaymentAmt()){
						return 0;}
					if(e1.getPaymentAmt()> e2.getPaymentAmt()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnStatus() {
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getstatusColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());
					}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}
	
	/** date 13/11/2017 added by komal for invoice amount **/
	public void addColumnInvoiceAmount()
	{
		getColumnInvoiceAmtColumn=new TextColumn<CustomerPayment>() {

			@Override
			public String getValue(CustomerPayment object) {
				return object.getInvoiceAmount()+"";
			}
		};
		
		table.addColumn(getColumnInvoiceAmtColumn,"Invoice Amt");
		getColumnInvoiceAmtColumn.setSortable(true);
	}
	
	public void addSortingOnInvoiceAmt(){
		List<CustomerPayment> list=getDataprovider().getList();
		columnSort=new ListHandler<CustomerPayment>(list);
		columnSort.setComparator(getColumnInvoiceAmtColumn, new Comparator<CustomerPayment>()
				{
			@Override
			public int compare(CustomerPayment e1,CustomerPayment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getInvoiceAmount()== e2.getInvoiceAmount()){
						return 0;}
					if(e1.getInvoiceAmount()> e2.getInvoiceAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}

}
