package com.slicktechnologies.client.views.customerDetails;

import java.util.Date;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;

public class InteractionToDoTableProxy extends SuperTable<InteractionType>{

	TextColumn<InteractionType> getColumnInteractionId;
	TextColumn<InteractionType> getColumnDueDate;
	TextColumn<InteractionType> getColumnPurpose;
	TextColumn<InteractionType> getColumnOutcome;
	TextColumn<InteractionType> getColumnModuleName;
	TextColumn<InteractionType> getColumnDocumentName;
	TextColumn<InteractionType> getColumnCreatedBy;
	TextColumn<InteractionType> getColumnStatus;
 	
	 public InteractionToDoTableProxy() {
		 super();
	 }
	
	@Override
	public void createTable() {

		createColumnInteractionId();
		createColumnDueDate();
		createColumnPurpose();
		createColumnOutcome();
		createColumnDocumentName();
		createColumnCreatedBy();
		createColumnStatus();
	}


	private void createColumnStatus() {
		getColumnStatus = new TextColumn<InteractionType>() {
			
			@Override
			public String getValue(InteractionType object) {
					return object.getInteractionStatus();
			}
		};	
		table.addColumn(getColumnStatus,"Status");
		table.setColumnWidth(getColumnStatus, 90,Unit.PX);
	}

	private void createColumnInteractionId() {

		getColumnInteractionId = new TextColumn<InteractionType>() {
			
			@Override
			public String getValue(InteractionType object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getColumnInteractionId,"ID");
		table.setColumnWidth(getColumnInteractionId, 90,Unit.PX);
	}


	private void createColumnDueDate() {

		final Date todaydate = new Date();
		CalendarUtil.addDaysToDate(todaydate,-1);
		todaydate.setHours(23);
		todaydate.setMinutes(59);
		todaydate.setSeconds(59);
		
		getColumnDueDate = new TextColumn<InteractionType>() {
			
			@Override
			public String getCellStyleNames(Context context,
					InteractionType object) {
				if(todaydate.after(object.getInteractionDueDate())){
					return "red";
				}
				else{
					return "black";
				}
			}

			@Override
			public String getValue(InteractionType object) {
				if(object.getInteractionDueDate()!=null){
					return AppUtility.parseDate(object.getInteractionDueDate());
				}
				return "";
			}
		};
		table.addColumn(getColumnDueDate,"Due Date");
		table.setColumnWidth(getColumnDueDate, 90,Unit.PX);
	}


	private void createColumnPurpose() {

		getColumnPurpose = new TextColumn<InteractionType>() {
			
			@Override
			public String getValue(InteractionType object) {
				if(object.getInteractionPurpose()!=null){
					return object.getInteractionPurpose();
				}
				return null;
			}
		};
		table.addColumn(getColumnPurpose,"Purpose");
		table.setColumnWidth(getColumnPurpose, 200,Unit.PX);
	}


	private void createColumnOutcome() {

		getColumnOutcome = new TextColumn<InteractionType>() {
			
			@Override
			public String getValue(InteractionType object) {
				if(object.getInteractionOutcome()!=null){
					return object.getInteractionOutcome();
				}
				return "";
			}
		};
		table.addColumn(getColumnOutcome,"Outcome");
		table.setColumnWidth(getColumnOutcome, 100,Unit.PX);
	}


	private void createColumnDocumentName() {

		getColumnDocumentName = new TextColumn<InteractionType>() {
			
			@Override
			public String getValue(InteractionType object) {
				if(object.getInteractionDocumentName()!=null){
					return object.getInteractionDocumentName();
				}
				return "";
			}
		};
		table.addColumn(getColumnDocumentName,"DocumentName");
		table.setColumnWidth(getColumnDocumentName, 110,Unit.PX);
	}


	private void createColumnCreatedBy() {

		getColumnCreatedBy = new TextColumn<InteractionType>() {
			
			@Override
			public String getValue(InteractionType object) {
				if(object.getCreatedBy()!=null){
					return object.getCreatedBy();
				}
				return "";
			}
		};
		table.addColumn(getColumnCreatedBy,"CreatedBy");
		table.setColumnWidth(getColumnCreatedBy, 100,Unit.PX);
	}


	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
