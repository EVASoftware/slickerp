package com.slicktechnologies.client.views.customerDetails;

import java.util.Date;
import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;
public class GoogleDriveLinkPopup  extends AbsolutePanel implements CompositeInterface{

	private DateBox dateBox;
	private Button btnOk;
	private Button btnCancel;
	ObjectListBox<Config> olbDocumnetType;
	private TextArea textArea;
	private TextArea remark;
	
	public GoogleDriveLinkPopup() {
		
		InlineLabel nlnlblNewServiceDate = new InlineLabel("Date        :");
		add(nlnlblNewServiceDate, 10, 28);
		
//		dateBox = new DateBox();
		dateBox = new DateBoxWithYearSelector();

//		dateBox.setEnabled(false);
		Date date=new Date();
		dateBox.setValue(date);
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MM-dd");
		DateBox.DefaultFormat defformat = new DateBox.DefaultFormat(format);
		dateBox.setFormat(defformat);
		add(dateBox, 184, 28);
		
		
		
		olbDocumnetType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbDocumnetType, Screen.DOCUMENTTYPE);
		
		
		InlineLabel specificReasonlbl = new InlineLabel("Document Type               :");
		add(specificReasonlbl, 10, 60);
		specificReasonlbl.setSize("200px", "18px");
		
		add(olbDocumnetType, 184, 60);
		olbDocumnetType.getElement().getStyle().setHeight(23, Unit.PX);
		
		InlineLabel nlnlblReasonForChange = new InlineLabel("Google Drive Link      :");
		add(nlnlblReasonForChange, 10, 90);
		nlnlblReasonForChange.setSize("200px", "18px");
		
		textArea = new TextArea();
		add(textArea, 184, 90);
		textArea.setSize("232px", "89px");
		
		
		InlineLabel remarklable = new InlineLabel("Remark      :");
		add(remarklable, 10, 200);
		remarklable.setSize("200px", "18px");
		
		remark = new TextArea();
		add(remark, 184, 200);
		remark.setSize("232px", "40px");
		
		
		btnOk = new Button("Ok");
		add(btnOk, 184, 260);
		
		
		btnCancel = new Button("Cancel");
		add(btnCancel,300,260);
		
		setSize("480px","310px");
		this.getElement().setId("form");
		
		
	}
	
	public void clear() {
		textArea.setValue(null);
		this.olbDocumnetType.setSelectedIndex(0);
		remark.setValue(null);
	}
	
	
	
	@Override
	public void setEnable(boolean status) {
		
	}

	@Override
	public boolean validate() {
		
		if(dateBox.getValue()==null){
			final GWTCAlert alert = new GWTCAlert(); 
		    alert.alert("Please select date!");
		    return false;
		}
		if(olbDocumnetType.getSelectedIndex()==0){
			final GWTCAlert alert = new GWTCAlert(); 
		    alert.alert("Please select document type!");
		    return false;
		}
		if(textArea.getValue().equals("")){
			final GWTCAlert alert = new GWTCAlert(); 
		    alert.alert("Please add google drive link!");
		    return false;
		}
		
		return true;
	}

	public DateBox getDateBox() {
		return dateBox;
	}

	public void setDateBox(DateBox dateBox) {
		this.dateBox = dateBox;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public ObjectListBox<Config> getOlbDocumnetType() {
		return olbDocumnetType;
	}

	public void setOlbDocumnetType(ObjectListBox<Config> olbDocumnetType) {
		this.olbDocumnetType = olbDocumnetType;
	}

	public TextArea getTextArea() {
		return textArea;
	}

	public void setTextArea(TextArea textArea) {
		this.textArea = textArea;
	}

	public TextArea getRemark() {
		return remark;
	}

	public void setRemark(TextArea remark) {
		this.remark = remark;
	}
}
