package com.slicktechnologies.client.views.customerDetails;


import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsTableProxy;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;

public class CustomerDetailsForm extends FormScreen<DummyEntityOnlyForScreen> implements ClickHandler,ChangeHandler{

	PersonInfoComposite person;
	
	/** Lead Table which can come on the form, this component can be made generic */
	public PaymentTableProxy paymentTable;
	
	/** Quotation Table which can come on form component can be made generic */
	public InvoiceTableProxy invoiceTable;
	
	/** Contract table which can come on form component can be made generic */
	public ContractTableProxy contractTable;
	
	/** Service table which can come on form component can be made generic */
	public ServiceTableProxy serviceTable;
	
	/** Lead table which can come on form component can be made generic */
	public LeadTableProxy leadTable;
	
	/** Quotation table which can come on form component can be made generic */
	public QuotationTableProxy quotationTable;
	
	/** Customer support table which can come on form component can be made generic */
	public CustomerSupportTableProxy customerSupportTableProxy;
	
	
	/** Contract Renewal table which can come on form component can be made generic */
	public ContractRenewalTableProxy contractrenewalTable;
	
	/**Date 22-10-2019 by Amol added a billing details table**/
	public BillingDetailsTableProxy billingdetailsTable;
	
	
	
	public GoogleDriveLinkTable googleDriveLinkTable;
	Button addlink;
	
	public TextArea addressinfo;
	
	ListBox contractList;
	Button gobutton,gobutton123,clearbutton;
	
	Button downLead,downQuotation,downContract,downService,downInvoice,downPayment,downBillingDetails;
	
	Button downCustSupport,downConRenew;
	
	TextBox redContract;
	TextBox greenContract;
	TextBox yellowContract;
	
	TextBox redInvoice;
	TextBox greenInvoice;
	TextBox yellowInvoice;
	
	TextBox redService;
	TextBox greenService;
	TextBox yellowService;
	
	TextBox redPayment;
	TextBox greenPayment;
	TextBox yellowPayment;
	
	TextBox redConRenew;
	TextBox greenConRenew;
	TextBox yellowConRenew;
	
	ListBox selectionMode;
	
	
	Label overDue;
	Label completed;
	Label due;
	
	//************rohan added customer number
	TextBox custRefNumber;
	
	/**
	 * Date 16 April 2017 added by vijay
	 */
	public InteractionToDoTableProxy interactionToDoTable;
	Button interactionDownload;
	/**  ends here */
	
	public CustomerDetailsForm() {
		super(FormStyle.DEFAULT);
		createGui();
		
		if(LoginPresenter.loggedInCustomer==false){
			processLevelBar.btnLabels[0].setVisible(true);
			processLevelBar.btnLabels[1].setVisible(true);
			processLevelBar.btnLabels[2].setVisible(true);
			processLevelBar.btnLabels[3].setVisible(true);
		}else{
			processLevelBar.btnLabels[4].setVisible(true);
		}
		
	}

	
	private void initalizeWidget() {

		person=AppUtility.customerInfoComposite(new Customer());
		
		addressinfo = new TextArea();
		addressinfo.setEnabled(false);
		
		paymentTable = new PaymentTableProxy();
		
		invoiceTable =new InvoiceTableProxy();
		
		contractTable = new ContractTableProxy();
		
		serviceTable= new ServiceTableProxy();
		
		leadTable = new LeadTableProxy();
		
		quotationTable = new QuotationTableProxy();
		
		customerSupportTableProxy= new CustomerSupportTableProxy();
		
		contractrenewalTable= new ContractRenewalTableProxy();
		billingdetailsTable=new BillingDetailsTableProxy();
		
		selectionMode = new ListBox();
		selectionMode.addItem("-SELECT-");
		selectionMode.addItem("OPEN");
		selectionMode.addItem("ALL");
		selectionMode.addChangeHandler(this);
		
		contractList= new ListBox();
		contractList.addChangeHandler(this);
		
		gobutton= new Button("Go");
		clearbutton = new Button("Clear");
		gobutton123 = new Button("Go");
		
		 redContract = new TextBox();
//		 redContract.addStyleName("red");
		 redContract.setEnabled(false);
		 
		 greenContract = new TextBox();
//		 greenContract.addStyleName("green");
		 greenContract.setEnabled(false);
		 
		 yellowContract =new TextBox();
//		 yellowContract.addStyleName("orange");
		 yellowContract.setEnabled(false);
		 
		 
		 redInvoice = new TextBox();
//		 redInvoice.addStyleName("red");
		 redInvoice.setEnabled(false);
		 
		 greenInvoice = new TextBox();
//		 greenInvoice.addStyleName("green");
		 greenInvoice.setEnabled(false);
		 
		 yellowInvoice =new TextBox();
//		 yellowInvoice.addStyleName("orange");
		 yellowInvoice.setEnabled(false);
		 
		 redService = new TextBox();
//		 redService.addStyleName("red");
		 redService.setEnabled(false);
		 
		 greenService = new TextBox();
//		 greenService.addStyleName("green");
		 greenService.setEnabled(false);

		 yellowService = new TextBox();
//		 yellowService.addStyleName("orange");
		 yellowService.setEnabled(false);
		
		 redPayment = new TextBox();
//		 redPayment.addStyleName("red");
		 redPayment.setEnabled(false);
		 
		 greenPayment =  new TextBox();
//		 greenPayment.addStyleName("green");
		 greenPayment.setEnabled(false);
		 
		 yellowPayment = new TextBox();
//		 yellowPayment.addStyleName("orange");
		 yellowPayment.setEnabled(false);
		 
		 redConRenew =new TextBox();
//		 redConRenew.addStyleName("red");
		 redConRenew.setEnabled(false);
		 
		 greenConRenew =new TextBox();
//		 greenConRenew.addStyleName("green");
		 greenConRenew.setEnabled(false);
		 
		 yellowConRenew = new TextBox();
//		 yellowConRenew.addStyleName("orange");
		 yellowConRenew.setEnabled(false);
		 
		 
		 downLead= new Button("Download");
		 downQuotation = new Button("Download");
		 downContract= new Button("Download");
		 downService = new Button("Download");
		 downInvoice= new Button("Download");
		 downPayment = new Button("Download");
		 downCustSupport = new Button("Download");
		 downConRenew =new Button("Download");
		 
		 downBillingDetails=new Button("Download");
		 
		 overDue = new Label("OverDue");
		 completed =  new Label("Completed");
		 due = new Label("Due");
		 
		 googleDriveLinkTable=new GoogleDriveLinkTable();
		 addlink=new Button("Upload Document Link");
		 
		 custRefNumber = new TextBox();
		 custRefNumber.setEnabled(false);
		 
		 interactionToDoTable = new InteractionToDoTableProxy();
		 interactionDownload = new Button("Download");
	}
	
	
	@Override
	public void onChange(ChangeEvent event) {
		
	}

	@Override
	public void onClick(ClickEvent event) {
		
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		
		Console.log("TOGGLE MENU");
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			Console.log("NEW");
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")){
					menus[k].setVisible(true); 
				}else{
					menus[k].setVisible(false);  
					Console.log(k+" "+menus[k].getText());
				}
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			Console.log("EDIT");
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
					menus[k].setVisible(true); 
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			Console.log("VIEW");
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
					menus[k].setVisible(true); 
				else
					menus[k].setVisible(false);  		   
			}
		}

	}

	@Override
	public void createScreen() {
		
		initalizeWidget();
		
		
		this.processlevelBarNames=new String[]{"Create Lead","Create Quotation","Create Order","Raise Ticket","Request Service"};
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		FormField fgroupingContractInformation=fbuilder.setlabel("Person Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("",person);
		FormField fperson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Address",addressinfo);
		FormField faddressinfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		FormField fgroupingDocInformation=fbuilder.setlabel("Documet Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("Contract List",contractList);
		FormField fcontractList= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		
		FormField fpaytable=fbuilder.setlabel("Payment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",paymentTable.getTable());
		FormField fpaymentTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		
		FormField finvoicetable=fbuilder.setlabel("Invoice").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",invoiceTable.getTable());
		FormField finvoiceTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		
		FormField fconTable=fbuilder.setlabel("Contract").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",contractTable.getTable());
		FormField fcontractTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		FormField fserTable=fbuilder.setlabel("Service").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",serviceTable.getTable());
		FormField fserviceTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		
		FormField fleadtable=fbuilder.setlabel("Lead").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",leadTable.getTable());
		FormField fleadTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		
		FormField fquotation=fbuilder.setlabel("Quotation").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",quotationTable.getTable());
		FormField fquotationTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		
		FormField fcon=fbuilder.setlabel("Contract").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",redContract);
		FormField redContract= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",greenContract);
		FormField fgreenContract= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",yellowContract);
		FormField fyellowContract= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();

		FormField fser=fbuilder.setlabel("Service").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",redService);
		FormField fredService= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",greenService);
		FormField fgreenService= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",yellowService);
		FormField fyellowService= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		FormField fpay=fbuilder.setlabel("Payment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",redPayment);
		FormField fredPayment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",greenPayment);
		FormField fgreenPayment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",yellowPayment);
		FormField fyellowPayment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fconRen=fbuilder.setlabel("Contract Renewal").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",redConRenew);
		FormField fredConRenew= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",greenConRenew);
		FormField fgreenConRenew= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",yellowConRenew);
		FormField fyellowConRenew= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("",gobutton);
		FormField fgobutton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("",clearbutton);
//		FormField fclearbutton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",gobutton123);
		FormField fgobutton123= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Selection Mode",selectionMode);
		FormField fselectionMode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fcomplain=fbuilder.setlabel("Customer Complain Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",customerSupportTableProxy.getTable());
		FormField fcustomerSupportTableProxy= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		
		FormField frenewal=fbuilder.setlabel("Contract Renewal Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",contractrenewalTable.getTable());
		FormField fcontractrenewalTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",downLead);
		FormField fdownLead= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",downQuotation);
		FormField fdownQuotation= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",downContract);
		FormField fdownContract= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",downService);
		FormField fdownService= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("",downInvoice);
		FormField fdownInvoice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("",downPayment);
		FormField fdownPayment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		
		
		FormField fInvoice=fbuilder.setlabel("Invoice").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",redInvoice);
		FormField fredInvoice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",greenInvoice);
		FormField fgreenInvoice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",yellowInvoice);
		FormField fyellowInvoice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField fdesc=fbuilder.setlabel("").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("",overDue);
		FormField foverDue= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",completed);
		FormField fcompleted= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",due);
		FormField fdue= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",downCustSupport);
		FormField fdownCustSupport= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",downConRenew);
		FormField fdownConRenew= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(6).build();
		
		FormField flinkgrp=fbuilder.setlabel("Google Drive Link").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",googleDriveLinkTable.getTable());
		FormField fgoogleDriveLinkTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",addlink);
		FormField faddlink= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(6).build();
		
		

		fbuilder = new FormFieldBuilder("Ref Number",custRefNumber);
		FormField fcustRefNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		FormField fInteractionToDoGroup=fbuilder.setlabel("Interaction To Do").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("",interactionToDoTable.getTable());
		FormField fInteractionToDoTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("",interactionDownload);
		FormField finteractionDownload= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
       FormField fbillingdeatils=fbuilder.setlabel("Billing Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",billingdetailsTable.getTable());
		FormField fbillingdeatilsTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",downBillingDetails);
		FormField fdownBillingDetails= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		if (LoginPresenter.loggedInCustomer == false) {
			FormField[][] formfield = {
					{ fgroupingContractInformation },
					{ fperson },
					{ faddressinfo ,fcustRefNumber },
					{ fdesc, fcon, fser, fInvoice, fpay, fconRen },
					{ foverDue, redContract, fredService, fredInvoice,
							fredPayment, fredConRenew },
					{ fdue, fyellowContract, fyellowService, fyellowInvoice,
							fyellowPayment, fyellowConRenew },
					{ fcompleted, fgreenContract, fgreenService, fgreenInvoice,
							fgreenPayment, fgreenConRenew },
					{ fgroupingDocInformation },
					{ fcontractList, fgobutton, fselectionMode, fgobutton123 },
					{ fcomplain, frenewal },
					{ fdownCustSupport, fdownConRenew },
					{ fcustomerSupportTableProxy, fcontractrenewalTable },
					{ fconTable, fserTable }, { fdownContract, fdownService },
					{ fcontractTable, fserviceTable },
					
					{fbillingdeatils,finvoicetable},
					{fdownBillingDetails,fdownInvoice},
					{fbillingdeatilsTable,finvoiceTable},
					
					{fpaytable,fleadtable},
					{fdownPayment,fdownLead},
					{fpaymentTable,fleadTable},
					
					{fquotation,flinkgrp},
					{fdownQuotation,faddlink},
					{fquotationTable,fgoogleDriveLinkTable},
					
					{fInteractionToDoGroup},
					{finteractionDownload},
					{fInteractionToDoTable}

			};
			this.fields = formfield;
		} else {

			FormField[][] formfield = {
					{ fgroupingContractInformation },
					{ fperson },
					{ faddressinfo ,fcustRefNumber},
					{ fdesc, fcon, fser, fInvoice, fpay, fconRen },
					{ foverDue, redContract, fredService, fredInvoice,
							fredPayment, fredConRenew },
					{ fdue, fyellowContract, fyellowService, fyellowInvoice,
							fyellowPayment, fyellowConRenew },
					{ fcompleted, fgreenContract, fgreenService, fgreenInvoice,
							fgreenPayment, fgreenConRenew },
					{ fgroupingDocInformation },
					{ fcontractList, fgobutton, fselectionMode, fgobutton123 },
					{ fcomplain, frenewal },
					{ fdownCustSupport, fdownConRenew },
					{ fcustomerSupportTableProxy, fcontractrenewalTable },
					{ fconTable, fserTable }, { fdownContract, fdownService },
					{ fcontractTable, fserviceTable },
					{ finvoicetable, fpaytable },
					{ fdownInvoice, fdownPayment },
					{ finvoiceTable, fpaymentTable },
					{flinkgrp,fInteractionToDoGroup},
					{faddlink,finteractionDownload},
					{fgoogleDriveLinkTable,fInteractionToDoTable}

			};
			this.fields = formfield;
		}
	}
	
	
	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateView(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

//******************getters and setters *****************************

	public PersonInfoComposite getPerson() {
		return person;
	}


	public void setPerson(PersonInfoComposite person) {
		this.person = person;
	}


	public PaymentTableProxy getPaymentTable() {
		return paymentTable;
	}


	public void setPaymentTable(PaymentTableProxy paymentTable) {
		this.paymentTable = paymentTable;
	}


	public InvoiceTableProxy getInvoiceTable() {
		return invoiceTable;
	}


	public void setInvoiceTable(InvoiceTableProxy invoiceTable) {
		this.invoiceTable = invoiceTable;
	}


	public ContractTableProxy getContractTable() {
		return contractTable;
	}


	public void setContractTable(ContractTableProxy contractTable) {
		this.contractTable = contractTable;
	}


	public ServiceTableProxy getServiceTable() {
		return serviceTable;
	}


	public void setServiceTable(ServiceTableProxy serviceTable) {
		this.serviceTable = serviceTable;
	}


	public TextArea getAddressinfo() {
		return addressinfo;
	}


	public void setAddressinfo(TextArea addressinfo) {
		this.addressinfo = addressinfo;
	}


	public ListBox getContractList() {
		return contractList;
	}


	public void setContractList(ListBox contractList) {
		this.contractList = contractList;
	}


	public Button getGobutton() {
		return gobutton;
	}


	public void setGobutton(Button gobutton) {
		this.gobutton = gobutton;
	}


	public Button getClearbutton() {
		return clearbutton;
	}


	public void setClearbutton(Button clearbutton) {
		this.clearbutton = clearbutton;
	}


	public TextBox getRedContract() {
		return redContract;
	}


	public void setRedContract(TextBox redContract) {
		this.redContract = redContract;
	}


	public TextBox getGreenContract() {
		return greenContract;
	}


	public void setGreenContract(TextBox greenContract) {
		this.greenContract = greenContract;
	}


	public TextBox getYellowContract() {
		return yellowContract;
	}


	public void setYellowContract(TextBox yellowContract) {
		this.yellowContract = yellowContract;
	}


	public TextBox getRedService() {
		return redService;
	}


	public void setRedService(TextBox redService) {
		this.redService = redService;
	}


	public TextBox getGreenService() {
		return greenService;
	}


	public void setGreenService(TextBox greenService) {
		this.greenService = greenService;
	}


	public TextBox getYellowService() {
		return yellowService;
	}


	public void setYellowService(TextBox yellowService) {
		this.yellowService = yellowService;
	}


	public TextBox getRedPayment() {
		return redPayment;
	}


	public void setRedPayment(TextBox redPayment) {
		this.redPayment = redPayment;
	}


	public TextBox getGreenPayment() {
		return greenPayment;
	}


	public void setGreenPayment(TextBox greenPayment) {
		this.greenPayment = greenPayment;
	}


	public TextBox getYellowPayment() {
		return yellowPayment;
	}


	public void setYellowPayment(TextBox yellowPayment) {
		this.yellowPayment = yellowPayment;
	}


	public TextBox getRedConRenew() {
		return redConRenew;
	}


	public void setRedConRenew(TextBox redConRenew) {
		this.redConRenew = redConRenew;
	}


	public TextBox getGreenConRenew() {
		return greenConRenew;
	}


	public void setGreenConRenew(TextBox greenConRenew) {
		this.greenConRenew = greenConRenew;
	}


	public TextBox getYellowConRenew() {
		return yellowConRenew;
	}


	public void setYellowConRenew(TextBox yellowConRenew) {
		this.yellowConRenew = yellowConRenew;
	}


	public LeadTableProxy getLeadTable() {
		return leadTable;
	}


	public void setLeadTable(LeadTableProxy leadTable) {
		this.leadTable = leadTable;
	}


	public QuotationTableProxy getQuotationTable() {
		return quotationTable;
	}


	public void setQuotationTable(QuotationTableProxy quotationTable) {
		this.quotationTable = quotationTable;
	}


	public ListBox getSelectionMode() {
		return selectionMode;
	}


	public void setSelectionMode(ListBox selectionMode) {
		this.selectionMode = selectionMode;
	}


	public Button getGobutton123() {
		return gobutton123;
	}


	public void setGobutton123(Button gobutton123) {
		this.gobutton123 = gobutton123;
	}


	public CustomerSupportTableProxy getCustomerSupportTableProxy() {
		return customerSupportTableProxy;
	}


	public void setCustomerSupportTableProxy(
			CustomerSupportTableProxy customerSupportTableProxy) {
		this.customerSupportTableProxy = customerSupportTableProxy;
	}


	public Button getDownLead() {
		return downLead;
	}


	public void setDownLead(Button downLead) {
		this.downLead = downLead;
	}


	public Button getDownQuotation() {
		return downQuotation;
	}


	public void setDownQuotation(Button downQuotation) {
		this.downQuotation = downQuotation;
	}


	public Button getDownContract() {
		return downContract;
	}


	public void setDownContract(Button downContract) {
		this.downContract = downContract;
	}


	public Button getDownService() {
		return downService;
	}


	public void setDownService(Button downService) {
		this.downService = downService;
	}


	public Button getDownInvoice() {
		return downInvoice;
	}


	public void setDownInvoice(Button downInvoice) {
		this.downInvoice = downInvoice;
	}


	public Button getDownPayment() {
		return downPayment;
	}


	public void setDownPayment(Button downPayment) {
		this.downPayment = downPayment;
	}


	public TextBox getRedInvoice() {
		return redInvoice;
	}


	public void setRedInvoice(TextBox redInvoice) {
		this.redInvoice = redInvoice;
	}


	public TextBox getGreenInvoice() {
		return greenInvoice;
	}


	public void setGreenInvoice(TextBox greenInvoice) {
		this.greenInvoice = greenInvoice;
	}


	public TextBox getYellowInvoice() {
		return yellowInvoice;
	}


	public void setYellowInvoice(TextBox yellowInvoice) {
		this.yellowInvoice = yellowInvoice;
	}


	public ContractRenewalTableProxy getContractrenewalTable() {
		return contractrenewalTable;
	}


	public void setContractrenewalTable(
			ContractRenewalTableProxy contractrenewalTable) {
		this.contractrenewalTable = contractrenewalTable;
	}


	public Button getDownCustSupport() {
		return downCustSupport;
	}


	public void setDownCustSupport(Button downCustSupport) {
		this.downCustSupport = downCustSupport;
	}


	public Button getDownConRenew() {
		return downConRenew;
	}


	public void setDownConRenew(Button downConRenew) {
		this.downConRenew = downConRenew;
	}


	public Button getAddlink() {
		return addlink;
	}


	public void setAddlink(Button addlink) {
		this.addlink = addlink;
	}


	public TextBox getCustRefNumber() {
		return custRefNumber;
	}


	public void setCustRefNumber(TextBox custRefNumber) {
		this.custRefNumber = custRefNumber;
	}
	
	public InteractionToDoTableProxy getInteractionToDoTable() {
		return interactionToDoTable;
	}


	public void setInteractionToDoTable(
			InteractionToDoTableProxy interactionToDoTable) {
		this.interactionToDoTable = interactionToDoTable;
	}


	public Button getInteractionDownload() {
		return interactionDownload;
	}


	public void setInteractionDownload(Button interactionDownload) {
		this.interactionDownload = interactionDownload;
	}


	public BillingDetailsTableProxy getBillingdetailsTable() {
		return billingdetailsTable;
	}


	public void setBillingdetailsTable(BillingDetailsTableProxy billingdetailsTable) {
		this.billingdetailsTable = billingdetailsTable;
	}


	public Button getDownBillingDetails() {
		return downBillingDetails;
	}


	public void setDownBillingDetails(Button downBillingDetails) {
		this.downBillingDetails = downBillingDetails;
	}


	


	
	
}
