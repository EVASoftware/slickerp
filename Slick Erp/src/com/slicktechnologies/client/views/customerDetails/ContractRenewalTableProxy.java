package com.slicktechnologies.client.views.customerDetails;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Contract;

public class ContractRenewalTableProxy extends SuperTable<Contract>{

	TextColumn<Contract> getContractCountColumn;
	TextColumn<Contract> getStartDate;
	TextColumn<Contract> getEndDate;
	TextColumn<Contract> getStatusColumn;
	
	
	public Object getVarRef(String varName) {
		if (varName.equals("getContractCountColumn"))
			return this.getContractCountColumn;
		if (varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		return null;
	}
	public ContractRenewalTableProxy() {
		super();
	}
	
	
	@Override
	public void createTable() {
	
		addColumngetCount();
		addColumngetStartDate();
		addColumngetEndDate();
		addColumngetStatus();
	
	}
	
	
	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				return object.getStatus();
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
		table.setColumnWidth(getStatusColumn, 80, Unit.PX);
	
	}
	
	protected void addColumngetCount() {
		getContractCountColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				if (object.getCount() == 0)
					return "N.A";
				else
					return object.getCount() + "";
			}
			
			
		};
		table.addColumn(getContractCountColumn, "ID");
	
		table.setColumnWidth(getContractCountColumn, 100, Unit.PX);
		getContractCountColumn.setSortable(true);
	}
	
	protected void addColumngetStartDate() {
		
		
		
				
		getStartDate = new TextColumn<Contract>() {
			
			@Override
			public String getValue(Contract object) {
				if (object.getStartDate() != null)
					return AppUtility.parseDate(object.getStartDate());
				else
					return " ";
	
			}
		};
		table.addColumn(getStartDate, "Contract Start Date");
		table.setColumnWidth(getStartDate, 140, Unit.PX);
		getStartDate.setSortable(true);
	}
	
	protected void addColumngetEndDate() {
		
		
		getEndDate = new TextColumn<Contract>() {
			
			@Override
			public String getCellStyleNames(Context context, Contract object) {
				
				final Date sub30Days=CalendarUtil.copyDate(new Date());
				CalendarUtil.addDaysToDate(sub30Days, -30);
				sub30Days.setHours(23);
				sub30Days.setMinutes(59);
				sub30Days.setSeconds(59);
				

				if(LoginPresenter.loggedInCustomer==false){
						 if((object.getEndDate().before(new Date())&&object.getEndDate().after(sub30Days))&&(object.getStatus().equals("Approved")))
						 {
							 return "orange";
						 }
						 else if((new Date().after(object.getEndDate()))&&(object.getStatus().equals("Approved")))
						 {
							 return "red";
						 }
						 else if(object.getStatus().equals("Approved")&&(object.getRefContractCount()!=-1))
						 {
							 return "green";
						 }
						 else 
						 {
							 return "black";
						 }
				}else{
					if((object.getEndDate().before(new Date())&&object.getEndDate().after(sub30Days))&&(object.getStatus().equals("Due")))
					 {
						 return "orange";
					 }
					 else if((new Date().after(object.getEndDate()))&&(object.getStatus().equals("Overdue")))
					 {
						 return "red";
					 }
					 else 
					 {
						 return "black";
					 }
				}
				
//								 
			}
			@Override
			public String getValue(Contract object) {
				if (object.getEndDate() != null)
					return AppUtility.parseDate(object.getEndDate());
				else
					return " ";
	
			}
		};
		table.addColumn(getEndDate, "Contract End Date");
		table.setColumnWidth(getEndDate, 140, Unit.PX);
		getEndDate.setSortable(true);
	}
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Date : 05-08-2017 By ANIL
	 */
	
	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		
		addColumnSortingCount();
		addColumnSortingStartDate();
		addColumnSortingEndDate();
		addColumnSortingStatus();
	}
	private void addColumnSortingCount() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getContractCountColumn,new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}
	private void addColumnSortingStartDate() {
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getStartDate, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStartDate()!=null && e2.getStartDate()!=null){
						return e1.getStartDate().compareTo(e2.getStartDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}
	private void addColumnSortingEndDate() {
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getEndDate, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEndDate()!=null && e2.getEndDate()!=null){
						return e1.getEndDate().compareTo(e2.getEndDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}
	private void addColumnSortingStatus() {
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}
	
	
	/**
	 * end
	 */
	
	

}
