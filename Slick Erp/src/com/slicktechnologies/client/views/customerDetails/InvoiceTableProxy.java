package com.slicktechnologies.client.views.customerDetails;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.paymentlayer.Payment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class InvoiceTableProxy extends SuperTable<Invoice>{

	
	TextColumn<Invoice> getinvoiceIdColumn;
	TextColumn<Invoice> getcontractIdColumn;
	TextColumn<Invoice> getinvoiceDateColumn;
	TextColumn<Invoice> getinvoiceAmtColumn;
	TextColumn<Invoice> getstatusColumn;
	
	
	@Override
	public void createTable() {

		addColumngetContractCount();
		addColumngetCount();
		addColumngetInvoiceDate();
		addColumngetInvoiceAmount();
		
		if(LoginPresenter.loggedInCustomer==false){
			addColumngetStatus();
			}
		
		
	}
	
	protected void addColumngetStatus()
	{
		getstatusColumn=new TextColumn<Invoice>()
				{
			@Override
			public String getValue(Invoice object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(getstatusColumn,"Status");
				getstatusColumn.setSortable(true);
	}
	
	protected void addColumngetInvoiceDate()
	{
		
		getinvoiceDateColumn=new TextColumn<Invoice>()
		{
			@Override
			public String getCellStyleNames(Context context, Invoice object) {
				
//				Date todaysDate=CalendarUtil.copyDate(object.getInvoiceDate());
//				Date date= CalendarUtil.copyDate(object.getInvoiceDate());
//				
//				CalendarUtil.addDaysToDate(todaysDate,1);
//				todaysDate.setHours(23);
//				todaysDate.setMinutes(59);
//				todaysDate.setSeconds(59);
//
//				
//				CalendarUtil.addDaysToDate(date,-7);
//				date.setHours(23);
//				date.setMinutes(59);
//				date.setSeconds(59);
//				
//				final Date yestDate=CalendarUtil.copyDate(new Date());
//				CalendarUtil.addDaysToDate(yestDate,-1);
//				yestDate.setHours(23);
//				yestDate.setMinutes(59);
//				yestDate.setSeconds(59);
//				System.out.println("Todays Date Converted ::: "+yestDate);
//				
//				if((object.getInvoiceDate().after(todaysDate))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
//					return "red";
//				 }
//				 else if((object.getInvoiceDate().after(date)&&object.getInvoiceDate().before(yestDate))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested")))
//				 {
//					 return "orange";
//				 }
//				
//				else if(object.getStatus().equals("Completed")){
//					return "green";
//				 }
//				else
//				{
//					return "black";
//				}
				
				Date todaysDate=CalendarUtil.copyDate(new Date());
				
				CalendarUtil.addDaysToDate(todaysDate,-7);
				todaysDate.setHours(23);
				todaysDate.setMinutes(59);
				todaysDate.setSeconds(59);
				
				if((object.getInvoiceDate().before(new Date()))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
					return "red";
				 }
				
				 else if((object.getInvoiceDate().after(todaysDate)&&object.getInvoiceDate().before(new Date()))&&(object.getStatus().equals("Requested")))
				 {
					 return "orange";
				 }
				 else if(object.getStatus().equals("Approved")){
					 return "green";
				 }
				 else
						{
						return "black";
					}
				
				
			}
			
			@Override
			public String getValue(Invoice object)
			{
				return AppUtility.parseDate(object.getInvoiceDate());
			}
		};
		table.addColumn(getinvoiceDateColumn,"Invoice Dt");
		getinvoiceDateColumn.setSortable(true);
	}
	
	protected void addColumngetInvoiceAmount()
	{
		getinvoiceAmtColumn=new TextColumn<Invoice>()
				{
			@Override
			public String getValue(Invoice object)
			{
				return object.getInvoiceAmount()+"";
			}
				};
				table.addColumn(getinvoiceAmtColumn,"Invoice Amt");
				getinvoiceAmtColumn.setSortable(true);
	}
	
	protected void addColumngetCount()
	{
		getinvoiceIdColumn=new TextColumn<Invoice>()
				{
			@Override
			public String getValue(Invoice object)
			{
				return object.getCount()+"";
			}
				};
				table.addColumn(getinvoiceIdColumn,"ID");
				getinvoiceIdColumn.setSortable(true);
	}
	
	protected void addColumngetContractCount()
	{
		getcontractIdColumn=new TextColumn<Invoice>()
				{
			@Override
			public String getValue(Invoice object)
			{
				return object.getContractCount()+"";
			}
				};
				table.addColumn(getcontractIdColumn,"Contract ID");
				getcontractIdColumn.setSortable(true);
	}
	

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	
	/**
	 * Date : 05-08-2017 By  ANIL
	 */
	
	@Override
	public void addColumnSorting() {
		// TODO Auto-generated method stub
		super.addColumnSorting();
		addColumnSortingContractCount();
		addColumnSortingCount();
		addColumnSortingInvoiceDate();
		addColumnSortingInvoiceAmount();
		addColumnSortingStatus();
	}

	private void addColumnSortingContractCount() {
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(getcontractIdColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingCount() {
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(getinvoiceIdColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingInvoiceDate() {
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(getinvoiceDateColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceDate()!=null && e2.getInvoiceDate()!=null){
						return e1.getInvoiceDate().compareTo(e2.getInvoiceDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingInvoiceAmount() {
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(getinvoiceAmtColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getInvoiceAmount()== e2.getInvoiceAmount()){
						return 0;}
					if(e1.getInvoiceAmount()> e2.getInvoiceAmount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnSortingStatus() {
		List<Invoice> list=getDataprovider().getList();
		columnSort=new ListHandler<Invoice>(list);
		columnSort.setComparator(getstatusColumn, new Comparator<Invoice>()
				{
			@Override
			public int compare(Invoice e1,Invoice e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}
	
	/**
	 * End
	 */

}
