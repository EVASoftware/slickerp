package com.slicktechnologies.client.views.customerDetails;

import java.io.Serializable;
import java.util.Date;

import com.google.gwt.user.cellview.client.TextColumn;
import com.googlecode.objectify.annotation.Embed;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

@Embed
public class ContractTableValueProxy implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 928767129376071083L;

	int contractId;
	Date StartDate;
	Date EndDate;
	String Status;
	
	
	//**************for payment *************
	int paymentId;
	Date paymentDate;
	double balAMt;
	double orderAmt;
	
	//******************for invoice *********
	int invoiceId;
	Date invoiceDate;
	double invoiceAmt;
	
	//***************for service *****************
	
	int ServiceId;
	Date serviceDate;
	
	
	//***************for lead *******************
	
	int leadID;
	Date leadDate;
	String leadTitle;
	
	
	//***************for quotation ********************
	int quotationId;
	Date quotationDate;
	Date validUntil;
	
	//********************for custmer support ********************
	Date ticketDate;
	int ticketNumbr;
	String title;
	
	//*****************getters and setters ***********************************
	
	public int getContractId() {
		return contractId;
	}
	public void setContractId(int contractId) {
		this.contractId = contractId;
	}
	public Date getStartDate() {
		return StartDate;
	}
	public void setStartDate(Date startDate) {
		StartDate = startDate;
	}
	public Date getEndDate() {
		return EndDate;
	}
	public void setEndDate(Date endDate) {
		EndDate = endDate;
	}
		
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public int getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public int getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(int invoiceId) {
		this.invoiceId = invoiceId;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public double getInvoiceAmt() {
		return invoiceAmt;
	}
	public void setInvoiceAmt(double invoiceAmt) {
		this.invoiceAmt = invoiceAmt;
	}
	public int getServiceId() {
		return ServiceId;
	}
	public void setServiceId(int serviceId) {
		ServiceId = serviceId;
	}
	public Date getServiceDate() {
		return serviceDate;
	}
	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}
	public int getLeadID() {
		return leadID;
	}
	public void setLeadID(int leadID) {
		this.leadID = leadID;
	}
	public Date getLeadDate() {
		return leadDate;
	}
	public void setLeadDate(Date leadDate) {
		this.leadDate = leadDate;
	}
	public String getLeadTitle() {
		return leadTitle;
	}
	public void setLeadTitle(String leadTitle) {
		this.leadTitle = leadTitle;
	}
	public int getQuotationId() {
		return quotationId;
	}
	public void setQuotationId(int quotationId) {
		this.quotationId = quotationId;
	}
	public Date getQuotationDate() {
		return quotationDate;
	}
	public void setQuotationDate(Date quotationDate) {
		this.quotationDate = quotationDate;
	}
	public Date getValidUntil() {
		return validUntil;
	}
	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}
	public Date getTicketDate() {
		return ticketDate;
	}
	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}
	public int getTicketNumbr() {
		return ticketNumbr;
	}
	public void setTicketNumbr(int ticketNumbr) {
		this.ticketNumbr = ticketNumbr;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public double getBalAMt() {
		return balAMt;
	}
	public void setBalAMt(double balAMt) {
		this.balAMt = balAMt;
	}
	public double getOrderAmt() {
		return orderAmt;
	}
	public void setOrderAmt(double orderAmt) {
		this.orderAmt = orderAmt;
	}
	
	
}
