package com.slicktechnologies.client.views.customerDetails;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionModel;
import com.itextpdf.text.Phrase;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.CopyDataConfig;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.CustomerProjectService;
import com.slicktechnologies.client.services.CustomerProjectServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.complain.ComplainForm;
import com.slicktechnologies.client.views.complain.ComplainPresenter;
import com.slicktechnologies.client.views.complain.ServiceDateBoxPopup;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.client.views.customer.CustomerPresenter;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionForm;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionPresenter;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsTableProxy;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter;
import com.slicktechnologies.client.views.productgroupidentification.productgroupdetails.ProductGroupDetailsForm;
import com.slicktechnologies.client.views.productgroupidentification.productgroupdetails.ProductGroupDetailsPresenter;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.GoogleDriveLink;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
public class CustomerDetailsPresenter extends FormScreenPresenter<DummyEntityOnlyForScreen> implements ClickHandler,SelectionHandler<Suggestion>{


	public static CustomerDetailsForm form;
	final GenricServiceAsync async =GWT.create(GenricService.class);
	
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	public List<Contract> globalContractlis=new ArrayList<Contract>();
	public List<Service> globalServicetlis=new ArrayList<Service>();
	public List<Invoice> globalInvoicelis=new ArrayList<Invoice>();
	public List<CustomerPayment> globalPaymentlis=new ArrayList<CustomerPayment>();
	public List<Lead> globalLeadlis=new ArrayList<Lead>();
	public List<Quotation> globalQuotationlis=new ArrayList<Quotation>();
	public List<InteractionType> globalInteractionlist = new ArrayList<InteractionType>();
	public List<BillingDocument> globalBillingList=new ArrayList<BillingDocument>();
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	CustomerProjectServiceAsync projectAsync = GWT.create(CustomerProjectService.class);
	ServiceDateBoxPopup datePopup = new ServiceDateBoxPopup("Service Date","Service Description");
	PopupPanel panel = new PopupPanel(true);
	int customerCounter;
	
	GoogleDriveLinkPopup googlelinkpopup=new GoogleDriveLinkPopup();
	public static Customer customer;
	
	public CustomerDetailsPresenter(UiScreen<DummyEntityOnlyForScreen> view,DummyEntityOnlyForScreen model) {
		super(view, model);
		form = (CustomerDetailsForm) view;
		form.person.getId().addSelectionHandler(this);
		form.person.getName().addSelectionHandler(this);
		form.person.getPhone().addSelectionHandler(this);
	
		form.getGobutton().addClickHandler(this);
		form.getClearbutton().addClickHandler(this);
		form.getGobutton123().addClickHandler(this);
		
		form.getDownBillingDetails().addClickHandler(this);
		
		form.getDownContract().addClickHandler(this);
		form.getDownService().addClickHandler(this);
		form.getDownInvoice().addClickHandler(this);
		form.getDownPayment().addClickHandler(this);
		form.getDownLead().addClickHandler(this);
		form.getDownQuotation().addClickHandler(this);
		form.getDownCustSupport().addClickHandler(this);
		form.getDownConRenew().addClickHandler(this);
		
		form.getAddlink().addClickHandler(this);
		googlelinkpopup.getBtnOk().addClickHandler(this);
		googlelinkpopup.getBtnCancel().addClickHandler(this);
		
		datePopup.getBtnOne().addClickHandler(this);
		datePopup.getBtnTwo().addClickHandler(this);
		
		//added by vijay on 17 April 2017
		form.getInteractionDownload().addClickHandler(this);
				
		setTableSelectionOnContract();
		setTableSelectionOnService();
		setTableSelectionOnInvoice();
		setTableSelectionOnPayment();
		setTableSelectionOnContractRenewal();
		setTableSelectionOnCustomerSupport();
		/**Date 11-9-2019 by Amol added Selection on Lead Quotation**/
		setTableSelectionOnLeads();
		setTableQuotation();
		//added by vijay on 17 April 2017
		setTableSelectionOnInteractionToDo();
		/**Date 22-10-2019 by Amol**/
		setTableSelectionOnBillingDetails();
		if (LoginPresenter.loggedInCustomer == true) {
			customerCounter = 0;
			getCustomerDetails(LoginPresenter.loggedInUser, false);
		}
	}
		
	private void setTableSelectionOnBillingDetails() {
		 
		 final NoSelectionModel<BillingDocument> selectionModelMyObj = new NoSelectionModel<BillingDocument>();
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final BillingDocument entity=selectionModelMyObj.getLastSelectedObject();
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Billing Details",Screen.BILLINGDETAILS);
	        	 final BillingDetailsForm form = BillingDetailsPresenter.initalize();
	        	
	        	 AppMemory.getAppMemory().stickPnel(form);
	        	 form.showWaitSymbol();
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					
	 					MyQuerry querry=new MyQuerry();
	 					Vector<Filter> filtervec=new Vector<Filter>();
	 					Filter temp=null;
	 					
	 						 					
	 					temp=new Filter();
	 					temp.setQuerryString("count");
	 					temp.setIntValue(entity.getCount());
	 					filtervec.add(temp);
	 					
	 					querry.setFilters(filtervec);
	 					querry.setQuerryObject(new BillingDocument());
	 					
	 					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

	 						@Override
	 						public void onFailure(Throwable caught) {
	 							form.showDialogMessage("An Unexpected Error occured !");
	 							
	 						}
	 						@Override
	 						public void onSuccess(ArrayList<SuperModel> result) {
	 							
	 							
	 							for(SuperModel pmodel:result)
	 							{
	 								BillingDocument billingdocs=(BillingDocument)pmodel;
	 								form.updateView(billingdocs);
	 								 form.setToViewState();
	 							}
	 							
	 						}
	 					});
	 					
	 					form.hideWaitSymbol();
	 				}
	 			};
	             timer.schedule(3000); 
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getBillingdetailsTable().getTable().setSelectionModel(selectionModelMyObj);
		 
		 
	 }

	private void setTableSelectionOnInteractionToDo() {

		 final NoSelectionModel<InteractionType> selectionModelMyObj = new NoSelectionModel<InteractionType>();
	     
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final InteractionType entity=selectionModelMyObj.getLastSelectedObject();
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Interaction TO DO",Screen.INTERACTION);
	        	 final InteractionForm form = InteractionPresenter.initalize();
	        	 form.showWaitSymbol();
					
				 AppMemory.getAppMemory().stickPnel(form);
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					 form.hideWaitSymbol();
					     form.updateView(entity);
					     form.setToViewState();
	 				}
	 			};
	            
	             timer.schedule(3000); 
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     
	     form.getInteractionToDoTable().getTable().setSelectionModel(selectionModelMyObj);
	 
	
	}

	private void getCustomerDetails(final String loggedInUser,boolean companyFlag) {
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		if (companyFlag == false) {
			filter = new Filter();
			filter.setQuerryString("fullname");
			filter.setStringValue(loggedInUser);
			filtervec.add(filter);
		} else {
			filter = new Filter();
			filter.setQuerryString("companyName");
			filter.setStringValue(loggedInUser);
			filtervec.add(filter);
		}
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if(result.size()!=0){
					Customer entity=(Customer) result.get(0);
					
					customer = entity;
					
					PersonInfo info=new PersonInfo();
					info.setCount(entity.getCount());
					if(entity.isCompany()){
						info.setFullName(entity.getCompanyName());
					}else{
						info.setFullName(entity.getFullname());
					}
					info.setCellNumber(entity.getCellNumber1());
					info.setPocName(entity.getFullname());
					
					
					form.person.setValue(info);
					form.person.setEnable(false);
					form.getAddressinfo().setValue(entity.getAdress().getCompleteAddress());
				
					if(!entity.getRefrNumber1().equals("")){
						form.getCustRefNumber().setValue(entity.getRefrNumber1());
					}
					else
					{
						form.getCustRefNumber().setValue("");
					}
					
					
					form.getContractList().clear();
					form.getContractList().addItem("-SELECT-");
					getcontractIdfomCustomerID();
//					getCustomerAddressDetails();
					getCustomerPaymentsDetails();
					getCustomerInvoiceDetails();
					getCustomerServiceDetails();
					getCustomerComplainDetails();
					
					form.googleDriveLinkTable.connectToLocal();
					if(entity.getGoogleDriveLinkList().size()!=0)
					{
						form.googleDriveLinkTable.getDataprovider().setList(entity.getGoogleDriveLinkList());
					}
					
					
				}else{
					
					//For Company Name........
					if(customerCounter==0){
						customerCounter++;
						getCustomerDetails(loggedInUser,true);
					}
					
					
					
					
				}
			}
		});
		
	}

	public static void initalize()
	{
		try {
			AppMemory.getAppMemory().currentScreen=Screen.CUSTOMERRECORD;
			AppMemory.getAppMemory().currentState=ScreeenState.NEW;
			AppMemory.getAppMemory().skeleton.isMenuVisble=false;
			CustomerDetailsForm homeForm = new CustomerDetailsForm();
			CustomerDetailsPresenter presenter= new CustomerDetailsPresenter(homeForm, new DummyEntityOnlyForScreen());
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Home/Home",Screen.CUSTOMERRECORD);
			AppMemory.getAppMemory().stickPnel(homeForm);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {

		if (event.getSource().equals(form.person.getId())||event.getSource().equals(form.person.getName())||
				event.getSource().equals(form.person.getPhone())) {
			
			form.getContractList().clear();
			form.getContractList().addItem("-SELECT-");
			getcontractIdfomCustomerID();
			getCustomerAddressDetails();
			getCustomerPaymentsDetails();
			getCustomerInvoiceDetails();
			getCustomerServiceDetails();
			getcustomerLeadDetails();
			getCustomerQuotationDetails();
			getCustomerComplainDetails();
			getBillingDetails();
			//added by vijay on 17 April 2017
			getCustomerInteractionDetails();
		}
	}

	private void getBillingDetails() {

		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("personInfo.count");
		filter.setIntValue(Integer.parseInt(form.person.getId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillingDocument());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				ArrayList<BillingDocument> billinglist = new ArrayList<BillingDocument>();
				if(result.size()!=0){
					for(SuperModel model :result){
						BillingDocument type = (BillingDocument) model;
						globalBillingList.add(type);
						if(type.getStatus().equals("Created"))
							billinglist.add(type);
					}
				}
				
				form.getBillingdetailsTable().connectToLocal();
				form.getBillingdetailsTable().getDataprovider().setList(billinglist);
				form.getBillingdetailsTable().addColumnSorting();
				
			}
			
			@Override
			public void onFailure(Throwable caught) {

				
			}
		});
	
	}

	private void getCustomerInteractionDetails() {

		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("personInfo.count");
		filter.setIntValue(Integer.parseInt(form.person.getId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new InteractionType());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				ArrayList<InteractionType> interactionToDolist = new ArrayList<InteractionType>();
				if(result.size()!=0){
					for(SuperModel model :result){
						InteractionType type = (InteractionType) model;
						globalInteractionlist.add(type);
						if(type.getInteractionStatus().equals("Created"))
						interactionToDolist.add(type);
					}
				}
				
				form.getInteractionToDoTable().connectToLocal();
				form.getInteractionToDoTable().getDataprovider().setList(interactionToDolist);
				form.getInteractionToDoTable().addColumnSorting();
				
			}
			
			@Override
			public void onFailure(Throwable caught) {

				
			}
		});
	
	}

	private void getCustomerComplainDetails() {
		
		System.out.println("getCustomerComplainDetails");
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("personinfo.count");
		filter.setIntValue(Integer.parseInt(form.person.getId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Complain());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught){
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				
				System.out.println("inside complain querry ");	
					List<ContractTableValueProxy> lis = new ArrayList<ContractTableValueProxy>();
				List<Complain> complainLis = new ArrayList<Complain>();	
				for(SuperModel model:result)
				{
					
					Complain complain = (Complain)model;
					complainLis.add(complain);
				}
				
//				globalLeadlis=leaList;
//				System.out.println("globallist"+globalLeadlis.size());
//				for(int j=0;j<complainLis.size();j++)
//				{
//					ContractTableValueProxy value = new ContractTableValueProxy();
//					
//					if(complainLis.get(j).getContrtactId()!=null)
//					{
//						value.setContractId(complainLis.get(j).getContrtactId());
//					}
//					if(complainLis.get(j).getServiceId()!=null)
//					{
//					value.setServiceId(complainLis.get(j).getServiceId());
//					}
//					value.setTicketDate(complainLis.get(j).getDate());
//					value.setTicketNumbr(complainLis.get(j).getCount());
//					value.setTitle(complainLis.get(j).getComplainTitle());
//					value.setStatus(complainLis.get(j).getCompStatus());
//					lis.add(value);
//				}
				form.getCustomerSupportTableProxy().connectToLocal();
				form.getCustomerSupportTableProxy().getDataprovider().setList(complainLis);
				form.getLeadTable().addColumnSorting();
			}
			});
	}

	private void getcustomerLeadDetails() {

		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("personInfo.count");
		filter.setIntValue(Integer.parseInt(form.person.getId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Lead());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught){
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				
					List<ContractTableValueProxy> lis = new ArrayList<ContractTableValueProxy>();
				List<Lead> leaList = new ArrayList<Lead>();	
				for(SuperModel model:result)
				{
					
					Lead invoice = (Lead)model;
					leaList.add(invoice);
				}
				
				globalLeadlis=leaList;
				System.out.println("globallist"+globalLeadlis.size());
				for(int j=0;j<leaList.size();j++)
				{
					if(leaList.get(j).getStatus().equals("Created")){
					ContractTableValueProxy value = new ContractTableValueProxy();
					
					value.setLeadID(leaList.get(j).getCount());
					value.setLeadDate(leaList.get(j).getCreationDate());
					value.setLeadTitle(leaList.get(j).getTitle());
					value.setStatus(leaList.get(j).getStatus());
					lis.add(value);
					}
				}
				form.getLeadTable().connectToLocal();
				form.getLeadTable().getDataprovider().setList(lis);
				form.getLeadTable().addColumnSorting();
			}
			});
	}

	private void getCustomerQuotationDetails() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(Integer.parseInt(form.person.getId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Quotation());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught){
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				
					List<ContractTableValueProxy> lis = new ArrayList<ContractTableValueProxy>();
				List<Quotation> quotationList = new ArrayList<Quotation>();	
				for(SuperModel model:result)
				{
					
					Quotation quotation = (Quotation)model;
					quotationList.add(quotation);
				}
				globalQuotationlis=quotationList;
				
				System.out.println("loading quotation global list size"+globalQuotationlis.size()+"-"+globalQuotationlis.size());
				for(int j=0;j<quotationList.size();j++)
				{
					if(quotationList.get(j).getStatus().equals("Approved")){
					ContractTableValueProxy value = new ContractTableValueProxy();
					
					value.setQuotationId(quotationList.get(j).getCount());
					value.setQuotationDate(quotationList.get(j).getCreationDate());
					value.setValidUntil(quotationList.get(j).getValidUntill());
					value.setStatus(quotationList.get(j).getStatus());
					lis.add(value);
					}
				}
				form.getQuotationTable().connectToLocal();
				form.getQuotationTable().getDataprovider().setList(lis);
				form.getQuotationTable().addColumnSorting();
				
			}
			});
	}

	private void getCustomerServiceDetails() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("personInfo.count");
		filter.setIntValue(Integer.parseInt(form.person.getId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught){
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				int red=0;
				int green=0;
				int yellow=0;
				
				
				List<ContractTableValueProxy> serlis = new ArrayList<ContractTableValueProxy>();
				List<Service> serviceList = new ArrayList<Service>();	
				for(SuperModel model:result)
				{
					
					Service service = (Service)model;
					serviceList.add(service);
				}
				
				globalServicetlis=serviceList;
				List<Service> custserviceList = new ArrayList<Service>();
				for(int j=0;j<serviceList.size();j++)
				{
					
					
					Date todaysDate=CalendarUtil.copyDate(new Date());
					
					CalendarUtil.addDaysToDate(todaysDate,-7);
					todaysDate.setHours(23);
					todaysDate.setMinutes(59);
					todaysDate.setSeconds(59);
					
					if((serviceList.get(j).getServiceDate().before(new Date()))&&(serviceList.get(j).getStatus().equals("Scheduled")||serviceList.get(j).getStatus().equals("Rescheduled"))){
						red = red+1;
					 }
					
					 else if((serviceList.get(j).getServiceDate().after(todaysDate)&&serviceList.get(j).getServiceDate().before(new Date()))&&(serviceList.get(j).getStatus().equals("Scheduled")||serviceList.get(j).getStatus().equals("Rescheduled")))
					 {
						 yellow=yellow+1;
					 }
					 else if(serviceList.get(j).getStatus().equals("Completed")){
						 green = green+1;
					 }
					
					 if(red!=0){
						 form.getRedService().addStyleName("red"); 
					 }
					 if(green!=0){
						 form.getGreenService().addStyleName("green");  
					 }
					 if(yellow!=0){
						 form.getYellowService().addStyleName("orange"); 
					 }
					
					form.getRedService().setValue(red+"");
					form.getGreenService().setValue(green+"");
					form.getYellowService().setValue(yellow+"");
					
					if(serviceList.get(j).getStatus().equals("Scheduled")){
					
//					ContractTableValueProxy value = new ContractTableValueProxy();
//					
//					value.setContractId(serviceList.get(j).getContractCount());
//					value.setServiceId(serviceList.get(j).getCount());
//					value.setServiceDate(serviceList.get(j).getServiceDate());
//					value.setStatus(serviceList.get(j).getStatus());
//					serlis.add(value);
						
						custserviceList.add(serviceList.get(j));
					}
				}
					
				form.getServiceTable().connectToLocal();
				form.getServiceTable().getDataprovider().setList(custserviceList);
				form.getServiceTable().addColumnSorting();
			}
			});																					
	}

	private void getCustomerInvoiceDetails() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("personInfo.count");
		filter.setIntValue(Integer.parseInt(form.person.getId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		
		if(LoginPresenter.loggedInCustomer==true){
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setStringValue(Invoice.APPROVED);
			filtervec.add(filter);
			querry.setFilters(filtervec);
		}
		
		querry.setQuerryObject(new Invoice());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught){
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				int red=0;
				int green=0;
				int yellow=0;
				
				List<ContractTableValueProxy> lis = new ArrayList<ContractTableValueProxy>();
				List<Invoice> invoicelist = new ArrayList<Invoice>();	
				for(SuperModel model:result)
				{
					
					Invoice invoice = (Invoice)model;
					invoicelist.add(invoice);
				}
				globalInvoicelis=invoicelist;
				
				List<Invoice> invoiceList= new ArrayList<Invoice>();
				for(int j=0;j<invoicelist.size();j++)
				{
					
					Date todaysDate=CalendarUtil.copyDate(new Date());
					
					CalendarUtil.addDaysToDate(todaysDate,-7);
					todaysDate.setHours(23);
					todaysDate.setMinutes(59);
					todaysDate.setSeconds(59);
					
					if((invoicelist.get(j).getInvoiceDate().before(new Date()))&&(invoicelist.get(j).getStatus().equals("Created")||invoicelist.get(j).getStatus().equals("Requested"))){
						red = red+1;
					 }
					
					 else if((invoicelist.get(j).getInvoiceDate().after(todaysDate)&&invoicelist.get(j).getInvoiceDate().before(new Date()))&&(invoicelist.get(j).getStatus().equals("Created")||invoicelist.get(j).getStatus().equals("Requested")))
					 {
						 yellow=yellow+1;
					 }
					 else if(invoicelist.get(j).getStatus().equals("Approved")){
						 green = green+1;
					 }
					
					 if(red!=0){
						 form.getRedInvoice().addStyleName("red"); 
					 }
					 if(green!=0){
						 form.getGreenInvoice().addStyleName("green");  
					 }
					 if(yellow!=0){
						 form.getYellowInvoice().addStyleName("orange"); 
					 }
					
					
					form.getRedInvoice().setValue(red+"");
					form.getGreenInvoice().setValue(green+"");
					form.getYellowInvoice().setValue(yellow+"");
					
					
					if(invoicelist.get(j).getStatus().equals("Created")||(globalInvoicelis.get(j).getStatus().equals("Requested"))){
						
						invoiceList.add(invoicelist.get(j));
					}
				}
				form.getInvoiceTable().connectToLocal();
				form.getInvoiceTable().getDataprovider().setList(invoiceList);
				form.getInvoiceTable().addColumnSorting();
			}
			});
	}

	private void getCustomerPaymentsDetails() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("personInfo.count");
		filter.setIntValue(Integer.parseInt(form.person.getId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerPayment());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				
				List<ContractTableValueProxy> lis = new ArrayList<ContractTableValueProxy>();
				List<CustomerPayment> paylist = new ArrayList<CustomerPayment>();	
				for(SuperModel model:result)
				{
					
					CustomerPayment payment = (CustomerPayment)model;
					paylist.add(payment);
				}
				
				globalPaymentlis=paylist;
				int red=0;
				int green = 0;
				int yellow=0;
				
				
				List<CustomerPayment> custpaylist = new ArrayList<CustomerPayment>();
				for(int j=0;j<paylist.size();j++)
				{
					Date todaysDate=CalendarUtil.copyDate(new Date());
					
					CalendarUtil.addDaysToDate(todaysDate,-7);
					todaysDate.setHours(23);
					todaysDate.setMinutes(59);
					todaysDate.setSeconds(59);
					
					if((paylist.get(j).getPaymentDate().before(new Date()))&&(paylist.get(j).getStatus().equals("Created"))){
						red = red+1;
					 }
					
					 else if((paylist.get(j).getPaymentDate().after(todaysDate)&&paylist.get(j).getPaymentDate().before(new Date()))&&(paylist.get(j).getStatus().equals("Created")))
					 {
						 yellow=yellow+1;
					 }
					 else if(paylist.get(j).getStatus().equals("Closed")){
						 green = green+1;
					 }
					
					 if(red!=0){
						 form.getRedPayment().addStyleName("red"); 
					 }
					 if(green!=0){
						 form.getGreenPayment().addStyleName("green");  
					 }
					 if(yellow!=0){
						 form.getYellowPayment().addStyleName("orange"); 
					 }
					
					form.getRedPayment().setValue(red+"");
					form.getGreenPayment().setValue(green+"");
					form.getYellowPayment().setValue(yellow+"");
					
					
					if(paylist.get(j).getStatus().equals("Created")){
						
						custpaylist.add(paylist.get(j));
					}
				}
				form.getPaymentTable().connectToLocal();
				form.getPaymentTable().getDataprovider().setList(custpaylist);
				form.getPaymentTable().addColumnSorting();
			}
			});
	}

	private void getCustomerAddressDetails() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(form.person.getId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				for(SuperModel model:result)
				{
					
					String address = null;
					String address2 = null;
					String landmark = null;
					String locality = null;
					Customer cust = (Customer)model;
					
					customer=cust;
					
					if(cust.getAdress().getAddrLine2()!=null && cust.getAdress().getAddrLine2().equals("")==false)
					{
						address2=cust.getAdress().getAddrLine2()+",";
					}
					else
					{
						address2="";
					}
					
					if(cust.getAdress().getLandmark()!=null && cust.getAdress().getLandmark().equals("")==false)
					{
						landmark=cust.getAdress().getLandmark()+",";
					}
					else
					{
						landmark="";
					}
					
					if(cust.getAdress().getLocality()!=null && cust.getAdress().getLocality().equals("")==false)
					{
						locality=cust.getAdress().getLocality()+",";
					}
					else
					{
						locality="";
					}
					
					address = cust.getAdress().getAddrLine1()+","+address2+landmark+locality+cust.getAdress().getCity()
							+"-"+cust.getAdress().getPin()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry();
					
					form.getAddressinfo().setValue(address);
					if(!cust.getRefrNumber1().equals("")){
						form.getCustRefNumber().setValue(cust.getRefrNumber1());
					}
					else
					{
						form.getCustRefNumber().setValue("");
					}
					form.googleDriveLinkTable.connectToLocal();
					if(cust.getGoogleDriveLinkList().size()!=0)
					{
						form.googleDriveLinkTable.getDataprovider().setList(cust.getGoogleDriveLinkList());
					}
					form.getLeadTable().addColumnSorting();
				}
				
			}
			});
		
	}

	private void getcontractIdfomCustomerID() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(Integer.parseInt(form.person.getId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		
		if(LoginPresenter.loggedInCustomer==true){
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setStringValue(Contract.APPROVED);
			filtervec.add(filter);
			querry.setFilters(filtervec);
		}
		
		
		
		querry.setQuerryObject(new Contract());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				int red=0;
				int green=0;
				int yellow=0;
				
				int renewred=0;
				int renewgreen=0;
				int renewyellow=0;
				
				List<Contract> conlist = new ArrayList<Contract>();	
				final List<String> contableList=  new ArrayList<String>();
				for(SuperModel model:result)
				{
					
					Contract conEntity = (Contract)model;
					contableList.add(conEntity.getCount()+"");
					conlist.add(conEntity);
				}
				globalContractlis=conlist;
				for(int i=0;i<contableList.size();i++)
				{
				      form.getContractList().addItem(contableList.get(i));
				}
				
				//***************for contract renewal *****************
				List<Contract> conrenewList = new ArrayList<Contract>();
				
				List<Contract> conrenewListForCustomer = new ArrayList<Contract>();
				
				for(int j=0;j<conlist.size();j++)
				{
					final Date sub30Days=CalendarUtil.copyDate(new Date());
					CalendarUtil.addDaysToDate(sub30Days, -30);
					sub30Days.setHours(23);
					sub30Days.setMinutes(59);
					sub30Days.setSeconds(59);
					
					
							 if((conlist.get(j).getEndDate().before(new Date())&&conlist.get(j).getEndDate().after(sub30Days))&&(conlist.get(j).getStatus().equals("Approved")))
							 {
								 renewyellow=renewyellow+1;
								 
								 Contract con=conlist.get(j);
								 con.setStatus("Due");
								 conrenewListForCustomer.add(con);
							 }
							 else if((new Date().after(conlist.get(j).getEndDate()))&&(conlist.get(j).getStatus().equals("Approved")))
							 {
								 renewred=renewred+1;
								 
								 Contract con=conlist.get(j);
								 con.setStatus("Overdue");
								 conrenewListForCustomer.add(con);
								 
							 }
							 else if(conlist.get(j).getStatus().equals("Approved")&&conlist.get(j).getRefContractCount()!=-1)
							 {
								 renewgreen = renewgreen+1;
							 }
					
							 
							 if((conlist.get(j).getEndDate().before(new Date())&&conlist.get(j).getEndDate().after(sub30Days))||(new Date().after(conlist.get(j).getEndDate()))
									 &&(conlist.get(j).getStatus().equals("Approved")))
							 {
								 conrenewList.add(conlist.get(j));
							 }
							
					}
				
				form.getContractrenewalTable().connectToLocal();
				if(LoginPresenter.loggedInCustomer==false){
					form.getContractrenewalTable().getDataprovider().setList(conrenewList);
				}else{
					form.getContractrenewalTable().getDataprovider().setList(conrenewListForCustomer);
				}
				
				 if(renewred!=0){
					 form.getRedConRenew().addStyleName("red"); 
				 }
				 if(renewgreen!=0){
					 form.getGreenConRenew().addStyleName("green");  
				 }
				 if(renewyellow!=0){
					 form.getYellowConRenew().addStyleName("orange"); 
				 }
				
				
				form.getRedConRenew().setValue(renewred+"");
				form.getYellowConRenew().setValue(renewyellow+"");;
				form.getGreenConRenew().setValue(renewgreen+"");;
				
				
				List<Contract> custContractList = new ArrayList<Contract>();
 				//**************contract statistics *****************************
				for(int j=0;j<conlist.size();j++)
				{
					
					final Date sub7Days=CalendarUtil.copyDate(conlist.get(j).getStartDate());
					System.out.println("Todays Date ::: "+sub7Days);
					CalendarUtil.addDaysToDate(sub7Days,+7);
					System.out.println("con start Date + 7 Days ::: "+sub7Days);
					sub7Days.setHours(23);
					sub7Days.setMinutes(59);
					sub7Days.setSeconds(59);
					System.out.println("Converted Date ::: "+sub7Days);
					
					 if((new Date().after(sub7Days))&&(conlist.get(j).getStatus().equals("Created")||conlist.get(j).getStatus().equals("Requested"))){
						
						 red=red+1;
					 }
					 else if((new Date().after(conlist.get(j).getStartDate())&&(new Date().before(sub7Days)))&&(conlist.get(j).getStatus().equals("Created")||conlist.get(j).getStatus().equals("Requested")))
					 {
						 yellow=yellow+1;
					 }
					 else if(conlist.get(j).getStatus().equals("Approved")){
						 green = green+1;
					 }
					
					 if(red!=0){
						 form.getRedContract().addStyleName("red"); 
					 }
					 if(green!=0){
						 form.getGreenContract().addStyleName("green");  
					 }
					 if(yellow!=0){
						 form.getYellowContract().addStyleName("orange"); 
					 }
					 
					 form.getRedContract().setValue(red+"");
					 form.getGreenContract().setValue(green+"");
					 form.getYellowContract().setValue(yellow+"");
					 
					 
					if((conlist.get(j).getStatus().equals("Created"))||(conlist.get(j).getStatus().equals("Requested"))){ 
					 
//					ContractTableValueProxy value = new ContractTableValueProxy();
//					
//					value.setContractId(conlist.get(j).getCount());
//					value.setStartDate(conlist.get(j).getStartDate());
//					value.setEndDate(conlist.get(j).getEndDate());
//					value.setStatus(conlist.get(j).getStatus());
//					lis.add(value);
						
						custContractList.add(conlist.get(j));
					}
				}
				form.getContractTable().connectToLocal();
				form.getContractTable().getDataprovider().setList(custContractList);
				form.getContractTable().addColumnSorting();
			}
			});
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {

		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if(text.equals("Create Lead"))
		{
			if(form.getPerson().getValue()!=null){
			createLeadFromCustomer();
			}
			else
			{
				form.showDialogMessage("Please Select Customer First");
			}
		}
		
		if(text.equals("Create Quotation"))
		{
			if(form.getPerson().getValue()!=null){
			createQuotationFromCustomer();
			}
			else
			{
				form.showDialogMessage("Please Select Customer First");
			}
		}
		
		if(text.equals("Create Order"))
		{	
			if(form.getPerson().getValue()!=null){
			createContractFromCustomer();
			}
			else
			{
				form.showDialogMessage("Please Select Customer First");
			}
		}
		
//		if(text.equals("Customer Support"))
//		{
//			if(form.getPerson().getValue()!=null)
//			{
//			connectToCustomerSupportScreen();
//			}
//			else
//			{
//				form.showDialogMessage("Please Select Customer First");
//			}
//		}
		/**
		 *  2-10-2017
		 *  customer support and raise ticket are same so this code is done
		 */
		if(text.equals("Raise Ticket"))
			{
				if(form.getPerson().getValue()!=null)
				{
				connectToCustomerSupportScreen();
				}
				else
				{
					form.showDialogMessage("Please Select Customer First");
				}
			}
		
		if(text.equals("Request Service"))
		{
			panel = new PopupPanel(true);
			panel.add(datePopup);
			datePopup.clear();
			panel.show();
			panel.center();
		}
	}

	private void createContractFromCustomer() {
		
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract",Screen.CONTRACT);
		final ContractForm quotationform=ContractPresenter.initalize();
		Timer t = new Timer() {
		      @Override
		      public void run() {
		    	  quotationform.setToNewState();
		    	  quotationform.getPersonInfoComposite().setValue(form.getPerson().getValue());
		    	  /**
		    	   * @author Abhinav Bihade
		    	   * @since 19/12/2019
		    	   * As per Rahul Tiwari's Requirement Orkin : From home page, 
		    	   * if I select a customer and if I try to create a contract then on contract I am unable to add service product.
		    	   */
		    	  quotationform.getChkbillingatBranch().setValue(false);
		    	  quotationform.getChkservicewithBilling().setValue(false);
		    	  quotationform.getCbServiceWiseBilling().setValue(false);
		    	  quotationform.checkCustomerStatus(form.getPerson().getIdValue(), true,true);
		    	  
		    	  /**
		    	   * End
		    	   */
		    	  
		      }
		    };
		    t.schedule(1000);
	}

	private void createQuotationFromCustomer() {
		
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quotation",Screen.QUOTATION);
		final QuotationForm quotationform=QuotationPresenter.initalize();
		Timer t = new Timer() {
		      @Override
		      public void run() {
		    	  quotationform.setToNewState();
		    	  quotationform.getPersonInfoComposite().setValue(form.getPerson().getValue());
		      }
		    };
		    t.schedule(1000);
	
	}

	private void createLeadFromCustomer() {
		
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Lead",Screen.LEAD);
		final LeadForm quotationform=LeadPresenter.initalize();
		Timer t = new Timer() {
		      @Override
		      public void run() {
		    	  quotationform.setToNewState();
		    	  quotationform.getPic().setValue(form.getPerson().getValue());
		      }
		    };
		    t.schedule(100);
	}

	private void connectToCustomerSupportScreen() {
		
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/CustomerSupport",Screen.COMPLAIN);
			final ComplainForm quotationform=ComplainPresenter.initalize();
			Timer t = new Timer() {
			      @Override
			      public void run() {
			    	  quotationform.setToNewState();
			    	  quotationform.getPic().setValue(form.getPerson().getValue());
			      }
			    };
			    t.schedule(100);
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		System.out.println("in side on click");
		
		if(event.getSource().equals(form.getAddlink())){
			if(form.person.getValue()!=null){
				panel =new PopupPanel(true);
				googlelinkpopup.clear();
				panel.add(googlelinkpopup);
				panel.center();
				panel.show();
			}else{
				form.showDialogMessage("Please select customer!");
			}
		}
		
		if(event.getSource()==googlelinkpopup.getBtnOk()){
			if(googlelinkpopup.validate()){
				String remark ="";
				if(!googlelinkpopup.getRemark().getValue().equals("")){
					remark=googlelinkpopup.getRemark().getValue();
				}
				saveGoogleDriveLink(googlelinkpopup.getDateBox().getValue(), googlelinkpopup.getOlbDocumnetType().getValue(), googlelinkpopup.getTextArea().getValue(),remark);
				panel.hide();
			}
		}
		
		if(event.getSource()==googlelinkpopup.getBtnCancel()){
			panel.hide();
		}
		
		if (event.getSource() == datePopup.getBtnOne()) {
			if (datePopup.getServiceDate().getValue() != null) {
				Date serviceDate = datePopup.getServiceDate().getValue();
				System.out.println("Date :::::::: " + serviceDate);
				
				String desc=null;
				if(datePopup.getTaDesc().getValue()!=null){
					desc=datePopup.getTaDesc().getValue();
				}
				System.out.println("Description :: "+desc);
				
				requestService(serviceDate,desc);
				panel.hide();
				
			} else {
				form.showDialogMessage("Please select date!");
			}
		}
		if (event.getSource() == datePopup.getBtnTwo()) {
			panel.hide();
		}
		
		
		
		
		
		
		if(event.getSource().equals(form.getGobutton()))
		{
			System.out.println("in side condition ");
			getCustomerContractDetails();
			getCustomerPaymentsDetailsFromContract();
			getCustomerInvoiceDetailsFromContract();
			getCustomerServiceDetailsFromContract();
			getCustomerBillingDetailsFromContract();
		}
		
		if(event.getSource().equals(form.getClearbutton()))
		{
			System.out.println("in side clear button");
			form.getContractTable().connectToLocal();
			form.getServiceTable().connectToLocal();
			form.getInvoiceTable().connectToLocal();
			form.getPaymentTable().connectToLocal();
		}
		
		
		if(event.getSource().equals(form.getGobutton123()))
		{
			System.out.println("in side getGobutton123 button");
			
			getAllDetails();
		}
		
		
		//***************for download  ************************
		
		if(event.getSource().equals(form.getDownConRenew()))
		{
			System.out.println("in side getDownContract  renewal button");
			
			ArrayList<Contract> custarray=new ArrayList<Contract>();
			List<Contract> list=(List<Contract>)  form.getContractrenewalTable().getDataprovider().getList();
			
			custarray.addAll(list);
			
			csvservice.setcontractlist(custarray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}

				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+6;
					Window.open(url, "test", "enabled");
				}
			});
		}
		
		
		if(event.getSource().equals(form.getDownCustSupport()))
		{
		ArrayList<Complain> woArray = new ArrayList<Complain>();
		List<Complain> list = (List<Complain>) form.getCustomerSupportTableProxy().getDataprovider().getList();
		woArray.addAll(list);
		csvservice.setComplainlist(woArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 95;
				Window.open(url, "test", "enabled");
			}
		});
		}
		
		if(event.getSource().equals(form.getDownContract()))
		{
			System.out.println("in side getDownContract button");
			
			ArrayList<Contract> custarray=new ArrayList<Contract>();
			List<Contract> list=(List<Contract>)  form.getContractTable().getDataprovider().getList();
			
			custarray.addAll(list);
			
			csvservice.setcontractlist(custarray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}

				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+6;
					Window.open(url, "test", "enabled");
				}
			});
			
		}
		
		if(event.getSource().equals(form.getDownService()))
		{
			System.out.println("in side getDownService button");
			
			ArrayList<Service> custarray=new ArrayList<Service>();
			List<Service> list=(List<Service>) form.getServiceTable().getListDataProvider().getList();
			
			custarray.addAll(list);
			
			csvservice.setservice(custarray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}

				@Override
				public void onSuccess(Void result) {
					

					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+5;
					Window.open(url, "test", "enabled");
					
				}
			});
			
		}
		
		if(event.getSource().equals(form.getDownInvoice()))
		{
			System.out.println("in side getDownInvoice button");
			
			ArrayList<Invoice> invoicearray=new ArrayList<Invoice>();
			List<Invoice> list=(List<Invoice>) form.getInvoiceTable().getDataprovider().getList();
			invoicearray.addAll(list);
			
			csvservice.setinvoicelist(invoicearray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				@Override
				public void onSuccess(Void result) {
					
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+15;
					Window.open(url, "test", "enabled");
				}
			});
		}
		
		if(event.getSource().equals(form.getDownPayment()))
		{
			System.out.println("in side getDownPayment button");
			
			
			ArrayList<CustomerPayment> paymentarray=new ArrayList<CustomerPayment>();
			List<CustomerPayment> list=(List<CustomerPayment>) form.getPaymentTable().getDataprovider().getList();
			paymentarray.addAll(list);
			
			csvservice.setpaymentlist(paymentarray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				@Override
				public void onSuccess(Void result) {
					
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+16;
					Window.open(url, "test", "enabled");
				}
			});
			
		}
		
		if(event.getSource().equals(form.getDownLead()))
		{
			System.out.println("in side getDownLead button");
			
			ArrayList<ContractTableValueProxy> custarray=new ArrayList<ContractTableValueProxy>();
			List<ContractTableValueProxy> list=(List<ContractTableValueProxy>) form.getLeadTable().getDataprovider().getList();
			
			custarray.addAll(list);
			
			csvservice.setCustomerHelpDesk(custarray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				
				@Override
				public void onSuccess(Void result) {
		
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+101;
					Window.open(url, "test", "enabled");
				}
			});
		}
		
		if(event.getSource().equals(form.getDownQuotation()))
		{
			System.out.println("in side getDownQuotation button");
			ArrayList<ContractTableValueProxy> custarray=new ArrayList<ContractTableValueProxy>();
			List<ContractTableValueProxy> list=(List<ContractTableValueProxy>) form.getQuotationTable().getDataprovider().getList();
			
			custarray.addAll(list);
			
			csvservice.setCustomerHelpDesk(custarray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				
				@Override
				public void onSuccess(Void result) {
		
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+102;
					Window.open(url, "test", "enabled");
				}
			});
		}
		
				//added by vijay on 17 April 2017
				if(event.getSource().equals(form.getInteractionDownload())){
					
					ArrayList<InteractionType> interactionarray = new ArrayList<InteractionType>();
					List<InteractionType> list = form.getInteractionToDoTable().getListDataProvider().getList();
					
					interactionarray.addAll(list);
					csvservice.setInteractionDetails(interactionarray, new AsyncCallback<Void>() {

						@Override
						public void onFailure(Throwable caught) {
							System.out.println("RPC call Failed"+caught);
							
						}

						@Override
						public void onSuccess(Void result) {
							

							String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
							final String url=gwt + "csvservlet"+"?type="+64;
							Window.open(url, "test", "enabled");
							
						}
					});
				}
		if(event.getSource().equals(form.getDownBillingDetails())){
			
			ArrayList<BillingDocument> conbillingarray = new ArrayList<BillingDocument>();
			List<BillingDocument> list = form.getBillingdetailsTable().getListDataProvider().getList();
			
			conbillingarray.addAll(list);
			csvservice.setconbillinglist(conbillingarray,new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+14;
					Window.open(url, "test", "enabled");
				}
			});
		}
	}
	
	private void getCustomerBillingDetailsFromContract() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		int index =form.getContractList().getSelectedIndex();
		filter.setIntValue(Integer.parseInt(form.getContractList().getValue(index)));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillingDocument());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught){
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				
//					List<ContractTableValueProxy> serlis = new ArrayList<ContractTableValueProxy>();
				List<BillingDocument> billingDocumentList = new ArrayList<BillingDocument>();	
				for(SuperModel model:result)
				{
					
					BillingDocument billingDocs = (BillingDocument)model;
					billingDocumentList.add(billingDocs);
				}
				
				
				form.getBillingdetailsTable().getDataprovider().setList(billingDocumentList);
				form.getBillingdetailsTable().addColumnSorting();
			}
			});	
	}

	private void getAllDetails() {
		
			if(form.getSelectionMode().getValue(form.getSelectionMode().getSelectedIndex()).equals("ALL")){
			
			System.out.println("in side all Contract value"+globalContractlis.size());
//			List<ContractTableValueProxy> contractList =new ArrayList<ContractTableValueProxy>();
//			for(int j=0;j<globalContractlis.size();j++)
//			{
//				 
//				ContractTableValueProxy value = new ContractTableValueProxy();
//				
//				value.setContractId(globalContractlis.get(j).getCount());
//				value.setStartDate(globalContractlis.get(j).getStartDate());
//				value.setEndDate(globalContractlis.get(j).getEndDate());
//				value.setStatus(globalContractlis.get(j).getStatus());
//				contractList.add(value);
//			}
			form.getContractTable().connectToLocal();
			form.getContractTable().getDataprovider().setList(globalContractlis);
			form.getContractTable().addColumnSorting();
			
			
			System.out.println("in side all Service value"+globalServicetlis.size());
//			List<ContractTableValueProxy> serviceList =new ArrayList<ContractTableValueProxy>();
//			for(int j=0;j<globalServicetlis.size();j++)
//			{
//			
//				ContractTableValueProxy value = new ContractTableValueProxy();
//				
//				value.setContractId(globalServicetlis.get(j).getContractCount());
//				value.setServiceId(globalServicetlis.get(j).getCount());
//				value.setServiceDate(globalServicetlis.get(j).getServiceDate());
//				value.setStatus(globalServicetlis.get(j).getStatus());
//				serviceList.add(value);
//			}
				
			form.getServiceTable().connectToLocal();
			form.getServiceTable().getDataprovider().setList(globalServicetlis);
			form.getServiceTable().addColumnSorting();
			
			form.getBillingdetailsTable().connectToLocal();
			form.getBillingdetailsTable().getDataprovider().setList(globalBillingList);
			form.getBillingdetailsTable().addColumnSorting();
			
			
			System.out.println("in side all invoice value"+globalInvoicelis.size());
			List<ContractTableValueProxy> invoicelis =new ArrayList<ContractTableValueProxy>();
//			for(int j=0;j<globalInvoicelis.size();j++)
//			{
//				
//				ContractTableValueProxy value = new ContractTableValueProxy();
//				
//				value.setContractId(globalInvoicelis.get(j).getContractCount());
//				value.setInvoiceId(globalInvoicelis.get(j).getCount());
//				value.setInvoiceDate(globalInvoicelis.get(j).getInvoiceDate());
//				value.setInvoiceAmt(globalInvoicelis.get(j).getInvoiceAmount());
//				value.setStatus(globalInvoicelis.get(j).getStatus());
//				invoicelis.add(value);
//			}
			form.getInvoiceTable().connectToLocal();
			form.getInvoiceTable().getDataprovider().setList(globalInvoicelis);
			form.getInvoiceTable().addColumnSorting();
			
			System.out.println("in side all payment value"+globalPaymentlis.size());
//			List<ContractTableValueProxy> paymentlis =new ArrayList<ContractTableValueProxy>();
//			for(int j=0;j<globalPaymentlis.size();j++)
//			{
//				ContractTableValueProxy value = new ContractTableValueProxy();
//				
//				value.setContractId(globalPaymentlis.get(j).getContractCount());
//				value.setPaymentId(globalPaymentlis.get(j).getCount());
//				value.setPaymentDate(globalPaymentlis.get(j).getPaymentDate());
//				
//				value.setOrderAmt(globalPaymentlis.get(j).getTotalSalesAmount());
//				value.setBalAMt(globalPaymentlis.get(j).getPaymentAmt());
//				value.setStatus(globalPaymentlis.get(j).getStatus());
//				
//				paymentlis.add(value);
//			}
			form.getPaymentTable().connectToLocal();
			form.getPaymentTable().getDataprovider().setList(globalPaymentlis);
			form.getPaymentTable().addColumnSorting();
			
			System.out.println("in side all quotation value"+globalQuotationlis.size());
			List<ContractTableValueProxy> quotationlis =new ArrayList<ContractTableValueProxy>();
			for(int j=0;j<globalQuotationlis.size();j++)
			{
				ContractTableValueProxy value = new ContractTableValueProxy();
				
				value.setQuotationId(globalQuotationlis.get(j).getCount());
				value.setQuotationDate(globalQuotationlis.get(j).getCreationDate());
				value.setValidUntil(globalQuotationlis.get(j).getValidUntill());
				value.setStatus(globalQuotationlis.get(j).getStatus());
				quotationlis.add(value);
			}
			form.getQuotationTable().connectToLocal();
			form.getQuotationTable().getDataprovider().setList(quotationlis);
			form.getQuotationTable().addColumnSorting();
			
			System.out.println("in side all the Lead data mapping "+globalLeadlis.size());
			List<ContractTableValueProxy> lis =new ArrayList<ContractTableValueProxy>();
			for(int i=0;i<globalLeadlis.size();i++)
			{
				ContractTableValueProxy value = new ContractTableValueProxy();
				
				value.setLeadID(globalLeadlis.get(i).getCount());
				value.setLeadDate(globalLeadlis.get(i).getCreationDate());
				value.setLeadTitle(globalLeadlis.get(i).getTitle());
				value.setStatus(globalLeadlis.get(i).getStatus());
				lis.add(value);
			}
			
			form.getLeadTable().connectToLocal();
			form.getLeadTable().getDataprovider().setList(lis);
			form.getLeadTable().addColumnSorting();
			
			
			/**
			 * Date 17 April 2017 added by vijay for interaction todo
			 */
			
			System.out.println("interaction global list size ==="+globalInteractionlist.size());
			form.getInteractionToDoTable().connectToLocal();
			form.getInteractionToDoTable().getDataprovider().setList(globalInteractionlist);
			form.getInteractionToDoTable().addColumnSorting();
			/**
			 * ends here
			 */
			
			}
			
			
			if(form.getSelectionMode().getValue(form.getSelectionMode().getSelectedIndex()).equals("OPEN")){
				
				form.getContractTable().connectToLocal();
				form.getServiceTable().connectToLocal();
				form.getInvoiceTable().connectToLocal();
				form.getPaymentTable().connectToLocal();
				form.getBillingdetailsTable().connectToLocal();
				
				System.out.println("in side Contract value");
				List<Contract> contractList =new ArrayList<Contract>();
				for(int j=0;j<globalContractlis.size();j++)
				{
					if((globalContractlis.get(j).getStatus().equals("Created"))||((globalContractlis.get(j).getStatus().equals("Requested")))){ 
					 
//					ContractTableValueProxy value = new ContractTableValueProxy();
//					
//					value.setContractId(globalContractlis.get(j).getCount());
//					value.setStartDate(globalContractlis.get(j).getStartDate());
//					value.setEndDate(globalContractlis.get(j).getEndDate());
//					value.setStatus(globalContractlis.get(j).getStatus());
//					contractList.add(value);
						
						contractList.add(globalContractlis.get(j));
					}
				}
				form.getContractTable().connectToLocal();
				form.getContractTable().getDataprovider().setList(contractList);
				form.getContractTable().addColumnSorting();
				
				
				System.out.println("in side Service value");
				List<Service> serviceList =new ArrayList<Service>();
				for(int j=0;j<globalServicetlis.size();j++)
				{
					if(globalServicetlis.get(j).getStatus().equals("Scheduled")){
//					ContractTableValueProxy value = new ContractTableValueProxy();
//					
//					value.setContractId(globalServicetlis.get(j).getContractCount());
//					value.setServiceId(globalServicetlis.get(j).getCount());
//					value.setServiceDate(globalServicetlis.get(j).getServiceDate());
//					value.setStatus(globalServicetlis.get(j).getStatus());
//					serviceList.add(value);
						serviceList.add(globalServicetlis.get(j));
					}
				}
					
				form.getServiceTable().connectToLocal();
				form.getServiceTable().getDataprovider().setList(serviceList);
				form.getServiceTable().addColumnSorting();
				
				
				System.out.println("in side invoice value");
				List<Invoice> invoicelis =new ArrayList<Invoice>();
				for(int j=0;j<globalInvoicelis.size();j++)
				{
					if(globalInvoicelis.get(j).getStatus().equals("Created")||globalInvoicelis.get(j).getStatus().equals("Requested"))
					{
						invoicelis.add(globalInvoicelis.get(j));
					};
				}
				form.getInvoiceTable().connectToLocal();
				form.getInvoiceTable().getDataprovider().setList(invoicelis);
				form.getInvoiceTable().addColumnSorting();
				
				System.out.println("in side payment value");
				List<CustomerPayment> paymentlis =new ArrayList<CustomerPayment>();
				for(int j=0;j<globalPaymentlis.size();j++)
				{
					if(globalPaymentlis.get(j).getStatus().equals("Created")){
						paymentlis.add(globalPaymentlis.get(j));
					}
				}
				form.getPaymentTable().connectToLocal();
				form.getPaymentTable().getDataprovider().setList(paymentlis);
				form.getPaymentTable().addColumnSorting();
				
				System.out.println("in side quotation value");
				List<ContractTableValueProxy> quotationlis =new ArrayList<ContractTableValueProxy>();
				for(int j=0;j<globalQuotationlis.size();j++)
				{
					if(globalQuotationlis.get(j).getStatus().equals("Approved")){
					ContractTableValueProxy value = new ContractTableValueProxy();
					
					value.setQuotationId(globalQuotationlis.get(j).getCount());
					value.setQuotationDate(globalQuotationlis.get(j).getCreationDate());
					value.setValidUntil(globalQuotationlis.get(j).getValidUntill());
					value.setStatus(globalQuotationlis.get(j).getStatus());
					quotationlis.add(value);
					}
				}
				form.getQuotationTable().connectToLocal();
				form.getQuotationTable().getDataprovider().setList(quotationlis);
				form.getQuotationTable().addColumnSorting();
				
				System.out.println("in side all the Lead data mapping ");
				List<ContractTableValueProxy> lis =new ArrayList<ContractTableValueProxy>();
				for(int i=0;i<globalLeadlis.size();i++)
				{
					if(globalLeadlis.get(i).getStatus().equals("Created")){
					
					ContractTableValueProxy value = new ContractTableValueProxy();
					
					value.setLeadID(globalLeadlis.get(i).getCount());
					value.setLeadDate(globalLeadlis.get(i).getCreationDate());
					value.setLeadTitle(globalLeadlis.get(i).getTitle());
					value.setStatus(globalLeadlis.get(i).getStatus());
					lis.add(value);
					}
				}
				
				form.getLeadTable().connectToLocal();
				form.getLeadTable().getDataprovider().setList(lis);
				form.getLeadTable().addColumnSorting();
				
				/** Date 17 April 2017 added by vijay for interaction todo
				 */
				  ArrayList<InteractionType> interactionTodoOpenlist = new ArrayList<InteractionType>();
					for(int i=0;i<globalInteractionlist.size();i++){
						if(globalInteractionlist.get(i).getInteractionStatus().equals("Created")){
							interactionTodoOpenlist.add(globalInteractionlist.get(i));
						}
					}
					form.getInteractionToDoTable().connectToLocal();
					form.getInteractionToDoTable().getDataprovider().setList(interactionTodoOpenlist);
					form.getInteractionToDoTable().addColumnSorting();
				/** ends here
				 */
				/**Date 23-10-2019 by Amol for billing Details**/
				ArrayList<BillingDocument> billingDocumentOpenList=new ArrayList<BillingDocument>();
					for(int i=0;i<globalBillingList.size();i++){
						if(globalBillingList.get(i).getStatus().equals("Created")){
							billingDocumentOpenList.add(globalBillingList.get(i));
						}
					}
					form.getBillingdetailsTable().connectToLocal();
					form.getBillingdetailsTable().getDataprovider().setList(billingDocumentOpenList);
					form.getBillingdetailsTable().addColumnSorting();
					
					
					
					
			}
			
		}

	private void getCustomerContractDetails() {


		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("count");
		int index =form.getContractList().getSelectedIndex();
		filter.setIntValue(Integer.parseInt(form.getContractList().getValue(index)));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				
					List<ContractTableValueProxy> lis = new ArrayList<ContractTableValueProxy>();
				List<Contract> conlist = new ArrayList<Contract>();	
				for(SuperModel model:result)
				{
					
					Contract conEntity = (Contract)model;
					conlist.add(conEntity);
				}
				
				
//				for(int j=0;j<conlist.size();j++)
//				{
//					
//					ContractTableValueProxy value = new ContractTableValueProxy();
//					
//					value.setContractId(conlist.get(j).getCount());
//					value.setStartDate(conlist.get(j).getStartDate());
//					value.setEndDate(conlist.get(j).getEndDate());
//					value.setStatus(conlist.get(j).getStatus());
//					lis.add(value);
//				}
				form.getContractTable().getDataprovider().setList(conlist);
				form.getContractTable().addColumnSorting();
			}
			});
	}

	private void getCustomerServiceDetailsFromContract() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		int index =form.getContractList().getSelectedIndex();
		filter.setIntValue(Integer.parseInt(form.getContractList().getValue(index)));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught){
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				
					List<ContractTableValueProxy> serlis = new ArrayList<ContractTableValueProxy>();
				List<Service> serviceList = new ArrayList<Service>();	
				for(SuperModel model:result)
				{
					
					Service service = (Service)model;
					serviceList.add(service);
				}
				
//				for(int j=0;j<serviceList.size();j++)
//				{
//					ContractTableValueProxy value = new ContractTableValueProxy();
//					
//					value.setContractId(serviceList.get(j).getContractCount());
//					value.setServiceId(serviceList.get(j).getCount());
//					value.setServiceDate(serviceList.get(j).getServiceDate());
//					value.setStatus(serviceList.get(j).getStatus());
//					serlis.add(value);
//				}
				
				form.getServiceTable().getDataprovider().setList(serviceList);
				form.getServiceTable().addColumnSorting();
			}
			});	
	}

	private void getCustomerInvoiceDetailsFromContract() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		int index =form.getContractList().getSelectedIndex();
		filter.setIntValue(Integer.parseInt(form.getContractList().getValue(index)));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Invoice());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught){
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				
				List<Invoice> invoicelist = new ArrayList<Invoice>();	
				for(SuperModel model:result)
				{
					
					Invoice invoice = (Invoice)model;
					invoicelist.add(invoice);
				}
				
				form.getInvoiceTable().getDataprovider().setList(invoicelist);
				form.getInvoiceTable().addColumnSorting();
			}
			});
		
	}

	private void getCustomerPaymentsDetailsFromContract(){
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		int index =form.getContractList().getSelectedIndex();
		filter.setIntValue(Integer.parseInt(form.getContractList().getValue(index)));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerPayment());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
					List<ContractTableValueProxy> lis = new ArrayList<ContractTableValueProxy>();
				List<CustomerPayment> paylist = new ArrayList<CustomerPayment>();	
				for(SuperModel model:result)
				{
					
					CustomerPayment payment = (CustomerPayment)model;
					paylist.add(payment);
				}
				
//				for(int j=0;j<paylist.size();j++)
//				{
//					ContractTableValueProxy value = new ContractTableValueProxy();
//					
//					value.setContractId(paylist.get(j).getContractCount());
//					value.setPaymentId(paylist.get(j).getCount());
//					value.setPaymentDate(paylist.get(j).getPaymentDate());
//					value.setStatus(paylist.get(j).getStatus());
//					lis.add(value);
//				}
				
				form.getPaymentTable().getDataprovider().setList(paylist);
				form.getPaymentTable().addColumnSorting();
			}
			});
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}
	
	//Big Patch fix search 
	public void setTableSelectionOnLeads()
	 {
		 
		 final NoSelectionModel<ContractTableValueProxy> selectionModelMyObj = new NoSelectionModel<ContractTableValueProxy>();
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final ContractTableValueProxy entity=selectionModelMyObj.getLastSelectedObject();
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Lead",Screen.LEAD);
	        	 final LeadForm form = LeadPresenter.initalize();
	        	
	        	 AppMemory.getAppMemory().stickPnel(form);
	        	 form.showWaitSymbol();
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					
	 					MyQuerry querry=new MyQuerry();
	 					Vector<Filter> filtervec=new Vector<Filter>();
	 					Filter temp=null;
	 					
	 						 					
	 					temp=new Filter();
	 					temp.setQuerryString("count");
	 					temp.setIntValue(entity.getLeadID());
	 					filtervec.add(temp);
	 					
	 					querry.setFilters(filtervec);
	 					querry.setQuerryObject(new Lead());
	 					
	 					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

	 						@Override
	 						public void onFailure(Throwable caught) {
	 							form.showDialogMessage("An Unexpected Error occured !");
	 							
	 						}
	 						@Override
	 						public void onSuccess(ArrayList<SuperModel> result) {
	 							
	 							
	 							for(SuperModel pmodel:result)
	 							{
	 								Lead lead=(Lead)pmodel;
	 								form.updateView(lead);
	 								 form.setToViewState();
	 							}
	 							
	 						}
	 					});
	 					
	 					form.hideWaitSymbol();
	 				}
	 			};
	             timer.schedule(3000); 
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getLeadTable().getTable().setSelectionModel(selectionModelMyObj);
		 
		 
	 }
	 
	private void setTableSelectionOnService()
	 {
		 
		 final NoSelectionModel<Service> selectionModelMyObj = new NoSelectionModel<Service>();
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final Service entity=selectionModelMyObj.getLastSelectedObject();
	        	 
	        	 if(LoginPresenter.loggedInCustomer==false){
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Service",Screen.SERVICE);
	        	 final DeviceForm form = DevicePresenter.initalize();
	        	
	        	 AppMemory.getAppMemory().stickPnel(form);
	        	 form.showWaitSymbol();
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					
	 					MyQuerry querry=new MyQuerry();
	 					Vector<Filter> filtervec=new Vector<Filter>();
	 					Filter temp=null;
	 					
	 						 					
	 					temp=new Filter();
	 					temp.setQuerryString("count");
	 					temp.setIntValue(entity.getCount());
	 					filtervec.add(temp);
	 					
	 					querry.setFilters(filtervec);
	 					querry.setQuerryObject(new Service());
	 					
	 					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

	 						@Override
	 						public void onFailure(Throwable caught) {
	 							form.showDialogMessage("An Unexpected Error occured !");
	 							
	 						}
	 						@Override
	 						public void onSuccess(ArrayList<SuperModel> result) {
	 							
	 							
	 							for(SuperModel pmodel:result)
	 							{
	 								Service service=(Service)pmodel;
	 								form.updateView(service);
	 								 form.setToViewState();
	 							}
	 							
	 						}
	 					});
	 					
	 					form.hideWaitSymbol();
	 				}
	 			};
	             timer.schedule(3000); 
	             
	        	 }else{
	        		
	        		 createProjectCorrespondingToService(entity);
	        	 }
	             
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getServiceTable().getTable().setSelectionModel(selectionModelMyObj);
	 }
	 	 
	public void setTableSelectionOnContract()
	 {
		 final NoSelectionModel<Contract> selectionModelMyObj = new NoSelectionModel<Contract>();
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final Contract entity=selectionModelMyObj.getLastSelectedObject();
	        	 
	        	 if(LoginPresenter.loggedInCustomer==false){
	        		 
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract",Screen.CONTRACT);
	        	 final ContractForm form = ContractPresenter.initalize();
	        	
	        	 AppMemory.getAppMemory().stickPnel(form);
	        	 form.showWaitSymbol();
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					
	 					MyQuerry querry=new MyQuerry();
	 					Vector<Filter> filtervec=new Vector<Filter>();
	 					Filter temp=null;
	 					
	 						 					
	 					temp=new Filter();
	 					temp.setQuerryString("count");
	 					temp.setIntValue(entity.getCount());
	 					filtervec.add(temp);
	 					
	 					querry.setFilters(filtervec);
	 					querry.setQuerryObject(new Contract());
	 					
	 					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

	 						@Override
	 						public void onFailure(Throwable caught) {
	 							form.showDialogMessage("An Unexpected Error occured !");
	 							
	 						}
	 						@Override
	 						public void onSuccess(ArrayList<SuperModel> result) {
	 							
	 							
	 							for(SuperModel pmodel:result)
	 							{
	 								Contract con=(Contract)pmodel;
	 								form.updateView(con);
	 								 form.setToViewState();
	 							}
	 							
	 						}
	 					});
	 					
	 					form.hideWaitSymbol();
	 				}
	 			};
	             timer.schedule(3000); 
	             
	        } else{
	        	
	        	final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+entity.getId()+"&"+"type="+"c"+"&"+"preprint="+"plane";
				 Window.open(url, "test", "enabled");
	        }
	             
	             
	             
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getContractTable().getTable().setSelectionModel(selectionModelMyObj);
		 
	 }
	 	 
	public void setTableQuotation()
	 {
		 
		 final NoSelectionModel<ContractTableValueProxy> selectionModelMyObj = new NoSelectionModel<ContractTableValueProxy>();
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final ContractTableValueProxy entity=selectionModelMyObj.getLastSelectedObject();
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quotation",Screen.QUOTATION);
	        	 final QuotationForm form = QuotationPresenter.initalize();
	        
	        	 AppMemory.getAppMemory().stickPnel(form);
	        	 form.showWaitSymbol();
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					
	 					MyQuerry querry=new MyQuerry();
	 					Vector<Filter> filtervec=new Vector<Filter>();
	 					Filter temp=null;
	 					
	 						 					
	 					temp=new Filter();
	 					temp.setQuerryString("count");
	 					temp.setIntValue(entity.getQuotationId());
	 					filtervec.add(temp);
	 					
	 					querry.setFilters(filtervec);
	 					querry.setQuerryObject(new Quotation());
	 					
	 					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

	 						@Override
	 						public void onFailure(Throwable caught) {
	 							form.showDialogMessage("An Unexpected Error occured !");
	 							
	 						}
	 						@Override
	 						public void onSuccess(ArrayList<SuperModel> result) {
	 							
	 							
	 							for(SuperModel pmodel:result)
	 							{
	 								Quotation quotation=(Quotation)pmodel;
	 								form.updateView(quotation);
	 								 form.setToViewState();
	 							}
	 							
	 						}
	 					});
	 					
	 					form.hideWaitSymbol();
	 				}
	 			};
	             timer.schedule(3000); 
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getQuotationTable().getTable().setSelectionModel(selectionModelMyObj);
		 
	 }
	 
	public void setTableSelectionOnInvoice()
	 {
		 
		 final NoSelectionModel<Invoice> selectionModelMyObj = new NoSelectionModel<Invoice>();
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final Invoice entity=selectionModelMyObj.getLastSelectedObject();
	        	 
	        	 if(LoginPresenter.loggedInCustomer==false){
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Invoice Details",Screen.INVOICEDETAILS);
	        	 final InvoiceDetailsForm form = InvoiceDetailsPresenter.initalize();
	        	
	        	 AppMemory.getAppMemory().stickPnel(form);
	        	 form.showWaitSymbol();
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					
	 					MyQuerry querry=new MyQuerry();
	 					Vector<Filter> filtervec=new Vector<Filter>();
	 					Filter temp=null;
	 					
	 						 					
	 					temp=new Filter();
	 					temp.setQuerryString("count");
	 					temp.setIntValue(entity.getCount());
	 					filtervec.add(temp);
	 					
	 					querry.setFilters(filtervec);
	 					querry.setQuerryObject(new Invoice());
	 					
	 					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

	 						@Override
	 						public void onFailure(Throwable caught) {
	 							form.showDialogMessage("An Unexpected Error occured !");
	 							
	 						}
	 						@Override
	 						public void onSuccess(ArrayList<SuperModel> result) {
	 							
	 							
	 							for(SuperModel pmodel:result)
	 							{
	 								Invoice invoice=(Invoice)pmodel;
	 								form.updateView(invoice);
	 								 form.setToViewState();
	 							}
	 							
	 						}
	 					});
	 					
	 					form.hideWaitSymbol();
	 				}
	 			};
	             timer.schedule(3000); 
	        	 }else{
	        		 
	        		 if(entity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)||entity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE))
	        			{
	        				final String url = GWT.getModuleBaseURL() + "salesinvoiceprint"+"?Id="+entity.getId();
	        				Window.open(url, "test", "enabled");
	        			}
	        			
	        			if(entity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
	        				final String url = GWT.getModuleBaseURL() + "serviceinvoiceprint"+"?Id="+entity.getId()+"&"+"preprint="+"plane";
							Window.open(url, "test", "enabled");
	        			}
	        	 }
	             
	             
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getInvoiceTable().getTable().setSelectionModel(selectionModelMyObj);
		 
	 }
	 
	public void setTableSelectionOnPayment()
	 {
		 
		 final NoSelectionModel<CustomerPayment> selectionModelMyObj = new NoSelectionModel<CustomerPayment>();
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final CustomerPayment entity=selectionModelMyObj.getLastSelectedObject();
	        	 
	        	 if(LoginPresenter.loggedInCustomer==false){
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Payment Details",Screen.PAYMENTDETAILS);
	        	 final PaymentDetailsForm form = PaymentDetailsPresenter.initalize();
	        	
	        	 AppMemory.getAppMemory().stickPnel(form);
	        	 form.showWaitSymbol();
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					
	 					MyQuerry querry=new MyQuerry();
	 					Vector<Filter> filtervec=new Vector<Filter>();
	 					Filter temp=null;
	 					
	 						 					
	 					temp=new Filter();
	 					temp.setQuerryString("count");
	 					temp.setIntValue(entity.getCount());
	 					filtervec.add(temp);
	 					
	 					querry.setFilters(filtervec);
	 					querry.setQuerryObject(new CustomerPayment());
	 					
	 					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

	 						@Override
	 						public void onFailure(Throwable caught) {
	 							form.showDialogMessage("An Unexpected Error occured !");
	 							
	 						}
	 						@Override
	 						public void onSuccess(ArrayList<SuperModel> result) {
	 							
	 							
	 							for(SuperModel pmodel:result)
	 							{
	 								CustomerPayment custpay=(CustomerPayment)pmodel;
	 								form.updateView(custpay);
	 								form.setToViewState();
	 							}
	 							
	 						}
	 					});
	 					
	 					form.hideWaitSymbol();
	 				}
	 			};
	             timer.schedule(3000); 
	        	 }else{
	        		 final String url = GWT.getModuleBaseURL() + "customerpayslip"+"?Id="+entity.getId();
						Window.open(url, "test", "enabled");
	        	 }
	             
	             
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getPaymentTable().getTable().setSelectionModel(selectionModelMyObj);
		 
	 }

	private void setTableSelectionOnContractRenewal() {
			
		 final NoSelectionModel<Contract> selectionModelMyObj = new NoSelectionModel<Contract>();
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final Contract entity=selectionModelMyObj.getLastSelectedObject();
	        	 
	        	 if(LoginPresenter.loggedInCustomer==false){
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract",Screen.CONTRACT);
	        	 final ContractForm form = ContractPresenter.initalize();
	        	
	        	 AppMemory.getAppMemory().stickPnel(form);
	        	 form.showWaitSymbol();
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					
	 					MyQuerry querry=new MyQuerry();
	 					Vector<Filter> filtervec=new Vector<Filter>();
	 					Filter temp=null;
	 					
	 						 					
	 					temp=new Filter();
	 					temp.setQuerryString("count");
	 					temp.setIntValue(entity.getCount());
	 					filtervec.add(temp);
	 					
	 					querry.setFilters(filtervec);
	 					querry.setQuerryObject(new Contract());
	 					
	 					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

	 						@Override
	 						public void onFailure(Throwable caught) {
	 							form.showDialogMessage("An Unexpected Error occured !");
	 							
	 						}
	 						@Override
	 						public void onSuccess(ArrayList<SuperModel> result) {
	 							
	 							
	 							for(SuperModel pmodel:result)
	 							{
	 								Contract con=(Contract)pmodel;
	 								form.updateView(con);
	 								 form.setToViewState();
	 							}
	 							
	 						}
	 					});
	 					
	 					form.hideWaitSymbol();
	 				}
	 			};
	             timer.schedule(3000); 
	        	 }else{
	        		 
	        		 /****
	        		  * Contract Renewal can not be printed as these records are taken from contract
	        		  * and not processed for renewal process.
	        		  */
	        		 
//	        		 final String url = GWT.getModuleBaseURL() + "pdcontractrenewal" + "?ContractId="+ entity.getCount()+ "&"+"CompanyId="+ entity.getCompanyId();
//	        		 Window.open(url, "test", "enabled");
	        	 }
	             
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getContractrenewalTable().getTable().setSelectionModel(selectionModelMyObj);
		}
	 
	private void setTableSelectionOnCustomerSupport() {
			
		 final NoSelectionModel<Complain> selectionModelMyObj = new NoSelectionModel<Complain>();
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final Complain entity=selectionModelMyObj.getLastSelectedObject();
	        	 
	        	 if(LoginPresenter.loggedInCustomer==false){
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Support",Screen.COMPLAIN);
	        	 final ComplainForm form = ComplainPresenter.initalize();
	        	
	        	 AppMemory.getAppMemory().stickPnel(form);
	        	 form.showWaitSymbol();
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					
	 					MyQuerry querry=new MyQuerry();
	 					Vector<Filter> filtervec=new Vector<Filter>();
	 					Filter temp=null;
	 					
	 						 					
	 					temp=new Filter();
	 					temp.setQuerryString("count");
	 					temp.setIntValue(entity.getCount());
	 					filtervec.add(temp);
	 					
	 					querry.setFilters(filtervec);
	 					querry.setQuerryObject(new Complain());
	 					
	 					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

	 						@Override
	 						public void onFailure(Throwable caught) {
	 							form.showDialogMessage("An Unexpected Error occured !");
	 							
	 						}
	 						@Override
	 						public void onSuccess(ArrayList<SuperModel> result) {
	 							
	 							
	 							for(SuperModel pmodel:result)
	 							{
	 								Complain con=(Complain)pmodel;
	 								form.updateView(con);
	 								 form.setToViewState();
	 							}
	 							
	 						}
	 					});
	 					
	 					form.hideWaitSymbol();
	 				}
	 			};
	             timer.schedule(3000); 
	        	 }else{
	        		 if(entity.getContrtactId()!=null){
		        		 final String url = GWT.getModuleBaseURL() + "Complainpdf"+"?Id="+entity.getId();
		        		 Window.open(url, "test", "enabled");
		        		 }
	        	 }
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getCustomerSupportTableProxy().getTable().setSelectionModel(selectionModelMyObj);
		}
	 	 
	private void createProjectCorrespondingToService(final Service model) {
		form.showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {

				projectAsync.createCustomerProject(model.getCompanyId(),
						model.getContractCount(), model.getCount(), model.getStatus(),
						new AsyncCallback<Integer>() {

							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(Integer result) {
								if (result == 1) {
									final String url = GWT.getModuleBaseURL()
											+ "customerserpdf" + "?Id="
											+ model.getId();
									Window.open(url, "test", "enabled");
								}
								if (result == 0) {
									form.showDialogMessage("An unexpected error occurred. Please try again!");
								}
								if (result == -1) {
									form.showDialogMessage("Project deos not exist for this Service Can not print Job Card");
								}
							}
						});
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);

	}
		
	private void requestService(final Date date,final String desc) {
		
		Complain con=new Complain();
		con.setServiceDate(date);
		con.setDestription(desc);
		con.setDate(new Date());
		con.setCompStatus("Created");
		con.setPersoninfo(form.person.getValue());
		form.showWaitSymbol();
		async.save(con,new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("An Unexpected error occurred!");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				form.hideWaitSymbol();
				System.out.println("TICKET NO. ::: "+result.count);
				reactOnEmail1(date,desc, result.count);
				getCustomerComplainDetails();
				form.showDialogMessage("Service requested successfully!");
			}
		});
		
	}
	
	public void reactOnEmail1(Date date,String desc,int count) {
		
		System.out.println("RRRRRR count value "+count);
		System.out.println("RRRRRRRRRRRRR"+form.person.getValue());
		emailService.initiateRequestServiceEmail(UserConfiguration.getCompanyId(),date,desc,form.person.getValue(),count,new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Resource Quota Ended ");
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(Void result) {
				Window.alert("Email Sent Sucessfully !");
			}
		});
		
	}
	 
	private void saveGoogleDriveLink(final Date date,final String documnetType,final String googleLink,final String remark) {
//		MyQuerry querry = new MyQuerry();
//	  	Company c = new Company();
//	  	Vector<Filter> filtervec=new Vector<Filter>();
//	  	Filter filter = null;
//	  	filter = new Filter();
//	  	filter.setQuerryString("companyId");
//		filter.setLongValue(c.getCompanyId());
//		filtervec.add(filter);
//		filter = new Filter();
//		filter.setQuerryString("count");
//		filter.setIntValue(Integer.parseInt(form.person.getId().getValue().trim()));
//		filtervec.add(filter);
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new Customer());
//		form.showWaitSymbol();
//		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				form.hideWaitSymbol();
//			}
//
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				
//				System.out.println("RESULT SIZE :: "+result.size());
//					final Customer entity=(Customer) result.get(0);
			form.showWaitSymbol();
			final Customer entity=customer;
					
					if(entity.getGoogleDriveLinkList().size()!=0){
						
						System.out.println("GOOGLE DRIVE LINK LIST IS NOT EMPTY");
						
						GoogleDriveLink googleObj=new GoogleDriveLink();
						googleObj.setDate(date);
						googleObj.setDocumentType(documnetType);
						googleObj.setGoogleDriveLink(googleLink);
						googleObj.setRemark(remark);
						
						entity.getGoogleDriveLinkList().add(googleObj);
						
					}else{
						System.out.println("GOOGLE DRIVE LINK LIST IS  EMPTY");
						ArrayList<GoogleDriveLink> list=new ArrayList<GoogleDriveLink>();
						
						GoogleDriveLink googleObj=new GoogleDriveLink();
						googleObj.setDate(date);
						googleObj.setDocumentType(documnetType);
						googleObj.setGoogleDriveLink(googleLink);
						googleObj.setRemark(remark);
						list.add(googleObj);
						
						entity.setGoogleDriveLinkList(list);
						
					}
					
					
					async.save(entity,new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
							final GWTCAlert alert = new GWTCAlert(); 
						     alert.alert("An Unexpected error occurred!");
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
							form.hideWaitSymbol();
							form.googleDriveLinkTable.connectToLocal();
							form.googleDriveLinkTable.getDataprovider().setList(entity.getGoogleDriveLinkList());
							form.googleDriveLinkTable.addColumnSorting();
							final GWTCAlert alert = new GWTCAlert(); 
						     alert.alert("Saved successfully!");
						     customer=entity;
						}
					});
					
//			}
//			});
	}
}
