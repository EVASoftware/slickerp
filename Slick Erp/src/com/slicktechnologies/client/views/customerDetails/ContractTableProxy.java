package com.slicktechnologies.client.views.customerDetails;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;

public class ContractTableProxy extends SuperTable<Contract>{

	TextColumn<Contract> getContractCountColumn;
	TextColumn<Contract> getStartDate;
	TextColumn<Contract> getEndDate;
	TextColumn<Contract> getStatusColumn;
	TextColumn<Contract> getProductName;
	TextColumn<Contract> getrefNumber;
	
	TextColumn<Contract> getNetAmount;

	public Object getVarRef(String varName) {
		if (varName.equals("getContractCountColumn"))
			return this.getContractCountColumn;
		if (varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		return null;
	}
	public ContractTableProxy() {
		super();
	}
	
	
	@Override
	public void createTable() {
	
		addColumngetCount();
		addRefNumberColumn();
		addColumngetStartDate();
		addColumngetEndDate();
		if(LoginPresenter.loggedInCustomer==false){
			addColumngetStatus();
			}
		addColumngetProductName();
		addColumnNetAmount();
	}
	
	/**
	 * Date 11 April 2017
	 * added by vijay for Contract NetPayable Amount
	 * requirement from PCAAMB
	 */
	private void addColumnNetAmount() {

		getNetAmount = new TextColumn<Contract>() {
			
			@Override
			public String getValue(Contract object) {
				return object.getNetpayable()+"";
			}
		};
		table.addColumn(getNetAmount,"Net Payable");
		table.setColumnWidth(getNetAmount, 100,Unit.PX);
		getNetAmount.setSortable(true);
	}
	/**
	 * ends here
	 */
	
	private void addRefNumberColumn() {
		getrefNumber = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				if (object.getRefNo()!=null)
					return object.getRefNo();
				else
					return "N.A";
			}
			
			
		};
		table.addColumn(getrefNumber, "Ref No");
	
		table.setColumnWidth(getrefNumber, 100, Unit.PX);
		getrefNumber.setSortable(true);
	}
	
	
	private void addColumngetProductName() {
		getProductName = new TextColumn<Contract>() {
			
			@Override
			public String getValue(Contract object) {
//				String productname =null;
//				for(int i=0;i<object.getItems().size();i++){
//					
//					if(productname!=null){
//						productname = productname+","+" "+object.getItems().get(i).getProductName();
//					}else{
//						productname=object.getItems().get(i).getProductName();
//					}
//				}
//				System.out.println("productname============="+productname);
//				return productname;
				
				return getNameFromList(object, "Product Name");
			}
		};
		
		table.addColumn(getProductName, "Product Name");
		getProductName.setSortable(true);
		table.setColumnWidth(getProductName, 150, Unit.PX);
	
		
	}
	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				return object.getStatus();
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
		table.setColumnWidth(getStatusColumn, 80, Unit.PX);
	
	}
	
	protected void addColumngetCount() {
		getContractCountColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
			
			
		};
		table.addColumn(getContractCountColumn, "ID");
		table.setColumnWidth(getContractCountColumn, 100, Unit.PX);
		getContractCountColumn.setSortable(true);
	}
	
	
	
	protected void addColumngetStartDate() {
		
		getStartDate = new TextColumn<Contract>() {
			
			@Override
			public String getCellStyleNames(Context context, Contract object) {
				
				
				final Date sub7Days=CalendarUtil.copyDate(object.getStartDate());
				System.out.println("Todays Date ::: "+sub7Days);
				CalendarUtil.addDaysToDate(sub7Days,+7);
				System.out.println("con start Date + 7 Days ::: "+sub7Days);
				sub7Days.setHours(23);
				sub7Days.setMinutes(59);
				sub7Days.setSeconds(59);
				System.out.println("Converted Date ::: "+sub7Days);
				
				 if((new Date().after(sub7Days))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
					
					 return "red";
				 }
				 else if((new Date().after(object.getStartDate())&&(new Date().before(sub7Days)))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested")))
				 {
					 return "orange";
				 }
				 else if(object.getStatus().equals("Approved")){
					 return "green";
				 }
				 else
					 {
						 return "black";
					 }
			}
			
			@Override
			public String getValue(Contract object) {
				if (object.getStartDate() != null)
					return AppUtility.parseDate(object.getStartDate());
				
				
				else
					return " ";
	
			}
		};
		table.addColumn(getStartDate, "Contract Start Date");
		table.setColumnWidth(getStartDate, 140, Unit.PX);
		getStartDate.setSortable(true);
	}
	
	protected void addColumngetEndDate() {
		getEndDate = new TextColumn<Contract>() {
			
			@Override
			public String getValue(Contract object) {
				if (object.getEndDate() != null)
					return AppUtility.parseDate(object.getEndDate());
				else
					return " ";
	
			}
		};
		table.addColumn(getEndDate, "Contract End Date");
		table.setColumnWidth(getEndDate, 140, Unit.PX);
		getEndDate.setSortable(true);
	}
	
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Date : 26-07-2017 By ANIL
	 */
	
	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		addSortinggetCount();
		if(LoginPresenter.loggedInCustomer==false){
			addSortinggetStatus();
		}
		addSortinggetrefNumber();
		addSortinggetContractStartDate();
		addSortinggetContractEndDate();
		
		addSortinggOnNetValue();
		addSortingOnProductName();
	}
	
	protected void addSortinggetCount()
	{
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getContractCountColumn,new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	protected void addSortinggetStatus()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addSortinggetrefNumber() {
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getrefNumber, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getRefNo()!=null && e2.getRefNo()!=null){
						return e1.getRefNo().compareTo(e2.getRefNo());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetContractStartDate()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getStartDate, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStartDate()!=null && e2.getStartDate()!=null){
						return e1.getStartDate().compareTo(e2.getStartDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetContractEndDate()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getEndDate, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEndDate()!=null && e2.getEndDate()!=null){
						return e1.getEndDate().compareTo(e2.getEndDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggOnNetValue()
	{
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getNetAmount,new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getNetpayable() == e2.getNetpayable()) {
						return 0;
					}
					if (e1.getNetpayable() > e2.getNetpayable()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	protected void addSortingOnProductName()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getProductName, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getItems()!=null && e2.getItems()!=null){
						return getNameFromList(e1, "Product Name").compareTo(getNameFromList(e2, "Product Name"));}
				}
				else{
					return 0;
				}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	public String getNameFromList(Contract contract, String type){
		if(contract==null){
			return "";
		}
		String name="";
		switch(type){
			
			case "Product Name":
				for(SalesLineItem item:contract.getItems()){
					if(item.getProductName()!=null){
						name=name+item.getProductName()+",";
					}
				}
			break;
			
			default:
				break;
		}
		try{
			if(!name.equals(""))
				name=name.substring(0,name.length()-1);
		}catch(Exception e){
			
		}
		return name;
		
	}
	
	/**
	 * End
	 */
	
}
