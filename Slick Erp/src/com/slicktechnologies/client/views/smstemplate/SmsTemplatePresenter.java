package com.slicktechnologies.client.views.smstemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class SmsTemplatePresenter extends FormTableScreenPresenter<SmsTemplate> {
	
	SmsTemplateForm form;
	CsvServiceAsync csvservice = GWT.create(CsvService.class);

	
	public SmsTemplatePresenter(FormTableScreen<SmsTemplate> view, SmsTemplate model){
		super(view,model);
		form=(SmsTemplateForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getsmsConfigQuery());
		form.setPresenter(this);
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SMSTEMPLATE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	
	
	
	public static SmsTemplateForm initalize() {
		
		SmsTemplateTable gentableScreen=new SmsTemplateTable();
		
		SmsTemplateForm  form=new  SmsTemplateForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		
		
		SmsTemplatePresenter  presenter=new  SmsTemplatePresenter(form,new SmsTemplate());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
		
	}


	/**
	 * Method template to set the processBar events
	 */

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {

		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("New"))
		{
			form.setToNewState();
			initalize();
		}	
	}




	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}


	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getsmsConfigQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new SmsTemplate());
		return quer;
	}
	

	@Override
	protected void makeNewModel() {
		model=new SmsTemplate();

	}
	
	public void setModel(SmsTemplate entity)
	{
		model=entity;
	}




	@Override
	public void reactOnDownload() {
		
		
		checkSenderIdConfiguration();
		
		
		
	}




	private void checkSenderIdConfiguration() {
		GenricServiceAsync genasync=GWT.create(GenricService.class);

		Vector<Filter> filtrvec = new Vector<Filter>();
		Filter filtr = null;
		
		filtr = new Filter();
		filtr.setBooleanvalue(true);
		filtr.setQuerryString("status");
		filtrvec.add(filtr);
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtrvec);
		querry.setQuerryObject(new SmsConfiguration());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				if(result.size()==0){
					form.showDialogMessage("SMS configuration is not active please activate on Setting -> SMS Configuration");
					return;
				}
				else{
					for(SuperModel model : result){
						SmsConfiguration smsconfiguration = (SmsConfiguration) model;
						if(smsconfiguration.getAccountSID().equals("")){
							form.showDialogMessage("Sender id not foun in SMS configuration please add sender on Setting -> SMS Configuration");
							return;
						}
					}
				}
				
				reactonDownloadSMSTemplate();
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}




	protected void reactonDownloadSMSTemplate() {
		
		List<SmsTemplate> list=form.getSupertable().getListDataProvider().getList();
		System.out.println("list size "+list.size());
		
		ArrayList<SmsTemplate> smstemplatelist = new ArrayList<SmsTemplate>();
		for(SmsTemplate smstemplate : list){
			if(smstemplate.getStatus()){
				smstemplatelist.add(smstemplate);
			}
		}

		form.showWaitSymbol();
		csvservice.setSMSTemplatelist(smstemplatelist, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(Void result) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+213;
				Window.open(url, "test", "enabled");
			}
		});
		
	}



}
