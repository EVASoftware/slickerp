package com.slicktechnologies.client.views.smstemplate;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;

public class SmsTemplateTable extends SuperTable<SmsTemplate> {
	
	TextColumn<SmsTemplate> getCountColumn;
	TextColumn<SmsTemplate> getEventColumn;
	TextColumn<SmsTemplate> getMessageColumn;
	TextColumn<SmsTemplate> getDayColumn;
	TextColumn<SmsTemplate> getStatusColumn;
	TextColumn<SmsTemplate> getavailableTagsColumn;
	
	TextColumn<SmsTemplate> getAutoMsgColumn;

	public SmsTemplateTable()
	{
		super();
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetEvent();
		addColumngetMessage();
		addColumngetAvailableTags();
		addColumngetDay();
		addColumngetStatus();
		addColumnAutoMsg();
		
		
	}

	private void addColumnAutoMsg() {
		
		getAutoMsgColumn = new TextColumn<SmsTemplate>() {
			
			@Override
			public String getValue(SmsTemplate object) {
				if(object.getAutomaticMsg()==true)
					return "Yes";
				else 
					return "No";
						
			}
		};
		table.addColumn(getAutoMsgColumn, "Auto Message");
		getAutoMsgColumn.setSortable(true);
		table.setColumnWidth(getAutoMsgColumn, 100, Unit.PX);
	}

	private void addColumngetStatus() {
		getStatusColumn = new TextColumn<SmsTemplate>() {
			
			@Override
			public String getValue(SmsTemplate object) {
				if(object.getStatus()==true)
					return "Active";
				else return "In Active";
						
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
		table.setColumnWidth(getStatusColumn, 100, Unit.PX);
		
	}

	private void addColumngetDay() {
		getDayColumn = new TextColumn<SmsTemplate>() {
			
			@Override
			public String getValue(SmsTemplate object) {
				return object.getDay()+"";
			}
		};
		table.addColumn(getDayColumn,"Day");
		getDayColumn.setSortable(true);
		table.setColumnWidth(getDayColumn, 100, Unit.PX);
		
	}

	private void addColumngetMessage() {
		getMessageColumn = new TextColumn<SmsTemplate>() {
		
			@Override
			public String getValue(SmsTemplate object) {
				return object.getMessage()+"";
			}
		};
		table.addColumn(getMessageColumn,"Message");
		getMessageColumn.setSortable(true);
		table.setColumnWidth(getMessageColumn, 100, Unit.PX);
	}

	private void addColumngetEvent() {
		getEventColumn = new TextColumn<SmsTemplate>() {
			
			@Override
			public String getValue(SmsTemplate object) {
				return object.getEvent()+"";
			}
		};
		table.addColumn(getEventColumn,"Event");
		getEventColumn.setSortable(true);
		table.setColumnWidth(getEventColumn, 100, Unit.PX);
		
	}

	private void addColumngetAvailableTags() {
		getavailableTagsColumn = new TextColumn<SmsTemplate>() {
			
			@Override
			public String getValue(SmsTemplate object) {
				return object.getAvailableTags()+"";
			}
		};
		table.addColumn(getavailableTagsColumn,"Available Tags");
		getavailableTagsColumn.setSortable(true);
		table.setColumnWidth(getavailableTagsColumn, 100, Unit.PX);
	}
	
	private void addColumngetCount() {

		getCountColumn = new TextColumn<SmsTemplate>() {

			@Override
			public String getValue(SmsTemplate object) {
				if(object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
		};
		table.addColumn(getCountColumn, "ID");
		getCountColumn.setSortable(true);
		table.setColumnWidth(getCountColumn, 100, Unit.PX);
	}

	public void addColumnSorting(){
			addSortinggetCount();
			addSortinggetEvent();
			addSortingetMessage();
			addSortingAvailableTags();
			addSortinggetDay();
			addSortinggetStatus();	
	}
	
	
	private void addSortinggetStatus() {
			
		
		List<SmsTemplate> list = getDataprovider().getList();
		columnSort = new ListHandler<SmsTemplate>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<SmsTemplate>() {
			
			@Override
			public int compare(SmsTemplate e1, SmsTemplate e2) {

				if(e1!=null && e2!=null)
				{
					if(e1.getStatus()==e2.getStatus()){
						return 0;
					}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
				
			}
		});
		
		table.addColumnSortHandler(columnSort);
	}

	private void addSortinggetDay() {
		
		List<SmsTemplate> list=getDataprovider().getList();
		columnSort=new ListHandler<SmsTemplate>(list);
		columnSort.setComparator(getDayColumn, new Comparator<SmsTemplate>()
				{
			@Override
			public int compare(SmsTemplate e1,SmsTemplate e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getDay()== e2.getDay()){
						return 0;}
					if(e1.getDay()> e2.getDay()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
		
		
	}

	private void addSortingetMessage() {
		List<SmsTemplate> list = getDataprovider().getList();
		columnSort = new ListHandler<SmsTemplate>(list);
		columnSort.setComparator(getMessageColumn, new Comparator<SmsTemplate>() {

			@Override
			public int compare(SmsTemplate e1, SmsTemplate e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getMessage()!=null && e2.getMessage()!=null){
							return e1.getMessage().compareTo(e2.getMessage());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort); 
	}
	
	private void addSortingAvailableTags() {
		
		List<SmsTemplate> list = getDataprovider().getList();
		columnSort = new ListHandler<SmsTemplate>(list);
		columnSort.setComparator(getavailableTagsColumn, new Comparator<SmsTemplate>() {

			@Override
			public int compare(SmsTemplate e1, SmsTemplate e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getAvailableTags()!=null && e2.getAvailableTags()!=null){
							return e1.getAvailableTags().compareTo(e2.getAvailableTags());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		table.addColumnSortHandler(columnSort); 
		}


	private void addSortinggetEvent() {

		List<SmsTemplate> list = getDataprovider().getList();
		columnSort = new ListHandler<SmsTemplate>(list);
		columnSort.setComparator(getEventColumn, new Comparator<SmsTemplate>() {

			@Override
			public int compare(SmsTemplate e1, SmsTemplate e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getEvent()!=null && e2.getEvent()!=null){
							return e1.getEvent().compareTo(e2.getEvent());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort); 
		
	}

	private void addSortinggetCount() {

		List<SmsTemplate> list=getDataprovider().getList();
		columnSort=new ListHandler<SmsTemplate>(list);
		columnSort.setComparator(getCountColumn, new Comparator<SmsTemplate>()
				{
			@Override
			public int compare(SmsTemplate e1,SmsTemplate e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
