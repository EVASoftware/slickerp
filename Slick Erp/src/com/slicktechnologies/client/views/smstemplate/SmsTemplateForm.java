package com.slicktechnologies.client.views.smstemplate;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class SmsTemplateForm  extends FormTableScreen<SmsTemplate> implements ChangeHandler {
	
	
	TextBox tbEvent;
	CheckBox chkboxStatus;
	TextArea taMessage,taavailabeTags;
	IntegerBox tbDay;
	TextBox ibId;
	Label lblsmsTemplate,lblActualSms,lblspace;
	
	SmsTemplate smstemplateobj;
	TextArea taLandingPagetext;
	
	CheckBox chkAutomaticMsg;
	
	public static List<String> autoMessageist = new ArrayList<String>();

	public SmsTemplateForm  (SuperTable<SmsTemplate> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();

	}

	private void initalizeWidget(){
		
		tbEvent = new TextBox();
		tbEvent.addChangeHandler(this);
		tbDay = new IntegerBox();
		chkboxStatus = new CheckBox();
		chkboxStatus.setValue(true);
		taMessage = new TextArea();
		taMessage.setVisibleLines(5);
		ibId = new TextBox();
		ibId.setEnabled(false);
		lblsmsTemplate=new Label();
		taavailabeTags=new TextArea();
		lblsmsTemplate.setText("Dear {CustomerName} Service No {ServiceNo} Product Name {ProductName} Dated {ServiceDate} has been completed.");
		lblActualSms=new Label();
		lblActualSms.setText("Dear Sagar Chougule Service No 2 Product Name BedsBugs Dated 14-07-2015 has been completed.");
		lblspace = new Label(" ");
	
		taLandingPagetext = new TextArea();
		
		chkAutomaticMsg = new CheckBox();
		chkAutomaticMsg.setTitle("Enable  template to sent message automatically i.e. on contract approval, quotation  approval etc");
		chkAutomaticMsg.setEnabled(false);
		List<String> autoMsglist = new ArrayList<String>();
		autoMsglist.add("Payment Received");
		autoMsglist.add("Amount Payable");
		autoMsglist.add("AssignCustomer (Nayara)");
		autoMsglist.add("AssignTechnician (Nayara)");
		autoMsglist.add("Complaint Created");
		autoMsglist.add("Complaint Completed");
		autoMsglist.add("Complaint Service Completed");
		autoMsglist.add("Contract Approved");
		autoMsglist.add("Contract Cancellation");
		autoMsglist.add("Delivery Note Approved");
		autoMsglist.add("New Lead");
		autoMsglist.add("Lead Transfer");
		autoMsglist.add("Lead Transfer Notification");
		autoMsglist.add("Lead Cancelled");
		autoMsglist.add("Lead Unsuccessful");
		autoMsglist.add("Lead Successful");
		autoMsglist.add("Logout OTP");
		autoMsglist.add("User App Registartion OTP");
		autoMsglist.add("Service Completion");
		autoMsglist.add("Service Reschedule OTP");
		autoMsglist.add("Technician Reported");
		autoMsglist.add("Technician Started");
		autoMsglist.add("Technician Reported");
		autoMsglist.add("Service Completion OTP");
		autoMsglist.add("StartServiceCustomer");
		autoMsglist.add("CompleteServiceCustomer");
		autoMsglist.add("Purchase Order");
		autoMsglist.add("Quotation Approval");
		autoMsglist.add("Invoice Approval");
		autoMsglist.add("GRN Approval");
		autoMsglist.add("Customer Portal Link Communication");//Ashwini Patil Date:25-01-2023

		
		autoMessageist.addAll(autoMsglist);

	}
	
	
	@Override
	public void createScreen() {
		
		initalizeWidget();
		
		
		this.processlevelBarNames=new String[]{"New"};
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingMessageInformation=fbuilder.setlabel("Message Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("ID",ibId);
		FormField fibId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Event",tbEvent);
		FormField ftbEvent= fbuilder.setMandatory(true).setMandatoryMsg("Event Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Message Text",taMessage);//Ashwini Patil Date:28-06-2023 removed 160 character limit as whatsapp messages can be lengthy. old label was:  (Maximum 160 characters allowed to SMS)
		FormField ftaMessage = fbuilder.setMandatory(true).setMandatoryMsg("Message is Mandatory!").setRowSpan(0).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Day",tbDay);
		FormField ftbDay = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",chkboxStatus);
		FormField fchkbxStatus = fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Availabe Tags",taavailabeTags);
		FormField ftaavailabeTags = fbuilder.setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fexampleMessageInformation=fbuilder.setlabel("Message Example").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Message Template", lblsmsTemplate);
		FormField flblsmsTemplate = fbuilder.setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("Actual Message",lblActualSms);
		FormField flblActualSms = fbuilder.setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder(" ",lblspace);
		FormField flblspace = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Landing Page Text",taLandingPagetext);
		FormField ftaLandingPagetext = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("Auto Message",chkAutomaticMsg);
		FormField fchkAutomaticMsg = fbuilder.setRowSpan(0).setColSpan(0).build();
		
//		flblspace
		
		FormField[][] formfield = { {fgroupingMessageInformation},
				  {fibId,ftbEvent,ftbDay,fchkbxStatus,fchkAutomaticMsg},
				  {ftaMessage},{ftaavailabeTags},
				  {ftaLandingPagetext},
				  {fexampleMessageInformation},
				  {flblsmsTemplate,flblActualSms},
				  };

				this.fields=formfield;		
		
	}
	
	
	@Override
	public void updateModel(SmsTemplate model) 
	{
		if(tbEvent.getValue()!=null)
			model.setEvent(tbEvent.getValue());
		if(tbDay.getValue()!=null)
			model.setDay(tbDay.getValue());
		if(taMessage.getValue()!=null)
			model.setMessage(taMessage.getValue());
		if(taavailabeTags.getValue()!=null)
			model.setAvailableTags(taavailabeTags.getValue());
		
		if(taLandingPagetext.getValue()!=null)
			model.setLandingPageText(taLandingPagetext.getValue());
		
		model.setStatus(chkboxStatus.getValue());
		
		if(chkAutomaticMsg.getValue()!=null) {
			model.setAutomaticMsg(chkAutomaticMsg.getValue());
		}
		smstemplateobj=model;
			presenter.setModel(model);
	}
	
	
	@Override
	public void updateView(SmsTemplate view) 
	{
		smstemplateobj=view;
		if(view.getEvent()!=null)
			tbEvent.setValue(view.getEvent());
		if(view.getDay()!=0)
			tbDay.setValue(view.getDay());
		if(view.getMessage()!=null)
			taMessage.setValue(view.getMessage());
		if(view.getAvailableTags()!=null)
			taavailabeTags.setValue(view.getAvailableTags());
		if(view.getLandingPageText()!=null)
			taLandingPagetext.setValue(view.getLandingPageText());
		
		ibId.setValue(view.getCount()+"");
		chkboxStatus.setValue(view.getStatus());
		
		if(view.getAutomaticMsg()!=null) {
			chkAutomaticMsg.setValue(view.getAutomaticMsg());
		}
		presenter.setModel(view);

	}	
	
	


	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION)||text.equals("Download"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION)||text.equals("Download"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SMSTEMPLATE,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		ibId.setValue(count+"");	
	}

	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibId.setEnabled(false);
		chkAutomaticMsg.setEnabled(false);
	}

	@Override
	public boolean validate() {
		boolean superRes=super.validate();
//		boolean smsTextVal=true;
//		
//		if(taMessage.getValue().length()>160){
//			showDialogMessage("Sms Text exceeds 160 characters!");
//			smsTextVal=false;
//		}
		
	    return superRes;
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new SmsTemplate();
		model=smstemplateobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.SMSTEMPLATE, smstemplateobj.getCount(), null,null,null, false, model, null);
	
	}

	
	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(smstemplateobj!=null){
			SuperModel model=new SmsTemplate();
			model=smstemplateobj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.SMSTEMPLATE, smstemplateobj.getCount(), null,null,null, false, model, null);
		
		}
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		
		if(tbEvent.getValue()!=null && !tbEvent.equals("")) {
			System.out.println("tbEvent.getValue() "+tbEvent.getValue());
			if(getAutoMsg(autoMessageist,tbEvent.getValue())) {
				if(chkAutomaticMsg.getValue()==false)
					chkAutomaticMsg.setValue(true);
			}
		}
	}
	
	private boolean getAutoMsg(List<String> autoMsglist, String templateName) {
		for(String name : autoMsglist) {
			System.out.println("Name "+name);
			System.out.println(name.equals(templateName));
			if(name.equals(templateName)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource()==tbEvent) {
			if(tbEvent.getValue()!=null && !tbEvent.equals("")) {
				if(getAutoMsg(autoMessageist,tbEvent.getValue())) {
					chkAutomaticMsg.setEnabled(true);
				}
			}
		}
	}
	
}
