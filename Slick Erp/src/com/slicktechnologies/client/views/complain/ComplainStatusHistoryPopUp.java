package com.slicktechnologies.client.views.complain;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class ComplainStatusHistoryPopUp extends PopupScreen{

	DocumentStatusHistoryTable docHistoryTable;
	public ComplainStatusHistoryPopUp(){
		super();
		createGui();
	}

	private void initialWidgets() {
		docHistoryTable = new DocumentStatusHistoryTable();
	}
	
	@Override
	public void createScreen() {

		initialWidgets();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDocHistoryformation=fbuilder.setlabel("Document Status History Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",docHistoryTable.getTable());
		FormField fdocthistoryTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField[][] formfield = { 
				{fgroupingDocHistoryformation},
				{fdocthistoryTable},
			};	
		
		this.fields=formfield;
		
	}


	@Override
	public void onClick(ClickEvent event) {
	
		if(event.getSource()==this.getLblCancel()){
			hidePopUp();
		}
	}

	public DocumentStatusHistoryTable getDocHistoryTable() {
		return docHistoryTable;
	}

	public void setDocHistoryTable(DocumentStatusHistoryTable docHistoryTable) {
		this.docHistoryTable = docHistoryTable;
	}
	
	
	
}
