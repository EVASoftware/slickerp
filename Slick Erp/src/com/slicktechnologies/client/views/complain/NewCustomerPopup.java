package com.slicktechnologies.client.views.complain;

import java.util.ArrayList;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;

public class NewCustomerPopup extends AbsolutePanel implements ValueChangeHandler<String> {
	
	Button btnCancel,btnOk;
//	TextBox tbfName;
//	TextBox tbmName;
//	TextBox tblName;
	
	TextBox tbCompanyName;
	TextBox tbFullName;
	
	EmailTextBox etbEmail;
	TextBox pnbLandlineNo;
	InlineLabel label;
	/**
	 * Date 27-4-2018
	 * BY jayshree
	 * 
	 */
	InlineLabel labelbilling;
	InlineLabel labelservice;
	Button btnserviceaddr,btnbillingaddr;
	//End
	
	/**
	 * rohan added this code for adding branch while saving customer so that customer can be search while branch
	 * level restriction is on.
	 * Date : 27/2/2017
	 * 
	 */
	
	ObjectListBox<Branch> olbbBranch;
	
	/**
	 * ends here 
	 */
	
	
	/**
	 * Date : 05-02-2017 BY ANIL
	 * Adding address composite as per requirement raise form Rohan on 05-08-2017
	 */
	
	AddressComposite addressComp;
	/**
	 * Date 26-4-2018
	 * By Jayshree
	 * des.add for service address
	 */
	AddressComposite addressCompService;
	TextBox tbGstNumber;
	/**
	 * End
	 */
	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	public NewCustomerPopup() {
		
		
		label=new InlineLabel("Customer Details : ");
		add(label,10,10);
		
		InlineLabel lable1=new InlineLabel("Company Name");
		add(lable1,10,30);
		tbCompanyName=new TextBox();
		tbCompanyName.addValueChangeHandler(this);
		
		add(tbCompanyName,10,50);
		
		InlineLabel lable2=new InlineLabel("* Full Name");
		add(lable2,200,30);
		tbFullName=new TextBox();
		tbFullName.addValueChangeHandler(this);
		
		add(tbFullName,200,50);
		
		InlineLabel lable5=new InlineLabel("* Branch");
		add(lable5,400,30);
		olbbBranch = new ObjectListBox<Branch>();
		AppUtility.makeCustomerBranchListBoxLive(olbbBranch);
		add(olbbBranch,400,50);

		InlineLabel lable3=new InlineLabel("Email");
		add(lable3,10,90);
		etbEmail=new EmailTextBox();
		add(etbEmail,10,110);
		
		InlineLabel lable4=new InlineLabel("Cell No.");
		add(lable4,200,90);
		pnbLandlineNo=new TextBox();
		add(pnbLandlineNo,200,110);
		
		/**
		 * Date : 05-08-2017 By Anil
		 */
		InlineLabel lable6=new InlineLabel("GST Number");
		add(lable6,400,90);
		tbGstNumber=new TextBox();
		add(tbGstNumber,400,110);
		
		
		//InlineLabel labelbilling=new InlineLabel("Billing Address :");
		InlineLabel labelbilling=new InlineLabel("Address :");
		add(labelbilling ,10,145);
		
//		btnserviceaddr=new Button("Copy Service Address");
//		add(btnserviceaddr,150,145);
//		
		addressComp=new AddressComposite(true);//add by jayshree
		add(addressComp,10,175);
		
		
		/**
		 * Date 26-4-2018
		 * By Jayshree
		 * Des.to add the Service address 
		 */
		
		
//		//InlineLabel labelservice=new InlineLabel("Service Address :");
//		InlineLabel labelservice=new InlineLabel("Address :");
//		
//		add(labelservice ,10,360);
//		
////		btnbillingaddr=new Button("Copy Billing Address");
////		add(btnbillingaddr,150,360);
////		
//		addressCompService=new AddressComposite(true);//add by jayshree
//		add(addressCompService,10,395);
//		
		
		
		/**
		 * End
		 */
		
		
		btnOk=new Button("  Ok  ");
		add(btnOk, 200, 580);//150, 350 Change by jayshree
		btnCancel = new Button("Cancel");
		add(btnCancel,300,580);//250,350)// change by jayshree
		
		
		/**
		 * Date 26-4-2018
		 * By Jayshree
		 * Des.to add the Service address change the lenth 
		 */
		setSize("600px","620px");//600px
		this.getElement().setId("form");
		
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}
	
	

//	public TextBox getTbfName() {
//		return tbfName;
//	}
//
//	public void setTbfName(TextBox tbfName) {
//		this.tbfName = tbfName;
//	}
//
//	public TextBox getTbmName() {
//		return tbmName;
//	}
//
//	public void setTbmName(TextBox tbmName) {
//		this.tbmName = tbmName;
//	}
//
//	public TextBox getTblName() {
//		return tblName;
//	}
//
//	public void setTblName(TextBox tblName) {
//		this.tblName = tblName;
//	}

	public ObjectListBox<Branch> getOlbbBranch() {
		return olbbBranch;
	}

	public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
		this.olbbBranch = olbbBranch;
	}

	public EmailTextBox getEtbEmail() {
		return etbEmail;
	}

	public void setEtbEmail(EmailTextBox etbEmail) {
		this.etbEmail = etbEmail;
	}

	public TextBox getPnbLandlineNo() {
		return pnbLandlineNo;
	}

	public void setPnbLandlineNo(TextBox pnbLandlineNo) {
		this.pnbLandlineNo = pnbLandlineNo;
	}

	public InlineLabel getLabel() {
		return label;
	}

	public void setLabel(InlineLabel label) {
		this.label = label;
	}

	public void clear(){
//		tbfName.setValue("");
//		tbmName.setValue("");
//		tblName.setValue("");
		
		tbFullName.setValue("");
		tbCompanyName.setValue("");
		
		
		pnbLandlineNo.setValue("");
		etbEmail.setValue("");
		
		/**
		 * Date 26-5-2018 by jayshree
		 */
		
		olbbBranch.setSelectedIndex(0);
		
		addressComp.getAdressline1().setValue("");
		addressComp.getAdressline2().setValue("");
		addressComp.getLandMark().setValue("");
		addressComp.getLocality().setValue("");
		addressComp.getCity().setSelectedIndex(0);;
		addressComp.getState().setSelectedIndex(0);
		addressComp.getCountry().setSelectedIndex(0);
		addressComp.getPin().setValue(null);
		
		addressCompService.getAdressline1().setValue("");
		addressCompService.getAdressline2().setValue("");
		addressCompService.getLandMark().setValue("");
		addressCompService.getLocality().setValue("");
		addressCompService.getCity().setSelectedIndex(0);
		addressCompService.getState().setSelectedIndex(0);
		addressCompService.getCountry().setSelectedIndex(0);
		addressCompService.getPin().setValue(null);
		
		//End by jayshreee
		
	}
	
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		ScreeenState scr=AppMemory.getAppMemory().currentState;
		
		if(scr.equals(ScreeenState.NEW))
		{
			if(event.getSource().equals(tbFullName)){
				validateCust();
			}
			
			if(event.getSource().equals(tbCompanyName)){
				validateCompanyName();
			}
			
//			if(event.getSource().equals(tbmName)){
//				validateCust();
//			}
//			if(event.getSource().equals(tblName)){
//				validateCust();
//			}
		}
	}
	
	private void validateCust(){
		if(!getTbFullName().getValue().equals("")){
			String custnameval=getTbFullName().getValue().trim();
			custnameval=custnameval.toUpperCase().trim();
			if(custnameval!=null){

					final MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter temp=null;
					
					
					temp=new Filter();
					temp.setQuerryString("isCompany");
					if(!getTbCompanyName().getValue().equals("")){
						System.out.println(" value =="+getTbCompanyName().getValue());
						temp.setBooleanvalue(true);
					}
					if(getTbCompanyName().getValue().equals("")){
						temp.setBooleanvalue(false);
						System.out.println("valueeeee =="+getTbCompanyName().getValue());
					}
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("companyId");
					temp.setLongValue(UserConfiguration.getCompanyId());
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("fullname");
					temp.setStringValue(custnameval);
					filtervec.add(temp);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());
					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
//								form.showDialogMessage("An Unexpected Error occured !");
								final GWTCAlert alert = new GWTCAlert(); 
							     alert.alert("An Unexpected Error occured !");
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								System.out.println("Success"+result.size());
								if(result.size()!=0){
									final GWTCAlert alert = new GWTCAlert(); 
								     alert.alert("Customer Name Already Exists");
//									form.showDialogMessage("Customer Name Already Exists");
//									getTbfName().setValue("");
//									getTbmName().setValue("");
//									getTblName().setValue("");
								     
								     getTbFullName().setValue("");
								}
								if(result.size()==0){
//									custflag=true;
								}
							}
						});
			}
		}
	}

	
	private void validateCompanyName() {


		if(getTbCompanyName().getValue().trim()!=null){
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setQuerryString("isCompany");
			temp.setBooleanvalue(true);
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("companyName");
			temp.setStringValue(getTbCompanyName().getValue().toUpperCase().trim());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(UserConfiguration.getCompanyId());
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						 final GWTCAlert alert = new GWTCAlert(); 
					     alert.alert("An Unexpected Error occured !");
						
					}
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("Success"+result.size());
						if(result.size()!=0){
//							clientflag=false;
							
							 final GWTCAlert alert = new GWTCAlert(); 
						     alert.alert("Company Name Already Exists!");
						}
						if(result.size()==0){
//							clientflag=true;
						}
					}
					
				});
		}

	
	}
	
	public TextBox getTbCompanyName() {
		return tbCompanyName;
	}

	public void setTbCompanyName(TextBox tbCompanyName) {
		this.tbCompanyName = tbCompanyName;
	}

	public TextBox getTbFullName() {
		return tbFullName;
	}

	public void setTbFullName(TextBox tbFullName) {
		this.tbFullName = tbFullName;
	}

	public AddressComposite getAddressComp() {
		return addressComp;
	}

	public void setAddressComp(AddressComposite addressComp) {
		this.addressComp = addressComp;
	}

	/**
	 * Date 26-4-2018
	 * By jayshree
	 */
	
	public AddressComposite getAddressCompservice() {
		return addressCompService;
	}

	public void setAddressCompservice(AddressComposite addressCompservice) {
		this.addressCompService = addressCompservice;
	}
	
	public Button getBtnserviceaddr() {
		return btnserviceaddr;
	}
	
	public void setBtnserviceaddr(Button btnserviceaddr) {
		this.btnserviceaddr = btnserviceaddr;
	}
	
	public Button getBtnbillingaddr() {
		return btnbillingaddr;
	}
	
	public void setBtnbillingaddr(Button btnbillingaddr) {
		this.btnbillingaddr = btnbillingaddr;
	}
	
	//End By jayshree
	
	public TextBox getTbGstNumber() {
		return tbGstNumber;
	}

	public void setTbGstNumber(TextBox tbGstNumber) {
		this.tbGstNumber = tbGstNumber;
	}
	
	
	
//	public String fillFullName()
//	{
//		String fullname="";
//		if(getTbmName().getValue().trim().equals("")){
//			fullname=getTbfName().getValue().trim()+" "+getTblName().getValue().trim();
//		}
//		else{
//		   fullname=getTbfName().getValue().trim()+" "+getTbmName().getValue().trim()+" "+getTblName().getValue().trim();
//		}
//		System.out.println("Full Name Of Cust"+fullname);
//		return fullname;
//	}
	

	
	

}
