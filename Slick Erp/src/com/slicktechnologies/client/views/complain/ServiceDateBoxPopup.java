package com.slicktechnologies.client.views.complain;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;

public class ServiceDateBoxPopup extends AbsolutePanel {
	Button btnOne,btnTwo;
	DateBox serviceDate;
	TextArea taDesc;
	
	IntegerBox ibIntegerBox;
	String action;
	
	ObjectListBox<Branch> lbBranch;
	ObjectListBox<HrProject> project;
	
	/**
	 * @author Anil
	 * @since 20-01-2022
	 * Adding quantity column for service wise bill service completion
	 * Raised by Nitin Sir and Nithila for innovative
	 */
	DoubleBox dbQuantity;
	IntegerBox otp;//Ashwini Patil Date:4-07-2023 Hi-tech wants OTP restriction for contract and service download
	DateBox contractStartDate;//Ashwini Patil Date:10-07-2023 orion wants to update frequency from contract productline to it's services
	

	public ServiceDateBoxPopup(String dateLabel) {
		btnOne=new Button("Ok");
		btnTwo=new Button("Cancel");
		
		if(dateLabel!=null&&dateLabel.equalsIgnoreCase("OTP")){
			InlineLabel displayMessage = new InlineLabel("Enter OTP");
			displayMessage.setTitle("OTP has been sent to your company or branch mobile number");
			add(displayMessage,10,10);
			
			otp=new IntegerBox();
			add(otp,10,30);
			
			btnOne.getElement().getStyle().setWidth(50, Unit.PX);
			btnTwo.getElement().getStyle().setWidth(60, Unit.PX);
			
			add(btnOne,30,80);
			add(btnTwo,120,80); 
			setSize("220px", "150px");
			this.getElement().setId("form");	
			
		}else if(dateLabel!=null&&dateLabel.equalsIgnoreCase("Update Frequency")) {
		
			InlineLabel displayMessage = new InlineLabel("Select contract start date:");
			displayMessage.setTitle("Select contract start date from which you want to update frequency. Frequency will be mapped from each productline from contract to it's respective services");
			add(displayMessage,10,10);
			
			contractStartDate=new DateBoxWithYearSelector();
			DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy"); 
			contractStartDate.setFormat(new DateBox.DefaultFormat(dateFormat));
			add(contractStartDate,10,30);
			
			btnOne.getElement().getStyle().setWidth(50, Unit.PX);
			btnTwo.getElement().getStyle().setWidth(60, Unit.PX);
			
			add(btnOne,30,80);
			add(btnTwo,120,80); 
			setSize("220px", "150px");
			this.getElement().setId("form");	
		}else{
			InlineLabel displayMessage = new InlineLabel(dateLabel);
			add(displayMessage,10,10);
			
			serviceDate=new DateBoxWithYearSelector();
			add(serviceDate,10,30);
			
			DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy"); 
			serviceDate.setFormat(new DateBox.DefaultFormat(dateFormat));
			
			InlineLabel qty = new InlineLabel("Quantity");
			add(qty,10,60);
			
			dbQuantity=new DoubleBox();
			add(dbQuantity,10,80);
			
			btnOne.getElement().getStyle().setWidth(50, Unit.PX);
			btnTwo.getElement().getStyle().setWidth(60, Unit.PX);
			
			add(btnOne,30,105);
			add(btnTwo,120,105); 
			setSize("220px", "150px");
			this.getElement().setId("form");	
		}
	}
	
	public ServiceDateBoxPopup(String dateLabel,String desc) {
		btnOne=new Button("Ok");
		btnTwo=new Button("Cancel");
		
		
		if(desc!=null){
			InlineLabel displayMessage = new InlineLabel(dateLabel);
			add(displayMessage,10,10);
			
			serviceDate=new DateBoxWithYearSelector();
			add(serviceDate,10,30);
			
			DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy"); 
			serviceDate.setFormat(new DateBox.DefaultFormat(dateFormat));
			
			
			
			InlineLabel description = new InlineLabel(desc);
			add(description,10,70);
			
			taDesc=new TextArea();
			add(taDesc,10,90);
			
			taDesc.getElement().getStyle().setWidth(250, Unit.PX);
			
			btnOne.getElement().getStyle().setWidth(50, Unit.PX);
			btnTwo.getElement().getStyle().setWidth(60, Unit.PX);
			
			add(btnOne,30,150);
			add(btnTwo,120,150); 
			setSize("300px", "180px");
			this.getElement().setId("form");
			
		}else{
			
			if(dateLabel.equalsIgnoreCase("Text")){
//				InlineLabel displayMessage = new InlineLabel("Employee Id");
//				add(displayMessage,10,10);
//				
//				ibIntegerBox=new IntegerBox();
//				add(ibIntegerBox,10,30);
//				
//				InlineLabel payrollMonth = new InlineLabel("Payroll Month");
//				add(payrollMonth,190,10);
//				
//				serviceDate=new DateBoxWithYearSelector();
//				add(serviceDate,190,30);
//				
//				DateTimeFormat dateFormat=DateTimeFormat.getFormat("yyyy-MMM");
//				serviceDate.setFormat(new DateBox.DefaultFormat(dateFormat));
//				
//				btnOne.getElement().getStyle().setWidth(50, Unit.PX);
//				btnTwo.getElement().getStyle().setWidth(60, Unit.PX);
//				
//				add(btnOne,30,150);
//				add(btnTwo,120,150); 
//				setSize("380px", "180px");
//				this.getElement().setId("form");
//				
//				this.getElement().setId("form");
				
				
				

				
				InlineLabel displayMessage = new InlineLabel("Employee Id");
				add(displayMessage,10,10);
				
				ibIntegerBox=new IntegerBox();
				add(ibIntegerBox,10,30);
				
				InlineLabel payrollMonth = new InlineLabel("Payroll Month");
				add(payrollMonth,190,10);
				
				serviceDate=new DateBoxWithYearSelector();
				add(serviceDate,190,30);
				
				DateTimeFormat dateFormat=DateTimeFormat.getFormat("yyyy-MMM");
				serviceDate.setFormat(new DateBox.DefaultFormat(dateFormat));
				
				
				/**
				 * 
				 */
				
				InlineLabel lblBranch = new InlineLabel("Branch");
				add(lblBranch,10,65);
				
				lbBranch=new ObjectListBox<Branch>();
				AppUtility.makeBranchListBoxLive(lbBranch);
				add(lbBranch,10,90);
				
				InlineLabel lblProject = new InlineLabel("Project");
				add(lblProject,190,65);
				
				project = new ObjectListBox<HrProject>();
				HrProject.MakeProjectListBoxLive(project);
				add(project,190,90);
				
				btnOne.getElement().getStyle().setWidth(50, Unit.PX);
				btnTwo.getElement().getStyle().setWidth(60, Unit.PX);
				
				add(btnOne,30,150);
				add(btnTwo,120,150); 
				setSize("380px", "180px");
				this.getElement().setId("form");
				
			
				
				
			}else{
			
				InlineLabel displayMessage = new InlineLabel(dateLabel);
				add(displayMessage,10,10);
				
				serviceDate=new DateBoxWithYearSelector();
				add(serviceDate,10,30);
				
				DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy"); 
				serviceDate.setFormat(new DateBox.DefaultFormat(dateFormat));
				
				btnOne.getElement().getStyle().setWidth(50, Unit.PX);
				btnTwo.getElement().getStyle().setWidth(60, Unit.PX);
				
				add(btnOne,30,65);
				add(btnTwo,120,65); 
				setSize("220px", "110px");
				this.getElement().setId("form");
			}
		}
	}
	
	public void clear(){
		serviceDate.setValue(null);
		taDesc.setValue(null);
	}
	
	public void cleardate(){
		serviceDate.setValue(null);
			
	}
	
	public void clearOtp(){
		otp.setValue(null);			
	}
	/**************************************Getters And Setters******************************************/
	
	public Button getBtnOne() {
		return btnOne;
	}

	public void setBtnOne(Button btnOne) {
		this.btnOne = btnOne;
	}

	public Button getBtnTwo() {
		return btnTwo;
	}

	public void setBtnTwo(Button btnTwo) {
		this.btnTwo = btnTwo;
	}


	public DateBox getServiceDate() {
		return serviceDate;
	}


	public void setServiceDate(DateBox serviceDate) {
		this.serviceDate = serviceDate;
	}


	public TextArea getTaDesc() {
		return taDesc;
	}


	public void setTaDesc(TextArea taDesc) {
		this.taDesc = taDesc;
	}

	public IntegerBox getIbIntegerBox() {
		return ibIntegerBox;
	}

	public void setIbIntegerBox(IntegerBox ibIntegerBox) {
		this.ibIntegerBox = ibIntegerBox;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	
	public String getPayRollPeriod() {
		DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MMM");
		if (serviceDate.getValue() == null) {
			return null;
		} else {
			return format.format(serviceDate.getValue());
		}
	}
	public ObjectListBox<Branch> getLbBranch() {
		return lbBranch;
	}

	public void setLbBranch(ObjectListBox<Branch> lbBranch) {
		this.lbBranch = lbBranch;
	}

	public ObjectListBox<HrProject> getProject() {
		return project;
	}

	public void setProject(ObjectListBox<HrProject> project) {
		this.project = project;
	}

	public DoubleBox getDbQuantity() {
		return dbQuantity;
	}

	public void setDbQuantity(DoubleBox dbQuantity) {
		this.dbQuantity = dbQuantity;
	}

	public IntegerBox getOtp() {
		return otp;
	}

	public void setOtp(IntegerBox otp) {
		this.otp = otp;
	}

	public DateBox getContractStartDate() {
		return contractStartDate;
	}

	public void setContractStartDate(DateBox contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
}
