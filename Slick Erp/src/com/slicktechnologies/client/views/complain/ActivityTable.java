package com.slicktechnologies.client.views.complain;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.complain.ComplainList;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApprovalDetails;


// TODO: Auto-generated Javadoc
/**
 * The Class ActivityTable.
 */
public class ActivityTable extends SuperTable<ComplainList>  {

	
	TextColumn<ComplainList>getColumnSrNo;
	TextColumn<ComplainList>getColumnDate;
	TextColumn<ComplainList>getColumnTime;
	TextColumn<ComplainList>getColumnRemark;
	TextColumn<ComplainList>getColumnUser;
	Column<ComplainList,String> deleteColumn;


	/**
	 * Instantiates a new activity table.
	 */
	public ActivityTable()
	{
		super();
	}
	
	/**
	 * Instantiates a new activity table.
	 *
	 * @param view the view
	 */

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperTable#createTable()
	 */
	public void createTable()
	{
		
		createColumnID();
		createColumndueDateColumn();
		createColumnTimeColumn();
		createColumnUserColumn();
		createColumnRemarkColumn();
		createColumndeleteColumn();
		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);		 
	}

	
	private void createColumnUserColumn() {
		getColumnUser=new TextColumn<ComplainList>() {

			@Override
			public String getValue(ComplainList object) {
				if(object.getUserName()!=null){
					return object.getUserName();
				}else{
					return null;
				}
				
			}
			
		};
		
		table.addColumn(getColumnUser,"User");
		table.setColumnWidth(getColumnUser, 200, Unit.PX);
		getColumnUser.setSortable(true);
		
	}

	public void addEditColumn(){
		createColumnID();
		createColumndueDateColumn();
		createColumnTimeColumn();
		createColumnUserColumn();
		createColumnRemarkColumn();
		createColumndeleteColumn();
		addFieldUpdater();
	}

	public void addViewColumn() {
		createColumnID();
		createColumndueDateColumn();
		createColumnTimeColumn();
		createColumnUserColumn();
		createColumnRemarkColumn();
	}
	
	
	
	
	
	private void createColumndeleteColumn() {

		
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<ComplainList,String>(btnCell)
				{
			@Override
			public String getValue(ComplainList object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
		
		
	}

	/**
	 * Creates the columndescription.
	 */
	protected void createColumnID()
	{
		getColumnSrNo=new TextColumn<ComplainList>()
				{
			@Override public String getValue(ComplainList object){
				return object.getSrNo()+"";
			}
				};
				table.addColumn(getColumnSrNo,"Sr. No");
				table.setColumnWidth(getColumnSrNo, 100, Unit.PX);
				getColumnSrNo.setSortable(true);
	}
	
	/**
	 * Creates the columndue date column.
	 */
	protected void createColumndueDateColumn()
	{
		getColumnDate=new TextColumn<ComplainList>() {

		@Override
		public String getValue(ComplainList object) {
			if(object.getDate()!=null){
				
				return AppUtility.parseDate(object.getDate());
			}else{
				return null;
			}
			
		}
		
	};
	
	table.addColumn(getColumnDate,"Date");
	table.setColumnWidth(getColumnDate, 100, Unit.PX);
	getColumnDate.setSortable(true);	}
	
	/**
	 * Creates the columnaction taken column.
	 */
	protected void createColumnTimeColumn()
	{
		getColumnTime=new TextColumn<ComplainList>()
				{
			@Override public String getValue(ComplainList object){
				return object.getTime();
			}
				};
				table.addColumn(getColumnTime,"Time");
				table.setColumnWidth(getColumnTime, 100, Unit.PX);
				getColumnTime.setSortable(true);
	}
	
	/**
	 * Creates the columnstatus column.
	 */
	protected void createColumnRemarkColumn()
	{
		getColumnRemark=new TextColumn<ComplainList>()
				{
			@Override public String getValue(ComplainList object){
				return object.getRemark();
			}
				};
				table.addColumn(getColumnRemark,"Remark");
				table.setColumnWidth(getColumnRemark, 300, Unit.PX);
				getColumnRemark.setSortable(true);
	}




	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperTable#addFieldUpdater()
	 */
	@Override public void addFieldUpdater() 
	{

		createFieldUpdaterdeleteColumn();
	}



	private void createFieldUpdaterdeleteColumn() {


		deleteColumn.setFieldUpdater(new FieldUpdater<ComplainList,String>()
				{
			@Override
			public void update(int index,ComplainList object,String value)
			{
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
				});
	
		
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperTable#addColumnSorting()
	 */
	public void addColumnSorting(){

//		createSortingidColumn();
//		createSortingdescription();
//		createSortingdueDateColumn();
//		createSortingactionTakenColumn();
//		createSortingstatusColumn();

	}



	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperTable#setEnable(boolean)
	 */
	@Override
	public void setEnable(boolean state)
	{

		System.out.println("setEnable");
		
			System.out.println("state === "+state);
			int tablecolcount = this.table.getColumnCount();
			for (int i = tablecolcount - 1; i > -1; i--){
				table.removeColumn(i);
			}
			if (state == true)
				addEditColumn();
			if (state == false)
				addViewColumn();
			
	}
	
	

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.ViewContainer#applyStyle()
	 */
	@Override
	public void applyStyle()
	{

	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperTable#initializekeyprovider()
	 */
}
