package com.slicktechnologies.client.views.complain;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;

public class SetTimePopUp extends AbsolutePanel{
	
	

	
	private ListBox p_servicehours;
	private ListBox p_servicemin;
	private ListBox p_ampm;
	private Button btnCancel,btnOk;
	private TextBox tbtime;
	public SetTimePopUp() {
	
		
		InlineLabel laqoution=new InlineLabel("Set Service Time");
		laqoution.getElement().getStyle().setFontSize(15, Unit.PX);
		add(laqoution,70,10);
		
		tbtime=new TextBox();
		add(tbtime,220,7);
		tbtime.setEnabled(false);
		tbtime.setSize("60px", "16px");
		
		InlineLabel hhlable=new InlineLabel(" HH ");
		add(hhlable,40,50);
		
		p_servicehours=new ListBox();
		add(p_servicehours,35,75);
		p_servicehours.setSize("60px", "24px");
		p_servicehours.insertItem("--", 0);
		p_servicehours.insertItem("00", 1);
		p_servicehours.insertItem("01", 2);
		p_servicehours.insertItem("02", 3);
		p_servicehours.insertItem("03", 4);
		p_servicehours.insertItem("04", 5);
		p_servicehours.insertItem("05", 6);
		p_servicehours.insertItem("06", 7);
		p_servicehours.insertItem("07", 8);
		p_servicehours.insertItem("08", 9);
		p_servicehours.insertItem("09", 10);
		p_servicehours.insertItem("10", 11);
		p_servicehours.insertItem("11", 12);
		
		InlineLabel mmlable=new InlineLabel(" MM ");
		add(mmlable,115,50);
		
		p_servicemin=new ListBox();
		add(p_servicemin,110,75);
		p_servicemin.setSize("60px", "24px");
		p_servicemin.insertItem("--", 0);
		p_servicemin.insertItem("00", 1);
		p_servicemin.insertItem("15", 2);
		p_servicemin.insertItem("30", 3);
		p_servicemin.insertItem("45", 4);
		
		InlineLabel sslable=new InlineLabel(" AM/PM ");
		add(sslable,185,50);
		
		
		p_ampm = new ListBox();
		add(p_ampm,180,75);
		p_ampm.setSize("60px", "24px");
		p_ampm.insertItem("--", 0);
		p_ampm.insertItem("AM", 1);
		p_ampm.insertItem("PM", 2);

		
		btnCancel = new Button("Cancel");
		btnOk=new Button("Ok");
		
//		btnOk.setSize("50px", "50px");
//		btnCancel.setSize("50px", "50px");
		
		add(btnOk, 80, 130);
		add(btnCancel,150,130);
		
		setSize("300px","180px");
		this.getElement().setId("form");	
}
	
	


/********************************************Getters And Setters*********************************/

	public ListBox getP_servicehours() {
		return p_servicehours;
	}
	public void setP_servicehours(ListBox p_servicehours) {
		this.p_servicehours = p_servicehours;
	}
	public ListBox getP_servicemin() {
		return p_servicemin;
	}
	public void setP_servicemin(ListBox p_servicemin) {
		this.p_servicemin = p_servicemin;
	}
	public ListBox getP_ampm() {
		return p_ampm;
	}
	public void setP_ampm(ListBox p_ampm) {
		this.p_ampm = p_ampm;
	}
	public Button getBtnCancel() {
		return btnCancel;
	}
	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}
	public Button getBtnOk() {
		return btnOk;
	}
	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public TextBox getTbtime() {
		return tbtime;
	}

	public void setTbtime(TextBox tbtime) {
		this.tbtime = tbtime;
	}
	



}
