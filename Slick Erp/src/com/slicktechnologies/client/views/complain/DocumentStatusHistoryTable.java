package com.slicktechnologies.client.views.complain;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.complain.ComplainList;

public class DocumentStatusHistoryTable extends SuperTable<ComplainList>{

	TextColumn<ComplainList> getcolumnDate;
	TextColumn<ComplainList> getcolumnTime;
	TextColumn<ComplainList> getcolumnUserName;
	TextColumn<ComplainList> getcolumnStatus;
	DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");

	DocumentStatusHistoryTable(){
		super();
		table.setWidth("600Px");
	}
	
	@Override
	public void createTable() {
		addDateColumn();
		addTimeColumn();
		addUserNameColumn();
		addStatusColumn();
		
	}

	private void addDateColumn() {
		getcolumnDate = new TextColumn<ComplainList>() {
			@Override
			public String getValue(ComplainList object) {
				if(object.getDate()!=null)
					return fmt.format(object.getDate());
				else
					return "NA";
			}
		};
		table.addColumn(getcolumnDate,"Date");
		table.setColumnWidth(getcolumnDate, 100,Unit.PX);
	}

	private void addTimeColumn() {
		getcolumnTime = new TextColumn<ComplainList>() {
			@Override
			public String getValue(ComplainList object) {
				if(object.getTime()!=null)
					return object.getTime();
				else
					return "";
			}
		};
		table.addColumn(getcolumnTime,"Time");
		table.setColumnWidth(getcolumnTime, 100,Unit.PX);
	}

	private void addUserNameColumn() {
		getcolumnUserName = new TextColumn<ComplainList>() {
			
			@Override
			public String getValue(ComplainList object) {
				if(object.getUserName()!=null)
					return object.getUserName();
				else
					return "";
			}
		};
		table.addColumn(getcolumnUserName,"User Name");
		table.setColumnWidth(getcolumnUserName, 100,Unit.PX);
	}

	private void addStatusColumn() {
		getcolumnStatus = new TextColumn<ComplainList>() {
			
			@Override
			public String getValue(ComplainList object) {
				if(object.getStatus()!=null)
					return object.getStatus();
				else 
					return "";
			}
		};
		table.addColumn(getcolumnStatus,"Status");
		table.setColumnWidth(getcolumnStatus, 100, Unit.PX);
		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
