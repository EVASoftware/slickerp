package com.slicktechnologies.client.views.complain;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.jar.JarEntry;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.complain.ComplainList;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
//import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentForm;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentPresenter;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersonlist.ContactListPresenter;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.popups.ReportPopup;
import com.slicktechnologies.client.views.popups.SMSPopUp;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.client.views.service.ServicePresenter;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.DateTimeFormat;

public class ComplainPresenter extends FormScreenPresenter<Complain> implements
		SelectionHandler<Suggestion>, ChangeHandler {

	static ComplainForm form;
	final static GenricServiceAsync async = GWT.create(GenricService.class);
	ServiceDateBoxPopup datePopup = new ServiceDateBoxPopup("Service Date",null);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	NewCustomerPopup custpoppup=new NewCustomerPopup();
	PopupPanel panel = new PopupPanel(true);
	public Customer cust;
	SmsServiceAsync smsService = GWT.create(SmsService.class);
	
	/**
	 * Date 27 Feb 2017 added by vijay to send email
	 */
	EmailServiceAsync email = GWT.create(EmailService.class);
	/**
	 * End here
	 */
	
	//************vaishnavi***************
	boolean forJobReportcumReceiptFlag = false;
	ComplainFeedbackPopup complainmarkcompletepopup=new ComplainFeedbackPopup();
	PopupPanel markpanel=new PopupPanel(true);
	int realGuard=0;

	static ArrayList<Service> serviceList;
	/*** Date 26-10-2018 By Vijay for Plan Report ****/
	ReportPopup reportPopup = new ReportPopup();
	ComplainStatusHistoryPopUp complainHistory = new ComplainStatusHistoryPopUp();
	
	DocumentCancellationPopUp popup = new DocumentCancellationPopUp();
	NewEmailPopUp emailpopup = new NewEmailPopUp();

	SMSPopUp smspopup = new SMSPopUp();
	
	ConditionDialogBox conditionPopup=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);


	public ComplainPresenter(FormScreen<Complain> view, Complain model) {
		super(view, model);
		form = (ComplainForm) view;
		form.pic.getId().addSelectionHandler(this);
		form.pic.getName().addSelectionHandler(this);
		form.pic.getPhone().addSelectionHandler(this);
		form.contractCount.addChangeHandler(this);
		
		form.getNewCustomer().addClickHandler(this);
		custpoppup.getBtnCancel().addClickHandler(this);
		custpoppup.getBtnOk().addClickHandler(this);
		
		
		form.olbServiceDetails.addChangeHandler(this);
		datePopup.getBtnOne().addClickHandler(this);
		datePopup.getBtnTwo().addClickHandler(this);
		
		form.dbNetPayable.addChangeHandler(this);
		form.dbAmountReceived.addChangeHandler(this);
		complainmarkcompletepopup.getBtnOk().addClickHandler(this);
		complainmarkcompletepopup.getBtnCancel().addClickHandler(this);
		//komal
		form.olbProductDetails.addChangeHandler(this);
		
		form.oblCustomerBranch.addChangeHandler(this);
		
		form.lbAssetId.addChangeHandler(this);
		
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
				
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		
		form.setPresenter(this);
	}

	/**
	 * Method template to set the processBar events
	 */

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals("New")) {
			reactToNew();
		}
		if (text.equals("Create Quotation")) {
			
			if(form.tbQuatationId.getValue().equals("")){
				if(form.tbContractId.getValue().equals("")){
					if(form.contractCount.getSelectedIndex()==0){
						reactToQuatation();
					}else{
						form.showDialogMessage("You have selected a existing contract!");
					}
				}else{
					form.showDialogMessage("Contract is already created!");
				}
			}else{
				form.showDialogMessage("Quotation is already created!");
			}
		}
		if (text.equals("Create Contract")) {
			if(form.tbContractId.getValue().equals("")){
				if(form.tbQuatationId.getValue().equals("")){
					if(form.contractCount.getSelectedIndex()==0){
						reactToContract();
					}else{
						form.showDialogMessage("You have selected a existing contract!");
					}
				}else{
					form.showDialogMessage("Quotation is already created!");
				}
			}else{
				form.showDialogMessage("Contract is already created!");
			}
		}
		if (text.equals("Create Service")) {
			/**
			 * @author Vijay Date :- 02-12-2020
			 * Des :- added validation if service is created for complaint then it should not allow to create service for the same.
			 */
			if(!form.tbServiceId.getValue().equals("")){
				form.showDialogMessage("Service is already created! for this complain");
				return;
			}
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "MakeServiceDirectlyFromComplain")){
				Console.log("Complain-MakeServiceDirectlyFromComplain config active");
//				if(form.prodInfoComposite.getValue()==null || form.prodInfoComposite.getValue().equals("")){
				if(form.pinfo==null){//18-01-2024
				form.showDialogMessage("Product information is mandatory");
				}
				if(!form.dbNewServiceDate.getValue().equals("") && form.dbNewServiceDate.getValue()!=null){
					createContract();
				}else{
					form.showDialogMessage("New Service Date is Mandatory.");
				}
			}
			else {
			if(!form.tbContractId.getValue().equals("")||form.contractCount.getSelectedIndex()!=0){
				//26-12-2023 if product is missing then screen gets freezed at the time of service creation so validating it
//				if(form.prodInfoComposite.getValue()==null || form.prodInfoComposite.getValue().equals("")){
				if(form.pinfo==null){//18-01-2024
					form.showDialogMessage("Product information is mandatory");
					return;
				}
				
				//				if(form.tbServiceId.getValue().equals("")){
				Console.log("Complain-in else of config calling reactToService");
					reactToService();
//				}else{
//					form.showDialogMessage("Service is already created!");
//				}
			}else{
				form.showDialogMessage("Please create contract or select existing contract!");
			}
		  }
		}
		//***************vaishnavi*******************
		if (text.equals("Mark Completed")){
			
			if(ComplainForm.forJobReportcumReceiptFlag==true){
				
				System.out.println("Pop Up Showing");
				
				markpanel=new PopupPanel(true);
				markpanel.add(complainmarkcompletepopup);
				markpanel.show();
				markpanel.center();	
				
			}
			else{
				System.out.println("INSIDE MARK COMPLETED");
				if(form.contractCount.getSelectedIndex()!=0&&(form.tbContractId.getValue().equals("")&&form.tbQuatationId.getValue().equals(""))){
					System.out.println("INSIDE EXISTING CONTRACT");
					if(!form.tbServiceId.getValue().equals("")){
						System.out.println("INSIDE SERVICE IS NOT NULL");
						getServiceStatus(Integer.parseInt(form.tbServiceId.getValue()),Integer.parseInt(form.tbcount.getValue()),Service.SERVICESTATUSCOMPLETED);
					}else{
						form.showDialogMessage("Please create service first!");
					}
				}
				else if(form.contractCount.getSelectedIndex()==0&&!form.tbContractId.getValue().equals("")){
					getContractStatus(Integer.parseInt(form.tbContractId.getValue()),Integer.parseInt(form.tbcount.getValue()),Contract.APPROVED);
				}
				else{
					form.showDialogMessage("Please create contract or select existing contract!");
				}
				
			}
			
		}
		
		if (text.equals("View Contact List")) {
			reactOnContactList();
		}
		
		if (text.equals("DownLoad"))
			reactOnDownload();
		
		if (text.equals("Email"))
			reactOnEmail();
		
//		if (text.equals("Send SMS")) {
//			reactOnSendSMS();
//		}
		
		/** Date:- 17-10-2018 By Vijay For Plan Report**/
		if(text.equals("Report")){
			reactOnReport();
		}
		/** Date:- 23-10-2018 By Vijay For Document status **/
		if(text.equals("StatusHistory")){
			reactOnStatusHistory();
		}
		
		/**
		 * @author Anil
		 * @since 11-05-2020
		 */
		if(text.equals("View Service")){
			viewServices();
		}
		
		if(text.equals("Cancel")){
			
			popup.getPaymentDate().setValue(new Date()+"");
			popup.getPaymentID().setValue(form.tbTicketId.getValue().trim());
			popup.getPaymentStatus().setValue(form.olbSatatus.getValue().trim());
			panel=new PopupPanel(true);
			panel.add(popup);
			panel.show();
			panel.center();
		}
		
		if(text.equals("View Contract")){
			Console.log("form.tbContractId"+form.tbContractId.getValue());
			if(model.getContrtactId()!=null && model.getContrtactId()!=0){
				reactonViewContract("New");
			}
			else if(model.getExistingContractId()!=null&&model.getExistingContractId()!=0){
				reactonViewContract("Existing");
			}
			else{
				form.showDialogMessage("Contract is not created for this complaint");
			}
		}
		
	}

	
	
	private void reactOnStatusHistory() {
		complainHistory.showPopUp();
		complainHistory.getLblOk().setVisible(false);
		complainHistory.getDocHistoryTable().getDataprovider().setList(model.getStatusHistory());
	}

	private void reactOnReport() {
		reportPopup.showPopUp();
	}

	/**
	 * Date 16-03-2017
	 * added by vijay
	 * for Sending SMS to customer for Complaint Raised
	 * requirement from Petra pest
	 */

	
	private void reactOnSendSMS() {

		String CompanyName = Slick_Erp.businessUnitName;
		form.showWaitSymbol();
		
		String servicedetails = model.getServiceDetails();
		String [] servicedet = servicedetails.split(" ");
		System.out.println("Service id ==="+servicedet[1]);
		String serviceId = servicedet[1];
		
		smsService.checkSMSConfigAndSendComplaintRegisterSMS(model.getCompanyId(), serviceId, model.getPersoninfo().getFullName(), model.getCount(), CompanyName, model.getPersoninfo().getCellNumber(),model.getPersoninfo().getCount(), new AsyncCallback<ArrayList<Integer>>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<Integer> result) {
				
				if(result.contains(-1)){
					form.showDialogMessage("SMS Service is not enable in order to enable SMS Service please contact at support@evasoftwaresolutions.com ");
				}
				else if(result.contains(-2)){
					form.showDialogMessage("SMS Configuration is InActive");
				}
				else if(result.contains(-3)){
					form.showDialogMessage("Please define SMS Template");
				}
				else if(result.contains(-4)){
					form.showDialogMessage("SMS Template is InActive");
				}
				else if(result.contains(1)){
					form.showDialogMessage("SMS Sent Sucessfully");
				}
				else if(result.contains(-5)){
					form.showDialogMessage("Can not send SMS customer DND status is active");
				}
				form.hideWaitSymbol();
			}
		});
	}
	
	/**
	 * ends here
	 */
	

	private void reactOnContactList() {
		
		MyQuerry querry=new MyQuerry();
		Vector<Filter>filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(model.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setIntValue(model.getPersoninfo().getCount());
		temp.setQuerryString("personInfo.count");
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ContactPersonIdentification());
		
    	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
    	 
    		 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contact Person List",Screen.CONTACTPERSONLIST);
    	 
    	 ContactListPresenter.initialize(querry);
		
	}
	
	private void getContractStatus(int contractId,int ticketNumer,String status) {
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = new Filter();
		temp.setIntValue(contractId);
		temp.setQuerryString("count");
		filtervec.add(temp);
		
		System.out.println("CONTRACT COUNT :: "+contractId);
		
		temp = new Filter();
		temp.setIntValue(ticketNumer);
		temp.setQuerryString("ticketNumber");
		filtervec.add(temp);

		temp = new Filter();
		temp.setStringValue(status);
		temp.setQuerryString("status");
		filtervec.add(temp);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
		form.showWaitSymbol();
		
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if (result.size() == 0) {
					form.showDialogMessage("Please approve contract first!");
					return;
				}else{
					System.out.println("INSIDE COMPLAIN COMPLETED!!");
					model.setCompStatus("Completed");
					
					async.save(model,new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
							form.showDialogMessage("An Unexpected error occurred!");
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
							
							form.showDialogMessage("Complain Completed!");
							form.hideWaitSymbol();
							for(int i=0;i<form.olbSatatus.getItemCount();i++){
								if(form.olbSatatus.getItemText(i).equals(model.getCompStatus())){
									form.olbSatatus.setSelectedIndex(i);
									break;
								}
							}
							updateStatusToCompleted(model);//Ashwini Patil Date:09-08-2023
							/**
							 * Date 17-03-2017
							 * added by vijay
							 * for sending sms to customer for complaint Resolved (complated) requirement from Petra pest
							 */
							
							form.showWaitSymbol();
							String companyName = Slick_Erp.businessUnitName;
							smsService.checkSMSConfigAndSendComplaintResolvedSMS(model.getCompanyId(), model.getNewServiceDate(), model.getPersoninfo().getFullName(), model.getCount(), companyName, model.getPersoninfo().getCellNumber(),model.getPersoninfo().getCount(), new AsyncCallback<ArrayList<Integer>>() {

								@Override
								public void onFailure(Throwable caught) {
									form.hideWaitSymbol();
								}

								@Override
								public void onSuccess(ArrayList<Integer> result) {
									 if(result.contains(-1)){
											form.showDialogMessage("SMS Configuration is InActive");
										}
										else if(result.contains(-2)){
											form.showDialogMessage("Please define SMS Template");
										}
										else if(result.contains(-3)){
											form.showDialogMessage("SMS Template is InActive");
										}
										else if(result.contains(1)){
											form.showDialogMessage("SMS Sent Sucessfully");
										}
										else if(result.contains(-5)){
											form.showDialogMessage("Can not send SMS customer DND status is active");
										}
										form.hideWaitSymbol();
								}
							});
							

							smsService.checkEmailConfigAndSendComplaintResolvedEmail(model.getCompanyId(), model.getNewServiceDate(), model.getPersoninfo().getFullName(), model.getCount(), companyName, model.getPersoninfo().getCellNumber(),model.getPersoninfo().getCount(), new AsyncCallback<String>() {

								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									
								}

								@Override
								public void onSuccess(String result) {
									// TODO Auto-generated method stub
									Console.log("in email success");
									if(result!=null)
										Console.log("in email success result="+result);
									if(result!=null&&result.equals("")){										
										form.showDialogMessage("Email sent sucessfully");
									}
								}
							});
							
							
							/**
							 * ends here
							 */
							
							form.setMenuAsPerStatus();
						}
					});
					
					
				}
			}
			public void onFailure(Throwable throwable)
			{
				form.hideWaitSymbol();
			    form.showDialogMessage("An unexpected error occurred!");
			    throwable.printStackTrace();
			}
		});
	}

	private void getServiceStatus(int serviceId,int ticketNumber ,String status) {
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = new Filter();
		temp.setIntValue(serviceId);
		temp.setQuerryString("count");
		filtervec.add(temp);
		
		System.out.println("Service COUNT :: "+serviceId);
		System.out.println("Ticket Number :: "+ticketNumber);
		System.out.println("Status :: "+status);
		
		temp = new Filter();
		temp.setStringValue(status);
		temp.setQuerryString("status");
		filtervec.add(temp);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
		 form.showWaitSymbol();
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				System.out.println("SERVIC RESULT SIZE "+result.size());
				if (result.size() == 0) {
					form.showDialogMessage("Please complete service first!");
					return;
				}else{
					System.out.println("INSIDE COMPLAIN COMPLETED!!");
					
					model.setCompStatus("Completed");
					
					async.save(model,new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
							form.showDialogMessage("An Unexpected error occurred!");
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
							
							updateStatusToCompleted(model);//Ashwini Patil Date:09-08-2023
							form.showDialogMessage("Complain Completed!");
							form.hideWaitSymbol();
							for(int i=0;i<form.olbSatatus.getItemCount();i++){
								if(form.olbSatatus.getItemText(i).equals(model.getCompStatus())){
									form.olbSatatus.setSelectedIndex(i);
									break;
								}
							}
							
							/**
							 * Date 17-03-2017
							 * added by vijay
							 * for sending sms to customer for complaint Resolved (complated) requirement from Petra pest
							 */
							form.showWaitSymbol();
							String companyName = Slick_Erp.businessUnitName;
							smsService.checkSMSConfigAndSendComplaintResolvedSMS(model.getCompanyId(), model.getServiceDate(), model.getPersoninfo().getFullName(), model.getCount(), companyName, model.getPersoninfo().getCellNumber(),model.getPersoninfo().getCount(), new AsyncCallback<ArrayList<Integer>>() {

								@Override
								public void onFailure(Throwable caught) {
									form.hideWaitSymbol();
								}

								@Override
								public void onSuccess(ArrayList<Integer> result) {
									
								    if(result.contains(-1)){
										form.showDialogMessage("SMS Configuration is InActive");
									}
									else if(result.contains(-2)){
										form.showDialogMessage("Please define SMS Template");
									}
									else if(result.contains(-3)){
										form.showDialogMessage("SMS Template is InActive");
									}
									else if(result.contains(-5)){
										form.showDialogMessage("Can not send SMS customer DND status is active");
									}
									else if(result.contains(1)){
										form.showDialogMessage("SMS Sent Sucessfully");
									}
									
									
									form.hideWaitSymbol();
								}
							});
							
							smsService.checkEmailConfigAndSendComplaintResolvedEmail(model.getCompanyId(), model.getNewServiceDate(), model.getPersoninfo().getFullName(), model.getCount(), companyName, model.getPersoninfo().getCellNumber(),model.getPersoninfo().getCount(), new AsyncCallback<String>() {

								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									
								}

								@Override
								public void onSuccess(String result) {
									// TODO Auto-generated method stub
									Console.log("in email success");
									if(result!=null)
										Console.log("in email success result="+result);
									if(result!=null&&result.equals("")){										
										form.showDialogMessage("Email sent sucessfully");
									}
								}
							});
							/**
							 * ends here
							 */
							
							form.setMenuAsPerStatus();
							
							
							
						}
					});
					
					
				}
			}
			public void onFailure(Throwable throwable)
			{
				form.hideWaitSymbol();
				System.out.println("ON Failurr!!!");
			    form.showDialogMessage("An unexpected error occurred!");
			    throwable.printStackTrace();
			}
		});
	}
	
	
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		
		if(event.getSource()==complainmarkcompletepopup.getBtnOk()){
			
			if(complainmarkcompletepopup.lbRating.getSelectedIndex()!=0){
				form.tbCustomerFeedback.setValue(complainmarkcompletepopup.lbRating.getValue(complainmarkcompletepopup.lbRating.getSelectedIndex()));
				reactOnComplete();
			}else{
//				form.tbCustomerFeedback.setValue(null);
				form.showDialogMessage("Please select Feedback!");
			}
			markpanel.hide();
			
		}
		if(event.getSource()==complainmarkcompletepopup.getBtnCancel()){
			markpanel.hide();
		}
		
		
		
		
		if(event.getSource().equals(form.getNewCustomer())){
			System.out.println("INSIDE POPUP CUSTOMER!!!");
			panel=new PopupPanel(true);
			panel.add(custpoppup);
			panel.center();
			custpoppup.clear();
			panel.show();
		}
		
		if(event.getSource()==custpoppup.getBtnOk()){
			
			if(validateCustomer()){
				System.out.println("YOU ARE ON TRACK.....!!!!");
				saveNewCustomer();
			}
			
		}
		if(event.getSource()==custpoppup.getBtnCancel()){
			panel.hide();
		}
		
		if(event.getSource()==popup.getBtnOk()){
			if(popup.getRemark().getValue().trim()!= null && !popup.getRemark().getValue().trim().equals("")){
				panel.hide();
				cancelComplain();
			}else{
				form.showDialogMessage("Please enter valid remark to cancel Billing Document..");
			}

		}
		else if(event.getSource()==popup.getBtnCancel()){
			panel.hide();
			popup.remark.setValue("");
		}
		
		if(event.getSource().equals(conditionPopup.getBtnOne()))
		{
			final String url = GWT.getModuleBaseURL() + "complainformat"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"yes";
			Window.open(url, "test", "enabled");
			panel.hide();
		}
		if(event.getSource().equals(conditionPopup.getBtnTwo()))
		{
			final String url = GWT.getModuleBaseURL() + "complainformat"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"no";
			Window.open(url, "test", "enabled");
			panel.hide();
		}
	}
	
	private void reactOnComplete(){
		
		model.setCustomerFeedback(complainmarkcompletepopup.lbRating.getValue(complainmarkcompletepopup.lbRating.getSelectedIndex()));
		model.setCompStatus("Completed");
		
		form.showWaitSymbol();
		
		service.save(model, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("Unexpected Error Occured!");	
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				form.tbCustomerFeedback.setValue(model.getCustomerFeedback());
				form.olbSatatus.setValue(model.getCompStatus());
				form.setToViewState();
				updateStatusToCompleted(model);//Ashwini Patil Date:09-08-2023
				createPayment(model.getNetPayable(), model.getAmountReceived(), model.getBalance());
				
			}
		});
		
	}
	
	
	private void createPayment(Double netPayable,Double amountReceived, Double amountBalance) {
		
		ArrayList<CustomerPayment> custPaymentList = new ArrayList<CustomerPayment>();
		ArrayList<SuperModel> modelList = new ArrayList<SuperModel>();
		custPaymentList=getCustomerPaymentList(netPayable, amountReceived, amountBalance);
		
		for(CustomerPayment payment:custPaymentList){
			SuperModel model = payment;
			modelList.add(model);
		}
		service.save(modelList, new AsyncCallback<ArrayList<ReturnFromServer>>() {

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("Unexpected Error Occured!");
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<ReturnFromServer> result) {
				form.showDialogMessage("Feedback saved succesfully");
				form.hideWaitSymbol();
			}
		});
    
	}
	
	
	private ArrayList<CustomerPayment> getCustomerPaymentList(Double netPayable,
			Double amountReceived, Double amountBalance) {
		
		ArrayList<CustomerPayment> custPaymentList = new ArrayList<CustomerPayment>();
		
		CustomerPayment custPay;
		
		if(amountReceived !=0 && amountBalance !=0){
			custPay=new CustomerPayment();
			custPay.setTicketId(model.getCount());
			custPay.setPersonInfo(model.getPersoninfo());
			custPay.setPaymentDate(new Date());
			custPay.setNetPay(netPayable.intValue());
			custPay.setPaymentReceived(amountReceived.intValue());
			custPay.setStatus(CustomerPayment.PAYMENTCLOSED);
			
			custPaymentList.add(custPay);
			
			custPay=new CustomerPayment();
			custPay.setTicketId(model.getCount());
			custPay.setPersonInfo(model.getPersoninfo());
			custPay.setPaymentDate(new Date());
			custPay.setNetPay(netPayable.intValue());
			custPay.setPaymentReceived(amountBalance.intValue());
			custPay.setStatus(CustomerPayment.CREATED);
			
			custPaymentList.add(custPay);
			
		}else if(amountReceived !=0 && amountBalance==0){
			
			custPay=new CustomerPayment();
			custPay.setTicketId(model.getCount());
			custPay.setPersonInfo(model.getPersoninfo());
			custPay.setPaymentDate(new Date());
			custPay.setNetPay(netPayable.intValue());
			custPay.setPaymentReceived(amountReceived.intValue());
			custPay.setStatus(CustomerPayment.PAYMENTCLOSED);
			
			custPaymentList.add(custPay);
			
		}else if(amountReceived==0 && amountBalance!=0){
			
			custPay=new CustomerPayment();
			custPay.setTicketId(model.getCount());
			custPay.setPersonInfo(model.getPersoninfo());
			custPay.setPaymentDate(new Date());
			custPay.setNetPay(netPayable.intValue());
			custPay.setPaymentReceived(amountBalance.intValue());
			custPay.setStatus(CustomerPayment.CREATED);
			
			custPaymentList.add(custPay);
			
		}
		
		return custPaymentList;
	}
	
	

	private void saveNewCustomer() {
		final Customer cust=new Customer();
		
		if(!custpoppup.getTbFullName().getValue().equals("")){
			cust.setFullname(custpoppup.getTbFullName().getValue().toUpperCase());
		}
		
		if(!custpoppup.getTbCompanyName().getValue().equals(""))
		{
			cust.setCompanyName(custpoppup.getTbCompanyName().getValue().toUpperCase());
			cust.setCompany(true);
		}
		
//		if(!custpoppup.getTbmName().getValue().equals("")){
//			cust.setMiddleName(custpoppup.getTbmName().getValue().toUpperCase());		
//		}
//		if(!custpoppup.getTblName().getValue().equals("")){
//			cust.setLastName(custpoppup.getTblName().getValue().toUpperCase());
//		}
		if(!custpoppup.getPnbLandlineNo().getValue().equals("")){
			cust.setCellNumber1(Long.parseLong(custpoppup.getPnbLandlineNo().getValue()));
		}
		if(!custpoppup.getEtbEmail().getValue().equals("")){
			cust.setEmail(custpoppup.getEtbEmail().getValue());
		}
		
	//  rohan added this code for setting branch in customer 
		
		if(custpoppup.getOlbbBranch().getSelectedIndex()!=0){
			cust.setBranch(custpoppup.getOlbbBranch().getValue(custpoppup.getOlbbBranch().getSelectedIndex()).trim());
		}
		
		/**
		 * Date 18-014-2018 By vijay
		 * added below code because in popup address and GST number fields showing but code was not here so Updated code
		 */
		/**
		 * Date : 08-08-2017 by ANIL
		 */
		boolean addressFlag=false;
		if(custpoppup.getAddressComp().getValue()!=null){
			cust.setAdress(custpoppup.getAddressComp().getValue());
			cust.setSecondaryAdress(custpoppup.getAddressComp().getValue());
			addressFlag=true;
		}
		
		if(custpoppup.getTbGstNumber().getValue()!=null&&!custpoppup.getTbGstNumber().getValue().equals("")){
			ArticleType articalType=new ArticleType();
			articalType.setDocumentName("Invoice");
			articalType.setArticleTypeName("GSTIN");
			articalType.setArticleTypeValue(custpoppup.getTbGstNumber().getValue());
			articalType.setArticlePrint("Yes");
			cust.getArticleTypeDetails().add(articalType);
		}
		
		if(addressFlag){
			cust.setNewCustomerFlag(false);
		}else{
			cust.setNewCustomerFlag(true);
		}
		/**
		 * End
		 */
		
		form.showWaitSymbol();
		async.save(cust,new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("An unexpected error occurred");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				System.out.println("BEFORRERERRERERERRERE");
				panel.hide(); 
				
				/** 
				 * Date 18-04-2018 by vijay
				 * Des :- below code is old code not required this code
				 */
//				MyQuerry querry=new MyQuerry();
//				querry.setQuerryObject(new Customer());
//				form.pic = new PersonInfoComposite(querry, false);
//			    System.out.println("AFTERRRRERERRERRERERER");
//			    
//				Timer t = new Timer() {
//				      @Override
//				      public void run() {
//						System.out.println("INSIDE SUCCESS !!!");
//						Complain complain=getFilledFormDetails();
//						setNewCustomerDetails(complain);
////						form.hideWaitSymbol();
//				      }
//				};
//			    t.schedule(2000);
				
				/** Date 18-04-2018 by vijay
				 * Des :- I have updated code for new customer save
				 */
				cust.setCount(result.count);
				setCustomerDetails(cust);
				form.hideWaitSymbol();
				/**
				 * ends here
				 */
				
			}
		});
	}

	/** Date 18-04-2018 by vijay
	 * Des :- I have updated code for new customer save
	 */
	public void setCustomerDetails(Customer entity){
		try {
			PersonInfo info=new PersonInfo();
			
			if(entity.isCompany())
			{
				System.out.println("hi vijay=="+entity.getCompanyName());
				info.setCount(entity.getCount());
				info.setFullName(entity.getCompanyName());
				info.setCellNumber(entity.getCellNumber1());
				info.setPocName(entity.getFullname());
			}
			else
			{
				System.out.println("hi vijay $$$ "+entity.getFullname());
				info.setCount(entity.getCount());
				info.setFullName(entity.getFullname());
				info.setFullName(entity.getFullname());
				info.setCellNumber(entity.getCellNumber1());
				info.setPocName(entity.getFullname());
			}
			
		    ComplainPresenterSearchProxy search = (ComplainPresenterSearchProxy) form.getSearchpopupscreen();
		    search.personInfo.setPersonInfoHashMap(info);
			
			/**
			 *  customer object set in personInfoComposite and 
			 *   and new entered value displayed in form using  
			 *   setNewCustomerDetails Method in LeadForm. java class
			 */
			
			PersonInfoComposite.globalCustomerArray.add(info);
			setNewCustomerDetails(info);
			form.showDialogMessage("Customer Saved Successfully!");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	public void setNewCustomerDetails(PersonInfo pInfo){
		try {
			
			System.out.println("pInfo.getFullName() =="+pInfo.getFullName());
			form.pic.getId().setValue(pInfo.getCount()+"");
			form.pic.getName().setValue(pInfo.getFullName());
			form.pic.getTbpocName().setValue(pInfo.getPocName());
			form.pic.getPhone().setValue(pInfo.getCellNumber()+"");
			form.pic.setPersonInfoHashMap(pInfo);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * ends here
	 */
	
	
	/** 
	 * Date 18-04-2018 by vijay
	 * Des :- below code is old code not required this code for new customer save
	 */
//	private void setNewCustomerDetails(final Complain complain){
////		form.showWaitSymbol();
//		if(!custpoppup.getTbFullName().getValue().equals("")){
//			String custnameval=custpoppup.getTbFullName().getValue().trim();
//			custnameval=custnameval.toUpperCase().trim();
//			if(custnameval!=null){
//					final MyQuerry querry=new MyQuerry();
//					Vector<Filter> filtervec=new Vector<Filter>();
//					Filter temp=null;
//					
//					temp=new Filter();
//					temp.setQuerryString("companyId");
//					temp.setLongValue(UserConfiguration.getCompanyId());
//					filtervec.add(temp);
//					
//					temp=new Filter();
//					temp.setQuerryString("fullname");
//					temp.setStringValue(custnameval);
//					filtervec.add(temp);
//					
//					querry.setFilters(filtervec);
//					querry.setQuerryObject(new Customer());
//					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
//						@Override
//						public void onFailure(Throwable caught) {
//							final GWTCAlert alert = new GWTCAlert(); 
//							alert.alert("An Unexpected Error occured !");
//							form.hideWaitSymbol();
//						}
//						@Override
//						public void onSuccess(final ArrayList<SuperModel> result) {
//							System.out.println("Success"+result.size());
//							AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Support",Screen.COMPLAIN);
//							final ComplainForm form = ComplainPresenter.initalize();
//							Timer t = new Timer() {
//								@Override
//							    public void run() {
//									if(result.size()!=0){
//										for(SuperModel model:result){
//											
//											Customer entity=(Customer) model;
//											PersonInfo info=new PersonInfo();
//											info.setCount(entity.getCount());
//											info.setFullName(entity.getFullname());
//											info.setCellNumber(entity.getCellNumber1());
//											info.setPocName(entity.getFullname());
//											
//											System.out.println("NEW CUSTOMER NAME ::: "+entity.getFullname());
//											complain.setPersoninfo(info);
//											
//											form.updateView(complain);
////											form.hideWaitSymbol();
//											form.showDialogMessage("Customer Saved Successfully!");
//											hideScreen();
//										}
//									}
//							    }
//							};
//						    t.schedule(3000);
//						}
//					});
//			}
//		}
//	}
//	
//	
//	public void hideScreen(){
//		form.hideWaitSymbol();
//	}
//	private Complain getFilledFormDetails() {
//		Complain complain=new Complain();
//		
//		Date date=new Date();
//		
//		DateTimeFormat dtf = DateTimeFormat.getFormat("HH:mm:ss");
//		DateTimeFormat dtf1 = DateTimeFormat.getFormat("dd/MM/yy");
//		
//		
//	    String time=new String(dtf.format(date).toString());
//	    String date1=new String(dtf1.format(date).toString());
//		
//		if(form.pic.getValue()!=null)
//			complain.setPersoninfo(form.pic.getValue());
//		
//		if(form.olbeSalesPerson.getValue()!=null)
//			complain.setSalesPerson(form.olbeSalesPerson.getValue());
//		if(form.olbbBranch.getValue()!=null){
//			complain.setBranch(form.olbbBranch.getValue());
//		}
//
//		if(form.dbDate.getValue()!=null)
//			complain.setDate(form.dbDate.getValue());
//		
//		if(form.tbtime==null)
//			complain.setTime(time);
//		
//		if(form.olbcPriority.getValue()!=null)
//			complain.setPriority(form.olbcPriority.getValue());
//		if(form.prodInfoComposite.getValue()!=null)
//			complain.setPic(form.prodInfoComposite.getValue());
//		if(form.olbassignto.getValue()!=null)
//			complain.setAssignto(form.olbassignto.getValue());
//		if(form.tbBrandName.getValue()!=null)
//			complain.setBrandName(form.tbBrandName.getValue());
//		if(form.tbModel.getValue()!=null)
//			complain.setModelNumber(form.tbModel.getValue());
//		if(form.taDescription.getValue()!=null)
//			complain.setDestription(form.taDescription.getValue());
//		if(form.dbDueDate.getValue()!=null)
//			complain.setDueDate(form.dbDueDate.getValue());
//		if(form.taRemark.getValue()!=null)
//			complain.setRemark(form.taRemark.getValue());
//		if(form.tbservicetime.getValue()!=null)
//			complain.setServicetime(form.tbservicetime.getValue());
//		if(form.serviceDate.getValue()!=null)
//			complain.setServiceDate(form.serviceDate.getValue());
//		if(form.olbSatatus.getValue()!=null)
//			complain.setCompStatus(form.olbSatatus.getValue());
//		if(form.contractCount.getSelectedIndex()!=0){
//			complain.setExistingContractId(Integer.parseInt(form.contractCount.getValue(form.contractCount.getSelectedIndex())));
//		}
//		if(form.olbServiceDetails.getSelectedIndex()!=0){
//			complain.setExistingServiceId(Integer.parseInt(form.olbServiceDetails.getValue(form.olbServiceDetails.getSelectedIndex())));
//		}
//		if(form.tbServiceEngineer.getValue()!=null){
//			complain.setServiceEngForExistingService(form.tbServiceEngineer.getValue());
//		}
//		if(!form.tbServiceId.getValue().equals("")){
//			complain.setServiceId(Integer.parseInt(form.tbServiceId.getValue()));
//		}
//		
//		complain.setBbillable(form.cbbillable.getValue());
//		
//		List<ComplainList> complainLis=form.activitytable.getDataprovider().getList();
//		ArrayList<ComplainList> complainArr=new ArrayList<ComplainList>();
//		complainArr.addAll(complainLis);
//		complain.setComplainList(complainArr);
//		
//		return complain;
//	}
	
	/**
	 * ends here
	 */
	
	public void updateView(Complain view) 
	{
		System.out.println("INSIDE UPDATE VIEW !!!!!");
		form.tbcount.setValue(view.getCount()+"");

		if(view.getDate()!=null)
			form.dbDate.setValue(view.getDate());
		if(view.getAssignto()!=null)
			form.olbassignto.setValue(view.getAssignto());
		if(view.getBranch()!=null)
			form.olbbBranch.setValue(view.getBranch());
		if(view.getTime()!=null)
			form.tbtime.setValue(view.getTime());
		if(view.getPriority()!=null)
			form.olbcPriority.setValue(view.getPriority());
		if(view.getPic()!=null) {
			form.prodInfoComposite.setValue(view.getPic());
			form.pinfo=view.getPic();//18-1-2024
		}
		if(view.getBrandName()!=null)
			form.tbBrandName.setValue(view.getBrandName());
		if(view.getModelNumber()!=null)
			form.tbModel.setValue(view.getModelNumber());
		if(view.getDestription()!=null)
			form.taDescription.setValue(view.getDestription());
		if(view.getRemark()!=null)
			form.taRemark.setValue(view.getRemark());
		if(view.getDueDate()!=null)
			form.dbDueDate.setValue(view.getDueDate());
		if(view.getSalesPerson()!=null)
			form.olbeSalesPerson.setValue(view.getSalesPerson());
		
		form.cbbillable.setValue(view.getBbillable());
		
		if(view.getServiceDate()!=null)
			form.serviceDate.setValue(view.getServiceDate());
		if(view.getServicetime()!=null)
			form.tbservicetime.setValue(view.getServicetime());
		if(view.getCompStatus()!=null)
			form.olbSatatus.setValue(view.getCompStatus());
		if(view.getContrtactId()!=null){
			form.tbContractId.setValue(view.getContrtactId()+"");
		}
		if(view.getQuatationId()!=null){
			form.tbQuatationId.setValue(view.getQuatationId()+"");
		}
		if(view.getServiceEngForExistingService()!=null){
			form.tbServiceEngineer.setValue(view.getServiceEngForExistingService());
		}
		if(view.getServiceId()!=null){
			form.tbServiceId.setValue(view.getServiceId()+"");
		}
		if(view.getExistingContractId()!=null){
			Date serviceId=null;
			if(view.getExistingServiceDate()!=null){
				serviceId=view.getExistingServiceDate();
			}
			form.getcontractIdfomCustomerID(view.getPersoninfo().getCount(),view.getExistingContractId(),serviceId,view.getServiceDetails());
		}
		form.activitytable.setValue(view.getComplainList());
	}
	
	////////////////////////////////////////////////////////// Changes Made here 16/10/2015
	
	
	private void reactToService() {
		if (!form.tbContractId.getValue().equals("")&& form.contractCount.getSelectedIndex() == 0&& form.cbbillable.getValue() == true) {
			Console.log("Complain-calling getContractDetailsForService Billable true, Contract created");
			getContractDetailsForService(model.getContrtactId(),Contract.CREATED);
		}
		if (form.contractCount.getSelectedIndex() != 0&& form.cbbillable.getValue() == false) {
			getContractDetailsForService(model.getExistingContractId(),Contract.APPROVED);
			Console.log("Complain-calling getContractDetailsForService Billable true, Contract approved");
		}
		
		

	}
	
	private void getContractDetailsForService(final int contractId, final String status) {
		System.out.println("INSIDE GET CONTRACT DETAILS FOR SERVICES....!!!");
		/** date 15.4.2019 added by komal to map service date from complain**/
		final Date serviceDate = form.dbNewServiceDate.getValue();
		Console.log("Complain-service date :" + serviceDate);
		
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = new Filter();
		temp.setIntValue(contractId);
		temp.setQuerryString("count");
		filtervec.add(temp);
		
		Console.log("Complain-CONTRACT COUNT :: "+contractId);

		temp = new Filter();
		temp.setStringValue(status);
		temp.setQuerryString("status");
		filtervec.add(temp);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
		form.showWaitSymbol();
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() == 0) {
					form.hideWaitSymbol();
					return;
				}
				final Contract cont = (Contract) result.get(0);
				if (cont.getStartDate() == null) {
					form.hideWaitSymbol();
					Console.log("Complain-Contract Start DATE NULL ");
					return;
				} 
				else {
					Console.log("Complain-Contract loaded. Loading customer.");
					final String proSerNo = (form.getTbProSerialNo().getValue()!=null ? (form.getTbProSerialNo().getValue().trim().length()>0 ?
							form.getTbProSerialNo().getValue() : "") :"");
					final String promodelNo = (form.getTbModel().getValue()!=null ?
							(form.getTbModel().getValue().trim().length()>0 ? form.getTbModel().getValue() : "") :"");
		
					MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Company c=new Company();
					Filter temp=null;
							
					temp=new Filter();
					temp.setQuerryString("count");
					temp.setIntValue(model.getPersoninfo().getCount());
					filtervec.add(temp);
							
					temp=new Filter();
					temp.setQuerryString("companyId");
					temp.setLongValue(c.getCompanyId());
					filtervec.add(temp);
							
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());
							
					service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			            public void onSuccess(ArrayList<SuperModel> result)
			            {
			            	Address addresInfo=new Address();
			            	if(result.size()==0)
			            	{
			            		form.showDialogMessage("No Customer Found. Please reload!");
			            	}
			            	if(result.size()>0)
			            	{ Console.log("Complain-customer loaded");
			            		for(SuperModel model1:result)
			            		{
				                		Customer custEntity=(Customer)model1;
				                		if(custEntity!=null){
				                			/**
				                			 * @author Anil
				                			 * @since 04-05-2020
				                			 * Setting expected completion date and time in service description
				                			 */
				                			String description="";
				                			if(form.dbExpectedCompletionDate.getValue()!=null) {
				                				description=description+"Expected completion date: "+AppUtility.parseDate(form.dbExpectedCompletionDate.getValue())+" ";
											}
				                			
				                			if(form.tmbExpectedCompletionTime.getValue()!=null&&!form.tmbExpectedCompletionTime.getValue().equals("")) {
				                				description=description+"Expected completion time: "+form.tmbExpectedCompletionTime.getValue();
											}
				                			
				                			addresInfo=custEntity.getAdress();
				                			if(status.equals(Contract.APPROVED)){
					            				setServiceDetailsWithServiceNo(addresInfo,contractId,cont,description);
					            			}else{
					            				form.hideWaitSymbol();
						                	
					            				///////////////////////////////////
											System.out.println("customer service");
											AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Service",Screen.SERVICE);
											final DeviceForm form = DevicePresenter.initalize();
											// form.showWaitSymbol();
											final Service quot = new Service();
											quot.setContractCount(contractId);
											quot.setBranch(model.getBranch());
											quot.setEmployee(model.getAssignto());
											quot.setPersonInfo(model.getPersoninfo());
											quot.setContractStartDate(cont.getStartDate());
											quot.setContractEndDate(cont.getEndDate());
	//										quot.setServiceDate();
											quot.setTicketNumber(model.getCount());
											/** date 15.4.2019 added by komal to map service date from complain**/
											if(serviceDate != null){ 
												quot.setServiceDate(serviceDate);
												form.getTbServiceDate().setValue(serviceDate);
											}
											
											form.setAddresInfo(addresInfo);
											quot.setAddress(addresInfo);
											/**
											 * nidhi
											 * 21-08-2018
											 */
											quot.setProModelNo(promodelNo);
											quot.setProSerialNo(proSerNo);
											if(model.getBbillable()==false){
												form.prodflag=true;
											}else{
												form.ticketBillableFlage=true;
											}
											quot.setStatus(Service.CREATED);
											
											quot.setDescription(description);
											
											System.out.println("Service -------");
											
											Timer t = new Timer() {
												@Override
												public void run() {
													form.setToNewState();
													// form.hideWaitSymbol();
													if(model.getCustomerBranch()!=null && !model.getCustomerBranch().equals("")){
														form.tbserviceBranch.clear();
														form.tbserviceBranch.addItem("--SELECT--");
														CustomerBranchDetails custBranch=new CustomerBranchDetails();
														custBranch.setBusinessUnitName(model.getCustomerBranch());
														form.tbserviceBranch.getItems().add(custBranch);
														form.tbserviceBranch.addItem(model.getCustomerBranch());
														form.tbserviceBranch.setValue(model.getCustomerBranch());
														
														quot.setServiceBranch(model.getCustomerBranch());
													}
													
													form.updateView(quot);
													if(model.getPic()!=null){
														form.getProdComposite().setValue(model.getPic());
														form.getProdComposite().setEnable(false);
													}else{
														form.getProdComposite().setEnable(true);
													}
													form.getTbServiceDate().setEnabled(true);
													form.setLineItemLis(cont.getItems());
													form.getIbServiceSrNo().setValue(1);
													System.out.println("Add 1111  "+quot.getAddress());
													form.getAddressComp().setValue(quot.getAddress());
													/**
													 * Date : 07-11-2017 BY ANIL
													 */
													form.getAddressComp().setEnable(true);
													form.getTbServiceDate().setEnabled(true);
													/**
													 * End
													 */
													
													/** Date 15-02-2018 By vijay for number setting range ***/
													if(cont.getNumberRange()!=null && !cont.getNumberRange().equals("")){
														form.numberRange=cont.getNumberRange();
													}
													/**
													 * ends here
													 */
													
													/** 
													 * Date 26-02-2018 By vijay for complaint service product data not saving in service issue
													 */
													getProductData(form);
													/**
													 * ends here
													 */
													
													System.out
															.println("ends here");
												}
											};
											t.schedule(3000);
											System.out.println("complated here");
											///////////////////////////////////////////
					            			}
						                }
						                else{
						                	form.showDialogMessage("Customer Address Not Set. Please reload");
						                }
			                		
//			            			}
				                		cust = custEntity;
					            }
					         }
			            }
					   
						public void onFailure(Throwable throwable)
					    {
					      	form.hideWaitSymbol();
					        form.showDialogMessage("An unexpected error occurred!");
					        throwable.printStackTrace();
					     }
					});
				}
						
			}
			public void onFailure(Throwable throwable) {
				form.hideWaitSymbol();
				form.showDialogMessage("Failed to retreive Contract details !");
				throwable.printStackTrace();
			}
		});
	}

	
	private void setServiceDetailsWithServiceNo(final Address addresInfo, final int contractId, final Contract cont, final String description) {
		/** date 15.4.2019 added by komal to map service date from complain**/
		final Date serviceDate = form.dbNewServiceDate.getValue();
		Console.log("Complain-service date :" + serviceDate);
		
//		service.getSearchResult(getServiceNoQuerry(Integer.parseInt(form.prodInfoComposite.getProdID().getValue())), new AsyncCallback<ArrayList<SuperModel>>() {
		service.getSearchResult(getServiceNoQuerry(form.pinfo.getProdID()), new AsyncCallback<ArrayList<SuperModel>>() {//18-01-2024
			       
			public void onSuccess(ArrayList<SuperModel> result)
            {
            	form.hideWaitSymbol();
            	Console.log("Complain-Service number result size :: "+result.size());
            	if(result.size()>0)
            	{
            		/**
            		 * Date :06-03-2017 By Rohan
            		 */
            		Service service=null;
            		if(form.olbServiceDetails.getSelectedIndex()!=0){
            			service=form.olbServiceDetails.getSelectedItem();
            			Console.log("Complain-in if form.olbServiceDetails.getSelectedIndex()!=0");
            		}
            				
            		final int serviceNo=result.size()+1;
            		
            		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Service",Screen.SERVICE);
					final DeviceForm form = DevicePresenter.initalize();
					// form.showWaitSymbol();
					final Service quot = new Service();
					quot.setContractCount(contractId);
					quot.setBranch(model.getBranch());
					quot.setEmployee(model.getAssignto());
					quot.setPersonInfo(model.getPersoninfo());
					quot.setContractStartDate(cont.getStartDate());
					quot.setContractEndDate(cont.getEndDate());
//										quot.setServiceDate();
					quot.setTicketNumber(model.getCount());
					
					Console.log("Complain-before setting address");
					if(service!=null){
						quot.setAddress(service.getAddress());
						form.setAddresInfo(service.getAddress());
					}else{
						quot.setAddress(addresInfo);
						form.setAddresInfo(addresInfo);
					}
						
					Console.log("Complain-before setting billable flag");
					if(model.getBbillable()==false){
						form.prodflag=true;
					}else{
						form.ticketBillableFlage=true;
					}
					/**
					 * @author Abhinav
					 * @since 03/12/2019
					 *I have change status FROM "CREATED" TO  "SERVICESTATUSSCHEDULE" As per Oolite Requirement by Rahul Tiwari
					 */
					quot.setStatus(Service.SERVICESTATUSSCHEDULE);
					
					//Moved this code up By ANIL ,Date :02-07-2019
					/** date 15.4.2019 added by komal to map service date from complain**/
					
					if(serviceDate != null){ 
						quot.setServiceDate(serviceDate);
						form.getTbServiceDate().setValue(serviceDate);
					}
					Console.log("Complain-after setting service date");
					if(description!=null) {
						quot.setDescription(description);
					}
					Console.log("Complain-after setting description");	
					
					if(model.getCustomerBranch()!=null && !model.getCustomerBranch().equals("")){
						form.tbserviceBranch.clear();
						form.tbserviceBranch.addItem("--SELECT--");
						CustomerBranchDetails custBranch=new CustomerBranchDetails();
						custBranch.setBusinessUnitName(model.getCustomerBranch());
						form.tbserviceBranch.getItems().add(custBranch);
						form.tbserviceBranch.addItem(model.getCustomerBranch());
						form.tbserviceBranch.setValue(model.getCustomerBranch());
						
						quot.setServiceBranch(model.getCustomerBranch());
					}
					
					Timer t = new Timer() {
						@Override
						public void run() {
							Console.log("Complain-in run");
							form.setToNewState();
							// form.hideWaitSymbol();
							form.updateView(quot);
							Console.log("Complain-after updateView");
							form.getProdComposite().setValue(model.getPic());
							Console.log("Complain-after setting model.getPic()"+model.getPic().getProductName());
							form.getProdComposite().setEnable(false);
							form.getTbServiceDate().setEnabled(true);
							form.setLineItemLis(cont.getItems());
							form.getIbServiceSrNo().setValue(1);
							form.getIbServiceSrNo().setValue(serviceNo);
							
							/**
							 * Date : 07-11-2017 BY ANIL
							 */
							form.getAddressComp().setEnable(true);
							form.getTbServiceDate().setEnabled(true);
							/**
							 * End
							 */
							/** Date 15-02-2018 By vijay for number setting range ***/
							if(cont.getNumberRange()!=null && !cont.getNumberRange().equals("")){
								form.numberRange=cont.getNumberRange();
								Console.log("Complain-contract number range set to service");
							}
							Console.log("Complain-after setting number range");
							/**
							 * ends here
							 */
							
							/** 
							 * Date 26-02-2018 By vijay for complaint service product data not saving in service issue
							 */
							
							getProductData(form);
							/**
							 * ends here
							 */
						}
					};
					t.schedule(3000);
					///////////////////////////////////////////
        			}
            }
            public void onFailure(Throwable throwable)
            {
            	form.showDialogMessage("An unexpected error occurred!");
            	throwable.printStackTrace();
            }
        });
	}
	
	
	/** 
	 * Date 26-02-2018 By vijay for complaint service product data not saving in service issue
	 * sending product details
	 */
	private void getProductData(final DeviceForm form2) {


		Console.log("Complain- in getProductData");
		MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("count");
//		temp.setIntValue(Integer.parseInt(form.prodInfoComposite.getProdID().getValue()));
		temp.setIntValue(form.pinfo.getProdID());//18-01-2024
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("productCode");
//		temp.setStringValue(form.prodInfoComposite.getProdCode().getValue().trim());
		temp.setStringValue(form.pinfo.getProductCode());//18-01-2024
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(model.getCompanyId());
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ServiceProduct());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			 
	            public void onSuccess(ArrayList<SuperModel> result)
	            {
	            	if(result.size()>0)
	            	{Console.log("Complain- in getProductData result size="+result.size());
	            		for(SuperModel smodel:result)
		            	{
	            			ServiceProduct superProdEntity=(ServiceProduct)smodel;
	            			form2.serviceProdEntity = superProdEntity;
		            	}
	            	}
	            }

				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("An unexpected error occurred!");
				}
		});
		
	
	}
	/**
	 * ends here
	 */
	private void reactToContract() {
//		if(form.prodInfoComposite.getValue()!=null){
//			ProdctType(form.prodInfoComposite.getProdCode().getValue(),
//					form.prodInfoComposite.getProdID().getValue(), false);
//		}
		if(form.pinfo!=null){//18-01-2024
			ProdctType(form.pinfo.getProductCode(),
					form.pinfo.getProdID()+"", false);
		}
		else{
			switchToForm(false);
		}
	}
	
	
	

	private void reactToQuatation() {
//		if(form.prodInfoComposite.getValue()!=null){
//			ProdctType(form.prodInfoComposite.getProdCode().getValue(),
//				form.prodInfoComposite.getProdID().getValue(), true);
//		}
		if(form.pinfo!=null){//18-01-2024
			ProdctType(form.pinfo.getProductCode(),
					form.pinfo.getProdID()+"", true);
		}else{
			switchToForm(true);
		}

	}
	
	
	

	@Override
	public void reactOnPrint() {
System.out.println("in side react on print");
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("Complain");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				
			}			

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result set size +++++++"+result.size());
				
				List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
				
				int realGuard=0;
				int premiumTech=0;
				if(result.size()==0){
//					final String url = GWT.getModuleBaseURL() + "pdfjob"+"?Id="+model.getId();
//					Window.open(url, "test", "enabled");
					
					panel=new PopupPanel(true);
					panel.add(conditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
					
				}
				else{
				
					for(SuperModel model:result)
					{
						ProcessConfiguration processConfig=(ProcessConfiguration)model;
						processList.addAll(processConfig.getProcessList());
						
					}
				
				for(int k=0;k<processList.size();k++){	
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("JobReportcumReceiptForRealGuard")&&processList.get(k).isStatus()==true){
					realGuard=realGuard+1;
					}
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PremiumTech")&&processList.get(k).isStatus()==true){
					premiumTech=premiumTech+1;
					Console.log("inside premiumTech process config");
					}
				
				
				}
				
			    if(realGuard>0){
			    	System.out.println("VVVVVVV   n side if");
			    	
			    	final String url = GWT.getModuleBaseURL() + "pdfjob"+"?Id="+model.getId();
					Window.open(url, "test", "enabled");
				}
			    else if(premiumTech>0){
			    	Console.log("inside premiumTech print");
			    	final String url = GWT.getModuleBaseURL() + "ComplainServlet"+"?Id="+model.getId();
					Window.open(url, "test", "enabled");
			    }
			    else{
			    	
			    	panel=new PopupPanel(true);
					panel.add(conditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
					
			    }
			}
			}
		});
		
		
//		final String url = GWT.getModuleBaseURL() + "Complainpdf"+"?Id="+model.getId();
//		 Window.open(url, "test", "enabled");
	}

	@Override
	protected void makeNewModel() {

		model = new Complain();
	}
	@Override
	public void setModel(Complain entity) {
		model = entity;
	}

	public static ComplainForm initalize() {
		
		AppMemory.getAppMemory().currentScreen.equals(ScreeenState.NEW);
		ComplainForm form = new ComplainForm();

		ComplainPresenterTable gentableSearch = new ComplainPresenterTableProxy();
		gentableSearch.setView(form);
		gentableSearch.applySelectionModle();
		ComplainPresenterSearchProxy.staticSuperTable = gentableSearch;
		ComplainPresenterSearchProxy searchpopup = new ComplainPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		ComplainPresenter presenter = new ComplainPresenter(form,new Complain());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
		
	}

	
public static void RetrieveProcessConfigStatus() {
		
	//  rohan commented on date : 3/2/2017
	
//		MyQuerry querry = new MyQuerry();
//	  	Company c = new Company();
//	  	Vector<Filter> filtervec=new Vector<Filter>();
//	  	Filter filter = null;
//	  	filter = new Filter();
//	  	filter.setQuerryString("companyId");
//		filter.setLongValue(c.getCompanyId());
//		filtervec.add(filter);
//		filter = new Filter();
//		filter.setQuerryString("processName");
//		filter.setStringValue("Complain");
//		filtervec.add(filter);
//		
//		filter = new Filter();
//		filter.setQuerryString("processList.status");
//		filter.setBooleanvalue(true);
//		filtervec.add(filter);
//		
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new ProcessConfiguration());
//		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				
//			}			
//
//			
//			
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				System.out.println(" result set size +++++++"+result.size());
//				int cnt=0;
//				List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();
//				
//				if(result.size()==0){
//					ComplainForm.forJobReportcumReceiptFlag=false;
//				}
//				else{
//				
//					for(SuperModel model:result)
//					{
//						ProcessConfiguration processConfig=(ProcessConfiguration)model;
//						processList.addAll(processConfig.getProcessList());
//						
//					}
//			System.out.println("RRRR  list size"+processList.size());
//			for(int k=0;k<processList.size();k++){	
//				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("ComplainFormForRealGuard")&&processList.get(k).isStatus()==true){
//					cnt= cnt+1;
//					}
//				}
//			
//			System.out.println("Counter value "+cnt);
//			if( cnt > 0)
//			{
//				ComplainForm.forJobReportcumReceiptFlag=true;
//				
//			}
//			System.out.println("Flag value "+ComplainForm.forJobReportcumReceiptFlag);
//			
//				}
//				
//			}
//		});	
	
	
	
	
	if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainFormForRealGuard"))
	{
			ComplainForm.forJobReportcumReceiptFlag=true;
			System.out.println("Flag value "+ComplainForm.forJobReportcumReceiptFlag);
	}
	else
	{
			ComplainForm.forJobReportcumReceiptFlag=false;
	}
		
	}
	
	
	
	
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.complain.Complain")
	public static class ComplainPresenterTable extends SuperTable<Complain>implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			return null;
		}

		@Override
		public void createTable() {

		}

		@Override
		protected void initializekeyprovider() {

		}

		@Override
		public void addFieldUpdater() {

		}

		@Override
		public void setEnable(boolean state) {

		}

		@Override
		public void applyStyle() {

		}
	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.complain.Complain")
	public static class ComplainPresenterSearch extends
			SearchPopUpScreen<Complain> {

		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
	};

	private void reactToNew() {
		this.initalize();
		form.setToNewState();
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if (event.getSource().equals(form.pic.getId())|| event.getSource().equals(form.pic.getName())|| event.getSource().equals(form.pic.getPhone())) {
			form.contractCount.clear();
			getcontractIdfomCustomerID(Integer.parseInt(form.pic.getId().getValue().trim()));	
//			checkCustomerStatus(Integer.parseInt(form.pic.getId().getValue()));
			form.oblCustomerBranch.clear();
			form.loadCustomerBranch(Integer.parseInt(form.pic.getId().getValue().trim()), null);
			
			Console.log("inside onSelection AMOL");
			form.setCustServAddress(Integer.parseInt(form.pic.getId().getValue().trim()));
		}
	}

	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(form.lbAssetId)){
			updateServiceTable();
			updateComponentDetails();
		}
		//komal
		if(event.getSource().equals(form.olbProductDetails)){
	       if(form.olbProductDetails.getSelectedIndex()!=0){
	    	   form.serviceInfoTable.getDataprovider().getList().clear();
	    	   getServiceTableDetails();
	    	   form.updateCustomerBranch();
	    	   form.initialilzeAssetId();
	       }
			
		}
		if(event.getSource().equals(form.oblCustomerBranch)){
	       if(form.olbProductDetails.getSelectedIndex()!=0&&form.oblCustomerBranch.getSelectedIndex()!=0){
	    	   form.serviceInfoTable.getDataprovider().getList().clear();
	    	   getServiceTableDetails();
	    	   form.initialilzeAssetId();
	       }
	       
	       
	       if(form.oblCustomerBranch.getSelectedIndex()!=0){
	    	   /**
	    	    * @author Anil
	    	    * @since 01-06-2020
	    	    */
	    	   CustomerBranchDetails customerBranch=form.oblCustomerBranch.getSelectedItem();
	    	   if(customerBranch.getTat()!=null&&!customerBranch.getTat().equals("")){
	    		   form.tmbTatSla.setValue(customerBranch.getTat());
	    	   }else{
	    		   form.tmbTatSla.setValue(form.custSla); 
	    	   }
	    	   form.calculateExpectedCompletionDateAndTime(form.dbDate.getValue(), form.tbtime.getValue(), form.convertTimeToNumbers(form.tmbTatSla.getValue()));
	    	   if(customerBranch.getAddress()!=null){
	    		   form.taServAddress.setValue(customerBranch.getAddress().getCompleteAddress());
	    	   }
	    	   
	       } else{
	    	   if(form.custSla!=null){
	    		   form.tmbTatSla.setValue(form.custSla);
	    		   form.calculateExpectedCompletionDateAndTime(form.dbDate.getValue(), form.tbtime.getValue(), form.convertTimeToNumbers(form.custSla));
	    	   }
	    	   if(form.custAddress!=null){
	    		   form.taServAddress.setValue(form.custAddress.getCompleteAddress());  
	    	   }
	       }
		}
		if (event.getSource().equals(form.contractCount)) {
			form.updateCustomerBranch();
			if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ProductwiseServicesInComplain")) {
				System.out.println("INSIDE CONTRACT CHANGE");
				if (form.contractCount.getSelectedIndex() != 0) {
					form.olbProductDetails.clear();
//					form.getProductDetailsFromContractId(Integer.parseInt(form.contractCount.getValue(form.contractCount.getSelectedIndex())), null,0);

					List<String> contableList = new ArrayList<String>();
					List<SalesLineItem> productList = new ArrayList<SalesLineItem>();
					 
					Contract conEntity = form.contractCount.getSelectedItem();
					if(conEntity.getItems().size() >0){
						for(SalesLineItem item : conEntity.getItems()){
							productList.add(item);
						}
					}
					
					if(conEntity.getTat()!=null) {
						form.custSla=conEntity.getTat();
						form.olbbBranch.setValue(conEntity.getBranch());
						form.tmbTatSla.setValue(conEntity.getTat());
						form.calculateExpectedCompletionDateAndTime(form.dbDate.getValue(), form.tbtime.getValue(), form.convertTimeToNumbers(form.tmbTatSla.getValue()));
					}
					form.olbProductDetails.setListItems(productList);
					form.olbProductDetails.setEnabled(true);
				
				
				
				} else {
					form.olbProductDetails.setSelectedIndex(0);
					form.olbProductDetails.setEnabled(false);
					form.prodInfoComposite.clear();					
					form.prodInfoComposite.setEnable(true);
					form.pinfo=null;//18-01-2024

					form.oblCustomerBranch.setSelectedIndex(0);
					form.lbAssetId.clear();
					form.lbAssetId.addItem("--SELECT--");
				}
			} 
			else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime")) {
				if (form.contractCount.getSelectedIndex() != 0) {
					form.olbServiceDetails.clear();
//					getServiceIdfomContractID(Integer.parseInt(form.contractCount.getValue(form.contractCount.getSelectedIndex()).trim()));
					getContractDetails(Integer.parseInt(form.contractCount.getValue(form.contractCount.getSelectedIndex()).trim()));
				} else {
					form.olbServiceDetails.setSelectedIndex(0);
					form.olbServiceDetails.setEnabled(false);
					form.tbServiceEngineer.setValue(null);
					form.prodInfoComposite.clear();
					form.prodInfoComposite.setEnable(true);
					form.pinfo=null;//18-01-2024
				}
			}
			else {
				if (form.contractCount.getSelectedIndex() != 0) {
					form.olbServiceDetails.clear();
					getServiceIdfomContractID(Integer.parseInt(form.contractCount.getValue(form.contractCount.getSelectedIndex()).trim()));
				} else {
					form.olbServiceDetails.setSelectedIndex(0);
					form.olbServiceDetails.setEnabled(false);
					form.tbServiceEngineer.setValue(null);
					form.prodInfoComposite.clear();
					form.prodInfoComposite.setEnable(true);
					form.pinfo=null;//18-01-2024
				}
			}
			
			updateComponentDetails();
		}

		if(event.getSource().equals(form.olbServiceDetails)){
					
			if(form.olbServiceDetails.getSelectedIndex()!=0){
				form.tbServiceEngineer.setValue(null);
				form.prodInfoComposite.clear();
				form.prodInfoComposite.setEnable(true);
				form.pinfo=null;//18-01-2024
				
				/**
				 * Date : 07-11-2017 BY ANIL
				 * commenting old code
				 */
//				String[] taskValue = form.olbServiceDetails.getValue(form.olbServiceDetails.getSelectedIndex()).trim().split("[ ]");
//				String serviceId = taskValue[0];
				Service object=form.olbServiceDetails.getSelectedItem();
				
				if(serviceList.size()!=0){
					for(int i=0;i<serviceList.size();i++){
//								if(serviceList.get(i).getCount()==Integer.parseInt(serviceId)){
						if(serviceList.get(i).getCount()==object.getCount()){
							form.tbServiceEngineer.setValue(serviceList.get(i).getEmployee());
							form.prodInfoComposite.getProdID().setValue(serviceList.get(i).getProduct().getCount()+"");
							form.prodInfoComposite.getProdName().setValue(serviceList.get(i).getProduct().getProductName());
							form.prodInfoComposite.getProdCode().setValue(serviceList.get(i).getProduct().getProductCode());
							form.prodInfoComposite.setEnable(false);
							form.pinfo.setProdID(serviceList.get(i).getProduct().getCount());//18-01-2024
							form.pinfo.setProductCode(serviceList.get(i).getProduct().getProductCode());
							form.pinfo.setProductName(serviceList.get(i).getProduct().getProductName());
							/**
							 * nidhi
							 * 9-08-2018
							 * 
							 */
							form.getTbProSerialNo().setValue(serviceList.get(i).getProSerialNo());
							form.getTbModel().setValue(serviceList.get(i).getProModelNo());
							
							if(serviceList.get(i).getServiceDate()!=null)
								form.getServiceDate().setValue(serviceList.get(i).getServiceDate());
							if(serviceList.get(i).getServiceTime()!=null)
								form.getTbservicetime().setValue(serviceList.get(i).getServiceTime());
							break;
						}
					}
				}
			}else{
	//						form.lbServiceCount.setEnabled(false);
				form.tbServiceEngineer.setValue(null);
				form.prodInfoComposite.clear();
				form.prodInfoComposite.setEnable(true);
				form.pinfo=null;//18-01-2024
			}
		}
				
		if(event.getSource().equals(form.dbNetPayable)){
			System.out.println("you are here on change");
			
			System.out.println(" net payment ==== "+form.dbNetPayable.getValue());
			System.out.println("hi recievd == "+form.dbAmountReceived.getValue());
			System.out.println("balance =="+form.dbBalance.getValue());
				form.dbBalance.setValue(form.dbNetPayable.getValue());
		}
						
		if(event.getSource().equals(form.dbAmountReceived)){
			if(form.dbAmountReceived.getValue()==null || form.dbAmountReceived.getValue()==0){
				form.dbBalance.setValue(form.dbNetPayable.getValue());
			}else{
				boolean valid =	validation(form.dbNetPayable.getValue(),form.dbAmountReceived.getValue());
				if(valid == false){
					form.showDialogMessage("amount recieved should not be greater than net payable amount");
					form.dbAmountReceived.setText(null);
					form.dbBalance.setValue(form.dbNetPayable.getValue());
				}
				if(valid){
					form.dbBalance.setValue(form.dbNetPayable.getValue()-form.dbAmountReceived.getValue());
					System.out.println(" if chnged value then == "+ form.dbBalance.getValue());
				}
			}
		}
				
	}
	private boolean validation(Double netPayable, Double amountReceived) {
		
		if(amountReceived > netPayable){
			System.out.println("it shouden't be greater::::::::::::::: ");
			return false;
		}
		
		return true;
	}
	

	public void getcontractIdfomCustomerID(int customerId) {

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(customerId);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.contractCount.addItem("--SELECT--");
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.contractCount.addItem("--SELECT--");
				if (result.size() == 0) {
//					form.pic.clear();
				} else {
					final List<Integer> contableList = new ArrayList<Integer>();
					List<Contract> contractList = new ArrayList<Contract>();
					
					for (SuperModel model : result) {
						Contract conEntity = (Contract) model;
						if(conEntity.getStatus().equals("Approved")){
							contableList.add(conEntity.getCount());
							contractList.add(conEntity);
						}
					}
							
					Collections.sort(contableList,new Comparator<Integer>() {
						@Override
						public int compare(Integer o1,Integer o2) {
							return o2.compareTo(o1);
						}
					});
					
					Comparator<Contract> comp = new Comparator<Contract>() {
						@Override
						public int compare(Contract p1, Contract p2) {
							return p2.getCreationDate().compareTo(p1.getCreationDate());
						}
					};
					Collections.sort(contractList, comp);

//					for (int i = 0; i < contableList.size(); i++) {
//						form.contractCount.addItem(contableList.get(i)+ "");
//						form.contractCount.setEnabled(true);
//					}
					
					form.contractCount.setListItems(contractList);
					form.contractCount.setEnabled(true);
				}
			}
		});
	}
	
	
//	public boolean validateCustomerVerification() {
//
//		if(form.pic.getValue()!=null&&(!form.tbfristname.getValue().equals("")||!form.tblastname.getValue().equals("")||form.lbcell.getValue()!=null||!form.etbemail.getValue().equals(""))){
//			form.showDialogMessage("Please add only One Customer!");
//			return false;
//		}
//		return true;
//	}

	public void getServiceIdfomContractID(int contractId) {

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(contractId);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.olbServiceDetails.addItem("--SELECT--");
//						form.tbServiceEngineer.setValue(null);
//						form.prodInfoComposite.clear();
				serviceList=new ArrayList<Service>();
				if (result.size() == 0) {
					// form.pic.clear();
				} else {
					for (SuperModel model : result) {
						Service conEntity = (Service) model;
						//  rohan added this code for adding only completed services in the list
						if(conEntity.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
							serviceList.add(conEntity);
						}
					}
					
					if (serviceList.size() != 0) {
						form.olbServiceDetails.setListItems(serviceList);
						form.olbServiceDetails.setEnabled(true);
					}
					else{
						form.olbServiceDetails.setEnabled(false);
					}
				}
			}
		});
	}

	public void ProdctType(String pcode, String productId,final boolean quotationFlag) {
		System.out.println("Started");
		Console.log("Inside Product Type");
		final GenricServiceAsync genasync = GWT.create(GenricService.class);
		Vector<Filter> filtervec = new Vector<Filter>();

		int prodId = Integer.parseInt(productId);

		final MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("productCode");
		filter.setStringValue(pcode.trim());
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(prodId);
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
		 form.showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {
				genasync.getSearchResult(querry,
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
								form.showDialogMessage("An Unexpected error occurred!");
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								form.hideWaitSymbol();
								final ArrayList<SalesLineItem> prodList = new ArrayList<SalesLineItem>();
								if (result.size() == 0) {
									form.showDialogMessage("Please check whether product status is active or not!");
								}
								SalesLineItem lis = null;
								for (SuperModel model : result) {
									SuperProduct superProdEntity = (SuperProduct) model;
									lis = AppUtility.ReactOnAddProductComposite(superProdEntity);
									lis.setQuantity(1.0);
									Integer prodSrNo = lis.getProductSrNo();
									/**
									 * nidhi
									 * 21-08-2018
									 */
									lis.setProSerialNo((form.getTbProSerialNo().getValue()!=null ? (form.getTbProSerialNo().getValue().trim().length()>0 ?
														form.getTbProSerialNo().getValue() : "") :""));
									lis.setProModelNo((form.getTbModel().getValue()!=null ?
														(form.getTbModel().getValue().trim().length()>0 ? form.getTbModel().getValue() : "") :""));
									prodList.add(lis);
								}

								if (quotationFlag == true) {
									// //////////////// QUOTATION

									AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quotation",Screen.QUOTATION);
									final QuotationForm form = QuotationPresenter.initalize();
									form.showWaitSymbol();
									final Quotation quot = new Quotation();
									quot.setBranch(model.getBranch());
									quot.setEmployee(model.getSalesPerson());
									quot.setCinfo(model.getPersoninfo());
									quot.setComplaintFlag(true);
									if(model.getExistingServiceId()!=null||model.getServiceId()!=null){
										quot.setDescription("Service Id-"+ model.getExistingServiceId());
									}

									System.out.println("TICKET NUMBER :::::: "+ model.getCount() + "  FLAGE :: "+ quot.getComplaintFlag());

									quot.setTicketNumber(model.getCount());
									quot.setStatus(Quotation.CREATED);
									quot.setIsQuotation(true);

									Timer t = new Timer() {
										@Override
										public void run() {
											form.setToNewState();
											form.hideWaitSymbol();
											form.updateView(quot);
											List<SalesLineItem> lineItemLis = prodList;
											QuotationPresenter.custcount = form.getPersonInfoComposite().getIdValue();
											if (lineItemLis.size() != 0) {
												form.getSaleslineitemquotationtable().setValue(lineItemLis);
											}
											form.getTbQuotatinId().setText("");
											form.getTbContractId().setText("");
											form.getPersonInfoComposite().setEnabled(false);
											form.getTbTicketNumber().setEnabled(false);
										}
									};
									t.schedule(5000);
									// //////////////////
								} else {
									// //////// CONTRACT
									final String branch =form.olbbBranch.getValue(form.olbbBranch.getSelectedIndex());
									AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract",Screen.CONTRACT);
									final String customerBranch = form.oblCustomerBranch.getValue(form.oblCustomerBranch.getSelectedIndex());
									final ContractForm form = ContractPresenter.initalize();
									form.showWaitSymbol();
									
									final Contract quot = new Contract();
									
									quot.setBranch(model.getBranch());
									quot.setEmployee(model.getSalesPerson());
									quot.setCinfo(model.getPersoninfo());
									quot.setComplaintFlag(true);
									if(model.getExistingServiceId()!=null){
										quot.setRefNo("Service Id-"+ model.getExistingServiceId());
									}
									form.checkCustomerBranch(model.getPersoninfo().getCount());
									System.out.println("TICKET NUMBER :::::: "
											+ model.getCount() + "  FLAGE :: "
											+ quot.getComplaintFlag());

									quot.setTicketNumber(model.getCount());
									quot.setIsQuotation(false);
									quot.setStatus(Quotation.CREATED);

									Timer t = new Timer() {
										@Override
										public void run() {
											form.setToNewState();
											form.hideWaitSymbol();
											form.updateView(quot);
											List<SalesLineItem> lineItemLis = prodList;
											ContractPresenter.custcount = form.getPersonInfoComposite().getIdValue();
											if (lineItemLis.size() != 0) {
												form.getSaleslineitemtable().setValue(lineItemLis);
											}
//											
											form.getTbQuotatinId().setText("");
											form.getTbContractId().setText("");
											form.getPersonInfoComposite().setEnabled(false);
											form.getTbTicketNumber().setEnabled(false);
											
											for(SalesLineItem saleObj : lineItemLis) {
												if(saleObj.getQty()!=0){
													saleObj.setArea(saleObj.getQty() + "");	
												}else{
													saleObj.setArea("NA");	
												}
												saleObj.setArea(saleObj.getQty() + "");
												saleObj.setTotalAmount(saleObj.getTotalAmount());
												
											
												/*** Date 17-05-2019 by Vijay bug branches added to if not added then contract approved then 
												 * services created but contract in created state so again approve then services created twice
												 */
												Console.log("customerBranchlist size in form "+form.customerbranchlist.size());
												ArrayList<BranchWiseScheduling> branchlist = AppUtility.getCustomerBranchSchedulinglistInfo(form.customerbranchlist,model.getBranch(),saleObj.getUnitOfMeasurement(),0);
												Integer prodSrNo = saleObj.getProductSrNo();
												HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
												customerBranchlist.put(prodSrNo, branchlist);
												
												Console.log("customerBranchlist in complain "+customerBranchlist.size());
												
												saleObj.setCustomerBranchSchedulingInfo(customerBranchlist);
												
												/**
												 * ends here
												 */
											}
											for(int i=0;i<lineItemLis.size();i++){
									    		  lineItemLis.get(i).setQuantity(1.0);
									    	  }
									    	  Console.log("lineItemLis "+lineItemLis.size());
									    	  if(lineItemLis.size()>0){
									    		  form.getSaleslineitemtable().setValue(lineItemLis);
									    	  }
									    	  
									    	 ContractPresenter.custcount=form.getPersonInfoComposite().getIdValue();
//									    	 LeadPresenter lead=new LeadPresenter(null, null);
									    	 
									    	 Console.log("SALESLINE ITEM TABLE "+form.getSaleslineitemtable().getDataprovider().getList().size());
									    	 Console.log("lineItemLis SIZEEE000 "+lineItemLis.size());
									    	 
									    	 if(form.getSaleslineitemtable().getDataprovider().getList().size() == 0){
									    		  Console.log("Inside getSaleslineitemtable size is 0 ");
													 List<ServiceSchedule> popuplist=reactfordefaultTable(lineItemLis,branch,form.customerbranchlist);
													 form.getServiceScheduleTable().getDataprovider().setList(popuplist);
													 Console.log("lineItemLis SIZEEE111 "+lineItemLis.size());
											  }else{
													 List<ServiceSchedule> privlist=form.getServiceScheduleTable().getDataprovider().getList();
													 Console.log("Inside getSaleslineitemtable size greater than zero "+privlist.size());
													 
													  Console.log("lineItemLis SIZEEE222 "+lineItemLis.size());
													 List<ServiceSchedule> popuplist=reactfordefaultTable(lineItemLis,branch,form.customerbranchlist);
													 Console.log("popuplist Size "+popuplist.size());
													 
													 List<ServiceSchedule> popuplist1=new ArrayList<ServiceSchedule>();
													 popuplist1.addAll(privlist);
													 popuplist1.addAll(popuplist);
													 form.getServiceScheduleTable().getDataprovider().setList(popuplist1);
													 System.out.println("popuplist1 == "+popuplist1.size());
											  }
										}
									};
									t.schedule(5000);
									// //////////////////
								}

							}
						});
				// form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}
	
	
	private ArrayList<BranchWiseScheduling> getupdatedlistwithCustomerBranch(
			ArrayList<BranchWiseScheduling> branchlist, String customerBranch) {
		
		boolean flag = false;
		System.out.println("customerBranch"+customerBranch);
		for(BranchWiseScheduling scheduling : branchlist){
			System.out.println("scheduling.getBranchName() "+scheduling.getBranchName());
			if(scheduling.getBranchName().trim().equals(customerBranch.trim())){
				scheduling.setCheck(true);
				flag = true;
				break;
			}
		}
		
		if(flag){
			System.out.println("flag"+flag);
			for(BranchWiseScheduling scheduling : branchlist){
				System.out.println("scheduling.getBranchName()== "+scheduling.getBranchName());
				if(scheduling.getBranchName().trim().equals("Service Address")){
					scheduling.setCheck(false);
					break;
				}
			}
		}
		
		return branchlist;
	}
	
	public List<ServiceSchedule> reactfordefaultTable(List<SalesLineItem> lineItemLis, String branch, ArrayList<CustomerBranchDetails> customerbranchlist) {

		Console.log("Inside reactfordefaultTable Lead "+lineItemLis.size());
		/**
		 * Date 02-05-2018 
		 * Developer : Vijay
		 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
		 * so i have updated below code using hashmap. 
		 */
		/**
		 * Date : 24-08-2017 BY ANIL
		 * setting customers branch information at product level if any
		 */
		for(SalesLineItem lis:lineItemLis){
			Console.log("branch Name "+branch);
			Console.log("Customer branch list size "+customerbranchlist.size());
			
			ArrayList<BranchWiseScheduling> branchlist = AppUtility.getCustomerBranchSchedulinglistInfo(customerbranchlist,branch,"",0);
			Console.log("branch list size "+branchlist.size());
			Integer prodSrNo = lis.getProductSrNo();
			HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
			customerBranchlist.put(prodSrNo, branchlist);
			
			if(model.getCustomerBranch()!=null && !model.getCustomerBranch().equals("")){
				branchlist = getupdatedlistwithCustomerBranch(branchlist,model.getCustomerBranch());
			}
			
			Console.log("Customer branch list size "+customerBranchlist.size());
			lis.setCustomerBranchSchedulingInfo(customerBranchlist);
			
		
		}

		
		List<SalesLineItem> salesitemlis=new ArrayList<SalesLineItem>();
		salesitemlis.addAll(lineItemLis);
		ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
		serschelist.clear();
		Console.log("Inside salesitemlis.size() "+salesitemlis.size());
		for(int i=0;i<salesitemlis.size();i++){
			
			int qty=0;
			qty=(int) salesitemlis.get(i).getQty();
			for(int k=0;k < qty;k++){
		
			long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
			int noOfdays=salesitemlis.get(i).getDuration();
			int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
			Date servicedate=new Date();
			Date d=new Date(servicedate.getTime());
			Date productEndDate= new Date(servicedate.getTime());
			CalendarUtil.addDaysToDate(productEndDate, noOfdays);
			Date prodenddate=new Date(productEndDate.getTime());
			productEndDate=prodenddate;
			Date tempdate=new Date();
			for(int j=0;j<noServices;j++){
				Console.log("Inside noServices ");
				if(j>0){
					CalendarUtil.addDaysToDate(d, interval);
					tempdate=new Date(d.getTime());
				}
				
				ServiceSchedule ssch=new ServiceSchedule();
				
			//  rohan added this 1 field 
				System.out.println("In side method RRRRRRRRRRRRR"+salesitemlis.get(i).getProductSrNo());
				ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
				
				Date Stardaate=new Date();
				ssch.setScheduleStartDate(Stardaate);
				ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
				ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
				ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
				ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
				ssch.setScheduleServiceNo(j+1);
				ssch.setScheduleProdStartDate(Stardaate);
				ssch.setScheduleProdEndDate(productEndDate);
				
				ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
				ssch.setServicingBranch(branch);
				System.out.println("SERVICING TIME ::: "+salesitemlis.get(i).getServicingTIme());
				System.out.println("BRANCH ::: "+branch);
				
				String str="Flexible";
				ssch.setScheduleServiceTime(str);
				if(j==0){
					ssch.setScheduleServiceDate(servicedate);
					}
				if(j!=0){
					if(productEndDate.before(d)==false){
						ssch.setScheduleServiceDate(tempdate);
					}else{
						ssch.setScheduleServiceDate(productEndDate);
					}
				}
				
				if(customerbranchlist.size()==qty){
					ssch.setScheduleProBranch(customerbranchlist.get(k).getBusinessUnitName());
				}else if(customerbranchlist.size()==0){
					ssch.setScheduleProBranch("Service Address");
				}else{
					ssch.setScheduleProBranch("Service Address");
				}
				
				ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
				
				serschelist.add(ssch);
				Console.log("service schedule list size "+serschelist.size());
			}
		}
		}
		return serschelist;
	
	}
	
	private void switchToForm(boolean isquotationFlag){
		if (isquotationFlag == true) {
			// //////////////// QUOTATION

			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quotation",Screen.QUOTATION);
			final QuotationForm form = QuotationPresenter.initalize();
			form.showWaitSymbol();
			final Quotation quot = new Quotation();
			quot.setBranch(model.getBranch());
			quot.setEmployee(model.getSalesPerson());
			quot.setCinfo(model.getPersoninfo());
			
			//*********************rohan added this code for loading customer branch Date : 3/2/2017****
			QuotationPresenter.custcount =model.getPersoninfo().getCount();
			
			//******************ends here ********************************************************
			quot.setComplaintFlag(true);
			if(model.getExistingServiceId()!=null||model.getServiceId()!=null){
				quot.setDescription("Service Id-"+ model.getExistingServiceId());
			}

			System.out.println("TICKET NUMBER :::::: "+ model.getCount() + "  FLAGE :: "+ quot.getComplaintFlag());

			quot.setTicketNumber(model.getCount());
			quot.setStatus(Quotation.CREATED);
			quot.setIsQuotation(true);

			Timer t = new Timer() {
				@Override
				public void run() {
					form.setToNewState();
					form.hideWaitSymbol();
					form.updateView(quot);
					QuotationPresenter.custcount = form.getPersonInfoComposite().getIdValue();
					form.getTbQuotatinId().setText("");
					form.getTbContractId().setText("");
					form.getPersonInfoComposite().setEnabled(false);
					form.getTbTicketNumber().setEnabled(false);
				}
			};
			t.schedule(5000);
			// //////////////////
		} else {
			// //////// CONTRACT
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract",Screen.CONTRACT);
			final ContractForm form = ContractPresenter.initalize();
//			form.showWaitSymbol();
			final Contract quot = new Contract();
			quot.setBranch(model.getBranch());
			quot.setEmployee(model.getSalesPerson());
			quot.setCinfo(model.getPersoninfo());
//			//*********************rohan added this code for loading customer branch Date : 3/2/2017****
//			ContractPresenter.custcount =model.getPersoninfo().getCount();
//			
//			//******************ends here ********************************************************
			quot.setComplaintFlag(true);
			if(model.getExistingServiceId()!=null){
			quot.setRefNo("Service Id-"+ model.getExistingServiceId());
			}

			System.out.println("TICKET NUMBER :::::: "
					+ model.getCount() + "  FLAGE :: "
					+ quot.getComplaintFlag());

			quot.setTicketNumber(model.getCount());
			quot.setIsQuotation(false);
			quot.setStatus(Quotation.CREATED);

			/**
			 * Date : 11-05-2017 BY ANIL
			 */
			form.checkCustomerBranch(model.getPersoninfo().getCount());
			/**
			 * End
			 */
//			form.showWaitSymbol();
			
			Timer t = new Timer() {
				@Override
				public void run() {
					form.setToNewState();
//					form.hideWaitSymbol();

					form.updateView(quot);

			    	form.getPersonInfoComposite().setValue(quot.getCinfo());
			    	
			    	
					ContractPresenter.custcount = form.getPersonInfoComposite().getIdValue();
					form.getTbQuotatinId().setText("");
					form.getTbContractId().setText("");
					form.getPersonInfoComposite().setEnabled(false);
					form.getTbTicketNumber().setEnabled(false);
					 form.hideWaitSymbol();
//					 form.hideWaitSymbol();

				}
			};
			t.schedule(5000);
			// //////////////////
			
//			 form.hideWaitSymbol();
//			 form.hideWaitSymbol();

		}
	}
	
	

	@Override
	public void reactOnDownload() {
		ArrayList<Complain> woArray = new ArrayList<Complain>();
		List<Complain> list = (List<Complain>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		woArray.addAll(list);
		csvservice.setComplainlist(woArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				/**
				 * Date : 24-10-2017 BY ANIL
				 * For NBHC we are providing a separate download with less fields in csv
				 * Process Type-CustomerSupportReport
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "CustomerSupportReport")){
					final String url = gwt + "csvservlet" + "?type=" + 126;
					Window.open(url, "test", "enabled");
				}
				/**
				 * @author Anil
				 * @since 04-05-2020
				 * Premium tech solution 
				 */
				else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime")){
					final String url = gwt + "csvservlet" + "?type=" + 182;
					Window.open(url, "test", "enabled");
				}else{
					final String url = gwt + "csvservlet" + "?type=" + 95;
					Window.open(url, "test", "enabled");
				}
			}
		});
	}
	
	
	
	protected MyQuerry getServiceNoQuerry(int prodCountId)
	{
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setLongValue(c.getCompanyId());
		temp.setQuerryString("companyId");
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setIntValue(Integer.parseInt(form.contractCount.getValue(form.contractCount.getSelectedIndex())));
		temp.setQuerryString("contractCount");
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setIntValue(prodCountId);
		temp.setQuerryString("product.count");
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
		
		return querry;
	}
	
	public void checkCustomerStatus(int cCount)
	{
		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(cCount);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel model:result)
					{
						Customer custEntity = (Customer)model;
						if(custEntity.getNewCustomerFlag()==true){
							form.showDialogMessage("Please fill customer information by editing customer");
							form.pic.clear();
						}
						else {
							getcontractIdfomCustomerID(Integer.parseInt(form.pic.getId().getValue().trim()));					
						}
					}
				}
			 });
	}
	
	
	
	public boolean validateCustomer() {

		if(custpoppup.getTbFullName().getValue().equals("")&&custpoppup.getPnbLandlineNo().getValue().equals("")&&custpoppup.getEtbEmail().getValue().equals("")){
			form.showDialogMessage("Please add Customer Fullname..!");
			return false;
		}
		
		if(custpoppup.getOlbbBranch().getSelectedIndex()==0){
			form.showDialogMessage("Please Select Customer Branch..!");
			return false;
		}
		
//		if(!custpoppup.getTbfName().getValue().equals("")&&custpoppup.getTblName().getValue().equals("")){
//			form.showDialogMessage("Please Fill Customer Last Name!");
//			return false;
//		}
//		if(custpoppup.getTbfName().getValue().equals("")&&!custpoppup.getTblName().getValue().equals("")){
//			form.showDialogMessage("Please Fill Customer First Name!");
//			return false;
//		}
		
		if(custpoppup.getPnbLandlineNo().getValue().equals("") && custpoppup.getEtbEmail().getValue().equals("")){
			form.showDialogMessage("Please Fill either customer cell or customer email..!");
			return false;
		}
		
		/**
		  * Date 26-07-2018 By Vijay
		  * Des:- GSTIN Number can not be greater than 15 characters Validation
		  */
		if(custpoppup.getTbGstNumber().getValue().trim()!=null && !custpoppup.getTbGstNumber().getValue().trim().equals("")){
			if(custpoppup.getTbGstNumber().getValue().trim().length()>15 || custpoppup.getTbGstNumber().getValue().trim().length()<15){
				form.showDialogMessage("GSTIN number must be 15 characters!");
				return false;
			}
		}
		/**
		 * ends here
		 */
		
		return true;
	}

	
	
	public void reactOnEmail() {
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "PremiumTech")){
			boolean conf = Window.confirm("Do you really want to send email?");
			if (conf == true) {
				email.initiateComplainEmail(model, new AsyncCallback<Void>() {
					
					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						Window.alert("Email Sent Sucessfully !");
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Window.alert("Resource Quota Ended ");
					}
				});
			}
			return;
		}
//		System.out.println("EMAIL");
//		
//				
//				String msgBody="</br></br> Dear "+model.getAssignto().trim()+ ", <br><br> The Following Ticket is assinged to you. The details are as follows:- <br> <br>";
//
//				//Table header 1
//				ArrayList<String> tbl1_header=new ArrayList<String>();
//				tbl1_header.add("Customer Name");
//				tbl1_header.add("Ticket No");
//				tbl1_header.add("Ticket Date");
//				tbl1_header.add("Product Name");
//				tbl1_header.add("Branch");
//				tbl1_header.add("Creation Date");
//				tbl1_header.add("Description");
//				System.out.println("Tbl1 header Size :"+tbl1_header.size());
//
//				// Table 1
//				ArrayList<String> tbl1=new ArrayList<String>();
//				tbl1.add(model.getPersoninfo().getFullName());
//				tbl1.add(model.getCount()+"");
//				
//				if(model.getComplainDate()!=null)
//				tbl1.add(AppUtility.parseDate(model.getComplainDate()));
//				
//				if(model.getPic()!=null)
//				tbl1.add(model.getPic().getProductName());
//				
//				tbl1.add(model.getBranch());
//				
//				if(model.getDate()!=null)
//				tbl1.add(AppUtility.parseDate(model.getDate()));
//				
//				if(model.getDestription()!=null && !model.getDestription().equals(""))
//				tbl1.add(model.getDestription());
//				
//				System.out.println("Tbl1 Size :"+tbl1.size());
//				
//
//				ArrayList<String> toemaillist = new ArrayList<String>();
//				Employee emp = form.olbassignto.getSelectedItem();
//				toemaillist.add(emp.getEmail());
//				
//				email.emailSend(model.getCompanyId(),model, toemaillist, "Complaint", msgBody, tbl1_header, tbl1, new AsyncCallback<Void>() {
//
//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//						
//						Window.alert("Resource Quota Ended ");
//						caught.printStackTrace();
//					}
//
//					@Override
//					public void onSuccess(Void result) {
//						Window.alert("Email Sent Sucessfully !");
//					}
//				} );
		
		
		/**
		 * @author Vijay Date 19-03-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData();
		
		/**
		 * ends here
		 */
				
	}
	
		/** date 19.4.2018 added by komal to create contract directly and make service**/
	private void createContract(){
		GeneralServiceAsync genservice = GWT.create(GeneralService.class);
		genservice.createAndGetContractDetails(UserConfiguration.getCompanyId(), model, LoginPresenter.loggedInUser, new AsyncCallback<Contract>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(final Contract result) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Service scheduled successfully.");
				Filter filter = new Filter();
				filter.setIntValue(result.getCount());
				filter.setQuerryString("contractCount");
				Vector<Filter> vecfilter = new Vector<Filter>();
				vecfilter.add(filter);
				MyQuerry query = new MyQuerry(vecfilter, new Service());
				ServicePresenter.initalize(query);
			}
		});

	}
			
	
	private void getServiceTableDetails(){
		int count = Integer.parseInt(form.contractCount.getValue(form.contractCount.getSelectedIndex()));
		SalesLineItem product = (SalesLineItem) form.olbProductDetails.getSelectedItem();
		
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(count);
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("product.count");
		filter.setIntValue(product.getPrduct().getCount());
		filtervec.add(filter);
		
		if(form.oblCustomerBranch.getValue()!=null&&form.oblCustomerBranch.getSelectedIndex()!=0){
			filter = new Filter();
			filter.setQuerryString("serviceBranch");
			filter.setStringValue(form.oblCustomerBranch.getValue());
			filtervec.add(filter);
		}
		
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
	
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.olbServiceDetails.addItem("--SELECT--");
//						form.tbServiceEngineer.setValue(null);
//						form.prodInfoComposite.clear();
				serviceList=new ArrayList<Service>();
				ProductInfo productInfo = new ProductInfo();
				if (result.size() == 0) {
					// form.pic.clear();
				} else {
					for (SuperModel model : result) {
						Service service = (Service) model;
						//if(service.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
							serviceList.add(service);
							productInfo.setProdID(service.getProduct().getCount());
							productInfo.setProductName(service.getProduct().getProductName());
                            productInfo.setProductCode(service.getProduct().getProductCode());
                            productInfo.setProductPrice(service.getProduct().getPrice());
                            productInfo.setProductCategory(service.getProduct().getProductCategory());
                            productInfo.setVat(service.getProduct().getVatTax().getPercentage());
                            productInfo.setServiceTax(service.getProduct().getServiceTax().getPercentage());
						//}
					}
					Comparator<Service> compare = new  Comparator<Service>() {
						
						@Override
						public int compare(Service o1, Service o2) {
							// TODO Auto-generated method stub
							return Integer.compare(o1.getServiceSerialNo(), o2.getServiceSerialNo());
						}
					};
					Collections.sort(serviceList,compare);
					if (serviceList.size() != 0) {
						form.serviceInfoTable.getDataprovider().setList(serviceList);
					    form.prodInfoComposite.setValue(productInfo);
					    form.pinfo=productInfo;//18-01-2024
					    /**
					     * @author Anil , 04-03-2020
					     * In case product composite is not loaded in that condition product details were not getting saved
					     * and service is not getting created from that complaint
					     */
					    form.prodInfoComposite.getProdIDtoProdInfo().put(productInfo.getProdID(), productInfo);
					    
					    form.prodInfoComposite.setEnabled(false);
					    
					}
					
				}
			}
		});

	}

	public void getContractDetails(final int contractId) {

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(contractId);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("Unable to load contract. "+caught.getMessage());
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if(result==null||result.size()==0) {
					form.showDialogMessage("Contract not fouund.");
				}else {
					Contract contract=(Contract) result.get(0);
					if(contract.getTat()!=null) {
						form.olbbBranch.setValue(contract.getBranch());
						form.tmbTatSla.setValue(contract.getTat());
						form.calculateExpectedCompletionDateAndTime(form.dbDate.getValue(), form.tbtime.getValue(), form.convertTimeToNumbers(form.tmbTatSla.getValue()));
					}
					
					getServiceIdfomContractID(contractId);
				}
			}
		});
	}
	
	
	public void viewServices(){
		if(form.tbServiceId.getValue()==null||form.tbServiceId.getValue().equals("")){
			form.showDialogMessage("Please create service first.");
			return;
		}
		Filter filter = new Filter();
		filter.setIntValue(model.getCount());
		filter.setQuerryString("ticketNumber");
		
		Vector<Filter> vecfilter = new Vector<Filter>();
		vecfilter.add(filter);
		MyQuerry query = new MyQuerry(vecfilter, new Service());
		ServicePresenter.initalize(query);
	}
	
	public void updateServiceTable(){
		if(form.lbAssetId.getSelectedIndex()!=0){
			int assetId=Integer.parseInt(form.lbAssetId.getValue(form.lbAssetId.getSelectedIndex()));
			List<Service> sList=new ArrayList<Service>();
			if(serviceList.size()!=0){
				for(Service serviceObj:serviceList){
					if(assetId==serviceObj.getAssetId()){
						sList.add(serviceObj);
					}
				}
				
				if(sList.size()!=0){
					form.serviceInfoTable.getDataprovider().setList(sList);
				}
			}
		}else{
			if(serviceList.size()!=0){
				form.serviceInfoTable.getDataprovider().setList(serviceList);
			}
		}
	}
	
	private void updateComponentDetails() {
		if(form.lbAssetId.getSelectedIndex()!=0){
			ComponentDetails comp=form.lbAssetId.getSelectedItem();
			if(comp!=null){
				if(comp.getMfgNum()!=null){
					form.tbMfgNum.setValue(comp.getMfgNum());
				}
				if(comp.getAssetUnit()!=null){
					form.tbAssetUnit.setValue(comp.getAssetUnit());
				}
				form.mfgDate=comp.getMfgDate();
				form.replacementDate=comp.getReplacementDate();
				form.componentName=comp.getComponentName();
			}
		}else{
			form.tbMfgNum.setValue("");
			form.tbAssetUnit.setValue("");
			form.mfgDate=null;
			form.replacementDate=null;
			form.componentName=null;
		}
	}
	
	private void cancelComplain() {
		model.setRemark("This document was cancelled with remark "+ popup.getRemark().getValue().trim());
		model.setDestription("Complain Id =" + model.getCount() + " "
				+ "Complain Status =" + model.getCompStatus() + "\n"
				+ "has been cancelled by " + LoginPresenter.loggedInUser.trim()
				+ " with remark " + "\n" + "Remark ="+ popup.getRemark().getValue()
				+"\n" +model.getDestription());
		model.setCompStatus("Cancelled");
		form.showWaitSymbol();
		async.save(model, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				form.hideWaitSymbol();
				form.showDialogMessage("Data Updated Successfully.");
				form.setToViewState();
				form.setAppHeaderBarAsPerStatus();
				form.olbSatatus.setValue(model.getCompStatus());
				form.taRemark.setValue(model.getRemark());
				form.taDescription.setValue(model.getDestription()); 
			}
		});	  
	}
	
	private void setEmailPopUpData() {
		form.showWaitSymbol();
		String customerEmail = "";

		String branchName = form.olbbBranch.getValue();
		String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getSalesPerson());
		customerEmail = AppUtility.getCustomerEmailid(model.getPersoninfo().getCount());
		
		String customerName = model.getPersoninfo().getFullName();
		if(!customerName.equals("") && !customerEmail.equals("")){
			label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersoninfo().getCount(),customerName,customerEmail,null);
			emailpopup.taToEmailId.setValue(customerEmail);

		}
		else{
	
			MyQuerry querry = AppUtility.getcustomerQuerry(model.getPersoninfo().getCount());
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					for(SuperModel smodel : result){
						Customer custEntity  = (Customer) smodel;
						System.out.println("customer Name = =="+custEntity.getFullname());
						System.out.println("Customer Email = == "+custEntity.getEmail());
						
						if(custEntity.getEmail()!=null){
							label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersoninfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
							emailpopup.taToEmailId.setValue(custEntity.getEmail());
						}
						else{
							label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersoninfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
						}
						break;
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
				}
			});
		}
		
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,LoginPresenter.currentModule.trim(),screenName);
		emailpopup.taFromEmailId.setValue(fromEmailId);
		emailpopup.smodel = model;
		form.hideWaitSymbol();
		
	}
	
	@Override
	public void reactOnSMS() {
		smspopup.showPopUp();
		loadSMSPopUpData();
	}

	private void loadSMSPopUpData() {
		form.showWaitSymbol();
		String customerCellNo = model.getPersoninfo().getCellNumber()+"";

		String customerName = model.getPersoninfo().getFullName();
		if(!customerName.equals("") && !customerCellNo.equals("")){
			AppUtility.loadContactPersonlist(smspopup.olbContactlistEmailId,"Customer",model.getPersoninfo().getCount(),customerName,customerCellNo,null,true);
		}
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(smspopup.oblsmsTemplate,LoginPresenter.currentModule.trim(),screenName,true,AppConstants.SMS);
		smspopup.getRbSMS().setValue(true);
		smspopup.smodel = model;
		form.hideWaitSymbol();
	}

	private void reactonViewContract(String conIdExistingOrNew) {

		Console.log("contract id parameter="+conIdExistingOrNew); 
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		if(conIdExistingOrNew.equals("New"))
			filter.setIntValue(model.getContrtactId());
		else if(conIdExistingOrNew.equals("Existing"))
			filter.setIntValue(model.getExistingContractId());
		else
			return;
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Contract());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No Contract document found.");
					return;
				}
					final Contract contract=(Contract) result.get(0);
					final ContractForm form=ContractPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(contract);
							form.setToViewState();
							
						}
					};
					timer.schedule(1000);
				
			}
		});
		
	
	
		
	}
	
	protected void updateStatusToCompleted(Complain complainObj){
		Console.log("EVA in updateStatusToCompleted status="+complainObj.getCompStatus());
		String mainScreenLabel="Customer Support Details";
		if(complainObj!=null&&complainObj.getCount()!=0){
		
			mainScreenLabel=complainObj.getCount()+" "+"/"+" "+AppUtility.parseDate(complainObj.getDate())+" "+"/"+" "+complainObj.getCompStatus();
		}
	 form.fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
	}
}
