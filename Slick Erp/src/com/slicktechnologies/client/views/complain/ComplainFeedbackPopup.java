package com.slicktechnologies.client.views.complain;


import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;


public class ComplainFeedbackPopup extends AbsolutePanel{

	Button btnCancel, btnOk;
	public ListBox lbRating;
	
	public ComplainFeedbackPopup() {

		btnCancel = new Button("Cancel");
		btnOk = new Button("Ok");
		
		//    rohan added service completion date for back dated records
		
		InlineLabel remark1 = new InlineLabel("Service Mark Complete");
		add(remark1, 60, 10);
		remark1.getElement().getStyle().setFontSize(20, Unit.PX);

		
		InlineLabel rating = new InlineLabel("Customer Feedback");
		add(rating, 10, 110);
		lbRating = new ListBox();
		lbRating.addItem("--SELECT--");
		lbRating.addItem("Excellent");
		lbRating.addItem("Very Good");
		lbRating.addItem("Good");
		lbRating.addItem("Satisfactory");
		lbRating.addItem("Poor");

		
		//************already commented part*********************
//		lbRating.addItem("* * * * *");
//		lbRating.addItem("* * * *");
//		lbRating.addItem("* * *");
//		lbRating.addItem("* *");
//		lbRating.addItem("*");
		
		
		add(lbRating, 10, 140);
		
		add(btnOk, 80, 250);
		add(btnCancel, 180, 250);
		setSize("320px", "300px");
		this.getElement().setId("form");
	}

	public void clear(){
		lbRating.setSelectedIndex(0);
	}
		

//		****************getter and setters*****************
		
		public Button getBtnCancel() {
			return btnCancel;
		}


		public void setBtnCancel(Button btnCancel) {
			this.btnCancel = btnCancel;
		}


		public Button getBtnOk() {
			return btnOk;
		}


		public void setBtnOk(Button btnOk) {
			this.btnOk = btnOk;
		}



		public ListBox getLbRating() {
			return lbRating;
		}


		public void setLbRating(ListBox lbRating) {
			this.lbRating = lbRating;
		}
	
	
	
}
