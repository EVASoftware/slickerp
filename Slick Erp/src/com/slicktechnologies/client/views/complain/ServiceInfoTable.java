package com.slicktechnologies.client.views.complain;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;

public class ServiceInfoTable extends SuperTable<Service> {

	TextColumn<Service> getColumnSrNo;
	TextColumn<Service> getColumnProductName;
	TextColumn<Service> getColumnServiceDate;
	TextColumn<Service> getColumnServiceType;
	TextColumn<Service> getColumnServiceTime;
	TextColumn<Service> getColumnStatus;
	TextColumn<Service> getColumnTechnician;
	TextColumn<Service> getColumnServiceId;
	TextColumn<Service> getColumnDescription;
	
	/**
	 * @author Anil
	 * @since 03-06-2020
	 * Adding service completion date labeled as performed date
	 */
	TextColumn<Service> getColumnSevicePerformedDate;
	String serviceDateLbl="Service Date";
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		
		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain","ComplainServiceWithTurnAroundTime")) {
			serviceDateLbl="Scheduled Date";
			addColumnserviceSrNo();
			addColumnserviceId();
			addColumnServiceDate();
			getColumnSevicePerformedDate();
			addColumnProductName();
			addColumnTechnician();
			addColumnServiceType();
			addColumnStatus();
//			addColumnDescription();
		}else{
			addColumnserviceSrNo();
			addColumnserviceId();
			addColumnServiceDate();
			addColumnServiceTime();
			addColumnProductName();
			addColumnTechnician();
			addColumnServiceType();
			addColumnStatus();
			addColumnDescription();
		}
	}

	private void getColumnSevicePerformedDate() {
		// TODO Auto-generated method stub
		getColumnSevicePerformedDate = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				if (object.getServiceCompletionDate() != null) {
					String serDate = AppUtility.parseDate(object.getServiceCompletionDate());
					return serDate;
				}
				return "";
			}
		};
		table.addColumn(getColumnSevicePerformedDate, "Performed Date");
		table.setColumnWidth(getColumnSevicePerformedDate, 120, Unit.PX);
		getColumnSevicePerformedDate.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	public void addColumnTechnician() {
		getColumnTechnician = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getEmployee();
			}
		};

		table.addColumn(getColumnTechnician, "Service Engineer");
		table.setColumnWidth(getColumnTechnician, 120, Unit.PX);
		getColumnTechnician.setSortable(true);
	}
	private void addColumnserviceSrNo() {

		getColumnSrNo = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getServiceSerialNo() + "";
			}
		};

		table.addColumn(getColumnSrNo, "Sr No");
		table.setColumnWidth(getColumnSrNo, 80, Unit.PX);
		getColumnSrNo.setSortable(true);
	}
	
	private void addColumnserviceId() {

		getColumnServiceId = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getCount()+"";
			}
		};

		table.addColumn(getColumnServiceId, "Service Id");
		table.setColumnWidth(getColumnServiceId, 120, Unit.PX);
		getColumnServiceId.setSortable(true);
	}

	public void addColumnStatus() {
		getColumnStatus = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getStatus();
			}
		};

		table.addColumn(getColumnStatus, "Status");
		table.setColumnWidth(getColumnStatus, 120, Unit.PX);
		getColumnStatus.setSortable(true);
	}
	public void addColumnServiceType() {
		getColumnServiceType = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getServiceType();
			}
		};

		table.addColumn(getColumnServiceType, "Service Type");
		table.setColumnWidth(getColumnServiceType, 120, Unit.PX);
		getColumnServiceType.setSortable(true);
	}
	
	public void addColumnProductName() {
		getColumnProductName = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getProductName();
			}
		};

		table.addColumn(getColumnProductName, "Product Name");
		table.setColumnWidth(getColumnProductName, 120, Unit.PX);
		getColumnProductName.setSortable(true);
	}

	private void addColumnServiceTime() {

		getColumnServiceTime = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				return object.getServiceTime();
			}
		};

		table.addColumn(getColumnServiceTime, "Service Time");
		table.setColumnWidth(getColumnServiceTime, 120, Unit.PX);
		getColumnServiceTime.setSortable(true);

	}
	public void addColumnServiceDate() {
				getColumnServiceDate = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				if (object.getServiceDate() != null) {
					String serDate = AppUtility.parseDate(object
							.getServiceDate());
					return serDate;
				}
				return "";
			}
		};
		
		table.addColumn(getColumnServiceDate, serviceDateLbl);
		table.setColumnWidth(getColumnServiceDate, 120, Unit.PX);
		getColumnServiceDate.setSortable(true);
	}
	public void addColumnDescription() {
		getColumnDescription = new TextColumn<Service>() {

			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getDescription();
			}
		};

		table.addColumn(getColumnDescription, "Description");
		table.setColumnWidth(getColumnDescription, 120, Unit.PX);
		getColumnDescription.setSortable(true);
	}
	public void addSerialNoSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(getColumnSrNo, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceSerialNo() == e2.getServiceSerialNo()) {
						return 0;
					}
					if (e1.getServiceSerialNo() > e2.getServiceSerialNo()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
   public void addServiceIdSorting(){
	   List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(getColumnServiceId, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
  }
   public void addServiceDateSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(getColumnServiceDate, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceDate() != null
							&& e2.getServiceDate() != null) {
						return e1.getServiceDate().compareTo(
								e2.getServiceDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
   private void addServiceTimeSorting() {

		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(getColumnServiceTime, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceTime() != null
							&& e2.getServiceTime() != null) {
						return e1.getServiceTime().compareTo(
								e2.getServiceTime());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
   public void addStatusSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(getColumnStatus, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null && e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
   public void addProductSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(getColumnProductName, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getProductName() != null
							&& e2.getProductName() != null) {
						return e1.getProductName().compareTo(
								e2.getProductName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

   private void addServiceTypeSorting(){
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(getColumnServiceType, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getServiceType() != null && e2.getServiceType() != null) {
						return e1.getServiceType().compareTo(e2.getServiceType());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
   public void addTechnicianSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(getColumnTechnician, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployee() != null && e2.getEmployee() != null) {
						return e1.getEmployee().compareTo(e2.getEmployee());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
   public void addDescriptionSorting() {
		List<Service> list = getDataprovider().getList();
		columnSort = new ListHandler<Service>(list);
		columnSort.setComparator(getColumnProductName, new Comparator<Service>() {

			@Override
			public int compare(Service e1, Service e2) {
				if (e1 != null && e2 != null) {
					if (e1.getDescription() != null
							&& e2.getDescription() != null) {
						return e1.getDescription().compareTo(
								e2.getDescription());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

@Override
public void addColumnSorting() {
	// TODO Auto-generated method stub
	addSerialNoSorting();
	addServiceIdSorting();
	addServiceDateSorting();
	addServiceTimeSorting();
	addProductSorting();
	addTechnicianSorting();
    addServiceTypeSorting();
	addStatusSorting();
	addDescriptionSorting();

	}

}
