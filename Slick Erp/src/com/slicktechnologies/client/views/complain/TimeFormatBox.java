package com.slicktechnologies.client.views.complain;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;

public class TimeFormatBox {
	
	ListBox lbMinutes;
	ListBox lbHours;
	
	
	public TimeFormatBox() {
		// TODO Auto-generated constructor stub
		initializeWidget();
	}
	
	private void initializeWidget() {
		lbHours=new ListBox();
		lbHours.setSize("50px", "24px");
		lbHours.insertItem("00", 0);
		lbHours.insertItem("01", 1);
		lbHours.insertItem("02", 2);
		lbHours.insertItem("03", 3);
		lbHours.insertItem("04", 4);
		lbHours.insertItem("05", 5);
		lbHours.insertItem("06", 6);
		lbHours.insertItem("07", 7);
		lbHours.insertItem("08", 8);
		lbHours.insertItem("09", 9);
		lbHours.insertItem("10", 10);
		lbHours.insertItem("11", 11);
		lbHours.insertItem("12", 12);
		lbHours.insertItem("13", 13);
		lbHours.insertItem("14", 14);
		lbHours.insertItem("15", 15);
		lbHours.insertItem("16", 16);
		lbHours.insertItem("17", 17);
		lbHours.insertItem("18", 18);
		lbHours.insertItem("19", 19);
		lbHours.insertItem("20", 20);
		lbHours.insertItem("21", 21);
		lbHours.insertItem("22", 22);
		lbHours.insertItem("23", 23);
		
		
		lbMinutes=new ListBox();
		lbMinutes.setSize("50px", "24px");
//		followupmin.insertItem("00", 0);
//		followupmin.insertItem("15", 1);
//		followupmin.insertItem("30", 2);
//		followupmin.insertItem("45", 3);
		
		lbMinutes.insertItem("00", 0);
		lbMinutes.insertItem("01", 1);
		lbMinutes.insertItem("02", 2);
		lbMinutes.insertItem("03", 3);
		lbMinutes.insertItem("04", 4);
		lbMinutes.insertItem("05", 5);
		lbMinutes.insertItem("06", 6);
		lbMinutes.insertItem("07", 7);
		lbMinutes.insertItem("08", 8);
		lbMinutes.insertItem("09", 9);
		lbMinutes.insertItem("10", 10);
		lbMinutes.insertItem("11", 11);
		lbMinutes.insertItem("12", 12);
		lbMinutes.insertItem("13", 13);
		lbMinutes.insertItem("14", 14);
		lbMinutes.insertItem("15", 15);
		lbMinutes.insertItem("16", 16);
		lbMinutes.insertItem("17", 17);
		lbMinutes.insertItem("18", 18);
		lbMinutes.insertItem("19", 19);
		lbMinutes.insertItem("20", 20);
		lbMinutes.insertItem("21", 21);
		lbMinutes.insertItem("22", 22);
		lbMinutes.insertItem("23", 23);
		lbMinutes.insertItem("24", 24);
		lbMinutes.insertItem("25", 25);
		lbMinutes.insertItem("26", 26);
		lbMinutes.insertItem("27", 27);
		lbMinutes.insertItem("28", 28);
		lbMinutes.insertItem("29", 29);
		lbMinutes.insertItem("30", 30);
		lbMinutes.insertItem("31", 31);
		lbMinutes.insertItem("32", 32);
		lbMinutes.insertItem("33", 33);
		lbMinutes.insertItem("34", 34);
		lbMinutes.insertItem("35", 35);
		lbMinutes.insertItem("36", 36);
		lbMinutes.insertItem("37", 37);
		lbMinutes.insertItem("38", 38);
		lbMinutes.insertItem("39", 39);
		lbMinutes.insertItem("40", 40);
		lbMinutes.insertItem("41", 41);
		lbMinutes.insertItem("42", 42);
		lbMinutes.insertItem("43", 43);
		lbMinutes.insertItem("44", 44);
		lbMinutes.insertItem("45", 45);
		lbMinutes.insertItem("46", 46);
		lbMinutes.insertItem("47", 47);
		lbMinutes.insertItem("48", 48);
		lbMinutes.insertItem("49", 49);
		lbMinutes.insertItem("50", 50);
		lbMinutes.insertItem("51", 51);
		lbMinutes.insertItem("52", 52);
		lbMinutes.insertItem("53", 53);
		lbMinutes.insertItem("54", 54);
		lbMinutes.insertItem("55", 55);
		lbMinutes.insertItem("56", 56);
		lbMinutes.insertItem("57", 57);
		lbMinutes.insertItem("58", 58);
		lbMinutes.insertItem("59", 59);
		
	
	}
	
	
	public HorizontalPanel getTimePanel() {
		HorizontalPanel hPanel=new HorizontalPanel();
		InlineLabel colLbl=new InlineLabel();
		colLbl.setText(":");
		hPanel.add(lbHours);
		hPanel.add(colLbl);
		hPanel.add(lbMinutes);
		return hPanel;
	}
	
	public String getValue() {
		String followptime="";
//		if(lbHours.getSelectedIndex()!=0 || lbMinutes.getSelectedIndex()!=0){
//			if(lbHours.getSelectedIndex()!=0){
				followptime = lbHours.getValue(lbHours.getSelectedIndex());
//			}
//			if(lbMinutes.getSelectedIndex()!=0){
				followptime = followptime +":"+lbMinutes.getValue(lbMinutes.getSelectedIndex());
//			}
//		}
		return followptime;
	}
	
	
	public void setValue(String followUpTime) {
		if(followUpTime==null||followUpTime.equals("")){
			lbHours.setSelectedIndex(0);
			lbMinutes.setSelectedIndex(0);
			return;
		}
		try {
			String[] time = getfollowuptime(followUpTime);
			String hrs = time[0];
			String min = time[1];
			
			if(hrs.length()<2&&Integer.parseInt(hrs)<=9) {
				hrs=0+hrs;
			}
			if(min.length()<2&&Integer.parseInt(min)<=9) {
				min=0+min;
			}
			/**
			 * @author Anil
			 * @since 15-05-2020
			 */
			if(Integer.parseInt(hrs)>23){
				lbHours.insertItem(hrs, lbHours.getItemCount()+1);
			}
			int count = lbHours.getItemCount();
			for (int i = 0; i < count; i++) {
				System.out.println("HTRRSSS ==" + lbHours.getItemText(i).trim());
				System.out.println(hrs.trim().equals(lbHours.getItemText(i).trim()));
				if (hrs.trim().equals(lbHours.getItemText(i).trim())) {
					lbHours.setSelectedIndex(i);
					break;
				}
			}
			

			int count1 = lbMinutes.getItemCount();
			for (int i = 0; i < count1; i++) {
				if (min.trim().equals(lbMinutes.getItemText(i).trim())) {
					lbMinutes.setSelectedIndex(i);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

	}
	private String[] getfollowuptime(String followUpTime) {
		 String arr[] =followUpTime.split(":");
		return arr;
	}

	public void setEnabled(boolean status) {
		lbHours.setEnabled(status);
		lbMinutes.setEnabled(status);
	}
	
	public void clear(){
		lbHours.setSelectedIndex(0);
		lbMinutes.setSelectedIndex(0);
	}

	
}
