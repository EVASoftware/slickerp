package com.slicktechnologies.client.views.complain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.humanresource.timereport.TimeReportPresenter;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.ComponentDetails;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.complain.ComplainList;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
/**
 * FormTablescreen template.
 */
public class ComplainForm extends FormScreen<Complain> implements ClickHandler, ChangeHandler, ValueChangeHandler<Date> {

	//Token to add the varialble declarations
	final GenricServiceAsync async =GWT.create(GenricService.class);
	
//	ComplainPresenter presenter;
//	TextBox tbfristname,tblastname;
//	MyLongBox lbcell;
//	EmailTextBox etbemail;
	
	
	Button newCustomer;
	
//	Label label;
	
	PersonInfoComposite pic;
	
	TextBox tbcount;
	DateBox dbDate;
	TextBox tbtime;
	TextBox tbTitle;
	
	Button btnsettime;
	
//	ListBox contractCount;
	
	//  rohan make 
//	ListBox lbServiceCount;
	
	// 
	ObjectListBox<Service> olbServiceDetails;
	
	TextBox tbServiceEngineer;
	
	TextBox tbQuatationId;
	TextBox tbContractId;
	TextBox tbServiceId;
	
	DateBox serviceDate;
	TextBox tbservicetime;
	
	ObjectListBox<Config> olbSatatus;
	ObjectListBox<Employee> olbeSalesPerson,olbassignto;
	ObjectListBox<Branch> olbbBranch;
	ObjectListBox<Config> olbcPriority;
	
	DateBox dbDueDate;
	CheckBox cbbillable;
	
	TextBox tbBrandName,tbModel,tbSerialNumber;
	TextArea taDescription,taRemark;
	/**Date 17-4-2020 by Amol**/
	TextArea taServAddress;
	ProductInfoComposite prodInfoComposite;
	ObjectListBox<Config> olbProductGroup; //Ashwini Patil Date:1-10-2024 for pecopp's trend chart
	
	ProductInfo pinfo;//18-1-2024
	Button btnadd;
	ActivityTable activitytable;
	
	Button clrCust;
	FormField fgroupingCustomerInformation ;
	TextBox tbCallerName;
	PhoneNumberBox pnbCallerCellNo;
	
	DateBox dbcompaintDate;
	
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
//	ObjectListBox<Config> olbActionTaken,olbSatatus;
//	TextBox tbtarget;
//	IntegerBox ibActivityId;
	
	SetTimePopUp settime=new SetTimePopUp();
	PopupPanel panel;
	/** date 19.4.2018 added by komal for creating new service(pestmorterm) **/
	DateBox dbNewServiceDate;
	
	private ListBox p_servicehours;
	private ListBox p_servicemin;
	private ListBox p_ampm;
	HorizontalPanel hPanel;
	VerticalPanel vPanel1;
	VerticalPanel vPanel2;
	VerticalPanel vPanel3;
	
	//***********vaishnavi*******************
	public static boolean forJobReportcumReceiptFlag=false;
	
	TextBox tbTicketId;
	DoubleBox dbNetPayable;
	DoubleBox dbAmountReceived;
	DoubleBox dbBalance;
	TextBox tbCustomerFeedback;
	DoubleBox dbExtraCharges;
	/** date 13.03.2018 added by komal for nbhc**/
	EmailTextBox etbEmail ;
	/** end komal **/
	/** date 20.4.2018 added by komal for product dropdown **/
	ObjectListBox<SalesLineItem> olbProductDetails;
	ServiceInfoTable serviceInfoTable;
	/**
	 * end komal
	 */
	
	Complain complainObj;
	
	
	/**
	 * nidhi
	 * for map model and serial number
	 * 
	 */
	TextBox tbProSerialNo ;

	/**
	 * Date 15-10-2018 By Vijay
	 * Des :- For compalin Tickte Category which is same to 
	 * Raise ticket Category from Support screen
	 */
	ObjectListBox<ConfigCategory> olbTicketCategory;
	DoubleBox dbEfforts;
	DoubleBox dbEarnedHours;
	DateBox dbPlanFromDate,dbPlanToDate;
	
	/**
	 * @author Anil , Date : 19-09-2019
	 * Added customer branch drop down list for hvac
	 * raised by Rohan
	 */
	ObjectListBox<CustomerBranchDetails> oblCustomerBranch;
	
	/**
	 * @author Anil
	 * @since 03-02-2020
	 * Adding following fields to calculate actual turn around time for premium tech
	 */
	DateBox dbExpectedCompletionDate;
	TimeFormatBox tmbExpectedCompletionTime;
	
	DateBox dbAssignedDate;
	TimeFormatBox tmbAssignedTime;
	
	DateBox dbActualCompletionDate;
	TimeFormatBox tmbActualCompletionTime;
		
	TimeFormatBox tmbRegistrationTime;
	TimeFormatBox tmbServiceTime;
	
	TimeFormatBox tmbTatSla;
	TimeFormatBox tmbTatActual;
	TimeFormatBox tmbTatTechnician;
	TimeFormatBox tmbTatSupervisor;
	
	/**
	 * End
	 */
	
	/**
	 * @author Anil
	 * @since 01-06-2020
	 */
	Address custAddress;
	String custSla;
	
	
	/**
	 * @author Anil
	 * @since 22-06-2020
	 * Adding asset and component details on complain for for UPS ind.
	 */
	ObjectListBox<ComponentDetails> lbAssetId;
	TextBox tbMfgNum;
	TextBox tbAssetUnit;
	Date mfgDate;
	Date replacementDate;
	String componentName;
	
	
	ObjectListBox<Contract> contractCount;
	boolean complainTatFlag=false;
	List<CustomerBranchDetails> customerBranchList=new ArrayList<CustomerBranchDetails>();
	
	/**
	 * @author Vijay Date :- 15-12-2021
	 */
	UploadComposite uploaddocument;
	ObjectListBox<Config> olbservicetype;

	public  ComplainForm() {
		super();
		createGui();
		
		settime.getBtnOk().addClickHandler(this);
		settime.getBtnCancel().addClickHandler(this);
		contractCount.setEnabled(false);
		
		cbbillable.addClickHandler(this);
		tbCustomerFeedback.setEnabled(false);
		taServAddress.setEnabled(false);
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain","NewCustomerButtonSetEnable")){
			newCustomer.setEnabled(false);
		}
		/**
		 * @author Anil,Date : 02-01-2019
		 * as creation date is only mapping at first ,
		 * after click on New button forms reinitialize but creation date is not getting mapped
		 */
		dbDate.setValue(new Date());
		dbDate.setEnabled(false);
		tmbRegistrationTime.setValue(getCreationTime(new Date()));
		
		dbNewServiceDate.setValue(new Date());
		tmbServiceTime.setValue(getCreationTime(new Date()));
		dbAssignedDate.setValue(new Date());
		tmbAssignedTime.setValue(getCreationTime(new Date()));
		
		dbcompaintDate.setValue(new Date());
		
		
		
		tbtime.setValue(getCreationTime(new Date()));
		dbExpectedCompletionDate.setEnabled(false);
		tmbExpectedCompletionTime.setEnabled(false);
		dbActualCompletionDate.setEnabled(false);
		tmbActualCompletionTime.setEnabled(false);
		tmbTatSla.setEnabled(false);
		tmbTatActual.setEnabled(false);
		tmbTatTechnician.setEnabled(false);
		tmbTatSupervisor.setEnabled(false);
		
		tbAssetUnit.setEnabled(false);
		tbMfgNum.setEnabled(false);
		
		complainTatFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain","ComplainServiceWithTurnAroundTime");
		if (complainTatFlag) {
			Timer timer=new Timer() {
				@Override
				public void run() {
//					setDefaultDataOfLoggedInUser();
					olbSatatus.setValue("Created");
				}
			};
			timer.schedule(2800);
			olbSatatus.setEnabled(false);
			if(UserConfiguration.getRole()!=null&&
					(UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")||
							UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator"))){
				dbDate.setEnabled(true);
				tmbRegistrationTime.setEnabled(true);
			}
			
		}
	}


	private void initalizeWidget()
	{
//		tbfristname=new TextBox();
//		tblastname=new TextBox();
//		lbcell=new MyLongBox();
//		etbemail=new EmailTextBox();
		
//		label=new Label("OR");
		
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.setQuerryObject(new Customer());
//		pic = new PersonInfoComposite(querry, false);
		
		pic=AppUtility.customerInfoComposite(new Customer());

		tbcount=new TextBox();
		tbcount.setEnabled(false);
		
		dbDate=new DateBoxWithYearSelector();
		dbDate.setValue(new Date());
		dbDate.setEnabled(false);
		
		tbTitle= new TextBox();
		
		tbtime=new TextBox();
		tbtime.setEnabled(false);
		
		btnsettime=new Button("Set");
		btnsettime.addClickHandler(this);
		
		contractCount= new ObjectListBox<Contract>();
//		contractCount= new ListBox();
		contractCount.addItem("--SELECT--");
		contractCount.setEnabled(false);
		
//		lbServiceCount=new ListBox();
//		lbServiceCount.addItem("--SELECT--");
//		lbServiceCount.setEnabled(false);
		
		olbServiceDetails = new ObjectListBox<Service>();
		olbServiceDetails.addItem("--SELECT--");
		olbServiceDetails.setEnabled(false);
		
		tbServiceEngineer=new TextBox();
		tbServiceEngineer.setEnabled(false);
		
		olbSatatus=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbSatatus, Screen.COMPLAINSTATUS);
		
		olbbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbBranch);
		
		olbcPriority=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcPriority,Screen.COMPLAINPRIORITY);
		
		olbeSalesPerson=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
		olbeSalesPerson.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERSUPPORT, "Sales Person");
		
		olbassignto=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbassignto);
		olbassignto.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERSUPPORT, "Assigned To");
		
//		serviceDate=new DateBox();
		serviceDate = new DateBoxWithYearSelector();
		serviceDate.setEnabled(false);
		tbservicetime=new TextBox();
		tbservicetime.setEnabled(false);
		cbbillable=new CheckBox();
		
		dbDueDate=new DateBoxWithYearSelector();
		
		tbQuatationId=new TextBox();
		tbContractId=new TextBox();
		tbServiceId=new TextBox();
		
		tbQuatationId.setEnabled(false);
		tbContractId.setEnabled(false);
		tbServiceId.setEnabled(false);
		
		taDescription=new TextArea();
		taServAddress=new TextArea();
		tbBrandName=new TextBox();
		tbModel=new TextBox();
		tbSerialNumber=new TextBox();
		taRemark=new TextArea();
		
		prodInfoComposite=AppUtility.initiatePurchaseProductComposite(new SuperProduct());

		btnadd=new Button("Add");
		btnadd.addClickHandler(this);
		
		activitytable=new ActivityTable();
		activitytable.connectToLocal();
		
		clrCust=new Button("Clear");
		clrCust.addClickHandler(this);
//		tbtarget=new TextBox();
//		ibActivityId=new IntegerBox();
//		olbActionTaken=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbActionTaken, Screen.ACTIONTAKEN);
		
		newCustomer=new Button("New Customer");
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain","NewCustomerButtonSetEnable"))
		{
			newCustomer.setEnabled(false);
		}
		
		
		tbCallerName=new TextBox();
		pnbCallerCellNo=new PhoneNumberBox();
		
		dbcompaintDate = new DateBoxWithYearSelector();
		dbcompaintDate.setValue(new Date()); 
		/**
		 * Date 27-07-2018 By Vijay on value change added, for Orion pest customization
		 */
		dbcompaintDate.addValueChangeHandler(this);
		setDueDate(new Date());
		/**
		 * ends here
		 */
		
		tbTicketId = new TextBox();
		tbTicketId.setEnabled(false);
		
		dbNetPayable = new DoubleBox();
		dbNetPayable.setEnabled(false);
		
		dbAmountReceived = new DoubleBox();
		dbAmountReceived.setEnabled(false);
		
		dbBalance = new DoubleBox();
		dbBalance.setEnabled(false);
		
		tbCustomerFeedback=new TextBox();
		tbCustomerFeedback.setEnabled(false);
	
		dbExtraCharges=new DoubleBox();
		dbExtraCharges.setEnabled(true);
		
		/** date 13.03.2018 added by komal for nbhc**/
		etbEmail = new EmailTextBox() ;
		/** end komal **/
		/** date 19.4.2018 added by komal for creating new service(pestmorterm) **/
		dbNewServiceDate = new DateBoxWithYearSelector();
		olbProductDetails = new ObjectListBox<SalesLineItem>();
		olbProductDetails.addItem("--SELECT--");
		olbProductDetails.setEnabled(false);
		//komal
		serviceInfoTable = new ServiceInfoTable();
		serviceInfoTable.connectToLocal();		
				
		p_servicehours=new ListBox();
		p_servicehours.insertItem("--", 0);
		p_servicehours.insertItem("01", 1);
		p_servicehours.insertItem("02", 2);
		p_servicehours.insertItem("03", 3);
		p_servicehours.insertItem("04", 4);
		p_servicehours.insertItem("05", 5);
		p_servicehours.insertItem("06", 6);
		p_servicehours.insertItem("07", 7);
		p_servicehours.insertItem("08", 8);
		p_servicehours.insertItem("09", 9);
		p_servicehours.insertItem("10", 10);
		p_servicehours.insertItem("11", 11);
		p_servicehours.insertItem("12", 12);
		
		p_servicemin=new ListBox();
		
		p_servicemin.insertItem("--", 0);
		p_servicemin.insertItem("00", 1);
		p_servicemin.insertItem("15", 2);
		p_servicemin.insertItem("30", 3);
		p_servicemin.insertItem("45", 4);
		
		
		
		
		p_ampm = new ListBox();
		p_ampm.insertItem("--", 0);
		p_ampm.insertItem("AM", 1);
		p_ampm.insertItem("PM", 2);
		hPanel = new HorizontalPanel();
		vPanel1 = new VerticalPanel();
		vPanel2 = new VerticalPanel();
		vPanel3 = new VerticalPanel();
		InlineLabel lbl1 = new InlineLabel("HH :");
		//lbl1.getElement().addClassName("verticalLable");
		vPanel1.add(lbl1);
		p_servicehours.getElement().addClassName("verticalSelect");
		vPanel1.add(p_servicehours);
		vPanel1.getElement().addClassName("verticalPanel");
		hPanel.add(vPanel1);
		
		InlineLabel lbl2 = new InlineLabel("MM : ");
		//lbl2.getElement().addClassName("verticalLable");
		vPanel2.add(lbl2);
		p_servicemin.getElement().addClassName("verticalSelect");
		vPanel2.add(p_servicemin);
		vPanel2.getElement().addClassName("verticalPanel");
        hPanel.add(vPanel2);
        
        InlineLabel lbl3 = new InlineLabel("AM/PM");
        //lbl3.getElement().addClassName("verticalLable");
		vPanel3.add(lbl3);
		p_ampm.getElement().addClassName("verticalSelect");
		vPanel3.add(p_ampm);
		vPanel3.getElement().addClassName("verticalPanel");
        hPanel.add(vPanel3);
        hPanel.getElement().addClassName("horizontalPanel");
        /** end **/
        
        /**
         * nidhi
         * 9-08-2018
         * for map serail number and  model number
         */
        tbProSerialNo = new TextBox();
        
    	olbTicketCategory = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbTicketCategory, Screen.TICKETCATEGORY);
		
		dbEfforts = new DoubleBox();
		dbEarnedHours = new DoubleBox();
		dbPlanFromDate = new DateBoxWithYearSelector();
		dbPlanToDate = new DateBoxWithYearSelector();
		
		oblCustomerBranch=new ObjectListBox<CustomerBranchDetails>();
		oblCustomerBranch.addItem("--SELECT--");
		
		dbNewServiceDate.setValue(new Date());
		tbtime.setValue(getCreationTime(new Date()));
		
		dbExpectedCompletionDate=new DateBoxWithYearSelector();
		tmbExpectedCompletionTime=new TimeFormatBox();
		
		dbAssignedDate=new DateBoxWithYearSelector();
		tmbAssignedTime=new TimeFormatBox();
		
		dbAssignedDate.setValue(new Date());
		tmbAssignedTime.setValue(getCreationTime(new Date()));
		
		dbActualCompletionDate=new DateBoxWithYearSelector();
		tmbActualCompletionTime=new TimeFormatBox();
				
		tmbTatSla=new TimeFormatBox();
		tmbTatActual=new TimeFormatBox();
		tmbTatTechnician=new TimeFormatBox();
		tmbTatSupervisor=new TimeFormatBox();
		
		dbExpectedCompletionDate.setEnabled(false);
		tmbExpectedCompletionTime.setEnabled(false);
		dbActualCompletionDate.setEnabled(false);
		tmbActualCompletionTime.setEnabled(false);
		tmbTatSla.setEnabled(false);
		tmbTatActual.setEnabled(false);
		tmbTatTechnician.setEnabled(false);
		tmbTatSupervisor.setEnabled(false);
		
        tmbRegistrationTime=new TimeFormatBox();
        tmbRegistrationTime.setValue(getCreationTime(new Date()));
        tmbServiceTime=new TimeFormatBox();
        tmbServiceTime.setValue(getCreationTime(new Date()));
        
        lbAssetId=new ObjectListBox<ComponentDetails>();
		lbAssetId.addItem("--SELECT--");
		tbMfgNum=new TextBox();
		tbAssetUnit=new TextBox();
		
		tbAssetUnit.setEnabled(false);
		tbMfgNum.setEnabled(false);
        
        if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain",
				"ComplainServiceWithTurnAroundTime")) {
			olbSatatus.setValue("Created");
			olbSatatus.setEnabled(false);
			dbDate.setEnabled(true);
			tmbRegistrationTime.setEnabled(true);
			
			dbDate.addValueChangeHandler(this);
			tmbRegistrationTime.lbHours.addChangeHandler(this);
			tmbRegistrationTime.lbMinutes.addChangeHandler(this);
			
			dbActualCompletionDate.addValueChangeHandler(this);
			dbNewServiceDate.addValueChangeHandler(this);
			dbAssignedDate.addValueChangeHandler(this);
						
			tmbActualCompletionTime.lbHours.addChangeHandler(this);
			tmbActualCompletionTime.lbMinutes.addChangeHandler(this);
			
			tmbServiceTime.lbHours.addChangeHandler(this);
			tmbServiceTime.lbMinutes.addChangeHandler(this);
			
			tmbAssignedTime.lbHours.addChangeHandler(this);
			tmbAssignedTime.lbMinutes.addChangeHandler(this);
			
		}
        
        uploaddocument = new UploadComposite();
        
    	olbservicetype=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbservicetype, Screen.SERVICETYPE);
		
		olbProductGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbProductGroup, Screen.PRODUCTGROUP);
		
        
	}
	
	
	

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {
		initalizeWidget();

		if(forJobReportcumReceiptFlag==true){
			System.out.println("inside processlevelBarNames:::::::::::");
			processlevelBarNames=new String[]{"Mark Completed","New"};
		}
		/**
		 * @author Anil
		 * @since 11-05-2020
		 * Added view service button for premium tech
		 */
		else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "OnlyForEVA")){
			processlevelBarNames=new String[]{"Mark Completed","Create Quotation","Create Contract","Create Service","View Contact List","New","Report","StatusHistory","View Service","Cancel","View Contract"};		
		}
		else{
			processlevelBarNames=new String[]{"Mark Completed","Create Quotation","Create Contract","Create Service","View Contact List","New","View Service","Cancel","View Contract"};		
		}

		String mainScreenLabel="Customer Support Details";
		if(complainObj!=null&&complainObj.getCount()!=0){
			mainScreenLabel=complainObj.getCount()+" "+"/"+" "+AppUtility.parseDate(complainObj.getDate())+" "+"/"+" "+complainObj.getCompStatus();
		}
		
		//customerObject.getCount()+" "+"/"+" "+customerObject.getStatus()+" "+"/"+" "+AppUtility.parseDate(customerObject.getCreationDate());
		//fgroupingCustomerInformation.setLabel(mainScreenLabel);
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingCustomerInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(6).setKeyField(true).build();		
		
//		FormFieldBuilder fbuilder;
//		fbuilder = new FormFieldBuilder();
//		FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",pic);
		FormField fpic= fbuilder.setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",clrCust);
		FormField fclrCust= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingOfficeUse=fbuilder.setlabel("Ticket Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Ticket No.",tbcount);
		FormField ftbcountId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fdbDate=null;
		FormField btntime=null;
		/**
		 * @author Anil
		 * @since 30-05-2020
		 */
		boolean hideBillable=true;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime")) {
			/**
			 * @author Anil @since 16-09-2021
			 * Change label to Ticket date raised by Nitin Sir and Swati for divin
			 */
//			fbuilder = new FormFieldBuilder("Complaint Registration Date",dbDate);
			fbuilder = new FormFieldBuilder("Creation Date",dbDate);//old label "Ticket Date" 
			fdbDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Complaint Registration Time",tmbRegistrationTime.getTimePanel());
			btntime= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			hideBillable=false;
		}else {
//			fbuilder = new FormFieldBuilder("Creation Date",dbDate);
			fbuilder = new FormFieldBuilder("Creation Date",dbDate);//old label "Ticket Date"
			fdbDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Ticket Time",tbtime);
			btntime= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		
		fbuilder = new FormFieldBuilder("Set Time",btnsettime);
		FormField btnsettime= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Billable",cbbillable);
		FormField rbtn1= fbuilder.setMandatory(false).setRowSpan(0).setVisible(hideBillable).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingrefDet=fbuilder.setlabel("Reference Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("Existing Contract ID",contractCount);
		FormField fcontractCount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Details",olbServiceDetails);
		FormField flbServiceCount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Technician",tbServiceEngineer);
		FormField ftbServiceEngineer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Quotation ID",tbQuatationId);
		FormField ftbQuatationId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contract ID",tbContractId);
		FormField ftbContractId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service ID",tbServiceId);
		FormField ftbServiceId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**update
		 * Date : 07-11-2017 By ANIL
		 * Change label from Service Person to Person Responsible
		 */
		fbuilder = new FormFieldBuilder("* Person Responsible",olbeSalesPerson);
		FormField folbeSalesPerson= fbuilder.setMandatory(true).setMandatoryMsg("Person Responsible is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Branch",olbbBranch);
		FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Priority",olbcPriority);
		FormField folbcPriority= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * @author Anil @since 16-09-2021
		 * As per the requirement raised by divine through Nitin sir and Swati
		 * need to make this field non mandatory
		 */
		fbuilder = new FormFieldBuilder("New Technician",olbassignto);
		FormField fassinto= fbuilder.setMandatory(false).setMandatoryMsg("Assigned To is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("Brand Name", tbBrandName);
		FormField ftbBrandName=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("Model", tbModel);
		FormField ftbModel=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingProductinfo=fbuilder.setlabel("Product Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",prodInfoComposite);
		FormField fprodInfoComposite= fbuilder.setMandatory(true).setMandatoryMsg("Product is Mandatory").setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingActivityTable=fbuilder.setlabel("Complaint History").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",activitytable.getTable());
		FormField factivitytable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	
		/** date 06/03/2018 added by komal **/
		FormField ftaDescription;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain","MakeDescriptionMandatory")){
			fbuilder = new FormFieldBuilder("* Description",taDescription);
			ftaDescription= fbuilder.setMandatory(true).setMandatoryMsg("Description is Mandatory").setRowSpan(0).setColSpan(4).build();
		}else{
			fbuilder = new FormFieldBuilder("Description",taDescription);
			ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		}
		/**
		 * end komal
		 */
		
		fbuilder = new FormFieldBuilder("Service Address",taServAddress);
		FormField ftaServAddress= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		
		fbuilder = new FormFieldBuilder("Due Date",dbDueDate);
		FormField fdbDueDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Date",serviceDate);
		FormField serviceDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Time",tbservicetime);
		FormField serviceTime= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Ticket Status",olbSatatus);
		FormField complaintstatus= fbuilder.setMandatory(true).setMandatoryMsg("Complain status is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Remark",taRemark);
		FormField fdbremark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		

		fbuilder = new FormFieldBuilder("",btnadd);
		FormField fdbtnadd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",newCustomer );
		FormField fbtnnewCustomer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Complain Title",tbTitle);
		FormField ftbTitle= fbuilder.setMandatory(true).setMandatoryMsg("Complain Title is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		/** date 06/03/2018 added by komal for nbhc **/
		FormField ftbCallerName;
		FormField fpnbCallerCellNo;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain","MakeCallerNameAndCellMandatory")){
			fbuilder = new FormFieldBuilder("* Caller Name",tbCallerName );
			ftbCallerName= fbuilder.setMandatory(true).setMandatoryMsg("Caller Name is Mandatory").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("* Caller Cell No.",pnbCallerCellNo );
			fpnbCallerCellNo= fbuilder.setMandatory(true).setMandatoryMsg("Caller Cell No. is Mandatory").setRowSpan(0).setColSpan(0).build();
			
		}else{
			fbuilder = new FormFieldBuilder("Caller Name",tbCallerName );
			ftbCallerName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Caller Cell No.",pnbCallerCellNo );
			fpnbCallerCellNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
		}
		/**
		 * end komal
		 */
		/** date 13.03.2018 added by komal for nbhc**/
		FormField fcompaintDate;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "makeRemarkMandatory")){
			fbuilder = new FormFieldBuilder("* Reported Date",dbcompaintDate);//old label "Ticket Date"
			fcompaintDate= fbuilder.setMandatory(true).setMandatoryMsg("Ticket Date is Mandatory").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Reported Date",dbcompaintDate);//old label "Ticket Date"
			fcompaintDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/**
		 * end date
		 */
		
		
		fbuilder = new FormFieldBuilder("Ticket Id",tbTicketId);
		FormField ftbTicketId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Net Payable",dbNetPayable);
		FormField fdbNetPayable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Amount Received",dbAmountReceived);
		FormField fdbAmountReceived= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Balance Payment",dbBalance);
		FormField fdbBalance= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer Feedback",tbCustomerFeedback);
		FormField ftbCustomerFeedback= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Extra Charges",dbExtraCharges);
		FormField fdbExtraCharges= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 13.03.2018 added by komal for nbhc**/
		FormField fetbEmail;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "makeEmailMandatory")){
			fbuilder = new FormFieldBuilder("* Caller Email",etbEmail);
			fetbEmail= fbuilder.setMandatory(true).setMandatoryMsg("Caller Email is Mandatory").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Caller Email",etbEmail);
			fetbEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/**date 19.04.2018 added by komal for pestmorterm **/
		/**
		 * @author Abhinav Bihade
		 * @since 26/12/2019\
		 * As per Rahul Tiwari's Requirement,
		 * For Oolite : Make Service Date as mandatory in Complain Dashboard page
		 * this change will applicable to all  
		 */
		fbuilder = new FormFieldBuilder("* Service Date",dbNewServiceDate);
		FormField fdbNewServiceDate = fbuilder.setMandatory(true).setMandatoryMsg("Service Date is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		/** date 06/03/2018 added by komal **/
		fbuilder = new FormFieldBuilder("Product Name",olbProductDetails);
		FormField folbProductDetails = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", serviceInfoTable.getTable());
		FormField fserviceInfoTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",hPanel);
		FormField fTime = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/*** Date 16-10-2018 By Vijay for Complain Ticket Category with process config for Mandatory***/
		FormField fTicketolbCategory;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "MakeTicketCategoryMandatory")){
			System.out.println("Inside Process config");
			fbuilder = new FormFieldBuilder("* Ticket Category", olbTicketCategory);
			fTicketolbCategory = fbuilder.setMandatory(true).setMandatoryMsg("Ticket Category is mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			System.out.println("Else block no process config");
			fbuilder = new FormFieldBuilder("Ticket Category", olbTicketCategory);
			fTicketolbCategory = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}

		/*** Date 16-10-2018 By Vijay for Efforts for EVA ***/
		fbuilder = new FormFieldBuilder("Efforts(Hours)", dbEfforts);
		FormField fdbEfforts = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*** Date 16-10-2018 By Vijay for Earned Efforts for EVA ***/
		fbuilder = new FormFieldBuilder("Earned(Hours)", dbEarnedHours);
		FormField fdbEarnedHours = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**** Date 21-10-2018 By Vijay for planning from Date To Date ****/
		fbuilder = new FormFieldBuilder("Plan From Date",dbPlanFromDate);
		FormField fdbPlanFromDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Plan To Date",dbPlanToDate);
		FormField fdbPlanToDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer Branch",oblCustomerBranch);
		FormField flbCustomerBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fempty = fbuilder.setRowSpan(0).setColSpan(0).build();

		FormField[] field  = new FormField[4];
		field[0] = fcontractCount;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ProductwiseServicesInComplain")){
//			field[1] = folbProductDetails;
//			field[2] = fempty;
//			field[3] = fempty;
			field[1] = folbProductDetails;
			field[2] = flbCustomerBranch;
			field[3] = fempty;
		}else{
			field[1] = flbServiceCount;
			field[2] = ftbServiceEngineer;
			field[3] = ftbQuatationId;
		}
		
		FormField[] field1 = new FormField[4];
		FormField[] field2 = new FormField[1];
		field1[0] = fcompaintDate;
		field1[1] = rbtn1;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ProductwiseServicesInComplain")){
		field1[2] = fdbNewServiceDate;
		field1[3] = fTime;
		field2[0] = fserviceInfoTable;
		}else{
			field1[2] = fempty;
			field1[3] = fempty;
			field2[0] = fempty;
		}

		fbuilder = new FormFieldBuilder("Product Serial Number",tbProSerialNo);
		FormField ftbProSerialNo = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		fbuilder = new FormFieldBuilder("Expected Completion Date",dbExpectedCompletionDate);
		FormField fdbExpectedCompletionDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Expected Completion Time",tmbExpectedCompletionTime.getTimePanel());
		FormField ftmbExpectedCompletionTime = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Assigned Date",dbAssignedDate);
		FormField fdbAssignedDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Assigned Time",tmbAssignedTime.getTimePanel());
		FormField ftmbAssignedTime = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
				
		fbuilder = new FormFieldBuilder("Completion Date",dbActualCompletionDate);
		FormField fdbActualCompletionDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Completion Time",tmbActualCompletionTime.getTimePanel());
		FormField ftmbActualCompletionTime = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("TAT(SLA)",tmbTatSla.getTimePanel());
		FormField fdbTatSla = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("TAT(Actual)",tmbTatActual.getTimePanel());
		FormField fdbTatActual = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("TAT(Technician)",tmbTatTechnician.getTimePanel());
		FormField fdbTatTechnician = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("TAT(Supervisor)",tmbTatSupervisor.getTimePanel());
		FormField ftmbTatSupervisor = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Service Time",tmbServiceTime.getTimePanel());
		FormField ftmbServiceTime = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Asset Id",lbAssetId);
		FormField flbAssetId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Mfg No.",tbMfgNum);
		FormField ftbMfgNum = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Asset Unit",tbAssetUnit);
		FormField ftbAssetUnit = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime")){
			field[3] = flbAssetId;
		}
		
		 fbuilder = new FormFieldBuilder("Upload Document",uploaddocument);
	     FormField fuploaddocument= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	        
	     
	     fbuilder = new FormFieldBuilder("Service Type",olbservicetype);
	     FormField folbservicetype= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setMandatoryMsg("Service Type is Mandatory").build();
		
	     
		FormField folbProductGroup;
			
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "MakeProductGroupMandatory")){
			fbuilder = new FormFieldBuilder("* Product Group", olbProductGroup);
			folbProductGroup = fbuilder.setMandatory(true).setMandatoryMsg("Product Group is mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Product Group", olbProductGroup);
			folbProductGroup = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
	        
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		if(forJobReportcumReceiptFlag==true){
			FormField[][] formfield = {   /** date 13.03.2018 rearranged by komal after adding caller email **/
//					{fgroupingCustomerInformation},
//					{fbtnnewCustomer},
//					{fpic},
//					{fclrCust},
//					{fgroupingrefDet},
//					{fcontractCount, folbProductDetails },  //flbServiceCount,ftbServiceEngineer,ftbQuatationId},
//					{ftbContractId,ftbServiceId,serviceDate,serviceTime},
//					field2,
//					{fgroupingOfficeUse},
//					{ftbTicketId,fdbDate,btntime,ftbCallerName},
//					{fpnbCallerCellNo,fetbEmail ,complaintstatus,folbeSalesPerson},
//					{folbbBranch ,folbcPriority,fdbDueDate,fassinto},
//					{fcompaintDate ,rbtn1,fdbNetPayable,fdbAmountReceived},
//					{fdbBalance ,ftbCustomerFeedback,fdbExtraCharges ,fdbNewServiceDate},/** date 19.4.2018 added by komal **/
//					{fTime, fTicketolbCategory,fdbEfforts,fdbEarnedHours},/** date 24.4.2018 added by komal **/
//					{ftaDescription},
//					{fgroupingProductinfo},
//					{fprodInfoComposite},
//					{ftbBrandName,ftbModel},
//					{fdbremark,fdbtnadd},
//					{fgroupingActivityTable},
//					{factivitytable},
//					{ftaServAddress},
//					{fdbPlanFromDate,fdbPlanToDate},
					/** Cust support Details **/
					{fgroupingCustomerInformation},
					{fbtnnewCustomer,fpic,fclrCust},
					{ftbTicketId,fdbDate,complaintstatus,folbbBranch,folbeSalesPerson,fassinto},
					/**Refernce Details**/
					{fgroupingrefDet},
					{fcontractCount,folbProductDetails },  //flbServiceCount,ftbServiceEngineer,ftbQuatationId},
					
					/**
					 * @author Anil @since 16-09-2021
					 * removing contract id and service id for divine raised by Nitin and Swati
					 */
//					{ftbContractId,ftbServiceId,serviceDate,serviceTime},
					{serviceDate,serviceTime},
					
					field2,
					/**Ticket Info**/
					{fgroupingOfficeUse},
					{btntime,ftbCallerName,fpnbCallerCellNo,fetbEmail},
					{folbcPriority,fdbDueDate,fcompaintDate,rbtn1},// ftbTicketId,fdbDate,complaintstatus
					{fdbNetPayable,fdbAmountReceived,fdbBalance,ftbCustomerFeedback},
					{fdbExtraCharges,fTime,fTicketolbCategory,fdbEfforts,},
					{fdbEarnedHours},/** date 24.4.2018 added by komal **/
					{ftaDescription},
					/**Prod Info**/
					{fgroupingProductinfo},
					{fprodInfoComposite},
					{folbProductGroup,ftbBrandName,ftbModel},
					{fdbremark},
					/**Complaint History**/
					{fgroupingActivityTable},
					{factivitytable},
					{ftaServAddress},
					{fdbPlanFromDate,fdbPlanToDate},

				};	
			
			this.fields=formfield;
			
		}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime")) {
			FormField[][] formfield = {   
//					{fgroupingCustomerInformation},
//					{fbtnnewCustomer},
//					{fpic},
//					{fclrCust},
//					{fgroupingrefDet},
//					field,
//					{ftbMfgNum,ftbAssetUnit,ftbContractId,ftbServiceId},
//					{serviceDate,serviceTime},
//					field2,	//komal
//					
//					{fgroupingOfficeUse},
//					{ftbcountId,fdbDate,btntime,ftbCallerName},
//					{fpnbCallerCellNo,fetbEmail ,complaintstatus,folbeSalesPerson},
//					{folbbBranch ,folbcPriority,fTicketolbCategory,fassinto,},
//					
//					{fdbNewServiceDate,ftmbServiceTime,fdbAssignedDate,ftmbAssignedTime},
//					{fdbExpectedCompletionDate,ftmbExpectedCompletionTime,fdbActualCompletionDate,ftmbActualCompletionTime},
//					{fdbTatSla,fdbTatTechnician,ftmbTatSupervisor,fdbTatActual},
//					{rbtn1},
//					
//					{ftaDescription},
//					
//					{fgroupingProductinfo},
//					{fprodInfoComposite},
//					{ftbBrandName,ftbModel,ftbProSerialNo},// nidhi 9-08-2018
//					{fdbremark,fdbtnadd},
//					{fgroupingActivityTable},
//					{factivitytable},
//					{ftaServAddress},
//					{fdbPlanFromDate,fdbPlanToDate},
					
					{fgroupingCustomerInformation},
					{fbtnnewCustomer,fpic,fclrCust},
					{ftbcountId,fdbDate,complaintstatus,folbbBranch,folbeSalesPerson,fassinto},
					//Reference Details
					{fgroupingrefDet},
					field,
					/**
					 * @author Anil @since 16-09-2021
					 * removing contract id and service id for divine raised by Nitin and Swati
					 */
//					{ftbMfgNum,ftbAssetUnit,ftbContractId,ftbServiceId},
//					{serviceDate,serviceTime},
					{ftbMfgNum,ftbAssetUnit,serviceDate,serviceTime},
					
					field2,	
					//Ticket Info
					{fgroupingOfficeUse},	
					{btntime,ftbCallerName,fpnbCallerCellNo,fetbEmail},
					{folbcPriority,fTicketolbCategory,ftmbServiceTime,fdbAssignedDate},  
					{ftmbAssignedTime,fdbExpectedCompletionDate,ftmbExpectedCompletionTime,fdbActualCompletionDate,},
					{ftmbActualCompletionTime,fdbTatSla,fdbTatTechnician,ftmbTatSupervisor,},
					{fdbTatActual,rbtn1},
					{ftaDescription},
					//Prduct Details
					{fgroupingProductinfo},
					{fprodInfoComposite},
					{folbProductGroup,ftbBrandName,ftbModel,ftbProSerialNo},
					{fdbremark},// nidhi 9-08-2018
					// Complain details
					{fgroupingActivityTable},
					{factivitytable},
					{ftaServAddress},
					{fdbPlanFromDate,fdbPlanToDate},
					
				};
				this.fields=formfield;
			}
		else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", AppConstants.PC_ENABLESHOWSERVICETYPEDROPDOWN)){
			
			FormField[][] formfield = {   
					{fgroupingCustomerInformation},
					{fbtnnewCustomer,fpic,fclrCust},
					{ftbcountId,fdbDate,complaintstatus,folbbBranch,folbeSalesPerson,fassinto},
					{flbCustomerBranch,folbservicetype},
					
					//Reference Details
					{fgroupingrefDet},
					field,
					/**
					 * @author Anil @since 16-09-2021
					 * removing contract id and service id for divine raised by Nitin and Swati
					 */
//					{ftbContractId,ftbServiceId,serviceDate,serviceTime},
					{serviceDate,serviceTime},
					field2,	//komal
					
					//Ticket info
					{fgroupingOfficeUse},
					{btntime,ftbCallerName,fpnbCallerCellNo,fetbEmail},
					{folbcPriority,fTicketolbCategory,fdbDueDate,fcompaintDate,},
					{fdbNewServiceDate,fdbEfforts,fdbEarnedHours,fuploaddocument},
					{fblankgroup},
					{rbtn1,fTime},
						/** date 19.4.2018 added by komal **/
					{ftaDescription},
					
					//Product Details
					{fgroupingProductinfo},
					{fprodInfoComposite},
					{folbProductGroup,ftbBrandName,ftbModel,ftbProSerialNo},
					{fdbremark},// nidhi 9-08-2018

					//complaint details
					{fgroupingActivityTable},
					{factivitytable},
					{ftaServAddress},
					{fdbPlanFromDate,fdbPlanToDate},
					
					
										
				};
			this.fields=formfield;
			
		}
		else{/** date 13.03.2018 rearranged by komal after adding caller email **/
			FormField[][] formfield = {   
					{fgroupingCustomerInformation},
					{fbtnnewCustomer,fpic,fclrCust},
					{ftbcountId,fdbDate,complaintstatus,folbbBranch,folbeSalesPerson,fassinto},
					{flbCustomerBranch},
					
					//Reference Details
					{fgroupingrefDet},
					field,
					/**
					 * @author Anil @since 16-09-2021
					 * removing contract id and service id for divine raised by Nitin and Swati
					 */
//					{ftbContractId,ftbServiceId,serviceDate,serviceTime},
					{serviceDate,serviceTime},
					field2,	//komal
					
					//Ticket info
					{fgroupingOfficeUse},
					{btntime,ftbCallerName,fpnbCallerCellNo,fetbEmail},
					{folbcPriority,fTicketolbCategory,fdbDueDate,fcompaintDate,},
					{fdbNewServiceDate,fdbEfforts,fdbEarnedHours,fuploaddocument},
					{fblankgroup},
					{rbtn1,fTime},
						/** date 19.4.2018 added by komal **/
					{ftaDescription},
					
					//Product Details
					{fgroupingProductinfo},
					{fprodInfoComposite},
					{folbProductGroup,ftbBrandName,ftbModel,ftbProSerialNo},
					{fdbremark},// nidhi 9-08-2018

					//complaint details
					{fgroupingActivityTable},
					{factivitytable},
					{ftaServAddress},
					{fdbPlanFromDate,fdbPlanToDate},
					
					
										
				};
			this.fields=formfield;
		}


	}

	/**
	 * method template to update the model with token entity name
	 */
	
	@Override
	public void updateModel(Complain model) 
	{
		
		Date date=new Date();
		
		DateTimeFormat dtf = DateTimeFormat.getFormat("HH:mm:ss");
		DateTimeFormat dtf1 = DateTimeFormat.getFormat("dd/MM/yy");
		
		System.err.println(dtf.format(date).toString());
		
	    String time=new String(dtf.format(date).toString());
	    String date1=new String(dtf1.format(date).toString());
		
	    
	    
	    if(tbTitle.getValue()!=null)
			model.setComplainTitle(tbTitle.getValue());
	    
//		if(!tbfristname.getValue().equals(""))
//			model.setCustFirstName(tbfristname.getValue());
//		if(!tblastname.getValue().equals(""))
//			model.setCustLastName(tblastname.getValue());
//		if(lbcell.getValue()!=null)
//			model.setCustCell(lbcell.getValue());
//		if(!etbemail.getValue().equals(""))
//			model.setCustEmail(etbemail.getValue());
		if(pic.getValue()!=null)
			model.setPersoninfo(pic.getValue());
		if(olbeSalesPerson.getValue()!=null)
			model.setSalesPerson(olbeSalesPerson.getValue());
		if(olbbBranch.getValue()!=null){
			model.setBranch(olbbBranch.getValue());
		}
		if(dbDate.getValue()!=null)
			model.setDate(dbDate.getValue());
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime")) {
			if(tmbRegistrationTime.getValue()!=null){
				model.setTime(tmbRegistrationTime.getValue());
			}
		}else {
			if(!tbtime.getValue().equals(""))
				model.setTime(time);
		}
		if(olbcPriority.getValue()!=null)
			model.setPriority(olbcPriority.getValue());
		
		/***28-12-2019 Deepak Salve added this code for Set Value from HashSet to Text box***/
		if(prodInfoComposite.getValue()!=null){
			Console.log("in updatemodel prodInfoComposite not null");
			ProductInfo productInfo=new ProductInfo();
			productInfo.setProdID(Integer.parseInt(prodInfoComposite.getProdID().getValueBox().getValue()));
			productInfo.setProductCode(prodInfoComposite.getProdCodeValue());
			productInfo.setProductName(prodInfoComposite.getProdName().getValueBox().getValue());
			System.out.println("Product Name "+prodInfoComposite.getProdName().getValueBox().getValue());
			productInfo.setProductCategory(prodInfoComposite.getProdCategory().getValue()+"");
			model.setPic(productInfo);
			pinfo=productInfo;//18-01-2024
		}else {
			//Ashwini Patil Date:28-12-2023 sometimes productInfo is not getting saved so added alternative code
			Console.log("in updatemodel prodInfoComposite null");
			Service object=olbServiceDetails.getSelectedItem();
			if(object!=null) {
				Console.log("in updatemodel service object not null");
				ProductInfo productInfo=new ProductInfo();
				productInfo.setProdID(object.getProduct().getCount());
				productInfo.setProductCode(object.getProduct().getProductCode());
				productInfo.setProductName(object.getProduct().getProductName());
				productInfo.setProductCategory(object.getProduct().getProductCategory());
				model.setPic(productInfo);	
				pinfo=productInfo;//18-01-2024
			}else
				Console.log("in updatemodel service object null");
			
		}
		/***End***/
		
		if(olbassignto.getValue()!=null)
			model.setAssignto(olbassignto.getValue());
		if(tbBrandName.getValue()!=null)
			model.setBrandName(tbBrandName.getValue());
		if(tbModel.getValue()!=null)
			model.setModelNumber(tbModel.getValue());
		if(taDescription.getValue()!=null)
			model.setDestription(taDescription.getValue());
		if(dbDueDate.getValue()!=null)
			model.setDueDate(dbDueDate.getValue());
		if(taRemark.getValue()!=null)
			model.setRemark(taRemark.getValue());
		
		if(taServAddress.getValue()!=null)
			model.setServiceAddress(taServAddress.getValue());
		
		
		if(tbservicetime.getValue()!=null)
			model.setServicetime(tbservicetime.getValue());
		if(serviceDate.getValue()!=null)
			model.setServiceDate(serviceDate.getValue());
		if(olbSatatus.getValue()!=null)
			model.setCompStatus(olbSatatus.getValue());
		if(contractCount.getSelectedIndex()!=0){
			model.setExistingContractId(Integer.parseInt(contractCount.getValue(contractCount.getSelectedIndex())));
		}
		if(olbServiceDetails.getSelectedIndex()!=0){
//			String serviceDt = lbServiceCount.getValue(lbServiceCount.getSelectedIndex());
//			Date serdate=DateTimeFormat.getFormat("dd-MMM-yyyy").parse(serviceDt);
//			model.setExistingServiceDate(serdate);
			
			model.setServiceDetails(olbServiceDetails.getValue(olbServiceDetails.getSelectedIndex()));
		}
		if(tbServiceEngineer.getValue()!=null){
			model.setServiceEngForExistingService(tbServiceEngineer.getValue());
		}
		if(!tbServiceId.getValue().equals("")){
			model.setServiceId(Integer.parseInt(tbServiceId.getValue()));
		}
		if(!tbCallerName.getValue().equals("")){
			model.setCallerName(tbCallerName.getValue());
		}
		if(pnbCallerCellNo.getValue()!=null){
			model.setCallerCellNo(pnbCallerCellNo.getValue());
		}
		model.setBbillable(cbbillable.getValue());
		
		List<ComplainList> complainLis=this.activitytable.getDataprovider().getList();
		ArrayList<ComplainList> complainArr=new ArrayList<ComplainList>();
		complainArr.addAll(complainLis);
		model.setComplainList(complainArr);
		
		
		if(dbcompaintDate.getValue()!=null){
			model.setComplainDate(dbcompaintDate.getValue());
		}
		
		
		if( dbNetPayable.getValue()!= null && !dbNetPayable.getValue().equals("")){
			model.setNetPayable(dbNetPayable.getValue());
		}
		
		if( dbAmountReceived.getValue()!= null && !dbAmountReceived.getValue().equals("")){
			model.setAmountReceived(dbAmountReceived.getValue());
		}
		
		if(dbBalance.getValue()!= null && !dbBalance.getValue().equals("")){
			model.setBalance(dbBalance.getValue());
		}

		if(!tbCustomerFeedback.getValue().equals("")){
			model.setCustomerFeedback(tbCustomerFeedback.getValue());
		}
		
		if( dbExtraCharges.getValue()!= null && !dbExtraCharges.getValue().equals("")){
			model.setExtraCharges(dbExtraCharges.getValue());
		}
		
		if(!tbTicketId.getValue().equals("")){
			model.setTicketId(Integer.parseInt(tbTicketId.getValue()));
		}
		/** date 13.03.2018 added by komal for nbhc **/
		if(etbEmail.getValue()!=null){
			model.setEmail(etbEmail.getValue());
		}
		
		
		/** date 19.04.2018 added by komal for new service date **/
		if(dbNewServiceDate.getValue()!=null){
			model.setNewServiceDate(dbNewServiceDate.getValue());
		}
		if(olbProductDetails.getSelectedIndex()!=0){
			model.setProductName(olbProductDetails.getValue(olbProductDetails.getSelectedIndex()));
		}
		
		List<Service> services=this.serviceInfoTable.getDataprovider().getList();
		ArrayList<Service> serviceArr=new ArrayList<Service>();
		serviceArr.addAll(services);
		model.setServiceList(serviceArr);
		String serviceHrs=getP_servicehours().getItemText(getP_servicehours().getSelectedIndex());
		String serviceMin=getP_servicemin().getItemText(getP_servicemin().getSelectedIndex());
		String serviceAmPm=getP_ampm().getItemText(getP_ampm().getSelectedIndex());
		String totalTime = "";
		String calculatedServiceTime;
		
		totalTime = serviceHrs+":"+serviceMin+""+serviceAmPm;
		if(totalTime.equalsIgnoreCase("--:----")){
			calculatedServiceTime = "Flexible";
		}else{
			calculatedServiceTime = totalTime;
		}
		/**
		 * nidhi
		 * 9-08-2018
		 */
		model.setProSerialNo(tbProSerialNo.getValue());
		
		model.setServicetime(calculatedServiceTime);
		/**
		 * end komal
		 */
		
		/*** Date 15-10-2018 By Vijay For Complain Ticket ***/
		if(olbTicketCategory.getSelectedIndex()!=0)
		model.setCategory(olbTicketCategory.getValue());
		
		/*** Date 17-10-2018 By Vijay For Efforts ***/
		if(dbEfforts.getValue()!=null)
		model.setPlannedHours(dbEfforts.getValue());	
		if(dbEarnedHours.getValue()!=null)
		model.setEarnedHours(dbEarnedHours.getValue());
		
		if(dbPlanFromDate.getValue()!=null)
			model.setPlanFromDate(dbPlanFromDate.getValue());
		if(dbPlanToDate.getValue()!=null)
			model.setPlanToDate(dbPlanToDate.getValue());
		
		
		if(oblCustomerBranch.getValue()!=null){
			model.setCustomerBranch(oblCustomerBranch.getValue());
		}else{
			model.setCustomerBranch("");
		}
		
		if(dbExpectedCompletionDate.getValue()!=null) {
			model.setExpectedCompletionDate(dbExpectedCompletionDate.getValue());
		}else {
			model.setExpectedCompletionDate(null);
		}
		if(tmbExpectedCompletionTime.getValue()!=null) {
			model.setExpectedCompletionTime(tmbExpectedCompletionTime.getValue());
		}else {
			model.setExpectedCompletionTime(null);
		}
		
		if(dbAssignedDate.getValue()!=null) {
			model.setAssignedDate(dbAssignedDate.getValue());
		}else {
			model.setAssignedDate(null);
		}
		if(tmbAssignedTime.getValue()!=null) {
			model.setAssignedTime(tmbAssignedTime.getValue());
		}else {
			model.setAssignedTime(null);
		}
		
		if(dbActualCompletionDate.getValue()!=null) {
			model.setCompletionDate(dbActualCompletionDate.getValue());
		}else {
			model.setCompletionDate(null);
		}
		if(tmbActualCompletionTime.getValue()!=null) {
			model.setCompletionTime(tmbActualCompletionTime.getValue());
		}else {
			model.setCompletionTime(null);
		}
		
		if(tmbTatTechnician.getValue()!=null) {
			model.setTat_Tech(tmbTatTechnician.getValue());
		}else {
			model.setTat_Tech(null);
		}
		
		if(tmbTatActual.getValue()!=null) {
			model.setTat_Actual(tmbTatActual.getValue());
		}else {
			model.setTat_Actual(null);
		}
		
		if(tmbTatSla.getValue()!=null) {
			model.setTat_sla(tmbTatSla.getValue());
		}else {
			model.setTat_sla(null);
		}
		
		if(tmbTatSupervisor.getValue()!=null) {
			model.setTat_Sup(tmbTatSupervisor.getValue());
		}else {
			model.setTat_Sup(null);
		}
		
		if(tmbServiceTime.getValue()!=null){
			model.setNewServiceTime(tmbServiceTime.getValue());
		}else{
			model.setNewServiceTime(null);
		}
		
		if(lbAssetId.getValue()!=null){
			model.setAssetId(Integer.parseInt(lbAssetId.getValue().trim()));
		}else{
			model.setAssetId(0);
		}
		model.setMfgNum(tbMfgNum.getValue());
		model.setAssetUnit(tbAssetUnit.getValue());
		
		if(mfgDate!=null){
			model.setMfgDate(mfgDate);
		}
		
		if(replacementDate!=null){
			model.setReplacementDate(replacementDate);
		}
		
		if(componentName!=null){
			model.setComponentName(componentName);
		}
		
		if(uploaddocument.getValue()!=null){
			model.setDocument(uploaddocument.getValue());
		}
		
		if(olbservicetype.getSelectedIndex()!=0){
			model.setServiceType(olbservicetype.getValue());
		}
		if(olbProductGroup.getSelectedIndex()!=0){
			model.setProductGroup(olbProductGroup.getValue());
		}
		
		
		complainObj=model;
		presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Complain view) 
	{

		System.out.println("rohan in side update view "+view.getCount());
		complainObj=view;
		
		if(view.getPic()!=null){
			this.tbcount.setValue(view.getCount()+"");
			this.setCustServAddress(view.getPic().getCount());
			pinfo=view.getPic();//18-01-2024
			Console.log("pinfo updated");
			if(pinfo!=null) {
				Console.log("pinfo.getProdID() "+pinfo.getProdID());
				Console.log("pinfo.getProductCode() "+pinfo.getProductCode());
				Console.log("pinfo.getProductName() "+pinfo.getProductName());
			}
			else
				Console.log("pinfo found null");
		}
		
//		if(view.getCustFirstName()!=null)
//			tbfristname.setValue(view.getCustFirstName());
//		if(view.getCustLastName()!=null)
//			tblastname.setValue(view.getCustLastName());
//		if(view.getCustCell()!=null)
//			lbcell.setValue(view.getCustCell());
//		if(view.getCustEmail()!=null)
//			etbemail.setValue(view.getCustEmail());
		
		if(view.getComplainTitle()!=null)
			tbTitle.setValue(view.getComplainTitle());
		
		
		if(view.getPersoninfo()!=null){
			pic.setValue(view.getPersoninfo());
		}
		if(view.getDate()!=null)
			dbDate.setValue(view.getDate());
		if(view.getAssignto()!=null)
			olbassignto.setValue(view.getAssignto());
		if(view.getBranch()!=null)
			olbbBranch.setValue(view.getBranch());
		if(view.getTime()!=null)
			tbtime.setValue(view.getTime());
		if(view.getPriority()!=null)
			olbcPriority.setValue(view.getPriority());
		if(view.getPic()!=null)
			prodInfoComposite.setValue(view.getPic());
		if(view.getBrandName()!=null)
			tbBrandName.setValue(view.getBrandName());
		if(view.getModelNumber()!=null)
			tbModel.setValue(view.getModelNumber());
		if(view.getDestription()!=null)
			taDescription.setValue(view.getDestription());
		if(view.getRemark()!=null)
			taRemark.setValue(view.getRemark());
		
		if(view.getServiceAddress()!=null)
			taServAddress.setValue(view.getServiceAddress());
		
		
		if(view.getDueDate()!=null)
			dbDueDate.setValue(view.getDueDate());
		if(view.getSalesPerson()!=null)
			olbeSalesPerson.setValue(view.getSalesPerson());
		
		cbbillable.setValue(view.getBbillable());
		
		if(view.getServiceDate()!=null)
			serviceDate.setValue(view.getServiceDate());
		if(view.getServicetime()!=null)
			tbservicetime.setValue(view.getServicetime());
		if(view.getCompStatus()!=null)
			olbSatatus.setValue(view.getCompStatus());
		if(view.getContrtactId()!=null){
			tbContractId.setValue(view.getContrtactId()+"");
		}
		if(view.getQuatationId()!=null){
			tbQuatationId.setValue(view.getQuatationId()+"");
		}
		if(view.getServiceEngForExistingService()!=null){
			tbServiceEngineer.setValue(view.getServiceEngForExistingService());
		}
		if(view.getServiceId()!=null){
			tbServiceId.setValue(view.getServiceId()+"");
		}
		
		if(view.getCallerName()!=null){
			tbCallerName.setValue(view.getCallerName());
		}
		
		if(view.getCallerCellNo()!=0){
			pnbCallerCellNo.setValue(view.getCallerCellNo());
		}
		
		if(view.getExistingContractId()!=null){
			Date servicedate=null;
			if(view.getExistingServiceDate()!=null){
				servicedate=view.getExistingServiceDate();
			}
			this.getcontractIdfomCustomerID(view.getPersoninfo().getCount(),view.getExistingContractId(),servicedate,view.getServiceDetails());
			
		}
		
		if(view.getComplainDate()!=null){
			dbcompaintDate.setValue(view.getComplainDate());
		}
		
		
		if(view.getNetPayable()!=null)
			dbNetPayable.setValue(view.getNetPayable());
		
		if(view.getAmountReceived()!=null)
			dbAmountReceived.setValue(view.getAmountReceived());
		
		if(view.getBalance()!=null)
			dbBalance.setValue(view.getBalance());
		
		if(view.getCustomerFeedback()!=null){
			tbCustomerFeedback.setValue(view.getCustomerFeedback());
		}
		
		if(view.getExtraCharges()!=null){
			dbExtraCharges.setValue(view.getExtraCharges());
		}
		
//		if(view.getServiceDetails()!= null){
//			olbServiceDetails.setValue(view.getServiceDetails());
//		}
		
		this.tbTicketId.setValue(view.getCount()+"");
		
		
		activitytable.setValue(view.getComplainList());
		/** date 13.03.2018 added by komal for nbhc **/
		if(view.getEmail()!=null){
	        etbEmail.setValue(view.getEmail());
		}
		/** end komal **/
		/** date 19.04.2018 added by komal for new service date **/
		if(view.getNewServiceDate()!=null){
			dbNewServiceDate.setValue(view.getNewServiceDate());
		}
		
		if(view.getCustomerBranch()!=null&&!view.getCustomerBranch().equals("")&&view.getPersoninfo()!=null&&view.getPersoninfo().getCount()!=0){
			/**
			 * @author Anil
			 * @since 03-06-2020
			 */
			loadCustomerBranch(view.getPersoninfo().getCount(), view.getCustomerBranch());
			oblCustomerBranch.setValue(view.getCustomerBranch());
		}
		
		if(view.getTat_sla()!=null) {
			tmbTatSla.setValue(view.getTat_sla());
		}
		
		if(view.getProductName()!=null){
			if(view.getExistingContractId()!=null){
			getProductDetailsFromContractId(view.getExistingContractId() , view.getProductName(),view.getAssetId(),view.getTat_sla());
			olbProductDetails.setValue(view.getProductName());
			}
		}
		if(view.getServiceList().size()>0){
			serviceInfoTable.setValue(view.getServiceList());
		}
		setpopupServiceTime(view.getServicetime());
		/**
		 * end komal
		 */
		/**
		 * nidhi
		 * 9-08-2018
		 */
		if(view.getProSerialNo()!=null){
			tbProSerialNo.setValue(view.getProSerialNo());
		}
		
		/*** Date 15-10-2018 By Vijay For Complain Ticket ***/
		if(view.getCategory()!=null){
			olbTicketCategory.setValue(view.getCategory());
		}
		
		
		/*** Date 17-10-2018 By Vijay For Efforts ***/
		dbEfforts.setValue(view.getPlannedHours());
		dbEarnedHours.setValue(view.getEarnedHours());
		
		if(view.getPlanFromDate()!=null)
			dbPlanFromDate.setValue(view.getPlanFromDate());
		if(view.getPlanToDate()!=null)
			dbPlanToDate.setValue(view.getPlanToDate());
		
		if(view.getExpectedCompletionDate()!=null) {
			dbExpectedCompletionDate.setValue(view.getExpectedCompletionDate());
		}
		if(view.getExpectedCompletionTime()!=null) {
			tmbExpectedCompletionTime.setValue(view.getExpectedCompletionTime());
		}
		
		if(view.getAssignedDate()!=null) {
			dbAssignedDate.setValue(view.getAssignedDate());
		}
		if(view.getAssignedTime()!=null) {
			tmbAssignedTime.setValue(view.getAssignedTime());
		}
		
		if(view.getCompletionDate()!=null) {
			dbActualCompletionDate.setValue(view.getCompletionDate());
		}
		if(view.getCompletionTime()!=null) {
			tmbActualCompletionTime.setValue(view.getCompletionTime());
		}
		
		if(view.getTat_Tech()!=null) {
			tmbTatTechnician.setValue(view.getTat_Tech());
		}
		if(view.getTat_Actual()!=null) {
			tmbTatActual.setValue(view.getTat_Actual());
		}
		if(view.getTat_Sup()!=null) {
			tmbTatSupervisor.setValue(view.getTat_Sup());
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime")) {
			if(view.getTime()!=null){
				tmbRegistrationTime.setValue(view.getTime());
			}
		}else {
			if(view.getTime()!=null){
				tbtime.setValue(view.getTime());
			}
		}
		
		if(view.getNewServiceTime()!=null){
			tmbServiceTime.setValue(view.getNewServiceTime());
		}
		
		if(view.getAssetId()!=0){
			for(int i=0;i<lbAssetId.getItemCount();i++){
				if(lbAssetId.getItemText(i).equals(view.getAssetId()+"")){
					lbAssetId.setSelectedIndex(i);
				}
			}
		}
		
		if(view.getMfgNum()!=null){
			tbMfgNum.setValue(view.getMfgNum());
		}
		
		if(view.getAssetUnit()!=null){
			tbAssetUnit.setValue(view.getAssetUnit());
		}
		if(view.getMfgDate()!=null){
			mfgDate=view.getMfgDate();
		}
		if(view.getReplacementDate()!=null){
			replacementDate=view.getReplacementDate();
		}
		if(view.getComponentName()!=null){
			componentName=view.getComponentName();
		}
		
		if(view.getDocument()!=null){
			uploaddocument.setValue(view.getDocument());
		}
		
		if(view.getServiceType()!=null){
			olbservicetype.setValue(view.getServiceType());
		}
		if(view.getProductGroup()!=null&&!view.getProductGroup().equals("")) {
			olbProductGroup.setValue(view.getProductGroup());
		}
		
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */
	}

	


	/**
	 * Toggles the app header bar menus as per screen state
	 */
	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")
						|| text.contains("Search")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Save") || text.contains("Discard")|| text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Edit") || text.contains("Discard")||text.contains("Print") || text.contains("Search")|| text.contains(AppConstants.NAVIGATION)||text.contains("Email") 
						||text.contains(AppConstants.MESSAGE))
					menus[k].setVisible(true);
				else
					menus[k].setVisible(false);
			}
		}
		
		
	AuthorizationHelper.setAsPerAuthorization(Screen.COMPLAIN,LoginPresenter.currentModule.trim());
}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
			tbcount.setValue(count+"");
			tbTicketId.setValue(count+"");
		
	}
	@Override
	public void setToViewState() {
	
		super.setToViewState();
		setMenuAsPerStatus();
		
		String mainScreenLabel="Customer Support Details";
		if(complainObj!=null&&complainObj.getCount()!=0){
			mainScreenLabel=complainObj.getCount()+" "+"/"+" "+AppUtility.parseDate(complainObj.getDate())+" "+"/"+" "+complainObj.getCompStatus();
		}
		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		
		SuperModel model=new Complain();
//		model=complaiObj;
		if(pic.getId().getValue()!=null && !pic.getId().getValue().equals(""))
		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.CUSTOMERSUPPORT, complainObj.getCount(), Integer.parseInt(pic.getId().getValue()),pic.getName().getValue(),Long.parseLong(pic.getPhone().getValue()), false, model, null);
		
		
		
	}
	
	
	
	public void setMenuAsPerStatus() {
		setAppHeaderBarAsPerStatus();
		toggleProcessLevelMenu();
	}
	
	@Override
	public void setToEditState() {
	
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		this.taServAddress.setEnabled(false);
		if(olbServiceDetails.getSelectedIndex()!=0&&prodInfoComposite!=null){
			System.out.println("INSIDE SERVICE COUNT SET");
			prodInfoComposite.setEnable(false);
		}
		if(contractCount.getItemCount()==1&&!tbContractId.getValue().equals("")){
			contractCount.setEnabled(false);
		}
		
		/**
		 * @author Anil
		 * @since 11-05-2020
		 */
		if(UserConfiguration.getRole()!=null&&
				(UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")||
						UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator"))){
			dbDate.setEnabled(true);
			tmbRegistrationTime.setEnabled(true);
			if(olbSatatus.getSelectedIndex()!=0){
				if(olbSatatus.getValue(olbSatatus.getSelectedIndex()).equals("Completed")){
					olbSatatus.setEnabled(false);
					dbActualCompletionDate.setEnabled(true);
					tmbActualCompletionTime.setEnabled(true);
				}
			}
		}
		
		String mainScreenLabel="Customer Support Details";
		if(complainObj!=null&&complainObj.getCount()!=0){
			
			mainScreenLabel=complainObj.getCount()+" "+"/"+" "+AppUtility.parseDate(complainObj.getDate())+" "+"/"+" "+complainObj.getCompStatus();
		}
		 fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		
		
	}

	public void setAppHeaderBarAsPerStatus() {

		Complain entity=(Complain) presenter.getModel();
		String status=entity.getCompStatus();
		
		if(forJobReportcumReceiptFlag==true){
			
			if (status.equals("Completed")) {
				InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
				for (int k = 0; k < menus.length; k++) {
					String text = menus[k].getText();
					if (text.contains("Discard") || text.contains("Search") || text.contains("Print") || text.contains(AppConstants.MESSAGE) ) {
						menus[k].setVisible(true);
					} else {
						menus[k].setVisible(false);
					}
				}
			}
			
		}else{
			/**
			 * @author Anil
			 * @since 11-05-2020
			 */
			if(UserConfiguration.getRole()!=null&&
					(UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")||
							UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator"))&&
							status.equals("Completed")){
				InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
				for (int k = 0; k < menus.length; k++) {
					String text = menus[k].getText();
					if (text.contains("Discard") || text.contains("Search")|| text.contains("Edit") || text.contains("Email") || text.contains("Print") || text.contains(AppConstants.MESSAGE)) {
						menus[k].setVisible(true);
					} else {
						menus[k].setVisible(false);
					}
				}
			}else if (status.equals("Completed")) {
				InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
				for (int k = 0; k < menus.length; k++) {
					String text = menus[k].getText();
					if (text.contains("Discard") || text.contains("Search") || text.contains("Email") || text.contains("Print") || text.contains(AppConstants.MESSAGE)) {
						menus[k].setVisible(true);
					} else {
						menus[k].setVisible(false);
					}
				}
			}
		}
		
	}
	
	public void toggleProcessLevelMenu() {	
		Complain entity=(Complain) presenter.getModel();
		String status=entity.getCompStatus();
	

		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();
			if ((status.equals("Completed"))) {
				if (text.equals(AppConstants.NEW))
					label.setVisible(true);
				else if(tbServiceId.getValue()!=null&&!tbServiceId.getValue().equals("")){
					if (text.equals("View Service"))
						label.setVisible(true);
					else
						label.setVisible(false);
				}else
					label.setVisible(false);
			}else if(tbServiceId.getValue()==null||tbServiceId.getValue().equals("")){
				if (text.equals("View Service"))
					label.setVisible(false);
				
			}
			
			/**
			 * @author Anil @since 27-09-2021
			 * Cancel button should be open for all. as per the issue log by Ashwini for pecopp
			 * earlier it was visible only if process complainwithturnaroudtime is active
			 * 
			 */
//			if(!complainTatFlag){
//				if (text.equals("Cancel")){
//					label.setVisible(false);
//				}
//			}else{
				if(text.equals("Cancel")&&status.equals("Cancelled")){
					label.setVisible(false);
				}
				
				if(text.equals("Service")&&status.equals("Cancelled")){
					label.setVisible(false);
				}
				if(text.equals("View Contract")){
					label.setVisible(true);
				}
//			}
		}
	}


	@Override
	public void onClick(ClickEvent event)
	{
		if(event.getSource().equals(clrCust)){
//			if(pic.getValue()!=null){
				pic.clear();
				contractCount.clear();
				contractCount.addItem("--SELECT--");
//			}
		}

		if(event.getSource().equals(btnsettime)){
			
			System.out.println("set button");
			
			String str=this.tbtime.getValue();
			settime.getTbtime().setValue(str);
			settime.getP_servicehours().setSelectedIndex(0);
			settime.getP_servicemin().setSelectedIndex(0);
			settime.getP_ampm().setSelectedIndex(0);
			
			panel=new PopupPanel(true);
			panel.add(settime);
			
			panel.show();
			panel.center();
			
			
		}
		
		if(event.getSource().equals(settime.getBtnOk())){
			System.out.println("pupup ok button");
			if(settime.getP_servicehours().getSelectedIndex()!=0&&settime.getP_servicemin().getSelectedIndex()!=0&&settime.getP_ampm().getSelectedIndex()!=0){
				String str1=settime.getP_servicehours().getValue(settime.getP_servicehours().getSelectedIndex());
				String str2=settime.getP_servicemin().getValue(settime.getP_servicemin().getSelectedIndex());
				String str3=settime.getP_ampm().getValue(settime.getP_ampm().getSelectedIndex());
				this.tbtime.setValue(str1+":"+str2+" "+str3);
				panel.hide();
			}else{
				showDialogMessage("Please enter time Properly!");
			}
		}
		
		if(event.getSource().equals(settime.getBtnCancel())){
			panel.hide();
		}
		
		if(event.getSource().equals(btnadd)){
			System.out.println("add remark out event");
			if(!this.taRemark.getValue().trim().equals("")){
				System.out.println("add remark In event");
				reactOnAdd();
				this.taRemark.setValue(" ");
			}else{
				showDialogMessage("Please Enter Remark First!");
			}
		}
		
		
		if(event.getSource().equals(cbbillable)){
			
			if(cbbillable.getValue()==true){
				
				System.out.println("on click on bilable when true::::::::::::::::::");
				dbNetPayable.setEnabled(true);
				dbAmountReceived.setEnabled(true);
				dbBalance.setEnabled(false);	
				
			}
				if(cbbillable.getValue()==false){
				
					System.out.println("on click on bilable when false::::::::::::::::: ");
					dbNetPayable.setEnabled(false);
					dbAmountReceived.setEnabled(false);
					dbBalance.setEnabled(false);	
				
				}
				
			}
		
	}
	
	
	public void reactOnAdd()
	{
				
		
		List<ComplainList> complainLis=this.activitytable.getDataprovider().getList();
		String remark=taRemark.getValue();
		Date date=new Date();
		
		DateTimeFormat dtf = DateTimeFormat.getFormat("HH:mm:ss");
		System.err.println(dtf.format(date).toString());
	
		
	     String time=new String(dtf.format(date).toString());
		
		ComplainList complist=new ComplainList();
		complist.setSrNo(complainLis.size()+1);
		complist.setDate(date);
		complist.setTime(time);
		complist.setRemark(remark);
		
		if(LoginPresenter.loggedInUser!=null){
			complist.setUserName(LoginPresenter.loggedInUser);
		}
		
		List<ComplainList>list=activitytable.getDataprovider().getList();
			list.add(complist);
	}


	@Override
	public void onChange(ChangeEvent event) {
		/**
		 * @author Anil
		 * @since 11-05-2020
		 */
		if(event.getSource().equals(tmbRegistrationTime.lbHours)){
			tmbServiceTime.setValue(tmbRegistrationTime.getValue());
			tmbAssignedTime.setValue(tmbRegistrationTime.getValue());
			calculateExpectedCompletionDateAndTime(dbDate.getValue(), tmbRegistrationTime.getValue(), convertTimeToNumbers(tmbTatSla.getValue()));
		}
		
		if(event.getSource().equals(tmbRegistrationTime.lbMinutes)){
			tmbServiceTime.setValue(tmbRegistrationTime.getValue());
			tmbAssignedTime.setValue(tmbRegistrationTime.getValue());
			calculateExpectedCompletionDateAndTime(dbDate.getValue(), tmbRegistrationTime.getValue(),convertTimeToNumbers(tmbTatSla.getValue()));
		}
		
		
		if(event.getSource().equals(tmbRegistrationTime.lbHours)||
				event.getSource().equals(tmbRegistrationTime.lbMinutes)||
				event.getSource().equals(tmbActualCompletionTime.lbHours)||
				event.getSource().equals(tmbActualCompletionTime.lbMinutes)||
				event.getSource().equals(tmbServiceTime.lbHours)||
				event.getSource().equals(tmbServiceTime.lbMinutes)||
				event.getSource().equals(tmbAssignedTime.lbHours)||
				event.getSource().equals(tmbAssignedTime.lbMinutes)){
			updateTatCalculation();
		}
	}


	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		this.taServAddress.setEnabled(false);
		this.tbcount.setEnabled(false);
		this.tbservicetime.setEnabled(false);
		this.dbDate.setEnabled(false);
		this.serviceDate.setEnabled(false);
		this.tbtime.setEnabled(false);
		this.activitytable.setEnable(state);
		tbQuatationId.setEnabled(false);
		tbContractId.setEnabled(false);
		tbServiceEngineer.setEnabled(false);
		tbServiceId.setEnabled(false);
		if(contractCount.getItemCount()==1&&!tbContractId.getValue().equals("")){
			contractCount.setEnabled(false);
		}
		
		
		tbTicketId.setEnabled(false);
		dbNetPayable.setEnabled(false);
		dbAmountReceived.setEnabled(false);
		dbBalance.setEnabled(false);
		tbCustomerFeedback.setEnabled(false);
		dbExtraCharges.setEnabled(state);
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain","NewCustomerButtonSetEnable"))
		{
			newCustomer.setEnabled(false);
		}
		
		olbTicketCategory.setEnabled(state);
		
		/*** Date :- 17-10-2018 By Vijay for Efforts ***/
		dbEfforts.setEnabled(state);
		dbEarnedHours.setEnabled(state);
		dbPlanFromDate.setEnabled(state);
		dbPlanToDate.setEnabled(state);
		
		dbExpectedCompletionDate.setEnabled(false);
		tmbExpectedCompletionTime.setEnabled(false);
		dbActualCompletionDate.setEnabled(false);
		tmbActualCompletionTime.setEnabled(false);
		tmbTatSla.setEnabled(false);
		tmbTatActual.setEnabled(false);
		tmbTatTechnician.setEnabled(false);
		tmbAssignedTime.setEnabled(state);
		
		tmbServiceTime.setEnabled(state);
		tmbRegistrationTime.setEnabled(state);
		
		olbProductDetails.setEnabled(state);
		oblCustomerBranch.setEnabled(state);
		
		tbMfgNum.setEnabled(false);
		tbAssetUnit.setEnabled(false);
		
		olbSatatus.setEnabled(state);
		
		uploaddocument.setEnable(state);
	}


	@Override
	public boolean validate() {
		boolean superValidate = super.validate();
		 
		if(superValidate==false){
			return false;
        }
		//////////////////////////
		
//		if(!tbfristname.getValue().equals("")){
//			System.out.println("FIRST NAME NOT NULL");
//		}
//		else{
//			System.out.println("FIRST NAME  NULL");
//		}
//		
//		if(!tblastname.getValue().equals("")){
//			System.out.println("LAST NAME NOT NULL");	
//		}
//		else{
//			System.out.println("LAST NAME  NULL");
//		}
//		
//		if(!etbemail.getValue().equals("")){
//			System.out.println("EMAIL NOT NULL");
//		}
//		else{
//			System.out.println("EMAIL  NULL");
//		}
//		
//		if(lbcell.getValue()!=null){
//			System.out.println("CELL NOT NULL1");	
//		}
//		else{
//			System.out.println("CELL  NULL1");
//		}
//		
//		if(pic.getValue()==null){
//			System.out.println("person info null");
//		}
//		if(pic.getValue()!=null){
//			System.out.println("person info not null");
//		}
		////////////////////////
		
//		if(validateCustomer()==false){
//	      	return false;
//	    }
		
//		if(prodInfoComposite.getValue()==null){
//			this.showDialogMessage("Please select product!");
//			return false;
//		}
		
		/**
		 * Date 12-09-2018 By Vijay
		 * Des :- For NBHC CCPM existing contract id must be mandatory 
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "MakeContractIdMandatory")){
			System.out.println("contractCount.getItemCount() =="+contractCount.getItemCount());
				if(contractCount.getItemCount()>1 && contractCount.getSelectedIndex()==0){
					showDialogMessage("Please Select Existing Contract ID !");
					return false;
				}
		}
		/**
		 * ends here
		 */
		
		if(contractCount.getSelectedIndex()!=0&&cbbillable.getValue()==true){
			this.showDialogMessage("Existing contract can not be billable!");
			return false;
		}
		/**
		 * Date 17-10-2018 By Vijay 
		 * Des :- Billable non Mandatory For EVA with process config
		 */
		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "enableBillableNonMandatory")){
		if(contractCount.getSelectedIndex()==0&&cbbillable.getValue()==false){
			this.showDialogMessage("Please select billable!");
			return false;
		}
		}
		/** date 13.03.2018 added by komal for nbhc **/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "makeRemarkMandatory")){
			if(activitytable.getDataprovider().getList().size()==0){
				this.showDialogMessage("Complain history can't empty");
				return false;
			}
		}
		/** 
		* end komal
		*/
		/** date 28.05.2018 added by komal **/
		if(cbbillable.getValue()==true){
			if(prodInfoComposite.getProdID().getValue().equals("") || prodInfoComposite.getProdName().getValue().equals("")  || prodInfoComposite.getProdCode().getValue().equals("")){
				this.showDialogMessage("Please select product.");
				return false;
			}
		}
		/** 
		* end komal
		*/
		
		/**
		 * Date 22-10-2018 By Vijay
		 * Des :- if status is planned then plan From Date and plan To Date Mandatory
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "EnablePlannedStatusMandatory")){
			if(olbSatatus.getValue().equals("Planned")){
				if(dbPlanFromDate.getValue()==null){
					this.showDialogMessage("Plan From Date Mandatory!");
					return false;
				}
				if(dbPlanToDate.getValue()==null){
					this.showDialogMessage("Plan To Date Mandatory!");
					return false;
				}
				if(dbEfforts.getValue()==null || dbEfforts.getValue()==0){
					this.showDialogMessage("Efforts is Mandatory!");
					return false;
				}
			}
			if(olbSatatus.getValue().equals("Planned") || olbSatatus.getValue().equals("Under Development") ||
			   olbSatatus.getValue().equals("UAT") || olbSatatus.getValue().equals("Client UAT") ||
			   olbSatatus.getValue().equals("Deployment") || olbSatatus.getValue().equals("Closed")){
				if(dbEfforts.getValue()==null || dbEfforts.getValue()==0){
					this.showDialogMessage("Efforts is Mandatory!");
					return false;
				}
			}
		}
		
		/**
		 * @author Anil
		 * @since 04-05-2020
		 * service date validation for premium tech requirement
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime")) {
			if(dbNewServiceDate.getValue()!=null) {
				if(dbDate.getValue()!=null) {
					if(dbNewServiceDate.getValue().before(dbDate.getValue())) {
						showDialogMessage("Service date should not be before registration date.");
						return false;
					}
				}
				
				if(dbExpectedCompletionDate.getValue()!=null) {
					if(dbNewServiceDate.getValue().after(dbExpectedCompletionDate.getValue())) {
						showDialogMessage("Service date should not be after expected completion date.");
						return false;
					}
				}
			}
		}
		
		return true;
	}


//	public boolean validateCustomer() {
//
//		if(this.pic.getValue()!=null&&(!this.tbfristname.getValue().equals("")||!this.tblastname.getValue().equals("")||this.lbcell.getValue()!=null||!this.etbemail.getValue().equals(""))){
//			this.showDialogMessage("Please add only One Customer!");
//			return false;
//		}
//		
//		if(this.pic.getValue()==null&&(this.tbfristname.getValue().equals("")&&this.tblastname.getValue().equals("")&&this.lbcell.getValue()==null&&this.etbemail.getValue().equals(""))){
//			this.showDialogMessage("Please add Customer!");
//			return false;
//		}
//		
//		if(this.pic.getValue()==null&&(!tbfristname.getValue().equals("")&&tblastname.getValue().equals(""))){
//			this.showDialogMessage("Please Fill Customer Last Name!");
//			return false;
//		}
//		if(this.pic.getValue()==null&&(this.tbfristname.getValue().equals("")&&!this.tblastname.getValue().equals(""))){
//			this.showDialogMessage("Please Fill Customer First Name!");
//			return false;
//		}
//		
//		if(this.pic.getValue()==null&&(!this.tbfristname.getValue().equals("")&&!this.tblastname.equals(""))&&(this.lbcell.getValue()==null&&this.etbemail.getValue().equals(""))){
//			this.showDialogMessage("Please Fill either customer cell or customer email!");
//			return false;
//		}
//		
//		
//		return true;
//	}

	
	
	/**
	 * 
	 * Date : 27-10-2017 BY ANIL
	 * Earlier we passed service date as date parameter but now we are passing date as String
	 * final String serviceDetails
	 */
	
public void getcontractIdfomCustomerID(int customerId,final int contractId,final Date serviceDt,final String serviceDetails) {
	
	System.out.println("FORM CONTRACT CUSTOMER");
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(customerId);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				List<Contract> contractList=new ArrayList<Contract>();
				contractCount.clear();
				contractCount.addItem("--SELECT--");
				final List<Integer> contableList=  new ArrayList<Integer>();
				if(result.size()==0){
					pic.clear();
				}else{
					for(SuperModel model:result){
						Contract conEntity = (Contract)model;
						contableList.add(conEntity.getCount());
						contractList.add(conEntity);
					}
					Collections.sort(contableList, new Comparator<Integer>() {
						@Override
						public int compare(Integer o1, Integer o2) {
							return o2.compareTo(o1);
						}
					});
					
//					for(int i=0;i<contableList.size();i++){
//					      contractCount.addItem(contableList.get(i)+"");
//					}
					contractCount.setListItems(contractList);
				}
				for(int i=1;i<=contractCount.getItemCount();i++){
					if(Integer.parseInt(contractCount.getItemText(i))==contractId){
						contractCount.setSelectedIndex(i);
						break;
					}
				}	
				if(contableList.size()!=0){
					getServiceIdfomContractID(Integer.parseInt(contractCount.getValue(contractCount.getSelectedIndex())),serviceDt,serviceDetails);
				}
				
				
				
		}
		});
	}

//	/**
//	 * 
//	 * @param customerId
//	 * @param contractId
//	 * @param serviceDt
//	 * Date : 27-10-2017 BY ANIL
//	 * Earlier we passed service date as date parameter but now we are passing date as String
//	 */
//	public void getcontractIdfomCustomerID(int customerId,final int contractId,final String serviceDt,final String serviceDetails) {
//	
//		MyQuerry querry = new MyQuerry();
//	  	Company c = new Company();
//	  	Vector<Filter> filtervec=new Vector<Filter>();
//	  	Filter filter = null;
//	  	filter = new Filter();
//	  	filter.setQuerryString("companyId");
//		filter.setLongValue(c.getCompanyId());
//		filtervec.add(filter);
//		filter = new Filter();
//		filter.setQuerryString("cinfo.count");
//		filter.setIntValue(customerId);
//		filtervec.add(filter);
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new Contract());
//	
//		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onFailure(Throwable caught) {
//			}
//
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				contractCount.clear();
//				contractCount.addItem("--SELECT--");
//				final List<Integer> contableList=  new ArrayList<Integer>();
//				if(result.size()==0){
//					pic.clear();
//				}
//				else{
//					for(SuperModel model:result)
//					{
//						Contract conEntity = (Contract)model;
//						contableList.add(conEntity.getCount());
//					}
//					Collections.sort(contableList, new Comparator<Integer>() {
//						@Override
//						public int compare(Integer o1, Integer o2) {
//							return o2.compareTo(o1);
//						}
//					});
//					
//					for(int i=0;i<contableList.size();i++){
//					      contractCount.addItem(contableList.get(i)+"");
//					}
//			}
//				for(int i=1;i<contractCount.getItemCount();i++){
//					if(Integer.parseInt(contractCount.getItemText(i))==contractId){
//						contractCount.setSelectedIndex(i);
//						break;
//					}
//				}	
//				if(contableList.size()!=0&&serviceDt!=null){
//					getServiceIdfomContractID(Integer.parseInt(contractCount.getValue(contractCount.getSelectedIndex())),serviceDt,serviceDetails);
//				}
//				
//				
//				
//		}
//		});
//	}
	
	/**
	 * Date : 27-10-2017 BY ANIL
	 *  Earlier we passed service date as date parameter but now we are passing date as String
	 */
	
	 
public void getServiceIdfomContractID(final int contractId,final Date serviceDt,final String serviceDetails) {

	MyQuerry querry = new MyQuerry();
	Company c = new Company();
	Vector<Filter> filtervec = new Vector<Filter>();
	Filter filter = null;
	
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(c.getCompanyId());
	filtervec.add(filter);
	/**
	 * OLD code commented by ANIL ON 28-10-2017
	 */
//	filter = new Filter();
//	filter.setQuerryString("existingServiceDate");
//	filter.setDateValue(serviceDt);
//	filtervec.add(filter);
	
	/**
	 * Date : 28 -10-2017 BY ANIL
	 */
	filter = new Filter();
	filter.setQuerryString("contractCount");
	filter.setIntValue(contractId);
	filtervec.add(filter);
	/**
	 * ENd
	 */
	
	querry.setFilters(filtervec);
	querry.setQuerryObject(new Service());

	async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

		@Override
		public void onFailure(Throwable caught) {
		}

		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			System.out.println(" in side date load size"+result.size());
			olbServiceDetails.clear();
			olbServiceDetails.addItem("--SELECT--");
			ComplainPresenter.serviceList=new ArrayList<Service>();
			if (result.size() == 0) {
//						form.pic.clear();
			} else {
				final List<String> contableList = new ArrayList<String>();
				for (SuperModel model : result) {
					Service conEntity = (Service) model;
					contableList.add(conEntity.getServiceDate()+"");
					if(conEntity.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
						ComplainPresenter.serviceList.add(conEntity);
					}
				}
//						Collections.sort(contableList,new Comparator<Integer>() {
//							@Override
//							public int compare(Integer o1,Integer o2) {
//								return o2.compareTo(o1);
//							}
//						});

				/**
				 * Old COde Date 28-10-2017 BY ANIL
				 */
//				for (int i = 0; i < contableList.size(); i++) {
//					olbServiceDetails.addItem(contableList.get(i)+ "");
//				}
				
				/**
				 * Date 28-10-2017 BY ANIL
				 */
				if (ComplainPresenter.serviceList.size() != 0) {
					olbServiceDetails.setListItems(ComplainPresenter.serviceList);
//					olbServiceDetails.setEnabled(true);
				}
				
				for(int i=1;i<=olbServiceDetails.getItemCount();i++){
					
					/**
					 * old code
					 * Date 27-10-2017 BY ANIL
					 */
//					String serviceDt = olbServiceDetails.getItemText(i);
//					Date serdate=DateTimeFormat.getFormat("dd-MMM-yyyy").parse(serviceDt);
					/**
					 * Date 27-10-2017 BY ANIL
					 */
					String serviceDet = olbServiceDetails.getItemText(i);
					Console.log("SERVICE DETAILS : "+serviceDet);
					if(serviceDetails.equalsIgnoreCase(serviceDet.trim())){
						olbServiceDetails.setSelectedIndex(i);
						break;
					}
				}	
			}
		}
	});
}





public Button getNewCustomer() {
	return newCustomer;
}


public void setNewCustomer(Button newCustomer) {
	this.newCustomer = newCustomer;
}


public TextBox getTbTitle() {
	return tbTitle;
}


public void setTbTitle(TextBox tbTitle) {
	this.tbTitle = tbTitle;
}


public PersonInfoComposite getPic() {
	return pic;
}


public void setPic(PersonInfoComposite pic) {
	this.pic = pic;
}


public TextBox getTbCallerName() {
	return tbCallerName;
}


public void setTbCallerName(TextBox tbCallerName) {
	this.tbCallerName = tbCallerName;
}


public PhoneNumberBox getPnbCallerCellNo() {
	return pnbCallerCellNo;
}


public void setPnbCallerCellNo(PhoneNumberBox pnbCallerCellNo) {
	this.pnbCallerCellNo = pnbCallerCellNo;
}


public ObjectListBox<Service> getLbServiceCount() {
	return olbServiceDetails;
}


public void setLbServiceCount(ObjectListBox<Service> lbServiceCount) {
	this.olbServiceDetails = lbServiceCount;
}




	 /*******************************************Type Drop Down Logic**************************************/
		
//komal

protected void getProductDetailsFromContractId(int count , final String productName,final int assetId,final String tatSla){

MyQuerry querry = new MyQuerry();
Company c = new Company();
Vector<Filter> filtervec = new Vector<Filter>();
Filter filter = null;
filter = new Filter();
filter.setQuerryString("companyId");
filter.setLongValue(c.getCompanyId());
filtervec.add(filter);
filter = new Filter();
filter.setQuerryString("count");
filter.setIntValue(count);
filtervec.add(filter);
querry.setFilters(filtervec);
querry.setQuerryObject(new Contract());

async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
	@Override
	public void onFailure(Throwable caught) {
		olbProductDetails.addItem("--SELECT--");
		showDialogMessage("failed." + caught);
	}
	@Override
	public void onSuccess(ArrayList<SuperModel> result) {
		olbProductDetails.addItem("--SELECT--");
		if (result.size() == 0) {
//			form.pic.clear();
		} else {
			 List<String> contableList = new ArrayList<String>();
			 List<SalesLineItem> productList = new ArrayList<SalesLineItem>();
			for (SuperModel model : result) {
				Contract conEntity = (Contract) model;
				if(conEntity.getStatus().equals("Approved")){
					if(conEntity.getItems().size() >0){
						for(SalesLineItem item : conEntity.getItems()){
//							String premises = "";
//							if(item.getPremisesDetails() != null){
//								premises = "-"+item.getPremisesDetails();
//							}
							productList.add(item);
						}
					}
					
					if(conEntity.getTat()!=null) {
						custSla=conEntity.getTat();
						olbbBranch.setValue(conEntity.getBranch());
						/**
						 * @author Anil
						 * @since 08-10-2020
						 */
						if(tatSla!=null&&!tatSla.equals("")){
							tmbTatSla.setValue(tatSla);
						}else{
							tmbTatSla.setValue(conEntity.getTat());
						}
						
						calculateExpectedCompletionDateAndTime(dbDate.getValue(), tbtime.getValue(), convertTimeToNumbers(tmbTatSla.getValue()));
					}
				}
			}
			olbProductDetails.setListItems(productList);
			if(productName!=null){
				olbProductDetails.setValue(productName);
				if(assetId!=0){
					Timer timer = new Timer() {
						@Override
						public void run() {
							initialilzeAssetId();
							lbAssetId.setValue(assetId + "");
						}
					};
					timer.schedule(3000);
				}else{
					initialilzeAssetId();
				}
			}else{
				olbProductDetails.setEnabled(true);
			}
		}
	}
});
}


	public ListBox getP_servicehours() {
		return p_servicehours;
	}


	public void setP_servicehours(ListBox p_servicehours) {
		this.p_servicehours = p_servicehours;
	}


	public ListBox getP_servicemin() {
		return p_servicemin;
	}


	public void setP_servicemin(ListBox p_servicemin) {
		this.p_servicemin = p_servicemin;
	}


	public ListBox getP_ampm() {
		return p_ampm;
	}


	public void setP_ampm(ListBox p_ampm) {
		this.p_ampm = p_ampm;
	}
	private void setpopupServiceTime(String serviceTime) {

		if(serviceTime!=null && !serviceTime.equals("")&&!serviceTime.equals("Flexible")){
			
			String[] time=serviceTime.split(":");
			System.out.println(" 0 "+time[0]);
			System.out.println(" 1 "+time[1]);
			String hrs=time[0];
			String min;
			String ampm;
			if(time[1].contains("PM")){
				min=time[1].replace("PM", "");
				ampm="PM";
			}else{
				min=time[1].replace("AM", "");
				ampm="AM";
			}
			System.out.println(" 2 "+hrs+" "+min+" "+ampm);
			
			int count = getP_servicehours().getItemCount();
			String item=hrs.toString();
			System.out.println("Hours =="+item);
			for(int i=0;i<count;i++)
			{
				System.out.println("HTRRSSS =="+getP_servicehours().getItemText(i).trim());
				System.out.println(item.trim().equals(getP_servicehours().getItemText(i).trim()));
				if(item.trim().equals(getP_servicehours().getItemText(i).trim()))
				{
					getP_servicehours().setSelectedIndex(i);
					break;
				}
			}
			
			int count1 = getP_servicemin().getItemCount();
			String item1=min.toString();
			for(int i=0;i<count1;i++)
			{
				if(item1.trim().equals(getP_servicemin().getItemText(i).trim()))
				{
					getP_servicemin().setSelectedIndex(i);
					break;
				}
			}
			
			int count2 = getP_ampm().getItemCount();
			String item2=ampm.toString();
			for(int i=0;i<count2;i++)
			{
				if(item2.trim().equals(getP_ampm().getItemText(i).trim()))
				{
					getP_ampm().setSelectedIndex(i);
					break;
				}
			}
			
		}else{
			getP_servicehours().setSelectedIndex(0);
			getP_servicemin().setSelectedIndex(0);
			getP_ampm().setSelectedIndex(0);
		}
	}


	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		Console.log("inside onValueChange");
  
		
		
		
		/**
		 * Date 27-07-2018 By Vijay
		 * Des :- for Orion pest only Complain Due date must be after 48 hours date based on ticket Date
		 * does not include Sunday. It is customization
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "OnlyForOrion")){
			if(event.getSource()==dbcompaintDate){
				setDueDate(dbcompaintDate.getValue());
			}
		}
		/**
		 * ends here
		 */
		
		/**
		 * @author Anil
		 * @since 11-05-2020
		 */
		if(event.getSource().equals(dbDate)){
			System.out.println("Registration Date handler");
			dbNewServiceDate.setValue(new Date(dbDate.getValue().getTime()));
			dbAssignedDate.setValue(new Date(dbDate.getValue().getTime()));
			calculateExpectedCompletionDateAndTime(dbDate.getValue(), tmbRegistrationTime.getValue(), convertTimeToNumbers(tmbTatSla.getValue()));
		}
		
		/**
		 * @author Anil
		 * @since 01-06-2020
		 */
		if(event.getSource().equals(dbDate)||
				event.getSource().equals(dbActualCompletionDate)||
				event.getSource().equals(dbNewServiceDate)||
				event.getSource().equals(dbAssignedDate)){
			updateTatCalculation();
		}
	}
	

	public void setCustServAddress(int cCount) {
		 Console.log("inside setCustServAddress");
		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(cCount);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("An Unexpected error occurred!");				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for (SuperModel model : result) {
					Console.log("inside setCustServAddress on success");
					Customer custEntity = (Customer) model;
					custAddress=null;
					if (custEntity.getSecondaryAdress() != null) {
						taServAddress.setValue(custEntity.getSecondaryAdress().getCompleteAddress());
						custAddress=custEntity.getSecondaryAdress();
					} else {
						taServAddress.setValue("");
					}
				}
			}
		});
		
	}


	private void setDueDate(Date DueDate){
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "OnlyForOrion")){
			DateTimeFormat dateFormat = DateTimeFormat.getFormat("c");
			String dayOfWeek = dateFormat.format(DueDate);
			if(dayOfWeek.equals("6")||dayOfWeek.equals("7")){
				CalendarUtil.addDaysToDate(DueDate, 3);
			}else{
				CalendarUtil.addDaysToDate(DueDate, 2);
			}
			dbDueDate.setValue(DueDate);
		}
		
	}


	public TextBox getTbModel() {
		return tbModel;
	}


	public void setTbModel(TextBox tbModel) {
		this.tbModel = tbModel;
	}


	public TextBox getTbProSerialNo() {
		return tbProSerialNo;
	}


	public void setTbProSerialNo(TextBox tbProSerialNo) {
		this.tbProSerialNo = tbProSerialNo;
	}

	/**
	 * @author Anil,Date : 02-01-2019
	 * as creation date is only mapping at first ,
	 * after click on New button forms reinitialize but creation date is not getting mapped
	 */
	@Override
	public void setToNewState() {
		// TODO Auto-generated method stub
		super.setToNewState();
		dbDate.setValue(new Date());
		dbDate.setEnabled(false);
		
		/***@author amol,Date 10-1-2019
		 *  to avoid "please add product" message ,(set the billable date Checkbox value false)
		 *  ***/
	    cbbillable.setValue(false);
	    
	    /***
	     @author Amol, Date:10-1-2019
	     Ticket Date is same as Creation date after click on New Button form***/
	    dbcompaintDate.setValue(new Date());
	    
	    
	    /**
		 * @author Anil
		 * @since 15-05-2020
		 */
		dbDate.setValue(new Date());
		dbDate.setEnabled(false);
		
		dbNewServiceDate.setValue(new Date());
		dbAssignedDate.setValue(new Date());
		tmbAssignedTime.setValue(getCreationTime(new Date()));
		tmbRegistrationTime.setValue(getCreationTime(new Date()));
		tmbServiceTime.setValue(getCreationTime(new Date()));
		
		dbcompaintDate.setValue(new Date());
		
		tbtime.setValue(getCreationTime(new Date()));
		dbExpectedCompletionDate.setEnabled(false);
		tmbExpectedCompletionTime.setEnabled(false);
		dbActualCompletionDate.setEnabled(false);
		tmbActualCompletionTime.setEnabled(false);
		tmbTatSla.setEnabled(false);
		tmbTatActual.setEnabled(false);
		tmbTatTechnician.setEnabled(false);
		tmbTatSupervisor.setEnabled(false);
		
		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain","ComplainServiceWithTurnAroundTime")) {
			Timer timer=new Timer() {
				@Override
				public void run() {
//					setDefaultDataOfLoggedInUser();
					olbSatatus.setValue("Created");
				}
			};
			timer.schedule(4000);
			olbSatatus.setEnabled(false);
			if(UserConfiguration.getRole()!=null&&
					(UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")||
							UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator"))){
				dbDate.setEnabled(true);
				tmbRegistrationTime.setEnabled(true);
			}
			
		}
	}


	public void loadCustomerBranch(int custId,final String custBranch){
		customerBranchList=new ArrayList<CustomerBranchDetails>();
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(custId);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				oblCustomerBranch.clear();
				oblCustomerBranch.addItem("--SELECT--");
				showDialogMessage("failed." + caught);
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<CustomerBranchDetails> custBranchList=new ArrayList<CustomerBranchDetails>();
				if(result.size()!=0){
					for(SuperModel model:result){
						CustomerBranchDetails obj=(CustomerBranchDetails) model;
						custBranchList.add(obj);
						customerBranchList.add(obj);
					}
				}
				
				if(custBranchList.size()!=0){
					oblCustomerBranch.setListItems(custBranchList);
					if(custBranch!=null){
						oblCustomerBranch.setValue(custBranch);
					}else{
						oblCustomerBranch.setEnabled(true);
					}
				}else{
					oblCustomerBranch.addItem("--SELECT--");
				}
			}
		});
	}
	

	public String getCreationTime(Date creationDate) {
		String time="";
		if(creationDate!=null) {
			time=creationDate.getHours()+":"+creationDate.getMinutes();
		}
		return time;
	}
	
	public String getShiftFromTime(Shift shift) {
		String time="";
		if(shift!=null) {
			String timeInString=shift.getFromTime()+"";
			String timeArr[] =timeInString.split("\\.");
			if(timeArr.length==2) {
				String hh = timeArr[0];
				String mm = timeArr[1];
				if(mm.length()<2&&Integer.parseInt(mm)<9){
					mm=mm+0;
				}
				int mins=(Integer.parseInt(mm)*60)/100;
				time=hh+":"+mins;
			}else {
				int fromTime=shift.getFromTime().intValue();
				time=fromTime+":"+"00";
			}
//			Console.log("HH:MM "+time);
		}
		return time;
	}
	
	public String getShiftToTime(Shift shift) {
		String time="";
		if(shift!=null) {
			String timeInString=shift.getToTime()+"";
			String timeArr[] =timeInString.split("\\.");
			if(timeArr.length==2) {
				String hh = timeArr[0];
				String mm = timeArr[1];
				if(mm.length()<2&&Integer.parseInt(mm)<9){
					mm=mm+0;
				}
				int mins=(Integer.parseInt(mm)*60)/100;
				time=hh+":"+mins;
			}else {
				int fromTime=shift.getToTime().intValue();
				time=fromTime+":"+"00";
			}
//			Console.log("HH:MM "+time);
		}
		return time;
	}
	
	public void calculateExpectedCompletionDateAndTime(Date date,String time,Double hours) {
		try{
			if(date==null){
				showDialogMessage("Date should not be blank.");
				return;
			}
			if(time==null||time.equals("")){
				showDialogMessage("Time should not be blank.");
				return;
			}
			if(hours==null){
				showDialogMessage("TAT(SLA) should not be blank.");
				return;
			}
			
			long HOUR = 3600*1000;
			DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
	//		Console.log("date: "+date);
			
			String dt=AppUtility.parseDate(date);
			Date frmDate=CalendarUtil.copyDate(dateFormat.parse(dt));
	//		Console.log("frmDate: "+frmDate);
			
			String timearr[] =time.split(":");
			if(timearr.length<2) {
				return;
			}
			String hh = timearr[0];
			String mm = timearr[1];
	//		Console.log("HH:MM "+hh+":"+mm);
			
			frmDate.setHours(Integer.parseInt(hh));
			frmDate.setMinutes(Integer.parseInt(mm));
			Console.log("FRM DT : "+frmDate);
			Console.log("TAT HOURS : "+hours);
			
			/**
			 * @author Anil
			 * @since 27-05-2020
			 * If complain is registered before and after shift time then expected completion date shoule be handled
			 */
			Calendar calendar=null;
			Shift shift=null;
			if(olbbBranch.getSelectedIndex()!=0) {
				Branch branch=olbbBranch.getSelectedItem();
				if(branch.getCalendar()!=null) {
					calendar=branch.getCalendar();
				}
				if(branch.getShift()!=null) {
					shift=branch.getShift();
				}
			}
			
			if(frmDate.before(getDateWithShiftStartDate(frmDate, shift))){
				Console.log("Reg Date is less than shift start datetime");
				checkForWoAndHoliday(frmDate, calendar,shift);
				frmDate=getDateWithShiftStartDate(frmDate, shift);
			}else if(frmDate.after(getDateWithShiftEndDate(frmDate, shift))){
				Console.log("Reg Date is greater than shift end datetime");
				if(getDifferenceDays(getDateWithShiftEndDate(frmDate, shift),frmDate)==0){
					CalendarUtil.addDaysToDate(frmDate, 1);
				}
				checkForWoAndHoliday(frmDate, calendar,shift);
				frmDate=getDateWithShiftStartDate(frmDate, shift);
			}else{
				frmDate=checkForWoAndHoliday(frmDate, calendar,shift);
			}
			
			Console.log("Updated frmDate: "+frmDate);
			
			long updatedDate=(long) (frmDate.getTime()+(hours*HOUR));
			Date completionDate=new Date(updatedDate);
			Console.log("completionDate: "+completionDate);
			
			
			Date shiftEndTimeDt=CalendarUtil.copyDate(frmDate);
//			Date shiftEndTimeDt=CalendarUtil.copyDate(dateFormat.parse(dt));
	//		Console.log("BF shiftEndTimeDt: "+shiftEndTimeDt);
			shiftEndTimeDt=getDateWithShiftEndDate(shiftEndTimeDt, shift);
	//		Console.log("AF shiftEndTimeDt: "+shiftEndTimeDt);
			double updatedHour=getDifferenceInHours(shiftEndTimeDt,frmDate,convertTimeToNumbers(tmbTatSla.getValue()));
			
			Console.log("Remaining Hours : "+updatedHour);
			
	//		Date shiftEndTimeDate=getDateWithShiftEndDate(completionDate, shift);
	//		Console.log("shiftEndTimeDate: "+shiftEndTimeDate);
			
			if(completionDate.after(shiftEndTimeDt)) {
	//		if(completionDate.getHours()>shiftEndTimeDt.getHours()) {
				if(getDifferenceDays(shiftEndTimeDt, completionDate)==0){
					CalendarUtil.addDaysToDate(completionDate, 1);
				}
				completionDate=getUpdatedDate(shift, completionDate, updatedHour, calendar);
			}
			
			if(calendar!=null) {
				
				if(isDateFallsOnWo(completionDate, calendar)) {
					Console.log("WO: "+completionDate);
					CalendarUtil.addDaysToDate(completionDate, 1);
					completionDate=getUpdatedDate(shift,completionDate,updatedHour,calendar);
				}
				if(calendar.isHolidayDate(dateFormat.parse(dateFormat.format(completionDate)))) {
					Console.log("HOLYDAY: "+completionDate);
					CalendarUtil.addDaysToDate(completionDate, 1);
					completionDate=getUpdatedDate(shift,completionDate,updatedHour,calendar);
				}
			}
			Console.log("FINAL completionDate: "+completionDate);
			dbExpectedCompletionDate.setValue(completionDate);
			String completionTime=completionDate.getHours()+":"+completionDate.getMinutes();
			Console.log("completionTime: "+completionTime);
			tmbExpectedCompletionTime.setValue(completionTime);
		
		}catch(Exception e){
			
		}
	}

	private Date getUpdatedDate(Shift shift,Date date, double updatedHour,Calendar cal) {
		long HOUR = 3600*1000;
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
		String dt=AppUtility.parseDate(date);
		Date frmDate=CalendarUtil.copyDate(dateFormat.parse(dt));
		Console.log("frmDate: "+frmDate);
		frmDate=getDateWithShiftStartDate(frmDate, shift);
		Console.log("frmDate: "+frmDate);
		
		long updatedDate=(long) (frmDate.getTime()+(updatedHour*HOUR));
		Date completionDate=new Date(updatedDate);
		Console.log("completionDate: "+completionDate);
		
		Date shiftEndTimeDate=getDateWithShiftEndDate(completionDate, shift);
		Console.log("shiftEndTimeDate: "+shiftEndTimeDate);
		
		if(completionDate.after(shiftEndTimeDate)) {
			Console.log("inside completion greater");
			double updatedHour1=getDifferenceInHours(completionDate,shiftEndTimeDate,null);
			CalendarUtil.addDaysToDate(completionDate, 1);
			completionDate=getUpdatedDate(shift, completionDate, updatedHour1, cal);
		}
		
		
		if(cal.isHolidayDate(dateFormat.parse(dateFormat.format(completionDate)))) {
			Console.log("Holiday recursive calling");
			CalendarUtil.addDaysToDate(completionDate, 1);
			completionDate=getUpdatedDate(shift, completionDate, updatedHour, cal);
		}
		
		if(isDateFallsOnWo(completionDate, cal)) {
			Console.log("WO: "+completionDate);
			CalendarUtil.addDaysToDate(completionDate, 1);
			completionDate=getUpdatedDate(shift,completionDate,updatedHour,cal);
		}
		return completionDate;
	}


	private double getDifferenceInHours(Date maxDate, Date minDate, Double tat) {
		Console.log("Max DT : "+maxDate);
		Console.log("Min DT : "+minDate);
		long secs = (maxDate.getTime() - minDate.getTime()) / 1000;
		
		int hours = (int) (secs / 3600);  
		Console.log("hours: "+hours);
		secs = secs % 3600;
		int mins = (int) (secs / 60);
		Console.log("mins: "+mins);
//		secs = secs % 60;
		
		double minutesInNum=mins/60.0;
		Console.log("minutesInNum : "+minutesInNum);
//		String hoursInString=(hours+minutesInNum)+"";
//		double remainingHours=Double.parseDouble(hoursInString);
		double consumedHours=hours+minutesInNum;
		Console.log("consumedHours : "+consumedHours);
		double remainingHours=0;
		if(tat!=null){
			Console.log("TAT : "+tat);
			remainingHours=tat-consumedHours;
		}else{
			remainingHours=consumedHours;
		}
		Console.log("remainingHours : "+remainingHours);
		if(Math.signum(remainingHours)==-1.0){
			remainingHours=remainingHours*-1;
		}
		return remainingHours;
	}


	public boolean isDateFallsOnWo(Date date,Calendar cal){
		boolean isFallOnWo=false;
//		Calendar cal=empInfo.getLeaveCalendar();
		if(cal!=null){
			ArrayList<Integer> dayList=new ArrayList<Integer>();
			if(cal.isSUNDAY()){
				dayList.add(0);
			}
			if(cal.isMONDAY()){
				dayList.add(1);
			}
			if(cal.isTUESDAY()){
				dayList.add(2);
			}
			if(cal.isWEDNESDAY()){
				dayList.add(3);
			}
			if(cal.isTHRUSDAY()){
				dayList.add(4);
			}
			if(cal.isFRIDAY()){
				dayList.add(5);
			}
			if(cal.isSATAURDAY()){
				dayList.add(6);
			}
			
			int day=date.getDay();
			if(dayList.contains(day)){
				isFallOnWo=true;
				return isFallOnWo;
			}
		}
		
		return isFallOnWo;
	}
	
	
	public Date getDateWithShiftEndDate(Date date,Shift shift) {
		Date date1=CalendarUtil.copyDate(date);
//		Console.log("date: "+date1);
		String shiftEndTime=getShiftToTime(shift);
		String endTimeArr[] =shiftEndTime.split(":");
		String e_hh = endTimeArr[0];
		String e_mm = endTimeArr[1];
//		Console.log("HH:MM "+e_hh+":"+e_mm);
		
		date1.setHours(Integer.parseInt(e_hh));
		date1.setMinutes(Integer.parseInt(e_mm));
//		Console.log("shift end date: "+date1);
		return date1;
	}
	
	public Date getDateWithShiftStartDate(Date date,Shift shift) {
		Date date1=CalendarUtil.copyDate(date);
//		Console.log("date: "+date1);
		String shiftEndTime=getShiftFromTime(shift);
		String endTimeArr[] =shiftEndTime.split(":");
		String e_hh = endTimeArr[0];
		String e_mm = endTimeArr[1];
//		Console.log("HH:MM "+e_hh+":"+e_mm);
		
		date1.setHours(Integer.parseInt(e_hh));
		date1.setMinutes(Integer.parseInt(e_mm));
//		Console.log("shift start date: "+date1);
		return date1;
	}
	
	
	public String convert24Hrsto12HrsFormat(String timeIn24){
		String timeIn12 = null;
		DateTimeFormat fmt24Hrs= DateTimeFormat.getFormat("HH:mm");
		DateTimeFormat fmt12Hrs= DateTimeFormat.getFormat("hh:mm a");
		timeIn12=fmt12Hrs.format(fmt24Hrs.parse(timeIn24));
		timeIn12 = timeIn12.replaceAll("\\s","");
		return timeIn12;
	}
	
	public String convert12Hrsto24HrsFormat(String timeIn12){
		String timeIn24 = null;
		DateTimeFormat fmt24Hrs= DateTimeFormat.getFormat("HH:mm");
		DateTimeFormat fmt12Hrs= DateTimeFormat.getFormat("hh:mm a");
		timeIn24=fmt24Hrs.format(fmt12Hrs.parse(timeIn12));
		timeIn24 = timeIn24.replaceAll("\\s","");
		return timeIn24;
	}
	
	public int getDifferenceDays(Date d1, Date d2) {
	    int daysdiff = 0;
//	    long diff = d2.getTime() - d1.getTime();
//	    long diffDays = diff / (24 * 60 * 60 * 1000) + 1;
//	    daysdiff = (int) diffDays;
	    daysdiff = CalendarUtil.getDaysBetween(d1,d2);
	    Console.log("getDifferenceDays: "+daysdiff);
	    return daysdiff;
	}
	
	public Double convertTimeToNumbers(String time){
//		Console.log("TAT HH:MM "+time);
		Double hrsInNum=null;
		try{
			String timeArr[] =time.split(":");
			String hh = timeArr[0];
			String mm = timeArr[1];
			double mins=Integer.parseInt(mm)/60.0;
			hrsInNum=Integer.parseInt(hh)+mins;
//			Console.log("TAT(SLA) "+hrsInNum);
		}catch(Exception e){
			
		}
		return hrsInNum;
	}


	@Override
	public void clear() {
		super.clear();
		tmbActualCompletionTime.clear();
		tmbAssignedTime.clear();
		tmbExpectedCompletionTime.clear();
		tmbRegistrationTime.clear();
		tmbServiceTime.clear();
		tmbTatActual.clear();
		tmbTatSla.clear();
		tmbTatSupervisor.clear();
		tmbTatTechnician.clear();
		
		mfgDate=null;
		replacementDate=null;
		
	}
	
	public Date checkForWoAndHoliday(Date date,Calendar calendar,Shift shift){
		Console.log("Passed Date : "+date);
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
		boolean updateFlag=false;
		if(isDateFallsOnWo(date, calendar)) {
			Console.log("WO: "+date);
			CalendarUtil.addDaysToDate(date, 1);
			updateFlag=true;
		}
		if(calendar.isHolidayDate(dateFormat.parse(dateFormat.format(date)))) {
			Console.log("HOLYDAY: "+date);
			CalendarUtil.addDaysToDate(date, 1);
			updateFlag=true;
		}
		if(updateFlag){
			date=getDateWithShiftStartDate(date, shift);
			Console.log("updateFlag : "+date);
			checkForWoAndHoliday(date, calendar,shift);
			Console.log("after updateFlag : "+date);
		}
		
		return date;
	}
	
	public void updateTatCalculation(){
		Console.log("updateTatCalculation : ");
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		if(olbSatatus.getSelectedIndex()!=0){
			if(!olbSatatus.getValue(olbSatatus.getSelectedIndex()).equals("Completed")){
				return;
			}
		}else{
			return;
		}
		Console.log("updateTatCalculation 1 : ");
		Date regDate=new Date(dbDate.getValue().getTime());
		String regTime=tmbRegistrationTime.getValue();
		
		Date completionDate=new Date(dbActualCompletionDate.getValue().getTime());
		String completionTime=tmbActualCompletionTime.getValue();
		
		Date serviceDate=new Date(dbNewServiceDate.getValue().getTime());
		String serviceTime=tmbServiceTime.getValue();
		
		Date assignDate=new Date(dbAssignedDate.getValue().getTime());
		String assignTime=tmbAssignedTime.getValue();
		
		if(regDate==null||regTime==null){
			showDialogMessage("Please select registration date and time.");
			return;
		}
		
		if(serviceDate==null||serviceTime==null){
			showDialogMessage("Please select service date and time.");
			return;
		}
		
		if(assignDate==null||assignTime==null){
			showDialogMessage("Please select assign date and time.");
			return;
		}
		
		if(completionDate==null||completionTime==null){
			showDialogMessage("Please select completion date and time.");
			return;
		}
		
		/**
		 * 
		 */
		Console.log("updateTatCalculation 2 : ");
		
//		String dt=AppUtility.parseDate(regDate);
		regDate=CalendarUtil.copyDate(regDate);
		
		Console.log("regDate : "+regDate+" regTime : "+regTime);
		
		String timearr[] =regTime.split(":");
		Console.log(timearr.length+"");
		if(timearr.length<2) {
			return;
		}
		String hh = timearr[0];
		String mm = timearr[1];
		regDate.setHours(Integer.parseInt(hh));
		regDate.setMinutes(Integer.parseInt(mm));
		Console.log("FRM DT : "+regDate);
		
		Calendar calendar=null;
		Shift shift=null;
		if(olbbBranch.getSelectedIndex()!=0) {
			Branch branch=olbbBranch.getSelectedItem();
			if(branch.getCalendar()!=null) {
				calendar=branch.getCalendar();
			}
			if(branch.getShift()!=null) {
				shift=branch.getShift();
			}
		}
		
		if(regDate.before(getDateWithShiftStartDate(regDate, shift))){
			Console.log("Reg Date is less than shift start datetime");
			checkForWoAndHoliday(regDate, calendar,shift);
			regDate=getDateWithShiftStartDate(regDate, shift);
		}else if(regDate.after(getDateWithShiftEndDate(regDate, shift))){
			Console.log("Reg Date is greater than shift end datetime");
			if(getDifferenceDays(getDateWithShiftEndDate(regDate, shift),regDate)==0){
				CalendarUtil.addDaysToDate(regDate, 1);
			}
			checkForWoAndHoliday(regDate, calendar,shift);
			regDate=getDateWithShiftStartDate(regDate, shift);
		}else{
			checkForWoAndHoliday(regDate, calendar,shift);
		}
		regTime=regDate.getHours()+":"+regDate.getMinutes();
		Console.log("Updated regDate/time: "+regDate+" / "+regTime);
		
		/**
		 * 
		 */
		
		
		tmbTatTechnician.setValue(convertNumbersToTime(calculateTurnAroundTime(completionDate,completionTime,serviceDate,serviceTime,shift,calendar)));
		tmbTatActual.setValue(convertNumbersToTime(calculateTurnAroundTime(completionDate,completionTime, regDate, regTime,shift,calendar)));
		tmbTatSupervisor.setValue(convertNumbersToTime(calculateTurnAroundTime(assignDate,assignTime, regDate, regTime,shift,calendar)));
	
		
	}
	
	
	public double calculateTurnAroundTime(Date date1,String fromTime,Date date2,String toTime,Shift shift, Calendar calendar){
		DateTimeFormat isoFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		Date fromDate=new Date(date1.getTime());
		Date toDate=new Date(date2.getTime());
		double tat=0;
		
		String fromTimeArr[] =fromTime.split(":");
		String f_hh = fromTimeArr[0];
		String f_mm = fromTimeArr[1];
		fromDate.setHours(Integer.parseInt(f_hh));
		fromDate.setMinutes(Integer.parseInt(f_mm));
		Console.log("from Date1 "+fromDate);
		
		String toTimeArr[] =toTime.split(":");
		String t_hh = toTimeArr[0];
		String t_mm = toTimeArr[1];
		toDate.setHours(Integer.parseInt(t_hh));
		toDate.setMinutes(Integer.parseInt(t_mm));
		Console.log("to Date1 "+toDate);
		
		int noOfDays=getDifferenceDays(toDate, fromDate);
		Console.log("NO OF DAYS "+noOfDays);
		if(noOfDays>0&&shift!=null){
			Date startTimeOfShift=getDateWithShiftStartDate(new Date(fromDate.getTime()),shift);
			Date endTimeOfShift=getDateWithShiftEndDate(new Date(toDate.getTime()),shift);
			
			Console.log("startTimeOfShift "+startTimeOfShift);
			Console.log("endTimeOfShift "+endTimeOfShift);
			
			double tat1=getHoursBetweenTwoDates(fromDate, startTimeOfShift);
			Console.log("tat1 "+tat1);
			double tat2=getHoursBetweenTwoDates(endTimeOfShift, toDate);
			Console.log("tat2 "+tat2);
			tat=tat1+tat2;
			
			Console.log("BEFORE "+tat);
			
			if(noOfDays>1) {
				
				Date frmDt=new Date(date2.getTime());
				Date toDt=new Date(date1.getTime());
				frmDt=isoFormat.parse(isoFormat.format(frmDt));
				toDt=isoFormat.parse(isoFormat.format(toDt));
				Console.log("frm dt"+frmDt);
				Console.log("to dt"+toDt);
//				frmDt=DateUtility.addDaysToDate(frmDt, 1);
				CalendarUtil.addDaysToDate(frmDt,1);
				while(frmDt.before(toDt)) {
					Console.log("frm dt1 "+frmDt);
					tat=tat+isWoOrHoliday(frmDt, calendar);
					CalendarUtil.addDaysToDate(frmDt,1);
				}
				Console.log("INSIDE MORE THAN ONE DAYS "+tat);
			}
		}else{
			tat=getHoursBetweenTwoDates(fromDate, toDate);
			
			/***
			 * @author Anil
			 * @since 10-12-2020
			 * if service is completing on weekly off and holiday then tat should be zero
			 * raised by Rahul Tiwari for PTSPL
			 */
			
			if(isWoOrHoliday(fromDate, calendar)==0){
				tat=0;
			}
		}
		Console.log("BF TAT : "+tat);
		tat=Math.round(tat* 100.0) / 100.0;
		Console.log("TAT : "+tat);
		return tat;
	}
	
	public double getHoursBetweenTwoDates(Date date1,Date date2){
		double diff=0;
		Date fromDate=new Date(date1.getTime());
		Date toDate=new Date(date2.getTime());
		long secs =0;
		if(fromDate.after(toDate)){
			secs = (fromDate.getTime() - toDate.getTime()) / 1000;
		}else if(fromDate.before(toDate)){
			secs = (toDate.getTime() - fromDate.getTime()) / 1000;
		}else{
			secs = (fromDate.getTime() - toDate.getTime()) / 1000;
		}
		int hours = (int) (secs / 3600);  
		secs = secs % 3600;
		int mins = (int) (secs / 60);
		secs = secs % 60;
		double minutesInNum=mins/60.0;
		diff=hours+minutesInNum;
		return diff;
	}
	
	public double isWoOrHoliday(Date date,Calendar calendar){
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
		Date date1=new Date(date.getTime());
		if(calendar!=null&&isDateFallsOnWo(date, calendar)){
			Console.log("WO");
			return 0;
		}
		if(calendar!=null&&calendar.isHolidayDate(dateFormat.parse(dateFormat.format(date)))){
			Console.log("HOLIDAYY");
			return 0;
		}
		return calendar.getWorkingHours();
	}
	
	public String convertNumbersToTime(double hours){
		Console.log("hours "+hours);
		String hrsInString=null;
		String time=hours+"";
		if(time.contains(".")){
			try{
				String timeArr[] =time.split("\\.");
				String hh = timeArr[0];
				String mm = timeArr[1];
				if(mm.length()<2&&Integer.parseInt(mm)<9){
					mm=mm+0;
				}
				int mins=(Integer.parseInt(mm)*60)/100;
				hrsInString=hh+":"+mins;
				Console.log("TAT HH:MM "+hrsInString);
			}catch(Exception e){
				
			}
		}else{
			hrsInString=hours+":00";
			Console.log("Does not contains decimal "+hrsInString);
		}
		return hrsInString;
	}
	
	public void updateCustomerBranch(){
		if(contractCount.getSelectedIndex()!=0){
			Contract contract=contractCount.getSelectedItem();
			HashSet<String> custBranchHs=new HashSet<String>();
			if(contract!=null){
				if(olbProductDetails.getSelectedIndex()!=0){
					SalesLineItem item=olbProductDetails.getSelectedItem();
					if(item.getCustomerBranchSchedulingInfo()!=null){
						for(BranchWiseScheduling branchObj:item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo())){
							if(branchObj.isCheck()){
								custBranchHs.add(branchObj.getBranchName());
							}
						}
					}
				}else{
					for(SalesLineItem item:contract.getItems()){
						if(item.getCustomerBranchSchedulingInfo()!=null){
							for(BranchWiseScheduling branchObj:item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo())){
								if(branchObj.isCheck()){
									custBranchHs.add(branchObj.getBranchName());
								}
							}
						}
					}
				}
				ArrayList<String> custBranch=new ArrayList<String>(custBranchHs);
				if(custBranch.size()!=0){
					List<CustomerBranchDetails> list=new ArrayList<CustomerBranchDetails>();
					for(String branch:custBranch){
						for(CustomerBranchDetails custBranchObj:customerBranchList){
							if(branch.equals(custBranchObj.getBusinessUnitName())){
								list.add(custBranchObj);
								break;
							}
						}
					}
					if(list.size()!=0){
						oblCustomerBranch.setListItems(list);
					}
				}
			}
		}else{
			if(customerBranchList.size()!=0){
				oblCustomerBranch.setListItems(customerBranchList);
			}
		}
	}
	
	public void initialilzeAssetId(){
		lbAssetId.clear();
		lbAssetId.addItem("--SELECT--");
		if(olbProductDetails.getSelectedIndex()!=0){
			SalesLineItem item=olbProductDetails.getSelectedItem();
			List<ComponentDetails> componentList=new ArrayList<ComponentDetails>();
			if(item!=null){
				for(BranchWiseScheduling branch:item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo())){
					if(branch.isCheck()){
						if(oblCustomerBranch.getSelectedIndex()!=0){
							if(branch.getBranchName().equals(oblCustomerBranch.getValue())&&branch.getComponentList()!=null&&branch.getComponentList().size()!=0){
								componentList.addAll(branch.getComponentList());
							}
						}else{
							if(branch.getComponentList()!=null&&branch.getComponentList().size()!=0){
								componentList.addAll(branch.getComponentList());
							}
						}
						
					}
				}
				
				if(componentList.size()!=0){
					lbAssetId.setListItems(componentList);
				}
			}
		}
	}


	public DateBox getServiceDate() {
		return serviceDate;
	}


	public void setServiceDate(DateBox serviceDate) {
		this.serviceDate = serviceDate;
	}


	public TextBox getTbservicetime() {
		return tbservicetime;
	}


	public void setTbservicetime(TextBox tbservicetime) {
		this.tbservicetime = tbservicetime;
	}

	
	
	
}
