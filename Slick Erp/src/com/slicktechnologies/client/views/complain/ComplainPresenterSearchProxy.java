package com.slicktechnologies.client.views.complain;

import java.util.Vector;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.complain.ComplainPresenter.ComplainPresenterSearch;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class ComplainPresenterSearchProxy extends ComplainPresenterSearch {
	public IntegerBox count;
	public DateBox regDate;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Employee> olbEmployee;
	
	public ObjectListBox<Employee> olbSalesPerson;
	
	ListBox cbBillable;
	
	public ObjectListBox<Config> olbComplainStatus;
	
	public PersonInfoComposite personInfo;
	
	/**
	 * Date : 12-07-2017 By ANIL
	 * added from and to date search for NBHC CCPM
	 */
	public DateComparator dateComparator;
	
	/**
	 * End
	 */
	
	/**
	 * Date 01-12-2018 By Vijay
	 * Des :- For compalin Ticket Category which is same to 
	 * Raise ticket Category from Support screen
	 */
	ObjectListBox<ConfigCategory> olbTicketCategory;
	
	public ComplainPresenterSearchProxy()
	{
		super();
		createGui();
	}
	public void initWidget()
	{
		count=new IntegerBox();
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		olbEmployee= new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERSUPPORT, "Assigned To");
		
		olbSalesPerson= new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbSalesPerson);
		olbSalesPerson.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERSUPPORT, "Sales Person");
		
		regDate= new DateBoxWithYearSelector();
//		completeDate=new DateBoxWithYearSelector();
		
		cbBillable=new ListBox();
		cbBillable.addItem("--SELECT--");
		cbBillable.addItem("Billable");
		cbBillable.addItem("Non Billable");
		
		
		olbComplainStatus=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbComplainStatus, Screen.COMPLAINSTATUS);
		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		
		dateComparator= new DateComparator("complainDate",new Contract());//date
		
		/**
		 * Date 01-12-2018 By Vijay
		 * Des :- Ticket Category search filter
		 */ 
		olbTicketCategory = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbTicketCategory, Screen.TICKETCATEGORY);
		/**
		 * ends here
		 */
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder("Ticket No",count);
		FormField ticketno= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Ticket Date",regDate);
		FormField rdate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Sales Person",olbSalesPerson);
		FormField olbSalesPerson= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Billable",cbBillable);
		FormField cbBillable= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Assign To",olbEmployee);
		FormField folbEmployee= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
//		builder = new FormFieldBuilder("Completed Date",completeDate);
//		FormField cdate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		builder = new FormFieldBuilder("Complain Status",olbComplainStatus);
		FormField folbQuotationStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		builder = new FormFieldBuilder("From Date (Reported Date)",dateComparator.getFromDate());
		FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (Reported Date)",dateComparator.getToDate());
		FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 25-01-2018 By Vijay 
		 * differentiated date combination search and other filter search
		 */
		builder = new FormFieldBuilder();
		FormField fgroupingDateFilterInformation=builder.setlabel("Date Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		builder = new FormFieldBuilder();
		FormField fgroupingOtherFilterInformation=builder.setlabel("Other Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		/**
		 * ends here
		 */
		
		/**
		 * Date 01-12-2018 By Vijay
		 * Des :- added Ticket Category 
		 */
		builder = new FormFieldBuilder("Ticket Category",olbTicketCategory);
		FormField folbTicketCategory = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				{fgroupingDateFilterInformation},
				{fdateComparator,fdateComparator1,folbBranch,folbQuotationStatus},
				{fgroupingOtherFilterInformation},
				{ticketno,olbSalesPerson,folbEmployee,cbBillable},
				{fpersonInfo},
				{folbTicketCategory}
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(personInfo.getIdValue()!=-1)
		{
			System.out.println("CUSTOMER ID NOT NULL ::: "+personInfo.getIdValue());
			temp=new Filter();
			temp.setIntValue(personInfo.getIdValue());
			temp.setQuerryString("personinfo.count");
			filtervec.add(temp);
		}
		  
		if(!(personInfo.getFullNameValue().equals("")))
		{
			System.out.println("CUSTOMER NAME NOT NULL "+personInfo.getFullNameValue());
			temp=new Filter();
			temp.setStringValue(personInfo.getFullNameValue());
			temp.setQuerryString("personinfo.fullName");
			filtervec.add(temp);
		}
		if(personInfo.getCellValue()!=-1l)
		{
			System.out.println("CUSTOMER CELL NOT NULL "+personInfo.getCellValue());
			temp=new Filter();
			temp.setLongValue(personInfo.getCellValue());
			temp.setQuerryString("personinfo.cellNumber");
			filtervec.add(temp);
		}
		
		if(count.getValue()!=null){
			System.out.println("TICKET ID NOT NULL ::: "+count.getValue());
			temp=new Filter();
			temp.setIntValue(count.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		  
		if(olbEmployee.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbEmployee.getValue().trim());
			temp.setQuerryString("assignto");
			filtervec.add(temp);
		}
		
		if(olbSalesPerson.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbSalesPerson.getValue().trim());
			temp.setQuerryString("SalesPerson");
			filtervec.add(temp);
		}
		
		if(cbBillable.getSelectedIndex()!=0){
			if(cbBillable.getValue(cbBillable.getSelectedIndex()).equals("Billable")){
				temp=new Filter();
				temp.setBooleanvalue(true);;
				temp.setQuerryString("bbillable");
				filtervec.add(temp);
			}else{
				temp=new Filter();
				temp.setBooleanvalue(false);;
				temp.setQuerryString("bbillable");
				filtervec.add(temp);
			}
			
		}
		
		if (LoginPresenter.branchRestrictionFlag == true
				&& olbBranch.getSelectedIndex() == 0) {
			temp = new Filter();
			temp.setList(AppUtility.getbranchlist());
			temp.setQuerryString("branch IN");
			filtervec.add(temp);
		} else {
			if(olbBranch.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbBranch.getValue().trim());
				temp.setQuerryString("branch");
				filtervec.add(temp);
			}
		}
		
		
		
		if(olbComplainStatus.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbComplainStatus.getValue().trim());
			temp.setQuerryString("compStatus");
			filtervec.add(temp);
		}
		
		if(regDate.getValue()!=null){
			temp=new Filter();
			temp.setDateValue(regDate.getValue());
			temp.setQuerryString("date");
			filtervec.add(temp);
		}
		
		/**
		 * Date : 12-07-2017 BY Anil
		 */
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		/**
		 * End
		 */
		
		/**
		 * Date 01-12-2018 By Vijay
		 * Des :- added Ticket Category 
		 */
		if(olbTicketCategory.getSelectedIndex()!=0){
			temp = new Filter();
			temp.setStringValue(olbTicketCategory.getValue());
			temp.setQuerryString("Category");
			filtervec.addAll(filtervec);
		}
		/**
		 * ends here
		 */
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Complain());
		return querry;
	}
	@Override
	public boolean validate() {
//		if(LoginPresenter.branchRestrictionFlag)
//		{
//		if(olbBranch.getSelectedIndex()==0)
//		{
//			showDialogMessage("Select Branch");
//			return false;
//		}
//		}
		
		/**
		 * Date 25-01-2018 By Vijay 
		 * for Date combination validation
		 */
		String msg = "Wrong filter selection with From Date and To Date ! Please select proper filters with Date";
		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null ){
			if(personInfo.getIdValue()!=-1 || !(personInfo.getFullNameValue().equals("")) || personInfo.getCellValue()!=-1l ||
					count.getValue()!=null || olbEmployee.getSelectedIndex()!=0 || olbSalesPerson.getSelectedIndex()!=0 ||	
					cbBillable.getSelectedIndex()!=0 || regDate.getValue()!=null  ){
				showDialogMessage(msg);
				return false;
			}
		}
		/**
		 * ends here
		 */
		
		
		
		
		//Ashwini Patil Date:25-01-2024
		boolean filterAppliedFlag=false;
		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null||
				olbBranch.getSelectedIndex()!=0 ||olbComplainStatus.getSelectedIndex()!=0 ||olbTicketCategory.getSelectedIndex()!=0 ||
				olbEmployee.getSelectedIndex()!=0 || personInfo.getIdValue()!=-1	|| !(personInfo.getFullNameValue().equals("")) || personInfo.getCellValue()!=-1l ||
			    olbSalesPerson.getSelectedIndex()!=0 ||
			    cbBillable.getSelectedIndex()!=0 ||
			    count.getValue()!= null ||regDate.getValue()!= null){
			
							filterAppliedFlag=true;
		}
						
		if(!filterAppliedFlag){
				showDialogMessage("Please apply at least one filter!");
				return false;
		}
					
		if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)){
				int diffdays = AppUtility.getDifferenceDays(dateComparator.getFromDate().getValue(), dateComparator.getToDate().getValue());
				Console.log("diffdays "+diffdays);
				if(diffdays>31){
						showDialogMessage("Please select from date and to date range within 30 days");
						return false;
				}
		}
				
		return true;
	}
	
	
	
}
