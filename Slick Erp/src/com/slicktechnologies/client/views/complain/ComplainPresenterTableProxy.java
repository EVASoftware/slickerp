package com.slicktechnologies.client.views.complain;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.complain.ComplainPresenter.ComplainPresenterTable;
import com.slicktechnologies.shared.common.complain.Complain;

public class ComplainPresenterTableProxy extends ComplainPresenterTable {

	


	
	TextColumn<Complain>getColumnTicketID;
	TextColumn<Complain>getColumnRegDate;
	TextColumn<Complain>getColumnCustomerCount;
	TextColumn<Complain>getColumnCustomerFullName;
	TextColumn<Complain>getColumnCustomerCell
	;
	TextColumn<Complain>getColumnSalesperson;
	TextColumn<Complain>getColumnassinto;
	TextColumn<Complain>getColumnBranch;
	TextColumn<Complain>getColumnBillable;
	
//	TextColumn<Complain>getColumnComDate;
	TextColumn<Complain>getColumnStatus;
	
	public ComplainPresenterTableProxy() {
		
		super();
	
	}
	
	@Override 	
	public void createTable() {
		createColumnTicketID();
		createColumnRegDate();
		createColumnCustId();
		createColumnCustNm();
		createColumnCustPh();
		createColumnSalesPer();
//		createColumnComDate();
		createColumnAssignTo();
		createColumnBranch();
		createColumnbillable();
		createColumnStatus();
		
	}

	private void createColumnCustId() {
		getColumnCustomerCount=new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				if(object.getPersoninfo()!=null){
					return object.getPersoninfo().getCount()+"";
				}
//				else{
//					return object.getCustCount()+"";
//				}
				return "";
			}
		};
		table.addColumn(getColumnCustomerCount, "Customer Id");
		getColumnCustomerCount.setSortable(true);
	}

	private void createColumnCustNm() {
		getColumnCustomerFullName=new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				if(object.getPersoninfo()!=null){
					return object.getPersoninfo().getFullName();
				}
//				else{
//					return object.getCustFullName()+"";
//				}
				return "";
			}
		};
		table.addColumn(getColumnCustomerFullName, "Customer Name");
		getColumnCustomerFullName.setSortable(true);
	}

	private void createColumnCustPh() {
		getColumnCustomerCell=new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				if(object.getPersoninfo()!=null){
					return object.getPersoninfo().getCellNumber()+"";
				}
//				else{
//					return object.getCustCell()+"";
//				}
				return "";
				
			}
		};
		table.addColumn(getColumnCustomerCell, "Customer Cell");
		getColumnCustomerCell.setSortable(true);
	}

	private void createColumnSalesPer() {
		getColumnSalesperson=new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				if(object.getSalesPerson()!=null){
					return object.getSalesPerson();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getColumnSalesperson, "Sales Person");
		getColumnSalesperson.setSortable(true);
	}

	private void createColumnbillable() {
		getColumnBillable=new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				if(object.getBbillable()!=null){
					if(object.getBbillable()==true){
						return "Billable";
					}
					else{
						return "Non Billable";
					}
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getColumnBillable, "Billable");
		getColumnBillable.setSortable(true);
	}

	private void createColumnRegDate() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
		getColumnRegDate=new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				/**
				 * Date : 14-12-2018 BY ANIL
				 * Earlier in place of ticket date we were showing creation date,replaced it with ticket date 
				 */
				if(object.getComplainDate()!=null){
					return fmt.format(object.getComplainDate());
				}else{
					return "";
				}
			}
		};
		table.addColumn(getColumnRegDate, "Ticket Date");
		getColumnRegDate.setSortable(true);
	}

//	private void createColumnComDate() {
//		
//		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
//		getColumnComDate=new TextColumn<Complain>() {
//			@Override
//			public String getValue(Complain object) {
//				if(object.getServiceDate()!=null){
//					return fmt.format(object.getServiceDate());
//				}else{
//					return null;
//				}
//			}
//		};
//		table.addColumn(getColumnComDate, "Complete Date");
//		getColumnComDate.setSortable(true);
//	}

	private void createColumnTicketID() {
		getColumnTicketID=new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getColumnTicketID, "Ticket No.");
		getColumnTicketID.setSortable(true);
		
	}

	private void createColumnBranch() {
		getColumnBranch=new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				return object.getBranch();
			}
		};
		table.addColumn(getColumnBranch, "Branch");
		getColumnBranch.setSortable(true);
	}

	private void createColumnStatus() {
		getColumnStatus=new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				if(object.getCompStatus()!=null){
					return object.getCompStatus();
				}else{
					return null;
				}
			}
		};
		table.addColumn(getColumnStatus, "Status");
		getColumnStatus.setSortable(true);
	}

	private void createColumnAssignTo() {
		getColumnassinto=new TextColumn<Complain>() {
			@Override
			public String getValue(Complain object) {
				if(object.getAssignto()!=null){
					return object.getAssignto();
				}else{
					return null;
				}
			}
		};
		table.addColumn(getColumnassinto, "Assigned To");
		getColumnassinto.setSortable(true);
	}
	
	
	
	public void addColumnSorting() {
		addColumnTicketNoSorting();
		addColumnRegdateSorting();
		
		addColumnCustomerIdSorting();
		addColumnCustomerNameSorting();
		addColumnCustomerCellSorting();
		
		addColumnSalesPersonSorting();
		addColumnAssigntoSorting();
		addColumnBranchSorting();
		
		addColumnBillableSorting();
//		addColumnCompDateSorting();
		addColumnStatusSorting();
		
		}

	private void addColumnCustomerIdSorting() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
//				if (e1 != null && e2 != null) {
					if(e1.getPersoninfo()!=null){
						if(e1.getPersoninfo().getCount()== e2.getPersoninfo().getCount()){
						  return 0;
						}
						if(e1.getPersoninfo().getCount()> e2.getPersoninfo().getCount()){
						  return 1;
						}
						else{
						  return -1;
						}
//					}
//					else{
//						if(e1.getCustCount()== e2.getCustCount()){
//							  return 0;
//							}
//							if(e1.getCustCount()> e2.getCustCount()){
//							  return 1;
//							}
//							else{
//							  return -1;
//							}
//					}
				} 
				else
					return 0;
			}
		};
		columnSort.setComparator(getColumnCustomerCount, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnCustomerNameSorting() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
					if(e1.getPersoninfo().getFullName()!=null){
//						if (e1.getPersoninfo().getFullName() != null&& e2.getPersoninfo().getFullName() != null)
							return e1.getPersoninfo().getFullName().compareTo(e2.getPersoninfo().getFullName());
//						return 0;
					}
//							else{
//						if (e1.getCustFullName() != null&& e2.getCustFullName() != null)
//							return e1.getCustFullName().compareTo(e2.getCustFullName());
						return 0;
//					}
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnCustomerFullName, quote);
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnCustomerCellSorting() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
//					if(e1.getPersoninfo()!=null){
						if(e1.getPersoninfo().getCellNumber()== e2.getPersoninfo().getCellNumber()){
						  return 0;
						}
						if(e1.getPersoninfo().getCellNumber()> e2.getPersoninfo().getCellNumber()){
						  return 1;
						}
						else{
						  return -1;
						}
//					}else{
//						if(e1.getCustCell()== e2.getCustCell()){
//							  return 0;
//							}
//							if(e1.getCustCell()> e2.getCustCell()){
//							  return 1;
//							}
//							else{
//							  return -1;
//							}
//					}
				} 
				else
					return 0;
			}
		};
		columnSort.setComparator(getColumnCustomerCell, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSalesPersonSorting() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
					if (e1.getSalesPerson() != null
							&& e2.getSalesPerson() != null)
						return e1.getSalesPerson().compareTo(
								e2.getSalesPerson());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnSalesperson, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnBillableSorting() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBbillable() != null&& e2.getBbillable() != null)
						return e1.getBbillable().compareTo(e2.getBbillable());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnBillable, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnRegdateSorting() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
					if(e1.getDate()== e2.getDate()){
					  return 0;}
				  if(e1.getDate().after(e2.getDate())){
					  return 1;}
				  else{
					  return -1;}} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnRegDate, quote);
		table.addColumnSortHandler(columnSort);
	}

//	private void addColumnCompDateSorting() {
//		List<Complain> list = getDataprovider().getList();
//		columnSort = new ListHandler<Complain>(list);
//		Comparator<Complain> quote = new Comparator<Complain>() {
//			@Override
//			public int compare(Complain e1, Complain e2) {
//				if (e1 != null && e2 != null) {
//					if(e1.getServiceDate()== e2.getServiceDate()){
//					  return 0;}
//				  if(e1.getServiceDate().after(e2.getServiceDate())){
//					  return 1;}
//				  else{
//					  return -1;}} else
//					return 0;
//			}
//		};
//		columnSort.setComparator(getColumnComDate, quote);
//		table.addColumnSortHandler(columnSort);
//	}

	private void addColumnTicketNoSorting() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
					if(e1.getCount()== e2.getCount()){
					  return 0;}
				  if(e1.getCount()> e2.getCount()){
					  return 1;}
				  else{
					  return -1;}} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnTicketID, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnBranchSorting() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null
							&& e2.getBranch() != null)
						return e1.getBranch().compareTo(
								e2.getBranch());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnBranch, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnStatusSorting() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCompStatus() != null
							&& e2.getCompStatus() != null)
						return e1.getBranch().compareTo(
								e2.getBranch());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnStatus, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnAssigntoSorting() {
		List<Complain> list = getDataprovider().getList();
		columnSort = new ListHandler<Complain>(list);
		Comparator<Complain> quote = new Comparator<Complain>() {
			@Override
			public int compare(Complain e1, Complain e2) {
				if (e1 != null && e2 != null) {
					if (e1.getAssignto() != null
							&& e2.getAssignto() != null)
						return e1.getAssignto().compareTo(
								e2.getAssignto());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnassinto, quote);
		table.addColumnSortHandler(columnSort);
		
	}
	
	
	
	
	
	

	
	
}
