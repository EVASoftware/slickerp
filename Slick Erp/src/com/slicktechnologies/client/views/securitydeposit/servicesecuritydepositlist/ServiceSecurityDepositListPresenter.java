package com.slicktechnologies.client.views.securitydeposit.servicesecuritydepositlist;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ServiceSecurityDepositListPresenter extends TableScreenPresenter<Quotation> {
	
	TableScreen<Quotation> form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final GenricServiceAsync async=GWT.create(GenricService.class);
	
	
public ServiceSecurityDepositListPresenter(TableScreen<Quotation> view,Quotation model) {
		super(view, model);
		form=view;
		form.getSuperTable().connectToLocal();
		view.retriveTable(getsecuritydepositListQuery());
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SERVICESECURITYDEPOSITLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}


public ServiceSecurityDepositListPresenter(TableScreen<Quotation> view,Quotation model,MyQuerry querry) {
	super(view, model);
	form=view;
	view.retriveTable(querry);
	
	boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SERVICESECURITYDEPOSITLIST,LoginPresenter.currentModule.trim());
	if(isDownload==false){
		form.getSearchpopupscreen().getDwnload().setVisible(false);
	}
}



	private MyQuerry getsecuritydepositListQuery() {
	
		Vector<Filter> filterec=null;
		
			filterec=new Vector<Filter>();
		Filter temp=null;
		  
		  temp=new Filter();
		  temp.setStringValue(AppConstants.YES);
		  temp.setQuerryString("isDepositPaid");
		  filterec.add(temp);
		
		  temp=new Filter();
		  temp.setLongValue(model.getCompanyId());
		  temp.setQuerryString("companyId");
		  filterec.add(temp);
		  
		  temp=new Filter();
		  temp.setBooleanvalue(false);
		  temp.setQuerryString("isDepositReceived");
		  filterec.add(temp);
		  
			MyQuerry quer=new MyQuerry();
			 quer.setFilters(filterec);
			  quer.setQuerryObject(new Quotation());
			  filterec=new Vector<Filter>();
			return quer;
	}
			
			@Override
			protected void makeNewModel() {
				
				model=new Quotation();
			}

			
			public static void initialize(){
				
				ServiceSecurityDepositListTableProxy table=new ServiceSecurityDepositListTableProxy();
				ServiceSecurityDepositListForm form=new ServiceSecurityDepositListForm(table);
				ServiceSecurityDepositListSearchProxy.staticSuperTable=table;
				ServiceSecurityDepositListSearchProxy searchPopUp=new ServiceSecurityDepositListSearchProxy(false);
				form.setSearchpopupscreen(searchPopUp);
				ServiceSecurityDepositListPresenter presenter=new ServiceSecurityDepositListPresenter(form, new Quotation());
				form.setToNewState();
				AppMemory.getAppMemory().stickPnel(form);
			}
			
			public static void initalize(MyQuerry querry)
				{
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Services/Security Deposit List",Screen.QUOTATION);
					ServiceSecurityDepositListTableProxy table=new ServiceSecurityDepositListTableProxy();
					ServiceSecurityDepositListForm form=new ServiceSecurityDepositListForm(table);
					ServiceSecurityDepositListSearchProxy.staticSuperTable=table;
					ServiceSecurityDepositListSearchProxy searchPopUp=new ServiceSecurityDepositListSearchProxy(false);
					form.setSearchpopupscreen(searchPopUp);
					ServiceSecurityDepositListPresenter presenter=new ServiceSecurityDepositListPresenter(form, new Quotation(),querry);
					form.setToNewState();
					AppMemory.getAppMemory().stickPnel(form);
				}
			
			
			
			@Override
			public void reactOnDownload() {
				CsvServiceAsync async=GWT.create(CsvService.class);
				ArrayList<Quotation> custarray=new ArrayList<Quotation>();
				List<Quotation> list=form.getSuperTable().getListDataProvider().getList();
				
				custarray.addAll(list); 
				
				
				async.setServiceSecurityDeposit(custarray, new AsyncCallback<Void>() {
			
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed"+caught);
						
					}
			
					@Override
					public void onSuccess(Void result) {
						String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url=gwt + "csvservlet"+"?type="+78;
						Window.open(url, "test", "enabled");
					}
				});
			}
			
			
			@Override
			public void reactOnPrint() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void reactOnEmail() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void reactOnSearch() {
				super.reactOnSearch();
				ServiceSecurityDepositListSearchProxy.depostatus.setValue(false);
			}
	
	}
