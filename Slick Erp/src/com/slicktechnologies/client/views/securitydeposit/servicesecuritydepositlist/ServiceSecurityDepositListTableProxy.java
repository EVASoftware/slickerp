package com.slicktechnologies.client.views.securitydeposit.servicesecuritydepositlist;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.views.securitydeposit.securitydepositpopup.DepositAmountReceivedPopUp;
import com.slicktechnologies.shared.Quotation;

public class ServiceSecurityDepositListTableProxy extends SuperTable<Quotation> implements ClickHandler {
	
	TextColumn<Quotation>getColumnConPerQuoid;
	TextColumn<Quotation>getColumnConPerCinfoID;
	TextColumn<Quotation>getColumnConPerCinfoName;
	TextColumn<Quotation>getColumnConPerCinfoCell;
	TextColumn<Quotation>getColumnConPerPaymethod;
	TextColumn<Quotation>getColumnConPerSecuritydepo;
	TextColumn<Quotation>getColumnConPerDepositstatus;
	Column<Quotation,String>getColumnConPerReceived;
	TextColumn<Quotation>getColumnConPerRemark;
	
	
	final GenricServiceAsync async=GWT.create(GenricService.class);
	int quotCount=0;
	String remark="";
	Date recidate=null;
	
	int rowIndex; 
	DepositAmountReceivedPopUp popup=new DepositAmountReceivedPopUp();
	PopupPanel panel ;
	
	
	public ServiceSecurityDepositListTableProxy() {
		
		super();
	
		addFieldUpdater();
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
		setHeight("800px");
		
	}

		@Override
		public void createTable() {
		
			createColumnConPerQuoid();
			createColumnConPerCinfoID();
			createColumnConPerCinfoName();
			createColumnConPerCinfoCell();
			createColumnConPerPaymethod();
			createColumnConPerSecuritydepo();
			createColumnConPerDepositstatus();
			createColumnConPerReceived();
			createColumnConPerRemark();
			
			
		}
		
		private void createColumnConPerQuoid() {
		
			getColumnConPerQuoid=new TextColumn<Quotation>() {
				
				@Override
				public String getValue(Quotation object) {
					return object.getCount()+"";
				}
			};
			table.addColumn(getColumnConPerQuoid, "Quoation Id");
			getColumnConPerQuoid.setSortable(true);
			
			
		}
		
		private void createColumnConPerPaymethod() {
		
			getColumnConPerPaymethod=new TextColumn<Quotation>() {
		
				@Override
				public String getValue(Quotation object) {
					return object.getSdPaymentMethod();
				}
			};
			table.addColumn(getColumnConPerPaymethod, "Payment Method");
			getColumnConPerPaymethod.setSortable(true);
			
		}
		
		private void createColumnConPerSecuritydepo() {
		
			getColumnConPerSecuritydepo=new TextColumn<Quotation>() {
		
				@Override
				public String getValue(Quotation object) {
					return object.getSdDepositAmount()+"";
				}
			};
			table.addColumn(getColumnConPerSecuritydepo, "Amount");
			getColumnConPerSecuritydepo.setSortable(true);
			
		}
		
		private void createColumnConPerDepositstatus() {
		
			getColumnConPerDepositstatus=new TextColumn<Quotation>() {
		
				@Override
				public String getValue(Quotation object) {
					if(object.isDepositReceived()==true){
						String str="Received";
						return str;
					}else{
						String str="Not Received";
						return str;
					}
				}
			};
			table.addColumn(getColumnConPerDepositstatus, "Deposit Status");
			getColumnConPerDepositstatus.setSortable(true);
			
			
		}
		
		private void createColumnConPerCinfoName() {
		
			getColumnConPerCinfoName=new TextColumn<Quotation>() {
				
				@Override
				public String getValue(Quotation object) {
					return object.getCinfo().getFullName();
				}
			};
			table.addColumn(getColumnConPerCinfoName, "Customer Name");
			getColumnConPerCinfoName.setSortable(true);
			
		}
		
		private void createColumnConPerCinfoCell() {
		
			getColumnConPerCinfoCell=new TextColumn<Quotation>() {
				
				@Override
				public String getValue(Quotation object) {
					return object.getCinfo().getCellNumber()+"";
				}
			};
			table.addColumn(getColumnConPerCinfoCell, "Customer Cell");
			getColumnConPerCinfoCell.setSortable(true);
			
			
		}
		
		private void createColumnConPerCinfoID() {
		
			getColumnConPerCinfoID=new TextColumn<Quotation>() {
				
				@Override
				public String getValue(Quotation object) {
					return object.getCinfo().getCount()+"";
				}
			};
			table.addColumn(getColumnConPerCinfoID, "Customer Id");
			getColumnConPerCinfoID.setSortable(true);
			
		}
		
		
		private void createColumnConPerReceived(){
			ButtonCell btnCell= new ButtonCell();
			getColumnConPerReceived = new Column<Quotation, String>(btnCell) {
				
				@Override
				public String getValue(Quotation object) {
					
					if(object.isDepositReceived()==false){
						quotCount=object.getCount();
						return "Return Deposit";
					}else{
					
					return null;
					}
				}
			};
			table.addColumn(getColumnConPerReceived, "Deposit Receive");
		}
		
		/*private void createColumnConPerReceived() {
		
			ButtonCell btnCell= new ButtonCell();
			getColumnConPerReceived = new Column<Quotation, String>() {
			}; 
			{
				
				@Override
				public String getValue(Quotation object) {
		
					if(object.isDepoStatus()==false){
						quotCount=object.getCount();
						return "Return Deposit";
					}else{
						return null;
					}
				
				}
			};
			table.addColumn(getColumnConPerReceived, "Deposit Receive");
		}*/
		
		private void createColumnConPerRemark() {
		
			getColumnConPerRemark=new TextColumn<Quotation>() {
				
				@Override
				public String getValue(Quotation object) {
					return object.getSdRemark();
				}
			};
			table.addColumn(getColumnConPerRemark, "Remark");
			
		}
		
			private void setFieldUpdaterOnReceived() {
		
			
			getColumnConPerReceived.setFieldUpdater(new FieldUpdater<Quotation, String>() {
				
				@Override
				public void update(int index, Quotation object, String value) {
					
		//			popup.getReceiDate().setValue(object.getRecivDate().getValue(object));
					popup.getSalesQuotCount().setValue(object.getCount()+"");
					popup.getRemark().setValue(getColumnConPerRemark.getValue(object));
					panel=new PopupPanel(true);
					panel.add(popup);
					panel.show();
					panel.center();
					rowIndex=index;	
				}
			});
		}
		
		public void addColumnSorting() {
			addColumnQuotionidSorting();
			addColumnCinfoIDSorting();
			addColumnCinfoNameSorting();
			addColumnCinfoCellSorting();
			addColumnPaymethodSorting();
			addColumnSecuritydepoSorting();
			addColumnDepostatusSorting();
			
			}
		
		private void addColumnQuotionidSorting() {
		
			List<Quotation> list = getDataprovider().getList();
			columnSort = new ListHandler<Quotation>(list);
			Comparator<Quotation> quote = new Comparator<Quotation>() {
				@Override
				public int compare(Quotation e1, Quotation e2) {
					if (e1 != null && e2 != null) {
						if(e1.getCount()== e2.getCount()){
						  return 0;}
					  if(e1.getCount()> e2.getCount()){
						  return 1;}
					  else{
						  return -1;}} else
						return 0;
				}
			};
			columnSort.setComparator(getColumnConPerQuoid, quote);
			table.addColumnSortHandler(columnSort);
			
			
		}
		
		private void addColumnPaymethodSorting() {
		
			List<Quotation> list = getDataprovider().getList();
			columnSort = new ListHandler<Quotation>(list);
			Comparator<Quotation> quote = new Comparator<Quotation>() {
				@Override
				public int compare(Quotation e1, Quotation e2) {
					if (e1 != null && e2 != null) {
						if (e1.getSdPaymentMethod()!= null&& e2.getSdPaymentMethod() != null)
							return e1.getSdPaymentMethod().compareTo(e2.getSdPaymentMethod());
						return 0;
					} else
						return 0;
				}
			};
			columnSort.setComparator(getColumnConPerPaymethod, quote);
			table.addColumnSortHandler(columnSort);
			
			
		}
		
		private void addColumnSecuritydepoSorting() {
		
			List<Quotation> list = getDataprovider().getList();
			columnSort = new ListHandler<Quotation>(list);
			Comparator<Quotation> quote = new Comparator<Quotation>() {
				@Override
				  public int compare(Quotation e1,Quotation e2)
				  {
				  if(e1!=null && e2!=null)
				  {
				  if(e1.getSdDepositAmount()== e2.getSdDepositAmount()){
				  return 0;}
				  if(e1.getSdDepositAmount()> e2.getSdDepositAmount()){
				  return 1;}
				  else{
				  return -1;}
				  }
				  else{
				  return 0;}
				  }
			};
			columnSort.setComparator(getColumnConPerSecuritydepo, quote);
			table.addColumnSortHandler(columnSort);
			
		}
		
		private void addColumnDepostatusSorting() {
		
			List<Quotation> list=getDataprovider().getList();
			columnSort=new ListHandler<Quotation>(list);
			columnSort.setComparator(getColumnConPerDepositstatus, new Comparator<Quotation>()
					{
				@Override
				public int compare(Quotation e1,Quotation e2)
				{
					if(e1!=null && e2!=null)
					{
						if(e1.isDepositReceived()!=null && e2.isDepositReceived()!=null){
							return e1.isDepositReceived().compareTo(e2.isDepositReceived());}
					}
					else{
						return 0;}
					return 0;
				}
					});
			table.addColumnSortHandler(columnSort);;
			
			
		} 
		
		private void addColumnCinfoIDSorting() {
		
			List<Quotation> list = getDataprovider().getList();
			columnSort = new ListHandler<Quotation>(list);
			Comparator<Quotation> quote = new Comparator<Quotation>() {
				@Override
				public int compare(Quotation e1, Quotation e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCinfo().getCount() != 0
								&& e2.getCinfo().getCount() != 0)
							return e1.getCinfo().getCount()+"".compareTo(e2.getCinfo().getCount()+"");
						return 0;
					} else
						return 0;
				}
			};
			columnSort.setComparator(getColumnConPerCinfoID, quote);
			table.addColumnSortHandler(columnSort);
			
			
		}
		
		private void addColumnCinfoNameSorting() {
		
			List<Quotation> list = getDataprovider().getList();
			columnSort = new ListHandler<Quotation>(list);
			Comparator<Quotation> quote = new Comparator<Quotation>() {
				@Override
				public int compare(Quotation e1, Quotation e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCinfo().getFullName() != null
								&& e2.getCinfo().getFullName() != null)
							return e1.getCinfo().getFullName().compareTo(e2.getCinfo().getFullName());
						return 0;
					} else
						return 0;
				}
			};
			columnSort.setComparator(getColumnConPerCinfoName, quote);
			table.addColumnSortHandler(columnSort);
			
		}
		
		private void addColumnCinfoCellSorting() {
		
			
			List<Quotation> list = getDataprovider().getList();
			columnSort = new ListHandler<Quotation>(list);
			Comparator<Quotation> quote = new Comparator<Quotation>() {
				@Override
				public int compare(Quotation e1, Quotation e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCinfo().getCellNumber() != null
								&& e2.getCinfo().getCellNumber() != null)
							return e1.getCinfo().getCellNumber().compareTo(e2.getCinfo().getCellNumber());
						return 0;
					} else
						return 0;
				}
			};
			columnSort.setComparator(getColumnConPerCinfoCell, quote);
			table.addColumnSortHandler(columnSort);
			
		}
		
		
		
		
		
		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub
			setFieldUpdaterOnReceived();
		}
		
		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onClick(ClickEvent event) {
		
			if(event.getSource()==popup.getBtnOk()){
				
				
				ArrayList<Quotation> salesArray= new ArrayList<Quotation>();
				salesArray.addAll(getDataprovider().getList());
				
				System.out.println(salesArray.get(rowIndex).getSdRemark()+"  b4........................ ");
				salesArray.get(rowIndex).setSdRemark(popup.getRemark().getValue());
				
				salesArray.get(rowIndex).setSdReturnDate(popup.getDbReturnDate().getValue());
				System.out.println(popup.getRemark().getValue());
				System.out.println(salesArray.get(rowIndex).getSdRemark()+"  after........................ ");
				
				
				remark=popup.getRemark().getValue();
				recidate=popup.getDbReturnDate().getValue();
				changeStatus(quotCount,remark,recidate);
				panel.hide();
				
				
			}else if(event.getSource()==popup.getBtnCancel()){
				panel.hide();
				popup.remark.setValue("");
			}
			
		}
		
		public void changeStatus(int countVal,final String remarkVal,final Date recidate)
		{
			MyQuerry querry=new MyQuerry();
			Filter filtercount=new Filter();
			filtercount.setQuerryString("count");
			filtercount.setIntValue(countVal);
			querry.getFilters().add(filtercount);
			querry.setQuerryObject(new Quotation());
			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
		
				@Override
				public void onFailure(Throwable caught) {
					
				}
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel smodel:result)
					{
						final Quotation bdentity = (Quotation)smodel;
						bdentity.setDepositReceived(true);
						bdentity.setSdRemark(remarkVal);
						
						bdentity.setSdReturnDate(recidate);
						async.save(bdentity,new AsyncCallback<ReturnFromServer>() {
		
							@Override
							public void onFailure(Throwable caught) {
								
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								
								System.out.println("Success");
								getListDataProvider().refresh();
								getListDataProvider().getList().get(rowIndex).setDepositReceived(true);
								getListDataProvider().getList().get(rowIndex).setSdReturnDate(recidate);
							}
						});
					}
				}
			});
		}
		
		

}
