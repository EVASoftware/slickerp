package com.slicktechnologies.client.views.securitydeposit.servicesecuritydepositlist;

import java.util.Vector;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.IntegerBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class ServiceSecurityDepositListSearchProxy extends SearchPopUpScreen<Quotation> {
	

	public static CheckBox depostatus;
	public PersonInfoComposite personInfoComposite;
	public IntegerBox quot_id;
	public ObjectListBox<Config> paymethod;
	
	public ServiceSecurityDepositListSearchProxy()
	{
		super();
		createGui();
	}
	public ServiceSecurityDepositListSearchProxy(boolean b) {
		super(b);
		createGui();
	}
	
	
	public void initWidget()
	{
		paymethod= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(paymethod, Screen.PAYMENTMETHODS);
		quot_id=new IntegerBox();
		depostatus= new CheckBox();
		depostatus.setValue(false);
		MyQuerry custquerry = new MyQuerry();
		custquerry.setQuerryObject(new Customer());
		personInfoComposite = new PersonInfoComposite(custquerry);
	}
	
	public void createScreen() {
		initWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder("Quotation Id",quot_id);
		FormField Quoation= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
	
		fbuilder = new FormFieldBuilder("", personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Received Deposit?", depostatus);
		FormField deposit= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
	
		
		fbuilder = new FormFieldBuilder("Payment Method", paymethod);
		FormField paymethode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		FormField[][] formfield = {
				{Quoation,paymethode,deposit},
				{fpersonInfoComposite}
		};
		this.fields=formfield;
	}
	
	
	@Override
	public MyQuerry getQuerry() 
	{
		
		Vector<Filter> filtervec=new Vector<Filter>();
		  Filter temp=null;
		  temp=new Filter();
		  temp.setStringValue(AppConstants.YES);
		  temp.setQuerryString("isDepositPaid");
		  filtervec.add(temp);
		  
		  if(quot_id.getValue()!=null)
		  {
			  temp=new Filter();
			  temp.setIntValue(quot_id.getValue());
			  temp.setQuerryString("count");
			  filtervec.add(temp);
		  }
		  temp=new Filter();
		  
		  if(depostatus.getValue()==true)
		  {
			temp.setBooleanvalue(depostatus.getValue());
		  }
		  else
		  {
			temp.setBooleanvalue(depostatus.getValue());
		  }
		  	temp.setQuerryString("isDepositReceived");
			filtervec.add(temp);
		  
		  
		  if(paymethod.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(paymethod.getValue().trim());
				temp.setQuerryString("sdPaymentMethod");
				filtervec.add(temp);
			}
		  
		  
		  if(personInfoComposite.getIdValue()!=-1)
		  {
			  temp=new Filter();
			  temp.setIntValue(Integer.parseInt(personInfoComposite.getId().getValue()));
			  temp.setQuerryString("cinfo.count");
			  filtervec.add(temp);
		  }
		  
		  if(!(personInfoComposite.getFullNameValue().equals("")))
		  {
			  temp=new Filter();
			  temp.setStringValue(personInfoComposite.getFullNameValue());
			  temp.setQuerryString("cinfo.fullName");
			  filtervec.add(temp);
		  }
		  if(personInfoComposite.getCellValue()!=-1l)
		  {
			  temp=new Filter();
			  temp.setLongValue(personInfoComposite.getCellValue());
			  temp.setQuerryString("cinfo.cellNumber");
			  filtervec.add(temp);
		  }
		  
		  
		  
		  MyQuerry querry= new MyQuerry();
		  querry.setFilters(filtervec);
		  querry.setQuerryObject(new Quotation());
		  return querry;
	}
	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}
	

}
