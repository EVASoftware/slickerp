package com.slicktechnologies.client.views.securitydeposit.securitydepositpopup;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;

public class DepositAmountReceivedPopUp extends AbsolutePanel {
	

	TextBox salesQuotCount;
	public TextArea remark;
	DateBox dbReturnDate;
	Button btnCancel,btnOk;
	
	public DepositAmountReceivedPopUp() {

		btnCancel= new Button("Cancel");
		btnOk= new Button("Ok");
		
		InlineLabel remark1 = new InlineLabel("Receive Security Deposit");
		add(remark1,100,10);
		remark1.getElement().getStyle().setFontSize(18, Unit.PX);
		
		
		InlineLabel processType = new InlineLabel("Quotation Id");
		add(processType,10,50);
		salesQuotCount= new TextBox();
		add(salesQuotCount,10,70);
		
		salesQuotCount.setSize("100px", "18px");
		salesQuotCount.setEnabled(false);
		
		
		
		InlineLabel processId = new InlineLabel("Return Date");
		add(processId,150,50);
		dbReturnDate= new DateBoxWithYearSelector();
		DateTimeFormat format=DateTimeFormat.getFormat("dd-MM-yyyy");
		DateBox.DefaultFormat defformat=new DateBox.DefaultFormat(format);
		dbReturnDate.setFormat(defformat);
		add(dbReturnDate,150,70);
		
		dbReturnDate.setSize("100px", "18px");
		dbReturnDate.setEnabled(true);
		
		
		InlineLabel Remark = new InlineLabel("Remark");
		add(Remark,10,110);
		remark= new TextArea();
		add(remark,10,140);
		
		remark.setSize("250px", "30px");
		
		add(btnOk,100,200);
		add(btnCancel,200,200);
		setSize("400px", "250px");
		this.getElement().setId("form");

	
	}

	

	
	/**************************************************Getter and Setter*****************************************************/
	public TextBox getSalesQuotCount() {
		return salesQuotCount;
	}

	public void setSalesQuotCount(TextBox salesQuotCount) {
		this.salesQuotCount = salesQuotCount;
	}

	public TextArea getRemark() {
		return remark;
	}

	public void setRemark(TextArea remark) {
		this.remark = remark;
	}

	public DateBox getDbReturnDate() {
		return dbReturnDate;
	}

	public void setDbReturnDate(DateBox dbReturnDate) {
		this.dbReturnDate = dbReturnDate;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}
	
	

}
