package com.slicktechnologies.client.views.securitydeposit.securitydepositpopup;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class SecurityDepositPopUp extends AbsolutePanel implements CompositeInterface{
	
		private TextBox p_docid;
		private TextBox p_docstatus;
		private TextBox p_amount;
		private DoubleBox p_securityDeposite;
		private CheckBox p_depstatus;
		private DateBox p_reDate;
		private TextBox p_bankName;
		private TextBox p_branch;
		private TextBox p_paybleAt;
		private TextBox p_favouring;
		private TextBox p_checknumber;
		private DateBox p_chekDate;
		private TextBox p_servisNo;
		private AddressComposite p_address;
		private TextBox p_accountNo;
		private TextBox p_ifscode;
		private TextBox p_micrCode;
		private TextBox p_swiftcode;
		private TextArea p_tainstruct;
		private ObjectListBox<Config> p_paymethod;
		private Button btnCancel,btnsavedetails,btnOk;
		
		/**************************************Constructor*******************************************/
		
		public SecurityDepositPopUp() 
		{
			btnCancel = new Button("Cancel");
			btnOk=new Button("Ok");
			p_address=new AddressComposite();
			
			InlineLabel laqoution=new InlineLabel("Quotation");
			laqoution.getElement().getStyle().setFontSize(20, Unit.PX);
			add(laqoution,260,10);
				
			InlineLabel ladocid=new InlineLabel("Document Id");
			add(ladocid, 10, 50);
			p_docid = new TextBox();
			add(p_docid, 10, 70);
			
			p_docid.setSize("100px", "16px");
			p_docid.setEnabled(false);
			
			InlineLabel docstats=new InlineLabel("Document Status");
			add(docstats, 150, 50);
			
			p_docstatus = new TextBox();
			add(p_docstatus, 150, 70);
			p_docstatus.setSize("100px", "16px");
			p_docstatus.setEnabled(false);
			
			
			InlineLabel secudeposite=new InlineLabel("Security Deposit");
			add(secudeposite,10,100);
			
			p_securityDeposite=new DoubleBox();
			add(p_securityDeposite,10,120);
			p_securityDeposite.setSize("100px", "16px");
			
			InlineLabel redate=new InlineLabel("Return Date");
			add(redate,150,100);
			
			p_reDate = new DateBoxWithYearSelector();
			DateTimeFormat format1=DateTimeFormat.getFormat("dd-MM-yyyy");
			DateBox.DefaultFormat defformat1=new DateBox.DefaultFormat(format1);
			p_reDate.setFormat(defformat1);
			add(p_reDate, 150, 120);
			p_reDate.setSize("100px", "16px");
			p_reDate.setEnabled(false);
			
			InlineLabel depostatus=new InlineLabel("Deposit Return");
			add(depostatus,300,100);
			
			p_depstatus=new CheckBox();
			add(p_depstatus,300,120);
			p_depstatus.setSize("100px", "16px");
			p_depstatus.setEnabled(false);
			
			InlineLabel paymethd=new InlineLabel("Payment Method");
			add(paymethd,10,150);
			
			p_paymethod=new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(p_paymethod, Screen.PAYMENTMETHODS);
			add(p_paymethod,10,170);
			p_paymethod.setSize("100px", "24px");
			
			InlineLabel account=new InlineLabel("Account No.");
			add(account,10,200);
			
			p_accountNo = new TextBox();
			p_accountNo.setMaxLength(14);
			add(p_accountNo,10,220);
			p_accountNo.setSize("100px", "16px");
			
			InlineLabel bankname=new InlineLabel("Bank Name");
			add(bankname,150,200);
			
			p_bankName =new TextBox();
			add(p_bankName,150,220);
			p_bankName.setSize("100px", "16px");
			
			InlineLabel brach=new InlineLabel("Branch");
			add(brach,300,200);
			
			p_branch = new TextBox();
			add(p_branch,300,220);
			p_branch.setSize("100px", "16px");
			
			InlineLabel paybleat=new InlineLabel("Payable At");
			add(paybleat,450,200);
			
			p_paybleAt = new TextBox();
			add(p_paybleAt,450,220);
			p_paybleAt.setSize("100px", "16px");
			
			InlineLabel faouring=new InlineLabel("Favouring");
			add(faouring,10,250);
			
			p_favouring = new TextBox();
			add(p_favouring,10,270);
			p_favouring.setSize("100px", "16px");
			
			InlineLabel checkNo=new InlineLabel("Cheque Number");
			add(checkNo,150,250);
			
			p_checknumber = new TextBox();
			add(p_checknumber,150,270);
			p_checknumber.setSize("100px", "16px");
			
			InlineLabel daate=new InlineLabel("Cheque Date");
			add(daate,300,250);
			
			p_chekDate = new DateBoxWithYearSelector();
			DateTimeFormat format2=DateTimeFormat.getFormat("dd-MM-yyyy");
			DateBox.DefaultFormat defformat2=new DateBox.DefaultFormat(format2);
			p_chekDate.setFormat(defformat2);
			add(p_chekDate,300,270);
			p_chekDate.setSize("100px", "16px");
			
			InlineLabel serNo=new InlineLabel("Reference No");
			add(serNo,450,250);
			
			p_servisNo = new TextBox();
			add(p_servisNo,450,270);
			p_servisNo.setSize("100px", "16px");
			
			
			InlineLabel ifccode=new InlineLabel("IFSC Code");
			add(ifccode,10,300);
			
			p_ifscode = new TextBox();
			add(p_ifscode,10,320);
			p_ifscode.setSize("100px", "16px");
			
			
			InlineLabel mriccode=new InlineLabel("MICR Code");
			add(mriccode,150,300);
			
			p_micrCode = new TextBox();
			add(p_micrCode,150,320);
			p_micrCode.setSize("100px", "16px");
			
			InlineLabel switf=new InlineLabel("SWIFT Code");
			add(switf,300,300);
			
			p_swiftcode = new TextBox();
			add(p_swiftcode,300,320);
			p_swiftcode.setSize("100px", "16px");
			
			add(p_address,10,350);
			
			InlineLabel instru=new InlineLabel("Comment");
			add(instru,10,510);
			
			p_tainstruct = new TextArea();
			add(p_tainstruct, 10, 530);
			p_tainstruct.setSize("550px", "30px");
			
			add(btnOk, 200, 575);
			add(btnCancel,350,575);
			
			setSize("600px","610px");
			this.getElement().setId("form");	
			
		}

		@Override
		public void setEnable(boolean status) {
			
		}

		@Override
		public boolean validate() {
			return false;
		}

		
/******************************************Getters And Setters********************************************//////		
		public TextBox getP_docid() {
			return p_docid;
		}


		public void setP_docid(TextBox p_docid) {
			this.p_docid = p_docid;
		}

		public TextBox getP_docstatus() {
			return p_docstatus;
		}


		public void setP_docstatus(TextBox p_docstatus) {
			this.p_docstatus = p_docstatus;
		}


		public TextBox getP_amount() {
			return p_amount;
		}


		public void setP_amount(TextBox p_amount) {
			this.p_amount = p_amount;
		}

		public CheckBox getP_depstatus() {
			return p_depstatus;
		}


		public void setP_depstatus(CheckBox p_depstatus) {
			this.p_depstatus = p_depstatus;
		}


		public DateBox getP_reDate() {
			return p_reDate;
		}

		public void setP_reDate(DateBox p_reDate) {
			this.p_reDate = p_reDate;
		}


		public TextBox getP_bankName() {
			return p_bankName;
		}


		public void setP_bankName(TextBox p_bankName) {
			this.p_bankName = p_bankName;
		}


		public TextBox getP_branch() {
			return p_branch;
		}


		public void setP_branch(TextBox p_branch) {
			this.p_branch = p_branch;
		}


		public TextBox getP_paybleAt() {
			return p_paybleAt;
		}


		public void setP_paybleAt(TextBox p_paybleAt) {
			this.p_paybleAt = p_paybleAt;
		}


		public TextBox getP_favouring() {
			return p_favouring;
		}


		public void setP_favouring(TextBox p_favouring) {
			this.p_favouring = p_favouring;
		}

		public DateBox getP_chekDate() {
			return p_chekDate;
		}


		public void setP_chekDate(DateBox p_chekDate) {
			this.p_chekDate = p_chekDate;
		}



		public AddressComposite getP_address() {
			return p_address;
		}


		public void setP_address(AddressComposite p_address) {
			this.p_address = p_address;
		}


	
		public TextBox getP_ifscode() {
			return p_ifscode;
		}


		public void setP_ifscode(TextBox p_ifscode) {
			this.p_ifscode = p_ifscode;
		}


		public TextBox getP_micrCode() {
			return p_micrCode;
		}


		public void setP_micrCode(TextBox p_micrCode) {
			this.p_micrCode = p_micrCode;
		}


		public TextBox getP_swiftcode() {
			return p_swiftcode;
		}


		public void setP_swiftcode(TextBox p_swiftcode) {
			this.p_swiftcode = p_swiftcode;
		}


		public TextArea getP_tainstruct() {
			return p_tainstruct;
		}


		public void setP_tainstruct(TextArea p_tainstruct) {
			this.p_tainstruct = p_tainstruct;
		}


		public ObjectListBox<Config> getP_paymethod() {
			return p_paymethod;
		}


		public void setP_paymethod(ObjectListBox<Config> p_paymethod) {
			this.p_paymethod = p_paymethod;
		}


		public Button getBtnCancel() {
			return btnCancel;
		}


		public void setBtnCancel(Button btnCancel) {
			this.btnCancel = btnCancel;
		}


		public Button getBtnsavedetails() {
			return btnsavedetails;
		}


		public void setBtnsavedetails(Button btnsavedetails) {
			this.btnsavedetails = btnsavedetails;
		}


		public Button getBtnOk() {
			return btnOk;
		}


		public void setBtnOk(Button btnOk) {
			this.btnOk = btnOk;
		}

		public DoubleBox getP_securityDeposite() {
			return p_securityDeposite;
		}

		public void setP_securityDeposite(DoubleBox p_securityDeposite) {
			this.p_securityDeposite = p_securityDeposite;
		}

		public TextBox getP_checknumber() {
			return p_checknumber;
		}

		public void setP_checknumber(TextBox p_checknumber) {
			this.p_checknumber = p_checknumber;
		}

		public TextBox getP_servisNo() {
			return p_servisNo;
		}

		public void setP_servisNo(TextBox p_servisNo) {
			this.p_servisNo = p_servisNo;
		}

		public TextBox getP_accountNo() {
			return p_accountNo;
		}

		public void setP_accountNo(TextBox p_accountNo) {
			this.p_accountNo = p_accountNo;
		}

		
		


}
