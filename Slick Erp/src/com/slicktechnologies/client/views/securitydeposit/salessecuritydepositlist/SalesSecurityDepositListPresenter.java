package com.slicktechnologies.client.views.securitydeposit.salessecuritydepositlist;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

public class SalesSecurityDepositListPresenter extends TableScreenPresenter<SalesQuotation> {

	

	TableScreen<SalesQuotation> form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final GenricServiceAsync async=GWT.create(GenricService.class);
	
	public SalesSecurityDepositListPresenter(TableScreen<SalesQuotation> view,SalesQuotation model) {
		super(view, model);
		form=view;
		form.getSuperTable().connectToLocal();
		view.retriveTable(getsecuritydepositListQuery());
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SALESSECURITYDEPOSITLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}


	public SalesSecurityDepositListPresenter(TableScreen<SalesQuotation> view,SalesQuotation model,MyQuerry querry) {
	super(view, model);
	form=view;
	view.retriveTable(querry);
	
	boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SALESSECURITYDEPOSITLIST,LoginPresenter.currentModule.trim());
	if(isDownload==false){
		form.getSearchpopupscreen().getDwnload().setVisible(false);
	}
	}



	private MyQuerry getsecuritydepositListQuery() {
		
		Vector<Filter> filterec=null;
		
			filterec=new Vector<Filter>();
			Filter temp=null;
		  
			  temp=new Filter();
			  temp.setStringValue(AppConstants.YES);
			  temp.setQuerryString("isDepositPaid");
			  filterec.add(temp);
			
			  temp=new Filter();
			  temp.setLongValue(model.getCompanyId());
			  temp.setQuerryString("companyId");
			  filterec.add(temp);
			  
			  temp=new Filter();
			  temp.setBooleanvalue(false);
			  temp.setQuerryString("isDepositReceived");
			  filterec.add(temp);
		  
		
		
		MyQuerry quer=new MyQuerry();
		 quer.setFilters(filterec);
		  quer.setQuerryObject(new SalesQuotation());
		  filterec=new Vector<Filter>();
		return quer;
	}
	
	
	
		
		@Override
		protected void makeNewModel() {
			
			model=new SalesQuotation();
		}
		
		public static SalesSecurityDepositListForm initialize(){
			SalesSecurityDepositListTableProxy table=new SalesSecurityDepositListTableProxy();
			SalesSecurityDepositListForm form=new SalesSecurityDepositListForm(table);
			SalesSecurityDepositListSearchProxy.staticSuperTable=table;
			SalesSecurityDepositListSearchProxy searchPopUp=new SalesSecurityDepositListSearchProxy(false);
			form.setSearchpopupscreen(searchPopUp);
			SalesSecurityDepositListPresenter presenter=new SalesSecurityDepositListPresenter(form, new SalesQuotation());
			form.setToNewState();
			AppMemory.getAppMemory().stickPnel(form);
			return form;
		}
		
		public static void initalize(MyQuerry querry)
		{
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Security Deposit List",Screen.SALESQUOTATION);
			SalesSecurityDepositListTableProxy table=new SalesSecurityDepositListTableProxy();
			SalesSecurityDepositListForm form=new SalesSecurityDepositListForm(table);
			SalesSecurityDepositListSearchProxy.staticSuperTable=table;
			SalesSecurityDepositListSearchProxy searchPopUp=new SalesSecurityDepositListSearchProxy(false);
			form.setSearchpopupscreen(searchPopUp);
			SalesSecurityDepositListPresenter presenter=new SalesSecurityDepositListPresenter(form, new SalesQuotation(),querry);
			form.setToNewState();
			AppMemory.getAppMemory().stickPnel(form);
			
		}
		


		@Override
		public void reactOnDownload() {
			CsvServiceAsync async=GWT.create(CsvService.class);
			ArrayList<SalesQuotation> custarray=new ArrayList<SalesQuotation>();
			List<SalesQuotation> list=form.getSuperTable().getListDataProvider().getList();
			
			custarray.addAll(list); 
			
			
			async.setSalesSecurityDeposit(custarray, new AsyncCallback<Void>() {
		
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}
		
				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+77;
					Window.open(url, "test", "enabled");
				}
			});
		}
		
		
		@Override
		public void reactOnPrint() {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void reactOnEmail() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void reactOnSearch() {
			super.reactOnSearch();
			SalesSecurityDepositListSearchProxy.depostatus.setValue(false);
		}

		
	
}
