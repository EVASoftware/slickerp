package com.slicktechnologies.client.views.fumigationAustralia;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class FumigationAustraliaPresenterSearchProxy extends SearchPopUpScreen<Fumigation>{

	


	
	IntegerBox ibcountId,ibcontractId,ibserviceId;
	TextBox tbTreatmentno;
	
	TextBox tbfromCompanyName;
	TextBox tbtoCompanyName;
	DateBox dbissuedate,dbCreationdate;
	
	//****************rohan added two fields in search (suma pest control changes  )
		TextBox tbCertificateNo;
		TextBox tbShippingBillNo;
		
		/** date 23.6.2018 added by komal for port or loading country **/
		ObjectListBox<Config> olbportcountyloading;
	
	public FumigationAustraliaPresenterSearchProxy() {
		super();
		createGui();
		}
	
	
	public void initWidget()
	{
		ibcountId=new IntegerBox();
		tbTreatmentno=new TextBox();
		ibcontractId=new IntegerBox();
		ibserviceId=new IntegerBox();
		dbissuedate=new DateBoxWithYearSelector();
		dbCreationdate=new DateBoxWithYearSelector();
	
		
		//****************rohan added two fields in search (suma pest control changes  )
		 tbCertificateNo= new TextBox();
		 tbShippingBillNo = new TextBox();
		 
		 tbfromCompanyName=new TextBox();
		 tbtoCompanyName=new TextBox();
		 /** date 23.6.2018 added by komal for port or loading country **/
			olbportcountyloading= new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(olbportcountyloading, Screen.COUNTRYOFLOADING);
	}


	public void createScreen() {
		initWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder("Fumigation ID",ibcountId);
		FormField ID= fbuilder.setRowSpan(0).setColSpan(0).build();	
		
		
		fbuilder = new FormFieldBuilder("Registration No.",tbTreatmentno);
		FormField Treatment= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contract ID",ibcontractId);
		FormField Contract= fbuilder.setRowSpan(0).setColSpan(0).build();
		 
		fbuilder = new FormFieldBuilder("Service ID",ibserviceId);
		FormField Service= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Issue Date",dbissuedate);
		FormField servicedate= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Creation Date",dbCreationdate);
		FormField creationdate= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Certificate No",tbCertificateNo);
		FormField ftbCertificateNo= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Shipping Bill No",tbShippingBillNo);
		FormField ftbShippingBillNo= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Exporter Name",tbfromCompanyName);
		FormField ftbfromCompanyName= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Importer Name",tbtoCompanyName);
		FormField ftbtoCompanyName= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		/** date 23.6.2018 added by komal for port or loading country **/
		fbuilder = new FormFieldBuilder("Port & Country of Loading" ,olbportcountyloading);
		FormField folbportcountyloading = fbuilder.setColSpan(0).setRowSpan(0).build();
		
	//for adding on form...	
		
		FormField[][] formfield = {
//				{FumigationInformation},
				{ID,Treatment,creationdate},
				{Contract,Service,servicedate},
				{ftbCertificateNo,ftbShippingBillNo,ftbfromCompanyName},
				{ftbtoCompanyName,folbportcountyloading}
		};
		this.fields=formfield;
		}





	@Override
	public MyQuerry getQuerry() {

		Vector<Filter> filtervec=new Vector<Filter>();
		  Filter temp=null;
		
		  temp=new Filter();
		  temp.setBooleanvalue(true);
		  temp.setQuerryString("australia");
		  filtervec.add(temp);
		  
		  if(!tbTreatmentno.getValue().equals(""))
		  {
		  temp=new Filter();
		  temp.setStringValue(tbTreatmentno.getValue().trim());
		  temp.setQuerryString("registrationno");
		  filtervec.add(temp);
		  }
		  
		  if(!tbfromCompanyName.getValue().equals(""))
		  {
		  temp=new Filter();
		  temp.setStringValue(tbfromCompanyName.getValue().trim());
		  temp.setQuerryString("fromCompanyname");
		  filtervec.add(temp);
		  }
		  
		  if(!tbtoCompanyName.getValue().equals(""))
		  {
		  temp=new Filter();
		  temp.setStringValue(tbtoCompanyName.getValue().trim());
		  temp.setQuerryString("toCompanyname");
		  filtervec.add(temp);
		  }
		
		
		  if(ibcountId.getValue()!=null)
		  {
		  temp=new Filter();
		  temp.setIntValue(ibcountId.getValue());
		  temp.setQuerryString("count");
		  filtervec.add(temp);
		  }
		  
		  if(ibcontractId.getValue()!=null)
		  {
		  temp=new Filter();
		  temp.setIntValue(ibcontractId.getValue());
		  temp.setQuerryString("contractID");
		  filtervec.add(temp);
		  }
		  
		  if(ibserviceId.getValue()!=null)
		  {
		  temp=new Filter();
		  temp.setIntValue(ibserviceId.getValue());
		  temp.setQuerryString("serviceID");
		  filtervec.add(temp);
		  }
		  
		  if(dbissuedate.getValue()!=null)
		  {
		  temp=new Filter();
		  temp.setDateValue(dbissuedate.getValue());
		  temp.setQuerryString("dateofissue");
		  filtervec.add(temp);
		  }
		  
		  if(dbCreationdate.getValue()!=null)
		  {
		  temp=new Filter();
		  temp.setDateValue(dbCreationdate.getValue());
		  temp.setQuerryString("creationDate");
		  filtervec.add(temp);
		  }
		  
		  if(!tbCertificateNo.getValue().equals(""))
		  {
		  temp=new Filter();
		  temp.setStringValue(tbCertificateNo.getValue());
		  temp.setQuerryString("certificateNo");
		  filtervec.add(temp);
		  }
		  
		  if(!tbShippingBillNo.getValue().equals(""))
		  {
		  temp=new Filter();
		  temp.setStringValue(tbShippingBillNo.getValue());
		  temp.setQuerryString("shippingBillNo");
		  filtervec.add(temp);
		  }
		  /** date 23.6.2018 added by komal for port of loading **/
		  if(olbportcountyloading.getSelectedIndex()!=0){
			  temp=new Filter();
			  temp.setStringValue(olbportcountyloading.getValue(olbportcountyloading.getSelectedIndex()));
			  temp.setQuerryString("portncountryloading");
			  filtervec.add(temp);
		  }
		  
		  MyQuerry querry= new MyQuerry();
		  querry.setFilters(filtervec);
		  querry.setQuerryObject(new Fumigation());
		  return querry;
	}


	@Override
	public boolean validate() {
		return true;
	}

}
