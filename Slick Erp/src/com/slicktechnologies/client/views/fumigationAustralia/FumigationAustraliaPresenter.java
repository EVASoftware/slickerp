package com.slicktechnologies.client.views.fumigationAustralia;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.fumigationdetails.FumigationALPPresenter;
import com.slicktechnologies.client.views.fumigationdetails.FumigationForm;
import com.slicktechnologies.client.views.fumigationdetails.FumigationNewCustomerPopup;
import com.slicktechnologies.client.views.fumigationdetails.FumigationPresenter;
import com.slicktechnologies.client.views.fumigationdetails.FumigationPresenterSearchProxy;
import com.slicktechnologies.client.views.fumigationdetails.FumigationPresenterTableProxy;
import com.slicktechnologies.client.views.fumigationdetails.SelectTemplatePopup;
import com.slicktechnologies.client.views.fumigationdetails.FumigationPresenter.FumigationPresenterTable;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter;
import com.slicktechnologies.client.views.service.ServiceTable;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.Declaration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

public class FumigationAustraliaPresenter  extends FormScreenPresenter<Fumigation> implements ClickHandler{

	
	public static FumigationAustraliaForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	//************changes changes here ****************
	PopupPanel panel;
	ConditionDialogBox conditionPopup=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
		//****************ends here ***********	
		
	FumigationNewCustomerPopup custPoppu=new FumigationNewCustomerPopup();
		
	static ServiceTable serviceTable = null;
	SelectTemplatePopup selectTemplatePopup = new SelectTemplatePopup();
	public FumigationAustraliaPresenter(UiScreen<Fumigation> view,
			Fumigation model) {
		super(view, model);
		
		form=(FumigationAustraliaForm) view;
		
		
		form.cbcommodity.addClickHandler(this);
		form.cbpacking.addClickHandler(this);
		
		form.cbbothComAndPacking.addClickHandler(this);
		form.stackUnderSheet.addClickHandler(this);
		form.ContainerUnderSheet.addClickHandler(this);
		form.permantChamber.addClickHandler(this);
		form.PressureTestedContaner.addClickHandler(this);
		form.addDeclarationButton.addClickHandler(this);
		form.addDeclarationButton1.addClickHandler(this);
		form.addDeclarationButton2.addClickHandler(this);
		form.getUnsheetedContainer().addClickHandler(this);
		//**********rohan ************************
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		
		form.getNewFromCustomer().addClickHandler(this);
		form.getNewToCustomer().addClickHandler(this);
		form.getAddbyCONSIGNEEComposite().addClickHandler(this);
		form.getAddbyCONSIGNERComposite().addClickHandler(this);
		form.getSelectTemplateButton().addClickHandler(this);
		selectTemplatePopup.getGoButton().addClickHandler(this);
        selectTemplatePopup.getSearchTable().applySelectionModle();
        setTableSelectionOnFumigation();
		
	}

	public static FumigationAustraliaForm initalize() {


		AppMemory.getAppMemory().currentScreen.equals(ScreeenState.NEW);
		FumigationAustraliaForm form=new FumigationAustraliaForm();
		
		
		FumigationAustraliaPresenterTable gentableSearch=new FumigationAustraliaPresenterTableProxy();
		gentableSearch.setView(form);
		gentableSearch.applySelectionModle();
		FumigationAustraliaPresenterSearchProxy.staticSuperTable=gentableSearch;
		FumigationAustraliaPresenterSearchProxy searchpopup=new FumigationAustraliaPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		
		FumigationAustraliaPresenter  presenter=new  FumigationAustraliaPresenter  (form,new Fumigation());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
		
	}
	
	
	public static class  FumigationAustraliaPresenterTable extends SuperTable<Fumigation> implements GeneratedVariableRefrence{

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub
			
		}

		
	}
	
	
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("New"))
		{
			if (form.isPopUpAppMenubar()) {

				serviceTable.getGenralInvoicePopup().getDocVerticalPanel()
						.add(form.content);
				FumigationAustraliaPresenter fumigationPresenter = new FumigationAustraliaPresenter(
						form, new Fumigation());
				serviceTable.getGenralInvoicePopup().setPresenter(
						fumigationPresenter);
				serviceTable.getGenralInvoicePopup().getPresenter().setModel(model);
				form.setPopUpAppMenubar(true);
				form.setToNewState();

			}else{
			form.setToNewState();
			this.initalize();
			}
		}
		
		if(lbl.getText().contains("Email")){
			reactOnEmail();
		}
		if(lbl.getText().contains("Copy Certificate")){
			reactOnCopyPreviousCertificate();
		}
		/** added by komal to generate invoice**/
		if(lbl.getText().contains("Invoice")){
			reactOnInvoice();
		}
	}

	@Override
	public void reactOnPrint() {
		
		
		//*************rohan changes ********************
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("Fumigation");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}			

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result set size +++++++"+result.size());
				
				List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
				int cnt=0;
				if(result.size()==0){
					
					final String url = GWT.getModuleBaseURL() + "FumigationAusPrint"+"?Id="+model.getId()+"&"+"type="+"FA"+"&"+"preprint="+"plane";
					 Window.open(url, "test", "enabled");
					
				}/**
				 * date 13-4-2018
				 * By jayshree
				 * des.to call the fumigation certificate
				 */
				else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Fumigation", "FumigationCertificate"))
				{
					final String url = GWT.getModuleBaseURL() +"FumigationCertificatePrint"+"?Id="+model.getId()+"&type="+"AFAS";
					Window.open(url, "test", "enabled");
				}
				else{
				
					for(SuperModel model:result)
					{
						ProcessConfiguration processConfig=(ProcessConfiguration)model;
						processList.addAll(processConfig.getProcessList());
						
					}
				
				for(int k=0;k<processList.size();k++){	
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
					
					cnt=cnt+1;
				
				}
				}
				
				
				if(cnt>0){
					System.out.println("in side react on prinnt");
					panel=new PopupPanel(true);
					panel.add(conditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
				}
				else
				{
					final String url = GWT.getModuleBaseURL() + "FumigationAusPrint"+"?Id="+model.getId()+"&"+"type="+"FA"+"&"+"preprint="+"plane";
					 Window.open(url, "test", "enabled");
				}
			}
			}
		});
		//**************************changes ends here ********************************
		
		
	}

	

	@Override
	public void onClick(ClickEvent event){
		super.onClick(event);
		System.out.println("inside on click .........");
		
		if(event.getSource()==form.getCbcommodity()){
			
			form.getCbpacking().setValue(false);
			form.getCbcommodity().setValue(true);
			form.getCbbothComAndPacking().setValue(false);
		}
		
		if(event.getSource()==form.getCbbothComAndPacking()){
			
			form.getCbpacking().setValue(false);
			form.getCbcommodity().setValue(false);
			form.getCbbothComAndPacking().setValue(true);
		}

		if(event.getSource()==form.getCbpacking()){
			
			form.getCbpacking().setValue(true);
			form.getCbcommodity().setValue(false);
			form.getCbbothComAndPacking().setValue(false);
		}
		
		form.stackUnderSheet.addClickHandler(this);
		form.ContainerUnderSheet.addClickHandler(this);
		form.permantChamber.addClickHandler(this);
		form.PressureTestedContaner.addClickHandler(this);
		
		
		if(event.getSource()==form.getStackUnderSheet()){
			
			form.getStackUnderSheet().setValue(true);
			form.getContainerUnderSheet().setValue(false);
			form.getPermantChamber().setValue(false);
			form.getPressureTestedContaner().setValue(false);
			form.getUnsheetedContainer().setValue(false);
			
		}
		if(event.getSource()==form.getContainerUnderSheet()){
					
			form.getStackUnderSheet().setValue(false);
			form.getContainerUnderSheet().setValue(true);
			form.getPermantChamber().setValue(false);
			form.getPressureTestedContaner().setValue(false);
			form.getUnsheetedContainer().setValue(false);
		}
		if(event.getSource()==form.getPermantChamber()){
			
			form.getStackUnderSheet().setValue(false);
			form.getContainerUnderSheet().setValue(false);
			form.getPermantChamber().setValue(true);
			form.getPressureTestedContaner().setValue(false);
			form.getUnsheetedContainer().setValue(false);
		}
		if(event.getSource()==form.getPressureTestedContaner()){
			
			form.getStackUnderSheet().setValue(false);
			form.getContainerUnderSheet().setValue(false);
			form.getPermantChamber().setValue(false);
			form.getPressureTestedContaner().setValue(true);
			form.getUnsheetedContainer().setValue(false);
		}
		
		if(event.getSource()==form.getUnsheetedContainer()){
			form.getUnsheetedContainer().setValue(true);
			form.getStackUnderSheet().setValue(false);
			form.getContainerUnderSheet().setValue(false);
			form.getPermantChamber().setValue(false);
			form.getPressureTestedContaner().setValue(false);
			
		}
		
		
		if(event.getSource().equals(form.getAddDeclarationButton())){
			
			Console.log("INSIDE BUTTON 1 CLICK");
	        if(form.declarationMessageList.getValue()!=null)
	        {
	        	
//	        	MyQuerry querry = new MyQuerry();
//			  	Company c = new Company();
//			  	Vector<Filter> filtervec=new Vector<Filter>();
//			  	Filter filter = null;
//			  	filter = new Filter();
//			  	filter.setQuerryString("companyId");
//				filter.setLongValue(c.getCompanyId());
//				filtervec.add(filter);
//				System.out.println("this.declarationMessageList().getValue().trim()====="+form.declarationMessageList.getValue().trim());
//				filter = new Filter();
//				filter.setQuerryString("title");
//				filter.setStringValue(form.declarationMessageList.getValue().trim());
//				filtervec.add(filter);
//				querry.setFilters(filtervec);
//				
//				querry.setQuerryObject(new Declaration());
//				
//				service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//					
//					@Override
//					public void onFailure(Throwable caught) {
//						Console.log(" Failure");
//					}
//
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						
//				System.out.println(" result size=="+result.size());
//				Console.log(" result size=="+result.size());
//					for(SuperModel model:result)
//					{
//						final Declaration delEntity=(Declaration)model;
//						
						Declaration dec=form.declarationMessageList.getSelectedItem();
//						if(dec.getStatus()){
							Console.log("Status trueee ");
							form.tadeclaration.setValue("");
							form.tadeclaration.setValue(dec.getDeclaratiomMsg());
//						}
//						else {
//							form.showDialogMessage("Declaration Message status is inactive");
//						}
//					}
//					}
//					});
	        	
//	        	Declaration dec=form.declarationMessageList.getSelectedItem();
//	        	form.tadeclaration.setValue(dec.getDeclaratiomMsg());
	            	
	            }
	       else
	        	{	
	    	   form.tadeclaration.setValue("");
	    	   form.showDialogMessage("Select atlest one template from template list");
	        	}
		
	}
		
		
		if(event.getSource().equals(form.getAddDeclarationButton1())){
			
			Console.log("INSIDE BUTTON 1 CLICK");
	        if(form.declarationMessageList1.getValue()!=null)
	            {
	        	
//	        	MyQuerry querry = new MyQuerry();
//			  	Company c = new Company();
//			  	Vector<Filter> filtervec=new Vector<Filter>();
//			  	Filter filter = null;
//			  	filter = new Filter();
//			  	filter.setQuerryString("companyId");
//				filter.setLongValue(c.getCompanyId());
//				filtervec.add(filter);
//				System.out.println("this.declarationMessageList1().getValue().trim()====="+form.declarationMessageList1.getValue().trim());
//				filter = new Filter();
//				filter.setQuerryString("title");
//				filter.setStringValue(form.declarationMessageList1.getValue().trim());
//				filtervec.add(filter);
//				querry.setFilters(filtervec);
//				
//				querry.setQuerryObject(new Declaration());
//				
//				service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//					
//					@Override
//					public void onFailure(Throwable caught) {
//						Console.log(" Failure");
//					}
//
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						Console.log(" result size=="+result.size());
//				System.out.println(" result size=="+result.size());
//					for(SuperModel model:result)
//					{
//						final Declaration delEntity=(Declaration)model;
						Declaration dec=form.declarationMessageList1.getSelectedItem();
//						if(dec.getStatus()){
							Console.log("Status trueee ");
							form.taadditionalDeclaration.setValue("");
							form.taadditionalDeclaration.setValue(dec.getDeclaratiomMsg());
//						}
//						else {
//							form.showDialogMessage("Declaration Message status is inactive");
//						}
//					}
//					}
//					});
	        	
	        	
	        	
	            	
	            }
	       else
	        	{	
	    	   form.tadeclaration.setValue("");
	    	   form.showDialogMessage("Select atlest one template from template list");
	        	}
		
	}
		if(event.getSource().equals(form.addDeclarationButton2)){
		
		Console.log("INSIDE BUTTON 3 CLICK");
        if(form.declarationMessageList2.getValue()!=null)
            {
        	Declaration dec=form.declarationMessageList2.getSelectedItem();
//			if(dec.getStatus()){
				Console.log("Status trueee ");
				form.tadMoreecl.setValue("");
				form.tadMoreecl.setValue(dec.getDeclaratiomMsg());
            }else
        	{	
    	   form.tadMoreecl.setValue("");
    	   form.showDialogMessage("Select atlest one template from template list");
        	}
	}
		
		
		
		
		if(event.getSource().equals(conditionPopup.getBtnOne()))
		{
			System.out.println("in side one yes");
			

			final String url = GWT.getModuleBaseURL() + "FumigationAusPrint"+"?Id="+model.getId()+"&"+"type="+"FA"+"&"+"preprint="+"yes";
			 Window.open(url, "test", "enabled");
			
				panel.hide();
		}
		
		if(event.getSource().equals(conditionPopup.getBtnTwo()))
		{
			System.out.println("inside two no");
			
			final String url = GWT.getModuleBaseURL() + "FumigationAusPrint"+"?Id="+model.getId()+"&"+"type="+"FA"+"&"+"preprint="+"no";
			 Window.open(url, "test", "enabled");
			
			
			    panel.hide();
		}
		
		
		
		
if (event.getSource().equals(form.getAddbyCONSIGNEEComposite())) {
			
			MyQuerry querry = new MyQuerry();
			Vector<Filter> filterve = new Vector<Filter>();
			Filter temp = null;
			
			temp = new Filter();
//			temp.setQuerryString("countryName");
			temp.setQuerryString("companyName");
			temp.setStringValue(form.getPersonInfoCONSIGNEEComposite().getName().getValue());
			filterve.add(temp);

			System.out.println("IMPORTER NAME :: "+form.getPersonInfoCONSIGNEEComposite().getName().getValue());
			temp = new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(model.getCompanyId());
			filterve.add(temp);
			querry.setFilters(filterve);
			querry.setQuerryObject(new Customer());

			service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {

					String impAddress="";
				System.out.println(" IMPORTER result set size +++++++"+ result.size());
					if (result.size() != 0) {
						for (SuperModel model : result) {
							Customer con = (Customer) model;
//							form.getFromacshippingcomposite().setValue(con.getAdress());
//							form.getNameExporter().setValue(form.getPersonInfoCONSIGNERComposite().getName().getValue());
//							form.getAddressOfExporter().setValue(con.getAdress());
							
							form.getNameImporter().setValue(form.getPersonInfoCONSIGNEEComposite().getName().getValue());
//							form.getAddressOfImporter().setValue(con.getAdress());
							
							impAddress= con.getAdress().getAddrLine1()+","+con.getAdress().getAddrLine2()
									+","+con.getAdress().getLandmark()+","+con.getAdress().getLocality()
									+","+con.getAdress().getCity()+","+con.getAdress().getState()+","+con.getAdress().getPin();
						}
						
						form.getImporterAdd().setValue(impAddress);
					}
				}
			});
		}

		if (event.getSource().equals(form.getAddbyCONSIGNERComposite())) {
			MyQuerry querry = new MyQuerry();
			Vector<Filter> filterve = new Vector<Filter>();
			
			System.out.println("EXPORTER NAME :: "+form.getPersonInfoCONSIGNERComposite().getName().getValue());
			
			Filter temp = null;
			temp = new Filter();
//			temp.setQuerryString("countryName");
			temp.setQuerryString("companyName");
			temp.setStringValue(form.getPersonInfoCONSIGNERComposite().getName().getValue());
			filterve.add(temp);

			temp = new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(model.getCompanyId());
			filterve.add(temp);
			querry.setFilters(filterve);
			querry.setQuerryObject(new Customer());

			service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
				}
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					String expAddress="";
					System.out.println(" EXPORTER result set size +++++++"+ result.size());
						if (result.size() != 0) {
							for (SuperModel model : result) {
								Customer con = (Customer) model;
//								form.getToacshippingcomposite().setValue(con.getAdress());
//								form.getNameImporter().setValue(form.getPersonInfoCONSIGNERComposite().getName().getValue());
//								form.getAddressOfImporter().setValue(con.getAdress());
								form.getNameExporter().setValue(form.getPersonInfoCONSIGNERComposite().getName().getValue());
//								form.getAddressOfExporter().setValue(con.getAdress());
								
								expAddress= con.getAdress().getAddrLine1()+","+con.getAdress().getAddrLine2()
										+","+con.getAdress().getLandmark()+","+con.getAdress().getLocality()
										+","+con.getAdress().getCity()+","+con.getAdress().getState()+","+con.getAdress().getPin();
							}
							
							form.getExporterAdd().setValue(expAddress);
						}
					}
				});

		}
		
		
		
		
		
	//////////////////////////////8888888888/////////////////////////////////////////////
		
		if(event.getSource().equals(form.getNewFromCustomer())){
			System.out.println("NEW FORM CUSTOMER!!!!");
			custPoppu=new FumigationNewCustomerPopup("Exporter");
			panel=new PopupPanel(true);
			panel.add(custPoppu);
			panel.center();
			panel.show();
			custPoppu.getBtnOk().addClickHandler(this);
			custPoppu.getBtnCancel().addClickHandler(this);
		}
		
		if(event.getSource().equals(form.getNewToCustomer())){
			System.out.println("NEW TO CUSTOMER!!!!");
			custPoppu=new FumigationNewCustomerPopup("Importer");
			panel=new PopupPanel(true);
			panel.add(custPoppu);
			panel.center();
			panel.show();
			custPoppu.getBtnOk().addClickHandler(this);
			custPoppu.getBtnCancel().addClickHandler(this);
		}
		
		if(event.getSource()==custPoppu.getBtnOk()){
			if(custPoppu.getLabel().getText().equals("Exporter")){
				
				if(!custPoppu.getTbcompanyName().getValue().equals("")){
					
//					form.getAddressOfExporter().clear();//comment by jayshree
					
					form.nameExporter.setValue(custPoppu.getTbcompanyName().getValue());
					form.setConsignerName(custPoppu.getTbcompanyName().getValue());
					if(!custPoppu.getTbPocName().getValue().equals("")){
						form.setConsignerPocName(custPoppu.getTbPocName().getValue());
					}
					if(!custPoppu.getEtbEmail().getValue().equals("")){
						form.setConsignerEmail(custPoppu.getEtbEmail().getValue());
					}
					if(!custPoppu.getPnbLandlineNo().getValue().equals("")){
						form.setConsignerCellNo(Long.parseLong(custPoppu.getPnbLandlineNo().getValue()));
					}
					panel.hide();
				}else{
					form.showDialogMessage("Please fill company name !");
				}
				
			}
			
			if(custPoppu.getLabel().getText().equals("Importer")){
				if(!custPoppu.getTbcompanyName().getText().equals("")){

//					form.getAddressOfImporter().clear();//comment by jayshree
					
					form.nameImporter.setValue(custPoppu.getTbcompanyName().getValue());
					form.setConsigneeName(custPoppu.getTbcompanyName().getValue());
					if(!custPoppu.getTbPocName().getValue().equals("")){
						form.setConsigneePocName(custPoppu.getTbPocName().getValue());
					}
					if(!custPoppu.getEtbEmail().getValue().equals("")){
						form.setConsigneeEmail(custPoppu.getEtbEmail().getValue());
					}
					if(!custPoppu.getPnbLandlineNo().getValue().equals("")){
						form.setConsigneeCellNo(Long.parseLong(custPoppu.getPnbLandlineNo().getValue()));
					}
					
					panel.hide();
				}else{
					form.showDialogMessage("Please fill company name !");
				}
			}
		}
		
		if(event.getSource()==custPoppu.getBtnCancel()){
			panel.hide();
		}
		if(event.getSource()==form.getSelectTemplateButton()){
			 selectTemplatePopup.showPopUp();
			 }
		if(event.getSource()==selectTemplatePopup.getGoButton()){
			selectTemplatePopup.getSearchTable2().getDataprovider().getList().clear();
			MyQuerry querry = selectTemplatePopup.getQuerry();
			GenricServiceAsync service=GWT.create(GenricService.class);
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					final GWTCAlert alert = new GWTCAlert();
					List<Fumigation> fList = new ArrayList<Fumigation>();
					if(result.size() >0){
						for(SuperModel s : result){
							Fumigation fumigation = (Fumigation) s;
							fList.add(fumigation);
						}
						selectTemplatePopup.getSearchTable2().getDataprovider().setList(fList);
					}else{
						alert.alert("No result found.");
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}

		//////////////////////////////////////////////////////////////////////////////////////
		
		
		
	}

	
		
	
	@Override
	public void reactOnDownload() {
//		super.reactOnDownload();
		
		ArrayList<Fumigation> woArray = new ArrayList<Fumigation>();
		List<Fumigation> list = (List<Fumigation>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		woArray.addAll(list);
		csvservice.setFumigationAuslist(woArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}
			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 94;
				Window.open(url, "test", "enabled");
			}
		});
		
		
	}

	@Override
	public void reactOnEmail() {
		
		boolean conf = Window.confirm("Do you really want to send email?");
		if (conf == true) {
		emailService.initiateFumigationEmail(model,new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Unable To Send Email");
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(Void result) {
				Window.alert("Email Sent Sucessfully !");
			}
		});
		}
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		model=new Fumigation();
	}
private void reactOnCopyPreviousCertificate() {
	if (form.isPopUpAppMenubar()) {

		serviceTable.getGenralInvoicePopup().getDocVerticalPanel().add(form.content);
		FumigationAustraliaPresenter fumigationPresenter = new FumigationAustraliaPresenter(form, new Fumigation());
		serviceTable.getGenralInvoicePopup().setPresenter(fumigationPresenter);
		serviceTable.getGenralInvoicePopup().getPresenter().setModel(model);
		form.setPopUpAppMenubar(true);

	} else {

		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Fumigation(AFAS)",Screen.FUMIGATIONAUSTRALIA);
		final FumigationAustraliaForm sform=FumigationAustraliaPresenter.initalize();
	}
		form.showWaitSymbol();
		Timer t = new Timer() {
		      @Override
		      public void run() {
		
		form.setToNewState();
		
		Fumigation view = model;
		
		
			form.tbcount.setValue(model.getCount()+"");
		
		
		
		if(model.getShippingBillNo()!=null)
			form.tbShippingBillNo.setValue(model.getShippingBillNo());
		
		if(model.getCommoditybool()!=null)
			form.cbcommodity.setValue(model.getCommoditybool());
		
		
		if(model.getPacking()!=null)
			form.cbpacking.setValue(model.getPacking());
		
		if(model.getBothCommodityandPacking()!=null)
			form.cbbothComAndPacking.setValue(model.getBothCommodityandPacking());
		
		
		
		if(model.getStackUnderSheet()!=null)
			form.stackUnderSheet.setValue(model.getStackUnderSheet());
		
		if(model.getMoredecl()!=null)
			form.tadMoreecl.setValue(model.getMoredecl());
		
		if(model.getContainerUnderSheet()!=null)
			form.ContainerUnderSheet.setValue(model.getContainerUnderSheet());
		
		
		if(model.getPermanentChamber()!=null)
			form.permantChamber.setValue(model.getPermanentChamber());
		
		if(model.getPressureTestedContainer()!=null)
			form.PressureTestedContaner.setValue(model.getPressureTestedContainer());
		
		
		
		if(model.getContainerNo()!=null)
			form.containerNamber.setValue(model.getContainerNo());
	
		if(model.getConsignmentlink()!=null)
			form.tbConsignmentLink.setValue(model.getConsignmentlink());
			

		if(model.getPortncountryloading()!=null)
			form.olbportcountyloading.setValue(model.getPortncountryloading());
		
		if(model.getVentilation()!=null)
			form.tbvetilation.setValue(model.getVentilation());
		
		if(model.getRegistrationno()!=null)
			form.tbregistrationNo.setValue(model.getRegistrationno());
			
		if(model.getCountryofdestination()!=null)
			form.olbcountrydestinatoin.setValue(model.getCountryofdestination());
		
		if(model.getImportcountry()!=null)
			form.olbcountryOfOrigin.setValue(model.getImportcountry());
		
		
		if(model.getFromCompanyname()!=null)
			form.nameExporter.setValue(model.getFromCompanyname());
		
		if(model.getToCompanyname()!=null)
			form.nameImporter.setValue(model.getToCompanyname());
			
		if(model.getAeiRegNO()!=null)
			form.tbAEI_RegNo.setValue(model.getAeiRegNO());
		
		if(model.getAfasRegNo()!=null)
			form.tbAFAS_RegNo.setValue(model.getAfasRegNo());
			
		
		Console.log("in side update view date of issue is "+model.getDateofissue());
		if(model.getDateofissue()!=null)
			form.dbdateofissue.setValue(model.getDateofissue());
		
		if(model.getCertificateNo()!=null)
			form.tbCertificateNo.setValue(model.getCertificateNo());
		
		if(model.getNameoffumigation()!=null)
		{
			for(int i=0;i<form.lbnamefumigation.getItemCount();i++)
			{
				String data=form.lbnamefumigation.getItemText(i);
				if(data.equals(model.getNameoffumigation()))
				{
					form.lbnamefumigation.setSelectedIndex(i);
					break;
				}
			}
		}
		
		
		if(model.getDateoffumigation()!=null)
			form.dbdatefumigation.setValue(model.getDateoffumigation());
		
		if(model.getPlaceoffumigation()!=null)
			form.tbplaceoffumigation.setValue(model.getPlaceoffumigation());
		
		if(model.getDoseratefumigation()!=null)
			form.olbAQISDoseRate.setValue(model.getDoseratefumigation());
		
		if(model.getDurartionfumigation()!=null)
			form.olbExposurePeriod.setValue(model.getDurartionfumigation());
		
		if(model.getMinairtemp()!=null)
			form.olbForecastminairtemp.setValue(model.getMinairtemp());
		
		if(model.getAppliedDoseRate()!=null)
			form.olbAppliedDoseRate.setValue(model.getAppliedDoseRate());
		
		if(model.getDeclaration()!=null)
			form.tadeclaration.setValue(model.getDeclaration());
		
		if(model.getAdditionaldeclaration()!=null)
			form.taadditionalDeclaration.setValue(model.getAdditionaldeclaration());
		
		if(model.getPdate()!=null)
			form.dbdateadd.setValue(model.getPdate());
		
		if(model.getPlace()!=null)
			form.tbplace.setValue(model.getPlace());
		
//		if(model.getContractID()!=null)
//			form.tbcontractId.setValue(model.getContractID()+"");
//		
//		if(model.getServiceID()!=null)
//			form.tbserviceId.setValue(model.getServiceID()+"");
//		
//		if(model.getContractEnddate()!=null)
//			form.dbcontractenddate.setValue(model.getContractEnddate());
//		
//		if(model.getContractStartdate()!=null)
//			form.dbcontractstartdate.setValue(model.getContractStartdate());
//		
//		if(model.getServiceDate()!=null)
//			form.dbservicedate.setValue(model.getServiceDate());
		
//		if(model.getFromaddress()!=null)
//			form.addressOfExporter.setValue(model.getFromaddress());
//		
//		if(model.getTomaddress()!=null)
//			form.addressOfImporter.setValue(model.getTomaddress());
		
		
		//Add by Jayshree
		if(model.getFromAddess()!=null)
		form.exporterAdd.setValue(model.getFromAddess());
	
		if(model.getToAddress()!=null)
		form.importerAdd.setValue(model.getToAddress());
		
		if(model.getNote1()!=null)
			form.lbnote2.setTitle(model.getNote1());
		
		//komal
				if(model.getUnSheetedContainer()!=null){
					form.unsheetedContainer.setValue(model.getUnSheetedContainer());	
				}
				form.tbSerialNumber.setValue(model.getSerialNumber());
				if(model.getAeiRegNO()!=null){
					form.tbDtePPQSRegistrationNumber.setValue(model.getAeiRegNO());
				}
				if(model.getBranch()!=null){
					form.olbBranch.setValue(model.getBranch());
				}
				if(model.getNameOfCommodity()!=null){					
					form.tbNameOfCommodity.setValue(model.getNameOfCommodity());
				}
				form.ibQuantity.setValue(model.getQuantity());
			    if(model.getAccreditedFumigator()!=null){
			    	form.tbAccreditedFumigator.setValue(model.getAccreditedFumigator());
			    }
				if(model.getCompletionDate()!=null){
					form.dbdateOfcompletion.setValue(model.getCompletionDate());
				}
				form.hideWaitSymbol();
		}
	};
		      
		      t.schedule(2000);
	}

private void reactOnInvoice(){
	GeneralServiceAsync genservice = GWT.create(GeneralService.class);
	Fumigation view = model;
	genservice.generateInvoiceForFumigation(UserConfiguration.getCompanyId(), view, new AsyncCallback<Invoice>() {

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onSuccess(final Invoice result) {
			// TODO Auto-generated method stub
			final InvoiceDetailsForm form1;
			if (form.isPopUpAppMenubar()) {
				form1 = new InvoiceDetailsForm();
				serviceTable.getGenralInvoicePopup().getDocVerticalPanel()
						.add(form1.content);
				InvoiceDetailsPresenter invoiceDetailsPresenter = new InvoiceDetailsPresenter(
						form1, new Invoice());
				serviceTable.getGenralInvoicePopup().setPresenter(
						invoiceDetailsPresenter);
				serviceTable.getGenralInvoicePopup().getPresenter().setModel(model);
				form1.setPopUpAppMenubar(true);

			}else{

			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Invoice Details",Screen.INVOICEDETAILS);
			form1=InvoiceDetailsPresenter.initalize();
			}
			//form.showWaitSymbol();
			Timer t = new Timer() {
			      @Override
			      public void run() {
			
			form1.setToNewState();
			form1.updateView(result);
			}
			}; t.schedule(2000);


		}
	});
	
	}
//komal
public static void setObject(SuperTable s ) {
	FumigationForm form = new FumigationForm();
  if(s instanceof ServiceTable){
  ServiceTable sTable = (ServiceTable) s;
  serviceTable = sTable;
  }
	

}
private void setTableSelectionOnFumigation()
{
	 final NoSelectionModel<Fumigation> selectionModelMyObj = new NoSelectionModel<Fumigation>();
    
    SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
    {
        @Override
        public void onSelectionChange(SelectionChangeEvent event) 
        {

       	 Fumigation model=selectionModelMyObj.getLastSelectedObject();
       	 selectTemplatePopup.hidePopUp();   
       
       	if(model.getShippingBillNo()!=null)
			form.tbShippingBillNo.setValue(model.getShippingBillNo());
		
		if(model.getCommoditybool()!=null)
			form.cbcommodity.setValue(model.getCommoditybool());
		
		
		if(model.getPacking()!=null)
			form.cbpacking.setValue(model.getPacking());
		
		if(model.getBothCommodityandPacking()!=null)
			form.cbbothComAndPacking.setValue(model.getBothCommodityandPacking());
		
		
		
		if(model.getStackUnderSheet()!=null)
			form.stackUnderSheet.setValue(model.getStackUnderSheet());
		
		if(model.getMoredecl()!=null)
			form.tadMoreecl.setValue(model.getMoredecl());
		
		if(model.getContainerUnderSheet()!=null)
			form.ContainerUnderSheet.setValue(model.getContainerUnderSheet());
		
		
		if(model.getPermanentChamber()!=null)
			form.permantChamber.setValue(model.getPermanentChamber());
		
		if(model.getPressureTestedContainer()!=null)
			form.PressureTestedContaner.setValue(model.getPressureTestedContainer());
		
		
		
		if(model.getContainerNo()!=null)
			form.containerNamber.setValue(model.getContainerNo());
	
		if(model.getConsignmentlink()!=null)
			form.tbConsignmentLink.setValue(model.getConsignmentlink());
			

		if(model.getPortncountryloading()!=null)
			form.olbportcountyloading.setValue(model.getPortncountryloading());
		
		if(model.getVentilation()!=null)
			form.tbvetilation.setValue(model.getVentilation());
		
		if(model.getRegistrationno()!=null)
			form.tbregistrationNo.setValue(model.getRegistrationno());
			
		if(model.getCountryofdestination()!=null)
			form.olbcountrydestinatoin.setValue(model.getCountryofdestination());
		
		if(model.getImportcountry()!=null)
			form.olbcountryOfOrigin.setValue(model.getImportcountry());
		
		
		if(model.getFromCompanyname()!=null)
			form.nameExporter.setValue(model.getFromCompanyname());
		
		if(model.getToCompanyname()!=null)
			form.nameImporter.setValue(model.getToCompanyname());
			
		if(model.getAeiRegNO()!=null)
			form.tbAEI_RegNo.setValue(model.getAeiRegNO());
		
		if(model.getAfasRegNo()!=null)
			form.tbAFAS_RegNo.setValue(model.getAfasRegNo());
			
		
		Console.log("in side update view date of issue is "+model.getDateofissue());
//		if(model.getDateofissue()!=null)
//			form.dbdateofissue.setValue(model.getDateofissue());
//		
//		if(model.getCertificateNo()!=null)
//			form.tbCertificateNo.setValue(model.getCertificateNo());
		
		if(model.getNameoffumigation()!=null)
		{
			for(int i=0;i<form.lbnamefumigation.getItemCount();i++)
			{
				String data=form.lbnamefumigation.getItemText(i);
				if(data.equals(model.getNameoffumigation()))
				{
					form.lbnamefumigation.setSelectedIndex(i);
					break;
				}
			}
		}
		
		
		if(model.getDateoffumigation()!=null)
			form.dbdatefumigation.setValue(model.getDateoffumigation());
		
		if(model.getPlaceoffumigation()!=null)
			form.tbplaceoffumigation.setValue(model.getPlaceoffumigation());
		
		if(model.getDoseratefumigation()!=null)
			form.olbAQISDoseRate.setValue(model.getDoseratefumigation());
		
		if(model.getDurartionfumigation()!=null)
			form.olbExposurePeriod.setValue(model.getDurartionfumigation());
		
		if(model.getMinairtemp()!=null)
			form.olbForecastminairtemp.setValue(model.getMinairtemp());
		
		if(model.getAppliedDoseRate()!=null)
			form.olbAppliedDoseRate.setValue(model.getAppliedDoseRate());
		
		if(model.getDeclaration()!=null)
			form.tadeclaration.setValue(model.getDeclaration());
		
		if(model.getAdditionaldeclaration()!=null)
			form.taadditionalDeclaration.setValue(model.getAdditionaldeclaration());
		
		if(model.getPdate()!=null)
			form.dbdateadd.setValue(model.getPdate());
		
		if(model.getPlace()!=null)
			form.tbplace.setValue(model.getPlace());
		
//		if(model.getContractID()!=null)
//			form.tbcontractId.setValue(model.getContractID()+"");
//		
//		if(model.getServiceID()!=null)
//			form.tbserviceId.setValue(model.getServiceID()+"");
//		
//		if(model.getContractEnddate()!=null)
//			form.dbcontractenddate.setValue(model.getContractEnddate());
//		
//		if(model.getContractStartdate()!=null)
//			form.dbcontractstartdate.setValue(model.getContractStartdate());
//		
//		if(model.getServiceDate()!=null)
//			form.dbservicedate.setValue(model.getServiceDate());
		
//		if(model.getFromaddress()!=null)
//			form.addressOfExporter.setValue(model.getFromaddress());
//		
//		if(model.getTomaddress()!=null)
//			form.addressOfImporter.setValue(model.getTomaddress());
//		
		
		//Add by Jayshree
		if(model.getFromAddess()!=null)
		form.exporterAdd.setValue(model.getFromAddess());
	
		if(model.getToAddress()!=null)
		form.importerAdd.setValue(model.getToAddress());

		
		
		if(model.getNote1()!=null)
			form.lbnote2.setTitle(model.getNote1());
		
		//komal
				if(model.getUnSheetedContainer()!=null){
					form.unsheetedContainer.setValue(model.getUnSheetedContainer());	
				}
//				form.tbSerialNumber.setValue(model.getSerialNumber());
//				if(model.getAeiRegNO()!=null){
//					form.tbDtePPQSRegistrationNumber.setValue(model.getAeiRegNO());
//				}
//				if(model.getBranch()!=null){
//					form.olbBranch.setValue(model.getBranch());
//				}
				if(model.getNameOfCommodity()!=null){					
					form.tbNameOfCommodity.setValue(model.getNameOfCommodity());
				}
				form.ibQuantity.setValue(model.getQuantity());
			    if(model.getAccreditedFumigator()!=null){
			    	form.tbAccreditedFumigator.setValue(model.getAccreditedFumigator());
			    }
				if(model.getCompletionDate()!=null){
					form.dbdateOfcompletion.setValue(model.getCompletionDate());
				}
		}
	

         //}
    };
    // Add the handler to the selection model
    selectionModelMyObj.addSelectionChangeHandler( tableHandler );
    // Add the selection model to the table

    	selectTemplatePopup.getSearchTable2().getTable().setSelectionModel(selectionModelMyObj);

}


}
