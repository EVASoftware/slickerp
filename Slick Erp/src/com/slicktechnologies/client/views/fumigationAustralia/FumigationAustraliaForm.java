package com.slicktechnologies.client.views.fumigationAustralia;

import java.util.Date;
import java.util.Vector;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.Declaration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class FumigationAustraliaForm extends FormScreen<Fumigation> {
	
//	CheckBox cbcommodity,cbpacking,cbbothComAndPacking;
//	CheckBox stackUnderSheet,ContainerUnderSheet,permantChamber,PressureTestedContaner;
	
	RadioButton cbcommodity,cbpacking,cbbothComAndPacking;
	RadioButton stackUnderSheet,ContainerUnderSheet,permantChamber,PressureTestedContaner,unsheetedContainer;
	TextBox nameExporter,nameImporter,containerNamber;
	//**************************
	TextBox tbShippingBillNo;
	
	
	ObjectListBox<Declaration> declarationMessageList;
	ObjectListBox<Declaration> declarationMessageList1;
	ObjectListBox<Declaration> declarationMessageList2;
	Button addDeclarationButton,addDeclarationButton1,addDeclarationButton2;
	
	TextBox tbplace;
	DateBox dbdateadd;
	
	TextArea tadeclaration,taadditionalDeclaration,tadMoreecl;
	
	TextBox tbCertificateNo,tbcount,tbConsignmentLink;//,portOfLoading;
	TextBox tbregistrationNo,tbAFAS_RegNo,tbAEI_RegNo;
	DateBox dbdateofissue;
	ObjectListBox<Country> olbcountrydestinatoin;
	ObjectListBox<Country> olbcountryOfOrigin;
	
	ListBox lbnamefumigation;
	DateBox dbdatefumigation; 
	TextBox tbplaceoffumigation;
	
	TextBox olbAQISDoseRate;			//Updated By: Viraj Date: 05-04-2019 Description: changed to textBox
	TextBox olbAppliedDoseRate;			//Updated By: Viraj Date: 05-04-2019 Description: changed to textBox
	TextBox olbExposurePeriod;			//Updated By: Viraj Date: 05-04-2019 Description: changed to textBox
	
	TextBox olbForecastminairtemp;		//Updated By: Viraj Date: 05-04-2019 Description: changed to textBox
	
//	AddressComposite addressOfExporter;
//	AddressComposite addressOfImporter;
	
	
	//Add By Jayshree
	Label Expoerteraddress,importeraddress1;
	TextArea exporterAdd;
	TextArea importerAdd;
	
	

	TextBox tbcontractId,tbvetilation;
	TextBox tbserviceId;
	DateBox dbcontractstartdate;
	DateBox dbcontractenddate;
	DateBox dbservicedate;

	Label targetOfFumigation,ventilationfirst,ventilatioSecond;
	Label note1,note2,containerNum;
	ListBox lbnote2;
	
	
	Button addbyCONSIGNERComposite;
	PersonInfoComposite personInfoCONSIGNERComposite;
	Button addbyCONSIGNEEComposite;
	PersonInfoComposite personInfoCONSIGNEEComposite;
	
	Button newFromCustomer;
	Button newToCustomer;
	
	
	String consignerName;
	String consignerPocName;
	String consignerEmail;
	Long consignerCellNo;
	
	
	
	String consigneeName;
	String consigneePocName;
	String consigneeEmail;
	Long consigneeCellNo;
	
	Fumigation fumigationObj;
	//komal
	DateBox dbdateOfcompletion;
	IntegerBox tbSerialNumber;
	TextBox tbDtePPQSRegistrationNumber;
	ObjectListBox<Branch> olbBranch;
	Label branch , certificateNumber , pqrsRegdNumber  , fumigationId , serialNumber , dateOfIssue ;
	
	TextBox tbNameOfCommodity;
	/** Updated  By: Viraj Date: 17-04-2019 Description:Changed according to pest mortom requirement **/
	TextBox ibQuantity;
	
	Label  commodity , qty;
	TextBox tbAccreditedFumigator;

	Button selectTemplateButton;
	/** date 24.5.2018 added by komal **/
	ObjectListBox<Config> olbNumberRange;
	PersonInfoComposite personInfoComposite;
	/** date 23.6.2018 added by komal for port of loading **/
	ObjectListBox<Config> olbportcountyloading;
	/** DATE: 13-04-2019 Description: To add text area for additional declaration **/
	TextArea taAddDeclaration;
	
	public FumigationAustraliaForm() {
		super();
		createGui();
	}
	
	public FumigationAustraliaForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		
	}
	
	

	private void initalizeWidget(){

		
		declarationMessageList = new ObjectListBox<Declaration>();
		MyQuerry querry = new MyQuerry(new Vector<Filter>(),new Declaration());
		declarationMessageList.MakeLive(querry);
		
		
		declarationMessageList1 = new ObjectListBox<Declaration>();
		MyQuerry querry123 = new MyQuerry(new Vector<Filter>(),new Declaration());
		declarationMessageList1.MakeLive(querry123);
		
		declarationMessageList2 = new ObjectListBox<Declaration>();
		MyQuerry querry1234 = new MyQuerry(new Vector<Filter>(),new Declaration());
		declarationMessageList2.MakeLive(querry1234);
		
		
		tbShippingBillNo= new TextBox();
		
		addDeclarationButton =new Button("Add Declaration");
		addDeclarationButton1= new Button("Add Declaration");
		addDeclarationButton2= new Button("Add Note");
		tbplace=new TextBox();
		dbdateadd=new  DateBoxWithYearSelector();
		
		lbnamefumigation=new ListBox();
		lbnamefumigation.addItem("--SELECT--");
		lbnamefumigation.addItem("Methyl Bromide");
		lbnamefumigation.addItem("Aluminium Phosphide");
		
		dbdatefumigation=new  DateBoxWithYearSelector();
		tbplaceoffumigation=new TextBox();
		
//		addressOfExporter =new AddressComposite(true);
//		addressOfImporter =new AddressComposite(true);
		
		
		
		targetOfFumigation=  new Label("Target of Fumigation :");
	
		ventilationfirst= new Label("Ventilation  Final TLV reading (ppm) :");
		ventilatioSecond = new Label("(not required for Stack or Permanent Chamber fumigations)");
		tbvetilation= new TextBox();
		
		
		stackUnderSheet = new RadioButton("");
		ContainerUnderSheet = new RadioButton("");
		permantChamber = new RadioButton("");
		PressureTestedContaner = new RadioButton("");
		unsheetedContainer = new RadioButton("");
		containerNum = new Label("Container number/s(where applicable) :");
		 note1 =new Label("How was the fumigation conducted ?");
		 note2 =new Label("Does the target of the fumigation conform to the AQIS plastic wrapping,"
		 		+ "impervious surface and timber thickness requirements at the timeof fumigation ?");
		
		 lbnote2 =new ListBox();
		 lbnote2.addItem("--SELECT--");
		 lbnote2.addItem("YES");
		 lbnote2.addItem("NO");
		 lbnote2.addItem("N.A.");
		
		tbAFAS_RegNo= new TextBox();
		tbAEI_RegNo =new TextBox();
		
		olbAQISDoseRate=new TextBox();
//		AppUtility.MakeLiveConfig(olbAQISDoseRate, Screen.DOSAGERATEOFFUMIGANT);
		
		olbAppliedDoseRate=new TextBox();
//		AppUtility.MakeLiveConfig(olbAppliedDoseRate, Screen.DOSAGERATEOFFUMIGANT);
		
		
		olbExposurePeriod=new TextBox();
//		AppUtility.MakeLiveConfig(olbExposurePeriod, Screen.DURATIONOFFUMIGATION);
		
		olbForecastminairtemp=new TextBox();
//		AppUtility.MakeLiveConfig(olbForecastminairtemp, Screen.MINIMUMAIRTEMP);
		
		tbcount=new TextBox();
		tbcount.setEnabled(false);
		
		dbdateofissue=new  DateBoxWithYearSelector();
		
		
		cbcommodity=new RadioButton("");
		cbpacking = new RadioButton("");
		cbbothComAndPacking= new RadioButton("");
		
		dbservicedate=new DateBoxWithYearSelector();
		dbservicedate.setEnabled(false);
		tbcontractId=new TextBox();
		tbcontractId.setEnabled(false);
		tbserviceId=new TextBox();
		tbserviceId.setEnabled(false);
		dbcontractstartdate=new DateBoxWithYearSelector();
		dbcontractstartdate.setEnabled(false);
		dbcontractenddate=new DateBoxWithYearSelector();
		dbcontractenddate.setEnabled(false);

		tadeclaration = new TextArea();
		tadMoreecl = new TextArea();
		taadditionalDeclaration= new TextArea();
		
		nameImporter=new TextBox();
		nameImporter.setEnabled(false);
		nameExporter= new TextBox();
		nameExporter.setEnabled(false);
		containerNamber = new TextBox();
		tbCertificateNo= new TextBox();
		/** date 24.5.2018 added by komal**/
		tbCertificateNo.setEnabled(false);
		tbregistrationNo= new TextBox();
		cbcommodity = new RadioButton("");
		cbpacking =new RadioButton("");
		cbbothComAndPacking =new RadioButton("");
		
		tbConsignmentLink= new TextBox();
	//	portOfLoading= new TextBox();
		olbcountryOfOrigin= new ObjectListBox<Country>();
		MyQuerry quer=new MyQuerry();
		quer.setQuerryObject(new Country());
		olbcountryOfOrigin.MakeLive(quer);
		
		olbcountrydestinatoin=new ObjectListBox<Country>();
		MyQuerry que=new MyQuerry();
		que.setQuerryObject(new Country());
		olbcountrydestinatoin.MakeLive(que);
		
		 addbyCONSIGNERComposite = new Button("Add");
		 	MyQuerry custquerry1 = new MyQuerry();
			Filter custfilteer1 = new Filter();
			custfilteer1.setQuerryString("status");
			custfilteer1.setBooleanvalue(true);
			custquerry1.setQuerryObject(new Customer());
			personInfoCONSIGNERComposite = new PersonInfoComposite(custquerry1, false);
		 
		
		
		addbyCONSIGNEEComposite= new Button("Add");
		
		MyQuerry custquerry = new MyQuerry();
		Filter custfilteer = new Filter();
		custfilteer.setQuerryString("status");
		custfilteer.setBooleanvalue(true);
		custquerry.setQuerryObject(new Customer());
		personInfoCONSIGNEEComposite = new PersonInfoComposite(custquerry, false);
		
		newFromCustomer=new Button("New Exporter");
		newToCustomer=new Button("New Importer");
		
		//komal
		tbSerialNumber = new IntegerBox();
		
		tbDtePPQSRegistrationNumber = new TextBox();
		olbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		branch = new Label("* Branch");
		certificateNumber =new Label("* Treatment Certificate No.");
		pqrsRegdNumber = new Label("Dte. PQRS Regd. No.");
		fumigationId = new Label("Fumigation ID");
		serialNumber = new Label("Serial No.");
		dateOfIssue = new Label("* Date of Issue");
        dbdateOfcompletion = new DateBoxWithYearSelector();
        commodity = new Label("Commodity");
		qty = new Label("QTY");
		tbNameOfCommodity = new TextBox();
		ibQuantity = new TextBox();
		tbAccreditedFumigator = new TextBox();
	//	olbBranch.addChangeHandler(this);
		selectTemplateButton = new Button("Select Template");
		/** date 24.5.2018 added by komal **/
		olbNumberRange = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbNumberRange, Screen.NUMBERRANGE);
		
		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		
		/** date 23.6.2018 added by komal for port of loading **/
		olbportcountyloading= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbportcountyloading, Screen.COUNTRYOFLOADING);
		
		
		//Add by Jayshree
		
		exporterAdd=new TextArea();
		importerAdd=new TextArea();
		
		taAddDeclaration = new TextArea();
	}
	
	@Override
	public void setCount(int count) {

		tbcount.setValue(count+"");
	
	}
	
	
	
	@Override
	public void toggleAppHeaderBarMenu() {

		
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			//InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * Date 14 Apr 2018 if condtion added by komal for GENERAL POPUP menus 
			 * and old code added to else block
			 */
			
			System.out.println("presenter.isPopUpAppMenubar()==== NEW "+isPopUpAppMenubar());
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			//InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * Date 14 Apr 2018 if condtion added by komal for GENERAL POPUP menus 
			 * and old code added to else block
			 */
			
			System.out.println("presenter.isPopUpAppMenubar()==== EDIT "+isPopUpAppMenubar());
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			//InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * Date 14 Apr 2018 if condtion added by komal for GENERAL POPUP menus 
			 * and old code added to else block
			 */
			
			System.out.println("presenter.isPopUpAppMenubar()==== VIEW "+isPopUpAppMenubar());
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Print")||text.equals("Edit")||text.equals("Email")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		
		AuthorizationHelper.setAsPerAuthorization(Screen.FUMIGATIONAUSTRALIA,LoginPresenter.currentModule.trim());
	}

	@Override
	public void createScreen() {
		
		initalizeWidget();
		
		this.processlevelBarNames = new String[] { "New" , "Copy Certificate" ,"Invoice"};/**date 25.4.2018 added by komal**/
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField information=fbuilder.setlabel("REFERENCE INFORMATION").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(8).build();
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoCompositeCustomer= fbuilder.setRowSpan(0).setColSpan(8).build();	
		fbuilder = new FormFieldBuilder("Contract ID",tbcontractId);
		FormField contrctid= fbuilder.setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Service ID",tbserviceId);
		FormField serviceid= fbuilder.setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Contract Start Date",dbcontractstartdate);
		FormField contractstart= fbuilder.setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Contract End Date",dbcontractenddate);
		FormField contractend= fbuilder.setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Service Date",dbservicedate);
		FormField servicedate= fbuilder.setColSpan(2).build();

		
		FormField fgroupingfumigationInformation=fbuilder.setlabel("* FUMIGATION DETAILS").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(8).build();
		fbuilder = new FormFieldBuilder("",fumigationId);
		FormField ffumigationId= fbuilder.setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",tbcount);
		FormField ftbfumigationcount= fbuilder.setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", serialNumber);
		FormField fserialNumber= fbuilder.setRowSpan(0).setColSpan(1).build();

		fbuilder = new FormFieldBuilder("", tbSerialNumber);
		FormField ftbSerialNumber= fbuilder.setMandatory(true).setMandatoryMsg("Serial No. is Mandatory").setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", dateOfIssue);
		FormField fdateOfIssue= fbuilder.setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",dbdateofissue);
		FormField fdbdateissue= fbuilder.setMandatory(true).setMandatoryMsg("Date of Issue is Mandatory").setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", new Label("AEI No."));
		FormField fpqrsRegdNumber = fbuilder.setColSpan(1).build();

		fbuilder = new FormFieldBuilder("", tbAEI_RegNo);
		FormField ftbDtePPQSRegistrationNumber= fbuilder.setMandatory(true).setMandatoryMsg("AEI No. is Mandatory").setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",branch);
		FormField fbranch = fbuilder.setColSpan(1).build();
	
		fbuilder = new FormFieldBuilder("",olbBranch);
		FormField folbBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(1).build();
			
		fbuilder = new FormFieldBuilder("",certificateNumber);
		FormField fcertificateNumber = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
	
		fbuilder = new FormFieldBuilder("",tbCertificateNo);
		FormField ftbCertificateNo= fbuilder.setMandatory(false).setMandatoryMsg("Certificate No. is Mandatory").setRowSpan(0).setColSpan(1).build();
	
		fbuilder = new FormFieldBuilder();
		FormField fempty2= fbuilder.setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fempty4= fbuilder.setRowSpan(1).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();		
		FormField fgroupingGOODSformation=fbuilder.setlabel("TARGET OF FUMIGATION DETAILS").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder();		
		FormField fgroupingTreatment=fbuilder.setlabel("TREATMENT DETAILS").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("", new Label("Target of Fumigation"));
		FormField fTargetOfFumigation = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder(" Commodity",cbcommodity);
		FormField cbcommodity= fbuilder.setRowSpan(0).setColSpan(2).build();	
		
		fbuilder = new FormFieldBuilder(" Packing",cbpacking);
		FormField cbpacking= fbuilder.setRowSpan(0).setColSpan(2).build();	
		
		fbuilder = new FormFieldBuilder("Both Commodity and Packing ",cbbothComAndPacking);
		FormField cbbothComAndPacking= fbuilder.setRowSpan(0).setColSpan(2).build();	

		fbuilder = new FormFieldBuilder("", commodity);
		FormField fcommodity= fbuilder.setRowSpan(1).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",tbNameOfCommodity);
		FormField ftbNameOfCommodity= fbuilder.setRowSpan(1).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("", qty);
		FormField fqty= fbuilder.setRowSpan(1).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("", ibQuantity);
		FormField fibQuantity= fbuilder.setRowSpan(1).setColSpan(2).build();

		fbuilder = new FormFieldBuilder("",new Label("* Consignment Link"));
		FormField fconsinglink= fbuilder.setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",tbConsignmentLink);
		FormField ftbconsinglink= fbuilder.setMandatory(true).setMandatoryMsg("Consignment Link is Mandatory").setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",new Label("* Country of Destination"));
		FormField fcountrydesct= fbuilder.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",olbcountrydestinatoin);
		FormField folbcountrydesct= fbuilder.setMandatory(true).setMandatoryMsg("Country of Destination is Mandatory").setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",new Label("* Country of Origin"));
		FormField fcountryOfOrigin= fbuilder.setMandatory(true).setColSpan(1).build();

		fbuilder = new FormFieldBuilder("",olbcountryOfOrigin);
		FormField olbcountryOfOrigin= fbuilder.setMandatory(true).setMandatoryMsg("Country of Origin is Mandatory").setColSpan(3).build();

		fbuilder = new FormFieldBuilder("",new Label("* Port of Loading"));
		FormField fcountryloading= fbuilder.setRowSpan(0).setColSpan(2).build();

		fbuilder = new FormFieldBuilder("",olbportcountyloading);
		FormField folbcountryloading= fbuilder.setMandatory(true).setMandatoryMsg("Port of Loading is Mandatory").setRowSpan(0).setColSpan(2).build();

		fbuilder = new FormFieldBuilder();
		FormField additionExp=fbuilder.setlabel("Name and Address of Exporter").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(8).build();
		
		
		//Comment By jayshree
//		fbuilder = new FormFieldBuilder(" ",addressOfExporter);
//		FormField addressOfExporter= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(8).build();
		
		
		//Add by jayshree
		
		fbuilder = new FormFieldBuilder("Exporter Address ",Expoerteraddress);
		FormField lbExpoerteraddress= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder(" ",exporterAdd);
		FormField addressOfExporter= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("* Name ",nameExporter);
		FormField nameExporter= fbuilder.setMandatory(true).setMandatoryMsg("Name is Mandatory").setRowSpan(0).setColSpan(2).build();

		fbuilder = new FormFieldBuilder();
		FormField addinfoImp=fbuilder.setlabel("Name and Adress of Importer").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("* Name",nameImporter);
		FormField nameImporter= fbuilder.setMandatory(true).setMandatoryMsg("Name is Mandatory").setRowSpan(0).setColSpan(2).build();
		
		//Comment By jayshree
		
//		fbuilder = new FormFieldBuilder(" ",addressOfImporter);
//		FormField addressOfImporter= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("Importer Address ",importeraddress1);
		FormField lbimporteraddress1= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder(" ",importerAdd);
		FormField addressOfImporter= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(8).build();
		
		
		fbuilder = new FormFieldBuilder("",personInfoCONSIGNEEComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder(" ",addbyCONSIGNEEComposite);
		FormField faddbyComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		
		fbuilder = new FormFieldBuilder("",personInfoCONSIGNERComposite);
		FormField fpersonInfoCONSIGNERComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder(" ",addbyCONSIGNERComposite);
		FormField faddbyCONSIGNERComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder(" ",newFromCustomer);
		FormField fnewformCustomer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder(" ",newToCustomer);
		FormField fnewtoCustomer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();

		fbuilder = new FormFieldBuilder("", new Label("* Date of fumigation"));
		FormField fdatefumigation= fbuilder.setRowSpan(2).setColSpan(2).build();

		fbuilder = new FormFieldBuilder("", dbdatefumigation);
		FormField fdbdatefumigation= fbuilder.setMandatory(true).setMandatoryMsg("Date is Mandatory").setRowSpan(0).setColSpan(2).build();

		fbuilder = new FormFieldBuilder("", new Label("* Date of fumigation Completed"));
		FormField fdateOfcompletion= fbuilder.setRowSpan(0).setColSpan(2).build();

		fbuilder = new FormFieldBuilder("", dbdateOfcompletion);
		FormField fdbdateOfcompletion= fbuilder.setMandatory(true).setMandatoryMsg("Completion Date is Mandatory").setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",new Label("* Name of Fumigation"));
		FormField fumigationname= fbuilder.setColSpan(2).build();

		fbuilder = new FormFieldBuilder("",lbnamefumigation);
		FormField flbfumigationname= fbuilder.setMandatory(true).setMandatoryMsg("Name of Fumigation is Mandatory").setColSpan(2).build();

		fbuilder = new FormFieldBuilder("",new Label("* Place of Fumigation"));
		FormField fumigationplace= fbuilder.setColSpan(2).build();
		
		
		fbuilder = new FormFieldBuilder("",tbplaceoffumigation);
		FormField ftbfumigationplace= fbuilder.setMandatory(true).setMandatoryMsg("Place of Fumigation is Mandatory").setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",new Label("* DAFF Prescribe Dose Rate"));
		FormField fAQISDoseRate= fbuilder.setColSpan(2).build();
	
		fbuilder = new FormFieldBuilder("",olbAQISDoseRate);
		FormField folbAQISDoseRate= fbuilder.setMandatory(true).setMandatoryMsg("Dosage Rate of Fumigation is Mandatory").setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",new Label("* Applied Dosage Rate"));
		FormField fAppliedDoseRate= fbuilder.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",olbAppliedDoseRate);
		FormField folbAppliedDoseRate= fbuilder.setMandatory(true).setMandatoryMsg("Dosage Rate of Fumigation is Mandatory").setColSpan(2).build();

		
		fbuilder = new FormFieldBuilder("", new Label("* Exposure Period"));
		FormField fExposurePeriod= fbuilder.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",olbExposurePeriod);
		FormField folbExposurePeriod= fbuilder.setMandatory(true).setMandatoryMsg("Exposure Period of Mandatory").setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",new Label("* Forecast Minimum Temp"));
		FormField forecastminairtemp= fbuilder.setColSpan(2).build();

		fbuilder = new FormFieldBuilder("",olbForecastminairtemp);
		FormField folbForecastminairtemp= fbuilder.setMandatory(true).setMandatoryMsg("Min. Air Temp. is Mandatory").setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder(" ",note1);
		FormField note1= fbuilder.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder(" Stack Under Sheet",stackUnderSheet);
		FormField stackUnderSheet= fbuilder.setRowSpan(0).setColSpan(2).build();	
		
		fbuilder = new FormFieldBuilder(" Container/s Under Sheet",ContainerUnderSheet);
		FormField containerUnderSheet= fbuilder.setRowSpan(0).setColSpan(2).build();	
		
		fbuilder = new FormFieldBuilder(" Chamber",permantChamber);
		FormField permantChamber= fbuilder.setRowSpan(0).setColSpan(2).build();	
		
		fbuilder = new FormFieldBuilder(" Pressure Tested Container/s",PressureTestedContaner);
		FormField pressureTestedContaner= fbuilder.setRowSpan(0).setColSpan(2).build();	

		fbuilder = new FormFieldBuilder(" Un-sheeted Container", unsheetedContainer);
		FormField unsheetedContainer= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(2).build();	
		fbuilder = new FormFieldBuilder(" ",containerNum);
		FormField containerNum= fbuilder.setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder(" ",containerNamber);
		FormField containerNumber= fbuilder.setMandatory(true).setMandatoryMsg("Container Number is Mandatory").setRowSpan(0).setColSpan(5).build();

		fbuilder = new FormFieldBuilder(" ",note2);
		FormField note2= fbuilder.setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder(" ",lbnote2);
		FormField lbnote2= fbuilder.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder(" ",tbvetilation);
		FormField tbvetilation= fbuilder.setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder(" ",ventilationfirst);
		FormField ventilationfirst= fbuilder.setColSpan(2).build();
	  
		fbuilder = new FormFieldBuilder(" ",ventilatioSecond);
		FormField ventilatioSecond= fbuilder.setColSpan(4).build();

		fbuilder = new FormFieldBuilder();
		FormField ADDINformation=fbuilder.setlabel("* DECLARATION").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(8).build();
			
		fbuilder = new FormFieldBuilder("Select Declaration Message ",declarationMessageList);
	    FormField fdeclarationMessageList= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
	    fbuilder = new FormFieldBuilder(" ",addDeclarationButton);
		FormField faddDeclarationButton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",tadeclaration);
		FormField ftadeclaration= fbuilder.setMandatory(true).setMandatoryMsg("Declaration is Mandatory").setRowSpan(0).setColSpan(8).build();

		fbuilder = new FormFieldBuilder("", new Label("* AFAS Accreditation No."));
		FormField fAFAS_RegNo= fbuilder.setRowSpan(0).setColSpan(2).build();	

		fbuilder = new FormFieldBuilder("",tbAFAS_RegNo);
		FormField ftbAFAS_RegNo= fbuilder.setMandatory(true).setMandatoryMsg("AFAS Accreditation No. is Mandatory").setRowSpan(0).setColSpan(2).build();	

		fbuilder = new FormFieldBuilder("", new Label("* Name and Accredited Fumigator"));
		FormField fAccreditedFumigator= fbuilder.setRowSpan(0).setColSpan(2).build();	

		fbuilder = new FormFieldBuilder("", tbAccreditedFumigator);
		FormField ftbAccreditedFumigator= fbuilder.setMandatory(true).setMandatoryMsg("AFAS Accreditation No. is Mandatory").setRowSpan(0).setColSpan(2).build();			
		
		fbuilder = new FormFieldBuilder(" ",selectTemplateButton);
		FormField fselectTemplateButton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fempty= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Number Range",olbNumberRange);
		FormField folbNumberRange= fbuilder.setMandatory(true).setMandatoryMsg("Number Range is Mandatory").setRowSpan(0).setColSpan(1).build();
		FormField fgroupingCustomerInformation=fbuilder.setlabel("CUSTOMER INFORMATION").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(8).build();
		/**
		 * Updated By: Viraj
		 * Date: 13-04-2019
		 * Desciption: To add additional declaration
		 */
		fbuilder = new FormFieldBuilder();
		FormField AddDeclaration =fbuilder.setlabel("*ADDITIONAL DECLARATION").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("",taAddDeclaration);
		FormField ftaAddDeclaration= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(8).build();
		/** Ends **/
		FormField[][] formfield = {
				{fgroupingCustomerInformation},
				{fpersonInfoCompositeCustomer},
				{information},{contrctid,serviceid,contractstart,contractend},
				{servicedate ,/**date 24.5.2018 added by komal **/folbNumberRange},
				{fgroupingfumigationInformation},
				{ffumigationId , ftbfumigationcount ,fserialNumber , ftbSerialNumber , fdateOfIssue ,fdbdateissue},
				{fbranch , folbBranch, fpqrsRegdNumber ,ftbDtePPQSRegistrationNumber , fcertificateNumber , ftbCertificateNo,fempty ,fselectTemplateButton},				
				{fgroupingGOODSformation},
				{fTargetOfFumigation ,cbcommodity , cbpacking , cbbothComAndPacking },
				{fcommodity ,ftbNameOfCommodity , fqty , fibQuantity },
				{fconsinglink , ftbconsinglink ,fcountryloading ,folbcountryloading },
				{fcountryOfOrigin ,olbcountryOfOrigin ,fcountrydesct ,folbcountrydesct	},
				{additionExp},
				
				{fpersonInfoCONSIGNERComposite,faddbyCONSIGNERComposite},
				{nameExporter,fnewformCustomer},
				//Add by jayshree
				{lbExpoerteraddress},
				{addressOfExporter},
				{addinfoImp},
				{fpersonInfoComposite,faddbyComposite},
				{nameImporter,fnewtoCustomer},
				{lbimporteraddress1},//Add by jayshree
				{addressOfImporter},
				{fgroupingTreatment},
				{fdateOfcompletion , fdbdateOfcompletion , fdatefumigation,fdbdatefumigation },
				{fumigationname ,flbfumigationname , fumigationplace ,ftbfumigationplace},
				{fAQISDoseRate ,folbAQISDoseRate , fAppliedDoseRate ,folbAppliedDoseRate},
				{fExposurePeriod , folbExposurePeriod , forecastminairtemp ,folbForecastminairtemp},
				{note1 ,unsheetedContainer , containerUnderSheet ,permantChamber },
				{fempty2 ,pressureTestedContaner ,stackUnderSheet },
				{containerNum ,containerNumber},
				{note2 ,lbnote2	},
				{ ventilationfirst,tbvetilation ,ventilatioSecond},
				{ADDINformation},
				{fdeclarationMessageList ,faddDeclarationButton },
				{ftadeclaration},
				{AddDeclaration},
				{ftaAddDeclaration},
				{fAccreditedFumigator ,ftbAccreditedFumigator ,fAFAS_RegNo , ftbAFAS_RegNo}
				
			};

			
		this.fields=formfield;
		
	
	}

	@Override
	public void updateModel(Fumigation model) {
		
		model.setAustralia(true);
		
		if(personInfoComposite.getValue()!=null)
			model.setcInfo(personInfoComposite.getValue());

		if(tbShippingBillNo.getValue()!=null)
			model.setShippingBillNo(tbShippingBillNo.getValue());
		
		if(cbcommodity.getValue()!=null)
			model.setCommoditybool(cbcommodity.getValue());
		if(cbpacking.getValue()!=null)
			model.setPacking(cbpacking.getValue());
		if(cbbothComAndPacking.getValue()!=null)
			model.setBothCommodityandPacking(cbbothComAndPacking.getValue());
		
		if(stackUnderSheet.getValue()!=null)
			model.setStackUnderSheet(stackUnderSheet.getValue());
		
		if(ContainerUnderSheet.getValue()!=null)
			model.setContainerUnderSheet(ContainerUnderSheet.getValue());
		
		if(permantChamber.getValue()!=null)
			model.setPermanentChamber(permantChamber.getValue());
		if(PressureTestedContaner.getValue()!=null)
			model.setPressureTestedContainer(PressureTestedContaner.getValue());
		if(unsheetedContainer.getValue()!=null){
			model.setUnSheetedContainer(unsheetedContainer.getValue());
		}
		if(containerNamber.getValue()!=null)
			model.setContainerNo(containerNamber.getValue());
			
			
		if(tbConsignmentLink.getValue()!=null)
			model.setConsignmentlink(tbConsignmentLink.getValue());
		
		/** date 23.6.2018 added by komal for port of country **/
		if(olbportcountyloading.getSelectedIndex()!=0)
			model.setPortncountryloading(olbportcountyloading.getValue(olbportcountyloading.getSelectedIndex()));
		
		if(tbvetilation.getValue()!=null)
			model.setVentilation(tbvetilation.getValue());
		
		if(olbcountryOfOrigin.getValue()!=null)
			model.setImportcountry(olbcountryOfOrigin.getValue());
		
		if(olbcountrydestinatoin.getValue()!=null)
			model.setCountryofdestination(olbcountrydestinatoin.getValue());
		
		
		if(tbregistrationNo.getValue()!=null)
			model.setRegistrationno(tbregistrationNo.getValue());
		
		if(olbcountrydestinatoin.getValue()!=null)
			model.setCountryofdestination(olbcountrydestinatoin.getValue());
		
		if(nameExporter.getValue()!=null)
			model.setFromCompanyname(nameExporter.getValue());
		
		if(nameImporter.getValue()!=null)
			model.setToCompanyname(nameImporter.getValue());
		
		if(tbAEI_RegNo.getValue()!=null)
			model.setAeiRegNO(tbAEI_RegNo.getValue());
		Console.log("AEI NO in update model: "+tbAEI_RegNo.getValue());
		
		if(tbAFAS_RegNo.getValue()!=null)
			model.setAfasRegNo(tbAFAS_RegNo.getValue());
		Console.log("date of issue before saving in update model ........"+dbdateofissue.getValue());
		if(dbdateofissue.getValue()!=null)
			model.setDateofissue(dbdateofissue.getValue());
		
		if(tbCertificateNo.getValue()!=null)
			model.setCertificateNo(tbCertificateNo.getValue());
		Console.log("Certificate No in update model: "+tbCertificateNo.getValue());
		
		Console.log("date of fumigation after saving in update model ........"+model.getDateoffumigation());

		model.setNameoffumigation(lbnamefumigation.getItemText(lbnamefumigation.getSelectedIndex()));
		
		if(dbdatefumigation.getValue()!=null)
			model.setDateoffumigation(dbdatefumigation.getValue());
		if(tbplaceoffumigation.getValue()!=null)
			model.setPlaceoffumigation(tbplaceoffumigation.getValue());
		if(olbAQISDoseRate.getValue()!=null)
			model.setDoseratefumigation(olbAQISDoseRate.getValue());
		if(olbExposurePeriod.getValue()!=null)
			model.setDurartionfumigation(olbExposurePeriod.getValue());
		if(olbForecastminairtemp.getValue()!=null)
			model.setMinairtemp(olbForecastminairtemp.getValue());
		
		if(olbAppliedDoseRate.getValue()!=null)
			model.setAppliedDoseRate(olbAppliedDoseRate.getValue());
		Console.log("date of fumigation after saving in update model ........"+model.getDateoffumigation());

//		if(tadMoreecl.getValue()!=null)
//			model.setMoredecl(tadMoreecl.getValue());
//		
		if(tadeclaration.getValue()!=null)
			model.setDeclaration(tadeclaration.getValue());
		
//		if(taadditionalDeclaration.getValue()!=null)
//			model.setAdditionaldeclaration(taadditionalDeclaration.getValue());
//		if(dbdateadd.getValue()!=null)
//			model.setPdate(dbdateadd.getValue());
		if(tbplace.getValue()!=null)
			model.setPlace(tbplace.getValue());
		
		if(!tbcontractId.getValue().equals("")){
			model.setContractID(Integer.parseInt(tbcontractId.getValue()));
		}
			
		if(!tbserviceId.getValue().equals("")){
			model.setServiceID(Integer.parseInt(tbserviceId.getValue()));
		}
		if(dbcontractenddate.getValue()!=null)
			model.setContractEnddate(dbcontractenddate.getValue());
		if(dbcontractstartdate.getValue()!=null)
			model.setContractStartdate(dbcontractstartdate.getValue());
		if(dbservicedate.getValue()!=null)
			model.setServiceDate(dbservicedate.getValue());
		Console.log("date of fumigation after saving in update model ........"+model.getDateoffumigation());

//		if (addressOfExporter.getValue() != null) {
//			model.setFromaddress(addressOfExporter.getValue());
//			}
//		if (addressOfImporter.getValue() != null) {
//			model.setTomaddress(addressOfImporter.getValue());
//		}
		
		if (exporterAdd.getValue() != null) {
		model.setFromAddess(exporterAdd.getValue());
		}
	if (importerAdd.getValue() != null) {
		model.setToAddress(importerAdd.getValue());
	}
		
		if(lbnote2.getSelectedIndex()!=0)
			model.setNote1(lbnote2.getItemText(lbnote2.getSelectedIndex()));
		Console.log("date of fumigation after saving in update model ........"+model.getDateoffumigation());

		if(consignerName!=null){
			model.setConsignerName(consignerName);
		}
		if(consignerPocName!=null){
			model.setConsignerPocName(consignerPocName);
		}
		if(consignerCellNo!=null){
			model.setConsignerCellNo(consignerCellNo);
		}
		if(consignerEmail!=null){
			model.setConsignerEmail(consignerEmail);
		}
		
		
		if(consigneeName!=null){
			model.setConsigneeName(consigneeName);
		}
		if(consigneePocName!=null){
			model.setConsigneePocName(consigneePocName);
		}
		if(consigneeCellNo!=null){
			model.setConsigneeCellNo(consigneeCellNo);
		}
		if(consigneeEmail!=null){
			model.setConsigneeEmail(consigneeEmail);
		}
		Console.log("date of fumigation after saving in update model ........"+model.getDateoffumigation());

		//komal
		if(unsheetedContainer.getValue()!=null){
			model.setUnSheetedContainer(unsheetedContainer.getValue());
		}
		if(tbSerialNumber.getValue()!=null){
			model.setSerialNumber(tbSerialNumber.getValue());
		}
//		if(tbDtePPQSRegistrationNumber.getValue()!=null){
//			model.setAeiRegNO(tbDtePPQSRegistrationNumber.getValue());
//		}
		if(olbBranch.getSelectedIndex()!=0){
			model.setBranch(olbBranch.getValue());
		}
		if(tbNameOfCommodity.getValue()!=null){
			model.setNameOfCommodity(tbNameOfCommodity.getValue());
		}
		if(ibQuantity.getValue()!=null){
			model.setQuantity(ibQuantity.getValue());
		}
		if(tbAccreditedFumigator.getValue()!=null){
			model.setAccreditedFumigator(tbAccreditedFumigator.getValue());
		}
		if(dbdateOfcompletion.getValue()!=null){
			model.setCompletionDate(dbdateOfcompletion.getValue());
		}
		Console.log("date of fumigation after saving in update model ........"+model.getDateoffumigation());
        /** date 24.5.2018 added by komal**/
		if(olbNumberRange.getValue()!=null){
			model.setNumberRange(olbNumberRange.getValue(olbNumberRange.getSelectedIndex()));
		}
		/**
		 * UPdated By: Viraj
		 * Date: 13-04-2019
		 * Description: To store additional declaration
		 */
		if(taAddDeclaration.getValue() != null) {
			model.setAddDeclaration(taAddDeclaration.getValue());
		}
		/** Ends **/
		fumigationObj=model;
		
		presenter.setModel(model);
		
	}

	
	@Override
	public void updateView(Fumigation model) {
		
		fumigationObj=model;
		
		this.tbcount.setValue(model.getCount()+"");
		
		if(model.getcInfo()!=null)
			personInfoComposite.setValue(model.getcInfo());

		
		if(model.getShippingBillNo()!=null)
			tbShippingBillNo.setValue(model.getShippingBillNo());
		
		if(model.getCommoditybool()!=null)
			cbcommodity.setValue(model.getCommoditybool());
		
		
		if(model.getPacking()!=null)
			cbpacking.setValue(model.getPacking());
		
		if(model.getBothCommodityandPacking()!=null)
			cbbothComAndPacking.setValue(model.getBothCommodityandPacking());
		
		
		
		if(model.getStackUnderSheet()!=null)
			stackUnderSheet.setValue(model.getStackUnderSheet());
		
		if(model.getMoredecl()!=null)
			tadMoreecl.setValue(model.getMoredecl());
		
		if(model.getContainerUnderSheet()!=null)
			ContainerUnderSheet.setValue(model.getContainerUnderSheet());
		
		
		if(model.getPermanentChamber()!=null)
			permantChamber.setValue(model.getPermanentChamber());
		
		if(model.getPressureTestedContainer()!=null)
			PressureTestedContaner.setValue(model.getPressureTestedContainer());
		
		
		
		if(model.getContainerNo()!=null)
			containerNamber.setValue(model.getContainerNo());
	
		if(model.getConsignmentlink()!=null)
			tbConsignmentLink.setValue(model.getConsignmentlink());
			
		/** date 23.6.2018 added by komal for port of country **/
		if(model.getPortncountryloading()!=null)
			olbportcountyloading.setValue(model.getPortncountryloading());
		
		if(model.getVentilation()!=null)
			tbvetilation.setValue(model.getVentilation());
		
		if(model.getRegistrationno()!=null)
			tbregistrationNo.setValue(model.getRegistrationno());
			
		if(model.getCountryofdestination()!=null)
			olbcountrydestinatoin.setValue(model.getCountryofdestination());
		
		if(model.getImportcountry()!=null)
		olbcountryOfOrigin.setValue(model.getImportcountry());
		
		
		if(model.getFromCompanyname()!=null)
			nameExporter.setValue(model.getFromCompanyname());
		
		if(model.getToCompanyname()!=null)
			nameImporter.setValue(model.getToCompanyname());
			
		if(model.getAeiRegNO()!=null)
			tbAEI_RegNo.setValue(model.getAeiRegNO());
		
		if(model.getAfasRegNo()!=null)
			tbAFAS_RegNo.setValue(model.getAfasRegNo());
			
		
		Console.log("in side update view date of issue is "+model.getDateofissue());
		if(model.getDateofissue()!=null)
			dbdateofissue.setValue(model.getDateofissue());
		
		if(model.getCertificateNo()!=null)
			tbCertificateNo.setValue(model.getCertificateNo());
		
		if(model.getNameoffumigation()!=null)
		{
			for(int i=0;i<lbnamefumigation.getItemCount();i++)
			{
				String data=lbnamefumigation.getItemText(i);
				if(data.equals(model.getNameoffumigation()))
				{
					lbnamefumigation.setSelectedIndex(i);
					break;
				}
			}
		}
		
		
		if(model.getDateoffumigation()!=null)
			dbdatefumigation.setValue(model.getDateoffumigation());
		
		if(model.getPlaceoffumigation()!=null)
			tbplaceoffumigation.setValue(model.getPlaceoffumigation());
		
		if(model.getDoseratefumigation()!=null)
			olbAQISDoseRate.setValue(model.getDoseratefumigation());
		
		if(model.getDurartionfumigation()!=null)
			olbExposurePeriod.setValue(model.getDurartionfumigation());
		
		if(model.getMinairtemp()!=null)
			olbForecastminairtemp.setValue(model.getMinairtemp());
		
		if(model.getAppliedDoseRate()!=null)
			olbAppliedDoseRate.setValue(model.getAppliedDoseRate());
		
		if(model.getDeclaration()!=null)
			tadeclaration.setValue(model.getDeclaration());
		
		if(model.getAdditionaldeclaration()!=null)
			taadditionalDeclaration.setValue(model.getAdditionaldeclaration());
		
		if(model.getPdate()!=null)
			dbdateadd.setValue(model.getPdate());
		
		if(model.getPlace()!=null)
			tbplace.setValue(model.getPlace());
		
		if(model.getContractID()!=null)
			tbcontractId.setValue(model.getContractID()+"");
		
		if(model.getServiceID()!=null)
			tbserviceId.setValue(model.getServiceID()+"");
		
		if(model.getContractEnddate()!=null)
			dbcontractenddate.setValue(model.getContractEnddate());
		
		if(model.getContractStartdate()!=null)
			dbcontractstartdate.setValue(model.getContractStartdate());
		
		if(model.getServiceDate()!=null)
			dbservicedate.setValue(model.getServiceDate());
		
//		if(model.getFromaddress()!=null)
//			addressOfExporter.setValue(model.getFromaddress());
//		
//		if(model.getTomaddress()!=null)
//			addressOfImporter.setValue(model.getTomaddress());
//		
		//Add by jayshree
		if(model.getFromAddess()!=null)
		exporterAdd.setValue(model.getFromAddess());
	
		if(model.getToAddress()!=null)
		importerAdd.setValue(model.getToAddress());
	
		
		if(model.getNote1()!=null)
		{
			for(int i=0;i<lbnote2.getItemCount();i++)
			{
				String data=lbnote2.getItemText(i);
				if(data.equals(model.getNote1()))
				{
					lbnote2.setSelectedIndex(i);
					break;
				}
			}
		}
		//komal
				if(model.getUnSheetedContainer()!=null){
					unsheetedContainer.setValue(model.getUnSheetedContainer());	
				}
				tbSerialNumber.setValue(model.getSerialNumber());
				if(model.getAeiRegNO()!=null){
					tbDtePPQSRegistrationNumber.setValue(model.getAeiRegNO());
				}
				if(model.getBranch()!=null){
					olbBranch.setValue(model.getBranch());
				}
				if(model.getNameOfCommodity()!=null){					
					tbNameOfCommodity.setValue(model.getNameOfCommodity());
				}
			    ibQuantity.setValue(model.getQuantity());
			    if(model.getAccreditedFumigator()!=null){
			    	tbAccreditedFumigator.setValue(model.getAccreditedFumigator());
			    }
				if(model.getCompletionDate()!=null){
					dbdateOfcompletion.setValue(model.getCompletionDate());
				}
				 /** date 24.5.2018 added by komal**/
				if(model.getNumberRange()!=null){
					olbNumberRange.setValue(model.getNumberRange());
				}
		
				/**
				 * UPdated By: Viraj
				 * Date: 13-04-2019
				 * Description: To store additional declaration
				 */
				if(model.getAddDeclaration() != null) {
					taAddDeclaration.setValue(model.getAddDeclaration());
				}
				/** Ends **/
				
		presenter.setModel(model);
	}
	
	@Override
	public void setEnable(boolean state) {
	
		super.setEnable(state);
		this.tbcount.setEnabled(false);
		this.tbcontractId.setEnabled(false);
		this.tbserviceId.setEnabled(false);
		this.dbcontractenddate.setEnabled(false);
		this.dbcontractstartdate.setEnabled(false);
		this.dbservicedate.setEnabled(false);
		nameExporter.setEnabled(false);
		nameImporter.setEnabled(false);
		/** date 24.5.2018 added by komal**/
		tbCertificateNo.setEnabled(false);
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		
		SuperModel model=new Fumigation();
		model=fumigationObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.FUMIGATIONAUS, fumigationObj.getCount(), null,null,null, false, model, null);
	}

	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

	public void toggleProcessLevelMenu()
	{
		Fumigation entity=(Fumigation) presenter.getModel();
		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();
			if(entity.getCount()!=0)
			{    
				if(text.equals("Copy Certificate")){
					label.setVisible(true);
				}else{
					label.setVisible(false);
				}
					

			}
		}
	}
	//******************getters and setters **************************
	
	
	public RadioButton getCbcommodity() {
		return cbcommodity;
	}

	public void setCbcommodity(RadioButton cbcommodity) {
		this.cbcommodity = cbcommodity;
	}

	public RadioButton getCbpacking() {
		return cbpacking;
	}

	public void setCbpacking(RadioButton cbpacking) {
		this.cbpacking = cbpacking;
	}

	public RadioButton getCbbothComAndPacking() {
		return cbbothComAndPacking;
	}

	public void setCbbothComAndPacking(RadioButton cbbothComAndPacking) {
		this.cbbothComAndPacking = cbbothComAndPacking;
	}

	public RadioButton getStackUnderSheet() {
		return stackUnderSheet;
	}

	public void setStackUnderSheet(RadioButton stackUnderSheet) {
		this.stackUnderSheet = stackUnderSheet;
	}

	public RadioButton getContainerUnderSheet() {
		return ContainerUnderSheet;
	}

	public void setContainerUnderSheet(RadioButton containerUnderSheet) {
		ContainerUnderSheet = containerUnderSheet;
	}

	public RadioButton getPermantChamber() {
		return permantChamber;
	}

	public void setPermantChamber(RadioButton permantChamber) {
		this.permantChamber = permantChamber;
	}

	public RadioButton getPressureTestedContaner() {
		return PressureTestedContaner;
	}

	public void setPressureTestedContaner(RadioButton pressureTestedContaner) {
		PressureTestedContaner = pressureTestedContaner;
	}

	public TextBox getNameExporter() {
		return nameExporter;
	}

	public void setNameExporter(TextBox nameExporter) {
		this.nameExporter = nameExporter;
	}

	public TextBox getNameImporter() {
		return nameImporter;
	}

	public void setNameImporter(TextBox nameImporter) {
		this.nameImporter = nameImporter;
	}

	public TextBox getContainerNamber() {
		return containerNamber;
	}

	public void setContainerNamber(TextBox containerNamber) {
		this.containerNamber = containerNamber;
	}

	public TextBox getTbplace() {
		return tbplace;
	}

	public void setTbplace(TextBox tbplace) {
		this.tbplace = tbplace;
	}

	public DateBox getDbdateadd() {
		return dbdateadd;
	}

	public void setDbdateadd(DateBox dbdateadd) {
		this.dbdateadd = dbdateadd;
	}

	public TextArea getTadeclaration() {
		return tadeclaration;
	}

	public void setTadeclaration(TextArea tadeclaration) {
		this.tadeclaration = tadeclaration;
	}

	public TextArea getTaadditionalDeclaration() {
		return taadditionalDeclaration;
	}

	public void setTaadditionalDeclaration(TextArea taadditionalDeclaration) {
		this.taadditionalDeclaration = taadditionalDeclaration;
	}

	public TextBox getTbCertificateNo() {
		return tbCertificateNo;
	}

	public void setTbCertificateNo(TextBox tbCertificateNo) {
		this.tbCertificateNo = tbCertificateNo;
	}

	public TextBox getTbcount() {
		return tbcount;
	}

	public void setTbcount(TextBox tbcount) {
		this.tbcount = tbcount;
	}

	public TextBox getTbConsignmentLink() {
		return tbConsignmentLink;
	}

	public void setTbConsignmentLink(TextBox tbConsignmentLink) {
		this.tbConsignmentLink = tbConsignmentLink;
	}

	

	public ObjectListBox<Config> getOlbportcountyloading() {
		return olbportcountyloading;
	}

	public void setOlbportcountyloading(ObjectListBox<Config> olbportcountyloading) {
		this.olbportcountyloading = olbportcountyloading;
	}

	public TextBox getTbregistrationNo() {
		return tbregistrationNo;
	}

	public void setTbregistrationNo(TextBox tbregistrationNo) {
		this.tbregistrationNo = tbregistrationNo;
	}

	public TextBox getTbAFAS_RegNo() {
		return tbAFAS_RegNo;
	}

	public void setTbAFAS_RegNo(TextBox tbAFAS_RegNo) {
		this.tbAFAS_RegNo = tbAFAS_RegNo;
	}

	public TextBox getTbAEI_RegNo() {
		return tbAEI_RegNo;
	}

	public void setTbAEI_RegNo(TextBox tbAEI_RegNo) {
		this.tbAEI_RegNo = tbAEI_RegNo;
	}

	public DateBox getDbdateofissue() {
		return dbdateofissue;
	}

	public void setDbdateofissue(DateBox dbdateofissue) {
		this.dbdateofissue = dbdateofissue;
	}

	public ObjectListBox<Country> getOlbcountrydestinatoin() {
		return olbcountrydestinatoin;
	}

	public void setOlbcountrydestinatoin(
			ObjectListBox<Country> olbcountrydestinatoin) {
		this.olbcountrydestinatoin = olbcountrydestinatoin;
	}

	public ListBox getLbnamefumigation() {
		return lbnamefumigation;
	}

	public void setLbnamefumigation(ListBox lbnamefumigation) {
		this.lbnamefumigation = lbnamefumigation;
	}

	public DateBox getDbdatefumigation() {
		return dbdatefumigation;
	}

	public void setDbdatefumigation(DateBox dbdatefumigation) {
		this.dbdatefumigation = dbdatefumigation;
	}

	public TextBox getTbplaceoffumigation() {
		return tbplaceoffumigation;
	}

	public void setTbplaceoffumigation(TextBox tbplaceoffumigation) {
		this.tbplaceoffumigation = tbplaceoffumigation;
	}

	/**
	 * Updated By: Viraj
	 * Date: 05-04-2019
	 * Description: Changed dropdownto textbox 
	 */
	public TextBox getOlbAQISDoseRate() {
		return olbAQISDoseRate;
	}

	public void setOlbAQISDoseRate(TextBox olbAQISDoseRate) {
		this.olbAQISDoseRate = olbAQISDoseRate;
	}

	public TextBox getOlbAppliedDoseRate() {
		return olbAppliedDoseRate;
	}

	public void setOlbAppliedDoseRate(TextBox olbAppliedDoseRate) {
		this.olbAppliedDoseRate = olbAppliedDoseRate;
	}

	public TextBox getOlbExposurePeriod() {
		return olbExposurePeriod;
	}

	public void setOlbExposurePeriod(TextBox olbExposurePeriod) {
		this.olbExposurePeriod = olbExposurePeriod;
	}

	public TextBox getOlbForecastminairtemp() {
		return olbForecastminairtemp;
	}

	public void setOlbForecastminairtemp(TextBox olbForecastminairtemp) {
		this.olbForecastminairtemp = olbForecastminairtemp;
	}
	/** Ends **/
//	public AddressComposite getAddressOfExporter() {
//		return addressOfExporter;
//	}
//
//	public void setAddressOfExporter(AddressComposite addressOfExporter) {
//		this.addressOfExporter = addressOfExporter;
//	}
//
//	public AddressComposite getAddressOfImporter() {
//		return addressOfImporter;
//	}
//
//	public void setAddressOfImporter(AddressComposite addressOfImporter) {
//		this.addressOfImporter = addressOfImporter;
//	}

	public TextBox getTbcontractId() {
		return tbcontractId;
	}

	public void setTbcontractId(TextBox tbcontractId) {
		this.tbcontractId = tbcontractId;
	}

	public TextBox getTbserviceId() {
		return tbserviceId;
	}

	public void setTbserviceId(TextBox tbserviceId) {
		this.tbserviceId = tbserviceId;
	}

	public DateBox getDbcontractstartdate() {
		return dbcontractstartdate;
	}

	public void setDbcontractstartdate(DateBox dbcontractstartdate) {
		this.dbcontractstartdate = dbcontractstartdate;
	}

	public DateBox getDbcontractenddate() {
		return dbcontractenddate;
	}

	public void setDbcontractenddate(DateBox dbcontractenddate) {
		this.dbcontractenddate = dbcontractenddate;
	}

	public DateBox getDbservicedate() {
		return dbservicedate;
	}

	public void setDbservicedate(DateBox dbservicedate) {
		this.dbservicedate = dbservicedate;
	}

	public Label getTargetOfFumigation() {
		return targetOfFumigation;
	}

	public void setTargetOfFumigation(Label targetOfFumigation) {
		this.targetOfFumigation = targetOfFumigation;
	}

	public Label getNote1() {
		return note1;
	}

	public void setNote1(Label note1) {
		this.note1 = note1;
	}

	public Label getNote2() {
		return note2;
	}

	public void setNote2(Label note2) {
		this.note2 = note2;
	}

	public Label getContainerNum() {
		return containerNum;
	}

	public void setContainerNum(Label containerNum) {
		this.containerNum = containerNum;
	}

	public ListBox getLbnote2() {
		return lbnote2;
	}

	public void setLbnote2(ListBox lbnote2) {
		this.lbnote2 = lbnote2;
	}

	public ObjectListBox<Declaration> getDeclarationMessageList() {
		return declarationMessageList;
	}

	public void setDeclarationMessageList(
			ObjectListBox<Declaration> declarationMessageList) {
		this.declarationMessageList = declarationMessageList;
	}

	public Button getAddDeclarationButton() {
		return addDeclarationButton;
	}

	public Button getAddDeclarationButton1() {
		return addDeclarationButton1;
	}

	public void setAddDeclarationButton1(Button addDeclarationButton1) {
		this.addDeclarationButton1 = addDeclarationButton1;
	}

	public void setAddDeclarationButton(Button addDeclarationButton) {
		this.addDeclarationButton = addDeclarationButton;
	}

	public TextArea getTadMoreecl() {
		return tadMoreecl;
	}

	public void setTadMoreecl(TextArea tadMoreecl) {
		this.tadMoreecl = tadMoreecl;
	}

	public TextBox getTbShippingBillNo() {
		return tbShippingBillNo;
	}

	public void setTbShippingBillNo(TextBox tbShippingBillNo) {
		this.tbShippingBillNo = tbShippingBillNo;
	}

	public ObjectListBox<Declaration> getDeclarationMessageList1() {
		return declarationMessageList1;
	}

	public void setDeclarationMessageList1(
			ObjectListBox<Declaration> declarationMessageList1) {
		this.declarationMessageList1 = declarationMessageList1;
	}

	public ObjectListBox<Declaration> getDeclarationMessageList2() {
		return declarationMessageList2;
	}

	public void setDeclarationMessageList2(
			ObjectListBox<Declaration> declarationMessageList2) {
		this.declarationMessageList2 = declarationMessageList2;
	}

	public Button getAddDeclarationButton2() {
		return addDeclarationButton2;
	}

	public void setAddDeclarationButton2(Button addDeclarationButton2) {
		this.addDeclarationButton2 = addDeclarationButton2;
	}

	public ObjectListBox<Country> getOlbcountryOfOrigin() {
		return olbcountryOfOrigin;
	}

	public void setOlbcountryOfOrigin(ObjectListBox<Country> olbcountryOfOrigin) {
		this.olbcountryOfOrigin = olbcountryOfOrigin;
	}

	public TextBox getTbvetilation() {
		return tbvetilation;
	}

	public void setTbvetilation(TextBox tbvetilation) {
		this.tbvetilation = tbvetilation;
	}

	public Label getVentilationfirst() {
		return ventilationfirst;
	}

	public void setVentilationfirst(Label ventilationfirst) {
		this.ventilationfirst = ventilationfirst;
	}

	public Label getVentilatioSecond() {
		return ventilatioSecond;
	}

	public void setVentilatioSecond(Label ventilatioSecond) {
		this.ventilatioSecond = ventilatioSecond;
	}

	public Button getAddbyCONSIGNERComposite() {
		return addbyCONSIGNERComposite;
	}

	public void setAddbyCONSIGNERComposite(Button addbyCONSIGNERComposite) {
		this.addbyCONSIGNERComposite = addbyCONSIGNERComposite;
	}

	public PersonInfoComposite getPersonInfoCONSIGNERComposite() {
		return personInfoCONSIGNERComposite;
	}

	public void setPersonInfoCONSIGNERComposite(
			PersonInfoComposite personInfoCONSIGNERComposite) {
		this.personInfoCONSIGNERComposite = personInfoCONSIGNERComposite;
	}

	public Button getAddbyCONSIGNEEComposite() {
		return addbyCONSIGNEEComposite;
	}

	public void setAddbyCONSIGNEEComposite(Button addbyCONSIGNEEComposite) {
		this.addbyCONSIGNEEComposite = addbyCONSIGNEEComposite;
	}

	public PersonInfoComposite getPersonInfoCONSIGNEEComposite() {
		return personInfoCONSIGNEEComposite;
	}

	public void setPersonInfoCONSIGNEEComposite(
			PersonInfoComposite personInfoCONSIGNEEComposite) {
		this.personInfoCONSIGNEEComposite = personInfoCONSIGNEEComposite;
	}

	public Button getNewFromCustomer() {
		return newFromCustomer;
	}

	public void setNewFromCustomer(Button newFromCustomer) {
		this.newFromCustomer = newFromCustomer;
	}

	public Button getNewToCustomer() {
		return newToCustomer;
	}

	public void setNewToCustomer(Button newToCustomer) {
		this.newToCustomer = newToCustomer;
	}

	public String getConsignerName() {
		return consignerName;
	}

	public void setConsignerName(String consignerName) {
		this.consignerName = consignerName;
	}

	public String getConsignerPocName() {
		return consignerPocName;
	}

	public void setConsignerPocName(String consignerPocName) {
		this.consignerPocName = consignerPocName;
	}

	public String getConsignerEmail() {
		return consignerEmail;
	}

	public void setConsignerEmail(String consignerEmail) {
		this.consignerEmail = consignerEmail;
	}

	public Long getConsignerCellNo() {
		return consignerCellNo;
	}

	public void setConsignerCellNo(Long consignerCellNo) {
		this.consignerCellNo = consignerCellNo;
	}

	public String getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	public String getConsigneePocName() {
		return consigneePocName;
	}

	public void setConsigneePocName(String consigneePocName) {
		this.consigneePocName = consigneePocName;
	}

	public String getConsigneeEmail() {
		return consigneeEmail;
	}

	public void setConsigneeEmail(String consigneeEmail) {
		this.consigneeEmail = consigneeEmail;
	}

	public Long getConsigneeCellNo() {
		return consigneeCellNo;
	}

	public void setConsigneeCellNo(Long consigneeCellNo) {
		this.consigneeCellNo = consigneeCellNo;
	}

	public RadioButton getUnsheetedContainer() {
		return unsheetedContainer;
	}

	public void setUnsheetedContainer(RadioButton unsheetedContainer) {
		this.unsheetedContainer = unsheetedContainer;
	}

	public Button getSelectTemplateButton() {
		return selectTemplateButton;
	}

	public void setSelectTemplateButton(Button selectTemplateButton) {
		this.selectTemplateButton = selectTemplateButton;
	}

	public TextBox getIbQuantity() {
		return ibQuantity;
	}

	public void setIbQuantity(TextBox ibQuantity) {
		this.ibQuantity = ibQuantity;
	}

	public TextArea getExporterAdd() {
		return exporterAdd;
	}

	public void setExporterAdd(TextArea exporterAdd) {
		this.exporterAdd = exporterAdd;
	}
	
	
	public TextArea getImporterAdd() {
		return importerAdd;
	}

	public void setImporterAdd(TextArea importerAdd) {
		this.importerAdd = importerAdd;
	}
	/**
	 *Updated By: Viraj
	 *Date: 13-04-2019
	 *Description: To store additional declaration 
	 */
	public TextArea getTaAddDeclaration() {
		return taAddDeclaration;
	}

	public void setTaAddDeclaration(TextArea taAddDeclaration) {
		this.taAddDeclaration = taAddDeclaration;
	}
	/** Ends **/
	
//	@Override
//	public void onChange(ChangeEvent event) {
//		// TODO Auto-generated method stub
//		if(event.getSource().equals(olbBranch)){
//			if(olbBranch.getSelectedIndex()!=0){
////				String b = ""+olbBranch.getValue().charAt(0);
////				String certificateNumber = "PMI/"+b+"/"+"AFAS/";
////					certificateNumber += tbSerialNumber.getValue()+"/";
////				}
////				String year = AppUtility.GetCurrentFinancialYear();
////				certificateNumber += year;
////				tbCertificateNo.setValue(certificateNumber);
//		}
//	  }
//	}
	
}


//package com.slicktechnologies.client.views.fumigationAustralia;
//
//import java.util.Date;
//import java.util.Vector;
//
//import com.google.gwt.user.client.ui.Button;
//import com.google.gwt.user.client.ui.CheckBox;
//import com.google.gwt.user.client.ui.InlineLabel;
//import com.google.gwt.user.client.ui.Label;
//import com.google.gwt.user.client.ui.ListBox;
//import com.google.gwt.user.client.ui.RadioButton;
//import com.google.gwt.user.client.ui.TextArea;
//import com.google.gwt.user.client.ui.TextBox;
//import com.google.gwt.user.datepicker.client.DateBox;
//import com.simplesoftwares.client.library.FieldType;
//import com.simplesoftwares.client.library.FormField;
//import com.simplesoftwares.client.library.FormFieldBuilder;
//import com.simplesoftwares.client.library.FlexForm.FormStyle;
//import com.simplesoftwares.client.library.appskeleton.AppMemory;
//import com.simplesoftwares.client.library.appskeleton.ScreeenState;
//import com.simplesoftwares.client.library.appstructure.SuperModel;
//import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
//import com.simplesoftwares.client.library.appstructure.search.Filter;
//import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
//import com.simplesoftwares.client.library.composite.AddressComposite;
//import com.simplesoftwares.client.library.composite.PersonInfoComposite;
//import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
//import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
//import com.slicktechnologies.client.login.LoginPresenter;
//import com.slicktechnologies.client.utility.AppConstants;
//import com.slicktechnologies.client.utility.AppUtility;
//import com.slicktechnologies.client.utility.Screen;
//import com.slicktechnologies.client.utils.Console;
//import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
//import com.slicktechnologies.shared.common.fumigation.Fumigation;
//import com.slicktechnologies.shared.common.helperlayer.Config;
//import com.slicktechnologies.shared.common.helperlayer.Declaration;
//import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
//import com.slicktechnologies.shared.common.humanresourcelayer.Country;
//import com.slicktechnologies.shared.common.role.AuthorizationHelper;
//
//public class FumigationAustraliaForm extends FormScreen<Fumigation>{
//	
////	CheckBox cbcommodity,cbpacking,cbbothComAndPacking;
////	CheckBox stackUnderSheet,ContainerUnderSheet,permantChamber,PressureTestedContaner;
//	
//	RadioButton cbcommodity,cbpacking,cbbothComAndPacking;
//	RadioButton stackUnderSheet,ContainerUnderSheet,permantChamber,PressureTestedContaner;
//	TextBox nameExporter,nameImporter,containerNamber;
//	//**************************
//	TextBox tbShippingBillNo;
//	
//	
//	ObjectListBox<Declaration> declarationMessageList;
//	ObjectListBox<Declaration> declarationMessageList1;
//	ObjectListBox<Declaration> declarationMessageList2;
//	Button addDeclarationButton,addDeclarationButton1,addDeclarationButton2;
//	
//	TextBox tbplace;
//	DateBox dbdateadd;
//	
//	TextArea tadeclaration,taadditionalDeclaration,tadMoreecl;
//	
//	TextBox tbCertificateNo,tbcount,tbCommodity,tbConsignmentLink,portOfLoading;
//	TextBox tbregistrationNo,tbAFAS_RegNo,tbAEI_RegNo;
//	DateBox dbdateofissue;
//	ObjectListBox<Country> olbcountrydestinatoin;
//	ObjectListBox<Country> olbcountryOfOrigin;
//	
//	ListBox lbnamefumigation;
//	DateBox dbdatefumigation; 
//	TextBox tbplaceoffumigation;
//	
//	ObjectListBox<Config> olbAQISDoseRate;
//	ObjectListBox<Config> olbAppliedDoseRate;
//	ObjectListBox<Config> olbExposurePeriod;
//	
//	ObjectListBox<Config> olbForecastminairtemp;
//	
//	AddressComposite addressOfExporter;
//	AddressComposite addressOfImporter;
//	
//	TextBox tbcontractId,tbvetilation;
//	TextBox tbserviceId;
//	DateBox dbcontractstartdate;
//	DateBox dbcontractenddate;
//	DateBox dbservicedate;
//
//	Label targetOfFumigation,ventilationfirst,ventilatioSecond;
//	Label note1,note2,containerNum;
//	ListBox lbnote2;
//	
//	
//	Button addbyCONSIGNERComposite;
//	PersonInfoComposite personInfoCONSIGNERComposite;
//	Button addbyCONSIGNEEComposite;
//	PersonInfoComposite personInfoCONSIGNEEComposite;
//	
//	Button newFromCustomer;
//	Button newToCustomer;
//	
//	
//	String consignerName;
//	String consignerPocName;
//	String consignerEmail;
//	Long consignerCellNo;
//	
//	
//	
//	String consigneeName;
//	String consigneePocName;
//	String consigneeEmail;
//	Long consigneeCellNo;
//	
//	Fumigation fumigationObj;
//	
//	public FumigationAustraliaForm() {
//		super();
//		createGui();
//	}
//	
//	public FumigationAustraliaForm  (String[] processlevel, FormField[][] fields,
//			FormStyle formstyle) 
//	{
//		super(processlevel, fields, formstyle);
//		createGui();
//		
//	}
//	
//	
//
//	private void initalizeWidget(){
//
//		
//		declarationMessageList = new ObjectListBox<Declaration>();
//		MyQuerry querry = new MyQuerry(new Vector<Filter>(),new Declaration());
//		declarationMessageList.MakeLive(querry);
//		
//		
//		declarationMessageList1 = new ObjectListBox<Declaration>();
//		MyQuerry querry123 = new MyQuerry(new Vector<Filter>(),new Declaration());
//		declarationMessageList1.MakeLive(querry123);
//		
//		declarationMessageList2 = new ObjectListBox<Declaration>();
//		MyQuerry querry1234 = new MyQuerry(new Vector<Filter>(),new Declaration());
//		declarationMessageList2.MakeLive(querry1234);
//		
//		
//		tbShippingBillNo= new TextBox();
//		
//		addDeclarationButton =new Button("Add Declaration");
//		addDeclarationButton1= new Button("Add Declaration");
//		addDeclarationButton2= new Button("Add Note");
//		tbplace=new TextBox();
//		dbdateadd=new  DateBoxWithYearSelector();
//		
//		lbnamefumigation=new ListBox();
//		lbnamefumigation.addItem("--SELECT--");
//		lbnamefumigation.addItem("Methyl Bromide");
//		lbnamefumigation.addItem("Aluminium Phosphide");
//		
//		dbdatefumigation=new  DateBoxWithYearSelector();
//		tbplaceoffumigation=new TextBox();
//		
//		addressOfExporter =new AddressComposite(true);
//		addressOfImporter =new AddressComposite(true);
//		targetOfFumigation=  new Label("Target of Fumigation :");
//	
//		ventilationfirst= new Label("Ventilation  Final TLV reading (ppm) :");
//		ventilatioSecond = new Label("(not required for Stack or Permanent Chamber fumigations)");
//		tbvetilation= new TextBox();
//		
//		
//		stackUnderSheet = new RadioButton("");
//		ContainerUnderSheet = new RadioButton("");
//		permantChamber = new RadioButton("");
//		PressureTestedContaner = new RadioButton("");
//		
//		containerNum = new Label("Container number/s(where applicable) :");
//		 note1 =new Label("How was the fumigation conducted ?");
//		 note2 =new Label("Does the target of the fumigation conform to the AQIS plastic wrapping,"
//		 		+ "impervious surface and timber thickness requirements at the timeof fumigation ?");
//		
//		 lbnote2 =new ListBox();
//		 lbnote2.addItem("--SELECT--");
//		 lbnote2.addItem("YES");
//		 lbnote2.addItem("NO");
//		 lbnote2.addItem("N.A.");
//		
//		tbAFAS_RegNo= new TextBox();
//		tbAEI_RegNo =new TextBox();
//		
//		olbAQISDoseRate=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbAQISDoseRate, Screen.DOSAGERATEOFFUMIGANT);
//		
//		olbAppliedDoseRate=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbAppliedDoseRate, Screen.DOSAGERATEOFFUMIGANT);
//		
//		
//		olbExposurePeriod=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbExposurePeriod, Screen.DURATIONOFFUMIGATION);
//		
//		olbForecastminairtemp=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbForecastminairtemp, Screen.MINIMUMAIRTEMP);
//		
//		tbcount=new TextBox();
//		tbcount.setEnabled(false);
//		
//		dbdateofissue=new  DateBoxWithYearSelector();
//		
//		
//		cbcommodity=new RadioButton("");
//		cbpacking = new RadioButton("");
//		cbbothComAndPacking= new RadioButton("");
//		
//		dbservicedate=new DateBoxWithYearSelector();
//		dbservicedate.setEnabled(false);
//		tbcontractId=new TextBox();
//		tbcontractId.setEnabled(false);
//		tbserviceId=new TextBox();
//		tbserviceId.setEnabled(false);
//		dbcontractstartdate=new DateBoxWithYearSelector();
//		dbcontractstartdate.setEnabled(false);
//		dbcontractenddate=new DateBoxWithYearSelector();
//		dbcontractenddate.setEnabled(false);
//
//		tadeclaration = new TextArea();
//		tadMoreecl = new TextArea();
//		taadditionalDeclaration= new TextArea();
//		
//		nameImporter=new TextBox();
//		nameImporter.setEnabled(false);
//		nameExporter= new TextBox();
//		nameExporter.setEnabled(false);
//		containerNamber = new TextBox();
//		tbCertificateNo= new TextBox();
//		tbregistrationNo= new TextBox();
//		cbcommodity = new RadioButton("");
//		cbpacking =new RadioButton("");
//		cbbothComAndPacking =new RadioButton("");
//		
//		tbCommodity =new TextBox();
//		tbConsignmentLink= new TextBox();
//		portOfLoading= new TextBox();
//		olbcountryOfOrigin= new ObjectListBox<Country>();
//		MyQuerry quer=new MyQuerry();
//		quer.setQuerryObject(new Country());
//		olbcountryOfOrigin.MakeLive(quer);
//		
//		olbcountrydestinatoin=new ObjectListBox<Country>();
//		MyQuerry que=new MyQuerry();
//		que.setQuerryObject(new Country());
//		olbcountrydestinatoin.MakeLive(que);
//		
//		 addbyCONSIGNERComposite = new Button("Add");
//		 	MyQuerry custquerry1 = new MyQuerry();
//			Filter custfilteer1 = new Filter();
//			custfilteer1.setQuerryString("status");
//			custfilteer1.setBooleanvalue(true);
//			custquerry1.setQuerryObject(new Customer());
//			personInfoCONSIGNERComposite = new PersonInfoComposite(custquerry1, false);
//		 
//		
//		
//		addbyCONSIGNEEComposite= new Button("Add");
//		
//		MyQuerry custquerry = new MyQuerry();
//		Filter custfilteer = new Filter();
//		custfilteer.setQuerryString("status");
//		custfilteer.setBooleanvalue(true);
//		custquerry.setQuerryObject(new Customer());
//		personInfoCONSIGNEEComposite = new PersonInfoComposite(custquerry, false);
//		
//		newFromCustomer=new Button("New Exporter");
//		newToCustomer=new Button("New Importer");
//	}
//	
//	@Override
//	public void setCount(int count) {
//
//		tbcount.setValue(count+"");
//	
//	}
//	
//	
//	
//	@Override
//	public void toggleAppHeaderBarMenu() {
//
//		
//		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
//				{
//					menus[k].setVisible(true); 
//				}
//				else
//					menus[k].setVisible(false);  		   
//			}
//		}
//
//		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
//					menus[k].setVisible(true); 
//
//				else
//					menus[k].setVisible(false);  		   
//			}
//		}
//
//		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.equals("Discard")||text.equals("Print")||text.equals("Edit")||text.equals("Email")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
//					menus[k].setVisible(true); 
//
//				else
//					menus[k].setVisible(false);  		   
//			}
//		}
//		
//		AuthorizationHelper.setAsPerAuthorization(Screen.FUMIGATIONAUSTRALIA,LoginPresenter.currentModule.trim());
//	}
//
//	@Override
//	public void createScreen() {
//		
//		initalizeWidget();
//		
//		this.processlevelBarNames = new String[] { "New" , "Copy Certificate"};
//		FormFieldBuilder fbuilder;
//		fbuilder = new FormFieldBuilder();
//		FormField fgroupingfumigationInformation=fbuilder.setlabel("* Fumigation Details").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		
//		fbuilder = new FormFieldBuilder(" Commodity",cbcommodity);
//		FormField cbcommodity= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(0).build();	
//		
//		fbuilder = new FormFieldBuilder(" Packing",cbpacking);
//		FormField cbpacking= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(0).build();	
//		
//		fbuilder = new FormFieldBuilder("Both Commodity and Packing ",cbbothComAndPacking);
//		FormField cbbothComAndPacking= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(0).build();	
//		
//		fbuilder = new FormFieldBuilder(" Commodity",tbCommodity);
//		FormField tbCommodity= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(0).build();	
//		
//		fbuilder = new FormFieldBuilder(" Consignment Link",tbConsignmentLink);
//		FormField tbConsignmentLink= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(0).build();	
//
//		
//		fbuilder = new FormFieldBuilder(" Port of Loading",portOfLoading);
//		FormField portOfLoading= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(0).build();	
//
//		fbuilder = new FormFieldBuilder("Country of Destination",olbcountrydestinatoin);
//		FormField countrydesct= fbuilder.setMandatory(true).setMandatoryMsg("Country of Destination is Mandatory").setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Country of Origin",olbcountryOfOrigin);
//		FormField olbcountryOfOrigin= fbuilder.setMandatory(true).setMandatoryMsg("Country of false is Mandatory").setColSpan(0).build();
//		
//		
//		fbuilder = new FormFieldBuilder("Fumigation ID",tbcount);
//		FormField fumigationcount= fbuilder.setRowSpan(0).setColSpan(0).build();	
//		
//		fbuilder = new FormFieldBuilder(" Registration No.",tbregistrationNo);
//		FormField treatmentcardno= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(0).build();	
//		
//		fbuilder = new FormFieldBuilder(" Certificate No.",tbCertificateNo);
//		FormField tbCertificateNo= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(0).build();	
//		
//		fbuilder = new FormFieldBuilder("  Date of Issue",dbdateofissue);
//		FormField dateissue= fbuilder.setMandatory(true).setMandatoryMsg("Date is Mandatory").setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder(" AFAS Registration No.",tbAFAS_RegNo);
//		FormField tbAFAS_RegNo= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(0).build();	
//		
//		
//		fbuilder = new FormFieldBuilder(" AEI Registration No.",tbAEI_RegNo);
//		FormField tbAEI_RegNo= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(0).build();	
//		
//		fbuilder = new FormFieldBuilder();
//		FormField fgroupingTRITMENTformation=fbuilder.setlabel("* Details of Treatment").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("Name of Fumigation",lbnamefumigation);
//		FormField fumigationname= fbuilder.setMandatory(true).setMandatoryMsg("Name of Fumigation is Mandatory").setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Date Of Fumigation Completed",dbdatefumigation);
//		FormField fumigationdate= fbuilder.setMandatory(true).setMandatoryMsg("Date of Fumigation is Mandatory").setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Place of Fumigation",tbplaceoffumigation);
//		FormField fumigationplace= fbuilder.setMandatory(true).setMandatoryMsg("Place of Fumigation is Mandatory").setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("AQIS Prescribe Dose Rate ",olbAQISDoseRate);
//		FormField olbAQISDoseRate= fbuilder.setMandatory(true).setMandatoryMsg("Dosage Rate of Fumigation is Mandatory").setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Applied Dose Rate ",olbAppliedDoseRate);
//		FormField olbAppliedDoseRate= fbuilder.setMandatory(true).setMandatoryMsg("Dosage Rate of Fumigation is Mandatory").setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Exposure Period(in hrs)",olbExposurePeriod);
//		FormField olbExposurePeriod= fbuilder.setMandatory(true).setMandatoryMsg("Duration of Mandatory").setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Forecast Minimum Temp(in "+"\u00b0"+"C)",olbForecastminairtemp);
//		FormField olbForecastminairtemp= fbuilder.setMandatory(true).setMandatoryMsg("Min. Air Temp. is Mandatory").setColSpan(0).build();
//
//		
//		fbuilder = new FormFieldBuilder();
//		FormField additionExp=fbuilder.setlabel("Name and Address of Exporter").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder(" ",addressOfExporter);
//		FormField addressOfExporter= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("Name ",nameExporter);
//		FormField nameExporter= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(2).build();
//
//		fbuilder = new FormFieldBuilder();
//		FormField addinfoImp=fbuilder.setlabel("Name and Adress of Importer").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("Name",nameImporter);
//		FormField nameImporter= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(2).build();
//		
//		fbuilder = new FormFieldBuilder(" ",addressOfImporter);
//		FormField addressOfImporter= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
//
//		fbuilder = new FormFieldBuilder(" ",targetOfFumigation);
//		FormField targetOfFumigation= fbuilder.setColSpan(1).build();
//		
//		
//		fbuilder = new FormFieldBuilder(" ",note1);
//		FormField note1= fbuilder.setColSpan(1).build();
//		
//		fbuilder = new FormFieldBuilder(" Stack Under Sheet",stackUnderSheet);
//		FormField stackUnderSheet= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(0).build();	
//		
//		fbuilder = new FormFieldBuilder(" Container/s Under Sheet",ContainerUnderSheet);
//		FormField ContainerUnderSheet= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(1).build();	
//		
//		fbuilder = new FormFieldBuilder(" Permanent Chamber",permantChamber);
//		FormField permantChamber= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(1).build();	
//		
//		fbuilder = new FormFieldBuilder(" Pressure Tested Container/s",PressureTestedContaner);
//		FormField PressureTestedContaner= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(1).build();	
//		
//		fbuilder = new FormFieldBuilder(" ",containerNum);
//		FormField containerNum= fbuilder.setRowSpan(0).setColSpan(1).build();
//		
//		fbuilder = new FormFieldBuilder(" ",containerNamber);
//		FormField containerNamber= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder(" ",note2);
//		FormField note2= fbuilder.setColSpan(3).build();
//		
//		fbuilder = new FormFieldBuilder(" ",lbnote2);
//		FormField lbnote2= fbuilder.setColSpan(0).build();
//		  
//		
//		fbuilder = new FormFieldBuilder(" ",tbvetilation);
//		FormField tbvetilation= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(1).build();
//		
//		fbuilder = new FormFieldBuilder(" ",ventilationfirst);
//		FormField ventilationfirst= fbuilder.setColSpan(1).build();
//		
//		fbuilder = new FormFieldBuilder(" ",ventilatioSecond);
//		FormField ventilatioSecond= fbuilder.setColSpan(2).build();
//		
//		fbuilder = new FormFieldBuilder();
//		FormField information=fbuilder.setlabel("Reference Information").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("Contract ID",tbcontractId);
//		FormField contrctid= fbuilder.setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Service ID",tbserviceId);
//		FormField serviceid= fbuilder.setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Contract Start Date",dbcontractstartdate);
//		FormField contractstart= fbuilder.setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Contract End Date",dbcontractenddate);
//		FormField contractend= fbuilder.setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Service Date",dbservicedate);
//		FormField servicedate= fbuilder.setColSpan(0).build();
//
//		
//		
//		fbuilder = new FormFieldBuilder();
//		FormField declaration=fbuilder.setlabel("Declaration").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("Select Declaration Message ",declarationMessageList);
//	    FormField fdeclarationMessageList= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//	    
//	    fbuilder = new FormFieldBuilder(" ",addDeclarationButton);
//		FormField faddDeclarationButton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
//		
//		fbuilder = new FormFieldBuilder("",tadeclaration);
//		FormField tadeclaration= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
//		
//		
//		
//		fbuilder = new FormFieldBuilder();
//		FormField additionaldeclaration=fbuilder.setlabel("Additional Declaration").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("Select Declaration Message ",declarationMessageList1);
//		FormField fdeclarationMessageList1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder(" ",addDeclarationButton1);
//		FormField faddDeclarationButton1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
//		    
//		fbuilder = new FormFieldBuilder("",taadditionalDeclaration);
//		FormField taadditionalDeclaration= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
//		
//		
//		fbuilder = new FormFieldBuilder("Shipping Bill No",tbShippingBillNo);
//		FormField ftbShippingBillNo= fbuilder.setMandatory(false).setColSpan(0).build();
//		
//		
//		fbuilder = new FormFieldBuilder("Place",tbplace);
//		FormField placefumigation= fbuilder.setMandatory(true).setMandatoryMsg("Place is Mandatory").setColSpan(0).build();
////		fbuilder = new FormFieldBuilder("Date",dbdateadd);
////		FormField datefumigation= fbuilder.setMandatory(true).setMandatoryMsg("Date is Madatory").setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Select Declaration Message ",declarationMessageList2);
//	    FormField fdeclarationMessageList2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//	    
//	    fbuilder = new FormFieldBuilder(" ",addDeclarationButton2);
//		FormField faddDeclarationButton2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
//		
//		fbuilder = new FormFieldBuilder("Note/Footer Information",tadMoreecl);
//		FormField tadMoreecl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("",personInfoCONSIGNEEComposite);
//		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
//		
//		fbuilder = new FormFieldBuilder(" ",addbyCONSIGNEEComposite);
//		FormField faddbyComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		
//		fbuilder = new FormFieldBuilder("",personInfoCONSIGNERComposite);
//		FormField fpersonInfoCONSIGNERComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
//		
//		fbuilder = new FormFieldBuilder(" ",addbyCONSIGNERComposite);
//		FormField faddbyCONSIGNERComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder(" ",newFromCustomer);
//		FormField fnewformCustomer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder(" ",newToCustomer);
//		FormField fnewtoCustomer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		
//		
//		FormField[][] formfield = {
//				{fgroupingfumigationInformation},
//				{fumigationcount,treatmentcardno,tbCertificateNo,dateissue},
//				{olbcountryOfOrigin,ftbShippingBillNo},
//				{targetOfFumigation,cbcommodity,cbpacking,cbbothComAndPacking},
//				{tbCommodity,tbConsignmentLink,portOfLoading,countrydesct},
//				{information},
//				{contrctid,serviceid,contractstart,contractend},
//				{servicedate},				
//				
//				{additionExp},
//				
//				{fpersonInfoCONSIGNERComposite,faddbyCONSIGNERComposite},
//				{nameExporter,fnewformCustomer},
//				{addressOfExporter},
//				
//				{addinfoImp},
//				
//				{fpersonInfoComposite,faddbyComposite},
//				{nameImporter,fnewtoCustomer},
//				{addressOfImporter},
//				
//				{fgroupingTRITMENTformation},
//				{fumigationname,fumigationdate,fumigationplace,olbAQISDoseRate},
//				{olbExposurePeriod,olbForecastminairtemp,olbAppliedDoseRate},
//				{note1},
//				{stackUnderSheet,ContainerUnderSheet,permantChamber,PressureTestedContaner},
//				{containerNum,containerNamber},
//				{note2,lbnote2},
//				{ventilationfirst,tbvetilation,ventilatioSecond},
//				{declaration},
//				{fdeclarationMessageList,faddDeclarationButton},
//				{tadeclaration},
//				{additionaldeclaration},
//				{fdeclarationMessageList1,faddDeclarationButton1},
//				{taadditionalDeclaration},
////				datefumigation
//				{placefumigation},
//				{fdeclarationMessageList2,faddDeclarationButton2},
//				{tadMoreecl}
//		};
//		
//		this.fields=formfield;
//		
//	
//	}
//
//	@Override
//	public void updateModel(Fumigation model) {
//		
//		model.setAustralia(true);
//		
//		
//		if(tbShippingBillNo.getValue()!=null)
//			model.setShippingBillNo(tbShippingBillNo.getValue());
//		
//		if(cbcommodity.getValue()!=null)
//			model.setCommoditybool(cbcommodity.getValue());
//		if(cbpacking.getValue()!=null)
//			model.setPacking(cbpacking.getValue());
//		if(cbbothComAndPacking.getValue()!=null)
//			model.setBothCommodityandPacking(cbbothComAndPacking.getValue());
//		
//		if(stackUnderSheet.getValue()!=null)
//			model.setStackUnderSheet(stackUnderSheet.getValue());
//		
//		if(ContainerUnderSheet.getValue()!=null)
//			model.setContainerUnderSheet(ContainerUnderSheet.getValue());
//		
//		if(permantChamber.getValue()!=null)
//			model.setPermanentChamber(permantChamber.getValue());
//		if(PressureTestedContaner.getValue()!=null)
//			model.setPressureTestedContainer(PressureTestedContaner.getValue());
//		
//		if(containerNamber.getValue()!=null)
//			model.setContainerNo(containerNamber.getValue());
//			
//		if(tbCommodity.getValue()!=null)
//			model.setCommodity(tbCommodity.getValue());
//			
//		if(tbConsignmentLink.getValue()!=null)
//			model.setConsignmentlink(tbConsignmentLink.getValue());
//		
//		if(portOfLoading.getValue()!=null)
//			model.setPortncountryloading(portOfLoading.getValue());
//		
//		if(tbvetilation.getValue()!=null)
//			model.setVentilation(tbvetilation.getValue());
//		
//		if(olbcountryOfOrigin.getValue()!=null)
//			model.setImportcountry(olbcountryOfOrigin.getValue());
//		
//		if(olbcountrydestinatoin.getValue()!=null)
//			model.setCountryofdestination(olbcountrydestinatoin.getValue());
//		
//		
//		if(tbregistrationNo.getValue()!=null)
//			model.setRegistrationno(tbregistrationNo.getValue());
//		
//		if(olbcountrydestinatoin.getValue()!=null)
//			model.setCountryofdestination(olbcountrydestinatoin.getValue());
//		
//		if(nameExporter.getValue()!=null)
//			model.setFromCompanyname(nameExporter.getValue());
//		
//		if(nameImporter.getValue()!=null)
//			model.setToCompanyname(nameImporter.getValue());
//		
//		if(tbAEI_RegNo.getValue()!=null)
//			model.setAeiRegNO(tbAEI_RegNo.getValue());
//		
//		if(tbAFAS_RegNo.getValue()!=null)
//			model.setAfasRegNo(tbAFAS_RegNo.getValue());
//		Console.log("date of issue before saving in update model ........"+dbdateofissue.getValue());
//		if(dbdateofissue.getValue()!=null)
//			model.setDateofissue(dbdateofissue.getValue());
//		
//		if(tbCertificateNo.getValue()!=null)
//			model.setCertificateNo(tbCertificateNo.getValue());
//		
//		model.setNameoffumigation(lbnamefumigation.getItemText(lbnamefumigation.getSelectedIndex()));
//		
//		if(dbdatefumigation.getValue()!=null)
//			model.setDateoffumigation(dbdatefumigation.getValue());
//		if(tbplaceoffumigation.getValue()!=null)
//			model.setPlaceoffumigation(tbplaceoffumigation.getValue());
//		if(olbAQISDoseRate.getValue()!=null)
//			model.setDoseratefumigation(olbAQISDoseRate.getValue());
//		if(olbExposurePeriod.getValue()!=null)
//			model.setDurartionfumigation(olbExposurePeriod.getValue());
//		if(olbForecastminairtemp.getValue()!=null)
//			model.setMinairtemp(olbForecastminairtemp.getValue());
//		
//		if(olbAppliedDoseRate.getValue()!=null)
//			model.setAppliedDoseRate(olbAppliedDoseRate.getValue());
//		
//		if(tadMoreecl.getValue()!=null)
//			model.setMoredecl(tadMoreecl.getValue());
//		
//		if(tadeclaration.getValue()!=null)
//			model.setDeclaration(tadeclaration.getValue());
//		
//		if(taadditionalDeclaration.getValue()!=null)
//			model.setAdditionaldeclaration(taadditionalDeclaration.getValue());
//		if(dbdateadd.getValue()!=null)
//			model.setPdate(dbdateadd.getValue());
//		if(tbplace.getValue()!=null)
//			model.setPlace(tbplace.getValue());
//		
//		if(!tbcontractId.getValue().equals("")){
//			model.setContractID(Integer.parseInt(tbcontractId.getValue()));
//		}
//			
//		if(!tbserviceId.getValue().equals("")){
//			model.setServiceID(Integer.parseInt(tbserviceId.getValue()));
//		}
//		if(dbcontractenddate.getValue()!=null)
//			model.setContractEnddate(dbcontractenddate.getValue());
//		if(dbcontractstartdate.getValue()!=null)
//			model.setContractStartdate(dbcontractstartdate.getValue());
//		if(dbservicedate.getValue()!=null)
//			model.setServiceDate(dbservicedate.getValue());
//
//		if (addressOfExporter.getValue() != null) {
//			model.setFromaddress(addressOfExporter.getValue());
//			}
//		if (addressOfImporter.getValue() != null) {
//			model.setTomaddress(addressOfImporter.getValue());
//		}
//		model.setNote1(lbnote2.getItemText(lbnote2.getSelectedIndex()));
//		
//		if(consignerName!=null){
//			model.setConsignerName(consignerName);
//		}
//		if(consignerPocName!=null){
//			model.setConsignerPocName(consignerPocName);
//		}
//		if(consignerCellNo!=null){
//			model.setConsignerCellNo(consignerCellNo);
//		}
//		if(consignerEmail!=null){
//			model.setConsignerEmail(consignerEmail);
//		}
//		
//		
//		if(consigneeName!=null){
//			model.setConsigneeName(consigneeName);
//		}
//		if(consigneePocName!=null){
//			model.setConsigneePocName(consigneePocName);
//		}
//		if(consigneeCellNo!=null){
//			model.setConsigneeCellNo(consigneeCellNo);
//		}
//		if(consigneeEmail!=null){
//			model.setConsigneeEmail(consigneeEmail);
//		}
//		
//		fumigationObj=model;
//		
//		presenter.setModel(model);
//		
//	}
//
//	
//	@Override
//	public void updateView(Fumigation model) {
//		
//		fumigationObj=model;
//		
//		this.tbcount.setValue(model.getCount()+"");
//		
//		
//		
//		if(model.getShippingBillNo()!=null)
//			tbShippingBillNo.setValue(model.getShippingBillNo());
//		
//		if(model.getCommoditybool()!=null)
//			cbcommodity.setValue(model.getCommoditybool());
//		
//		
//		if(model.getPacking()!=null)
//			cbpacking.setValue(model.getPacking());
//		
//		if(model.getBothCommodityandPacking()!=null)
//			cbbothComAndPacking.setValue(model.getBothCommodityandPacking());
//		
//		
//		
//		if(model.getStackUnderSheet()!=null)
//			stackUnderSheet.setValue(model.getStackUnderSheet());
//		
//		if(model.getMoredecl()!=null)
//			tadMoreecl.setValue(model.getMoredecl());
//		
//		if(model.getContainerUnderSheet()!=null)
//			ContainerUnderSheet.setValue(model.getContainerUnderSheet());
//		
//		
//		if(model.getPermanentChamber()!=null)
//			permantChamber.setValue(model.getPermanentChamber());
//		
//		if(model.getPressureTestedContainer()!=null)
//			PressureTestedContaner.setValue(model.getPressureTestedContainer());
//		
//		
//		
//		if(model.getContainerNo()!=null)
//			containerNamber.setValue(model.getContainerNo());
//		if(model.getCommodity()!=null)
//			tbCommodity.setValue(model.getCommodity());
//			
//		if(model.getConsignmentlink()!=null)
//			tbConsignmentLink.setValue(model.getConsignmentlink());
//			
//
//		if(model.getPortncountryloading()!=null)
//			portOfLoading.setValue(model.getPortncountryloading());
//		
//		if(model.getVentilation()!=null)
//			tbvetilation.setValue(model.getVentilation());
//		
//		if(model.getRegistrationno()!=null)
//			tbregistrationNo.setValue(model.getRegistrationno());
//			
//		if(model.getCountryofdestination()!=null)
//			olbcountrydestinatoin.setValue(model.getCountryofdestination());
//		
//		if(model.getImportcountry()!=null)
//		olbcountryOfOrigin.setValue(model.getImportcountry());
//		
//		
//		if(model.getFromCompanyname()!=null)
//			nameExporter.setValue(model.getFromCompanyname());
//		
//		if(model.getToCompanyname()!=null)
//			nameImporter.setValue(model.getToCompanyname());
//			
//		if(model.getAeiRegNO()!=null)
//			tbAEI_RegNo.setValue(model.getAeiRegNO());
//		
//		if(model.getAfasRegNo()!=null)
//			tbAFAS_RegNo.setValue(model.getAfasRegNo());
//			
//		
//		Console.log("in side update view date of issue is "+model.getDateofissue());
//		if(model.getDateofissue()!=null)
//			dbdateofissue.setValue(model.getDateofissue());
//		
//		if(model.getCertificateNo()!=null)
//			tbCertificateNo.setValue(model.getCertificateNo());
//		
//		if(model.getNameoffumigation()!=null)
//		{
//			for(int i=0;i<lbnamefumigation.getItemCount();i++)
//			{
//				String data=lbnamefumigation.getItemText(i);
//				if(data.equals(model.getNameoffumigation()))
//				{
//					lbnamefumigation.setSelectedIndex(i);
//					break;
//				}
//			}
//		}
//		
//		
//		if(model.getDateoffumigation()!=null)
//			dbdatefumigation.setValue(model.getDateoffumigation());
//		
//		if(model.getPlaceoffumigation()!=null)
//			tbplaceoffumigation.setValue(model.getPlaceoffumigation());
//		
//		if(model.getDoseratefumigation()!=null)
//			olbAQISDoseRate.setValue(model.getDoseratefumigation());
//		
//		if(model.getDurartionfumigation()!=null)
//			olbExposurePeriod.setValue(model.getDurartionfumigation());
//		
//		if(model.getMinairtemp()!=null)
//			olbForecastminairtemp.setValue(model.getMinairtemp());
//		
//		if(model.getAppliedDoseRate()!=null)
//			olbAppliedDoseRate.setValue(model.getAppliedDoseRate());
//		
//		if(model.getDeclaration()!=null)
//			tadeclaration.setValue(model.getDeclaration());
//		
//		if(model.getAdditionaldeclaration()!=null)
//			taadditionalDeclaration.setValue(model.getAdditionaldeclaration());
//		
//		if(model.getPdate()!=null)
//			dbdateadd.setValue(model.getPdate());
//		
//		if(model.getPlace()!=null)
//			tbplace.setValue(model.getPlace());
//		
//		if(model.getContractID()!=null)
//			tbcontractId.setValue(model.getContractID()+"");
//		
//		if(model.getServiceID()!=null)
//			tbserviceId.setValue(model.getServiceID()+"");
//		
//		if(model.getContractEnddate()!=null)
//			dbcontractenddate.setValue(model.getContractEnddate());
//		
//		if(model.getContractStartdate()!=null)
//			dbcontractstartdate.setValue(model.getContractStartdate());
//		
//		if(model.getServiceDate()!=null)
//			dbservicedate.setValue(model.getServiceDate());
//		
//		if(model.getFromaddress()!=null)
//			addressOfExporter.setValue(model.getFromaddress());
//		
//		if(model.getTomaddress()!=null)
//			addressOfImporter.setValue(model.getTomaddress());
//		
//		if(model.getNote1()!=null)
//			lbnote2.setTitle(model.getNote1());
//		presenter.setModel(model);
//	}
//	
//	@Override
//	public void setEnable(boolean state) {
//	
//		super.setEnable(state);
//		this.tbcount.setEnabled(false);
//		this.tbcontractId.setEnabled(false);
//		this.tbserviceId.setEnabled(false);
//		this.dbcontractenddate.setEnabled(false);
//		this.dbcontractstartdate.setEnabled(false);
//		this.dbservicedate.setEnabled(false);
//		nameExporter.setEnabled(false);
//		nameImporter.setEnabled(false);
//	}
//
//	@Override
//	public void setToViewState() {
//		super.setToViewState();
//		
//		SuperModel model=new Fumigation();
//		model=fumigationObj;
//		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.FUMIGATIONAUS, fumigationObj.getCount(), null,null,null, false, model, null);
//	}
//	//******************getters and setters **************************
//	
//	
//	public RadioButton getCbcommodity() {
//		return cbcommodity;
//	}
//
//	public void setCbcommodity(RadioButton cbcommodity) {
//		this.cbcommodity = cbcommodity;
//	}
//
//	public RadioButton getCbpacking() {
//		return cbpacking;
//	}
//
//	public void setCbpacking(RadioButton cbpacking) {
//		this.cbpacking = cbpacking;
//	}
//
//	public RadioButton getCbbothComAndPacking() {
//		return cbbothComAndPacking;
//	}
//
//	public void setCbbothComAndPacking(RadioButton cbbothComAndPacking) {
//		this.cbbothComAndPacking = cbbothComAndPacking;
//	}
//
//	public RadioButton getStackUnderSheet() {
//		return stackUnderSheet;
//	}
//
//	public void setStackUnderSheet(RadioButton stackUnderSheet) {
//		this.stackUnderSheet = stackUnderSheet;
//	}
//
//	public RadioButton getContainerUnderSheet() {
//		return ContainerUnderSheet;
//	}
//
//	public void setContainerUnderSheet(RadioButton containerUnderSheet) {
//		ContainerUnderSheet = containerUnderSheet;
//	}
//
//	public RadioButton getPermantChamber() {
//		return permantChamber;
//	}
//
//	public void setPermantChamber(RadioButton permantChamber) {
//		this.permantChamber = permantChamber;
//	}
//
//	public RadioButton getPressureTestedContaner() {
//		return PressureTestedContaner;
//	}
//
//	public void setPressureTestedContaner(RadioButton pressureTestedContaner) {
//		PressureTestedContaner = pressureTestedContaner;
//	}
//
//	public TextBox getNameExporter() {
//		return nameExporter;
//	}
//
//	public void setNameExporter(TextBox nameExporter) {
//		this.nameExporter = nameExporter;
//	}
//
//	public TextBox getNameImporter() {
//		return nameImporter;
//	}
//
//	public void setNameImporter(TextBox nameImporter) {
//		this.nameImporter = nameImporter;
//	}
//
//	public TextBox getContainerNamber() {
//		return containerNamber;
//	}
//
//	public void setContainerNamber(TextBox containerNamber) {
//		this.containerNamber = containerNamber;
//	}
//
//	public TextBox getTbplace() {
//		return tbplace;
//	}
//
//	public void setTbplace(TextBox tbplace) {
//		this.tbplace = tbplace;
//	}
//
//	public DateBox getDbdateadd() {
//		return dbdateadd;
//	}
//
//	public void setDbdateadd(DateBox dbdateadd) {
//		this.dbdateadd = dbdateadd;
//	}
//
//	public TextArea getTadeclaration() {
//		return tadeclaration;
//	}
//
//	public void setTadeclaration(TextArea tadeclaration) {
//		this.tadeclaration = tadeclaration;
//	}
//
//	public TextArea getTaadditionalDeclaration() {
//		return taadditionalDeclaration;
//	}
//
//	public void setTaadditionalDeclaration(TextArea taadditionalDeclaration) {
//		this.taadditionalDeclaration = taadditionalDeclaration;
//	}
//
//	public TextBox getTbCertificateNo() {
//		return tbCertificateNo;
//	}
//
//	public void setTbCertificateNo(TextBox tbCertificateNo) {
//		this.tbCertificateNo = tbCertificateNo;
//	}
//
//	public TextBox getTbcount() {
//		return tbcount;
//	}
//
//	public void setTbcount(TextBox tbcount) {
//		this.tbcount = tbcount;
//	}
//
//	public TextBox getTbCommodity() {
//		return tbCommodity;
//	}
//
//	public void setTbCommodity(TextBox tbCommodity) {
//		this.tbCommodity = tbCommodity;
//	}
//
//	public TextBox getTbConsignmentLink() {
//		return tbConsignmentLink;
//	}
//
//	public void setTbConsignmentLink(TextBox tbConsignmentLink) {
//		this.tbConsignmentLink = tbConsignmentLink;
//	}
//
//	public TextBox getPortOfLoading() {
//		return portOfLoading;
//	}
//
//	public void setPortOfLoading(TextBox portOfLoading) {
//		this.portOfLoading = portOfLoading;
//	}
//
//	public TextBox getTbregistrationNo() {
//		return tbregistrationNo;
//	}
//
//	public void setTbregistrationNo(TextBox tbregistrationNo) {
//		this.tbregistrationNo = tbregistrationNo;
//	}
//
//	public TextBox getTbAFAS_RegNo() {
//		return tbAFAS_RegNo;
//	}
//
//	public void setTbAFAS_RegNo(TextBox tbAFAS_RegNo) {
//		this.tbAFAS_RegNo = tbAFAS_RegNo;
//	}
//
//	public TextBox getTbAEI_RegNo() {
//		return tbAEI_RegNo;
//	}
//
//	public void setTbAEI_RegNo(TextBox tbAEI_RegNo) {
//		this.tbAEI_RegNo = tbAEI_RegNo;
//	}
//
//	public DateBox getDbdateofissue() {
//		return dbdateofissue;
//	}
//
//	public void setDbdateofissue(DateBox dbdateofissue) {
//		this.dbdateofissue = dbdateofissue;
//	}
//
//	public ObjectListBox<Country> getOlbcountrydestinatoin() {
//		return olbcountrydestinatoin;
//	}
//
//	public void setOlbcountrydestinatoin(
//			ObjectListBox<Country> olbcountrydestinatoin) {
//		this.olbcountrydestinatoin = olbcountrydestinatoin;
//	}
//
//	public ListBox getLbnamefumigation() {
//		return lbnamefumigation;
//	}
//
//	public void setLbnamefumigation(ListBox lbnamefumigation) {
//		this.lbnamefumigation = lbnamefumigation;
//	}
//
//	public DateBox getDbdatefumigation() {
//		return dbdatefumigation;
//	}
//
//	public void setDbdatefumigation(DateBox dbdatefumigation) {
//		this.dbdatefumigation = dbdatefumigation;
//	}
//
//	public TextBox getTbplaceoffumigation() {
//		return tbplaceoffumigation;
//	}
//
//	public void setTbplaceoffumigation(TextBox tbplaceoffumigation) {
//		this.tbplaceoffumigation = tbplaceoffumigation;
//	}
//
//	public ObjectListBox<Config> getOlbAQISDoseRate() {
//		return olbAQISDoseRate;
//	}
//
//	public void setOlbAQISDoseRate(ObjectListBox<Config> olbAQISDoseRate) {
//		this.olbAQISDoseRate = olbAQISDoseRate;
//	}
//
//	public ObjectListBox<Config> getOlbAppliedDoseRate() {
//		return olbAppliedDoseRate;
//	}
//
//	public void setOlbAppliedDoseRate(ObjectListBox<Config> olbAppliedDoseRate) {
//		this.olbAppliedDoseRate = olbAppliedDoseRate;
//	}
//
//	public ObjectListBox<Config> getOlbExposurePeriod() {
//		return olbExposurePeriod;
//	}
//
//	public void setOlbExposurePeriod(ObjectListBox<Config> olbExposurePeriod) {
//		this.olbExposurePeriod = olbExposurePeriod;
//	}
//
//	public ObjectListBox<Config> getOlbForecastminairtemp() {
//		return olbForecastminairtemp;
//	}
//
//	public void setOlbForecastminairtemp(ObjectListBox<Config> olbForecastminairtemp) {
//		this.olbForecastminairtemp = olbForecastminairtemp;
//	}
//
//	public AddressComposite getAddressOfExporter() {
//		return addressOfExporter;
//	}
//
//	public void setAddressOfExporter(AddressComposite addressOfExporter) {
//		this.addressOfExporter = addressOfExporter;
//	}
//
//	public AddressComposite getAddressOfImporter() {
//		return addressOfImporter;
//	}
//
//	public void setAddressOfImporter(AddressComposite addressOfImporter) {
//		this.addressOfImporter = addressOfImporter;
//	}
//
//	public TextBox getTbcontractId() {
//		return tbcontractId;
//	}
//
//	public void setTbcontractId(TextBox tbcontractId) {
//		this.tbcontractId = tbcontractId;
//	}
//
//	public TextBox getTbserviceId() {
//		return tbserviceId;
//	}
//
//	public void setTbserviceId(TextBox tbserviceId) {
//		this.tbserviceId = tbserviceId;
//	}
//
//	public DateBox getDbcontractstartdate() {
//		return dbcontractstartdate;
//	}
//
//	public void setDbcontractstartdate(DateBox dbcontractstartdate) {
//		this.dbcontractstartdate = dbcontractstartdate;
//	}
//
//	public DateBox getDbcontractenddate() {
//		return dbcontractenddate;
//	}
//
//	public void setDbcontractenddate(DateBox dbcontractenddate) {
//		this.dbcontractenddate = dbcontractenddate;
//	}
//
//	public DateBox getDbservicedate() {
//		return dbservicedate;
//	}
//
//	public void setDbservicedate(DateBox dbservicedate) {
//		this.dbservicedate = dbservicedate;
//	}
//
//	public Label getTargetOfFumigation() {
//		return targetOfFumigation;
//	}
//
//	public void setTargetOfFumigation(Label targetOfFumigation) {
//		this.targetOfFumigation = targetOfFumigation;
//	}
//
//	public Label getNote1() {
//		return note1;
//	}
//
//	public void setNote1(Label note1) {
//		this.note1 = note1;
//	}
//
//	public Label getNote2() {
//		return note2;
//	}
//
//	public void setNote2(Label note2) {
//		this.note2 = note2;
//	}
//
//	public Label getContainerNum() {
//		return containerNum;
//	}
//
//	public void setContainerNum(Label containerNum) {
//		this.containerNum = containerNum;
//	}
//
//	public ListBox getLbnote2() {
//		return lbnote2;
//	}
//
//	public void setLbnote2(ListBox lbnote2) {
//		this.lbnote2 = lbnote2;
//	}
//
//	public ObjectListBox<Declaration> getDeclarationMessageList() {
//		return declarationMessageList;
//	}
//
//	public void setDeclarationMessageList(
//			ObjectListBox<Declaration> declarationMessageList) {
//		this.declarationMessageList = declarationMessageList;
//	}
//
//	public Button getAddDeclarationButton() {
//		return addDeclarationButton;
//	}
//
//	public Button getAddDeclarationButton1() {
//		return addDeclarationButton1;
//	}
//
//	public void setAddDeclarationButton1(Button addDeclarationButton1) {
//		this.addDeclarationButton1 = addDeclarationButton1;
//	}
//
//	public void setAddDeclarationButton(Button addDeclarationButton) {
//		this.addDeclarationButton = addDeclarationButton;
//	}
//
//	public TextArea getTadMoreecl() {
//		return tadMoreecl;
//	}
//
//	public void setTadMoreecl(TextArea tadMoreecl) {
//		this.tadMoreecl = tadMoreecl;
//	}
//
//	public TextBox getTbShippingBillNo() {
//		return tbShippingBillNo;
//	}
//
//	public void setTbShippingBillNo(TextBox tbShippingBillNo) {
//		this.tbShippingBillNo = tbShippingBillNo;
//	}
//
//	public ObjectListBox<Declaration> getDeclarationMessageList1() {
//		return declarationMessageList1;
//	}
//
//	public void setDeclarationMessageList1(
//			ObjectListBox<Declaration> declarationMessageList1) {
//		this.declarationMessageList1 = declarationMessageList1;
//	}
//
//	public ObjectListBox<Declaration> getDeclarationMessageList2() {
//		return declarationMessageList2;
//	}
//
//	public void setDeclarationMessageList2(
//			ObjectListBox<Declaration> declarationMessageList2) {
//		this.declarationMessageList2 = declarationMessageList2;
//	}
//
//	public Button getAddDeclarationButton2() {
//		return addDeclarationButton2;
//	}
//
//	public void setAddDeclarationButton2(Button addDeclarationButton2) {
//		this.addDeclarationButton2 = addDeclarationButton2;
//	}
//
//	public ObjectListBox<Country> getOlbcountryOfOrigin() {
//		return olbcountryOfOrigin;
//	}
//
//	public void setOlbcountryOfOrigin(ObjectListBox<Country> olbcountryOfOrigin) {
//		this.olbcountryOfOrigin = olbcountryOfOrigin;
//	}
//
//	public TextBox getTbvetilation() {
//		return tbvetilation;
//	}
//
//	public void setTbvetilation(TextBox tbvetilation) {
//		this.tbvetilation = tbvetilation;
//	}
//
//	public Label getVentilationfirst() {
//		return ventilationfirst;
//	}
//
//	public void setVentilationfirst(Label ventilationfirst) {
//		this.ventilationfirst = ventilationfirst;
//	}
//
//	public Label getVentilatioSecond() {
//		return ventilatioSecond;
//	}
//
//	public void setVentilatioSecond(Label ventilatioSecond) {
//		this.ventilatioSecond = ventilatioSecond;
//	}
//
//	public Button getAddbyCONSIGNERComposite() {
//		return addbyCONSIGNERComposite;
//	}
//
//	public void setAddbyCONSIGNERComposite(Button addbyCONSIGNERComposite) {
//		this.addbyCONSIGNERComposite = addbyCONSIGNERComposite;
//	}
//
//	public PersonInfoComposite getPersonInfoCONSIGNERComposite() {
//		return personInfoCONSIGNERComposite;
//	}
//
//	public void setPersonInfoCONSIGNERComposite(
//			PersonInfoComposite personInfoCONSIGNERComposite) {
//		this.personInfoCONSIGNERComposite = personInfoCONSIGNERComposite;
//	}
//
//	public Button getAddbyCONSIGNEEComposite() {
//		return addbyCONSIGNEEComposite;
//	}
//
//	public void setAddbyCONSIGNEEComposite(Button addbyCONSIGNEEComposite) {
//		this.addbyCONSIGNEEComposite = addbyCONSIGNEEComposite;
//	}
//
//	public PersonInfoComposite getPersonInfoCONSIGNEEComposite() {
//		return personInfoCONSIGNEEComposite;
//	}
//
//	public void setPersonInfoCONSIGNEEComposite(
//			PersonInfoComposite personInfoCONSIGNEEComposite) {
//		this.personInfoCONSIGNEEComposite = personInfoCONSIGNEEComposite;
//	}
//
//	public Button getNewFromCustomer() {
//		return newFromCustomer;
//	}
//
//	public void setNewFromCustomer(Button newFromCustomer) {
//		this.newFromCustomer = newFromCustomer;
//	}
//
//	public Button getNewToCustomer() {
//		return newToCustomer;
//	}
//
//	public void setNewToCustomer(Button newToCustomer) {
//		this.newToCustomer = newToCustomer;
//	}
//
//	public String getConsignerName() {
//		return consignerName;
//	}
//
//	public void setConsignerName(String consignerName) {
//		this.consignerName = consignerName;
//	}
//
//	public String getConsignerPocName() {
//		return consignerPocName;
//	}
//
//	public void setConsignerPocName(String consignerPocName) {
//		this.consignerPocName = consignerPocName;
//	}
//
//	public String getConsignerEmail() {
//		return consignerEmail;
//	}
//
//	public void setConsignerEmail(String consignerEmail) {
//		this.consignerEmail = consignerEmail;
//	}
//
//	public Long getConsignerCellNo() {
//		return consignerCellNo;
//	}
//
//	public void setConsignerCellNo(Long consignerCellNo) {
//		this.consignerCellNo = consignerCellNo;
//	}
//
//	public String getConsigneeName() {
//		return consigneeName;
//	}
//
//	public void setConsigneeName(String consigneeName) {
//		this.consigneeName = consigneeName;
//	}
//
//	public String getConsigneePocName() {
//		return consigneePocName;
//	}
//
//	public void setConsigneePocName(String consigneePocName) {
//		this.consigneePocName = consigneePocName;
//	}
//
//	public String getConsigneeEmail() {
//		return consigneeEmail;
//	}
//
//	public void setConsigneeEmail(String consigneeEmail) {
//		this.consigneeEmail = consigneeEmail;
//	}
//
//	public Long getConsigneeCellNo() {
//		return consigneeCellNo;
//	}
//
//	public void setConsigneeCellNo(Long consigneeCellNo) {
//		this.consigneeCellNo = consigneeCellNo;
//	}
//
//	
//	
//
//	
//	
//	
//	
//	
//
//
//}
