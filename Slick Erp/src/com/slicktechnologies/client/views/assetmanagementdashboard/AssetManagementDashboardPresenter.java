package com.slicktechnologies.client.views.assetmanagementdashboard;

import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.project.tool.ToolForm;
import com.slicktechnologies.client.views.project.tool.ToolPresenter;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

public class AssetManagementDashboardPresenter implements ClickHandler {

	
	AssetManagementDashboardForm form;
//	AssetManagementDashboardSearchProxy searchpopup;
	protected GenricServiceAsync service=GWT.create(GenricService.class);

	public  AssetManagementDashboardPresenter(AssetManagementDashboardForm form,AssetManagementDashboardSearchProxy searchpopup)
	{
		this.form=form;
//		this.searchpopup=searchpopup;
		SetEventHandling();
//		searchpopup.applyHandler(this);
		form.getBtnGo().addClickHandler(this);
		
		Date date=new Date();
		System.out.println("Today Date ::::::::: "+date);
		// **** Here We Add 7 Days to current date and pass this date to default search.
		CalendarUtil.addDaysToDate(date,30);
	    System.out.println("Changed Date + 30 ::::::::: "+date);
		
	    
	    Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		
	    filtervec=new Vector<Filter>();
		
		filter = new Filter();
		filter.setQuerryString("collabrationDate <=");
		filter.setDateValue(date);
		filtervec.add(filter);
		
		
		querry=new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CompanyAsset());
		this.retriveTable(querry,form.getCompanyAssetTbl());
		
		setTableSelectionOnCompanyAsset();
		}

	public static void initalize()
	{
		AssetManagementDashboardForm accHomeForm = new AssetManagementDashboardForm();
		AppMemory.getAppMemory().currentScreen=Screen.ASSETMANAGEMENTDASHBOARD;
		AppMemory.getAppMemory().skeleton.getProcessName().setText("Dashboard/Asset Management");
		AssetManagementDashboardSearchProxy searchpopup=new AssetManagementDashboardSearchProxy();
		AssetManagementDashboardPresenter presenter= new AssetManagementDashboardPresenter(accHomeForm,searchpopup);
		AppMemory.getAppMemory().stickPnel(accHomeForm);	
	}

	@Override
	public void onClick(ClickEvent event) {
		
//		if(event.getSource() instanceof InlineLabel)
//		{
//		InlineLabel lbl= (InlineLabel) event.getSource();
//		String text= lbl.getText();
//		if(text.equals("Search"))
//			searchpopup.showPopUp();
//		
//		if(text.equals("Go"))
//			reactOnGo();
//		
//		}
		
		if(event.getSource().equals(form.getBtnGo())){if(form.getDbFromDate().getValue()==null||form.getDbToDate().getValue()==null){
			final GWTCAlert alert = new GWTCAlert();
			alert.alert("Select From and To Date.");
		}
		if(form.getDbFromDate().getValue()!=null&&form.getDbToDate()!=null){
			Date formDate=form.getDbFromDate().getValue();
			Date toDate=form.getDbToDate().getValue();
			if(toDate.equals(formDate)||toDate.after(formDate)){
				System.out.println("From Date ::::::::: "+formDate);
				CalendarUtil.addDaysToDate(formDate,7);
				System.out.println("Changed Date ::::::::: "+formDate);
				System.out.println("To Date ::::::::: "+toDate);
			
				if(toDate.before(formDate)){
					System.out.println("Inside Date Condition"); 
					searchByFromToDate();
				}
				else{
					final GWTCAlert alert = new GWTCAlert();
					alert.alert("From Date and To Date Difference Can Not Be More Than 7 Days.");
				}
			}
			else{
				final GWTCAlert alert = new GWTCAlert();
				alert.alert("To Date should be greater than From Date.");
			}
		}}
	}

	public void SetEventHandling()
	{
		 InlineLabel label[] =AppMemory.getAppMemory().skeleton.getMenuLabels();
		  for(int k=0;k<label.length;k++)
		  {
			 if( AppMemory.getAppMemory().skeleton.registration[k]!=null)
			    AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
		  }
		 
		  for( int k=0;k<label.length;k++)
		  {
			  AppMemory.getAppMemory().skeleton.registration[k]= label[k].addClickHandler(this);
		  }
	}

	
	/****
	 * Search pop up Query
	 * 
	 * 
	 */
	
//	public void reactOnGo()
//	{ 
//		if(!searchpopup.personInfo.getId().getValue().equals("")&&!searchpopup.personInfo.getName().getValue().equals("")&&!searchpopup.personInfo.getPhone().getValue().equals(""))
//		{
//			MyQuerry quer=createCompanyAssetFilter(new CompanyAsset());
//			this.form.companyAssetTbl.connectToLocal();
//			retriveTable(quer, form.getCompanyAssetTbl());
//			searchpopup.hidePopUp();
//		}
//		if(searchpopup.personInfo.getId().getValue().equals("")&&searchpopup.personInfo.getName().getValue().equals("")&&searchpopup.personInfo.getPhone().getValue().equals(""))
//		{
//			GWTCAlert alertMsg=new GWTCAlert();
//			alertMsg.alert("Please enter customer information");
//		}
//	}
//
//	private MyQuerry createCompanyAssetFilter(CompanyAsset biling)
//	{
//		Vector<Filter>filterVec = new Vector<Filter>();
//		 Filter temp;
//		 
//		 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
//			{
//				temp=new Filter();
//				temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
//				temp.setQuerryString("personInfo.count");
//				filterVec.add(temp);	
//			}
//			
//			if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
//			{
//				temp=new Filter();
//				temp.setStringValue(searchpopup.personInfo.getName().getValue());
//				temp.setQuerryString("personInfo.fullName");
//				filterVec.add(temp);	
//			}
//			
//			if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
//			{
//				temp=new Filter();
//				temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
//				temp.setQuerryString("personInfo.cellNumber");
//				filterVec.add(temp);	
//			}
//			
//			MyQuerry billing=new MyQuerry();
//			billing.setQuerryObject(new CompanyAsset());
//			billing.setFilters(filterVec);
//			
//			return billing;
//	}

	

	public <T> void retriveTable(MyQuerry querry,final SuperTable<T>table)
	 {
				table.connectToLocal();
				service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						for(SuperModel model:result)
						{
							table.getListDataProvider().getList().add((T) model);
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
					caught.printStackTrace();
						
					}
				}); 
	 }


	 public void setTableSelectionOnCompanyAsset()
	 {
		 final NoSelectionModel<CompanyAsset> selectionModelMyObj = new NoSelectionModel<CompanyAsset>();
		 
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Asset Management/Company Asset",Screen.COMPANYASSET);
	        	 final CompanyAsset entity=selectionModelMyObj.getLastSelectedObject();
	        	 final ToolForm form = ToolPresenter.initalize();
	        	  
	        	 form.showWaitSymbol();
	        	 Timer timer=new Timer() {
	 				@Override
	 				public void run() {
	 					form.hideWaitSymbol();
					    form.updateView(entity);
					    form.setToViewState();
	 				}
	 			};
	            timer.schedule(5000);  
	          }
	     };
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.companyAssetTbl.getTable().setSelectionModel(selectionModelMyObj);
	 }
	 
	 
	 private void searchByBranch(){
		 
		 System.out.println("in side search by branch ......");
		 
		 
		 
	 }
	 
	 
	 
	 
	 private void searchByFromToDate(){
			
			
			System.out.println("Hiiiiiiiiiiii........");
			
			Vector<Filter> filtervec=null;
			Filter filter = null;
			MyQuerry querry;
		    filtervec=new Vector<Filter>();
		    
			
			filter = new Filter();
			filter.setQuerryString("collabrationDate >=");
			filter.setDateValue(form.dbFromDate.getValue());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("collabrationDate <=");
			filter.setDateValue(form.getDbToDate().getValue());
			filtervec.add(filter);
			
			
			querry=new MyQuerry();
			querry.setFilters(filtervec);
			querry.setQuerryObject(new CompanyAsset());
			form.getCompanyAssetTbl().connectToLocal();
			this.retriveTable(querry,form.getCompanyAssetTbl());
			
		}
	 
}
