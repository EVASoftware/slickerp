package com.slicktechnologies.client.views.assetmanagementdashboard;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.MyInlineLabel;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.project.tool.ToolPresenter;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.Person;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

public class AssetManagementDashboardForm extends ViewContainer implements ClickHandler  {

	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	/** String array repersenting the table header menus*/
	String[] tableheaderbarnames={"New","Download"};
	
	/** Flow panel for holding the Table header menus */
	FlowPanel tableheader;
	
	/** Company Asset Table which can come on the form, this component can be made generic */
	public CompanyAssetTableProxy companyAssetTbl;
	
//	/** Client Asset Table which can come on form component can be made generic */
//	public ClientAssetTableProxy clientAssetTbl;
//	
//	/** ToolSet table which can come on form component can be made generic */
//	public ToolSetTableProxy toolSetTbl;
	
		
	
	DateBox dbFromDate;
	DateBox dbToDate;
	Button btnGo;
	
	
	public AssetManagementDashboardForm()
	{
		//Toggle the App header bar menu , In this case only search should come
		toggleAppHeaderBarMenu();
		//Create the Gui, actual Gui gets created here.
		createGui();
		//Sets the event handling
	}	
	
	private void initializeWidget(){
		
		
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		dbFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		dbToDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		btnGo=new Button("GO");
		
	}
			
	/**
	 * Toggles the app header bar menu.
	 */
	private void toggleAppHeaderBarMenu() {
		InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
		for(int k=0;k<menus.length;k++)
		{
//			String text=menus[k].getText();
//			if(text.contains("Search"))
//			{
//				menus[k].setVisible(true); 
//			}
//			else
				menus[k].setVisible(false);  		   
			
		}
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.ViewContainer#createGui()
	 */
	@Override
	public void createGui() 
	{
		initializeWidget();
		FlowPanel panel= new FlowPanel();
		
		panel.getElement().getStyle().setMarginTop(10, Unit.PX);
		panel.getElement().getStyle().setMarginLeft(290, Unit.PX);
		panel.getElement().getStyle().setMarginRight(290, Unit.PX);
		
		FlowPanel innerpanel= new FlowPanel();
		
		InlineLabel blank = new InlineLabel("    ");
		
		
		
		InlineLabel fromDate = new InlineLabel("From Date ");
		fromDate.setSize("90px", "20px");
		innerpanel.add(fromDate);
		innerpanel.add(dbFromDate);
		
		InlineLabel toDate = new InlineLabel("  To Date ");
		innerpanel.add(toDate);
		innerpanel.add(dbToDate);
		innerpanel.add(blank);
		innerpanel.add(blank);
		innerpanel.add(blank);
		innerpanel.add(blank);
		innerpanel.add(btnGo);
		
		panel.add(innerpanel);
		content.add(panel);
		
		
		//Create the Four Scroll Panel To hold four types of Screen
		ScrollPanel[] scrollPanes=new ScrollPanel[4];
		//String array to hold captions
		
		String[]captions={"Company Asset"};
//		String[]captions={"Company Asset","Client Asset","Tool Set"};
		
		//Screen array to hold new Screen object for the purpose of Redirection
		
		Screen[]screens={Screen.COMPANYASSET};
//		Screen[]screens={Screen.COMPANYASSET,Screen.CLIENTSIDEASSET,Screen.TOOLGROUP};	
		
		//Create  Flow Panel,Inside Flow Panel 
		for(int i=0;i<captions.length;i++)
		{
			FlowPanel holder= new FlowPanel();
			tableheader= new FlowPanel();
			tableheader.getElement().setId("tableheader");
			// Provides Table Header which is responsible for new or Download Buttons
			setTableHeaderBar(screens[i]);
			
			tableheader.getElement().getStyle().setMarginTop(10, Unit.PX);
			tableheader.getElement().getStyle().setMarginBottom(10, Unit.PX);
			
			scrollPanes[i]= new ScrollPanel();
			scrollPanes[i].setHeight("150px");
		
			holder.add(tableheader);
			holder.add(scrollPanes[i]);
			//Caption Panel inside which a Flow Panel is added
			CaptionPanel cap= new CaptionPanel(captions[i]);
			cap.add(holder);
			cap.getElement().setClassName("tablecaption");
			//Add caption Panel on Content
		    content.add(cap);
		}
		//Add Billing Table inside the Scroll Panel
		companyAssetTbl = new CompanyAssetTableProxy();
		scrollPanes[0].add(companyAssetTbl.content);

//		//Add Invoice Table inside the Scroll Panel
//		clientAssetTbl = new ClientAssetTableProxy();
//		scrollPanes[1].add(clientAssetTbl.content);
//		
//		//Add Payment Panel inside the table
//		toolSetTbl = new ToolSetTableProxy();
//		scrollPanes[2].add(toolSetTbl.content);
		
		content.getElement().setId("homeview");
	  }
	
	
	public void setTableHeaderBar(Screen screen)
	{
//		if(screen == Screen.INVOICEDETAILS)
//		{
//			MyInlineLabel lbl;
//			lbl=new MyInlineLabel("Download",screen);
//			getTableheader().add(lbl);
//			lbl.getElement().setId("tableheadermenu");
//			lbl.addClickHandler(this);
//			return;
//		}
//		if(screen == Screen.PAYMENTDETAILS)
//		{
//			MyInlineLabel lbl;
//			lbl=new MyInlineLabel("Download",screen);
//			getTableheader().add(lbl);
//			lbl.getElement().setId("tableheadermenu");
//			lbl.addClickHandler(this);
//			return;
//		}
		
		for(int j=0;j<getTableheaderbarnames().length;j++)
		{
			MyInlineLabel lbl;
			lbl=new MyInlineLabel(getTableheaderbarnames()[j],screen);
            getTableheader().add(lbl);
			lbl.getElement().setId("tableheadermenu");
			lbl.addClickHandler(this);
		}
	}

		public String[] getTableheaderbarnames() {
			return tableheaderbarnames;
		}

		public void setTableheaderbarnames(String[] tableheaderbarnames) {
			this.tableheaderbarnames = tableheaderbarnames;
		}

		/**
		 * Gets the tableheader.
		 *
		 * @return the tableheader
		 */
		public FlowPanel getTableheader() {
			return tableheader;
		}

		/**
		 * Sets the tableheader.
		 *
		 * @param tableheader the new tableheader
		 */
		public void setTableheader(FlowPanel tableheader) {
			this.tableheader = tableheader;
		}

		public CompanyAssetTableProxy getCompanyAssetTbl() {
			return companyAssetTbl;
		}

		public void setCompanyAssetTbl(CompanyAssetTableProxy companyAssetTbl) {
			this.companyAssetTbl = companyAssetTbl;
		}
		
		

		/* (non-Javadoc)
		 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
		 */
		@Override
		public void onClick(ClickEvent event) {
			if(event.getSource() instanceof MyInlineLabel)
			{
				MyInlineLabel lbl=(MyInlineLabel) event.getSource();
				redirectOnScreens(lbl);
				
			}
			
		}
		
		private void redirectOnScreens(MyInlineLabel lbl)
		{
			Screen screen=(Screen) lbl.redirectScreen;
			String title=lbl.getText().trim();
			switch(screen)
			{
			  case COMPANYASSET:
				  
					if(title.equals("Download"))
					{
						reactOnCompanyAssetDownLoad();
					}
					
					if(title.equals("New"))
					{
						ToolPresenter.initalize();
					}
					break;
					
			  default:
				  break;
					
			}
		}

		private void reactOnCompanyAssetDownLoad()
		{
			ArrayList<CompanyAsset> billingarray=new ArrayList<CompanyAsset>();
			List<CompanyAsset> list=(List<CompanyAsset>) companyAssetTbl.getListDataProvider().getList();
			billingarray.addAll(list);
			
			csvservice.setToollist(billingarray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				@Override
				public void onSuccess(Void result) {
					
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+23;
					Window.open(url, "test", "enabled");
				}
			});
		}
		
//		private void reactOnInvoiceDownLoad()
//		{
//			ArrayList<Invoice> invoicearray=new ArrayList<Invoice>();
//			List<Invoice> list=(List<Invoice>) clientAssetTbl.getListDataProvider().getList();
//			invoicearray.addAll(list);
//			
//			csvservice.setinvoicelist(invoicearray, new AsyncCallback<Void>() {
//
//				@Override
//				public void onFailure(Throwable caught) {
//					System.out.println("RPC call Failed"+caught);
//				}
//				@Override
//				public void onSuccess(Void result) {
//					
//					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//					final String url=gwt + "csvservlet"+"?type="+15;
//					Window.open(url, "test", "enabled");
//				}
//			});
//		}
//		
//		private void reactOnPaymentDownLoad()
//		{
//			ArrayList<CustomerPayment> paymentarray=new ArrayList<CustomerPayment>();
//			List<CustomerPayment> list=(List<CustomerPayment>) toolSetTbl.getListDataProvider().getList();
//			paymentarray.addAll(list);
//			
//			csvservice.setpaymentlist(paymentarray, new AsyncCallback<Void>() {
//
//				@Override
//				public void onFailure(Throwable caught) {
//					System.out.println("RPC call Failed"+caught);
//				}
//				@Override
//				public void onSuccess(Void result) {
//					
//					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//					final String url=gwt + "csvservlet"+"?type="+16;
//					Window.open(url, "test", "enabled");
//				}
//			});
//		}

		
	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}

	public Button getBtnGo() {
		return btnGo;
	}

	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
