package com.slicktechnologies.client.views.teammanagement;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.scheduling.TeamGroup;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;

public class TeamManagementForm extends FormTableScreen<TeamManagement> implements ClickHandler,ChangeHandler {
	
	
	TextBox tbTeamGroupName;
	CheckBox cbTeamgroupstatus;
	DoubleBox dbTotalWorkinHrs;
	DoubleBox dbFromTime,dbToTime;
	ObjectListBox<Shift> olbShift;
	ObjectListBox<Employee> olbTeamMember;
	
	Button btnadd;
	TeamGroupTable teamGroupTable;
	
	public static boolean editFlag=false;
	
	TeamManagement teamManagementObj;
	
	public TeamManagementForm(SuperTable<TeamManagement> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		teamGroupTable.connectToLocal();
	}
	
	
	private void initalizeWidget() {
		tbTeamGroupName = new TextBox();
		
		dbTotalWorkinHrs=new DoubleBox();
		dbTotalWorkinHrs.setEnabled(false);
		
		dbFromTime=new DoubleBox();
		dbFromTime.setEnabled(false);
		
		dbToTime=new DoubleBox();
		dbToTime.setEnabled(false);
		
		cbTeamgroupstatus = new CheckBox();
		cbTeamgroupstatus.setValue(true);
		
		olbShift = new ObjectListBox<Shift>();
		this.initalizeShiftListBox();
		olbShift.addChangeHandler(this);
		
		olbTeamMember = new ObjectListBox<Employee>();
		this.initalizeEmployeeListBox();
		
		btnadd = new Button("Add");
		btnadd.addClickHandler(this);
		teamGroupTable = new TeamGroupTable();
	}


	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames = new String[] { "New" };

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingGroupInformation = fbuilder
				.setlabel("Team Management Information").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("* Team Name", tbTeamGroupName);
		FormField ftbgroupname = fbuilder.setMandatory(true)
				.setMandatoryMsg("Team Name can't be empty").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Working Hours", dbTotalWorkinHrs);
		FormField fdbTotalWorkinHrs = fbuilder.setMandatory(false)
				.setMandatoryMsg("Reserved Time can't be empty").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Shift", olbShift);
		FormField fshift = fbuilder.setMandatory(true).setMandatoryMsg("Shift is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status", cbTeamgroupstatus);
		FormField fcbstatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("From Time (24hrs Format)", dbFromTime);
		FormField fdbFromTime = fbuilder.setMandatory(true).setMandatoryMsg("From Time is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Time (24hrs Format)", dbToTime);
		FormField fdbToTime = fbuilder.setMandatory(true).setMandatoryMsg("To Time is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDaysInformation = fbuilder
				.setlabel("Team Group Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Employee", olbTeamMember);
		FormField folbleavetype = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", btnadd);
		FormField fbtnadd = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", teamGroupTable.getTable());
		FormField fleaveallocatedtable = fbuilder.setMandatory(true)
				.setMandatoryMsg("Team Member table can't be empty")
				.setRowSpan(0).setColSpan(4).build();


		FormField[][] formfield = { 
				{ fgroupingGroupInformation },
				{ ftbgroupname,fshift,fdbTotalWorkinHrs, fcbstatus },
				{ fdbFromTime,fdbToTime},
				{ fgroupingDaysInformation },
				{ folbleavetype, fbtnadd }, 
				{ fleaveallocatedtable } 
				};

		this.fields = formfield;
	}
	
	
	
	
	
	
	
	
	
	@Override
	public void updateModel(TeamManagement model) {
		if (tbTeamGroupName != null)
			model.setTeamName(tbTeamGroupName.getValue());
		if(olbShift.getValue()!=null){
			model.setShiftName(olbShift.getValue());
		}
		if(dbTotalWorkinHrs.getValue()!=null){
			model.setTotalWorkingHrs(dbTotalWorkinHrs.getValue());
		}
		if (cbTeamgroupstatus != null)
			model.setStatus(cbTeamgroupstatus.getValue());
		
		if(dbFromTime.getValue()!=null){
			model.setFromTime(dbFromTime.getValue());
		}
		if(dbToTime.getValue()!=null){
			model.setToTime(dbToTime.getValue());
		}
		
		List<TeamGroup> daysalloc = teamGroupTable.getDataprovider().getList();
		ArrayList<TeamGroup> alloc = new ArrayList<TeamGroup>();
		alloc.addAll(daysalloc);
		model.setTeamList(alloc);

		teamManagementObj=model;
		
		presenter.setModel(model);
	}


	@Override
	public void updateView(TeamManagement view) {
		
		
		teamManagementObj=view;
		
		if(view.getTeamName()!=null){
			tbTeamGroupName.setValue(view.getTeamName());
		}
		if(view.getShiftName()!=null){
			olbShift.setValue(view.getShiftName());
		}
		if(view.getTotalWorkingHrs()!=null){
			dbTotalWorkinHrs.setValue(view.getTotalWorkingHrs());
		}
		if(view.getStatus()!=null){
			cbTeamgroupstatus.setValue(view.getStatus());
		}

		dbFromTime.setValue(view.getFromTime());
		dbToTime.setValue(view.getToTime());
		teamGroupTable.setValue(view.getTeamList());
		
		presenter.setModel(view);
		
	}


	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")|| text.equals(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")|| text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")|| text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.TEAMMANAGEMENT,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public boolean validate() {
		boolean val= super.validate();
		if(val==false){
			return false;
		}
		
		if(teamGroupTable.getDataprovider().getList().size()==0){
			this.showDialogMessage("Please add employee !");
			return false;
		}
		
		return true;
	}


	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource().equals(btnadd)){
			if(olbTeamMember.getValue()!=null){
				if(validateDuplicateEmployee()){
					editFlag=true;
					TeamGroup tg=new TeamGroup();
					tg.setEmployeeName(olbTeamMember.getValue());
					/** date 05/03/2018 added by komal to set more values **/
					Employee emp = olbTeamMember.getSelectedItem();
					if(!(emp.getDesignation().equals("")))
					tg.setEmpDesignation(emp.getDesignation());
					tg.setPhoneNumber(emp.getCellNumber1());
					tg.setCustomerCount(emp.getCount());

					teamGroupTable.getDataprovider().getList().add(tg);
				}
			}else{
				this.showDialogMessage("Please select employee !");
			}
		}
	}
	
	
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(olbShift)){
			System.out.println("INSIDE CHANGE HANDLER....");
			
			if(olbShift.getSelectedIndex()!=0){
				Shift shift=olbShift.getSelectedItem();
				
				dbFromTime.setValue(shift.getFromTime());
				dbToTime.setValue(shift.getToTime());
			}
			else{
				dbFromTime.setValue(null);
				dbToTime.setValue(null);
			}
		}
	}

	
	
	public boolean validateDuplicateEmployee(){
		if(teamGroupTable.getDataprovider().getList().size()!=0){
			for(int i=0;i<teamGroupTable.getDataprovider().getList().size();i++){
				if(teamGroupTable.getDataprovider().getList().get(i).getEmployeeName().equals(olbTeamMember.getValue())){
					this.showDialogMessage("Can't add duplicate data!");
					return false;
				}
			}
		}
		return true;
	}
	
	protected void initalizeShiftListBox() {
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Shift());
		olbShift.MakeLive(querry);
	}
	protected void initalizeEmployeeListBox() {
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Employee());
		olbTeamMember.MakeLive(querry);
	}

	@Override
	public void clear() {
		super.clear();
		cbTeamgroupstatus.setValue(true);
		teamGroupTable.clear();

	}


	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		dbFromTime.setEnabled(false);
		dbToTime.setEnabled(false);
		dbTotalWorkinHrs.setEnabled(false);
		teamGroupTable.setEnable(state);
	}
	
	@Override
	public void setToViewState() {
		super.setToViewState();
		System.out.println("Inside view method");
		
		
		SuperModel model=new TeamManagement();
		model=teamManagementObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.TEAMMANAGEMENT, teamManagementObj.getCount(), null,null,null, false, model, null);
		
	}


	@Override
	public void setToNewState() {
		super.setToNewState();
		System.out.println("Inside NEW method");
		if(teamManagementObj!=null){
			System.out.println("Inside Entity NOt NUll");
			SuperModel model=new TeamManagement();
			model=teamManagementObj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.TEAMMANAGEMENT, teamManagementObj.getCount(), null,null,null, false, model, null);
			
		}
	}
}
