package com.slicktechnologies.client.views.teammanagement;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;

public class TeamManagementPresenterTableProxy extends SuperTable<TeamManagement> {

	TextColumn<TeamManagement> getGroupNameColumn;
	TextColumn<TeamManagement> getStatusColumn;
	TextColumn<TeamManagement> getCountColumn;
	TextColumn<TeamManagement> getTotalWorkingHrColumn;
	
	TextColumn<TeamManagement> getShiftNameColumn;

	public Object getVarRef(String varName) {
		if (varName.equals("getGroupNameColumn"))
			return this.getGroupNameColumn;
		if (varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if (varName.equals("getCountColumn"))
			return this.getCountColumn;
		if (varName.equals("getShiftNameColumn"))
			return this.getShiftNameColumn;
		return null;
	}

	public TeamManagementPresenterTableProxy() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetGroupName();
		addColumngetShiftName();
		getTotalWorkingHrColumn();
		addColumngetStatus();
	}

	

	

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<TeamManagement>() {
			@Override
			public Object getKey(TeamManagement item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetGroupName();
		addSortinggetShiftName();
		addSortinggetTotalWorkingHrs();
		addSortinggetStatus();
	}

	

	@Override
	public void addFieldUpdater() {
	}

	
	private void getTotalWorkingHrColumn() {
		getTotalWorkingHrColumn = new TextColumn<TeamManagement>() {
			@Override
			public String getValue(TeamManagement object) {
				if (object.getTotalWorkingHrs()==null)
					return "N.A";
				else
					return object.getTotalWorkingHrs() + "";
			}
		};
		table.addColumn(getTotalWorkingHrColumn, "Total Workin Hours");
		getTotalWorkingHrColumn.setSortable(true);
	}
	
	private void addSortinggetTotalWorkingHrs() {
		List<TeamManagement> list = getDataprovider().getList();
		columnSort = new ListHandler<TeamManagement>(list);
		columnSort.setComparator(getTotalWorkingHrColumn, new Comparator<TeamManagement>() {
			@Override
			public int compare(TeamManagement e1, TeamManagement e2) {
				if (e1 != null && e2 != null) {
					if (e1.getTotalWorkingHrs() == e2.getTotalWorkingHrs()) {
						return 0;
					}
					if (e1.getTotalWorkingHrs() > e2.getTotalWorkingHrs()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCount() {
		List<TeamManagement> list = getDataprovider().getList();
		columnSort = new ListHandler<TeamManagement>(list);
		columnSort.setComparator(getCountColumn, new Comparator<TeamManagement>() {
			@Override
			public int compare(TeamManagement e1, TeamManagement e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCount() {
		getCountColumn = new TextColumn<TeamManagement>() {
			@Override
			public String getValue(TeamManagement object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "Id");
		getCountColumn.setSortable(true);
	}

	protected void addSortinggetGroupName() {
		List<TeamManagement> list = getDataprovider().getList();
		columnSort = new ListHandler<TeamManagement>(list);
		columnSort.setComparator(getGroupNameColumn,new Comparator<TeamManagement>() {
			@Override
			public int compare(TeamManagement e1, TeamManagement e2) {
				if (e1 != null && e2 != null) {
					if (e1.getTeamName() != null&& e2.getTeamName() != null) {
						return e1.getTeamName().compareTo(e2.getTeamName());
					}
					} else {
						return 0;
					}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetGroupName() {
		getGroupNameColumn = new TextColumn<TeamManagement>() {
			@Override
			public String getValue(TeamManagement object) {
				return object.getTeamName() + "";
			}
		};
		table.addColumn(getGroupNameColumn, "Team Name");
		getGroupNameColumn.setSortable(true);
	}

	protected void addSortinggetStatus() {
		List<TeamManagement> list = getDataprovider().getList();
		columnSort = new ListHandler<TeamManagement>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<TeamManagement>() {
			@Override
			public int compare(TeamManagement e1, TeamManagement e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() == e2.getStatus()) {
						return 0;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	
	
	
	
	
	
	protected void addSortinggetShiftName() {
		List<TeamManagement> list = getDataprovider().getList();
		columnSort = new ListHandler<TeamManagement>(list);
		columnSort.setComparator(getShiftNameColumn,new Comparator<TeamManagement>() {
			@Override
			public int compare(TeamManagement e1, TeamManagement e2) {
				if (e1 != null && e2 != null) {
					if (e1.getShiftName() != null&& e2.getShiftName() != null) {
						return e1.getShiftName().compareTo(e2.getShiftName());
					}
					} else {
						return 0;
					}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetShiftName() {
		getShiftNameColumn = new TextColumn<TeamManagement>() {
			@Override
			public String getValue(TeamManagement object) {
				return object.getShiftName() + "";
			}
		};
		table.addColumn(getShiftNameColumn, "Shift Name");
		getShiftNameColumn.setSortable(true);
	}
	
	
	
	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<TeamManagement>() {
			@Override
			public String getValue(TeamManagement object) {
				if (object.getStatus() == true)
					return "Active";
				else
					return "In Active";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
	}

}
