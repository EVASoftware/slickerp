package com.slicktechnologies.client.views.teammanagement;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.scheduling.TeamGroup;

public class TeamGroupTable extends SuperTable<TeamGroup> {

	TextColumn<TeamGroup> getTeamMemberName;
	private Column<TeamGroup, String> delete;
	
	
	public TeamGroupTable() {
		super();
	}
	
	@Override
	public void createTable() {
		getTeamMemberName();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	private void addeditColumn()
	{
		getTeamMemberName();
		addColumnDelete();
		setFieldUpdaterOnDelete();
	}
	
	private void addViewColumn()
	{
		getTeamMemberName();
	}

	private void getTeamMemberName() {
		getTeamMemberName = new TextColumn<TeamGroup>() {
			@Override
			public String getValue(TeamGroup object) {
				return object.getEmployeeName();
			}
		};
		table.addColumn(getTeamMemberName,"Team Member");
	}

	private void addColumnDelete() {
		ButtonCell btnCell= new ButtonCell();
		delete = new Column<TeamGroup, String>(btnCell) {
			@Override
			public String getValue(TeamGroup object) {
				return "Delete";
			}
		};
		table.addColumn(delete,"Delete");
	}

	private void setFieldUpdaterOnDelete() {
		delete.setFieldUpdater(new FieldUpdater<TeamGroup, String>() {
			@Override
			public void update(int index, TeamGroup object, String value) {
				TeamManagementForm.editFlag=true;
				getDataprovider().getList().remove(object);
			
			}
		});
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
	}

	@Override
	public void applyStyle() {
		
	}

}
