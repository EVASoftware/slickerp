package com.slicktechnologies.client.views.teammanagement;

import java.util.ArrayList;
import java.util.Vector;

import org.apache.poi.hssf.record.DBCellRecord;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.RowCountChangeEvent.Handler;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterial;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.slicktechnologies.shared.common.workorder.WorkOrderProductDetails;

public class TeamManagementPresenter extends FormTableScreenPresenter<TeamManagement> implements Handler {

	TeamManagementForm form;
	final static  GenricServiceAsync async = GWT.create(GenricService.class);
	int counter;
	double workingHrSum;
	
	public TeamManagementPresenter(FormTableScreen<TeamManagement> view,TeamManagement model) {
		super(view, model);
		form=(TeamManagementForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getTeamGroupQuery());
		form.teamGroupTable.getTable().addRowCountChangeHandler(this);
		form.setPresenter(this);
	}
	
	public static TeamManagementForm initalize()
	{
		TeamManagementPresenterTableProxy gentableScreen=new TeamManagementPresenterTableProxy();
		TeamManagementForm  form=new  TeamManagementForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
			
		TeamManagementPresenter  presenter=new  TeamManagementPresenter(form,new TeamManagement());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	private MyQuerry getTeamGroupQuery() {
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new TeamManagement());
		return quer;
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
	}

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}

	
	@Override
	protected void makeNewModel() {
		model=new TeamManagement();
	}
	
	
	public void setModel(TeamManagement entity)
	{
		model=entity;
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		System.out.println("INSIDE ROW COUNT CHANGE METHOD.....");
		if(event.getSource()==form.teamGroupTable.getTable()){
			if(form.editFlag==true){
				if(form.teamGroupTable.getDataprovider().getList().size()!=0){
					
					counter=0;
					workingHrSum=0;
					for(int i=0;i<form.teamGroupTable.getDataprovider().getList().size();i++){
						getTeamWorkingHours(form.teamGroupTable.getDataprovider().getList().get(i).getEmployeeName());
						form.editFlag=false;
					}
					
				}else{
					System.out.println("ELSE...");
					workingHrSum=0;
					form.dbTotalWorkinHrs.setValue(workingHrSum);
					form.editFlag=false;
				}
			}
		}
		
	}

	private void getTeamWorkingHours(String employeeName) {
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = new Filter();
		filter.setQuerryString("fullName");
		filter.setStringValue(employeeName);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new EmployeeInfo());
		view.showWaitSymbol();
		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					counter++;
					for (SuperModel model : result) {
						EmployeeInfo entity = (EmployeeInfo) model;
						if(entity.isLeaveAllocated()){
//							workingHrSum=workingHrSum+entity.getLeaveCalendar().getWorkingHours();
							workingHrSum=entity.getLeaveCalendar().getWorkingHours();
						}else{
							int size=form.teamGroupTable.getDataprovider().getList().size();
							System.out.println("TABLE SIZE :: "+size);
							form.teamGroupTable.getDataprovider().getList().remove(size-1);
							form.showDialogMessage("Calendar is not allocated !");
						}
					}
				}
				if(counter==form.teamGroupTable.getDataprovider().getList().size()){
					System.out.println("INSIDE ROW COMPLETE...");
					System.out.println("WOrKING Hr SUM :: "+workingHrSum);
					
					form.dbTotalWorkinHrs.setValue(workingHrSum);
				}
				
				
			}
			@Override
			public void onFailure(Throwable caught) {
				view.hideWaitSymbol();
			}
		});
		view.hideWaitSymbol();
	}

}
