package com.slicktechnologies.client.views.interaction.interactiondashboard;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.complaindashboard.complainHomeForm;
import com.slicktechnologies.client.views.complaindashboard.complainHomePresenter;
import com.slicktechnologies.client.views.complaindashboard.complainHomePresenterSearchProxy;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionForm;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionPresenter;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionPresenterTableProxy;
import com.slicktechnologies.shared.common.WhatsNew;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;

public class InteractionDashboardPresentor implements ClickHandler{

	public static InteractionDashboardForm form;
	InteractionDashboardSerachProxy searchpopup;
	
	protected GenricServiceAsync service=GWT.create(GenricService.class);
	
	
public InteractionDashboardPresentor(InteractionDashboardForm form, InteractionDashboardSerachProxy searchpopup){
		
		this.form=form;
		this.searchpopup=searchpopup;
		SetEventHandling();
		searchpopup.applyHandler(this);
		form.getBtnGo().addClickHandler(this);
		
		MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter=null;
		
		
		filter=new Filter();
		filter.setQuerryString("interactionStatus");
		filter.setStringValue(InteractionType.INTERACTIONCREATED);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new InteractionType());
		this.retriveTable(querry,form.getInteractionTable());
		
		
		setTableSelectionOnInteractionList();
	}


		public static void initialize()
		{
			getwhatnewdata();
			InteractionDashboardForm homeForm = new InteractionDashboardForm();
		//	AppMemory.getAppMemory().currentScreen=Screen.COMPLAINDASHBOARD;
			AppMemory.getAppMemory().skeleton.getProcessName().setText("Dashboard/Home");
			InteractionDashboardSerachProxy searchpopup=new InteractionDashboardSerachProxy();
			InteractionDashboardPresentor presenter= new InteractionDashboardPresentor(homeForm,searchpopup);
			AppMemory.getAppMemory().stickPnel(homeForm);	
		}

		
		
		
	private static void getwhatnewdata() {
		
		GenricServiceAsync service=GWT.create(GenricService.class);
		
		
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter=null;
		Company comp = new Company();
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(comp.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new WhatsNew());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				System.out.println("REslut CRON JOB"+result.size());
				WhatsNew whatnew = new WhatsNew();
				
				for(SuperModel model:result){
					
					whatnew =(WhatsNew) model;
				}
				
				if(whatnew!=null)
				{
					form.updatewhatsnew(whatnew);
				}
			}
			@Override
			public void onFailure(Throwable caught) {
			}
		});
	
		}


	private void setTableSelectionOnInteractionList() {


		 final NoSelectionModel<InteractionType> selectionModelMyObj = new NoSelectionModel<InteractionType>();
	     
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 //setContractRedirection();
	        	 final InteractionType entity=selectionModelMyObj.getLastSelectedObject();
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Interaction TO DO",Screen.INTERACTION);
	        	// DeviceForm.flag=false;
	        	 final InteractionForm form = InteractionPresenter.initalize();
	        	 form.showWaitSymbol();
					
				 AppMemory.getAppMemory().stickPnel(form);
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					 form.hideWaitSymbol();
					     form.updateView(entity);
					     form.setToViewState();
	 				}
	 			};
	            
	             timer.schedule(3000); 
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     
	     form.interactionTable.getTable().setSelectionModel(selectionModelMyObj);
	 
	}

	public <T> void retriveTable(MyQuerry querry,final SuperTable<T>table)
	 {
		table.connectToLocal();
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
			@SuppressWarnings("unchecked")
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("Rsult=========="+result.size());
				
//				if(result.size()==0){
//					final GWTCAlert alert = new GWTCAlert();
//					alert.alert("No Result To Display");
//				}
				
				// vijay added code for in list show only to list as per login user by default
				String user= LoginPresenter.loggedInUser;
				for(SuperModel model:result){
					InteractionType interactiontype = (InteractionType) model;
					  System.out.println(" user loggen in name === "+user);
					  System.out.println("person responsible from do tolist"+interactiontype.getInteractionPersonResponsible());
					if(user.equals(interactiontype.getInteractionPersonResponsible()))
						table.getListDataProvider().getList().add((T) interactiontype);
					 
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
			caught.printStackTrace();
				
			}
		}); 
	 }

	@Override
	public void onClick(ClickEvent event) {

		if(event.getSource() instanceof InlineLabel)
		{
		InlineLabel lbl= (InlineLabel) event.getSource();
		String text= lbl.getText();
		if(text.equals("Search"))
			searchpopup.showPopUp();
		
		if(text.equals("Go"))
			reactOnGo();
		}
		
		
		if(event.getSource().equals(form.getBtnGo())){
			if(form.getOlbbranch().getSelectedIndex()==0&&
			form.getOlbassignTo().getSelectedIndex()==0&&
			form.getDbFromDate().getValue()==null&&form.getDbToDate().getValue()==null){
		
			final GWTCAlert alert = new GWTCAlert();
			alert.alert("Select atleast one field to search");
		}
			
		else{	
			
			if(form.getOlbassignTo().getSelectedIndex()!=0&&form.getOlbbranch().getSelectedIndex()==0
			   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
				
				System.out.println("Person Responsible");
				searchByFromToDate();
				
			}
			else if(form.getOlbassignTo().getSelectedIndex()==0&&form.getOlbbranch().getSelectedIndex()!=0
					   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
				
				
				System.out.println("branch ");
				searchByFromToDate();
			}
			else if(form.getOlbassignTo().getSelectedIndex()!=0&&form.getOlbbranch().getSelectedIndex()!=0
					   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
				
				System.out.println("for both AssignTo  and branch");
				searchByFromToDate();
			}
			else{
			System.out.println("Remaining all situations ");
			
			if(form.getDbFromDate().getValue()==null||form.getDbToDate().getValue()==null){
				final GWTCAlert alert = new GWTCAlert();
				alert.alert("Select From and To Date.");
			}
			 if(form.getDbFromDate().getValue()!=null&&form.getDbToDate()!=null){
			Date formDate=form.getDbFromDate().getValue();
			Date toDate=form.getDbToDate().getValue();
			if(toDate.equals(formDate)||toDate.after(formDate)){
					System.out.println("Inside Date Condition"); 
					searchByFromToDate();
			}
			else{
				final GWTCAlert alert = new GWTCAlert();
				alert.alert("To Date should be greater than From Date.");
			}
			}
			}
		}
		}
		
	
	}
	
	private void searchByFromToDate() {

		System.out.println("Hiiiiiiiiiiii........");
		Long compId=UserConfiguration.getCompanyId();
		System.out.println("Company Id :: "+compId);
		    
		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		
		filtervec=new Vector<Filter>();
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("interactionStatus");
		filter.setStringValue(InteractionType.INTERACTIONCREATED);
		filtervec.add(filter);
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("interactionDueDate >=");
		filter.setDateValue(form.getDbFromDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
			
		filter = new Filter();
		filter.setQuerryString("interactionDueDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
		System.out.println("inside Complain***********");
			filter = new Filter();
			filter.setQuerryString("interactionBranch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);
			
		}
		
		if(form.getOlbassignTo().getSelectedIndex()!=0){
			filter = new Filter();
			filter.setQuerryString("interactionPersonResponsible");
			filter.setStringValue(form.getOlbassignTo().getValue());
			filtervec.add(filter);
		}
		
		querry=new MyQuerry();
		querry.setQuerryObject(new InteractionType());
		querry.setFilters(filtervec);
		this.retriveIntractionDetails(querry,form.getInteractionTable());
	
	}


	private void reactOnGo() {
		
		System.out.println(" hi vijay inside reacton go");
		MyQuerry querry =  searchpopup.createFilter1();
		form.interactionTable.connectToLocal();
		this.retriveIntractionDetails(querry, form.getInteractionTable());
		 searchpopup.hidePopUp();
			searchpopup.oblPersonType.setSelectedIndex(0);


	}


	private void retriveIntractionDetails(MyQuerry querry,final InteractionPresenterTableProxy interactionTable) {

		
		interactionTable.connectToLocal();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {

				if(result.size()==0){
					final GWTCAlert alert = new GWTCAlert();
					alert.alert("No Result To Display");
				}
				
				for(SuperModel model:result){
					
					InteractionType interactiontype = (InteractionType) model;
					interactionTable.getListDataProvider().getList().add(interactiontype);
					 
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	
	/**
	 * Sets the event handling.
	 */
	public void SetEventHandling()
	{
		 InlineLabel label[] =AppMemory.getAppMemory().skeleton.getMenuLabels();
		  for(int k=0;k<label.length;k++)
		  {
			 if( AppMemory.getAppMemory().skeleton.registration[k]!=null)
			    AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
			 
				 
		  }
		 
		  for( int k=0;k<label.length;k++)
		  {
			  
			  AppMemory.getAppMemory().skeleton.registration[k]= label[k].addClickHandler(this);
		  }
	}

}
