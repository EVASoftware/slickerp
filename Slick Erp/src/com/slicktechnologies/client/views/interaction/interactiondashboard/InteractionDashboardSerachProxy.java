package com.slicktechnologies.client.views.interaction.interactiondashboard;

import java.util.Vector;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;

public class InteractionDashboardSerachProxy extends ViewContainer implements ChangeHandler{

	ObjectListBox<Config> oblPersonType;
	PersonInfoComposite personInfoComposite;
	EmployeeInfoComposite employeeInfoComposite;
	PersonInfoComposite personInfoVendorComposite;
	
	protected MyQuerry interaction;
	protected PopupPanel popup;
	protected InlineLabel golbl;
	public HorizontalPanel horizontal;
	FlexForm form;
	
	TextBox tbblank;
	
	
	public InteractionDashboardSerachProxy(){
		super();
		createGui();
		applyStyle();
	}
	
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("Business Partner Type",oblPersonType);
		FormField foblPersonType= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", personInfoComposite);
		FormField fpersonInfoComposite = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
				

		fbuilder = new FormFieldBuilder("", employeeInfoComposite);
		FormField femployeeInfoComposite = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
				

		fbuilder = new FormFieldBuilder("", personInfoVendorComposite);
		FormField fvendorInfoComposite = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("", tbblank);
		FormField flbblank = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(3).build();
		
		
		FormField[][] fields=new FormField[][]{
				{foblPersonType,flbblank},
				{fpersonInfoComposite},{fvendorInfoComposite},{femployeeInfoComposite},
		};
		form=new FlexForm(fields,FormStyle.COLUMNFORM);
		content.add(form);
		horizontal=new HorizontalPanel();
		//Go lbl
		golbl=new InlineLabel("Go");
		golbl.getElement().setId("addbutton");
		horizontal.add(golbl);
		horizontal.getElement().addClassName("centering");
	
		content.add(horizontal);
		
		popup=new PopupPanel(true);
		popup.add(content);
		
	
	}
	
	private void initWidget() {

		oblPersonType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblPersonType, Screen.PERSONTYPE);
		oblPersonType.addChangeHandler(this);
		
		tbblank = new TextBox();
		tbblank.setVisible(false);
		
		MyQuerry custquerry = new MyQuerry();
		Filter custfilteer = new Filter();
		custfilteer.setQuerryString("status");
		custfilteer.setBooleanvalue(true);
		custquerry.setQuerryObject(new Customer());
		personInfoComposite = new PersonInfoComposite(custquerry, false);

		MyQuerry vendorquerry = new MyQuerry();
		Filter vendorfilteer = new Filter();
		vendorfilteer.setQuerryString("status");
		vendorfilteer.setBooleanvalue(true);
		vendorquerry.setQuerryObject(new Vendor());
		personInfoVendorComposite = new PersonInfoComposite(vendorquerry, false);

		MyQuerry emplquerry = new MyQuerry();
		Filter emplfilteer = new Filter();
		emplfilteer.setQuerryString("status");
		emplfilteer.setBooleanvalue(true);
		emplquerry.setQuerryObject(new EmployeeInfo());
		employeeInfoComposite = new EmployeeInfoComposite(emplquerry, false);


		personInfoComposite.setEnabled(false);
		employeeInfoComposite.setEnable(false);
		personInfoVendorComposite.setEnabled(false);
		
		employeeInfoComposite.setVisible(false);
		personInfoComposite.setVisible(false);
		personInfoVendorComposite.setVisible(false);
		
	}

	public MyQuerry createFilter1()
	 {
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		 
		if(oblPersonType.getValue()!=null)
		{
			temp=new Filter();
			temp.setStringValue(oblPersonType.getValue().trim());
			temp.setQuerryString("interactionPersonType");
			filtervec.add(temp);
		}
		
		  if(!personInfoComposite.getId().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setIntValue(Integer.parseInt(personInfoComposite.getId().getValue()));
			  temp.setQuerryString("personInfo.count");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoComposite.getName().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setStringValue(personInfoComposite.getName().getValue().trim());
			  temp.setQuerryString("personInfo.fullName");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoComposite.getPhone().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setLongValue(Long.parseLong(personInfoComposite.getPhone().getValue().trim()));
			  temp.setQuerryString("personInfo.cellNumber");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoVendorComposite.getId().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setIntValue(Integer.parseInt(personInfoVendorComposite.getId().getValue()));
			  temp.setQuerryString("personVendorInfo.count");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoVendorComposite.getName().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setStringValue(personInfoVendorComposite.getName().getValue().trim());
			  temp.setQuerryString("personVendorInfo.fullName");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoVendorComposite.getPhone().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setLongValue(Long.parseLong(personInfoVendorComposite.getPhone().getValue().trim()));
			  temp.setQuerryString("personVendorInfo.cellNumber");
			  filtervec.add(temp);
		  }
		  
		  if(!employeeInfoComposite.getId().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setIntValue(Integer.parseInt(employeeInfoComposite.getId().getValue()));
			  temp.setQuerryString("employeeInfo.empCount");
			  filtervec.add(temp);
		  }
		  
		  if(!employeeInfoComposite.getName().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setStringValue(employeeInfoComposite.getName().getValue().trim());
			  temp.setQuerryString("employeeInfo.fullName");
			  filtervec.add(temp);
		  }
		  
		  if(!employeeInfoComposite.getPhone().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setLongValue(Long.parseLong(employeeInfoComposite.getPhone().getValue().trim()));
			  temp.setQuerryString("employeeInfo.cellNumber");
			  filtervec.add(temp);
		  }
		  
		  // end here
		
		MyQuerry querry=new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new InteractionType());
		return querry;
		
	 }
	

	@Override
	protected void createGui() {
		// TODO Auto-generated method stub
		createScreen();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		content.getElement().setId("formcontent");
		form.getElement().setId("form");
	}


	@Override
	public void onChange(ChangeEvent event) {

		if (event.getSource() == oblPersonType) {

			if (oblPersonType.getSelectedIndex() == 0) {
				clearComposit();
				employeeInfoComposite.setVisible(false);
				personInfoComposite.setVisible(false);
				personInfoVendorComposite.setVisible(false);

				personInfoComposite.setEnabled(false);
				employeeInfoComposite.setEnable(false);
				personInfoVendorComposite.setEnabled(false);
			}
			if (oblPersonType.getValue().equals("Customer")) {
				clearComposit();
				employeeInfoComposite.setVisible(false);
				personInfoComposite.setVisible(true);
				personInfoVendorComposite.setVisible(false);

				personInfoComposite.setEnabled(true);
				employeeInfoComposite.setEnable(false);
				personInfoVendorComposite.setEnabled(false);

			}
			if (oblPersonType.getValue() == "Employee"
					|| oblPersonType.getValue().equals("Employee")) {
				clearComposit();
				employeeInfoComposite.setVisible(true);
				personInfoComposite.setVisible(false);
				personInfoVendorComposite.setVisible(false);

				personInfoComposite.setEnabled(false);
				employeeInfoComposite.setEnable(true);
				personInfoVendorComposite.setEnabled(false);

			}
			if (oblPersonType.getValue() == "Vendor"
					|| oblPersonType.getValue().equalsIgnoreCase("Vendor")) {
				clearComposit();
				employeeInfoComposite.setVisible(false);
				personInfoComposite.setVisible(false);
				personInfoVendorComposite.setVisible(true);

				personInfoComposite.setEnabled(false);
				employeeInfoComposite.setEnable(false);
				personInfoVendorComposite.setEnabled(true);
			}
		}
	}
	
	
	private void clearComposit() {

		if (personInfoComposite.getValue() != null) {
			personInfoComposite.clear();
		}
		if (personInfoVendorComposite.getValue() != null) {
			personInfoVendorComposite.clear();
		}
		if (employeeInfoComposite.getValue() != null) {
			employeeInfoComposite.clear();
		}

	}
	
	public void showPopUp() {
		clearComposit();
		popup.center();
		popup.setSize("500px","200px");
		popup.getElement().setId("searchpopup");
	    popup.setAnimationEnabled(true);
	}

	public void hidePopUp()
	{
		popup.hide();
	}
	
	public void applyHandler(ClickHandler handler)
	{
		golbl.addClickHandler(handler);
	}

}
