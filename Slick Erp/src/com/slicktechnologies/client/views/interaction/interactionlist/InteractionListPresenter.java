package com.slicktechnologies.client.views.interaction.interactionlist;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionForm;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionPresenter;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionPresenterSearchProxy;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionPresenterTableProxy;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class InteractionListPresenter extends TableScreenPresenter<InteractionType>{

	TableScreen<InteractionType>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	public InteractionListPresenter(TableScreen<InteractionType> view,InteractionType model) {
		super(view, model);
		Console.log("ERP in InteractionListPresenter");
		form=view;
		Console.log("ERP in InteractionListPresenter step 1");
		view.retriveTable(getInteractionListQuerry());
		Console.log("ERP in InteractionListPresenter step 2");
		setTableSelectionOnInteractionList();
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.INTERACTIONLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	public InteractionListPresenter(TableScreen<InteractionType> view,InteractionType model,MyQuerry querry) {
		super(view, model);
		Console.log("ERP in InteractionListPresenter2");
		
		form=view;
		view.retriveTable(querry);
		setTableSelectionOnInteractionList();
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.INTERACTIONLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		model=new InteractionType();
	}
	
	
	@Override
	public void reactOnDownload() {
		ArrayList<InteractionType> interactionarray=new ArrayList<InteractionType>();
		List<InteractionType> list=(List<InteractionType>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		interactionarray.addAll(list);
		
		csvservice.setInteractionDetails(interactionarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				

				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+64;
				Window.open(url, "test", "enabled");
				
			}
		});
	}
	
	
	
	
	public MyQuerry getInteractionListQuerry()
	{
		Console.log("ERP in getInteractionListQuerry");
		MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(model.getCompanyId());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("interactionStatus");
		filter.setStringValue(InteractionType.INTERACTIONCREATED);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("interactionDueDate >=");
		filter.setDateValue(new Date());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("interactionDueDate <=");
		filter.setDateValue(new Date());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new InteractionType());
		Console.log("ERP in getInteractionListQuerry returning query");
		return querry;
	}
	
	
	public void setTableSelectionOnInteractionList()
	 {
		 final NoSelectionModel<InteractionType> selectionModelMyObj = new NoSelectionModel<InteractionType>();
	     
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 //setContractRedirection();
	        	 final InteractionType entity=selectionModelMyObj.getLastSelectedObject();
	        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
	        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Follow Up",Screen.INTERACTION);//Ashwini Patil changed "Interaction TO do" to "Follow UP"
	        	// DeviceForm.flag=false;
	        	 final InteractionForm form = InteractionPresenter.initalize();
	        	 form.showWaitSymbol();
					
				 AppMemory.getAppMemory().stickPnel(form);
	        	
				 Timer timer=new Timer() 
	        	 {
	 				@Override
	 				public void run() {
	 					 form.hideWaitSymbol();
					     form.updateView(entity);
					     form.setToViewState();
	 				}
	 			};
	            
	             timer.schedule(3000); 
	          }
	     };
	     
	     // Add the handler to the selection model
	     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	     // Add the selection model to the table
	     form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
	 }
	
	
	public static void initalize()
	{
		Console.log("ERP in interaction initialize");
		InteractionPresenterTableProxy gentable=new InteractionPresenterTableProxy();
		InteractionListForm form=new InteractionListForm(gentable);
		
		InteractionPresenterSearchProxy.staticSuperTable=gentable;
		InteractionPresenterSearchProxy searchpopup=new InteractionPresenterSearchProxy(false);
		form.setSearchpopupscreen(searchpopup);
		
		InteractionListPresenter presenter=new InteractionListPresenter(form,new InteractionType());
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	public static void initialize(MyQuerry querry)
	{
		InteractionPresenterTableProxy gentable=new InteractionPresenterTableProxy();
		InteractionListForm form=new InteractionListForm(gentable);
		
		InteractionPresenterSearchProxy.staticSuperTable=gentable;
		InteractionPresenterSearchProxy searchpopup=new InteractionPresenterSearchProxy(false);
		form.setSearchpopupscreen(searchpopup);
		
		InteractionListPresenter presenter=new InteractionListPresenter(form,new InteractionType(),querry);
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	
	

}
