package com.slicktechnologies.client.views.interaction.interactionlist;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class InteractionListForm extends TableScreen<InteractionType>{

	public InteractionListForm(SuperTable<InteractionType> supertable) {
		super(supertable);
		createGui();
	}
	
	
	@Override
	public void retriveTable(MyQuerry querry) {
		Console.log("ERP in retriveTable listform");
		superTable.connectToLocal();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				Console.log("ERP in retriveTable listform on success");
				// vijay added code for in list show only to list as per login user by default
				String user= LoginPresenter.loggedInUser;
				for(SuperModel model:result){
					InteractionType interactiontype = (InteractionType) model;
					  System.out.println(" user loggen in name === "+user);
					  System.out.println("person responsible from do tolist"+interactiontype.getInteractionPersonResponsible());
					  Console.log("ERP in retriveTable listform in for interactiontype.getInteractionPersonResponsible()="+interactiontype.getInteractionPersonResponsible()+" user="+user);
					  if(user.equals(interactiontype.getInteractionPersonResponsible()))
						superTable.getListDataProvider().getList().add(interactiontype);
					 
				}
				
				//   old code 
//				for(SuperModel model:result)
//				superTable.getListDataProvider().getList().add((InteractionType) model);
//			//	superTable.getTable().setVisibleRange(0, result.size());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				
			}
		});
		
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if(superTable!=null&&superTable.getListDataProvider().getList()!=null){
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Rows:")){
					menus[k].setText("Rows:"+superTable.getListDataProvider().getList().size());	
					menus[k].getElement().setId("addlabel2");
				}
			}
		}
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Download")||text.equals("Search")||text.equals(AppConstants.NAVIGATION)||text.contains("Rows:"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.INTERACTIONLIST,LoginPresenter.currentModule.trim());
	}

}
