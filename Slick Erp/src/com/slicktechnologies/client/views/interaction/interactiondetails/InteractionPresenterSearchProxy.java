package com.slicktechnologies.client.views.interaction.interactiondetails;

import java.util.Vector;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class InteractionPresenterSearchProxy extends SearchPopUpScreen<InteractionType> implements ChangeHandler {
	
	IntegerBox ibinteractionId;
	TextBox tbinteractionStatus,tbinteractionTitle,tbinteractionPurpose,tbinteractionOutcome;
//	DateBox dbinteractionCreationDate,dbinteractionDueDate;
	
	ObjectListBox<Employee> oblPersonResponsible;
	ObjectListBox<Branch> oblbranch;
	
	ObjectListBox<Config> oblGroupType;
	ObjectListBox<Config> oblCommunicationCategory;
	ObjectListBox<Config> oblCommunicationType;
	ObjectListBox<Type> oblDocumentName;
	ObjectListBox<ConfigCategory> oblModuleName;
	ObjectListBox<HrProject> oblinteractionProject;
	
	
	
//	public DateComparator dateComparator;
	public DateComparator dateCompDue;
	public ListBox lbStatus;
	
	/**
	 * rohan added created by 
	 */
	
		public ObjectListBox<Employee> olbCreatedBy;
	
	/**
	 * 
	 */
	
	
	PersonInfoComposite personInfoComposite;
	EmployeeInfoComposite employeeInfoComposite;
	PersonInfoComposite personInfoVendorComposite;
	ObjectListBox<Config> oblPersonType;
	
	/**
	 * Date : 13-07-2017 By ANIL
	 * added document id search 
	 */
	
	IntegerBox ibDocumentId;
	/**
	 * End
	 */
	
	public Object getVarRef(String varName)
	{
		return null;
	}
	public InteractionPresenterSearchProxy() {
		super();
		createGui();
	}
	
	public InteractionPresenterSearchProxy(boolean b) {
		super(b);
		createGui();
	}
	
	public void initWidget()
	{
		ibinteractionId=new IntegerBox();
		tbinteractionTitle=new TextBox();
		tbinteractionStatus=new TextBox();
		
		tbinteractionPurpose=new TextBox();
		tbinteractionOutcome=new TextBox();
		
		oblinteractionProject=new ObjectListBox<HrProject>();
		initializeProject();
		
//		dbinteractionCreationDate=new DateBox();
//		dbinteractionDueDate=new DateBox();
		
		oblPersonResponsible =new ObjectListBox<Employee>();
//		intializePersonResponsible();
		oblPersonResponsible.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.INTERACTIONTODO, "Person Responsible");
		
		oblbranch =new ObjectListBox<Branch>();
		intializeBranch();
		oblModuleName=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblModuleName, Screen.MODULENAME);
		oblDocumentName=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(oblDocumentName, Screen.DOCUMENTNAME);
		oblModuleName.addChangeHandler(this);
		
		oblCommunicationType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblCommunicationType, Screen.COMMUNICATIONTYPE);
		oblCommunicationCategory=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblCommunicationCategory, Screen.COMMUNICATIONCATEGORY);
		oblGroupType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblGroupType, Screen.GROUPTYPE);
		
		oblPersonType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblPersonType, Screen.PERSONTYPE);
		oblPersonType.addChangeHandler(this);
		
//		dateComparator= new DateComparator("interactionCreationDate",new InteractionType());
		dateCompDue= new DateComparator("interactionDueDate",new InteractionType());
		
		lbStatus=new ListBox();
		AppUtility.setStatusListBox(lbStatus, InteractionType.getStatusList());
		
		// vijay commented this for employee composite,personInfoVendorComposite,personInfoComposite
		
//		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
//		employeeInfoComposite=AppUtility.employeeInfoComposite(new EmployeeInfo(),true);
//		personInfoVendorComposite=AppUtility.vendorInfoComposite(new Vendor());
		
		MyQuerry custquerry = new MyQuerry();
		Filter custfilteer = new Filter();
		custfilteer.setQuerryString("status");
		custfilteer.setBooleanvalue(true);
		custquerry.setQuerryObject(new Customer());
		personInfoComposite = new PersonInfoComposite(custquerry, false);

		MyQuerry vendorquerry = new MyQuerry();
		Filter vendorfilteer = new Filter();
		vendorfilteer.setQuerryString("status");
		vendorfilteer.setBooleanvalue(true);
		vendorquerry.setQuerryObject(new Vendor());
		personInfoVendorComposite = new PersonInfoComposite(vendorquerry, false);

		MyQuerry emplquerry = new MyQuerry();
		Filter emplfilteer = new Filter();
		emplfilteer.setQuerryString("status");
		emplfilteer.setBooleanvalue(true);
		emplquerry.setQuerryObject(new EmployeeInfo());
		employeeInfoComposite = new EmployeeInfoComposite(emplquerry, false);


		personInfoComposite.setEnabled(false);
		employeeInfoComposite.setEnable(false);
		personInfoVendorComposite.setEnabled(false);

		employeeInfoComposite.setVisible(false);
		personInfoComposite.setVisible(false);
		personInfoVendorComposite.setVisible(false);
		
		
		olbCreatedBy= new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbCreatedBy);
		
		ibDocumentId=new IntegerBox();
	}
	
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("Id",ibinteractionId);
		FormField fibinteractionid= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Title",tbinteractionTitle);
		FormField ftbinteractionTitle= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Purpose",tbinteractionPurpose);
		FormField ftbinteractionPurpose= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Outcome",tbinteractionOutcome);
		FormField ftbinteractionOutcome= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("From Date (Creation Date)",dateComparator.getFromDate());
//		FormField fdbFromCreationDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		  
//		fbuilder = new FormFieldBuilder("To Date (Creation Date)",dateComparator.getToDate());
//		FormField fdbToCreationDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Due Date (From Date)",dateCompDue.getFromDate());
		FormField fdbFromDueDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		  
		fbuilder = new FormFieldBuilder("Due Date (To Date)",dateCompDue.getToDate());
		FormField fdbToDueDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		  
		fbuilder = new FormFieldBuilder("Project",oblinteractionProject);
		FormField foblinteractionProject= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Branch",oblbranch);
		FormField ftbinteractionBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Interaction Group",oblGroupType);
		FormField ftbinteractionGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status",lbStatus);
		FormField flbststus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Person Responsible",oblPersonResponsible);
		FormField ftbinteractionPersonResponsible= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Document Type",oblDocumentName);
		FormField ftbinteractionDocument= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Module Name",oblModuleName);
		FormField ftbinteractionModuleName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Communication Type",oblCommunicationType);
		FormField ftbinteractionCommunicationType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Communication Category",oblCommunicationCategory);
		FormField ftbinteractionCommunicationCategory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Business Partner Type",oblPersonType);
		FormField foblPersonType= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", personInfoComposite);
		FormField fpersonInfoComposite = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
				

		fbuilder = new FormFieldBuilder("", employeeInfoComposite);
		FormField femployeeInfoComposite = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
				

		fbuilder = new FormFieldBuilder("", personInfoVendorComposite);
		FormField fvendorInfoComposite = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Created By",olbCreatedBy);
		FormField folbCreatedBy= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Document Id",ibDocumentId);
		FormField fibDocumentId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = {	
				{foblPersonType},
				{fpersonInfoComposite},{fvendorInfoComposite},{femployeeInfoComposite},
				{fibinteractionid,ftbinteractionTitle,ftbinteractionPurpose,ftbinteractionOutcome},
				{foblinteractionProject,flbststus,fdbFromDueDate,fdbToDueDate},
				//{/*fdbFromCreationDate,fdbToCreationDate,*/},
				{ftbinteractionModuleName,ftbinteractionDocument,fibDocumentId,ftbinteractionBranch},
				{ftbinteractionPersonResponsible,ftbinteractionGroup,ftbinteractionCommunicationType,ftbinteractionCommunicationCategory},
				{folbCreatedBy},
				};
		this.fields=formfield;
		
		
	}
/**************************Retrivi***********************************/	
	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(ibinteractionId.getValue()!=null)
		{
			temp=new Filter();
			temp.setIntValue(ibinteractionId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		if(tbinteractionTitle.getValue().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(tbinteractionTitle.getValue().trim());
			temp.setQuerryString("interactionTitle");
			filtervec.add(temp);
		}
		
//		if(dateComparator.getValue()!=null)
//		{
//			filtervec.addAll(dateComparator.getValue());
//		}
		 
		if(dateCompDue.getValue()!=null)
		{
			filtervec.addAll(dateCompDue.getValue());
		}
		
		if(!tbinteractionOutcome.getValue().equals(""))
		{
			temp=new Filter();
			temp.setStringValue(tbinteractionOutcome.getValue().trim());
			temp.setQuerryString("interactionOutcome");
			filtervec.add(temp);
		}
		 
		if(oblinteractionProject.getValue()!=null)
		{
			temp=new Filter();
			temp.setStringValue(oblinteractionProject.getValue().trim());
			temp.setQuerryString("interactionProject");
			filtervec.add(temp);
		}
		if(oblModuleName.getValue()!=null)
		{
			temp=new Filter();
			temp.setStringValue(oblModuleName.getValue(oblModuleName.getSelectedIndex()).trim());
			temp.setQuerryString("interactionModuleName");
			filtervec.add(temp);
		}
		if(oblDocumentName.getValue()!=null)
		{
			temp=new Filter();
			temp.setStringValue(oblDocumentName.getValue(oblDocumentName.getSelectedIndex()).trim());
			temp.setQuerryString("interactionDocumentName");
			filtervec.add(temp);
		}
		if(oblCommunicationType.getValue()!=null)
		{
			temp=new Filter();
			temp.setStringValue(oblCommunicationType.getValue().trim());
			temp.setQuerryString("interactionCommunicationType");
			filtervec.add(temp);
		}
		if(oblCommunicationCategory.getValue()!=null)
		{
			temp=new Filter();
			temp.setStringValue(oblCommunicationCategory.getValue().trim());
			temp.setQuerryString("interactionCommunicationCategory");
			filtervec.add(temp);
		}
		if(oblPersonResponsible.getValue()!=null)
		{
			temp=new Filter();
			temp.setStringValue(oblPersonResponsible.getValue().trim());
			temp.setQuerryString("interactionPersonResponsible");
			filtervec.add(temp);
		}
		
		if(!tbinteractionPurpose.getValue().equals(""))
		{
			temp=new Filter();
			temp.setStringValue(tbinteractionPurpose.getValue().trim());
			temp.setQuerryString("interactionPurpose");
			filtervec.add(temp);
		}
		
		if(olbCreatedBy.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbCreatedBy.getValue());
			temp.setQuerryString("createdBy");
			filtervec.add(temp);
		}
		
		if(oblGroupType.getValue()!=null)
		{
			temp=new Filter();
			temp.setStringValue(oblGroupType.getValue().trim());
			temp.setQuerryString("interactionGroup");
			filtervec.add(temp);
		}
		
		if(oblbranch.getValue()!=null)
		{
			temp=new Filter();
			temp.setStringValue(oblbranch.getValue().trim());
			temp.setQuerryString("interactionBranch");
			filtervec.add(temp);
		}
		
		
		if(lbStatus.getSelectedIndex()!=0){
			temp=new Filter();
			int selValue=lbStatus.getSelectedIndex();
			String selectedVal=lbStatus.getItemText(selValue);
			temp.setStringValue(selectedVal);
			temp.setQuerryString("interactionStatus");
			filtervec.add(temp);
		}
		
		
		
		// vijay 
		
		  if(!personInfoComposite.getId().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setIntValue(Integer.parseInt(personInfoComposite.getId().getValue()));
			  temp.setQuerryString("personInfo.count");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoComposite.getName().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setStringValue(personInfoComposite.getName().getValue().trim());
			  temp.setQuerryString("personInfo.fullName");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoComposite.getPhone().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setLongValue(Long.parseLong(personInfoComposite.getPhone().getValue().trim()));
			  temp.setQuerryString("personInfo.cellNumber");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoVendorComposite.getId().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setIntValue(Integer.parseInt(personInfoVendorComposite.getId().getValue()));
			  temp.setQuerryString("personVendorInfo.count");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoVendorComposite.getName().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setStringValue(personInfoVendorComposite.getName().getValue().trim());
			  temp.setQuerryString("personVendorInfo.fullName");
			  filtervec.add(temp);
		  }
		  
		  if(!personInfoVendorComposite.getPhone().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setLongValue(Long.parseLong(personInfoVendorComposite.getPhone().getValue().trim()));
			  temp.setQuerryString("personVendorInfo.cellNumber");
			  filtervec.add(temp);
		  }
		  
		  if(!employeeInfoComposite.getId().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setIntValue(Integer.parseInt(employeeInfoComposite.getId().getValue()));
			  temp.setQuerryString("employeeInfo.empCount");
			  filtervec.add(temp);
		  }
		  
		  if(!employeeInfoComposite.getName().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setStringValue(employeeInfoComposite.getName().getValue().trim());
			  temp.setQuerryString("employeeInfo.fullName");
			  filtervec.add(temp);
		  }
		  
		  if(!employeeInfoComposite.getPhone().getValue().equals(""))
		  {
			  temp=new Filter();
			  temp.setLongValue(Long.parseLong(employeeInfoComposite.getPhone().getValue().trim()));
			  temp.setQuerryString("employeeInfo.cellNumber");
			  filtervec.add(temp);
		  }
		  if(oblPersonType.getValue()!=null)
			{
				temp=new Filter();
				temp.setStringValue(oblPersonType.getValue().trim());
				temp.setQuerryString("interactionPersonType");
				filtervec.add(temp);
			}
		  
		  if(ibDocumentId.getValue()!=null)
			{
				temp=new Filter();
				temp.setIntValue(ibDocumentId.getValue());
				temp.setQuerryString("interactionDocumentId");
				filtervec.add(temp);
			}
		
		MyQuerry querry=new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new InteractionType());
		return querry;
	}
	/***************************initializing objectlist data other than config**********************************/
	protected void intializePersonResponsible()
	{
		MyQuerry qurr=new MyQuerry(new Vector<Filter>(),new Employee());
		oblPersonResponsible.MakeLive(qurr);

	}
	protected void initializeProject() {
		MyQuerry qurr=new MyQuerry(new Vector<Filter>(),new HrProject());
		oblinteractionProject.MakeLive(qurr);
		
	}
	protected void intializeBranch() {
		MyQuerry qurr=new MyQuerry(new Vector<Filter>(),new Branch());
		oblbranch.MakeLive(qurr);
		
	}
	/*************************initializing objectlist data other than config END*****************************/
	
	
	
	
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(oblModuleName))
		{
				if(oblModuleName.getSelectedIndex()!=0){
					ConfigCategory cat=oblModuleName.getSelectedItem();
					if(cat!=null){
						AppUtility.makeLiveTypeDropDown(oblDocumentName, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
					}
				}
		}
		
		//vijay
		if (event.getSource() == oblPersonType) {

			if (oblPersonType.getSelectedIndex() == 0) {
				clearComposit();
				employeeInfoComposite.setVisible(false);
				personInfoComposite.setVisible(false);
				personInfoVendorComposite.setVisible(false);

				personInfoComposite.setEnabled(false);
				employeeInfoComposite.setEnable(false);
				personInfoVendorComposite.setEnabled(false);
			}
			if (oblPersonType.getValue().equals("Customer")) {
				clearComposit();
				employeeInfoComposite.setVisible(false);
				personInfoComposite.setVisible(true);
				personInfoVendorComposite.setVisible(false);

				personInfoComposite.setEnabled(true);
				employeeInfoComposite.setEnable(false);
				personInfoVendorComposite.setEnabled(false);

			}
			if (oblPersonType.getValue() == "Employee"
					|| oblPersonType.getValue().equals("Employee")) {
				clearComposit();
				employeeInfoComposite.setVisible(true);
				personInfoComposite.setVisible(false);
				personInfoVendorComposite.setVisible(false);

				personInfoComposite.setEnabled(false);
				employeeInfoComposite.setEnable(true);
				personInfoVendorComposite.setEnabled(false);

			}
			if (oblPersonType.getValue() == "Vendor"
					|| oblPersonType.getValue().equalsIgnoreCase("Vendor")) {
				clearComposit();
				employeeInfoComposite.setVisible(false);
				personInfoComposite.setVisible(false);
				personInfoVendorComposite.setVisible(true);

				personInfoComposite.setEnabled(false);
				employeeInfoComposite.setEnable(false);
				personInfoVendorComposite.setEnabled(true);
			}
		}
	}
	
	
	private void clearComposit() {

		if (personInfoComposite.getValue() != null) {
			personInfoComposite.getId().setValue(null);
			personInfoComposite.getName().setValue(null);
			personInfoComposite.getPhone().setValue(null);
		}
		if (personInfoVendorComposite.getValue() != null) {
			personInfoVendorComposite.getId().setValue(null);
			personInfoVendorComposite.getName().setValue(null);
			personInfoVendorComposite.getPhone().setValue(null);
		}
		if (employeeInfoComposite.getValue() != null) {
			employeeInfoComposite.getId().setValue(null);
			employeeInfoComposite.getName().setValue(null);
			employeeInfoComposite.getPhone().setValue(null);
			employeeInfoComposite.setBranch("");
			employeeInfoComposite.setDepartment("");
			
			employeeInfoComposite.setEmployeeType("");
			employeeInfoComposite.setEmpDesignation(null);
			employeeInfoComposite.setEmpRole(null);
		}

	}
	
	
	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(oblbranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}
		return true;
	}
}
