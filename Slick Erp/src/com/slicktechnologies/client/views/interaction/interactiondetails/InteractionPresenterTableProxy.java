package com.slicktechnologies.client.views.interaction.interactiondetails;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;

public class InteractionPresenterTableProxy extends SuperTable<InteractionType> {

	TextColumn<InteractionType> getIdColumn;
	TextColumn<InteractionType> getTitleColumn;
	TextColumn<InteractionType> getPersonResponsibleColumn;
	TextColumn<InteractionType> getStatusColumn;

	TextColumn<InteractionType> getCreationDateColumn;
	TextColumn<InteractionType> getDueDateColumn;
	TextColumn<InteractionType> getProjectColumn;
	TextColumn<InteractionType> getBranchColumn;
	TextColumn<InteractionType> getGroupColumn;

	TextColumn<InteractionType> getModuleNameColumn;
	TextColumn<InteractionType> getDocumentNameColumn;
	TextColumn<InteractionType> getDocumentidColumn;
	TextColumn<InteractionType> getCommunicationtypeColumn;
	TextColumn<InteractionType> getCommunicationCategoryColumn;
	TextColumn<InteractionType> getPriority;

	TextColumn<InteractionType> getPurposeColumn;
	TextColumn<InteractionType> getOutcomeColumn;

	TextColumn<InteractionType> getPersonTypeColumn;
	TextColumn<InteractionType> getPersonTypeNameColumn;

	ListHandler<InteractionType> columnSort;

	
	/**
	 * rohan added this code for NBHC for 
	 */

	TextColumn<InteractionType> getCreatedByColumn;
	
	
	
	public InteractionPresenterTableProxy() {
		super();
		setHeight("800px");
	}
	
	// this costructor for dashboard
	public InteractionPresenterTableProxy(boolean b) {
		super();
	}

	@Override
	public void createTable() {
		addColumnId();
		addColumngetCreatedBy();
		addColumnTitle();
		addColumnPurpose();
		addColumnOutcome();
		addColumnCommunicationCategory();
		addColumnCommunicationType();
		addColumnModuleName();
		addColumnDocumnetName();
		addColumnDocumentId();
		addColumnDueDate();
		addColumnPersonResponsible();
		addColumnBranch();
		addColumnProject();
		addColumnStatus();
		addColumnPersonType();
		addColumnPersonName();
	}

	
	private void addColumngetCreatedBy() {
		
		getCreatedByColumn=new TextColumn<InteractionType>()
				{
			@Override
			public String getValue(InteractionType object)
			{
				return object.getCreatedBy();
			}
				};
				table.addColumn(getCreatedByColumn,"Created By");
				table.setColumnWidth(getCreatedByColumn, 110, Unit.PX);
				getCreatedByColumn.setSortable(true);
	}
	
	
	protected void addSortinggetCreatedBy()
	{
		List<InteractionType> list=getDataprovider().getList();
		columnSort=new ListHandler<InteractionType>(list);
		columnSort.setComparator(getCreatedByColumn, new Comparator<InteractionType>()
				{
			@Override
			public int compare(InteractionType e1,InteractionType e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreatedBy()!=null && e2.getCreatedBy()!=null){
						return e1.getCreatedBy().compareTo(e2.getCreatedBy());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub

	}

	/******************************** Retrieving Data and Showing in tabular Form ********************************/

	// ID Column
	protected void addColumnId() {
		getIdColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getCount() + "";
			}
		};
		table.addColumn(getIdColumn, "ID");
		table.setColumnWidth(getIdColumn, 100, Unit.PX);
		getIdColumn.setSortable(true);
	}

	protected void addColumnTitle() {
		getTitleColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getInteractionTitle() + "";
			}
		};
		table.addColumn(getTitleColumn, "Title");
		table.setColumnWidth(getTitleColumn, 110, Unit.PX);
		getTitleColumn.setSortable(true);

	}

	protected void addColumnPersonType() {
		getPersonTypeColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getInteractionPersonType() + "";
			}
		};
		table.addColumn(getPersonTypeColumn, "Partner Type");
		table.setColumnWidth(getPersonTypeColumn, 110, Unit.PX);
		getPersonTypeColumn.setSortable(true);
	}

	protected void addColumnPersonName() {
		getPersonTypeNameColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {

				return object.getPersonInfo().getFullName() + ""
						+ object.getEmployeeInfo().getFullName() + ""
						+ object.getPersonVendorInfo().getFullName() + "";
			}
		};
		table.addColumn(getPersonTypeNameColumn, "Person Name");
		table.setColumnWidth(getPersonTypeNameColumn, 110, Unit.PX);
		getPersonTypeNameColumn.setSortable(true);

	}

	protected void addColumnCreationDate() {
		getCreationDateColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getinteractionCreationDate() + "";
			}
		};
		table.addColumn(getCreationDateColumn, "Creation Date");
		table.setColumnWidth(getCreationDateColumn, 100, Unit.PX);
		getCreationDateColumn.setSortable(true);
	}

	protected void addColumnStatus() {
		getStatusColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getInteractionStatus() + "";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		table.setColumnWidth(getStatusColumn, 100, Unit.PX);
		getStatusColumn.setSortable(true);
	}

	protected void addColumnDocumentId() {
		getDocumentidColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getInteractionDocumentId() + "";
			}
		};
		table.addColumn(getDocumentidColumn, "Doc ID");
		table.setColumnWidth(getDocumentidColumn, 100, Unit.PX);
		getDocumentidColumn.setSortable(true);

	}

	protected void addColumnOutcome() {
		getOutcomeColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getInteractionOutcome() + "";
			}
		};
		table.addColumn(getOutcomeColumn, "Outcome");
		table.setColumnWidth(getOutcomeColumn, 100, Unit.PX);
		getOutcomeColumn.setSortable(true);
	}

	protected void addColumnPurpose() {
		getPurposeColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getInteractionPurpose() + "";
			}
		};
		table.addColumn(getPurposeColumn, "Purpose");
		table.setColumnWidth(getPurposeColumn, 250, Unit.PX);
		getPurposeColumn.setSortable(true);
	}

	protected void addColumnCommunicationCategory() {
		getCommunicationCategoryColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getInteractionCommunicationCategory() + "";
			}
		};
		table.addColumn(getCommunicationCategoryColumn, "Com Category");
		table.setColumnWidth(getCommunicationCategoryColumn, 100, Unit.PX);
		getCommunicationCategoryColumn.setSortable(true);
	}

	protected void addColumnCommunicationType() {
		getCommunicationtypeColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getInteractionCommunicationType() + "";
			}
		};
		table.addColumn(getCommunicationtypeColumn, "Com Type");
		table.setColumnWidth(getCommunicationtypeColumn, 100, Unit.PX);
		getCommunicationtypeColumn.setSortable(true);
	}

	protected void addColumnDocumnetName() {
		getDocumentNameColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getInteractionDocumentName() + "";
			}
		};
		table.addColumn(getDocumentNameColumn, "Doc Name");
		table.setColumnWidth(getDocumentNameColumn, 100, Unit.PX);
		getDocumentNameColumn.setSortable(true);
	}

	protected void addColumnModuleName() {
		getModuleNameColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getInteractionModuleName() + "";
			}
		};
		table.addColumn(getModuleNameColumn, "Module Name");
		table.setColumnWidth(getModuleNameColumn, 110, Unit.PX);
		getModuleNameColumn.setSortable(true);
	}

	protected void addColumnDueDate() {
		getDueDateColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return AppUtility.parseDate(object.getInteractionDueDate());
			}
		};
		table.addColumn(getDueDateColumn, "Due Date");
		table.setColumnWidth(getDueDateColumn, 100, Unit.PX);
		getDueDateColumn.setSortable(true);	//vijay added for red n black color
		
		final Date todaydate = new Date();
		CalendarUtil.addDaysToDate(todaydate,-1);
		todaydate.setHours(23);
		todaydate.setMinutes(59);
		todaydate.setSeconds(59);
		
		System.out.println(" today date"+todaydate);
		getDueDateColumn = new TextColumn<InteractionType>() {
			
			@Override
			public String getCellStyleNames(Context context,	InteractionType object) {
				System.out.println(" due date ==="+object.getInteractionDueDate());
				if(todaydate.after(object.getInteractionDueDate())){
					return "red";
				}
				else{
					return "black";
				}
			}

			@Override
			public String getValue(InteractionType object) {

				return AppUtility.parseDate(object.getInteractionDueDate());
			}
		};
		table.addColumn(getDueDateColumn, "Due Date");
		table.setColumnWidth(getDueDateColumn, 100, Unit.PX);
		getDueDateColumn.setSortable(true);

	}

	protected void addColumnGroup() {
		getGroupColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getInteractionGroup() + "";
			}
		};
		table.addColumn(getGroupColumn, "Group Type");
		table.setColumnWidth(getGroupColumn, 100, Unit.PX);
		getGroupColumn.setSortable(true);
	}

	protected void addColumnBranch() {
		getBranchColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getInteractionBranch() + "";
			}
		};
		table.addColumn(getBranchColumn, "Branch");
		table.setColumnWidth(getBranchColumn, 110, Unit.PX);
		getBranchColumn.setSortable(true);
	}

	protected void addColumnProject() {
		getProjectColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getInteractionProject() + "";
			}
		};
		table.addColumn(getProjectColumn, "Project");
		table.setColumnWidth(getProjectColumn, 100, Unit.PX);
		getProjectColumn.setSortable(true);
	}

	protected void addColumnPersonResponsible() {
		getPersonResponsibleColumn = new TextColumn<InteractionType>() {
			@Override
			public String getValue(InteractionType object) {
				return object.getInteractionPersonResponsible() + "";
			}
		};
		table.addColumn(getPersonResponsibleColumn, "Person Resp");
		table.setColumnWidth(getPersonResponsibleColumn, 110, Unit.PX);
		getPersonResponsibleColumn.setSortable(true);
	}

	/*************************************** Retrieving END ***************************************************/

	/************************************** Column Sorting *****************************************/

	public void addColumnSorting() {
		addColumnIdSorting();
		addColumnTitleSorting();
		addColumnPurposeSorting();
		addColumnOutcomeSorting();
		addColumnCommunicationCategorySorting();
		addColumnCommunicationTypeSorting();
		addColumnModuleNameSorting();
		addColumnDocumnetNameSorting();
		addColumnDocumentIdSorting();
		addColumnDueDateSorting();
		addColumnPersonResponsibleSorting();
		addColumnProjectSorting();
		addColumnStatusSorting();
		addColumnPersonTypeSorting();
		addColumnPersonNameSorting();
		addSortinggetCreatedBy();
	}

	// Id sorting..
	public void addColumnIdSorting() {
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount())
						return 0;
					else if (e1.getCount() < e2.getCount())
						return 1;
					else
						return -1;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getIdColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	// title Sorting....
	public void addColumnTitleSorting() {
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInteractionTitle() != null
							&& e2.getInteractionTitle() != null)
						return e1.getInteractionTitle().compareTo(
								e2.getInteractionTitle());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getTitleColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	// Purpose Sorting
	private void addColumnPurposeSorting() {
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInteractionPurpose() != null
							&& e2.getInteractionPurpose() != null)
						return e1.getInteractionPurpose().compareTo(
								e2.getInteractionPurpose());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getPurposeColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	// Outcome Sorting
	private void addColumnOutcomeSorting() {
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInteractionOutcome() != null
							&& e2.getInteractionOutcome() != null)
						return e1.getInteractionOutcome().compareTo(
								e2.getInteractionOutcome());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getOutcomeColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	// Communication Category Sorting
	private void addColumnCommunicationCategorySorting() {
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInteractionCommunicationCategory() != null
							&& e2.getInteractionCommunicationCategory() != null)
						return e1
								.getInteractionCommunicationCategory()
								.compareTo(
										e2.getInteractionCommunicationCategory());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getCommunicationCategoryColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	// Communication Type Sorting
	private void addColumnCommunicationTypeSorting() {
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInteractionCommunicationType() != null
							&& e2.getInteractionCommunicationType() != null)
						return e1.getInteractionCommunicationType().compareTo(
								e2.getInteractionCommunicationType());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getCommunicationtypeColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	// Module name Sorting
	private void addColumnModuleNameSorting() {
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInteractionModuleName() != null
							&& e2.getInteractionModuleName() != null)
						return e1.getInteractionModuleName().compareTo(
								e2.getInteractionModuleName());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getModuleNameColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	// Document name sorting
	private void addColumnDocumnetNameSorting() {
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInteractionDocumentName() != null
							&& e2.getInteractionDocumentName() != null)
						return e1.getInteractionDocumentName().compareTo(
								e2.getInteractionDocumentName());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getDocumentNameColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	// Document Id Sorting
	private void addColumnDocumentIdSorting() {
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInteractionDocumentId() == e2
							.getInteractionDocumentId())
						return 0;
					else if (e1.getInteractionDocumentId() < e2
							.getInteractionDocumentId())
						return 1;
					else
						return -1;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getDocumentidColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	// Due Date Sorting
	private void addColumnDueDateSorting() {
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInteractionDueDate() != null
							&& e2.getInteractionDueDate() != null)
						return e1.getInteractionDueDate().compareTo(
								e2.getInteractionDueDate());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getDueDateColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	// Person Responsible Sorting
	private void addColumnPersonResponsibleSorting() {
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInteractionPersonResponsible() != null
							&& e2.getInteractionPersonResponsible() != null)
						return e1.getInteractionPersonResponsible().compareTo(
								e2.getInteractionPersonResponsible());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getPersonResponsibleColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	// Project Sorting
	private void addColumnProjectSorting() {
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInteractionProject() != null
							&& e2.getInteractionProject() != null)
						return e1.getInteractionProject().compareTo(
								e2.getInteractionProject());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getProjectColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	// Status Sorting
	private void addColumnStatusSorting() {
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInteractionStatus() != null
							&& e2.getInteractionStatus() != null)
						return e1.getInteractionStatus().compareTo(
								e2.getInteractionStatus());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getStatusColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	// Person Type Sorting
	private void addColumnPersonTypeSorting() {
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					if (e1.getInteractionPersonType() != null
							&& e2.getInteractionPersonType() != null)
						return e1.getInteractionPersonType().compareTo(
								e2.getInteractionPersonType());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getPersonTypeColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	// Person Name Sorting
	private void addColumnPersonNameSorting() {
		// final ArrayList<String> personNameList = new ArrayList<String>();
		List<InteractionType> list = getDataprovider().getList();
		columnSort = new ListHandler<InteractionType>(list);
		Comparator<InteractionType> quote = new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
				if (e1 != null && e2 != null) {
					// while(e1!=null){
					// personNameList.add(e1.getPersonInfo().getFullName());
					// personNameList.add(e1.getPersonVendorInfo().getFullName());
					// personNameList.add(e1.getEmployeeInfo().getFullName());
					// }

					if ((e1.getPersonInfo().getFullName() != null && e2
							.getPersonInfo().getFullName() != null)
							|| (e1.getPersonVendorInfo().getFullName() != null && e2
									.getPersonVendorInfo().getFullName() != null)
							|| (e1.getEmployeeInfo().getFullName() != null && e2
									.getEmployeeInfo().getFullName() != null))
						return e1.getPersonInfo().getFullName()
								.compareTo(e2.getPersonInfo().getFullName());
					return 0;
					// Collections.sort(personNameList);
					// return personNameList;

				} else
					return 0;
			}
		};
		columnSort.setComparator(getPersonTypeNameColumn, quote);
		table.addColumnSortHandler(columnSort);
	}
	/************************************** Column Sorting End *****************************************/

}
