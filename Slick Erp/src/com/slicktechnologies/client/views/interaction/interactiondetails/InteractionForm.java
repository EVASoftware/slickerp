package com.slicktechnologies.client.views.interaction.interactiondetails;

import java.util.Date;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.EmployeeInfoComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.history.HistoryPopup;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class InteractionForm extends FormScreen<InteractionType> implements
		ChangeHandler {

	// /////////////////////////////////////Form Field
	// Declaration///////////////////////////////////////////////
	TextBox ibinteractionId, ibinteractionDocumentId;
	DateBox dbinteractionCreationDate, dbinteractionDueDate;
	TextBox tbinteractionTitle, tbinteractionStatus, tbinteractionbranch;

	//  rohan commented this 
	//	TextBox tbinteractionPurpose, tbinteractionOutcome;

	/**
	 * rohan added this code for increase in size of outcome and and purpose 
	 */
	TextArea tbinteractionPurpose, tbinteractionOutcome;
	
	
	/**
	 * Rohan added created By field for NBHC to know who is going to create/ generate interaction
	 * date : 03/11/2016
	 */
		TextBox tbCreatedBy;
	
	/**
	 * 
	 */
	
	ObjectListBox<Type> oblDocumentName;
	ObjectListBox<ConfigCategory> oblModuleName;
	
	ObjectListBox<Config> oblCommunicationCategory;
	ObjectListBox<Config> oblCommunicationType;

	ObjectListBox<Config> oblPersonType;
	ObjectListBox<Config> oblGroupType;

	PersonInfoComposite personInfoComposite;
	EmployeeInfoComposite employeeInfoComposite;
	PersonInfoComposite personInfoVendorComposite;

	ObjectListBox<Employee> oblPersonResponsible;
	ObjectListBox<Branch> oblbranch;

	ObjectListBox<HrProject> oblProject;
	ObjectListBox<Config> oblPriority;

	// Todays Date Initilization
	Date creatndate = new Date();
	InteractionType interactionObj;
	
	EmailServiceAsync emailService = GWT.create(EmailService.class);
	
	// Date 14 April 2017 added by vijay
	TextBox tbTime;
	
	/**
	 * @author Anil , Date : 27-12-2019
	 */
	TextBox tbInteractionDueTime;
	

	// ////////////////////////////////////////////Declaration
	// END////////////////////////////////////////////////////

	/************************************* CONSTRUCTOR ***********************************************/
	public InteractionForm() {
		super();
		createGui();
		tbinteractionStatus.setValue(InteractionType.INTERACTIONCREATED);
		this.dbinteractionCreationDate.setValue(creatndate);
		this.dbinteractionCreationDate.setValue(creatndate);
	}

	public InteractionForm(String[] processlevel, FormField[][] fields,
			FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
	}

	/********************************************************************************************/
	// //*******************************************Initializing All Form
	// Fields************************************////
	private void initalizeWidget() {
		ibinteractionId = new TextBox();
		ibinteractionId.setEnabled(false);
		tbinteractionTitle = new TextBox();
//		dbinteractionCreationDate = new DateBox();
		dbinteractionCreationDate = new DateBoxWithYearSelector();

		tbinteractionStatus = new TextBox();
		tbinteractionStatus.setEnabled(false);
		dbinteractionCreationDate.setEnabled(false);

		oblProject = new ObjectListBox<HrProject>();
		initializeProject();
	
//		dbinteractionDueDate = new DateBox();
		dbinteractionDueDate = new DateBoxWithYearSelector();


		oblModuleName = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(oblModuleName, Screen.MODULENAME);
		oblModuleName.addChangeHandler(this);
		oblDocumentName = new ObjectListBox<Type>();		
		AppUtility.makeTypeListBoxLive(oblDocumentName, Screen.DOCUMENTNAME);
//		setOneToManyCombo();
		
		oblCommunicationType = new ObjectListBox<Config>();
		ibinteractionDocumentId = new TextBox();
		AppUtility.MakeLiveConfig(oblCommunicationType,
				Screen.COMMUNICATIONTYPE);
		oblCommunicationCategory = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblCommunicationCategory,
				Screen.COMMUNICATIONCATEGORY);

		oblPersonResponsible = new ObjectListBox<Employee>();
//		initializePersonResponsible();
		oblPersonResponsible.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.INTERACTIONTODO, "Person Responsible");
		oblbranch = new ObjectListBox<Branch>();
		initiliazebranch();
		oblGroupType = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblGroupType, Screen.GROUPTYPE);

		oblPriority = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblPriority, Screen.INTERACTIONPRIORITY);

		oblPersonType = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(oblPersonType, Screen.PERSONTYPE);
		oblPersonType.addChangeHandler(this);

		/***************** Person,Vendor,Employee Composite DEPENDS ON PERSON TYPE **********/

		MyQuerry custquerry = new MyQuerry();
		Filter custfilteer = new Filter();
		custfilteer.setQuerryString("status");
		custfilteer.setBooleanvalue(true);
		custquerry.setQuerryObject(new Customer());
		personInfoComposite = new PersonInfoComposite(custquerry, false);

		MyQuerry vendorquerry = new MyQuerry();
		Filter vendorfilteer = new Filter();
		vendorfilteer.setQuerryString("status");
		vendorfilteer.setBooleanvalue(true);
		vendorquerry.setQuerryObject(new Vendor());
		personInfoVendorComposite = new PersonInfoComposite(vendorquerry, false);

		MyQuerry emplquerry = new MyQuerry();
		Filter emplfilteer = new Filter();
		emplfilteer.setQuerryString("status");
		emplfilteer.setBooleanvalue(true);
		emplquerry.setQuerryObject(new EmployeeInfo());
		employeeInfoComposite = new EmployeeInfoComposite(emplquerry, false);

		personInfoComposite.setEnabled(false);
		employeeInfoComposite.setEnable(false);
		personInfoVendorComposite.setEnabled(false);

		employeeInfoComposite.setVisible(false);
		personInfoComposite.setVisible(false);
		personInfoVendorComposite.setVisible(false);
		
		
		//     rohan bhagde  commented this 
		
//		tbinteractionPurpose = new TextBox();
//		tbinteractionOutcome = new TextBox();
		
		tbinteractionPurpose = new TextArea();
		tbinteractionOutcome = new TextArea();
		
		tbCreatedBy = new TextBox();
		tbCreatedBy.setEnabled(false);
		tbCreatedBy.setValue(UserConfiguration.getUserconfig().getUser().getEmployeeName().trim());
		
		tbTime = new TextBox();
		
		tbInteractionDueTime=new TextBox();
	}
	
	
	/************************************ End*********************************************/

	@Override
	public void createScreen() {
		initalizeWidget();
		// Token to initialize the processlevel menus.
		this.processlevelBarNames = new String[] { "New", "Next Action","Close" };

		// ////////////////////////// Form Field
		// Initialization////////////////////////////////////////////////////
		// Token to initialize formfield
		FormFieldBuilder fbuilder;

		fbuilder = new FormFieldBuilder();
		FormField fgroupingCustomerInteraction = fbuilder
				.setlabel("Interaction").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("ID", ibinteractionId);
		FormField fibinteractionid = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Title", tbinteractionTitle);
		FormField ftbinteractionTitle = fbuilder.setMandatory(true)
				.setMandatoryMsg("Title is mandatory!").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Creation Date",
				dbinteractionCreationDate);
		FormField fdbinteractionCreationDate = fbuilder.setMandatory(true)
				.setMandatoryMsg("Date is mandatory!").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Status", tbinteractionStatus);
		FormField ftbinteractionStatus = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Project", oblProject);
		FormField ftbinteractionProject = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Purpose", tbinteractionPurpose);
		FormField ftbinteractionPurpose = fbuilder.setMandatory(true)
				.setMandatoryMsg("Purpose is mandatory!").setRowSpan(0)
				.setColSpan(4).build();

		fbuilder = new FormFieldBuilder("* Outcome", tbinteractionOutcome);
		FormField ftbinteractionOutcome = fbuilder.setMandatory(true)
				.setMandatoryMsg("Outcome is mandatory!").setRowSpan(0)
				.setColSpan(4).build();

		fbuilder = new FormFieldBuilder("* Due Date", dbinteractionDueDate);
		FormField fdbinteractionDueDate = fbuilder.setMandatory(true)
				.setMandatoryMsg("Date is mandatory!").setRowSpan(0)
				.setColSpan(0).build();

		FormField fgroupingCustomerInformation = fbuilder
				.setlabel("Customer Information")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Document Type", oblDocumentName);
		FormField ftbinteractionDocument = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Document ID", ibinteractionDocumentId);
		FormField ftbinteractionDocumentId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Module Name", oblModuleName);
		FormField ftbinteractionModuleName = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Communication Type",
				oblCommunicationType);
		FormField ftbinteractionCommunicationType = fbuilder
				.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Communication Category",
				oblCommunicationCategory);
		FormField ftbinteractionCommunicationCategory = fbuilder
				.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Person Responsible",oblPersonResponsible);
		FormField ftbinteractionPersonResponsible = fbuilder.setMandatory(true)
				.setMandatoryMsg("Person Responsible is mandatory!")
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Branch", oblbranch);
		FormField ftbinteractionBranch = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Interaction Group", oblGroupType);
		FormField ftbinteractionGroup = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Interaction Priority", oblPriority);
		FormField ftbinteractionPriority = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(0).build();

		FormField fgroupingPersonInformation = fbuilder
				.setlabel("Person Information").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("* Business Partner Type", oblPersonType);
		FormField ftbinteractionPersonType = fbuilder.setMandatory(true)
				.setMandatoryMsg("Person Type is mandatory!").setRowSpan(0)
				.setColSpan(0).build();

		// FOR PERSON TYPE INFORMATION
		fbuilder = new FormFieldBuilder("", personInfoComposite);
		FormField fpersonInfoComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("", employeeInfoComposite);
		FormField femployeeInfoComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("", personInfoVendorComposite);
		FormField fvendorInfoComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("Created By", tbCreatedBy);
		FormField ftbCreatedBy = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Creation Time", tbTime);
		FormField ftbTime = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Due Time", tbInteractionDueTime);
		FormField ftbInteractionDueTime = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		FormField[][] formfield = {
				
				{ fgroupingPersonInformation },
				{ ftbinteractionPersonType },
				{ femployeeInfoComposite }, 
				{ fpersonInfoComposite },
				{ fvendorInfoComposite },
				{ fgroupingCustomerInteraction },
				{ fibinteractionid, ftbinteractionTitle,fdbinteractionCreationDate, ftbinteractionStatus },
				{ ftbinteractionProject,ftbinteractionModuleName,ftbinteractionDocument,ftbinteractionDocumentId},
				{ ftbinteractionGroup,ftbinteractionCommunicationCategory,ftbinteractionCommunicationType,ftbinteractionPriority},
				{ ftbinteractionBranch,ftbinteractionPersonResponsible,fdbinteractionDueDate,ftbInteractionDueTime },
				{ ftbinteractionPurpose},
				{ ftbinteractionOutcome},
				{ ftbTime,ftbCreatedBy},
				};
		this.fields = formfield;
	}

	@Override
	public void updateModel(InteractionType model) {

		if (tbinteractionTitle.getValue() != null)
			model.setInteractionTitle(tbinteractionTitle.getValue());
		if (dbinteractionCreationDate.getValue() != null)
			model.setinteractionCreationDate(dbinteractionCreationDate
					.getValue());
		if (dbinteractionDueDate.getValue() != null)
			model.setInteractionDueDate(dbinteractionDueDate.getValue());
		if (tbinteractionPurpose.getValue() != null)
			model.setInteractionPurpose(tbinteractionPurpose.getValue());
		if (tbinteractionOutcome.getValue() != null)
			model.setInteractionOutcome(tbinteractionOutcome.getValue());

		if (oblProject.getValue() != null)
			model.setInteractionProject(oblProject.getValue());

		if (oblModuleName.getValue() != null)
			model.setInteractionModuleName(oblModuleName.getValue());

		if (oblDocumentName.getValue() != null)
			model.setInteractionDocumentName(oblDocumentName.getValue());
		
		if(!ibinteractionDocumentId.getValue().equals("")){
			model.setInteractionDocumentId(Integer.parseInt(ibinteractionDocumentId.getValue()));
		}
		
		if (oblCommunicationType.getValue() != null)
			model.setInteractionCommunicationType(oblCommunicationType
					.getValue());
		if (oblCommunicationCategory.getValue() != null)
			model.setInteractionCommunicationCategory(oblCommunicationCategory
					.getValue());

		if (oblPersonResponsible.getValue() != null)
			model.setInteractionPersonResponsible(oblPersonResponsible
					.getValue());
		if (oblGroupType.getValue() != null)
			model.setInteractionGroup(oblGroupType.getValue());
		if (oblbranch.getValue() != null)
			model.setInteractionBranch(oblbranch.getValue());
		if (tbinteractionStatus.getValue() != null)
			model.setInteractionStatus(tbinteractionStatus.getValue());
		if (oblPriority.getValue() != null)
			model.setInteractionPriority(oblPriority.getValue());

		if(tbTime.getValue()!=null){
			model.setInteractionTime(tbTime.getValue());
		}
		
		if (oblPersonType.getValue() != null)
			model.setInteractionPersonType(oblPersonType.getValue());

		if (personInfoComposite.getValue() != null) {
			model.setPersonInfo(personInfoComposite.getValue());
		}
		try{
			if (employeeInfoComposite != null) {
				model.getEmployeeInfo().setEmpCount(
						employeeInfoComposite.getEmployeeId());
				model.getEmployeeInfo().setFullName(
						employeeInfoComposite.getEmployeeName());
				model.getEmployeeInfo().setCellNumber(
						employeeInfoComposite.getCellNumber());
				model.getEmployeeInfo()
						.setBranch(employeeInfoComposite.getBranch());
				model.getEmployeeInfo().setDepartment(
						employeeInfoComposite.getDepartment());
				model.getEmployeeInfo().setEmployeeType(
						employeeInfoComposite.getEmployeeType());
				model.getEmployeeInfo().setDesignation(
						employeeInfoComposite.getEmpDesignation());
				model.getEmployeeInfo().setEmployeerole(
						employeeInfoComposite.getEmpRole());
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		if (personInfoVendorComposite.getValue() != null) {
			model.setPersonVendorInfo(personInfoVendorComposite.getValue());
		}
		
		if(tbInteractionDueTime.getValue()!=null){
			model.setInteractionDueTime(tbInteractionDueTime.getValue());
		}

		reactOnEmail(model);
		interactionObj=model;
		presenter.setModel(model);
	}

	@Override
	public void updateView(InteractionType view) {
		
		interactionObj=view;
		
		ibinteractionId.setValue(view.getCount()+"");
		if (view.getInteractionTitle() != null)
			tbinteractionTitle.setValue(view.getInteractionTitle());
		if (view.getinteractionCreationDate() != null)
			dbinteractionCreationDate.setValue(view
					.getinteractionCreationDate());
		if (view.getInteractionStatus() != null)
			tbinteractionStatus.setValue(view.getInteractionStatus());

		if (view.getInteractionProject() != null)
			oblProject.setValue(view.getInteractionProject());
		if (view.getInteractionPurpose() != null)
			tbinteractionPurpose.setValue(view.getInteractionPurpose());
		if (view.getInteractionOutcome() != null)
			tbinteractionOutcome.setValue(view.getInteractionOutcome());
		if (view.getInteractionDueDate() != null)
			dbinteractionDueDate.setValue(view.getInteractionDueDate());

		if (view.getInteractionModuleName() != null){
			AppMemory.setList(view.getInteractionModuleName(), oblModuleName);
			oblModuleName.setValue(view.getInteractionModuleName());
		}
		if (view.getInteractionCommunicationCategory() != null)
			oblCommunicationCategory.setValue(view
					.getInteractionCommunicationCategory());
		if (view.getInteractionCommunicationType() != null)
			oblCommunicationType.setValue(view
					.getInteractionCommunicationType());
		if (view.getInteractionDocumentName() != null)
			oblDocumentName.setValue(view.getInteractionDocumentName());
		ibinteractionDocumentId.setValue(view.getInteractionDocumentId()+"");

		if (view.getInteractionPersonResponsible() != null)
			oblPersonResponsible.setValue(view
					.getInteractionPersonResponsible());
		if (view.getInteractionBranch() != null)
			oblbranch.setValue(view.getInteractionBranch());
		if (view.getInteractionGroup() != null)
			oblGroupType.setValue(view.getInteractionGroup());

		if (view.getInteractionPriority() != null)
			oblPriority.setValue(view.getInteractionPriority());

		if (view.getInteractionPersonType() != null)
			oblPersonType.setValue(view.getInteractionPersonType());

		if (view.getPersonInfo() != null)
			personInfoComposite.setValue(view.getPersonInfo());
		if (employeeInfoComposite != null) {
			employeeInfoComposite.setEmployeeId(view.getEmployeeInfo()
					.getEmpCount());
			employeeInfoComposite.setEmployeeName(view.getEmployeeInfo()
					.getFullName());
			employeeInfoComposite.setCellNumber(view.getEmployeeInfo()
					.getCellNumber());
			employeeInfoComposite.setBranch(view.getEmployeeInfo().getBranch());
			employeeInfoComposite.setDepartment(view.getEmployeeInfo()
					.getDepartment());
			employeeInfoComposite.setEmployeeType(view.getEmployeeInfo()
					.getEmployeeType());
			employeeInfoComposite.setEmpDesignation(view.getEmployeeInfo()
					.getDesignation());
			employeeInfoComposite.setEmpRole(view.getEmployeeInfo()
					.getEmployeerole());

		}
		if (view.getCreatedBy() != null)
			tbCreatedBy.setValue(view.getCreatedBy());

		if (view.getPersonVendorInfo() != null)
			personInfoVendorComposite.setValue(view.getPersonVendorInfo());
		
		if(view.getInteractionTime()!=null){
			tbTime.setValue(view.getInteractionTime());
		}
		
		if(view.getInteractionDueTime()!=null){
			tbInteractionDueTime.setValue(view.getInteractionDueTime());
		}
		
		setVisibleComposite();

		presenter.setModel(view);

	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")
						|| text.equals("Search")|| text.equals(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);

			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")|| text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")
						|| text.equals("Search")|| text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.INTERACTION,LoginPresenter.currentModule.trim());
	}


	// Object List Of Employee(Person Responsible),Branch and Project

	protected void initializePersonResponsible() {
		MyQuerry qurr = new MyQuerry(new Vector<Filter>(), new Employee());
		oblPersonResponsible.MakeLive(qurr);
	}

	protected void initiliazebranch() {
		MyQuerry querry = new MyQuerry(new Vector<Filter>(), new Branch());
		oblbranch.MakeLive(querry);
	}

	private void initializeProject() {
		MyQuerry quer = new MyQuerry(new Vector<Filter>(), new HrProject());
		oblProject.MakeLive(quer);
	}

	/************************** this method make field disabled at edit state or new state **************************/
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbinteractionStatus.setEnabled(false);
		dbinteractionCreationDate.setEnabled(false);
		ibinteractionId.setEnabled(false);
		tbTime.setEnabled(state);
	}

	/*********************** ON PERSON TYPE CHANGE ******************************/
	@Override
	public void onChange(ChangeEvent event) {

		if (event.getSource() == oblPersonType) {

			if (oblPersonType.getSelectedIndex() == 0) {
				clearComposit();
				employeeInfoComposite.setVisible(false);
				personInfoComposite.setVisible(false);
				personInfoVendorComposite.setVisible(false);

				personInfoComposite.setEnabled(false);
				employeeInfoComposite.setEnable(false);
				personInfoVendorComposite.setEnabled(false);
			}
			if (oblPersonType.getValue().equals("Customer")) {
				clearComposit();
				employeeInfoComposite.setVisible(false);
				personInfoComposite.setVisible(true);
				personInfoVendorComposite.setVisible(false);

				personInfoComposite.setEnabled(true);
				employeeInfoComposite.setEnable(false);
				personInfoVendorComposite.setEnabled(false);

			}
			if (oblPersonType.getValue() == "Employee"
					|| oblPersonType.getValue().equals("Employee")) {
				clearComposit();
				employeeInfoComposite.setVisible(true);
				personInfoComposite.setVisible(false);
				personInfoVendorComposite.setVisible(false);

				personInfoComposite.setEnabled(false);
				employeeInfoComposite.setEnable(true);
				personInfoVendorComposite.setEnabled(false);

			}
			if (oblPersonType.getValue() == "Vendor"
					|| oblPersonType.getValue().equalsIgnoreCase("Vendor")) {
				clearComposit();
				employeeInfoComposite.setVisible(false);
				personInfoComposite.setVisible(false);
				personInfoVendorComposite.setVisible(true);

				personInfoComposite.setEnabled(false);
				employeeInfoComposite.setEnable(false);
				personInfoVendorComposite.setEnabled(true);
			}
		}
		
		if(event.getSource().equals(oblModuleName))
		{
				if(oblModuleName.getSelectedIndex()!=0){
					ConfigCategory cat=oblModuleName.getSelectedItem();
					if(cat!=null){
						AppUtility.makeLiveTypeDropDown(oblDocumentName, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
					}
				}
		}

	}

	/*********************** ON PERSON TYPE CHANGE ******************************/

	public void setVisibleComposite() {
		if (personInfoComposite.getValue() != null)
			personInfoComposite.setVisible(true);
		if (employeeInfoComposite.getValue() != null)
			employeeInfoComposite.setVisible(true);
		if (personInfoVendorComposite.getValue() != null)
			personInfoVendorComposite.setVisible(true);
	}

	public void setVisiblePersonType() {
		if (oblPersonType.getValue().equalsIgnoreCase("Customer"))
			personInfoComposite.setVisible(true);
		if (oblPersonType.getValue().equalsIgnoreCase("Employee"))
			employeeInfoComposite.setVisible(true);
		if (oblPersonType.getValue().equalsIgnoreCase("Vendor"))
			personInfoVendorComposite.setVisible(true);
	}

	public void clearComposit() {
		if (personInfoComposite.getValue() != null) {
			personInfoComposite.getId().setValue(null);
			personInfoComposite.getName().setValue(null);
			personInfoComposite.getPhone().setValue(null);
		}
		if (personInfoVendorComposite.getValue() != null) {
			personInfoVendorComposite.getId().setValue(null);
			personInfoVendorComposite.getName().setValue(null);
			personInfoVendorComposite.getPhone().setValue(null);
		}
		if (employeeInfoComposite.getValue() != null) {
			employeeInfoComposite.getId().setValue(null);
			employeeInfoComposite.getName().setValue(null);
			employeeInfoComposite.getPhone().setValue(null);
			employeeInfoComposite.setBranch("");
			employeeInfoComposite.setDepartment(null);
			;
			employeeInfoComposite.setEmployeeType(null);
			employeeInfoComposite.setEmpDesignation(null);
			employeeInfoComposite.setEmpRole(null);
		}
	}

	@Override
	public void setCount(int count) {
		ibinteractionId.setValue(count+"");
	}
	
	/*******************************************************************************************************************/

	@Override
	public void setToNewState() {
		tbinteractionStatus.setValue(InteractionType.INTERACTIONCREATED);
		this.dbinteractionCreationDate.setValue(creatndate);
		//toggleProcessLevelMenu();
		setMenusAsPerStatus();

	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		//toggleProcessLevelMenu();
		setMenusAsPerStatus();
		
		SuperModel model=new InteractionType();
		model=interactionObj;
		if(HistoryPopup.historyObj!=null){
			AppUtility.addDocumentToHistoryTable(HistoryPopup.historyObj.getModuleName(),AppConstants.INTERACTIONTODO, interactionObj.getCount(), Integer.parseInt(personInfoComposite.getId().getValue()),personInfoComposite.getName().getValue(),Long.parseLong(personInfoComposite.getPhone().getValue()), false, model, null);
			HistoryPopup.historyObj=null;
		}else{
		AppUtility.addDocumentToHistoryTable(LoginPresenter.currentModule.trim(),AppConstants.INTERACTIONTODO, interactionObj.getCount(), Integer.parseInt(personInfoComposite.getId().getValue()),personInfoComposite.getName().getValue(),Long.parseLong(personInfoComposite.getPhone().getValue()), false, model, null);
		}
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		//toggleProcessLevelMenu();
		//setMenusAsPerStatus();
		this.processLevelBar.setVisibleFalse(false);
	}
/***************************************PROCESS LEVEL MENU VALIDATION********************************************************/
	
	
	
	public void setMenusAsPerStatus()
	{
		this.toggleProcessLevelMenu();
		this.toggleHeaderBarAsPerStatus();
	}
	
	
	public void toggleProcessLevelMenu() {
		InteractionType entity = (InteractionType) presenter.getModel();
		String status = entity.getInteractionStatus();

		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();
			
			if (status.equals(InteractionType.INTERACTIONCLOSED)) {
				if (text.equals("New")) {
					label.setVisible(true);
				}
				if (text.equals("Next Action")) {
					label.setVisible(false);
				}
				if (text.equals("Close")) {
					label.setVisible(false);
				}
			}
			
			if (status.equals(InteractionType.INTERACTIONCREATED)) {
				if (text.equals("New")) {
					label.setVisible(true);
				}
				if (text.equals("Next Action")) {
					label.setVisible(true);
				}
				if (text.equals("Close")) {
					label.setVisible(true);
				}
			}
		}
	}

	public void toggleHeaderBarAsPerStatus()
	{
		InteractionType entity = (InteractionType) presenter.getModel();
		String status = entity.getInteractionStatus();
		
		if(status.equals(InteractionType.INTERACTIONCLOSED)||status.equals(InteractionType.INTERACTIONCLOSED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true);
				}
				else
				{
					menus[k].setVisible(false);
				}
			}
		}
	}

	/*public void toggleProcessLevelMenu(String status) {
		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();
			System.out.println("Under loop" + status);
			
			if (status.equals(InteractionType.INTERACTIONCLOSED)) {
				if (text.equals("New")) {
					label.setVisible(true);
				}
				if (text.equals("Next Action")) {
					label.setVisible(false);
				}
				if (text.equals("Close")) {
					label.setVisible(false);
				}
			}
			
			if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
				InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
				for (int k = 0; k < menus.length; k++) {
					String text1 = menus[k].getText();
					if (text1.equals("Discard"))
						menus[k].setVisible(true);

					else
						menus[k].setVisible(false);
				}
			}
			
			
		}
	}*/
	
	/******************************************Validation Method(Overridden Method)*******************************************/
	
	@Override
	public boolean validate() {
		boolean b = super.validate();
		boolean dateValidation = false;
		
		boolean PurposeLimit=true,outComeLimit=true ;
		
		if (b == false)
			return false;

		Date d1 = dbinteractionCreationDate.getValue();
		Date d2 = dbinteractionDueDate.getValue();
		if (d1 != null && d2 != null && d1.before(d2)||d1.equals(d2)) {
			dateValidation = true;
		}
		if (d1.after(d2)) {
			this.showDialogMessage("Due Date Must be Greater Than Or Equal To Creation Date.");
			dateValidation = false;
		}

		if (oblPersonType.getValue().equalsIgnoreCase("Employee")) {
			if (employeeInfoComposite.getValue() == null) {
				this.showDialogMessage("Add Employee Details");
				return false;
			}
		}

		if (oblPersonType.getValue().equalsIgnoreCase("Vendor")) {
			if (personInfoVendorComposite.getValue() == null) {
				this.showDialogMessage("Add Vendor Details");
				return false;
			}
		}

		if (oblPersonType.getValue().equalsIgnoreCase("Customer")) {
			if (personInfoComposite.getValue()==null) {
				this.showDialogMessage("Add Person Details");
				return false;
			}
		}
		
		//  rohan added validation for 500 
		int commentsLength = tbinteractionPurpose.getValue().length();
		if(commentsLength>=500)
		{
			PurposeLimit=false;
			showDialogMessage("Purpose Description exceeds 500 characters.");
		}
		
		int outComeLength = tbinteractionOutcome.getValue().length();
		if(outComeLength>=500)
		{
			outComeLimit=false;
			showDialogMessage("OutCome Description exceeds 500 characters.");
		}
		
		
		return b && dateValidation && PurposeLimit && outComeLimit ;
	}

	
	/*****************************************Set One To Many Combo*********************************************/
	/**
	 * This method is for retrieving document type names on select of module name
	 */
	 public void setOneToManyCombo()
	   {
		  CategoryTypeFactory.initiateOneManyFunctionality(this.oblModuleName,this.oblDocumentName);
		 AppUtility.makeTypeListBoxLive(this.oblDocumentName,Screen.MODULENAME);
		
	   } 

	
	
/***************************************PROCESS LEVEL MENU VALIDATION END**************************************************/	

	 
	 public void reactOnEmail(InteractionType model) {
		 System.out.println("inside email method !!!");
				emailService.initiateInteractionEmail((InteractionType) model,new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Resource Quota Ended ");
						caught.printStackTrace();
					}

					@Override
					public void onSuccess(Void result) {
						System.out.println("Email Sent Sucessfully !");
					}
				});
		}
	 
	 
	 
	 
/*******************************************FORM FIEALD GETTER SETTER******************************************************/

	public ObjectListBox<Config> getOblPersonType() {
		return oblPersonType;
	}

	public void setOblPersonType(ObjectListBox<Config> oblPersonType) {
		this.oblPersonType = oblPersonType;
	}

	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	public PersonInfoComposite getPersonInfoVendorComposite() {
		return personInfoVendorComposite;
	}

	public void setPersonInfoVendorComposite(
			PersonInfoComposite personInfoVendorComposite) {
		this.personInfoVendorComposite = personInfoVendorComposite;
	}

	

	public TextBox getTbinteractionTitle() {
		return tbinteractionTitle;
	}

	public void setTbinteractionTitle(TextBox tbinteractionTitle) {
		this.tbinteractionTitle = tbinteractionTitle;
	}

	public TextBox getTbinteractionbranch() {
		return tbinteractionbranch;
	}

	public void setTbinteractionbranch(TextBox tbinteractionbranch) {
		this.tbinteractionbranch = tbinteractionbranch;
	}

	public ObjectListBox<Type> getOblDocumentName() {
		return oblDocumentName;
	}

	public void setOblDocumentName(ObjectListBox<Type> oblDocumentName) {
		this.oblDocumentName = oblDocumentName;
	}

	public ObjectListBox<ConfigCategory> getOblModuleName() {
		return oblModuleName;
	}

	public void setOblModuleName(ObjectListBox<ConfigCategory> oblModuleName) {
		this.oblModuleName = oblModuleName;
	}

	public ObjectListBox<Config> getOblCommunicationCategory() {
		return oblCommunicationCategory;
	}

	public void setOblCommunicationCategory(
			ObjectListBox<Config> oblCommunicationCategory) {
		this.oblCommunicationCategory = oblCommunicationCategory;
	}

	public ObjectListBox<Config> getOblCommunicationType() {
		return oblCommunicationType;
	}

	public void setOblCommunicationType(ObjectListBox<Config> oblCommunicationType) {
		this.oblCommunicationType = oblCommunicationType;
	}

	public ObjectListBox<Config> getOblGroupType() {
		return oblGroupType;
	}

	public void setOblGroupType(ObjectListBox<Config> oblGroupType) {
		this.oblGroupType = oblGroupType;
	}

	public EmployeeInfoComposite getEmployeeInfoComposite() {
		return employeeInfoComposite;
	}

	public void setEmployeeInfoComposite(EmployeeInfoComposite employeeInfoComposite) {
		this.employeeInfoComposite = employeeInfoComposite;
	}

	public ObjectListBox<Employee> getOblPersonResponsible() {
		return oblPersonResponsible;
	}

	public void setOblPersonResponsible(ObjectListBox<Employee> oblPersonResponsible) {
		this.oblPersonResponsible = oblPersonResponsible;
	}

	public ObjectListBox<Branch> getOblbranch() {
		return oblbranch;
	}

	public void setOblbranch(ObjectListBox<Branch> oblbranch) {
		this.oblbranch = oblbranch;
	}

	public ObjectListBox<HrProject> getOblProject() {
		return oblProject;
	}

	public void setOblProject(ObjectListBox<HrProject> oblProject) {
		this.oblProject = oblProject;
	}

	public ObjectListBox<Config> getOblPriority() {
		return oblPriority;
	}

	public void setOblPriority(ObjectListBox<Config> oblPriority) {
		this.oblPriority = oblPriority;
	}

	public TextBox getTbinteractionStatus() {
		return tbinteractionStatus;
	}

	public void setTbinteractionStatus(TextBox tbinteractionStatus) {
		this.tbinteractionStatus = tbinteractionStatus;
	}

	public Date getCreatndate() {
		return creatndate;
	}

	public void setCreatndate(Date creatndate) {
		this.creatndate = creatndate;
	}

	public TextBox getIbinteractionId() {
		return ibinteractionId;
	}

	public void setIbinteractionId(TextBox ibinteractionId) {
		this.ibinteractionId = ibinteractionId;
	}

	public TextBox getIbinteractionDocumentId() {
		return ibinteractionDocumentId;
	}

	public void setIbinteractionDocumentId(TextBox ibinteractionDocumentId) {
		this.ibinteractionDocumentId = ibinteractionDocumentId;
	}

	public DateBox getDbinteractionCreationDate() {
		return dbinteractionCreationDate;
	}

	public void setDbinteractionCreationDate(DateBox dbinteractionCreationDate) {
		this.dbinteractionCreationDate = dbinteractionCreationDate;
	}

	public DateBox getDbinteractionDueDate() {
		return dbinteractionDueDate;
	}

	public void setDbinteractionDueDate(DateBox dbinteractionDueDate) {
		this.dbinteractionDueDate = dbinteractionDueDate;
	}

	public TextArea getTbinteractionPurpose() {
		return tbinteractionPurpose;
	}

	public void setTbinteractionPurpose(TextArea tbinteractionPurpose) {
		this.tbinteractionPurpose = tbinteractionPurpose;
	}

	public TextArea getTbinteractionOutcome() {
		return tbinteractionOutcome;
	}

	public void setTbinteractionOutcome(TextArea tbinteractionOutcome) {
		this.tbinteractionOutcome = tbinteractionOutcome;
	}

	
	
/*******************************************FORM FIEALD GETTER SETTER END***********************************************/

}
