package com.slicktechnologies.client.views.interaction.interactiondetails;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentForm;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenter;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class InteractionPresenter extends FormScreenPresenter<InteractionType> {

	public static InteractionForm form;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	CsvServiceAsync csvservice = GWT.create(CsvService.class);

	public InteractionPresenter(FormScreen<InteractionType> view,
			InteractionType model) {
		super(view, model);
		form = (InteractionForm) view;
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.INTERACTION,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals("New")) {
			form.setToNewState();
			initalize();
		}
		if (text.equals("Next Action")) {
			reactOnNewAction();
		}
		if (text.equals("Close")) {
			reactOnCloseStatus();
		}
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		model = new InteractionType();
	}

	@Override
	public void reactOnDownload() {
		ArrayList<InteractionType> interactionarray = new ArrayList<InteractionType>();
		List<InteractionType> list = (List<InteractionType>) form
				.getSearchpopupscreen().getSupertable().getDataprovider()
				.getList();

		interactionarray.addAll(list);

		csvservice.setInteractionDetails(interactionarray,
				new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed" + caught);

					}

					@Override
					public void onSuccess(Void result) {

						String gwt = com.google.gwt.core.client.GWT
								.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 64;
						Window.open(url, "test", "enabled");

					}
				});
	}


	public static InteractionForm initalize() {

		AppMemory.getAppMemory().currentState = ScreeenState.NEW;
		InteractionForm form = new InteractionForm();

		InteractionPresenterTableProxy gentable = new InteractionPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();

		InteractionPresenterSearchProxy.staticSuperTable = gentable;

		InteractionPresenterSearchProxy searchpopup = new InteractionPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		InteractionPresenter presenter = new InteractionPresenter(form,
				new InteractionType());
		AppMemory.getAppMemory().stickPnel(form);

		return form;

	}

	public void reactOnCloseStatus() {
		if (form.tbinteractionStatus.getValue().equals(InteractionType.INTERACTIONCREATED)) {
			boolean conf = com.google.gwt.user.client.Window.confirm("Do You Want to Close This Interaction");
			if (conf == true) {
				this.changeStatusToClosed();
			}
		} 
	}

	public void reactOnNewAction() {
		if (form.tbinteractionStatus.getValue().equals(InteractionType.INTERACTIONCREATED)) {
			boolean conf = com.google.gwt.user.client.Window
					.confirm("Do You Want to Close Prevoius Interaction and Continue with New.");
			if (conf == true) {
				changeStatusNextAction();
				System.out.println("One Interaction");
				final InteractionForm form=InteractionPresenter.initalize();
				form.showWaitSymbol();
				Timer t = new Timer() {
					@Override
					public void run() {
						form.setToNewState();
				    	form.hideWaitSymbol();
						form.oblProject.setValue(model.getInteractionProject());
						form.oblModuleName.setValue(model.getInteractionModuleName());
						form.oblDocumentName.setValue(model.getInteractionDocumentName());
						form.oblCommunicationType.setValue(model.getInteractionCommunicationType());
						form.oblCommunicationCategory.setValue(model.getInteractionCommunicationCategory());
						form.oblPersonResponsible.setValue(model.getInteractionPersonResponsible());
						form.oblGroupType.setValue(model.getInteractionGroup());
						form.oblbranch.setValue(model.getInteractionBranch());
						form.oblPersonType.setValue(model.getInteractionPersonType());
						form.personInfoComposite.setValue(model.getPersonInfo());
						form.employeeInfoComposite.setValue(model.getEmployeeInfo());
						form.personInfoVendorComposite.setValue(model.getPersonVendorInfo());
					}
				};
				t.schedule(3000);
				
			}
		} 
	}
	
	/**************************************************************************************************************/

	public void changeStatusToClosed() {
		model.setInteractionStatus(InteractionType.INTERACTIONCLOSED);
		async.save(model, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onSuccess(ReturnFromServer result) {
				form.tbinteractionStatus.setValue(InteractionType.INTERACTIONCLOSED);
				model.setInteractionStatus(InteractionType.INTERACTIONCLOSED);
				form.setToViewState();
				//String status = form.tbinteractionStatus.getValue();
				//form.toggleProcessLevelMenu(status);

			}

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An unexpected error occured!");
			}
		});

	}
	
	/****************************************************************************************************************/

	public void changeStatusNextAction() {
		model.setInteractionStatus(InteractionType.INTERACTIONCLOSED);
		async.save(model, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onSuccess(ReturnFromServer result) {
				form.getTbinteractionStatus().setValue(model.getInteractionStatus());
			}

			@Override
			public void onFailure(Throwable caught) {
				
				form.showDialogMessage("An unexpected error occured!");
			}
		});

	}

}
