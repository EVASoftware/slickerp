package com.slicktechnologies.client.views.quotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.*;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.*;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.*;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.ContractService;
import com.slicktechnologies.client.views.contract.ContractServiceAsync;
import com.slicktechnologies.client.views.contract.PaymentTermsTable;
import com.slicktechnologies.client.views.contract.ProductChargesTable;
import com.slicktechnologies.client.views.contract.ProductTaxesTable;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.salesquotation.TermsAndConditionTable;
import com.slicktechnologies.client.views.scheduleservice.ServiceSchedulePopUp;
import com.slicktechnologies.client.views.scheduleservice.ServiceScheduleTable;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.coststructure.ManPowerCostStructure;
import com.slicktechnologies.shared.common.coststructure.MaterialCostStructure;
import com.slicktechnologies.shared.common.coststructure.OtherCost;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.*;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;


// TODO: Auto-generated Javadoc
/**
 * Represents a Quotation.
 */
public class QuotationForm extends ApprovableFormScreen<Quotation> implements ClickHandler, ChangeHandler, ValueChangeHandler<Date> {

	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	//Token to add the varialble declarations
	/** The db valid untill. */
	DateBox dbValidUntill;
	
	/** The olbb branch. */
	ObjectListBox<Branch> olbbBranch;
	
	/** The olbc payment terms. */
	ObjectListBox<Config> olbcPriority,olbcPaymentMethods;
	
	/** The tb quotation status. */
	TextBox tbRefeRRedBy,tbQuotationStatus;
	TextBox tbLeadId,tbContractId;
	
	/** The tb quotatin id. */
	TextBox tbQuotatinId;
	
	/** The ta description. */
	TextArea taDescription;
	
	/** The uc upload t and cs. */
	UploadComposite ucUploadTAndCs;
	
	UploadComposite customQuotationUpload;
	
	CheckBox customeQuotation;
	
	TextBox tbrefNum;
	DateBox dbrefDate;
	
	
	/** The person info composite. */
	PersonInfoComposite personInfoComposite;
	
	/** The olb approver name. */
	ObjectListBox<Employee> olbeSalesPerson,olbApproverName;
	
	/** The saleslineitemquotationtable. */
	SalesLineItemQuotationTable saleslineitemquotationtable;
	
	/** The olb quotation group. */
	ObjectListBox<Config>olbQuotationGroup;
	
	/** The olb quotation type. */
	ObjectListBox<Type> olbQuotationType;
	
	/** The olb quotation category. */
	ObjectListBox<ConfigCategory> olbQuotationCategory;
	

	DoubleBox dototalamt,doamtincltax,donetpayamt;
	DateBox dbQuotationDate;
	IntegerBox ibdays;
	DoubleBox dopercent;
	TextBox tbcomment;
	Button addPaymentTerms;
	PaymentTermsTable paymentTermsTable;
	ProductChargesTable chargesTable;
	Button addOtherCharges;
	ProductTaxesTable prodTaxTable;
	ProductInfoComposite prodInfoComposite;
	Button baddproducts;
	TextBox tbremark;
	IntegerBox ibCreditPeriod;
	FormField fgroupingCustomerInformation;
	
	
	TextBox tbTicketNumber;
	
	/**
	 * rohan added this flag for checking process configuration
	 */
	
		boolean salesPersonRestrictionFlag= false;
	
	/**
	 * ends here 
	 */
	
	
	/**
	 * Rohan added created By field for NBHC to know who is going to create/ generate interaction
	 * date : 03/11/2016
	 */
		TextBox tbCreatedBy;
	
	/**
	 * ends here 
	 */
	
	
	
	//  rohan added this 
	public static int productSrNo=0;
	//************rohan added this for om pest control ************
	
	TextBox premisesDesc;
	TextBox inspectedBy;
	DateBox inspectionDate;
//	TextBox employeeDesignation;
	TextArea employeeDesignation;
	
	//***********added for pestle pest control *****************
	Button additionalDetails;
	Quotation quotationObject;
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	
	/**
	 *   Variable Declaration For Security Deposit Pop Up
	 */
	
	Button btnsecurityDep;
	int f_docid;
	Date f_docDate;
	String f_docStatus;
	Double f_securityDeposit=null;
	Date f_reDate;
	boolean f_depositStatus;
	String f_payMethod;
	String f_accountNo;
	String f_bankName;
	String f_branch;
	String f_paybleAt;
	String f_fouring;
	String f_checkName;
	int f_checkNo=0;
	Date f_checkDate;
	String f_serviceNo="";
	String f_ifscCode;
	String f_micrCode;
	String f_swiftCode;
	String f_instruction;
	Address f_address;
	
	
	/**
	 * Older version developed by vaishnavi    
	 * I have made this method global so that it can be reusable   
	 * This variable are used for calculating payment terms Automatically
	 * I/p : 1.MaxDuration (Calculate max duration in all the product present in the table) 
	 * Developed by : Rohan Bhagde.
	 */
		ObjectListBox<ConfigCategory> olbPaymentTerms;
		Button addPayTerms;
		CheckBox cbStartOfPeriod;
		NumberFormat nf = NumberFormat.getFormat("0.00");
		
	/**
	 * Ends here 
	 */
	
	
	/**
	 *   Service Scheduling Fields
	 */
	
	public ArrayList<CustomerBranchDetails> customerbranchlist=new ArrayList<CustomerBranchDetails>();
	public boolean servicesFlag=false;
	
	String f_serviceDay,f_serviceTime; 
	Button f_btnservice;
	Date f_conStartDate;
	Date f_conEndDate;
	int f_duration;
	ServiceScheduleTable serviceScheduleTable;
	ServiceSchedule scheduleEntity;
	ServiceSchedulePopUp servicepop=new ServiceSchedulePopUp("Quotation Id", "Quotation Date");
	
	/**
	 * Date : 19-05-2017 By ANIL
	 */
	Button btnCosting;
	
	public List<MaterialCostStructure> matCostList;
	public List<ManPowerCostStructure> manPowCostList;
	public List<OtherCost> otherCostList;
	
	/**
	 * End
	 */
	/*
	 * 
	 * Nidhi
	 *  20-06-2017
	 *  
	 */
	static DateTimeFormat format = DateTimeFormat.getFormat("c"); 
	/**
	 * End
	 */
	
	
	/**
	 * Date 1-07-2017 added by vijay for Tax validation
	 */
	Date date = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss").parse("2017-07-01 00:00:00");
	final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
	/**
	 * ends here
	 */
	
	/***
	 * Date : 08-08-2017 By ANIL
	 * adding payment date 
	 */
	
	DateBox dbPaymentDate;
	/**
	 * End
	 */
	
	/** Date 01-09-2017 added by vijay for discount on total and round off **/
	DoubleBox dbDiscountAmt;
	DoubleBox dbFinalTotalAmt;
	TextBox tbroundOffAmt;
	DoubleBox dbGrandTotalAmt;
	
	/** date 11/10/2017 added by komal for followup date **/
	DateBox dbFollowUpDate;
	
	/******************************************End Of Declaration*********************************/

	/**
	 * Instantiates a new quotation form.
	 */
	/**
	 * nidhi
	 * 1-08-2018
	 * for status reason
	 */
	TextBox tbDocStatusreason ;
	/**
	 * Instantiates a new quotation form.
	 */
	/**
	 *  nidhi
	 *  10-10-2018
	 */
	Customer cust;
	/**
	 * nidhi 14-12-2018
	 */
	boolean validateFlag = false;
	int duration = -1;
	/**
	 * nidhi |*|
	 * for material cost for bom servicess
	 */
	public List<MaterialCostStructure> matSerivceCostList;
	public HashMap<String, List<MaterialCostStructure>> serviceBomMaterialList = new HashMap<String, List<MaterialCostStructure>>();
	public boolean bomValidationActive = false;
	/**
	 * Updated By: Viraj
	 * Date: 04-04-2019
	 * Description: To show consolidated data for checkbox
	 */
	public CheckBox cbConsolidatePrice;
	
		/**Date 29-8-2019 by Amol added a process configuration for hide the Payment method and quotation group**/
	boolean hidePaymentgroup=false;
	boolean hideQuotationgroup=false;
	boolean custCatandType=false;
	boolean quotationDateDisable=false;
	/**Date 23-10-2019 by Amol**/
	ObjectListBox<Config> olbProductGroup;
	ObjectListBox<ServiceProduct>olbProductName;
	boolean changeProductComposites=false;
	
	final static CommonServiceAsync commonservice = GWT.create(CommonService.class);
	Quotation quotationEntity = new Quotation();
	public static boolean addnewproductFlag = false;
	
	/**
	 * @author Vijay Date :- 19-07-2021
	 * Des :- added Number Range functionality for Payment Gateway functionality
	 */
	public ObjectListBox<Config> olbcNumberRange;
	
	boolean validUntilDateFlag = false;
	
	/**
	 * @author Vijay  Date :- 28-03-2022 
	 * Des :- When this checkbox is true then service address will not print in pdf
	 */
	CheckBox cbdonotprintserviceAddress;
	
	/**Sheetal:30-03-2022 , Adding Payment mode in payment terms tab on quotation**/
	ObjectListBox<CompanyPayment> objPaymentMode;
    ArrayList<CompanyPayment> paymentModeList = new ArrayList<CompanyPayment>();
    
    CheckBox chkdonotprintTotal;
    
    TermsAndConditionTable termsAndConditionTable;
	ObjectListBox<TermsAndConditions> olbtermsAndConditionTitle;
	Button btnTermsAndConditionAdd;
	CheckBox chkPrintAllTermsAndCondition;
	TextBox tbZohoQuotID;
	public  QuotationForm() {
		super();
		createGui();
		saleslineitemquotationtable.connectToLocal();
		paymentTermsTable.connectToLocal();
		chargesTable.connectToLocal();
		prodTaxTable.connectToLocal();
		//************************************************************************
//		processLevelBar.btnLabels[5].setVisible(true);
		
		/**
		 * Date : 12-05-2017 By ANil
		 */
		ContractPresenter.custcount=0;
		/**
		 * End
		 */
		
		/**
		 * Date : 22-05-2017 By ANIL
		 */
		matCostList=new ArrayList<MaterialCostStructure>();
		manPowCostList=new ArrayList<ManPowerCostStructure>();
		otherCostList=new ArrayList<OtherCost>();
		
		/**
		 * End
		 */
		
		/** Date 01-09-2017 added by vijay for discount amount change handler ***/
		dbDiscountAmt.addChangeHandler(this);
		tbroundOffAmt.addChangeHandler(this);
		
		System.out.println("inside 1 st costructor");
		productSrNo=0;
		/**
		 * nidhi |*|
		 */
		bomValidationActive = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","BillOfMaterialActive");
		
		Date date=new Date();
		dbQuotationDate.setValue(date);

		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", "MakeQuotationDateDisable")
				&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
			quotationDateDisable=true;
			this.dbQuotationDate.setEnabled(false);
		}
		
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
			salesPersonRestrictionFlag=true;
			olbeSalesPerson.setValue(LoginPresenter.loggedInUser);
			olbeSalesPerson.setEnabled(false);
		
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "MakeCategoryAndTypeDisable")
				 &&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
	   		 this.olbQuotationCategory.setEnabled(false);
	   		 this.olbQuotationType.setEnabled(false);
	   	  }
		
		
		/**
		 * nidhi 14-12-2018
		 */
		validateFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", "DisableQuotationValidateOption");
	
		String dur = AppUtility.getForProcessConfigurartionIsActiveOrNot("QuotationValidateDuration");
		
		if(dur!=null && dur.trim().length()>0 && !validateFlag){
			
			duration = Integer.parseInt(dur);
			
			Date dt = dbQuotationDate.getValue();
			if(dt!=null){
				CalendarUtil.addDaysToDate(dt, duration);
				dbValidUntill.setValue(dt);
			}
			
			
		}
		if(validateFlag){
			if(duration == -1){
				duration = 0;
			}
			dbValidUntill.setEnabled(false);
	
		}
		this.tbZohoQuotID.setEnabled(false);
	}

	/**
	 * Instantiates a new quotation form.
	 *
	 * @param processlevel the processlevel
	 * @param fields the fields
	 * @param formstyle the formstyle
	 */
	public QuotationForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		/**
		 * nidhi |*|
		 */
		bomValidationActive = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","BillOfMaterialActive");
	
		System.out.println("inside 2 st costructor");
	}


	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget()
	{	/**
		 * nidhi |*|
		 */
		bomValidationActive = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","BillOfMaterialActive");
	
		dbValidUntill=new DateBoxWithYearSelector();

		olbbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbBranch);
		olbbBranch.addChangeHandler(this);

		olbcPriority=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcPriority,Screen.QUOTATIONPRIORITY);

		olbcPaymentMethods=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcPaymentMethods,Screen.PAYMENTMETHODS);

		tbLeadId=new TextBox();

		tbQuotatinId=new TextBox();

		tbContractId=new TextBox();

		tbRefeRRedBy=new TextBox();

		tbQuotationStatus=new TextBox();

		taDescription=new TextArea();

		ucUploadTAndCs=new UploadComposite();
		saleslineitemquotationtable=new SalesLineItemQuotationTable();
		/**
		 * nidhi |*|
		 */
		saleslineitemquotationtable.form = this;
		customeQuotation = new CheckBox();
		customeQuotation.addClickHandler(this);
		customQuotationUpload = new UploadComposite();
		customQuotationUpload.setEnable(false);
		
		//************rohan added this for om pest control ************
		premisesDesc =new TextBox();
		inspectedBy = new TextBox();
		inspectionDate =  new DateBox();
		employeeDesignation = new TextArea();
		
		
		tbrefNum = new TextBox();
//		dbrefDate = new DateBox();
		/** Date 19-06-2020 added by vijay to set time mid of the day to date ***/
		dbrefDate = new DateBoxWithYearSelector();
		
		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
			
			salesPersonRestrictionFlag = true;
			olbeSalesPerson=new ObjectListBox<Employee>();
//			AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
			olbeSalesPerson.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.QUOTATION, "Sales Person");
			Timer t = new Timer() {
				
				@Override
				public void run() {

					makeSalesPersonEnable();
				}

				
			};t.schedule(3000);
		}
		else
		{
		olbeSalesPerson=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
		olbeSalesPerson.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.QUOTATION, "Sales Person");
		}
		olbQuotationCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbQuotationCategory, Screen.QUOTATIONCATEGORY);
		olbQuotationCategory.addChangeHandler(this);
		
		olbQuotationType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbQuotationType, Screen.QUOTATIONTYPE);
		
		olbQuotationGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbQuotationGroup, Screen.QUOTATIONGROUP);
		
		tbremark=new TextBox();
		tbremark.setEnabled(false);
		dbQuotationDate=new DateBoxWithYearSelector();
		
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApproverName,"Quotation");
//		setOneToManyCombo();
		
		prodInfoComposite=AppUtility.initiateServiceProductComposite(new SuperProduct());
		
		this.tbContractId.setEnabled(false);
		this.tbQuotationStatus.setEnabled(false);
		this.tbLeadId.setEnabled(false);
		this.tbQuotationStatus.setText(Contract.CREATED);
		this.tbQuotatinId.setEnabled(false);
		
		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		
		ibdays=new IntegerBox();
		ibdays.addChangeHandler(this);
		dopercent=new DoubleBox();
		tbcomment=new TextBox();
		addPaymentTerms=new Button("ADD");
		addPaymentTerms.addClickHandler(this);
		paymentTermsTable=new PaymentTermsTable();
		chargesTable=new ProductChargesTable();
		addOtherCharges=new Button("+");
		addOtherCharges.addClickHandler(this);
		prodTaxTable=new ProductTaxesTable();
		dototalamt=new DoubleBox();
		dototalamt.setEnabled(false);
		doamtincltax=new DoubleBox();
		doamtincltax.setEnabled(false);
		donetpayamt=new DoubleBox();
		donetpayamt.setEnabled(false);
		baddproducts=new Button("ADD");
		baddproducts.addClickHandler(this);
		
		ibCreditPeriod=new IntegerBox();
		
		btnsecurityDep = new Button("Security Deposit");
		this.btnsecurityDep.setVisible(false);
		
		validateSecurityDeposit();
		
		f_btnservice = new Button("Schedule");
		this.f_btnservice.setEnabled(true);
		serviceScheduleTable = new ServiceScheduleTable();
		scheduleEntity= new ServiceSchedule();
		
		

		tbTicketNumber=new TextBox();
		tbTicketNumber.setEnabled(false);
		
		
		additionalDetails=new Button("Additional Details");
		
		olbPaymentTerms = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbPaymentTerms,Screen.ADDPAYMENTTERMS);
		
		/**
		 * @author Anil @since  02-08-2021
		 */
		olbPaymentTerms.addChangeHandler(this);

		addPayTerms = new Button("ADD");
		addPayTerms.addClickHandler(this);

		cbStartOfPeriod = new CheckBox();
		cbStartOfPeriod.addClickHandler(this);
		cbStartOfPeriod.setValue(false);
		
		tbCreatedBy = new TextBox();
		tbCreatedBy.setEnabled(false);
		tbCreatedBy.setValue(UserConfiguration.getUserconfig().getUser().getEmployeeName().trim());		

		/**
		 * Date : 19-05-2017 By ANIL
		 */
		btnCosting=new Button("Costing");
		/**
		 * End
		 */
		
		/**
		 * Date : 08-08-2017 BY ANIL
		 */
		dbPaymentDate=new DateBoxWithYearSelector();
		/**
		 * End
		 */
		
		/** Date 01-09-2017 added by vijay for discount amt round off total amt***/
		dbDiscountAmt = new DoubleBox();
		dbFinalTotalAmt = new DoubleBox();
		dbFinalTotalAmt.setEnabled(false);
		tbroundOffAmt = new TextBox();		
		dbGrandTotalAmt = new DoubleBox();
		dbGrandTotalAmt.setEnabled(false);
		this.changeWidgets();
		
		/** date 11/10/2017 added by komal for followup date **/
//		dbFollowUpDate = new DateBox();
		dbFollowUpDate = new DateBoxWithYearSelector();
		
		/**
		 * nidhi
		 * 1-08-2018
		 * for status reason
		 */
		tbDocStatusreason = new TextBox();
		tbDocStatusreason.setEnabled(false);
		/*
		 * end
		 */
//		/**
//		 * nidhi 14-12-2018
//		 */
//		validateFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", "DisableQuotationValidateOption");
//	
//		String dur = AppUtility.getForProcessConfigurartionIsActiveOrNot("QuotationValidateDuration");
//		
//		if(dur!=null && dur.trim().length()>0 && !validateFlag){
//			
//			duration = Integer.parseInt(dur);
//			
//			Date dt = dbQuotationDate.getValue();
//			if(dt!=null){
//				CalendarUtil.addDaysToDate(dt, duration);
//				dbValidUntill.setValue(dt);
//			}
//			
//			
//		}
//		if(validateFlag){
//			if(duration == -1){
//				duration = 0;
//			}
//			dbValidUntill.setEnabled(false);
//	
//		}
		dbQuotationDate.addValueChangeHandler(this);
		/**
		 * Updated By: Viraj
		 * Date: 04-04-2019
		 * Description: To show consolidated data for checkbox
		 */
		cbConsolidatePrice = new CheckBox();
		olbProductGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbProductGroup, Screen.PRODUCTGROUP);
		olbProductGroup.addChangeHandler(this);
		
		olbProductName=new ObjectListBox<ServiceProduct>();
		AppUtility.initializeServiceProduct(olbProductName);
		
		 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","ChangeProductComposite")){
				changeProductComposites=true;
			}
		 
		
		olbcNumberRange = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange, Screen.NUMBERRANGE); 
		

		cbdonotprintserviceAddress = new CheckBox();
		cbdonotprintserviceAddress.setValue(false);
		
		objPaymentMode = new ObjectListBox<CompanyPayment>();
		loadPaymentMode();
		
		chkdonotprintTotal = new CheckBox();
		
		olbtermsAndConditionTitle = new ObjectListBox<TermsAndConditions>();
		AppUtility.makeLiveTermsAndCondition(olbtermsAndConditionTitle, "Quotation");
		termsAndConditionTable = new TermsAndConditionTable();
		btnTermsAndConditionAdd = new Button("Add");
		btnTermsAndConditionAdd.setEnabled(false);
		btnTermsAndConditionAdd.addClickHandler(this);
		chkPrintAllTermsAndCondition = new CheckBox();
		chkPrintAllTermsAndCondition.setValue(true);
		chkPrintAllTermsAndCondition.addClickHandler(this);
		tbZohoQuotID = new TextBox();
	}

	private void loadPaymentMode() {

		try {
			 GenricServiceAsync async =  GWT.create(GenricService.class);

			MyQuerry querry = new MyQuerry();
			
			
			Company c = new Company();
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			
			
			querry.setQuerryObject(new CompanyPayment());

			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub

					System.out.println(" in side date load size"+result.size());
					Console.log("get result size of payment mode --" + result.size());
					objPaymentMode.clear();
					objPaymentMode.addItem("--SELECT--");
//					paymentModeList = new ArrayList<CompanyPayment>();
					if (result.size() == 0) {

					} else {
						for (SuperModel model : result) {
							CompanyPayment payList = (CompanyPayment) model;
							paymentModeList.add(payList);
						}
						
						if (paymentModeList.size() != 0) {
							objPaymentMode.setListItems(paymentModeList);
						}
						
						for(int i=1;i<objPaymentMode.getItemCount();i++){
							String paymentDet = objPaymentMode.getItemText(i);
							Console.log("SERVICE DETAILS : "+paymentDet);
						}	
					}
				
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	
		
	}

	/**
	 * rohan added this method for setting value to sales person and and make it non editable
	 */
	private void makeSalesPersonEnable() {
	
		olbeSalesPerson.setValue(LoginPresenter.loggedInUser);
		olbeSalesPerson.setEnabled(false);
	}
	
	/**
	 * method template to create screen formfields.
	 */
	@Override
	public void createScreen() {


		initalizeWidget();

		//Token to initialize the processlevel menus.
		
		/*
		 *  07-07-2017
		 *  nidhi
		 *  changes for revise quotation 
		 *  reise quotation button added 
		 */
		/**
		 * Date : 06-11-2017 BY ANIL
		 * added cancel button
		 */
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","PCAMBCustomization"))
//		{
//			this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CREATECONTRACT,AppConstants.Unsucessful,"Print CheckList WoodBorer","Print CheckList BedBugs","Print CheckList FlyMosquito","Print CheckList Rodent","Print CheckList Termite","Print CheckList Gipc",
//					AppConstants.NEW,"Mark Successful",AppConstants.CREATEQUICKCONTRACT,"Revised Quotation",ManageApprovals.SUBMIT,AppConstants.CANCEL};
//		}
//		else
//		{
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","OnlyForNBHC"))
//			{
//				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CREATECONTRACT,AppConstants.Unsucessful,
//						AppConstants.NEW,"Mark Successful","Print LOI","Revised Quotation",ManageApprovals.SUBMIT,AppConstants.CANCEL};
//			}
//			else
//			{
//				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CREATECONTRACT,AppConstants.Unsucessful,
//				AppConstants.NEW,"Mark Successful",AppConstants.CREATEQUICKCONTRACT,"Revised Quotation",ManageApprovals.SUBMIT,AppConstants.CANCEL};
//			}
//		    
//		}
		/**
		 * Updated By: Priyanka
		 * Date: 26-12-2020
		 * Description: Change Navigation flow as per suggestion of Nitin Sir. 
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","PCAMBCustomization"))
		{
				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CREATECONTRACT,AppConstants.Unsucessful,"Print CheckList WoodBorer","Print CheckList BedBugs","Print CheckList FlyMosquito","Print CheckList Rodent","Print CheckList Termite","Print CheckList Gipc",
					AppConstants.NEW,"Mark Successful",AppConstants.CREATEQUICKCONTRACT,"Revised Quotation",ManageApprovals.SUBMIT,AppConstants.CANCEL,AppConstants.CUSTOMERADDRESS,AppConstants.COPY};

		}
		else
		{
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","OnlyForNBHC"))
			{
				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CREATECONTRACT,AppConstants.Unsucessful,
						AppConstants.NEW,"Mark Successful","Print LOI","Revised Quotation",ManageApprovals.SUBMIT,AppConstants.CANCEL,AppConstants.CUSTOMERADDRESS,AppConstants.COPY};
			}
			else
			{
				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CREATECONTRACT,AppConstants.VIEWLEAD,AppConstants.Unsucessful,
				AppConstants.NEW,"Mark Successful",AppConstants.CREATEQUICKCONTRACT,"Revised Quotation",ManageApprovals.SUBMIT,AppConstants.CANCEL,AppConstants.CUSTOMERADDRESS,AppConstants.COPY};
			}
		    
		}
		
		

		
//		this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,
//		AppConstants.VIEWSERVICES,AppConstants.CANCELLATIONSUMMARY, AppConstants.NEW,AppConstants.CONTRACTRENEWAL,AppConstants.WORKORDER,AppConstants.CUSTOMERINTEREST,AppConstants.MATERIALREQUIREDREPORT,"Update Taxes"};
		
//===============,"Update Taxes"

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		String mainScreenLabel="Quotation Information";
		if(quotationObject!=null&&quotationObject.getCount()!=0){
			mainScreenLabel=quotationObject.getCount()+"/"+quotationObject.getStatus()+"/"+AppUtility.parseDate(quotationObject.getCreationDate());
		}
		
		//fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingCustomerInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build(); // added by priyanka. 
		//FormFieldBuilder fbuilder;
		//fbuilder = new FormFieldBuilder();
		//FormField fgroupingCustomerInformation=fbuilder.setlabel("Quotation Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		/** date 06.03.2018 added by komal for nbhc **/
		FormField ftbLeadId;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","makeLeadIdMandatory")
				&& !LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin")){
			fbuilder = new FormFieldBuilder("* Lead ID",tbLeadId);
			ftbLeadId= fbuilder.setMandatory(true).setMandatoryMsg("Lead Id is Mandatory").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Lead ID",tbLeadId);
			ftbLeadId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/**
		 * end komal
		 */

		fbuilder = new FormFieldBuilder("Quotation ID",tbQuotatinId);
		FormField ftbQuotatinId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Order ID",tbContractId);
		FormField ftbContractId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Valid Until",dbValidUntill);
		FormField fdbValidUntill= fbuilder.setMandatory(true).setMandatoryMsg("Valid Until is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Referred By",tbRefeRRedBy);
		FormField ftbRefeRRedBy= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDocuments=fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Upload Document",ucUploadTAndCs); //old label Upload T&Cs change done on 30-01-2025
		FormField fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder();//
		FormField fInspectiongroup=fbuilder.setlabel("Inspection").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fgroupingSalesInformation=fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		//FormField fgroupingSalesInformation=fbuilder.setlabel("Sales Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Quotation Status",tbQuotationStatus);
		FormField ftbQuotationStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Priority",olbcPriority);
		FormField folbcPriority= fbuilder.setMandatory(false).setMandatoryMsg("Priority is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Branch",olbbBranch);
		FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is mandatory!").setRowSpan(0).setColSpan(0).build();
		/**@Sheetal:01-02-2022
        Des : Making Sales person non mandatory,requirement by UDS Water**/
		fbuilder = new FormFieldBuilder("Sales Person",olbeSalesPerson);
		FormField folbeSalesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Payment Method",olbcPaymentMethods);
		FormField folbcPaymentMethods= fbuilder.setMandatory(false).setMandatoryMsg("Payment Methods is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDescription=fbuilder.setlabel("Description").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)",taDescription);
		FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingProducts=fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingGeneralInformation=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		/** date 06.03.2018 added by komal for nbhc **/
		FormField folbQuotationType;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","makeTypeMandatory")){
			fbuilder = new FormFieldBuilder("* Quotation Type",olbQuotationType);
			folbQuotationType= fbuilder.setMandatory(true).setMandatoryMsg("Quotation Type is mandatory!").setRowSpan(0).setColSpan(0).build();

		}else{
			fbuilder = new FormFieldBuilder("Quotation Type",olbQuotationType);
			folbQuotationType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		}
		/**
		 * end komal
		 */

		fbuilder = new FormFieldBuilder("Remark",tbremark);
		FormField ftbremark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/** date 06.03.2018 added by komal for nbhc **/
		FormField folbQuotationGroup;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","makeGroupMandatory")){
			fbuilder = new FormFieldBuilder("* Quotation Group",olbQuotationGroup);
			folbQuotationGroup= fbuilder.setMandatory(true).setMandatoryMsg("Quotation Group is mandatory!").setRowSpan(0).setColSpan(0).build();

		}else{
			fbuilder = new FormFieldBuilder("Quotation Group",olbQuotationGroup);
			folbQuotationGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		}
		/**
		 * end komal
		 */
		/** date 06.03.2018 added by komal for nbhc **/
		FormField folbQuotationCategory;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","makeCategoryMandatory")){
			fbuilder = new FormFieldBuilder("* Quotation Category",olbQuotationCategory);
			folbQuotationCategory= fbuilder.setMandatory(true).setMandatoryMsg("Quotation Category is mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Quotation Category",olbQuotationCategory);
			folbQuotationCategory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/**
		 * end komal
		 */

		fbuilder = new FormFieldBuilder("* Quotation Date",dbQuotationDate);
		FormField fdbQuotationDate= fbuilder.setMandatory(true).setMandatoryMsg("Quotation Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnsecurityDep);
		FormField fbtnsecurity= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",prodInfoComposite);
		FormField fprodInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("* Approver Name",olbApproverName);
		FormField folbApproverName= fbuilder.setMandatory(true).setMandatoryMsg("Approver Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPaymentTermsDetails=fbuilder.setlabel("Payment Terms").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Days",ibdays);
		FormField fibdays= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		fbuilder = new FormFieldBuilder("Percent",dopercent);
		FormField fdopercent= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		fbuilder = new FormFieldBuilder("Comment",tbcomment);
		FormField ftbcomment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		fbuilder = new FormFieldBuilder("",addPaymentTerms);
		FormField faddPaymentTerms= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		fbuilder = new FormFieldBuilder("",paymentTermsTable.getTable());
		FormField fpaymentTermsTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupthree=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupone=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",prodTaxTable.getTable());
		FormField fprodTaxTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("",saleslineitemquotationtable.getTable());
		FormField fsaleslineitemquotationtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		//fbuilder = new FormFieldBuilder("Total Amount",doAmtExcludingTax);
		//FormField fdoAmtExcludingTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Total Amount",doamtincltax);
		FormField fdoAmtIncludingTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Total Amount",dototalamt);
		FormField fpayCompTotalAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",addOtherCharges);
		FormField faddOtherCharges= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",chargesTable.getTable());
		FormField fchargesTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("",baddproducts);
		FormField fbaddproducts= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Net Payable",donetpayamt);
		FormField fpayCompNetPay= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("Custom Quotation",customeQuotation);
		FormField fcustomeQuotation= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Quotation",customQuotationUpload);
		FormField fcustomQuotationUpload= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Reference No",tbrefNum);
		FormField ftbrefNum = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Reference Date",dbrefDate);
		FormField fdbrefDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Credit Period (Days)",ibCreditPeriod);
		FormField fibCreditPeriod= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",f_btnservice);
		FormField fbtnservice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Ticket Number",tbTicketNumber);
		FormField ftbTicketNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		//***************rohan added this for om pest control and made mandatory only for NBHC*********************
		FormField fpremisesDesc=null,finspectionDate=null,finspectedBy=null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForNBHC"))
		{
			fbuilder = new FormFieldBuilder("* Premises under contract",premisesDesc);
			 fpremisesDesc = fbuilder.setMandatory(true).setMandatoryMsg("This is mandatory").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("* Inspection Date",inspectionDate);
			 finspectionDate = fbuilder.setMandatory(true).setMandatoryMsg("This is mandatory").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("* Inspected By",inspectedBy);
			 finspectedBy = fbuilder.setMandatory(true).setMandatoryMsg("This is mandatory").setRowSpan(0).setColSpan(0).build();
			
		}
		else
		{
			fbuilder = new FormFieldBuilder("Premises under contract",premisesDesc);
			 fpremisesDesc = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Inspection Date",inspectionDate);
			 finspectionDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Inspected By",inspectedBy);
			 finspectedBy = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		fbuilder = new FormFieldBuilder("Notes ",employeeDesignation);//old label Designation change done on 30-01-2025
		FormField femployeeDesignation = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("",additionalDetails);
		FormField fadditionalDetails = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Payment Terms", olbPaymentTerms);
		FormField folbPaymentTerms = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", addPayTerms);
		FormField faddPayTerms = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Start Of Period", cbStartOfPeriod);
		FormField fcbStartOfPeriod = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Created By", tbCreatedBy);
		FormField ftbCreatedBy = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date : 19-05-2017 BY ANIL
		 */
		fbuilder = new FormFieldBuilder("", btnCosting);
		FormField fbtnCosting = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 * End
		 */
		
		/**
		 *Date : 08-08-2017 BY ANIL
		 */
		
		fbuilder =  new FormFieldBuilder("Payment Date",dbPaymentDate);
		FormField fdbPaymentDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 *  end
		 */
		
		/** Date 01-09-2017 added by vijay for discount amount final total amount and round off ***/
		
		fbuilder = new FormFieldBuilder("Discount Amount", dbDiscountAmt);
		FormField fdbDiscountAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Amount", dbFinalTotalAmt);
		FormField fdbFinalTotalAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Round Off", tbroundOffAmt);
		FormField fdbroundOffAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Total Amount", dbGrandTotalAmt);
		FormField fdbtotalAmtwithAlltaxes = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 11/10/2017 added by komal for followup date **/
		fbuilder = new FormFieldBuilder("Follow-Up Date", dbFollowUpDate);
		FormField fdbfollowUpDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Product Group",olbProductGroup);
		FormField folbProductGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Product",olbProductName);
		FormField folbProductName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * nidhi
		 * 1-08-208
		 */
		fbuilder = new FormFieldBuilder("Unsuccessful Reason" , tbDocStatusreason);
		FormField ftbdocstatusreson = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 * end
		 */
		/**
		 * Updated By: Viraj
		 * Date: 04-04-2019
		 * Description: To show consolidated data for checkbox
		 */
		fbuilder = new FormFieldBuilder("Consolidate Price",cbConsolidatePrice);
		FormField fcbConsolidatePrice= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		/** Ends **/
		
		/**
		 * @author Anil @since 05-10-2021
		 * Earlier Number range mandatory section is checked with process name 'NumberRange' and was used throughout the project
		 * if we disable it at one place it got disabled from everywhere
		 * So as discussed with Nitin And Poonam we are creating new process configuration for it
		 * Raised for pecopp by Pooname and Nitin Sir
		 */
		FormField folbcNumberRange;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", "PC_MakeNumberRangeMandatory")){
			fbuilder = new FormFieldBuilder("* Number Range",olbcNumberRange);				
			folbcNumberRange= fbuilder.setMandatory(true).setMandatoryMsg("Number Range is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Number Range",olbcNumberRange);
			folbcNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		/*
		 * Ashwini Patil
		 * Date:29-05-2024
		 * Ultima search want to map number range directly from branch so they want this field read only
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
			olbcNumberRange.setEnabled(false);
		
		fbuilder = new FormFieldBuilder("Do not print service address" , cbdonotprintserviceAddress);
		FormField fcbdonotprintserviceAddress = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Payment Mode",objPaymentMode);
		FormField fobjPaymentMode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Do not print total",chkdonotprintTotal);
		FormField fchkdonotprintTotal= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupTermsAndConditions=fbuilder.setlabel("Terms And Conditions").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
				
		fbuilder = new FormFieldBuilder("" , termsAndConditionTable.getTable());
		FormField fTermsAndConditiontable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
		fbuilder = new FormFieldBuilder("Terms And Conditions Title" , olbtermsAndConditionTitle);
		FormField folbtermsAndConditionTitle = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("" , btnTermsAndConditionAdd);
		FormField fbtnTermsAndConditionAdd = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Print All Terms And Condition" , chkPrintAllTermsAndCondition);
		FormField fchkPrintAll = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		 fbuilder = new FormFieldBuilder("Zoho Quotation ID",tbZohoQuotID);
	     FormField ftbZohoQuotID= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				
		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
              if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","HidePaymentMethod")){
            	  hidePaymentgroup=true;
              }
              if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","HideQuotationGroup")){
            	  hideQuotationgroup=true;
              }
              
              
              if(changeProductComposites&&hidePaymentgroup&&hideQuotationgroup){
//          		FormField[][] formfield = {   {fgroupingCustomerInformation},
//        				{fpersonInfoComposite},
//        				{ftbQuotatinId,ftbContractId,fdbValidUntill},
//        				{fdbQuotationDate,fdbfollowUpDate,ftbrefNum,fdbrefDate},
//        				{fgroupingSalesInformation},
//        				{ftbQuotationStatus,folbcPriority,folbbBranch,folbeSalesPerson},
//        				
//        				
//        				{folbQuotationCategory,folbQuotationType,folbApproverName,fibCreditPeriod},
//        				{ftbremark,finspectedBy,finspectionDate,femployeeDesignation},
//        				{fpremisesDesc,ftbCreatedBy,ftbTicketNumber,ftbdocstatusreson},
//        				{fadditionalDetails},
//        				{fgroupingDescription},
//        				{ftaDescription},
//        				{fgroupingDocuments},
//        				{fucUploadTAndCs},
//        				{fcustomeQuotation,fcustomQuotationUpload},
//        				{fgroupingPaymentTermsDetails},
//        				{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//        				{folbPaymentTerms, fcbStartOfPeriod,fdbPaymentDate ,faddPayTerms },
//        				{fpaymentTermsTable},
//        				{fgroupingProducts},
////        				{fprodInfoComposite,fbaddproducts},
//        				{folbProductGroup,folbProductName,fbaddproducts},
//        				{fbtnservice,fbtnCosting},
//        				{fsaleslineitemquotationtable},
//        				{fblankgroupthree,fpayCompTotalAmt},
////        				{fblankgroupthree},
//        				{fblankgroupthree,fdbDiscountAmt},
//        				{fblankgroupthree,fdbFinalTotalAmt},
//        				{fblankgroup,fprodTaxTable},
//        				{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//        				{fblankgroup,fchargesTable},
//        				{fblankgroupthree,fdbtotalAmtwithAlltaxes},
//        				{fblankgroupthree,fdbroundOffAmt},
//        				{fblankgroup,fblankgroupone,fpayCompNetPay}
//        		};
            	  
            	  FormField[][] formfield = {
        				  
        				  /**Quotations DETAILS**/
        				    {fgroupingCustomerInformation},
        				    {fpersonInfoComposite},
        				    {fdbQuotationDate,fdbValidUntill,folbbBranch,folbeSalesPerson},
            				{folbApproverName,ftbremark,ftbdocstatusreson,fdbfollowUpDate},
            				{fcustomeQuotation,fcustomQuotationUpload,folbcNumberRange,fchkdonotprintTotal},
                     		/**PRODUCT DETAILS**/
            				{fgroupingProducts},
            				{fcbConsolidatePrice,fbtnservice,fbtnCosting,fadditionalDetails},
            				{fprodInfoComposite,fbaddproducts},
            				{fsaleslineitemquotationtable},
            				{fblankgroupthree,fpayCompTotalAmt},
            				{fblankgroupthree,fdbDiscountAmt},
            				{fblankgroupthree,fdbFinalTotalAmt},
            				{fblankgroup,fprodTaxTable},
            				{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
            				{fblankgroup,fchargesTable},
            				{fblankgroupthree,fdbtotalAmtwithAlltaxes},
            				{fblankgroupthree,fdbroundOffAmt},
            				{fblankgroup,fblankgroupone,fpayCompNetPay},
            				/**PAYMENT DETAILS**/
            				{fgroupingPaymentTermsDetails},
            				{folbcPaymentMethods,fobjPaymentMode,fibCreditPeriod},
            				{fcbStartOfPeriod,folbPaymentTerms,fdbPaymentDate ,faddPayTerms },
            				{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
            				{fpaymentTermsTable},
            				/**Inspection**/
            				{fInspectiongroup},
            				{fpremisesDesc,finspectedBy,finspectionDate},
            				{femployeeDesignation},
            				/**CLASSIFICATION DETAILS**/
            				{fgroupingSalesInformation},
            				{folbQuotationGroup,folbQuotationCategory,folbQuotationType,folbcPriority},
            				/**GENERAL INFO DETAILS**/
            				{fgroupingGeneralInformation},
            				{ftbLeadId,ftbContractId,ftbrefNum,fdbrefDate},
            				{ftbRefeRRedBy,ftbTicketNumber,ftbCreatedBy,fcbdonotprintserviceAddress},
            				{ftbZohoQuotID},
            				{ftaDescription},
            				/**Attachment**/
            				{fgroupingDocuments},
            				{fucUploadTAndCs},
            				
            				{fgroupTermsAndConditions},
        					{folbtermsAndConditionTitle,fbtnTermsAndConditionAdd,fchkPrintAll},
        					{fTermsAndConditiontable}
            				

          		};


        		this.fields=formfield;
                      } else if(changeProductComposites){
//                    		FormField[][] formfield = {   {fgroupingCustomerInformation},
//                    				{fpersonInfoComposite},
//                    				{ftbQuotatinId,ftbLeadId,ftbContractId,fdbValidUntill},
//                    				{fdbQuotationDate,fdbfollowUpDate,ftbrefNum,fdbrefDate},
//                    				{fgroupingSalesInformation},
//                    				{ftbQuotationStatus,folbcPriority,folbbBranch,folbeSalesPerson},
//                    				
//                    				
//                    				{folbQuotationCategory,folbQuotationType,folbApproverName,fibCreditPeriod},
//                    				{ftbremark,finspectedBy,finspectionDate,femployeeDesignation},
//                    				{fpremisesDesc,ftbCreatedBy,ftbTicketNumber,ftbdocstatusreson},
//                    				{fadditionalDetails},
//                    				{fgroupingDescription},
//                    				{ftaDescription},
//                    				{fgroupingDocuments},
//                    				{fucUploadTAndCs},
//                    				{fcustomeQuotation,fcustomQuotationUpload},
//                    				{fgroupingPaymentTermsDetails},
//                    				{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//                    				{folbPaymentTerms, fcbStartOfPeriod,fdbPaymentDate ,faddPayTerms },
//                    				{fpaymentTermsTable},
//                    				{fgroupingProducts},
////                    				{fprodInfoComposite,fbaddproducts},
//                    				{folbProductGroup,folbProductName,fbaddproducts},
//                    				{fbtnservice,fbtnCosting},
//                    				{fsaleslineitemquotationtable},
//                    				{fblankgroupthree,fpayCompTotalAmt},
////                    				{fblankgroupthree},
//                    				{fblankgroupthree,fdbDiscountAmt},
//                    				{fblankgroupthree,fdbFinalTotalAmt},
//                    				{fblankgroup,fprodTaxTable},
//                    				{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//                    				{fblankgroup,fchargesTable},
//                    				{fblankgroupthree,fdbtotalAmtwithAlltaxes},
//                    				{fblankgroupthree,fdbroundOffAmt},
//                    				{fblankgroup,fblankgroupone,fpayCompNetPay}
//                    		};

                    	  
                    	  FormField[][] formfield = {
                				  
                				  /**Quotations DETAILS**/
                				    {fgroupingCustomerInformation},
                				    {fpersonInfoComposite},
                				    {fdbQuotationDate,fdbValidUntill,folbbBranch,folbeSalesPerson},
                    				{folbApproverName,ftbremark,ftbdocstatusreson,fdbfollowUpDate},
                    				{fcustomeQuotation,fcustomQuotationUpload,folbcNumberRange,fchkdonotprintTotal},
                    				/**PRODUCT DETAILS**/
                    				{fgroupingProducts},
                    				{fcbConsolidatePrice,fbtnservice,fbtnCosting,fadditionalDetails},
                    				{fprodInfoComposite,fbaddproducts},
                    				{fsaleslineitemquotationtable},
                    				{fblankgroupthree,fpayCompTotalAmt},
                    				{fblankgroupthree,fdbDiscountAmt},
                    				{fblankgroupthree,fdbFinalTotalAmt},
                    				{fblankgroup,fprodTaxTable},
                    				{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
                    				{fblankgroup,fchargesTable},
                    				{fblankgroupthree,fdbtotalAmtwithAlltaxes},
                    				{fblankgroupthree,fdbroundOffAmt},
                    				{fblankgroup,fblankgroupone,fpayCompNetPay},
                    				/**PAYMENT DETAILS**/
                    				{fgroupingPaymentTermsDetails},
                    				{folbcPaymentMethods,fobjPaymentMode,fibCreditPeriod},
                    				{fcbStartOfPeriod,folbPaymentTerms,fdbPaymentDate ,faddPayTerms },
                    				{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
                    				{fpaymentTermsTable},
                    				/**Inspection**/
                    				{fInspectiongroup},
                    				{fpremisesDesc,finspectedBy,finspectionDate},
                    				{femployeeDesignation},
                    				/**CLASSIFICATION DETAILS**/
                    				{fgroupingSalesInformation},
                    				{folbQuotationGroup,folbQuotationCategory,folbQuotationType,folbcPriority},
                    				/**GENERAL INFO DETAILS**/
                    				{fgroupingGeneralInformation},
                    				{ftbLeadId,ftbContractId,ftbrefNum,fdbrefDate},
                    				{ftbRefeRRedBy,ftbTicketNumber,ftbCreatedBy,fcbdonotprintserviceAddress},
                    				{ftbZohoQuotID},
                    				{ftaDescription},
                    				/**Attachment**/
                    				{fgroupingDocuments},
                    				{fucUploadTAndCs},
                    				
                    				{fgroupTermsAndConditions},
                					{folbtermsAndConditionTitle,fbtnTermsAndConditionAdd,fchkPrintAll},
                					{fTermsAndConditiontable}

                    		};

                    		this.fields=formfield;
                                  }
                      else if(hidePaymentgroup){
//		FormField[][] formfield = {   {fgroupingCustomerInformation},
//				{fpersonInfoComposite},
//				{ftbQuotatinId,ftbLeadId,ftbContractId,fdbValidUntill},
//				{fdbQuotationDate,fdbfollowUpDate,ftbrefNum,fdbrefDate},
//				{fgroupingSalesInformation},
//				{ftbQuotationStatus,folbcPriority,folbbBranch,folbeSalesPerson},
//				
//				
//				{folbQuotationGroup,folbQuotationCategory,folbQuotationType},
//				{folbApproverName,fibCreditPeriod,ftbremark,fbtnsecurity},
//				{fpremisesDesc,finspectedBy,finspectionDate,femployeeDesignation},
//				{fadditionalDetails,ftbCreatedBy,ftbTicketNumber,ftbdocstatusreson},
//				{fgroupingDescription},
//				{ftaDescription},
//				{fgroupingDocuments},
//				{fucUploadTAndCs},
//				{fcustomeQuotation,fcustomQuotationUpload},
//				{fgroupingPaymentTermsDetails},
//				{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//				{folbPaymentTerms, fcbStartOfPeriod,fdbPaymentDate ,faddPayTerms },
//				{fcbConsolidatePrice},
//				{fpaymentTermsTable},
//				{fgroupingProducts},
//				{fprodInfoComposite,fbaddproducts},
//				{fbtnservice,fbtnCosting},
//				{fsaleslineitemquotationtable},
//				{fblankgroupthree,fpayCompTotalAmt},
////				{fblankgroupthree},
//				{fblankgroupthree,fdbDiscountAmt},
//				{fblankgroupthree,fdbFinalTotalAmt},
//				{fblankgroup,fprodTaxTable},
//				{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//				{fblankgroup,fchargesTable},
//				{fblankgroupthree,fdbtotalAmtwithAlltaxes},
//				{fblankgroupthree,fdbroundOffAmt},
//				{fblankgroup,fblankgroupone,fpayCompNetPay}
//		};
                    	  
                    	  FormField[][] formfield = {
                				  
                				  /**Quotations DETAILS**/
                				    {fgroupingCustomerInformation},
                				    {fpersonInfoComposite},
                				    {fdbQuotationDate,fdbValidUntill,folbbBranch,folbeSalesPerson},
                    				{folbApproverName,ftbremark,ftbdocstatusreson,fdbfollowUpDate},
                    				{fcustomeQuotation,fcustomQuotationUpload,folbcNumberRange,fchkdonotprintTotal},
                    				/**PRODUCT DETAILS**/
                    				{fgroupingProducts},
                    				{fcbConsolidatePrice,fbtnservice,fbtnCosting,fadditionalDetails},
                    				{fprodInfoComposite,fbaddproducts},
                    				{fsaleslineitemquotationtable},
                    				{fblankgroupthree,fpayCompTotalAmt},
                    				{fblankgroupthree,fdbDiscountAmt},
                    				{fblankgroupthree,fdbFinalTotalAmt},
                    				{fblankgroup,fprodTaxTable},
                    				{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
                    				{fblankgroup,fchargesTable},
                    				{fblankgroupthree,fdbtotalAmtwithAlltaxes},
                    				{fblankgroupthree,fdbroundOffAmt},
                    				{fblankgroup,fblankgroupone,fpayCompNetPay},
                    				/**PAYMENT DETAILS**/
                    				{fgroupingPaymentTermsDetails},
                    				{folbcPaymentMethods,fobjPaymentMode,fibCreditPeriod},
                    				{fcbStartOfPeriod,folbPaymentTerms,fdbPaymentDate ,faddPayTerms },
                    				{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
                    				{fpaymentTermsTable},
                    				/**Inspection**/
                    				{fInspectiongroup},
                    				{fpremisesDesc,finspectedBy,finspectionDate},
                    				{femployeeDesignation},
                    				/**CLASSIFICATION DETAILS**/
                    				{fgroupingSalesInformation},
                    				{folbQuotationGroup,folbQuotationCategory,folbQuotationType,folbcPriority},
                    				/**GENERAL INFO DETAILS**/
                    				{fgroupingGeneralInformation},
                    				{ftbLeadId,ftbContractId,ftbrefNum,fdbrefDate},
                    				{ftbRefeRRedBy,ftbTicketNumber,ftbCreatedBy,fcbdonotprintserviceAddress},
                    				{ftbZohoQuotID},
                    				{ftaDescription},
                    				/**Attachment**/
                    				{fgroupingDocuments},
                    				{fucUploadTAndCs},
                    				
                    				{fgroupTermsAndConditions},
                					{folbtermsAndConditionTitle,fbtnTermsAndConditionAdd,fchkPrintAll},
                					{fTermsAndConditiontable}
                    				

                    		};


		this.fields=formfield;
              }else if(hideQuotationgroup){
//          		FormField[][] formfield = {   {fgroupingCustomerInformation},
//        				{fpersonInfoComposite},
//        				{ftbQuotatinId,ftbLeadId,ftbContractId,fdbValidUntill},
//        				{fdbQuotationDate,fdbfollowUpDate,ftbrefNum,fdbrefDate},
//        				{fgroupingSalesInformation},
//        				{ftbQuotationStatus,folbcPriority,folbbBranch,folbeSalesPerson},
//        				{folbcPaymentMethods,folbQuotationCategory,folbQuotationType},
//        				{folbApproverName,fibCreditPeriod,ftbremark,fbtnsecurity},
//        				{fpremisesDesc,finspectedBy,finspectionDate,femployeeDesignation},
//        				{fadditionalDetails,ftbCreatedBy,ftbTicketNumber,ftbdocstatusreson},
//        				{fgroupingDescription},
//        				{ftaDescription},
//        				{fgroupingDocuments},
//        				{fucUploadTAndCs},
//        				{fcustomeQuotation,fcustomQuotationUpload},
//        				{fgroupingPaymentTermsDetails},
//        				{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//        				{folbPaymentTerms, fcbStartOfPeriod,fdbPaymentDate ,faddPayTerms },
//        				{fpaymentTermsTable},
//        				{fgroupingProducts},
//        				{fprodInfoComposite,fbaddproducts},
//        				{fbtnservice,fbtnCosting},
//        				{fsaleslineitemquotationtable},
//        				{fblankgroupthree,fpayCompTotalAmt},
////        				{fblankgroupthree},
//        				{fblankgroupthree,fdbDiscountAmt},
//        				{fblankgroupthree,fdbFinalTotalAmt},
//        				{fblankgroup,fprodTaxTable},
//        				{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//        				{fblankgroup,fchargesTable},
//        				{fblankgroupthree,fdbtotalAmtwithAlltaxes},
//        				{fblankgroupthree,fdbroundOffAmt},
//        				{fblankgroup,fblankgroupone,fpayCompNetPay}
//        		};
            	  
            	  FormField[][] formfield = {
        				  
        				  /**Quotations DETAILS**/
        				    {fgroupingCustomerInformation},
        				    {fpersonInfoComposite},
        				    {fdbQuotationDate,fdbValidUntill,folbbBranch,folbeSalesPerson},
            				{folbApproverName,ftbremark,ftbdocstatusreson,fdbfollowUpDate},
            				{fcustomeQuotation,fcustomQuotationUpload,folbcNumberRange,fchkdonotprintTotal},
            				/**PRODUCT DETAILS**/
            				{fgroupingProducts},
            				{fcbConsolidatePrice,fbtnservice,fbtnCosting,fadditionalDetails},
            				{fprodInfoComposite,fbaddproducts},
            				{fsaleslineitemquotationtable},
            				{fblankgroupthree,fpayCompTotalAmt},
            				{fblankgroupthree,fdbDiscountAmt},
            				{fblankgroupthree,fdbFinalTotalAmt},
            				{fblankgroup,fprodTaxTable},
            				{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
            				{fblankgroup,fchargesTable},
            				{fblankgroupthree,fdbtotalAmtwithAlltaxes},
            				{fblankgroupthree,fdbroundOffAmt},
            				{fblankgroup,fblankgroupone,fpayCompNetPay},
            				/**PAYMENT DETAILS**/
            				{fgroupingPaymentTermsDetails},
            				{folbcPaymentMethods,fobjPaymentMode,fibCreditPeriod},
            				{fcbStartOfPeriod,folbPaymentTerms,fdbPaymentDate ,faddPayTerms },
            				{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
            				{fpaymentTermsTable},
            				/**Inspection**/
            				{fInspectiongroup},
            				{fpremisesDesc,finspectedBy,finspectionDate},
            				{femployeeDesignation},
            				/**CLASSIFICATION DETAILS**/
            				{fgroupingSalesInformation},
            				{folbQuotationGroup,folbQuotationCategory,folbQuotationType,folbcPriority},
            				/**GENERAL INFO DETAILS**/
            				{fgroupingGeneralInformation},
            				{ftbLeadId,ftbContractId,ftbrefNum,fdbrefDate},
            				{ftbRefeRRedBy,ftbTicketNumber,ftbCreatedBy,fcbdonotprintserviceAddress},
            				{ftbZohoQuotID},
            				{ftaDescription},
            				/**Attachment**/
            				{fgroupingDocuments},
            				{fucUploadTAndCs},
            				
            				{fgroupTermsAndConditions},
        					{folbtermsAndConditionTitle,fbtnTermsAndConditionAdd,fchkPrintAll},
        					{fTermsAndConditiontable}
          				
            		};

        		this.fields=formfield;
                      }
            	  else{
//          		FormField[][] formfield = {   {fgroupingCustomerInformation},
//          				{fpersonInfoComposite},
//          				{ftbQuotatinId,ftbLeadId,ftbContractId,fdbValidUntill},
//          				{fdbQuotationDate,fdbfollowUpDate,ftbrefNum,fdbrefDate},
//          				{fgroupingSalesInformation},
//          				{ftbQuotationStatus,folbcPriority,folbbBranch,folbeSalesPerson},
//          				{folbcPaymentMethods,folbQuotationGroup,folbQuotationCategory,folbQuotationType},
//          				{folbApproverName,fibCreditPeriod,ftbremark,fbtnsecurity},
//          				{fpremisesDesc,finspectedBy,finspectionDate,femployeeDesignation},
//          				{fadditionalDetails,ftbCreatedBy,ftbTicketNumber,ftbdocstatusreson},
//          				{fgroupingDescription},
//          				{ftaDescription},
//          				{fgroupingDocuments},
//          				{fucUploadTAndCs},
//          				{fcustomeQuotation,fcustomQuotationUpload},
//          				{fgroupingPaymentTermsDetails},
//          				{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//          				{folbPaymentTerms, fcbStartOfPeriod,fdbPaymentDate ,faddPayTerms },
//          				{fcbConsolidatePrice},
//          				{fpaymentTermsTable},
//          				{fgroupingProducts},
//          				{fprodInfoComposite,fbaddproducts},
//          				{fbtnservice,fbtnCosting},
//          				{fsaleslineitemquotationtable},
//          				{fblankgroupthree,fpayCompTotalAmt},
////          				{fblankgroupthree},
//          				{fblankgroupthree,fdbDiscountAmt},
//          				{fblankgroupthree,fdbFinalTotalAmt},
//          				{fblankgroup,fprodTaxTable},
//          				{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//          				{fblankgroup,fchargesTable},
//          				{fblankgroupthree,fdbtotalAmtwithAlltaxes},
//          				{fblankgroupthree,fdbroundOffAmt},
//          				{fblankgroup,fblankgroupone,fpayCompNetPay}
//          		};
            		  
            		  
            		  FormField[][] formfield = {
            				  
            				  /**Quotations DETAILS**/
            				    {fgroupingCustomerInformation},
                				{fpersonInfoComposite},
                				{fdbQuotationDate,fdbValidUntill,folbbBranch,folbeSalesPerson},
                				{folbApproverName,ftbremark,ftbdocstatusreson,fdbfollowUpDate},
                				{fcustomeQuotation,fcustomQuotationUpload,folbcNumberRange,fchkdonotprintTotal},
                				/**PRODUCT DETAILS**/
                				{fgroupingProducts},
                				{fcbConsolidatePrice,fbtnservice,fbtnCosting,fadditionalDetails},
                				{fprodInfoComposite,fbaddproducts},
                				{fsaleslineitemquotationtable},
                				{fblankgroupthree,fpayCompTotalAmt},
                				{fblankgroupthree,fdbDiscountAmt},
                				{fblankgroupthree,fdbFinalTotalAmt},
                				{fblankgroup,fprodTaxTable},
                				{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
                				{fblankgroup,fchargesTable},
                				{fblankgroupthree,fdbtotalAmtwithAlltaxes},
                				{fblankgroupthree,fdbroundOffAmt},
                				{fblankgroup,fblankgroupone,fpayCompNetPay},
                				/**PAYMENT DETAILS**/
                				{fgroupingPaymentTermsDetails},
                				{folbcPaymentMethods,fobjPaymentMode,fibCreditPeriod},
                				{fcbStartOfPeriod,folbPaymentTerms,fdbPaymentDate ,faddPayTerms },
                				{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
                				{fpaymentTermsTable},
                				/**Inspection**/
                				{fInspectiongroup},
                				{fpremisesDesc,finspectedBy,finspectionDate},
                				{femployeeDesignation},
                				/**CLASSIFICATION DETAILS**/
                				{fgroupingSalesInformation},
                				{folbQuotationGroup,folbQuotationCategory,folbQuotationType,folbcPriority},
                				/**GENERAL INFO DETAILS**/
                				{fgroupingGeneralInformation},
                				{ftbLeadId,ftbContractId,ftbrefNum,fdbrefDate},
                				{ftbRefeRRedBy,ftbTicketNumber,ftbCreatedBy,fcbdonotprintserviceAddress},
                				{ftbZohoQuotID},
                				{ftaDescription},
                				/**Attachment**/
                				{fgroupingDocuments},
                				{fucUploadTAndCs},
                				
                				{fgroupTermsAndConditions},
            					{folbtermsAndConditionTitle,fbtnTermsAndConditionAdd,fchkPrintAll},
            					{fTermsAndConditiontable}
                				
                				
                		};

          		this.fields=formfield;
                        }
		customeQuotation.setValue(Boolean.valueOf(false));
	}

	/**
	 * method template to update the model with token entity name.
	 *
	 * @param model the model
	 */
	@Override
	public void updateModel(Quotation model) 
	{
		
		model.setPremisesDesc(premisesDesc.getValue());
		model.setInspectedBy(inspectedBy.getValue());
		model.setInspectionDate(inspectionDate.getValue());
		model.setDesignation(employeeDesignation.getValue());
		
		if(personInfoComposite.getValue()!=null)
			model.setCinfo(personInfoComposite.getValue());
		if(dbValidUntill.getValue()!=null)
			model.setValidUntill(dbValidUntill.getValue());
		if(tbRefeRRedBy.getValue()!=null)
			model.setReferedBy(tbRefeRRedBy.getValue());
		
			model.setDocument(ucUploadTAndCs.getValue());
			System.out.println("Value of Uploaded doc"+ucUploadTAndCs.getValue());
		if(tbQuotationStatus.getValue()!=null)
			model.setStatus(tbQuotationStatus.getValue());
		
		model.setDocument(ucUploadTAndCs.getValue());
		
		if(customQuotationUpload.getValue()!=null){
		model.setCustomeQuotationUpload(customQuotationUpload.getValue());
		} // Date 24-04-2018 by vijay for issue des :- if we updload document and save it and again i have edit and remove the docs still display the old document.
		else{   
			model.setCustomeQuotationUpload(null);
		}
		
		if(customeQuotation.getValue()!=null){
			model.setCustomequotation(customeQuotation.getValue());
		}
		
		//******************changes ends here *************************
		if(!tbrefNum.getValue().equals("")){
			model.setReferenceNumber(tbrefNum.getValue());
		} // Date 11-01-2018 else condition added by vijay  first time reference number data  store if i remove and save then not overwrite so added else for overwrite 
		else{
			model.setReferenceNumber("");
		}
		
		if(dbrefDate.getValue() !=null){
			model.setReferenceDate(dbrefDate.getValue());
		} // Date 11-01-2018 else condition added by vijay first time reference number data  store if i remove and save then not overwrite so added else for overwrite 
		else{
			model.setReferenceDate(null);
		}

		if(!tbTicketNumber.getValue().equals("")){
			model.setTicketNumber(Integer.parseInt(tbTicketNumber.getValue()));
			model.setComplaintFlag(true);
		}
		
		
		
		
		if(olbcPriority.getValue()!=null)
			model.setPriority(olbcPriority.getValue());
		if(olbbBranch.getValue(olbbBranch.getSelectedIndex())!=null)
			model.setBranch(olbbBranch.getValue(olbbBranch.getSelectedIndex()));
		if(olbeSalesPerson.getValue()!=null)
			model.setEmployee(olbeSalesPerson.getValue());
		if(olbcPaymentMethods.getValue()!=null)
			model.setPaymentMethod(olbcPaymentMethods.getValue());
		if(taDescription.getValue()!=null)
			model.setDescription(taDescription.getValue());
		/**
		 * Date 10-4-2018
		 * By Jayshree
		 */
		if(olbQuotationType.getValue()!=null){
			model.setType(olbQuotationType.getValue(olbQuotationType.getSelectedIndex()));
		}else{
			model.setType("");
		}
		if(olbQuotationGroup.getValue()!=null)
			model.setGroup(olbQuotationGroup.getValue());
		if(olbQuotationCategory.getValue()!=null)
			model.setCategory(olbQuotationCategory.getValue());
		if(olbApproverName.getValue()!=null)
			model.setApproverName(olbApproverName.getValue());
		if(!tbContractId.getValue().equals(""))
			model.setContractCount(Integer.parseInt(tbContractId.getValue()));
		if(!tbLeadId.getValue().equals(""))
			model.setLeadCount(Integer.parseInt(tbLeadId.getValue()));
		if(dbQuotationDate.getValue()!=null){
			model.setQuotationDate(dbQuotationDate.getValue());
		}
		if(ibCreditPeriod.getValue()!=null){
			model.setCreditPeriod(ibCreditPeriod.getValue());
		}
		
		List<SalesLineItem> saleslis=this.getSaleslineitemquotationtable().getDataprovider().getList();
		if(saleslis.size()!=0){
			model.setStartDate(AppUtility.calculateQuotationStartDate(saleslis));
		}
		
		if(saleslis.size()!=0)
		{
			model.setEndDate(AppUtility.calculateQuotationEndDate(saleslis));
		}
		
		if(dototalamt.getValue()!=null)
			model.setTotalAmount(dototalamt.getValue());
		if(donetpayamt.getValue()!=null)
			model.setNetpayable(donetpayamt.getValue());
		
		List<SalesLineItem> saleslist=this.getSaleslineitemquotationtable().getDataprovider().getList();
		ArrayList<SalesLineItem> arrItems=new ArrayList<SalesLineItem>();
		arrItems.addAll(saleslist);
		model.setItems(arrItems);
		
		List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
		ArrayList<PaymentTerms> payTermsArr=new ArrayList<PaymentTerms>();
		payTermsArr.addAll(payTermsLis);
		model.setPaymentTermsList(payTermsArr);
		
		List<ProductOtherCharges> productTaxLis=this.prodTaxTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> productTaxArr=new ArrayList<ProductOtherCharges>();
		productTaxArr.addAll(productTaxLis);
		model.setProductTaxes(productTaxArr);
		
		List<ProductOtherCharges> productChargeLis=this.chargesTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> productChargeArr=new ArrayList<ProductOtherCharges>();
		productChargeArr.addAll(productChargeLis);
		model.setProductCharges(productChargeArr);
		
				
				if (f_securityDeposit != null) {
					if(f_securityDeposit>0){
						model.setIsDepositPaid(AppConstants.YES);
					}else{
						model.setIsDepositPaid(AppConstants.NO);
					}
					model.setSdDepositAmount(f_securityDeposit);
				}

				if (f_reDate != null) {
					model.setSdReturnDate(f_reDate);
				}
				if (f_depositStatus != false) {
					model.setDepositReceived(f_depositStatus);
				}
				if (f_payMethod != null) {
					model.setSdPaymentMethod(f_payMethod);
				}
				if(f_accountNo != null){
					model.setSdBankAccNo(f_accountNo);
				}
				
				if (f_bankName != null) {
					model.setSdBankName(f_bankName);
				}
				
				System.out.println(f_bankName);
				if (f_branch != null) {
					model.setSdBankBranch(f_branch);
				}
				if (f_paybleAt != null) {
					model.setSdPayableAt(f_paybleAt);
				}
				if (f_fouring != null) {
					model.setSdFavouring(f_fouring);
				}
//				if (f_checkName != null) {
//					model.setCheckName(f_checkName);
//				}
				if(f_checkNo!=0){
					model.setSdChequeNo(f_checkNo);
				}
				if (f_checkDate != null) {
					model.setSdChequeDate(f_checkDate);
				}
				if(!f_serviceNo.equals("")){
					model.setSdReferenceNo(f_serviceNo);
				}
				if (f_ifscCode != null) {
					model.setSdIfscCode(f_ifscCode);
				}
				if (f_micrCode != null) {
					model.setSdMicrCode(f_micrCode);
				}
				if (f_swiftCode != null) {
					model.setSdSwiftCode(f_swiftCode);
				}

				if (f_address != null) {
					model.setSdAddress(f_address);
				}
				if (f_instruction != null) {
					model.setSdInstruction(f_instruction);
				}
				
				
				List<ServiceSchedule> prodservicelist=this.serviceScheduleTable.getDataprovider().getList();
				ArrayList<ServiceSchedule> prodserviceArr=new ArrayList<ServiceSchedule>();
				prodserviceArr.addAll(prodservicelist);
				model.setServiceScheduleList(prodserviceArr);
		
		
				if (f_serviceDay!= null) {
					model.setScheduleServiceDay(f_serviceDay);
				}
				
		
				if (olbPaymentTerms.getSelectedIndex() != 0){
					model.setPayTerms(olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()));
				}else{
					model.setPayTerms("");
				}
		
		/**
		 * Date:22-05-2017 By ANIL
		 */
		if(matCostList!=null){
			model.setMatCostList(matCostList);
		}
		if(manPowCostList!=null){
			model.setManPowCostList(manPowCostList);
		}
		if(otherCostList!=null){
			model.setOtherCostList(otherCostList);
		}
		/**
		 * End
		 */		
		
		/** Date 1-09-2017 added by vijay for discount amount final total amount and round off ***/
		if(dbDiscountAmt.getValue()!=null)
			model.setDiscountAmt(dbDiscountAmt.getValue());
		if(dbFinalTotalAmt.getValue() !=null)
			model.setFinalTotalAmt(dbFinalTotalAmt.getValue());
		if(tbroundOffAmt.getValue()!=null && !tbroundOffAmt.getValue().equals(""))
			model.setRoundOffAmt(Double.parseDouble(tbroundOffAmt.getValue()));
		if(dbGrandTotalAmt.getValue()!=null)
			model.setGrandTotalAmount(dbGrandTotalAmt.getValue());
		if(doamtincltax.getValue()!=null){
			model.setInclutaxtotalAmount(doamtincltax.getValue());
		}
		
		/** date 10/10/2017 added by komal for follow-up date **/
		
		if(dbFollowUpDate.getValue() == null){
			model.setFollowUpDate(dbQuotationDate.getValue());
		}else{
			model.setFollowUpDate(dbFollowUpDate.getValue());
		}
		/**
		 * nidhi
		 * 1-08-2018
		 */
		model.setDocStatusReason(tbDocStatusreason.getValue());
		/**
		 * end
		 */
		/**
		 * Updated by: Viraj
		 * Date: 04-04-2019
		 * Description: To show consolidate data checkbox
		 */
		if(cbConsolidatePrice.getValue() !=null){
			model.setConsolidatePrice(cbConsolidatePrice.getValue());
		}
		
		if(olbProductGroup.getValue()!=null)
			  model.setProductGroup(olbProductGroup.getValue());
		
		/** Ends **/
		
		
		if(olbcNumberRange.getSelectedIndex()!=0){
			model.setQuotationNumberRange(olbcNumberRange.getValue());
		}
		else{
			model.setQuotationNumberRange("");
		}
		
		quotationObject=model;
		
		presenter.setModel(model);
		
		/**
		 * nidhi |*|
		 */
		getServiceMaterialBomList();
		/**
		 * end
		 */
	
		model.setDonotprintServiceAddress(cbdonotprintserviceAddress.getValue());
		
		if(objPaymentMode.getSelectedIndex()!=0){
			model.setPaymentModeName(objPaymentMode.getValue(objPaymentMode.getSelectedIndex()));
		}else{
			model.setPaymentModeName("");
			
		}
		
		model.setDonotPrintTotal(chkdonotprintTotal.getValue());
		
		if(this.termsAndConditionTable.getDataprovider().getList()!=null && this.termsAndConditionTable.getDataprovider().getList().size()>0){
			ArrayList<TermsAndConditions> arraylist = new ArrayList<TermsAndConditions>();
			List<TermsAndConditions> list = this.termsAndConditionTable.getDataprovider().getList();
			arraylist.addAll(list);
			model.setTermsAndConditionlist(arraylist);
		}
		
		model.setPrintAllTermsConditions(chkPrintAllTermsAndCondition.getValue());
		
		if(cbStartOfPeriod.getValue()!=null)
		model.setStartOfPeriod(cbStartOfPeriod.getValue());
	}

	
	/**
	 * method template to update the view with token entity name.
	 *
	 * @param view the view
	 */
	@Override
	public void updateView(Quotation view) 
	{
		quotationObject =view;
		//****************rohan changes here for om pest  ************************
				if(view.getPremisesDesc()!=null)
					premisesDesc.setValue(view.getPremisesDesc());
				
				if(view.getInspectedBy()!=null)
					inspectedBy.setValue(view.getInspectedBy());
				
				if(view.getInspectionDate()!=null)
					inspectionDate.setValue(view.getInspectionDate());
				
				if(view.getDesignation()!=null)
				employeeDesignation.setValue(view.getDesignation());
				
		// *****************changes ends here ***********************
		
		/**
		 * Date : 12-05-2017 By ANil
		 * for loading customers branches in service schedule popup.
		 */
		QuotationPresenter.custcount=view.getCinfo().getCount();
		/**
		 * End
		 */
		
		this.tbQuotatinId.setValue(view.getCount()+"");
		
		this.checkCustomerStatus(view.getCinfo().getCount());
		
		
		if(view.getCinfo()!=null)
			personInfoComposite.setValue(view.getCinfo());
		if(view.getValidUntill()!=null)
			dbValidUntill.setValue(view.getValidUntill());
		if(view.getReferedBy()!=null)
			tbRefeRRedBy.setValue(view.getReferedBy());
		if(view.getDocument()!=null)
			ucUploadTAndCs.setValue(view.getDocument());
		if(view.getStatus()!=null)
			tbQuotationStatus.setValue(view.getStatus());
		if(view.getPriority()!=null)
			olbcPriority.setValue(view.getPriority());
		if(view.getBranch()!=null)
			olbbBranch.setValue(view.getBranch());
		if(view.getEmployee()!=null)
			
			
			if(view.getCustomeQuotationUpload()!=null){
				customQuotationUpload.setValue(view.getCustomeQuotationUpload());
			}
		
				customeQuotation.setValue(view.isCustomequotation());
		

				if(view.getReferenceNumber() !=null){
					
					tbrefNum.setValue(view.getReferenceNumber());
				}
				
				if(view.getReferenceDate() !=null){
					
					dbrefDate.setValue(view.getReferenceDate());
				}
				
				
				if(view.getTicketNumber()!=-1){
					tbTicketNumber.setValue(view.getTicketNumber()+"");
					System.out.println("TICKET NUMBER :: "+view.getTicketNumber()+" && COMPLAINT FLAG :: "+view.getComplaintFlag() );
				}
				
				
			
			olbeSalesPerson.setValue(view.getEmployee());
		if(view.getPaymentMethod()!=null)
			olbcPaymentMethods.setValue(view.getPaymentMethod());
		if(view.getDescription()!=null)
			taDescription.setValue(view.getDescription());
		if(view.getCategory()!=null)
			olbQuotationCategory.setValue(view.getCategory());
		if(view.getType()!=null)
			olbQuotationType.setValue(view.getType());
		if(view.getGroup()!=null)
			olbQuotationGroup.setValue(view.getGroup());
		if(view.getApproverName()!=null)
			olbApproverName.setValue(view.getApproverName());
		
		if(view.getLeadCount()!=-1)
			tbLeadId.setValue(view.getLeadCount()+"");
		if(view.getContractCount()!=-1)
			tbContractId.setValue(view.getContractCount()+"");
		if(view.getCreditPeriod()!=null){
			ibCreditPeriod.setValue(view.getCreditPeriod());
		}
		if(view.getQuotationDate()!=null){
			dbQuotationDate.setValue(view.getQuotationDate());
		}
		if(view.getRemark()!=null){
			tbremark.setValue(view.getRemark());
		}
		
		saleslineitemquotationtable.setValue(view.getItems());
		
		paymentTermsTable.setValue(view.getPaymentTermsList());
		prodTaxTable.setValue(view.getProductTaxes());
		chargesTable.setValue(view.getProductCharges());
		dototalamt.setValue(view.getTotalAmount());
		donetpayamt.setValue(view.getNetpayable());
		
		f_accountNo=view.getSdBankAccNo();
		f_bankName=view.getSdBankName();
		 f_securityDeposit=view.getSdDepositAmount();
		 f_reDate=view.getSdReturnDate();
		 f_depositStatus=view.isDepositReceived();
		 f_payMethod=view.getSdPaymentMethod();
		 f_branch=view.getSdBankBranch();
		 f_paybleAt=view.getSdPayableAt();
		 f_fouring=view.getSdFavouring();
		 f_checkNo=view.getSdChequeNo();
		 f_checkDate=view.getSdChequeDate();
		 f_serviceNo=view.getSdReferenceNo();
		 f_ifscCode=view.getSdIfscCode();
		 f_micrCode=view.getSdMicrCode();
		 f_swiftCode=view.getSdSwiftCode();
		 f_instruction=view.getSdInstruction();
		 f_address=view.getSdAddress();
		
		 serviceScheduleTable.setValue(view.getServiceScheduleList());
			if(view.getScheduleServiceDay()!=null){
				f_serviceDay=view.getScheduleServiceDay();
			}
			
			
			if (view.getPayTerms() != null)
				olbPaymentTerms.setValue(view.getPayTerms());	
		
		if (view.getCreatedBy() != null)
			tbCreatedBy.setValue(view.getCreatedBy());	
			
		/**
		 * Date:22-05-2017 By ANIL
		 */
		if(view.getMatCostList()!=null){
			matCostList=new ArrayList<MaterialCostStructure>();
			matCostList.addAll(view.getMatCostList());
		}
		if(view.getManPowCostList()!=null){
			manPowCostList=new ArrayList<ManPowerCostStructure>();
			manPowCostList.addAll(view.getManPowCostList());
		}
		if(view.getOtherCostList()!=null){
			otherCostList=new ArrayList<OtherCost>();
			otherCostList.addAll(view.getOtherCostList());
		}
		/**
		 * End
		 */
		
		/** Date 1-09-2017 added by vijay for discount amount final total amount and round off ***/
		dbDiscountAmt.setValue(view.getDiscountAmt());
		dbFinalTotalAmt.setValue(view.getFinalTotalAmt());
		tbroundOffAmt.setValue(view.getRoundOffAmt()+"");
		dbGrandTotalAmt.setValue(view.getGrandTotalAmount());
		doamtincltax.setValue(view.getInclutaxtotalAmount());
		
		/** date 11/10/2017 added by komal for follow-up date **/
		
		if(view.getFollowUpDate()!= null)
			dbFollowUpDate.setValue(view.getFollowUpDate());
		
		/**
		 * nidhi
		 * 1-08-2018
		 */
		tbDocStatusreason.setValue(view.getDocStatusReason());
		/**
		 * end
		 */
		
		/**
		 * Date : 31-08-2017 BY Vijay
		 * If service schedule list is greater than 700 then we have to retrieve data from separate entity as 
		 * we are storing it in separate entity.
		 */
		if(view.getServiceScheduleList().size()==0){
			Console.log("SERVICE SCHEDULE LIST ");
			Console.log("COM_ID "+view.getCompanyId()+" DOC_TYPE "+"Contract"+" DOC_ID "+view.getCount());
			final ContractServiceAsync conSer=GWT.create(ContractService.class);
			
			conSer.getScheduleServiceSearchResult(view.getCompanyId(), "Quotation", view.getCount(), new AsyncCallback<ArrayList<ServiceSchedule>>() {
				@Override
				public void onFailure(Throwable caught) {
					Console.log("SERVICE SCHEDULE FAILURE ");
				}
				@Override
				public void onSuccess(ArrayList<ServiceSchedule> result) {
					Console.log("SERVICE SCHEDULE SUCCESS "+result.size());
					if(result.size()!=0){
						serviceScheduleTable.setValue(result);
						/**
						 * nidhi |*|
						 */
						getServiceMaterialBomList();
						/**
						 * end
						 */
					}
				}
			});
			      
		}
		/**
		 * End
		 */
		
		/**
		 * Updated by: Viraj
		 * Date: 04-04-2019
		 * Description: To show consolidate data checkbox
		 */
		cbConsolidatePrice.setValue(view.isConsolidatePrice());
		/** Ends **/
		
		if(view.getQuotationNumberRange()!=null){
			olbcNumberRange.setValue(view.getQuotationNumberRange());
		}
		
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		if(view.getProductGroup()!=null)
			olbProductGroup.setValue(view.getProductGroup());
		/*
		 *  end
		 */
		
		/**
		 * nidhi |*|
		 */
		getServiceMaterialBomList();
		/**
		 * end
		 */

		cbdonotprintserviceAddress.setValue(view.isDonotprintServiceAddress());
		
		if(view.getPaymentModeName()!=null){
        	objPaymentMode.setValue(view.getPaymentModeName());
        	}
		
		chkdonotprintTotal.setValue(view.isDonotPrintTotal());
		
		if(view.getTermsAndConditionlist()!=null && view.getTermsAndConditionlist().size()>0){
			termsAndConditionTable.getDataprovider().setList(view.getTermsAndConditionlist());
		}
		
		chkPrintAllTermsAndCondition.setValue(view.isPrintAllTermsConditions());
		
		cbStartOfPeriod.setValue(view.isStartOfPeriod());
		if(view.getZohoQuotID()!=null&&!view.getZohoQuotID().equals("")){
			tbZohoQuotID.setValue(view.getZohoQuotID());
		}
	}

	// Hand written code shift in presenter
	
	/**
	 * Toggles the app header bar menus as per screen state.
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Anil @since 15-07-2021
			 * for summary screen
			 */
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				/**
			     * @author Anil
			     * @since 31-12-2020
			     * remove communication log ftom navigation
			     */
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Anil @since 15-07-2021
			 * for summary screen
			 */
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Anil @since 15-07-2021
			 * for summary screen
			 */
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains("Search")
						||text.contains("Print")||text.contains(AppConstants.NAVIGATION)|| text.contains(AppConstants.COMMUNICATIONLOG))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.QUOTATION,LoginPresenter.currentModule.trim());
	}


	/**
	 * Sets the viewas per process.
	 */
	public void setAppHeaderBarAsPerStatus()
	{
		Quotation entity=(Quotation) presenter.getModel();
		String status=entity.getStatus();
		if(status.equals(Quotation.QUOTATIONSUCESSFUL)||status.equals(Quotation.QUOTATIONUNSUCESSFUL)
				||status.equals(Quotation.REJECTED)||status.equals(Quotation.APPROVED)|| status.equals(Quotation.CANCELLED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION) || text.contains(AppConstants.MESSAGE))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}

		}

		if(status.equals(Quotation.APPROVED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Email")||text.contains("Print")
						||text.contains(AppConstants.COMMUNICATIONLOG)||text.contains(AppConstants.NAVIGATION) || text.contains(AppConstants.MESSAGE))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}
		
		if(status.equals(Quotation.REQUESTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		if(status.equals(Quotation.REJECTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Edit")||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}
		/*
		if(status.equals(Quotation.CREATED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Save")||text.contains("Edit"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}*/
	}

	/**
	 * Toggle process level menu.
	 */
	public void toggleProcessLevelMenu()
 {
		Quotation entity = (Quotation) presenter.getModel();
		String status = entity.getStatus();

		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();

			if (status.equals(Quotation.CREATED)) {
				Console.log("Inside CREATED");
				if(getManageapproval().isSelfApproval())
				{  
					if(text.trim().equals(ManageApprovals.APPROVALREQUEST)){
						label.setVisible(false);
					}else if(text.trim().equals(ManageApprovals.SUBMIT)){
						label.setVisible(true);
					}
                    
				}
				else
				{  
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(true);
					if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						label.setVisible(false);
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(false);
				}

				if (text.equals(AppConstants.CREATECONTRACT))
					label.setVisible(false);
				if (text.equals("Mark Successful"))
					label.setVisible(false);
				if (text.equals(AppConstants.CREATEQUICKCONTRACT))
					label.setVisible(false);
				/*
				 * nidhi 07-07-2017
				 */
				if (text.equals("Revised Quotation"))
					label.setVisible(false);
				/*
				 * end
				 */
				/**
				 * Date : 06-11-2017 BY ANIL
				 */
				if(text.equals(AppConstants.CANCEL)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.Unsucessful)){
					label.setVisible(false);
				}
				/**
				 * End
				 */

			} else if (status.equals(Quotation.APPROVED)) {
				Console.log("Inside APPROVED");
				if (text.equals(AppConstants.CREATECONTRACT))
					label.setVisible(true);
				if (text.equals(AppConstants.Approve))
					label.setVisible(false);
				if (text.equals("Mark Successful"))
					label.setVisible(true);
				if (text.equals(AppConstants.CREATEQUICKCONTRACT))
					label.setVisible(true);
				/*
				 * nidhi 07-07-2017
				 */
				if (text.equals("Revised Quotation"))
					label.setVisible(true);
				/*
				 * end
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				
				/**
				 * Date : 06-11-2017 BY ANIL
				 */
				if(text.equals(AppConstants.CANCEL)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.Unsucessful)){
					label.setVisible(true);
				}
				/**
				 * End
				 */

			}

			else if (status.equals(Quotation.REQUESTED)) {
				Console.log("Inside REQUESTED");
				if (text.contains(AppConstants.CREATECONTRACT))
					label.setVisible(false);
				if (text.contains(AppConstants.Unsucessful))
					label.setVisible(false);
				if (text.equals("Mark Successful"))
					label.setVisible(false);
				if (text.equals(AppConstants.CREATEQUICKCONTRACT))
					label.setVisible(false);
				/*
				 * nidhi 07-07-2017
				 */
				if (text.equals("Revised Quotation"))
					label.setVisible(false);
				/*
				 * end
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				
				/**
				 * Date : 06-11-2017 BY ANIL
				 */
				if(text.equals(AppConstants.CANCEL)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.Unsucessful)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
			}
			/**Date 20-12-2019 by Amol Added a Create Contract Button when Quotation is SUCCESSFUL**/
			else if (status.equals(Quotation.QUOTATIONSUCESSFUL)) {

				Console.log("Inside SUCCESSFULL");
				if (text.contains(AppConstants.CREATECONTRACT)) {
					label.setVisible(true);
					Console.log("value of create contract"
							+ text.contains(AppConstants.CREATECONTRACT));
				}

				else if (text.equals(AppConstants.CANCEL)) {
					label.setVisible(true);
				} else if (text.equals(AppConstants.Unsucessful)) {
					label.setVisible(true);
				}

				else {
					if ((text.contains(AppConstants.NEW))) {
						label.setVisible(true);
					} else {
						label.setVisible(false);
					}
				}

			}

			else if (status.equals(Quotation.QUOTATIONUNSUCESSFUL)
//					|| status.equals(Quotation.QUOTATIONSUCESSFUL)
					|| status.equals(Quotation.REJECTED)) {

				Console.log("Inside UNSUCCESSFULL and REJECTED");
				if ((text.contains(AppConstants.NEW)))
					label.setVisible(true);
				else
					label.setVisible(false);

				/*
				 * nidhi 07-07-2017
				 */
				if (text.equals("Revised Quotation"))
					label.setVisible(false);
				/*
				 * end
				 */

				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				
				/**
				 * Date : 06-11-2017 BY ANIL
				 */
				if(text.equals(AppConstants.CANCEL)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.Unsucessful)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
			}

			else if (status.equals(Quotation.REJECTED)) {
				Console.log("Inside REJECTED");
				if (text.contains(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);

				/*
				 * nidhi 07-07-2017
				 */
				if (text.equals("Revised Quotation"))
					label.setVisible(false);
				/*
				 * end
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * Date : 06-11-2017 BY ANIL
				 */
				if(text.equals(AppConstants.CANCEL)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.Unsucessful)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
			}
			/**
			 * Date 09-05-2018 added by vijay for cancel quotation process bar battons disable
			 */
			else if (status.equals(Quotation.CANCELLED)) {
 
				Console.log("Inside CANCELLED");
				if (text.contains(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);

				/*
				 * nidhi 07-07-2017
				 */
				if (text.equals("Revised Quotation"))
					label.setVisible(false);
				/*
				 * end
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * Date : 06-11-2017 BY ANIL
				 */
				if(text.equals(AppConstants.CANCEL)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.Unsucessful)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				if (text.contains(AppConstants.CREATECONTRACT))
					label.setVisible(false);
				if (text.contains(AppConstants.Unsucessful))
					label.setVisible(false);
				if (text.equals("Mark Successful"))
					label.setVisible(false);
				if (text.equals(AppConstants.CREATEQUICKCONTRACT))
					label.setVisible(false);
				
			}
			
			
			//Ashwini Patil Date:20-09-2022
			if(isHideNavigationButtons()) {
				Console.log("in changeProcessLevel isHideNavigationButtons");
				if(text.equalsIgnoreCase(AppConstants.CREATECONTRACT)||text.equalsIgnoreCase(AppConstants.CREATEQUICKCONTRACT)||text.equalsIgnoreCase(AppConstants.NEW)||text.equalsIgnoreCase(AppConstants.VIEWLEAD)||text.equalsIgnoreCase("Revised Quotation"))
					label.setVisible(false);	
			}
		}

	}
	
	/**
	 * The method is responsible for changing Application state as per 
	 * status
	 */
	public void setMenuAsPerStatus()
	{
		
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
	}
	
	
	
	                  //////////////////////////// End Here //////////////////////////
	
	

	@Override
	public void setToNewState() {
		super.setToNewState();
	Date date=new Date();
		dbQuotationDate.setValue(date);	
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", "MakeQuotationDateDisable")
				&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
			this.dbQuotationDate.setEnabled(false);
		}
		
		if(salesPersonRestrictionFlag&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
			olbeSalesPerson.setEnabled(false);
		}
		
		
		
		/**
		 * Date: 04-08-2017 By Anil 
		 * we don't need to update to toggle level or header level button at new State
		 */
//		setMenuAsPerStatus();
		
		/**
		 * @author Vijay for pecopp when screen switch from lead to quotation then validuntil Date should calculate automatically
		 */
		if(!validateFlag){
			String dur = AppUtility.getForProcessConfigurartionIsActiveOrNot("QuotationValidateDuration");
			if(dur!=null && !dur.equals("")){
				Date dt = dbQuotationDate.getValue();
				int duration = Integer.parseInt(dur); 
				if(dt != null){
					CalendarUtil.addDaysToDate(dt, duration);
					dbValidUntill.setValue(dt);
				}
				
				
			}
			
		}
		 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
	  		{	olbcNumberRange.setEnabled(false);	  		
	  		}
		 /*
			 * Ashwini Patil
			 * Date:29-05-2024
			 * Ultima search want to map number range directly from branch so they want this field read only
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
				olbcNumberRange.setEnabled(false);
	}

	/**
	 * sets the id textbox with the passed count value.
	 *
	 * @param count the new count
	 */
	@Override
	public void setCount(int count)
	{
		tbQuotatinId.setValue(count+"");
	}


	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event)
	{
		if(event.getSource().equals(addPaymentTerms)){
			reactOnAddPaymentTerms();
		}
		
		
		//********************rohan changes here for custom quotation *******************
		
		if(event.getSource()==this.getCustomeQuotation()){
			
			if(this.getCustomeQuotation().getValue()==true){
				
				this.getCustomQuotationUpload().setEnable(true);
			}
			
			if(this.getCustomeQuotation().getValue()==false){
				
				this.getCustomQuotationUpload().setEnable(false);
			}
			
		}
		
		
		if(event.getSource().equals(addOtherCharges)){
			int size=chargesTable.getDataprovider().getList().size();
			if(this.doamtincltax.getValue()!=null){
				ProductOtherCharges prodCharges=new ProductOtherCharges();
				prodCharges.setAssessableAmount(this.getDoamtincltax().getValue());
				prodCharges.setFlagVal(false);
				prodCharges.setIndexCheck(size+1);
				this.chargesTable.getDataprovider().getList().add(prodCharges);
			}
		}
		if(event.getSource().equals(baddproducts)){
			Console.log("Inside Add Method");
			/**
			 * @author Vijay Date 08-02-2021
			 * Des when new customer created from customer then customer not loaded so updated code to load customer Master
			 */
			if(cust==null){
				checkCustomerStatus(getPersonInfoComposite().getIdValue());
			}
			if (AppUtility.checkForProcessConfigurartionIsActiveOrNot(
					"Quotation", "ChangeProductComposite")) {
				changeProductComposites = true;
			}else{
				changeProductComposites = false;
			}
			if(changeProductComposites){
					Console.log("Inside Change Product Composites");
					reactOnAddServiceProduct();
			}else{
			if(personInfoComposite.getId().getValue().equals("")){
				showDialogMessage("Please add customer information!");
			}
			else{
				if(olbbBranch.getValue()!=null){
					if(!prodInfoComposite.getProdCode().getValue().equals("")){
						System.out.println("PrintPCD"+prodInfoComposite.getProdCode().getValue());
						this.checkCustomerBranch(personInfoComposite.getIdValue());
						
						/* this validation is remove by rohan as per pest india request */     
						
//						boolean productValidation=validateProductDuplicate(prodInfoComposite.getProdCode().getValue().trim());
//						
//						if(productValidation==true){
//							showDialogMessage("Product already exists!");
//						}
//						else{
						
						/**
						 * @author Vijay Date 08-02-2021
						 * Des when new customer created from customer then customer not loaded so updated code to load customer Master
						 */
						if(cust==null){
							Timer timer = new Timer() {
								
								@Override
								public void run() {
									ProdctType(prodInfoComposite.getProdCode().getValue().trim(),prodInfoComposite.getProdID().getValue().trim());
									/**
									 * Date : 12-10-2017 BY ANIL
									 * After adding product ,product composite should be clear
									 */
									prodInfoComposite.clear();
								}
							};
							timer.schedule(3000);
							
						}
						else{
							this.ProdctType(this.prodInfoComposite.getProdCode().getValue().trim(),this.prodInfoComposite.getProdID().getValue().trim());
							/**
							 * Date : 12-10-2017 BY ANIL
							 * After adding product ,product composite should be clear
							 */
							prodInfoComposite.clear();
						}
//						}
					}
				}else{
					showDialogMessage("Please select branch !");
				}
			
		}
	}
		}
		
		if (event.getSource().equals(addPayTerms)) {
			
			reactOnAddPayTerms();
		}
		
		if(event.getSource().equals(btnTermsAndConditionAdd)){
			reactonAddTermsAndCondition();
		}
		
		if(event.getSource().equals(chkPrintAllTermsAndCondition)){
			if(chkPrintAllTermsAndCondition.getValue()== false){
				this.btnTermsAndConditionAdd.setEnabled(true);
			}
			else{
				this.btnTermsAndConditionAdd.setEnabled(false);
				this.termsAndConditionTable.clear();
				this.olbtermsAndConditionTitle.setSelectedIndex(0);
			}
		}
	}
	
	
	private void reactOnAddServiceProduct() {
		Console.log("Inside reactOnService product");
		
		if(personInfoComposite.getId().getValue().equals("")){
			showDialogMessage("Please add customer information!");
		}

		if(olbbBranch.getValue().equals("")){
			showDialogMessage("Please Select The Branch");
		}
		
		if (olbProductName.getSelectedIndex() == 0
				&&olbProductGroup.getSelectedIndex() == 0) {
			Console.log("Inside please select Product");
			showDialogMessage("Please select Product Group And Product Name!");
		}
		
        if(olbProductGroup.getSelectedIndex()!=0&&olbProductName.getSelectedIndex() == 0){
        	showDialogMessage("Please Select Product Name");
        }
        if(olbProductGroup.getSelectedIndex()==0&&olbProductName.getSelectedIndex()!=0){
        	showDialogMessage("Please Select Product Group");
        }
		ServiceProduct serProduct = olbProductName.getSelectedItem();

		Console.log("serice product code" + serProduct);

		if (olbProductName.getSelectedIndex() != 0&&olbProductGroup.getSelectedIndex() != 0) {
			Console.log("Inside the add method");
//			boolean productValidation = validateProductDuplicate(
//					serProduct.getCount(), serProduct.getProductCode());
//			if (productValidation == true) {
//				showDialogMessage("Product already exists!");
//			} else {
				Console.log("Inside Else");
				ProdctType(serProduct.getProductCode(),String.valueOf(serProduct.getCount()));
				
//			}
		}
	}

	private void reactOnAddPayTerms() {

		if (olbPaymentTerms.getSelectedIndex() == 0) {
			showDialogMessage("Please select payment terms");
			return;
		}

		if (olbPaymentTerms.getSelectedIndex() != 0) {

			ConfigCategory conCategory = olbPaymentTerms.getSelectedItem();
			if (conCategory != null) {

				String str = conCategory.getDescription();
				if (str.equals("")) {
					showDialogMessage("Please add Payment Days.");
					return;
				}

				int days = Integer.parseInt(str);
				System.out.println("Payment terms Days::::::::::::::" + days);

				if (saleslineitemquotationtable.getDataprovider().getList().size() == 0) {
					showDialogMessage("Please add Product");
					return;
				}

				int duration = getDurationFromProductTable();
				System.out.println("Duration::::::::: " + duration);

				if (duration == 0) {
					showDialogMessage("Please add duration!");
					return;
				}

				int noOfPayTerms = duration / days;
				System.out.println("No. of payment terms::::::::: "+ noOfPayTerms);

				if (noOfPayTerms == 0) {
					noOfPayTerms = 1;
				}

				Float no = (float) noOfPayTerms;
				float percent1 = 100 / no;
				System.out.println("Percent1::::::::: " + percent1);

				Double percent = (double) percent1;
				//rohan commentd this line for Date : 04/10/2017 
//				percent = Double.parseDouble(nf.format(percent));
				System.out.println("Percent2:::::::::  " + percent);

				int day = 0;
				double totalPer = 0;

				if (cbStartOfPeriod.getValue() == true) {
					day = 0;
				}

				if (cbStartOfPeriod.getValue() == false) {
					day = days;
				}

				/**
				 * Date : 08-08-2017 BY ANIL
				 * 
				 */
				if(dbPaymentDate.getValue()!=null){
					if(days<30&&noOfPayTerms>1){
						showDialogMessage("Payment term duration should be greater than 30 days for using payment date option.");
						return;
					}
				}
				int monthCounter=0;
				
				if(days==30){
					monthCounter=1;
				}else if(days==60){
					monthCounter=2;
				}else if(days==90){
					monthCounter=3;
				}else if(days==120){
					monthCounter=4;
				}else if(days==150){
					monthCounter=5;
				}else if(days==180){
					monthCounter=6;
				}else if(days==210){
					monthCounter=7;
				}else if(days==240){
					monthCounter=8;
				}else if(days==270){
					monthCounter=9;
				}else if(days==300){
					monthCounter=10;
				}else if(days==330){
					monthCounter=11;
				}else if(days>360&&days<=366){
					monthCounter=12;
				}
				Date paymentDate=null;
				if(dbPaymentDate.getValue()!=null){
					paymentDate=CalendarUtil.copyDate(dbPaymentDate.getValue());
				}
				/**
				 * End
				 */
				ArrayList<PaymentTerms> ptermsList = new ArrayList<PaymentTerms>();
				
				System.out.println("month counter "+monthCounter);
				for (int i = 1; i <= noOfPayTerms; i++) {
					Date paymentDt=CalendarUtil.copyDate(paymentDate);
					
					
					totalPer = totalPer + percent;
					if (i == noOfPayTerms) {
						if (totalPer == 100) {
							PaymentTerms pterm = new PaymentTerms();
							pterm.setPayTermDays(day);
							pterm.setPayTermPercent(percent);
							pterm.setPayTermComment(i + " Payment");
							/**
							 * Date : 08-08-2017 BY ANIL
							 */
							if(dbPaymentDate.getValue()!=null){
								pterm.setPaymentDate(paymentDt);
							}
							/**
							 * End
							 */
							ptermsList.add(pterm);

						} else {
							double diff = 0;
							if (totalPer < 100) {
								diff = 100 - totalPer;
								percent = percent + diff;
								//rohan commented this code on date date 04/10/2017
//								percent = Double.parseDouble(nf.format(percent));

								PaymentTerms pterm = new PaymentTerms();
								pterm.setPayTermDays(day);
								pterm.setPayTermPercent(percent);
								pterm.setPayTermComment(i + " Payment");
								/**
								 * Date : 08-08-2017 BY ANIL
								 */
								if(dbPaymentDate.getValue()!=null){
									pterm.setPaymentDate(paymentDt);
								}
								/**
								 * End
								 */
								ptermsList.add(pterm);
							} else {
								diff = totalPer - 100;
								percent = percent - diff;
								//rohan commented this code on date date 04/10/2017
//								percent = Double.parseDouble(nf.format(percent));

								PaymentTerms pterm = new PaymentTerms();
								pterm.setPayTermDays(day);
								pterm.setPayTermPercent(percent);
								pterm.setPayTermComment(i + " Payment");
								/**
								 * Date : 08-08-2017 BY ANIL
								 */
								if(dbPaymentDate.getValue()!=null){
									pterm.setPaymentDate(paymentDt);
								}
								/**
								 * End
								 */
								ptermsList.add(pterm);
							}
						}

					} else {
						PaymentTerms pterm = new PaymentTerms();
						pterm.setPayTermDays(day);
						pterm.setPayTermPercent(percent);
						pterm.setPayTermComment(i + " Payment");
						/**
						 * Date : 08-08-2017 BY ANIL
						 */
						if(dbPaymentDate.getValue()!=null){
							pterm.setPaymentDate(paymentDt);
						}
						/**
						 * End
						 */
						ptermsList.add(pterm);
					}
					day = day + days;
					
					if(dbPaymentDate.getValue()!=null){
						System.out.println(" BF DATE :: "+paymentDate);
						int month=paymentDate.getMonth();
						month=month+monthCounter;
						paymentDate.setMonth(month);
						System.out.println(" AF DATE :: "+paymentDate);
					}
				}
				
				
				paymentTermsTable.getDataprovider().setList(ptermsList);
			}
		}}

	
	private int getDurationFromProductTable() {
		int maxDur = 0;
		int currDur = 0;
		for (int i = 0; i < saleslineitemquotationtable.getDataprovider()
				.getList().size(); i++) {
			currDur = saleslineitemquotationtable.getDataprovider().getList()
					.get(i).getDuration();
			if (maxDur <= currDur) {
				maxDur = currDur;
			}
		}
		return maxDur;
	}
	
	public void ProdctType(String pcode,String productId)
	{
		System.out.println("Started");
		final GenricServiceAsync genasync=GWT.create(GenricService.class);
		Vector<Filter> filtervec=new Vector<Filter>();
		
		
		int prodId=Integer.parseInt(productId);
		
		final MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("productCode");
		filter.setStringValue(pcode.trim());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
		showWaitSymbol();
		 Timer timer=new Timer() 
    	 {
				@Override
				public void run() {
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("An Unexpected error occurred!");
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					if(result.size()==0)
					{
						showDialogMessage("Please check whether product status is active or not!");
					}
					
					SalesLineItem lis=null;
					for(SuperModel model:result)
					{
						SuperProduct superProdEntity = (SuperProduct)model;
//						lis=AppUtility.ReactOnAddProductComposite(superProdEntity);
						Branch branchEntity = olbbBranch.getSelectedItem();
						lis=AppUtility.ReactOnAddProductComposite(superProdEntity,cust,branchEntity,true);


						lis.setQuantity(1.0);
						lis.setProductSrNo(productSrNo+1);
						/**
						 * nidhi
						 * 22-06-2017
						 */
						lis.setStartDate(new Date());
						
						/**
						 * 
						 */
						
						/**
						 * Date 02-05-2018 
						 * Developer : Vijay
						 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
						 * so i have updated below code using hashmap.  
						 */
						
//						/*
//						 * Nidhi 23-06-2017
//						 * 
//						 */
//						
//						String branchArray[] = AppUtility.getCustomersBranchsForSchedulingInfo(customerbranchlist, olbbBranch.getValue());
//						
//						if(branchArray[0]!=null){
//							lis.setBranchSchedulingInfo(branchArray[0]);
//						}
//						if(branchArray[1]!=null){
//							lis.setBranchSchedulingInfo(branchArray[1]);
//						}
//						if(branchArray[2]!=null){
//							lis.setBranchSchedulingInfo(branchArray[2]);
//						}
//						if(branchArray[3]!=null){
//							lis.setBranchSchedulingInfo(branchArray[3]);
//						}
//						if(branchArray[4]!=null){
//							lis.setBranchSchedulingInfo(branchArray[4]);
//						}
//						if(branchArray[5]!=null){
//							lis.setBranchSchedulingInfo(branchArray[5]);
//						}				
//						if(branchArray[6]!=null){
//							lis.setBranchSchedulingInfo(branchArray[6]);
//						}
//						
//						if(branchArray[7]!=null){
//							lis.setBranchSchedulingInfo(branchArray[7]);
//						}
//						
//						if(branchArray[8]!=null){
//							lis.setBranchSchedulingInfo(branchArray[8]);
//						}
//						if(branchArray[9]!=null){
//							lis.setBranchSchedulingInfo(branchArray[9]);
//						}
//					
//						
//						/*
//						 * 
//						 * End
//						 */
						
						/**
						 * Date 02-05-2018 By vijay updated code using hashmap for customer branches scheduling services
						 * above old code commented
						 */
						ArrayList<BranchWiseScheduling> branchlist = AppUtility.getCustomerBranchSchedulinglistInfo(customerbranchlist,olbbBranch.getValue(),cust.getUnitOfMeasurement(),cust.getArea());
						Integer prodSrNo = lis.getProductSrNo();
						HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
						customerBranchlist.put(prodSrNo, branchlist);
						lis.setCustomerBranchSchedulingInfo(customerBranchlist);
						
						/**
						 * ends here	
						 */
						
					    getSaleslineitemquotationtable().getDataprovider().getList().add(lis);
					    productSrNo++;
					    System.out.println("Product value "+productSrNo);
					    
						if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT ){
						    addnewproductFlag = true;
						}

					}
					
					if(serviceScheduleTable.getDataprovider().getList().size() == 0){
						
						 List<ServiceSchedule> popuplist=reactfordefaultTable(lis);
						 getServiceScheduleTable().getDataprovider().setList(popuplist);
						 
						}else{
							
							 List<ServiceSchedule> privlist=serviceScheduleTable.getDataprovider().getList();
									 
							 List<ServiceSchedule> popuplist=reactfordefaultTable(lis);
							 List<ServiceSchedule> popuplist1=new ArrayList<ServiceSchedule>();
							 
							 popuplist1.addAll(privlist);
							 popuplist1.addAll(popuplist);
							 getServiceScheduleTable().getDataprovider().setList(popuplist1);
							 
							}
					
					/**
					 * nidhi |*|
					 * 8-1-2019
					 */
					getServiceMaterialBomList();
					/**
					 * end
					 */
					}
				 });
				hideWaitSymbol();
 				}
	 	  };
          timer.schedule(3000); 
	}
	
	
	
	/******************************React On Add Payment Terms Method*******************************/
	/**
	 * This method is called when add button of payment terms is clicked.
	 * This method involves the inclusion of payment terms in the corresponding table
	 */
	
	public void reactOnAddPaymentTerms()
	{
		System.out.println("In method of payemtn terms");	
		int payTrmDays=0;
		double payTrmPercent=0;
		String payTrmComment="";
		boolean flag=true;
		
		if(tbcomment.getValue()==null||tbcomment.getValue().equals(""))
		{
			this.showDialogMessage("Comment cannot be empty!");
		}
		
		boolean checkTermDays=this.validatePaymentTrmDays(ibdays.getValue());
		if(checkTermDays==false)
		{
			if(chk==0){
				showDialogMessage("Days already defined.Please enter unique days!");
			}
//			clearPaymentTermsFields();
			flag=false;
		}
		
		
		if(ibdays.getValue()!=null&&ibdays.getValue()>=0){
			payTrmDays=ibdays.getValue();
		}
		else{
			showDialogMessage("Days cannot be empty!");
			flag=false;
		}
		
		
		if(dopercent.getValue()!=null&&dopercent.getValue()>=0)
		{
			if(dopercent.getValue()>100){
				showDialogMessage("Percent cannot be greater than 100");
				dopercent.setValue(null);
				flag=false;
			}
			else if(dopercent.getValue()==0){
				showDialogMessage("Percent should be greater than 0!");
				dopercent.setValue(null);
			}
			else{
			payTrmPercent=dopercent.getValue();
			}
		}
		else{
			showDialogMessage("Percent cannot be empty!");
		}
		
		if(tbcomment.getValue()!=null){
			payTrmComment=tbcomment.getValue();
		}
		
		if(!payTrmComment.equals("")&&payTrmPercent!=0&&flag==true)
		{
			PaymentTerms payTerms=new PaymentTerms();
			payTerms.setPayTermDays(payTrmDays);
			payTerms.setPayTermPercent(payTrmPercent);
			payTerms.setPayTermComment(payTrmComment);
			paymentTermsTable.getDataprovider().getList().add(payTerms);
			clearPaymentTermsFields();
		}
	}
	
	/*****************************************Add Product Taxes Method***********************************/
	
	/**
	 * This method is called when a product is added to SalesLineItem table.
	 * Taxes related to corresponding products will be added in ProductTaxesTable.
	 * @throws Exception
	 */
	public void addProdTaxes() throws Exception
	{
		List<SalesLineItem> salesLineItemLis=this.saleslineitemquotationtable.getDataprovider().getList();
		List<ProductOtherCharges> taxList=this.prodTaxTable.getDataprovider().getList();
		NumberFormat nf=NumberFormat.getFormat("0.00");
		for(int i=0;i<salesLineItemLis.size();i++)
		{
			double priceqty=0,taxPrice=0;
//			if(salesLineItemLis.get(i).getPercentageDiscount()==null){
//				taxPrice=this.getSaleslineitemquotationtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//				priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice)*salesLineItemLis.get(i).getQty();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}
//			if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
//				taxPrice=this.getSaleslineitemquotationtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//				priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
//				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
//				priceqty=priceqty*salesLineItemLis.get(i).getQty();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}
//			System.out.println("Total Prodcut"+priceqty);
			
			
			//****************new code for discount by rohan ***************
			
			/**
			 * rohan removed qty form all the price calculation required for NBHC
			 * date : 14-10-2016
			 * i have removed (salesLineItemLis.get(i).getQty()) from all the calculation
			 * 
			 * i have added Area in calculations in quotation 
			 * Date : 16/12/2016 
			 */
			System.out.println(" AREA Value ===== "+salesLineItemLis.get(i).getArea());
			if((salesLineItemLis.get(i).getPercentageDiscount()==null && salesLineItemLis.get(i).getPercentageDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
						
				//   old code 
//						System.out.println("inside both 0 condition");
//						
//						taxPrice=this.getSaleslineitemquotationtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//						priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
//						priceqty=Double.parseDouble(nf.format(priceqty));
				
				//  new code 
				taxPrice=this.getSaleslineitemquotationtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
					double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
					priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice)*squareArea;
					priceqty=Double.parseDouble(nf.format(priceqty));
					System.out.println("RRRRRRRRRRRRRR price == "+priceqty);
					}else{
						System.out.println("old code");
						priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
						priceqty=Double.parseDouble(nf.format(priceqty));
					}
				
					}
					
					else if(((salesLineItemLis.get(i).getPercentageDiscount()!=null)&& (salesLineItemLis.get(i).getDiscountAmt()!=0))){
						
						System.out.println("inside both not null condition");
						
						taxPrice=this.getSaleslineitemquotationtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
						priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
//						priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
//						priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
////						priceqty=priceqty*salesLineItemLis.get(i).getQty();
//						priceqty=Double.parseDouble(nf.format(priceqty));
						
						
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=priceqty*squareArea;
							priceqty= priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							priceqty=Double.parseDouble(nf.format(priceqty));
						}else{
//							priceqty=priceqty*salesLineItemLis.get(i).getQty();
							priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							priceqty=Double.parseDouble(nf.format(priceqty));
							
						}
						
					}
					else 
					{
						System.out.println("inside oneof the null condition");
						
						
						taxPrice=this.getSaleslineitemquotationtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
						priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
						
						if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
//						priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
						
						
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
							if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
								Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
								priceqty = priceqty*squareArea;
								priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							}else{
//								priceqty=priceqty*salesLineItemLis.get(i).getQty();
								priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							}
						}
						else 
						{
//						priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
							if(!salesLineItemLis.get(i).getArea().equals("NA")){
								Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
								priceqty = priceqty*squareArea;
								priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							}else{
//								priceqty=priceqty*salesLineItemLis.get(i).getQty();
								priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							}
						}
//						priceqty=priceqty*salesLineItemLis.get(i).getQty();
						priceqty=Double.parseDouble(nf.format(priceqty));
					}
					
					//****************changes ends here *********************************
			
			/* bellow code is commented by vijay because we when select vat or service tax is NA mean percentage 0 then it shows in prod tax table tax name & value 0. 00.
			 * 
			 */
			
//			if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
//				ProductOtherCharges pocentity=new ProductOtherCharges();
//				pocentity.setChargeName("VAT");
//				pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
//				pocentity.setIndexCheck(i+1);
//				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
//				if(indexValue!=-1){
//					pocentity.setAssessableAmount(0);
//					this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//				}
//				
//				if(indexValue==-1){
//					pocentity.setAssessableAmount(0);
//				}
//				this.prodTaxTable.getDataprovider().getList().add(pocentity);
//			}
//			
//			if(salesLineItemLis.get(i).getServiceTax().getPercentage()==0){
//				ProductOtherCharges pocentity=new ProductOtherCharges();
//				pocentity.setChargeName("Service Tax");
//				pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
//				pocentity.setIndexCheck(i+1);
//				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
//				if(indexValue!=-1){
//					pocentity.setAssessableAmount(0);
//					this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//				}
//				
//				if(indexValue==-1){
//					pocentity.setAssessableAmount(0);
//				}
//				this.prodTaxTable.getDataprovider().getList().add(pocentity);
//			}
//
//			
//			if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
//				System.out.println("Vattx");
//				ProductOtherCharges pocentity=new ProductOtherCharges();
//				pocentity.setChargeName("VAT");
//				pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
//				pocentity.setIndexCheck(i+1);
//				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
//				
//				if(indexValue!=-1){
//					System.out.println("Index As Not -1");
//					pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//					this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//				}
//				if(indexValue==-1){
//					pocentity.setAssessableAmount(priceqty);
//				}
//				this.prodTaxTable.getDataprovider().getList().add(pocentity);
//			}
//			
//			if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
//				System.out.println("ServiceTx");
//				ProductOtherCharges pocentity=new ProductOtherCharges();
//				pocentity.setChargeName("Service Tax");
//				pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
//				pocentity.setIndexCheck(i+1);
//				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
//				if(indexValue!=-1){
//					System.out.println("Index As Not -1");
//					double assessValue=0;
//					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
//						assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
//						pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
//						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//					}
//					if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
//						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//					}
//				}
//				if(indexValue==-1){
//					System.out.println("Index As -1");
//					double assessVal=0;
//					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
//						assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
//						pocentity.setAssessableAmount(assessVal);
//					}
//					if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
//						pocentity.setAssessableAmount(priceqty);
//					}
//					
//				}
//				this.prodTaxTable.getDataprovider().getList().add(pocentity);
//			}
			

			/**
		     * Date 1-09-2017 added by vijay for if discount amount is there then taxes calculation based on after
		     * discount Total amount
		     */
			if(dbDiscountAmt.getValue()!=null && dbDiscountAmt.getValue()!=0){
//				System.out.println("hi vijay inside discount amount");
//				priceqty = dbFinalTotalAmt.getValue();
				
				/**
				 * Date : 23-09-2017 BY ANIL
				 * 
				 */
				if(isValidForDiscount()){
					priceqty = dbFinalTotalAmt.getValue()/saleslineitemquotationtable.getDataprovider().getList().size();
					System.out.println("hi vijay inside discount amount "+priceqty);
				}else{
					dbDiscountAmt.setValue(0.0);
					dbFinalTotalAmt.setValue(dototalamt.getValue());
				}
			}
			
			/**
			 * ends here
			 */
			
			/**
			 *  New code added by vijay beacuse when we select vat or service tax NA means percentage 0 then product tax table will show blank
			 */
			
			if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
				if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside vat GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					//   old code commented by rohan on date : 22-6-2017
	//				pocentity.setChargeName("VAT");
					//   new code by rohan Date 22-6-2017 
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(priceqty);
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
			else
				{
					System.out.println("Vattx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("VAT");
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
			
			if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
				if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside service GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					//   old code commented by rohan on date : 22-6-2017
	//				pocentity.setChargeName("VAT");
					//   new code by rohan Date 22-6-2017 
					System.out.println("salesLineItemLis.get(i).getServiceTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(priceqty);
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
				else{
					System.out.println("ServiceTx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("Service Tax");
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						double assessValue=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						
					}
					if(indexValue==-1){
						System.out.println("Index As -1");
						double assessVal=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessVal);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty);
						}
						
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
					System.out.println("DONE ....");
				}
				
			}
		}
	}

	/*********************************Check Tax Percent*****************************************/
	/**
	 * This method returns 
	 * @param taxValue
	 * @param taxName
	 * @return
	 */
	
	public int checkTaxPercent(double taxValue,String taxName)
	{
		List<ProductOtherCharges> taxesList=this.prodTaxTable.getDataprovider().getList();
		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("Service")){
				
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					System.out.println("zero");
					return i;
				}
			}
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					System.out.println("zero");
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("CGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("SGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("IGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
			}
		}
		return -1;
	}
	
	public boolean checkProducts(String pcode)
	{
		List<SalesLineItem> salesitemlis=saleslineitemquotationtable.getDataprovider().getList();
		for(int i=0;i<salesitemlis.size();i++)
		{
			if(salesitemlis.get(i).getProductCode().equals(pcode)||salesitemlis.get(i).getProductCode()==pcode){
				return true;
			}
		}
		return false;
	}
	
	
	
	
	/*************************Update Charges Table****************************************/
	/**
	 * This method updates the charges table.
	 * The table is updated when any row value is changed through field updater in table i.e
	 * row change event is fired
	 */
	
	public void updateChargesTable()
	{
		List<ProductOtherCharges> prodOtrLis=this.chargesTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> prodOtrArr=new ArrayList<ProductOtherCharges>();
		prodOtrArr.addAll(prodOtrLis);
		
		this.chargesTable.connectToLocal();
		System.out.println("prodo"+prodOtrArr.size());
		for(int i=0;i<prodOtrArr.size();i++)
		{
			ProductOtherCharges prodChargesEnt=new ProductOtherCharges();
			prodChargesEnt.setChargeName(prodOtrArr.get(i).getChargeName());
			prodChargesEnt.setChargePercent(prodOtrArr.get(i).getChargePercent());
			prodChargesEnt.setChargeAbsValue(prodOtrArr.get(i).getChargeAbsValue());
			//prodChargesEnt.setAssessableAmount(prodOtrArr.get(i).getAssessableAmount());
			prodChargesEnt.setFlagVal(prodOtrArr.get(i).getFlagVal());
			prodChargesEnt.setIndexCheck(prodOtrArr.get(i).getIndexCheck());
			if(prodOtrArr.get(i).getFlagVal()==false){
				prodChargesEnt.setAssessableAmount(this.getDoamtincltax().getValue());
			}
			if(prodOtrArr.get(i).getFlagVal()==true){
				double assesableVal=this.retrieveSurchargeAssessable(prodOtrArr.get(i).getIndexCheck());
				prodChargesEnt.setAssessableAmount(assesableVal);
			}
			
			this.chargesTable.getDataprovider().getList().add(prodChargesEnt);
		}
	}
	
	public double retrieveSurchargeAssessable(int indexValue)
	{
		List<ProductOtherCharges> otrChrgLis=this.chargesTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> otrChrgArr=new ArrayList<ProductOtherCharges>();
		otrChrgArr.addAll(otrChrgLis);
	
		double getAssessVal=0;
		for(int i=0;i<otrChrgArr.size();i++)
		{
			if(otrChrgArr.get(i).getFlagVal()==false&&otrChrgArr.get(i).getIndexCheck()==indexValue)
			{
				if(otrChrgArr.get(i).getChargePercent()!=0){
					getAssessVal=otrChrgArr.get(i).getAssessableAmount()*otrChrgArr.get(i).getChargePercent()/100;
				}
				if(otrChrgArr.get(i).getChargeAbsValue()!=0){
					getAssessVal=otrChrgArr.get(i).getChargeAbsValue();
				}
			}
		}
		return getAssessVal;
	}


	/**
	 * Validate For Duplicate Products
	 */
	
	public boolean validateProductDuplicate(String pcode)
	{
		List<SalesLineItem> salesItemLis=this.getSaleslineitemquotationtable().getDataprovider().getList();
		for(int i=0;i<salesItemLis.size();i++)
		{
			System.out.println("salesl"+salesItemLis.get(i).getProductCode());
			if(salesItemLis.get(i).getProductCode().trim().equals(pcode)){
				return true;
			}
		}
		System.out.println("Out");
		return false;
	}

	/**
	 * Sets the one to many combo.
	 */
	public void setOneToManyCombo()
	   {
		 CategoryTypeFactory.initiateOneManyFunctionality(olbQuotationCategory,olbQuotationType);  
		  AppUtility.makeTypeListBoxLive(this.olbQuotationType, Screen.QUOTATIONCATEGORY);
	   } 
	   
	


	/* (non-Javadoc)
	 * @see com.slicktechnologies.client.approvalutility.ApprovableScreen#getstatustextbox()
	 */
	@Override
	public TextBox getstatustextbox() {
		
		return this.tbQuotationStatus;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		super.clear();
		getstatustextbox().setText(Quotation.CREATED);
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
		this.tbContractId.setEnabled(false);
		this.tbQuotationStatus.setEnabled(false);
		this.tbLeadId.setEnabled(false);
		this.tbQuotatinId.setEnabled(false);
		this.saleslineitemquotationtable.setEnable(state);
		this.paymentTermsTable.setEnable(state);
		this.chargesTable.setEnable(state);
		this.prodTaxTable.setEnable(state);
		this.doamtincltax.setEnabled(false);
		this.donetpayamt.setEnabled(false);
		this.dototalamt.setEnabled(false);
		this.btnsecurityDep.setVisible(false);
		this.tbremark.setEnabled(false);
		this.f_btnservice.setEnabled(true);
		this.tbTicketNumber.setEnabled(false);
		this.btnCosting.setEnabled(true);
		
		//Date 01-09-2017 added by vijay for total amount discount and roundoff
		this.dbDiscountAmt.setEnabled(state);
		this.dbFinalTotalAmt.setEnabled(false);
		this.tbroundOffAmt.setEnabled(state);
		dbGrandTotalAmt.setEnabled(false);
		/**
		 * nidhi
		 * 31-07-2018
		 * for set  quotaion created employee 
		 */
		this.tbCreatedBy.setEnabled(false);
		if(quotationDateDisable){
			this.dbQuotationDate.setEnabled(false);
		}
		if(salesPersonRestrictionFlag){
			this.olbeSalesPerson.setEnabled(false);
		}
		 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "MakeCategoryAndTypeDisable")
				 &&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
	   		 this.olbQuotationCategory.setEnabled(false);
	   		 this.olbQuotationType.setEnabled(false);
	   	  }
		 
		 if(tbQuotatinId.getValue()!=null&&!tbQuotatinId.getValue().equals("")){
				this.olbcNumberRange.setEnabled(false);
		 } 
		 objPaymentMode.setEnabled(state);
		 termsAndConditionTable.setEnable(state);
		 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
	  		{	olbcNumberRange.setEnabled(false);	  		
	  		}
		 this.tbZohoQuotID.setEnabled(false);
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		setMenuAsPerStatus();
	
		SuperModel model=new Quotation();
		model=quotationObject;
		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.QUOTATION, quotationObject.getCount(), Integer.parseInt(personInfoComposite.getId().getValue()),personInfoComposite.getName().getValue(),Long.parseLong(personInfoComposite.getPhone().getValue()), false, model, null);
	
		String mainScreenLabel="Quotation Information";
		if(quotationObject!=null&&quotationObject.getCount()!=0){
			mainScreenLabel=quotationObject.getCount()+"/"+quotationObject.getStatus()+"/"+AppUtility.parseDate(quotationObject.getCreationDate());
		}
		
		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
	}

	@Override
	 public boolean validate()
	    {
		  boolean superValidate = super.validate();
	        if(saleslineitemquotationtable.getDataprovider().getList().size() == 0)
	        {
	            showDialogMessage("Please Select at least one Product!");
	            return false;
	        }
	        
	        boolean valPayPercent=this.validatePaymentTermPercent();
	        
	        
	        System.out.println("Rohan Inside validation"+valPayPercent);
	        if(valPayPercent==false)
	        {
	        	showDialogMessage("Payment Term Not 100%, Please Correct!");
	        	return false;
	        }
	        
	        /**
	         * Date : 14-11-2017 BY MANISHA
	         * 
	         */
//	        if(validateQuotationPrice()>0){
//	        	showDialogMessage("Please enter product price greater than 0!");
//	        	return false;
//	        }
	        
	        
	        if(!validateServiceQuotProdQty()){
				showDialogMessage("Please enter appropriate quantity required!");
				return false;
			}
			
	        
	      
	        if(superValidate==false)
	        {
	        	 return false;
	        }
	        
	        if(validateServicePopupBranch()==false){
	        	this.showDialogMessage("Please Select Branch in Schedule Popup");
	        	return false;
	        }
	        
	        /**
	         * Date 2 March 2017
	         * added by vijay
	         * Quotation valid until date must be greater or equal to Quotation date
	         */
	        if(dbValidUntill.getValue().before(dbQuotationDate.getValue())){
	        	showDialogMessage("Quotation Valid until date must be greater or equal quotation date!");
	        	return false;
	        }
	        /**
	         * Ends here
	         */
	        
	        
	        
	        /**
	         * Date : 27-05-2017 By ANIL
	         */
	        System.out.println("IN SIDE VALIDATION METHOD ");
	        System.out.println("Material List SiZE "+matCostList.size()+"Man Power Cost List Size "+manPowCostList.size());
	      
	        if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", "OnlyForNBHC")){
		        boolean matFlag=false;
		        boolean manFlag=false;
		        String msg="";
		        
		        if(matCostList==null||matCostList.size()==0){
		        	msg=msg+"Please fill material costing details.\n";
		        	matFlag=true;
		        }
		        if(manPowCostList==null||manPowCostList.size()==0){
		        	msg=msg+"Please fill man power costing details.\n";
		        	manFlag=true;
		        }
		        
		        if(matFlag||manFlag){
		        	showDialogMessage(msg);
		        	return false;
		        }
	        }
	        
	        /**
	         * End
	         */
	        
	        /**
			 * Date 03-07-2017 added by vijay tax validation
			 */
			List<ProductOtherCharges> prodtaxlist = this.prodTaxTable.getDataprovider().getList();
			for(int i=0;i<prodtaxlist.size();i++){
				
				if(dbQuotationDate.getValue()!=null && dbQuotationDate.getValue().before(date)){
					if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CGST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("SGST")
							|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("IGST")){
						showDialogMessage("As Quotation date "+fmt.format(dbQuotationDate.getValue())+" GST Tax is not applicable");
						return false;
					}
				}
				/**
				 * @author Anil @since 21-07-2021
				 * Issue raised by Rahul for Innovative Pest(Thailand) 
				 * Vat is applicable there so system should not through validation for VAT
				 */
//				else if(dbQuotationDate.getValue()!=null && dbQuotationDate.getValue().equals(date) || dbQuotationDate.getValue().after(date)){
//					if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("Service Tax")
//							|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("VAT")){
//						showDialogMessage("As Quotation date "+fmt.format(dbQuotationDate.getValue())+" VAT/CST/Service Tax is not applicable");
//						return false;
//					}
//				}
			}
			/**
			 * ends here
			 */
			
			/**
			 * Date 07-02-2019 by Vijay 
			 * Des :- As per vaishali madam they dont want this validation
			 * because they will not maintain BOM for all services so for not able to create contracts 
			 */
			
//			/**
//			 * nidhi
//			 * 7-01-2019 |*|
//			 * for validate bom area and unit
//			 */
//			if(LoginPresenter.billofMaterialActive && bomValidationActive){
//				boolean flag = validateAreaAndUnit();
//				if(!flag){
//					showDialogMessage("Unit of mesuarement or area is not define for selected branch ."
//							+ "\n Please define area and unit of masuarement.");
//					return flag;
//				}
//			}
			/**
			 * ends here
			 */
			
			/**
			 * @author Vijay Date 11-02-2022
			 * Des :- if duplicate Sr no found in product list then here not processing payment document
			 * issue pecopp having some lead duplicate SR no so creating double services
			 */
			if(validateProductSerialNumber()){
				return true;
			}
	       
	        return true;
	    }

	public boolean validateAreaAndUnit(){
		boolean flag = true ,bomCheck = true;
		
		for(SalesLineItem item:saleslineitemquotationtable.getDataprovider().getList()){
			
			 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(item.getPrduct());
			 if(billPrDt!=null){
					ArrayList<BranchWiseScheduling> customerBrancheslist = new ArrayList<BranchWiseScheduling>();
					customerBrancheslist.addAll(item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo()));
					
					if(LoginPresenter.billofMaterialActive && customerBrancheslist!=null && customerBrancheslist.size()>0 && bomValidationActive){
						for(BranchWiseScheduling branch : customerBrancheslist){
							if(branch.isCheck() )
								{
								if(branch.getUnitOfMeasurement()==null){
									flag = false;
									break;
								}
								if( branch.getArea()==0 ||  branch.getUnitOfMeasurement().trim().length() ==0){
										flag = false;
										break;
								}
								
								UnitConversionUtility unitConver = new UnitConversionUtility();

								 
								 if(!unitConver.varifyUnitConversion(billPrDt,branch.getUnitOfMeasurement())){
									 showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
									 flag = false;
										break;
								 }
							}
						}
					}
			 }else{
				 bomCheck = false;
				 flag = false;
				 showDialogMessage("Bill of materila is not created for " + item.getProductName() + " service. Please create Bom first. \n "
				 		+ " You are not allow to save Quotation with this service.");
				 break;
			 }
		}
		return flag;
	}
	@Override
	public void setToEditState() {
	
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		
		if(salesPersonRestrictionFlag)
		{
			olbeSalesPerson.setEnabled(false);
		}
		
		findMaxServiceSrNumber();
		changeStatusToCreated();
		

		String mainScreenLabel="Quotation Information";
		if(quotationObject!=null&&quotationObject.getCount()!=0){
			mainScreenLabel=quotationObject.getCount()+"/"+quotationObject.getStatus()+"/"+AppUtility.parseDate(quotationObject.getCreationDate());
		}
		
		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);

		QuotationPresenter.updateAddressFlag = false;
		
		if(chkPrintAllTermsAndCondition.getValue()){
			btnTermsAndConditionAdd.setEnabled(false);
		}
		else{
			btnTermsAndConditionAdd.setEnabled(true);
		}

	}
	
	private void findMaxServiceSrNumber() {
		
		int maxSrNumber = 0 ;
		System.out.println("rohan in side max service number");
		
		List<SalesLineItem> list = this.saleslineitemquotationtable.getDataprovider().getList();
		System.out.println("mazza list size "+list.size());
		
		Comparator<SalesLineItem> compare = new  Comparator<SalesLineItem>() {
			
			@Override
			public int compare(SalesLineItem o1, SalesLineItem o2) {
				// TODO Auto-generated method stub
				return Integer.compare(o2.getProductSrNo(), o1.getProductSrNo());
			}
		};
		Collections.sort(list,compare);
		
			System.out.println("product sr number "+list.get(0).getProductSrNo());
			maxSrNumber = list.get(0).getProductSrNo();
			productSrNo = maxSrNumber;
	}

	public void changeStatusToCreated()
	{
		Quotation entity=(Quotation) presenter.getModel();
		String status=entity.getStatus();
		
		if(Quotation.REJECTED.equals(status.trim()))
		{
			this.tbQuotationStatus.setValue(Quotation.CREATED);
		}
	}

	
	
	
	public boolean validateServiceQuotProdQty()
	{
		List<SalesLineItem> quotlis=saleslineitemquotationtable.getDataprovider().getList();
		int ctr=0;
		for(int i=0;i<quotlis.size();i++)
		{
			if(quotlis.get(i).getQty()==0)
			{
				ctr=ctr+1;
			}
		}
		
		if(ctr==0){
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 *  Validation for selection of branch while scheduling services. Method called on  saving data i.e in
	 *  validate method
	 * @return
	 */
	public boolean validateServicePopupBranch(){
		
		if(customerbranchlist.size()!=0){
			List<ServiceSchedule> prodservicelist=this.serviceScheduleTable.getDataprovider().getList();
			System.out.println("list size === "+prodservicelist.size());
			for(int i=0;i<prodservicelist.size();i++){
				//  rohan added this code for validating while saving data   
				if(prodservicelist.get(i).getScheduleProBranch()==null || prodservicelist.get(i).getScheduleProBranch().equalsIgnoreCase("SELECT") ){
					System.out.println("inside false");
					return false;
				}
			}
			
			return true;
			
			}else{
				return true;
			}
	}
	
/********************************Clear Payment Terms Fields*******************************************/
	
	public void clearPaymentTermsFields()
	{
		ibdays.setValue(null);
		dopercent.setValue(null);
		tbcomment.setValue("");
	}
	
	
	/*******************************Validate Payment Term Percent*****************************************/
	/**
	 * This method validates payment term percent. i.e the total of percent should be equal to 100
	 * @return
	 */
	
	public boolean validatePaymentTermPercent()
	{
		List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
		double totalPayPercent=0.0;
		System.out.println("list size"+payTermsLis.size());
		for(int i=0;i<payTermsLis.size();i++)
		{
			System.out.println(totalPayPercent+"=="+totalPayPercent+payTermsLis.get(i).getPayTermPercent());
			totalPayPercent=totalPayPercent+payTermsLis.get(i).getPayTermPercent();
		}
		System.out.println("Validation of Payment Percent as 100=="+Math.round(totalPayPercent));
		
		if(Math.round(totalPayPercent)!=100)
		{
			return false;
		}
		
		return true;
	}
	
	/********************************Validate Payment Terms Days******************************************/
	/**
	 * This method represents the validation of payment terms days
	 * @param retrDays
	 * @return
	 */
	
	int chk=0;
	public boolean validatePaymentTrmDays(int retrDays)
	{
		List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
		for(int i=0;i<payTermsLis.size();i++)
		{
			if(retrDays==payTermsLis.get(i).getPayTermDays())
			{
				return false;
			}
			if(retrDays<payTermsLis.get(i).getPayTermDays())
			{
				showDialogMessage("Days should be defined in increasing order!");
				chk=1;
				return false;
			}
			else{
				chk=0;
			}
			if(retrDays>10000){
				showDialogMessage("Days cannot be greater than 4 digits!");
				return false;
			}
		}
		return true;
	}
	
	public int validateQuotationPrice()
	{
		List<SalesLineItem> salesItemLis=this.getSaleslineitemquotationtable().getDataprovider().getList();
		int ctr=0;
		for(int i=0;i<salesItemLis.size();i++)
		{
			if(salesItemLis.get(i).getPrice()==0){
				ctr=ctr+1;
			}
		}
		
		return ctr;
	}
	
	
	
	
	/**********************************Change Widgets Method******************************************/
	/**
	 * This method represents changing the widgets style. Used for modifying payment composite
	 * widget fields
	 */
	
	private void changeWidgets()
	{
		//  For changing the text alignments.
		this.changeTextAlignment(dototalamt);
		this.changeTextAlignment(donetpayamt);
		this.changeTextAlignment(doamtincltax);
		
		/** Date 01-09-2017 added by vijay for total amount discount and roundoff **/
		this.changeTextAlignment(dbDiscountAmt);
		this.changeTextAlignment(dbFinalTotalAmt);
		this.changeTextAlignment(dbGrandTotalAmt);
		this.changeTextAlignment(tbroundOffAmt);

		
		//  For changing widget widths.
		this.changeWidth(dototalamt, 200, Unit.PX);
		this.changeWidth(donetpayamt, 200, Unit.PX);
		this.changeWidth(doamtincltax, 200, Unit.PX);
		/** Date 01-09-2017 added by vijay for total amount discount and roundoff **/
		this.changeWidth(dbDiscountAmt, 200, Unit.PX);
		this.changeWidth(dbFinalTotalAmt, 200, Unit.PX);
		this.changeWidth(dbGrandTotalAmt, 200, Unit.PX);
		this.changeWidth(tbroundOffAmt, 200, Unit.PX);

		
	}
	
	/*******************************Text Alignment Method******************************************/
	/**
	 * Method used for align of payment composite fields
	 * @param widg
	 */
	
	private void changeTextAlignment(Widget widg)
	{
		widg.getElement().getStyle().setTextAlign(TextAlign.CENTER);
	}
	
	/*****************************Change Width Method**************************************/
	/**
	 * Method used for changing width of fields
	 * @param widg
	 * @param size
	 * @param unit
	 */
	
	private void changeWidth(Widget widg,double size,Unit unit)
	{
		widg.getElement().getStyle().setWidth(size, unit);
	}
	
	
	protected void validateSecurityDeposit()
	{
		System.out.println("Security Deposit COde");
		List<Config> configGlobal=LoginPresenter.globalConfig;
		
		for(int f=0;f<configGlobal.size();f++)
		{
			if(configGlobal.get(f).getType()==73&&configGlobal.get(f).getName().trim().equalsIgnoreCase("Security Deposit"))
			{
				if(configGlobal.get(f).isStatus()==true)
				{
					System.out.println("True Valuue");
					securityDepositButton();
				}
			}
		}
		
		}
	
	protected void securityDepositButton()
	{
		System.out.println("Process Config For Security0");
		List<ProcessConfiguration> processLis=LoginPresenter.globalProcessConfig;
		List<ProcessTypeDetails> processDetailsLis=new ArrayList<ProcessTypeDetails>();
		for(int i=0;i<processLis.size();i++)
		{
			if(processLis.get(i).getProcessName().equals("Quotation"))
			{
				processDetailsLis=processLis.get(i).getProcessList();
			}
		}
		System.out.println("Siz SD"+processDetailsLis.size());
		for(int d=0;d<processDetailsLis.size();d++)
		{
			if(processDetailsLis.get(d).getProcessType().trim().equalsIgnoreCase("Security Deposit")&&processDetailsLis.get(d).isStatus()==true)
			{
				System.out.println("Status"+processDetailsLis.get(d).isStatus());
				btnsecurityDep.setVisible(true);
			}
		}

	}

	public void checkCustomerStatus(int cCount)
	{
		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(cCount);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel model:result)
					{
						Customer custEntity = (Customer)model;
						
						if(custEntity.getNewCustomerFlag()==true){
							showDialogMessage("Please fill customer information by editing customer");
							personInfoComposite.clear();
						}
						else if(AppConstants.ACTIVE.equals(custEntity.getStatus().trim())&&(custEntity.getNewCustomerFlag()==false)){
							 getPersonInfoComposite().getId().getElement().addClassName("personactive");
				    		 getPersonInfoComposite().getName().getElement().addClassName("personactive");
				    		 getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
				    		 getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
						}
						
						/**Date 9-6-2019 by Amol map a customer category and customer type For ORKIN using Process Confi ****/
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "MapCustomerCategoryAndTypeaAsaDocumentCategoryAndType")){
							custCatandType=true;
						}
						if(custCatandType){
						olbQuotationCategory.setValue(custEntity.getCategory());
						olbQuotationType.setValue(custEntity.getType());
						}
						
						/**
						 * Date : 04-11-2017 BY ANIL
						 * if customer approval process is true then we can not make Quotation of customer whose status is created. 
						 */
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "CustomerApprovalProcess")){
							if(!custEntity.getStatus().equals(Customer.CUSTOMERACTIVE)){
								showDialogMessage("Please approve the customer!");
								personInfoComposite.clear();
							}
						}
						/**
						 * End
						 */
						cust = custEntity;
					}
					
					if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT || AppMemory.getAppMemory().currentState==ScreeenState.NEW){
						updateTaxesDetails(cust);
					}
				}
			 });
		
		
	}
	
	
	/*******************************************************************************************************/
	/***************************************Service Schedule*********************************************/
	
	private List<ServiceSchedule> reactfordefaultTable(SalesLineItem lis) 
	{
		List<SalesLineItem> salesitemlis=new ArrayList<SalesLineItem>();
		salesitemlis.add(lis);
		System.out.println("@@@@@@@@ PRODUCT LIST SIZE ::: "+salesitemlis.size());
		ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
		serschelist.clear();
		boolean unitflag = false , bomCheck = true;
		try {

			for(int i=0;i<salesitemlis.size();i++){
				
				System.out.println("lis size"+salesitemlis.get(i).getBranchSchedulingInfo()+"-"+salesitemlis.get(i).getBranchSchedulingInfo1()
						+"-"+salesitemlis.get(i).getBranchSchedulingInfo2()+"-"+salesitemlis.get(i).getBranchSchedulingInfo3());
				/**
				 * 
				 * Date : 29-03-2017 By ANil
				 */
				boolean branchSchFlag=false;
				int qty=0;
				
//				if(salesitemlis.get(i).getBranchSchedulingList()!=null
//						&&salesitemlis.get(i).getBranchSchedulingList().size()!=0){
//					branchSchFlag=true;
//					for(BranchWiseScheduling branch:salesitemlis.get(i).getBranchSchedulingList()){
//						if(branch.isCheck()==true){
//							qty++;
//						}
//					}
//				}else{
//					qty=(int) salesitemlis.get(i).getQty();
//				}
				
				/**
				 * Date 02-05-2018 
				 * Developer : Vijay
				 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
				 * so i have updated below code using hashmap. 
				 */
				
//				/**
//				 * Date : 03-04-2017 By ANIL
//				 */
//				
////				ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(salesitemlis.get(i).getBranchSchedulingInfo());
//				/**
//				 * Date : 12-06-2017 BY ANIL
//				 */
//				String[] branchArray=new String[10];
//				if(salesitemlis.get(i).getBranchSchedulingInfo()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo().equals("")){
//					branchArray[0]=salesitemlis.get(i).getBranchSchedulingInfo();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo1()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo1().equals("")){
//					branchArray[1]=salesitemlis.get(i).getBranchSchedulingInfo1();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo2()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo2().equals("")){
//					branchArray[2]=salesitemlis.get(i).getBranchSchedulingInfo2();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo3()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo3().equals("")){
//					branchArray[3]=salesitemlis.get(i).getBranchSchedulingInfo3();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo4()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo4().equals("")){
//					branchArray[4]=salesitemlis.get(i).getBranchSchedulingInfo4();
//				}
//				
//				if(salesitemlis.get(i).getBranchSchedulingInfo5()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo5().equals("")){
//					branchArray[5]=salesitemlis.get(i).getBranchSchedulingInfo5();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo6()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo6().equals("")){
//					branchArray[6]=salesitemlis.get(i).getBranchSchedulingInfo6();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo7()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo7().equals("")){
//					branchArray[7]=salesitemlis.get(i).getBranchSchedulingInfo7();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo8()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo8().equals("")){
//					branchArray[8]=salesitemlis.get(i).getBranchSchedulingInfo8();
//				}
//				if(salesitemlis.get(i).getBranchSchedulingInfo9()!=null
//						&&!salesitemlis.get(i).getBranchSchedulingInfo9().equals("")){
//					branchArray[9]=salesitemlis.get(i).getBranchSchedulingInfo9();
//				}
//				
//				/**
//				 * End
//				 */
//				
//				ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
				
				/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
				ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
		
				/**
				 * ends here
				 */
				
				
				if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
					branchSchFlag=true;
					qty=branchSchedulingList.size();
//					for(BranchWiseScheduling branch:branchSchedulingList){
//						if(branch.isCheck()==true){
//							qty++;
//						}
//					}
				}else{
					qty=(int) salesitemlis.get(i).getQty();
				}
				
				/**
				 * End
				 */
				
				if(branchSchFlag==true){
					for(int k=0;k < qty;k++){
						
					 	if(branchSchedulingList.get(k).isCheck()==true){
						long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
						int noOfdays=salesitemlis.get(i).getDuration();
						int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
						Date servicedate=new Date();
						Date d=new Date(servicedate.getTime());
						Date productEndDate= new Date(servicedate.getTime());
						CalendarUtil.addDaysToDate(productEndDate, noOfdays);
						Date prodenddate=new Date(productEndDate.getTime());
						productEndDate=prodenddate;
						Date tempdate=new Date();
						
						for(int j=0;j<noServices;j++){
							if(j>0)
							{
								CalendarUtil.addDaysToDate(d, interval);
								tempdate=new Date(d.getTime());
							}
							
							ServiceSchedule ssch=new ServiceSchedule();
							Date Stardaate=new Date();
							
							//  rohan added this 1 field 
//							System.out.println("In side method RRRRRRRRRRRRR"+salesitemlis.get(i).getProductSrNo());
							ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
							
							ssch.setScheduleStartDate(Stardaate);
							ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
							ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
							ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
							ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
							ssch.setScheduleServiceNo(j+1);
							ssch.setScheduleProdStartDate(Stardaate);
							ssch.setScheduleProdEndDate(productEndDate);
							ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
							/**
							 *   rohan commented this code for NBHC 
							 *   Purpose : wanetd to set servicing branch as customer branch is selected  
							 */
//							ssch.setServicingBranch(olbbBranch.getValue());
							/**
							 * ends here 
							 */
							
//							System.out.println("SERVICING TIME ::: "+salesitemlis.get(i).getServicingTIme());
//							System.out.println("BRANCH ::: "+olbbBranch.getValue());
							ssch.setScheduleServiceTime("Flexible");
							
							if(j==0){
								ssch.setScheduleServiceDate(servicedate);
							}
							if(j!=0){
								if(productEndDate.before(d)==false){
									ssch.setScheduleServiceDate(tempdate);
								}else{
									ssch.setScheduleServiceDate(productEndDate);
								}
							}
						
							ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
							if (branchSchedulingList.get(k).getServicingBranch()!=null) {
								ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
							} 
							
							int dayIndex=AppUtility.getDayIndex(branchSchedulingList.get(k).getDay());
							
							if(dayIndex!=0){
								String dayOfWeek1 = format.format(d);
								int adate = Integer.parseInt(dayOfWeek1);
								int adday1 = dayIndex - 1;
								int adday = 0;
								if (adate <= adday1) {
									adday = (adday1 - adate);
								} else {
									adday = (7 - adate + adday1);
								}
								
								Date newDate = new Date(d.getTime());
								CalendarUtil.addDaysToDate(newDate, adday);
								Date day = new Date(newDate.getTime());

								if (productEndDate.before(day) == false) {
									ssch.setScheduleServiceDate(day);
								} else {
									ssch.setScheduleServiceDate(productEndDate);
								}

								CalendarUtil.addDaysToDate(d, interval);
								Date tempdate1 = new Date(d.getTime());
								d = tempdate1;
								ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
							}
							
							/**
							 * Date 01-04-2017 added by vijay for week number
							 */
							int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
							ssch.setWeekNo(weaknumber);
							
							/**
							 * ends here
							 */
							
						ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						/***
						  *  |*|
						  * nidhi
						  * 7-01-2018
						  * for map bill product
						  * if any update please update in else conditions also
						  */
						 if(LoginPresenter.billofMaterialActive && bomValidationActive){
							 
							 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(lis.getPrduct());
							 if(billPrDt!=null){
								 
								 
								 UnitConversionUtility unitConver = new UnitConversionUtility();

								 
								 if(unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
									 
									 List<ServiceSchedule> sev = new ArrayList<ServiceSchedule>();
									 sev.add(ssch);
									 sev = unitConver.getServiceitemProList(sev,lis.getPrduct(),lis,billPrDt);
									 
									 if(sev!=null && sev.size()>0){
										 ssch = sev.get(0).copyOfObject();
									 }
									 
								 }else{
									 unitflag = true;
//									 showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
								 }
							 }else{
								 bomCheck = false;
							 }
						 }
						 /**
						  * end
						  */
						serschelist.add(ssch);
						}
						
					}
					}
				}else{
				
				for(int k=0;k < qty;k++){
				
				 	
				long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
				int noOfdays=salesitemlis.get(i).getDuration();
				int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
				Date servicedate=new Date();
				Date d=new Date(servicedate.getTime());
				Date productEndDate= new Date(servicedate.getTime());
				CalendarUtil.addDaysToDate(productEndDate, noOfdays);
				Date prodenddate=new Date(productEndDate.getTime());
				productEndDate=prodenddate;
				Date tempdate=new Date();
				
				for(int j=0;j<noServices;j++){
					if(j>0)
					{
						CalendarUtil.addDaysToDate(d, interval);
						tempdate=new Date(d.getTime());
					}
					
					ServiceSchedule ssch=new ServiceSchedule();
					Date Stardaate=new Date();
					
					//  rohan added this 1 field 
//					System.out.println("In side method RRRRRRRRRRRRR"+salesitemlis.get(i).getProductSrNo());
					ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
					
					ssch.setScheduleStartDate(Stardaate);
					ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
					ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
					ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
					ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
					ssch.setScheduleServiceNo(j+1);
					ssch.setScheduleProdStartDate(Stardaate);
					ssch.setScheduleProdEndDate(productEndDate);
					ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
					/**
					 *   rohan commented this code for NBHC 
					 *   Purpose : wanetd to set servicing branch as customer branch is selected  
					 */
//					ssch.setServicingBranch(olbbBranch.getValue());
					/**
					 * ends here 
					 */
					
//					System.out.println("SERVICING TIME ::: "+salesitemlis.get(i).getServicingTIme());
//					System.out.println("BRANCH ::: "+olbbBranch.getValue());
					ssch.setScheduleServiceTime("Flexible");
					
					if(j==0){
						ssch.setScheduleServiceDate(servicedate);
					}
					if(j!=0){
						if(productEndDate.before(d)==false){
							ssch.setScheduleServiceDate(tempdate);
						}else{
							ssch.setScheduleServiceDate(productEndDate);
						}
					}
					
					if(customerbranchlist.size()==qty){
						ssch.setScheduleProBranch(customerbranchlist.get(k).getBusinessUnitName());
						/**
						 * rohan added this code for automatic servicing branch setting for NBHC 
						 */
						
						if(customerbranchlist.get(k).getBranch()!=null)
						{
							ssch.setServicingBranch(customerbranchlist.get(k).getBranch());
						}
						else
						{
							ssch.setServicingBranch(olbbBranch.getValue());
						}
						/**
						 * ends here 
						 */
					}else if(customerbranchlist.size()==0){
						ssch.setScheduleProBranch("Service Address");
						/**
						 * rohan added this code for automatic servicing branch setting for NBHC 
						 */
							ssch.setServicingBranch(olbbBranch.getValue());
						/**
						 * ends here 
						 */	
							
					}else{
						ssch.setScheduleProBranch(null);
					}
					
					ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
					
					/**
					 * Date 01-04-2017 added by vijay for week number
					 */
					int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
					ssch.setWeekNo(weaknumber);
					
					/**
					 * ends here
					 */
					/***
					  *  |*|
					  * nidhi
					  * 7-01-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive && bomValidationActive){
						 
						 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(lis.getPrduct());
						 if(billPrDt!=null){
							 
							 
							 UnitConversionUtility unitConver = new UnitConversionUtility();
							 
							 if(unitConver.varifyUnitConversion(billPrDt, lis.getAreaUnit())){
								 
								 List<ServiceSchedule> sev = new ArrayList<ServiceSchedule>();
								 sev.add(ssch);
								 sev = unitConver.getServiceitemProList(sev,lis.getPrduct(),lis,billPrDt);
								 
								 if(sev!=null && sev.size()>0){
									 ssch = sev.get(0).copyOfObject();
								 }
								 
							 }else{
								 unitflag = true;
//								 showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
							 }
						 }else{
								 bomCheck = false;
						 }
					 }
					 /**
					  * end
					  */
					serschelist.add(ssch);
				}
			}
				
			}
				
				
			}
			
			
			//  rohan added this for testing 
			
//			for (int l = 0; l < serschelist.size(); l++)
//			{
//				System.out.println("My Rohan in service schedule table "+serschelist.get(l).getSerSrNo());
//			}
			
			/**
			 * nidhi |*|
			 */
			if(unitflag){
				showDialogMessage("Unit of service area and product group detail area not mapped at branch level. Or conversion not available");
			}
			if(!bomCheck){
				showDialogMessage("Bill of material is not Created for this service please create first . \n you are not allow to save quotation.");
			}
			/**
			 * end
			 */
			
		
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return serschelist;
	
	}
	
	public void checkCustomerBranch(int customerId) {

		int custid=customerId;
		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("cinfo.count");
		temp.setIntValue(custid);
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				customerbranchlist.clear();
					for(SuperModel model:result)
					{
						CustomerBranchDetails custbranchEntity = (CustomerBranchDetails)model;
						CustomerBranchDetails bbb=new CustomerBranchDetails(); 
		    		 	bbb.setBusinessUnitName(custbranchEntity.getBusinessUnitName());
						bbb.setCinfo(custbranchEntity.getCinfo());
						bbb.setBranch(custbranchEntity.getBranch());
						customerbranchlist.add(bbb);
					}
				}
			 });
		}
	
	public void checkCustomerBranchFromLead(int customerId) {

		int custid=customerId;
		final MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("cinfo.count");
		temp.setIntValue(custid);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());
		 Timer timer=new Timer() 
    	 {
				@Override
				public void run() {
					genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onFailure(Throwable caught) {
							showDialogMessage("An Unexpected error occurred!");
						}
			
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							customerbranchlist=new ArrayList<CustomerBranchDetails>();
							customerbranchlist.clear();
								for(SuperModel model:result)
								{
									CustomerBranchDetails custbranchEntity = (CustomerBranchDetails)model;
									CustomerBranchDetails bbb=new CustomerBranchDetails(); 
					    		 	bbb.setBusinessUnitName(custbranchEntity.getBusinessUnitName());
									bbb.setCinfo(custbranchEntity.getCinfo());
									customerbranchlist.add(bbb);
								}
							}
						 });
				}
			};
          timer.schedule(3000); 
		}
	
		
/*****************************************Type Drop Down Logic****************************************/
	
	@Override
	public void onChange(ChangeEvent event) {
		
		if(event.getSource().equals(olbProductGroup)){
			retrieveServiceProduct();
		}
		
		
		if(event.getSource().equals(olbQuotationCategory))
		{
			if(olbQuotationCategory.getSelectedIndex()!=0){
				ConfigCategory cat=olbQuotationCategory.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(olbQuotationType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
		
		/**
	     * Date 01-09-2017 added by vijay for final total amount and net payable after discount
	     */
		
		if(event.getSource().equals(dbDiscountAmt)){
			System.out.println("on change amt ==");

			if(this.getDbDiscountAmt().getValue()==null){
				this.getDbFinalTotalAmt().setValue(this.getDototalamt().getValue());
				
				this.prodTaxTable.connectToLocal();
				try {
					this.addProdTaxes();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getProdTaxTable().calculateTotalTaxes();

				double netPay = Double.parseDouble(nf.format(totalIncludingTax));
				netPay = Math.round(netPay);
				
				this.getDoamtincltax().setValue(netPay);
				
				//this is for if there in any other charges
				netPay = Math.round(netPay + this.getChargesTable().calculateNetPayable());
				this.getDbGrandTotalAmt().setValue(netPay);


				if(!tbroundOffAmt.getValue().equals("") && tbroundOffAmt.getValue()!=null){
					String roundOff = tbroundOffAmt.getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					if(roundoffAmt!=0){
						this.getDonetpayamt().setValue(roundoffAmt);
					}else{
						this.getDonetpayamt().setValue(netPay);
					}
				}else{
					this.getDonetpayamt().setValue(netPay);

				}
			}
			else if(this.getDbDiscountAmt().getValue() > this.getDototalamt().getValue()){
				showDialogMessage("Discount Amount can not be greater than total amount");
				this.getDbDiscountAmt().setValue(0d);
			}else{
				reactOnDiscountAmtChange();
			}
			
		}
		/**
		 * ends here
		 */
		
		/**
		 * Date 01-09-2017 added by vijay for roundoff and managing net payable
		 */
		if(event.getSource().equals(tbroundOffAmt)){

			double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getProdTaxTable().calculateTotalTaxes();
			double netPay = Double.parseDouble(nf.format(totalIncludingTax));
			//this is for if there in any other charges
			netPay = Math.round(netPay + this.getChargesTable().calculateNetPayable());
			
			if(tbroundOffAmt.getValue().equals("") || tbroundOffAmt.getValue()==null){
				this.getDonetpayamt().setValue(netPay);
			}else{
				if(!tbroundOffAmt.getValue().equals("")&& tbroundOffAmt.getValue()!=null){
					String roundOff = tbroundOffAmt.getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					if(roundoffAmt!=0){
						this.getDonetpayamt().setValue(roundoffAmt);
					}else{
						this.getDonetpayamt().setValue(netPay);
						this.getTbroundOffAmt().setValue("");
					}
				}
			}
		}
		/**
		 * ends here
		 */
		
		if(event.getSource().equals(olbbBranch)){
			System.out.println("on chnage branch");
			if(olbbBranch.getSelectedIndex()!=0) {
				/*
				 * Ashwini Patil 
				 * Date:13-05-2024 
				 * Added for ultima search. 
				 * If number range is selected on branch then it will automatically get selected on contract screen.
				 */
				
				Branch branchEntity =olbbBranch.getSelectedItem();
				if(branchEntity!=null) {
					Console.log("branch entity not null");
					Console.log("branch "+branchEntity.getBusinessUnitName()+"number range="+branchEntity.getNumberRange());
					if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
						olbcNumberRange.setValue(branchEntity.getNumberRange());
						Console.log("Number range set to drop down by default");
					}else {
						Console.log("in else Number range");
						String range=LoginPresenter.branchWiseNumberRangeMap.get(branchEntity.getBusinessUnitName());
						if(range!=null&&!range.equals("")) {
							olbcNumberRange.setValue(range);
							Console.log("in else Number range set to drop down by default");							
						}else {
							olbcNumberRange.setSelectedIndex(0);
						}
					}
				}else
					Console.log("branch entity null");
				
			updateTaxesDetails(cust);
			}
		}
		
		if(event.getSource().equals(olbPaymentTerms)){
			if(paymentTermsTable.getDataprovider().getList().size()>0){
				paymentTermsTable.getDataprovider().getList().clear();
			}
		}
		
		if(event.getSource().equals(ibdays)){
			if(olbPaymentTerms.getSelectedIndex()!=0){
				olbPaymentTerms.setSelectedIndex(0);
				if(paymentTermsTable.getDataprovider().getList().size()>0){
					paymentTermsTable.getDataprovider().getList().clear();
				}
			}
		}
	}
	
	private void retrieveServiceProduct() {
	       
		
			MyQuerry query = new MyQuerry();
			Company c=new Company();
			System.out.println("Company Id :: "+c.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			if(olbProductGroup.getSelectedIndex()!=0){
			Console.log("Inside product group query");
			filter = new Filter();
			filter.setQuerryString("productGroup");
			filter.setStringValue(olbProductGroup.getValue(olbProductGroup.getSelectedIndex()));
			filtervec.add(filter);
			}
			
			filter=new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(true);
			filtervec.add(filter);
			
			query.setFilters(filtervec);
			query.setQuerryObject(new ServiceProduct());
			
			async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					olbProductName.clear();
					olbProductName.removeAllItems();
					
					olbProductName.addItem("--SELECT--");
					List<ServiceProduct> list = new ArrayList<ServiceProduct>();
					for (SuperModel model : result) { 
						ServiceProduct entity = (ServiceProduct) model;
						if(olbProductGroup.getValue().equals(entity.getProductGroup())){
						  list.add(entity);
						//	olbProductName.addItem(entity.toString());
						//	setListItems();
						//Console.log("Inside set product name");
						  
						}
					}
					olbProductName.setListItems(list);
				}
			});
		
		}

	/**
     * Date 01-09-2017 added by vijay for final total amount and net payable after discount
     */
	
	private void reactOnDiscountAmtChange() {
				showWaitSymbol();
				NumberFormat nf=NumberFormat.getFormat("0.00");
				double discountAmt = this.getDbDiscountAmt().getValue();
				double finalAmt = this.getDototalamt().getValue()-discountAmt;
			    this.getDbFinalTotalAmt().setValue(finalAmt);
			    
				try {
					this.prodTaxTable.connectToLocal();
					this.addProdTaxes();
					double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getProdTaxTable().calculateTotalTaxes();
					double netPay = Double.parseDouble(nf.format(totalIncludingTax));
					netPay = Math.round(netPay);
					
					this.getDoamtincltax().setValue(netPay);

					//this is for if there in any other charges
					netPay = Math.round(netPay + this.getChargesTable().calculateNetPayable());
					this.getDbGrandTotalAmt().setValue(netPay);

					if(!tbroundOffAmt.getValue().equals("") && tbroundOffAmt.getValue()!=null){
						String roundOff = tbroundOffAmt.getValue();
						double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
						if(roundoffAmt!=0){
							this.getDonetpayamt().setValue(roundoffAmt);
						}else{
							this.getDonetpayamt().setValue(netPay);
							this.getTbroundOffAmt().setValue("");
						}
					}
					else{
						this.getDonetpayamt().setValue(netPay);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				this.hideWaitSymbol();
	}

	/**
	 * ends here
	 */
	
	/**********************************Getters And Setters*********************************************/
	/**
	 * Gets the db valid untill.
	 *
	 * @return the db valid untill
	 */
	public DateBox getDbValidUntill() {
		return dbValidUntill;
	}

	/**
	 * Sets the db valid untill.
	 *
	 * @param dbValidUntill the new db valid untill
	 */
	public void setDbValidUntill(DateBox dbValidUntill) {
		this.dbValidUntill = dbValidUntill;
	}

	/**
	 * Gets the olbb branch.
	 *
	 * @return the olbb branch
	 */
	public ObjectListBox<Branch> getOlbbBranch() {
		return olbbBranch;
	}

	/**
	 * Sets the olbb branch.
	 *
	 * @param olbbBranch the new olbb branch
	 */
	public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
		this.olbbBranch = olbbBranch;
	}

	/**
	 * Gets the olbc priority.
	 *
	 * @return the olbc priority
	 */
	public ObjectListBox<Config> getOlbcPriority() {
		return olbcPriority;
	}

	/**
	 * Sets the olbc priority.
	 *
	 * @param olbcPriority the new olbc priority
	 */
	public void setOlbcPriority(ObjectListBox<Config> olbcPriority) {
		this.olbcPriority = olbcPriority;
	}

	/**
	 * Gets the olbc payment methods.
	 *
	 * @return the olbc payment methods
	 */
	public ObjectListBox<Config> getOlbcPaymentMethods() {
		return olbcPaymentMethods;
	}

	/**
	 * Sets the olbc payment methods.
	 *
	 * @param olbcPaymentMethods the new olbc payment methods
	 */
	public void setOlbcPaymentMethods(ObjectListBox<Config> olbcPaymentMethods) {
		this.olbcPaymentMethods = olbcPaymentMethods;
	}

	public TextBox getTbLeadId() {
		return tbLeadId;
	}

	public void setTbLeadId(TextBox tbLeadId) {
		this.tbLeadId = tbLeadId;
	}

	public TextBox getTbContractId() {
		return tbContractId;
	}

	public void setTbContractId(TextBox tbContractId) {
		this.tbContractId = tbContractId;
	}

	public TextBox getTbQuotatinId() {
		return tbQuotatinId;
	}

	public void setTbQuotatinId(TextBox tbQuotatinId) {
		this.tbQuotatinId = tbQuotatinId;
	}

	/**
	 * Gets the tb refe r red by.
	 *
	 * @return the tb refe r red by
	 */
	public TextBox getTbRefeRRedBy() {
		return tbRefeRRedBy;
	}

	/**
	 * Sets the tb refe r red by.
	 *
	 * @param tbRefeRRedBy the new tb refe r red by
	 */
	public void setTbRefeRRedBy(TextBox tbRefeRRedBy) {
		this.tbRefeRRedBy = tbRefeRRedBy;
	}

	/**
	 * Gets the tb quotation status.
	 *
	 * @return the tb quotation status
	 */
	public TextBox getTbQuotationStatus() {
		return tbQuotationStatus;
	}

	/**
	 * Sets the tb quotation status.
	 *
	 * @param tbQuotationStatus the new tb quotation status
	 */
	public void setTbQuotationStatus(TextBox tbQuotationStatus) {
		this.tbQuotationStatus = tbQuotationStatus;
	}

	/**
	 * Gets the ta description.
	 *
	 * @return the ta description
	 */
	public TextArea getTaDescription() {
		return taDescription;
	}

	/**
	 * Sets the ta description.
	 *
	 * @param taDescription the new ta description
	 */
	public void setTaDescription(TextArea taDescription) {
		this.taDescription = taDescription;
	}

	/**
	 * Gets the uc upload t and cs.
	 *
	 * @return the uc upload t and cs
	 */
	public UploadComposite getUcUploadTAndCs() {
		return ucUploadTAndCs;
	}

	/**
	 * Sets the uc upload t and cs.
	 *
	 * @param ucUploadTAndCs the new uc upload t and cs
	 */
	public void setUcUploadTAndCs(UploadComposite ucUploadTAndCs) {
		this.ucUploadTAndCs = ucUploadTAndCs;
	}

	/**
	 * Gets the person info composite.
	 *
	 * @return the person info composite
	 */
	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	/**
	 * Sets the person info composite.
	 *
	 * @param personInfoComposite the new person info composite
	 */
	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	/**
	 * Gets the olbe sales person.
	 *
	 * @return the olbe sales person
	 */
	public ObjectListBox<Employee> getOlbeSalesPerson() {
		return olbeSalesPerson;
	}

	/**
	 * Sets the olbe sales person.
	 *
	 * @param olbeSalesPerson the new olbe sales person
	 */
	public void setOlbeSalesPerson(ObjectListBox<Employee> olbeSalesPerson) {
		this.olbeSalesPerson = olbeSalesPerson;
	}
	
	/**
	 * Gets the saleslineitemquotationtable.
	 *
	 * @return the saleslineitemquotationtable
	 */
	public SalesLineItemQuotationTable getSaleslineitemquotationtable() {
		return saleslineitemquotationtable;
	}

	/**
	 * Sets the saleslineitemquotationtable.
	 *
	 * @param saleslineitemquotationtable the new saleslineitemquotationtable
	 */
	public void setSaleslineitemquotationtable(
			SalesLineItemQuotationTable saleslineitemquotationtable) {
		this.saleslineitemquotationtable = saleslineitemquotationtable;
	}
	
	public TextBox getTbremark() {
		return tbremark;
	}

	public void setTbremark(TextBox tbremark) {
		this.tbremark = tbremark;
	}

	public ObjectListBox<Employee> getOlbApproverName() {
		return olbApproverName;
	}

	public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
		this.olbApproverName = olbApproverName;
	}

	public ObjectListBox<Config> getOlbQuotationGroup() {
		return olbQuotationGroup;
	}

	public void setOlbQuotationGroup(ObjectListBox<Config> olbQuotationGroup) {
		this.olbQuotationGroup = olbQuotationGroup;
	}

	public ObjectListBox<Type> getOlbQuotationType() {
		return olbQuotationType;
	}

	public void setOlbQuotationType(ObjectListBox<Type> olbQuotationType) {
		this.olbQuotationType = olbQuotationType;
	}

	public ObjectListBox<ConfigCategory> getOlbQuotationCategory() {
		return olbQuotationCategory;
	}

	public void setOlbQuotationCategory(
			ObjectListBox<ConfigCategory> olbQuotationCategory) {
		this.olbQuotationCategory = olbQuotationCategory;
	}
	
	public IntegerBox getIbCreditPeriod() {
		return ibCreditPeriod;
	}

	public void setIbCreditPeriod(IntegerBox ibCreditPeriod) {
		this.ibCreditPeriod = ibCreditPeriod;
	}

	public DoubleBox getDototalamt() {
		return dototalamt;
	}

	public void setDototalamt(DoubleBox dototalamt) {
		this.dototalamt = dototalamt;
	}

	public DoubleBox getDoamtincltax() {
		return doamtincltax;
	}

	public void setDoamtincltax(DoubleBox doamtincltax) {
		this.doamtincltax = doamtincltax;
	}

	public DoubleBox getDonetpayamt() {
		return donetpayamt;
	}

	public void setDonetpayamt(DoubleBox donetpayamt) {
		this.donetpayamt = donetpayamt;
	}

	public IntegerBox getIbdays() {
		return ibdays;
	}

	public void setIbdays(IntegerBox ibdays) {
		this.ibdays = ibdays;
	}

	public DoubleBox getDopercent() {
		return dopercent;
	}

	public void setDopercent(DoubleBox dopercent) {
		this.dopercent = dopercent;
	}

	public TextBox getTbcomment() {
		return tbcomment;
	}

	public void setTbcomment(TextBox tbcomment) {
		this.tbcomment = tbcomment;
	}

	public PaymentTermsTable getPaymentTermsTable() {
		return paymentTermsTable;
	}

	public void setPaymentTermsTable(PaymentTermsTable paymentTermsTable) {
		this.paymentTermsTable = paymentTermsTable;
	}

	public ProductChargesTable getChargesTable() {
		return chargesTable;
	}

	public void setChargesTable(ProductChargesTable chargesTable) {
		this.chargesTable = chargesTable;
	}

	public ProductTaxesTable getProdTaxTable() {
		return prodTaxTable;
	}

	public void setProdTaxTable(ProductTaxesTable prodTaxTable) {
		this.prodTaxTable = prodTaxTable;
	}
	
	public Button getBtnsecurityDep() {
		return btnsecurityDep;
	}

	public void setBtnsecurityDep(Button btnsecurityDep) {
		this.btnsecurityDep = btnsecurityDep;
	}

	public int getF_docid() {
		return f_docid;
	}

	public void setF_docid(int f_docid) {
		this.f_docid = f_docid;
	}

	public Date getF_docDate() {
		return f_docDate;
	}

	public void setF_docDate(Date f_docDate) {
		this.f_docDate = f_docDate;
	}

	public String getF_docStatus() {
		return f_docStatus;
	}

	public void setF_docStatus(String f_docStatus) {
		this.f_docStatus = f_docStatus;
	}


	public Date getF_reDate() {
		return f_reDate;
	}

	public void setF_reDate(Date f_reDate) {
		this.f_reDate = f_reDate;
	}

	public boolean isF_depositStatus() {
		return f_depositStatus;
	}

	public void setF_depositStatus(boolean f_depositStatus) {
		this.f_depositStatus = f_depositStatus;
	}

	public String getF_payMethod() {
		return f_payMethod;
	}

	public void setF_payMethod(String f_payMethod) {
		this.f_payMethod = f_payMethod;
	}


	public String getF_bankName() {
		return f_bankName;
	}

	public void setF_bankName(String f_bankName) {
		this.f_bankName = f_bankName;
	}

	public String getF_branch() {
		return f_branch;
	}

	public void setF_branch(String f_branch) {
		this.f_branch = f_branch;
	}

	public String getF_paybleAt() {
		return f_paybleAt;
	}

	public void setF_paybleAt(String f_paybleAt) {
		this.f_paybleAt = f_paybleAt;
	}

	public String getF_fouring() {
		return f_fouring;
	}

	public void setF_fouring(String f_fouring) {
		this.f_fouring = f_fouring;
	}

	public String getF_checkName() {
		return f_checkName;
	}

	public void setF_checkName(String f_checkName) {
		this.f_checkName = f_checkName;
	}



	public Date getF_checkDate() {
		return f_checkDate;
	}

	public void setF_checkDate(Date f_checkDate) {
		this.f_checkDate = f_checkDate;
	}


	public String getF_ifscCode() {
		return f_ifscCode;
	}

	public void setF_ifscCode(String f_ifscCode) {
		this.f_ifscCode = f_ifscCode;
	}

	public String getF_micrCode() {
		return f_micrCode;
	}

	public void setF_micrCode(String f_micrCode) {
		this.f_micrCode = f_micrCode;
	}

	public String getF_swiftCode() {
		return f_swiftCode;
	}

	public void setF_swiftCode(String f_swiftCode) {
		this.f_swiftCode = f_swiftCode;
	}

	public String getF_instruction() {
		return f_instruction;
	}

	public void setF_instruction(String f_instruction) {
		this.f_instruction = f_instruction;
	}

	public Address getF_address() {
		return f_address;
	}

	public void setF_address(Address f_address) {
		this.f_address = f_address;
	}

	public Double getF_securityDeposit() {
		return f_securityDeposit;
	}

	public void setF_securityDeposit(Double f_securityDeposit) {
		this.f_securityDeposit = f_securityDeposit;
	}

	

	public String getF_accountNo() {
		return f_accountNo;
	}

	public void setF_accountNo(String f_accountNo) {
		this.f_accountNo = f_accountNo;
	}

	public int getF_checkNo() {
		return f_checkNo;
	}

	public void setF_checkNo(int f_checkNo) {
		this.f_checkNo = f_checkNo;
	}

	public String getF_serviceNo() {
		return f_serviceNo;
	}

	public void setF_serviceNo(String f_serviceNo) {
		this.f_serviceNo = f_serviceNo;
	}

	public Button getBaddproducts() {
		return baddproducts;
	}

	public void setBaddproducts(Button baddproducts) {
		this.baddproducts = baddproducts;
	}

	
	public Button getF_btnservice() {
		return f_btnservice;
	}

	public void setF_btnservice(Button f_btnservice) {
		this.f_btnservice = f_btnservice;
	}

	public Date getF_conStartDate() {
		return f_conStartDate;
	}

	public void setF_conStartDate(Date f_conStartDate) {
		this.f_conStartDate = f_conStartDate;
	}

	public Date getF_conEndDate() {
		return f_conEndDate;
	}

	public void setF_conEndDate(Date f_conEndDate) {
		this.f_conEndDate = f_conEndDate;
	}

	public int getF_duration() {
		return f_duration;
	}

	public void setF_duration(int f_duration) {
		this.f_duration = f_duration;
	}

	public ServiceScheduleTable getServiceScheduleTable() {
		return serviceScheduleTable;
	}

	public void setServiceScheduleTable(ServiceScheduleTable serviceScheduleTable) {
		this.serviceScheduleTable = serviceScheduleTable;
	}

	public ServiceSchedule getScheduleEntity() {
		return scheduleEntity;
	}

	public void setScheduleEntity(ServiceSchedule scheduleEntity) {
		this.scheduleEntity = scheduleEntity;
	}

	public ServiceSchedulePopUp getServicepop() {
		return servicepop;
	}

	public void setServicepop(ServiceSchedulePopUp servicepop) {
		this.servicepop = servicepop;
	}

	public DateBox getDbQuotationDate() {
		return dbQuotationDate;
	}

	public void setDbQuotationDate(DateBox dbQuotationDate) {
		this.dbQuotationDate = dbQuotationDate;
	}

	public String getF_serviceDay() {
		return f_serviceDay;
	}

	public void setF_serviceDay(String f_serviceDay) {
		this.f_serviceDay = f_serviceDay;
	}

	public String getF_serviceTime() {
		return f_serviceTime;
	}

	public void setF_serviceTime(String f_serviceTime) {
		this.f_serviceTime = f_serviceTime;
	}

	public UploadComposite getCustomQuotationUpload() {
		return customQuotationUpload;
	}

	public void setCustomQuotationUpload(UploadComposite customQuotationUpload) {
		this.customQuotationUpload = customQuotationUpload;
	}

	public CheckBox getCustomeQuotation() {
		return customeQuotation;
	}

	public void setCustomeQuotation(CheckBox customeQuotation) {
		this.customeQuotation = customeQuotation;
	}

	public TextBox getTbTicketNumber() {
		return tbTicketNumber;
	}

	public void setTbTicketNumber(TextBox tbTicketNumber) {
		this.tbTicketNumber = tbTicketNumber;
	}

	public boolean isSalesPersonRestrictionFlag() {
		return salesPersonRestrictionFlag;
	}

	public void setSalesPersonRestrictionFlag(boolean salesPersonRestrictionFlag) {
		this.salesPersonRestrictionFlag = salesPersonRestrictionFlag;
	}
	
	

	public DoubleBox getDbDiscountAmt() {
		return dbDiscountAmt;
	}


	public void setDbDiscountAmt(DoubleBox dbDiscountAmt) {
		this.dbDiscountAmt = dbDiscountAmt;
	}


	public DoubleBox getDbFinalTotalAmt() {
		return dbFinalTotalAmt;
	}


	public void setDbFinalTotalAmt(DoubleBox dbFinalTotalAmt) {
		this.dbFinalTotalAmt = dbFinalTotalAmt;
	}

	public TextBox getTbroundOffAmt() {
		return tbroundOffAmt;
	}


	public void setTbroundOffAmt(TextBox tbroundOffAmt) {
		this.tbroundOffAmt = tbroundOffAmt;
	}
	
	public DoubleBox getDbGrandTotalAmt() {
		return dbGrandTotalAmt;
	}

	public void setDbGrandTotalAmt(DoubleBox dbGrandTotalAmt) {
		this.dbGrandTotalAmt = dbGrandTotalAmt;
	}
	
	public DateBox getDbFollowUpDate() {
		return dbFollowUpDate;
	}

	public void setDbFollowUpDate(DateBox dbFollowUpDate) {
		this.dbFollowUpDate = dbFollowUpDate;
	}
	
	/**
	 * Date :23-09-2017 BY ANIL
	 * This method checks whether we should apply discount or not
	 * if taxes applied on all products then we can give discount
	 */
	public boolean isValidForDiscount(){
		String tax1="",tax2="";
		if(saleslineitemquotationtable.getDataprovider().getList().size()==0){
			return false;
		}
		tax1=saleslineitemquotationtable.getDataprovider().getList().get(0).getVatTax().getTaxConfigName().trim();
		tax2=saleslineitemquotationtable.getDataprovider().getList().get(0).getServiceTax().getTaxConfigName().trim();
		for(SalesLineItem obj:saleslineitemquotationtable.getDataprovider().getList()){
			if(!obj.getVatTax().getTaxConfigName().trim().equals(tax1)
					||!obj.getServiceTax().getTaxConfigName().trim().equals(tax2)){
				showDialogMessage("Discount is applicable only if same taxes are applied to all products.");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * End
	 */
	
	/**
	 * nidhi
	 * 31-07-2018
	 * @return
	 */
	public TextBox getTbCreatedBy() {
		return tbCreatedBy;
	}

	public void setTbCreatedBy(TextBox tbCreatedBy) {
		this.tbCreatedBy = tbCreatedBy;
	}

	public TextBox getTbDocStatusreason() {
		return tbDocStatusreason;
	}

	public void setTbDocStatusreason(TextBox tbDocStatusreason) {
		this.tbDocStatusreason = tbDocStatusreason;
	}

	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(dbQuotationDate) && !validateFlag){
			
			String dur = AppUtility.getForProcessConfigurartionIsActiveOrNot("QuotationValidateDuration");
			if(dur!=null && !dur.equals("")){
				Date dt = dbQuotationDate.getValue();
				if(dt == null){
					showDialogMessage("Please add Quotation Date");
					return;
				}
				
				CalendarUtil.addDaysToDate(dt, duration);
				dbValidUntill.setValue(dt);
			}
			
		}
	}

	public void getServiceMaterialBomList(){
		NumberFormat dft2 = NumberFormat.getFormat("#0.000");
		List<ServiceSchedule> origserviceitemlist = serviceScheduleTable.getDataprovider().getList();
		serviceBomMaterialList.clear();
		for(int i=0;i<origserviceitemlist.size();i++){
				
				if(origserviceitemlist.get(i).getServiceProductList()!=null 
						&& origserviceitemlist.get(i).getServiceProductList().size()>0 ){
					if(serviceBomMaterialList.containsKey(origserviceitemlist.get(i).getScheduleProdName())){
						List<MaterialCostStructure> serBomDt = new ArrayList<MaterialCostStructure>(); 
						serBomDt.addAll(serviceBomMaterialList.get(origserviceitemlist.get(i).getScheduleProdName()));
						
						List<ProductGroupList> serSchdulBomDt = new ArrayList<ProductGroupList>(); 
						serSchdulBomDt.addAll(origserviceitemlist.get(i).getServiceProductList().get(0));
						boolean get = false;
						double qty =0,price=0;
						for(int j=0 ; j<serSchdulBomDt.size();j++){
							Console.log(" main get product name -- "  + serSchdulBomDt.get(j).getName());
							 get = false;
							for(int ij=0;ij<serBomDt.size();ij++){
								if(serSchdulBomDt.get(j).getName().equals(serBomDt.get(ij).getProductName())){
									Console.log("get product name -- "  + serBomDt.get(ij).getProductName());
									get = true;
									qty =0;
									price=0;
									 qty =  serBomDt.get(ij).getProductQty() + serSchdulBomDt.get(j).getQuantity();
									 price = serBomDt.get(ij).getTotalCost() + serSchdulBomDt.get(j).getPrice();
									serBomDt.get(ij).setProductQty(Double.parseDouble(dft2.format(qty)));
									serBomDt.get(ij).setTotalCost(Double.parseDouble(dft2.format(price)));
									break;
								}
							}
							if(!get){
								MaterialCostStructure matObj=new MaterialCostStructure();
								matObj.setServiceName(origserviceitemlist.get(i).getScheduleProdName());
								matObj.setProductName(serSchdulBomDt.get(j).getName());
								matObj.setProductQty(serSchdulBomDt.get(j).getQuantity());
								matObj.setTotalCost(serSchdulBomDt.get(j).getPrice());
								matObj.setBomActive(true);
								
								for(ProductInfo prod :  ProductInfoComposite.globalItemProdArray){
									if(prod.getProductCode().equals(serSchdulBomDt.get(j).getCode())){
										matObj.setProductCost(prod.getProductPrice());
										break;
									}
								}
								
								serBomDt.add(matObj);
							}
						}
						
						if(serBomDt.size()>0){
							Console.log(origserviceitemlist.get(i).getScheduleProdName() + "  get serBomDt list size -- " + serBomDt.size());
							System.out.println(origserviceitemlist.get(i).getScheduleProdName() + "  get serBomDt list size -- " + serBomDt.size());
							serviceBomMaterialList.put(origserviceitemlist.get(i).getScheduleProdName(), serBomDt);
						}
						
					}else{
						List<MaterialCostStructure> serBomDt = new ArrayList<MaterialCostStructure>(); 
						List<ProductGroupList> serSchdulBomDt = new ArrayList<ProductGroupList>(); 
						serSchdulBomDt.addAll(origserviceitemlist.get(i).getServiceProductList().get(0));
						for(int j=0 ; j<serSchdulBomDt.size();j++){
								MaterialCostStructure matObj=new MaterialCostStructure();
								matObj.setServiceName(origserviceitemlist.get(i).getScheduleProdName());
								matObj.setProductName(serSchdulBomDt.get(j).getName());
								matObj.setProductQty(serSchdulBomDt.get(j).getQuantity());
								matObj.setTotalCost(serSchdulBomDt.get(j).getPrice());
								matObj.setBomActive(true);
								
								for(ProductInfo prod :  ProductInfoComposite.globalItemProdArray){
									if(prod.getProductCode().equals(serSchdulBomDt.get(j).getCode())){
										matObj.setProductCost(prod.getProductPrice());
										break;
									}
								}
								
								serBomDt.add(matObj);
						}
						
						if(serBomDt.size()>0){
							Console.log(origserviceitemlist.get(i).getScheduleProdName() + "  get new serBomDt list size -- " + serBomDt.size());
							System.out.println(origserviceitemlist.get(i).getScheduleProdName() +"  get neew serBomDt list size -- " + serBomDt.size());
							serviceBomMaterialList.put(origserviceitemlist.get(i).getScheduleProdName(), serBomDt);
						}
					}
				}
		}
		
	}

   /**25-12-2020 Added by Priyanka issue raised by Nitin Sir**/
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		prodTaxTable.getTable().redraw();
		chargesTable.getTable().redraw();
		paymentTermsTable.getTable().redraw();
		saleslineitemquotationtable.getTable().redraw();
	}

	protected void updateTaxesDetails(Customer cust) {
		if(glassPanel!=null){
			hideWaitSymbol();
		}
		
		List<SalesLineItem> prodlist = saleslineitemquotationtable.getDataprovider().getList();
		if(prodlist.size()>0){
			Branch branchEntity =olbbBranch.getSelectedItem();
			
			if(cust!=null){
				System.out.println("customer onselection");
				 String customerBillingaddressState = cust.getAdress().getState();
				 List<SalesLineItem> productlist = AppUtility.updateTaxesDetails(prodlist, branchEntity, customerBillingaddressState,true);
				 saleslineitemquotationtable.getDataprovider().setList(productlist);
				 saleslineitemquotationtable.getTable().redraw();
				RowCountChangeEvent.fire(saleslineitemquotationtable.getTable(), saleslineitemquotationtable.getDataprovider().getList().size(), true);
				setEnable(true);
			}
			hideWaitSymbol();
		}
	}

	public ObjectListBox<Config> getOlbcNumberRange() {
		return olbcNumberRange;
	}

	public void setOlbcNumberRange(ObjectListBox<Config> olbcNumberRange) {
		this.olbcNumberRange = olbcNumberRange;
	}
	
	
	private boolean validateProductSerialNumber() {
		
		HashSet<Integer> productSrNoHs=new HashSet<Integer>();
		List<SalesLineItem> itemList=saleslineitemquotationtable.getDataprovider().getList();
		
		if(itemList!=null&&itemList.size()!=0){
			for(SalesLineItem item:itemList){
				productSrNoHs.add(item.getProductSrNo());
			}
			
			if(itemList.size()!=productSrNoHs.size()){
				String alert="";
				for(Integer prodSrNo:productSrNoHs){
					int srNoCtr=0;
					for(SalesLineItem item:itemList){
						if(prodSrNo==item.getProductSrNo()){
							srNoCtr++;
							if(srNoCtr>1){
								alert=alert+"Product serial number for "+item.getProductName()+" is duplicated. Kindly remove this product and add it again.\n";
							}
						}
					}
				}
				
				if(!alert.equals("")){
					showDialogMessage(alert);
					return false;
				}
			}
		}
		return true;
	}
	
	private void reactonAddTermsAndCondition() {
		
		if(olbtermsAndConditionTitle.getSelectedIndex()!=0){
			TermsAndConditions termsAndCondition = olbtermsAndConditionTitle.getSelectedItem();
			termsAndCondition.setMsg("");
			this.termsAndConditionTable.getDataprovider().getList().add(termsAndCondition);
		}
		else{
			showDialogMessage("Please select terms and condition title");
		}
		
	}
}
