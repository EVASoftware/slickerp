package com.slicktechnologies.client.views.quotation.coststructure;


import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.common.coststructure.EmployeeLevel;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;

public class EmployeeLevelPresenter extends FormTableScreenPresenter<EmployeeLevel>{

	public static EmployeeLevelForm form;
	final GenricServiceAsync async=GWT.create(GenricService.class);
	
	public EmployeeLevelPresenter (FormTableScreen<EmployeeLevel> view,EmployeeLevel model) {
		super(view, model);
		form=(EmployeeLevelForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(employeeLevelQuery());
		form.setPresenter(this);
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel lbl = (InlineLabel) e.getSource();
		if (lbl.getText().contains("New")) {
			form.setToNewState();
			this.initialize();
		}
	}
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		model=new EmployeeLevel();
	}
	public MyQuerry employeeLevelQuery()
	{
		MyQuerry quer=new MyQuerry();
		quer.setQuerryObject(new EmployeeLevel());
		return quer;
	}

	
	public void setModel(EmployeeLevel entity)
	{
		model=entity;
	}
	
	public static void initialize()
	{
		EmployeeLevelTable gentableScreen = new EmployeeLevelTable();
		EmployeeLevelForm form = new EmployeeLevelForm(gentableScreen, FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		EmployeeLevelPresenter presenter = new EmployeeLevelPresenter(form,new EmployeeLevel());
		AppMemory.getAppMemory().stickPnel(form);
	}

}
