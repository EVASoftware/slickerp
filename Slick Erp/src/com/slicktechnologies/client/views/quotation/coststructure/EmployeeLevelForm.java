package com.slicktechnologies.client.views.quotation.coststructure;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.coststructure.EmployeeLevel;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class EmployeeLevelForm extends FormTableScreen<EmployeeLevel> {


	/** The cb status. */
	CheckBox cbStatus;
	
	TextBox tbEmpLvlName;
	
	DoubleBox dbAvgHourlyRate;
	
	/** The ta description. */
	TextArea taDescription;
	
	/** The cat id. */
	IntegerBox ibLvlId;



	/**
	 * Instantiates a new category form.
	 *
	 * @param table the table
	 * @param mode the mode
	 * @param captionmode the captionmode
	 */
	public EmployeeLevelForm  (SuperTable<EmployeeLevel> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		ibLvlId.setEnabled(false);
		cbStatus.setValue(true);

	}

	/**
	 * Initalize widget.
	 */
	private void initializeWidget()
	{

		cbStatus=new CheckBox();
		cbStatus.setValue(true);
		tbEmpLvlName=new TextBox();
		dbAvgHourlyRate=new DoubleBox();
		taDescription=new TextArea();
		ibLvlId=new IntegerBox();

	}
	@Override
	public void createScreen() {

		initializeWidget();
		this.processlevelBarNames=new String[]{"New"};

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder("* Employee Level Name",tbEmpLvlName);
		FormField ftbCategoryName= fbuilder.setMandatory(true).setMandatoryMsg("Employee Level Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Average Hourly Rate",dbAvgHourlyRate);
		FormField ftbCategoryCode= fbuilder.setMandatory(true).setMandatoryMsg("Average Hourly Rate is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Description",taDescription);
		FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		

		FormField[][] formfield = {
				{ftbCategoryName,ftbCategoryCode,fcbStatus},
				{ftaDescription},
		};


		this.fields=formfield;		
	}

	@Override
	public void updateModel(EmployeeLevel model) 
	{
		if(tbEmpLvlName.getValue()!=null)
			model.setEmpLvlName(tbEmpLvlName.getValue());
			model.setStatus(cbStatus.getValue());
		if(dbAvgHourlyRate.getValue()!=null)
			model.setAvgHourlyRate(dbAvgHourlyRate.getValue());
		if(taDescription.getValue()!=null)
			model.setDescription(taDescription.getValue());
		
		presenter.setModel(model);
	}

	@Override
	public void updateView(EmployeeLevel view) 
	{
		if(view.getEmpLvlName()!=null)
			tbEmpLvlName.setValue(view.getEmpLvlName());
//		if(view.getStatus()!=null)
			cbStatus.setValue(view.getStatus());
		if(view.getDescription()!=null)
			taDescription.setValue(view.getDescription());
//		if(view.getAvgHourlyRate()!=null)
			dbAvgHourlyRate.setValue(view.getAvgHourlyRate());
		if(view.getDescription()!=null)
			taDescription.setValue(view.getDescription());
		
		ibLvlId.setValue(view.getCount());
		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state.
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		AuthorizationHelper.setAsPerAuthorization(Screen.EMPLOYEELEVEL,LoginPresenter.currentModule.trim());
	}

	@Override
	public void setCount(int count)
	{
        ibLvlId.setValue(count);
	}

	


	@Override
	public void clear() {
		super.clear();
		cbStatus.setValue(true);
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		this.ibLvlId.setEnabled(false);
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}

	


}
