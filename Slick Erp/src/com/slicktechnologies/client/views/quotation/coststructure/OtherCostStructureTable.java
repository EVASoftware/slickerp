package com.slicktechnologies.client.views.quotation.coststructure;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.coststructure.OtherCost;

public class OtherCostStructureTable extends SuperTable<OtherCost> {

	TextColumn<OtherCost>getOtherCostNameCol;
	TextColumn<OtherCost>getOtherCostCol;
	
	Column<OtherCost,String> getDeleteButtonCol;
	
	
	@Override
	public void createTable() {
		getOtherCostNameCol();
		getOtherCostCol();
		getDeleteButtonCol();
		addFieldUpdater();
	}


	private void getOtherCostNameCol() {
		getOtherCostNameCol=new TextColumn<OtherCost>() {
			
			@Override
			public String getValue(OtherCost object) {
				return object.getOtherCostName();
			}
		};
		table.addColumn(getOtherCostNameCol,"Other Cost");
	}

		private void getOtherCostCol() {
		getOtherCostCol=new TextColumn<OtherCost>() {
			
			@Override
			public String getValue(OtherCost object) {
				return object.getTotalCost()+"";
			}
		};
		table.addColumn(getOtherCostCol,"Total Cost");
	}
	
	
	
	private void getDeleteButtonCol() {
		ButtonCell cell=new ButtonCell();
		getDeleteButtonCol=new Column<OtherCost,String>(cell) {
			@Override
			public String getValue(OtherCost object) {
				return "Delete";
			}
		};
		table.addColumn(getDeleteButtonCol,"");
	}
	
	
	
	
	

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		setUpdatorOnDeleteCol();
	}
	
	
	private void setUpdatorOnDeleteCol() {
		getDeleteButtonCol.setFieldUpdater(new FieldUpdater<OtherCost, String>() {
			@Override
			public void update(int index, OtherCost object, String value) {
				getDataprovider().getList().remove(index);
				table.redraw();
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			}
		});
	}


	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true){
			addEditColumn();
		}
		if (state == false){
			addViewColumn();
		}
		
	}

	private void addViewColumn() {
		getOtherCostNameCol();
		getOtherCostCol();
	}


	private void addEditColumn() {
		getOtherCostNameCol();
		getOtherCostCol();
		getDeleteButtonCol();
		addFieldUpdater();
	}


	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
