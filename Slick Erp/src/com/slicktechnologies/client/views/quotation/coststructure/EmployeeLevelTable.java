package com.slicktechnologies.client.views.quotation.coststructure;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.coststructure.EmployeeLevel;

public class EmployeeLevelTable extends SuperTable<EmployeeLevel>{
	TextColumn<EmployeeLevel> getEmpLvlIdCol;
	TextColumn<EmployeeLevel> getEmpLvlNameCol;
	TextColumn<EmployeeLevel> getEmpLvlHourlyRateCol;
	TextColumn<EmployeeLevel> getEmpLvlStatusCol;
	
	@Override
	public void createTable() {
		getEmpLvlIdCol();
		getEmpLvlNameCol();
		getEmpLvlHourlyRateCol();
		getEmpLvlStatusCol();
	}

	private void getEmpLvlIdCol() {
		getEmpLvlIdCol=new TextColumn<EmployeeLevel>() {
			
			@Override
			public String getValue(EmployeeLevel object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getEmpLvlIdCol,"Id");
		getEmpLvlIdCol.setSortable(true);
	}

	private void getEmpLvlNameCol() {
		getEmpLvlNameCol=new TextColumn<EmployeeLevel>() {
			
			@Override
			public String getValue(EmployeeLevel object) {
				return object.getEmpLvlName();
			}
		};
		table.addColumn(getEmpLvlNameCol,"Employee Level Name");
		getEmpLvlNameCol.setSortable(true);
	}

	private void getEmpLvlHourlyRateCol() {
		getEmpLvlHourlyRateCol=new TextColumn<EmployeeLevel>() {
			
			@Override
			public String getValue(EmployeeLevel object) {
				return object.getAvgHourlyRate()+"";
			}
		};
		table.addColumn(getEmpLvlHourlyRateCol,"Average Hourly Rate");
		getEmpLvlHourlyRateCol.setSortable(true);
	}

	private void getEmpLvlStatusCol() {
		getEmpLvlStatusCol=new TextColumn<EmployeeLevel>() {
			
			@Override
			public String getValue(EmployeeLevel object) {
				if(object.getStatus()==true){
					return "Active";
				}
				return "In Active";
			}
		};
		table.addColumn(getEmpLvlStatusCol,"Status");
		getEmpLvlStatusCol.setSortable(true);
	}
	
	public void addColumnSorting() {
		addSortingToIdCol();
		addSortingToNameCol();
		addSortingToRateCol();
		addSortingToStatusCol();
	}

	private void addSortingToRateCol() {
		List<EmployeeLevel> list = getDataprovider().getList();
		columnSort = new ListHandler<EmployeeLevel>(list);
		columnSort.setComparator(getEmpLvlHourlyRateCol,new Comparator<EmployeeLevel>() {
			@Override
			public int compare(EmployeeLevel e1, EmployeeLevel e2) {
				if (e1 != null && e2 != null) {
					if (e1.getAvgHourlyRate() == e2.getAvgHourlyRate()) {
						return 0;
					}
					if (e1.getAvgHourlyRate() > e2.getAvgHourlyRate()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingToStatusCol() {
		List<EmployeeLevel> list = getDataprovider().getList();
		columnSort = new ListHandler<EmployeeLevel>(list);
		columnSort.setComparator(getEmpLvlStatusCol,new Comparator<EmployeeLevel>() {
			@Override
			public int compare(EmployeeLevel e1, EmployeeLevel e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null && e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingToIdCol() {

		List<EmployeeLevel> list = getDataprovider().getList();
		columnSort = new ListHandler<EmployeeLevel>(list);
		columnSort.setComparator(getEmpLvlIdCol,new Comparator<EmployeeLevel>() {
			@Override
			public int compare(EmployeeLevel e1, EmployeeLevel e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);

	}
	
	private void addSortingToNameCol() {
		List<EmployeeLevel> list = getDataprovider().getList();
		columnSort = new ListHandler<EmployeeLevel>(list);
		columnSort.setComparator(getEmpLvlNameCol,new Comparator<EmployeeLevel>() {
			@Override
			public int compare(EmployeeLevel e1, EmployeeLevel e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmpLvlName() != null&& e2.getEmpLvlName() != null) {
						return e1.getEmpLvlName().compareTo(e2.getEmpLvlName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}

}
