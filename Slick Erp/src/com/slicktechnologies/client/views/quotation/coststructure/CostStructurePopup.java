package com.slicktechnologies.client.views.quotation.coststructure;

import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.coststructure.EmployeeLevel;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class CostStructurePopup extends AbsolutePanel {
	ProductInfoComposite prodInfoComposite;
	Button btnAddProd;
	MaterialCostStructureTable materialTbl;
	
	ObjectListBox<EmployeeLevel> olbEmployeeLevel;
	Button btnEmpLvl;
	ManPowerCostStructureTable manPowTbl;
	
	TextBox tbOtherCostName;
	DoubleBox dbOtherCost;
	Button btnAddCost;
	OtherCostStructureTable otherCostTbl;
	
	public CostStructurePopup(){
		initializeWidget();
		
		InlineLabel label=new InlineLabel("Costing");
		label.getElement().getStyle().setFontSize(20, Unit.PX);
		label.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		label.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		add(label,425,10);
		
		add(prodInfoComposite,10,40);add(btnAddProd, 860, 40);
		add(materialTbl.getTable(),10,90);
		
		InlineLabel empLbl=new InlineLabel("Employee Level Name");
		add(empLbl,10,250);
		add(olbEmployeeLevel,10,275);add(btnEmpLvl,300,275);
		add(manPowTbl.getTable(),10,315);
		
		InlineLabel otCostNmLbl=new InlineLabel("Other Cost Name");
		InlineLabel otcostLbl=new InlineLabel("Amount");
		add(otCostNmLbl,10,460);add(otcostLbl,300,460);
		add(tbOtherCostName,10,485);add(dbOtherCost,300,485);add(btnAddCost,600,485);
		add(otherCostTbl.getTable(),10,525);
		
		ScrollPanel scrollPanes= new ScrollPanel();
		scrollPanes.add(this);
		setSize("950px","600px");
		this.getElement().setId("form");
		
	}

	private void initializeWidget() {
		prodInfoComposite=AppUtility.initiateServiceProductComposite(new SuperProduct());
		prodInfoComposite.getElement().getStyle().setWidth(850, Unit.PX);
		btnAddProd=new Button("Add");
		materialTbl=new MaterialCostStructureTable();
		
		olbEmployeeLevel=new ObjectListBox<EmployeeLevel>();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		AppUtility.makeObjectListBoxLive(olbEmployeeLevel, new EmployeeLevel(), filter);
		olbEmployeeLevel.getElement().getStyle().setHeight(25, Unit.PX);
		btnEmpLvl=new Button("Add");
		manPowTbl=new ManPowerCostStructureTable();
		
		tbOtherCostName=new TextBox();
		dbOtherCost=new DoubleBox();
		dbOtherCost.getElement().getStyle().setHeight(25, Unit.PX);
		btnAddCost=new Button("Add");
		otherCostTbl=new OtherCostStructureTable();
	}
}
