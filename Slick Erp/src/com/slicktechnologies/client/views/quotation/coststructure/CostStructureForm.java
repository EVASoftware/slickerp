package com.slicktechnologies.client.views.quotation.coststructure;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.HTMLTable.RowFormatter;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.coststructure.EmployeeLevel;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class CostStructureForm extends FormScreen<DummyEntityOnlyForScreen>{

	ListBox lbServiceName;
	ProductInfoComposite prodInfoComposite;
	Button btnAddProd;
	MaterialCostStructureTable materialTbl;
	
	ObjectListBox<EmployeeLevel> olbEmployeeLevel;
	Button btnEmpLvl;
	ManPowerCostStructureTable manPowTbl;
	
	TextBox tbOtherCostName;
	DoubleBox dbOtherCost;
	Button btnAddCost;
	OtherCostStructureTable otherCostTbl;
	
	DoubleBox dbTotalCosting;
	DoubleBox dbTotalMatCost;
	DoubleBox dbTotalManPowCost;
	DoubleBox dbTotalOtherCost;
	
	DoubleBox dbTotalRevenue;
	DoubleBox dbOperatingMargin;
	
	Button btnOk;
	Button btnCancel;
	
	
	/**
	 *  nidhi
	 * 8-01-2019 |*|
	 *  for set ser Engineer in service 
	 */
	  ObjectListBox<Employee> olbEmployee;
	public  CostStructureForm() {
		super(FormStyle.DEFAULT);
		createGui();
	}
	
	private void initializeWidget() {
		lbServiceName=new ListBox();
		lbServiceName.addItem("--SELECT--");
		prodInfoComposite=AppUtility.initiateItemProductComposite(new SuperProduct());
		prodInfoComposite.getElement().getStyle().setWidth(850, Unit.PX);
		btnAddProd=new Button("Add");
		materialTbl=new MaterialCostStructureTable();
		
		olbEmployeeLevel=new ObjectListBox<EmployeeLevel>();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		AppUtility.makeObjectListBoxLive(olbEmployeeLevel, new EmployeeLevel(), filter);
		olbEmployeeLevel.getElement().getStyle().setHeight(25, Unit.PX);
		btnEmpLvl=new Button("Add");
		manPowTbl=new ManPowerCostStructureTable();
		
		tbOtherCostName=new TextBox();
		dbOtherCost=new DoubleBox();
		dbOtherCost.getElement().getStyle().setHeight(25, Unit.PX);
		btnAddCost=new Button("Add");
		otherCostTbl=new OtherCostStructureTable();
		dbTotalCosting=new DoubleBox();
		dbTotalCosting.setEnabled(false);
		
		dbTotalMatCost=new DoubleBox();
		dbTotalMatCost.setEnabled(false);
		dbTotalManPowCost=new DoubleBox();
		dbTotalManPowCost.setEnabled(false);
		dbTotalOtherCost=new DoubleBox();
		dbTotalOtherCost.setEnabled(false);
		
		dbTotalRevenue=new DoubleBox();
		dbTotalRevenue.setEnabled(false);
		
		dbOperatingMargin=new DoubleBox();
		dbOperatingMargin.setEnabled(false);
		
		btnOk=new Button("Ok");
		btnOk.getElement().getStyle().setWidth(60, Unit.PX);
		btnCancel=new Button("Cancel");
		/*
		 * nidhi
		 * 8-01-2019 |*|
		 * 		 */
		   olbEmployee = new ObjectListBox<Employee>();
	}
	
	public void initializeServiceListBox(ArrayList<String> list){
		lbServiceName.clear();
		lbServiceName.addItem("--SELECT--");
		for(String serv:list){
			lbServiceName.addItem(serv);
		}
		/**
		 *  nidhi 8-01-2019 |*|
		 */
		olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERPROJECT, "Technician");
		/**
		 *  end
		 */
	}
	
	@Override
	public void toggleAppHeaderBarMenu() {
		
	}

	@Override
	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingMCD=fbuilder.setlabel("Material Cost Details").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",prodInfoComposite);
		FormField fprodInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("",btnAddProd);
		FormField fbtnAddProd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		fbuilder = new FormFieldBuilder("",materialTbl.getTable());
		FormField fmaterialTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingMPCD=fbuilder.setlabel("Man Power Cost Details").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(4).build();
		/**
		 * nidhi |*|
		 * 8-01-2019
		 */
		
		FormField folbEmployeeLevel ;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", "CostingForEmployeeBase")){
			fbuilder = new FormFieldBuilder("Employee Name",olbEmployee);
			 folbEmployeeLevel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		}else{
			fbuilder = new FormFieldBuilder("Employee Level Name",olbEmployeeLevel);
			 folbEmployeeLevel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
			
		}
		 
		fbuilder = new FormFieldBuilder("",btnEmpLvl);
		FormField fbtnEmpLvl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		fbuilder = new FormFieldBuilder("",manPowTbl.getTable());
		FormField fmanPowTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingOCD=fbuilder.setlabel("Other Cost Details").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Other Cost Name",tbOtherCostName);
		FormField ftbOtherCostName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		fbuilder = new FormFieldBuilder("Amount",dbOtherCost);
		FormField fdbOtherCost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		fbuilder = new FormFieldBuilder("",btnAddCost);
		FormField fbtnAddCost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		fbuilder = new FormFieldBuilder("",otherCostTbl.getTable());
		FormField fotherCostTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Service Name",lbServiceName);
		FormField flbServiceName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Operating Cost",dbTotalCosting);
		FormField fdbTotalCosting= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Total Material Cost",dbTotalMatCost);
		FormField fdbTotalMatCost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Total Man Power Cost",dbTotalManPowCost);
		FormField fdbTotalManPowCost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Total Other Cost",dbTotalOtherCost);
		FormField fdbTotalOtherCost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",btnOk);
		FormField fbtnOk= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",btnCancel);
		FormField fbtnCancel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Total Revenue",dbTotalRevenue);
		FormField fdbTotalRevenue= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Operating Margin in %",dbOperatingMargin);
		FormField fdbOperatingMargin= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgrouptwo=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupone=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		FormField[][] formfield = {
				{fblankgrouptwo,fdbTotalRevenue},
				{fblankgrouptwo,fdbTotalCosting},
				{flbServiceName,fblankgroupone,fdbOperatingMargin},
				
				{fgroupingMCD},
				{fprodInfoComposite,fbtnAddProd},
				{fmaterialTbl},
				{fblankgrouptwo,fdbTotalMatCost},
				
				{fgroupingMPCD},
				{folbEmployeeLevel,fbtnEmpLvl},
				{fmanPowTbl},
				{fblankgrouptwo,fdbTotalManPowCost},
				
				{fgroupingOCD},
				{ftbOtherCostName,fdbOtherCost,fbtnAddCost},
				{fotherCostTbl},
				{fblankgrouptwo,fdbTotalOtherCost},
				
				{fblankgrouptwo,fblankgrouptwo},
				{fblankgrouptwo,fbtnOk,fbtnCancel},
				
		};

		this.fields=formfield;
	}

	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		
	}

	@Override
	public void updateView(DummyEntityOnlyForScreen model) {
		
	}
	
	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		dbTotalCosting.setEnabled(false);
		dbTotalMatCost.setEnabled(false);
		dbTotalManPowCost.setEnabled(false);
		dbTotalOtherCost.setEnabled(false);
		dbTotalRevenue.setEnabled(false);
		dbOperatingMargin.setEnabled(false);
	}

	public ListBox getLbServiceName() {
		return lbServiceName;
	}

	public void setLbServiceName(ListBox lbServiceName) {
		this.lbServiceName = lbServiceName;
	}

	public ProductInfoComposite getProdInfoComposite() {
		return prodInfoComposite;
	}

	public void setProdInfoComposite(ProductInfoComposite prodInfoComposite) {
		this.prodInfoComposite = prodInfoComposite;
	}

	public Button getBtnAddProd() {
		return btnAddProd;
	}

	public void setBtnAddProd(Button btnAddProd) {
		this.btnAddProd = btnAddProd;
	}

	public MaterialCostStructureTable getMaterialTbl() {
		return materialTbl;
	}

	public void setMaterialTbl(MaterialCostStructureTable materialTbl) {
		this.materialTbl = materialTbl;
	}

	public ObjectListBox<EmployeeLevel> getOlbEmployeeLevel() {
		return olbEmployeeLevel;
	}

	public void setOlbEmployeeLevel(ObjectListBox<EmployeeLevel> olbEmployeeLevel) {
		this.olbEmployeeLevel = olbEmployeeLevel;
	}

	public Button getBtnEmpLvl() {
		return btnEmpLvl;
	}

	public void setBtnEmpLvl(Button btnEmpLvl) {
		this.btnEmpLvl = btnEmpLvl;
	}

	public ManPowerCostStructureTable getManPowTbl() {
		return manPowTbl;
	}

	public void setManPowTbl(ManPowerCostStructureTable manPowTbl) {
		this.manPowTbl = manPowTbl;
	}

	public TextBox getTbOtherCostName() {
		return tbOtherCostName;
	}

	public void setTbOtherCostName(TextBox tbOtherCostName) {
		this.tbOtherCostName = tbOtherCostName;
	}

	public DoubleBox getDbOtherCost() {
		return dbOtherCost;
	}

	public void setDbOtherCost(DoubleBox dbOtherCost) {
		this.dbOtherCost = dbOtherCost;
	}

	public Button getBtnAddCost() {
		return btnAddCost;
	}

	public void setBtnAddCost(Button btnAddCost) {
		this.btnAddCost = btnAddCost;
	}

	public OtherCostStructureTable getOtherCostTbl() {
		return otherCostTbl;
	}

	public void setOtherCostTbl(OtherCostStructureTable otherCostTbl) {
		this.otherCostTbl = otherCostTbl;
	}

	public DoubleBox getDbTotalCosting() {
		return dbTotalCosting;
	}

	public void setDbTotalCosting(DoubleBox dbTotalCosting) {
		this.dbTotalCosting = dbTotalCosting;
	}

	public DoubleBox getDbTotalMatCost() {
		return dbTotalMatCost;
	}

	public void setDbTotalMatCost(DoubleBox dbTotalMatCost) {
		this.dbTotalMatCost = dbTotalMatCost;
	}

	public DoubleBox getDbTotalManPowCost() {
		return dbTotalManPowCost;
	}

	public void setDbTotalManPowCost(DoubleBox dbTotalManPowCost) {
		this.dbTotalManPowCost = dbTotalManPowCost;
	}

	public DoubleBox getDbTotalOtherCost() {
		return dbTotalOtherCost;
	}

	public void setDbTotalOtherCost(DoubleBox dbTotalOtherCost) {
		this.dbTotalOtherCost = dbTotalOtherCost;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public DoubleBox getDbTotalRevenue() {
		return dbTotalRevenue;
	}

	public void setDbTotalRevenue(DoubleBox dbTotalRevenue) {
		this.dbTotalRevenue = dbTotalRevenue;
	}

	public DoubleBox getDbOperatingMargin() {
		return dbOperatingMargin;
	}

	public void setDbOperatingMargin(DoubleBox dbOperatingMargin) {
		this.dbOperatingMargin = dbOperatingMargin;
	}

	public ObjectListBox<Employee> getOlbEmployee() {
		return olbEmployee;
	}

	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}
	
	
	

}
