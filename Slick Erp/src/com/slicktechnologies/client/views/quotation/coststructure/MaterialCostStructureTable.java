package com.slicktechnologies.client.views.quotation.coststructure;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.coststructure.MaterialCostStructure;

public class MaterialCostStructureTable extends SuperTable<MaterialCostStructure> {

	TextColumn<MaterialCostStructure>getServiceNameCol;
	TextColumn<MaterialCostStructure>getMaterialNameCol;
	TextColumn<MaterialCostStructure>getMaterialQtyCol;
	TextColumn<MaterialCostStructure>getMaterialCostCol;
	TextColumn<MaterialCostStructure>getTotalCostCol;
	
	Column<MaterialCostStructure,String> getEditableMaterialQtyCol;
	Column<MaterialCostStructure,String> getEditableMaterialCostCol;
	Column<MaterialCostStructure,String> getDeleteButtonCol;
	
	
	@Override
	public void createTable() {
		getServiceNameCol();
		getMaterialNameCol();
		getEditableMaterialQtyCol();
		getEditableMaterialCostCol();
//		getMaterialQtyCol();
//		getMaterialCostCol();
		getTotalCostCol();
		getDeleteButtonCol();
		addFieldUpdater();
	}


	private void getServiceNameCol() {
		getServiceNameCol=new TextColumn<MaterialCostStructure>() {
			
			@Override
			public String getValue(MaterialCostStructure object) {
				return object.getServiceName();
			}
		};
		table.addColumn(getServiceNameCol,"Service Name");
	}

	private void getMaterialNameCol() {
		getMaterialNameCol=new TextColumn<MaterialCostStructure>() {
			
			@Override
			public String getValue(MaterialCostStructure object) {
				return object.getProductName();
			}
		};
		table.addColumn(getMaterialNameCol,"Product Name");
	}

	private void getMaterialQtyCol() {
		getMaterialQtyCol=new TextColumn<MaterialCostStructure>() {
			
			@Override
			public String getValue(MaterialCostStructure object) {
				return object.getProductQty()+"";
			}
			/** 9-01-2019
			 * nidhi |*| for make non editable for bom item product
			 */
			@Override
			public void onBrowserEvent(Context context, Element elem,
					MaterialCostStructure object, NativeEvent event) {
				// TODO Auto-generated method stub
			
				if(!object.isBomActive()){
					super.onBrowserEvent(context, elem, object, event);
				}
				
			}
			/**
			 * end
			 */
			
		};
		table.addColumn(getMaterialQtyCol,"#Quantity");
	}
	
	private void getEditableMaterialQtyCol() {
		EditTextCell cell=new EditTextCell();
		getEditableMaterialQtyCol=new Column<MaterialCostStructure,String>(cell) {
			@Override
			public String getValue(MaterialCostStructure object) {
				return object.getProductQty()+"";
			}
			/** 9-01-2019
			 * nidhi |*| for make non editable for bom item product
			 */
			@Override
			public void onBrowserEvent(Context context, Element elem,
					MaterialCostStructure object, NativeEvent event) {
				// TODO Auto-generated method stub
			
				if(!object.isBomActive()){
					super.onBrowserEvent(context, elem, object, event);
				}
				
			}
			/**
			 * end
			 */
		};
		table.addColumn(getEditableMaterialQtyCol,"#Quantity");
	}

	private void getMaterialCostCol() {
		getMaterialCostCol=new TextColumn<MaterialCostStructure>() {
			
			@Override
			public String getValue(MaterialCostStructure object) {
				return object.getProductCost()+"";
			}
			/** 9-01-2019
			 * nidhi |*| for make non editable for bom item product
			 */
			@Override
			public void onBrowserEvent(Context context, Element elem,
					MaterialCostStructure object, NativeEvent event) {
				// TODO Auto-generated method stub
			
				if(!object.isBomActive()){
					super.onBrowserEvent(context, elem, object, event);
				}
				
			}
			/**
			 * end
			 */
		};
		table.addColumn(getMaterialCostCol,"#Price");
	}
	
	private void getEditableMaterialCostCol() {
		EditTextCell cell=new EditTextCell();
		getEditableMaterialCostCol=new Column<MaterialCostStructure,String>(cell) {
			@Override
			public String getValue(MaterialCostStructure object) {
				return object.getProductCost()+"";
			}
			/** 9-01-2019
			 * nidhi |*| for make non editable for bom item product
			 */
			@Override
			public void onBrowserEvent(Context context, Element elem,
					MaterialCostStructure object, NativeEvent event) {
				// TODO Auto-generated method stub
			
				if(!object.isBomActive()){
					super.onBrowserEvent(context, elem, object, event);
				}
				
			}
			/**
			 * end
			 */
		};
		table.addColumn(getEditableMaterialCostCol,"#Price");
	}

	private void getTotalCostCol() {
		getTotalCostCol=new TextColumn<MaterialCostStructure>() {
			
			@Override
			public String getValue(MaterialCostStructure object) {
				return object.getTotalCost()+"";
			}
		};
		table.addColumn(getTotalCostCol,"Total Cost");
	}
	
	private void getDeleteButtonCol() {
		ButtonCell cell=new ButtonCell();
		getDeleteButtonCol=new Column<MaterialCostStructure,String>(cell) {
			@Override
			public String getValue(MaterialCostStructure object) {
				return "Delete";
			}
		};
		table.addColumn(getDeleteButtonCol,"");
	}
	
	
	
	
	

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		setUpdatorOnMaterialQtyCol();
		setUpdatorOnMaterialCostCol();
		setUpdatorOnDeleteCol();
	}
	
	
	

	private void setUpdatorOnMaterialQtyCol() {
		getEditableMaterialQtyCol.setFieldUpdater(new FieldUpdater<MaterialCostStructure, String>() {
			@Override
			public void update(int index, MaterialCostStructure object, String value) {
				if(value!=null){
					try {
						Double qty=Double.parseDouble(value);
						Double totalCost=qty*object.getProductCost();
						object.setProductQty(qty);
						object.setTotalCost(totalCost);
					} catch (NumberFormatException e) {
					}
					table.redrawRow(index);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
			}
		});
	}


	private void setUpdatorOnMaterialCostCol() {
		getEditableMaterialCostCol.setFieldUpdater(new FieldUpdater<MaterialCostStructure, String>() {
			@Override
			public void update(int index, MaterialCostStructure object, String value) {
				if(value!=null){
					try {
						Double cost=Double.parseDouble(value);
						Double totalCost=cost*object.getProductQty();
						object.setProductCost(cost);
						object.setTotalCost(totalCost);
					} catch (NumberFormatException e) {
					}
					table.redrawRow(index);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
			}
		});
		
	}


	private void setUpdatorOnDeleteCol() {
		getDeleteButtonCol.setFieldUpdater(new FieldUpdater<MaterialCostStructure, String>() {
			@Override
			public void update(int index, MaterialCostStructure object, String value) {
				getDataprovider().getList().remove(index);
				table.redraw();
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			}
		});
	}


	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true){
			addEditColumn();
		}
		if (state == false){
			addViewColumn();
		}
		
	}

	private void addViewColumn() {
		getServiceNameCol();
		getMaterialNameCol();
		getMaterialQtyCol();
		getMaterialCostCol();
		getTotalCostCol();
	}


	private void addEditColumn() {
		getServiceNameCol();
		getMaterialNameCol();
		getEditableMaterialQtyCol();
		getEditableMaterialCostCol();
		getTotalCostCol();
		getDeleteButtonCol();
		addFieldUpdater();
	}


	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
