package com.slicktechnologies.client.views.quotation.coststructure;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.coststructure.ManPowerCostStructure;

public class ManPowerCostStructureTable extends SuperTable<ManPowerCostStructure> {

	TextColumn<ManPowerCostStructure>getServiceNameCol;
	TextColumn<ManPowerCostStructure>getEmployeeLevelNameCol;
	TextColumn<ManPowerCostStructure>getNoOfOperatorCol;
	TextColumn<ManPowerCostStructure>getManHourCostCol;
	TextColumn<ManPowerCostStructure>getTotalCostCol;
	
	Column<ManPowerCostStructure,String> getEditableNoOfOprCol;
	Column<ManPowerCostStructure,String> getEditableManHourCostCol;
	Column<ManPowerCostStructure,String> getDeleteButtonCol;
	
	/** Date 16-01-2018 By vijay for Man hours calculations ****/
	TextColumn<ManPowerCostStructure> getManNoHour;
	Column<ManPowerCostStructure, String> getManNoHourCol;
	GWTCAlert gwtalert = new GWTCAlert();
	/** ends here ***/
	
	@Override
	public void createTable() {
		getServiceNameCol();
		getMaterialNameCol();
		getEditableNoOfOprCol();
		getEditableManNoHourCostCol();
		getEditableManHourCostCol();
//		getNoOfOperatorCol();
//		getManHourCostCol();
		getTotalCostCol();
		getDeleteButtonCol();
		addFieldUpdater();
	}


	/**
	 * Date 16-01-2018 By vijay 
	 * for man no of hour column
	 */
	private void getNoOfManHour() {
		getManNoHour = new TextColumn<ManPowerCostStructure>() {
			
			@Override
			public String getValue(ManPowerCostStructure object) {
				// TODO Auto-generated method stub
				return object.getNoOfHour()+"";
			}
		};
		table.addColumn(getManNoHour,"#No Of Hour");
	}

	private void getEditableManNoHourCostCol() {
		EditTextCell editcell = new EditTextCell();
		getManNoHourCol = new Column<ManPowerCostStructure, String>(editcell) {
			
			@Override
			public String getValue(ManPowerCostStructure object) {
				// TODO Auto-generated method stub
				return object.getNoOfHour()+"";
			}
		};
		table.addColumn(getManNoHourCol,"#No Of Hour");
	}

	/**
	 * ends here
	 */
	
	private void getServiceNameCol() {
		getServiceNameCol=new TextColumn<ManPowerCostStructure>() {
			
			@Override
			public String getValue(ManPowerCostStructure object) {
				return object.getServiceName();
			}
		};
		table.addColumn(getServiceNameCol,"Service Name");
	}

	private void getMaterialNameCol() {
		getEmployeeLevelNameCol=new TextColumn<ManPowerCostStructure>() {
			
			@Override
			public String getValue(ManPowerCostStructure object) {
				return object.getEmployeeLevel();
			}
		};
		table.addColumn(getEmployeeLevelNameCol,"Employee Level Name");
	}

	private void getNoOfOperatorCol() {
		getNoOfOperatorCol=new TextColumn<ManPowerCostStructure>() {
			
			@Override
			public String getValue(ManPowerCostStructure object) {
				return object.getNoOfOprator()+"";
			}
		};
		table.addColumn(getNoOfOperatorCol,"#No.Of Operator");
	}
	
	private void getEditableNoOfOprCol() {
		EditTextCell cell=new EditTextCell();
		getEditableNoOfOprCol=new Column<ManPowerCostStructure,String>(cell) {
			@Override
			public String getValue(ManPowerCostStructure object) {
				return object.getNoOfOprator()+"";
			}
		};
		table.addColumn(getEditableNoOfOprCol,"#No.Of Operator");
	}

	private void getManHourCostCol() {
		getManHourCostCol=new TextColumn<ManPowerCostStructure>() {
			
			@Override
			public String getValue(ManPowerCostStructure object) {
				return object.getManCostPerHour()+"";
			}
		};
		table.addColumn(getManHourCostCol,"#Man Cost/Hour");
	}
	
	private void getEditableManHourCostCol() {
		EditTextCell cell=new EditTextCell();
		getEditableManHourCostCol=new Column<ManPowerCostStructure,String>(cell) {
			@Override
			public String getValue(ManPowerCostStructure object) {
				return object.getManCostPerHour()+"";
			}
		};
		table.addColumn(getEditableManHourCostCol,"#Man Cost/Hour");
	}

	private void getTotalCostCol() {
		getTotalCostCol=new TextColumn<ManPowerCostStructure>() {
			
			@Override
			public String getValue(ManPowerCostStructure object) {
				return object.getTotalCost()+"";
			}
		};
		table.addColumn(getTotalCostCol,"Total Cost");
	}
	
	private void getDeleteButtonCol() {
		ButtonCell cell=new ButtonCell();
		getDeleteButtonCol=new Column<ManPowerCostStructure,String>(cell) {
			@Override
			public String getValue(ManPowerCostStructure object) {
				return "Delete";
			}
		};
		table.addColumn(getDeleteButtonCol,"");
	}
	
	
	
	
	

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		setUpdatorOnNoOfOprCol();
		setUpdatorOnManHourCostCol();
		setUpdatorOnDeleteCol();
		
		setUpdatorManHour();

	}
	
	
	
	
	/**
	 * Date 16-01-2018 By vijay 
	 * for man No of hour column fieldupdator
	 */

	private void setUpdatorManHour() {

		getManNoHourCol.setFieldUpdater(new FieldUpdater<ManPowerCostStructure, String>() {
			
			@Override
			public void update(int index, ManPowerCostStructure object, String value) {
				// TODO Auto-generated method stub
				if(value!=null && !value.equals("")){
					
					if(value.contains("-")){
						gwtalert.alert("Please add Positive numbers only");
						setEnable(true);
					}else{
						
						try {
							Double manNoOfhour = Double.parseDouble(value);
							manNoOfhour = Math.abs(manNoOfhour);
							double totalcost = manNoOfhour*object.getNoOfOprator()*object.getManCostPerHour();
							object.setNoOfHour(manNoOfhour);
							object.setTotalCost(totalcost);
						} catch (Exception e) {
						}
						table.redrawRow(index);
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					}
				}
				if(value.equals("")){
					setEnable(true);
				}
			}
		});
	}

/**
 * ends here
 */

	private void setUpdatorOnNoOfOprCol() {
		getEditableNoOfOprCol.setFieldUpdater(new FieldUpdater<ManPowerCostStructure, String>() {
			@Override
			public void update(int index, ManPowerCostStructure object, String value) {
				if(value!=null && !value.equals("")){
					 /** Date 16-01-2018 by Vijay if condition for validation for negative numbers
					  * else block added old code
					  */
						if(value.contains("-")){
							gwtalert.alert("Please add Positive numbers only");
							setEnable(true);
						}else{
						
							try {
								int qty=Integer.parseInt(value);
//								Double totalCost=qty*object.getManCostPerHour();
								/**
								 * Date 16-01-2018 by Vijay for Total including no of hour calculation
								 */
								Double totalCost= qty*object.getManCostPerHour()*object.getNoOfHour();
								/**
								 * ends here
								 */
								object.setNoOfOprator(qty);
								object.setTotalCost(totalCost);
							} catch (NumberFormatException e) {
							}
							table.redrawRow(index);
							RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
						
						}
						
				}
				if(value.equals("")){
					setEnable(true);
				}
			}
		});
	}


	private void setUpdatorOnManHourCostCol() {
		getEditableManHourCostCol.setFieldUpdater(new FieldUpdater<ManPowerCostStructure, String>() {
			@Override
			public void update(int index, ManPowerCostStructure object, String value) {
				if(value!=null && !value.equals("")){
					
					/** Date 16-01-2018 by Vijay if condition for validation for negative numbers
					  * else block added old code
					  */
						if(value.contains("-")){
							gwtalert.alert("Please add Positive numbers only");
							setEnable(true);
						}else{
							
							try {
								Double cost=Double.parseDouble(value);
//								Double totalCost=cost*object.getNoOfOprator();
								/**
								 * Date 16-01-2018 by Vijay above old code commented and for Total including no of hour calculation
								 */
								Double totalCost=cost*object.getNoOfOprator()*object.getNoOfHour();
								/**
								 * ends here
								 */
								object.setManCostPerHour(cost);
								object.setTotalCost(totalCost);
							} catch (NumberFormatException e) {
							}
							table.redrawRow(index);
							RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
						}
				}
				if(value.equals("")){
					setEnable(true);
				}
			}
		});
		
	}


	private void setUpdatorOnDeleteCol() {
		getDeleteButtonCol.setFieldUpdater(new FieldUpdater<ManPowerCostStructure, String>() {
			@Override
			public void update(int index, ManPowerCostStructure object, String value) {
				getDataprovider().getList().remove(index);
				table.redraw();
			}
		});
	}


	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true){
			addEditColumn();
		}
		if (state == false){
			addViewColumn();
		}
		
	}

	private void addViewColumn() {
		getServiceNameCol();
		getMaterialNameCol();
		getNoOfOperatorCol();
		getNoOfManHour();
		getManHourCostCol();
		getTotalCostCol();
	}


	private void addEditColumn() {
		getServiceNameCol();
		getMaterialNameCol();
		getEditableNoOfOprCol();
		getEditableManNoHourCostCol();
		getEditableManHourCostCol();
		getTotalCostCol();
		getDeleteButtonCol();
		addFieldUpdater();
	}


	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
