package com.slicktechnologies.client.views.quotation;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.Quotation;
@RemoteServiceRelativePath("quotationservice")
public interface  QuotationService extends RemoteService 
{
  public void changeStatus(Quotation q);
}
