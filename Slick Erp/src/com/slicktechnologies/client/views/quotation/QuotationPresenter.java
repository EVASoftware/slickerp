package com.slicktechnologies.client.views.quotation;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formscreen.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.coststructure.EmployeeLevel;
import com.slicktechnologies.shared.common.coststructure.ManPowerCostStructure;
import com.slicktechnologies.shared.common.coststructure.MaterialCostStructure;
import com.slicktechnologies.shared.common.coststructure.OtherCost;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.reusabledata.CopyDataConfig;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utility.UnitConversionUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogPopUp;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.ContractService;
import com.slicktechnologies.client.views.contract.ContractServiceAsync;
import com.slicktechnologies.client.views.contract.CustomerAddressPopup;
import com.slicktechnologies.client.views.contract.SalesLineItemTable;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.lead.LeadPresenterSearchProxy;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentlist.PaymentListPresenter;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.popups.SMSPopUp;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.quickcontract.QuickContractForm;
import com.slicktechnologies.client.views.quickcontract.QuickContractPresentor;
import com.slicktechnologies.client.views.quotation.coststructure.CostStructureForm;
import com.slicktechnologies.client.views.scheduleservice.ServiceSchedulePopUp;
import com.slicktechnologies.client.views.securitydeposit.securitydepositpopup.SecurityDepositPopUp;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.view.client.RowCountChangeEvent;
/**
 * FormScreen presenter template.
 */
public class QuotationPresenter extends ApprovableFormScreenPresenter<Quotation> implements RowCountChangeEvent.Handler, SelectionHandler<Suggestion>, ChangeHandler, ValueChangeHandler<Integer> {

	//Token to set the concrete FormScreen class name
	public static  QuotationForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final QuotationServiceAsync async=GWT.create(QuotationService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	SecurityDepositPopUp securitydeposit=new SecurityDepositPopUp();
	PopupPanel panel;
	//**************rohan made changes here for om pest     
	
	int cnt=0;
	
	/**
	 * Date 14 may 2017 addded by Rohan for communication log
	 */
	CommunicationLogPopUp CommunicationLogPopUp = new CommunicationLogPopUp();
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	/**
	 * ends here
	 */

	int customQuotation = 0;
	int pestQuotation=0;
	int pestoIndia = 0;
	int nbhc = 0;
	int universal =0;
	int pestMasters =0;
	int quotationFormatVersion1=0;
	AdditionalDetailsPopup addpopup=new AdditionalDetailsPopup();
	
	//************changes changes here ****************
	ConditionDialogBox conditionPopup=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
	//****************ends here ***********	
	
	ServiceSchedulePopUp serviceSchedulePopUp=new ServiceSchedulePopUp("Quotation Id", "Quotation Date");
	PopupPanel schedulePanel;
	DateTimeFormat format = DateTimeFormat.getFormat("c");
	public static int specificday=0;
	public static int custcount=0;
	
/************************* updation Code *********************/
	
	FromAndToDateBoxPopup frmAndToPop=new FromAndToDateBoxPopup();
	ArrayList<TaxDetails> vattaxlist;
	ArrayList<TaxDetails> servicetaxlist;	

	/**
	 * Date : 19-05-2017 By ANIL
	 */
		CostStructureForm costForm=new CostStructureForm();
	/**
	 * End
	 */
		
	/**
	 * Date : 06-11-2017 BY ANIL
	 * Adding cancellation popup 
	 */
	DocumentCancellationPopUp cancelPopup = new DocumentCancellationPopUp();
	/**
	 * nidhi
	 * 1-08-2018
	 * for make visible only for lead and quotation.
	 */
	DocumentCancellationPopUp unSucessfullPopup = new DocumentCancellationPopUp(true);
	/**
	 * end
	 */
	PopupPanel cancelpanel ;
	
	/**
	 * End
	 */
	/**
	 * Date 14/12/2017
	 * By Jayshree
	 * adding the popup for NBHC to print the quotation pdf with data and without data
	 */
	ConditionDialogBox loiConditionPopup=new ConditionDialogBox
    ("Do You Want To Print the Quotation with Data OR without data",AppConstants.YES,AppConstants.NO,true);
    PopupPanel panelLOI;
	//End
	
    /**
     * nidhi
     * @param view
     * @param model
     */
    ConditionDialogBox conditionLOIPopup=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO,true);
	
    CustomerAddressPopup custAddresspopup = new CustomerAddressPopup();
	InlineLabel lblUpdate = new InlineLabel("Update");
	public static boolean updateAddressFlag = false;

	NewEmailPopUp emailpopup = new NewEmailPopUp();

	CommonServiceAsync commonservice = GWT.create(CommonService.class);

	SMSPopUp smspopup = new SMSPopUp();
	
	public QuotationPresenter(FormScreen<Quotation> view, Quotation model){
		super(view, model);
		form=(QuotationForm) view;
		/**
		 * nidhi 14-12-2018
		 */
		form.validateFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", "DisableQuotationValidateOption");
	
		String dur = AppUtility.getForProcessConfigurartionIsActiveOrNot("QuotationValidateDuration");
		
		if(dur!=null && dur.trim().length()>0 && form.validateFlag){
			
			form.duration = Integer.parseInt(dur);
		}
		if(form.validateFlag){
			if(form.duration == -1){
				form.duration = 0;
			}
			form.dbValidUntill.setEnabled(false);
	
		}
		/**
		 * nidhi |*|
		 */
		form.bomValidationActive = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","BillOfMaterialActive");
	
		form.setPresenter(this);
		form.getSaleslineitemquotationtable().getTable().addRowCountChangeHandler(this);
		form.getChargesTable().getTable().addRowCountChangeHandler(this);
		
		form.btnsecurityDep.addClickHandler(this);
		form.btnsecurityDep.setEnabled(true);
		securitydeposit.getBtnOk().addClickHandler(this);
		securitydeposit.getBtnCancel().addClickHandler(this);
		form.getPersonInfoComposite().getId().addSelectionHandler(this);
		form.getPersonInfoComposite().getName().addSelectionHandler(this);
		form.getPersonInfoComposite().getPhone().addSelectionHandler(this);
		
		//**********rohan ************************
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		
		/******************************Service Schedule************************************/
		
		form.f_btnservice.addClickHandler(this);
		form.f_btnservice.setEnabled(true);
		serviceSchedulePopUp.getBtnOk().addClickHandler(this);
		serviceSchedulePopUp.getBtnCancel().addClickHandler(this);
		serviceSchedulePopUp.getBtnReset().addClickHandler(this);
		
		serviceSchedulePopUp.getP_default().addClickHandler(this);
		serviceSchedulePopUp.getP_customise().addClickHandler(this);
		
		serviceSchedulePopUp.getP_mondaytofriday().addChangeHandler(this);
		serviceSchedulePopUp.getP_interval().addValueChangeHandler(this);
		
		serviceSchedulePopUp.getP_servicehours().addChangeHandler(this);
		serviceSchedulePopUp.getP_servicemin().addChangeHandler(this);
		serviceSchedulePopUp.getP_ampm().addChangeHandler(this);
		
		/**********************************************************************************/
		
		
		frmAndToPop.btnOne.addClickHandler(this);
		frmAndToPop.btnTwo.addClickHandler(this);
		
		/***************************Rahul made This for Pestle Pest control*********************/
		form.additionalDetails.addClickHandler(this);
		addpopup.btnOk.addClickHandler(this);
		addpopup.btnCancel.addClickHandler(this);
		
		//Vijay on 3 march 2017
		serviceSchedulePopUp.getP_serviceWeek().addChangeHandler(this);

		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.QUOTATION,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
		
		CommunicationLogPopUp.getBtnOk().addClickHandler(this);
		CommunicationLogPopUp.getBtnCancel().addClickHandler(this);
		CommunicationLogPopUp.getBtnAdd().addClickHandler(this);
		
		
		/**
		 * Date : 20-05-2017 By ANIL
		 */
		costForm.getBtnAddProd().addClickHandler(this);
		costForm.getBtnAddCost().addClickHandler(this);
		costForm.getBtnEmpLvl().addClickHandler(this);
		costForm.getBtnOk().addClickHandler(this);
		costForm.getBtnCancel().addClickHandler(this);
		
		costForm.getMaterialTbl().getTable().addRowCountChangeHandler(this);
		costForm.getManPowTbl().getTable().addRowCountChangeHandler(this);
		costForm.getOtherCostTbl().getTable().addRowCountChangeHandler(this);
		
		form.btnCosting.addClickHandler(this);
		/**
		 * End
		 */
		
		/**
		 * Date : 06-11-2017 BY ANIL
		 */
		cancelPopup.getBtnOk().addClickHandler(this);
		cancelPopup.getBtnCancel().addClickHandler(this);
		
		unSucessfullPopup.getBtnOk().addClickHandler(this);
		unSucessfullPopup.getBtnCancel().addClickHandler(this);
		/**
		 * End
		 */
		
		/**
		 * Date 14/12/2017
		 * By Jayshree 
		 * Add the buttons for NBHC quotation
		 */
		
		loiConditionPopup.getBtnOne().addClickHandler(this);
		loiConditionPopup.getBtnTwo().addClickHandler(this);
		
		//End
	
		custAddresspopup.getLblOk().addClickHandler(this);
		custAddresspopup.getLblCancel().addClickHandler(this);
		custAddresspopup.getBtnbillingaddr().addClickHandler(this);
		lblUpdate.addClickHandler(this);
		
	}

	
	
	
	@Override
	public void reactOnSearch() {
		super.reactOnSearch();  
		
		QuotationPresenterSearchProxy.makeSalesPersonEnable();
	}




	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		super.reactToProcessBarEvents(e);
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();

		
		if(text.equals(AppConstants.CREATECONTRACT)){
			/**
			 * Date 31-01-2019 by Vijay For NBHC CCPM Lead and quotation mandatory so contract creation not allowed 
			 * from here if process config is active
			 */
			if(form.isPopUpAppMenubar()){
				Console.log("SERVICE QUOTATION POPUP : Create Contract clicked!!");
				return;
			}
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MakeLeadAndQuotationIdMandatory")
					&& !LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin") && form.tbLeadId.getValue().equals("")){
				form.showDialogMessage("Please create lead first! then create contract!");
			}
			else{
				reactToCreateContract();
			}
		}
		/**@Sheetal : 15-02-2022
		 *   Added View lead button**/
		if(text.equals(AppConstants.VIEWLEAD)){  
			if(form.isPopUpAppMenubar()){
				Console.log("SERVICE QUOTATION POPUP : View Lead clicked!!");
				return;
			}
			viewLead();
		}
		/**
		 * Date : 06-11-2017 By ANIL
		 */
//		if(text.equals(AppConstants.Unsucessful))
//			reactToUnsuccessful();
		/**
		 * End
		 */
		if(text.equals(AppConstants.NEW)){
			if(form.isPopUpAppMenubar()){
				Console.log("SERVICE QUOTATION POPUP : New clicked!!");
				return;
			}
			reactToNew();
		}
		if(text.equals("Email"))
			reactOnEmail();
		
		if(text.equals("Update Taxes")){
			
			panel = new PopupPanel(true);
			panel.add(frmAndToPop);
			panel.show();
			panel.center();
			
		}
		
		if(text.endsWith("Mark Successful"))
		{
			markQuotationSuccessful();
		}
		
		if(text.endsWith(AppConstants.CREATEQUICKCONTRACT))
		{
			if(form.isPopUpAppMenubar()){
				Console.log("SERVICE QUOTATION POPUP : Create Quick Contract clicked!!");
				return;
			}
			reactToCreateQuickContract();
		}
		/// below method created by Ajinkya (Date:13/12/2016
		
		if(text.equals("Print CheckList WoodBorer")){
			reactOnprintchecklistWoodBorer();
		}
		if(text.equals("Print CheckList BedBugs")){
			reactOnprintchecklistBedBugs();
		}
		if(text.equals("Print CheckList FlyMosquito")){
			reactOnprintchecklistFlyMosquito();
		}
		
		if(text.equals("Print CheckList Rodent")){  
			reactOnprintchecklistRodent();
		}
		if(text.equals("Print CheckList Termite")){
			reactOnprintchecklistTermite();
		}
		if(text.equals("Print CheckList Gipc")){
			reactOnprintchecklistGipc();
		}
		/**
		 * Rohan Added this for Printing NBHC LOI 
		 * Date : 14/3/2017
		 */
		/**
		 * Commented By Jayshree Chavan
		 */
//		if(text.equals("Print LOI")){
//			reactOnPrintNBHCLOIPdf();
//		}
		/**
		 * Ends for Jayshree
		 */
		/*
		 *  nidhi
		 *  07-07-2017
		 *  for revise quotation
		 */ 
		if(text.equals("Revised Quotation")){
			reactnReviseQuotation();
		}
		/*
		 * end
		 */
		
		/**
		 * Date : 06-11-2017 BY ANIL
		 * cancellation with remark
		 * for NBHC
		 */
		if(text.equals(AppConstants.CANCEL)){
			cancelPopup.getPaymentDate().setValue(AppUtility.parseDate(new Date())+"");
			cancelPopup.getPaymentID().setValue(model.getCount()+"");
			cancelPopup.getPaymentStatus().setValue(model.getStatus());
			cancelpanel=new PopupPanel(true);
			cancelpanel.add(cancelPopup);
			cancelpanel.show();
			cancelpanel.center();
		}
		
		if(text.equals(AppConstants.Unsucessful)){
			unSucessfullPopup.getPaymentDate().setValue(AppUtility.parseDate(new Date())+"");
			unSucessfullPopup.getPaymentID().setValue(model.getCount()+"");
			unSucessfullPopup.getPaymentStatus().setValue(model.getStatus());
			cancelpanel=new PopupPanel(true);
			cancelpanel.add(unSucessfullPopup);
			cancelpanel.show();
			cancelpanel.center();
		}
		/**
		 * End
		 */
			
		/**
		 * Date 14/12/2017 
		 * By Jayshree
		 */
		if(text.equals("Print LOI")){
			
			panelLOI=new PopupPanel(true);
			panelLOI.add(loiConditionPopup);
			panelLOI.setGlassEnabled(true);
			panelLOI.show();
			panelLOI.center();
			
//			reactOnPrintNBHCLOIPdf();//comment by jayshree
		}
		
		if(text.equals(AppConstants.CUSTOMERADDRESS)) {
			reactOnCustomerAddress();
		}
		
		if(text.equals(AppConstants.SIGNUP)){
			reactOnQuotationSignUp();
		}
		
		if(text.equals(AppConstants.COPY)){
			reactOnCopy();
		}
	}
	







	private void viewLead() {

		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getLeadCount());
		temp.add(filter);

		querry.setFilters(temp);
		querry.setQuerryObject(new Lead());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No Lead document found.");
					Console.log("lead size: "+result.size());
					return;
				}
				else{
					final Lead leadDocument=(Lead) result.get(0);
					final LeadForm form=LeadPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(leadDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}
				
			}
		});
		
	
		
	}




	private void createContractAutoOnQuotationMarkSuccessfull() {
		   
		                Console.log("Inside create contract Automatically");
						Contract contract=new Contract();
						contract.setCompanyId(model.getCompanyId());
						
						contract.setCinfo(model.getCinfo());
						contract.setBranch(model.getBranch());
						contract.setType(model.getType());
						contract.setGroup(model.getGroup());
						contract.setCategory(model.getCategory());
						contract.setApproverName(model.getApproverName());
						contract.setEmployee(model.getEmployee());
						contract.setContractDate(model.getQuotationDate());
						contract.setCreatedBy(model.getCreatedBy());
						contract.setPriority(model.getPriority());
						contract.setStatus(Contract.CREATED);
						contract.setStartDate(model.getStartDate());
						contract.setEndDate(model.getEndDate());
						contract.setItems(model.getItems());
						contract.setPaymentTermsList(model.getPaymentTermsList());
						contract.setPayTerms(model.getPayTerms());
						contract.setServiceScheduleList(model.getServiceScheduleList());
						contract.setProductTaxes(model.getProductTaxes());
						contract.setProductCharges(model.getProductCharges());
						contract.setGrandTotal(model.getGrandTotal());
						contract.setNetpayable(model.getNetpayable());
						contract.setQuotationCount(model.getCount());
						Console.log("End");
						genasync.save(contract, new AsyncCallback<ReturnFromServer>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								 Console.log("contract failed");
							}

							@Override
							public void onSuccess(ReturnFromServer result) {
								// TODO Auto-generated method stub
								  Console.log("contract create successfully");
							}
						});
	}




	private void reactnReviseQuotation(){
		try {
			System.out.println("get id : " +model.getCount());
			Quotation quo = (Quotation)model;
			//Quotation quotation=(Quotation) deepClone(quo);
			final Quotation quotation=(Quotation)getRevisedQuotation(model);
			quotation.setCount(0);
			quotation.setContractCount(0);
			quotation.setQuotationCount(0);
			quotation.setQuotationDate(null);
			quotation.setApprovalDate(null);
			quotation.setCreationDate(null);
			quotation.setValidUntill(null);
			quotation.setStatus(Quotation.CREATED);
			quotation.setId(null);
			
			System.out.println("get id after : " +quotation.getCount()+ " ID "+quotation.getId() + " get value model  : "+ model.getCount() +" model Id ;;;; -- "+model.getId());
			AppMemory.getAppMemory().currentState=ScreeenState.NEW;
			form.setToNewState();
			
			/********** Date 27 sep 2017 added by vijay code added in timer because product table is getting repeating same column ******/ 
			Timer timer = new Timer() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					
					/** Date 11-08-2018 by Vijay added for revised old quotation status change ***/
					quotation.setOldQuotationId(model.getCount());
					
					form.updateView(quotation);
					form.setToEditState();
					form.toggleAppHeaderBarMenu();
					
					
					
					form.getTbQuotatinId().setValue("");
					form.getTbContractId().setValue("");
					form.getTbQuotatinId().setValue("");
					form.getDbQuotationDate().setValue(null);
					form.getDbValidUntill().setValue(null);
					
					form.getOlbbBranch().setValue(model.getBranch());
			    	 Branch branchEntity =form.getOlbbBranch().getSelectedItem();
			    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
							form.olbcNumberRange.setValue(branchEntity.getNumberRange());
							Console.log("Number range set to drop down by default");
			    	  }else {
			    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
					  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
							if(range!=null&&!range.equals("")) {
								form.olbcNumberRange.setValue(range);
								Console.log("in else Number range set to drop down by default");							
							}else {
								form.olbcNumberRange.setSelectedIndex(0);
								Console.log("could not set number range");
							}					
			    	  }
				}
			};
			timer.schedule(2000);
			
//			form.tbQuotationStatus.setValue(Quotation.CREATED);
//			
		} 
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	

	private Quotation getRevisedQuotation(Quotation model) {
		Quotation object=new Quotation();
		
		object.setCategory(model.getCategory());
		object.setCinfo(model.getCinfo());
		object.setCreatedBy(model.getCreatedBy());
		object.setTotalAmount(model.getTotalAmount());
		object.setTotalAmount(model.getTotalAmount());
		object.setServiceScheduleList(model.getServiceScheduleList());
		object.setSdRemark(model.getSdRemark());
		object.setCustomerCellNumber(model.getCustomerCellNumber());
		object.setCustomerFullName(model.getCustomerFullName());
		object.setCstpercent(model.getCstpercent());
		object.setCstName(model.getCstName());
		object.setEmployee(model.getEmployee());
		object.setSdBankAccNo(model.getSdBankAccNo());
		object.setWarrantyPeriod(model.getWarrantyPeriod());
		object.setCategoryKey(model.getCategoryKey());
		object.setComplaintFlag(model.getComplaintFlag());
		object.setCformstatus(model.getCformstatus());
		object.setChemicaltouse(model.getChemicaltouse());
		object.setCreationDate(model.getCreationDate());
		object.setCreatedBy(model.getCreatedBy());
		object.setCreationTime(model.getCreationTime());
		object.setCreditPeriod(model.getCreditPeriod());
		object.setCustomeQuotationUpload(model.getCustomeQuotationUpload());
		object.setDescription(model.getDescription());
		object.setDeleted(model.isDeleted());
		object.setDepositReceived(model.isDepositReceived());
		object.setValidUntill(model.getValidUntill());
		object.setTicketNumber(model.getTicketNumber());
		object.setSdRemark(model.getSdRemark());
		object.setFreqService(model.getFreqService());
		object.setFlatDiscount(model.getFlatDiscount());
		object.setSdDocDate(model.getSdDocDate());
		object.setSdBankAccNo(model.getSdBankAccNo());
		object.setSdBankBranch(model.getSdBankBranch());
		object.setSdBankName(model.getSdBankName());
		object.setGroup(model.getGroup());
		object.setGroupKey(model.getGroupKey());
		object.setGrandTotal(model.getGrandTotal());
		object.setBranch(model.getBranch());
		object.setPriority(model.getPriority());
		object.setPaymentTermsList(model.getPaymentTermsList());
		object.setProductCharges(model.getProductCharges());
		object.setProductTaxes(model.getProductTaxes());
		object.setPaymentTermsList(model.getPaymentTermsList());
		object.setItems(model.getItems());
		object.setManPowCostList(model.getManPowCostList());
		object.setMatCostList(model.getMatCostList());
		object.setSdDocStatus(model.getSdDocStatus());
		object.setTotalAmount(model.getTotalAmount());
		object.setNetpayable(model.getNetpayable());
		object.setSdPayableAt(model.getSdPayableAt());
		object.setSdPaymentMethod(model.getSdPaymentMethod());
		object.setSdReturnDate(model.getSdReturnDate());
		
		if(model.getPremisesDesc()!=null)
			object.setPremisesDesc(model.getPremisesDesc());
		
		if(model.getInspectedBy()!=null)
			object.setInspectedBy(model.getInspectedBy());
		
		if(model.getInspectionDate()!=null)
			object.setInspectionDate(model.getInspectionDate());
		
		if(model.getDesignation()!=null)
			object.setDesignation(model.getDesignation());
		
		if(model.getServiceScheduleList()!=null){
			object.setServiceScheduleList(model.getServiceScheduleList());
		}
		if(model.getDocument()!=null){
			object.setDocument(model.getDocument());
		}
		if(model.getSdInstruction()!=null){
			object.setSdInstruction(model.getSdInstruction());
		}
		if(model.getSdAddress()!=null){
			object.setSdAddress(model.getSdAddress());
		}
		/*** Date 10-04-2019 by Vijay for NBHC CCPM Lead id mapping in revised quotation.***/
		if(model.getLeadCount()!=0){
		object.setLeadCount(model.getLeadCount());
		}
		return object;
	}

	/**
	 * Date 14/12/2017
	 * By Jayshree
	 * Des.To called the quotation pdf with with data and without data parameter is added
	 * 
	 */
	private void reactOnPrintNBHCLOIPdf(String loitype,String prePrint) {
	System.out.println("inside react on print NBHC"+loitype);
	final String url = GWT.getModuleBaseURL()+"pdfprintservice"+"?Id="+model.getId()
			+"&"+"type="+"LOI"+"&"+"loiType="+loitype+"&prePrint="+prePrint;
	Window.open(url, "test", "enabled");
	
	}
	/**
	 * Ends For Jayshree
	 */
	
	
	/// below method created by Ajinkya (Date:13/12/2016
	
private void reactOnprintchecklistWoodBorer() {
		
		final String url = GWT.getModuleBaseURL() + "PcambQuotationpdf"+"?Id="+model.getId()+"&"+"type="+"WoodBorer";
		Window.open(url, "test", "enabled");
	}

private void reactOnprintchecklistBedBugs() {
	
	final String url = GWT.getModuleBaseURL() + "PcambQuotationpdf"+"?Id="+model.getId()+"&"+"type="+"BedBugs";
	Window.open(url, "test", "enabled");
}
  
private void reactOnprintchecklistFlyMosquito() {
	
	final String url = GWT.getModuleBaseURL() + "PcambQuotationpdf"+"?Id="+model.getId()+"&"+"type="+"FlyMosquito";
	Window.open(url, "test", "enabled");
}
   

	private void reactOnprintchecklistRodent() {
		
		final String url = GWT.getModuleBaseURL() + "PcambQuotationpdf"+"?Id="+model.getId()+"&"+"type="+"Rodent";
		Window.open(url, "test", "enabled");
	}


/// below method created by Ajinkya (Date:9/1/2017
private void reactOnprintchecklistTermite() {
	
	final String url = GWT.getModuleBaseURL() + "PcambQuotationpdf"+"?Id="+model.getId()+"&"+"type="+"Termite";
	Window.open(url, "test", "enabled");
}

private void reactOnprintchecklistGipc() {
	
	final String url = GWT.getModuleBaseURL() + "PcambQuotationpdf"+"?Id="+model.getId()+"&"+"type="+"Gipc";
	Window.open(url, "test", "enabled");
}
// method ends here  edited9/1/2017
	
	private void reactToCreateQuickContract() {
		
		boolean validateCopyingForData=CopyDataConfig.validateDataCopying(AppConstants.CONTRACTCOPYDATA);
		boolean categoryDataVal=false;
		boolean typeDataVal=false;
		boolean groupDataVal=false;
		
		if(validateCopyingForData==true)
		{
			categoryDataVal=CopyDataConfig.validateData(AppConstants.COPYCATEGORYDATA, model.getCategory(), Screen.CONTRACTCATEGORY);
			typeDataVal=CopyDataConfig.validateData(AppConstants.COPYTYPEDATA, model.getType(), Screen.CONTRACTTYPE);
			groupDataVal=CopyDataConfig.validateData(AppConstants.COPYGROUPDATA, model.getGroup(), Screen.CONTRACTGROUP);
			
			
			
			if(categoryDataVal==false){
				form.showDialogMessage("Contract Category Value does not exists in Quotation Category!");
			}
			
			if(categoryDataVal==true&&typeDataVal==false){
				form.showDialogMessage("Contract Type value does not exists in Quotation Type!");
			}
			
			if(groupDataVal==false&&typeDataVal==true&&categoryDataVal==true){
				form.showDialogMessage("Contract Group value does not exists in Quotation Group!");
			}
			
			
			if(groupDataVal==true&&typeDataVal==true&&categoryDataVal==true){
				switchToQuickContractScreen(validateCopyingForData);
			}
		}
		else{
			switchToQuickContractScreen(validateCopyingForData);
		}
		
		
		//   rohan bhagde added this method 
		findMaxServiceSrNumberQuotation();
	}



	/**
	 * This is used to create quick Contract form quotation 
	 * Date : 15/12/2016
	 *  	
	 */
	private void switchToQuickContractScreen(boolean validateCopyingForData) {
		
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quick Contract",Screen.QUICKCONTRACT);
		final QuickContractForm form=QuickContractPresentor.initalize();
		
		/**
		 * Date : 11-05-2017 BY ANIL
		 */
		form.checkCustomerBranch(model.getCinfo().getCount());
		/**
		 * End
		 */
		
		form.showWaitSymbol();
		Timer t = new Timer() {
		      @Override
		      public void run() {
		    	  form.setToNewState();
		    	  form.hideWaitSymbol();
		    	  form.getTbContractId().setText("");
		    	  form.getPersonInfoComposite().setEnabled(false);
		    	  form.getPersonInfoComposite().setValue(model.getCinfo());
		    	  
		    	  //  rohan added this code for loading customer branch date :3/2/2017
		    	  
		    	  QuickContractPresentor.custcount=model.getCinfo().getCount();
		    	  //   ends here 
		    	  
		    	  form.getOlbbBranch().setValue(model.getBranch());
		    	  form.getOlbApproverName().setValue(model.getApproverName());

//		    	  if(model.getContractCount()!=-1){
//		    		  form.getTbContractId().setValue(model.getContractCount()+"");
//		    	  }
		    	  
		    	  
		    	  if(model.getCreditPeriod()!=null){
		    		  form.getIbCreditPeriod().setValue(model.getCreditPeriod());
		    	  }
//		    	  form.getCustomerAddress(model.getCinfo().getCount());
		    	  form.getTbQuotatinId().setValue(model.getCount()+"");
		    	  form.getTbQuotatinId().setEnabled(false);
		    	  
		    	  /**
		    	   * Date : 12-05-2017 By Anil
		    	   * This method update itemlist by adding branch info at product level.
		    	   */
		    	  List<SalesLineItem>itemList=AppUtility.getItemList(model.getItems(), model.getServiceScheduleList(),form.customerbranchlist,model.getBranch());
//		    	  List<SalesLineItem>itemList=null;
		    	  if(itemList!=null){
		    		  form.getSaleslineitemtable().setValue(itemList);
		    	  }else{
		    		 form.setToNewState();
		    		 form.showDialogMessage("No. of branches at product level and no. of selected branchs at service scheduling popup are different.");
		    		 return;
		    		 
		    	  }
		    	  /**
		    	   * End
		    	   */
		    	  
//		    	  form.getSaleslineitemtable().setValue(model.getItems());
		    	
		    	  form.getOlbcPaymentMethods().setValue(model.getPaymentMethod());
		    	  form.getOlbeSalesPerson().setValue(model.getEmployee());
		    	  form.getTbStatus().setValue(Quotation.CREATED);
		    	  form.getProdTaxTable().setValue(model.getProductTaxes());
		    	  form.getChargesTable().setValue(model.getProductCharges());
//		    	  form.getChargesTable().setEnable(false);
		    	  form.getDototalamt().setValue(model.getTotalAmount());
		    	  form.getDonetpayamt().setValue(model.getNetpayable());
		    	  if(model.getServiceScheduleList().size()!=0){
		    		  form.getServiceScheduleTable().setValue(model.getServiceScheduleList());
		    	  }
		    	  
//		    	  if(model.getScheduleServiceDay()!=null){
//		    		  form.setF_serviceDay(model.getScheduleServiceDay());
//		    	  }
//		    	  
//		    	  form.getChkbillingatBranch().setValue(false);
//		    	  form.getChkservicewithBilling().setValue(false);
//		    	  if(model.getPremisesDesc()!=null){
//		    	  form.getTbPremisesUnderContract().setValue(model.getPremisesDesc());
		    	  if(QuotationPresenter.this.form.isSalesPersonRestrictionFlag())
		    	  {
		    		  form.getOlbeSalesPerson().setEnabled(false);
		    	  }
		    	  else
		    	  {
		    		  form.getOlbeSalesPerson().setEnabled(true);
		    	  }
		    	  
		    	  	form.getTbLeadId().setValue(model.getCount()+"");
					form.getTbLeadId().setEnabled(false);
			    	form.hideWaitSymbol();

			    	if(model.getQuotationNumberRange()!=null && !model.getQuotationNumberRange().equals("")){
				    	  form.getOlbcNumberRange().setValue(model.getQuotationNumberRange());
			    	}
			    	form.getOlbbBranch().setValue(model.getBranch());
			    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
			    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
							form.olbcNumberRange.setValue(branchEntity.getNumberRange());
							Console.log("Number range set to drop down by default");
			    	  }else {
			    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
					  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
							if(range!=null&&!range.equals("")) {
								form.olbcNumberRange.setValue(range);
								Console.log("in else Number range set to drop down by default");							
							}					
			    	  }
		    	  }
		    };
		    t.schedule(5000);
	}




	/**
	 * Rohan added this method for making quotation as successful
	 * Date : 14/12/2016
	 * Reason : in case of quick contract we have to make quotation successful (Bhavani pest requirement)
	 */
	private void markQuotationSuccessful() {
		
		model.setStatus(Quotation.QUOTATIONSUCESSFUL);
		form.showWaitSymbol();
		Timer timer=new Timer() 
    	 {
				@Override
				public void run() {
					genasync.save(model,new AsyncCallback<ReturnFromServer>() {
						
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
					}

					@Override
					public void onSuccess(ReturnFromServer result) {
						form.getTbQuotationStatus().setValue(Quotation.QUOTATIONSUCESSFUL); 
						form.setAppHeaderBarAsPerStatus();
						form.setToViewState();
						/**Date 20-12-2019 by Amol when quotation is mark successfull contract will Automatically Created**/
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", "CreateContractAutoOnMarkSuccessfull")){
							Console.log("Inside ON Success quotationSuccessful");
							createContractAutoOnQuotationMarkSuccessfull();
							
						}
						form.showDialogMessage("Updated Successfully!");
					}
					});
						form.hideWaitSymbol();
					}
 			};
            timer.schedule(3000);
	}




	@Override
	public void reactOnPrint() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("Quotation");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}			

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result set size +++++++"+result.size());
				
				List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
				
				if(result.size()==0){
					
					final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
					Window.open(url, "test", "enabled");
				}
				else{
				
					for(SuperModel model:result)
					{
						ProcessConfiguration processConfig=(ProcessConfiguration)model;
						processList.addAll(processConfig.getProcessList());
						
					}
				
				for(int k=0;k<processList.size();k++){	
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
					
					cnt=cnt+1;
				
				}
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PrintCustomQuotation")&&processList.get(k).isStatus()==true){
					
					customQuotation=customQuotation+1;
				
				}
				
				//******************pestal quotations code ******************
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestleQuotations")&&processList.get(k).isStatus()==true){
					System.out.println("in side pestle ");
					pestQuotation=pestQuotation+1;
				
				}
				
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestoIndiaQuotations")&&processList.get(k).isStatus()==true){
					
					pestoIndia=pestoIndia+1;
				
				}
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("NBHCQuotation")&&processList.get(k).isStatus()==true){
					nbhc=nbhc+1;
				}
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("UniversalPestCustomization")&&processList.get(k).isStatus()==true){
					universal=universal+1;
				}
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase(AppConstants.PC_CUSTOMQUOTATION)&&processList.get(k).isStatus()==true){
					pestMasters=pestMasters+1;
				}
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase(AppConstants.PC_QUOTATIONFOTMATVERSION1)&&processList.get(k).isStatus()==true){
					quotationFormatVersion1=quotationFormatVersion1+1;
				}
				
				}
				
				
				
				if(pestQuotation > 0)
				{
					if(model.getGroup().equalsIgnoreCase("Five Years Quotation")|| model.getGroup().equalsIgnoreCase("Normal Quotation"))
					{
					System.out.println("in side react on prinnt pestQuotation");
					panel=new PopupPanel(true);
					panel.add(conditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
					}
					else
					{
							final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
							Window.open(url, "test", "enabled");
							panel.hide();
					}
				
				}
				else if(nbhc > 0)
				{
					System.out.println("in side react on prinnt NBHC Quotation");
					panel=new PopupPanel(true);
					panel.add(conditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
				}
				else if(universal > 0)
				{
					System.out.println("in side react on prinnt Universal pest Quotation");
					panel=new PopupPanel(true);
					panel.add(conditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
				}
				else if(pestoIndia > 0)
				{
					System.out.println("in side react on prinnt pestQuotation");
					panel=new PopupPanel(true);
					panel.add(conditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
					
				}
				
				else if(customQuotation > 0)
				{
					
					if(model.getGroup().equalsIgnoreCase("Five Years")|| model.getGroup().equalsIgnoreCase("Direct Quotation")
							|| model.getGroup().equalsIgnoreCase("Building")|| model.getGroup().equalsIgnoreCase("After Inspection"))
					{
					
					System.out.println("in side react on prinnt customQuotation");
					panel=new PopupPanel(true);
					panel.add(conditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
					}
					else
					{
							final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
							Window.open(url, "test", "enabled");
							panel.hide();
					}
				
				}
				
				else if(cnt>0){
					System.out.println("in side react on prinnt cnt");
					panel=new PopupPanel(true);
					panel.add(conditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
				}
				
				else if(pestMasters>0){
					panel=new PopupPanel(true);
					panel.add(conditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
				}
				
				else if(quotationFormatVersion1>0){
					
					panel=new PopupPanel(true);
					panel.add(conditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
				}
				
				else
				{
					final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
					Window.open(url, "test", "enabled");
				
				}
				
			}
			}
		});
	}

	@Override
	public void reactOnDownload() 
	{
		ArrayList<Quotation> custarray=new ArrayList<Quotation>();
		List<Quotation> list=(List<Quotation>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		custarray.addAll(list);
		
		csvservice.setquotationlist(custarray, new AsyncCallback<Void>() {
			

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+2;
				Window.open(url, "test", "enabled");
				
//				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//				final String url=gwt + "CreateXLXSServlet"+"?type="+11;
//				Window.open(url, "test", "enabled");
			}
		});

	}

	
	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model=new Quotation();
	}




	public static QuotationForm initalize()
	{
				QuotationForm form=new  QuotationForm();
				QuotationPresenterTable gentable=new QuotationPresenterTableProxy();
				gentable.setView(form);
				gentable.applySelectionModle();
				QuotationPresenterSearch.staticSuperTable=gentable;
				QuotationPresenterSearch searchpopup=new QuotationPresenterSearchProxy();
				form.setSearchpopupscreen(searchpopup);
				QuotationPresenter  presenter=new QuotationPresenter(form,new Quotation());
//				form.setToNewState();
				AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.Quotation")
	public static  class QuotationPresenterSearch extends SearchPopUpScreen<Quotation>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};

		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.Quotation")
		public static class QuotationPresenterTable extends SuperTable<Quotation> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub

			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub

			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub

			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub

			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub

			}} ;

			
			private void reactToCreateContract()
			{

				boolean validateCopyingForData=CopyDataConfig.validateDataCopying(AppConstants.CONTRACTCOPYDATA);
				boolean categoryDataVal=false;
				boolean typeDataVal=false;
				boolean groupDataVal=false;
				
				if(validateCopyingForData==true)
				{
					categoryDataVal=CopyDataConfig.validateData(AppConstants.COPYCATEGORYDATA, model.getCategory(), Screen.CONTRACTCATEGORY);
					typeDataVal=CopyDataConfig.validateData(AppConstants.COPYTYPEDATA, model.getType(), Screen.CONTRACTTYPE);
					groupDataVal=CopyDataConfig.validateData(AppConstants.COPYGROUPDATA, model.getGroup(), Screen.CONTRACTGROUP);
					
					
					
					if(categoryDataVal==false){
						form.showDialogMessage("Contract Category Value does not exists in Quotation Category!");
					}
					
					if(categoryDataVal==true&&typeDataVal==false){
						form.showDialogMessage("Contract Type value does not exists in Quotation Type!");
					}
					
					if(groupDataVal==false&&typeDataVal==true&&categoryDataVal==true){
						form.showDialogMessage("Contract Group value does not exists in Quotation Group!");
					}
					
					
					if(groupDataVal==true&&typeDataVal==true&&categoryDataVal==true){
						switchToContractScreen(validateCopyingForData);
					}
				}
				else{
					switchToContractScreen(validateCopyingForData);
				}
				
				
				//   rohan bhagde added this method 
				findMaxServiceSrNumberQuotation();
			}
			
			//  rohan added this method
			private void findMaxServiceSrNumberQuotation() {
				
				int maxSrNumber = 0 ;
				System.out.println("rohan in side max service number");
				
				List<SalesLineItem> list = form.saleslineitemquotationtable.getDataprovider().getList();
				System.out.println("mazza list size "+list.size());
				
				Comparator<SalesLineItem> compare = new  Comparator<SalesLineItem>() {
					
					@Override
					public int compare(SalesLineItem o1, SalesLineItem o2) {
						// TODO Auto-generated method stub
						return Integer.compare(o2.getProductSrNo(), o1.getProductSrNo());
					}
				};
				Collections.sort(list,compare);
				
					System.out.println("product sr number "+list.get(0).getProductSrNo());
						maxSrNumber = list.get(0).getProductSrNo();
						ContractForm.productSrNo = maxSrNumber;
			}
			
			private void switchToContractScreen(boolean statusValue)
			{
				AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract",Screen.CONTRACT);
				final ContractForm form=ContractPresenter.initalize();
				/**Date 16-9-2019 by Amol for nonediteble column in product level**/
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","MakeColumnDisable")){
				form.saleslineitemtable.quotationCount=model.getCount();
				}
				/**Date 12-9-2019 by Amol**/
				final Contract contrct=new Contract();
				contrct.setBranch(model.getBranch());
				contrct.setEmployee(model.getEmployee());
				contrct.setType(model.getType());
				contrct.setCategory(model.getCategory());
				contrct.setGroup(model.getGroup());
				
				/**
				 * Date : 11-05-2017 BY ANIL
				 */
				form.checkCustomerBranch(model.getCinfo().getCount());
				/**
				 * End
				 */
				
				form.showWaitSymbol();
				Timer t = new Timer() {
				      @Override
				      public void run() {
				    	  form.setToNewState();
				    	  form.hideWaitSymbol();
				    	  form.updateView(contrct);
				    	  form.getTbContractId().setText("");
				    	  if(model.getLeadCount()==-1)
				    		  form.getTbLeadId().setText("");
				    	  form.getPersonInfoComposite().setEnabled(false);
				    	  form.getTbReferenceNumber().setText("");
				    	  form.getPersonInfoComposite().setValue(model.getCinfo());
				    	  
				    	  //  rohan added this code for loading customer branch date :3/2/2017
				    	  
				    	  	ContractPresenter.custcount=model.getCinfo().getCount();
				    	  //   ends here 
				    	  	 /**
					    	   * Date : 18-07-2017 by Anil
					    	   * added this code for mapping customer category to segment field for NBHC CCPM
					    	   */
					    	  form.checkCustomerStatus(model.getCinfo().getCount(),true,true);
					    	  /**
					    	   * End
					    	   */
					      if(model.getPaymentModeName()!=null) {
				    	  form.getObjPaymentMode().setValue(model.getPaymentModeName());
					      }
				    	  form.getOlbbBranch().setValue(model.getBranch());
				    	  form.getOlbApproverName().setValue(model.getApproverName());
				    	  if(model.getLeadCount()!=-1){
				    		  form.getTbLeadId().setValue(model.getLeadCount()+"");
				    	  }
//				    	  if(model.getContractCount()!=-1){
//				    		  form.getTbContractId().setValue(model.getContractCount()+"");
//				    	  }
				    	  
				    	  if(model.getTicketNumber()!=-1){
				    		  System.out.println("SETTING TICKET NUMBER FROM QUOTATION !!!!!!!!!!!");
				    		  form.getTbTicketNumber().setValue(model.getTicketNumber()+"");
				    	  }
				    	  
				    	  
				    	  if(model.getCreditPeriod()!=null){
				    		  form.getIbCreditPeriod().setValue(model.getCreditPeriod());
				    	  }
				    	  
				    	  /**
					  		 * Date : 31-08-2017 BY Vijay
					  		 * If service schedule list is greater than 700 then we have to retrieve data from separate entity as 
					  		 * we are storing it in separate entity.
					  		 * we have to read schedule data from an entity and then add it contract shcedule popup
					  		 * old code added in else block and on success
					  		 */
					  		if(model.getServiceScheduleList().size()==0){
					  			Console.log("SERVICE SCHEDULE LIST ");
					  			Console.log("COM_ID "+model.getCompanyId()+" DOC_TYPE "+"Contract"+" DOC_ID "+model.getCount());
					  			final ContractServiceAsync conSer=GWT.create(ContractService.class);
					  			
					  			conSer.getScheduleServiceSearchResult(model.getCompanyId(), "Quotation", model.getCount(), new AsyncCallback<ArrayList<ServiceSchedule>>() {
					  				@Override
					  				public void onFailure(Throwable caught) {
					  					Console.log("SERVICE SCHEDULE FAILURE ");
					  				}
					  				@Override
					  				public void onSuccess(ArrayList<ServiceSchedule> result) {
					  					Console.log("SERVICE SCHEDULE SUCCESS "+result.size());
					  					if(result.size()!=0){
					  						/**
									    	   * Date : 12-05-2017 By Anil
									    	   * This method update item list by adding branch info at product level.
									    	   */
									    	  List<SalesLineItem>itemList=AppUtility.getItemList(model.getItems(),result,form.customerbranchlist,model.getBranch());
//									    	  List<SalesLineItem>itemList=null;
									    	  if(itemList!=null){
									    		  form.getSaleslineitemtable().setValue(itemList);
									    	  }else{
									    		 form.setToNewState();
									    		 form.showDialogMessage("No. of branches at product level and no. of selected branchs at service scheduling popup are different.");
									    		 return;
									    		 
									    	  }
									    	  /**
									    	   * End
									    	   */
									    	  
//									    	  form.getSaleslineitemtable().setValue(model.getItems());
									    	  
									    	  form.getTbQuotatinId().setValue(model.getCount()+"");
									    	  form.getOlbcPaymentMethods().setValue(model.getPaymentMethod());
									    	  form.getOlbeSalesPerson().setValue(model.getEmployee());
									    	  form.getTbQuotationStatus().setValue(Quotation.CREATED);
									    	  form.getProdTaxTable().setValue(model.getProductTaxes());
									    	  form.getChargesTable().setValue(model.getProductCharges());
//									    	  form.getChargesTable().setEnable(false);
									    	  form.getPaymentTermsTable().setValue(model.getPaymentTermsList());
									    	  form.getDototalamt().setValue(model.getTotalAmount());
									    	  form.getDonetpayamt().setValue(model.getNetpayable());
									    	  form.getServiceScheduleTable().setValue(result);
									    	  
									    	  if(model.getScheduleServiceDay()!=null){
									    		  form.setF_serviceDay(model.getScheduleServiceDay());
									    	  }
									    	  
									    	  form.getChkbillingatBranch().setValue(false);
									    	  form.getChkservicewithBilling().setValue(false);
									    	  if(model.getPremisesDesc()!=null){
									    	  form.getTbPremisesUnderContract().setValue(model.getPremisesDesc());
									    	  if(QuotationPresenter.this.form.isSalesPersonRestrictionFlag())
									    	  {
									    		  form.getOlbeSalesPerson().setEnabled(false);
									    	  }
									    	  else
									    	  {
									    		  form.getOlbeSalesPerson().setEnabled(true);
									    	  }
									    	  }
									    	  
									    	  /** Date 05-09-2017 added by vijay for as per new code discount and round off ****/
									    	  form.getDbDiscountAmt().setValue(model.getDiscountAmt());
									    	  form.getTbroundOffAmt().setValue(model.getRoundOffAmt()+"");
									    	  
									    	  /**
									    	   * Date : 30-10-2017 BY ANIL
									    	   */
									    	  form.getCbServiceWiseBilling().setValue(false);
									    	  /**
									    	   * End
									    	   */
									    	  /**
									    	   * Date : 24-02-2018
									    	   * map  contact list
									    	   * 
									    	   */
									    	  form.getCustomerContactList(model.getCount(),null);
									    	  /**
									    	   * end
									    	   */
									    	  

									    	  /**
									    	   * Updated By: Viraj
									    	   * Date: 05-04-2019
									    	   * Description: To map consolidate amount checkbox value in contract
									    	   */
									    	  Console.log("cbConsolidatePrice: "+ model.isConsolidatePrice());
								    		  form.getCbConsolidatePrice().setValue(model.isConsolidatePrice());
									    	  
									    	  /** Ends **/
									    	  /**
									    	   * end
									    	   */
									    	  
									    	  /**
										  		 * Date 28-09-2018 By Vijay 
										  		 * Des:- NBHC CCPM for every contract this must be true by default it does not allow to change for user
										  		 */
										  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
										  			form.cbStartOfPeriod.setValue(true);
										  			form.cbStartOfPeriod.setEnabled(false);
										  		}
										  		/**
										  		 * ends here
										  		 */
										  		
										  		if(model.getBillingAddress()!=null && model.getBillingAddress().getAddrLine1()!=null 
										  				  && !model.getBillingAddress().getAddrLine1().equals("")) {
											  		form.customerBillingAddress = model.getBillingAddress();
										  		}
										  		if(model.getServiceAddress()!=null && model.getServiceAddress().getAddrLine1()!=null 
										  				  && !model.getServiceAddress().getAddrLine1().equals("")) {
											  		form.customerServiceAddress = model.getServiceAddress();
										  		}
										  		
										  		/**
										  		 * @author Vijay Date :- 28-01-2021
										  		 * Des :- if Free Material process config is active then stationed Service flag will true 
										  		 * or else it will false. if this flag is true then service will create with Automatic Scheduling 
										  		 */
										  		form.getChkStationedService().setValue(false);
										  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.PC_FREEMATERIAL)){
											  		form.getChkStationedService().setValue(true);
												}
										  		else{
											  		form.getChkStationedService().setValue(false);
										  		}
					  					}
					  				}
					  			});
					  		}else{
					  			/**
						    	   * Date : 12-05-2017 By Anil
						    	   * This method update item list by adding branch info at product level.
						    	   */
						    	  List<SalesLineItem>itemList=AppUtility.getItemList(model.getItems(), model.getServiceScheduleList(),form.customerbranchlist,model.getBranch());
//						    	  List<SalesLineItem>itemList=null;
						    	  if(itemList!=null){
						    		  form.getSaleslineitemtable().setValue(itemList);
						    	  }else{
						    		 form.setToNewState();
						    		 form.showDialogMessage("No. of branches at product level and no. of selected branchs at service scheduling popup are different.");
						    		 return;
						    		 
						    	  }
						    	  /**
						    	   * End
						    	   */
						    	  
//						    	  form.getSaleslineitemtable().setValue(model.getItems());
						    	  
						    	  form.getTbQuotatinId().setValue(model.getCount()+"");
						    	  form.getOlbcPaymentMethods().setValue(model.getPaymentMethod());
						    	  form.getOlbeSalesPerson().setValue(model.getEmployee());
						    	  form.getTbQuotationStatus().setValue(Quotation.CREATED);
						    	  form.getProdTaxTable().setValue(model.getProductTaxes());
						    	  form.getChargesTable().setValue(model.getProductCharges());
//						    	  form.getChargesTable().setEnable(false);
						    	  form.getPaymentTermsTable().setValue(model.getPaymentTermsList());
						    	  form.getDototalamt().setValue(model.getTotalAmount());
						    	  form.getDonetpayamt().setValue(model.getNetpayable());
						    	  if(model.getServiceScheduleList().size()!=0){
						    		  form.getServiceScheduleTable().setValue(model.getServiceScheduleList());
						    	  }
						    	  
						    	  if(model.getScheduleServiceDay()!=null){
						    		  form.setF_serviceDay(model.getScheduleServiceDay());
						    	  }
						    	  
						    	  form.getChkbillingatBranch().setValue(false);
						    	  form.getChkservicewithBilling().setValue(false);
						    	  if(model.getPremisesDesc()!=null){
						    	  form.getTbPremisesUnderContract().setValue(model.getPremisesDesc());
						    	  if(QuotationPresenter.this.form.isSalesPersonRestrictionFlag())
						    	  {
						    		  form.getOlbeSalesPerson().setEnabled(false);
						    	  }
						    	  else
						    	  {
						    		  form.getOlbeSalesPerson().setEnabled(true);
						    	  }
						    	  }
						    	  
						    	  /** Date 05-09-2017 added by vijay for as per new code discount and round off ****/
						    	  form.getDbDiscountAmt().setValue(model.getDiscountAmt());
						    	  form.getTbroundOffAmt().setValue(model.getRoundOffAmt()+"");
						    	  
						    	  /**
						    	   * Date : 30-10-2017 BY ANIL
						    	   */
						    	  form.getCbServiceWiseBilling().setValue(false);
						    	  /**
						    	   * End
						    	   */
						    	  /**
						    	   * Date : 24-02-2018
						    	   * map  contact list
						    	   * 
						    	   */
						    	  form.getCustomerContactList(model.getCount(),null);
						    	  /**
						    	   * end
						    	   */
						    	  

						    	  /**
						    	   * Updated By: Viraj
						    	   * Date: 05-04-2019
						    	   * Description: To map consolidate amount checkbox value in contract
						    	   */
						    	  Console.log("cbConsolidatePrice: "+ model.isConsolidatePrice());
					    		  form.getCbConsolidatePrice().setValue(model.isConsolidatePrice());
						    	  
						    	  /** Ends **/
						    	  /**
						    	   * end
						    	   */
						    	  
						    	   /**
							  		 * Date 28-09-2018 By Vijay 
							  		 * Des:- NBHC CCPM for every contract this must be true by default it does not allow to change for user
							  		 */
							  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
							  			form.cbStartOfPeriod.setValue(true);
							  			form.cbStartOfPeriod.setEnabled(false);
							  		}
							  		/**
							  		 * ends here
							  		 */
							  		
							  		if(model.getBillingAddress()!=null && model.getBillingAddress().getAddrLine1()!=null 
							  				  && !model.getBillingAddress().getAddrLine1().equals("")) {
								  		form.customerBillingAddress = model.getBillingAddress();
								  		Console.log("quotation ==");
							  		}
							  		if(model.getServiceAddress()!=null && model.getServiceAddress().getAddrLine1()!=null 
							  				  && !model.getServiceAddress().getAddrLine1().equals("")) {
								  		form.customerServiceAddress = model.getServiceAddress();
								  		Console.log("quotation $ ==");
							  		}
							  		
							  		/**
							  		 * @author Vijay Date :- 28-01-2021
							  		 * Des :- if Free Material process config is active then stationed Service flag will true 
							  		 * or else it will false. if this flag is true then service will create with Automatic Scheduling 
							  		 */
							  		form.getChkStationedService().setValue(false);
//							  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.PC_FREEMATERIAL)){
//								  		form.getChkStationedService().setValue(true);
//									}
//							  		else{
//								  		form.getChkStationedService().setValue(false);
//							  		}
							  		
							  		
							  		if(model.getQuotationNumberRange()!=null && !model.getQuotationNumberRange().equals("")){
								    	  form.getOlbcNumberRange().setValue(model.getQuotationNumberRange());
							    	  }
							  		
							  		form.getOlbbBranch().setValue(model.getBranch());
							    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
							    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
											form.olbcNumberRange.setValue(branchEntity.getNumberRange());
											Console.log("Number range set to drop down by default");
							    	  }else {
							    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
									  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
											if(range!=null&&!range.equals("")) {
												form.olbcNumberRange.setValue(range);
												Console.log("in else Number range set to drop down by default");							
											}					
							    	  }
							  		if(model.getPaymentModeName()!=null) {
							  		form.getObjPaymentMode().setValue(model.getPaymentModeName());
							  		}
					  		}
					    	  form.hideWaitSymbol();
				      }
				    };
				    t.schedule(5000);
				
			}
			private void reactToUnsuccessful()
			{
				model.setStatus(Quotation.QUOTATIONUNSUCESSFUL);
				form.getTbQuotationStatus().setText(Quotation.QUOTATIONUNSUCESSFUL);
				save("Marked Unsuccessful !","Failed To Mark Unsuccessful !");
				form.setMenuAsPerStatus();
			}
			private void reactToNew()
			{
				form.setToNewState(); 
				this.initalize();
			}
			
			private void save(final String success,final String failure)
			{
				
				async.changeStatus(model,new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage(failure);
						
					}

					@Override
					public void onSuccess(Void result) {
						form.showDialogMessage(success);
						
					}
				});
			}

			

			@Override
			public void onRowCountChange(RowCountChangeEvent event) 
			{
				NumberFormat nf=NumberFormat.getFormat("0.00");
				if(event.getSource().equals(form.getSaleslineitemquotationtable().getTable()))
				{
//				    double totalExcludingTax=form.getSaleslineitemquotationtable().calculateTotalExcludingTax();
//				    totalExcludingTax=Double.parseDouble(nf.format(totalExcludingTax));
//				    form.getDototalamt().setValue(totalExcludingTax);
//				    
//					boolean chkSize=form.getSaleslineitemquotationtable().removeChargesOnDelete();
//					if(chkSize==false){
//						form.getChargesTable().connectToLocal();
//					}
//					
//					try {
//						form.prodTaxTable.connectToLocal();
//						form.addProdTaxes();
//						double totalIncludingTax=form.getDototalamt().getValue()+form.getProdTaxTable().calculateTotalTaxes();
//						form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
//						form.updateChargesTable();
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//					double netPay=this.fillNetPayable(form.getDoamtincltax().getValue());
//					netPay=Math.round(netPay);
//					int netPayable=(int) netPay;
//					netPay=netPayable;
//					form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
					this.reactOnLineItem();
					
					if(form.getSaleslineitemquotationtable().getDataprovider().getList().size()==0)
					{
						form.getServiceScheduleTable().connectToLocal();
						serviceSchedulePopUp.getPopScheduleTable().connectToLocal();
					}
					
//					if(form.servicesFlag!=true){
							reactFordeleteproduct(form.getSaleslineitemquotationtable().identifyProductId(),form.getSaleslineitemquotationtable().identifySrNo());
//					}
//					form.servicesFlag=false;
			    }
				
				if(event.getSource().equals(form.getChargesTable().getTable())){
					if(form.getDoamtincltax().getValue()!=null){
						double newnetpay=form.getDoamtincltax().getValue()+form.getChargesTable().calculateNetPayable();
						newnetpay=Math.round(newnetpay);
						int netPayAmt=(int) newnetpay;
						newnetpay=netPayAmt;
//						form.getDonetpayamt().setValue(Double.parseDouble(nf.format(newnetpay)));
						/** Date 02-09-2017 added by vijay for discount amount net payable calculation  with other charges***/
						if(form.getDonetpayamt().getValue()==null || form.getDonetpayamt().getValue()==0 ){
							form.getDonetpayamt().setValue(newnetpay);
						}else{
							String roundOff = form.getTbroundOffAmt().getValue();
							double roundoffAmt=0;
							if(!roundOff.equals(""))
							 roundoffAmt =  AppUtility.calculateRoundOff(roundOff, newnetpay);
							if(roundoffAmt!=0){
								form.getDonetpayamt().setValue(roundoffAmt);

							}else{
								form.getDonetpayamt().setValue(roundoffAmt);
								form.getTbroundOffAmt().setValue("");
							}
						}
						form.getDbGrandTotalAmt().setValue(newnetpay);
					}
				}
				
				/**
				 * Date : 22-05-2017 BY ANIL
				 * 
				 */
				if(event.getSource().equals(costForm.getMaterialTbl().getTable())
						||event.getSource().equals(costForm.getManPowTbl().getTable())
						||event.getSource().equals(costForm.getOtherCostTbl().getTable())){
					
					double totalCost=0;
					double matCost=0;
					double manCost=0;
					double otherCost=0;
					
					for(MaterialCostStructure obj:costForm.getMaterialTbl().getDataprovider().getList()){
						matCost=matCost+obj.getTotalCost();
					}
					costForm.getDbTotalMatCost().setValue(matCost);
					
					for(ManPowerCostStructure obj:costForm.getManPowTbl().getDataprovider().getList()){
						manCost=manCost+obj.getTotalCost();
					}
					costForm.getDbTotalManPowCost().setValue(manCost);
					
					for(OtherCost obj:costForm.getOtherCostTbl().getDataprovider().getList()){
						otherCost=otherCost+obj.getTotalCost();
					}
					costForm.getDbTotalOtherCost().setValue(otherCost);
					
					totalCost=matCost+manCost+otherCost;
					costForm.getDbTotalCosting().setValue(totalCost);
					
					double totalRevenue=costForm.getDbTotalRevenue().getValue();
					double operatingMargin=(totalRevenue-totalCost)/totalRevenue*100;
					costForm.getDbOperatingMargin().setValue(operatingMargin);
					
					
				}

				/**
				 * End
				 */
				
				
			}
			
//			private void setProductSrNoMethod() {
//				if(form.getSaleslineitemquotationtable().getDataprovider().getList().size()!=0){
//				List<SalesLineItem> tableList = form.getSaleslineitemquotationtable().getDataprovider().getList();
//				for (int i=0;i<tableList.size();i++)
//				{
//					tableList.get(i).setProductSrNo(i+1);
//				}
//				
//				form.getSaleslineitemquotationtable().getDataprovider().setList(tableList);
//			}
//			}
			
			public void reactOnEmail()
			{
//				boolean conf = Window.confirm("Do you really want to send email?");
//				if (conf == true) {
//					emailService.passServiceQuotationobject((Sales) model,new AsyncCallback<Void>() {
//	
//						@Override
//						public void onFailure(Throwable caught) {
//							Window.alert("Resource Quota Ended ");
//							caught.printStackTrace();
//							
//						}
//	
//						@Override
//						public void onSuccess(Void result) {
//							Window.alert("Email Sent Sucessfully !");
//							
//						}
//					});
//				}
				
				/**
				 * @author Vijay Date 19-03-2021
				 * Des :- above old code commented 
				 * Added New Email Popup Functionality
				 */
				emailpopup.showPopUp();
				setEmailPopUpData();
				
				/**
				 * ends here
				 */
			} 
			

			private double fillNetPayable(double amtincltax)
			{
				double amtval=0;
				if(form.getChargesTable().getDataprovider().getList().size()==0)
				{
					amtval=amtincltax;
				}
				if(form.getChargesTable().getDataprovider().getList().size()!=0)
				{
					amtval=amtincltax+form.getChargesTable().calculateNetPayable();
				}
				amtval=Math.round(amtval);
				int retAmtVal=(int) amtval;
				amtval=retAmtVal;
				return amtval;
			}
			
	
		private void reactOnLineItem()
		{
			form.showWaitSymbol();
			Timer timer=new Timer() 
		     {
				@Override
				public void run() 
				{
					NumberFormat nf=NumberFormat.getFormat("0.00");
				    double totalExcludingTax=form.getSaleslineitemquotationtable().calculateTotalExcludingTax();
				    totalExcludingTax=Double.parseDouble(nf.format(totalExcludingTax));
				    form.getDototalamt().setValue(totalExcludingTax);
				    
				    /**
				     * Date 01-09-2017 added by vijay for final total amount after discount
				     */
				    if(form.getDbDiscountAmt().getValue()!=null && form.getDbDiscountAmt().getValue()!=0){
			    		if(form.getDbDiscountAmt().getValue()>totalExcludingTax){
				    		form.getDbDiscountAmt().setValue(0d);
			    		}
			    		else if(form.getDbDiscountAmt().getValue()<totalExcludingTax){
					    	form.getDbFinalTotalAmt().setValue(totalExcludingTax - form.getDbDiscountAmt().getValue());
			    		}
			    		else{
				    		form.getDbFinalTotalAmt().setValue(totalExcludingTax);
			    		}
			    	}
				    else{
				    	form.getDbFinalTotalAmt().setValue(totalExcludingTax);
			    	}
				    
				    /**
				     * ends here
				     */
				    
					boolean chkSize=form.getSaleslineitemquotationtable().removeChargesOnDelete();
					if(chkSize==false){
						form.getChargesTable().connectToLocal();
					}
					
					try {
						form.prodTaxTable.connectToLocal();
						form.addProdTaxes();
//						double totalIncludingTax=form.getDototalamt().getValue()+form.getProdTaxTable().calculateTotalTaxes();
						/** Date 01-09-2017 above old code commented and new code below line added by vijay for final total amount after discount **/
						double totalIncludingTax=form.getDbFinalTotalAmt().getValue()+form.getProdTaxTable().calculateTotalTaxes();
						
						form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
						form.updateChargesTable();
					} catch (Exception e) {
						e.printStackTrace();
					}
					double netPay=fillNetPayable(form.getDoamtincltax().getValue());
					netPay=Math.round(netPay);
					int netPayable=(int) netPay;
					netPay=netPayable;
					form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
					
					/** Date 01-09-2017 added by vijay for final netpayable amount after discount **/
					form.getDbGrandTotalAmt().setValue(netPay);

					if(!form.getTbroundOffAmt().getValue().equals("") && form.getTbroundOffAmt().getValue()!=null){
						String roundOff = form.getTbroundOffAmt().getValue();
						double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
						if(roundoffAmt!=0){
							form.getDonetpayamt().setValue(roundoffAmt);

						}else{
							form.getDonetpayamt().setValue(roundoffAmt);
							form.getTbroundOffAmt().setValue("");
						}
						
					}
					else{
						form.getDonetpayamt().setValue(netPay);
					}
					if(form.getSaleslineitemquotationtable().getDataprovider().getList().size()==0){
						form.getDbDiscountAmt().setValue(0d);
						form.getTbroundOffAmt().setValue(0d+"");
						form.getDonetpayamt().setValue(0d);
					}
					/** ends here *******/
					
					String approverName="";
					if(form.olbApproverName.getValue()!=null){
						approverName=form.olbApproverName.getValue();
					}
					AppUtility.getApproverListAsPerTotalAmount(form.olbApproverName, "Quotation", approverName, form.getDonetpayamt().getValue());
					form.hideWaitSymbol();
				}
		     };
	    	 timer.schedule(3000);
		}
		
		
		@Override
			public void onClick(ClickEvent event) {
				super.onClick(event);
				
				
				/*************For Pestle Pest Control*********************/
				
				if(event.getSource()==form.additionalDetails){
					System.out.println("Pop Will Fire Here");
					
					panel = new PopupPanel(true);
					
					panel.add(addpopup);
					panel.show();
					panel.center();
					
				}
				
				if(event.getSource()==addpopup.btnOk){
					System.out.println("Here we will save in entity");
					System.out.println(!addpopup.txtchemused.getValue().trim().equals(""));
					System.out.println(!addpopup.txtfreqused.getValue().trim().equals(""));
					System.out.println(!addpopup.txtwarranty.getValue().trim().equals(""));
					if((!addpopup.txtchemused.getValue().trim().equals(""))&&(!addpopup.txtfreqused.getValue().trim().equals(""))&&(!addpopup.txtwarranty.getValue().trim().equals(""))){
						model.setChemicaltouse(addpopup.txtchemused.getText().trim());
						System.out.println("Inside If chem"+addpopup.txtchemused.getText().trim());
						model.setFreqService(addpopup.txtfreqused.getText().trim());
						System.out.println("Inside If txtfreqused"+addpopup.txtfreqused.getText().trim());
						model.setWarrantyPeriod(addpopup.txtwarranty.getText().trim());
						System.out.println("Inside If txtfreqused"+addpopup.txtwarranty.getText().trim());
						panel.hide();
					}else{
						form.showDialogMessage("Please enter All Details");
					}
					System.out.println("Saved in Entity");
				}
				
				if(event.getSource()==addpopup.btnCancel){
					addpopup.txtchemused.setText("");
					addpopup.txtfreqused.setText("");
					addpopup.txtwarranty.setText("");
					panel.hide();
				}
				
				
				
				if(event.getSource()==frmAndToPop.btnOne){
					getTaxesDetails();
					panel.hide();
				}
				
				if(event.getSource()==frmAndToPop.btnTwo){
					panel.hide();
				}
				
				if(event.getSource()==form.btnsecurityDep){
					reactToSecurityDep();
				 }
				
				if(event.getSource()==securitydeposit.getBtnOk()){
					setPopupValueToFields();
				}
				
				if(event.getSource()==securitydeposit.getBtnCancel()){
					panel.hide();
				}	
				
				
				if(event.getSource().equals(conditionPopup.getBtnOne()))
				{
					
					if(cnt >0){
						System.out.println("in side one yes");
						final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"yes";
						Window.open(url, "test", "enabled");
						
						panel.hide();
					}
					
					else if(customQuotation >0)
					{
						System.out.println("customQuotation value"+customQuotation);
						
						if(customQuotation > 0 && model.getGroup().equalsIgnoreCase("Five Years"))
						{
							System.out.println("in side five years ");
							final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+model.getId()+"&"+"group="+"Five Years"+"&"+"preprint="+"yes";
							 Window.open(url, "test", "enabled");
							 
							 panel.hide();
						}
						
						else if(customQuotation > 0 && model.getGroup().equalsIgnoreCase("Direct Quotation"))
						{
							System.out.println("in side Direct Quotation ");
							final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+model.getId()+"&"+"group="+"Direct Quotation"+"&"+"preprint="+"yes";
							 Window.open(url, "test", "enabled");
							 
							 panel.hide();
						}
						
						else if(customQuotation > 0 && model.getGroup().equalsIgnoreCase("Building"))
						{
							System.out.println("in side Building ");
							 final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+model.getId()+"&"+"group="+"Building"+"&"+"preprint="+"yes";
							 Window.open(url, "test", "enabled");
							 
							 panel.hide();
						}
						
						else if(customQuotation > 0 && model.getGroup().equalsIgnoreCase("After Inspection"))
						{
							System.out.println("in side After Inspection ");
							 final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+model.getId()+"&"+"group="+"After Inspection"+"&"+"preprint="+"yes";
							 Window.open(url, "test", "enabled");
							 
							 panel.hide();
						}
						
						
					}
					
					
					else if(pestQuotation >0)
					{
						System.out.println("Pestle Pest quotation");
						final String url = GWT.getModuleBaseURL() + "pdfpestleservice"+"?Id="+model.getId()+"&"+"group="+model.getGroup()+"&"+"preprint="+"yes";//1st PDF
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					
					else if(pestoIndia > 0)
					{
						System.out.println("Pesto india quotation");
						final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"yes"; //+"&"+"group="+model.getGroup();//1st PDF
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					else if(nbhc > 0)
					{
						System.out.println("nbhc quotation");
						final String url = GWT.getModuleBaseURL() + "nbhcServiceQuotation"+"?Id="+model.getId()+"&"+"preprint="+"yes"; 
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					else if(universal > 0)
					{
						System.out.println("universal pest quotation");
						final String url = GWT.getModuleBaseURL() + "universalServiceQuotation"+"?Id="+model.getId()+"&"+"preprint="+"yes"+"&"+"type="+"q"; 
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					
					else if(pestMasters > 0)
					{
						final String url = GWT.getModuleBaseURL() + "customquotationpdf"+"?Id="+model.getId()+"&"+"preprint="+"yes"; 
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					
					else if(quotationFormatVersion1>0){
						final String url = GWT.getModuleBaseURL() + "quotationfmtver1"+"?Id="+model.getId()+"&"+"preprint="+"yes"; 
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					
				}
				
				if(event.getSource().equals(conditionPopup.getBtnTwo()))
				{
					
					if(cnt >0){
						System.out.println("inside two no");
						final String url = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"no";
						Window.open(url, "test", "enabled");
					    
						panel.hide();
					}
					
					else if(customQuotation >0)
					{
						System.out.println("customQuotation value"+customQuotation);
						
						if(customQuotation > 0 && model.getGroup().equalsIgnoreCase("Five Years"))
						{
							System.out.println("in side five years ");
							final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+model.getId()+"&"+"group="+"Five Years"+"&"+"preprint="+"no";
							 Window.open(url, "test", "enabled");
							 
							 panel.hide();
						}
						
						else if(customQuotation > 0 && model.getGroup().equalsIgnoreCase("Direct Quotation"))
						{
							System.out.println("in side Direct Quotation ");
							final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+model.getId()+"&"+"group="+"Direct Quotation"+"&"+"preprint="+"no";
							 Window.open(url, "test", "enabled");
							 
							 panel.hide();
						}
						
						else if(customQuotation > 0 && model.getGroup().equalsIgnoreCase("Building"))
						{
							System.out.println("in side Building ");
							 final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+model.getId()+"&"+"group="+"Building"+"&"+"preprint="+"no";
							 Window.open(url, "test", "enabled");
							 
							 panel.hide();
						}
						
						else if(customQuotation > 0 && model.getGroup().equalsIgnoreCase("After Inspection"))
						{
							System.out.println("in side After Inspection ");
							 final String url = GWT.getModuleBaseURL() + "customQuotation"+"?Id="+model.getId()+"&"+"group="+"After Inspection"+"&"+"preprint="+"no";
							 Window.open(url, "test", "enabled");
							 
							 panel.hide();
						}
					}
					
					else if(pestQuotation >0)
					{
								System.out.println("in side no condition");
								final String url = GWT.getModuleBaseURL() + "pdfpestleservice"+"?Id="+model.getId()+"&"+"group="+model.getGroup()+"&"+"preprint="+"no";//1st PDF
								Window.open(url, "test", "enabled");
								
								 panel.hide();
						
					}
					
					else if(pestoIndia > 0)
					{
						System.out.println("Pesto india quotation");
						final String url = GWT.getModuleBaseURL() + "pdfpestoIndiaservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"no"; //+"&"+"group="+model.getGroup();//1st PDF
						Window.open(url, "test", "enabled");
					
						 panel.hide();
					
					}
					
					else if(nbhc > 0)
					{
						System.out.println("nbhc quotation");
						final String url = GWT.getModuleBaseURL() + "nbhcServiceQuotation"+"?Id="+model.getId()+"&"+"preprint="+"no"; 
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					else if(universal > 0)
					{
						System.out.println("universal pest quotation");
						final String url = GWT.getModuleBaseURL() + "universalServiceQuotation"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"type="+"q"; 
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					else if(pestMasters > 0)
					{
						cnt = 0;
						final String url = GWT.getModuleBaseURL() + "customquotationpdf"+"?Id="+model.getId()+"&"+"preprint="+"no"; 
						Window.open(url, "test", "enabled");
						panel.hide();
					}
					else if(quotationFormatVersion1>0){
						cnt = 0;
						final String url = GWT.getModuleBaseURL() + "quotationfmtver1"+"?Id="+model.getId()+"&"+"preprint="+"no"; 
						Window.open(url, "test", "enabled");
						panel.hide();
					}
				}
				
				
				/************************Service Scheduling On Click Events***********************/
				
				if(event.getSource().equals(form.f_btnservice)){
					if(form.getSaleslineitemquotationtable().getDataprovider().getList().size()==0){
						form.showDialogMessage("Add Atleast One Product");
					}
					else
					{
						reactToService();
						reactforTable();
						serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
					}
				}
				
				if(event.getSource().equals(serviceSchedulePopUp.getBtnOk()))
				{

					int cnt=0;
					int pic=0;
					List<SalesLineItem> salesitemlis=form.saleslineitemquotationtable.getDataprovider().getList();
					List<ServiceSchedule> prodservicelist=this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
					
					for(int k=0;k<salesitemlis.size();k++)
					{
						for(int i=0;i<prodservicelist.size();i++)
						{
							if(calculateEndDate(salesitemlis.get(k).getStartDate(),salesitemlis.get(k).getDuration()).equals(prodservicelist.get(i).getScheduleServiceDate()))
							{
								cnt=cnt+1;
							}
						}
					}
					
					if(cnt>1){
						form.showDialogMessage("Please Schedule services properly");
					}
					else{
						reactOnOk();
					}
				}
				
				if(event.getSource()==serviceSchedulePopUp.getBtnCancel()){
					schedulePanel.hide();
				}	
			
				if(event.getSource()==serviceSchedulePopUp.getP_default()){
					reactfordefaultTable();
					serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
				}
				
				if(event.getSource()==serviceSchedulePopUp.getP_customise()){
					List<ServiceSchedule> prodservicelist=this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
					if(prodservicelist.size()!=0){
						serviceSchedulePopUp.getPopScheduleTable().setEnable(true);
						reactforcustumize();
					}else{
						form.showDialogMessage("Please Select Either Equal or Interval");
						serviceSchedulePopUp.getP_customise().setValue(false);
					}
					
				}
				if(event.getSource()==serviceSchedulePopUp.getBtnReset()){
					reactforreset();
				}
				/******************************************************************/
			
				
				/** added by rohan for communication log
				 */
				if(event.getSource() == CommunicationLogPopUp.getBtnOk()){
					form.showWaitSymbol();
				    List<InteractionType> list = CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
				   
				    ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
				    interactionlist.addAll(list);
				    
				    boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);
				    
				    if(checkNewInteraction==false){
				    	form.showDialogMessage("Please add new interaction details");
				    	form.hideWaitSymbol();
				    }	
				    else{	
				    	/**
				    	 * Date : 08-11-2017 KOMAL
				    	 */
				    	final Date followUpDate = interactionlist.get(interactionlist.size()-1).getInteractionDueDate();
				    	 communicationService.saveCommunicationLog(interactionlist, new AsyncCallback<Void>() {

								@Override
								public void onFailure(Throwable caught) {
									form.showDialogMessage("Unexpected Error");
									form.hideWaitSymbol();
									LoginPresenter.communicationLogPanel.hide();
								}

								@Override
								public void onSuccess(Void result) {
									form.showDialogMessage("Data Save Successfully");
									form.hideWaitSymbol();
									model.setFollowUpDate(followUpDate);
									form.getDbFollowUpDate().setValue(followUpDate);
//									communicationPanel.hide();
									LoginPresenter.communicationLogPanel.hide();
								}
							});
				    }
				   
				}
				if(event.getSource() == CommunicationLogPopUp.getBtnCancel()){
//					communicationPanel.hide();
					LoginPresenter.communicationLogPanel.hide();
				}
				if(event.getSource() == CommunicationLogPopUp.getBtnAdd()){
						form.showWaitSymbol();
						String remark = CommunicationLogPopUp.getRemark().getValue();
						Date dueDate = CommunicationLogPopUp.getDueDate().getValue();
						String interactiongGroup =null;
						if(CommunicationLogPopUp.getOblinteractionGroup().getSelectedIndex()!=0)
							interactiongGroup=CommunicationLogPopUp.getOblinteractionGroup().getValue(CommunicationLogPopUp.getOblinteractionGroup().getSelectedIndex());
						boolean validationFlag = AppUtility.validateCommunicationlog(remark,dueDate);
						if(validationFlag){
							InteractionType communicationLog =  AppUtility.getCommunicationLog(AppConstants.SERVICEMODULE,AppConstants.QUOTATION,model.getCount(),model.getEmployee(),remark,dueDate,model.getCinfo(),null,interactiongGroup, model.getBranch(),"");
							CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().getList().add(communicationLog);
							CommunicationLogPopUp.getRemark().setValue("");
							CommunicationLogPopUp.getDueDate().setValue(null);
							CommunicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
						}
						form.hideWaitSymbol();
				}
				/**ends here
				 */
				
				/**
				 * Date : 20-05-2017 By ANIl
				 */
				
				if(event.getSource().equals(costForm.getBtnAddProd())){
					boolean servFlag=false;
					boolean matFlag=false;
					String msg="";
					if(costForm.getLbServiceName().getSelectedIndex()==0){
						servFlag=true;
						msg=msg+"Please select service name.\n";
					}
					if(costForm.getProdInfoComposite().getValue()==null){
						matFlag=true;
						msg=msg+"Please select product.\n";
					}
					
					if(servFlag||matFlag){
						form.showDialogMessage(msg);
						return;
					}
					
					if(costForm.getMaterialTbl().getDataprovider().getList().size()!=0){
						for(MaterialCostStructure object:costForm.getMaterialTbl().getDataprovider().getList()){
							if(object.getServiceName().equals(costForm.getLbServiceName().getValue(costForm.getLbServiceName().getSelectedIndex()))
									&&object.getProductName().equals(costForm.getProdInfoComposite().getProdNameValue())){
								form.showDialogMessage(costForm.getProdInfoComposite().getProdNameValue()+" already added.");
								return;
							}
						}
					}
					
					ArrayList<String> serviceName = new ArrayList<String>();
					/**
					 * @author Anil ,  Date : 24-02-2020
					 * discussed with Nitin sir and commented
					 */
//					if(form.serviceBomMaterialList !=null 
//						&&LoginPresenter.billofMaterialActive 
//						&& form.bomValidationActive 
//						&&form.serviceBomMaterialList.containsKey(costForm.getLbServiceName().getValue(costForm.getLbServiceName().getSelectedIndex()))){
////						serviceName.add(item.getProductName());
//						form.showDialogMessage("This service as define BOM so not allow to add Material.");
//						return;
//					}
					
					/*if(AppUtility.verifyBillofMaterilProd(item.getPrduct())==null 
							&& LoginPresenter.billofMaterialActive){
						itemSet.add(item.getProductName());
					}else if(! form.serviceBomMaterialList.containsKey(item.getProductName()) 
							&& LoginPresenter.billofMaterialActive){
						itemSet.add(item.getProductName());
					}else if(!LoginPresenter.billofMaterialActive) {
						
					}*/
					
					MaterialCostStructure matObj=new MaterialCostStructure();
					matObj.setServiceName(costForm.getLbServiceName().getValue(costForm.getLbServiceName().getSelectedIndex()));
					matObj.setProductName(costForm.getProdInfoComposite().getProdNameValue());
					matObj.setProductQty(1);
					matObj.setBomActive(false);
					matObj.setProductCost(costForm.getProdInfoComposite().getProdPrice().getValue());
					matObj.setTotalCost(costForm.getProdInfoComposite().getProdPrice().getValue());
					
					costForm.getMaterialTbl().getDataprovider().getList().add(matObj);
					
					
					
				}
				if(event.getSource().equals(costForm.getBtnAddCost())){
					
					boolean costNmFlag=false;
					boolean costFlag=false;
					String msg="";
					if(costForm.getTbOtherCostName().getValue().equals("")){
						costNmFlag=true;
						msg=msg+"Please fill cost name.\n";
					}
					if(costForm.getDbOtherCost().getValue()==null){
						costFlag=true;
						msg=msg+"Please fill amount.\n";
					}
					
					if(costNmFlag||costFlag){
						form.showDialogMessage(msg);
						return;
					}
					
					OtherCost oc=new OtherCost();
					oc.setOtherCostName(costForm.getTbOtherCostName().getValue());
					oc.setTotalCost(costForm.getDbOtherCost().getValue());
					
					costForm.getOtherCostTbl().getDataprovider().getList().add(oc);
				}
				if(event.getSource().equals(costForm.getBtnEmpLvl())){
					/**
					 * nidhi 9-01-2019 |*| for nbhc employee costing
					 */
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", "CostingForEmployeeBase")){
						
						if(costForm.getLbServiceName().getSelectedIndex()==0){
							form.showDialogMessage("Please select service name.");
							return;
						}
						
						if(costForm.getOlbEmployee().getSelectedIndex()==0){
							form.showDialogMessage("Please select employee!");
							return;
						}
						Employee employeeLevel=costForm.getOlbEmployee().getSelectedItem();
						if(costForm.getManPowTbl().getDataprovider().getList().size()!=0){
							for(ManPowerCostStructure object:costForm.getManPowTbl().getDataprovider().getList()){
								if(object.getServiceName().equals(costForm.getLbServiceName().getValue(costForm.getLbServiceName().getSelectedIndex()))
										&&object.getEmployeeLevel().equals(costForm.getOlbEmployee().getValue())){
									form.showDialogMessage(costForm.getOlbEmployee().getValue()+" already added.");
									return;
								}
							}
						}
						
						if(employeeLevel.getMonthlyWorkingHrs()==0 || employeeLevel.getSalaryAmt()==0){
							form.showDialogMessage(costForm.getOlbEmployee().getValue()+" Please add working hours or salary amount properly.");
							return ;
						}
						
						ManPowerCostStructure manPowCostObj=new ManPowerCostStructure();
//						manPowCostObj.setServiceId(Integer.parseInt(pic.getProdID().getValue()));
						manPowCostObj.setServiceName(costForm.getLbServiceName().getValue(costForm.getLbServiceName().getSelectedIndex()));
//						manPowCostObj.setServiceCode(pic.getProdCodeValue());
						manPowCostObj.setEmployeeLevel(employeeLevel.getFirstName());
						manPowCostObj.setNoOfOprator(1);
						manPowCostObj.setManCostPerHour((employeeLevel.getSalaryAmt()/employeeLevel.getMonthlyWorkingHrs()));
						manPowCostObj.setTotalCost((employeeLevel.getSalaryAmt()/employeeLevel.getMonthlyWorkingHrs()));
						costForm.getManPowTbl().getDataprovider().getList().add(manPowCostObj);
						
						
						
					}else{
						if(costForm.getLbServiceName().getSelectedIndex()==0){
							form.showDialogMessage("Please select service name.");
							return;
						}
						
						if(costForm.getOlbEmployeeLevel().getSelectedIndex()==0){
							form.showDialogMessage("Please select employee level!");
							return;
						}
						EmployeeLevel employeeLevel=costForm.getOlbEmployeeLevel().getSelectedItem();
						if(costForm.getManPowTbl().getDataprovider().getList().size()!=0){
							for(ManPowerCostStructure object:costForm.getManPowTbl().getDataprovider().getList()){
								if(object.getServiceName().equals(costForm.getLbServiceName().getValue(costForm.getLbServiceName().getSelectedIndex()))
										&&object.getEmployeeLevel().equals(costForm.getOlbEmployeeLevel().getValue())){
									form.showDialogMessage(costForm.getOlbEmployeeLevel().getValue()+" already added.");
									return;
								}
							}
						}
						
						ManPowerCostStructure manPowCostObj=new ManPowerCostStructure();
//						manPowCostObj.setServiceId(Integer.parseInt(pic.getProdID().getValue()));
						manPowCostObj.setServiceName(costForm.getLbServiceName().getValue(costForm.getLbServiceName().getSelectedIndex()));
//						manPowCostObj.setServiceCode(pic.getProdCodeValue());
						manPowCostObj.setEmployeeLevel(employeeLevel.getEmpLvlName());
						manPowCostObj.setNoOfOprator(1);
						manPowCostObj.setManCostPerHour(employeeLevel.getAvgHourlyRate());
						manPowCostObj.setTotalCost(employeeLevel.getAvgHourlyRate());
						costForm.getManPowTbl().getDataprovider().getList().add(manPowCostObj);
						
						
					}
					
				}
				
				if(event.getSource().equals(costForm.getBtnOk())){
					form.matCostList=new ArrayList<MaterialCostStructure>(costForm.getMaterialTbl().getDataprovider().getList());
					form.manPowCostList=new ArrayList<ManPowerCostStructure>(costForm.getManPowTbl().getDataprovider().getList());
					form.otherCostList=new ArrayList<OtherCost>(costForm.getOtherCostTbl().getDataprovider().getList());
					
					System.out.println("MAT SIZE : "+form.matCostList.size()+" MAN SIZE : "+form.manPowCostList.size()+" OTH SIZE : "+form.otherCostList.size());
					panel.hide();
					
				}
				if(event.getSource().equals(costForm.getBtnCancel())){
					panel.hide();
				}
				
				/**
				 * End
				 */	
				
				
				/**
				 * Date: 19-05-2017 By ANIL
				 */
				if(event.getSource().equals(form.btnCosting)){
					
					ScreeenState scr=AppMemory.getAppMemory().currentState;
					if(scr.equals(ScreeenState.VIEW)){
						costForm.getMaterialTbl().setEnable(false);
						costForm.getManPowTbl().setEnable(false);
						costForm.getOtherCostTbl().setEnable(false);
//						costForm.getBtnOk().setEnabled(false);
						costForm.setEnable(false);
					}else{
						costForm.setEnable(true);
					}
					
					if(form.saleslineitemquotationtable.getDataprovider().getList().size()==0){
						form.showDialogMessage("Please add product first.");
						return;
					}
//					costForm.getDbTotalRevenue().setValue(form.donetpayamt.getValue());
					
					/**
					 * Date 16-01-2018 By vijay
					 * above one line old code commented and added in else block
					 * in if condition added code without taxes amount because costing must be based on without taxes amt
					 */
					if(form.dbFinalTotalAmt.getValue()!=null){
						costForm.getDbTotalRevenue().setValue(form.dbFinalTotalAmt.getValue());
					}else{
						costForm.getDbTotalRevenue().setValue(form.donetpayamt.getValue());
					}
					/**
					 * ends here
					 */
					
					
					
					HashSet<String> itemSet=new HashSet<String>();
					
					for(SalesLineItem item:form.saleslineitemquotationtable.getDataprovider().getList()){
						/**
						 *  nidhi |*|
						 *  7-01-2018
						 *  for add only service product name which contain bom and process configration active
						 */
					/*	if(AppUtility.verifyBillofMaterilProd(item.getPrduct())==null && LoginPresenter.billofMaterialActive){
							itemSet.add(item.getProductName());
						}else if(!LoginPresenter.billofMaterialActive) {
							itemSet.add(item.getProductName());
						}*/
						/**
						 * end
						 */
						itemSet.add(item.getProductName());
					}
					ArrayList<String> itemList=new ArrayList<String>(itemSet);
					costForm.initializeServiceListBox(itemList);
					
					costForm.getMaterialTbl().connectToLocal();
					costForm.getMaterialTbl().getDataprovider().getList().addAll(form.matCostList);
					
					costForm.getManPowTbl().connectToLocal();
					costForm.getManPowTbl().getDataprovider().getList().addAll(form.manPowCostList);
					
					costForm.getOtherCostTbl().connectToLocal();
					costForm.getOtherCostTbl().getDataprovider().getList().addAll(form.otherCostList);
					
					/**
					 * nidhi |*|
					 */
//					if(LoginPresenter.billofMaterialActive){
						manageCostingListOnServiceChanges();
//					}
					
					panel=new PopupPanel(true);
					costForm.content.getElement().getStyle().setHeight(570, Unit.PX);
					costForm.content.getElement().getStyle().setWidth(950, Unit.PX);
//					costForm.content.getElement().setId("login-form");
					panel.add(costForm.content);
					panel.show();
					panel.center();
				}
				/**
				 * End
				 */
				
				/**
				 * Date : 06-11-2017 BY ANIL
				 */
				if (event.getSource() == cancelPopup.getBtnOk()) {
					cancelpanel.hide();
					cancelDocument(AppConstants.CANCELLED);
				}
				if (event.getSource() == cancelPopup.getBtnCancel()) {
					cancelpanel.hide();
					cancelPopup.remark.setValue("");
				}
				
				if (event.getSource() == unSucessfullPopup.getBtnOk()) {
					cancelpanel.hide();
					cancelDocument(AppConstants.Unsucessful);
				}
		
				if (event.getSource() == unSucessfullPopup.getBtnCancel()) {
					cancelpanel.hide();
					cancelPopup.remark.setValue("");
				}
				/**
				 * End
				 */
				

				/**
				 * Date 14/12/2017
				 * By Jayshree
				 * ONLY for NBHC quotation pdf 
				 */
				
				if(event.getSource()==loiConditionPopup.getBtnOne()){
					
					String prePrint = (loiConditionPopup.getCbPrePrint().getValue() == true ? "yes" : "no");
					System.out.println("Inside one button"+loiConditionPopup.getBtnOne());
					reactOnPrintNBHCLOIPdf("yes",prePrint);
					panelLOI.hide();
				}
				if(event.getSource()==loiConditionPopup.getBtnTwo()){
					System.out.println("Inside second button"+loiConditionPopup.getBtnTwo());
					
					String prePrint = (loiConditionPopup.getCbPrePrint().getValue() == true ? "yes" : "no");
					reactOnPrintNBHCLOIPdf("no",prePrint);
					panelLOI.hide();
					
				}
				
				//End
				
				if(event.getSource()==lblUpdate){
					customerAddressEdiatable();
				}
				
				if(event.getSource()==custAddresspopup.getLblOk()){
					saveCustomerAddress();
				}
				if(event.getSource()==custAddresspopup.getLblCancel()){
					custAddresspopup.hidePopUp();
					cancelpanel.hide();
				}
				if(event.getSource()==custAddresspopup.getBtnbillingaddr()){
					custAddresspopup.getServiceAddressComposite().setValue(custAddresspopup.getBillingAddressComposite().getValue());
				}
				
			}
		
		private void cancelDocument(final String status) {
			model.setStatus(status);
			model.setDescription(model.getDescription()+"\n"+"Quotation ID ="+model.getCount()+" "+"Quotation status ="+form.getTbQuotationStatus().getValue().trim()+"\n"
					+"has been "+status+"by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelPopup.getRemark().getValue().trim()
					+" "+" Date ="+AppUtility.parseDate(new Date()));
			/**
			 * nidhi
			 * 2227 for store reason for unsucessful 
			 */
			if(unSucessfullPopup.getReason().getSelectedIndex()!=0)
				model.setDocStatusReason(unSucessfullPopup.getReason().getValue(unSucessfullPopup.getReason().getSelectedIndex()));
			/**
			 * end
			 */
			form.showWaitSymbol();
			service.save(model, new AsyncCallback<ReturnFromServer>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ReturnFromServer result) {
					form.hideWaitSymbol();
					form.getTbQuotationStatus().setValue(status);
					form.getTaDescription().setValue(model.getDescription());
					form.setMenuAsPerStatus();
					/*
					 * nidhi
					 * 1717
					 */
					int index = unSucessfullPopup.getReason().getSelectedIndex(); 
					String status = (index != 0 ? unSucessfullPopup.getReason().getValue(unSucessfullPopup.getReason().getSelectedIndex()) : "");
					form.getTbDocStatusreason().setValue(status);
					/**
					 * end
					 */
					form.showDialogMessage("Document updated sucessfully!");
				}
			});
		}
		private void reactToSecurityDep(){
			System.out.println("Inside Click ......................");
			System.out.println(form.tbQuotationStatus);
			if(form.tbQuotatinId.getValue()==null){
				securitydeposit.getP_docid().setValue("");
			}else{
			securitydeposit.getP_docid().setValue(form.tbQuotatinId.getValue()+"");
			}
			
			securitydeposit.getP_docstatus().setValue(form.tbQuotationStatus.getValue());
			
			panel=new PopupPanel(true);
			
			ScreeenState TransactionStatus=AppMemory.getAppMemory().currentState;
			System.out.println("Status Mode"+TransactionStatus.toString());
			if(TransactionStatus.toString().equals("VIEW")){
				System.out.println("IN if loop.....................................................................");
				securitydeposit.getP_securityDeposite().setEnabled(false);
				securitydeposit.getP_depstatus().setEnabled(false);
				securitydeposit.getP_reDate().setEnabled(false);
				securitydeposit.getP_bankName().setEnabled(false);
				securitydeposit.getP_branch().setEnabled(false);
				securitydeposit.getP_paybleAt().setEnabled(false);
				securitydeposit.getP_favouring().setEnabled(false);
//				securitydeposit.getP_checkName().setEnabled(false);
				securitydeposit.getP_checknumber().setEnabled(false);
				securitydeposit.getP_chekDate().setEnabled(false);
				securitydeposit.getP_servisNo().setEnabled(false);
				securitydeposit.getP_address().setEnable(false);
				securitydeposit.getP_accountNo().setEnabled(false);
				securitydeposit.getP_ifscode().setEnabled(false);
				securitydeposit.getP_micrCode().setEnabled(false);
				securitydeposit.getP_swiftcode().setEnabled(false);
				securitydeposit.getP_tainstruct().setEnabled(false);
				securitydeposit.getP_paymethod().setEnabled(false);
			}
			
			if(TransactionStatus.toString().equals("EDIT")){
				System.out.println("IN if loop.....................................................................");
				securitydeposit.getP_securityDeposite().setEnabled(true);
				securitydeposit.getP_depstatus().setEnabled(false);
				securitydeposit.getP_reDate().setEnabled(false);
				securitydeposit.getP_bankName().setEnabled(true);
				securitydeposit.getP_branch().setEnabled(true);
				securitydeposit.getP_paybleAt().setEnabled(true);
				securitydeposit.getP_favouring().setEnabled(true);
//				securitydeposit.getP_checkName().setEnabled(true);
				securitydeposit.getP_checknumber().setEnabled(true);
				securitydeposit.getP_chekDate().setEnabled(true);
				 securitydeposit.getP_servisNo().setEnabled(true);
				 securitydeposit.getP_address().setEnable(true);
				 securitydeposit.getP_accountNo().setEnabled(true);
				 securitydeposit.getP_ifscode().setEnabled(true);
				 securitydeposit.getP_micrCode().setEnabled(true);
				 securitydeposit.getP_swiftcode().setEnabled(true);
				 securitydeposit.getP_tainstruct().setEnabled(true);
				 securitydeposit.getP_paymethod().setEnabled(true);
			}
			
			
			recallfeildonpupinedit();
			panel.add(securitydeposit);
			panel.show();
			panel.center();
			System.out.println(TransactionStatus);
			
			
		}

		private void recallfeildonpupinedit()
		{

			
			securitydeposit.getP_bankName().setValue(form.f_bankName);
			securitydeposit.getP_branch().setValue(form.f_branch);
			securitydeposit.getP_securityDeposite().setValue(form.f_securityDeposit);
			securitydeposit.getP_reDate().setValue(form.f_reDate);
			securitydeposit.getP_depstatus().setValue(form.f_depositStatus);
			
			if(form.f_payMethod!=null){
			securitydeposit.getP_paymethod().setValue(form.f_payMethod);
			}
			
			securitydeposit.getP_accountNo().setValue(form.f_accountNo);
			securitydeposit.getP_paybleAt().setValue(form.f_paybleAt);
			securitydeposit.getP_favouring().setValue(form.f_fouring);
//			securitydeposit.getP_checkName().setValue(form.f_checkName);
			
			securitydeposit.getP_checknumber().setValue(form.f_checkNo+"");
			securitydeposit.getP_chekDate().setValue(form.f_checkDate);
			
			securitydeposit.getP_servisNo().setValue(form.f_serviceNo);
			securitydeposit.getP_ifscode().setValue(form.f_ifscCode);
			securitydeposit.getP_micrCode().setValue(form.f_micrCode);
			securitydeposit.getP_swiftcode().setValue(form.f_swiftCode);
			
			if(form.f_address!=null){
			securitydeposit.getP_address().setValue(form.f_address);
			}
			securitydeposit.getP_tainstruct().setValue(form.f_instruction);
			
		}

		private void setPopupValueToFields() {

			if(securitydeposit.getP_securityDeposite().getValue()!=null){
			form.setF_securityDeposit(securitydeposit.getP_securityDeposite().getValue());
		}else{
			double i=0;
			form.setF_securityDeposit(i);
		}
		if(securitydeposit.getP_reDate().getValue()!=null){
			form.setF_reDate(securitydeposit.getP_reDate().getValue());
		}
		if(securitydeposit.getP_depstatus().getValue()!=false){
			form.setF_depositStatus(securitydeposit.getP_depstatus().getValue());
		}else{
			form.setF_depositStatus(false);
		}
		if(securitydeposit.getP_paymethod().getSelectedIndex()!=0){
			form.setF_payMethod(securitydeposit.getP_paymethod().getValue());
		}else{
			form.setF_payMethod(null);
		}
		
		
		if(securitydeposit.getP_accountNo().getValue()!=null){
			form.setF_accountNo(securitydeposit.getP_accountNo().getValue());
		}
		
		if(securitydeposit.getP_bankName().getValue()!=null){
			
			form.setF_bankName(securitydeposit.getP_bankName().getValue());
		}
		if(securitydeposit.getP_branch().getValue()!=null){
			form.setF_branch(securitydeposit.getP_branch().getValue());
		}
		if(securitydeposit.getP_paybleAt().getValue()!=null){
			form.setF_paybleAt(securitydeposit.getP_paybleAt().getValue());
		}
		if(securitydeposit.getP_favouring().getValue()!=null){
			form.setF_fouring(securitydeposit.getP_favouring().getValue());
		}
//		if(securitydeposit.getP_checkName().getValue()!=null){
//			form.setF_checkName(securitydeposit.getP_checkName().getValue());
//		}
		
			
		if(securitydeposit.getP_checknumber().getValue()==null){		
				form.setF_checkNo(0);
			}
		
			
			if(!securitydeposit.getP_checknumber().getValue().equals("")){
			form.setF_checkNo(Integer.parseInt(securitydeposit.getP_checknumber().getValue()));
		}
		if(securitydeposit.getP_chekDate().getValue()!=null){
			form.setF_checkDate(securitydeposit.getP_chekDate().getValue());
		}
			
		if(securitydeposit.getP_servisNo().getValue()==null){
				form.setF_serviceNo("");
			}
			if(securitydeposit.getP_servisNo().getValue()!=null){
			
			form.setF_serviceNo(securitydeposit.getP_servisNo().getValue());
		}
		
		if(securitydeposit.getP_address().getValue()!=null){
			form.setF_address(securitydeposit.getP_address().getValue());
		}
		if(securitydeposit.getP_ifscode().getValue()!=null){
			form.setF_ifscCode(securitydeposit.getP_ifscode().getValue());
		}
		if(securitydeposit.getP_micrCode().getValue()!=null){
			form.setF_micrCode(securitydeposit.getP_micrCode().getValue());
		}
		if(securitydeposit.getP_swiftcode().getValue()!=null){
			form.setF_swiftCode(securitydeposit.getP_swiftcode().getValue());
		}
		if(securitydeposit.getP_tainstruct().getValue()!=null){
			form.setF_instruction(securitydeposit.getP_tainstruct().getValue());
		}
		panel.hide();
			
		}

		@Override
		public void onSelection(SelectionEvent<Suggestion> event) {
			if(event.getSource().equals(form.getPersonInfoComposite().getId())||event.getSource().equals(form.getPersonInfoComposite().getName())||event.getSource().equals(form.getPersonInfoComposite().getPhone())){
				form.checkCustomerStatus(form.getPersonInfoComposite().getIdValue());
				custcount=form.getPersonInfoComposite().getIdValue();
			}
		}

		@Override
		public void onValueChange(ValueChangeEvent<Integer> event) {
			if(event.getSource()==serviceSchedulePopUp.getP_interval()){
				valueChangeMethodLogicForScheduling();
			}
			serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
		
		}
		
		public boolean isAdvanceFilterSelected(){
			/*if(serviceSchedulePopUp.getLbServices().getSelectedIndex()!=0
					||serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()!=0
					||serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()!=0){
				return true;
			}*/
			return false;
		}
		public void sortSechduleServicesByProdSerNoAndSerSrNo(List<ServiceSchedule> list){
			Comparator<ServiceSchedule> prodSerNoComp = new Comparator<ServiceSchedule>() {  
				@Override  
				public int compare(ServiceSchedule o1, ServiceSchedule o2) {  
					if (o1.getSerSrNo() == o2.getSerSrNo()) {
						return 0;
					}
					if (o1.getSerSrNo() > o2.getSerSrNo()) {
						return 1;
					} else {
						return -1;
					}
				}  
			};
			Comparator<ServiceSchedule> serSerNoComp = new Comparator<ServiceSchedule>() {  
				@Override  
				public int compare(ServiceSchedule o1, ServiceSchedule o2) {  
					if (o1.getScheduleServiceNo() == o2.getScheduleServiceNo()) {
						return 0;
					}
					if (o1.getScheduleServiceNo() > o2.getScheduleServiceNo()) {
						return 1;
					} else {
						return -1;
					}
				}  
			};
			Collections.sort(list,serSerNoComp);
			Collections.sort(list,prodSerNoComp);
			
//			for(ServiceSchedule ss:list){
//				System.out.println("Product Sr No. - "+ss.getSerSrNo()+" Service Sr No. - "+ss.getScheduleServiceNo());
//			}
		}
		
public List<ServiceSchedule> getUpdatedScheduledServices(List<ServiceSchedule> list,String subFilterType,int dayIndex,int weekIndex, int excludeDay){
			
			sortSechduleServicesByProdSerNoAndSerSrNo(list);
			Date temp =null;
			boolean flag =false;
			boolean sameMonthserviceflag = false;
			boolean serviceGreaterFlag = false;
			
//			HashSet<UniqueProduct> productsHs =new HashSet<ContractPresenter.UniqueProduct>();
			HashSet<Integer> productsHs =new HashSet<Integer>();
//			HashMap<Integer,Integer> productsMap=new HashMap<Integer,Integer>();
			for(ServiceSchedule serSch:list){
//				UniqueProduct obj=new UniqueProduct();
//				obj.setProductId(serSch.getScheduleProdId());
//				obj.setSrNo(serSch.getSerSrNo());
//				productsHs.add(obj);
				productsHs.add(serSch.getScheduleProdId());
				
				
			}
			System.out.println("HASH SET SIZE : "+productsHs.size());
//			ArrayList<UniqueProduct> productList =new ArrayList<ContractPresenter.UniqueProduct>(productsHs);
			ArrayList<Integer> productList =new ArrayList<Integer>(productsHs);
			System.out.println("UNIQUE PROD LIST  SIZE : "+productList.size());
			
			ArrayList<SalesLineItem> itemList=new ArrayList<SalesLineItem>();
			for(Integer obj:productList){
				for(SalesLineItem item:form.getSaleslineitemquotationtable().getDataprovider().getList()){
					if(obj==item.getPrduct().getCount()){
						itemList.add(item);
					}
				}
			}
			System.out.println("ITEM LIST SIZE : "+itemList.size());
			
			for(SalesLineItem item:itemList){
				/**
				 * Filtering data through week 
				 */
				if(subFilterType.equals("Week")){
					
					Date Stardaate = null;
					Stardaate = item.getStartDate();
					long noServices = (long) (item.getNumberOfServices());
					int noOfdays = item.getDuration();
					int interval = (int) (noOfdays / item.getNumberOfServices());
					Date servicedate = item.getStartDate();
//					Date d = new Date(servicedate.getTime());
					int months = noOfdays/30;
					
					if(noServices>months){
						serviceGreaterFlag = true;
						form.showDialogMessage("Services greater than availabe weeks can not schedule services with weeks");
						serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
						return null;
					}
					
					Date productEndDate = new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate = new Date(productEndDate.getTime());
//					productEndDate = prodenddate;
					/**
					 * nidhi
					 *  ))..
					 *  for exclude day.
					 */
					productEndDate = calculateEndDate(item.getStartDate(),item.getDuration()-1);
					int startday = servicedate.getDay();
					
					if(excludeDay !=0  && startday == excludeDay-1){
						CalendarUtil.addDaysToDate(servicedate, 1);
					}
					Date d = new Date(servicedate.getTime());
					
					
					if(excludeDay !=0 ){
						CalendarUtil.addDaysToDate(productEndDate, -1);
					}
					int lastday = productEndDate.getDay();
					if(lastday == excludeDay -1){
						CalendarUtil.addDaysToDate(productEndDate, -1);
					}
					/**
					 * end
					 */
					
				
					for(ServiceSchedule schSer:list){
						
						if(item.getPrduct().getCount()==schSer.getScheduleProdId()
								&&item.getProductSrNo()==schSer.getSerSrNo()){
							
						Date day = new Date(d.getTime());
						if(temp!=null){
							String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
							String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
							if(currentServiceMonth.equals(lastserviceDateMonth)){
								sameMonthserviceflag =true;
							}
						}
						if(sameMonthserviceflag){
							CalendarUtil.addDaysToDate(day, 30);
						}
						
						CalendarUtil.setToFirstDayOfMonth(day);
						if (weekIndex == 1) {
							day = AppUtility.getCalculatedDate(day,dayIndex);
							if (day.before(Stardaate)) {
								if (schSer.getScheduleServiceNo() == 1) {
									day = Stardaate;
								} else {
									flag = true;
								}
							}
							if (flag || schSer.getScheduleServiceNo() > 1) {
								day = AppUtility.getCalculatedDate(day,dayIndex);
							}
						}else if (weekIndex == 2) {
							CalendarUtil.addDaysToDate(day, 7);
							day = AppUtility.getCalculatedDate(day,dayIndex);
							if (day.before(Stardaate)) {
								if (schSer.getScheduleServiceNo() == 1) {
									day = Stardaate;
								} else {
									flag = true;
								}
							}
							if (flag || schSer.getScheduleServiceNo() > 1) {
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 7);
								day = AppUtility.getCalculatedDate(day,dayIndex);
							}
						}else if (weekIndex == 3) {
							CalendarUtil.addDaysToDate(day, 14);
							day = AppUtility.getCalculatedDate(day,dayIndex);
							if (day.before(Stardaate)) {
								if (schSer.getScheduleServiceNo() == 1) {
									day = Stardaate;
								} else {
									flag = true;
								}
							}
							if (flag || schSer.getScheduleServiceNo() > 1) {
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 14);
								day = AppUtility.getCalculatedDate(day,dayIndex);
							}
						}else if (weekIndex == 4) {
							CalendarUtil.addDaysToDate(day, 21);
							day = AppUtility.getCalculatedDate(day,dayIndex);
							if (day.before(Stardaate)) {
								if (schSer.getScheduleServiceNo() == 1) {
									day = Stardaate;
								} else {
									flag = true;
								}
							}
							if (flag || schSer.getScheduleServiceNo() > 1) {
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 21);
								day = AppUtility.getCalculatedDate(day,dayIndex);
							}
						}
						
						
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						int serDay = day.getDay();
						if(excludeDay !=0  && serDay == excludeDay-1){
							CalendarUtil.addDaysToDate(day, 1);
						
						}
						/**
						 * end
						 */
						
						temp = day;
						
						
						if (productEndDate.before(day) == false) {
							schSer.setScheduleServiceDate(day);
						} else {
							schSer.setScheduleServiceDate(productEndDate);
							
						}
						CalendarUtil.addDaysToDate(d, interval);
						int days =0;
						String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
						if(serviceStartDate.equals("1")){
							String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
							days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
							CalendarUtil.addDaysToDate(d, days);
						}
						Date tempdate = new Date(d.getTime());
						d = tempdate;
						
						schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
						int weeknumber = AppUtility.getweeknumber(schSer.getScheduleServiceDate());
						schSer.setWeekNo(weeknumber);
//						schSer.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable()
//								.getDataprovider().getList().get(i).getServiceProductList())
								 /***
								  * nidhi *:*:*
								  * 18-09-2018
								  * for map bill product
								  * if any update please update in else conditions also
								  */
								 if(LoginPresenter.billofMaterialActive && form.bomValidationActive && schSer.getServiceProductList()!=null){
									 schSer.setServiceProductList(schSer.getServiceProductList());
								 }
								 /**
								  * end
								  */
					}
						
				}
			}
			/**
			 * Filtering through Day
			 */
			else if(subFilterType.equals("Day")){
				long noServices = (long) (item.getNumberOfServices());
				int noOfdays = item.getDuration();
				double nodays =  noOfdays;
				double noofservices= item.getNumberOfServices();
				double interval =  nodays / noofservices;
				double tempinterval = interval;
				double tempvalue=interval;
				Date servicedate = item.getStartDate();
//				Date d = new Date(servicedate.getTime());
				Date productEndDate = new Date(servicedate.getTime());
				CalendarUtil.addDaysToDate(productEndDate, noOfdays);
				Date prodenddate = new Date(productEndDate.getTime());
				
//				productEndDate = prodenddate;
				/**
				 * nidhi
				 *  ))..
				 *  for exclude day.
				 */
				productEndDate = calculateEndDate(item.getStartDate(),item.getDuration()-1);
				int startday = servicedate.getDay();
				
				if(excludeDay !=0  && startday == excludeDay-1){
					CalendarUtil.addDaysToDate(servicedate, 1);
				}
				Date d = new Date(servicedate.getTime());
				
				
				if(excludeDay !=0 ){
					CalendarUtil.addDaysToDate(productEndDate, -1);
				}
				int lastday = productEndDate.getDay();
				if(lastday == excludeDay -1){
					CalendarUtil.addDaysToDate(productEndDate, -1);
				}
				/**
				 * end
				 */
				
				
				
				for(ServiceSchedule schSer:list){
					
					if(item.getPrduct().getCount()==schSer.getScheduleProdId()
							&&item.getProductSrNo()==schSer.getSerSrNo()){
					
					Date Stardaate = null;
					Stardaate = item.getStartDate();
					
					String dayOfWeek1 = format.format(d);
					int adate = Integer.parseInt(dayOfWeek1);
					int adday1 = dayIndex - 1;
					
					int adday = 0;
					if (adate <= adday1) {
						adday = (adday1 - adate);
					} else {
						adday = (7 - adate + adday1);
					}
					Date newDate = new Date(d.getTime());
					CalendarUtil.addDaysToDate(newDate, adday);

					Date day = new Date(newDate.getTime());
					
					if(weekIndex!=0){
						if(temp!=null){
							String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
							String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
							if(currentServiceMonth.equals(lastserviceDateMonth)){
								sameMonthserviceflag =true;
							}else{
								sameMonthserviceflag= false;
							}
						}
						if(sameMonthserviceflag){
							CalendarUtil.addDaysToDate(day, 10);
						}
												
						CalendarUtil.setToFirstDayOfMonth(day);
						if(weekIndex==1){
							day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
							if(day.before(Stardaate)){
								if(schSer.getScheduleServiceNo() == 1){
									day =Stardaate;
								}else{
									flag = true;
								}
							}
							if(flag || schSer.getScheduleServiceNo() >1){
								CalendarUtil.setToFirstDayOfMonth(day);
								day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
							}
						}else if(weekIndex==2){
							
							CalendarUtil.addDaysToDate(day, 7);

							 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
							if(day.before(Stardaate)){
								if(schSer.getScheduleServiceNo()==1){
									day =Stardaate;
								}
								else{
									flag = true;
								}
							}
							if(flag || schSer.getScheduleServiceNo()>1){
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 7);
								 day = AppUtility.getCalculatedDatewithWeek(day, adday1);;
							}
						}else if(weekIndex==3){
							CalendarUtil.addDaysToDate(day, 14);
							 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
							if(day.before(Stardaate)){
								if(schSer.getScheduleServiceNo()==1){
									day =Stardaate;
								}
								else{
									flag = true;
								}
							}
							if(flag || schSer.getScheduleServiceNo()>1){
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 14);
								 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
							}
						}else if(weekIndex==4){
							CalendarUtil.addDaysToDate(day, 21);

							 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
							if(day.before(Stardaate)){
								if(schSer.getScheduleServiceNo()==1){
									day =Stardaate;
								}
								else{
									flag = true;
								}
							}
							if(flag || schSer.getScheduleServiceNo()>1){
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 21);
								 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
							}
					     }
						
						 temp = day;
						
						CalendarUtil.addDaysToDate(d, (int)interval);
						int days =0;
						String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
						if(serviceStartDate.equals("1")){
							String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
							 days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
							CalendarUtil.addDaysToDate(d, days);

						}
					}
					if (productEndDate.before(day) == false) {
						schSer.setScheduleServiceDate(day);
					} else {
						schSer.setScheduleServiceDate(productEndDate);
					}
					int interval2 =(int) Math.round(interval);
					if(weekIndex==0){
						CalendarUtil.addDaysToDate(d, interval2);
					}
					Date tempdate = new Date(d.getTime());
					d = tempdate;
					schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
					
					interval =(interval+tempinterval)-tempvalue;
				    tempvalue = Math.round(interval);
				   
					int weeknumber = AppUtility.getweeknumber(schSer.getScheduleServiceDate());
					schSer.setWeekNo(weeknumber);
//					schSer.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable()
//							.getDataprovider().getList().get(i).getServiceProductList())
					 /***
					  * nidhi *:*:*
					  * 18-09-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive && form.bomValidationActive && schSer.getServiceProductList()!=null ){
						 schSer.setServiceProductList(schSer.getServiceProductList());
					 }
					 /**
					  * end
					  */
				}
				}
			}
			/**
			 * Filtering through Time
			 */
			else if(subFilterType.equals("Time")){
				for(ServiceSchedule schSer:list){
					String serviceHrs = serviceSchedulePopUp.getP_servicehours().getItemText(serviceSchedulePopUp.getP_servicehours().getSelectedIndex());
					String serviceMin = serviceSchedulePopUp.getP_servicemin().getItemText(serviceSchedulePopUp.getP_servicemin().getSelectedIndex());
					String serviceAmPm = serviceSchedulePopUp.getP_ampm().getItemText(serviceSchedulePopUp.getP_ampm().getSelectedIndex());
					String calculatedServiceTime = serviceHrs + ":" + serviceMin + ""+ serviceAmPm;
					schSer.setScheduleServiceTime(calculatedServiceTime);
				}
			}
			/**
			 * Filtering through Interval
			 */
			else if(subFilterType.equals("Interval")){
				serviceSchedulePopUp.getP_customise().setValue(false);
				serviceSchedulePopUp.getP_default().setValue(false);
				serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
				serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
				serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
				serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
				serviceSchedulePopUp.getP_servicehours().setEnabled(true);
				serviceSchedulePopUp.getP_servicemin().setEnabled(true);
				serviceSchedulePopUp.getP_ampm().setEnabled(true);
				serviceSchedulePopUp.getP_customise().setEnabled(true);
				serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
				
				long noServices = (long) (item.getNumberOfServices());
				int inreval = serviceSchedulePopUp.getP_interval().getValue();
				int noOfdays = item.getDuration();
				Date servicedate = new Date(item.getStartDate().getTime());
//				Date d = new Date(servicedate.getTime());

				Date productEndDate = new Date(servicedate.getTime());
				CalendarUtil.addDaysToDate(productEndDate, noOfdays);
				Date prodenddate = new Date(productEndDate.getTime());
				productEndDate = prodenddate;
				Date tempdate = new Date();
				
				/**
				 * nidhi
				 *  ))..
				 *  for exclude day.
				 */
				productEndDate = calculateEndDate(item.getStartDate(),item.getDuration()-1);
				int startday = servicedate.getDay();
				
				if(excludeDay !=0  && startday == excludeDay-1){
					CalendarUtil.addDaysToDate(servicedate, 1);
				}
				Date d = new Date(servicedate.getTime());
				
				
				if(excludeDay !=0 ){
					CalendarUtil.addDaysToDate(productEndDate, -1);
				}
				int lastday = productEndDate.getDay();
				if(lastday == excludeDay -1){
					CalendarUtil.addDaysToDate(productEndDate, -1);
				}
				/**
				 * end
				 */
				
				
				for(ServiceSchedule schSer:list){
					
					if(item.getPrduct().getCount()==schSer.getScheduleProdId()
							&&item.getProductSrNo()==schSer.getSerSrNo()){
					if (schSer.getScheduleServiceNo() > 1) {
						CalendarUtil.addDaysToDate(d, inreval);
						
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						int serDay = d.getDay();
						if(excludeDay !=0  && serDay == excludeDay-1){
							CalendarUtil.addDaysToDate(d, 1);
						
						}
						/**
						 * end
						 */
						
						tempdate = new Date(d.getTime());
					}

					Date Stardaate = null;
					if (schSer.getScheduleServiceNo() == 1) {
						schSer.setScheduleServiceDate(servicedate);
						schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
					}
					if (schSer.getScheduleServiceNo() != 1) {
						if (productEndDate.after(d)) {
							schSer.setScheduleServiceDate(tempdate);
							schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
						} else {
							schSer.setScheduleServiceDate(productEndDate);
							schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
						}
					}
					int weeknumber = AppUtility.getweeknumber(schSer.getScheduleServiceDate());
					schSer.setWeekNo(weeknumber);
//					schSer.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable()
//							.getDataprovider().getList().get(i).getServiceProductList())
					 /***
					  * nidhi *:*:*
					  * 18-09-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive && form.bomValidationActive && schSer.getServiceProductList()!=null ){
						 schSer.setServiceProductList(schSer.getServiceProductList());
					 }
					 /**
					  * end
					  */
				}
				}
			}
			/**
			 * Filtering through Default
			 */
			else if(subFilterType.equals("Default")){
				serviceSchedulePopUp.getP_customise().setValue(false);
				serviceSchedulePopUp.getP_interval().setValue(null);
				serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
				serviceSchedulePopUp.getP_customise().setEnabled(true);
				serviceSchedulePopUp.getP_servicehours().setEnabled(true);
				serviceSchedulePopUp.getP_servicemin().setEnabled(true);
				serviceSchedulePopUp.getP_ampm().setEnabled(true);
				serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
				serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
				serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
				serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
				
				long noServices = (long) (item.getNumberOfServices());
				int noOfdays = item.getDuration();
				double days =  noOfdays;
				double noofservices= item.getNumberOfServices();
				double interval =  days / noofservices;
				double temp1 = interval;
				double tempvalue=interval;
				
				calculateEndDate(item.getStartDate(),item.getDuration());
				Date servicedate = item.getStartDate();
				
				Date d = new Date(servicedate.getTime());
				Date tempdate = new Date();
				
				for(ServiceSchedule schSer:list){
					if(item.getPrduct().getCount()==schSer.getScheduleProdId()
							&&item.getProductSrNo()==schSer.getSerSrNo()){
					int interval2 =(int) Math.round(interval);
					
					if (schSer.getScheduleServiceNo() > 1) {
						CalendarUtil.addDaysToDate(d, interval2);
						tempdate = new Date(d.getTime());
						interval =(interval+temp1)-tempvalue;
					    tempvalue = Math.round(interval);
					}
					
					schSer.setScheduleProdEndDate(calculateEndDate(item.getStartDate(), item.getDuration()));
					if (schSer.getScheduleServiceNo() == 1) {
						schSer.setScheduleServiceDate(servicedate);
					}
					if (schSer.getScheduleServiceNo() != 1) {
						if (calculateEndDate(item.getStartDate(),item.getDuration()).before(d) == false) {
							schSer.setScheduleServiceDate(tempdate);
						} else {
							schSer.setScheduleServiceDate(calculateEndDate(item.getStartDate(),item.getDuration()));
						}
					}
					schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
					int weaknumber = AppUtility.getweeknumber(schSer.getScheduleServiceDate());
					schSer.setWeekNo(weaknumber);
//					schSer.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable()
//							.getDataprovider().getList().get(i).getServiceProductList())
					 /***
					  * nidhi *:*:*
					  * 18-09-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive && form.bomValidationActive && schSer.getServiceProductList() != null){
						 schSer.setServiceProductList(schSer.getServiceProductList());
					 }
					 /**
					  * end
					  */
				}
				}
			}else if(subFilterType.equals("ExcludeDay")){
				
				long noServices = (long) (item.getNumberOfServices());
				int noOfdays = item.getDuration();
				double nodays =  noOfdays;
				double noofservices= item.getNumberOfServices();
				double interval2 =  nodays / noofservices;
				if(serviceSchedulePopUp.getP_interval().getValue() != null && serviceSchedulePopUp.getP_interval().getValue() != 0){
					interval2 =  serviceSchedulePopUp.getP_interval().getValue().doubleValue();
				}
//				double tempinterval = interval;
//				double tempvalue=interval;
				int interval = (int) Math.round(interval2);
				Date servicedate = item.getStartDate();
//				Date d = new Date(servicedate.getTime());
				Date productEndDate = new Date(servicedate.getTime());
				CalendarUtil.addDaysToDate(productEndDate, noOfdays);
				Date prodenddate = new Date(productEndDate.getTime());
				productEndDate = prodenddate;
				Date tempdate = new Date();
				/**
				 * nidhi
				 *  ))..
				 *  for exclude day.
				 */
				productEndDate = calculateEndDate(item.getStartDate(),item.getDuration()-1);
				int startday = servicedate.getDay();
				
				if(excludeDay !=0  && startday == excludeDay-1){
					CalendarUtil.addDaysToDate(servicedate, 1);
				}
				Date d = new Date(servicedate.getTime());
				
				
				if(excludeDay !=0 ){
					CalendarUtil.addDaysToDate(productEndDate, -1);
				}
				int lastday = productEndDate.getDay();
				if(lastday == excludeDay -1){
					CalendarUtil.addDaysToDate(productEndDate, -1);
				}
				/**
				 * end
				 */
				for(ServiceSchedule schSer:list){
					
					if(item.getPrduct().getCount()==schSer.getScheduleProdId()
							&&item.getProductSrNo()==schSer.getSerSrNo()){
					if (schSer.getScheduleServiceNo() > 1) {
						CalendarUtil.addDaysToDate(d,interval );
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						int serDay = d.getDay();
						if(excludeDay !=0  && serDay == excludeDay-1){
							CalendarUtil.addDaysToDate(d, 1);
						
						}
						/**
						 * end
						 */
						tempdate = new Date(d.getTime());
					}

					Date Stardaate = null;
					if (schSer.getScheduleServiceNo() == 1) {
						schSer.setScheduleServiceDate(servicedate);
						schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
					}
					if (schSer.getScheduleServiceNo() != 1) {
						if (productEndDate.after(d)) {
							schSer.setScheduleServiceDate(tempdate);
							schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
						} else {
							schSer.setScheduleServiceDate(productEndDate);
							schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
						}
					}
					int weeknumber = AppUtility.getweeknumber(schSer.getScheduleServiceDate());
					schSer.setWeekNo(weeknumber);
//					schSer.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable()
//							.getDataprovider().getList().get(i).getServiceProductList())
					 /***
					  * nidhi *:*:*
					  * 18-09-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive && form.bomValidationActive  && schSer.getServiceProductList()!=null){
						 schSer.setServiceProductList(schSer.getServiceProductList());
					 }
					 /**
					  * end
					  */
				}
				}
			}
		}
			return list;
		}

		@Override
		public void onChange(ChangeEvent event) {
			
			try {
				Console.log("event.getSource().equals(serviceSchedulePopUp.getP_mondaytofriday()) "+(event.getSource().equals(serviceSchedulePopUp.getP_mondaytofriday())));

			if(event.getSource().equals(serviceSchedulePopUp.getP_mondaytofriday())){
				
				serviceSchedulePopUp.getP_default().setValue(false);
				serviceSchedulePopUp.getP_customise().setValue(false);
				serviceSchedulePopUp.getP_interval().setValue(null);
				serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
				serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
				serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
				
				
				serviceSchedulePopUp.getP_customise().setEnabled(true);
				serviceSchedulePopUp.getP_servicehours().setEnabled(true);
				serviceSchedulePopUp.getP_servicemin().setEnabled(true);
				serviceSchedulePopUp.getP_ampm().setEnabled(true);
				
				
				reactOnMondaytoFridayList(serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex(), serviceSchedulePopUp.getP_serviceWeek().getSelectedIndex());
				
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
			}
			} catch (Exception e) {
				Console.log("exception "+e.getMessage());
			}	
			
			if(event.getSource().equals(serviceSchedulePopUp.getP_servicehours())){
				
//				serviceSchedulePopUp.getP_default().setValue(false);
//				serviceSchedulePopUp.getP_customise().setValue(false);
//				serviceSchedulePopUp.getP_interval().setValue(null);
				if(serviceSchedulePopUp.getP_servicehours().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_servicemin().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_ampm().getSelectedIndex()!=0){
					List<ServiceSchedule> prodservicelist=this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
					if(prodservicelist.size()!=0){
						reactOnHoursList();
					}else{
						form.showDialogMessage("Please Select Either Equal or Interval");
						serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
						serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
						serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
					}
					
					
				}
				
				
//				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
			}
			
			if(event.getSource().equals(serviceSchedulePopUp.getP_servicemin())){
				
				if(serviceSchedulePopUp.getP_servicehours().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_servicemin().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_ampm().getSelectedIndex()!=0){
					List<ServiceSchedule> prodservicelist=this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
					if(prodservicelist.size()!=0){
						reactOnHoursList();
					}else{
						form.showDialogMessage("Please Select At leat One Criteria");
						serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
						serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
						serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
					}
				}
			}
			
			if(event.getSource().equals(serviceSchedulePopUp.getP_ampm())){
				
				if(serviceSchedulePopUp.getP_servicehours().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_servicemin().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_ampm().getSelectedIndex()!=0){
					List<ServiceSchedule> prodservicelist=this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
					if(prodservicelist.size()!=0){
						reactOnHoursList();
					}else{
						form.showDialogMessage("Please Select At leat One Criteria");
						serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
						serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
						serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
					}
				}
			}
			
			if(event.getSource().equals(serviceSchedulePopUp.getP_serviceWeek())){
				serviceSchedulePopUp.getP_default().setValue(false);
				serviceSchedulePopUp.getP_customise().setValue(false);
				serviceSchedulePopUp.getP_interval().setValue(null);
				serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
				serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
				serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);

				serviceSchedulePopUp.getP_servicehours().setEnabled(true);
				serviceSchedulePopUp.getP_servicemin().setEnabled(true);
				serviceSchedulePopUp.getP_ampm().setEnabled(true);
				serviceSchedulePopUp.getP_customise().setEnabled(true);

				reactOnWeekList(serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex(),serviceSchedulePopUp.getP_serviceWeek().getSelectedIndex());
				
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
			}
			
		}
		
		/**
		 * Date : 08-03-2017
		 * added by vijay
		 * for scheduling week wise services 
		 * by default Monday is service day of that specific week
		 * requirement from PCAAMB
		 */
		
		/**
		 * Here i am scheduling service days by week of month and day of week
		 * if we not selected any day then service day is scheduled by monday of week
		 * if we select both day and week and service date scheduled by as per selected day and week
		 */
		
		private void reactOnWeekList(int selectedIndex, int selectedWeekindex) {/*
			

		
		
			if(selectedIndex==0 && selectedWeekindex==0){
				reactforTable();
				serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
			}else{ 
					System.out.println("Only for week scheduling services");
					List<SalesLineItem> salesitemlis = form.saleslineitemquotationtable .getDataprovider().getList();
					ArrayList<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();
					serschelist.clear();
					List<ServiceSchedule> popuptablelist = serviceSchedulePopUp
							.getPopScheduleTable().getDataprovider().getList();
					boolean flag =false;
					
					boolean greterservicesthanweeks = false;
					
					boolean sameMonthserviceflag = false;
					Date temp =null;
					
					for (int i = 0; i < salesitemlis.size(); i++) {

						int qty = 0;
						qty = (int) salesitemlis.get(i).getQty();
						for (int k = 0; k < qty; k++) {

							long noServices = (long) (salesitemlis.get(i)
									.getNumberOfServices());
							int noOfdays = salesitemlis.get(i).getDuration();
							int interval = (int) (noOfdays / salesitemlis.get(i)
									.getNumberOfServices());
							Date servicedate = salesitemlis.get(i).getStartDate();
							Date d = new Date(servicedate.getTime());
							
							int months = noOfdays/30;
							System.out.println("no of months===="+months);
							if(noServices>months){
								greterservicesthanweeks = true;
								form.showDialogMessage("Services greater than availabe weeks can not schedule services with weeks");
								serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
								break;
							}
							

							Date productEndDate = new Date(servicedate.getTime());
							CalendarUtil.addDaysToDate(productEndDate, noOfdays);
							Date prodenddate = new Date(productEndDate.getTime());
							productEndDate = prodenddate;

							for (int j = 0; j < noServices; j++) {

								ServiceSchedule scheduleEntity = new ServiceSchedule();

								Date Stardaate = null;
								Stardaate = salesitemlis.get(i).getStartDate();

								System.out.println("In side method RRRRRRRRRRRRR"
										+ salesitemlis.get(i).getProductSrNo());
								scheduleEntity.setSerSrNo(salesitemlis.get(i)
										.getProductSrNo());

								scheduleEntity.setScheduleStartDate(Stardaate);
								scheduleEntity.setScheduleProdId(salesitemlis.get(i)
										.getPrduct().getCount());
								scheduleEntity.setScheduleProdName(salesitemlis.get(i)
										.getProductName());
								scheduleEntity.setScheduleDuration(salesitemlis.get(i)
										.getDuration());
								scheduleEntity.setScheduleNoOfServices(salesitemlis
										.get(i).getNumberOfServices());
								scheduleEntity.setScheduleServiceNo(j + 1);

								scheduleEntity.setScheduleProdStartDate(Stardaate);
								scheduleEntity.setScheduleProdEndDate(productEndDate);

								scheduleEntity.setServicingBranch(form.olbbBranch
										.getValue());
								scheduleEntity.setTotalServicingTime(salesitemlis
										.get(i).getServicingTIme());

								String calculatedServiceTime = "";
								if (popuptablelist.size() != 0) {
									calculatedServiceTime = popuptablelist.get(j)
											.getScheduleServiceTime();
								} else {
									calculatedServiceTime = "Flexible";
								}

								scheduleEntity
										.setScheduleServiceTime(calculatedServiceTime);

								if (form.customerbranchlist.size() == qty) {
									scheduleEntity
											.setScheduleProBranch(form.customerbranchlist
													.get(k).getBusinessUnitName());

									System.out
											.println("updateServices presenter cust Branch Name "
													+ form.customerbranchlist.get(k)
															.getBusinessUnitName());
									System.out.println("servicing Branch "
											+ form.customerbranchlist.get(k)
													.getBranch());
									if (form.customerbranchlist.get(k).getBranch() != null) {
										System.out.println("if servicing Branch "
												+ form.customerbranchlist.get(k)
														.getBranch());
										scheduleEntity
												.setServicingBranch(form.customerbranchlist
														.get(k).getBranch());
									} else {
										System.out.println("else servicing Branch "
												+ form.customerbranchlist.get(k)
														.getBranch());
										scheduleEntity
												.setServicingBranch(form.olbbBranch
														.getValue());
									}
								} else if (form.customerbranchlist.size() == 0) {
									scheduleEntity
											.setScheduleProBranch("Service Address");

									scheduleEntity.setServicingBranch(form.olbbBranch
											.getValue());

								} else {
									scheduleEntity.setScheduleProBranch(null);
								}
								
								
								
								Date day = new Date(d.getTime());
								
								*//**
								 * Date 1 jun 2017 added by vijay for service getting same month 
								 *//*
								if(temp!=null){
									String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
									String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
									if(currentServiceMonth.equals(lastserviceDateMonth)){
										sameMonthserviceflag =true;
									}
								}
								
								if(sameMonthserviceflag){
									CalendarUtil.addDaysToDate(day, 30);
								}
								*//**
								 * ends here
								 *//*
								
								CalendarUtil.setToFirstDayOfMonth(day);
								
								if(selectedWeekindex==1){
									 day = AppUtility.getCalculatedDate(day,selectedIndex);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										 day =  AppUtility.getCalculatedDate(day,selectedIndex);
									}
									
								}
								else if(selectedWeekindex==2){
									
									CalendarUtil.addDaysToDate(day, 7);
									 day =  AppUtility.getCalculatedDate(day,selectedIndex);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 7);
										 day =  AppUtility.getCalculatedDate(day,selectedIndex);
									}
									 
								}else if(selectedWeekindex==3){
									CalendarUtil.addDaysToDate(day, 14);
									 day =  AppUtility.getCalculatedDate(day,selectedIndex);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 14);
										 day =  AppUtility.getCalculatedDate(day,selectedIndex);
									}
								}
								else if(selectedWeekindex==4){
									CalendarUtil.addDaysToDate(day, 21);
									 day =  AppUtility.getCalculatedDate(day,selectedIndex);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 21);
										 day =  AppUtility.getCalculatedDate(day,selectedIndex);
									}
								}
								 temp = day;
								
								
//								CalendarUtil.addDaysToDate(d, interval);
//								Date tempdate = new Date(d.getTime());
//								
//								d = tempdate;
//								System.out.println("Interval added date"+d);
								
								
								*//**
								 * Date 26 jun 2017 added by vijay for issues problems in 3 and 4 service gap 
								 *//*
								 
								System.out.println(productEndDate.before(day) == false);
								if (productEndDate.before(day) == false) {
									scheduleEntity.setScheduleServiceDate(day);
								} else {
									scheduleEntity.setScheduleServiceDate(productEndDate);
									
								}
								
								System.out.println("DDDDDDDDDD"+d);
								
								CalendarUtil.addDaysToDate(d, interval);
								System.out.println("DDDDDDDDDD ============= "+d);

								int days =0;
								String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
								System.out.println("serviceStartDate"+serviceStartDate);
								if(serviceStartDate.equals("1")){
										
									String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
									System.out.println("currentServiceDay"+currentServiceDate);
									
									 days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
									System.out.println("VIJAY NEW DATE+++++++"+days);
								
									CalendarUtil.addDaysToDate(d, days);

								}
								
								

								System.out.println("Sceduled Date==="+d);
								Date tempdate = new Date(d.getTime());
								
								d = tempdate;
								System.out.println("Interval added date"+d);
								
								
								if (productEndDate.before(day) == false) {
									scheduleEntity.setScheduleServiceDate(day);
								} else {
									scheduleEntity.setScheduleServiceDate(productEndDate);
								}
								
								*//**
								 * ends here
								 *//*
								
								scheduleEntity.setScheduleServiceDay(ContractForm.serviceDay(scheduleEntity
												.getScheduleServiceDate()));

								scheduleEntity.setServiceRemark(salesitemlis.get(i).getRemark());
								
								*//**
								 * Date 03-04-2017 added by vijay for week number
								 *//*
								int weeknumber = AppUtility.getweeknumber(scheduleEntity.getScheduleServiceDate());
								scheduleEntity.setWeekNo(weeknumber);
								
								*//**
								 * ends here
								 *//*
								
								serschelist.add(scheduleEntity);
							}
						}

						if(greterservicesthanweeks){
							break;
						}
						serviceSchedulePopUp.getPopScheduleTable().getDataprovider()
								.setList(serschelist);
					}
					
			}
		*/
			

			/**
			 * @author Vijay Date :-  07-09-2021
			 *  Des :- Service Scheduling with new single common method with new round up logic for innovative
			 */
			if(LoginPresenter.serviceScheduleNewLogicFlag){
				if(selectedIndex==0 && selectedWeekindex==0 ){
					reactforTable();
					serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
					selectedIndex = 0;
					
				}else{ 
					
					 List<SalesLineItem> salesitem = new ArrayList<SalesLineItem>();
					 salesitem = form.getSaleslineitemquotationtable().getDataprovider().getList();
					 ContractForm contractform = new ContractForm();
					 List<ServiceSchedule> serschelist=contractform.getServiceSchedulelist(AppConstants.WEEK,salesitem,form.dbQuotationDate.getValue(),form.customerbranchlist,form.olbbBranch.getValue(),0,selectedIndex,selectedWeekindex,0,serviceSchedulePopUp);
				
					 serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
					 
				}
				 return;
			}
			/**
			 * ends here
			 */
			
			if(selectedIndex==0 && selectedWeekindex==0){
				reactforTable();
				serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
			}else{ 	
				boolean unitflag = false , bomCheck = true;
				List<SalesLineItem> salesItemList = form.saleslineitemquotationtable.getDataprovider().getList();
				ArrayList<ServiceSchedule> updatedSchSerList = new ArrayList<ServiceSchedule>();
				updatedSchSerList.clear();
				List<ServiceSchedule> schServList = serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				
				boolean flag =false;
				boolean greterservicesthanweeks = false;
				boolean sameMonthserviceflag = false;
				Date temp =null;
				
				for (int i = 0; i < salesItemList.size(); i++) {
					int qty = 0;
					qty = (int) salesItemList.get(i).getQty();
					for (int k = 0; k < qty; k++) {

						long noServices = (long) (salesItemList.get(i).getNumberOfServices());
						int noOfdays = salesItemList.get(i).getDuration();
						int interval = (int) (noOfdays / salesItemList.get(i).getNumberOfServices());

						Date servicedate = salesItemList.get(i).getStartDate();
						Date d = new Date(servicedate.getTime());
						
						int months = noOfdays/30;
						if(noServices>months){
							greterservicesthanweeks = true;
							form.showDialogMessage("Services greater than availabe weeks can not schedule services with weeks");
							serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
							break;
						}

						Date productEndDate = new Date(servicedate.getTime());
						CalendarUtil.addDaysToDate(productEndDate, noOfdays);
						Date prodenddate = new Date(productEndDate.getTime());
						productEndDate = calculateEndDate(servicedate,salesItemList.get(i).getDuration()-1 );
						
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
					/*	int startday = servicedate.getDay();
						
						if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(servicedate, 1);
						}
//						Date d = new Date(servicedate.getTime());
						
						if(selectedExcludDay !=0 ){
							CalendarUtil.addDaysToDate(productEndDate, -1);
						}
						
						int lastday = productEndDate.getDay();
						
						if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(productEndDate, -1);
						}*/
						/**
						 * end
						 */
						
						
						for (int j = 0; j < noServices; j++) {

							ServiceSchedule scheduleEntity = new ServiceSchedule();

							Date Stardaate = null;
							Stardaate = salesItemList.get(i).getStartDate();
							scheduleEntity.setSerSrNo(salesItemList.get(i).getProductSrNo());
							scheduleEntity.setScheduleStartDate(Stardaate);
							scheduleEntity.setScheduleProdId(salesItemList.get(i).getPrduct().getCount());
							scheduleEntity.setScheduleProdName(salesItemList.get(i).getProductName());
							scheduleEntity.setScheduleDuration(salesItemList.get(i).getDuration());
							scheduleEntity.setScheduleNoOfServices(salesItemList.get(i).getNumberOfServices());
							scheduleEntity.setScheduleServiceNo(j + 1);
							scheduleEntity.setScheduleProdStartDate(Stardaate);
							scheduleEntity.setScheduleProdEndDate(productEndDate);
							scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
							scheduleEntity.setTotalServicingTime(salesItemList.get(i).getServicingTIme());

							String calculatedServiceTime = "";
							if (schServList.size() != 0) {
								calculatedServiceTime = schServList.get(j).getScheduleServiceTime();
							} else {
								calculatedServiceTime = "Flexible";
							}
							scheduleEntity.setScheduleServiceTime(calculatedServiceTime);

							if (form.customerbranchlist.size() == qty) {
								scheduleEntity.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
								if (form.customerbranchlist.get(k).getBranch() != null) {
									scheduleEntity.setServicingBranch(form.customerbranchlist.get(k).getBranch());
								} else {
									scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
								}
							} else if (form.customerbranchlist.size() == 0) {
								scheduleEntity.setScheduleProBranch("Service Address");
								scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
							} else {
								scheduleEntity.setScheduleProBranch(null);
							}
							Date day = new Date(d.getTime());
							
							/**
							 * Date 1 jun 2017 added by vijay for service getting same month 
							 */
							if(temp!=null){
								String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
								String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
								if(currentServiceMonth.equals(lastserviceDateMonth)){
									sameMonthserviceflag =true;
								}
							}
							if(sameMonthserviceflag){
								CalendarUtil.addDaysToDate(day, 30);
							}
							/**
							 * ends here
							 */
							
							CalendarUtil.setToFirstDayOfMonth(day);
							
							if (selectedWeekindex == 1) {
								day = AppUtility.getCalculatedDate(day,selectedIndex);
								if (day.before(Stardaate)) {
									if (j == 0) {
										day = Stardaate;
									} else {
										flag = true;
									}
								}
								if (flag || j > 0) {
									day = AppUtility.getCalculatedDate(day,selectedIndex);
								}
							}else if (selectedWeekindex == 2) {
								CalendarUtil.addDaysToDate(day, 7);
								day = AppUtility.getCalculatedDate(day,selectedIndex);
								if (day.before(Stardaate)) {
									if (j == 0) {
										day = Stardaate;
									} else {
										flag = true;
									}
								}
								if (flag || j > 0) {
									CalendarUtil.setToFirstDayOfMonth(day);
									CalendarUtil.addDaysToDate(day, 7);
									day = AppUtility.getCalculatedDate(day,selectedIndex);
								}
							}else if (selectedWeekindex == 3) {
								CalendarUtil.addDaysToDate(day, 14);
								day = AppUtility.getCalculatedDate(day,
										selectedIndex);
								if (day.before(Stardaate)) {
									if (j == 0) {
										day = Stardaate;
									} else {
										flag = true;
									}
								}
								if (flag || j > 0) {
									CalendarUtil.setToFirstDayOfMonth(day);
									CalendarUtil.addDaysToDate(day, 14);
									day = AppUtility.getCalculatedDate(day,selectedIndex);
								}
							}else if (selectedWeekindex == 4) {
								CalendarUtil.addDaysToDate(day, 21);
								day = AppUtility.getCalculatedDate(day,selectedIndex);
								if (day.before(Stardaate)) {
									if (j == 0) {
										day = Stardaate;
									} else {
										flag = true;
									}
								}
								if (flag || j > 0) {
									CalendarUtil.setToFirstDayOfMonth(day);
									CalendarUtil.addDaysToDate(day, 21);
									day = AppUtility.getCalculatedDate(day,selectedIndex);
								}
							}
							temp = day;
							 
							/**
							 * Date 26 jun 2017 added by vijay for issues problems in 3 and 4 service gap 
							 */
							/**
							 * nidhi
							 *  ))..
							 *  for exclude day.
							 */
							int serDay = day.getDay();
							/*if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
								CalendarUtil.addDaysToDate(day, 1);
//								d = day;
							}*/
							/**
							 * end
							 */
							
							System.out.println(productEndDate.before(day) == false);
							if (productEndDate.before(day) == false) {
								scheduleEntity.setScheduleServiceDate(day);
							} else {
								scheduleEntity.setScheduleServiceDate(productEndDate);
								
							}
							CalendarUtil.addDaysToDate(d, interval);
							int days =0;
							String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
							if(serviceStartDate.equals("1")){
								String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
								days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
								CalendarUtil.addDaysToDate(d, days);
							}
							Date tempdate = new Date(d.getTime());
							d = tempdate;
							/**
							 * ends here
							 */
							
							scheduleEntity.setScheduleServiceDay(ContractForm.serviceDay(scheduleEntity.getScheduleServiceDate()));
							scheduleEntity.setServiceRemark(salesItemList.get(i).getRemark());
							/**
							 * Date 03-04-2017 added by vijay for week number
							 */
							int weeknumber = AppUtility.getweeknumber(scheduleEntity.getScheduleServiceDate());
							scheduleEntity.setWeekNo(weeknumber);
							/**
							 * ends here
							 */
							
							 /***
							  * nidhi *:*:*
							  * 18-09-2018
							  * for map bill product
							  * if any update please update in else conditions also
							  */
							 if(LoginPresenter.billofMaterialActive && form.bomValidationActive ){
								 
								 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesItemList.get(i).getPrduct());
								 if(billPrDt!=null){
									 
									 UnitConversionUtility unitConver = new UnitConversionUtility();
									 
									 if(form.customerbranchlist.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, form.customerbranchlist.get(k).getUnitOfMeasurement())){
										 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
										 seList.add(scheduleEntity);
										 seList = unitConver.getServiceitemProList(seList,salesItemList.get(i).getPrduct(),salesItemList.get(i),billPrDt);
										 if(seList!=null && seList.size()>0)
											 scheduleEntity = seList.get(0);
										 
									 }else{
										 unitflag = true;
//										 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
									 }
									 
								 }else{
									 bomCheck = false;
								 }
							 }
							 /**
							  * end
							  */
							updatedSchSerList.add(scheduleEntity);
						}
					}

					if(greterservicesthanweeks){
						break;
					}
					serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(updatedSchSerList);
					/**
					 * Date : 24-11-2017 BY ANIL
					 */
//					form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
					/**
					 * End
					 */
				}
				if(unitflag){
					form.showDialogMessage("Unit of service area and product group detail area not mapped at branch level. Or conversion not available");
				}
				if(!bomCheck){
					form.showDialogMessage("Bill of material is not Created for this service please create first . \n you are not allow to save quotation.");
				}
			}
		
		
		
		}
		
		/**
		 * ends here
		 */
		
		
		/** NOTE:  Service Schedule Any Change Add Here */
		
		/**
		 *  Service Schedule Logic Starts Here
		 */
		/********************************Below Is The Code For Service Scheduling***************************/

		private void reactforreset() {

			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_interval().setValue(null);
			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
			serviceSchedulePopUp.getP_customise().setValue(false);
			
			serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
			
			serviceSchedulePopUp.getP_customise().setEnabled(false);
			serviceSchedulePopUp.getP_servicehours().setEnabled(false);
			serviceSchedulePopUp.getP_servicemin().setEnabled(false);
			serviceSchedulePopUp.getP_ampm().setEnabled(false);
			
			
			serviceSchedulePopUp.getPopScheduleTable().connectToLocal();
		}

		private void reactforcustumize() {

//			if(specificday==1){
//				form.showDialogMessage("You Selected Specific Day is Remove");
//				specificday=0;
//			}

			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_interval().setValue(null);
//			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
			
			serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
			
			form.checkCustomerBranch(custcount);
			
		}
		
		private void reactforTable() {/*
			
			System.out.println("REACT TO TABLE....");
			for(int i=0;i<form.serviceScheduleTable.getDataprovider().getList().size();i++){
				System.out.println();
				 System.out.println("SERVICING BRANCH ::: "+form.serviceScheduleTable.getDataprovider().getList().get(i).getServicingBranch());
				 System.out.println("SERVICING TIME ::: "+form.serviceScheduleTable.getDataprovider().getList().get(i).getTotalServicingTime());
				 System.out.println();
			}
			
			
			List<ServiceSchedule> serviceitemlist=form.serviceScheduleTable.getDataprovider().getList();
			List<SalesLineItem> servlist=form.getSaleslineitemquotationtable().getDataprovider().getList();
			ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
			
			System.out.println("AFTER!!!!");
			for(int i=0;i<serviceitemlist.size();i++){
				
				 System.out.println();
				 System.out.println("SERVICING BRANCH ::: "+serviceitemlist.get(i).getServicingBranch());
				 System.out.println("SERVICING TIME ::: "+serviceitemlist.get(i).getTotalServicingTime());
				 System.out.println();
			 }
			
			for(int i=0;i<serviceitemlist.size();i++)
			{
				ServiceSchedule scheduleEntity =new ServiceSchedule();
				
				if(serviceitemlist.get(i).getScheduleStartDate()!=null){
					scheduleEntity.setScheduleStartDate(serviceitemlist.get(i).getScheduleStartDate());
				}else{
					scheduleEntity.setScheduleStartDate(null);
				}
				
				
			//   rohan added  this for pesto india changes 
				
				if(serviceitemlist.get(i).getSerSrNo()!=0){
					scheduleEntity.setSerSrNo(serviceitemlist.get(i).getSerSrNo());
				}else{
					scheduleEntity.setSerSrNo(0);
				}
				
				if(serviceitemlist.get(i).getScheduleProdId()!=0){
					scheduleEntity.setScheduleProdId(serviceitemlist.get(i).getScheduleProdId());
				}else{
					scheduleEntity.setScheduleProdId(0);
				}
				if(!serviceitemlist.get(i).getScheduleProdName().equals("")){
					scheduleEntity.setScheduleProdName(serviceitemlist.get(i).getScheduleProdName());
				}else{
					scheduleEntity.setScheduleProdName(null);
				}
				if(serviceitemlist.get(i).getScheduleDuration()!=0){
					scheduleEntity.setScheduleDuration(serviceitemlist.get(i).getScheduleDuration());
				}else{
					scheduleEntity.setScheduleDuration(0);
				}
				
				if(serviceitemlist.get(i).getScheduleNoOfServices()!=0){
					scheduleEntity.setScheduleNoOfServices(serviceitemlist.get(i).getScheduleNoOfServices());
				}else{
					scheduleEntity.setScheduleNoOfServices(0);
				}
				if(serviceitemlist.get(i).getScheduleServiceNo()!=0){
					scheduleEntity.setScheduleServiceNo(serviceitemlist.get(i).getScheduleServiceNo());
				}else{
					scheduleEntity.setScheduleServiceNo(0);
				}
				if(serviceitemlist.get(i).getScheduleServiceDate()!=null){
					scheduleEntity.setScheduleServiceDate(serviceitemlist.get(i).getScheduleServiceDate());
				}else{
					scheduleEntity.setScheduleServiceDate(null);
				}
				
				if(serviceitemlist.get(i).getScheduleProdStartDate()!=null){
					scheduleEntity.setScheduleProdStartDate(serviceitemlist.get(i).getScheduleProdStartDate());
				}else{
					scheduleEntity.setScheduleProdStartDate(null);
				}
				if(serviceitemlist.get(i).getScheduleProdEndDate()!=null){
					scheduleEntity.setScheduleProdEndDate(serviceitemlist.get(i).getScheduleProdEndDate());
				}else{
					scheduleEntity.setScheduleProdEndDate(null);
				}
				if(serviceitemlist.get(i).getScheduleServiceTime()!=null){
					scheduleEntity.setScheduleServiceTime(serviceitemlist.get(i).getScheduleServiceTime());
				}else{
					scheduleEntity.setScheduleServiceTime("Flexible");
				}
				
				if(serviceitemlist.get(i).getScheduleProBranch()!=null){
					scheduleEntity.setScheduleProBranch(serviceitemlist.get(i).getScheduleProBranch());
				}else{
					scheduleEntity.setScheduleProBranch(null);
				}
				if(serviceitemlist.get(i).getScheduleServiceDay()!=null){
					scheduleEntity.setScheduleServiceDay(serviceitemlist.get(i).getScheduleServiceDay());
				}else{
					scheduleEntity.setScheduleServiceDay(null);
				}
				
				if(serviceitemlist.get(i).getPremisesDetails()!=null){
					scheduleEntity.setPremisesDetails(serviceitemlist.get(i).getPremisesDetails());
				}else{
					scheduleEntity.setPremisesDetails(null);
				}
				
				System.out.println("BRANCH NOT NULL ...."+serviceitemlist.get(i).getServicingBranch());
				if(serviceitemlist.get(i).getServicingBranch()!=null){
					scheduleEntity.setServicingBranch(serviceitemlist.get(i).getServicingBranch());
				}else{
					scheduleEntity.setServicingBranch(null);
				}
				System.out.println("SERVICING TIME NOT NULL ...."+serviceitemlist.get(i).getTotalServicingTime());
				if(serviceitemlist.get(i).getTotalServicingTime()!=null){
					scheduleEntity.setTotalServicingTime(serviceitemlist.get(i).getTotalServicingTime());
				}else{
					scheduleEntity.setTotalServicingTime(null);
				}
				
				*//**
				 * Date 01-04-2017 added by vijay for week number
				 *//*
				scheduleEntity.setWeekNo(serviceitemlist.get(i).getWeekNo());
				*//**
				 * ends here
				 *//*
				*//**
				 * nidhi |*|
				 *//*
				serschelist.add(scheduleEntity);
			}
			
			serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
			
		*/
		
			

			List<ServiceSchedule> serviceitemlist = form.serviceScheduleTable.getDataprovider().getList();
			ArrayList<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();
			for (int i = 0; i < serviceitemlist.size(); i++) {
				ServiceSchedule scheduleEntity = new ServiceSchedule();
				if (serviceitemlist.get(i).getScheduleStartDate() != null) {
					scheduleEntity.setScheduleStartDate(serviceitemlist.get(i).getScheduleStartDate());
				} else {
					scheduleEntity.setScheduleStartDate(null);
				}
				// rohan added this for pesto india changes
				if (serviceitemlist.get(i).getSerSrNo() != 0) {
					scheduleEntity.setSerSrNo(serviceitemlist.get(i).getSerSrNo());
				} else {
					scheduleEntity.setSerSrNo(0);
				}
				if (serviceitemlist.get(i).getScheduleProdId() != 0) {
					scheduleEntity.setScheduleProdId(serviceitemlist.get(i).getScheduleProdId());
				} else {
					scheduleEntity.setScheduleProdId(0);
				}
				if (!serviceitemlist.get(i).getScheduleProdName().equals("")) {
					scheduleEntity.setScheduleProdName(serviceitemlist.get(i).getScheduleProdName());
				} else {
					scheduleEntity.setScheduleProdName(null);
				}
				if (serviceitemlist.get(i).getScheduleDuration() != 0) {
					scheduleEntity.setScheduleDuration(serviceitemlist.get(i).getScheduleDuration());
				} else {
					scheduleEntity.setScheduleDuration(0);
				}
				if (serviceitemlist.get(i).getScheduleNoOfServices() != 0) {
					scheduleEntity.setScheduleNoOfServices(serviceitemlist.get(i).getScheduleNoOfServices());
				} else {
					scheduleEntity.setScheduleNoOfServices(0);
				}
				if (serviceitemlist.get(i).getScheduleServiceNo() != 0) {
					scheduleEntity.setScheduleServiceNo(serviceitemlist.get(i).getScheduleServiceNo());
				} else {
					scheduleEntity.setScheduleServiceNo(0);
				}
				if (serviceitemlist.get(i).getScheduleServiceDate() != null) {
					scheduleEntity.setScheduleServiceDate(serviceitemlist.get(i).getScheduleServiceDate());
				} else {
					scheduleEntity.setScheduleServiceDate(null);
				}
				if (serviceitemlist.get(i).getScheduleProdStartDate() != null) {
					scheduleEntity.setScheduleProdStartDate(serviceitemlist.get(i).getScheduleProdStartDate());
				} else {
					scheduleEntity.setScheduleProdStartDate(null);
				}
				if (serviceitemlist.get(i).getScheduleProdEndDate() != null) {
					scheduleEntity.setScheduleProdEndDate(serviceitemlist.get(i).getScheduleProdEndDate());
				} else {
					scheduleEntity.setScheduleProdEndDate(null);
				}
				if (serviceitemlist.get(i).getScheduleServiceTime() != null) {
					scheduleEntity.setScheduleServiceTime(serviceitemlist.get(i).getScheduleServiceTime());
				} else {
					scheduleEntity.setScheduleServiceTime("Flexible");
				}
				if (serviceitemlist.get(i).getPremisesDetails() != null) {
					scheduleEntity.setPremisesDetails(serviceitemlist.get(i).getPremisesDetails());
				} else {
					scheduleEntity.setPremisesDetails(null);
				}
				if (serviceitemlist.get(i).getScheduleProBranch() != null) {
					scheduleEntity.setScheduleProBranch(serviceitemlist.get(i).getScheduleProBranch());
				} else {
					scheduleEntity.setScheduleProBranch(null);
				}
				if (serviceitemlist.get(i).getScheduleServiceDay() != null) {
					scheduleEntity.setScheduleServiceDay(serviceitemlist.get(i).getScheduleServiceDay());
				} else {
					scheduleEntity.setScheduleServiceDay(null);
				}
				if (serviceitemlist.get(i).getServicingBranch() != null) {
					scheduleEntity.setServicingBranch(serviceitemlist.get(i).getServicingBranch());
				} else {
					scheduleEntity.setServicingBranch(null);
				}
				if (serviceitemlist.get(i).getTotalServicingTime() != null) {
					scheduleEntity.setTotalServicingTime(serviceitemlist.get(i).getTotalServicingTime());
				} else {
					scheduleEntity.setServicingBranch(null);
				}
				// vijay
				if(serviceitemlist.get(i).getServiceRemark()!=null){
					scheduleEntity.setServiceRemark(serviceitemlist.get(i).getServiceRemark());
				}else{
					scheduleEntity.setServiceRemark("");
				}
				/**
				 * Date 01-04-2017 added by vijay for week number
				 */
				scheduleEntity.setWeekNo(serviceitemlist.get(i).getWeekNo());
				/**
				 * ends here
				 */
				 /***
				  * nidhi *:*:*
				  * 18-09-2018
				  * for map bill product
				  * if any update please update in else conditions also
				  */
				 if(LoginPresenter.billofMaterialActive && form.bomValidationActive  && serviceitemlist.get(i).getServiceProductList()!=null){
					 scheduleEntity.setServiceProductList(serviceitemlist.get(i).getServiceProductList());
				 }
				 /**
				  * end
				  */
				serschelist.add(scheduleEntity);
			}
			
			/** 
			 * Date 1 nov 2017 added by vijay for sorting with product Id and Sr.No
			 */
			Collections.sort(serschelist, new Comparator<ServiceSchedule>() {
				@Override
				public int compare(ServiceSchedule s1, ServiceSchedule s2) {
					Integer productId = s1.getScheduleProdId();
					Integer productId2 = s2.getScheduleProdId();
					return productId.compareTo(productId2);
				}
			});
			Collections.sort(serschelist, new Comparator<ServiceSchedule>() {
				@Override
				public int compare(ServiceSchedule s1, ServiceSchedule s2) {
					Integer srNo1 = s1.getSerSrNo();
					Integer srNo2 = s2.getSerSrNo();
					return srNo1.compareTo(srNo2);
				}
			});
			/**
			 * ends here
			 */
			serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
			/**
			 * Date : 23-11-2017 BY ANIL
			 */
//			form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
//			form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
			/**
			 * End
			 */
		
		
		}

		

private void reactfordefaultTable() {

	
	serviceSchedulePopUp.getP_customise().setValue(false);
	serviceSchedulePopUp.getP_interval().setValue(null);
	serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);

	serviceSchedulePopUp.getP_customise().setEnabled(true);
	serviceSchedulePopUp.getP_servicehours().setEnabled(true);
	serviceSchedulePopUp.getP_servicemin().setEnabled(true);
	serviceSchedulePopUp.getP_ampm().setEnabled(true);

	serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
	serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
	serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
	//date 04-04-2017 added by vijay
	serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);

	/**
	 * @author Vijay Date :-  07-09-2021
	 *  Des :- Service Scheduling with new single common method with new round up logic for innovative
	 */
	if(LoginPresenter.serviceScheduleNewLogicFlag){
		 List<SalesLineItem> salesitem = new ArrayList<SalesLineItem>();
		 salesitem = form.getSaleslineitemquotationtable().getDataprovider().getList();
		 ContractForm contractform = new ContractForm();
		 List<ServiceSchedule> serschelist=contractform.getServiceSchedulelist("Default",salesitem,form.dbQuotationDate.getValue(),form.customerbranchlist,form.olbbBranch.getValue(),0,0,0,0,serviceSchedulePopUp);
		 serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
		return;
	}
	/**
	 * ends here
	 */
	
	List<SalesLineItem> salesitemlis = form.saleslineitemquotationtable.getDataprovider().getList();
	System.out.println("sizeeeeee() == " + salesitemlis.size());
	ArrayList<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();
	serschelist.clear();

	List<ServiceSchedule> popuptablelist = serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();

	for (int i = 0; i < salesitemlis.size(); i++) {
		/**
		 * Date : 29-03-2017 By ANil
		 */
		boolean branchSchFlag=false;
		int qty=0;
		
		/**
		 * Date 02-05-2018 
		 * Developer : Vijay
		 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
		 * so i have updated below code using hashmap. 
		 */
		
//		/**
//		 * Date : 03-04-2017 By ANIL
//		 */
////		ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(salesitemlis.get(i).getBranchSchedulingInfo());
//		
//		/**
//		 * Date : 12-06-2017 BY ANIL
//		 */
//		String[] branchArray=new String[10];
//		
//		if(salesitemlis.get(i).getBranchSchedulingInfo()!=null
//				&&!salesitemlis.get(i).getBranchSchedulingInfo().equals("")){
//			branchArray[0]=salesitemlis.get(i).getBranchSchedulingInfo();
//		}
//		if(salesitemlis.get(i).getBranchSchedulingInfo1()!=null
//				&&!salesitemlis.get(i).getBranchSchedulingInfo1().equals("")){
//			branchArray[1]=salesitemlis.get(i).getBranchSchedulingInfo1();
//		}
//		if(salesitemlis.get(i).getBranchSchedulingInfo2()!=null
//				&&!salesitemlis.get(i).getBranchSchedulingInfo2().equals("")){
//			branchArray[2]=salesitemlis.get(i).getBranchSchedulingInfo2();
//		}
//		if(salesitemlis.get(i).getBranchSchedulingInfo3()!=null
//				&&!salesitemlis.get(i).getBranchSchedulingInfo3().equals("")){
//			branchArray[3]=salesitemlis.get(i).getBranchSchedulingInfo3();
//		}
//		if(salesitemlis.get(i).getBranchSchedulingInfo4()!=null
//				&&!salesitemlis.get(i).getBranchSchedulingInfo4().equals("")){
//			branchArray[4]=salesitemlis.get(i).getBranchSchedulingInfo4();
//		}
//		
//		if(salesitemlis.get(i).getBranchSchedulingInfo5()!=null
//				&&!salesitemlis.get(i).getBranchSchedulingInfo5().equals("")){
//			branchArray[5]=salesitemlis.get(i).getBranchSchedulingInfo5();
//		}
//		if(salesitemlis.get(i).getBranchSchedulingInfo6()!=null
//				&&!salesitemlis.get(i).getBranchSchedulingInfo6().equals("")){
//			branchArray[6]=salesitemlis.get(i).getBranchSchedulingInfo6();
//		}
//		if(salesitemlis.get(i).getBranchSchedulingInfo7()!=null
//				&&!salesitemlis.get(i).getBranchSchedulingInfo7().equals("")){
//			branchArray[7]=salesitemlis.get(i).getBranchSchedulingInfo7();
//		}
//		if(salesitemlis.get(i).getBranchSchedulingInfo8()!=null
//				&&!salesitemlis.get(i).getBranchSchedulingInfo8().equals("")){
//			branchArray[8]=salesitemlis.get(i).getBranchSchedulingInfo8();
//		}
//		if(salesitemlis.get(i).getBranchSchedulingInfo9()!=null
//				&&!salesitemlis.get(i).getBranchSchedulingInfo9().equals("")){
//			branchArray[9]=salesitemlis.get(i).getBranchSchedulingInfo9();
//		}
//		
//		/**
//		 * End
//		 */
//		ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
		
		/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
		ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
		
		/**
		 * ends here
		 */
		
		if(branchSchedulingList!=null
				&&branchSchedulingList.size()!=0){
			branchSchFlag=true;
			qty=branchSchedulingList.size();
		}else{
			qty=(int) salesitemlis.get(i).getQty();
		}
		
		/**
		 * End
		 */
		System.out.println("reactfordefaultTable Mathod cAall :- " + branchSchedulingList + " main");
		if(branchSchFlag){
			
			for (int k = 0; k < qty; k++) {
				if(branchSchedulingList.get(k).isCheck()==true){
					
				long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
				int noOfdays = salesitemlis.get(i).getDuration();
				
				/********* Date 02 oct 2017 below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//				int interval = (int) (noOfdays / salesitemlis.get(i).getNumberOfServices());
				
				/*** Date 02 oct 2017 below code added by vijay interval calculation with precision and using roundoff ******************/
				double days =  noOfdays;
				double noofservices= salesitemlis.get(i).getNumberOfServices();
				double interval =  days / noofservices;
				double temp = interval;
				double tempvalue=interval;
				/******************** ends here ********************/
				
				
				Date servicedate = salesitemlis.get(i).getStartDate();
				Date d = new Date(servicedate.getTime());
				Date productEndDate = new Date(servicedate.getTime());
				CalendarUtil.addDaysToDate(productEndDate, noOfdays);
				Date prodenddate = new Date(productEndDate.getTime());
				productEndDate = prodenddate;
				Date tempdate = new Date();
				for (int j = 0; j < noServices; j++) {
						
					ServiceSchedule ssch = new ServiceSchedule();
					Date Stardaate = salesitemlis.get(i).getStartDate();

					ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
					ssch.setScheduleStartDate(Stardaate);
					ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
					ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
					ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
					ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
					ssch.setScheduleServiceNo(j + 1);
					ssch.setScheduleProdStartDate(Stardaate);
					ssch.setScheduleProdEndDate(productEndDate);
					ssch.setScheduleServiceTime("Flexible");

					ssch.setServicingBranch(form.olbbBranch.getValue());
					ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());

					
				
					ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
					if (branchSchedulingList.get(k).getServicingBranch()!=null) {
						ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
					} 
					
					int dayIndex=AppUtility.getDayIndex(branchSchedulingList.get(k).getDay());
					
					if(dayIndex!=0){
						String dayOfWeek1 = format.format(d);
						int adate = Integer.parseInt(dayOfWeek1);
						int adday1 = dayIndex - 1;
						int adday = 0;
						if (adate <= adday1) {
							adday = (adday1 - adate);
						} else {
							adday = (7 - adate + adday1);
						}
						
						Date newDate = new Date(d.getTime());
						CalendarUtil.addDaysToDate(newDate, adday);
						Date day = new Date(newDate.getTime());

						if (productEndDate.before(day) == false) {
							ssch.setScheduleServiceDate(day);
						} else {
							ssch.setScheduleServiceDate(productEndDate);
						}

						/*** Date 02 oct 2017 added by vijay for interval calculation *************/
						int interval2 =(int) Math.round(interval);

						
						CalendarUtil.addDaysToDate(d, interval2);
						Date tempdate1 = new Date(d.getTime());
						d = tempdate1;
						ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						
						/************* Date 02 oct 2017 added by vijay for scheduling interval calculation with including precision using roundoff*********************/
						interval =(interval+temp)-tempvalue;
					    tempvalue = Math.round(interval);
					    /**************** ends here **********************************/

					    
					}else{
						
						/*** Date 02 oct 2017 added by vijay for interval calculation *************/
						int interval2 =(int) Math.round(interval);
						
						
						if (j > 0) {
							CalendarUtil.addDaysToDate(d, interval2);
							tempdate = new Date(d.getTime());
							
							/************* Date 02 oct 2017 added by vijay for scheduling interval including calculation using roundoff *********************/
							interval =(interval+temp)-tempvalue;
						    tempvalue = Math.round(interval);
						    /**************** ends here **********************************/
						}
						
						if (j == 0) {
							ssch.setScheduleServiceDate(servicedate);
						}
						if (j != 0) {
							if (productEndDate.before(d) == false) {
								ssch.setScheduleServiceDate(tempdate);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
							}
						}
					}
					

//					ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
					ssch.setServiceRemark(salesitemlis.get(i).getRemark());
					
					/**
					 * Date 01-04-2017 added by vijay for week number
					 */
					int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
					ssch.setWeekNo(weaknumber);
					
					/**
					 * ends here
					 */
					 /***
					  * nidhi |*|
					  * 7-01-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive && form.bomValidationActive 
							 && ssch.getServiceProductList()!=null){
						 ssch.setServiceProductList(ssch.getServiceProductList());
					 }
					 /**
					  * end
					  */
					serschelist.add(ssch);
				}
			}
			}
		
		}else{
		
//		int qty=0;
//		qty=(int) salesitemlis.get(i).getQty();
		
		for (int k = 0; k < qty; k++) {

			long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
			int noOfdays = salesitemlis.get(i).getDuration();
			
			/********* below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//			int interval = (int) (noOfdays / salesitemlis.get(i).getNumberOfServices());
			

			/*** Date 02 Oct 2017 below code added by vijay interval calculation with precision and using roundoff ******************/
			double days =  noOfdays;
			double noofservices= salesitemlis.get(i).getNumberOfServices();
			double interval =  days / noofservices;
			double temp = interval;
			double tempvalue=interval;
			/******************** ends here ********************/
			
			
			calculateEndDate(salesitemlis.get(i).getStartDate(),salesitemlis.get(i).getDuration());
			Date servicedate = salesitemlis.get(i).getStartDate();
			
			Date d = new Date(servicedate.getTime());
			Date tempdate = new Date();
			for (int j = 0; j < noServices; j++) {

				/*** Date 02 Oct 2017 added by vijay for interval calculation *************/
				int interval2 =(int) Math.round(interval);
				
				if (j > 0) {
					CalendarUtil.addDaysToDate(d, interval2);
					tempdate = new Date(d.getTime());
					
					/************* Date 02 Oct 2017 added by vijay *********************/
					interval =(interval+temp)-tempvalue;
				    tempvalue = Math.round(interval);
				    /************** ends here ************************/
				}

				ServiceSchedule ssch = new ServiceSchedule();

				// rohan added this 1 field
				System.out.println("In side method RRRRRRRRRRRRR"+ salesitemlis.get(i).getProductSrNo());
				ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());

				Date Stardaate = null;
				Stardaate = salesitemlis.get(i).getStartDate();
				ssch.setScheduleStartDate(Stardaate);
				ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
				ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
				ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
				ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
				ssch.setScheduleServiceNo(j + 1);
				ssch.setScheduleProdStartDate(Stardaate);

				ssch.setServicingBranch(form.olbbBranch.getValue());
				ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());

				String calculatedServiceTime = "";
				if (popuptablelist.size() != 0) {
					calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
				} else {
					calculatedServiceTime = "Flexible";
				}

				ssch.setScheduleServiceTime(calculatedServiceTime);

				if (form.customerbranchlist.size() == qty) {
					ssch.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());

					/**
					 * rohan added this code for automatic servicing branch
					 * setting for NBHC
					 */

					System.out.println("updateServices presenter cust Branch Name "+ form.customerbranchlist.get(k).getBusinessUnitName());
					System.out.println("servicing Branch "+ form.customerbranchlist.get(k).getBranch());
					if (form.customerbranchlist.get(k).getBranch() != null) {
						System.out.println("if servicing Branch "+ form.customerbranchlist.get(k).getBranch());
						ssch.setServicingBranch(form.customerbranchlist.get(k).getBranch());
					} else {
						System.out.println("else servicing Branch "+ form.customerbranchlist.get(k).getBranch());
						ssch.setServicingBranch(form.olbbBranch.getValue());
					}
					/**
					 * ends here
					 */

				} else if (form.customerbranchlist.size() == 0) {
					ssch.setScheduleProBranch("Service Address");

					/**
					 * rohan added this code for automatic servicing branch
					 * setting for NBHC
					 */
					ssch.setServicingBranch(form.olbbBranch.getValue());
					/**
					 * ends here
					 */

				} else {
					ssch.setScheduleProBranch("");
				}

				ssch.setScheduleProdEndDate(calculateEndDate(salesitemlis.get(i).getStartDate(), salesitemlis.get(i).getDuration()));
				if (j == 0) {
					ssch.setScheduleServiceDate(servicedate);
				}
				if (j != 0) {
					if (calculateEndDate(salesitemlis.get(i).getStartDate(),salesitemlis.get(i).getDuration()).before(d) == false) {
						ssch.setScheduleServiceDate(tempdate);
					} else {
						ssch.setScheduleServiceDate(calculateEndDate(salesitemlis.get(i).getStartDate(),salesitemlis.get(i).getDuration()));
					}
					
				}
				ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
				ssch.setServiceRemark(salesitemlis.get(i).getRemark());
				
				/**
				 * Date 01-04-2017 added by vijay for week number
				 */
				int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
				ssch.setWeekNo(weaknumber);
				
				/**
				 * ends here
				 */
				 /***
				  * nidhi |*|
				  * 7-01-2018
				  * for map bill product
				  * if any update please update in else conditions also
				  */
				 if(LoginPresenter.billofMaterialActive && form.bomValidationActive 
						 && ssch.getServiceProductList()!=null){
					 ssch.setServiceProductList(ssch.getServiceProductList());
				 }
				 /**
				  * end
				  */
				serschelist.add(ssch);
			}
		}
	}
		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
	}

}
		
	private Date calculateEndDate(Date servicedt,int noOfdays)
	{
		
		Date servicedate = servicedt;
		Date d=new Date(servicedate.getTime());
		Date productEndDate= new Date(servicedate.getTime());
		CalendarUtil.addDaysToDate(productEndDate, noOfdays);
		Date prodenddate=new Date(productEndDate.getTime());
		 productEndDate=prodenddate;
		 
		 return productEndDate;
		
	}		
		
		
//**********************changes ends here *************************			
			
	private void reactOnOk() {/*
		
		List<ServiceSchedule> prodservicelist=this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
		ArrayList<ServiceSchedule> prodserviceArr=new ArrayList<ServiceSchedule>();
		List<SalesLineItem> salesitemlis=form.saleslineitemquotationtable.getDataprovider().getList();
//		System.out.println("Start date  === "+AppUtility.calculateQuotationStartDate(salesitemlis));
		Date bigdate=new Date();
		Date smalldate=new Date();
		
		if(prodservicelist.size()!=0){
		
			
		if(serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex()!=0){
			form.f_serviceDay=serviceSchedulePopUp.getP_mondaytofriday().getItemText(serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex());
		}else{
			form.f_serviceDay="Not Select";
		}
		
			if((serviceSchedulePopUp.getP_servicehours().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_servicemin().getSelectedIndex()!=0&&serviceSchedulePopUp.getP_ampm().getSelectedIndex()!=0)
					||(serviceSchedulePopUp.getP_servicehours().getSelectedIndex()==0&&serviceSchedulePopUp.getP_servicemin().getSelectedIndex()==0&&serviceSchedulePopUp.getP_ampm().getSelectedIndex()==0)){
				
				int branchflag=0;
				
		for(int i=0;i<prodservicelist.size();i++){
			
			if(form.customerbranchlist.size()!=0){
			if(prodservicelist.get(i).getScheduleProBranch()==null||prodservicelist.get(i).getScheduleProBranch().equalsIgnoreCase("SELECT")){
				branchflag=1;
			}
			}	
			
			
			ServiceSchedule sss=new ServiceSchedule();
			
			sss.setSerSrNo(prodservicelist.get(i).getSerSrNo());
			sss.setScheduleStartDate(prodservicelist.get(i).getScheduleStartDate());
			sss.setScheduleProdId(prodservicelist.get(i).getScheduleProdId());
			sss.setScheduleProdName(prodservicelist.get(i).getScheduleProdName());
			sss.setScheduleDuration(prodservicelist.get(i).getScheduleDuration());
			sss.setScheduleNoOfServices(prodservicelist.get(i).getScheduleNoOfServices());
			sss.setScheduleServiceNo(prodservicelist.get(i).getScheduleServiceNo());
//			System.out.println("service date == "+prodservicelist.get(i).getService_Date());
			if(bigdate.before(prodservicelist.get(i).getScheduleServiceDate())){
				bigdate=prodservicelist.get(i).getScheduleServiceDate();
			}
			if(smalldate.after(prodservicelist.get(i).getScheduleServiceDate())){
				smalldate=prodservicelist.get(i).getScheduleServiceDate();
			}
			sss.setScheduleServiceDate(prodservicelist.get(i).getScheduleServiceDate());
			sss.setScheduleProdStartDate(prodservicelist.get(i).getScheduleProdStartDate());
			sss.setScheduleProdEndDate(prodservicelist.get(i).getScheduleProdEndDate());
			sss.setScheduleServiceTime(prodservicelist.get(i).getScheduleServiceTime());
			
			sss.setServicingBranch(prodservicelist.get(i).getServicingBranch());
			sss.setTotalServicingTime(prodservicelist.get(i).getTotalServicingTime());
			sss.setPremisesDetails(prodservicelist.get(i).getPremisesDetails());
			sss.setScheduleProBranch(prodservicelist.get(i).getScheduleProBranch());
			sss.setScheduleServiceDay(prodservicelist.get(i).getScheduleServiceDay());
			
			*//**
			 * Date 03-04-2017 added by vijay for week number
			 *//*
			sss.setWeekNo(prodservicelist.get(i).getWeekNo());
			*//**
			 * ends here
			 *//*
			prodserviceArr.add(sss);
			
		}
		Date d=new Date(bigdate.getTime());
//		CalendarUtil.addDaysToDate(d, -1);
		Date d1=new Date(smalldate.getTime());
		CalendarUtil.addDaysToDate(d1, 1);
		
		if(branchflag==0)
		{
			form.getServiceScheduleTable().getDataprovider().setList(prodserviceArr);
			schedulePanel.hide();
			
		}else{
			form.showDialogMessage("Please Set Branch By Clicking On Customize");
		}
		}else{
				
				form.showDialogMessage("Please Schedule Service Time Properly");
				
		}
	  }else{
		  form.showDialogMessage("Please Schedule Service Date");
	  }
		
	*/
		

		/**
		 * Date : 24-11-2017 BY ANIL
		 * setting the data from global list to service schedule table if any of filter is selected 
		 */
//		List<ServiceSchedule> popupSchSerList = this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
		List<ServiceSchedule> popupSchSerList=null;
		/*if(isAdvanceFilterSelected()&&form.globalScheduleServiceList!=null&&form.globalScheduleServiceList.size()!=0){
			popupSchSerList = new ArrayList<ServiceSchedule>(form.globalScheduleServiceList);
		}else{
			popupSchSerList = this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
		}*/
		/**
		 * End
		 */
		popupSchSerList = this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
		ArrayList<ServiceSchedule> updatedSchSerList = new ArrayList<ServiceSchedule>();
//		List<SalesLineItem> salesItemList = form.saleslineitemtable.getDataprovider().getList();
		Date bigdate = new Date();
		Date smalldate = new Date();

		if (popupSchSerList.size() != 0) {
			if (serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex() != 0) {
				form.f_serviceDay = serviceSchedulePopUp.getP_mondaytofriday().getItemText(serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex());
			} else {
				form.f_serviceDay = "Not Select";
			}

			if ((serviceSchedulePopUp.getP_servicehours().getSelectedIndex() != 0
					&& serviceSchedulePopUp.getP_servicemin().getSelectedIndex() != 0 
					&& serviceSchedulePopUp.getP_ampm().getSelectedIndex() != 0) || 
					(serviceSchedulePopUp.getP_servicehours().getSelectedIndex() == 0
						&& serviceSchedulePopUp.getP_servicemin().getSelectedIndex() == 0 
						&& serviceSchedulePopUp.getP_ampm().getSelectedIndex() == 0)) {

				int branchflag = 0;

				for (int i = 0; i < popupSchSerList.size(); i++) {
					if (form.customerbranchlist.size() != 0) {
						if (popupSchSerList.get(i).getScheduleProBranch() == null|| popupSchSerList.get(i).getScheduleProBranch().equalsIgnoreCase("SELECT")) {
							branchflag = 1;
						}
					}

					ServiceSchedule sss = new ServiceSchedule();
					// rohan added this 1 field
					sss.setSerSrNo(popupSchSerList.get(i).getSerSrNo());
					sss.setScheduleStartDate(popupSchSerList.get(i).getScheduleStartDate());
					sss.setScheduleProdId(popupSchSerList.get(i).getScheduleProdId());
					sss.setScheduleProdName(popupSchSerList.get(i).getScheduleProdName());
					sss.setScheduleDuration(popupSchSerList.get(i).getScheduleDuration());
					sss.setScheduleNoOfServices(popupSchSerList.get(i).getScheduleNoOfServices());
					sss.setScheduleServiceNo(popupSchSerList.get(i).getScheduleServiceNo());
					if (bigdate.before(popupSchSerList.get(i).getScheduleServiceDate())) {
						bigdate = popupSchSerList.get(i).getScheduleServiceDate();
					}
					if (smalldate.after(popupSchSerList.get(i).getScheduleServiceDate())) {
						smalldate = popupSchSerList.get(i).getScheduleServiceDate();
					}
					sss.setScheduleServiceDate(popupSchSerList.get(i).getScheduleServiceDate());
					sss.setScheduleProdStartDate(popupSchSerList.get(i).getScheduleProdStartDate());
					sss.setScheduleProdEndDate(popupSchSerList.get(i).getScheduleProdEndDate());
					sss.setScheduleServiceTime(popupSchSerList.get(i).getScheduleServiceTime());
					sss.setPremisesDetails(popupSchSerList.get(i).getPremisesDetails());
					sss.setServicingBranch(popupSchSerList.get(i).getServicingBranch());
					sss.setTotalServicingTime(popupSchSerList.get(i).getTotalServicingTime());
					if (popupSchSerList.get(i).getScheduleProBranch() != null) {
						sss.setScheduleProBranch(popupSchSerList.get(i).getScheduleProBranch());
					} else {
						sss.setScheduleProBranch("");
					}
					sss.setScheduleServiceDay(popupSchSerList.get(i).getScheduleServiceDay());
					//vijay
					sss.setServiceRemark(popupSchSerList.get(i).getServiceRemark());
					/**
					 * Date 03-04-2017 added by vijay for week number
					 */
					sss.setWeekNo(popupSchSerList.get(i).getWeekNo());
					/**
					 * ends here
					 */
//					sss.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable()
//							.getDataprovider().getList().get(i).getServiceProductList());
					
					 /***
					  * nidhi |*|
					  * 7-01-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive && form.bomValidationActive 
							 && popupSchSerList.get(i).getServiceProductList()!=null){
						 sss.setServiceProductList(popupSchSerList.get(i).getServiceProductList());
					 }
					 /**
					  * end
					  */
					updatedSchSerList.add(sss);
				}
				Date d = new Date(bigdate.getTime());
				Date d1 = new Date(smalldate.getTime());
				CalendarUtil.addDaysToDate(d1, 1);
				if (branchflag == 0) {
					form.getServiceScheduleTable().getDataprovider().setList(updatedSchSerList);
					schedulePanel.hide();
				} else {
					form.showDialogMessage("Please Set Branch By Clicking On Customize");
				}
			} else {
				form.showDialogMessage("Please Schedule Service Time Properly");
			}
		} else {
			form.showDialogMessage("Please Schedule Service Date");
		}

	
	
	}

		private void reactToService() 
		{
			if(form.tbQuotatinId.getValue()==null){
				serviceSchedulePopUp.getP_docid().setValue("");
			}else{
				serviceSchedulePopUp.getP_docid().setValue(form.tbQuotatinId.getValue()+"");
			}
			if(form.dbQuotationDate.getValue()==null){
				serviceSchedulePopUp.getP_docdate().setValue(null);
			}else{
				serviceSchedulePopUp.getP_docdate().setValue(form.dbQuotationDate.getValue());
			}
			
			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_customise().setValue(false);
			serviceSchedulePopUp.getP_interval().setValue(null);
			serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
			schedulePanel=new PopupPanel(true);
			// below setter added by vijay on 8-03-2017
			serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
			
			ScreeenState transactionStatus=AppMemory.getAppMemory().currentState;
			if(transactionStatus.toString().equals("VIEW")){
				serviceSchedulePopUp.getP_default().setEnabled(false);
				serviceSchedulePopUp.getP_mondaytofriday().setEnabled(false);
				serviceSchedulePopUp.getP_interval().setEnabled(false);
				serviceSchedulePopUp.getP_customise().setEnabled(false);
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
				serviceSchedulePopUp.getBtnReset().setEnabled(false);
				serviceSchedulePopUp.getP_servicehours().setEnabled(false);
				serviceSchedulePopUp.getP_servicemin().setEnabled(false);
				serviceSchedulePopUp.getP_ampm().setEnabled(false);
				// below setter added by vijay on 16-03-2017
				serviceSchedulePopUp.getP_serviceWeek().setEnabled(false);
			}
			if(transactionStatus.toString().equals("EDIT")){
				serviceSchedulePopUp.getP_default().setEnabled(true);
				serviceSchedulePopUp.getP_mondaytofriday().setEnabled(true);
				serviceSchedulePopUp.getP_interval().setEnabled(true);
				serviceSchedulePopUp.getP_customise().setEnabled(true);
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
				serviceSchedulePopUp.getBtnReset().setEnabled(true);
				serviceSchedulePopUp.getP_servicehours().setEnabled(true);
				serviceSchedulePopUp.getP_servicemin().setEnabled(true);
				serviceSchedulePopUp.getP_ampm().setEnabled(true);
				// below setter added by vijay on 16-03-2017
				serviceSchedulePopUp.getP_serviceWeek().setEnabled(true);
			}
			
			schedulePanel.add(serviceSchedulePopUp);
			schedulePanel.show();
			/*
			 * nidhi
			 * 05-08-2017
			 *  for make schedule popup responsive
			 *  style css class declared in SimpleERP.css
			 */
			schedulePanel.getElement().addClassName("schedulepopup");
			/*
			 * end
			 */
			schedulePanel.center();
		}
		
		private void reactOnMondaytoFridayList(int selectedIndex , int selectedServiceWeekIndex) {/*
			

			if(selectedIndex==0)
			{
				reactforTable();
				serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
			}
			else
			{
				specificday=1;
				
				List<SalesLineItem> salesitemlis=form.saleslineitemquotationtable.getDataprovider().getList();
				ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
				serschelist.clear();
				List<ServiceSchedule> popuptablelist=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				
				boolean flag =false;
				
				boolean sameMonthserviceflag = false;
				Date temp =null;
				
				
				for(int i=0;i<salesitemlis.size();i++){
					
					int qty=0;
					qty=(int) salesitemlis.get(i).getQty();
					for(int k=0;k < qty;k++){
				
					long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
					int noOfdays=salesitemlis.get(i).getDuration();
					
					*//********* Date 02 oct 2017 below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********//*
//					int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
					
					*//*** Date 02 oct 2017 added by vijay  for scheduling proper calculation with precision number
					 *  for interval  
					 *//*
					double nodays =  noOfdays;
					double noofservices= salesitemlis.get(i).getNumberOfServices();
					double interval =  nodays / noofservices;
					double tempinterval = interval;
					double tempvalue=interval;
					*//******************** ends here ********************//*
					
					
					Date servicedate=salesitemlis.get(i).getStartDate();
					Date d=new Date(servicedate.getTime());
					
					Date productEndDate= new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate=new Date(productEndDate.getTime());
					productEndDate=prodenddate;
					
					for(int j=0;j<noServices;j++){
			
						ServiceSchedule scheduleEntity=new ServiceSchedule();
						
						Date Stardaate=null;
						Stardaate=salesitemlis.get(i).getStartDate();
						
						scheduleEntity.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						scheduleEntity.setScheduleStartDate(Stardaate);
						scheduleEntity.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						scheduleEntity.setScheduleProdName(salesitemlis.get(i).getProductName());
						scheduleEntity.setScheduleDuration(salesitemlis.get(i).getDuration());
						scheduleEntity.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						scheduleEntity.setScheduleServiceNo(j+1);
						
						scheduleEntity.setScheduleProdStartDate(Stardaate);
						scheduleEntity.setScheduleProdEndDate(productEndDate);
						
//						scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
						scheduleEntity.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
						
						String calculatedServiceTime="";
						if(popuptablelist.size()!=0){
							 calculatedServiceTime=popuptablelist.get(j).getScheduleServiceTime();
							}else{
								calculatedServiceTime="Flexible";
							}
						scheduleEntity.setScheduleServiceTime(calculatedServiceTime);
						
						if(form.customerbranchlist.size()==qty){
							
							scheduleEntity.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
							
							*//**
							 * rohan added this code for automatic servicing branch setting for NBHC 
							 *//*
							
							System.out.println("updateServices presenter cust Branch Name "+form.customerbranchlist.get(k).getBusinessUnitName());
							System.out.println("servicing Branch "+form.customerbranchlist.get(k).getBranch());
							if(form.customerbranchlist.get(k).getBranch()!=null)
							{
								System.out.println("if servicing Branch "+form.customerbranchlist.get(k).getBranch());
								scheduleEntity.setServicingBranch(form.customerbranchlist.get(k).getBranch());
							}
							else
							{
								System.out.println("else servicing Branch "+form.customerbranchlist.get(k).getBranch());
								scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
							}
							*//**
							 * ends here 
							 *//*
							
						}else if(form.customerbranchlist.size()==0){
							scheduleEntity.setScheduleProBranch("Service Address");
							
							*//**
							 * rohan added this code for automatic servicing branch setting for NBHC 
							 *//*
							scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
							*//**
							 * ends here 
							 *//*	
							
						}else{
							scheduleEntity.setScheduleProBranch(null);
						}
						
						String dayOfWeek1 = format.format(d);
							int adate=Integer.parseInt(dayOfWeek1);
							int adday1=selectedIndex-1;
//							if(j==0){
								System.out.println("adate = "+adate);
								System.out.println("adday1 = "+adday1);
								int adday = 0;
								if(adate<=adday1){
									 adday=(adday1-adate);
								}else{
									adday=(7-adate+adday1);
								}
								
								Date newDate=new Date(d.getTime());
								
								CalendarUtil.addDaysToDate(newDate, adday);
//							}
//							if(j!=0){
//								int adday=(7-adate+selectedIndex)-1;
//								CalendarUtil.addDaysToDate(d, adday);
//								
//							}
							
							*//**
							 * Date 8-03-2017
							 * added by vijay for scheduling service with week day and week
							 *//*
							
							Date day = new Date(newDate.getTime());
							if(selectedServiceWeekIndex!=0){
								
								*//**
								 * Date 1 jun 2017 added by vijay for service getting same month 
								 *//*
								if(temp!=null){
									String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
									String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
									System.out.println("currentServiceMonth"+currentServiceMonth);
									System.out.println("lastserviceDateMonth"+lastserviceDateMonth);

									if(currentServiceMonth.equals(lastserviceDateMonth)){
										sameMonthserviceflag =true;
									}else{
										sameMonthserviceflag= false;
									}
								}
								System.out.println("sameMonthserviceflag"+sameMonthserviceflag);
								if(sameMonthserviceflag){
									CalendarUtil.addDaysToDate(day, 10);
									System.out.println("DAYYYYYYYYYYYY ==="+day);
								}
								*//**
								 * ends here
								 *//*
								
								CalendarUtil.setToFirstDayOfMonth(day);

								if(selectedServiceWeekIndex==1){
								 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
								if(day.before(Stardaate)){
									if(j==0){
										day =Stardaate;
									}
									else{
										flag = true;
									}
								}
								if(flag || j>0){
									CalendarUtil.setToFirstDayOfMonth(day);
									 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
								}
							 }
							else if(selectedServiceWeekIndex==2){
									
									CalendarUtil.addDaysToDate(day, 7);
									 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 7);
										 day = AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
								 
							}else if(selectedServiceWeekIndex==3){
									CalendarUtil.addDaysToDate(day, 14);

									 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 14);
										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
								 
							}
							else if(selectedServiceWeekIndex==4){
									CalendarUtil.addDaysToDate(day, 21);

									 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 21);
										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
								 
							     }
								
								temp = day;
								
								
								 *//**
								  * Date 26 jun 2017 added by vijay 
								  * for getting issues in 4 services and 6 services gap 
								  *//*
								 
								 	System.out.println("DDDDDDDDDD"+d);
									
									CalendarUtil.addDaysToDate(d, (int)interval);
									System.out.println("DDDDDDDDDD ============= "+d);

									int days =0;
									String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
									System.out.println("serviceStartDate"+serviceStartDate);
									if(serviceStartDate.equals("1")){
											
										String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
										System.out.println("currentServiceDay"+currentServiceDate);
										
										 days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
										System.out.println("VIJAY NEW DATE+++++++"+days);
									
										CalendarUtil.addDaysToDate(d, days);

									}
									
									*//**
									 * ends here
									 *//*
								
								
								
							}
							
						if(productEndDate.before(day)==false){
							scheduleEntity.setScheduleServiceDate(day);
							}else{
								scheduleEntity.setScheduleServiceDate(productEndDate);
							}
							
							*//*** Date 02 oct 2017 added by vijay for interval calculation *************//*
							int interval2 =(int) Math.round(interval);
						
							//Date 26 jun if condtion added by vijay for applicable only weeak is not selected
							if(selectedServiceWeekIndex==0){
								CalendarUtil.addDaysToDate(d, interval2);
							}
							//ends here
							
							Date tempdate=new Date(d.getTime());
							d=tempdate;
							scheduleEntity.setScheduleServiceDay(ContractForm.serviceDay(scheduleEntity.getScheduleServiceDate()));
							
							*//**
							 * Date 03-04-2017 added by vijay for week number
							 *//*
							int weeknumber = AppUtility.getweeknumber(scheduleEntity.getScheduleServiceDate());
							scheduleEntity.setWeekNo(weeknumber);
							
							*//**
							 * ends here
							 *//*
							
							*//************* Date 02 oct 2017 added by vijay for scheduling interval calculation with including precision using roundoff*********************//*
							interval =(interval+tempinterval)-tempvalue;
						    tempvalue = Math.round(interval);
						    *//**************** ends here **********************************//*;
						    
						  
							serschelist.add(scheduleEntity);
						
					}
				}
				
				serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
				}
			}
		*/
			
			
			Console.log("inside day wise scheduling");
			try {
				
				/**
				 * @author Vijay Date :-  07-09-2021
				 *  Des :- Service Scheduling with new single common method with new round up logic for innovative
				 */
				if(LoginPresenter.serviceScheduleNewLogicFlag){
					Console.log("in quotation Day wise selectedIndex" +selectedIndex);
					if (selectedIndex == 0) {
						reactforTable();
						serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
					} else {
						
						 Console.log("selectedIndex "+selectedIndex);
						 ContractForm contractform = new ContractForm();
						 List<SalesLineItem> salesitem = new ArrayList<SalesLineItem>();
						 salesitem = form.getSaleslineitemquotationtable().getDataprovider().getList();
						 List<ServiceSchedule> serschelist= contractform.getServiceSchedulelist(AppConstants.DAY,salesitem,form.dbQuotationDate.getValue(),form.customerbranchlist,form.olbbBranch.getValue(),0,selectedIndex,selectedServiceWeekIndex,0,serviceSchedulePopUp);
						 Console.log("serschelist size"+serschelist.size());
						 serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
					}
					return;
				}
				/**
				 * ends here
				 */
				
			} catch (Exception e) {
				Console.log(" inside method "+e.getMessage());
				Console.log(" inside method "+e.getCause());

			}
			
			
			if (selectedIndex == 0) {
				reactforTable();
				serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
			} else {
				boolean unitFlag= false, bomCheck = true;
				specificday = 1;
				List<SalesLineItem> salesitemlis = form.saleslineitemquotationtable.getDataprovider().getList();
				ArrayList<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();
				serschelist.clear();
				List<ServiceSchedule> popuptablelist = serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();

				boolean flag =false;
				boolean sameMonthserviceflag = false;
				Date temp =null;
				
				for (int i = 0; i < salesitemlis.size(); i++) {
//					int qty = 0;
//					qty = (int) salesitemlis.get(i).getQty();
					
					/**
					 * Date : 29-03-2017 By ANil
					 */
					boolean branchSchFlag=false;
					int qty=0;
					
					
					/**
					 * Date 02-05-2018 
					 * Developer : Vijay
					 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
					 * so i have updated below code using hashmap.
					 */
					
//					/**
//					 * Date : 03-04-2017 By ANIL
//					 */
////					ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(salesitemlis.get(i).getBranchSchedulingInfo());
//					
//					/**
//					 * Date : 12-06-2017 BY ANIl
//					 */
//					String[] branchArray=new String[10];
//					
//					if(salesitemlis.get(i).getBranchSchedulingInfo()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo().equals("")){
//						branchArray[0]=salesitemlis.get(i).getBranchSchedulingInfo();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo1()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo1().equals("")){
//						branchArray[1]=salesitemlis.get(i).getBranchSchedulingInfo1();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo2()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo2().equals("")){
//						branchArray[2]=salesitemlis.get(i).getBranchSchedulingInfo2();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo3()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo3().equals("")){
//						branchArray[3]=salesitemlis.get(i).getBranchSchedulingInfo3();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo4()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo4().equals("")){
//						branchArray[4]=salesitemlis.get(i).getBranchSchedulingInfo4();
//					}
//					
//					if(salesitemlis.get(i).getBranchSchedulingInfo5()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo5().equals("")){
//						branchArray[5]=salesitemlis.get(i).getBranchSchedulingInfo5();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo6()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo6().equals("")){
//						branchArray[6]=salesitemlis.get(i).getBranchSchedulingInfo6();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo7()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo7().equals("")){
//						branchArray[7]=salesitemlis.get(i).getBranchSchedulingInfo7();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo8()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo8().equals("")){
//						branchArray[8]=salesitemlis.get(i).getBranchSchedulingInfo8();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo9()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo9().equals("")){
//						branchArray[9]=salesitemlis.get(i).getBranchSchedulingInfo9();
//					}
//					/**
//					 * End
//					 */
//					ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
					
					/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
					ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
					
					/**
					 * ends here
					 */
					
					if(branchSchedulingList!=null
							&&branchSchedulingList.size()!=0){
						branchSchFlag=true;
						qty=branchSchedulingList.size();
					}else{
						qty=(int) salesitemlis.get(i).getQty();
					}
					
					/**
					 * End
					 */
					if(branchSchFlag){
						for (int k = 0; k < qty; k++) {
							if(branchSchedulingList.get(k).isCheck()==true){
							long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
							int noOfdays = salesitemlis.get(i).getDuration();
//							int interval = (int) (noOfdays / salesitemlis.get(i).getNumberOfServices());
							
							/*** Date 28 sep 2017 added by vijay  for scheduling proper calculation with precision number
							 *  for interval  
							 */
							double nodays =  noOfdays;
							double noofservices= salesitemlis.get(i).getNumberOfServices();
							double interval =  nodays / noofservices;
							double tempinterval = interval;
							double tempvalue=interval;
							/******************** ends here ********************/
							
							Date servicedate = salesitemlis.get(i).getStartDate();
							Date d = new Date(servicedate.getTime());
		
							Date productEndDate = new Date(servicedate.getTime());
							CalendarUtil.addDaysToDate(productEndDate, noOfdays);
							Date prodenddate = new Date(productEndDate.getTime());
							productEndDate = prodenddate;
		
							for (int j = 0; j < noServices; j++) {
		
								ServiceSchedule scheduleEntity = new ServiceSchedule();
		
								Date Stardaate = null;
								Stardaate = salesitemlis.get(i).getStartDate();
		
								scheduleEntity.setSerSrNo(salesitemlis.get(i).getProductSrNo());
								scheduleEntity.setScheduleStartDate(Stardaate);
								scheduleEntity.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
								scheduleEntity.setScheduleProdName(salesitemlis.get(i).getProductName());
								scheduleEntity.setScheduleDuration(salesitemlis.get(i).getDuration());
								scheduleEntity.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
								scheduleEntity.setScheduleServiceNo(j + 1);
		
								scheduleEntity.setScheduleProdStartDate(Stardaate);
								scheduleEntity.setScheduleProdEndDate(productEndDate);
		
								scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
								scheduleEntity.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
		
								String calculatedServiceTime = "";
								if (popuptablelist.size() != 0) {
									calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
								} else {
									calculatedServiceTime = "Flexible";
								}
		
								scheduleEntity.setScheduleServiceTime(calculatedServiceTime);
		
//								if (form.customerbranchlist.size() == qty) {
//									scheduleEntity.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
//									if (form.customerbranchlist.get(k).getBranch() != null) {
//										scheduleEntity.setServicingBranch(form.customerbranchlist.get(k).getBranch());
//									} else {
//										scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
//									}
//								} else if (form.customerbranchlist.size() == 0) {
//									scheduleEntity.setScheduleProBranch("Service Address");
//									scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
//								} else {
//									scheduleEntity.setScheduleProBranch(null);
//								}
								
								scheduleEntity.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
								if (branchSchedulingList.get(k).getServicingBranch()!=null) {
									scheduleEntity.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
								} 
		
								String dayOfWeek1 = format.format(d);
								int adate = Integer.parseInt(dayOfWeek1);
								int adday1 = selectedIndex - 1;
								
								int adday = 0;
								if (adate <= adday1) {
									adday = (adday1 - adate);
								} else {
									adday = (7 - adate + adday1);
								}
								Date newDate = new Date(d.getTime());
								CalendarUtil.addDaysToDate(newDate, adday);
		
								Date day = new Date(newDate.getTime());
								
								/**
								 * Date 8-03-2017
								 * added by vijay for scheduling service with week day and week
								 */
								
								if(selectedServiceWeekIndex!=0){
									
									/**
									 * Date 1 jun 2017 added by vijay for service getting same month 
									 */
									if(temp!=null){
										String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
										String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
										if(currentServiceMonth.equals(lastserviceDateMonth)){
											sameMonthserviceflag =true;
										}else{
											sameMonthserviceflag= false;
										}
									}
									
									if(sameMonthserviceflag){
										CalendarUtil.addDaysToDate(day, 10);
									}
									/**
									 * ends here
									 */
									
									CalendarUtil.setToFirstDayOfMonth(day);

									if(selectedServiceWeekIndex==1){
									 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
								 }
								else if(selectedServiceWeekIndex==2){
										CalendarUtil.addDaysToDate(day, 7);
										 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
										if(day.before(Stardaate)){
											if(j==0){
												day =Stardaate;
											}
											else{
												flag = true;
											}
										}
										if(flag || j>0){
											CalendarUtil.setToFirstDayOfMonth(day);
											CalendarUtil.addDaysToDate(day, 7);
											 day = AppUtility.getCalculatedDatewithWeek(day, adday1);;
										}
								}else if(selectedServiceWeekIndex==3){
										CalendarUtil.addDaysToDate(day, 14);
										 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
										if(day.before(Stardaate)){
											if(j==0){
												day =Stardaate;
											}
											else{
												flag = true;
											}
										}
										if(flag || j>0){
											CalendarUtil.setToFirstDayOfMonth(day);
											CalendarUtil.addDaysToDate(day, 14);
											 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
										}
								}
								else if(selectedServiceWeekIndex==4){
										CalendarUtil.addDaysToDate(day, 21);

										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
										if(day.before(Stardaate)){
											if(j==0){
												day =Stardaate;
											}
											else{
												flag = true;
											}
										}
										if(flag || j>0){
											CalendarUtil.setToFirstDayOfMonth(day);
											CalendarUtil.addDaysToDate(day, 21);
											 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
										}
								 }
									 temp = day;
									 
									 /**
									  * Date 26 jun 2017 added by vijay 
									  * for getting issues in 4 services and 6 services gap  with day selection
									  */
										
										CalendarUtil.addDaysToDate(d, (int)interval);
										int days =0;
										String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
										if(serviceStartDate.equals("1")){
											String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
											 days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
											CalendarUtil.addDaysToDate(d, days);

										}
										/**
										 * ends here
										 */
								}
								/**
								 * ends here
								 */
									
								if (productEndDate.before(day) == false) {
									scheduleEntity.setScheduleServiceDate(day);
								} else {
									scheduleEntity.setScheduleServiceDate(productEndDate);
								}
								
								/*** Date 28 sep 2017 added by vijay for interval calculation *************/
								int interval2 =(int) Math.round(interval);


								//Date 26 jun 2017 aaded by vijay this only when week is not selected
								if(selectedServiceWeekIndex==0){
									CalendarUtil.addDaysToDate(d, interval2);
								}  // ends here
								
								Date tempdate = new Date(d.getTime());
								d = tempdate;
								
								
								/************* Date 28 sep 2017 added by vijay for scheduling interval calculation with including precision using roundoff*********************/
								interval =(interval+tempinterval)-tempvalue;
							    tempvalue = Math.round(interval);
							    /**************** ends here **********************************/;
							    
							    
								scheduleEntity.setScheduleServiceDay(ContractForm.serviceDay(scheduleEntity.getScheduleServiceDate()));
								//  Vijay addd this code 
								scheduleEntity.setServiceRemark(salesitemlis.get(i).getRemark());
								/**
								 * Date 03-04-2017 added by vijay for week number
								 */
								int weeknumber = AppUtility.getweeknumber(scheduleEntity.getScheduleServiceDate());
								scheduleEntity.setWeekNo(weeknumber);
								
								/**
								 * ends here
								 */
								 /***
								   * nidhi |*|
								  * 7-01-2019
								  * for map bill product
								  * if any update please update in else conditions also
								  */
								 if(LoginPresenter.billofMaterialActive && form.bomValidationActive ){
									 
									 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
									 if(billPrDt!=null){
										 
										 
										 UnitConversionUtility unitConver = new UnitConversionUtility();

										 
										 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
											 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
											 seList.add(scheduleEntity);
											 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
											 if(seList!=null && seList.size()>0)
												 scheduleEntity = seList.get(0);
											 
										 }else{
											 unitFlag = true;
//											 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
										 }
										 
										 
										 
										 
									 }
								 }
								 /**
								  * end
								  */
								serschelist.add(scheduleEntity);
							}
						}
						}
					}else{
						for (int k = 0; k < qty; k++) {
							long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
							int noOfdays = salesitemlis.get(i).getDuration();
//							int interval = (int) (noOfdays / salesitemlis.get(i).getNumberOfServices());
							
							/*** Date 28 sep 2017 added by vijay  for scheduling proper calculation with precision number
							 *  for interval  
							 */
							double nodays =  noOfdays;
							double noofservices= salesitemlis.get(i).getNumberOfServices();
							double interval =  nodays / noofservices;
							double tempinterval = interval;
							double tempvalue=interval;
							/******************** ends here ********************/
							
							Date servicedate = salesitemlis.get(i).getStartDate();
							Date d = new Date(servicedate.getTime());
		
							Date productEndDate = new Date(servicedate.getTime());
							CalendarUtil.addDaysToDate(productEndDate, noOfdays);
							Date prodenddate = new Date(productEndDate.getTime());
							productEndDate = prodenddate;
		
							for (int j = 0; j < noServices; j++) {
		
								ServiceSchedule scheduleEntity = new ServiceSchedule();
		
								Date Stardaate = null;
								Stardaate = salesitemlis.get(i).getStartDate();
		
								scheduleEntity.setSerSrNo(salesitemlis.get(i).getProductSrNo());
		
								scheduleEntity.setScheduleStartDate(Stardaate);
								scheduleEntity.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
								scheduleEntity.setScheduleProdName(salesitemlis.get(i).getProductName());
								scheduleEntity.setScheduleDuration(salesitemlis.get(i).getDuration());
								scheduleEntity.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
								scheduleEntity.setScheduleServiceNo(j + 1);
		
								scheduleEntity.setScheduleProdStartDate(Stardaate);
								scheduleEntity.setScheduleProdEndDate(productEndDate);
		
								scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
								scheduleEntity.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
		
								String calculatedServiceTime = "";
								if (popuptablelist.size() != 0) {
									calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
								} else {
									calculatedServiceTime = "Flexible";
								}
		
								scheduleEntity.setScheduleServiceTime(calculatedServiceTime);
		
								if (form.customerbranchlist.size() == qty) {
									scheduleEntity.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
									/**
									 * rohan added this code for automatic servicing
									 * branch setting for NBHC
									 */
									if (form.customerbranchlist.get(k).getBranch() != null) {
										scheduleEntity.setServicingBranch(form.customerbranchlist.get(k).getBranch());
									} else {
										scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
									}
									/**
									 * ends here
									 */
								} else if (form.customerbranchlist.size() == 0) {
									scheduleEntity.setScheduleProBranch("Service Address");
									/**
									 * rohan added this code for automatic servicing
									 * branch setting for NBHC
									 */
									scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
									/**
									 * ends here
									 */
								} else {
									scheduleEntity.setScheduleProBranch(null);
								}
		
								String dayOfWeek1 = format.format(d);
								int adate = Integer.parseInt(dayOfWeek1);
								int adday1 = selectedIndex - 1;
								
								int adday = 0;
								if (adate <= adday1) {
									adday = (adday1 - adate);
								} else {
									adday = (7 - adate + adday1);
								}
								Date newDate = new Date(d.getTime());
								CalendarUtil.addDaysToDate(newDate, adday);
		
								Date day = new Date(newDate.getTime());
								
								/**
								 * Date 8-03-2017
								 * added by vijay for scheduling service with week day and week
								 */
								if(selectedServiceWeekIndex!=0){
									
									/**
									 * Date 1 jun 2017 added by vijay for service getting same month 
									 */
									if(temp!=null){
										String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
										String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
										if(currentServiceMonth.equals(lastserviceDateMonth)){
											sameMonthserviceflag =true;
										}else{
											sameMonthserviceflag= false;
										}
									}
									
									if(sameMonthserviceflag){
										CalendarUtil.addDaysToDate(day, 10);
									}
									/**
									 * ends here
									 */
									
									CalendarUtil.setToFirstDayOfMonth(day);
									if(selectedServiceWeekIndex==1){
									 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
								 }
								else if(selectedServiceWeekIndex==2){
										
										CalendarUtil.addDaysToDate(day, 7);

										 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
										if(day.before(Stardaate)){
											if(j==0){
												day =Stardaate;
											}
											else{
												flag = true;
											}
										}
										if(flag || j>0){
											CalendarUtil.setToFirstDayOfMonth(day);
											CalendarUtil.addDaysToDate(day, 7);
											 day = AppUtility.getCalculatedDatewithWeek(day, adday1);;
										}
								}else if(selectedServiceWeekIndex==3){
										CalendarUtil.addDaysToDate(day, 14);
										 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
										if(day.before(Stardaate)){
											if(j==0){
												day =Stardaate;
											}
											else{
												flag = true;
											}
										}
										if(flag || j>0){
											CalendarUtil.setToFirstDayOfMonth(day);
											CalendarUtil.addDaysToDate(day, 14);
											 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
										}
								}
								else if(selectedServiceWeekIndex==4){
										CalendarUtil.addDaysToDate(day, 21);

										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
										if(day.before(Stardaate)){
											if(j==0){
												day =Stardaate;
											}
											else{
												flag = true;
											}
										}
										if(flag || j>0){
											CalendarUtil.setToFirstDayOfMonth(day);
											CalendarUtil.addDaysToDate(day, 21);
											 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
										}
								     }
									
									 temp = day;
									 
									 /**
									  * Date 26 jun 2017 added by vijay 
									  * for getting issues in 4 services and 6 services gap 
									  */
										
										CalendarUtil.addDaysToDate(d, (int)interval);
										int days =0;
										String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
										if(serviceStartDate.equals("1")){
											String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
											 days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
											CalendarUtil.addDaysToDate(d, days);

										}
										
										/**
										 * ends here
										 */
								}
								/**
								 * ends here
								 */
								
								if (productEndDate.before(day) == false) {
									scheduleEntity.setScheduleServiceDate(day);
								} else {
									scheduleEntity.setScheduleServiceDate(productEndDate);
								}
								
								/*** Date 28 sep 2017 added by vijay for interval calculation *************/
								int interval2 =(int) Math.round(interval);
								
								//Date 26 jun if condtion added by vijay for applicable only weeak is not selected
								if(selectedServiceWeekIndex==0){
									CalendarUtil.addDaysToDate(d, interval2);
								}
								//ends here
								
								Date tempdate = new Date(d.getTime());
								d = tempdate;
								scheduleEntity.setScheduleServiceDay(ContractForm.serviceDay(scheduleEntity.getScheduleServiceDate()));
								//  Vijay addd this code 
								scheduleEntity.setServiceRemark(salesitemlis.get(i).getRemark());
								
								
								/************* Date 28 sep 2017 added by vijay for scheduling interval calculation with including precision using roundoff*********************/
								interval =(interval+tempinterval)-tempvalue;
							    tempvalue = Math.round(interval);
							    /**************** ends here **********************************/;
							    
							    
								
								/**
								 * Date 03-04-2017 added by vijay for week number
								 */
								int weeknumber = AppUtility.getweeknumber(scheduleEntity.getScheduleServiceDate());
								scheduleEntity.setWeekNo(weeknumber);
								
								/**
								 * ends here
								 */
								 /***
								  * nidhi |*|
								  * 7-01-2019
								  * for map bill product
								  * if any update please update in else conditions also
								  */
								 if(LoginPresenter.billofMaterialActive && form.bomValidationActive ){
									 
									 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
									 if(billPrDt!=null){
										 
										 UnitConversionUtility unitConver = new UnitConversionUtility();

										 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
											 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
											 seList.add(scheduleEntity);
											 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
											 if(seList!=null && seList.size()>0)
												 scheduleEntity = seList.get(0);
											 
										 }else{
											 unitFlag =true;
//											 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
										 }
									 }else{
										 bomCheck = false;
									 }
								 }
								 /**
								  * end
								  */
								serschelist.add(scheduleEntity);
							}
						}
					}

					serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
					/**
					 * Date : 25-11-2017 BY ANIL
					 */
//					form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
//					form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
					/**
					 * End
					 */
					if(unitFlag){
						 form.showDialogMessage("Unit of service area and product group detail area not mapped at branch level. Or conversion not available");
					}if(!bomCheck){
						form.showDialogMessage("Bill of material is not Created for this service please create first . \n you are not allow to save quotation.");
					}
				}
			}
		
		
		}
		
		/**
		 * Date : 21-06-2017
		 * Nidhi
		 * setting data on schedule popup 
		 */
		

		public static void setScheduleServiceOnSchedulingTable(ArrayList<ServiceSchedule> list){
			System.out.println("Get selected List  -- ");
			form.getServiceScheduleTable().connectToLocal();
			form.getServiceScheduleTable().getDataprovider().setList(list);
			/**
			 * nidhi |*|
			 * 8-1-2019
			 */
			form.getServiceMaterialBomList();
			/**
			 * end
			 */
		}
		/** End **/

		
private void reactOnHoursList() {/*

	List<ServiceSchedule> scheduleLis=this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
	ArrayList<ServiceSchedule> arrOfServices=new ArrayList<ServiceSchedule>();
	for(int i=0;i<scheduleLis.size();i++)
	{
		ServiceSchedule serviceScheduleEntity=new ServiceSchedule();
		
		String serviceHrs=serviceSchedulePopUp.getP_servicehours().getItemText(serviceSchedulePopUp.getP_servicehours().getSelectedIndex());
		String serviceMin=serviceSchedulePopUp.getP_servicemin().getItemText(serviceSchedulePopUp.getP_servicemin().getSelectedIndex());
		String serviceAmPm=serviceSchedulePopUp.getP_ampm().getItemText(serviceSchedulePopUp.getP_ampm().getSelectedIndex());
		String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
		System.out.println("Sout"+calculatedServiceTime);
		serviceScheduleEntity.setScheduleServiceTime(calculatedServiceTime);
		serviceScheduleEntity.setScheduleProdStartDate(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdStartDate());
		serviceScheduleEntity.setScheduleProdId(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdId());
		serviceScheduleEntity.setScheduleProdName(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdName());
		serviceScheduleEntity.setScheduleDuration(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleDuration());
		serviceScheduleEntity.setScheduleNoOfServices(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleNoOfServices());
		serviceScheduleEntity.setScheduleServiceNo(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleServiceNo());
		serviceScheduleEntity.setScheduleServiceDate(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleServiceDate());
		serviceScheduleEntity.setScheduleProdEndDate(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdEndDate());
		serviceScheduleEntity.setScheduleStartDate(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleStartDate());
		serviceScheduleEntity.setScheduleProBranch(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProBranch());
		serviceScheduleEntity.setScheduleServiceDay(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleServiceDay());
		serviceScheduleEntity.setServicingBranch(form.olbbBranch.getValue());
		serviceScheduleEntity.setTotalServicingTime(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().get(i).getTotalServicingTime());
		
		*//**
		 * Date 03-04-2017 added by vijay for week number
		 *//*
		serviceScheduleEntity.setWeekNo(serviceSchedulePopUp.getPopScheduleTable().
				getDataprovider().getList().get(i).getWeekNo());
		*//**
		 * ends here
		 *//*
		
		
		arrOfServices.add(serviceScheduleEntity);
	}
	serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(arrOfServices);
*/
	


	List<ServiceSchedule> scheduleLis = this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
	ArrayList<ServiceSchedule> arrOfServices = new ArrayList<ServiceSchedule>();
	for (int i = 0; i < scheduleLis.size(); i++) {
		ServiceSchedule serviceScheduleEntity = new ServiceSchedule();

		String serviceHrs = serviceSchedulePopUp.getP_servicehours().getItemText(serviceSchedulePopUp.getP_servicehours().getSelectedIndex());
		String serviceMin = serviceSchedulePopUp.getP_servicemin().getItemText(serviceSchedulePopUp.getP_servicemin().getSelectedIndex());
		String serviceAmPm = serviceSchedulePopUp.getP_ampm().getItemText(serviceSchedulePopUp.getP_ampm().getSelectedIndex());
		String calculatedServiceTime = serviceHrs + ":" + serviceMin + ""+ serviceAmPm;
		serviceScheduleEntity.setScheduleServiceTime(calculatedServiceTime);
		scheduleLis.get(i).setScheduleServiceTime(calculatedServiceTime);

		/***
		 * @author Vijay Date :- 20-09-2021 
		 * Des :- here just we need to update time below code not required its already exist in the list.
		 */
		
//		// rohan added this for
//		serviceScheduleEntity.setSerSrNo(serviceSchedulePopUp
//				.getPopScheduleTable().getDataprovider().getList().get(i).getSerSrNo());
//
//		serviceScheduleEntity.setScheduleProdStartDate(serviceSchedulePopUp
//				.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdStartDate());
//		serviceScheduleEntity.setScheduleProdId(serviceSchedulePopUp
//				.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdId());
//		serviceScheduleEntity.setScheduleProdName(serviceSchedulePopUp
//				.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdName());
//		serviceScheduleEntity.setScheduleDuration(serviceSchedulePopUp
//				.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleDuration());
//		serviceScheduleEntity.setScheduleNoOfServices(serviceSchedulePopUp
//				.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleNoOfServices());
//		serviceScheduleEntity.setScheduleServiceNo(serviceSchedulePopUp
//				.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleServiceNo());
//		serviceScheduleEntity.setScheduleServiceDate(serviceSchedulePopUp
//				.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleServiceDate());
//		serviceScheduleEntity.setScheduleProdEndDate(serviceSchedulePopUp
//				.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdEndDate());
//		serviceScheduleEntity.setScheduleStartDate(serviceSchedulePopUp
//				.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleStartDate());
//		serviceScheduleEntity.setScheduleProBranch(serviceSchedulePopUp
//				.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProBranch());
//		serviceScheduleEntity.setScheduleServiceDay(serviceSchedulePopUp
//				.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleServiceDay());
//		serviceScheduleEntity.setServicingBranch(form.olbbBranch.getValue());
//		serviceScheduleEntity.setTotalServicingTime(serviceSchedulePopUp
//				.getPopScheduleTable().getDataprovider().getList().get(i).getTotalServicingTime());
//		//  vijay aaded this 
//		serviceScheduleEntity.setServiceRemark(serviceSchedulePopUp.getPopScheduleTable().
//				getDataprovider().getList().get(i).getServiceRemark());
//		/**
//		 * Date 03-04-2017 added by vijay for week number
//		 */
//		serviceScheduleEntity.setWeekNo(serviceSchedulePopUp.getPopScheduleTable().
//				getDataprovider().getList().get(i).getWeekNo());
//		/**
//		 * ends here
//		 */
//		/**
//		 * nidhi 
//		 *  10-10-2018
//		 *  
//		 */
////		serviceScheduleEntity.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable()
////				.getDataprovider().getList().get(i).getServiceProductList());
//		 /***
//		  * nidhi |*|
//		  * 18-09-2018
//		  * for map bill product
//		  * if any update please update in else conditions also
//		  */
//		 if(LoginPresenter.billofMaterialActive && serviceSchedulePopUp.getPopScheduleTable().
//					getDataprovider().getList().get(i).getServiceProductList()!=null && form.bomValidationActive ){
//			 serviceScheduleEntity.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable().
//						getDataprovider().getList().get(i).getServiceProductList());
//		 }
//		 /**
//		  * end
//		  */
//		arrOfServices.add(serviceScheduleEntity);
	}
	serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(scheduleLis);


}
		protected void valueChangeMethodLogicForScheduling()
		{/*
			serviceSchedulePopUp.getP_customise().setValue(false);
		serviceSchedulePopUp.getP_default().setValue(false);
		serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
		
		serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
		serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
		serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
		
		
		serviceSchedulePopUp.getP_customise().setEnabled(true);
		serviceSchedulePopUp.getP_servicehours().setEnabled(true);
		serviceSchedulePopUp.getP_servicemin().setEnabled(true);
		serviceSchedulePopUp.getP_ampm().setEnabled(true);
		
		//04-04-2017 added by vijay 
		serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
		
		List<SalesLineItem> salesitemlis=form.saleslineitemquotationtable.getDataprovider().getList();
		ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
		serschelist.clear();
		List<ServiceSchedule> popuptablelist=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
		
		for(int i=0;i<salesitemlis.size();i++){
		
			//******************rohan changes here on 1/6/15 (changes i==k)****************
			int qty=0;
			qty=(int) salesitemlis.get(i).getQty();
			for(int k=0;k < qty;k++){
			
			
			long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
			int inreval=serviceSchedulePopUp.getP_interval().getValue();
			int noOfdays=salesitemlis.get(i).getDuration();
			Date servicedate=new Date(salesitemlis.get(i).getStartDate().getTime());
			Date d=new Date(servicedate.getTime());
			
			
			Date productEndDate= new Date(servicedate.getTime());
			CalendarUtil.addDaysToDate(productEndDate, noOfdays);
			Date prodenddate=new Date(productEndDate.getTime());
			productEndDate=prodenddate;
			Date tempdate=new Date();
			for(int j=0;j<noServices;j++){
	
				if(j>0)
				{
					CalendarUtil.addDaysToDate(d, inreval);
					tempdate=new Date(d.getTime());
				}
				
				
				ServiceSchedule ssch=new ServiceSchedule();
				Date Stardaate=null;
				Stardaate=salesitemlis.get(i).getStartDate();
				

				System.out.println("In side method RRRRRRRRRRRRR"+salesitemlis.get(i).getProductSrNo());
				ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
				
				ssch.setScheduleStartDate(Stardaate);
				ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
				ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
				ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
				ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
				ssch.setScheduleServiceNo(j+1);
				ssch.setScheduleProdStartDate(Stardaate);
				ssch.setScheduleProdEndDate(productEndDate);
//				ssch.setServicingBranch(form.olbbBranch.getValue());
				ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
				String calculatedServiceTime="";
				if(popuptablelist.size()!=0){
					 calculatedServiceTime=popuptablelist.get(j).getScheduleServiceTime();
					}else{
						calculatedServiceTime="Flexible";
					}
				ssch.setScheduleServiceTime(calculatedServiceTime);
				
				if(form.customerbranchlist.size()==qty){
					ssch.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
					
					*//**
					 * rohan added this code for automatic servicing branch setting for NBHC 
					 *//*
					
					System.out.println("updateServices presenter cust Branch Name "+form.customerbranchlist.get(k).getBusinessUnitName());
					System.out.println("servicing Branch "+form.customerbranchlist.get(k).getBranch());
					if(form.customerbranchlist.get(k).getBranch()!=null)
					{
						System.out.println("if servicing Branch "+form.customerbranchlist.get(k).getBranch());
						ssch.setServicingBranch(form.customerbranchlist.get(k).getBranch());
					}
					else
					{
						System.out.println("else servicing Branch "+form.customerbranchlist.get(k).getBranch());
						ssch.setServicingBranch(form.olbbBranch.getValue());
					}
					*//**
					 * ends here 
					 *//*
					
					
				}else if(form.customerbranchlist.size()==0){
					ssch.setScheduleProBranch("Service Address");
					
					*//**
					 * rohan added this code for automatic servicing branch setting for NBHC 
					 *//*
						ssch.setServicingBranch(form.olbbBranch.getValue());
					*//**
					 * ends here 
					 *//*	
					
				}else{
					ssch.setScheduleProBranch(null);
				}
				
				if(j==0){
						ssch.setScheduleServiceDate(servicedate);
						ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						serschelist.add(ssch);
					}
				if(j!=0){
				
					if(productEndDate.after(d)){
						ssch.setScheduleServiceDate(tempdate);
						
						ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						serschelist.add(ssch);
						
						
					}else{
						ssch.setScheduleServiceDate(productEndDate);
						ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						serschelist.add(ssch);
					}
				}
				
				*//**
				 * Date 03-04-2017 added by vijay for week number
				 *//*
				int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
				ssch.setWeekNo(weeknumber);
				
				*//**
				 * ends here
				 *//*
		  }
		}
		
		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
		}
		*/

			boolean unitFlag = false , bomCheck = true;
			
			serviceSchedulePopUp.getP_customise().setValue(false);
			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);

			serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);

			serviceSchedulePopUp.getP_servicehours().setEnabled(true);
			serviceSchedulePopUp.getP_servicemin().setEnabled(true);
			serviceSchedulePopUp.getP_ampm().setEnabled(true);
			serviceSchedulePopUp.getP_customise().setEnabled(true);

			//04-04-2017 added by vijay 
			serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
					
			int selectedExcludDay = serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex();
			
			/**
			 * @author Vijay Date :-  07-09-2021
			 *  Des :- Service Scheduling with new single common method with new round up logic for innovative
			 */
			if(LoginPresenter.serviceScheduleNewLogicFlag){
				int inreval = 0;
				if(serviceSchedulePopUp.getP_interval().getValue() != null && serviceSchedulePopUp.getP_interval().getValue() != 0){
					inreval =  serviceSchedulePopUp.getP_interval().getValue();
				}	
				if(serviceSchedulePopUp.getP_interval().getValue()==null){
					inreval = -1;
				}
				 List<SalesLineItem> salesitem = new ArrayList<SalesLineItem>();
				 salesitem = form.getSaleslineitemquotationtable().getDataprovider().getList();
				 ContractForm contractform = new ContractForm();
				 List<ServiceSchedule> serschelist=contractform.getServiceSchedulelist(AppConstants.INTERNAL,salesitem,form.dbQuotationDate.getValue(),form.customerbranchlist,form.olbbBranch.getValue(),selectedExcludDay,0,0,inreval,serviceSchedulePopUp);
				 serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
				return;
			}
			/**
			 * ends here
			 */
			
			List<SalesLineItem> salesitemlis = form.saleslineitemquotationtable.getDataprovider().getList();
			ArrayList<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();
			serschelist.clear();

			List<ServiceSchedule> popuptablelist = serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();

			for (int i = 0; i < salesitemlis.size(); i++) {


//				int qty = 0;
//				qty = (int) salesitemlis.get(i).getQty();
				
				/**
				 * Date : 29-03-2017 By ANil
				 */
				boolean branchSchFlag=false;
				int qty=0;
				
				/**
				 * Date 02-05-2018 
				 * Developer : Vijay
				 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
				 * so i have updated below code using hashmap. 
				 */
				
				
				/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
				ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
				
			/**
			 * ends here	
			 */
				if(branchSchedulingList!=null
						&&branchSchedulingList.size()!=0){
					branchSchFlag=true;
					qty=branchSchedulingList.size();
				}else{
					qty=(int) salesitemlis.get(i).getQty();
				}
				
				/**
				 * End
				 */
				if(branchSchFlag){
					for (int k = 0; k < qty; k++) {
						
						if(branchSchedulingList.get(k).isCheck()==true){
							
					/*	long noServices = (long) (item.getNumberOfServices());
						int noOfdays = item.getDuration();
						double nodays =  noOfdays;
						double noofservices= item.getNumberOfServices();
						int inreval = nodays / noofservices;
						if(serviceSchedulePopUp.getP_interval().getValue() != null || serviceSchedulePopUp.getP_interval().getValue() != 0){
							inreval =  serviceSchedulePopUp.getP_interval().getValue().doubleValue();
						}
						int noOfdays = salesitemlis.get(i).getDuration();*/

							
						long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
						int noOfdays = salesitemlis.get(i).getDuration();
						double nodays =  noOfdays;
						double noofservices= salesitemlis.get(i).getNumberOfServices();
//						int inreval = serviceSchedulePopUp.getP_interval().getValue();
						int inreval =  (int) (nodays / noofservices);
						if(serviceSchedulePopUp.getP_interval().getValue() != null && serviceSchedulePopUp.getP_interval().getValue() != 0){
							inreval =  serviceSchedulePopUp.getP_interval().getValue();
						}	
						Date servicedate = new Date(salesitemlis.get(i).getStartDate().getTime());
		
						Date productEndDate = new Date(servicedate.getTime());
						CalendarUtil.addDaysToDate(productEndDate, noOfdays);
						Date prodenddate = new Date(productEndDate.getTime());
						productEndDate = prodenddate;
						
						

						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
					/*	int startday = servicedate.getDay();
						
						if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(servicedate, 1);
						}
						Date d = new Date(servicedate.getTime());
						int lastday = productEndDate.getDay();
						
						if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(productEndDate, -1);
						}
						*/
						/**
						 * end
						 */
						
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						productEndDate = calculateEndDate(servicedate, noOfdays-1);
						int startday = servicedate.getDay();
						
						if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(servicedate, 1);
						}
//						Date d = new Date(servicedate.getTime());
						
						if(selectedExcludDay !=0 ){
							CalendarUtil.addDaysToDate(productEndDate, -1);
						}
						
						int lastday = productEndDate.getDay();
						
						if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(productEndDate, -1);
						}
						Date d = new Date(servicedate.getTime());
						/**
						 * end
						 */
						Date tempdate = new Date();
						tempdate = servicedate;
						for (int j = 0; j < noServices; j++) {
							/*if (j > 0) {
								CalendarUtil.addDaysToDate(d, inreval);
								tempdate = new Date(d.getTime());
							}*/
							

							if (j > 0) {
								CalendarUtil.addDaysToDate(d, inreval);
							}
		
							/**
							 * nidhi
							 *  ))..
							 *  for exclude day.
							 */
							int serDay = d.getDay();
							if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
								CalendarUtil.addDaysToDate(d, 1);
							}
							
							tempdate = new Date(d.getTime());
							
							ServiceSchedule ssch = new ServiceSchedule();
							Date Stardaate = null;
							
							ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
							Stardaate = salesitemlis.get(i).getStartDate();
							ssch.setScheduleStartDate(Stardaate);
							ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
							ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
							ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
							ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
							ssch.setScheduleServiceNo(j + 1);
							ssch.setScheduleProdStartDate(Stardaate);
							ssch.setScheduleProdEndDate(productEndDate);
							ssch.setServicingBranch(form.olbbBranch.getValue());
							ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
							String calculatedServiceTime = "";
							if (popuptablelist.size() != 0) {
								calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
							} else {
								calculatedServiceTime = "Flexible";
							}
		
							//vijay
							ssch.setServiceRemark(salesitemlis.get(i).getRemark());
							ssch.setScheduleServiceTime(calculatedServiceTime);
		
							
							ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
							if (branchSchedulingList.get(k).getServicingBranch()!=null) {
								ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
							} 
							
							
		
							if (j == 0) {
								ssch.setScheduleServiceDate(servicedate);
								ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
								serschelist.add(ssch);
							}
							if (j != 0) {
		
								if (productEndDate.after(d)) {
									ssch.setScheduleServiceDate(tempdate);
									ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
									serschelist.add(ssch);
								} else {
									ssch.setScheduleServiceDate(productEndDate);
									ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
									serschelist.add(ssch);
								}
							}
							/**
							 * Date 03-04-2017 added by vijay for week number
							 */
							int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
							ssch.setWeekNo(weeknumber);
							 /***
							  * nidhi |*|
							  * 7-01-2019
							  * for map bill product
							  * if any update please update in else conditions also
							  */
							 if(LoginPresenter.billofMaterialActive && form.bomValidationActive ){
								 
								 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
								 if(billPrDt!=null){
									 
									 UnitConversionUtility unitConver = new UnitConversionUtility();

									 
									 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
										 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
										 seList.add(ssch);
										 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
										 if(seList!=null && seList.size()>0)
											 ssch = seList.get(0);
										 
									 }else{
										 unitFlag =true;
//										 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
									 }
									 
								 }else{
									 bomCheck = false;
								 }
							 }
							 /**
							  * end
							  */
							/**
							 * ends here
							 */
						}
					}
					}
					
				}else{
					for (int k = 0; k < qty; k++) {
						long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
						int noOfdays = salesitemlis.get(i).getDuration();
						double nodays =  noOfdays;
						double noofservices= salesitemlis.get(i).getNumberOfServices();
						int inreval =  (int) (nodays / noofservices);
						if(serviceSchedulePopUp.getP_interval().getValue() != null && serviceSchedulePopUp.getP_interval().getValue() != 0){
							inreval =  serviceSchedulePopUp.getP_interval().getValue();
						}	
						Date servicedate = new Date(salesitemlis.get(i).getStartDate().getTime());
						Date d = new Date(servicedate.getTime());
		
						Date productEndDate = new Date(servicedate.getTime());
						CalendarUtil.addDaysToDate(productEndDate, noOfdays);
						Date prodenddate = new Date(productEndDate.getTime());
						productEndDate = prodenddate;
						
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						int startday = servicedate.getDay();
						
						if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(servicedate, 1);
						}
						int lastday = productEndDate.getDay();
						
						if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(productEndDate, -1);
						}
						/**
						 * end
						 */
						Date tempdate = new Date();
						tempdate = servicedate;
						for (int j = 0; j < noServices; j++) {
		
							
							
							if (j > 0) {
								CalendarUtil.addDaysToDate(d, inreval);
							}
		
							/**
							 * nidhi
							 *  ))..
							 *  for exclude day.
							 */
							int serDay = d.getDay();
							if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
								CalendarUtil.addDaysToDate(d, 1);
							
							}
							
							tempdate = new Date(d.getTime());
							/**
							 * end
							 */
							
							ServiceSchedule ssch = new ServiceSchedule();
							Date Stardaate = null;
							
							ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
							Stardaate = salesitemlis.get(i).getStartDate();
							ssch.setScheduleStartDate(Stardaate);
							ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
							ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
							ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
							ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
							ssch.setScheduleServiceNo(j + 1);
							ssch.setScheduleProdStartDate(Stardaate);
							ssch.setScheduleProdEndDate(prodenddate);
							ssch.setServicingBranch(form.olbbBranch.getValue());
							ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
							String calculatedServiceTime = "";
							if (popuptablelist.size() != 0) {
								calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
							} else {
								calculatedServiceTime = "Flexible";
							}
		
							//vijay
							ssch.setServiceRemark(salesitemlis.get(i).getRemark());
							
							ssch.setScheduleServiceTime(calculatedServiceTime);
		
							if (form.customerbranchlist.size() == qty) {
								ssch.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
								/**
								 * rohan added this code for automatic servicing branch
								 * setting for NBHC
								 */
								if (form.customerbranchlist.get(k).getBranch() != null) {
									ssch.setServicingBranch(form.customerbranchlist.get(k).getBranch());
								} else {
									ssch.setServicingBranch(form.olbbBranch.getValue());
								}
								/**
								 * ends here
								 */
		
							} else if (form.customerbranchlist.size() == 0) {
								ssch.setScheduleProBranch("Service Address");
		
								/**
								 * rohan added this code for automatic servicing branch
								 * setting for NBHC
								 */
								ssch.setServicingBranch(form.olbbBranch.getValue());
								/**
								 * ends here
								 */
		
							} else {
								ssch.setScheduleProBranch(null);
							}
						
							
		
							if (j == 0) {
								ssch.setScheduleServiceDate(servicedate);
								ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
							}
							if (j != 0) {
		
								if (productEndDate.after(d)) {
									ssch.setScheduleServiceDate(tempdate);
									ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
								} else {
									ssch.setScheduleServiceDate(productEndDate);
									ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
									
								}
							}
							
							/**
							 * Date 03-04-2017 added by vijay for week number
							 */
							int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
							ssch.setWeekNo(weeknumber);
							 /***
							  * nidhi |*|
							  * 7-01-2019
							  * for map bill product
							  * if any update please update in else conditions also
							  */
							 if(LoginPresenter.billofMaterialActive && form.bomValidationActive ){
								 
								 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
								 if(billPrDt!=null){
									 
									 UnitConversionUtility unitConver = new UnitConversionUtility();

									 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
										 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
										 seList.add(ssch);
										 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
										 if(seList!=null && seList.size()>0)
											 ssch = seList.get(0);
										 
									 }else{
										 unitFlag =true;
//										 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
									 }
									 
								 }else{
									 bomCheck = false;
								 }
							 }
							 /**
							  * end
							  */
									/**
							 * ends here
							 */
							 serschelist.add(ssch);
						}
					}
			}

				serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
				/**
				 * nidhi |*| 10-1-2019
				 */
				if(unitFlag){
					 form.showDialogMessage("Unit of service area and product group detail area not mapped at branch level. Or conversion not available");
				}if(!bomCheck){
					form.showDialogMessage("Bill of material is not Created for this service please create first . \n you are not allow to save quotation.");
				}
				/**
				 * Date : 25-11-2017 BY ANIL
				 */
//				form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
//				form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				/**
				 * End
				 */
			}
		
		
		
	
		
		}
		
private void reactFordeleteproduct(int proid, int srNo) {
			
			int productid=proid;
			if(form.getSaleslineitemquotationtable().getDataprovider().getList().size()!=0)
			{
				System.out.println("Before Updare Services Method");
				updateServices(productid,srNo);
			}
		}
		
protected void updateServices(int changeproductid, int srNo){
	
	System.out.println("PRODUCT ID "+changeproductid+" SR NO. "+srNo);

	List<ServiceSchedule> origserviceitemlist = form.serviceScheduleTable.getDataprovider().getList();
	List<Boolean> serLis = new ArrayList<Boolean>();
	List<ServiceSchedule> newlist = new ArrayList<ServiceSchedule>();
	newlist.clear();

	for (int j = 0; j < origserviceitemlist.size(); j++) {
		boolean prodIdCheck = findProductId(origserviceitemlist.get(j)
				.getScheduleProdId(), origserviceitemlist.get(j).getSerSrNo());
		if (prodIdCheck == false) {
			serLis.add(false);
		} else {
			serLis.add(true);
		}
	}

	form.serviceScheduleTable.connectToLocal();
	boolean unitFlag = false ,bomCheck = true;
	for (int k = 0; k < serLis.size(); k++) {

		System.out.println("boolean loop " + serLis.get(k));
		if (serLis.get(k)) {
//			System.out.println(" conditin for dele product");
//			System.out.println(" chnge product" + changeproductid);
//			System.out.println(" form pooup table product"
//					+ origserviceitemlist.get(k).getScheduleProdId());
			//
			// old code
			// if(changeproductid!=origserviceitemlist.get(k).getScheduleProdId()
			// && srNo != origserviceitemlist.get(k).getSerSrNo()){

			// new code by for rohan for same product add in contract
			if (srNo != origserviceitemlist.get(k).getSerSrNo()) {

				System.out.println(" conditin for dele product 11");

				ServiceSchedule serviceScheduleObj = new ServiceSchedule();

				serviceScheduleObj.setSerSrNo(origserviceitemlist.get(k)
						.getSerSrNo());

				serviceScheduleObj.setScheduleStartDate(origserviceitemlist
						.get(k).getScheduleStartDate());
				serviceScheduleObj.setScheduleProdId(origserviceitemlist
						.get(k).getScheduleProdId());
				serviceScheduleObj.setScheduleProdName(origserviceitemlist
						.get(k).getScheduleProdName());
				serviceScheduleObj.setScheduleDuration(origserviceitemlist
						.get(k).getScheduleDuration());
				serviceScheduleObj
						.setScheduleNoOfServices(origserviceitemlist.get(k)
								.getScheduleNoOfServices());
				serviceScheduleObj.setScheduleServiceNo(origserviceitemlist
						.get(k).getScheduleServiceNo());
				serviceScheduleObj
						.setScheduleServiceDate(origserviceitemlist.get(k)
								.getScheduleServiceDate());

				if (origserviceitemlist.get(k).getScheduleProdEndDate() != null) {
					serviceScheduleObj
							.setScheduleProdEndDate(origserviceitemlist
									.get(k).getScheduleProdEndDate());
				} else {
					serviceScheduleObj.setScheduleProdEndDate(new Date());
				}

				if (origserviceitemlist.get(k).getScheduleProdStartDate() != null) {
					serviceScheduleObj
							.setScheduleProdStartDate(origserviceitemlist
									.get(k).getScheduleProdStartDate());
				} else {
					serviceScheduleObj.setScheduleProdStartDate(new Date());
				}
				serviceScheduleObj
						.setScheduleServiceTime(origserviceitemlist.get(k)
								.getScheduleServiceTime());
				serviceScheduleObj.setPremisesDetails(origserviceitemlist
						.get(k).getPremisesDetails());
				serviceScheduleObj.setScheduleProBranch(origserviceitemlist
						.get(k).getScheduleProBranch());
				serviceScheduleObj
						.setScheduleServiceDay(origserviceitemlist.get(k)
								.getScheduleServiceDay());
				serviceScheduleObj.setServicingBranch(origserviceitemlist
						.get(k).getServicingBranch());
				serviceScheduleObj
						.setTotalServicingTime(origserviceitemlist.get(k)
								.getTotalServicingTime());
				// vijay
				serviceScheduleObj.setServiceRemark(origserviceitemlist
						.get(k).getServiceRemark());

				/**
				 * Date 03-04-2017 added by vijay for week number
				 */
				int weeknumber = AppUtility.getweeknumber(serviceScheduleObj.getScheduleServiceDate());
				serviceScheduleObj.setWeekNo(weeknumber);
				
				/**
				 * ends here
				 */
				 /***
				  * nidhi *:*:*
				  * 18-09-2018
				  * for map bill product
				  * if any update please update in else conditions also
				  */
				 if(LoginPresenter.billofMaterialActive && serviceScheduleObj.getServiceProductList()!=null
						 && form.bomValidationActive ){
					 serviceScheduleObj.setServiceProductList(serviceScheduleObj.getServiceProductList());
				 }
				 /**
				  * end
				  */
				 
				newlist.add(serviceScheduleObj);
			}
		}
	}

	List<SalesLineItem> formLis = form.getSaleslineitemquotationtable().getDataprovider().getList();

	for (int i = 0; i < formLis.size(); i++) {

		if (changeproductid == formLis.get(i).getPrduct().getCount()
				&& srNo == formLis.get(i).getProductSrNo()) {
			System.out.println("CHANGED PRODUCT "+changeproductid+" SR "+srNo);
			/**
			 * Date : 29-03-2017 By ANil
			 */
			boolean branchSchFlag=false;
			
			/**
			 * @author Vijay Date :- 16-09-2021
			 * Des :- Updated service scheduling with new logic and single common method with process config
			 */
			if(LoginPresenter.serviceScheduleNewLogicFlag){
				 List<SalesLineItem> salesitem = new ArrayList<SalesLineItem>();
				 salesitem.add(formLis.get(i));
				 ContractForm contractform = new ContractForm();
				 List<ServiceSchedule> serschelist=contractform.getServiceSchedulelist("Default",salesitem,form.dbQuotationDate.getValue(),form.customerbranchlist,form.olbbBranch.getValue(),0,0,0,0,serviceSchedulePopUp);
				 System.out.println("serschelist size "+serschelist.size());
				 newlist.addAll(serschelist); 
				 form.serviceScheduleTable.getDataprovider().setList(newlist);
				 return;
			}
			
			int qty = 0;
//			qty = (int) formLis.get(i).getQty();
			
			/**
			 * Date 02-05-2018 
			 * Developer : Vijay
			 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
			 * so i have updated below code using hashmap. 
			 */
			
//			/**
//			 * Date : 03-04-2017 By ANIL
//			 */
////			ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(formLis.get(i).getBranchSchedulingInfo());
//			
//			/**
//			 * Date : 12-06-2017 BY ANIL
//			 */
//			String[] branchArray=new String[10];
//			if(formLis.get(i).getBranchSchedulingInfo()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo().equals("")){
//				branchArray[0]=formLis.get(i).getBranchSchedulingInfo();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo1()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo1().equals("")){
//				branchArray[1]=formLis.get(i).getBranchSchedulingInfo1();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo2()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo2().equals("")){
//				branchArray[2]=formLis.get(i).getBranchSchedulingInfo2();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo3()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo3().equals("")){
//				branchArray[3]=formLis.get(i).getBranchSchedulingInfo3();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo4()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo4().equals("")){
//				branchArray[4]=formLis.get(i).getBranchSchedulingInfo4();
//			}
//			
//			if(formLis.get(i).getBranchSchedulingInfo5()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo5().equals("")){
//				branchArray[5]=formLis.get(i).getBranchSchedulingInfo5();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo6()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo6().equals("")){
//				branchArray[6]=formLis.get(i).getBranchSchedulingInfo6();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo7()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo7().equals("")){
//				branchArray[7]=formLis.get(i).getBranchSchedulingInfo7();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo8()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo8().equals("")){
//				branchArray[8]=formLis.get(i).getBranchSchedulingInfo8();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo9()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo9().equals("")){
//				branchArray[9]=formLis.get(i).getBranchSchedulingInfo9();
//			}
//			
//			/**
//			 * End
//			 */
//			ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
			
			/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
			ArrayList<BranchWiseScheduling> branchSchedulingList = null;
			if(formLis.get(i).getCustomerBranchSchedulingInfo()!=null)
			 branchSchedulingList = formLis.get(i).getCustomerBranchSchedulingInfo().get(formLis.get(i).getProductSrNo());
		
			/**
			 * ends here	
			 */
			
			if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
				branchSchFlag=true;
				qty=branchSchedulingList.size();
			}else{
				qty=(int) formLis.get(i).getQty();
			}
			
			/**
			 * End
			 */
			if(branchSchFlag){
			for (int k = 0; k < qty; k++) {
				if(branchSchedulingList.get(k).isCheck()==true){

				System.out.println("condition == ");

				long noServices = (long) (formLis.get(i).getNumberOfServices());
				int noOfdays = formLis.get(i).getDuration();
				
				/********* below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//				int interval = (int) (noOfdays / formLis.get(i).getNumberOfServices());
				
				/*** Date 28 sep 2017 added by vijay  for scheduling proper calculation with precision number
				 *  for interval  
				 */
				double days =  noOfdays;
				double noofservices= formLis.get(i).getNumberOfServices();
				double interval =  days / noofservices;
				System.out.println(Math.round(days/noofservices));
				System.out.println("interval "+interval);
				double temp = interval;
				double tempvalue=interval;
				/******************** ends here ********************/

				
				Date servicedate = formLis.get(i).getStartDate();
				Date d = new Date(servicedate.getTime());
				Date productEndDate = new Date(servicedate.getTime());
				CalendarUtil.addDaysToDate(productEndDate, noOfdays);
				Date prodenddate = new Date(productEndDate.getTime());
				productEndDate = prodenddate;
				Date tempdate = new Date();
				for (int j = 0; j < noServices; j++) {
					/**
					 * Date :09/06/2017   by Ajinkya 
					 * Nbhc Scheduling Issue 
					 */
//					if (j > 0) {
//						CalendarUtil.addDaysToDate(d, interval);
//						tempdate = new Date(d.getTime());
//					}
					
					/**
					 *  End Here
					 */

					ServiceSchedule ssch = new ServiceSchedule();
					Date Stardaate = formLis.get(i).getStartDate();

					ssch.setSerSrNo(formLis.get(i).getProductSrNo());

					ssch.setScheduleStartDate(Stardaate);
					ssch.setScheduleProdId(formLis.get(i).getPrduct()
							.getCount());
					ssch.setScheduleProdName(formLis.get(i)
							.getProductName());
					ssch.setScheduleDuration(formLis.get(i).getDuration());
					ssch.setScheduleNoOfServices(formLis.get(i)
							.getNumberOfServices());
					ssch.setScheduleServiceNo(j + 1);
					ssch.setScheduleProdStartDate(Stardaate);
					ssch.setScheduleProdEndDate(productEndDate);
					ssch.setScheduleServiceTime("Flexible");

					ssch.setServicingBranch(form.olbbBranch.getValue());
					ssch.setTotalServicingTime(formLis.get(i).getServicingTIme());

					ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
					if (branchSchedulingList.get(k).getServicingBranch()!=null) {
						ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
					} 
					
					int dayIndex=AppUtility.getDayIndex(branchSchedulingList.get(k).getDay());
					
					if(dayIndex!=0){
						String dayOfWeek1 = format.format(d);
						int adate = Integer.parseInt(dayOfWeek1);
						int adday1 = dayIndex - 1;
						int adday = 0;
						if (adate <= adday1) {
							adday = (adday1 - adate);
						} else {
							adday = (7 - adate + adday1);
						}
						
						Date newDate = new Date(d.getTime());
						CalendarUtil.addDaysToDate(newDate, adday);
						Date day = new Date(newDate.getTime());

						if (productEndDate.before(day) == false) {
							ssch.setScheduleServiceDate(day);
						} else {
							ssch.setScheduleServiceDate(productEndDate);
						}
						
						/*** added by vijay for interval calculation *************/
						int interval2 =(int) Math.round(interval);


						CalendarUtil.addDaysToDate(d, interval2);
						Date tempdate1 = new Date(d.getTime());
						d = tempdate1;
						ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						
						/************* added by vijay for scheduling interval calculation with including precision using roundoff*********************/
						interval =(interval+temp)-tempvalue;
					    tempvalue = Math.round(interval);
					    /**************** ends here **********************************/

					    
					}else{
						
						/*** added by vijay for interval calculation *************/
						int interval2 =(int) Math.round(interval);
						
						if (j > 0) {
							CalendarUtil.addDaysToDate(d, interval2);
							tempdate = new Date(d.getTime());
							
							/************* added by vijay for scheduling interval including calculation using roundoff *********************/
							interval =(interval+temp)-tempvalue;
						    tempvalue = Math.round(interval);
						    /**************** ends here **********************************/

						}
						
						if (j == 0) {
							ssch.setScheduleServiceDate(servicedate);
						}
						if (j != 0) {
							if (productEndDate.before(d) == false) {
								ssch.setScheduleServiceDate(tempdate);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
							}
						}
					}

					

					ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
					ssch.setServiceRemark(formLis.get(i).getRemark());
					/**
					 * Date 01-04-2017 added by vijay for week number
					 */
					int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
					ssch.setWeekNo(weeknumber);
					
					/**
					 * ends here
					 */
					 /***
					  * nidhi *:*:*
					  * 18-09-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive && form.bomValidationActive ){
						 
						 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(formLis.get(i).getPrduct());
						 if(billPrDt!=null){
							 
							 UnitConversionUtility unitConver = new UnitConversionUtility();

							 
							 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
								 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
								 seList.add(ssch);
								 seList = unitConver.getServiceitemProList(seList,formLis.get(i).getPrduct(),formLis.get(i),billPrDt);
								 if(seList!=null && seList.size()>0)
									 ssch = seList.get(0);
								 
							 }else{
								 unitFlag = true;
//								 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
							 }
							 
						 }else{
							 bomCheck = false;
						 }
					 }
					 /**
					  * end
					  */
					newlist.add(ssch);
				}
			}

			}
			}
			else{
				
				for (int k = 0; k < qty; k++) {
					
					long noServices = (long) (formLis.get(i).getNumberOfServices());
					int noOfdays = formLis.get(i).getDuration();
					/********* below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//					int interval = (int) (noOfdays / formLis.get(i).getNumberOfServices());
					
					/*** Date 28 sep 2017 below code added by vijay interval calculation with precision and using roundoff ******************/
					double days =  noOfdays;
					double noofservices= formLis.get(i).getNumberOfServices();
					double interval =  days / noofservices;
					System.out.println(Math.round(days/noofservices));
					System.out.println("interval "+interval);
					double temp = interval;
					double tempvalue=interval;
					/******************** ends here ********************/
					
					Date servicedate = formLis.get(i).getStartDate();
					Date d = new Date(servicedate.getTime());
					Date productEndDate = new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate = new Date(productEndDate.getTime());
					productEndDate = prodenddate;
					Date tempdate = new Date();
					
					for (int j = 0; j < noServices; j++) {
						
						/*** added by vijay for interval calculation *************/
						int interval2 =(int) Math.round(interval);
						
						if (j > 0) {
							CalendarUtil.addDaysToDate(d, interval2);
							tempdate = new Date(d.getTime());
							
							/************* added by vijay *********************/
							interval =(interval+temp)-tempvalue;
						    tempvalue = Math.round(interval);
						    /************** ends here ************************/
						}

						ServiceSchedule ssch = new ServiceSchedule();
						Date Stardaate = formLis.get(i).getStartDate();

						ssch.setSerSrNo(formLis.get(i).getProductSrNo());
						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(formLis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(formLis.get(i).getProductName());
						ssch.setScheduleDuration(formLis.get(i).getDuration());
						ssch.setScheduleNoOfServices(formLis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(j + 1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(productEndDate);
						ssch.setScheduleServiceTime("Flexible");

						ssch.setServicingBranch(form.olbbBranch.getValue());
						ssch.setTotalServicingTime(formLis.get(i).getServicingTIme());

						if (j == 0) {
							ssch.setScheduleServiceDate(servicedate);
						}
						if (j != 0) {
							if (productEndDate.before(d) == false) {
								ssch.setScheduleServiceDate(tempdate);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
							}
						}
						
						if (form.customerbranchlist.size() == qty) {
							String branchName = form.customerbranchlist.get(k).getBusinessUnitName();
							ssch.setScheduleProBranch(branchName);

							/**
							 * rohan added this code for automatic servicing
							 * branch setting for NBHC
							 */

							if (form.customerbranchlist.get(k).getBranch() != null) {
								ssch.setServicingBranch(form.customerbranchlist.get(k).getBranch());
							} else {
								ssch.setServicingBranch(form.olbbBranch.getValue());
							}
							/**
							 * ends here
							 */

						} else if (form.customerbranchlist.size() == 0) {
							ssch.setScheduleProBranch("Service Address");

							/**
							 * rohan added this code for automatic servicing
							 * branch setting for NBHC
							 */
							ssch.setServicingBranch(form.olbbBranch.getValue());
							/**
							 * ends here
							 */

						} else {
							ssch.setScheduleProBranch(null);
						}

						ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						ssch.setServiceRemark(formLis.get(i).getRemark());
						/**
						 * Date 01-04-2017 added by vijay for week number
						 */
						int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
						ssch.setWeekNo(weeknumber);
						
						/**
						 * ends here
						 */
						 /***
						  * nidhi *:*:*
						  * 18-09-2018
						  * for map bill product
						  * if any update please update in else conditions also
						  */
						 if(LoginPresenter.billofMaterialActive && form.bomValidationActive ){
							 
							 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(formLis.get(i).getPrduct());
							 if(billPrDt!=null){
								 
								 UnitConversionUtility unitConver = new UnitConversionUtility();

								 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
									 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
									 seList.add(ssch);
									 seList = unitConver.getServiceitemProList(seList,formLis.get(i).getPrduct(),formLis.get(i),billPrDt);
									 if(seList!=null && seList.size()>0)
										 ssch = seList.get(0);
									 
								 }else{
									 unitFlag = true;
//									 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
								 }
								 
							 }else{
								 bomCheck = false;
							 }
						 }
						 /**
						  * end
						  */
						newlist.add(ssch);
					}
				}
			}
		}
		if(unitFlag){
			form.showDialogMessage("Unit of service area and product group detail area not mapped at branch level. Or conversion not available");
		}if(!bomCheck){
			form.showDialogMessage("Bill of material is not Created for this service please create first . \n you are not allow to save quotation.");
		}
	}

	form.serviceScheduleTable.getDataprovider().setList(newlist);

}


protected void updateServices1(int changeproductid, int srNo)
{

	try {
		
	} catch (Exception e) {
		System.out.println("Update service : "+e.getMessage());
	}
	
	List<ServiceSchedule> origserviceitemlist=form.serviceScheduleTable.getDataprovider().getList();
	List<Boolean> serLis=new ArrayList<Boolean>();
	
	List<ServiceSchedule> newlist=new ArrayList<ServiceSchedule>();
	newlist.clear();
	
	
	for(int j=0;j<origserviceitemlist.size();j++)
	{
		boolean prodIdCheck=findProductId(origserviceitemlist.get(j).getScheduleProdId(), origserviceitemlist.get(j).getSerSrNo());
		if(prodIdCheck==false){
			serLis.add(false);
		}
		else{
			serLis.add(true);
		}
	}
	form.serviceScheduleTable.connectToLocal();
	serviceSchedulePopUp.getPopScheduleTable().connectToLocal();
	for(int k=0;k<serLis.size();k++)
	{
		
		System.out.println("boolean loop "+serLis.get(k));
		if(serLis.get(k))
		{
			System.out.println("change pro id = "+changeproductid);
			System.out.println("list pro id = "+origserviceitemlist.get(k).getScheduleProdId());
			
//			if(changeproductid!=origserviceitemlist.get(k).getScheduleProdId()){
			
			//
			// old code
			// if(changeproductid!=origserviceitemlist.get(k).getScheduleProdId()
			// && srNo != origserviceitemlist.get(k).getSerSrNo()){

			// new code by for rohan for same product add in contract
			if (srNo != origserviceitemlist.get(k).getSerSrNo()) {
				
		System.out.println("======In=======");
			
			ServiceSchedule serviceScheduleObj=new ServiceSchedule();
			
			serviceScheduleObj.setSerSrNo(origserviceitemlist.get(k).getSerSrNo());
			
			serviceScheduleObj.setScheduleStartDate(origserviceitemlist.get(k).getScheduleStartDate());
			serviceScheduleObj.setScheduleProdId(origserviceitemlist.get(k).getScheduleProdId());
			serviceScheduleObj.setScheduleProdName(origserviceitemlist.get(k).getScheduleProdName());
			serviceScheduleObj.setScheduleDuration(origserviceitemlist.get(k).getScheduleDuration());
			serviceScheduleObj.setScheduleNoOfServices(origserviceitemlist.get(k).getScheduleNoOfServices());
			serviceScheduleObj.setScheduleServiceNo(origserviceitemlist.get(k).getScheduleServiceNo());
			serviceScheduleObj.setScheduleServiceDate(origserviceitemlist.get(k).getScheduleServiceDate());
			
			if(origserviceitemlist.get(k).getScheduleProdEndDate()!=null){
				serviceScheduleObj.setScheduleProdEndDate(origserviceitemlist.get(k).getScheduleProdEndDate());
			}else{
				serviceScheduleObj.setScheduleProdEndDate(new Date());
			}
			
			if(origserviceitemlist.get(k).getScheduleProdStartDate()!=null){
				serviceScheduleObj.setScheduleProdStartDate(origserviceitemlist.get(k).getScheduleProdStartDate());
			}else{
				serviceScheduleObj.setScheduleProdStartDate(new Date());
			}
			
			serviceScheduleObj.setScheduleServiceTime(origserviceitemlist.get(k).getScheduleServiceTime());
			serviceScheduleObj.setPremisesDetails(origserviceitemlist.get(k).getPremisesDetails());
			serviceScheduleObj.setScheduleProBranch(origserviceitemlist.get(k).getScheduleProBranch());
			serviceScheduleObj.setScheduleServiceDay(origserviceitemlist.get(k).getScheduleServiceDay());
			serviceScheduleObj.setServicingBranch(origserviceitemlist.get(k).getServicingBranch());
			
			
			serviceScheduleObj.setTotalServicingTime(origserviceitemlist.get(k).getTotalServicingTime());
			
			/**
			 * Date 03-04-2017 added by vijay for week number
			 */
			int weeknumber = AppUtility.getweeknumber(serviceScheduleObj.getScheduleServiceDate());
			serviceScheduleObj.setWeekNo(weeknumber);
			
			/**
			 * ends here
			 */
			newlist.add(serviceScheduleObj);
			}
		}
	}
	
	
	
	List<SalesLineItem> formLis=form.getSaleslineitemquotationtable().getDataprovider().getList();
	
	
	for(int i=0;i<formLis.size();i++){
		
			
		if(changeproductid==formLis.get(i).getPrduct().getCount()
				&& srNo == formLis.get(i).getProductSrNo()){
			
			
			/* 
			 * Nidhi 
			 * 22-06-2017
			 */
			boolean branchSchFlag=false;
			
			int qty = 0;
			//qty=(int) formLis.get(i).getQty();
			//System.out.println("Qty== "+qty);
			//System.out.println("Branch List Size"+form.customerbranchlist.size());
			
			
			/**
			 * Date 02-05-2018 
			 * Developer : Vijay
			 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
			 * so i have updated below code using hashmap. 
			 */
			
			
//			String[] branchArray=new String[10];
//			if(formLis.get(i).getBranchSchedulingInfo()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo().equals("")){
//				branchArray[0]=formLis.get(i).getBranchSchedulingInfo();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo1()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo1().equals("")){
//				branchArray[1]=formLis.get(i).getBranchSchedulingInfo1();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo2()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo2().equals("")){
//				branchArray[2]=formLis.get(i).getBranchSchedulingInfo2();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo3()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo3().equals("")){
//				branchArray[3]=formLis.get(i).getBranchSchedulingInfo3();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo4()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo4().equals("")){
//				branchArray[4]=formLis.get(i).getBranchSchedulingInfo4();
//			}
//			
//			if(formLis.get(i).getBranchSchedulingInfo5()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo5().equals("")){
//				branchArray[5]=formLis.get(i).getBranchSchedulingInfo5();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo6()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo6().equals("")){
//				branchArray[6]=formLis.get(i).getBranchSchedulingInfo6();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo7()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo7().equals("")){
//				branchArray[7]=formLis.get(i).getBranchSchedulingInfo7();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo8()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo8().equals("")){
//				branchArray[8]=formLis.get(i).getBranchSchedulingInfo8();
//			}
//			if(formLis.get(i).getBranchSchedulingInfo9()!=null
//					&&!formLis.get(i).getBranchSchedulingInfo9().equals("")){
//				branchArray[9]=formLis.get(i).getBranchSchedulingInfo9();
//			}
//			
//			/**
//			 * End
//			 */
//			ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
			
			/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
			ArrayList<BranchWiseScheduling> branchSchedulingList = formLis.get(i).getCustomerBranchSchedulingInfo().get(formLis.get(i).getProductSrNo());
			
			
			if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
				branchSchFlag=true;
				qty=branchSchedulingList.size();
			}else{
				qty=(int) formLis.get(i).getQty();
			}
			
			if(branchSchFlag){
				for (int k = 0; k < qty; k++) {
					if(branchSchedulingList.get(k).isCheck()==true){

					System.out.println("condition == ");

					long noServices = (long) (formLis.get(i).getNumberOfServices());
					int noOfdays = formLis.get(i).getDuration();
					/********* Date 28 sep 2017 below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//					int interval = (int) (noOfdays / formLis.get(i).getNumberOfServices());
					
					/*** Date 28 sep 2017 added by vijay  for scheduling proper calculation with precision number
					 *  for interval  
					 */
					double days =  noOfdays;
					double noofservices= formLis.get(i).getNumberOfServices();
					double interval =  days / noofservices;
					System.out.println(Math.round(days/noofservices));
					System.out.println("interval "+interval);
					double temp = interval;
					double tempvalue=interval;
					/******************** ends here ********************/
				
					
					Date servicedate = formLis.get(i).getStartDate();
					Date d = new Date(servicedate.getTime());
					Date productEndDate = new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate = new Date(productEndDate.getTime());
					productEndDate = prodenddate;
					Date tempdate = new Date();
					for (int j = 0; j < noServices; j++) {
						/**
						 * Date :09/06/2017   by Ajinkya 
						 * Nbhc Scheduling Issue 
						 */
//						if (j > 0) {
//							CalendarUtil.addDaysToDate(d, interval);
//							tempdate = new Date(d.getTime());
//						}
						
						/**
						 *  End Here
						 */

						ServiceSchedule ssch = new ServiceSchedule();
						Date Stardaate = formLis.get(i).getStartDate();

						ssch.setSerSrNo(formLis.get(i).getProductSrNo());

						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(formLis.get(i).getPrduct()
								.getCount());
						ssch.setScheduleProdName(formLis.get(i)
								.getProductName());
						ssch.setScheduleDuration(formLis.get(i).getDuration());
						ssch.setScheduleNoOfServices(formLis.get(i)
								.getNumberOfServices());
						ssch.setScheduleServiceNo(j + 1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(productEndDate);
						ssch.setScheduleServiceTime("Flexible");

						ssch.setServicingBranch(form.olbbBranch.getValue());
						ssch.setTotalServicingTime(formLis.get(i).getServicingTIme());

						ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
						if (branchSchedulingList.get(k).getServicingBranch()!=null) {
							ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
						} 
						
						int dayIndex=AppUtility.getDayIndex(branchSchedulingList.get(k).getDay());
						
						if(dayIndex!=0){
							String dayOfWeek1 = format.format(d);
							int adate = Integer.parseInt(dayOfWeek1);
							int adday1 = dayIndex - 1;
							int adday = 0;
							if (adate <= adday1) {
								adday = (adday1 - adate);
							} else {
								adday = (7 - adate + adday1);
							}
							
							Date newDate = new Date(d.getTime());
							CalendarUtil.addDaysToDate(newDate, adday);
							Date day = new Date(newDate.getTime());

							if (productEndDate.before(day) == false) {
								ssch.setScheduleServiceDate(day);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
							}

							/*** Date 28 sep 2017 added by vijay for interval calculation *************/
							int interval2 =(int) Math.round(interval);

							
							CalendarUtil.addDaysToDate(d, interval2);
							Date tempdate1 = new Date(d.getTime());
							d = tempdate1;
							ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
							
							/************* Date 28 sep 2017 added by vijay for scheduling interval calculation with including precision using roundoff*********************/
							interval =(interval+temp)-tempvalue;
						    tempvalue = Math.round(interval);
						    /**************** ends here **********************************/

						    
						}else{
							
							/*** Date 28 sep 2017 added by vijay for interval calculation *************/
							int interval2 =(int) Math.round(interval);
							
							if (j > 0) {
								CalendarUtil.addDaysToDate(d, interval2);
								tempdate = new Date(d.getTime());
								
								/************* Date 28 sep 2017 added by vijay for scheduling interval including calculation using roundoff *********************/
								interval =(interval+temp)-tempvalue;
							    tempvalue = Math.round(interval);
							    /**************** ends here **********************************/
							    
							}
							
							if (j == 0) {
								ssch.setScheduleServiceDate(servicedate);
							}
							if (j != 0) {
								if (productEndDate.before(d) == false) {
									ssch.setScheduleServiceDate(tempdate);
								} else {
									ssch.setScheduleServiceDate(productEndDate);
								}
							}
						}

						

						ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						ssch.setServiceRemark(formLis.get(i).getRemark());
						/**
						 * Date 01-04-2017 added by vijay for week number
						 */
						int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
						ssch.setWeekNo(weeknumber);
						
						/**
						 * ends here
						 */
						newlist.add(ssch);
					}
				}

				}
			}else
			{
				for(int k=0;k < qty;k++){
					
					
					long noServices=(long) (formLis.get(i).getNumberOfServices());
					int noOfdays=formLis.get(i).getDuration();
					
					/********* Date 28 sep 2017 below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//					int interval= (int) (noOfdays/formLis.get(i).getNumberOfServices());
					
					/*** Date 28 sep 2017 below code added by vijay interval calculation with precision and using roundoff ******************/
					double days =  noOfdays;
					double noofservices= formLis.get(i).getNumberOfServices();
					double interval =  days / noofservices;
					double temp = interval;
					double tempvalue=interval;
					/******************** ends here ********************/
					
					Date servicedate=formLis.get(i).getStartDate();
					Date d=new Date(servicedate.getTime());
					Date productEndDate= new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate=new Date(productEndDate.getTime());
					productEndDate=prodenddate;
					Date tempdate=new Date();
					for(int j=0;j<noServices;j++){
			
						/*** Date 28 sep 2017 added by vijay for interval calculation *************/
						int interval2 =(int) Math.round(interval);
						
						if(j>0)
						{
							CalendarUtil.addDaysToDate(d, interval2);
							tempdate=new Date(d.getTime());
							
							/************* Date 28 sep 2017 added by vijay *********************/
							interval =(interval+temp)-tempvalue;
						    tempvalue = Math.round(interval);
						    /************** ends here ************************/
						}
						
						ServiceSchedule ssch=new ServiceSchedule();
						Date Stardaate=formLis.get(i).getStartDate();
						
						ssch.setSerSrNo(formLis.get(i).getProductSrNo());
						
						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(formLis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(formLis.get(i).getProductName());
						ssch.setScheduleDuration(formLis.get(i).getDuration());
						ssch.setScheduleNoOfServices(formLis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(j+1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(productEndDate);
//						ssch.setServicingBranch(form.olbbBranch.getValue());
						ssch.setTotalServicingTime(formLis.get(i).getServicingTIme());
						ssch.setScheduleServiceTime("Flexible");
						if(j==0){
							ssch.setScheduleServiceDate(servicedate);
							}
						if(j!=0){
							if(productEndDate.before(d)==false){
								ssch.setScheduleServiceDate(tempdate);
							}else{
								ssch.setScheduleServiceDate(productEndDate);
							}
						}
						
//						ssch.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
						if(form.customerbranchlist.size()==qty){
							String branchName=form.customerbranchlist.get(k).getBusinessUnitName();
							ssch.setScheduleProBranch(branchName);
							
							/**
							 * rohan added this code for automatic servicing branch setting for NBHC 
							 */
							
							System.out.println("updateServices presenter cust Branch Name "+form.customerbranchlist.get(k).getBusinessUnitName());
							System.out.println("servicing Branch "+form.customerbranchlist.get(k).getBranch());
							if(form.customerbranchlist.get(k).getBranch()!=null)
							{
								System.out.println("if servicing Branch "+form.customerbranchlist.get(k).getBranch());
								ssch.setServicingBranch(form.customerbranchlist.get(k).getBranch());
							}
							else
							{
								System.out.println("else servicing Branch "+form.customerbranchlist.get(k).getBranch());
								ssch.setServicingBranch(form.olbbBranch.getValue());
							}
							/**
							 * ends here 
							 */
							
						}else if(form.customerbranchlist.size()==0){
							ssch.setScheduleProBranch("Service Address");
							
							/**
							 * rohan added this code for automatic servicing branch setting for NBHC 
							 */
								ssch.setServicingBranch(form.olbbBranch.getValue());
							/**
							 * ends here 
							 */	
							
						}else{
							ssch.setScheduleProBranch(null);
						}
						
						System.out.println("  kk === "+k);
						ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						/**
						 * Date 01-04-2017 added by vijay for week number
						 */
						int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
						ssch.setWeekNo(weeknumber);
						
						/**
						 * ends here
						 */
						newlist.add(ssch);
					}
					
					}
			}
			
		}
		
	
	form.serviceScheduleTable.getDataprovider().setList(newlist);
//	serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(newlist);
	
	}
	
}

		
		
protected boolean findProductId(int prodId, int srno)
{
	List<SalesLineItem> formLis=form.getSaleslineitemquotationtable().getDataprovider().getList();
	boolean flagVal=false;
	for(int i=0;i<formLis.size();i++)
	{
		if(formLis.get(i).getPrduct().getCount()==prodId && formLis.get(i).getProductSrNo() == srno){
			flagVal=true;
		}
	}
	return flagVal;
}

		
		/**
		 * Service Scheduling Logic Ends Here
		 */
		/***************************************End Of Service Scheduling Logic******************************/
		
		
		private void getTaxesDetails() {
			final GenricServiceAsync service = GWT.create(GenricService.class);
			MyQuerry query = new MyQuerry();
			Company c=new Company();
			System.out.println("Company Id :: "+c.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			query.setFilters(filtervec);
			query.setQuerryObject(new TaxDetails());
			form.showWaitSymbol();
			service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
				}

			
				public void onSuccess(ArrayList<SuperModel> result) {
					
					System.out.println("result size of tax "+result);
					servicetaxlist=new ArrayList<TaxDetails>();
					vattaxlist = new ArrayList<TaxDetails>();
					List<TaxDetails> backlist=new ArrayList<TaxDetails>();
					
					for (SuperModel model : result) {
						TaxDetails entity = (TaxDetails) model;
						backlist.add(entity);
						if(entity.getServiceTax().equals(true)){
							servicetaxlist.add(entity);
						}
						if(entity.getVatTax().equals(true)){
							vattaxlist.add(entity);
						}
					}
					System.out.println();
					System.out.println();
					for(int i=0;i<servicetaxlist.size();i++){
						System.out.println(servicetaxlist.get(i).getTaxChargeName()+" "+servicetaxlist.get(i).getTaxChargePercent());
					}
					System.out.println();
					System.out.println();
					for(int i=0;i<vattaxlist.size();i++){
						System.out.println(vattaxlist.get(i).getTaxChargeName()+" "+vattaxlist.get(i).getTaxChargePercent());
					}
					System.out.println();
					System.out.println();
					
					updateTaxes();
					
				}
			});
		}

		private void updateTaxes() {
			Vector<Filter> filtervec=new Vector<Filter>();
			
			MyQuerry querry = new MyQuerry();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(UserConfiguration.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("creationDate >=");
			filter.setDateValue(frmAndToPop.fromDate.getValue());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("creationDate <=");
			filter.setDateValue(frmAndToPop.toDate.getValue());
			filtervec.add(filter);
			
//			filter = new Filter();
//			filter.setQuerryString("status");
//			filter.setStringValue(BillingDocument.CREATED);
//			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Quotation());
//			form.showWaitSymbol();
			genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					if (result.size() != 0) {
						for(SuperModel model:result){
							Quotation con=(Quotation) model;
							
							for(int i=0;i<con.getItems().size();i++){
								
								if(con.getItems().get(i).getServiceTax()!=null){
									for(int j=0;j<servicetaxlist.size();j++){
										if(con.getItems().get(i).getServiceTax().getPercentage()==servicetaxlist.get(j).getTaxChargePercent()){
											con.getItems().get(i).setServiceTaxEdit(servicetaxlist.get(j).getTaxChargeName());
										}
									}
								}
								
								if(con.getItems().get(i).getVatTax()!=null){
									for(int j=0;j<vattaxlist.size();j++){
										if(con.getItems().get(i).getVatTax().getPercentage()==vattaxlist.get(j).getTaxChargePercent()){
											con.getItems().get(i).setVatTaxEdit(vattaxlist.get(j).getTaxChargeName());
										}
									}
								}
							}
							
							genasync.save(con,new AsyncCallback<ReturnFromServer>() {
								@Override
								public void onFailure(Throwable caught) {
									form.hideWaitSymbol();
								}
								@Override
								public void onSuccess(ReturnFromServer result) {
									form.hideWaitSymbol();
									form.showDialogMessage("Entity updated successfully!");
								}
							});
							
						}
					}
				}

				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
					form.showDialogMessage("Unexpected Error!");
				}
			});
		}
		
		//  rohan added this code for communication logs in quotation
		@Override
		public void reactOnCommunicationLog() {
			
			form.showWaitSymbol();
			MyQuerry querry = AppUtility.communicationLogQuerry(model.getCount(), model.getCompanyId(),AppConstants.SERVICEMODULE,AppConstants.QUOTATION);
			
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					ArrayList<InteractionType> list = new ArrayList<InteractionType>();
					
					for(SuperModel model : result){
						
						InteractionType interactionType = (InteractionType) model;
						list.add(interactionType);
						
					}
					/**
					 * Date : 08-11-2017 BY Komal
					 */
					Comparator<InteractionType> comp=new Comparator<InteractionType>() {
						@Override
						public int compare(InteractionType e1, InteractionType e2) {
//							Integer coutn1=o1.getCount();
//							Integer count2=o2.getCount();
//							return ;
//							if (e1.getCount() == e2.getCount()) {
//								return 0;
//							}
//							if (e1.getCount() > e2.getCount()) {
//								return 1;
//							} else {
//								return -1;
//							}
							/**
							 * Updated By: Viraj
							 * Date: 06-02-2018
							 * Description: To sort communication log
							 */
							Integer ee1=e1.getCount();
							Integer ee2=e2.getCount();
							return ee2.compareTo(ee1);
							/** Ends **/
							
						}
					};
					Collections.sort(list,comp);

					form.hideWaitSymbol();
					CommunicationLogPopUp.getRemark().setValue("");
					CommunicationLogPopUp.getDueDate().setValue(null);
					CommunicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
					CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
//					communicationPanel = new PopupPanel(true);
//					communicationPanel.add(CommunicationLogPopUp);
//					communicationPanel.show();
//					communicationPanel.center();
					
					LoginPresenter.communicationLogPanel = new PopupPanel(true);
					LoginPresenter.communicationLogPanel.add(CommunicationLogPopUp);
					LoginPresenter.communicationLogPanel.show();
					LoginPresenter.communicationLogPanel.center();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
			});
			
		}
		
		public void manageCostingListOnServiceChanges(){
			
			HashSet<String> itemSet = new HashSet<String>();
			for(SalesLineItem item:form.saleslineitemquotationtable.getDataprovider().getList()){
				/**
				 *  nidhi |*|
				 *  7-01-2018
				 *  for add only service product name which contain bom and process configration active
				 */
				
				if(AppUtility.verifyBillofMaterilProd(item.getPrduct())==null 
						&& LoginPresenter.billofMaterialActive && form.bomValidationActive ){
					itemSet.add(item.getProductName());
				}else if(! form.serviceBomMaterialList.containsKey(item.getProductName()) 
						&& LoginPresenter.billofMaterialActive && form.bomValidationActive ){
					itemSet.add(item.getProductName());
				}else if(!LoginPresenter.billofMaterialActive) {
					itemSet.add(item.getProductName());
				}
				/**
				 * end
				 */
//				itemSet.add(item.getProductName());
			}
			
			 List<MaterialCostStructure> matCostList = new ArrayList<MaterialCostStructure>();
			 List<MaterialCostStructure> mianMatCostList = new ArrayList<MaterialCostStructure>();
			 matCostList = costForm.getMaterialTbl().getDataprovider().getList();
			 Console.log("matCostList size -- " + matCostList.size());
			 System.out.println("matCostList size -- " + matCostList.size());
			if(matCostList != null && matCostList.size()>0){
				
				 
				  Iterator itr = matCostList.iterator();
				    HashSet<String> containSet = new HashSet<String>();
			       while(itr.hasNext()){
			    	   MaterialCostStructure exchange = (MaterialCostStructure) itr.next();
			           if(!itemSet.contains(exchange.getServiceName())
			        		   && form.serviceBomMaterialList.containsKey(exchange.getServiceName())){
			        	   containSet.add(exchange.getProductName());
			        	   itr.remove(); //Use itr.remove() method
			           }else if(!LoginPresenter.billofMaterialActive  
			        		   && !itemSet.contains(exchange.getServiceName())){
			        	   itr.remove();
			           }
			       }
			       
			       for(MaterialCostStructure metDt : matCostList ){
		    		   
		    		   MaterialCostStructure matObj=new MaterialCostStructure();
						matObj.setServiceName(metDt.getServiceName());
						matObj.setProductName(metDt.getProductName());
						matObj.setProductQty(metDt.getProductQty());
						matObj.setProductCost(metDt.getProductCost());
						matObj.setTotalCost(metDt.getTotalCost());
						
						mianMatCostList.add(matObj);
						
						Console.log("get mat cost list size -- " + mianMatCostList.size());
						System.out.println("get mat cost list size -- " + mianMatCostList.size());
		    	   }
			       
			}
			Console.log("matCostList size -- " + matCostList.size());
			 System.out.println("matCostList size -- " + matCostList.size());
			 if(matCostList== null ){
				 matCostList = new  ArrayList<MaterialCostStructure>();
			 }
			 
			 
			 
			 
			 
		       for(String name : form.serviceBomMaterialList.keySet()){
		    	   
		    	   for(MaterialCostStructure metDt : form.serviceBomMaterialList.get(name) ){
		    		   
		    		   MaterialCostStructure matObj=new MaterialCostStructure();
						matObj.setServiceName(metDt.getServiceName());
						matObj.setProductName(metDt.getProductName());
						matObj.setProductQty(metDt.getProductQty());
						matObj.setProductCost(metDt.getProductCost());
						matObj.setTotalCost(metDt.getTotalCost());
						matObj.setBomActive(metDt.isBomActive());
						mianMatCostList.add(matObj);
						
						Console.log("get mat cost list size -- " + matCostList.size());
						System.out.println("get mat cost list size -- " + matCostList.size());
						
		    	   }
		    	   
		       }
		       System.out.println("get mianMatCostList size -- " + mianMatCostList.size());
		       System.out.println("get table list size -- " +  costForm.getMaterialTbl().getDataprovider().getList().size());
		       costForm.getMaterialTbl().getDataprovider().getList().clear();
		       costForm.getMaterialTbl().getDataprovider().getList().addAll(mianMatCostList);
		       
		}

		
		private void reactOnCustomerAddress() {

			lblUpdate.getElement().setId("addbutton");
			custAddresspopup.getHorizontal().add(lblUpdate);
			custAddresspopup.showPopUp();
			if(model.getServiceAddress()!=null && model.getServiceAddress().getAddrLine1()!=null && 
					  !model.getServiceAddress().getAddrLine1().equals("")){
				
					if(model.getBillingAddress()!=null) {
					custAddresspopup.getBillingAddressComposite().setValue(model.getBillingAddress());
					}
					if(model.getServiceAddress()!=null) {
					custAddresspopup.getServiceAddressComposite().setValue(model.getServiceAddress());
					}
					
					custAddresspopup.getBillingAddressComposite().setEnable(false);
					custAddresspopup.getServiceAddressComposite().setEnable(false);
					
			}
			else{
				getCustomerAddress();
			}
			
		}
		
		private void saveCustomerAddress() {
			if(custAddresspopup.getBillingAddressComposite().getValue()!=null) {
				model.setBillingAddress(custAddresspopup.getBillingAddressComposite().getValue());
			}
			if(custAddresspopup.getServiceAddressComposite().getValue()!=null) {
				model.setServiceAddress(custAddresspopup.getServiceAddressComposite().getValue());
			}
			Console.log("address"+custAddresspopup.getBillingAddressComposite().getValue().getCompleteAddress());
			Console.log("address"+custAddresspopup.getServiceAddressComposite().getValue().getCompleteAddress());

			genasync.save(model, new AsyncCallback<ReturnFromServer>() {
				
				@Override
				public void onSuccess(ReturnFromServer arg0) {
					// TODO Auto-generated method stub
					custAddresspopup.hidePopUp();
					Console.log("address updated");
				}
				
				@Override
				public void onFailure(Throwable arg0) {
					// TODO Auto-generated method stub
					custAddresspopup.hidePopUp();
				}
			});
			
		}

		private void customerAddressEdiatable() {
			// TODO Auto-generated method stub
			custAddresspopup.getBillingAddressComposite().setEnable(true);
			custAddresspopup.getServiceAddressComposite().setEnable(true);
			
		}
		
		private void getCustomerAddress() {
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(model.getCinfo().getCount());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new Customer());
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();

					for(SuperModel model:result){
						
						Customer customer = (Customer)model;
						lblUpdate.getElement().setId("addbutton");
						custAddresspopup.getHorizontal().add(lblUpdate);
						custAddresspopup.showPopUp();
						custAddresspopup.getBillingAddressComposite().setValue(customer.getAdress());
						custAddresspopup.getServiceAddressComposite().setValue(customer.getSecondaryAdress());
						custAddresspopup.getBillingAddressComposite().setEnable(false);
						custAddresspopup.getServiceAddressComposite().setEnable(false);
						
					}
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Unexpected Error Ouccured");
					form.hideWaitSymbol();

				}
			});
		}




		@Override
		public void reactOnSave() {
			if(validateAndUpdateModel()){
				
			Console.log("1-INSIDE Quotation SAVE");
			final ContractServiceAsync conSer=GWT.create(ContractService.class);
			final Quotation quotation=(Quotation) model;
			/**
			 * Date : 20-03-2018 By ANIL
			 * Changed services from 1000 to 700 
			 * Issue faced by Express pesticides (Rohan Bhagde)
			 */
			/**nidhi |*|
			 * 8-01-2019 for update range for bom list save
			 */
			int numberOfSerLimit = 700;
			
			if(LoginPresenter.billofMaterialActive){
				numberOfSerLimit = 350;
			}
			if(quotation.getServiceScheduleList().size()>700){
				for(ServiceSchedule sch:quotation.getServiceScheduleList()){
					sch.setDocumentType("Quotation");
					sch.setDocumentId(quotation.getCount());
					sch.setCompanyId(UserConfiguration.getCompanyId());
				}
				final ArrayList<ServiceSchedule> schSerList=new ArrayList<ServiceSchedule>(quotation.getServiceScheduleList());
				Console.log("1(A)-SCHEDULE LIST "+schSerList.size());
				
				view.showWaitSymbol();
				Console.log("COM_ID "+quotation.getCompanyId()+" DOC_TYPE "+"Contract"+" DOC_ID "+quotation.getCount());
				
				/////////////////////////
				Timer t = new Timer() {
				      @Override
				      public void run() {
				conSer.getScheduleServiceSearchResult(quotation.getCompanyId(), "Quotation", quotation.getCount(), new AsyncCallback<ArrayList<ServiceSchedule>>() {
					@Override
					public void onFailure(Throwable caught) {
						view.hideWaitSymbol();
					}
					@Override
					public void onSuccess(ArrayList<ServiceSchedule> result) {
						Console.log("2-ALREADY SAVED RECORDS "+result.size());
						if(result!=null&&result.size()!=0){
							
							conSer.updateScheduleService("Delete", result, new AsyncCallback<Void>() {
								@Override
								public void onFailure(Throwable caught) {
									view.hideWaitSymbol();
								}
								@Override
								public void onSuccess(Void result) {
									Console.log("3-Deleting.... ");
									
									
									///////////////
									Timer t = new Timer() {
									      @Override
									      public void run() {
									
						    	  quotation.getServiceScheduleList().clear();
									service.save(model,new AsyncCallback<ReturnFromServer>() {
										@Override
										public void onSuccess(ReturnFromServer result) {
											Console.log("3C-Contract SAVED.... ");
											// set the count returned from server to this model
											model.setCount(result.count);
											  //set the id from server to this model
											final long ID = result.id;
											//Set count on view
											view.setCount(result.count);
											
											Console.log("3(A)-SCHEDULE LIST "+schSerList.size());
											for(ServiceSchedule sch:schSerList){
												sch.setDocumentType("Quotation");
												sch.setDocumentId(result.count);
												sch.setCompanyId(UserConfiguration.getCompanyId());
											}
											
											Console.log("3(B)-SCHEDULE FIRST "+schSerList.get(0).getDocumentId()+" LAST "+schSerList.get(schSerList.size()-1).getDocumentId());
											conSer.updateScheduleService("Save", schSerList, new AsyncCallback<Void>() {
												@Override
												public void onFailure(Throwable caught) {
													view.hideWaitSymbol();
												}
												@Override
												public void onSuccess(final Void result) {
													Console.log("4-Saving sch list .... ");
													//Async run sucessfully , hide the wait symbol
													
													Timer t = new Timer() {
												      @Override
												      public void run() {
												    	  Console.log("5-SHow popup.... ");
														view.hideWaitSymbol();
														view.setHideNavigationButtons(form.isHideNavigationButtons());//Ashwini Patil DATE: 3-10-2022
														
														view.setToViewState();
														view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
												      
														if(updateAddressFlag || model.getId()==null){
															model.setId(ID);
															
														CommonServiceAsync commonservice = GWT.create(CommonService.class);
														commonservice.updateAddress(model, new AsyncCallback<Quotation>() {
															
															@Override
															public void onSuccess(Quotation result) {
																// TODO Auto-generated method stub
																if(result!=null){
																	if(result.getBillingAddress()!=null){
																		model.setBillingAddress(result.getBillingAddress());
																	}
																	if(result.getServiceAddress()!=null){
																		model.setServiceAddress(result.getServiceAddress());
																	}
																}
															}
															
															@Override
															public void onFailure(Throwable caught) {
																// TODO Auto-generated method stub
																
															}
														});
														
												      }
													
														
												      }
												    };
												    t.schedule(40000);
												}
											});
											
											
											  //set the id from server to this model
											model.setId(result.id);
											
										}
										
										@Override
										public void onFailure(Throwable caught) {
											view.hideWaitSymbol();
											view.showDialogMessage("Data Save Unsuccessful! Try again.");
											caught.printStackTrace();
										}
									});
									      }
								    };
								    t.schedule(40000);
									
									/////////////////
									
								}
							});
							
							
							
						}else{
							
							quotation.getServiceScheduleList().clear();
							/**
							 * Date : 25-04-2018 By ANIL
							 * earlier by passing model ,it saves the contract 
							 * but now there is schedule list is also in reference of model object we have clear it from contract object 
							 */
							service.save(quotation,new AsyncCallback<ReturnFromServer>() {
//							service.save(model,new AsyncCallback<ReturnFromServer>() {
								@Override
								public void onSuccess(ReturnFromServer result) {
									// set the count returned from server to this model
									model.setCount(result.count);
								   
									final long ID = result.id;

									//Set count on view
									view.setCount(result.count);
									Console.log("5(A)-SCHEDULE LIST "+schSerList.size());
									for(ServiceSchedule sch:schSerList){
										sch.setDocumentType("Quotation");
										sch.setDocumentId(result.count);
										sch.setCompanyId(UserConfiguration.getCompanyId());
									}
									Console.log("5(B)-SCHEDULE FIRST "+schSerList.get(0).getDocumentId()+" LAST "+schSerList.get(schSerList.size()-1).getDocumentId());
									conSer.updateScheduleService("Save", schSerList, new AsyncCallback<Void>() {
										@Override
										public void onFailure(Throwable caught) {
											view.hideWaitSymbol();
										}
										@Override
										public void onSuccess(final Void result) {
											Console.log("5-Saving.... ");
//											view.hideWaitSymbol();
											//Async run sucessfully , hide the wait symbol
											Timer t = new Timer() {
											      @Override
											      public void run() {
											    	  Console.log("5-SHow popup.... ");
													view.hideWaitSymbol();
													view.setHideNavigationButtons(form.isHideNavigationButtons());//Ashwini Patil DATE: 3-10-2022
													view.setToViewState();
													
													view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
											     
													if(updateAddressFlag || model.getId()==null){
														model.setId(ID);
													CommonServiceAsync commonservice = GWT.create(CommonService.class);
													commonservice.updateAddress(model, new AsyncCallback<Quotation>() {
														
														@Override
														public void onSuccess(Quotation result) {
															// TODO Auto-generated method stub
															if(result!=null){
																if(result.getBillingAddress()!=null){
																	model.setBillingAddress(result.getBillingAddress());
																}
																if(result.getServiceAddress()!=null){
																	model.setServiceAddress(result.getServiceAddress());
																}
															}
														}
														
														@Override
														public void onFailure(Throwable caught) {
															// TODO Auto-generated method stub
															
														}
													});
													
											      }
													
													
													
											      }
											    };
											    t.schedule(40000);
											    }
									});
									
									
									//set the id from server to this model
									model.setId(result.id);
									
								}
								
								@Override
								public void onFailure(Throwable caught) {
									view.hideWaitSymbol();
									view.showDialogMessage("Data Save Unsuccessful! Try again.");
									caught.printStackTrace();
								}
							});
							
						}
					}
				});
				
				      }
			    };
			    t.schedule(20000);
				/////////////////////////
				
			}else{
				view.showWaitSymbol();
				service.save(model,new AsyncCallback<ReturnFromServer>() {
					@Override
					public void onSuccess(ReturnFromServer result) {
						//Async run sucessfully , hide the wait symbol
						view.hideWaitSymbol();
						// set the count returned from server to this model
						model.setCount(result.count);
					   
						final long ID = result.id;
						view.setHideNavigationButtons(form.isHideNavigationButtons());//Ashwini Patil DATE: 3-10-2022
						
						view.setToViewState();
						//Set count on view
						view.setCount(result.count);
						view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
					
						Console.log("updateAddressFlag =="+updateAddressFlag);
						if(updateAddressFlag || model.getId()==null){
							model.setId(ID);
							CommonServiceAsync commonservice = GWT.create(CommonService.class);
							commonservice.updateAddress(model, new AsyncCallback<Quotation>() {
								
								@Override
								public void onSuccess(Quotation result) {
									// TODO Auto-generated method stub
									if(result!=null){
										if(result.getBillingAddress()!=null){
											model.setBillingAddress(result.getBillingAddress());
										}
										if(result.getServiceAddress()!=null){
											model.setServiceAddress(result.getServiceAddress());
										}
									}
								}
								
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									
								}
							});
						}
						
						
						//set the id from server to this model
						model.setId(result.id);
						

					}
					
					@Override
					public void onFailure(Throwable caught) {
						view.hideWaitSymbol();
						view.showDialogMessage("Data Save Unsuccessful! Try again.");
						caught.printStackTrace();
					}
				});
			}
		 }
			form.addClickEventOnActionAndNavigationMenus();//Ashwini Patil Date:21-02-2023
		
		}
		
		private void setEmailPopUpData() {
			form.showWaitSymbol();
			String customerEmail = "";

			String branchName = form.olbbBranch.getValue();
			String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
			customerEmail = AppUtility.getCustomerEmailid(model.getCinfo().getCount());
			
			String customerName = model.getCinfo().getFullName();
			if(!customerName.equals("") && !customerEmail.equals("")){
				label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),customerName,customerEmail,null);
				emailpopup.taToEmailId.setValue(customerEmail);
			}
			else{
		
				MyQuerry querry = AppUtility.getcustomerQuerry(model.getCinfo().getCount());
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						for(SuperModel smodel : result){
							Customer custEntity  = (Customer) smodel;
							System.out.println("customer Name = =="+custEntity.getFullname());
							System.out.println("Customer Email = == "+custEntity.getEmail());
							
							if(custEntity.getEmail()!=null){
								label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
								emailpopup.taToEmailId.setValue(custEntity.getEmail());

							}
							else{
								label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
							}
							break;
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
					}
				});
			}
			
			Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
			Console.log("screenName "+screenName);
			AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.SERVICE,screenName);
			emailpopup.taFromEmailId.setValue(fromEmailId);
			emailpopup.smodel = model;
			form.hideWaitSymbol();
			
		}
		
		private void reactOnQuotationSignUp() {


			List<SalesLineItem> list = form.saleslineitemquotationtable.getDataprovider().getList();
			boolean conf = true;
			if(form.tbQuotationStatus.getValue()!=null && form.tbQuotationStatus.getValue().equalsIgnoreCase(Lead.SUCCESSFULL) || form.tbQuotationStatus.getValue().equalsIgnoreCase("Successful") ) {
				form.showDialogMessage("Lead is already sucessful");
				return;
			}
			for(SalesLineItem lineItem : list) {
				if(lineItem.getNumberOfServices()<=0) {
					conf =  Window.confirm("No of services is 0 , Do you want to continue?");
				}
			}
			Console.log("conf "+conf);
			if(conf) {
				String Url = com.google.gwt.core.client.GWT.getModuleBaseURL(); 
				String Appid = "";
				if(Url.contains("-dot-")){
					String [] urlarray = Url.split("-dot-");
					 Appid = urlarray[1];
				}
				else{
					String [] urlarray = Url.split("\\.");
					 Appid = urlarray[1];
				}
				Console.log("Appid "+Appid);
				form.showWaitSymbol();
//				commonservice.CallPaymentGatewayAPI(model,Url,Appid, new AsyncCallback<String>() {
//					
//					@Override
//					public void onSuccess(String result) {
//						// TODO Auto-generated method stub
////						form.showDialogMessage(result);
//						if(result.contains("@Error")){
//							String error[] = result.split("@Error");
//							form.showDialogMessage(error[0]);
//
//						}
//						else{
//							form.showDialogMessage("Sign Up Initiated And SMS Sent To Customer");
//							form.getTbpaymentgatewayid().setValue(result);
//							
//						}
//							form.hideWaitSymbol();
//					}
//					
//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//						form.hideWaitSymbol();
//						form.showDialogMessage("Please try again!");
//					}
//				});
				
				final String pdfURLLink = GWT.getModuleBaseURL() + "pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";

				commonservice.CallPaymentGatewayAPI(model.getCount(), AppConstants.QUOTATION, Url, Appid, model.getCompanyId(), model.getCinfo().getCount(),pdfURLLink, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
						form.showDialogMessage("Please try again!");
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						if(result.contains("@Error")){
							String error[] = result.split("@Error");
							form.showDialogMessage(error[0]);

						}
						else{
							form.showDialogMessage("Sign Up Initiated And SMS Sent To Customer");
//							form.getTbpaymentgatewayid().setValue(result);
							
						}
							form.hideWaitSymbol();
					}
				});
			}
			
		}





		@Override
		public void reactOnSMS() {
			
			smspopup.showPopUp();
			loadSMSPopUpData();
			
		}

		private void loadSMSPopUpData() {

			form.showWaitSymbol();
			String customerCellNo = model.getCinfo().getCellNumber()+"";

			String customerName = model.getCinfo().getFullName();
			if(!customerName.equals("") && !customerCellNo.equals("")){
				AppUtility.loadContactPersonlist(smspopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),customerName,customerCellNo,null,true);
			}
			Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
			Console.log("screenName "+screenName);
			AppUtility.makeLiveSmsTemplateConfig(smspopup.oblsmsTemplate,AppConstants.SERVICE,screenName,true,AppConstants.SMS);
			smspopup.getRbSMS().setValue(true);
			smspopup.smodel = model;
			form.hideWaitSymbol();
		
		}
		
		private void reactOnCopy(){
			try {
				System.out.println("get id : " +model.getCount());
				Quotation quo = (Quotation)model;
				//Quotation quotation=(Quotation) deepClone(quo);
				final Quotation quotation=(Quotation)model;
				quotation.setCount(0);
				quotation.setContractCount(0);
				quotation.setQuotationCount(0);
				quotation.setQuotationDate(null);
				quotation.setApprovalDate(null);
				quotation.setCreationDate(null);
				quotation.setValidUntill(null);
				quotation.setStatus(Quotation.CREATED);
				quotation.setId(null);
				
				System.out.println("get id after : " +quotation.getCount()+ " ID "+quotation.getId() + " get value model  : "+ model.getCount() +" model Id ;;;; -- "+model.getId());
				AppMemory.getAppMemory().currentState=ScreeenState.NEW;
				form.setToNewState();
				
				/********** Date 27 sep 2017 added by vijay code added in timer because product table is getting repeating same column ******/ 
				Timer timer = new Timer() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						
						
						form.updateView(quotation);
						form.setToEditState();
						form.toggleAppHeaderBarMenu();
						
						
						form.getTbLeadId().setValue("");
						form.tbrefNum.setValue("");
						form.dbrefDate.setValue(null);
						form.getTbRefeRRedBy().setValue("");
						form.getTbTicketNumber().setValue("");
						form.getTbCreatedBy().setValue(LoginPresenter.loggedInUser);					
						form.getTbQuotatinId().setValue("");
						form.getTbContractId().setValue("");
						form.getTbQuotatinId().setValue("");
						form.getDbFollowUpDate().setValue(null);
						form.getDbQuotationDate().setValue(new Date());
						form.getDbValidUntill().setValue(null);
						form.getOlbbBranch().setValue(model.getBranch());
				    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
				    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
								form.olbcNumberRange.setValue(branchEntity.getNumberRange());
								Console.log("Number range set to drop down by default");
				    	  }else {
				    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
						  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
								if(range!=null&&!range.equals("")) {
									form.olbcNumberRange.setValue(range);
									Console.log("in else Number range set to drop down by default");							
								}else {
									form.olbcNumberRange.setSelectedIndex(0);
									Console.log("could not set number range");
								}					
				    	  }
					}
				};
				timer.schedule(2000);
				
//				form.tbQuotationStatus.setValue(Quotation.CREATED);
//				
			} 
			catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
		
}
