package com.slicktechnologies.client.views.quotation;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.itextpdf.text.log.SysoCounter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.client.views.quotation.QuotationPresenter.QuotationPresenterTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;

public class QuotationPresenterTableProxy extends QuotationPresenterTable {
	TextColumn<Quotation> getCustomerCellNumberColumn;
	TextColumn<Quotation> getCustomerFullNameColumn;
	TextColumn<Quotation> getCustomerIdColumn;
	TextColumn<Quotation> getContractCountColumn;
	TextColumn<Quotation> getQuotationCountColumn;
	TextColumn<Quotation> getLeadCountColumn;
	TextColumn<Quotation> getBranchColumn;
	TextColumn<Quotation> getEmployeeColumn;
	TextColumn<Quotation> getStatusColumn;
	TextColumn<Quotation> getCreationDateColumn;
	TextColumn<Quotation> getValidUntilColumn;
	TextColumn<Quotation> getGroupColumn;
	TextColumn<Quotation> getCategoryColumn;
	TextColumn<Quotation> getTypeColumn;
	TextColumn<Quotation> getCountColumn;
	TextColumn<Quotation> getPriorityColumn;
	TextColumn<Quotation>getColumnTicketID;
	
	/**
	 * rohan added this code for NBHC for 
	 */

	TextColumn<Quotation> getCreatedByColumn;
	
	/**
	 * rohan added this code for Bhavani pest
	 */
	
	TextColumn<Quotation> getQuotationDateColumn;
	
	/** date 11/10/2017 added by komal for followup date **/
	TextColumn<Quotation> getFollowUpDateColumn;
	
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("getCustomerCellNumberColumn"))
			return this.getCustomerCellNumberColumn;
		if(varName.equals("getCustomerFullNameColumn"))
			return this.getCustomerFullNameColumn;
		if(varName.equals("getCustomerIdColumn"))
			return this.getCustomerIdColumn;
		if(varName.equals("getContractCountColumn"))
			return this.getContractCountColumn;
		if(varName.equals("getQuotationCountColumn"))
			return this.getQuotationCountColumn;
		if(varName.equals("getLeadCountColumn"))
			return this.getLeadCountColumn;
		if(varName.equals("getBranchColumn"))
			return this.getBranchColumn;
		if(varName.equals("getEmployeeColumn"))
			return this.getEmployeeColumn;
		if(varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if(varName.equals("getCreationDateColumn"))
			return this.getCreationDateColumn;
		if(varName.equals("getValidUntilColumn"))
			return this.getValidUntilColumn;
		if(varName.equals("getGroupColumn"))
			return this.getGroupColumn;
		if(varName.equals("getCategoryColumn"))
			return this.getCategoryColumn;
		if(varName.equals("getTypeColumn"))
			return this.getTypeColumn;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		/** date 11/10/2017 added by komal for followup date **/
		if(varName.equals("getFollowUpDateColumn"))
			return this.getFollowUpDateColumn;
		return null ;
	}
	public QuotationPresenterTableProxy()
	{
		super();
	}
	@Override public void createTable() {
		addColumngetCount();
		addCloumnGetQuotationDate();
		addCoulmnGetFollowUpDate();
		addColumngetStatus();
		addColumngetCustomerFullName();
		addColumngetCustomerCellNumber();
		addColumngetEmployee();
		addColumngetBranch();
		addColumnValidUntil();
		addColumngetPriority();
		addColumngetLeadCount();
		addColumngetContractCount();
		createColumnTicketID();
		addColumngetCreatedBy();
		addColumngetCustomerId();
		addColumngetCreationDate();
		addColumngetGroup();
		addColumngetCategory();
		addColumngetType();
		addColumnSorting();
		
		}
	
	
	
	private void addCloumnGetQuotationDate() {
		
		getQuotationDateColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				if(object.getQuotationDate()!=null)
				     return  AppUtility.parseDate(object.getQuotationDate());
				  else 
					  return "N.A";
				
			}
				};
				table.addColumn(getQuotationDateColumn,"Quotation Date");
				table.setColumnWidth(getQuotationDateColumn,120,Unit.PX);
				getQuotationDateColumn.setSortable(true);
	}
	
	private void addColumngetCreatedBy() {
		
		getCreatedByColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				return object.getCreatedBy();
			}
				};
				table.addColumn(getCreatedByColumn,"Created By");
				table.setColumnWidth(getCreatedByColumn, 110, Unit.PX);
				getCreatedByColumn.setSortable(true);
	}	
	
	private void createColumnTicketID() {
		getColumnTicketID=new TextColumn<Quotation>() {
			@Override
			public String getValue(Quotation object) {
				if(object.getTicketNumber()!=-1){
				return object.getTicketNumber()+"";
				}else
					return "N.A.";
			}
		};
		table.addColumn(getColumnTicketID, "Ticket Id");
		table.setColumnWidth(getColumnTicketID,120,Unit.PX);
		getColumnTicketID.setSortable(true);
		
	}
	
	private void addColumnTicketNoSorting() {
		List<Quotation> list = getDataprovider().getList();
		columnSort = new ListHandler<Quotation>(list);
		Comparator<Quotation> quote = new Comparator<Quotation>() {
			@Override
			public int compare(Quotation e1, Quotation e2) {
				if (e1 != null && e2 != null) {
					if(e1.getTicketNumber()== e2.getTicketNumber()){
					  return 0;}
				  if(e1.getTicketNumber()> e2.getTicketNumber()){
					  return 1;}
				  else{
					  return -1;}} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnTicketID, quote);
		table.addColumnSortHandler(columnSort);
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Quotation>()
				{
			@Override
			public Object getKey(Quotation item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetBranch();
		addColumnTicketNoSorting();
		addSortinggetCreationDate();
		addSortingOnQuotationDate();
		adddSortingValidUntil();
		addSortinggetEmployee();
		addSortinggetStatus();
		addSortinggetLeadCount();
		addSortinggetGroup();
		addSortinggetCategory();
		addSortinggetCustomerId();
		addSortinggetType();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortinggetContractCount();
		addSortingQuotationPriority();
		addSortinggetCreatedBy();
		/** date 11/10/2017 added by komal for followup date **/
		addSortinggetFollowUpDate();
		
	}
	
	
	private void addSortingOnQuotationDate() {
		
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getQuotationDateColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getQuotationDate()!=null && e2.getQuotationDate()!=null){
						return e1.getQuotationDate().compareTo(e2.getQuotationDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addSortinggetCreatedBy()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getCreatedByColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreatedBy()!=null && e2.getCreatedBy()!=null){
						return e1.getCreatedBy().compareTo(e2.getCreatedBy());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetCount()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				table.setColumnWidth(getCountColumn,100,Unit.PX);
				getCountColumn.setSortable(true);
	}
	protected void addColumngetCreationDate()
	{
		getCreationDateColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				if(object.getCreationDate()!=null)
				     return  AppUtility.parseDate(object.getCreationDate());
				  else 
					  return "N.A";
				
			}
				};
				table.addColumn(getCreationDateColumn,"Creation Date");
				table.setColumnWidth(getCreationDateColumn,120,Unit.PX);
				getCreationDateColumn.setSortable(true);
	}
	
	
	protected void addSortinggetCreationDate()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getCreationDateColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
						return e1.getCreationDate().compareTo(e2.getCreationDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumnValidUntil()
	{
		getValidUntilColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				if(object.getCreationDate()!=null)
				     return  AppUtility.parseDate(object.getValidUntill());
				  else 
					  return "N.A";
				
			}
				};
				table.addColumn(getValidUntilColumn,"Valid Until");
				table.setColumnWidth(getValidUntilColumn,120,Unit.PX);
				getValidUntilColumn.setSortable(true);
	}
	
	
	protected void adddSortingValidUntil()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getValidUntilColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getValidUntill()!=null && e2.getValidUntill()!=null){
						return e1.getValidUntill().compareTo(e2.getValidUntill());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	
	
	protected void addSortinggetBranch()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				return object.getBranch()+"";
			}
				};
				table.addColumn(getBranchColumn,"Branch");
				table.setColumnWidth(getBranchColumn,100,Unit.PX);
				getBranchColumn.setSortable(true);
	}
	protected void addSortinggetEmployee()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getEmployeeColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployee()
	{
		getEmployeeColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				return object.getEmployee()+"";
			}
				};
				table.addColumn(getEmployeeColumn,"Sales Person");
				table.setColumnWidth(getEmployeeColumn,110,Unit.PX);
				getEmployeeColumn.setSortable(true);
	}
	protected void addSortinggetStatus()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				table.setColumnWidth(getStatusColumn,120,Unit.PX);
				getStatusColumn.setSortable(true);
	}
	protected void addSortinggetLeadCount()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getLeadCountColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getLeadCount()== e2.getLeadCount()){
						return 0;}
					if(e1.getLeadCount()> e2.getLeadCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetLeadCount()
	{
		getLeadCountColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				if( object.getLeadCount()==-1)
					return "N.A";
				else return object.getLeadCount()+"";
			}
				};
				table.addColumn(getLeadCountColumn,"Lead ID");
				table.setColumnWidth(getLeadCountColumn,100,Unit.PX);
				getLeadCountColumn.setSortable(true);
	}
	protected void addSortinggetGroup()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getGroupColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getGroup()!=null && e2.getGroup()!=null){
						return e1.getGroup().compareTo(e2.getGroup());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetGroup()
	{
		getGroupColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				return object.getGroup()+"";
			}
				};
				table.addColumn(getGroupColumn,"Group");
				table.setColumnWidth(getGroupColumn,120,Unit.PX);

				getGroupColumn.setSortable(true);
	}
	protected void addSortinggetQuotationCount()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getQuotationCountColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getQuotationCount()== e2.getQuotationCount()){
						return 0;}
					if(e1.getQuotationCount()> e2.getQuotationCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetQuotationCount()
	{
		getQuotationCountColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				if( object.getQuotationCount()==-1)
					return "N.A";
				else return object.getQuotationCount()+"";
			}
				};
				table.addColumn(getQuotationCountColumn,"Quotation Id");
				getQuotationCountColumn.setSortable(true);
	}
	protected void addSortinggetCategory()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getCategoryColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCategory()!=null && e2.getCategory()!=null){
						return e1.getCategory().compareTo(e2.getCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCategory()
	{
		getCategoryColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				return object.getCategory()+"";
			}
				};
				table.addColumn(getCategoryColumn,"Category");
				table.setColumnWidth(getCategoryColumn,120,Unit.PX);
				getCategoryColumn.setSortable(true);
	}
	protected void addSortinggetCustomerId()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getCustomerIdColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCustomerId()== e2.getCustomerId()){
						return 0;}
					if(e1.getCustomerId()> e2.getCustomerId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerId()
	{
		getCustomerIdColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				if( object.getCustomerId()==-1)
					return "N.A";
				else return object.getCustomerId()+"";
			}
				};
				table.addColumn(getCustomerIdColumn,"Customer ID");
				table.setColumnWidth(getCustomerIdColumn,110,Unit.PX);
				getCustomerIdColumn.setSortable(true);
	}
	protected void addSortinggetType()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getTypeColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getType()!=null && e2.getType()!=null){
						return e1.getType().compareTo(e2.getType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetType()
	{
		getTypeColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				return object.getType()+"";
			}
				};
				table.addColumn(getTypeColumn,"Type");
				
				table.setColumnWidth(getTypeColumn,120,Unit.PX);
				getTypeColumn.setSortable(true);
	}
	
	protected void addSortingQuotationPriority()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getPriorityColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPriority()!=null && e2.getPriority()!=null){
						return e1.getPriority().compareTo(e2.getPriority());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}


	protected void addColumngetPriority()
	{
		getPriorityColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				return object.getPriority()+"";
			}
				};
				table.addColumn(getPriorityColumn,"Priority");
				table.setColumnWidth(getPriorityColumn,120,Unit.PX);
				getPriorityColumn.setSortable(true);
	}

	protected void addSortinggetCustomerFullName()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getCustomerFullNameColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCustomerFullName()!=null && e2.getCustomerFullName()!=null){
						return e1.getCustomerFullName().compareTo(e2.getCustomerFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerFullName()
	{
		getCustomerFullNameColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				return object.getCustomerFullName()+"";
			}
				};
				table.addColumn(getCustomerFullNameColumn,"Customer Name");
				table.setColumnWidth(getCustomerFullNameColumn,150,Unit.PX);
				getCustomerFullNameColumn.setSortable(true);
	}
	protected void addSortinggetCustomerCellNumber()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getCustomerCellNumberColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCustomerCellNumber()== e2.getCustomerCellNumber()){
						return 0;}
					if(e1.getCustomerCellNumber()> e2.getCustomerCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerCellNumber()
	{
		getCustomerCellNumberColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				return object.getCustomerCellNumber()+"";
			}
				};
				table.addColumn(getCustomerCellNumberColumn,"Customer Cell");
				table.setColumnWidth(getCustomerCellNumberColumn,120,Unit.PX);
				getCustomerCellNumberColumn.setSortable(true);
	}
	protected void addSortinggetContractCount()
	{
		List<Quotation> list=getDataprovider().getList();
		columnSort=new ListHandler<Quotation>(list);
		columnSort.setComparator(getContractCountColumn, new Comparator<Quotation>()
				{
			@Override
			public int compare(Quotation e1,Quotation e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetContractCount()
	{
		getContractCountColumn=new TextColumn<Quotation>()
				{
			@Override
			public String getValue(Quotation object)
			{
				if( object.getContractCount()==-1)
					return "N.A";
				else return object.getContractCount()+"";
			}
				};
				table.addColumn(getContractCountColumn,"Contract ID");
				table.setColumnWidth(getContractCountColumn,100,Unit.PX);
				getContractCountColumn.setSortable(true);
	}
	
	/** date 11/10/2017 added by komal for followup date **/
	private void addCoulmnGetFollowUpDate() {
		getFollowUpDateColumn = new TextColumn<Quotation>() {
			@Override
			public String getValue(Quotation object) {
				if (object.getFollowUpDate() != null)
					return AppUtility.parseDate(object.getFollowUpDate());
				else
					return "N.A.";

			}
		};
		table.addColumn(getFollowUpDateColumn, "Follow-up Date");
		table.setColumnWidth(getFollowUpDateColumn, 120, Unit.PX);
		getFollowUpDateColumn.setSortable(true);
	}

	private void addSortinggetFollowUpDate() {

		List<Quotation> list = getDataprovider().getList();
		columnSort = new ListHandler<Quotation>(list);
		columnSort.setComparator(getFollowUpDateColumn,
				new Comparator<Quotation>() {
					@Override
					public int compare(Quotation e1, Quotation e2) {
						if (e1 != null && e2 != null) {
							if (e1.getFollowUpDate() == null
									&& e2.getFollowUpDate() == null) {
								return 0;
							}
							if (e1.getFollowUpDate() == null)
								return 1;
							if (e2.getFollowUpDate() == null)
								return -1;

							return e1.getFollowUpDate().compareTo(
									e2.getFollowUpDate());
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
}
