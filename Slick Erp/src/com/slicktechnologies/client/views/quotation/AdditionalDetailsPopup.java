package com.slicktechnologies.client.views.quotation;

import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;

public class AdditionalDetailsPopup extends AbsolutePanel{


	public TextBox txtchemused;
	public TextBox txtfreqused;
	public TextBox txtwarranty;
	Button btnCancel,btnOk;
	
	public AdditionalDetailsPopup() {
		// TODO Auto-generated constructor stub
		btnCancel= new Button("Cancel");
		btnOk= new Button("Ok");
		
		InlineLabel lheading=new InlineLabel("ADDITIONAL DETAILS");
		add(lheading,120,10);
		
		InlineLabel lchemuse = new InlineLabel("Chemical to be Used  :");
		add(lchemuse,10,70);
		txtchemused= new TextBox();
		add(txtchemused,150,70);
		
		InlineLabel lfreqser= new InlineLabel("Frequency of Service :");
		add(lfreqser,10,110);
		txtfreqused= new TextBox();
		add(txtfreqused,150,110);
		
		InlineLabel lwarranty = new InlineLabel("Warranty Period      :");
		add(lwarranty,10,150);
		txtwarranty= new TextBox();
		add(txtwarranty,150,150);
		
		add(btnOk,100,250);
		add(btnCancel,220,250);
		setSize("400px", "300px");
		
		
	}

	//*************getters and setters **********************
	
	public TextBox getTxtchemused() {
		return txtchemused;
	}

	public void setTxtchemused(TextBox txtchemused) {
		this.txtchemused = txtchemused;
	}

	public TextBox getTxtfreqused() {
		return txtfreqused;
	}

	public void setTxtfreqused(TextBox txtfreqused) {
		this.txtfreqused = txtfreqused;
	}

	public TextBox getTxtwarranty() {
		return txtwarranty;
	}

	public void setTxtwarranty(TextBox txtwarranty) {
		this.txtwarranty = txtwarranty;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}
	
	
	
}
