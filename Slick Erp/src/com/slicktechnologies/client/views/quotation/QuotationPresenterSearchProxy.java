package com.slicktechnologies.client.views.quotation;

import java.util.Vector;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.quotation.QuotationPresenter.QuotationPresenterSearch;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class QuotationPresenterSearchProxy extends QuotationPresenterSearch {
	public PersonInfoComposite personInfo;
	public IntegerBox tbContractId;
	public IntegerBox tbQuotationId;
	public IntegerBox tbLeadId;
	public IntegerBox tbTicketNumber;
	public ObjectListBox<Branch> olbBranch;
	public static ObjectListBox<Employee> olbEmployee;
	public DateComparator dateComparator;
	
	public ObjectListBox<Config> olbQuotationGroup;
	public ObjectListBox<ConfigCategory> olbQuotationCategory;
	public ObjectListBox<Type> olbQuotationType;
	public ListBox olbQuotationStatus;
	public ObjectListBox<Config> olbQuotationPriority;
	
	/**
	 *  rohan added this code for NBHC  
	 */
	
	public ObjectListBox<Employee> olbCreatedBy;
	
	/**
	 * ends here 
	 */
	
	
	/**
	 * Rohan added this for bhavani pest control 
	 */
		DateBox dbQuotationDate;
	
	/**
	 *ends here 
	 */
		
	/** Date 11/10/2017 added by komal for followup date **/
	   public DateComparator dateComparatorFollowUp;
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("personInfo"))
			return this.personInfo;
		if(varName.equals("tbContractId"))
			return this.tbContractId;
		if(varName.equals("tbQuotationId"))
			return this.tbQuotationId;
		if(varName.equals("tbLeadId"))
			return this.tbLeadId;
		if(varName.equals("olbBranch"))
			return this.olbBranch;
		if(varName.equals("olbEmployee"))
			return this.olbEmployee;
		if(varName.equals("dateComparator"))
			return this.dateComparator;
		
		if(varName.equals("olbQuotationGroup"))
			return this.olbQuotationGroup;
					
		if(varName.equals("olbQuotationCategory"))
			return this.olbQuotationCategory;
					
		if(varName.equals("olbQuotationType"))
			return this.olbQuotationType;
					
		if(varName.equals("olbQuotationStatus"))
			return this.olbQuotationStatus;
								
		if(varName.equals("olbQuotationPriority"))
			return this.olbQuotationPriority;
		/** Date 11/10/2017 added by komal for followup date **/
		if(varName.equals("dateComparatorFollowUp"))
			return this.dateComparatorFollowUp;
		return null ;
	}
	
	
	public QuotationPresenterSearchProxy()
	{
		super();
		createGui();
	}
	
	
	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		tbContractId= new IntegerBox();
		tbQuotationId= new IntegerBox();
		tbLeadId= new IntegerBox();
		tbTicketNumber=new IntegerBox();
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
//		if(ProjectPresenter.checkForProcessConfigurartionIsActiveOrNot("Quotation","SalesPersonRestriction"))
//		{
//			
//			olbEmployee=new ObjectListBox<Employee>();
//			AppUtility.makeSalesPersonListBoxLive(olbEmployee);
//		
//			Timer t = new Timer() {
//			
//			@Override
//			public void run() {
//
//				makeSalesPersonEnable();
//			}
//
//			
//		};t.schedule(3000);
//	}
//	else
//	{
		olbEmployee= new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbEmployee);

		/**
		 * Date 19-01-2019 By Vijay
		 * Des :- For Zonal Head Login in search pop who employee are reporting to him they all are show in Sales person
		 * Drop Down including all branches
		 * Requirement :- NBHC CCPM
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", "EnableSalesPersonAsPerReportingTo")
				&& LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Zonal Head")){
				olbEmployee.makeEmployeeLiveAsperReportingTo(LoginPresenter.loggedInUser);
		}else{
			olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.QUOTATION, "Sales Person");

		}
//	}
		
		
		dateComparator= new DateComparator(new Quotation());
		
		olbQuotationGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbQuotationGroup, Screen.QUOTATIONGROUP);
		
		olbQuotationCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbQuotationCategory, Screen.QUOTATIONCATEGORY);
		
		
		olbQuotationType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbQuotationType, Screen.QUOTATIONTYPE);
		
		olbQuotationStatus=new ListBox();
		AppUtility.setStatusListBox(olbQuotationStatus, Quotation.getStatusList());
		
		olbQuotationPriority=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbQuotationPriority, Screen.QUOTATIONPRIORITY);
		// rohan comments this line
//		CategoryTypeFactory.initiateOneManyFunctionality(olbQuotationCategory,olbQuotationType);
		
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		olbCreatedBy= new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbCreatedBy);
		
		
		dbQuotationDate=new DateBoxWithYearSelector();
		/** Date 11/10/2017 added by komal for followup date **/
		dateComparatorFollowUp = new DateComparator("followUpDate" , new Quotation());
		
	}
	
	/**
	 * rohan added this method for setting value to sales person and and make it non editable
	 */
	public static void makeSalesPersonEnable() {
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
		olbEmployee.setValue(LoginPresenter.loggedInUser);
		olbEmployee.setEnabled(false);
		}
	}
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		builder = new FormFieldBuilder("Contract Id",tbContractId);
		FormField ftbContractId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Quotation Id",tbQuotationId);
		FormField ftbQuotationId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Lead Id",tbLeadId);
		FormField ftbLeadId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Ticket Id",tbTicketNumber);
		FormField ftbTicketNumber= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Sales Person",olbEmployee);
		FormField folbEmployee= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date (Creation Date)",dateComparator.getFromDate());
		FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (Creation Date)",dateComparator.getToDate());
		FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		//  rohan added this 
		builder = new FormFieldBuilder("Quotation Date",dbQuotationDate);
		FormField fdbQuotationDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Quotation Group",olbQuotationGroup);
		FormField folbQuotationGroup= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Quotation Category",olbQuotationCategory);
		FormField folbQuotationCategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Quotation Type",olbQuotationType);
		FormField folbQuotationType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Quotation Status",olbQuotationStatus);
		FormField folbQuotationStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Quotation Priority",olbQuotationPriority);
		FormField folbQuotationPriority= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Created By",olbCreatedBy);
		FormField folbCreatedBy= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 11/10/2017 added by komal for followup date **/
		builder = new FormFieldBuilder("From Date (Follow-up)",dateComparatorFollowUp.getFromDate());
		FormField ffollowupdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (Follow-up)",dateComparatorFollowUp.getToDate());
		FormField ffollowupdateComparator1 = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 12-01-2018 By Vijay 
		 * differentiated date combination search and other filter search
		 */
		builder = new FormFieldBuilder();
		FormField fgroupingDateFilterInformation=builder.setlabel("Date Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		builder = new FormFieldBuilder();
		FormField fgroupingOtherFilterInformation=builder.setlabel("Other Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		/**
		 * ends here
		 */
		
		
		this.fields=new FormField[][]{
//				{fdateComparator,fdateComparator1,ffollowupdateComparator,ffollowupdateComparator1,folbQuotationGroup},
//				{folbQuotationCategory,folbQuotationType, folbQuotationStatus,folbEmployee },
//				{folbBranch,folbQuotationPriority,ftbLeadId,ftbQuotationId},
//				{ftbContractId,ftbTicketNumber,folbCreatedBy,fdbQuotationDate},
//				{fpersonInfo}
				/**  date 12-01-2018 by vijay above old code commented and below groping added  **/
				{fgroupingDateFilterInformation},
				{ffollowupdateComparator,ffollowupdateComparator1},//fdateComparator,fdateComparator1, Ashwini Patil Removed creation date filter as per Nitin sir's Instruction. As there is no index for it.
				{folbBranch, folbQuotationStatus, folbQuotationGroup},
				{fgroupingOtherFilterInformation},
				{folbQuotationCategory,folbQuotationType,folbEmployee,folbQuotationPriority },
				{ftbLeadId,ftbQuotationId,ftbContractId,ftbTicketNumber},
				{folbCreatedBy,fdbQuotationDate},
				{fpersonInfo}
				
		
		};
		
		
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		if(olbEmployee.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbEmployee.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		
		//   rohan added this for branch level restriction
		if(LoginPresenter.branchRestrictionFlag==true && olbBranch.getSelectedIndex()==0)
		{
//			/**
//			 * Date 19-01-2019 By Vijay
//			 * Des :- added if condition if branch value is nothing then should not add filter of branch
//			 * NBHC CCPM when zonal head login then for sales person filter not required to branch restriction
//			 * so if branch selected then add filter
//			 */
//			if(!olbBranch.getValue(olbBranch.getSelectedIndex()).trim().equals("--SELECT--")){
//				temp=new Filter();
//				temp.setStringValue(olbBranch.getValue(olbBranch.getSelectedIndex()).trim());
//				temp.setQuerryString("branch");
//				filtervec.add(temp);
//			}
			
			temp=new Filter();
			temp.setList(AppUtility.getbranchlist());
			temp.setQuerryString("branch IN");
			filtervec.add(temp);
			
		}
		else
		{
			if(olbBranch.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbBranch.getValue().trim());
				temp.setQuerryString("branch");
				filtervec.add(temp);
			}
		}
		
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("cinfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("cinfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
			  temp=new Filter();
			  temp.setLongValue(personInfo.getCellValue());
			  temp.setQuerryString("cinfo.cellNumber");
			  filtervec.add(temp);
		  }
		if(tbQuotationId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(tbQuotationId.getValue());
			filtervec.add(temp);
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		if(tbLeadId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(tbLeadId.getValue());
			filtervec.add(temp);
			temp.setQuerryString("leadCount");
			filtervec.add(temp);
		}
		
		if(tbContractId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(tbContractId.getValue());
			filtervec.add(temp);
			temp.setQuerryString("contractCount");
			filtervec.add(temp);
		}
		
		
		if(olbCreatedBy.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbCreatedBy.getValue());
			temp.setQuerryString("createdBy");
			filtervec.add(temp);
		}
		
		if(olbQuotationGroup.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbQuotationGroup.getValue().trim());
			temp.setQuerryString("group");
			filtervec.add(temp);
		}
		
		if(olbQuotationCategory.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbQuotationCategory.getValue().trim());
			temp.setQuerryString("category");
			filtervec.add(temp);
		}
		
		if(olbQuotationType.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbQuotationType.getValue().trim());
			temp.setQuerryString("type");
			filtervec.add(temp);
		}
		
		if(olbQuotationStatus.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbQuotationStatus.getValue(olbQuotationStatus.getSelectedIndex()).trim());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		
		if(olbQuotationPriority.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbQuotationPriority.getValue().trim());
			temp.setQuerryString("priority");
			filtervec.add(temp);
		}
		
		if(tbTicketNumber.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(tbTicketNumber.getValue());
			filtervec.add(temp);
			temp.setQuerryString("ticketNumber");
			filtervec.add(temp);
		}
		
		//  rohan added this code 
		if(dbQuotationDate.getValue()!= null)
		{
			temp=new Filter();
			temp.setDateValue(dbQuotationDate.getValue());
			filtervec.add(temp);
			temp.setQuerryString("quotationDate");
			filtervec.add(temp);
		}
		/** date 11/10/2017 added by komal for followup date **/
		if(dateComparatorFollowUp.getValue()!=null)
		{
			filtervec.addAll(dateComparatorFollowUp.getValue());
		}
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Quotation());
		return querry;
	}


	@Override
	public boolean validate() {
//		 if(LoginPresenter.branchRestrictionFlag)
//		{
//			   /**
//				 * Date 19-01-2019 By Vijay
//				 * Des :- NBHC CCPM when zonal head login then if he selected sales person name in the search filter
//				 * then branch restriction not required 
//				 */
//				boolean flag = true;
//				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "EnableSalesPersonAsPerReportingTo")
//						&& LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Zonal Head")){
//					if(olbEmployee.getSelectedIndex()!=0){
//						flag = false;
//					}
//				}
//				/**
//				 * ends here
//				 */
//				
//				if(flag && olbBranch.getSelectedIndex()==0)
//				{
//					showDialogMessage("Select Branch");
//					return false;
//				}
//		}	
		 
	 	/**
		 * Date 12-01-2018 By Vijay 
		 * for Date combination validation 
		 */
		String msg = "Wrong filter selection with From Date and To Date ! Please select proper filters with Date";
		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null){
			if(personInfo.getIdValue()!=-1	||
				    !(personInfo.getFullNameValue().equals("")) || personInfo.getCellValue()!=-1l ||
				    tbLeadId.getValue()!=null || tbContractId.getValue()!=null ||olbCreatedBy.getSelectedIndex()!=0 ||
		    		olbQuotationCategory.getSelectedIndex()!=0 ||olbQuotationType.getSelectedIndex()!=0 || 
		    		olbQuotationPriority.getSelectedIndex()!=0 || tbTicketNumber.getValue()!=null || 
		    		dbQuotationDate.getValue()!= null || tbQuotationId.getValue()!=null){
					showDialogMessage(msg);
					return false;
			}
		}
		if(dateComparatorFollowUp.getFromDate().getValue()!=null || dateComparatorFollowUp.getToDate().getValue()!=null){
			
			if(olbEmployee.getSelectedIndex()!=0 || personInfo.getIdValue()!=-1	||
				    !(personInfo.getFullNameValue().equals("")) || personInfo.getCellValue()!=-1l ||
				    tbLeadId.getValue()!=null || tbContractId.getValue()!=null ||olbCreatedBy.getSelectedIndex()!=0 ||
		    		olbQuotationCategory.getSelectedIndex()!=0 ||olbQuotationType.getSelectedIndex()!=0 || 
		    		olbQuotationPriority.getSelectedIndex()!=0 || tbTicketNumber.getValue()!=null || 
		    		dbQuotationDate.getValue()!= null || tbQuotationId.getValue()!=null){
					showDialogMessage(msg);
					return false;
				}
		}
		/**
		 * ends here
		 */
		
		
		//Ashwini Patil Date:25-01-2024 		

		boolean filterAppliedFlag=false;
		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null||
				dateComparatorFollowUp.getFromDate().getValue()!=null || dateComparatorFollowUp.getToDate().getValue()!=null||
				olbBranch.getSelectedIndex()!=0 ||olbQuotationStatus.getSelectedIndex()!=0 ||olbQuotationGroup.getSelectedIndex()!=0 ||
				olbEmployee.getSelectedIndex()!=0 || personInfo.getIdValue()!=-1	|| !(personInfo.getFullNameValue().equals("")) || personInfo.getCellValue()!=-1l ||
			    tbLeadId.getValue()!=null || tbContractId.getValue()!=null ||olbCreatedBy.getSelectedIndex()!=0 ||
	    		olbQuotationCategory.getSelectedIndex()!=0 ||olbQuotationType.getSelectedIndex()!=0 || 
	    		olbQuotationPriority.getSelectedIndex()!=0 || tbTicketNumber.getValue()!=null || 
	    		dbQuotationDate.getValue()!= null || tbQuotationId.getValue()!=null){
			
							filterAppliedFlag=true;
		}
						
		if(!filterAppliedFlag){
				showDialogMessage("Please apply at least one filter!");
				return false;
		}
					
		if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)){
				int diffdays = AppUtility.getDifferenceDays(dateComparator.getFromDate().getValue(), dateComparator.getToDate().getValue());
				Console.log("diffdays "+diffdays);
				if(diffdays>31){
						showDialogMessage("Please select from date and to date range within 30 days");
						return false;
				}
		}
		if((dateComparatorFollowUp.getFromDate().getValue()!=null && dateComparatorFollowUp.getToDate().getValue()!=null)){
					int diffdays = AppUtility.getDifferenceDays(dateComparatorFollowUp.getFromDate().getValue(), dateComparatorFollowUp.getToDate().getValue());
					Console.log("diffdays "+diffdays);
					if(diffdays>31){
						showDialogMessage("Please select from date and to date range within 30 days");
						return false;
					}
		}
		
		
		return super.validate();
	}
	
	
}
