package com.slicktechnologies.client.views.quotation;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.Quotation;

public interface QuotationServiceAsync 
{
	 public void changeStatus(Quotation q,AsyncCallback<Void>callback);
}
