package com.slicktechnologies.client.views.quotation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.AdvanceSchedulingPopup;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

public class SalesLineItemQuotationTable extends SuperTable<SalesLineItem> implements ClickHandler {
	Column<SalesLineItem, String> deleteColumn;
	Column<SalesLineItem, String> vatColumn, serviceColumn, totalColumn;
	TextColumn<SalesLineItem> prodCategoryColumn, prodCodeColumn;
	Column<SalesLineItem, Date> dateColumn;
	Column<SalesLineItem, String> priceColumn;
	Column<SalesLineItem, String> prodPercentDiscColumn;
	Column<SalesLineItem, String> prodQtyColumn, durationColumn,
	noServicesColumn, servicingTime, discountAmt, premisesDetails;
	/**
	 * rohan added this code for name editable in quotation Date : 06-04-2017
	 */

	Column<SalesLineItem, String> prodNameColumn;
	TextColumn<SalesLineItem> viewprodQtyColumn, viewdurationColumn,
			viewnoServicesColumn, viewdColumn, viewdiscountAmt,
			viewpremisesDetails, viewPriceColumn, viewprodPercDiscColumn,
			viewServicingTime;

	TextColumn<SalesLineItem> productSrNo;
	TextColumn<SalesLineItem> viewServiceTaxColumn;
	TextColumn<SalesLineItem> viewVatTaxColumn;

	TextColumn<SalesLineItem> productName;

	/************
	 * rohan for area wise ******** date : 16/12/2016 this is used for area wise
	 * calculation of net payable
	 **/

	Column<SalesLineItem, String> prodAreaColumn;
	TextColumn<SalesLineItem> viewProdAreaColumn;

	public ArrayList<TaxDetails> vattaxlist;

	public ArrayList<TaxDetails> servicetaxlist;
	public ArrayList<String> list;
	public ArrayList<String> vatlist;

	/**
	 * Nidhi Date : 20-06-2017 By Adding button branches,which will be used to
	 * map branches in services shown on the schedule popup
	 * 
	 */

	Column<SalesLineItem, String> getBranchButtonColumn;
	AdvanceSchedulingPopup advSchPopup = new AdvanceSchedulingPopup();
	PopupPanel panel = new PopupPanel();
	int rowIndex = 0;

	/**
	 * End
	 */

	/*
	 * Nidhi 22-06-2017
	 */
	int productSrNoCount = 0;
	/*
	 * End
	 */
	
	int productIdCount = 0;
	public static boolean newModeFlag = false;
	public boolean disablecolumn=false;
	NumberFormat nf = NumberFormat.getFormat("0.00");
	/**
	 * Date :08-08-2017 By ANIL
	 * adding service end date
	 */
	Column<SalesLineItem,Date> serviceEndDateCol;
	TextColumn<SalesLineItem> viewServiceEndDateCol;
	/**
	 * End
	 */
	
	
	/**
	 * Date 13-jun-2018
	 * Developer :- Vijay
	 * Des :- Inclusive tax calculation
	 * Requirements :- Neatedge services 
	 */
	
	Column<SalesLineItem,Boolean> getColIsTaxInclusive;
	TextColumn<SalesLineItem> viewColIsTaxInclusive;
	GWTCAlert alert = new GWTCAlert();

	/**
	 * ends here
	 */
	/**
	 * nidhi |*| 9-01-2019
	 */
	public QuotationForm form = null;
	
	/**
	 * @author Anil ,Date : 16-11-2019
	 * Make price column non editable and make editable only price is zero
	 * raised by rahul tiwari for orkins
	 */
	boolean disablePriceCol=false;
	
	/**
	 * @author Anil
	 * @since 30-06-2020
	 * Adding service wise total amount calculation flag
	 */
	Column<SalesLineItem,Boolean> editServiceWiseRateCol;
	TextColumn<SalesLineItem> viewServiceWiseRateCol;
	
	Column<SalesLineItem, String> remark;
	TextColumn<SalesLineItem> viewremark;
	
	public SalesLineItemQuotationTable() {
		super();
		/*
		 * Nidhi Date : 20-06-2017
		 */
		advSchPopup.getBtnOk().addClickHandler(this);
		advSchPopup.getBtnCancel().addClickHandler(this);
		/*
		 * End
		 */
		table.setHeight("300px");
	}

	public SalesLineItemQuotationTable(UiScreen<SalesLineItem> view) {
		super(view);
		/*
		 * Nidhi Date : 20-06-2017
		 */
		advSchPopup.getBtnOk().addClickHandler(this);
		advSchPopup.getBtnCancel().addClickHandler(this);
		/*
		 * End
		 */
		table.setHeight("300px");
	}

	

	public void createTable() {
		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot(
				"Quotation", "MakeColumnDisable")) {
			disablePriceCol=true;
			Console.log("dis flag value : "+disablePriceCol);
		}
		if (newModeFlag == false) {
			// createColumnprodCategoryColumn();
			// createColumnprodSrNoColumn();
			
			createColumnprodNameColumn();
			createColumndateColumn();
			/**
			 * Date : 08-08-2017 By ANIL
			 */
			createColumnServiceEndDateColumn();
			/**
			 * End
			 */
			
			createColumndurationColumn();
			createColumnnoServicesColumn();
			
			
			// createViewQtyColumn();
			createColumnprodAreaColumn();
			createColumnPremisesDetails();
			
			createColumnRemark();
		
			editServiceWiseRateCol();	
//			if (AppUtility.checkForProcessConfigurartionIsActiveOrNot(
//					"Quotation", "MakeColumnDisable")) {
//				createViewColumnpriceColumn();
//			} else {
				createColumnpriceColumn();
//			}
			createColumnPercDiscount();
			createColumnDiscountAmt();
			createColumntotalColumn();
			
			retriveServiceTax();

			// createColumnvatColumn();
			// createColumnserviceColumn();
			// createColumnPercDiscount();
			// createColumntotalColumn();
			// createColumndeleteColumn();
			// addFieldUpdater();
			table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		}
		/**
		 *  nidhi
		 *  02-08-2017
		 *  For set table size auto scroll and fit data in table.
		 */
		table.setWidth("auto");
		/*
		 *  end
		 */
	}
	
	

	/**
	 * Date : 08-08-2017 BY ANIL
	 */
	
	private void createColumnServiceEndDateColumn() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		serviceEndDateCol = new Column<SalesLineItem, Date>(date) {
			@Override
			public Date getValue(SalesLineItem object) {
				if (object.getEndDate() != null) {
					return fmt.parse(fmt.format(object.getEndDate()));
				} else {
					Date date=CalendarUtil.copyDate(object.getStartDate());
					CalendarUtil.addDaysToDate(date,object.getDuration()-1);
					object.setEndDate(date);
					return fmt.parse(fmt.format(date));
				}
			}
		};
		table.addColumn(serviceEndDateCol, "#End Date");
		table.setColumnWidth(serviceEndDateCol, 100, Unit.PX);
		
	}
	
	
	protected void addFieldUpdaterOnServiceEndDateColumn()
	{
		serviceEndDateCol.setFieldUpdater(new FieldUpdater<SalesLineItem, Date>() {
			@Override
			public void update(int index, SalesLineItem object, Date value) {
				object.setEndDate(value);
				int duration=CalendarUtil.getDaysBetween(object.getStartDate(), object.getEndDate());
				duration++;
				object.setDuration(duration);
				System.out.println("Updated End Date : "+value+" ST DATE : "+object.getStartDate()+" EN DATE : "+object.getEndDate());
				System.out.println("Contract Duration : "+duration+"  Dur "+object.getDuration());
				productIdCount = object.getPrduct().getCount();
				productSrNoCount = object.getProductSrNo();
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}
	
	protected void createViewColumnServiceEndDateColumn() {
		viewServiceEndDateCol = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getEndDate() != null) {
					return AppUtility.parseDate(object.getEndDate());
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(viewServiceEndDateCol, "#End Date");
		table.setColumnWidth(viewServiceEndDateCol, 100, Unit.PX);
	}
	/**
	 * End
	 */

	private void createColumnprodAreaColumn() {
		EditTextCell editCell = new EditTextCell();

		prodAreaColumn = new Column<SalesLineItem, String>(editCell) {

			@Override
			public String getValue(SalesLineItem object) {
				if (object.getArea() != null) {
					return object.getArea();
				} else {
					return "";
				}

			}
		};

		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","OnlyForNBHC"))
		{
			table.addColumn(prodAreaColumn,"#Unit");
		}else{
//			table.addColumn(prodAreaColumn,"#Area");
			table.addColumn(prodAreaColumn,"#Qty");
		}
		table.setColumnWidth(prodAreaColumn, 100, Unit.PX);

	}

	private void createColumnprodSrNoColumn() {

		productSrNo = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductSrNo() + "";
			}
		};
		table.addColumn(productSrNo, "Prod SrNo");
		table.setColumnWidth(productSrNo, 100, Unit.PX);
	}

	private void createColumnPremisesDetails() {
		EditTextCell editCell = new EditTextCell();
		premisesDetails = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getPremisesDetails() != null) {
					return object.getPremisesDetails() + "";
				} else {
					return "";
				}
			}
		};
		table.addColumn(premisesDetails, "#Premises");
		table.setColumnWidth(premisesDetails, 100, Unit.PX);
	}

	private void retriveServiceTax() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c = new Company();
		System.out.println("Company Id :: " + c.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);

		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());

		service.getSearchResult(query,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
					}

					public void onSuccess(ArrayList<SuperModel> result) {

						System.out.println("result size of tax " + result);
						list = new ArrayList<String>();
						servicetaxlist = new ArrayList<TaxDetails>();
						vattaxlist = new ArrayList<TaxDetails>();
						vatlist = new ArrayList<String>();
						List<TaxDetails> backlist = new ArrayList<TaxDetails>();

						for (SuperModel model : result) {
							TaxDetails entity = (TaxDetails) model;

							backlist.add(entity);
							servicetaxlist.add(entity);
							vattaxlist.add(entity);
						}

						for (int i = 0; i < backlist.size(); i++) {
//							if (backlist.get(i).getServiceTax().equals(true)) {
								list.add(backlist.get(i).getTaxChargeName());
								System.out.println("list size" + list.size());
//							} else if (backlist.get(i).getVatTax().equals(true)) {
								vatlist.add(backlist.get(i).getTaxChargeName());
								System.out.println("valist size"
										+ vatlist.size());
//							}
						}

						addEditColumnVatTax();
						addEditColumnServiceTax();
						
						/**
						 * Date 06-jun-2018
						 * Developer :- Vijay
						 * Des :- Inclusive tax calculation
						 */
						getColIsTaxInclusive();
						/**
						 * END
						 */
						
						createColumnServicingTimeColumn();

						/**
						 * Nidhi Date : 20-06-2017 By
						 */
						getBranchButtonColumn();

						createColumnprodQtyColumn();
						createColumnprodCodeColumn();
						createColumndeleteColumn();
						addFieldUpdater();
					}
				});
	}

	
	/**
	 * Date 13-jun-2018
	 * Developer :- Vijay
	 * Des :- Inclusive tax calculation
	 * Requirements :- Neatedge services 
	 */
	
	private void getColIsTaxInclusive() {
		
		
		CheckboxCell cell=new CheckboxCell();
		getColIsTaxInclusive=new Column<SalesLineItem, Boolean>(cell) {
			@Override
			public Boolean getValue(SalesLineItem object) {
				
				if(object.getVatTax()!=null&&object.getServiceTax()!=null){
					if(object.getVatTax().isInclusive()==true&&object.getServiceTax().isInclusive()==true){
						object.setInclusive(true);
					}else if(object.getVatTax().isInclusive()==false&&object.getServiceTax().isInclusive()==false){
						object.setInclusive(false);
					}else{
						object.setInclusive(false);
					}
				}
				
				return object.isInclusive();
			}
		};
		table.addColumn(getColIsTaxInclusive, "Is Inclusive");
		table.setColumnWidth(getColIsTaxInclusive,80,Unit.PX);
		
		getColIsTaxInclusive.setFieldUpdater(new FieldUpdater<SalesLineItem, Boolean>() {
			@Override
			public void update(int index, SalesLineItem object, Boolean value) {
				object.setInclusive(value);
				object.getServiceTax().setInclusive(value);
				object.getVatTax().setInclusive(value);
				
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redraw();
				
				setEnable(true);
			}
		});
	}
	
	private void viewColIsTaxInclusive() {
		viewColIsTaxInclusive=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				// TODO Auto-generated method stub
				
				if(object.isInclusive()==true){
					return "Yes";
				}else{
					return "No";
				}
			}
		};
		table.addColumn(viewColIsTaxInclusive, "Is Inclusive");
		table.setColumnWidth(viewColIsTaxInclusive,80,Unit.PX);
	}
	/**
	 * ends here
	 */
	
	// ***************discount Amt column ***************

	private void createColumnDiscountAmt() {
		EditTextCell editCell = new EditTextCell();
		discountAmt = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getDiscountAmt() == 0) {
					return 0 + "";
				} else {
					return object.getDiscountAmt() + "";
				}
			}
		};
		table.addColumn(discountAmt, "#Disc Amt");
		table.setColumnWidth(discountAmt, 100, Unit.PX);
	}

	protected void createViewDiscountAmt() {
		viewdiscountAmt = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getDiscountAmt() == 0) {
					return 0 + "";
				} else {
					return object.getDiscountAmt() + "";
				}
			}
		};
		table.addColumn(viewdiscountAmt, "#Disc Amt");
		table.setColumnWidth(viewdiscountAmt, 100, Unit.PX);
	}

	// vat tax Selection List Column
	public void addEditColumnVatTax() {
		SelectionCell employeeselection = new SelectionCell(vatlist);
		vatColumn = new Column<SalesLineItem, String>(employeeselection) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getVatTax().getTaxConfigName() != null) {
					
					object.setVatTaxEdit(object.getVatTax().getTaxConfigName());

					return object.getVatTax().getTaxConfigName();
				} else
					return "N/A";
			}
		};
		table.addColumn(vatColumn, "#Tax 1");
		table.setColumnWidth(vatColumn, 100, Unit.PX);
	}

	public void updateVatTaxColumn() {

		vatColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {

			@Override
			public void update(int index, SalesLineItem object, String value) {
				try {
					String val1 = (value.trim());
					object.setVatTaxEdit(val1);
					Tax tax = object.getVatTax();
					for (int i = 0; i < vattaxlist.size(); i++) {
						if (val1.trim().equals(
								vattaxlist.get(i).getTaxChargeName())) {
							tax.setTaxName(val1);
							tax.setPercentage(vattaxlist.get(i)
									.getTaxChargePercent());
							//Date 17-08-2017 added by vijay for GST
							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());

							/**
							 * Date : 03-08-2017 By ANIL
							 */
							tax.setTaxConfigName(val1);
							/**
							 * End
							 */
							/**
							 * nidhi
							 * 8-06-2018
							 * for tax selection option
							 */
//							checkTaxValue(tax,object);
							Tax taxDt = AppUtility.checkTaxValue(tax, object.getServiceTax(), vattaxlist);
							if(taxDt != null){
								object.setServiceTax(taxDt);
							}
							/**
							 * end
							 */
							/**
							 * Date 28-07-2018 By Vijay
							 * Des :- If we select IGST then second tax box will be disable.if we select other than IGST then second tax box will be visible
							 */
							if(vattaxlist.get(i).getTaxPrintName().equals("IGST") || vattaxlist.get(i).getTaxPrintName().equals("IGST@18") || vattaxlist.get(i).getTaxPrintName().equals("SEZ")){
								serviceColumn.setCellStyleNames("hideVisibility");
							}else{
								serviceColumn.setCellStyleNames("showVisibility");
							}
							/**
							 * ends here
							 */
							break;
						}
					}
					object.setVatTax(tax);
					// Nidhi 22-06-2017
					productIdCount = object.getPrduct().getCount();

					productSrNoCount = object.getProductSrNo();
					RowCountChangeEvent.fire(table, getDataprovider().getList()
							.size(), true);

				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	private void createColumnServicingTimeColumn() {
		EditTextCell editCell = new EditTextCell();
		servicingTime = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getServicingTIme() == 0) {
					return 0 + "";
				} else {
					return object.getServicingTIme() + "";
				}
			}
		};
		table.addColumn(servicingTime, "#Ser.Time");
		table.setColumnWidth(servicingTime, 100, Unit.PX);

	}

	private void createViewColumnServicingTimeColumn() {
		viewServicingTime = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getServicingTIme() == 0) {
					return "N.A" + "";
				} else {
					return object.getServicingTIme() + "";
				}
			}
		};
		table.addColumn(viewServicingTime, "Ser.Time");
		table.setColumnWidth(viewServicingTime, 100, Unit.PX);
	}

	// service tax Selection List Column
	public void addEditColumnServiceTax() {
		SelectionCell employeeselection = new SelectionCell(list);
		serviceColumn = new Column<SalesLineItem, String>(employeeselection) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getServiceTax().getTaxConfigName() != null) {
					
					object.setServiceTaxEdit(object.getServiceTax().getTaxConfigName());

					return object.getServiceTax().getTaxConfigName();
				} else
					return "N/A";
			}

		};
		table.addColumn(serviceColumn, "#Tax 2");
		table.setColumnWidth(serviceColumn, 100, Unit.PX);
	}

	public void updateServiceTaxColumn() {

		serviceColumn
				.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {

					@Override
					public void update(int index, SalesLineItem object,
							String value) {
						try {
							String val1 = (value.trim());
							object.setServiceTaxEdit(val1);
							Tax tax = object.getServiceTax();
							for (int i = 0; i < servicetaxlist.size(); i++) {
								if (val1.trim().equals(
										servicetaxlist.get(i)
												.getTaxChargeName())) {
									tax.setTaxName(val1);
									tax.setPercentage(servicetaxlist.get(i)
											.getTaxChargePercent());
									//Date 17-08-2017 added by vijay
									tax.setTaxPrintName(servicetaxlist.get(i).getTaxPrintName());

									/**
									 * Date : 03-08-2017 By ANIL
									 */
									tax.setTaxConfigName(val1);
									/**
									 * End
									 */
									/**
									 * nidhi
									 * 8-06-2018
									 * 
									 */
									//checkServiceTaxValue(tax, object);
//									Tax vatTax =AppUtility.checkse
									Tax vatTax = AppUtility.checkTaxValue(tax, object.getVatTax(), vattaxlist);
									if(vatTax!=null){
										object.setVatTax(vatTax);
									}
									/**
									 * end
									 */
									/**
									 * Date 28-07-2018 By Vijay
									 * Des :- If we select IGST then second tax box will be disable.if we select other than IGST then second tax box will be visible
									 */
									
									if(servicetaxlist.get(i).getTaxPrintName().equals("IGST") || servicetaxlist.get(i).getTaxPrintName().equals("IGST@18") || servicetaxlist.get(i).getTaxPrintName().equals("SEZ")){
										vatColumn.setCellStyleNames("hideVisibility");
									}else{
										vatColumn.setCellStyleNames("showVisibility");
									}
									/**
									 * ends here
									 */
									break;
								}
							}
							object.setServiceTax(tax);
							productIdCount = object.getPrduct().getCount();
							productSrNoCount = object.getProductSrNo();
							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);

						} catch (NumberFormatException e) {
						}
						table.redrawRow(index);
					}
				});
	}

	protected void createViewServiceTaxColumn() {

		viewServiceTaxColumn = new TextColumn<SalesLineItem>() {
			public String getValue(SalesLineItem object) {
				return object.getServiceTaxEdit();
			}
		};
		table.addColumn(viewServiceTaxColumn, "#Tax 2");
		table.setColumnWidth(viewServiceTaxColumn, 100, Unit.PX);
	}

	protected void createViewVatTaxColumn() {

		viewVatTaxColumn = new TextColumn<SalesLineItem>() {
			public String getValue(SalesLineItem object) {
				return object.getVatTaxEdit();
			}
		};
		table.addColumn(viewVatTaxColumn, "#Tax 1");
		table.setColumnWidth(viewVatTaxColumn, 100, Unit.PX);
	}

	protected void createColumnprodCategoryColumn() {
		prodCategoryColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(prodCategoryColumn, "Category");
		table.setColumnWidth(prodCategoryColumn, 100, Unit.PX);

	}

	protected void createColumnprodCodeColumn() {
		prodCodeColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductCode();
			}
		};
		table.addColumn(prodCodeColumn, "PCode");
		table.setColumnWidth(prodCodeColumn, 100, Unit.PX);

	}

	protected void createColumnprodNameColumn() {
		EditTextCell editCell = new EditTextCell();
		prodNameColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getProductName();
			}
		};
		table.addColumn(prodNameColumn, "#Name");
		table.setColumnWidth(prodNameColumn, 100, Unit.PX);

	}

	protected void createColumnprodQtyColumn() {
		EditTextCell editCell = new EditTextCell();
		prodQtyColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getQty() == 0) {
					return 0 + "";
				} else {
					return object.getQty() + "";
				}
			}
		};
		table.addColumn(prodQtyColumn, "#No of Branches");
		table.setColumnWidth(prodQtyColumn, 100, Unit.PX);
	}

	protected void createViewQtyColumn() {
		viewprodQtyColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getQty() == 0) {
					return 0 + "";
				} else {
					return object.getQty() + "";
				}
			}
		};
		table.addColumn(viewprodQtyColumn, "#No of Branches");
		table.setColumnWidth(viewprodQtyColumn, 100, Unit.PX);
	}

	protected void createColumnPercDiscount() {
		EditTextCell editCell = new EditTextCell();
		prodPercentDiscColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getPercentageDiscount() == 0
						|| object.getPercentageDiscount() == null) {
					return 0 + "";
				} else {
					return object.getPercentageDiscount() + "";
				}
			}
		};
		table.addColumn(prodPercentDiscColumn, "#% Disc");
		table.setColumnWidth(prodPercentDiscColumn, 100, Unit.PX);
	}

	protected void createViewPercDiscount() {

		viewprodPercDiscColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getPercentageDiscount() == 0) {
					return 0 + "";
				} else {
					return object.getPercentageDiscount() + "";
				}
			}
		};
		table.addColumn(viewprodPercDiscColumn, "#% Disc");
		table.setColumnWidth(viewprodPercDiscColumn, 100, Unit.PX);
	}

	protected void createColumndateColumn() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		dateColumn = new Column<SalesLineItem, Date>(date) {
			@Override
			public Date getValue(SalesLineItem object) {
				if (object.getStartDate() != null) {
					return fmt.parse(fmt.format(object.getStartDate()));
				} else {
					object.setStartDate(new Date());
					return fmt.parse(fmt.format(new Date()));
				}
			}
		};
		table.addColumn(dateColumn, "#Start Date");
		table.setColumnWidth(dateColumn, 100, Unit.PX);
	}

	protected void createViewColumndateColumn() {
		viewdColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getStartDate() != null) {

					return AppUtility.parseDate(object.getStartDate());
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(viewdColumn, "Start Date");
		table.setColumnWidth(viewdColumn, 100, Unit.PX);
	}

	protected void createColumndurationColumn() {
		EditTextCell editCell = new EditTextCell();
		durationColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getDuration() == 0) {
					return 0 + "";
				} else {
					return object.getDuration() + "";
				}
			}
		};
		table.addColumn(durationColumn, "#Duration");
		table.setColumnWidth(durationColumn, 100, Unit.PX);
	}

	protected void createColumnViewDuration() {

		viewdurationColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getDuration() == 0)
					return "N.A";
				else {
					return object.getDuration() + "";
				}
			}
		};
		table.addColumn(viewdurationColumn, "Duration");
		table.setColumnWidth(viewdurationColumn, 100, Unit.PX);
	}

	protected void createColumnnoServicesColumn() {
		EditTextCell editCell = new EditTextCell();
		noServicesColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getNumberOfServices() == 0) {
					return 0 + "";
				} else {
					return object.getNumberOfServices() + "";
				}
			}
		};
		table.addColumn(noServicesColumn, "#Services");
		table.setColumnWidth(noServicesColumn, 100, Unit.PX);
	}

	protected void createViewColumnnoServicesColumn() {

		viewnoServicesColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getNumberOfServices() == 0) {
					return "N.A" + "";
				} else {
					return object.getNumberOfServices() + "";
				}
			}
		};
		table.addColumn(viewnoServicesColumn, "Services");
		table.setColumnWidth(viewnoServicesColumn, 100, Unit.PX);
	}

	protected void createColumnpriceColumn() {
		EditTextCell editCell = new EditTextCell();
		priceColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				SuperProduct product = object.getPrduct();
				double tax = removeAllTaxes(product);
				double origPrice = object.getPrice() - tax;
				return nf.format(origPrice);
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "MakePriceColumnDisable")){
					disablePriceCol=true;
				}
				Console.log("dis flag : "+disablePriceCol+" price : "+object.getPrice());
				if(disablePriceCol==false||(disablePriceCol==true&&object.getPrice()==0)){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(priceColumn, "#Price");
		table.setColumnWidth(priceColumn, 100, Unit.PX);
	}

	protected void createViewColumnpriceColumn() {

		viewPriceColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getPrice() == 0) {
					return 0 + "";
				} else {
					SuperProduct product = object.getPrduct();
					double tax = removeAllTaxes(product);
					double origPrice = object.getPrice() - tax;

					return nf.format(origPrice);
				}
			}
		};
		table.addColumn(viewPriceColumn, "#Price");
		table.setColumnWidth(viewPriceColumn, 100, Unit.PX);
	}

	protected void createColumnvatColumn() {
		TextCell editCell = new TextCell();
		vatColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getVatTax() != null) {
					return object.getVatTax().getPercentage() + "";
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(vatColumn, "VAT");
		table.setColumnWidth(vatColumn, 100, Unit.PX);
	}

	protected void createColumnserviceColumn() {
		TextCell editCell = new TextCell();
		serviceColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getServiceTax() != null) {
					return object.getServiceTax().getPercentage() + "";
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(serviceColumn, "Ser. Tax");
		table.setColumnWidth(serviceColumn, 100, Unit.PX);
	}

	protected void createColumntotalColumn() {
		TextCell editCell = new TextCell();
		totalColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				double total = 0;
				SuperProduct product = object.getPrduct();
				double tax = removeAllTaxes(product);
				double origPrice = object.getPrice() - tax;
				
				/**
				 * @author Anil
				 * @since 30-06-2020
				 * For sai care by Vaishnavi and Nitin Sir
				 * If Area is null then total price will be multiplication of rate and no. of services
				 */
				if (object.isServiceRate()&&(object.getArea()==null||object.getArea().equals("")||object.getArea().equalsIgnoreCase("NA"))) {
					origPrice=origPrice*object.getNumberOfServices();
				}

				// *****************old code

				// if(object.getPercentageDiscount()==null){
				// total=origPrice*object.getQty();
				// }
				// if(object.getPercentageDiscount()!=null){
				// total=origPrice-(origPrice*object.getPercentageDiscount()/100);
				// total=total*object.getQty();
				// }

				// ************new code by rohan *******************

				/**
				 * rohan removed qty form all the price calculation required for
				 * NBHC date : 14-10-2016 i have removed (object.getQty()) from
				 * all the calculation
				 */

				if ((object.getPercentageDiscount() == null && object.getPercentageDiscount() == 0)&& (object.getDiscountAmt() == 0)) {

					System.out.println("inside both 0 condition");
					// total=origPrice;

					// new code
					/********************
					 * Square Area Calculation code added in if condition and
					 * without square area calculation code in else block
					 ***************************/
					System.out.println("Get value from area =="+ object.getArea());
					System.out.println("total amount before area calculation =="+ total);
					if (!object.getArea().equalsIgnoreCase("NA")) {
						double area = Double.parseDouble(object.getArea());
						total = origPrice * area;
						System.out.println(" Final TOTAL if no discount per & no discount Amt ==="+ total);
					} else {
						total = origPrice;
					}
				}
				else if ((object.getPercentageDiscount() != null)&& (object.getDiscountAmt() != 0)) {

					System.out.println("inside both not null condition");

					// total=origPrice-(origPrice*object.getPercentageDiscount()/100);
					// total=total-object.getDiscountAmt();
					// total=total*object.getQty();

					if (!object.getArea().equalsIgnoreCase("NA")) {
						double area = Double.parseDouble(object.getArea());
						total = origPrice * area;
//						System.out.println("total before discount per ===="+ total);
						total = total- (total * object.getPercentageDiscount() / 100);
//						System.out.println("after discount per total === "+ total);
						total = total - object.getDiscountAmt();
//						System.out.println("after discount AMT total === "+ total);
//						System.out.println(" Final TOTAL   discount per &  discount Amt ==="+ total);
					} else {
//						System.out.println(" normal === total " + total);
						total = origPrice;
						total = total- (total * object.getPercentageDiscount() / 100);
						total = total - object.getDiscountAmt();
					}

				} else {
					System.out.println("inside oneof the null condition");

					if (object.getPercentageDiscount() != null) {
						// System.out.println("inside getPercentageDiscount oneof the null condition");
						// total=origPrice-(origPrice*object.getPercentageDiscount()/100);

						if (!object.getArea().equalsIgnoreCase("NA")) {
							double area = Double.parseDouble(object.getArea());
							total = origPrice * area;
//							System.out.println("total before discount per ===="+ total);
							total = total- (total * object.getPercentageDiscount() / 100);
//							System.out.println("after discount per total === "+ total);
						} else {
//							System.out.println("old code");
							total = origPrice;
							total = total- (total * object.getPercentageDiscount() / 100);
						}
					} else {
						// System.out.println("inside getDiscountAmt oneof the null condition");
						// total=origPrice-object.getDiscountAmt();

						if (!object.getArea().equalsIgnoreCase("NA")) {
							double area = Double.parseDouble(object.getArea());
							total = origPrice * area;
//							System.out.println("total before discount amt ===="+ total);
							total = total - object.getDiscountAmt();
//							System.out.println("after discount amt total === "+ total);

						} else {
							total = total - object.getDiscountAmt();
						}
					}

					// total=total*object.getQty();

				}

				return nf.format(total);
			}
		};
		table.addColumn(totalColumn, "Total");
		table.setColumnWidth(totalColumn, 100, Unit.PX);
	}

	protected void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<SalesLineItem, String>(btnCell) {
			@Override
			public String getValue(SalesLineItem object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn, 100, Unit.PX);
	}

	/**
	 * Nidhi Date : 20-06-2017 BY
	 */

	protected void getBranchButtonColumn() {
		try {
			ButtonCell btnCell = new ButtonCell();

			System.out.println("Get here..!");
			getBranchButtonColumn = new Column<SalesLineItem, String>(btnCell) {
				@Override
				public String getValue(SalesLineItem object) {
					return "Branches";
				}
			};

			table.addColumn(getBranchButtonColumn, "Branches");
			table.setColumnWidth(getBranchButtonColumn, 100, Unit.PX);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	/**
	 * End Nidhi
	 * 
	 */
	protected void getBranchButtonColumn1() {
		ButtonCell btnCell = new ButtonCell();
		getBranchButtonColumn = new Column<SalesLineItem, String>(btnCell) {
			@Override
			public String getValue(SalesLineItem object) {
				return "Branches";
			}
		};
		table.addColumn(getBranchButtonColumn, "Branches");
		table.setColumnWidth(getBranchButtonColumn, 100, Unit.PX);
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterproductNameColumn();
		createFieldUpdaterprodQtyColumn();
		createFieldUpdaterdateColumn();
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", "MakeColumnDisable")){
//			createFieldUpdatePrice();
//		}else{
			createFieldUpdaterpriceColumn();	
//		}
		createFieldUpdaterPremisesColumn();
		createFieldUpdaterprodDurationColumn();
		createFieldUpdaterprodServices();
		createFieldUpdaterPercDiscColumn();
		createFieldUpdaterDiscAmtColumn();
		createFieldUpdaterdeleteColumn();

		createFieldUpdaterProdAreaColumn();
		updateVatTaxColumn();
		updateServiceTaxColumn();
		
		
		/*
		 * Nidhi 20-06-2017
		 */
		createFieldUpdaterOnBranchesColumn();
		
		addFieldUpdaterOnServiceEndDateColumn();
		
		addFieldUpdaterTotal();
		
		addFieldUpdaterRemark();
	}

	
	private void addFieldUpdaterRemark() {

		remark.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				// TODO Auto-generated method stub
				object.setRemark(value);
				table.redrawRow(index);
			}
		});
	}

	private void createFieldUpdatePrice() {
		viewPriceColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object, String value) {
				try {
					Double val1 = Double.parseDouble(value.trim());

					object.setPrice(val1);
					productIdCount = object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					RowCountChangeEvent.fire(table, getDataprovider().getList()
							.size(), true);

				} catch (Exception e) {
				}
				table.redrawRow(index);
				// added by vijay for reverse calculation impact
				setEnable(true); 
			}
		});
	}

	/**
	 * Date 13-jun-2018
	 * Developer :- Vijay
	 * Des :- Reverse calculation with area(Unit)
	 * Requirements :- Neatedge services 
	 */
	
	private void addFieldUpdaterTotal() {

		totalColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				double oldtotalAmt = object.getTotalAmount();
				double totalAmt = 0;
				try {
					totalAmt = Double.parseDouble(value);
					object.setTotalAmount(totalAmt);
				} catch (NumberFormatException e) {
					alert.alert("Only Numric values are allowed !");
					object.setTotalAmount(oldtotalAmt);
				}
				
				try {
					double area = Double.parseDouble(object.getArea());
					double price = totalAmt/area;
					object.setPrice(price);
						
				} catch (Exception e) {
					alert.alert("Please Define Area first for reverse calculation with Area!");
					object.setTotalAmount(object.getTotalAmount());
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
				setEnable(true);
			}
		});
	}
	
	/**
	 * ends here
	 */
	protected void createFieldUpdaterproductNameColumn() {
		prodNameColumn
				.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, SalesLineItem object,
							String value) {

						try {
							String val1 = value.trim();
							object.setProductName(val1);
							productIdCount = object.getPrduct().getCount();
							// nidhi 22-06-2017
							productSrNoCount = object.getProductSrNo();
							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);

						} catch (NumberFormatException e) {

						}
						table.redrawRow(index);
					}
				});
	}

	private void createFieldUpdaterProdAreaColumn() {
		prodAreaColumn
				.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {

					@Override
					public void update(int index, SalesLineItem object,
							String value) {

						try {
							// Double val1=Double.parseDouble(value.trim());
							object.setArea(value);
							productIdCount = object.getPrduct().getCount();
							productSrNoCount = object.getProductSrNo();

							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);
						} catch (NumberFormatException e) {
							System.out.println(e.getMessage());
						}
						table.redrawRow(index);
						setEnable(true);
					}
				});

	}

	private void createFieldUpdaterPremisesColumn() {
		premisesDetails
				.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, SalesLineItem object,
							String value) {

						try {
							System.out.println("value" + value);
							object.setPremisesDetails(value);
						} catch (NumberFormatException e) {

						}
						table.redrawRow(index);
					}
				});
	}

	protected void createFieldUpdaterprodQtyColumn() {
		prodQtyColumn
				.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, SalesLineItem object,
							String value) {

						try {
							Double val1 = Double.parseDouble(value.trim());
							object.setQuantity(val1);
							productIdCount = object.getPrduct().getCount();
							productSrNoCount = object.getProductSrNo();
							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);

						} catch (NumberFormatException e) {

						}

						table.redrawRow(index);
					}
				});
	}

	protected void createFieldUpdaterdateColumn() {
		dateColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, Date>() {
			@Override
			public void update(int index, SalesLineItem object, Date value) {
				object.setStartDate(value);
				/**
				 * Date : 08-08-2017 By ANIL
				 */
				Date date=CalendarUtil.copyDate(object.getStartDate());
				CalendarUtil.addDaysToDate(date, object.getDuration()-1);	
				object.setEndDate(date);
				
				System.out.println("Updated Start Date : "+value+" ST DATE : "+object.getStartDate()+" EN DATE : "+object.getEndDate());
				/**
				 * End
				 */
				productIdCount = object.getPrduct().getCount();
				productSrNoCount = object.getProductSrNo();
				RowCountChangeEvent.fire(table, getDataprovider().getList()
						.size(), true);
				table.redrawRow(index);
			}
		});
	}

	protected void createFieldUpdaterPercDiscColumn() {
		prodPercentDiscColumn
				.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, SalesLineItem object,
							String value) {

						try {
							Double val1 = Double.parseDouble(value.trim());
							object.setPercentageDiscount(val1);
							productIdCount = object.getPrduct().getCount();
							productSrNoCount = object.getProductSrNo();

							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);

						} catch (NumberFormatException e) {

						}
						table.redrawRow(index);
					}
				});
	}

	protected void createFieldUpdaterprodDurationColumn() {
		durationColumn
				.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, SalesLineItem object,
							String value) {

						try {
							Integer val1 = Integer.parseInt(value.trim());
							object.setDuration(val1);
							/**
							 * Date : 09-08-2017 BY ANIL
							 */
							Date date=CalendarUtil.copyDate(object.getStartDate());
							CalendarUtil.addDaysToDate(date, object.getDuration()-1);
							object.setEndDate(date);
							/**
							 * End
							 */
							productIdCount = object.getPrduct().getCount();
							// nidhi 22-06-2017
							productSrNoCount = object.getProductSrNo();
							RowCountChangeEvent.fire(table, getDataprovider()
									.getList().size(), true);

						} catch (NumberFormatException e) {
						}
						table.redrawRow(index);
					}
				});
	}

	protected void createFieldUpdaterprodServices() {
		noServicesColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object,
					String value) {

				try {
					Integer val1 = Integer.parseInt(value.trim());
					object.setNumberOfService(val1);
					productIdCount = object.getPrduct().getCount();
					/**
					 * Date : 26-08-2017 By ANIL
					 * setting product serial no. while updating prod services
					 */
					productSrNoCount=object.getProductSrNo();
					/**
					 * End
					 */
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	protected void createFieldUpdaterpriceColumn() {
		priceColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object, String value) {
				try {
					Double val1 = Double.parseDouble(value.trim());

					object.setPrice(val1);
					productIdCount = object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					RowCountChangeEvent.fire(table, getDataprovider().getList()
							.size(), true);

				} catch (Exception e) {
				}
				table.redrawRow(index);
				// added by vijay for reverse calculation impact
				setEnable(true); 
			}
		});
	}

	/**
	 * Nidhi Date : 20-06-2017
	 */

	protected void createFieldUpdaterOnBranchesColumn() {
		getBranchButtonColumn
				.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
					@Override
					public void update(int index, SalesLineItem object,
							String value) {
						System.out.println("Click call..!");

//						/**
//						 * Nidhi Date : 20-06-2017
//						 */
//						String[] branchArray = new String[10];
//						if (object.getBranchSchedulingInfo() != null
//								&& !object.getBranchSchedulingInfo().equals("")) {
//							branchArray[0] = object.getBranchSchedulingInfo();
//						}
//						if (object.getBranchSchedulingInfo1() != null
//								&& !object.getBranchSchedulingInfo1()
//										.equals("")) {
//							branchArray[1] = object.getBranchSchedulingInfo1();
//						}
//						if (object.getBranchSchedulingInfo2() != null
//								&& !object.getBranchSchedulingInfo2()
//										.equals("")) {
//							branchArray[2] = object.getBranchSchedulingInfo2();
//						}
//						if (object.getBranchSchedulingInfo3() != null
//								&& !object.getBranchSchedulingInfo3()
//										.equals("")) {
//							branchArray[3] = object.getBranchSchedulingInfo3();
//						}
//						if (object.getBranchSchedulingInfo4() != null
//								&& !object.getBranchSchedulingInfo4()
//										.equals("")) {
//							branchArray[4] = object.getBranchSchedulingInfo4();
//						}
//
//						if (object.getBranchSchedulingInfo5() != null
//								&& !object.getBranchSchedulingInfo5()
//										.equals("")) {
//							branchArray[5] = object.getBranchSchedulingInfo5();
//						}
//						if (object.getBranchSchedulingInfo6() != null
//								&& !object.getBranchSchedulingInfo6()
//										.equals("")) {
//							branchArray[6] = object.getBranchSchedulingInfo6();
//						}
//						if (object.getBranchSchedulingInfo7() != null
//								&& !object.getBranchSchedulingInfo7()
//										.equals("")) {
//							branchArray[7] = object.getBranchSchedulingInfo7();
//						}
//						if (object.getBranchSchedulingInfo8() != null
//								&& !object.getBranchSchedulingInfo8()
//										.equals("")) {
//							branchArray[8] = object.getBranchSchedulingInfo8();
//						}
//						if (object.getBranchSchedulingInfo9() != null
//								&& !object.getBranchSchedulingInfo9()
//										.equals("")) {
//							branchArray[9] = object.getBranchSchedulingInfo9();
//						}
//
//						/**
//						 * End
//						 */
//						ArrayList<BranchWiseScheduling> branchSchedulingList = AppUtility
//								.getBranchSchedulingList(branchArray);
//						System.out.println("String Array : " + branchArray);
						
						/**
						 *  vijay on 02-05-2018
						 *  for scheduling based on customer branch. above old code commented 
						 */
						List<BranchWiseScheduling> branchSchedulingList = new ArrayList<BranchWiseScheduling>();
						branchSchedulingList = object.getCustomerBranchSchedulingInfo().get(object.getProductSrNo());
						/**
						 * ends here
						 */

						if (branchSchedulingList != null
								&& branchSchedulingList.size() != 0) {
							panel = new PopupPanel(true);
							advSchPopup.clear();
//							advSchPopup.getBranchTable().getDataprovider()
//									.getList().addAll(branchSchedulingList);
							advSchPopup.getBranchTable().getDataprovider().setList(branchSchedulingList);
							panel.add(advSchPopup);
							panel.center();
							panel.show();
							rowIndex = index;
							QuotationForm.addnewproductFlag = false;
							
						} else {
							GWTCAlert alert = new GWTCAlert();
							alert.alert("Customer branch is not defined.");
						}
					}
				});
	}

	/*
	 * End By Nidhi
	 */
	protected void createFieldUpdaterdeleteColumn() {
		deleteColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object, String value) {
				getDataprovider().getList().remove(object);
				productIdCount = 0;
				/**
				 * Date : 26-08-2017 BY ANIL
				 */
				productSrNoCount=0;
				/**
				 * End
				 */
				table.redrawRow(index);
			}
		});
	}

	public void addColumnSorting() {

	}

	@Override
	public void setEnable(boolean state) {

		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot(
				"Quotation", "MakeColumnDisable")) {
			disablePriceCol=true;
			Console.log("dis flag value : "+disablePriceCol);
		}
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);

		if (state == true) {
			// createColumnprodSrNoColumn();
			createColumnprodCategoryColumn();
			createColumnprodCodeColumn();
			createColumnprodNameColumn();
			
			createColumndateColumn();
			/**
      		 * Date : 09-08-2017 BY ANIL
      		 */
      		createColumnServiceEndDateColumn();
      		/**
      		 * End
      		 */
      		createColumndurationColumn();
			createColumnnoServicesColumn();
			createColumnprodQtyColumn();
			// createViewQtyColumn();
			createColumnprodAreaColumn();
			createColumnPremisesDetails();
			
			createColumnRemark();
			
			editServiceWiseRateCol();
//			if (AppUtility.checkForProcessConfigurartionIsActiveOrNot(
//					"Quotation", "MakeColumnDisable")) {
//				createViewColumnpriceColumn();
//			} else {
				createColumnpriceColumn();
//			}
			createColumnPercDiscount();
			createColumnDiscountAmt();
			createColumntotalColumn();
			retriveServiceTax();
			// createColumnvatColumn();
			// createColumnserviceColumn();
			// createColumnPercDiscount();
			// createColumntotalColumn();
			// createColumndeleteColumn();
			// addFieldUpdater();

		} else {
			// createColumnprodSrNoColumn();
			createColumnprodCategoryColumn();
			createColumnprodCodeColumn();
			createViewColumnprodNameColumn();
			
			createViewColumndateColumn();
			/**
      		 * Date :09-08-2017 By ANIL
      		 */
      		createViewColumnServiceEndDateColumn();
      		/**
      		 * ENd
      		 */
      		createColumnViewDuration();
			createViewColumnnoServicesColumn();
			createViewQtyColumn();
			createViewColumnProdAreaView();
			createViewColumnPremisesDetailsColumn();
			
			createViewColumnRemark();
			
			viewServiceWiseRateCol();
			
			createViewColumnpriceColumn();
			createViewPercDiscount();
			createViewDiscountAmt();
			createColumntotalColumn();
			createViewVatTaxColumn();
			createViewServiceTaxColumn();
			/**
      		 * Date :13-06-2018 By VIJAY
      		 */
			viewColIsTaxInclusive();
			/**
			 * ends here
			 */
			createViewColumnServicingTimeColumn();
			// createColumnvatColumn();
			// createColumnserviceColumn();
			
			// addFieldUpdater();
		

		}
		/**
		 *  nidhi
		 *  02-08-2017
		 *  For set table size auto scroll and fit data in table.
		 */
		table.setWidth("auto");
		/*
		 *  end
		 */
	}

	

	private void createViewColumnprodNameColumn() {

		productName = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getProductName() != null) {
					return object.getProductName();
				} else {

					return "";
				}
			}
		};
		table.addColumn(productName, "#Name");
		table.setColumnWidth(productName, 100, Unit.PX);
	}

	private void createViewColumnPremisesDetailsColumn() {
		viewpremisesDetails = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getPremisesDetails() != null) {
					return object.getPremisesDetails();
				} else {

					return "";
				}
			}
		};
		table.addColumn(viewpremisesDetails, "#Premises");
		table.setColumnWidth(viewpremisesDetails, 100, Unit.PX);

	}

	private void createViewColumnProdAreaView() {

		viewProdAreaColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getArea();
			}
		};
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","OnlyForNBHC"))
		{
			table.addColumn(viewProdAreaColumn,"#Unit");
		}else{
//			table.addColumn(viewProdAreaColumn,"#Area");//commented by Ashwini
			table.addColumn(viewProdAreaColumn,"#Qty");//Added by Ashwini
		}
		table.setColumnWidth(viewProdAreaColumn, 100, Unit.PX);
	}

	@Override
	public void applyStyle() {

	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<SalesLineItem>() {
			@Override
			public Object getKey(SalesLineItem item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}

	public double calculateTotalExcludingTax() {
		List<SalesLineItem> list = getDataprovider().getList();
		double sum = 0, priceVal = 0;
		for (int i = 0; i < list.size(); i++) {
			SalesLineItem entity = list.get(i);
			SuperProduct prod = entity.getPrduct();
			double taxAmt = removeAllTaxes(prod);

			// ********************old code
			// *************************************

			// if(entity.getPercentageDiscount()==null){
			// priceVal=entity.getPrice()-taxAmt;
			// priceVal=priceVal*entity.getQty();
			// sum=sum+priceVal;
			// }
			// if(entity.getPercentageDiscount()!=null){
			// priceVal=entity.getPrice()-taxAmt;
			// priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
			// priceVal=priceVal*entity.getQty();
			//
			// sum=sum+priceVal;
			// }

			// ******************new code for discount in quotation by rohan
			// ************

			/**
			 * rohan removed qty form all the price calculation required for
			 * NBHC date : 14-10-2016 i have removed (entity.getQty()) from all
			 * the calculation
			 * 
			 * rohan added area in calculation for universal pest and for all
			 * Date : 16/112/2016
			 *
			 */

			System.out.println("Square Area === " + entity.getArea());
			if ((entity.getPercentageDiscount() == null && entity
					.getPercentageDiscount() == 0)
					&& (entity.getDiscountAmt() == 0)) {

				System.out.println("inside both 0 condition");
				/********************
				 * Square Area Calculation code added in if condition and
				 * without square area calculation code in else block
				 ***************************/

				// priceVal=entity.getPrice()-taxAmt;
				// // priceVal=priceVal*entity.getQty();
				// sum=sum+priceVal;

				// new code
				if (!entity.getArea().equalsIgnoreCase("NA")) {
					double squareArea = Double.parseDouble(entity.getArea());
					priceVal = entity.getPrice() - taxAmt;
					priceVal = priceVal * squareArea;
					sum = sum + priceVal;
					System.out.println("RRRRRRRRRRRRRR sum" + sum);
				} else {
					System.out.println("Old code");
					priceVal = entity.getPrice() - taxAmt;
					// priceVal=priceVal*entity.getQty();
					/**
					 * @author Anil
					 * @since 30-06-2020
					 * For sai care by Vaishnavi and Nitin Sir
					 * If Area is null then total price will be multiplication of rate and no. of services
					 */
					if (entity.isServiceRate()) {
						priceVal=priceVal*entity.getNumberOfServices();
					}
					
					sum = sum + priceVal;
					System.out.println("RRRRRRRRRRRRRR sum" + sum);
				}

				System.out.println("RRRRRRRRRRRRRR sum" + sum);
			}

			else if ((entity.getPercentageDiscount() != null)
					&& (entity.getDiscountAmt() != 0)) {

				System.out.println("inside both not null condition");
				// old code
				// priceVal=entity.getPrice()-taxAmt;
				// priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
				// priceVal=priceVal-entity.getDiscountAmt();
				// priceVal=priceVal*entity.getQty();
				// sum=sum+priceVal;

				// new code
				/********************
				 * Square Area Calculation code added in if condition and
				 * without square area calculation code in else block
				 ***************************/
				if (!entity.getArea().equalsIgnoreCase("NA")) {
					priceVal = entity.getPrice() - taxAmt;
					double squareArea = Double.parseDouble(entity.getArea());
					priceVal = priceVal * squareArea;
					priceVal = priceVal
							- (priceVal * entity.getPercentageDiscount() / 100);
					priceVal = priceVal - entity.getDiscountAmt();
					sum = sum + priceVal;
					System.out.println("RRRRRRRRRRRRRR sum" + sum);

				} else {
					System.out.println("Old code ===");
					priceVal = entity.getPrice() - taxAmt;
					// priceVal=priceVal*entity.getQty();
					if (entity.isServiceRate()) {
						priceVal=priceVal*entity.getNumberOfServices();
					}
					priceVal = priceVal
							- (priceVal * entity.getPercentageDiscount() / 100);
					priceVal = priceVal - entity.getDiscountAmt();
					sum = sum + priceVal;
					System.out.println("RRRRRRRRRRRRRR sum" + sum);
				}

				System.out.println("RRRRRRRRRRRRRR sum" + sum);
			} else {
				System.out.println("inside oneof the null condition");

				priceVal = entity.getPrice() - taxAmt;
				if (entity.isServiceRate()) {
					priceVal=priceVal*entity.getNumberOfServices();
				}
				if (entity.getPercentageDiscount() != null) {
					System.out
							.println("inside getPercentageDiscount oneof the null condition");
					// priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);

					/********************
					 * Square Area Calculation code added in if condition and
					 * without square area calculation code in else block
					 ***************************/
					if (!entity.getArea().equalsIgnoreCase("NA")) {
						double squareArea = Double
								.parseDouble(entity.getArea());
						priceVal = priceVal * squareArea;
						priceVal = priceVal
								- (priceVal * entity.getPercentageDiscount() / 100);
						sum = sum + priceVal;
						System.out.println("RRRRRRRRRRRRRR sum" + sum);
					} else {
						System.out.println("old code");
						// priceVal=priceVal*entity.getQty();
						sum = sum
								+ priceVal
								- (priceVal * entity.getPercentageDiscount() / 100);

					}
				} else {
					System.out
							.println("inside getDiscountAmt oneof the null condition");
					// priceVal=priceVal-entity.getDiscountAmt();

					System.out
							.println("inside getDiscountAmt oneof the null condition");
					/********************
					 * Square Area Calculation code added in if condition and
					 * without square area code calculation in else block
					 ***************************/
					if (!entity.getArea().equalsIgnoreCase("NA")) {
						double squareArea = Double
								.parseDouble(entity.getArea());
						priceVal = priceVal * squareArea;
						priceVal = priceVal - entity.getDiscountAmt();
						sum = sum + priceVal;
						System.out.println("RRRRRRRRRRRRRR sum" + sum);
					} else {
						System.out.println("old code");
						// priceVal=priceVal*entity.getQty();
						sum = sum + priceVal - entity.getDiscountAmt();

					}
				}

				// priceVal=priceVal*entity.getQty();

				// sum=sum+priceVal;
				System.out.println("RRRRRRRRRRRRRR sum" + sum);
			}

		}
		return sum;
	}

	protected void createFieldUpdaterDiscAmtColumn() {
		discountAmt.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			@Override
			public void update(int index, SalesLineItem object, String value) {

				try {
					Double val1 = Double.parseDouble(value.trim());
					object.setDiscountAmt(val1);
					productIdCount = object.getPrduct().getCount();
					// nidhi 22-06-2017
					productSrNoCount = object.getProductSrNo();
					RowCountChangeEvent.fire(table, getDataprovider().getList()
							.size(), true);

				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}

	public double removeAllTaxes(SuperProduct entity) {
		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;
		if (entity instanceof ServiceProduct) {
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
		}

		if (entity instanceof ItemProduct) {
			ItemProduct prod = (ItemProduct) entity;
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			retrVat = entity.getPrice() / (1 + (vat / 100));
			retrVat = entity.getPrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = entity.getPrice() / (1 + service / 100);
			retrServ = entity.getPrice() - retrServ;
		}
		if (service != 0 && vat != 0) {
//			// Here if both are inclusive then first remove service tax and then
//			// on that amount
//			// calculate vat.
//			double removeServiceTax = (entity.getPrice() / (1 + service / 100));
//
//			// double taxPerc=service+vat;
//			// retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed
//			// below
//			retrServ = (removeServiceTax / (1 + vat / 100));
//			retrServ = entity.getPrice() - retrServ;
			
			/**
			 * Date 13-06-2018
			 * By Vijay 
			 * Des :- above old code commented and new code added below
			 */
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
//					retrVat=0;

				}
				else{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}
				else
				{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			/**
			 * ends here
			 */
		}
		tax = retrVat + retrServ;
		return tax;
	}

	public boolean removeChargesOnDelete() {

		int tableSize = getDataprovider().getList().size();
		System.out.println("Printing Here The Table Size" + tableSize);

		if (tableSize < 1) {
			return false;
		}
		return true;
	}

	public int identifyProductId() {
		return productIdCount;
	}

	/*
	 * Nidhi 21-06-2017
	 */

	public int identifySrNo() {
		return productSrNoCount;
	}

	public void onClick(ClickEvent event) {
		if (event.getSource().equals(advSchPopup.getBtnOk())) {
			getDataprovider()
					.getList()
					.get(rowIndex)
					.setQuantity(
							getNoOfSelectedBranch(advSchPopup.getBranchTable()
									.getValue()));
			
			/**
			 * Date 02-05-2018 
			 * Developer : Vijay
			 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
			 * so i have updated below code using hashmap. 
			 */
			
//			// branchSchedulingInfo=AppUtility.getCustomersUpdatedBranchSchedulingList(advSchPopup.getBranchTable().getValue());
//
//			String branchSchedulingInfo = "";
//			String branchSchedulingInfo1 = "";
//			String branchSchedulingInfo2 = "";
//			String branchSchedulingInfo3 = "";
//			String branchSchedulingInfo4 = "";
//
//			/**
//			 * Date : 14-06-2017 BY ANIL
//			 */
//			String branchSchedulingInfo5 = "";
//			String branchSchedulingInfo6 = "";
//			String branchSchedulingInfo7 = "";
//			String branchSchedulingInfo8 = "";
//			String branchSchedulingInfo9 = "";
//			/**
//			 * End
//			 */
//
//			String branchArray[] = AppUtility
//					.getCustomersUpdatedBranchSchedulingList(advSchPopup
//							.getBranchTable().getValue());
//			System.out.println("BRANCH ARRAY OK " + branchArray.length);
//
//			if (branchArray[0] != null) {
//				branchSchedulingInfo = branchArray[0];
//			}
//			if (branchArray[1] != null) {
//				branchSchedulingInfo1 = branchArray[1];
//			}
//			if (branchArray[2] != null) {
//				branchSchedulingInfo2 = branchArray[2];
//			}
//			if (branchArray[3] != null) {
//				branchSchedulingInfo3 = branchArray[3];
//			}
//			if (branchArray[4] != null) {
//				branchSchedulingInfo4 = branchArray[4];
//			}
//
//			/**
//			 * Date : 14-06-2017 BY ANIL
//			 */
//			if (branchArray[5] != null) {
//				branchSchedulingInfo5 = branchArray[5];
//			}
//			if (branchArray[6] != null) {
//				branchSchedulingInfo6 = branchArray[6];
//			}
//			if (branchArray[7] != null) {
//				branchSchedulingInfo7 = branchArray[7];
//			}
//			if (branchArray[8] != null) {
//				branchSchedulingInfo8 = branchArray[8];
//			}
//			if (branchArray[9] != null) {
//				branchSchedulingInfo9 = branchArray[9];
//			}
//
//			/**
//			 * End
//			 */
//
//			getDataprovider().getList().get(rowIndex)
//					.setBranchSchedulingInfo(branchSchedulingInfo);
//			getDataprovider().getList().get(rowIndex)
//					.setBranchSchedulingInfo1(branchSchedulingInfo1);
//			getDataprovider().getList().get(rowIndex)
//					.setBranchSchedulingInfo2(branchSchedulingInfo2);
//			getDataprovider().getList().get(rowIndex)
//					.setBranchSchedulingInfo3(branchSchedulingInfo3);
//			getDataprovider().getList().get(rowIndex)
//					.setBranchSchedulingInfo4(branchSchedulingInfo4);
//
//			/**
//			 * Date : 14-06-2017 By ANIL
//			 */
//
//			getDataprovider().getList().get(rowIndex)
//					.setBranchSchedulingInfo5(branchSchedulingInfo5);
//			getDataprovider().getList().get(rowIndex)
//					.setBranchSchedulingInfo6(branchSchedulingInfo6);
//			getDataprovider().getList().get(rowIndex)
//					.setBranchSchedulingInfo7(branchSchedulingInfo7);
//			getDataprovider().getList().get(rowIndex)
//					.setBranchSchedulingInfo8(branchSchedulingInfo8);
//			getDataprovider().getList().get(rowIndex)
//					.setBranchSchedulingInfo9(branchSchedulingInfo9);
//
//			/**
//			 * End
//			 */

			/**
			 * Date 02-05-2018 By vijay updated code using hashmap for customer branches scheduling services
			 * above old code commented
			 */
			List<BranchWiseScheduling> brancheslist = advSchPopup.getBranchTable().getDataprovider().getList();
			ArrayList<BranchWiseScheduling> customerBrancheslist = new ArrayList<BranchWiseScheduling>();
			customerBrancheslist.addAll(brancheslist);
			
			
			if(LoginPresenter.billofMaterialActive && customerBrancheslist!=null && customerBrancheslist.size()>0){
				 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(getDataprovider().getList().get(rowIndex).getPrduct());
				 if(billPrDt!=null){
//						customerBrancheslist.addAll(getDataprovider().getList().get(rowIndex).getCustomerBranchSchedulingInfo().get(getDataprovider().getList().get(rowIndex).getProductSrNo()));
						
						if(LoginPresenter.billofMaterialActive && customerBrancheslist!=null && customerBrancheslist.size()>0){
							for(BranchWiseScheduling branch : customerBrancheslist){
								if(branch.isCheck() )
									{
									if(branch.getUnitOfMeasurement()==null){
										form.showDialogMessage("Unit of mesuarement or area is not define for selected branch ."
												+ "\n Please define area and unit of masuarement.");
										break;
									}
									if( branch.getArea()==0 ||  branch.getUnitOfMeasurement().trim().length() ==0){
										form.showDialogMessage("Unit of mesuarement or area is not define for selected branch ."
												+ "\n Please define area and unit of masuarement.");
											break;
									}
								}
							}
						}
				 }
				
			}
			
			
			if(getDataprovider().getList().get(rowIndex).getCustomerBranchSchedulingInfo().containsKey(getDataprovider().getList().get(rowIndex).getProductSrNo())){
				Integer productSrNo = getDataprovider().getList().get(rowIndex).getProductSrNo();
				getDataprovider().getList().get(rowIndex).getCustomerBranchSchedulingInfo().put(productSrNo, customerBrancheslist);
			}
			/**
			 * ends here
			 */
			
			table.redraw();
			System.out.println("DONE");
			if(!LoginPresenter.serviceScheduleNewLogicFlag){
				QuotationPresenter.setScheduleServiceOnSchedulingTable(AppUtility.getScheduleList(getDataprovider().getList()));

				
			}
			else{
				/**
				 * @author Vijay Date :- 13-11-2021
				 * Des :- added code for innovative service scheduling new logic
				 */
				ContractForm form = new ContractForm();
				ArrayList<ServiceSchedule> serviceschedulelist = new ArrayList<ServiceSchedule> ();
				serviceschedulelist.addAll(form.getServiceSchedulelist("default",getDataprovider().getList(),null,null,null,0,0,0,0,null));
				QuotationPresenter.setScheduleServiceOnSchedulingTable(serviceschedulelist);
				
			}
//			QuotationPresenter.setScheduleServiceOnSchedulingTable(AppUtility.getScheduleList(getDataprovider().getList()));
			panel.hide();
		}
		if (event.getSource().equals(advSchPopup.getBtnCancel())) {
			panel.hide();
		}
	}

	/*
	 * Nidhi 20-06-2017
	 */
	public double getNoOfSelectedBranch(List<BranchWiseScheduling> list) {
		int noOfBranches = 0;
		for (BranchWiseScheduling branch : list) {
			if (branch.isCheck() == true) {
				noOfBranches++;
			}
		}
		return noOfBranches;
	}
	
	private void editServiceWiseRateCol() {
		CheckboxCell cell=new CheckboxCell();
		editServiceWiseRateCol=new Column<SalesLineItem, Boolean>(cell) {
			@Override
			public Boolean getValue(SalesLineItem object) {
				return object.isServiceRate();
			}
		};
		table.addColumn(editServiceWiseRateCol, "#Service wise rate");
		table.setColumnWidth(editServiceWiseRateCol,80,Unit.PX);
		
		editServiceWiseRateCol.setFieldUpdater(new FieldUpdater<SalesLineItem, Boolean>() {
			@Override
			public void update(int index, SalesLineItem object, Boolean value) {
				object.setServiceRate(value);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redraw();
				setEnable(true);
			}
		});
	}
	
	private void viewServiceWiseRateCol() {
		viewServiceWiseRateCol=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				// TODO Auto-generated method stub
				if(object.isServiceRate()==true){
					return "Yes";
				}else{
					return "No";
				}
			}
		};
		table.addColumn(viewServiceWiseRateCol, "Service wise rate");
		table.setColumnWidth(viewServiceWiseRateCol,80,Unit.PX);
	}
	
	
	private void createViewColumnRemark() {
		viewremark = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getRemark() != null) {
					return object.getRemark();
				} else {

					return "";
				}
			}
		};
		table.addColumn(viewremark, "#Remark");
		table.setColumnWidth(viewremark, 100, Unit.PX);
		
	}
	

	private void createColumnRemark() {
		
		EditTextCell editCell = new EditTextCell();
		remark = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getRemark() != null) {
					return object.getRemark() + "";
				} else {
					return "";
				}
			}
		};
		table.addColumn(remark, "#Remark");
		table.setColumnWidth(remark, 100, Unit.PX);
		
	}
}
