package com.slicktechnologies.client.views.salespersontarget;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.salesperson.SalesPersonInfo;
import com.slicktechnologies.shared.common.salesperson.TargetInformation;

public class SalesPersonTargetTable extends SuperTable<TargetInformation>{

	
	TextColumn<TargetInformation> getcolumnMonths;
	TextColumn<TargetInformation> getcolumnTarget;
	TextColumn<TargetInformation> getcolumnpercentage;
	TextColumn<TargetInformation> getcolumnflatAmount;
	TextColumn<TargetInformation> getcolumnproductCategory;
	TextColumn<TargetInformation> getcolumnfinancialyear;
	TextColumn<TargetInformation> getcolumnDuration;
	
	Column<TargetInformation, String> getColumnemployeeDeleteButton;

	
	@Override
	public void createTable() {

		addcolumnProductCategory();
		addcolumnFinancialYear();
		addcolumnMonths();
		addcolumnDuration();
		addcolumnTarget();
		addcolumnPercenatge();
		addcolumnflatAmount();
		addcolumnDeletebutton();
		addcolumndeleteFieldUpdator();
		
	}

	
	
	private void addcolumnDuration() {

		getcolumnDuration = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getDuration();
			}
		};
		
		table.addColumn(getcolumnDuration, "Duration");
		table.setColumnWidth(getcolumnDuration,60,Unit.PX);
	}



	private void addcolumndeleteFieldUpdator() {

		getColumnemployeeDeleteButton.setFieldUpdater(new FieldUpdater<TargetInformation, String>() {
			
			@Override
			public void update(int index, TargetInformation object, String value) {

				getDataprovider().getList().remove(index);
				table.redrawRow(index);
			}
		});
	}



	private void addcolumnDeletebutton() {

		ButtonCell btncell = new ButtonCell();
		getColumnemployeeDeleteButton = new Column<TargetInformation, String>(btncell) {
			
			@Override
			public String getValue(TargetInformation object) {
				return "Delete";
			}
		};
		
		table.addColumn(getColumnemployeeDeleteButton, "Delete");
		table.setColumnWidth(getColumnemployeeDeleteButton,60,Unit.PX);
	}



	private void  addcolumnFinancialYear() {
		getcolumnfinancialyear = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getFinancialYear();
			}
		};
		
		table.addColumn(getcolumnfinancialyear,"Financial Year");
		table.setColumnWidth(getcolumnfinancialyear, 90,Unit.PX);
	}



	private void addcolumnMonths() {
		getcolumnMonths = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getMonths();
			}
		};
		table.addColumn(getcolumnMonths,"Months");
		table.setColumnWidth(getcolumnMonths, 90,Unit.PX);
	}



	private void addcolumnProductCategory() {

		getcolumnproductCategory = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getProductCategory();
			}
		};
		table.addColumn(getcolumnproductCategory,"Product Category");
		table.setColumnWidth(getcolumnproductCategory, 90,Unit.PX);
	}



	private void addcolumnTarget() {

		getcolumnTarget = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getTarget()+"";
			}
		};
		table.addColumn(getcolumnTarget,"Target");
		table.setColumnWidth(getcolumnTarget, 90,Unit.PX);
	}



	private void addcolumnPercenatge() {

		getcolumnpercentage = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getPercatage()+"";
			}
		};
		table.addColumn(getcolumnpercentage,"Incentives Percentage");
		table.setColumnWidth(getcolumnpercentage, 90,Unit.PX);
	}

	private void addcolumnflatAmount() {
		
		getcolumnflatAmount = new TextColumn<TargetInformation>() {
			
			@Override
			public String getValue(TargetInformation object) {
				return object.getFlatAmount()+"";
			}
		};
		
		table.addColumn(getcolumnflatAmount,"Incentives Amount");
		table.setColumnWidth(getcolumnflatAmount, 90,Unit.PX);	
		
		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}


	
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		/**
		 * Date:15-11-2017 BY: Manisha 
		 * To hide the delete button in view state...!!
		 */
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);

		System.out.println("salespersontargettable" + state);

		if (state == true) {
			addcolumnProductCategory();
			addcolumnFinancialYear();
			addcolumnMonths();
			addcolumnDuration();
			addcolumnTarget();
			addcolumnPercenatge();
			addcolumnflatAmount();
			addcolumnDeletebutton();
			addcolumndeleteFieldUpdator();

		} else {
			addcolumnProductCategory();
			addcolumnFinancialYear();
			addcolumnMonths();
			addcolumnDuration();
			addcolumnTarget();
			addcolumnPercenatge();
			addcolumnflatAmount();
			addcolumnuneditable();
		}
		/**
		 * Ends for Manisha
		 */
	}
	

	/**
	 * Date :20/11/2017
	 * Created By :Manisha
	 * Description :To hide delete button in view state
	 */
	private void addcolumnuneditable() {
		
		getColumnemployeeDeleteButton = new TextColumn<TargetInformation>() {
			
		
			@Override
			public String getValue(TargetInformation object) {
				// TODO Auto-generated method stub
				return "";
			}
		};
		table.addColumn(getColumnemployeeDeleteButton,"Delete");
		table.setColumnWidth(getColumnemployeeDeleteButton, 90,Unit.PX);
	}

	/**Ends**/


	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
