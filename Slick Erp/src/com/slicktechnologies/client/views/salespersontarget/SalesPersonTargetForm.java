package com.slicktechnologies.client.views.salespersontarget;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.FinancialCalender;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesperson.SalesPersonInfo;
import com.slicktechnologies.shared.common.salesperson.SalesPersonTargets;
import com.slicktechnologies.shared.common.salesperson.TargetInformation;

public class SalesPersonTargetForm extends FormScreen<SalesPersonTargets>{

	

	public EmployeeInfoComposite employeeInfo;
	IntegerBox ibTarget;
	DoubleBox dbpercentage,dbflatAmount;
	
	TextBox tbblank,tbId;
	SalesPersonTargetTable targetTable;
	EmployeeTable employeetable;
	Button btnAdd,btnemployeeadd;
	
	ListBox lbDuration;
	
	ObjectListBox<Config> olbemployeeRole;
	ObjectListBox<Category> olbcProductCategory;
	ObjectListBox<FinancialCalender> olbcFinancialCalender;


	SalesPersonTargets salespersontargetobject;
	
	public  SalesPersonTargetForm() {
		super();
		createGui();
		targetTable.connectToLocal();
	}
	
	private void initialize() {

		employeeInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false,false);
		
		ibTarget = new IntegerBox();
		tbId = new TextBox();
		tbId.setEnabled(false);
		
		dbpercentage = new DoubleBox();
		dbflatAmount = new DoubleBox();
		targetTable = new SalesPersonTargetTable();
		btnAdd = new Button("Add");
		btnemployeeadd = new Button("Add");
		olbcProductCategory=new ObjectListBox<Category>();
		initalizeCategoryComboBox();
		
		olbcFinancialCalender = new ObjectListBox<FinancialCalender>();
		initializeCalendarBox();
		
		lbDuration = new ListBox();
		lbDuration.addItem("--SELECT--");
		lbDuration.addItem("Yearly");
		lbDuration.addItem("Half Yearly");
		lbDuration.addItem("Quaterely");
		lbDuration.addItem("Monthly");
		
		olbemployeeRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbemployeeRole,Screen.EMPLOYEEROLE);
		
		employeetable = new EmployeeTable();
		
	}
	
	private void initializeCalendarBox() {

		MyQuerry querry=new MyQuerry();
		Vector<Filter> filervec = new Vector<Filter>();
		
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filervec.add(filter);
		
		querry.setFilters(filervec);
		querry.setQuerryObject(new FinancialCalender());

		this.olbcFinancialCalender.MakeLive(querry);
	}

	private void initalizeCategoryComboBox() {

		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Category());
		Filter categoryFilter = new Filter();
		categoryFilter.setQuerryString("isServiceCategory");
		categoryFilter.setBooleanvalue(true);
		Filter statusfilter=new Filter();
		   statusfilter.setQuerryString("status");
		   statusfilter.setBooleanvalue(true);
		querry.getFilters().add(categoryFilter);
		querry.getFilters().add(statusfilter);
		this.olbcProductCategory.MakeLive(querry);
	
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initialize();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		FormField fgroupingEmployeeInformation=fbuilder.setlabel("Sales Person Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",employeeInfo);
		FormField fperson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField fgroupingTargetInformation=fbuilder.setlabel("Target Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Duration",lbDuration);
		FormField lbduration= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Target",ibTarget);
		FormField fibTarget= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Incentives Percentage",dbpercentage);
		FormField fdbpercentage= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Incentives Amount",dbflatAmount);
		FormField fdbflatAmount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("",tbblank);
		FormField ftbblank= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",targetTable.getTable());
		FormField ftargetTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",btnAdd);
		FormField fbtnAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Product Category",olbcProductCategory);
		FormField folbcProductCategory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Financial Year",olbcFinancialCalender);
		FormField folbcFinancialCalender= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Employee Role",olbemployeeRole);
		FormField olbemployeeRole= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",employeetable.getTable());
		FormField femployeetable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",btnemployeeadd);
		FormField fbtnemployeeadd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Id",tbId);
		FormField fidId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formFields = {   
				{fgroupingEmployeeInformation},
				{fperson,ftbblank},
				{fidId,olbemployeeRole,fbtnemployeeadd},
				{femployeetable},
				{fgroupingTargetInformation},
				{folbcFinancialCalender,folbcProductCategory,lbduration,fibTarget},
				{fdbpercentage,fdbflatAmount},
				{fbtnAdd},
				{ftargetTable}
				
		};
		this.fields=formFields;
		
	}

	
	

	@Override
	public void updateModel(SalesPersonTargets model) {
		
		List<TargetInformation> targetsinfo = this.targetTable.getDataprovider().getList();
		ArrayList<TargetInformation> targetsArr = new ArrayList<TargetInformation>();
		targetsArr.addAll(targetsinfo);
		model.setTargetInfoList(targetsArr);
		
		List<SalesPersonInfo> employeeinfo = this.employeetable.getDataprovider().getList();
		ArrayList<SalesPersonInfo> employeearr = new ArrayList<SalesPersonInfo>();
		employeearr.addAll(employeeinfo);
		model.setEmployeeInfo(employeearr);
		
		
		if(olbemployeeRole.getSelectedIndex()!=0)
			model.setEmployeeRole(olbemployeeRole.getValue(olbemployeeRole.getSelectedIndex()));
		if(olbcFinancialCalender.getSelectedIndex()!=0)
			model.setFinancialYear(olbcFinancialCalender.getValue(olbcFinancialCalender.getSelectedIndex()));
		
		salespersontargetobject=model;
	}
	
	
	@Override
	public void setToViewState() {
		super.setToViewState();
		
		SuperModel model=new SalesPersonTargets();
		model=salespersontargetobject;
		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.SALESPERSONTARGET, salespersontargetobject.getCount(), null,null,null, false, model, null);
	
	}


	@Override
	public void updateView(SalesPersonTargets view) {

		salespersontargetobject=view;
		
		targetTable.setValue(view.getTargetInfoList());
		
		if(view.getEmployeeRole()!=null)
			olbemployeeRole.setValue(view.getEmployeeRole());
		
		employeetable.setValue(view.getEmployeeInfo());
		
		if(view.getFinancialYear()!=null)
			olbcFinancialCalender.setValue(view.getFinancialYear());
		
    	tbId.setValue(view.getCount()+"");

	}
	
	
	
	@Override
	public void setCount(int count) {
		tbId.setValue(count+"");
	}
	
	/**Date- 16/11/2017 By-Manisha
	 * To make the delete button invisible in view state!!
	 */
	
	public void setEnable(boolean state){
		super.setEnable(state);
        targetTable.setEnable(state);
        employeetable.setEnable(state);
        
	}
	

	@Override
	public void clear() {
			super.clear();
			targetTable.clear();
			employeetable.clear();
	}
	
	/******Ends******/
	

	@Override
	public void toggleAppHeaderBarMenu() {


		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		
		AuthorizationHelper.setAsPerAuthorization(Screen.SALESPERSONTARGET,LoginPresenter.currentModule.trim());
	
	}
	
	
	@Override
	public boolean validate() {
		System.out.println(" hi vijay   1111 ");
		if(this.employeetable.getDataprovider().getList().size()==0 && this.targetTable.getDataprovider().getList().size()==0){
			showDialogMessage("Please add sales person and its target");
			return false;
		}
		return super.validate();
	}

}
