package com.slicktechnologies.client.views.salespersontarget;


import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.salespersontarget.SalesPersonTargetPresentor.SalesPersonTargetPresentorSearch;
import com.slicktechnologies.shared.FinancialCalender;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.salesperson.SalesPersonTargets;

public class SalesPersonTargetPresentorSearchProxy extends SalesPersonTargetPresentorSearch{

	
	IntegerBox ibId,ibblank;
	ObjectListBox<FinancialCalender> olbfinancialCalendar;
	ObjectListBox<Config> olbemployeeRole;
	
	//**********************Date 02/09/2017 by jayshree changes are made to add the employee info composite********************************
		EmployeeInfoComposite employeeInfo;
		

	
	public SalesPersonTargetPresentorSearchProxy(){
		super();
		createGui();
	}
	
	
	public void createScreen()
	{
		initializewidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder("Id",ibId);
		FormField fId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Financial Year",olbfinancialCalendar);
		FormField folbfinancialCalendar= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Employee Role",olbemployeeRole);
		FormField folbemployeeRole= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("",ibblank);
		FormField fibblank= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		//*********************************Date 02/09/2017 by jayshree***************
		
		fbuilder = new FormFieldBuilder("",employeeInfo);
		FormField fperson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		//****************end*****************
				
		FormField[][] formFields = {   
				{fId,folbfinancialCalendar,folbemployeeRole,fibblank},
				{fperson}
				
		};
		this.fields=formFields;
	}


	private void initializewidget() {
		
		ibId = new IntegerBox();
		
		olbfinancialCalendar = new ObjectListBox<FinancialCalender>();
		initializeCalendarBox();
		
		olbemployeeRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbemployeeRole,Screen.EMPLOYEEROLE);
		
		//*************************Date 02/09/2017 by jayshree*******************
		employeeInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false,false);
		//*************************end****************

	}
	
	
	private void initializeCalendarBox() {

		MyQuerry querry=new MyQuerry();
		Vector<Filter> filervec = new Vector<Filter>();
		
		Filter filter = null;
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filervec.add(filter);
		
		querry.setFilters(filervec);
		querry.setQuerryObject(new FinancialCalender());

		this.olbfinancialCalendar.MakeLive(querry);
	
	}


	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(olbemployeeRole.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbemployeeRole.getValue().trim());
			temp.setQuerryString("employeeRole");
			filtervec.add(temp);
		}
		
		if(ibId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		
		if(olbfinancialCalendar.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbfinancialCalendar.getValue().trim());
			temp.setQuerryString("financialYear");
			filtervec.add(temp);
		}
		
		//***********************************Date 02/09/2017 by jayshree
		if(employeeInfo.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(employeeInfo.getEmployeeId());
			temp.setQuerryString("employeeInfo.Id");
			filtervec.add(temp);
		}
		
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SalesPersonTargets());
		return querry;
	}


	@Override
	public boolean validate() {
		return super.validate();
	}
	
	
}
