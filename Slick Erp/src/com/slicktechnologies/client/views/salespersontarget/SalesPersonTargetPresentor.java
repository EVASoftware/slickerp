package com.slicktechnologies.client.views.salespersontarget;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.ContractPresenterSearchProxy;
import com.slicktechnologies.client.views.contract.ContractPresenterTableProxy;
import com.slicktechnologies.client.views.contract.ContractPresenter.ContractPresenterSearch;
import com.slicktechnologies.client.views.contract.ContractPresenter.ContractPresenterTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.FinancialCalender;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesperson.SalesPersonInfo;
import com.slicktechnologies.shared.common.salesperson.SalesPersonTargets;
import com.slicktechnologies.shared.common.salesperson.TargetInformation;

public class SalesPersonTargetPresentor extends FormScreenPresenter<SalesPersonTargets> implements ClickHandler, SelectionHandler<Suggestion>, ChangeHandler{

	SalesPersonTargetForm form;
	
	final GenricServiceAsync async =GWT.create(GenricService.class);

	CsvServiceAsync csvservice = GWT.create(CsvService.class);

	public SalesPersonTargetPresentor(UiScreen<SalesPersonTargets> view, SalesPersonTargets model) {
		
		super(view, model);
		form=(SalesPersonTargetForm) view;
		form.setPresenter(this);
		
		form.btnAdd.addClickHandler(this);
		form.btnemployeeadd.addClickHandler(this);
		form.employeeInfo.addSelectionHandler(this);
		form.olbcProductCategory.addChangeHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SALESPERSONTARGET,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	
	}

	public static SalesPersonTargetForm initalize()
	{
				SalesPersonTargetForm form=new  SalesPersonTargetForm();
				SalesPersonTargetPresentorTable gentable=new SalesPersonTargetPresentorSearchTableProxy();
				gentable.setView(form);
				gentable.applySelectionModle();
				SalesPersonTargetPresentorSearch.staticSuperTable=gentable;
				SalesPersonTargetPresentorSearch searchpopup=new SalesPersonTargetPresentorSearchProxy();
				form.setSearchpopupscreen(searchpopup);
				SalesPersonTargetPresentor  presenter=new SalesPersonTargetPresentor(form,new SalesPersonTargets());
				AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.salesperson")
	public static class SalesPersonTargetPresentorSearch extends SearchPopUpScreen<SalesPersonTargets>{

	@Override
	public MyQuerry getQuerry() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}
		
	}
	
	@EntityNameAnnotation(EntityName ="com.slicktechnologies.shared.common.salesperson")
	public static class SalesPersonTargetPresentorTable extends SuperTable<SalesPersonTargets> implements GeneratedVariableRefrence{

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		System.out.println("hiiii");
		if(event.getSource()==form.btnAdd){
			addtotargettable();
		}
		
		if(event.getSource()==form.btnemployeeadd){
			System.out.println(" hi in employee add button");
			addToEmployeeTable();
		}
	}

	

	private void addToEmployeeTable() {

		System.out.println("employee idd "+form.employeeInfo.getEmployeeId());
		if(form.employeeInfo.getEmployeeId()==-1 && form.olbemployeeRole.getSelectedIndex()==0){
			form.showDialogMessage("Please Select employee");
		}else if(form.employeeInfo.getEmployeeId()!=-1 && form.olbemployeeRole.getSelectedIndex()!=0){
			form.showDialogMessage("please select either single employee or employee role");
		}
		else{
			System.out.println("Inside employee table");
			
			if(form.olbemployeeRole.getSelectedIndex()!=0){
				System.out.println(" hi vijay employee role");
				MyQuerry querry  = new MyQuerry();
				Vector<Filter> filterVec = new Vector<Filter>();
				Filter filter=null;
				
				filter = new Filter();
				filter.setQuerryString("roleName");
				filter.setStringValue(form.olbemployeeRole.getValue(form.olbemployeeRole.getSelectedIndex()));
				filterVec.add(filter);
				
				filter = new  Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(model.getCompanyId());
				filterVec.add(filter);
				
				querry.setFilters(filterVec);
				querry.setQuerryObject(new Employee());
				
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						if(result.size()!=0){
							
							ArrayList<Employee> employeelist = new ArrayList<Employee>();
							
							for(SuperModel model : result){
								
								Employee emp = (Employee) model;
								employeelist.add(emp);
							}
							
							ArrayList<SalesPersonInfo> emplist = new ArrayList<SalesPersonInfo>();
							
							for(int i=0;i<employeelist.size();i++){
								
								SalesPersonInfo info = new SalesPersonInfo();
								info.setId(employeelist.get(i).getCount());
								info.setFullName(employeelist.get(i).getFullName());
								info.setCellNumber(employeelist.get(i).getCellNumber1());
								
								emplist.add(info);
								
							}
							
							form.employeetable.getDataprovider().setList(emplist);
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
				
				
			}else{
				
				ArrayList<SalesPersonInfo> employeelist = new ArrayList<SalesPersonInfo>();
				
				SalesPersonInfo info = new SalesPersonInfo();

				info.setId(form.employeeInfo.getEmployeeId());
				info.setFullName(form.employeeInfo.getEmployeeName());
				info.setCellNumber(form.employeeInfo.getCellNumber());
				employeelist.add(info);
				form.employeetable.getDataprovider().setList(employeelist);
				
				
				form.employeeInfo.clear();
			}
			
			
		}
	}

	private void addtotargettable() {
		
		System.out.println("inside add button");
		System.out.println(" value == "+form.olbcProductCategory.getValue());
		System.out.println(" target == "+form.ibTarget.getValue());
		System.out.println(" per "+form.dbpercentage.getValue());
		
		System.out.println(" duration == "+form.lbDuration.getSelectedIndex());
		
		/**
		 * Date 07/09/2018
		 * Developer : Vijay
		 * Des :- Can not allow to add same duration target again Validation
		 */
		List<TargetInformation> targetList = form.targetTable.getDataprovider().getList();
		for(int i=0;i<targetList.size();i++){
			if(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()).equals(targetList.get(i).getDuration())){
				form.showDialogMessage("Can not add Same Duration again!");
				return;
			}
		}
		/**
		 * ends here
		 */
		
		if(form.olbcFinancialCalender.getValue()==null){
			form.showDialogMessage("Please Select Financial Year");
		}
		else if(form.lbDuration.getSelectedIndex()==0){
			form.showDialogMessage("Please Select Duration");
		}
		else if(form.ibTarget.getValue()==null){
			form.showDialogMessage("Target Cannot be blank");
		}
		else if(form.dbpercentage.getValue()==null && form.dbflatAmount.getValue()==null){
			form.showDialogMessage("Please fill either Incentives Percentage or Incentives Amount ");
		}
		else{
			System.out.println(" hi vijay");
			
			getfinancialyearInfo();
			
//			if(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()).equals("Yearly")){
//				System.out.println(" inside yearly");
//				getfinancialyearInfo();
//			}
//			if(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()).equals("Half Yearly")){
//				System.out.println(" inside half yearly ");
//				getfinancialyearInfo();
//			}
//			if(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()).equals("Quaterely")){
//				System.out.println(" inside Quaterly");
//			}
//			if(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()).equals("Monthly")){
//				System.out.println(" inside monthly");
//			}
			
//			ConfigCategory cat = form.lbDuration.getSelectedItem();
//			
//			int days = Integer.parseInt(cat.getDescription());
//			
//			int iteration = 365/days;
//			
//		     	getfinancialyearInfo();
//			
//			for(int i=0;i<iteration;i++){
//				
//				TargetInformation targetinfo = new TargetInformation();
//				
//				targetinfo.setProductCategory(form.olbcProductCategory.getValue(form.olbcProductCategory.getSelectedIndex()));
//				targetinfo.setFinancialYear(form.olbcFinancialCalender.getValue(form.olbcFinancialCalender.getSelectedIndex()));
//				targetinfo.setDuration(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()));
//				targetinfo.setTarget(form.ibTarget.getValue());
//				if(form.dbpercentage.getValue()!=null)
//				targetinfo.setPercatage(form.dbpercentage.getValue());
//				if(form.dbflatAmount.getValue()!=null)
//				targetinfo.setFlatAmount(form.dbflatAmount.getValue());	
//				form.targetTable.getDataprovider().getList().add(targetinfo);
//
//			}
			
			
		}
			
		
		
	}

	private void getfinancialyearInfo() {

		
		
		MyQuerry querry = new  MyQuerry();
		Vector<Filter> filterVec= new Vector<Filter>();
		
		Filter filter = null;
		filter= new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(model.getCompanyId());
		filterVec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filterVec.add(filter);
		
		querry.setFilters(filterVec);
		querry.setQuerryObject(new FinancialCalender());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				for(SuperModel model : result){
					
					FinancialCalender cal = (FinancialCalender) model;
					String date1 ;
					
					date1 = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(cal.getStartDate()).split( "-")[1];

					System.out.println(" date month=="+date1);
					System.out.println(" in numbers month"+cal.getStartDate().getMonth());
					
					String date2 = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(cal.getStartDate()).split( "-")[2];
					
//					int days = CalendarUtil.getDaysBetween(cal.getStartDate(), cal.getEndDate()); 
//					System.out.println("diffrence days =="+days);
//					int monthsiteration =days/30;
//					System.out.println(" months =="+monthsiteration);
					
					int monthsiteration = 0;
					
					if(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()).equals("Yearly")){
						System.out.println(" inside Yearly");
						monthsiteration = 1;
					}
					if(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()).equals("Half Yearly")){
						System.out.println("Half yearly");

						monthsiteration = 2;
					}
					if(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()).equals("Quaterely")){
						System.out.println(" inside Quaterly");
						monthsiteration = 4;
					}
					if(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()).equals("Monthly")){
						System.out.println(" inside monthly");
						monthsiteration =  12;
					}

					for(int i=0;i<monthsiteration;i++){
						
						TargetInformation targetinfo = new TargetInformation();
						if(form.olbcProductCategory.getSelectedIndex()!=0){
							System.out.println("inside product category ==");
							targetinfo.setProductCategory(form.olbcProductCategory.getValue(form.olbcProductCategory.getSelectedIndex()));

						}
						targetinfo.setFinancialYear(form.olbcFinancialCalender.getValue(form.olbcFinancialCalender.getSelectedIndex()));
					
						// these conditions for setting accurate months
						if(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()).equals("Yearly")){
							System.out.println(" inside Yearly");
						
							Date date = cal.getEndDate();
							String month ;
							month = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[1];
							System.out.println(" date month=="+month);
							String year = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[2];
							String duration = month+"-"+year;
							System.out.println("Duration  === "+duration);
							targetinfo.setMonths(duration);
						}

						if(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()).equals("Half Yearly")){
							System.out.println("Half yearly");
							int increamentforhalfyearly = 0;
							System.out.println("Half yearly == "+increamentforhalfyearly);
							
							Date secondhalfyearStart = cal.getStartDate();
							if(i==0){
								Date date = cal.getStartDate();
								
								String startingmonth = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[1];
								System.out.println(" date month=="+startingmonth);
								String startingyear = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[2];
								String startingmonthyear = startingmonth+"-"+startingyear;
								
								
								CalendarUtil.addMonthsToDate(date, increamentforhalfyearly+=5);
								System.out.println(" Half yearly $$$$$$  "+increamentforhalfyearly);
								System.out.println(" date with monthssssss"+date);
								String month ;
								month = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[1];
								System.out.println(" date month=="+month);
								String year = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[2];
								String startinghalfyear = month+"-"+year;
								System.out.println("Duration  === "+startinghalfyear);
								targetinfo.setMonths(startingmonthyear+"To"+startinghalfyear);
							}else{
								
								CalendarUtil.addMonthsToDate(secondhalfyearStart, 1);
								System.out.println("second half year start ==="+secondhalfyearStart);
								
								String startingmonth = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(secondhalfyearStart).split( "-")[1];
								System.out.println(" date month=="+startingmonth);
								String startingyear = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(secondhalfyearStart).split( "-")[2];
								String secondhalfmonthyear = startingmonth+"-"+startingyear;
								
								Date date = cal.getStartDate();
								CalendarUtil.addMonthsToDate(date, increamentforhalfyearly+=5);
								System.out.println(" Half yearly $$$$$$  "+increamentforhalfyearly);
								System.out.println(" date with monthssssss"+date);
								String month ;
								month = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[1];
								System.out.println(" date month=="+month);
								String year = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[2];
								String secondhalfEndmonthyear = month+"-"+year;
								System.out.println("Duration  === "+secondhalfEndmonthyear);
								targetinfo.setMonths(secondhalfmonthyear+"To"+secondhalfEndmonthyear);
							}
							
							
						}
						
						if(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()).equals("Quaterely")){
							int increamentforQuarterly = 0;
							System.out.println("Quaterly == "+increamentforQuarterly);
							if(i==0){
								Date date = cal.getStartDate();
								
								String startingmonth = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[1];
								System.out.println(" date month=="+startingmonth);
								String startingyear = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[2];
								String startingmonthyear = startingmonth+"-"+startingyear;
								
								
								CalendarUtil.addMonthsToDate(date, increamentforQuarterly+=2);
							
								System.out.println(" quaterely $$$$$$  "+increamentforQuarterly);
								System.out.println(" date with monthssssss"+date);
								String month ;
								month = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[1];
								System.out.println(" date month=="+month);
								String year = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[2];
								String quaterlyOne = month+"-"+year;
								System.out.println("Duration  === "+startingmonthyear+"To"+quaterlyOne);
								targetinfo.setMonths(startingmonthyear+"To"+quaterlyOne);
							}else{
								Date date = cal.getStartDate();
								System.out.println("second n next quaters ===="+date);
								CalendarUtil.addMonthsToDate(date, increamentforQuarterly+=1);

								
								String startingmonth = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[1];
								System.out.println(" date month=="+startingmonth);
								String startingyear = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[2];
								String startingmonthyear = startingmonth+"-"+startingyear;
								
								System.out.println(" month and year quterrrrrrr "+startingmonthyear);
								
								CalendarUtil.addMonthsToDate(date, increamentforQuarterly+=1);
							
								System.out.println(" quaterely $$$$$$  "+increamentforQuarterly);
								System.out.println(" date with monthssssss"+date);
								String month ;
								month = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[1];
								System.out.println(" date month=="+month);
								String year = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[2];
								String quaterly = month+"-"+year;
								System.out.println("Duration  === "+quaterly);
								targetinfo.setMonths(startingmonthyear+"To"+quaterly);
							}
							
					
						}
						
						
						if(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()).equals("Monthly")){
								if(i==0){
									String startingmonth ;
									startingmonth = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(cal.getStartDate()).split( "-")[1];
									System.out.println(" date month=="+startingmonth);
									String year = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(cal.getStartDate()).split( "-")[2];
									String duration = startingmonth+"-"+year;
									System.out.println("Duration  === "+duration);
									targetinfo.setMonths(duration);
		
								}else{
									Date date = cal.getStartDate();
									int increament = 0;
									CalendarUtil.addMonthsToDate(date, ++increament);
									
									System.out.println(" date with monthssssss"+date);
									String month ;
									month = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[1];
									System.out.println(" date month=="+month);
									String year = DateTimeFormat.getFormat( "d-MMM-yyyy" ).format(date).split( "-")[2];
									String duration = month+"-"+year;
									System.out.println("Duration  === "+duration);
									targetinfo.setMonths(duration);
							}
						
						}
						
						
						targetinfo.setDuration(form.lbDuration.getValue(form.lbDuration.getSelectedIndex()));
						targetinfo.setTarget(form.ibTarget.getValue());
						if(form.dbpercentage.getValue()!=null)
						targetinfo.setPercatage(form.dbpercentage.getValue());
						if(form.dbflatAmount.getValue()!=null)
						targetinfo.setFlatAmount(form.dbflatAmount.getValue());	
						form.targetTable.getDataprovider().getList().add(targetinfo);
		
					}
						

				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
		
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		
		if(event.getSource().equals(form.employeeInfo.getEmployeeId())||event.getSource().equals(form.employeeInfo.getEmployeeName()) || event.getSource().equals(form.employeeInfo.getCellNumber())){
			retrieveSalesPerson();
		}
	}

	private void retrieveSalesPerson() {

		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = new Filter();
		filter.setQuerryString("employeeInfo.Id");
		filter.setIntValue(form.employeeInfo.getEmployeeId());
		filtervec.add(filter);
		
//		filter=new Filter();
//		filter.setLongValue(c.getCompanyId());
//		filter.setQuerryString("companyId");
//		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SalesPersonTargets());
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				if (result.size() != 0) {
					form.showDialogMessage("Sales Person Target Already");
					form.employeeInfo.clear();
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}

	/**Date03/09/2017 By jayshree
	to made changes for download the sales persons target report*/    
		public void reactOnDownload() {
			ArrayList<SalesPersonTargets> SalesPersonarray = new ArrayList<SalesPersonTargets>();
			List<SalesPersonTargets> salesPersonList = form.getSearchpopupscreen()
					.getSupertable().getDataprovider().getList();

			SalesPersonarray.addAll(salesPersonList);
			csvservice.setSalesPersonTargetReport(SalesPersonarray,new AsyncCallback<Void>(){

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC Call Failed "+caught);
					
				}

				@Override
				public void onSuccess(Void result) {
					
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+125;
					Window.open(url, "test", "enabled");
				}
				
			});
			
	//******************************changes End here****************		

//			ArrayList<Contract> custarray = new ArrayList<Contract>();
//			List<Contract> list = (List<Contract>) form.getSearchpopupscreen()
//					.getSupertable().getDataprovider().getList();
//
//			custarray.addAll(list);
//
//			csvservice.setcontractlist(custarray, new AsyncCallback<Void>() {
//
//				@Override
//				public void onFailure(Throwable caught) {
//					System.out.println("RPC call Failed" + caught);
//
//				}
//
//				@Override
//				public void onSuccess(Void result) {
//					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
//					final String url = gwt + "csvservlet" + "?type=" + 6;
//					Window.open(url, "test", "enabled");
//				}
//			});

		
		
			

		}

	
	@Override
	public void onChange(ChangeEvent event) {
		System.out.println(" inside category change");
		System.out.println(" valuee"+form.olbcProductCategory.getValue());
		System.out.println(" 1111 "+form.olbcProductCategory.getValue(form.olbcProductCategory.getSelectedIndex()));
		
		if(form.employeetable.getDataprovider().getList().size()==0){
			form.showDialogMessage("please add employee first");
		}
		if(form.employeetable.getDataprovider().getList().size()>0){
			validate(form.olbcProductCategory.getValue());
		}
		
		
	}

	private void validate(String productCategory) {
		
		for(int i=0;i<form.employeetable.getDataprovider().getList().size();i++){

		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter =null;
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("employeeInfo.Id");
		filter.setIntValue(form.employeetable.getDataprovider().getList().get(i).getId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("targetInfoList.productCategory");
		filter.setStringValue(productCategory);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SalesPersonTargets());
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				if (result.size() != 0) {
					form.showDialogMessage("This Sales Person Target Already exist");
					form.olbcProductCategory.setSelectedIndex(0);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
		}
	
	}


}
