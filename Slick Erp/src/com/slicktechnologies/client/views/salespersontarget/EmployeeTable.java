package com.slicktechnologies.client.views.salespersontarget;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesperson.SalesPersonInfo;

public class EmployeeTable extends SuperTable<SalesPersonInfo> {

	TextColumn<SalesPersonInfo> getcolumnemployeeId;
	TextColumn<SalesPersonInfo> getcolumnemployeeName;
	TextColumn<SalesPersonInfo> getcolumnemployeeCell;

	Column<SalesPersonInfo, String> getColumnemployeeDeleteButton;

	@Override
	public void createTable() {

		addcolumnemployeeId();
		addcolumnemployeeName();
		addcolumnemployeeCell();
		addcolumnemployeedelete();
		addColumnDeleteUpdater();

	}

	private void addColumnDeleteUpdater() {

		getColumnemployeeDeleteButton
				.setFieldUpdater(new FieldUpdater<SalesPersonInfo, String>() {

					@Override
					public void update(int index, SalesPersonInfo object,
							String value) {
						getDataprovider().getList().remove(index);
						table.redrawRow(index);
					}
				});
	}

	private void addcolumnemployeedelete() {
		ButtonCell btnCell = new ButtonCell();

		getColumnemployeeDeleteButton = new Column<SalesPersonInfo, String>(
				btnCell) {

			@Override
			public String getValue(SalesPersonInfo object) {
				return "Delete";
			}
		};
		table.addColumn(getColumnemployeeDeleteButton, "Delete");
		table.setColumnWidth(getColumnemployeeDeleteButton, 60, Unit.PX);

	}

	private void addcolumnemployeeId() {

		getcolumnemployeeId = new TextColumn<SalesPersonInfo>() {

			@Override
			public String getValue(SalesPersonInfo object) {
				return object.getId() + "";
			}
		};
		table.addColumn(getcolumnemployeeId, "Employee Id");
		table.setColumnWidth(getcolumnemployeeId, 90, Unit.PX);
	}

	private void addcolumnemployeeName() {

		getcolumnemployeeName = new TextColumn<SalesPersonInfo>() {

			@Override
			public String getValue(SalesPersonInfo object) {
				return object.getFullName();
			}
		};
		table.addColumn(getcolumnemployeeName, "Employee Name");
		table.setColumnWidth(getcolumnemployeeName, 90, Unit.PX);
	}

	private void addcolumnemployeeCell() {

		getcolumnemployeeCell = new TextColumn<SalesPersonInfo>() {

			@Override
			public String getValue(SalesPersonInfo object) {
				return object.getCellNumber() + "";
			}
		};
		table.addColumn(getcolumnemployeeCell, "Cell Number");
		table.setColumnWidth(getcolumnemployeeCell, 90, Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub

		/**
		 * Date :16/11/2017 , By-Manisha To hide the delete button in view
		 * state.
		 */
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);

		if (state == true) {
			addcolumnemployeeId();
			addcolumnemployeeName();
			addcolumnemployeeCell();
			addcolumnemployeedelete();
			addColumnDeleteUpdater();

		}

		else {
			addcolumnemployeeId();
			addcolumnemployeeName();
			addcolumnemployeeCell();
			addcolumnuneditable();

		}
		/**
		 * Ends For Manisha
		 */

	}

	/**
	 * Created By :Manisha
	 * Date :16/11/2017
	 * Description :To hide delete button in view state
	 */
	private void addcolumnuneditable() {
		getColumnemployeeDeleteButton = new TextColumn<SalesPersonInfo>() {
			
			@Override
			public String getValue(SalesPersonInfo object) {
				return "";
			}
		};
		table.addColumn(getColumnemployeeDeleteButton,"Delete");
		table.setColumnWidth(getColumnemployeeDeleteButton, 90,Unit.PX);
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub

	}

}
