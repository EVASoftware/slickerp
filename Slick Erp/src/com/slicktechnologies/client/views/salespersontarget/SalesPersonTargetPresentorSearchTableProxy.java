package com.slicktechnologies.client.views.salespersontarget;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.slicktechnologies.client.views.salespersontarget.SalesPersonTargetPresentor.SalesPersonTargetPresentorTable;
import com.slicktechnologies.shared.common.salesperson.SalesPersonInfo;
import com.slicktechnologies.shared.common.salesperson.SalesPersonTargets;
import com.slicktechnologies.shared.common.salesperson.TargetInformation;

public class SalesPersonTargetPresentorSearchTableProxy extends SalesPersonTargetPresentorTable{

	TextColumn<SalesPersonTargets> getcolumnId;
	TextColumn<SalesPersonTargets> getcolumneemployeeRole;
	TextColumn<SalesPersonTargets> getcolumneFinancialYear;
	
	/**********************Date02/09/2017 by Jayshree changes are made to add the EmployeeInfo 
	composite in the table*************************/
	TextColumn<SalesPersonTargets> getcolumnEmployName;
	TextColumn<SalesPersonTargets> getcolumnProductCategory;
	
	TextColumn<SalesPersonTargets> getcolumnMonth;
	TextColumn<SalesPersonTargets> getcolumnDuration;
	TextColumn<SalesPersonTargets> getcolumntarget;
	TextColumn<SalesPersonTargets> getcolumnIncenteevPercentage;
	TextColumn<SalesPersonTargets> getcolumnIncentiveAmount;
	//*************************************end**************
	
	
	public SalesPersonTargetPresentorSearchTableProxy(){
		super();
	}

	@Override
	public void createTable() {
		
		addcolumnId();
		addcolumnemployeeRole();
		addcolumnFinancialYear();
		
		/***************************Date02/09/2017 by Jayshree changes are made to add the EmployeeInfo 
		composite in the table  ****************/
		addcolumnEmployName();
		addcolumnProductCategory();
		
		addcolumnMonth();
		addcolumnDuration();
		addcolumntarget();
		addcolumnIncenteevPercentage();
		addcolumnIncentiveAmount();
		table.setWidth("auto");
	}

		private void addcolumnEmployName() {
		// TODO Auto-generated method stub
		getcolumnEmployName=new TextColumn<SalesPersonTargets>(){

			@Override
			public String getValue(SalesPersonTargets object) {
				// TODO Auto-generated method stub
				return getSalesPerTargetNameFromList(object,"fullName","salesPerson");
			}
		};
		
		table.addColumn(getcolumnEmployName, "Employee Name");
		table.setColumnWidth(getcolumnEmployName, 180, Unit.PX);
		getcolumnEmployName.setSortable(true);
		}
	
		
		
		private void addcolumnProductCategory() {
		// TODO Auto-generated method stub
		getcolumnProductCategory=new TextColumn<SalesPersonTargets>(){

			@Override
			public String getValue(SalesPersonTargets object) {
				// TODO Auto-generated method stub
				return getSalesPerTargetNameFromList(object,"productCategory","salesPersonTarget");
			}
		};
		table.addColumn(getcolumnProductCategory, "product Catagory");
		table.setColumnWidth(getcolumnProductCategory, 200, Unit.PX);
		getcolumnProductCategory.setSortable(true);
		}
	
	
		
		private void addcolumnMonth() {
		// TODO Auto-generated method stub
		getcolumnMonth=new TextColumn<SalesPersonTargets>(){

			@Override
			public String getValue(SalesPersonTargets object) {
				// TODO Auto-generated method stub
				return getSalesPerTargetNameFromList(object,"Months","salesPersonTarget");
			}
		};
		table.addColumn(getcolumnMonth, "Month");
		table.setColumnWidth(getcolumnMonth, 180, Unit.PX);
		getcolumnMonth.setSortable(true);
		}
	
	
		private void addcolumnDuration() {
		// TODO Auto-generated method stub
		getcolumnDuration=new TextColumn<SalesPersonTargets>(){

			@Override
			public String getValue(SalesPersonTargets object) {
				// TODO Auto-generated method stub
				return getSalesPerTargetNameFromList(object,"Duration","salesPersonTarget");
			}
		};
		table.addColumn(getcolumnDuration, "Durations");
		table.setColumnWidth(getcolumnDuration, 180, Unit.PX);
		getcolumnDuration.setSortable(true);
		}

	
		private void addcolumntarget() {
		// TODO Auto-generated method stub
		getcolumntarget=new TextColumn<SalesPersonTargets>(){

			@Override
			public String getValue(SalesPersonTargets object) {
				// TODO Auto-generated method stub
				return getSalesPerTargetNameFromList(object,"target","salesPersonTarget");
			}
		};
		table.addColumn(getcolumntarget, "Target");
		table.setColumnWidth(getcolumntarget, 180, Unit.PX);
		getcolumntarget.setSortable(true);
		}

	
		private void addcolumnIncenteevPercentage() {
		// TODO Auto-generated method stub
		getcolumnIncenteevPercentage=new TextColumn<SalesPersonTargets>(){

			@Override
			public String getValue(SalesPersonTargets object) {
				// TODO Auto-generated method stub
				return getSalesPerTargetNameFromList(object,"percatage","salesPersonTarget");
			}
		};
		table.addColumn(getcolumnIncenteevPercentage, "Percentage");
		table.setColumnWidth(getcolumnIncenteevPercentage, 180, Unit.PX);
		getcolumnIncenteevPercentage.setSortable(true);
		}
	
	
		private void addcolumnIncentiveAmount() {
		// TODO Auto-generated method stub
		getcolumnIncentiveAmount=new TextColumn<SalesPersonTargets>(){

			@Override
			public String getValue(SalesPersonTargets object) {
				// TODO Auto-generated method stub
				return getSalesPerTargetNameFromList(object,"achievedTargetAmt","salesPersonTarget");
			}
		};
		table.addColumn(getcolumnIncentiveAmount, "Target Amount");
		table.setColumnWidth(getcolumnIncentiveAmount, 180, Unit.PX);
		getcolumnIncentiveAmount.setSortable(true);
		}

		
		
		public String getSalesPerTargetNameFromList(
			SalesPersonTargets object, String type, String listType) {
		
			if(object==null){
			return "";
			}
		
			String name="";

		
			if(listType.equals("salesPerson"))
			{
			
				switch(type){
				
				case "fullName":
				for(SalesPersonInfo salesperson:object.getEmployeeInfo()){
					name=name+salesperson.getFullName()+",";
				}
				break;
				default:
				break;
				}
			}
			
			else if(listType.equals("salesPersonTarget"))
			{
				if(object.getTargetInfoList()!=null){
				
				switch(type){
			
				case "productCategory":
				for(TargetInformation salesPersonTarget:object.getTargetInfoList()){
					name=name+salesPersonTarget.getProductCategory()+",";
				}
				break;
				
				case "Months":
				for(TargetInformation salesPersonTarget:object.getTargetInfoList()){
					name=name+salesPersonTarget.getMonths()+",";
				}
				break;
				
				case "Duration":
				for(TargetInformation salesPersonTarget:object.getTargetInfoList()){
					name=name+salesPersonTarget.getDuration()+",";
				}
				break;
				
				case "target":
				for(TargetInformation salesPersonTarget:object.getTargetInfoList()){
					name=name+salesPersonTarget.getTarget()+",";
				}
				break;
				
				
				case "percatage":
				for(TargetInformation salesPersonTarget:object.getTargetInfoList()){
					name=name+salesPersonTarget.getPercatage()+",";
				}
				break;
				
				
				case "achievedTargetAmt":
				for(TargetInformation salesPersonTarget:object.getTargetInfoList()){
					name=name+salesPersonTarget.getAchievedTargetAmt()+",";
				}
				break;
				default:
				break;
			}
			}	
		}
			
			try{
				if(!name.equals(""))
					name=name.substring(0,name.length()-1);
			}catch(Exception e){
				
			}

		return name;
	}

//*******************************Changes end******************



	
	
	private void addcolumnId() {
		
		getcolumnId = new TextColumn<SalesPersonTargets>() {
			
			@Override
			public String getValue(SalesPersonTargets object) {
				return object.getCount()+"";
			}
		};
		
		table.addColumn(getcolumnId,"Id");
		table.setColumnWidth(getcolumnId, 90,Unit.PX);
		getcolumnId.setSortable(true);
		
	}

	private void addcolumnemployeeRole() {

		getcolumneemployeeRole = new TextColumn<SalesPersonTargets>() {
			
			@Override
			public String getValue(SalesPersonTargets object) {
				return object.getEmployeeRole();
			}
		};
		
		table.addColumn(getcolumneemployeeRole,"Employee Role");
		table.setColumnWidth(getcolumneemployeeRole, 90,Unit.PX);
		getcolumneemployeeRole.setSortable(true);
	}

	private void addcolumnFinancialYear() {

		getcolumneFinancialYear = new TextColumn<SalesPersonTargets>() {
			
			@Override
			public String getValue(SalesPersonTargets object) {
				return object.getFinancialYear();
			}
		};
		table.addColumn(getcolumneFinancialYear,"Financial Year");
		table.setColumnWidth(getcolumneFinancialYear, 90,Unit.PX);
		getcolumneFinancialYear.setSortable(true);
		
	}

	
	public void addColumnSorting(){
		addSortinggetCount();
		addSortingemployeeRole();
		addSortingFinancialYear();
	}
	
	

	private void addSortinggetCount() {
		List<SalesPersonTargets> list = getDataprovider().getList();
		columnSort = new ListHandler<SalesPersonTargets>(list);
		
		columnSort.setComparator(getcolumnId, new Comparator<SalesPersonTargets>() {
			
			@Override
			public int compare(SalesPersonTargets e1, SalesPersonTargets e2) {
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	

	
	private void addSortingemployeeRole() {

		List<SalesPersonTargets> list = getDataprovider().getList();
		columnSort = new ListHandler<SalesPersonTargets>(list);
		columnSort.setComparator(getcolumneemployeeRole, new Comparator<SalesPersonTargets>() {
			
			@Override
			public int compare(SalesPersonTargets e1, SalesPersonTargets e2) {
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeeRole()!=null && e2.getEmployeeRole()!=null){
						return e1.getEmployeeRole().compareTo(e2.getEmployeeRole());}
				}
				else{
					return 0;
					}
				return 0;	
				}
		});
		table.addColumnSortHandler(columnSort);

	}

	private void addSortingFinancialYear() {

		List<SalesPersonTargets> list = getDataprovider().getList();
		columnSort = new ListHandler<SalesPersonTargets>(list);
		columnSort.setComparator(getcolumneFinancialYear, new Comparator<SalesPersonTargets>() {
			
			@Override
			public int compare(SalesPersonTargets e1, SalesPersonTargets e2) {
				if(e1!=null && e2!=null)
					{
						if( e1.getFinancialYear()!=null && e2.getFinancialYear()!=null){
							return e1.getFinancialYear().compareTo(e2.getFinancialYear());}
					}
					else{
						return 0;
						}
					return 0;	
					}
		});
		
		table.addColumnSortHandler(columnSort);

	
	}
	
	
	
	
	
	
}
