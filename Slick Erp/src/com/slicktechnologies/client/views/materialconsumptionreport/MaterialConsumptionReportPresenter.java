package com.slicktechnologies.client.views.materialconsumptionreport;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.MaterialConsumptionService;
import com.slicktechnologies.client.services.MaterialConsumptionServiceAsync;
import com.slicktechnologies.client.services.XlsxService;
import com.slicktechnologies.client.services.XlsxServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ExpenseCost;
import com.slicktechnologies.shared.common.inventory.LabourCost;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;

public class MaterialConsumptionReportPresenter  extends FormScreenPresenter<PhysicalInventoryMaintaince> implements SelectionHandler<Suggestion> ,ClickHandler{
	
	MaterialConsumptionReportForm form;
	final GenricServiceAsync async =GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final MaterialConsumptionServiceAsync consumptionAsync=GWT.create(MaterialConsumptionService.class);
	
	 NumberFormat fmt = NumberFormat.getFormat("0.00");
	XlsxServiceAsync xlxsService = GWT.create(XlsxService.class);
	
	DateTimeFormat format1= DateTimeFormat.getFormat("dd-MMM-yyyy");
	
	ArrayList<MaterialConsumptionReport> consumptionList=new ArrayList<MaterialConsumptionReport>();
	ArrayList<MaterialConsumptionReport> billRevanueAndQuoList=new ArrayList<MaterialConsumptionReport>();
	public MaterialConsumptionReportPresenter(FormScreen<PhysicalInventoryMaintaince> view,PhysicalInventoryMaintaince model) {
		super(view, model);
		form =(MaterialConsumptionReportForm)view;
		form.personInfoComposite.getId().addSelectionHandler(this);
		form.personInfoComposite.getName().addSelectionHandler(this);
		form.personInfoComposite.getPhone().addSelectionHandler(this);
		form.getGobutton().addClickHandler(this);
		
		//  rohan added this on date :29-04-2017
		
		form.BranchLevel.addClickHandler(this);
		form.ContractLevel.addClickHandler(this);
		form.customerLevel.addClickHandler(this);
		form.SalesPersonLevel.addClickHandler(this);
		form.oranisationLevel.addClickHandler(this);
		form.SegmentLevel.addClickHandler(this);
		//    ends here 
		form.setPresenter(this);
		
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
	}
		

	@Override
	public void reactOnPrint() {
		int index= form.getContractCount().getSelectedIndex();
		final String url = GWT.getModuleBaseURL() + "reportPdf"+"?Id="+Integer.parseInt(form.getContractCount().getItemText(index));
		 Window.open(url, "test", "enabled");
	}

	@Override
	public void reactOnEmail() {
		
	}
	
	@Override
	public void reactOnDownload()
	{
		boolean summeryReport = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractP&LReport");
		
		if(summeryReport){

			
			ArrayList<MaterialConsumptionReport> reportarray=new ArrayList<MaterialConsumptionReport>();
			
			List<MaterialConsumptionReport> list=(List<MaterialConsumptionReport>) form.table.getDataprovider().getList();
			HashSet<Integer> contractCount = new HashSet<Integer>();
			HashSet<Integer> contractAmtCount = new HashSet<Integer>();
//			reportarray.addAll(list);
			for(int i=0;i<list.size();i++){
				contractCount.add(list.get(i).getContractId());
				contractAmtCount.add(list.get(i).getContractId());
			}
			Console.log("contract count 00 " + contractAmtCount.size());
			for(Integer contCount : contractCount){
				double totalAmount= 0,minCost = 0,oprtCost = 0,otherCost=0;
				
				MaterialConsumptionReport report = null;
				for(int i=0;i<list.size();i++){
					if(list.get(i).getContractId()==contCount){
						if(report==null){
							 report = new MaterialConsumptionReport();
							 	report.setCustId(list.get(i).getCustId());
								report.setCustCellNo(list.get(i).getCustCellNo());
								report.setCustName(list.get(i).getCustName());	
								report.setPoc(list.get(i).getPoc());
								report.setContractId(list.get(i).getContractId());
								report.setServiceId(list.get(i).getServiceId());
								report.setBranch(list.get(i).getBranch());
								System.out.println("group contract:::"+list.get(i).getContGroup());
								System.out.println("type contract::::"+list.get(i).getContType());
								report.setContGroup(list.get(i).getContGroup());
								report.setContType(list.get(i).getContType());
								report.setDocType(list.get(i).getDocType());
								report.setDocID(list.get(i).getDocID());
								report.setProdId(list.get(i).getProdId());
								report.setProdCode(list.get(i).getProdCode());
								report.setProdName(list.get(i).getProdName());
								report.setProdUOM(list.get(i).getProdUOM());
								report.setQuantity(list.get(i).getQuantity());
								report.setPrice(list.get(i).getPrice());
								report.setTotal(list.get(i).getTotal());
								report.setContAmount(list.get(i).getContAmount());
						}
						minCost = minCost + list.get(i).getTotal();
					}
					
					
					
				}
				if(report!=null){
					report.setMinCost(minCost);
					reportarray.add(report);	
				}
				
			}
			
			Console.log("report aarray -- " + reportarray.size());
			ArrayList<LabourCost> labourcostarray =  new ArrayList<LabourCost>();
			
			
			List<LabourCost> labourcostlist =	form.getCosttable().getDataprovider().getList();
				
				for(int i=0;i<labourcostlist.size();i++){
					double totalAmount= 0,minCost = 0,oprtCost = 0,otherCost=0;
					boolean get = false;
					for(int j =0 ;j<reportarray.size();j++){
						if(labourcostlist.get(i).getContractId() == reportarray.get(j).getContractId()){
							get = true;
							oprtCost = reportarray.get(j).getOperatorCost();
							oprtCost = labourcostlist.get(i).getLabourCost() + oprtCost;
							 reportarray.get(j).setOperatorCost(oprtCost);
							if(labourcostlist.get(i).getContTotal()!=0
									&& contractAmtCount.contains(labourcostlist.get(i).getContractId())){
								reportarray.get(j).setContAmount(labourcostlist.get(i).getContTotal());
								contractAmtCount.remove(labourcostlist.get(i).getContractId());
							}
							 
						}
					}
					if(!get){

						MaterialConsumptionReport report  = new MaterialConsumptionReport();
						 	report.setCustId(labourcostlist.get(i).getCustId());
							report.setCustCellNo(labourcostlist.get(i).getCustCellNo());
							report.setCustName(labourcostlist.get(i).getCustName());	
							report.setPoc(labourcostlist.get(i).getPoc());
							report.setContractId(labourcostlist.get(i).getContractId());
							report.setServiceId(labourcostlist.get(i).getServiceId());
							report.setTotal(labourcostlist.get(i).getTotal());
							report.setBranch(labourcostlist.get(i).getBranch());
							report.setOperatorCost(labourcostlist.get(i).getLabourCost());
							report.setContAmount(labourcostlist.get(i).getContTotal());
							if(labourcostlist.get(i).getContTotal()!=0
									&& contractAmtCount.contains(labourcostlist.get(i).getContractId())){
								
								contractAmtCount.remove(labourcostlist.get(i).getContractId());
							}
							reportarray.add(report);	
					
					}
				}
			
			ArrayList<ExpenseCost> expensecostarray = new ArrayList<ExpenseCost>();
			
			List<ExpenseCost> expensecostlist = form.getExpensetable().getDataprovider().getList();
			Console.log("from expense table =="+expensecostlist.size());
			
			
			
			for(int i=0;i<expensecostlist.size();i++){
				double totalAmount= 0,minCost = 0,oprtCost = 0,otherCost=0;
				boolean get = false;
				for(int j =0 ;reportarray.size()<0;j++){
					if(labourcostlist.get(i).getContractId() == reportarray.get(j).getContractId()){
						get = true;
						otherCost = reportarray.get(j).getOtherCost();
						otherCost = expensecostlist.get(i).getExpenseAmt() + otherCost;
						if(labourcostlist.get(i).getContTotal()!=0
								&& contractAmtCount.contains(labourcostlist.get(i).getContractId())){
							contractAmtCount.remove(labourcostlist.get(i).getContractId());
						}
						 reportarray.get(j).setOperatorCost(oprtCost);
					}
				}
				if(!get){/*

					MaterialConsumptionReport report  = new MaterialConsumptionReport();
					 	report.setCustId(expensecostlist.get(i).getCustId());
						report.setCustCellNo(expensecostlist.get(i).getCustCellNo());
						report.setCustName(expensecostlist.get(i).getCustName());	
						report.setPoc(expensecostlist.get(i).getPoc());
						report.setContractId(expensecostlist.get(i).getContractId());
						report.setServiceId(expensecostlist.get(i).getServiceId());
						report.setTotal(expensecostlist.get(i).getTotal());
						report.setBranch(expensecostlist.get(i).getBranch());
						report.setMinCost(minCost);(expensecostlist.get(i).getLabourCost());
						reportarray.add(report);	
				
				*/}
			}
			
			
			
			
			Console.log("H vijay expense cost size == "+expensecostarray.size());
			
			ArrayList<String> summaryreportlist = new ArrayList<String>();
			summaryreportlist.add(form.getTbmaterialcost().getValue());
			summaryreportlist.add(form.getContractAmount().getValue());
			summaryreportlist.add(form.getTblabourcost().getValue());
			summaryreportlist.add(form.getTbprofitloss().getValue());
			summaryreportlist.add(form.getTbexpensecost().getValue());
			summaryreportlist.add(form.getTbadmincost().getValue());
			summaryreportlist.add(form.getTbfinaltotal().getValue());
		
//			double administrativecost=form.getDbadministratorCost().getValue();
			String date ="",segment ="";
			if(form.dbFromDate.getValue()!=null && form.dbToDate.getValue()!=null){
				date = "Period : " + format1.format(form.dbFromDate.getValue())
						+ " To : "+ format1.format(form.dbToDate.getValue());
			}
			
			if(form.olbSegment.getSelectedIndex()!=0){
				segment = "Segment :"+ form.olbSegment.getValue(form.olbSegment.getSelectedIndex());
			}
			Console.log("report aarray -- on call -- " + reportarray.size());
//			XlsxWriter.contractProfitLossReportList = reportarray;
//			xlxsService.setContractPNLReport(reportarray, date, segment,model.getCompanyId(), new AsyncCallback<Void>() {
				xlxsService.setContractPNLReport(reportarray, form.dbFromDate.getValue(),form.dbToDate.getValue(), segment, model.getCompanyId(), new AsyncCallback<Void>() {
						
				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "CreateXLXSServlet"+"?type="+5;
					Window.open(url, "test", "enabled");
					form.hideWaitSymbol();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
			

		}else{

			ArrayList<MaterialConsumptionReport> reportarray=new ArrayList<MaterialConsumptionReport>();
			
			List<MaterialConsumptionReport> list=(List<MaterialConsumptionReport>) form.table.getDataprovider().getList();
			
//			reportarray.addAll(list);
			for(int i=0;i<list.size();i++){
				
				MaterialConsumptionReport report = new MaterialConsumptionReport();
				
				report.setCustId(Integer.parseInt(form.getPersonInfoComposite().getId().getValue()));
				report.setCustCellNo(form.getPersonInfoComposite().getPhone().getValue());
				report.setCustName(form.getPersonInfoComposite().getName().getValue());	
				report.setPoc(form.getPersonInfoComposite().getName().getValue());
				report.setContractId(list.get(i).getContractId());
				report.setServiceId(list.get(i).getServiceId());
				report.setDocType(list.get(i).getDocType());
				report.setDocID(list.get(i).getDocID());
				report.setProdId(list.get(i).getProdId());
				report.setProdCode(list.get(i).getProdCode());
				report.setProdName(list.get(i).getProdName());
				report.setProdUOM(list.get(i).getProdUOM());
				report.setQuantity(list.get(i).getQuantity());
				report.setPrice(list.get(i).getPrice());
				report.setTotal(list.get(i).getTotal());
				
				if(list.size()==i+1)
					report.setNetAmount(form.getNetAmt().getValue());
				
				reportarray.add(report);	
				
			}
			

			ArrayList<LabourCost> labourcostarray =  new ArrayList<LabourCost>();
			List<LabourCost> labourcostlist =	form.getCosttable().getDataprovider().getList();
			
			for(int i=0;i<labourcostlist.size();i++){
				
				LabourCost labourcost = new LabourCost();
				labourcost.setContractId(labourcostlist.get(i).getContractId());
				labourcost.setServiceId(labourcostlist.get(i).getServiceId());
				labourcost.setEmployeeId(labourcostlist.get(i).getEmployeeId());
				labourcost.setEmployeeName(labourcostlist.get(i).getEmployeeName());
				labourcost.setEmployeeCell(labourcostlist.get(i).getEmployeeCell());
				labourcost.setServiceHours(labourcostlist.get(i).getServiceHours());
				labourcost.setEmpHourSalary(labourcostlist.get(i).getEmpHourSalary());
				labourcost.setLabourCost(labourcostlist.get(i).getLabourCost());
				
				if(labourcostlist.size()==i+1)
					labourcost.setLabourcostNetAmount(form.getLabourcostNetAmt().getValue());
				
				labourcostarray.add(labourcost);
				
			}
			
			ArrayList<ExpenseCost> expensecostarray = new ArrayList<ExpenseCost>();
			
			List<ExpenseCost> expensecostlist = form.getExpensetable().getDataprovider().getList();
			System.out.println("from expense table =="+expensecostlist.size());
			
			for(int i=0;i<expensecostlist.size();i++){
				
				ExpenseCost expensecost = new ExpenseCost();
				expensecost.setContractId(expensecostlist.get(i).getContractId());
				expensecost.setServiceId(expensecostlist.get(i).getServiceId());
				expensecost.setExpenseId(expensecostlist.get(i).getExpenseId());
				expensecost.setExpenseCategory(expensecostlist.get(i).getExpenseCategory());
				expensecost.setExpenseType(expensecostlist.get(i).getExpenseType());
				expensecost.setEmpployeeName(expensecostlist.get(i).getEmpployeeName());
				expensecost.setExpenseDate(expensecostlist.get(i).getExpenseDate());
				expensecost.setApprovarName(expensecostlist.get(i).getApprovarName());
				expensecost.setExpenseAmt(expensecostlist.get(i).getExpenseAmt());
				
				if(expensecostlist.size()==i+1){
					System.out.println("form.getExpesecostNetAmt().getValue() ====== "+form.getExpesecostNetAmt().getValue());
					expensecost.setExpensenetAmount(form.getExpesecostNetAmt().getValue());
				}
				expensecostarray.add(expensecost);
				
			}
			System.out.println("H vijay expense cost size == "+expensecostarray.size());
			
			ArrayList<String> summaryreportlist = new ArrayList<String>();
			summaryreportlist.add(form.getTbmaterialcost().getValue());
			summaryreportlist.add(form.getContractAmount().getValue());
			summaryreportlist.add(form.getTblabourcost().getValue());
			summaryreportlist.add(form.getTbprofitloss().getValue());
			summaryreportlist.add(form.getTbexpensecost().getValue());
			summaryreportlist.add(form.getTbadmincost().getValue());
			summaryreportlist.add(form.getTbfinaltotal().getValue());
		
			double administrativecost=form.getDbadministratorCost().getValue();
			
			csvservice.setMaterialConsumptionReport(reportarray,labourcostarray,expensecostarray,summaryreportlist,administrativecost, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}

				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+90;
					Window.open(url, "test", "enabled");
				}
			});

		
		}
		
	}

	

	@Override
	protected void makeNewModel() {
		
	}
	
	public static MaterialConsumptionReportForm initialize(){
		
		MaterialConsumptionReportForm form = new MaterialConsumptionReportForm();
		
		MaterialConsumptionReportPresenter presenter=new MaterialConsumptionReportPresenter(form, new PhysicalInventoryMaintaince());		
		AppMemory.getAppMemory().stickPnel(form);
		return form;		 
		
		
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if (event.getSource().equals(form.personInfoComposite.getId())||event.getSource().equals(form.personInfoComposite.getName())||
				event.getSource().equals(form.personInfoComposite.getPhone())) {
			
			form.contractCount.clear();
			form.table.connectToLocal();
			form.contractCount.addItem("Select");
			getcontractIdfomCustomerID();
		}
	}

	
	private void getcontractIdfomCustomerID() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(Integer.parseInt(form.personInfoComposite.getId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				if(result.size()==0){
					
					form.showDialogMessage("No contract exist for this customer");
					form.personInfoComposite.clear();
				}
				else{
				
				final List<String> contableList=  new ArrayList<String>();
				for(SuperModel model:result)
				{
					
					Contract conEntity = (Contract)model;
					contableList.add(conEntity.getCount()+"");
				}
				
				for(int i=0;i<contableList.size();i++){
				      form.getContractCount().addItem(contableList.get(i));
					 }
			}
			}
			});
	}

	
	double materialNetAmt=0;
	double labousrCostNetAmt=0;
	double expensecostNetAmt=0;
	
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource().equals(form.getGobutton()))
		{
			
			form.getTbmaterialcost().setText("0");
			form.getTblabourcost().setText("0");
			form.getTblabourcost().setText("0");
			form.getTbexpensecost().setText("0");
			form.getTbadmincost().setText("0");
			form.getTbfinaltotal().setText("0");
			form.getTbprofitloss().setText("0");
			form.getContractAmount().setText("0");
			
//			if(form.getOranisationLevel().getValue() || form.getBranchLevel().getValue() 
//				|| form.getSalesPersonLevel().getValue() || form.getCustomerLevel().getValue()
//				|| form.getContractLevel().getValue())
			if(form.getCustomerLevel().getValue()|| form.getContractLevel().getValue() || form.getBranchLevel().getValue() || form.getSegmentLevel().getValue())
			{
				if(form.getBranchLevel().getValue()){
					if (form.olbbranch.getSelectedIndex()!=0){
						
						if(form.dbFromDate.getValue() !=null && form.dbToDate.getValue()!=null){
							
							if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractP&LAsPerBom")){
								
								consumptionList.clear();	
								calcutatePAndLBranchWsieBomBase(form.getOlbbranch().getItemText(form.getOlbbranch().getSelectedIndex()),form.dbFromDate.getValue(),form.dbToDate.getValue(),"branch");
						// vijay for costing 
								labourcostlist.clear();
								getLabourCostList(form.getOlbbranch().getItemText(form.getOlbbranch().getSelectedIndex()),"branch");		
								
						// calculate expense cost and admin cost		
								expensecostlist.clear();
								calculateExpenseDetailsCustomerWise(form.getOlbbranch().getItemText(form.getOlbbranch().getSelectedIndex()),"branch");
								getAdministrativeCost();
								Timer timer = new Timer() {
										
										@Override
										public void run() {
											getGrandTotal(form.getOlbbranch().getItemText(form.getOlbbranch().getSelectedIndex()),"branch");
										}
									};
									timer.schedule(4000);
								
							}else{
								consumptionList.clear();	
								calcutatePAndLBranchWsie(form.getOlbbranch().getItemText(form.getOlbbranch().getSelectedIndex()),form.dbFromDate.getValue(),form.dbToDate.getValue(),"branch");
						// vijay for costing 
								labourcostlist.clear();
								getLabourCostList(form.getOlbbranch().getItemText(form.getOlbbranch().getSelectedIndex()),"branch");		
								
						// calculate expense cost and admin cost		
								expensecostlist.clear();
								calculateExpenseDetailsCustomerWise(form.getOlbbranch().getItemText(form.getOlbbranch().getSelectedIndex()),"branch");
								getAdministrativeCost();
								Timer timer = new Timer() {
										
										@Override
										public void run() {
											getGrandTotal(form.getOlbbranch().getItemText(form.getOlbbranch().getSelectedIndex()),"branch");
										}
									};
									timer.schedule(4000);
							}
							
								}
								else{
									form.showDialogMessage("Please select from date & to date..!");
								}
							
						
					
					}
					else
					{
						form.showDialogMessage("Please select branch..!");
					}
				}
				
				
//				if(form.getBranchLevel().getValue()){
//					if (form.olbbranch.getSelectedIndex()!=0){
//						
////						if(form.dbFromDate.getValue() !=null && form.dbToDate.getValue()!=null){
////							if(ValidateDate()){
////								
////							}
////						}
////						else{
////							form.showDialogMessage("Please select from date & to date..!");
////						}
//						
//						consumptionList.clear();	
//						calcutatePAndLBranchWsie(form.getOlbbranch().getItemText(form.getOlbbranch().getSelectedIndex()));
//				// vijay for costing 
//						labourcostlist.clear();
//						getLabourCostList(form.getOlbbranch().getItemText(form.getOlbbranch().getSelectedIndex()),"branch");		
//						
//				// calculate expense cost and admin cost		
//						expensecostlist.clear();
//						calculateExpenseDetailsCustomerWise(form.getOlbbranch().getItemText(form.getOlbbranch().getSelectedIndex()),"branch");
//						getAdministrativeCost();
//						Timer timer = new Timer() {
//								
//								@Override
//								public void run() {
//									getGrandTotal(form.getOlbbranch().getItemText(form.getOlbbranch().getSelectedIndex()),"branch");
//								}
//							};
//							timer.schedule(4000);
//						
//					}
//					else
//					{
//						form.showDialogMessage("Please select branch..!");
//					}
//				}
				
				if(form.getSegmentLevel().getValue()){
					if (form.olbSegment.getSelectedIndex()!=0){
						
						if(form.dbFromDate.getValue() !=null && form.dbToDate.getValue()!=null){
						
						consumptionList.clear();	
						calcutatePAndLBranchWsie(form.getOlbSegment().getItemText(form.getOlbSegment().getSelectedIndex()),form.dbFromDate.getValue(),form.dbToDate.getValue(),"segment");
				// vijay for costing 
						labourcostlist.clear();
						getLabourCostList(form.getOlbSegment().getItemText(form.getOlbSegment().getSelectedIndex()),"segment");		
						
				// calculate expense cost and admin cost		
						expensecostlist.clear();
						calculateExpenseDetailsCustomerWise(form.getOlbSegment().getItemText(form.getOlbSegment().getSelectedIndex()),"segment");
						getAdministrativeCost();
						Timer timer = new Timer() {
								
								@Override
								public void run() {
									getGrandTotal((form.getPersonInfoComposite().getId().getValue()),"segment");
								}
							};
							timer.schedule(4000);
						
					}
					else
					{
						form.showDialogMessage("Please add From and To Date..!");
					}
					}else
					{
						form.showDialogMessage("Please select Segment..!");
						
					}
				}
				
//				else if(form.getSalesPersonLevel().getValue()){
//					if (form.olbsalesPerson.getSelectedIndex()!=0){
////						calcutatePAndLSalesPersonWsie();
//						if(form.dbFromDate.getValue() !=null && form.dbToDate.getValue()!=null){
//							if(ValidateDate()){
//								
//							}
//						}
//						else{
//							form.showDialogMessage("Please select from date & to date..!");
//						}
//					}
//					else
//					{
//						form.showDialogMessage("Please select sales person..!");
//					}
//				}
				
				 if(form.getCustomerLevel().getValue()){
					if (!form.getPersonInfoComposite().getId().getValue().equals("")){
						
						
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractP&LAsPerBom")){
							
							consumptionList.clear();	
							calcutatePAndLBranchWsieBomBase(form.getPersonInfoComposite().getId().getValue(),form.dbFromDate.getValue(),form.dbToDate.getValue(),"Customer");
							labourcostlist.clear();
							getLabourCostList(form.getPersonInfoComposite().getId().getValue(),"Customer");		
							
							expensecostlist.clear();
							calculateExpenseDetailsCustomerWise(form.getPersonInfoComposite().getId().getValue(),"Customer");
							getAdministrativeCost();
							Timer timer = new Timer() {
									
									@Override
									public void run() {
										getGrandTotal((form.getPersonInfoComposite().getId().getValue()),"Customer");
									}
								};
								timer.schedule(4000);
							
						}else{
							
									consumptionList.clear();	
									calcutatePAndLCustomerWsie(form.getPersonInfoComposite().getId().getValue());
									labourcostlist.clear();
									getLabourCostList(form.getPersonInfoComposite().getId().getValue(),"Customer");		
									
							// calculate expense cost and admin cost		
									expensecostlist.clear();
									calculateExpenseDetailsCustomerWise(form.getPersonInfoComposite().getId().getValue()+"","Customer");
									getAdministrativeCost();
									Timer timer = new Timer() {
											
											@Override
											public void run() {
												getGrandTotal((form.getPersonInfoComposite().getId().getValue()),"Customer");
											}
										};
										timer.schedule(4000);
//								}
//							}
//							else{
//								form.showDialogMessage("Please select from date & to date..!");
//							}
						
						}
						
					}
					else
					{
						form.showDialogMessage("Please select customer information..!");
					}
				}
				else if(form.getContractLevel().getValue()){
					
					if(!form.getPersonInfoComposite().getId().getValue().equals("")){
						
						if(form.getContractCount().getSelectedIndex()!=0){
							if(form.getContractStatus().getValue().equals(Contract.APPROVED)){
								consumptionList.clear();
								getMinList();
								
								// vijay for costing 
								labourcostlist.clear();
								getLabourCostList(form.getContractCount().getItemText(form.getContractCount().getSelectedIndex()),"Contract");
								
								expensecostlist.clear();
								getExpenseCostList();
								
								getAdministrativeCost();
								
								 Timer timer = new Timer() {
									
									@Override
									public void run() {
										getGrandTotal((form.getContractCount().getItemText(form.getContractCount().getSelectedIndex())),"Contract");
									}
								};
								timer.schedule(4000); 
								
							}
							else{
								
								form.showDialogMessage("Contract status is not Approvrd");
								}
							}
						else{
							form.showDialogMessage("Please select atleast one contract");
						}
				
					}else{
						form.showDialogMessage("Please select customer information");
					}
				}
//				else if(form.getOranisationLevel().getValue()){
//					
//					if(form.dbFromDate.getValue() !=null && form.dbToDate.getValue()!=null){
//						if(ValidateDate()){
//							
//						}
//					}
//					else{
//						form.showDialogMessage("Please select from date & to date..!");
//					}
//				}
			}
			else{
				form.showDialogMessage("Please select how you want to calculate P & L ");
			}
			
			
		
			
		}
		
		
		if(event.getSource()==form.getCustomerLevel()){
			
			form.olbbranch.setSelectedIndex(0);
			form.olbbranch.setEnabled(false);
			
			form.olbsalesPerson.setSelectedIndex(0);
			form.olbsalesPerson.setEnabled(false);
			
			form.personInfoComposite.setEnable(true);
			
			form.contractCount.setSelectedIndex(0);
			form.contractCount.setEnabled(false);
			
			/**
			 *  nidhi
			 *  9-10-2017
			 *  
			 */
			form.olbbranch.setEnabled(false);
			form.tbSegment.setEnabled(false);
			form.olbSegment.setEnabled(false);
			/**
			 * end
			 */
			
		}
		if(event.getSource()==form.getContractLevel()){
			
			
			form.olbbranch.setSelectedIndex(0);
			form.olbbranch.setEnabled(false);
			
			form.olbsalesPerson.setSelectedIndex(0);
			form.olbsalesPerson.setEnabled(false);
			
			form.dbFromDate.setEnabled(false);
			form.dbFromDate.setValue(null);
			form.dbToDate.setEnabled(false);
			form.dbToDate.setValue(null);
			
			form.personInfoComposite.setEnable(true);
			form.contractCount.setEnabled(true);
			/**
			 *  nidhi
			 *  9-10-2017
			 *  
			 */
			form.olbbranch.setEnabled(false);
			form.tbSegment.setEnabled(false);
			form.olbSegment.setEnabled(false);
			/**
			 * end
			 */
		}
		if(event.getSource()==form.getOranisationLevel()){
			
			
			form.olbbranch.setSelectedIndex(0);
			form.olbbranch.setEnabled(false);
			
			form.olbsalesPerson.setSelectedIndex(0);
			form.olbsalesPerson.setEnabled(false);
			
			form.personInfoComposite.clear();
			form.personInfoComposite.setEnable(false);
			
			form.contractCount.setSelectedIndex(0);
			form.contractCount.setEnabled(false);
			
		}
		if(event.getSource()==form.getBranchLevel()){
			
			
			form.olbsalesPerson.setSelectedIndex(0);
			form.olbsalesPerson.setEnabled(false);
			
			form.personInfoComposite.clear();
			form.personInfoComposite.setEnable(false);
			
			form.contractCount.setSelectedIndex(0);
			form.contractCount.setEnabled(false);
			
			form.olbbranch.setEnabled(true);
			
			/**
			 *  nidhi
			 *  9-10-2017
			 *  
			 */
			form.tbSegment.setEnabled(false);
			form.olbSegment.setEnabled(false);
			form.dbFromDate.setEnabled(true);
			form.dbFromDate.setValue(null);
			form.dbToDate.setEnabled(true);
			form.dbToDate.setValue(null);
			/**
			 * end
			 */
		}
		/**
		 *  nidhi
		 *  9-10-2017
		 *  
		 */
		if(event.getSource()==form.getSegmentLevel()){
			
			
			form.olbsalesPerson.setSelectedIndex(0);
			form.olbsalesPerson.setEnabled(false);
			
			form.personInfoComposite.clear();
			form.personInfoComposite.setEnable(false);
			
			form.contractCount.setSelectedIndex(0);
			form.contractCount.setEnabled(false);
			
			form.olbbranch.setEnabled(true);
			
			
			form.tbSegment.setEnabled(true);
			form.olbSegment.setEnabled(true);
			
			form.dbFromDate.setEnabled(true);
			form.dbFromDate.setValue(null);
			form.dbToDate.setEnabled(true);
			form.dbToDate.setValue(null);
			
		}
		/**
		 * end
		 */
	}
	

	/**
	 * rohan added this method o validate from and to date is of same month
	 * @return
	 */
	private boolean ValidateDate() {
		
		Date fromdate = form.dbFromDate.getValue();
		int fromMonth =fromdate.getMonth();
		Date todate = form.dbToDate.getValue();
		int toMonth =todate.getMonth();
		
		
		if(fromMonth==toMonth){
			return true;
		}else{
		
			form.showDialogMessage("From date and to date shhould be of same date..!");
			return false;
		}
		
	}

	private void getGrandTotal(String conOrcustCount, String type){

		System.out.println("In grand total");
		double grandTotal=0;
		
			 System.out.println("materal cost =="+form.getNetAmt().getValue());
			 System.out.println("Labour cost =="+form.getLabourcostNetAmt().getValue());
			 System.out.println("Expense cost =="+form.getExpesecostNetAmt().getValue());

			 if(form.getNetAmt().getValue()==null)
				 form.getNetAmt().setValue(0d);
			 if(form.getLabourcostNetAmt().getValue()==null)
				 form.getLabourcostNetAmt().setValue(0d);
			 if(form.getExpesecostNetAmt().getValue()==null)
				 form.getExpesecostNetAmt().setValue(0d);
			 if(form.getDbadministratorCost().getValue()==null)
				 form.getDbadministratorCost().setValue(0d);
			 
		grandTotal = form.getNetAmt().getValue()+form.getLabourcostNetAmt().getValue()+form.getExpesecostNetAmt().getValue()+form.getDbadministratorCost().getValue();
		form.getGrandTotal().setValue(grandTotal);
		System.out.println(" grand total === "+grandTotal);
		
		// summary total
		
		if(type.equalsIgnoreCase("Contract")){
			int contractid = Integer.parseInt(form.getContractCount().getItemText(form.getContractCount().getSelectedIndex()));
			System.out.println(" contract idd ==="+contractid);
			
			MyQuerry querry = new MyQuerry();
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter =null;
					
			filter = new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(contractid);
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(model.getCompanyId());
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Contract());
			
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					System.out.println("in success "+result.size());
					for(SuperModel model: result){
						Contract cont = (Contract) model;
						
						form.getContractAmount().setValue(cont.getTotalAmount()+"");
						
					}
					System.out.println("  material cost "+form.getTbmaterialcost().getValue());
					System.out.println("labour cost =="+form.getTblabourcost().getValue());
					if(form.getTbmaterialcost().getValue().equals(""))
						form.getTbmaterialcost().setValue(0+"");
					if(form.getTblabourcost().getValue().equals(""))
						form.getTblabourcost().setValue(0+"");
					if(form.getTbexpensecost().getValue().equals(""))
						form.getTbexpensecost().setValue(0+"");
					if(form.getTbadmincost().getValue().equals(""))
						form.getTbadmincost().setValue(0+"");
					
					double materialcost = Double.parseDouble(form.getTbmaterialcost().getValue());
					double labourcost = Double.parseDouble(form.getTblabourcost().getValue());
					double expensecost = Double.parseDouble(form.getTbexpensecost().getValue());
					double admincost = Double.parseDouble(form.getTbadmincost().getValue());
					double total = materialcost+labourcost+expensecost+admincost;
					form.getTbfinaltotal().setValue(total+"");
					double profitloss = Double.parseDouble(form.getContractAmount().getValue()) - Double.parseDouble(form.getTbfinaltotal().getValue());
					String profitlossvalue = fmt.format(profitloss);
					form.getTbprofitloss().setValue(profitlossvalue);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
			
			

		}
		else{
			
		

		String contractid = conOrcustCount;
		System.out.println(" contract idd ==="+contractid);
		
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter =null;
				
		if(type.equals("Customer")){
			filter = new Filter();
			filter.setQuerryString("cinfo.count");
			filter.setIntValue(Integer.parseInt(contractid));
			filtervec.add(filter);
		}else if(type.equals("segment")){
			filter = new Filter();
			filter.setQuerryString("segment");
			filter.setStringValue(contractid);
			filtervec.add(filter);
		}else if(type.equals("branch")){
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(contractid);
			filtervec.add(filter);
		}
		
		
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(model.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setStringValue("Approved");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("in success "+result.size());
				
		List<Contract> conList=new ArrayList<Contract>();		
				for(SuperModel model: result){
					Contract cont = (Contract) model;
					conList.add(cont);
					
				}
				
				///  rohan added this 
				double conAmt=0;
				for (int i = 0; i < conList.size(); i++) {
					conAmt = conAmt + conList.get(i).getTotalAmount();
				}
				form.getContractAmount().setValue(conAmt+"");
				
				
				System.out.println("  material cost "+form.getTbmaterialcost().getValue());
				System.out.println("labour cost =="+form.getTblabourcost().getValue());
				if(form.getTbmaterialcost().getValue().equals(""))
					form.getTbmaterialcost().setValue(0+"");
				if(form.getTblabourcost().getValue().equals(""))
					form.getTblabourcost().setValue(0+"");
				if(form.getTbexpensecost().getValue().equals(""))
					form.getTbexpensecost().setValue(0+"");
				if(form.getTbadmincost().getValue().equals(""))
					form.getTbadmincost().setValue(0+"");
				
				double materialcost = Double.parseDouble(form.getTbmaterialcost().getValue());
				double labourcost = Double.parseDouble(form.getTblabourcost().getValue());
				double expensecost = Double.parseDouble(form.getTbexpensecost().getValue());
				double admincost = Double.parseDouble(form.getTbadmincost().getValue());
				double total = materialcost+labourcost+expensecost+admincost;
				form.getTbfinaltotal().setValue(total+"");
				double profitloss = Double.parseDouble(form.getContractAmount().getValue()) - Double.parseDouble(form.getTbfinaltotal().getValue());
				String profitlossvalue = fmt.format(profitloss);
				form.getTbprofitloss().setValue(profitlossvalue);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	
		}
	}
	
	
	private void getAdministrativeCost() {
		System.out.println(" in administrative cost");
		
		String contractdate =   form.getContractDate().getValue();
		System.out.println(" string contarct date ="+contractdate);
		
		
		consumptionAsync.retrieveAdministrativeCost(model.getCompanyId(), contractdate, new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<String> result) {
				// TODO Auto-generated method stub
				System.out.println(" value ===="+result);
//				if(result.size()==0){
//					form.showDialogMessage("No Financial Year found for this contract Date");
//				}else{
//					
//					form.dbadministratorCost.setValue(Double.parseDouble(result.get(0)));
//					form.tbadmincost.setValue(fmt.format(Double.parseDouble(result.get(0))));
//					form.tbfinancialYear.setValue(result.get(1));
//
//				}
				
			}
		});
		
		
		
	}
	
	
	ArrayList<ExpenseCost> expensecostlist = new ArrayList<ExpenseCost>();
	private void getExpenseCostList() {

		final Company c=new Company();
		consumptionAsync.retrieveExpenseCost(c.getCompanyId(), form.getContractCount().getItemText(form.getContractCount().getSelectedIndex()),"Contract" ,form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<ExpenseCost>>() {

			@Override
			public void onFailure(Throwable caught){
				
			}

			@Override
			public void onSuccess(ArrayList<ExpenseCost> result) {

				if (result.size()!=0) {
					
					expensecostlist.addAll(result);
					form.getExpensetable().getDataprovider().setList(expensecostlist);
					double expenseCostNetAmt = 0;
					for(int i=0;i<expensecostlist.size();i++){
						expenseCostNetAmt+=expensecostlist.get(i).getExpenseAmt();
					}
					String expensecost = fmt.format(expenseCostNetAmt);

					form.getExpesecostNetAmt().setValue(Double.parseDouble(expensecost));
					form.getTbexpensecost().setValue(expensecost);
				}
			}
		} );
		
	}	
	
ArrayList<LabourCost> labourcostlist = new ArrayList<LabourCost>();
	
	private void getLabourCostList(String conOrCUstCount, String type) {
		final Company c=new Company();
		
		consumptionAsync.retrieveLabourCost(c.getCompanyId(),conOrCUstCount,type,form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<LabourCost>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<LabourCost> result) {
				// TODO Auto-generated method stub
				System.out.println(" In successsss");
				
				if(result.size()!=0){
					labourcostlist.addAll(result);
					form.getCosttable().getDataprovider().setList(labourcostlist);
					double labourNetCost =0;
					for(int i=0;i<result.size();i++){
						
						 labourNetCost += result.get(i).getLabourCost();
					}
					
					String labourcost = fmt.format(labourNetCost);
					form.getLabourcostNetAmt().setValue(Double.parseDouble(labourcost));
					form.getTblabourcost().setValue(labourcost);
				}
			}
		});
		
	}
	
	
//	ArrayList<MaterialConsumptionReport> consumptionList=new ArrayList<MaterialConsumptionReport>();
	protected void getMinList()
	{
		final Company c=new Company();
		
		consumptionAsync.retriveMaterialDetails(c.getCompanyId(), form.getContractCount().getItemText(form.getContractCount().getSelectedIndex()),"Contract" ,form.dbFromDate.getValue(),form.dbToDate.getValue(),new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An unexpected error occurred. Please try again!");
			}

			@Override
			public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
				if(result.size()==0)
				{
					form.showDialogMessage("Material is not issued yet!");
				}
				else
				{
					consumptionList.addAll(result);
					consumptionAsync.retriveMaterialMMNDetails(c.getCompanyId(), form.getContractCount().getItemText(form.getContractCount().getSelectedIndex()),"Contract",form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An unexpected error occurred. Please try again!");
						}

						@Override
						public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
							if(result.size()!=0)
							{
								consumptionList.addAll(result);
								
							}
							if(consumptionList.size()!=0)
							{
								form.getTable().getDataprovider().setList(consumptionList);
								double sum=0;
								double totVal=0;
								
								for(int f=0;f<consumptionList.size();f++)
								{
									if(consumptionList.get(f).getDocType().trim().equals("MMN"))
									{
										totVal=-(consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity());
									}
									else{
										totVal=consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity();
									}
									sum=sum+totVal;
								}
								form.getNetAmt().setValue(sum);
								String materialAmt = fmt.format(sum);
								form.getTbmaterialcost().setValue(materialAmt);
							}
						}
					});
				}
			}
		});
	}
	
	
	/**
	 * rohan added this method for calculating customer wise contract P & L 
	 */
		private void calcutatePAndLCustomerWsie(final String customerID) {
			
			final Company c=new Company();
			
			consumptionAsync.retriveMaterialDetails(c.getCompanyId(),customerID,"Customer",form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("An unexpected error occurred. Please try again!");
				}

				@Override
				public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
					if(result.size()==0)
					{
						form.showDialogMessage("Material is not issued yet!");
					}
					else
					{
						consumptionList.addAll(result);
						consumptionAsync.retriveMaterialMMNDetails(c.getCompanyId(),customerID,"Customer",form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An unexpected error occurred. Please try again!");
							}

							@Override
							public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
								if(result.size()!=0)
								{
									consumptionList.addAll(result);
									
								}
								if(consumptionList.size()!=0)
								{
									form.getTable().getDataprovider().setList(consumptionList);
									double sum=0;
									double totVal=0;
									
									for(int f=0;f<consumptionList.size();f++)
									{
										if(consumptionList.get(f).getDocType().trim().equals("MMN"))
										{
											totVal=-(consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity());
										}
										else{
											totVal=consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity();
										}
										sum=sum+totVal;
									}
									form.getNetAmt().setValue(sum);
									String materialAmt = fmt.format(sum);
									form.getTbmaterialcost().setValue(materialAmt);
								}
							}
						});
					}
				}
			});
		
		}
		
		
	/**
	 * ends here 
	 */
		
		
		private void calculateExpenseDetailsCustomerWise(String customerID, String type) {
			
			final Company c=new Company();
			consumptionAsync.retrieveExpenseCost(c.getCompanyId(), customerID,type ,form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<ExpenseCost>>() {

				@Override
				public void onFailure(Throwable caught){
					
				}

				@Override
				public void onSuccess(ArrayList<ExpenseCost> result) {

					if (result.size()!=0) {
						
						expensecostlist.addAll(result);
						form.getExpensetable().getDataprovider().setList(expensecostlist);
						double expenseCostNetAmt = 0;
						for(int i=0;i<expensecostlist.size();i++){
							expenseCostNetAmt+=expensecostlist.get(i).getExpenseAmt();
						}
						String expensecost = fmt.format(expenseCostNetAmt);

						form.getExpesecostNetAmt().setValue(Double.parseDouble(expensecost));
						form.getTbexpensecost().setValue(expensecost);
					}
				}
			} );
		}	
		
		
	void calcutatePAndLBranchWsie(String Branch,Date fromDate, Date endDate,String optType){
		try {

			
			final Company c=new Company();
			
			consumptionAsync.retriveMaterialDetails(c.getCompanyId(), Branch,optType,form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("An unexpected error occurred. Please try again!");
				}

				@Override
				public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
					if(result.size()==0)
					{
						form.showDialogMessage("Material is not issued yet!");
					}
					else
					{
						consumptionList.addAll(result);
						consumptionAsync.retriveMaterialMMNDetails(c.getCompanyId(), form.getOlbbranch().getItemText(form.getOlbbranch().getSelectedIndex()),"branch",form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An unexpected error occurred. Please try again!");
							}

							@Override
							public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
								if(result.size()!=0)
								{
									consumptionList.addAll(result);
									
								}
								if(consumptionList.size()!=0)
								{
									form.getTable().getDataprovider().setList(consumptionList);
									double sum=0;
									double totVal=0;
									
									for(int f=0;f<consumptionList.size();f++)
									{
										if(consumptionList.get(f).getDocType().trim().equals("MMN"))
										{
											totVal=-(consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity());
										}
										else{
											totVal=consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity();
										}
										sum=sum+totVal;
									}
									form.getNetAmt().setValue(sum);
									String materialAmt = fmt.format(sum);
									form.getTbmaterialcost().setValue(materialAmt);
								}
							}
						});
					}
				}
			});
		
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
		
	void calcutatePAndLBranchWsieBomBase(final String Branch,Date fromDate, Date endDate,final String optType){
		try {

			
			final Company c=new Company();
//			consumptionAsync.retriveMaterialDetails(c.getCompanyId(),customerID,"Customer",form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {
			consumptionAsync.retriveMaterialDetailsBaseOnBOM(c.getCompanyId(), Branch,optType,form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("An unexpected error occurred. Please try again!");
				}

				@Override
				public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
					if(result.size()==0)
					{
						form.showDialogMessage("Material is not issued yet!");
					}
					else
					{
						consumptionList.addAll(result);
						consumptionAsync.retriveMaterialMMNDetails(c.getCompanyId(), Branch,optType,form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An unexpected error occurred. Please try again!");
							}

							@Override
							public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
								if(result.size()!=0)
								{
									consumptionList.addAll(result);
									
								}
								if(consumptionList.size()!=0)
								{
									form.getTable().getDataprovider().setList(consumptionList);
									double sum=0;
									double totVal=0;
									final HashSet<Integer> contract = new HashSet<Integer>();
									final HashSet<Integer> serSet = new HashSet<Integer>();
									final HashMap<Integer,HashSet<Integer>> contSer  = new HashMap<Integer, HashSet<Integer>>();
									for(int f=0;f<consumptionList.size();f++)
									{
										if(consumptionList.get(f).getDocType().trim().equals("MMN"))
										{
											totVal=-(consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity());
										}
										else{
											totVal=consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity();
										}
										contract.add(consumptionList.get(f).getContractId());
										if(consumptionList.get(f).getQuotationId()!=null 
												&& consumptionList.get(f).getQuotationId().size()>0){
											serSet.addAll(consumptionList.get(f).getQuotationId());
										}
										
										if(contSer.containsKey(consumptionList.get(f).getContractId())){
											contSer.get(consumptionList.get(f).getContractId()).add(consumptionList.get(f).getServiceId());
										}else{
											HashSet<Integer> serSet1 = new HashSet<Integer>();
											serSet1.add(consumptionList.get(f).getServiceId());
											if(consumptionList.get(f).getQuotationId()!=null 
													&& consumptionList.get(f).getQuotationId().size()>0){
												serSet.addAll(consumptionList.get(f).getQuotationId());
											}

											contSer.put(consumptionList.get(f).getContractId(), serSet1);
										}
										
										sum=sum+totVal;
									}
									form.getNetAmt().setValue(sum);
									String materialAmt = fmt.format(sum);
									form.getTbmaterialcost().setValue(materialAmt);
									
									if(serSet.size()>0){
										consumptionAsync.retriveServicePlannedRevanueAndBillingRevanue(null, contract, serSet, contSer, c.getCompanyId(),new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

											@Override
											public void onFailure(Throwable caught) {
												// TODO Auto-generated method stub
												
											}

											@Override
											public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
												
												billRevanueAndQuoList.clear();
												
												if(result.size()!=0)
												{
													billRevanueAndQuoList.addAll(result);
													if(billRevanueAndQuoList.get(0).getQuotationId() !=null && 
															billRevanueAndQuoList.get(0).getQuotationId().size()>0)
													getPlannedLabourCost(billRevanueAndQuoList, contract, serSet, contSer, c.getCompanyId());
												}
												
											}
										
										});
									}
									
								}
							}
						});
					}
				}
			});
		
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	void getPlannedLabourCost(ArrayList<MaterialConsumptionReport> ServiceContractDetails
			,HashSet<Integer> contract,HashSet<Integer> serSet ,HashMap<Integer,HashSet<Integer>> contSer,long companyID){
		

		consumptionAsync.retrivelabourPlannedRevanueAndBillingRevanue(billRevanueAndQuoList, contract, serSet, contSer,companyID,new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
				
				if(result.size()!=0)
				{
					if(billRevanueAndQuoList.get(0).getQuotationId() !=null && 
							billRevanueAndQuoList.get(0).getQuotationId().size()>0){
						billRevanueAndQuoList.clear();
						billRevanueAndQuoList.addAll(result);
						for(int i=0; i<result.size();i++){
							for(Integer quoCnt : result.get(0).getPlannedQuoCost().keySet()){
								System.out.println("get" + quoCnt + " cost -- "+ result.get(0).getPlannedQuoCost().get(quoCnt));
							}
							
						}
					}
				}
				
			}
		
		});
	
			
	}

}
