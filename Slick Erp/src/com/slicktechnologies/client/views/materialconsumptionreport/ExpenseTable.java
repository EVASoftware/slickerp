package com.slicktechnologies.client.views.materialconsumptionreport;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.inventory.ExpenseCost;

public class ExpenseTable extends SuperTable<ExpenseCost>{


	TextColumn<ExpenseCost> getColumnContractId;
	TextColumn<ExpenseCost> getColumnServiceId;
	TextColumn<ExpenseCost> getColumnExpenseId;
	TextColumn<ExpenseCost> getColumnExpenseDate;
	TextColumn<ExpenseCost> getColumnApproverName;
	TextColumn<ExpenseCost> getColumnExpenseAmount;
	TextColumn<ExpenseCost> getColumnexpenseCategory;
	TextColumn<ExpenseCost> getColumnexpenseType;
	TextColumn<ExpenseCost> getColumnemployee;
	
	ExpenseTable(){
		super();
		setHeight("200PX");
	}
	
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		
		addcolumnContractId();
		addcolumnServiceId();
		addcolumnExpenseId();
		addcolumnExpenseCategory();
		addcolumnExpenseType();
		addcolumnEmployee();
		addcolumnExpenseDate();
		addcolumnApproverName();
		addcolumnExpenseAmount();
		
		
	}
	
	private void addcolumnServiceId() {
		getColumnServiceId = new TextColumn<ExpenseCost>() {
			
			@Override
			public String getValue(ExpenseCost object) {
				return object.getServiceId()+"";
			}
		};
		table.addColumn(getColumnServiceId,"Service Id");
		table.setColumnWidth(getColumnServiceId, 90,Unit.PX);
	}

	private void addcolumnExpenseCategory() {
		getColumnexpenseCategory = new TextColumn<ExpenseCost>() {
			
			@Override
			public String getValue(ExpenseCost object) {
				return object.getExpenseCategory();
			}
		};
		table.addColumn(getColumnexpenseCategory,"Expense Category");
		table.setColumnWidth(getColumnexpenseCategory, 90,Unit.PX);
	}


	private void addcolumnExpenseType() {
		getColumnexpenseType = new TextColumn<ExpenseCost>() {
			
			@Override
			public String getValue(ExpenseCost object) {
				return object.getExpenseType();
			}
		};
		table.addColumn(getColumnexpenseType,"Expense Type");
		table.setColumnWidth(getColumnexpenseType, 90,Unit.PX);
	}


	private void addcolumnEmployee() {
		getColumnemployee = new TextColumn<ExpenseCost>() {
			
			@Override
			public String getValue(ExpenseCost object) {
				return object.getEmpployeeName();
			}
		};
		table.addColumn(getColumnemployee,"Employee Name");
		table.setColumnWidth(getColumnemployee, 90,Unit.PX);
	}


	private void addcolumnContractId() {
		getColumnContractId = new TextColumn<ExpenseCost>() {
			
			@Override
			public String getValue(ExpenseCost object) {
				return object.getContractId()+"";
			}
		};
		
		table.addColumn(getColumnContractId, "Contract ID");
		table.setColumnWidth(getColumnContractId, 90,Unit.PX);
	}


	private void addcolumnExpenseId() {
		getColumnExpenseId = new TextColumn<ExpenseCost>() {
			
			@Override
			public String getValue(ExpenseCost object) {
				return object.getExpenseId()+"";
			}
		};
		
		table.addColumn(getColumnExpenseId,"Expense ID");
		table.setColumnWidth(getColumnExpenseId,90,Unit.PX);
	}


	private void addcolumnExpenseDate() {
		getColumnExpenseDate = new TextColumn<ExpenseCost>() {
			
			@Override
			public String getValue(ExpenseCost object) {
				return object.getExpenseDate()+"";
			}
		};
		table.addColumn(getColumnExpenseDate, "Expense Date");
		table.setColumnWidth(getColumnExpenseDate, 90,Unit.PX);
	}


	private void addcolumnApproverName() {
		
		getColumnApproverName =  new TextColumn<ExpenseCost>() {
			
			@Override
			public String getValue(ExpenseCost object) {
				return object.getApprovarName();
			}
		};
		table.addColumn(getColumnApproverName, "Approver Name");
		table.setColumnWidth(getColumnApproverName, 90,Unit.PX);
	}


	private void addcolumnExpenseAmount() {
		getColumnExpenseAmount = new TextColumn<ExpenseCost>() {
			
			@Override
			public String getValue(ExpenseCost object) {
				return object.getExpenseAmt()+"";
			}
		};
		table.addColumn(getColumnExpenseAmount,"Expense Amount");
		table.setColumnWidth(getColumnExpenseAmount, 90, Unit.PX);
	}


	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
}
