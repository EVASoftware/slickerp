package com.slicktechnologies.client.views.materialconsumptionreport;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class MaterialConsumptionReportForm extends FormScreen<PhysicalInventoryMaintaince> implements ChangeHandler {
	
	ListBox contractCount;
	TextBox contractStatus;
	PersonInfoComposite personInfoComposite;
	DoubleBox netAmt;
	MaterialConsumptionReportTable table;
	

	
	//  rohan added this code for calculation 
	
	RadioButton oranisationLevel,SalesPersonLevel,customerLevel,ContractLevel;
	ObjectListBox<Employee> olbsalesPerson;
	
	DateBox dbFromDate,dbToDate;
	
	//	Label selectLable;
	//   ends here 
	

	//  vijay added this for costing 
	DoubleBox labourcostNetAmt, expesecostNetAmt, grandTotal,dbadministratorCost;
	ServiceEngineerCostTable costtable;
	ExpenseTable expensetable;
	
	
	Label lblmaterialCost,lbllabourCost,lblexpenses,lbladministrativeCost,lblContractAmt,lblgrandTotal,lblprofitloss;
	TextBox  contractDate,tbfinancialYear,tbmaterialcost,tblabourcost,tbexpensecost,tbadmincost,tbfinaltotal,contractAmount,tbprofitloss,tbblank;
	
	
	TextBox startDate;
	TextBox endDate;
	Button gobutton,clearbutton;
	final GenricServiceAsync async=GWT.create(GenricService.class);
	/**
	 *  nidhi
	 *  9-10-2017
	 *  segment - customer category for NBHC  and for other client 
	 * @param processlevel
	 * @param fields
	 * @param formstyle
	 */
	ObjectListBox<Branch> olbbranch;
	ObjectListBox<ConfigCategory> olbSegment; /** ONly for NBHC */
	RadioButton BranchLevel,SegmentLevel;
	TextBox tbSegment; /** Not for Nbhc */
	/**
	 * end
	 * @param processlevel
	 * @param fields
	 * @param formstyle
	 */
	
	public MaterialConsumptionReportForm(String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}


	
	public MaterialConsumptionReportForm() {
//			super();
			super(FormStyle.DEFAULT);
	       createGui();
	       table.connectToLocal();
	       expensetable.connectToLocal();
	       costtable.connectToLocal();
	}



	@SuppressWarnings("unused")
	private void initalizeWidget()
	{
		contractCount= new ListBox();
		contractCount.addChangeHandler(this);
		contractStatus= new TextBox();
		contractStatus.setEnabled(false);
		startDate= new TextBox();
		startDate.setEnabled(false);
		endDate = new TextBox();
		endDate.setEnabled(false);
		netAmt= new DoubleBox();
		netAmt.setEnabled(false);
		gobutton= new Button("Go");
		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		table= new MaterialConsumptionReportTable();
		
		costtable = new ServiceEngineerCostTable();
		labourcostNetAmt = new DoubleBox();
		labourcostNetAmt.setEnabled(false);
		
		expensetable = new ExpenseTable();
		expesecostNetAmt = new DoubleBox();
		expesecostNetAmt.setEnabled(false);
		
		grandTotal = new DoubleBox();
		grandTotal.setEnabled(false);
		
		// vijay
		
		dbadministratorCost = new DoubleBox();
		dbadministratorCost.setEnabled(false);
		lblmaterialCost = new Label("Material Cost");
		lbllabourCost = new Label("            Operator cost");
		lblexpenses = new Label("other operational cost");
		lbladministrativeCost = new Label("        Administrative Cost");
		lblContractAmt = new Label(" Contract Amount");
		lblgrandTotal = new Label("Total");
		lblprofitloss = new Label("Profit/Loss");
		
		tbmaterialcost = new TextBox();
		tbmaterialcost.setEnabled(false);
		tblabourcost = new TextBox();
		tblabourcost.setEnabled(false);
		tbexpensecost = new TextBox();
		tbexpensecost.setEnabled(false);
		tbadmincost = new TextBox();
		tbadmincost.setEnabled(false);
		tbfinaltotal = new TextBox();
		tbfinaltotal.setEnabled(false);
		tbprofitloss = new TextBox();
		tbprofitloss.setEnabled(false);
		contractAmount = new TextBox();
		contractAmount.setEnabled(false);
		
		contractDate = new TextBox();
		contractDate.setEnabled(false);
		tbfinancialYear = new TextBox();
		tbfinancialYear.setEnabled(false);
		
		
		// rohan added this for calculation of P & L
		oranisationLevel = new RadioButton("");
		BranchLevel = new RadioButton("radioGroup");
		SalesPersonLevel = new RadioButton("radioGroup");
		customerLevel = new RadioButton("radioGroup");
		ContractLevel = new RadioButton("radioGroup");
		
		SegmentLevel = new RadioButton("radioGroup");
//		selectLable =new Label("Select How You Want to Calculate P & L"); 
		
		olbsalesPerson = new ObjectListBox<Employee>();
		olbsalesPerson.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CONTRACT, "Sales Person");
		
		
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
	
		/**
		 *  nidhi
		 *  9-10-2017
		 *  
		 */
		olbbranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbranch);
		
		olbSegment= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbSegment, Screen.CUSTOMERCATEGORY);
		
		tbSegment = new TextBox();
	}

	



	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen(){


		initalizeWidget();
		/////////////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingContractInformation=fbuilder.setlabel("Contract Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Contract ID",contractCount);
		FormField fcontractCount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contract Status ",contractStatus);
		FormField fcontractStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Start Date ",startDate);
		FormField fstartDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("End Date ",endDate);
		FormField fendDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",gobutton);
		FormField fgobutton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("Net Amount",netAmt);
		FormField fnetAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingtable=fbuilder.setlabel("Material Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("",table.getTable());
		FormField ftable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();

		/*********** vijay ********************/
		fbuilder = new FormFieldBuilder();
		FormField fgroupingLobourCosttable=fbuilder.setlabel("Lobur Cost Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("",costtable.getTable());
		FormField fcosttable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Labour Cost Net Amount",labourcostNetAmt);
		FormField flabourNetAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingexpensetable=fbuilder.setlabel("Contract Expense Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("",expensetable.getTable());
		FormField fexpensetable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Expense Cost Net Amount",expesecostNetAmt);
		FormField fexpesecostNetAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Grand Total",grandTotal);
		FormField fgrandTotal= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		

		fbuilder = new FormFieldBuilder("Administrative Cost",dbadministratorCost);
		FormField fdbadministratorCost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingSummary=fbuilder.setlabel("Summary Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("",lblmaterialCost);
		FormField lblmaterialCost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",lbllabourCost);
		FormField flbllabourCost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",lblexpenses);
		FormField flblexpenses= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",lbladministrativeCost);
		FormField flbladministrativeCost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",lblprofitloss);
		FormField flblprofitloss= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",lblContractAmt);
		FormField flblContractAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",contractAmount);
		FormField fcontractAmount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",lblgrandTotal);
		FormField flblgrandTotal= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbmaterialcost);
		FormField fdbmaterialcost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",tblabourcost);
		FormField fdblabourcost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",tbexpensecost);
		FormField fdbexpensecost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",tbadmincost);
		FormField fdbadmincost= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",tbprofitloss);
		FormField fdbprofitloss= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",tbfinaltotal);
		FormField fdbfinaltotal= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contract Date",contractDate);
		FormField fcontractDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Financial Year",tbfinancialYear);
		FormField ftbfinancialYear= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		//  rohan added this code for NBHC 
		
		fbuilder = new FormFieldBuilder();
		FormField plDetails=fbuilder.setlabel("Select How You Want to Calculate Profit & Loss Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
//		fbuilder = new FormFieldBuilder(" ",selectLable);
//		FormField fselectLable= fbuilder.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder(" Organization Level",oranisationLevel);
		FormField oranisationLevel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		
		fbuilder = new FormFieldBuilder(" Branch Level",BranchLevel);
		FormField BranchLevel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		
		fbuilder = new FormFieldBuilder("Segment Level",SegmentLevel);
		FormField SegmentLevel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" Customer Level",customerLevel);
		FormField customerLevel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		
		fbuilder = new FormFieldBuilder(" SalesPerson Level",SalesPersonLevel);
		FormField salesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
		
		fbuilder = new FormFieldBuilder(" Contract Level",ContractLevel);
		FormField ContractLevel= fbuilder.setMandatory(true).setMandatoryMsg("Registration No. is Mandatory").setRowSpan(0).setColSpan(0).build();	
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSalesPerson=fbuilder.setlabel("Sales Person Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Sales Person",olbsalesPerson);
		FormField fsalesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fbranchGroupingInfo=fbuilder.setlabel("Filter Options").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Branch",olbbranch);
		FormField olbbranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("From Date",dbFromDate);
		FormField fdbFromDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date",dbToDate);
		FormField fdbToDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 *  nidhi
		 *  9-10-2017
		 *  
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC"))
		{
			fbuilder = new FormFieldBuilder("Segment Type",olbSegment);
			
		}
		else{
			fbuilder = new FormFieldBuilder("Segment Type",tbSegment);
		}
		FormField folbSegment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		FormField[][] formFields = { 
//				  rohan added this for contract P & L on many levels
				{plDetails},
				{customerLevel,ContractLevel,BranchLevel,SegmentLevel},
				{fbranchGroupingInfo},
				{olbbranch,folbSegment,fdbFromDate,fdbToDate},
//				{fgroupingSalesPerson},
//				{fsalesPerson},
				//  ends here 
				{fgroupingCustomerInformation},
				{fpersonInfoComposite},
				{fgroupingContractInformation},
				{fcontractCount,fcontractStatus,fstartDate,fendDate},
				{fcontractDate,ftbfinancialYear,fgobutton},
//				rohan added this for contract P & L on many levels
				
				{fgobutton},
				// ends here
				{fgroupingSummary},
				{lblmaterialCost,fdbmaterialcost,flblContractAmt,fcontractAmount},
				{flbllabourCost,fdblabourcost,flblprofitloss,fdbprofitloss},
				{flblexpenses,fdbexpensecost},
				{flbladministrativeCost,fdbadmincost},
				{flblgrandTotal,fdbfinaltotal},
				{fgroupingtable},
				{ftable},
				{fblankgroup,fnetAmt},
				{fgroupingLobourCosttable},
				{fcosttable},
				{fblankgroup,flabourNetAmt},
				{fgroupingexpensetable},
				{fexpensetable},
				{fblankgroup,fexpesecostNetAmt},
				{fblankgroup,fdbadministratorCost},
				{fblankgroup,fgrandTotal}
				
		};
		this.fields=formFields;
	}

	@Override
	public void onChange(ChangeEvent event) {
		
			  if (this.getContractCount().getSelectedIndex()==0){
				  table.connectToLocal();
				  this.getContractStatus().setValue("");
				  this.getStartDate().setValue("");
				  this.getEndDate().setValue("");
			  }
			  else{
				  table.connectToLocal();
				  getContractDetails();
			  }
		
	}

	private void getContractDetails() {
		// TODO Auto-generated method stub
		showWaitSymbol();
		Timer timer=new Timer() 
	     {
			@Override
			public void run() 
			{
				
				MyQuerry querry = new MyQuerry();
				Company c=new Company();
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter filter = null;
				
				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("count");
				int index = contractCount.getSelectedIndex();
				filter.setIntValue(Integer.parseInt(contractCount.getItemText(index)));
				filtervec.add(filter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Contract());
				
				
				
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
					}
		
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						System.out.println("result size is ========"+result.size());
						
						for(SuperModel model:result)
						{
							Contract conEntity=(Contract)model;
							
							contractStatus.setValue(conEntity.getStatus());
							startDate.setValue(AppUtility.parseDate(conEntity.getStartDate()));
							endDate.setValue(AppUtility.parseDate(conEntity.getEndDate()));
							contractDate.setValue(AppUtility.parseDate(conEntity.getContractDate()));
						}
						}
					 });
		
		 hideWaitSymbol();
			}
	     };
  	 timer.schedule(2000);
		
	}


//	int rowmmnindex=0;
//	private void getReturnmaterialFromContractCount(int contractCount,final double totalAmt) {
//		
//		System.out.println("contract count inside mmn ==="+contractCount);
//		MyQuerry querry = new MyQuerry();
//	  	Company c = new Company();
//	  	Vector<Filter> filtervec=new Vector<Filter>();
//	  	Filter filter = null;
//	  	filter = new Filter();
//	  	filter.setQuerryString("companyId");
//		filter.setLongValue(c.getCompanyId());
//		filtervec.add(filter);
//		filter = new Filter();
//		filter.setQuerryString("orderID");
//		filter.setStringValue(contractCount+"");
//		filtervec.add(filter);
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new MaterialMovementNote());
//	
//		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onFailure(Throwable caught) {
//			}
//
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				System.out.println("result size of query in mmn"+result.size());
//				
//				if(result.size()==0){
//					
//					System.out.println("materialReportlis inside size zero mmn "+materialReportlis.size());
//					 getTable().setValue(materialReportlis);
//			    	 getNetAmt().setValue(totalAmt);
//				}
//				else
//				{
//				
//				final List<MaterialConsumptionReport> reportList = new ArrayList<MaterialConsumptionReport>();
//				for(SuperModel model:result)
//				{
//					MaterialMovementNote mmnEntity = (MaterialMovementNote)model;
//					
//					if(mmnEntity.getStatus().equals(MaterialMovementNote.APPROVED)){
//						
//						for(int i=0;i<mmnEntity.getSubProductTableMmn().size();i++)
//						{
//							
//						
//							MaterialConsumptionReport report = new MaterialConsumptionReport();
//						report.setContractId(Integer.parseInt(mmnEntity.getOrderID()));
//						report.setServiceId(mmnEntity.getServiceId());
//						report.setDocID(mmnEntity.getCount());
//						report.setProdId(mmnEntity.getSubProductTableMmn().get(i).getMaterialProductId());
//						report.setProdName(mmnEntity.getSubProductTableMmn().get(i).getMaterialProductName());
//						report.setProdCode(mmnEntity.getSubProductTableMmn().get(i).getMaterialProductCode());
//						report.setProdUOM(mmnEntity.getSubProductTableMmn().get(i).getMaterialProductUOM());
//						report.setQuantity(mmnEntity.getSubProductTableMmn().get(i).getMaterialProductRequiredQuantity());
//						
//						reportList.add(report);
//						
//					}
//					}
//				}
//				System.out.println("mmn list size "+reportList.size());
//				
//				for(int  i=0;i<reportList.size();i++){
//					System.out.println(reportList.get(i).getProdId());
//					System.out.println(reportList.get(i).getProdCode());
//					MyQuerry querry = new MyQuerry();
//				  	Company c = new Company();
//				  	Vector<Filter> filtervec=new Vector<Filter>();
//				  	Filter filter = null;
//				  	filter = new Filter();
//				  	filter.setQuerryString("companyId");
//					filter.setLongValue(c.getCompanyId());
//					filtervec.add(filter);
//					filter = new Filter();
//					filter.setQuerryString("count");
//					filter.setIntValue(reportList.get(i).getProdId());
//					filtervec.add(filter);
//					
//					filter = new Filter();
//					filter.setQuerryString("productCode");
//					filter.setStringValue(reportList.get(i).getProdCode());
//					filtervec.add(filter);
//					
//					querry.setFilters(filtervec);
//					querry.setQuerryObject(new SuperProduct());
//				
//					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//						
//						@Override
//						public void onFailure(Throwable caught) {
//						}
//
//						@Override
//						public void onSuccess(ArrayList<SuperModel> result) {
//							
//														
//							System.out.println("result size of query in sper product "+result.size());
//							List<SuperProduct> prodPrizelis= new ArrayList<SuperProduct>();
//							
//							for(SuperModel model:result)
//							{
//								SuperProduct prodEntity = (SuperProduct)model;
//								prodEntity.setPrice(prodEntity.getPrice());
//								prodPrizelis.add(prodEntity);
//								
//								
//							}
//							
//							System.out.println("prod prize"+prodPrizelis.get(0).getPrice());
//							
//							MaterialConsumptionReport materialReport =new MaterialConsumptionReport();
//							materialReport.setContractId(reportList.get(rowmmnindex).getContractId());
//							materialReport.setServiceId(reportList.get(rowmmnindex).getServiceId());
//							materialReport.setDocType("MMN");
//							materialReport.setDocID(reportList.get(rowmmnindex).getDocID());
//							materialReport.setProdId(reportList.get(rowmmnindex).getProdId());
//							materialReport.setProdName(reportList.get(rowmmnindex).getProdName());
//							materialReport.setProdCode(reportList.get(rowmmnindex).getProdCode());
//							materialReport.setProdUOM(reportList.get(rowmmnindex).getProdUOM());
//							materialReport.setQuantity(reportList.get(rowmmnindex).getQuantity());
//							
//								for(int  j=0;j<prodPrizelis.size();j++){	
//									materialReport.setPrice(prodPrizelis.get(j).getPrice());
//									materialReport.setTotal(-(prodPrizelis.get(j).getPrice()*reportList.get(rowmmnindex).getQuantity()));
//								}
//								
//								
//								materialReportlis.add(materialReport);
//							
//							System.out.println("materialReportlis"+materialReportlis.size());	
//								
//							
//							
//							if(rowmmnindex==reportList.size()-1){
//								
//								System.out.println("materialReportlis inside if"+materialReportlis.size());
//							
//								    	 double picamt=0;
//								    	  for(int i=0;i<materialReportlis.size();i++){
//								    		picamt=picamt+materialReportlis.get(i).getTotal();
//								    	  }
//								    	  
//								    	  getTable().setValue(materialReportlis);
//								    	  getNetAmt().setValue(picamt);
//							}	
//								if(rowmmnindex!=0){
//									rowmmnindex=rowmmnindex+1;
//								}
//								else{
//									rowmmnindex=1;
//								}
//						}
//					});
//				}
//				}
//			}
//		});
//					
//	}


//	 int rowindex=0;
//	private void getMaterialDetailsFromContractCount(final int contractCount) {
//
//		System.out.println("contract count ======"+contractCount);
//		
//				MyQuerry querry = new MyQuerry();
//			  	Company c = new Company();
//			  	Vector<Filter> filtervec=new Vector<Filter>();
//			  	Filter filter = null;
//			  	filter = new Filter();
//			  	filter.setQuerryString("companyId");
//				filter.setLongValue(c.getCompanyId());
//				filtervec.add(filter);
//				filter = new Filter();
//				filter.setQuerryString("minSoId");
//				filter.setIntValue(contractCount);
//				filtervec.add(filter);
//				querry.setFilters(filtervec);
//				querry.setQuerryObject(new MaterialIssueNote());
//			
//				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//					
//					@Override
//					public void onFailure(Throwable caught) {
//					}
//
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						System.out.println("result size of query in min"+result.size());
//						if(result.size()==0){
//							showDialogMessage("Material is not issued yet..!!");
//						}
//						else
//						{
//						final List<MaterialConsumptionReport> reportList = new ArrayList<MaterialConsumptionReport>();
//						for(SuperModel model:result)
//						{
//							
//							MaterialIssueNote MINEntity = (MaterialIssueNote)model;
//							
//						
//							
//							if(MINEntity.getStatus().equals(MaterialIssueNote.APPROVED)){
//								
//							for(int i=0;i<MINEntity.getSubProductTablemin().size();i++)
//							{
//								MaterialConsumptionReport report = new MaterialConsumptionReport();
//								
//								report.setContractId(MINEntity.getMinSoId());
//								report.setServiceId(MINEntity.getServiceId());
//								report.setDocID(MINEntity.getCount());
//								report.setProdId(MINEntity.getSubProductTablemin().get(i).getMaterialProductId());
//								report.setProdName(MINEntity.getSubProductTablemin().get(i).getMaterialProductName());
//								report.setProdCode(MINEntity.getSubProductTablemin().get(i).getMaterialProductCode());
//								report.setProdUOM(MINEntity.getSubProductTablemin().get(i).getMaterialProductUOM());
//								report.setQuantity(MINEntity.getSubProductTablemin().get(i).getMaterialProductRequiredQuantity());
//								
//								reportList.add(report);
//							}
//						}
//						}
//						System.out.println("reportList size=="+reportList.size());
//						for(int  i=0;i<reportList.size();i++){
//							System.out.println(reportList.get(i).getProdId());
//							System.out.println(reportList.get(i).getProdCode());
//							MyQuerry querry = new MyQuerry();
//						  	Company c = new Company();
//						  	Vector<Filter> filtervec=new Vector<Filter>();
//						  	Filter filter = null;
//						  	filter = new Filter();
//						  	filter.setQuerryString("companyId");
//							filter.setLongValue(c.getCompanyId());
//							filtervec.add(filter);
//							filter = new Filter();
//							filter.setQuerryString("count");
//							filter.setIntValue(reportList.get(i).getProdId());
//							filtervec.add(filter);
//							
//							filter = new Filter();
//							filter.setQuerryString("productCode");
//							filter.setStringValue(reportList.get(i).getProdCode());
//							filtervec.add(filter);
//							
//							querry.setFilters(filtervec);
//							querry.setQuerryObject(new SuperProduct());
//						
//							async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//								
//								@Override
//								public void onFailure(Throwable caught) {
//								}
//
//								@Override
//								public void onSuccess(ArrayList<SuperModel> result) {
//									
//																
//									System.out.println("result size of query in sper product "+result.size());
//									List<SuperProduct> prodPrizelis= new ArrayList<SuperProduct>();
//									for(SuperModel model:result)
//									{
//										SuperProduct prodEntity = (SuperProduct)model;
//										prodEntity.setPrice(prodEntity.getPrice());
//										prodPrizelis.add(prodEntity);
//									}
//									
//									System.out.println("prod prize"+prodPrizelis.get(0).getPrice());
//									System.out.println("rowindex value ===="+rowindex);
//									MaterialConsumptionReport materialreport =new MaterialConsumptionReport();
//									materialreport.setContractId(reportList.get(rowindex).getContractId());
//									materialreport.setServiceId(reportList.get(rowindex).getServiceId());
//									materialreport.setDocType("MIN");
//									materialreport.setDocID(reportList.get(rowindex).getDocID());
//									materialreport.setProdId(reportList.get(rowindex).getProdId());
//									materialreport.setProdName(reportList.get(rowindex).getProdName());
//									materialreport.setProdCode(reportList.get(rowindex).getProdCode());
//									materialreport.setProdUOM(reportList.get(rowindex).getProdUOM());
//									materialreport.setQuantity(reportList.get(rowindex).getQuantity());
//									
//										for(int  j=0;j<prodPrizelis.size();j++){	
//											System.out.println("reportList.get(j).getPrize() value  ============="+prodPrizelis.get(j).getPrice());
//											materialreport.setPrice(prodPrizelis.get(j).getPrice());
//											materialreport.setTotal(prodPrizelis.get(j).getPrice()*reportList.get(rowindex).getQuantity());
//										}
//										
//										
//										materialReportlis.add(materialreport);
//									
//									System.out.println("materialReportlis"+materialReportlis.size());	
//										
//									
//									
//									if(rowindex==reportList.size()-1){
//										
//										
//										    	 double amt=0;
//										    	  for(int i=0;i<materialReportlis.size();i++){
//										    		amt=amt+materialReportlis.get(i).getTotal();
//										    	  }
////										    	  form.getTable().setValue(materialReportlis);
////										    	  form.getNetAmt().setValue(amt);
//										    	  
//										    	  getReturnmaterialFromContractCount(contractCount,amt);  	  
//									}	
//										
//									
//										if(rowindex!=0){
//											rowindex=rowindex+1;
//										}
//										else{
//											rowindex=1;
//										}
//								}
//							});
//							
//						}
//					}
//					}
//				});
//			}
		

	@Override
	public void updateModel(PhysicalInventoryMaintaince model) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void updateView(PhysicalInventoryMaintaince model) {
		// TODO Auto-generated method stub
		
	}
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Print")||text.equals("Download")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Print")||text.equals("Download")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Print")||text.equals("Download")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
//		
		AuthorizationHelper.setAsPerAuthorization(Screen.MATERIALCONSUMPTIONREPORT,LoginPresenter.currentModule.trim());
	}
	
	
//	@Override
//	public boolean validate() {
//		return false;
//		
//	}
//	
	
	
	@Override
	public void setEnable(boolean state) {
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		
		this.processLevelBar.setVisibleFalse(false);
	}




	
//*************************getters and setters *******************************
	
	


	public ListBox getContractCount() {
		return contractCount;
	}

	public void setContractCount(ListBox contractCount) {
		this.contractCount = contractCount;
	}

	public TextBox getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(TextBox contractStatus) {
		this.contractStatus = contractStatus;
	}

	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	public DoubleBox getNetAmt() {
		return netAmt;
	}

	public void setNetAmt(DoubleBox netAmt) {
		this.netAmt = netAmt;
	}

	
	public MaterialConsumptionReportTable getTable() {
		return table;
	}



	public void setTable(MaterialConsumptionReportTable table) {
		this.table = table;
	}



	public TextBox getStartDate() {
		return startDate;
	}

	public void setStartDate(TextBox startDate) {
		this.startDate = startDate;
	}

	public TextBox getEndDate() {
		return endDate;
	}

	public void setEndDate(TextBox endDate) {
		this.endDate = endDate;
	}

	public Button getGobutton() {
		return gobutton;
	}

	public void setGobutton(Button gobutton) {
		this.gobutton = gobutton;
	}

	public Button getClearbutton() {
		return clearbutton;
	}

	public void setClearbutton(Button clearbutton) {
		this.clearbutton = clearbutton;
	}



	public DoubleBox getLabourcostNetAmt() {
		return labourcostNetAmt;
	}



	public void setLabourcostNetAmt(DoubleBox labourcostNetAmt) {
		this.labourcostNetAmt = labourcostNetAmt;
	}



	public DoubleBox getExpesecostNetAmt() {
		return expesecostNetAmt;
	}



	public void setExpesecostNetAmt(DoubleBox expesecostNetAmt) {
		this.expesecostNetAmt = expesecostNetAmt;
	}



	public DoubleBox getGrandTotal() {
		return grandTotal;
	}



	public void setGrandTotal(DoubleBox grandTotal) {
		this.grandTotal = grandTotal;
	}



	public ServiceEngineerCostTable getCosttable() {
		return costtable;
	}



	public void setCosttable(ServiceEngineerCostTable costtable) {
		this.costtable = costtable;
	}



	public ExpenseTable getExpensetable() {
		return expensetable;
	}



	public void setExpensetable(ExpenseTable expensetable) {
		this.expensetable = expensetable;
	}



	public TextBox getTbmaterialcost() {
		return tbmaterialcost;
	}



	public void setTbmaterialcost(TextBox tbmaterialcost) {
		this.tbmaterialcost = tbmaterialcost;
	}



	public TextBox getTblabourcost() {
		return tblabourcost;
	}



	public void setTblabourcost(TextBox tblabourcost) {
		this.tblabourcost = tblabourcost;
	}



	public TextBox getTbexpensecost() {
		return tbexpensecost;
	}



	public void setTbexpensecost(TextBox tbexpensecost) {
		this.tbexpensecost = tbexpensecost;
	}



	public TextBox getTbadmincost() {
		return tbadmincost;
	}



	public void setTbadmincost(TextBox tbadmincost) {
		this.tbadmincost = tbadmincost;
	}



	public TextBox getTbfinaltotal() {
		return tbfinaltotal;
	}



	public void setTbfinaltotal(TextBox tbfinaltotal) {
		this.tbfinaltotal = tbfinaltotal;
	}



	public TextBox getTbprofitloss() {
		return tbprofitloss;
	}



	public void setTbprofitloss(TextBox tbprofitloss) {
		this.tbprofitloss = tbprofitloss;
	}



	public TextBox getContractAmount() {
		return contractAmount;
	}



	public void setContractAmount(TextBox contractAmount) {
		this.contractAmount = contractAmount;
	}



	public DoubleBox getDbadministratorCost() {
		return dbadministratorCost;
	}



	public void setDbadministratorCost(DoubleBox dbadministratorCost) {
		this.dbadministratorCost = dbadministratorCost;
	}



	public TextBox getContractDate() {
		return contractDate;
	}



	public void setContractDate(TextBox contractDate) {
		this.contractDate = contractDate;
	}



	public TextBox getTbfinancialYear() {
		return tbfinancialYear;
	}



	public void setTbfinancialYear(TextBox tbfinancialYear) {
		this.tbfinancialYear = tbfinancialYear;
	}
	
	public RadioButton getOranisationLevel() {
		return oranisationLevel;
	}



	public void setOranisationLevel(RadioButton oranisationLevel) {
		this.oranisationLevel = oranisationLevel;
	}



	public RadioButton getBranchLevel() {
		return BranchLevel;
	}



	public void setBranchLevel(RadioButton branchLevel) {
		BranchLevel = branchLevel;
	}



	public RadioButton getSalesPersonLevel() {
		return SalesPersonLevel;
	}



	public void setSalesPersonLevel(RadioButton salesPersonLevel) {
		SalesPersonLevel = salesPersonLevel;
	}



	public RadioButton getCustomerLevel() {
		return customerLevel;
	}



	public void setCustomerLevel(RadioButton customerLevel) {
		this.customerLevel = customerLevel;
	}



	public RadioButton getContractLevel() {
		return ContractLevel;
	}



	public void setContractLevel(RadioButton contractLevel) {
		ContractLevel = contractLevel;
	}



	public ObjectListBox<ConfigCategory> getOlbSegment() {
		return olbSegment;
	}



	public void setOlbSegment(ObjectListBox<ConfigCategory> olbSegment) {
		this.olbSegment = olbSegment;
	}



	public RadioButton getSegmentLevel() {
		return SegmentLevel;
	}



	public void setSegmentLevel(RadioButton segmentLevel) {
		SegmentLevel = segmentLevel;
	}



	public TextBox getTbSegment() {
		return tbSegment;
	}



	public void setTbSegment(TextBox tbSegment) {
		this.tbSegment = tbSegment;
	}
	public ObjectListBox<Branch> getOlbbranch() {
		return olbbranch;
	}



	public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
		this.olbbranch = olbbranch;
	}
	

}
