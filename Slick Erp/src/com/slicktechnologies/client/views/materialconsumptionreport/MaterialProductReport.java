package com.slicktechnologies.client.views.materialconsumptionreport;

public class MaterialProductReport {
	
	int prodId;
	double prodPrice;
	
	
	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	
	public double getProdPrice() {
		return prodPrice;
	}
	public void setProdPrice(double prodPrice) {
		this.prodPrice = prodPrice;
	}
	
	
	
}
