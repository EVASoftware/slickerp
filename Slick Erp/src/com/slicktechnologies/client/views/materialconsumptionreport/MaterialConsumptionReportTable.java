package com.slicktechnologies.client.views.materialconsumptionreport;

import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class MaterialConsumptionReportTable extends SuperTable<MaterialConsumptionReport> {
	
	TextColumn<MaterialConsumptionReport> getColumnContractId;
	TextColumn<MaterialConsumptionReport> getColumnServiceId;;
	TextColumn<MaterialConsumptionReport> getColumndocType;
	TextColumn<MaterialConsumptionReport> getColumndocID;
	TextColumn<MaterialConsumptionReport> getColumnProdID;
	TextColumn<MaterialConsumptionReport> getColumnProdCode;
	TextColumn<MaterialConsumptionReport> getColumnProdName;
	TextColumn<MaterialConsumptionReport> getColumnProdQuantity;
	TextColumn<MaterialConsumptionReport> getColumnProdUOM;
	TextColumn<MaterialConsumptionReport> getColumnProdPrice;
	Column<MaterialConsumptionReport,String> getColumntotal;
	
	public MaterialConsumptionReportTable() {
		super();
		setHeight("300px");
	}
	
	@Override
	public void createTable() {
		
		addColumndocContractId();
		addColumndocServiceId();
		addColumndocType();
		addColumndocID();
		addColumnProdID();
		addColumnProdCode();
		addColumnProdName();
		addColumnProdUOM();
		addColumnProdQuantity();
		addColumnProdPrice();
		addColumntotal();
	}
	
	private void addColumndocContractId() {
		
		getColumnContractId = new TextColumn<MaterialConsumptionReport>() {
			@Override
			public String getValue(MaterialConsumptionReport object) {
				if (object.getContractId() == -1)
					return "N.A";
				else
					return object.getContractId()+ "";
			}
		};
		table.addColumn(getColumnContractId, "Contarct ID");
		table.setColumnWidth(getColumnContractId,90,Unit.PX);
	}


	private void addColumndocServiceId() {
		
		getColumnServiceId = new TextColumn<MaterialConsumptionReport>() {
			@Override
			public String getValue(MaterialConsumptionReport object) {
				if (object.getServiceId() == -1)
					return "N.A";
				else
					return object.getServiceId() + "";
			}
		};
		table.addColumn(getColumnServiceId, "Service ID");
		table.setColumnWidth(getColumnServiceId,90,Unit.PX);
	}


	private void addColumndocType() {
		
		getColumndocType = new TextColumn<MaterialConsumptionReport>() {
			@Override
			public String getValue(MaterialConsumptionReport object) {
				if (object.getDocType()!=null){
					return object.getDocType();
				}
				return null;
			}
		};
		table.addColumn(getColumndocType, "Doc Type");
		table.setColumnWidth(getColumndocType,90,Unit.PX);
	}


	private void addColumndocID() {
		getColumndocID = new TextColumn<MaterialConsumptionReport>() {
			@Override
			public String getValue(MaterialConsumptionReport object) {
				if (object.getDocID() == -1)
					return "N.A";
				else
					return object.getDocID() + "";
			}
		};
		table.addColumn(getColumndocID, "Doc ID");
		table.setColumnWidth(getColumndocID,90,Unit.PX);
	}


	protected void addColumntotal()
	{
		TextCell editCell=new TextCell();
		getColumntotal=new Column<MaterialConsumptionReport,String>(editCell)
		{
			
			@Override
			public String getCellStyleNames(Context context, MaterialConsumptionReport object) {
				if(object.getDocType().equals("MMN")){
					 return "red";
				}
				return "black";
			}
			
			
			@Override
			public String getValue(MaterialConsumptionReport object)
			{
				object.setTotal(object.getQuantity()*object.getPrice());
				if(object.getDocType().equals("MMN")){
					double value=-object.getTotal();
					return value+"";
				}
				else{
					return object.getTotal()+"";
				}
//				return object.getTotal()+"";
			}
				};
				table.addColumn(getColumntotal,"Total");
				table.setColumnWidth(getColumntotal,90,Unit.PX);
	}
	
	
	private void addColumntotal1() {
		
		getColumntotal = new TextColumn<MaterialConsumptionReport>() {
			
			
			@Override
			public String getCellStyleNames(Context context, MaterialConsumptionReport object) {
				if(object.getDocType().equals("MMN")){
					 return "red";
				}
				return "black";
			}
			
			
			@Override
			public String getValue(MaterialConsumptionReport object) {
				
				
				if (object.getTotal() == -1){
					return "N.A";
				}
				else{
					if(object.getDocType().equals("MMN")){
						double value=-object.getTotal();
						return value+ "";
					}
					else{
						return object.getTotal()+"";
					}
				}
			}
		};
		table.addColumn(getColumntotal, "Total");
		table.setColumnWidth(getColumntotal,90,Unit.PX);
	}


	private void addColumnProdPrice() {
		
		getColumnProdPrice = new TextColumn<MaterialConsumptionReport>() {
			@Override
			public String getValue(MaterialConsumptionReport object) {
				if (object.getPrice() == -1)
					return "N.A";
				else
					return object.getPrice() + "";
			}
		};
		table.addColumn(getColumnProdPrice, "Price");
		table.setColumnWidth(getColumnProdPrice,90,Unit.PX);
		
	}


	private void addColumnProdUOM() {
		
		getColumnProdUOM = new TextColumn<MaterialConsumptionReport>() {
			@Override
			public String getValue(MaterialConsumptionReport object) {
				if (object.getProdUOM()!=null){
					return object.getProdUOM();
				}
				return null;
			}
		};
		table.addColumn(getColumnProdUOM, "UOM");
		table.setColumnWidth(getColumnProdUOM,90,Unit.PX);
	}


	private void addColumnProdQuantity() {
		
		getColumnProdQuantity = new TextColumn<MaterialConsumptionReport>() {
			@Override
			public String getValue(MaterialConsumptionReport object) {
				if (object.getQuantity() == -1)
					return "N.A";
				else
					return object.getQuantity() + "";
			}
		};
		table.addColumn(getColumnProdQuantity, "Quantity");
		table.setColumnWidth(getColumnProdQuantity,90,Unit.PX);
	}


	private void addColumnProdName() {
		
		
		getColumnProdName = new TextColumn<MaterialConsumptionReport>() {
			@Override
			public String getValue(MaterialConsumptionReport object) {
				if (object.getProdName()!=null){
					return object.getProdName();
				}
				return null;
			}
		};
		table.addColumn(getColumnProdName, "Name");
		table.setColumnWidth(getColumnProdName,90,Unit.PX);
		
	}


	private void addColumnProdCode() {
		
		getColumnProdCode = new TextColumn<MaterialConsumptionReport>() {
			@Override
			public String getValue(MaterialConsumptionReport object) {
				if (object.getProdCode()!=null){
					return object.getProdCode();
				}
				return null;
			}
		};
		table.addColumn(getColumnProdCode, "Code");
		table.setColumnWidth(getColumnProdCode,90,Unit.PX);
		
	}


	private void addColumnProdID() {
		getColumnProdID = new TextColumn<MaterialConsumptionReport>() {
			@Override
			public String getValue(MaterialConsumptionReport object) {
				if (object.getProdId() == -1)
					return "N.A";
				else
					return object.getProdId() + "";
			}
		};
		table.addColumn(getColumnProdID, "Prod Id");
		table.setColumnWidth(getColumnProdID,90,Unit.PX);
	}


	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}
	
	
}
