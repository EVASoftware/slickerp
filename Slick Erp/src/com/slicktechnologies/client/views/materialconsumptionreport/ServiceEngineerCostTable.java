package com.slicktechnologies.client.views.materialconsumptionreport;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.inventory.LabourCost;

public class ServiceEngineerCostTable extends SuperTable<LabourCost>{


	TextColumn<LabourCost> getColumnContractId;
	TextColumn<LabourCost> getColumnServiceId;
	TextColumn<LabourCost> getColumnEmpId;
	TextColumn<LabourCost> getColumnEmpName;
	TextColumn<LabourCost> getColumnEmpCell;
	TextColumn<LabourCost> getColumnservicehours;
	TextColumn<LabourCost> getColumnempHourSalary;
	TextColumn<LabourCost> getColumnLobourCost;

	ServiceEngineerCostTable(){
		super();
		setHeight("300px");
	}
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addColumnContractId();
		addColumnServiceId();
		addColumnEmpId();
		addColumnEmpName();
		addColumnEmpCell();
		addColumnservicehours();
		addColumnempHourSalary();
		addColumnLabourCost();
	}
	
	

	private void addColumnempHourSalary() {

		getColumnempHourSalary = new TextColumn<LabourCost>() {
			
			@Override
			public String getValue(LabourCost object) {
				return object.getEmpHourSalary()+"";
			}
		};
		
		table.addColumn(getColumnempHourSalary,"Emp Hour Salary");
		table.setColumnWidth(getColumnempHourSalary, 90, Unit.PX);
	}

	private void addColumnContractId() {
		getColumnContractId = new TextColumn<LabourCost>() {
			
			@Override
			public String getValue(LabourCost object) {
				return object.getContractId()+"";
			}
		};
		table.addColumn(getColumnContractId,"Contract Id");
		table.setColumnWidth(getColumnContractId, 90, Unit.PX);
	}

	private void addColumnServiceId() {
		getColumnServiceId = new TextColumn<LabourCost>() {
			
			@Override
			public String getValue(LabourCost object) {
				return object.getServiceId()+"";
			}
		};
		table.addColumn(getColumnServiceId,"Service Id");
		table.setColumnWidth(getColumnServiceId, 90, Unit.PX);
	}

	private void addColumnEmpId() {
		getColumnEmpId = new TextColumn<LabourCost>() {
			
			@Override
			public String getValue(LabourCost object) {
				return object.getEmployeeId()+"";
			}
		};
		table.addColumn(getColumnEmpId,"Employee Id");
		table.setColumnWidth(getColumnEmpId, 90, Unit.PX);
	}

	private void addColumnEmpName() {
		getColumnEmpName = new TextColumn<LabourCost>() {
			
			@Override
			public String getValue(LabourCost object) {
				return object.getEmployeeName();
			}
		};
		table.addColumn(getColumnEmpName,"Employee Name");
		table.setColumnWidth(getColumnEmpName, 90, Unit.PX);
	}

	private void addColumnEmpCell() {
		getColumnEmpCell = new TextColumn<LabourCost>() {
			
			@Override
			public String getValue(LabourCost object) {
				return object.getEmployeeCell();
			}
		};
		
		table.addColumn(getColumnEmpCell,"Employee Cell");
		table.setColumnWidth(getColumnEmpCell, 90, Unit.PX);
	}

	private void addColumnservicehours() {
		getColumnservicehours = new TextColumn<LabourCost>() {
			
			@Override
			public String getValue(LabourCost object) {
				return object.getServiceHours();
			}
		};
		table.addColumn(getColumnservicehours,"Service Hours");
		table.setColumnWidth(getColumnservicehours,90,Unit.PX);
	}

	private void addColumnLabourCost() {
		getColumnLobourCost = new TextColumn<LabourCost>() {
			
			@Override
			public String getValue(LabourCost object) {
				return object.getLabourCost()+"";
			}
		};
		
		table.addColumn(getColumnLobourCost,"Labour Cost");
		table.setColumnWidth(getColumnLobourCost,90,Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
}
