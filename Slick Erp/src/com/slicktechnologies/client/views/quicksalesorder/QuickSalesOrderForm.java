package com.slicktechnologies.client.views.quicksalesorder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FocusWidget;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.ValueBoxBase.TextAlignment;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.OtherChargesTable;
import com.slicktechnologies.client.views.contract.PaymentTermsTable;
import com.slicktechnologies.client.views.contract.ProductChargesTable;
import com.slicktechnologies.client.views.contract.ProductTaxesTable;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.quickcontract.QuickContractPresentor;
import com.slicktechnologies.client.views.salesorder.SalesOrderLineItemTable;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;

public class QuickSalesOrderForm extends FormScreen<SalesOrder> implements ClickHandler, ChangeHandler{

	ObjectListBox<Branch> olbbBranch;
	TextBox tbReferenceNumber;
	ObjectListBox<Config> olbcPaymentMethods;
	TextBox tbContractId;
	TextBox tbRefeRRedBy;
	DateBox dbReferenceDate;
	TextBox tbQuotationStatus;
	TextArea taDescription;
	UploadComposite ucUploadTAndCs;
	PersonInfoComposite personInfoComposite;
	ObjectListBox<Employee> olbeSalesPerson,olbApproverName;
	ObjectListBox<Config> olbSalesOrderGroup;
	ObjectListBox<Type> olbSalesOrderType;
	ObjectListBox<ConfigCategory> olbSalesOrderCategory;
	ListBox cbcformlis;
	ProductInfoComposite prodInfoComposite;
	DoubleBox dototalamt,doamtincltax,donetpayamt;
	TextBox tbLeadId,tbQuotatinId;
//	IntegerBox ibdays;
//	DoubleBox dopercent;
	IntegerBox ibCreditPeriod;
//	TextBox tbcomment;
//	Button addPaymentTerms;
//	PaymentTermsTable paymentTermsTable;
	SalesOrderLineItemTable salesorderlineitemtable;
	ProductChargesTable chargesTable;
	Button addOtherCharges;
	ProductTaxesTable prodTaxTable;
	Button baddproducts;
	ObjectListBox<TaxDetails> olbcstpercent;
	DateBox dbdeliverydate;
	AddressComposite acshippingcomposite;
	CheckBox cbcheckaddress;
	DateBox dbsalesorderdate;
	TextBox tbremark;
	FormField fgroupingCustomerInformation;
	SalesOrder salesorderobj;
	
	final GenricServiceAsync genasync=GWT.create(GenricService.class);

	/**
	 * Date 11 july 2017 added by vijay for Cform validation
	 */
	Date date = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss").parse("2017-07-01 00:00:00");
	final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
	/**
	 * ends here
	 */
	
	TextBox tbFullName, tbCompanyName;
	PhoneNumberBox pnbCellNo;
	EmailTextBox etbEmail;
	AddressComposite customerAddressComposite;
	DateBox dbinvoiceDate;
	DoubleBox dbpaymentrecieved,dbbalancepayment;
	//ObjectListBox<Branch> olbbCustomerBranch;
	/**
	 * @author Priyanka , Date : 31-12-2020
	 * Des - In customer branch branch drop-down, company branch is reflecting issue raised by Rahul
	 */
	ObjectListBox<CustomerBranchDetails> olbbCustomerBranch;
	boolean customerEditFlag=false;
	NumberFormat numfmt = NumberFormat.getFormat("0.00");
	
	Button btnCopyAddress;
	
	// Date 21 july 2017 added by vijay for warrentyUntil calculation for product level
	public static Date salesorderDate=null;
	
	TextBox tbbankAccNo,tbbankName,tbbankBranch,tbchequeNo,tbreferenceno,tbChequeIssuedBy;
	DateBox dbChequeDate,dbtransferDate;
	
	
	// Date 02-09-2017 added by  vijay for new customer GST Number
	TextBox tbGSTNumber;	
	
	/**
	 * Date : 14-09-2017 BY ANIL
	 * Adding other charges table
	 */
	OtherChargesTable tblOtherCharges;
	Button addOthrChargesBtn;
	DoubleBox dbOtherChargesTotal;
	/**
	 * End
	 */
	/** daet 29/1/2018 added by komal for hvac **/
	public static ArrayList<String> cList = new ArrayList<String>();
	/**
	 * End
	 */
	
	/**
	 * Date 03-05-2018 By vijay
	 * for same product sr no because allow to add duplicate product so
	 * differentiate same product with product SRNO
	 */
	public static int productSrNo=0;
	/**
	 * ends here
	 */
    /**Date 16-7-2020 by Amol added number Range raised by Rahul Tiwari**/
	public ObjectListBox<Config> olbcNumberRange;
	
	public Customer cust;

	
	public QuickSalesOrderForm() {
		super();
		createGui();
		this.tbContractId.setEnabled(false);
		this.tbQuotationStatus.setEnabled(false);
		this.tbQuotationStatus.setText(SalesOrder.CREATED);
		salesorderlineitemtable.connectToLocal();
//		paymentTermsTable.connectToLocal();
		chargesTable.connectToLocal();
		prodTaxTable.connectToLocal();
		
		/**
		 * Date :14-09-2017 By ANIL
		 */
		tblOtherCharges.connectToLocal();
		dbOtherChargesTotal.setEnabled(false);
		/**
		 * ENd
		 */
		 /*
  		 * Ashwini Patil
  		 * Date:14-06-2024
  		 * Ultima search want to map number range directly from branch so they want this field read only
  		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
  		{	olbcNumberRange.setEnabled(false);
  		}
	}
	
	public QuickSalesOrderForm  (String[] processlevel, FormField[][] fields, FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}

	//***********************************Variable Initialization********************************************//

		/**
		 * Method template to initialize the declared variables.
		 */
		@SuppressWarnings("unused")
		private void initalizeWidget()
		{

			tbReferenceNumber=new TextBox();
			olbbBranch=new ObjectListBox<Branch>();
			AppUtility.makeBranchListBoxLive(olbbBranch);
			olbbBranch.addChangeHandler(this);
			olbcPaymentMethods=new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(olbcPaymentMethods,Screen.PAYMENTMETHODS);
			olbcPaymentMethods.addChangeHandler(this);
			tbLeadId=new TextBox();
			tbQuotatinId=new TextBox();
			tbContractId=new TextBox();
			dbsalesorderdate=new DateBoxWithYearSelector();
			tbRefeRRedBy=new TextBox();
			tbQuotationStatus=new TextBox();
			dbReferenceDate=new DateBoxWithYearSelector();
			taDescription=new TextArea();
			ucUploadTAndCs=new UploadComposite();
			salesorderlineitemtable=new SalesOrderLineItemTable();
//			personInfoComposite=AppUtility.customerInfoComposite(new Customer());
			
			MyQuerry custquerry1 = new MyQuerry();
			custquerry1.setQuerryObject(new Customer());
			personInfoComposite = new PersonInfoComposite(custquerry1, false);
			
			olbeSalesPerson=new ObjectListBox<Employee>();
//			AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
			olbeSalesPerson.makeEmployeeLive(AppConstants.SALESMODULE, AppConstants.SALESORDER, "Sales Person");
			olbSalesOrderGroup=new ObjectListBox<Config>();
			olbSalesOrderCategory=new ObjectListBox<ConfigCategory>();
			AppUtility.MakeLiveCategoryConfig(olbSalesOrderCategory, Screen.SALESORDERCATEGORY);
			olbSalesOrderCategory.addChangeHandler(this);
			olbSalesOrderType=new ObjectListBox<Type>();
			AppUtility.makeTypeListBoxLive(olbSalesOrderType,Screen.SALESORDERTYPE);
			AppUtility.MakeLiveConfig(olbSalesOrderGroup, Screen.SALESORDERGROUP);
			olbApproverName=new ObjectListBox<Employee>();
			AppUtility.makeApproverListBoxLive(olbApproverName,"Sales Order");
			this.tbContractId.setEnabled(false);
			this.tbLeadId.setEnabled(false);
			this.tbQuotatinId.setEnabled(false);
			
			prodInfoComposite=AppUtility.initiateSalesProductComposite(new SuperProduct());
			ibCreditPeriod=new IntegerBox();
			chargesTable=new ProductChargesTable();
			addOtherCharges=new Button("+");
			addOtherCharges.addClickHandler(this);
			prodTaxTable=new ProductTaxesTable();
			dototalamt=new DoubleBox();
			dototalamt.setEnabled(false);
			doamtincltax=new DoubleBox();
			doamtincltax.setEnabled(false);
			donetpayamt=new DoubleBox();
			donetpayamt.setEnabled(false);
			
			/**
			 * Date : 14-09-2017 By ANIL
			 */
			tblOtherCharges=new OtherChargesTable();
			
			addOthrChargesBtn=new Button("+");
			addOthrChargesBtn.addClickHandler(this);
			
			dbOtherChargesTotal=new DoubleBox();
			dbOtherChargesTotal.setEnabled(false);
			
			/**
			 * End
			 */
			
			this.changeWidgets();
			
			tbremark=new TextBox();
			tbremark.setEnabled(false);
			
//			dbdeliverydate=new DateBox();
			dbdeliverydate = new DateBoxWithYearSelector();

			cbcheckaddress=new CheckBox();
			cbcheckaddress.setValue(false);
			acshippingcomposite=new AddressComposite();
			acshippingcomposite.setEnable(false);
			
			cbcformlis=new ListBox();
			cbcformlis.addItem("SELECT");
			cbcformlis.addItem(AppConstants.YES);
			cbcformlis.addItem(AppConstants.NO);
			cbcformlis.setEnabled(false);
			olbcstpercent=new ObjectListBox<TaxDetails>();
			olbcstpercent.setEnabled(false);
			initializePercentCst();
			baddproducts=new Button("ADD");
			baddproducts.addClickHandler(this);
			
			
			tbCompanyName =new TextBox();
			tbFullName = new TextBox();
			pnbCellNo =  new PhoneNumberBox();
			etbEmail=new EmailTextBox();
			dbinvoiceDate = new DateBoxWithYearSelector();
			customerAddressComposite = new AddressComposite(true);
			
			dbpaymentrecieved= new DoubleBox();
			dbpaymentrecieved.setWidth("200px");
			dbpaymentrecieved.setAlignment(TextAlignment.CENTER);
			
			dbbalancepayment = new DoubleBox();
			dbbalancepayment.setEnabled(false);
			dbbalancepayment.setWidth("200px");
			dbbalancepayment.setAlignment(TextAlignment.CENTER);
			
			//olbbCustomerBranch = new ObjectListBox<Branch>();
			//AppUtility.makeCustomerBranchListBoxLive(olbbCustomerBranch);
			/**
			 * @author Priyanka , Date : 31-12-2020
			 * Des - In customer branch branch drop-down, company branch is reflecting issue raised by Rahul
			 */
			olbbCustomerBranch=new ObjectListBox<CustomerBranchDetails>();
			olbbCustomerBranch.addItem("--SELECT--");
			olbbCustomerBranch.addChangeHandler(this);
			
			btnCopyAddress = new Button("Copy Address");
			btnCopyAddress.addClickHandler(this);
			
			personInfoComposite.getCustomerId().getHeaderLabel().setText("Customer ID");
			personInfoComposite.getCustomerName().getHeaderLabel().setText("Customer Name");
			personInfoComposite.getCustomerCell().getHeaderLabel().setText("Customer Cell");
			
			
			tbbankAccNo=new TextBox();
			tbbankAccNo.setEnabled(false);
			
			tbbankName=new TextBox();
			tbbankName.setEnabled(false);
			
			tbbankBranch=new TextBox();
			tbbankBranch.setEnabled(false);
			
			tbchequeNo=new TextBox(); 
			tbchequeNo.setEnabled(false);
			
			dbChequeDate = new DateBoxWithYearSelector();
			dbChequeDate.setEnabled(false);
			
			tbreferenceno=new TextBox();
			tbreferenceno.setEnabled(false);
			
			dbtransferDate = new DateBoxWithYearSelector();
			dbtransferDate.setEnabled(false);
			
			tbChequeIssuedBy=new TextBox();
			// Date 02-04-2018 By vijay
			tbChequeIssuedBy.setEnabled(false);
			
			tbGSTNumber = new TextBox();
			olbcNumberRange = new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(olbcNumberRange, Screen.NUMBERRANGE);
			olbcNumberRange.addChangeHandler(this);
			
			 /*
	  		 * Ashwini Patil
	  		 * Date:14-06-2024
	  		 * Ultima search want to map number range directly from branch so they want this field read only
	  		 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
	  		{	olbcNumberRange.setEnabled(false);
	  		}
		}

		/**
		 * method template to create screen formfields
		 */
		@Override
		public void createScreen() {


			initalizeWidget();
		//	setDisabled(paymentComposite);

			//Token to initialize the processlevel menus.
			//this.processlevelBarNames=new String[]{AppConstants.CANCLESALESORDER,AppConstants.CANCELLATIONSUMMARY,AppConstants.WORKORDER,
					//AppConstants.NEW,AppConstants.CREATEAMC,AppConstants.SUBMIT,AppConstants.VIEWINVOICEPAYMENT};
			/**
			 * Date : 28-12-2020 By Priyanka
			 * Des : Change Navigation flow as per requirement of simplification Project raised by Nitin Sir.
			 */
			
			this.processlevelBarNames=new String[]{AppConstants.SUBMIT,AppConstants.CANCLESALESORDER,AppConstants.NEW,AppConstants.WORKORDER,AppConstants.CREATEAMC,AppConstants.VIEWINVOICEPAYMENT,AppConstants.CANCELLATIONSUMMARY};
			
			//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
			//Token to initialize formfield
			String mainScreenLabel="Sales Order";
			if(salesorderobj!=null&&salesorderobj.getCount()!=0){
				mainScreenLabel=salesorderobj.getCount()+" "+"/"+" "+salesorderobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(salesorderobj.getCreationDate());
			}
				//fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
			
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder();
			fgroupingCustomerInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
			
			
			//FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("",personInfoComposite);
			FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Lead ID",tbLeadId);
			FormField ftbLeadId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Quotation ID",tbQuotatinId);
			FormField ftbQuotatinId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Sales Order ID",tbContractId);
			FormField ftbContractId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Reference Number",tbReferenceNumber);
			FormField ftbReferenceNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Referred By",tbRefeRRedBy);
			FormField ftbRefeRRedBy= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Reference Date",dbReferenceDate);
			FormField fdbReferenceDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("* Sales Order Date",dbsalesorderdate);
			FormField fdbsalesorderdate= fbuilder.setMandatory(true).setMandatoryMsg("Sales Order Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder();
			FormField fgroupingDocuments=fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Upload T&Cs",ucUploadTAndCs);
			FormField fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder();
			FormField fgroupingSalesInformation=fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Status",tbQuotationStatus);
			FormField ftbQuotationStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("* Branch",olbbBranch);
			FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(0).build();
			/**@Sheetal:01-02-2022
	               Des : Making Sales person non mandatory,requirement by UDS Water**/
			fbuilder = new FormFieldBuilder("Sales Person",olbeSalesPerson);
			FormField folbeSalesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			/** date 08/12/2017 update by komal for hvac (remove mandatory )**/
			FormField folbcPaymentMethods;
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("QuickSalesOrder", "MakePaymentMethodNonMandatory")){
			fbuilder = new FormFieldBuilder("Payment Methods",olbcPaymentMethods);
			folbcPaymentMethods= fbuilder.setMandatory(false).setMandatoryMsg("Payment Methods is Mandatory").setRowSpan(0).setColSpan(0).build();
			}else{
			fbuilder = new FormFieldBuilder("Payment Methods",olbcPaymentMethods);
			folbcPaymentMethods= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
			}
			fbuilder = new FormFieldBuilder();
			FormField fgroupingDescription=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Description (Max 500 characters)",taDescription);
			FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder();
			FormField fgroupingProducts=fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Sales Order Type",olbSalesOrderType);
			FormField folbContractType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Sales Order Group",olbSalesOrderGroup);
			FormField folbContractGroup= fbuilder.setMandatory(false).setMandatoryMsg("Sales Order Group is Mandatory").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Credit Days",ibCreditPeriod);
			FormField fibCreditPeriod= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Sales Order Category",olbSalesOrderCategory);
			FormField folbContractCategory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("* Approver Name",olbApproverName);
			FormField folbApproverName= fbuilder.setMandatory(true).setMandatoryMsg("Approver Name is Mandatory").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder();
			FormField ftbcomment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Remark",tbremark);
			FormField ftbremark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder();
			FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
			fbuilder = new FormFieldBuilder();
			FormField fblankgroupthree=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
			fbuilder = new FormFieldBuilder();
			FormField fblankgroupone=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",prodTaxTable.getTable());
			FormField fprodTaxTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
			fbuilder = new FormFieldBuilder("Total Amount",doamtincltax);
			FormField fdoAmtIncludingTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Total Amount",dototalamt);
			FormField fpayCompTotalAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("",addOtherCharges);
			FormField faddOtherCharges= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",chargesTable.getTable());
			FormField fchargesTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
			fbuilder = new FormFieldBuilder("Net Payable",donetpayamt);
			FormField fpayCompNetPay= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder();
			FormField fgroupingShippingInfo=fbuilder.setlabel("Shipping Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("* Delivery Date",dbdeliverydate);
			FormField fdbdeliverydate= fbuilder.setMandatory(true).setMandatoryMsg("Delivery Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Is Shipping Address Different",cbcheckaddress);
			FormField fcbcheckaddress= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",acshippingcomposite);
			FormField facshippingcomposite=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("",baddproducts);
			FormField fbaddproducts= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",prodInfoComposite);
			FormField fprodInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
			fbuilder = new FormFieldBuilder("C Form",cbcformlis);
			FormField fcbcform= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("CST Percent",olbcstpercent);
			FormField folbcstpercent= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("",salesorderlineitemtable.getTable());
			FormField fsalesorderlineitemtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			
			
			fbuilder = new FormFieldBuilder("Payment Recieved",dbpaymentrecieved);
			FormField fpaymentrecieved= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder("Balance Payment",dbbalancepayment);
			FormField fbalancepayment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			
			fbuilder = new FormFieldBuilder("Company Name",tbCompanyName);
			FormField ftbCompanyName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Customer Name",tbFullName);
			FormField ftbFullName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Cell No",pnbCellNo);
			FormField fpnbCellNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Email",etbEmail);
			FormField fetbEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			
			fbuilder = new FormFieldBuilder("",customerAddressComposite);
			FormField fcustomerAddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder();
			FormField fgroupingNewCustomerInformation=fbuilder.setlabel(" New Customer ").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder("* Invoice Date",dbinvoiceDate);
			FormField fdbinvoiceDate= fbuilder.setMandatory(true).setMandatoryMsg("Invoice Date is Mandetory").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Customer Branch",olbbCustomerBranch);
			FormField folbbCustomerBranch= fbuilder.setMandatory(false).setMandatoryMsg("Customer Branch is Mandatory").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("",btnCopyAddress);
			FormField fbtnCopyAddress= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			
			fbuilder = new FormFieldBuilder();
			FormField fPaymentInformation=fbuilder.setlabel("Payment Terms").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder("Cheque-Bank Account No.",tbbankAccNo);
			FormField ftbbankAccNo=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Cheque-Bank Name",tbbankName);
			FormField ftbbankName=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Cheque-Bank Branch",tbbankBranch);
			FormField ftbbankBranch=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Cheque No",tbchequeNo);
			FormField fibchequeNo=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Cheque Date",dbChequeDate);
			FormField fdbChequeDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Reference No.",tbreferenceno);
			FormField ftbreferenceno=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Digital-Transfer Date",dbtransferDate);
			FormField fdbtransferDate=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Cheque Issued By",tbChequeIssuedBy);
			FormField ftbChequeIssuedBy=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			
			fbuilder = new FormFieldBuilder("GST Number",tbGSTNumber);
			FormField ftbGSTNumber=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			
			fbuilder = new FormFieldBuilder("",tblOtherCharges.getTable());
			FormField fothChgTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
			
			fbuilder = new FormFieldBuilder("",addOthrChargesBtn);
			FormField faddOthrChargesBtn= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Total Amount",dbOtherChargesTotal);
			FormField fdbOtherChargesTotal= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder("Number Range",olbcNumberRange);
			FormField folbcNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


//			FormField[][] formfield = { 
//					{fgroupingCustomerInformation},
//					{fpersonInfoComposite},
//					
//					{fgroupingNewCustomerInformation},
//					{ftbCompanyName,ftbFullName,fpnbCellNo,fetbEmail},
//					{folbbCustomerBranch, ftbGSTNumber},
//					{fcustomerAddressComposite},
//					
//					{fgroupingSalesInformation},
//					{ftbContractId, ftbQuotationStatus,folbbBranch,folbeSalesPerson,},
//					{folbApproverName,folbContractGroup,folbContractCategory,folbContractType},
//					{folbcPaymentMethods,fibCreditPeriod,ftbLeadId,ftbQuotatinId},
//					{fdbsalesorderdate,ftbremark,ftbReferenceNumber,ftbRefeRRedBy},
//					{fdbReferenceDate,folbcNumberRange,fdbinvoiceDate},
//					
//					{fPaymentInformation},
//					{ftbbankAccNo,ftbbankName,ftbbankBranch,fibchequeNo},
//					{fdbChequeDate,fdbtransferDate,ftbreferenceno,ftbChequeIssuedBy},
//					
//					{fgroupingDocuments},
//					{fucUploadTAndCs},
//					{fgroupingDescription},
//					{ftaDescription},
//					{fgroupingProducts},
//					{fprodInfoComposite,fbaddproducts},
//					{fsalesorderlineitemtable},
//					{fblankgroupthree,fpayCompTotalAmt},
//					
//					{fblankgroup,faddOthrChargesBtn},
//					{fblankgroup,fothChgTbl},/** Date : 14-09-2017 BY ANIL**/
//					{fblankgroupthree,fdbOtherChargesTotal},
//					
//					{fblankgroupthree},
//					{fblankgroup,fprodTaxTable},
//					
////					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
////					{fblankgroup,fchargesTable},
//					
//					{fblankgroup,fblankgroupone,fpayCompNetPay},
//					{fblankgroup,fblankgroupone,fpaymentrecieved},
//					{fblankgroup,fblankgroupone,fbalancepayment},
//					{fgroupingShippingInfo},
//					{fdbdeliverydate,fcbcheckaddress,fbtnCopyAddress},
//					{facshippingcomposite}
			
			
			
			FormField[][] formfield = { 
					/**MAIN SCREEN**/
					{fgroupingCustomerInformation},
					{fpersonInfoComposite},
					{fdbsalesorderdate,fdbinvoiceDate,folbbBranch,folbeSalesPerson},
					{folbcNumberRange,folbApproverName,ftbremark},
					/** New Customer Details**/
					{fgroupingNewCustomerInformation},
					{ftbCompanyName,ftbFullName,fpnbCellNo,fetbEmail},
					{folbbCustomerBranch, ftbGSTNumber},
					{fcustomerAddressComposite},
					/** Product Details**/
					{fgroupingProducts},
					{fprodInfoComposite,fbaddproducts},
					{fsalesorderlineitemtable},
					{fblankgroupthree,fpayCompTotalAmt},
					{fblankgroup,faddOthrChargesBtn},
					{fblankgroup,fothChgTbl},
					{fblankgroupthree,fdbOtherChargesTotal},
					{fblankgroupthree},
					{fblankgroup,fprodTaxTable},
					{fblankgroup,fblankgroupone,fpayCompNetPay},
					{fblankgroup,fblankgroupone,fpaymentrecieved},
					{fblankgroup,fblankgroupone,fbalancepayment},
					/** Delivery Details**/
					{fgroupingShippingInfo},
					{fdbdeliverydate,fcbcheckaddress},
					{facshippingcomposite},
					/** Payment Details**/
					{fPaymentInformation},
					{folbcPaymentMethods,fibCreditPeriod,fibchequeNo,fdbChequeDate},
					{ftbbankAccNo,ftbbankName,ftbbankBranch,ftbChequeIssuedBy},
					{fdbtransferDate,ftbreferenceno,},
					/** Classification**/
					{fgroupingSalesInformation},
					{folbContractGroup,folbContractCategory,folbContractType},
					/** Reference/Genral**/
					{fgroupingDescription},
					{ftbLeadId,ftbQuotatinId,ftbReferenceNumber,fdbReferenceDate},
					{ftbRefeRRedBy},
					{ftaDescription},
					/**Attachment**/
					{fgroupingDocuments},
					{fucUploadTAndCs}

			};

			this.fields=formfield;

		}
		
		/**
		 * method template to update the model with token entity name
		 */
		@SuppressWarnings("unchecked")
		@Override
		public void updateModel(SalesOrder model) 
		{
			if(personInfoComposite.getValue()!=null)
				model.setCinfo(personInfoComposite.getValue());
			if(tbReferenceNumber.getValue()!=null)
				model.setRefNo(tbReferenceNumber.getValue());
			if(tbRefeRRedBy.getValue()!=null)
				model.setReferedBy(tbRefeRRedBy.getValue());
			if(dbReferenceDate.getValue()!=null){
				model.setReferenceDate(dbReferenceDate.getValue());
			}
			//if(validuntil.getValue()!=null)
				//model.setValidUntill(validuntil.getValue());
			   
				model.setDocument(ucUploadTAndCs.getValue());
			if(tbQuotationStatus.getValue()!=null)
				model.setStatus(tbQuotationStatus.getValue());
			if(dbsalesorderdate.getValue()!=null)
				model.setSalesOrderDate(dbsalesorderdate.getValue());
			if(olbbBranch.getValue()!=null)
				model.setBranch(olbbBranch.getValue());
			if(olbeSalesPerson.getValue()!=null)
				model.setEmployee(olbeSalesPerson.getValue());
			if(olbcPaymentMethods.getValue()!=null)
				model.setPaymentMethod(olbcPaymentMethods.getValue());
			if(taDescription.getValue()!=null)
				model.setDescription(taDescription.getValue());
			if(olbSalesOrderType.getValue()!=null)
				model.setType(olbSalesOrderType.getValue(olbSalesOrderType.getSelectedIndex()));
			if(olbSalesOrderGroup.getValue()!=null)
				model.setGroup(olbSalesOrderGroup.getValue());
			if(olbSalesOrderCategory.getValue()!=null)
				model.setCategory(olbSalesOrderCategory.getValue());
			if(olbApproverName.getValue()!=null)
				model.setApproverName(olbApproverName.getValue());
			if(dototalamt.getValue()!=null)
				model.setTotalAmount(dototalamt.getValue());
			if(donetpayamt.getValue()!=null)
				model.setNetpayable(donetpayamt.getValue());
			if(!tbLeadId.getValue().equals(""))
				model.setLeadCount(Integer.parseInt(tbLeadId.getValue()));
			if(!tbQuotatinId.getValue().equals(""))
				model.setQuotationCount(Integer.parseInt(tbQuotatinId.getValue()));
			if(!tbContractId.getValue().equals(""))
				model.setContractCount(Integer.parseInt(tbContractId.getValue()));
			
			if(ibCreditPeriod.getValue()!=null){
				model.setCreditPeriod(ibCreditPeriod.getValue());
			}
			
			if(cbcformlis.getSelectedIndex()!=0){
				model.setCformstatus(cbcformlis.getItemText(cbcformlis.getSelectedIndex()));
			}
			
			if(olbcstpercent.getValue()!=null){
				TaxDetails cstPerc=olbcstpercent.getSelectedItem();
				model.setCstpercent(cstPerc.getTaxChargePercent());
			}
			
			if(olbcstpercent.getValue()!=null){
				model.setCstName(olbcstpercent.getValue());
			}
			
			List<SalesLineItem> saleslis=this.getSalesorderlineitemtable().getDataprovider().getList();
			ArrayList<SalesLineItem> arrItems=new ArrayList<SalesLineItem>();
			arrItems.addAll(saleslis);
			model.setItems(arrItems);
			
			List<ProductOtherCharges> productTaxLis=this.prodTaxTable.getDataprovider().getList();
			ArrayList<ProductOtherCharges> productTaxArr=new ArrayList<ProductOtherCharges>();
			productTaxArr.addAll(productTaxLis);
			model.setProductTaxes(productTaxArr);
			
			/**
			 * Date : 20-09-2017 BY ANIL
			 */
			List<OtherCharges> otherChargesList=this.tblOtherCharges.getDataprovider().getList();
			ArrayList<OtherCharges> otherChargesArr=new ArrayList<OtherCharges>();
			otherChargesArr.addAll(otherChargesList);
			model.setOtherCharges(otherChargesArr);
			
			/**
			 * End
			 */
			
			List<ProductOtherCharges> productChargeLis=this.chargesTable.getDataprovider().getList();
			ArrayList<ProductOtherCharges> productChargeArr=new ArrayList<ProductOtherCharges>();
			productChargeArr.addAll(productChargeLis);
			model.setProductCharges(productChargeArr);
			
			if(dbdeliverydate.getValue()!=null)
				model.setDeliveryDate(dbdeliverydate.getValue());
			if(cbcheckaddress.getValue()!=null)
				model.setCheckAddress(cbcheckaddress.getValue());
			if(acshippingcomposite.getValue()!=null)
				model.setShippingAddress(acshippingcomposite.getValue());
			
			
			
			if(dbpaymentrecieved.getValue()!=null){
				model.setPaymentRecieved(dbpaymentrecieved.getValue());
			}
			if(dbbalancepayment.getValue()!=null){
				model.setBalancePayment(dbbalancepayment.getValue());
			}
			
			ArrayList<PaymentTerms> PayTermslist=new ArrayList<PaymentTerms>();
			PayTermslist = getpaymentterms(donetpayamt.getValue(),dbpaymentrecieved.getValue());
			model.setPaymentTermsList(PayTermslist);

			
			if(tbCompanyName.getValue()!=null){
				model.setNewcompanyname(tbCompanyName.getValue());
			}
			
			if(tbFullName.getValue()!=null){
				model.setNewcustomerfullName(tbFullName.getValue());
			}
			
			if(pnbCellNo.getValue()!=null){
				model.setNewcustomercellNumber(pnbCellNo.getValue());
			}
			
			if(etbEmail.getValue()!=null){
				model.setNewcustomerEmail(etbEmail.getValue());
			}
			
			if(customerAddressComposite.getValue()!=null){
				model.setNewcustomerAddress(customerAddressComposite.getValue());
			}
			
			if(olbbCustomerBranch.getSelectedIndex()!= 0)
			{
				model.setNewCustomerBranch(olbbCustomerBranch.getValue(olbbCustomerBranch.getSelectedIndex()).trim());
			}
			if(model.getNewcustomerfullName()!=null && !model.getNewcustomerfullName().equals("") && model.getCinfo().getCount()==0){
				System.out.println("VIJAY");
				customerEditFlag = true;
			}
			
			if(model.getNewcompanyname()!=null && !model.getNewcompanyname().equals("") && model.getCinfo().getCount()==0){
				customerEditFlag = true;
			}
			if(dbinvoiceDate.getValue()!=null){
				model.setQuickSalesInvoiceDate(dbinvoiceDate.getValue());
			}
			
			
			if(tbbankAccNo.getValue()!=null)
				model.setBankAccNo(tbbankAccNo.getValue());
			if(tbbankName.getValue()!=null)
				model.setBankName(tbbankName.getValue());
			if(tbbankBranch.getValue()!=null)
				model.setBankBranch(tbbankBranch.getValue());
			if(tbchequeNo.getValue()!=null)
				model.setChequeNo(tbchequeNo.getValue());
			if(dbChequeDate.getValue()!=null)
				model.setChequeDate(dbChequeDate.getValue());
			if(tbreferenceno.getValue()!=null)
				model.setReferenceNo(tbreferenceno.getValue());
			if(dbtransferDate.getValue()!=null)
				model.setAmountTransferDate(dbtransferDate.getValue());
			if(!tbChequeIssuedBy.getValue().equals("")){
				model.setChequeIssuedBy(tbChequeIssuedBy.getValue());
			}
			
			model.setQuickSalesOrder(true);
			
			if(tbGSTNumber.getValue()!=null){
				model.setCustomerGSTNumber(tbGSTNumber.getValue());
			}
			 if(olbcNumberRange.getValue()!=null){
				  model.setNumberRange(olbcNumberRange.getValue(olbcNumberRange.getSelectedIndex()));
			  }else{
				  /** date 9.10.2018 added by komal**/
				  model.setNumberRange("");
			  }
			salesorderobj=model;
			presenter.setModel(model);

		}

		/**
		 * method template to update the view with token entity name
		 */
		@SuppressWarnings("unchecked")
		@Override
		public void updateView(SalesOrder view) 
		{
			salesorderobj=view;
	        	tbContractId.setValue(view.getCount()+"");
	        	
	        	/**
	    		 * Date 20-04-2018 By vijay for orion pest locality load  
	    		 */
	    		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
	    			setAddressDropDown();
	    		}
	    		/**
	    		 * ends here
	    		 */
	    		
//	        	this.checkCustomerStatus(view.getCinfo().getCount());
	        	
			if(view.getCinfo()!=null)
				personInfoComposite.setValue(view.getCinfo());
			if(view.getRefNo()!=null)
				tbReferenceNumber.setValue(view.getRefNo());
			if(view.getReferedBy()!=null)
				tbRefeRRedBy.setValue(view.getReferedBy());
			if(view.getReferenceDate()!=null)
				dbReferenceDate.setValue(view.getReferenceDate());
			if(view.getSalesOrderDate()!=null)
				dbsalesorderdate.setValue(view.getSalesOrderDate());
			if(view.getDocument()!=null)
				ucUploadTAndCs.setValue(view.getDocument());
			if(view.getStatus()!=null)
				tbQuotationStatus.setValue(view.getStatus());
			if(view.getBranch()!=null)
				olbbBranch.setValue(view.getBranch());
			if(view.getEmployee()!=null)
				olbeSalesPerson.setValue(view.getEmployee());
			if(view.getPaymentMethod()!=null)
				olbcPaymentMethods.setValue(view.getPaymentMethod());
			if(view.getDescription()!=null)
				taDescription.setValue(view.getDescription());
			/*if(view.getPayments()!=null)
				paymenttable.setValue(view.getPayments());*/
			if(view.getCategory()!=null)
				olbSalesOrderCategory.setValue(view.getCategory());
			if(view.getType()!=null)
				olbSalesOrderType.setValue(view.getType());
			if(view.getGroup()!=null)
				olbSalesOrderGroup.setValue(view.getGroup());
			if(view.getApproverName()!=null)
				olbApproverName.setValue(view.getApproverName());

			if(view.getCreditPeriod()!=null){
				ibCreditPeriod.setValue(view.getCreditPeriod());
			}
			
			if(view.getLeadCount()!=-1)
				tbLeadId.setValue(view.getLeadCount()+"");
			if(view.getQuotationCount()!=-1)
				tbQuotatinId.setValue(view.getQuotationCount()+"");

				
			if(view.getDeliveryDate()!=null)
				dbdeliverydate.setValue(view.getDeliveryDate());
			if(view.getCheckAddress()!=null)
				cbcheckaddress.setValue(view.getCheckAddress());
			if(view.getShippingAddress()!=null)
				acshippingcomposite.setValue(view.getShippingAddress());
			salesorderlineitemtable.setValue(view.getItems());
			if(view.getCformstatus()!=null)
			{
				for(int i=0;i<cbcformlis.getItemCount();i++)
				{
					String data=cbcformlis.getItemText(i);
					if(data.equals(view.getCformstatus()))
					{
						cbcformlis.setSelectedIndex(i);
						break;
					}
				}
			}
			
			if(view.getRemark()!=null){
				tbremark.setValue(view.getRemark());
			}
			
			olbcstpercent.setValue(view.getCstName());
			prodTaxTable.setValue(view.getProductTaxes());
			
			/**
			 * Date : 20-09-2017 BY ANIL
			 */
			tblOtherCharges.setValue(view.getOtherCharges());
			
			if(view.getOtherCharges()!=null){
				double sum=0;
				for(OtherCharges oc:view.getOtherCharges()){
					sum=sum+oc.getAmount();
				}
				dbOtherChargesTotal.setValue(sum);
			}
			/**
			 * End
			 */
			chargesTable.setValue(view.getProductCharges());
			dototalamt.setValue(view.getTotalAmount());
			donetpayamt.setValue(view.getNetpayable());
			
			
			if(view.getPaymentRecieved()!=0){
				dbpaymentrecieved.setValue(view.getPaymentRecieved());
			}
			
			if(view.getBalancePayment()!=0){
				dbbalancepayment.setValue(view.getBalancePayment());
				
			}
			
			
			if(view.getNewcompanyname()!=null){
				tbCompanyName.setValue(view.getNewcompanyname());
			}
			
			if(view.getNewcustomerfullName()!=null){
				tbFullName.setValue(view.getNewcustomerfullName());
			}
			
			if(view.getNewcustomercellNumber()!=null){
				pnbCellNo.setValue(view.getNewcustomercellNumber());
			}
			
			if(view.getNewcustomerEmail()!=null){
				etbEmail.setValue(view.getNewcustomerEmail());
			}
			
			if(view.getNewcustomerAddress()!=null){
				customerAddressComposite.setValue(view.getNewcustomerAddress());
			}
			
			if(view.getNewCustomerBranch()!= null && !view.getNewCustomerBranch().equals(""))
			{
				olbbCustomerBranch.setValue(view.getNewCustomerBranch());
			}
			
			if(view.getQuickSalesInvoiceDate()!=null){
				dbinvoiceDate.setValue(view.getQuickSalesInvoiceDate());
			}
			
			
			if(view.getBankAccNo()!=null)
				tbbankAccNo.setValue(view.getBankAccNo());
			if(view.getBankName()!=null)
				tbbankName.setValue(view.getBankName());
			if(view.getBankBranch()!=null)
				tbbankBranch.setValue(view.getBankBranch());
			if(view.getChequeNo()!=null)
				tbchequeNo.setValue(view.getChequeNo());
			if(view.getChequeDate()!=null)
				dbChequeDate.setValue(view.getChequeDate());
			if(view.getReferenceNo()!=null)
				tbreferenceno.setValue(view.getReferenceNo());
			if(view.getAmountTransferDate()!=null)
				dbtransferDate.setValue(view.getAmountTransferDate());
			if(view.getChequeIssuedBy()!=null && !view.getChequeIssuedBy().equals(""))
				tbChequeIssuedBy.setValue(view.getChequeIssuedBy());
			
			if(view.getCustomerGSTNumber()!=null){
				tbGSTNumber.setValue(view.getCustomerGSTNumber());
			}
			if(view.getNumberRange()!=null)
				olbcNumberRange.setValue(view.getNumberRange());
			presenter.setModel(view);
		}
		
		
		/**
		 * @author Priyanka , Date : 31-12-2020
		 * Des - In customer branch branch drop-down, company branch is reflecting issue raised by Rahul
		 */
		public void loadCustomerBranch(final String custBranch,int custId) {
			// TODO Auto-generated method stub
			GenricServiceAsync async=GWT.create(GenricService.class);
			MyQuerry querry=new MyQuerry();
			Filter temp=null;
			Vector<Filter> vecList=new Vector<Filter>();
			
			temp=new Filter();
			temp.setQuerryString("cinfo.count");
			temp.setIntValue(custId);
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			vecList.add(temp);
			
			querry.setFilters(vecList);
			querry.setQuerryObject(new CustomerBranchDetails());
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					olbbCustomerBranch.clear();
					olbbCustomerBranch.addItem("--SELECT--");
//					tbCustPocName.setValue("");
					if(result!=null&&result.size()!=0){
						ArrayList<CustomerBranchDetails>custBranchList=new ArrayList<CustomerBranchDetails>();
						for(SuperModel model:result){
							CustomerBranchDetails obj=(CustomerBranchDetails) model;
							custBranchList.add(obj);
						}
						
						olbbCustomerBranch.setListItems(custBranchList);
						if(custBranch!=null){
							olbbCustomerBranch.setValue(custBranch);
						}
					}
				}
			});
			
		}


		/**
		 * Date 20-04-2018 By vijay
		 * here i am refreshing address composite data if global list size greater than drop down list size
		 * because we are adding locality and city from an entity in view state to global list if doest not exist in global list 
		 */
		private void setAddressDropDown() {
			if(LoginPresenter.globalLocality.size()>customerAddressComposite.locality.getItems().size()
				|| LoginPresenter.globalLocality.size()>acshippingcomposite.locality.getItems().size()	){
				customerAddressComposite.locality.setListItems(LoginPresenter.globalLocality);
				acshippingcomposite.locality.setListItems(LoginPresenter.globalLocality);
			}
		}
		/**
		 * ends here 
		 */
		
		
		private ArrayList<PaymentTerms> getpaymentterms(Double netpayble, Double paymentrecieved) {

			double percentage = 0 ;

			System.out.println(" net payable =="+netpayble);
			System.out.println(" payment recived === "+paymentrecieved);
			if(paymentrecieved==null || paymentrecieved==0){
				percentage = 100;
			}else{
				percentage = getpercentage(netpayble,paymentrecieved);
			}
				
			System.out.println(" percentage  == "+percentage);
			ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
			
			if(100-percentage==0 || 100-percentage==100.0){
				
				if(paymentrecieved == null || paymentrecieved==0){
					
					PaymentTerms paymentTerms=new PaymentTerms();

					System.out.println(" for 100 % balance payment ");
					paymentTerms.setPayTermDays(0);
					
					String pay = numfmt.format(percentage);
					double paypercent= Double.parseDouble(pay);
					
					paymentTerms.setPayTermPercent(paypercent);
					paymentTerms.setPayTermComment("Balance Payment");
					billingPayTerms.add(paymentTerms);
					return billingPayTerms;
				}
				else{
					
					PaymentTerms paymentTerms=new PaymentTerms();

					System.out.println(" for 100 % payment recieved ");
					paymentTerms.setPayTermDays(0);
//					double paypercent= Math.round(percentage);
					String pay = numfmt.format(percentage);
					double paypercent= Double.parseDouble(pay);
					
					paymentTerms.setPayTermPercent(paypercent);
					paymentTerms.setPayTermComment("Payment Recieved");
					billingPayTerms.add(paymentTerms);
					return billingPayTerms;
				}
				
			}else{
				
				for(int i=0;i<2;i++){
					System.out.println(" for partial payment");
					if(i==1){
						double percent = 100-percentage;
						System.out.println("Percentage ==== $$$$$$$$$$$$$$$$$ "+percent);
						PaymentTerms paymentTerms=new PaymentTerms();
						paymentTerms.setPayTermDays(0);  
						double paypercent= Math.round(percent);
						paymentTerms.setPayTermPercent(paypercent);
						paymentTerms.setPayTermComment("Balance Payment");
						billingPayTerms.add(paymentTerms);
						
						return billingPayTerms;

					}else{
						PaymentTerms paymentTerms=new PaymentTerms();

						System.out.println("Payment recieved billing doc ================");
						paymentTerms.setPayTermDays(0);
						
						double paypercent= Math.round(percentage);

						paymentTerms.setPayTermPercent(paypercent);
						paymentTerms.setPayTermComment("Payment Recieved");
						billingPayTerms.add(paymentTerms);

					}
					
				}
			
			}
			
			return billingPayTerms;
		}

		private double getpercentage(Double netpayble, Double paymentrecieved) {
			double totalpercentage;
			totalpercentage= paymentrecieved/netpayble*100;
			return totalpercentage;
		}
		
		/**
		 * Toggles the app header bar menus as per screen state
		 */

		public void toggleAppHeaderBarMenu() {
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				/**
				 * @author Anil @since 10-12-2021
				 */
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		   
					
				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				/**
				 * @author Anil @since 10-12-2021
				 */
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
						menus[k].setVisible(true); 
					
					else
						menus[k].setVisible(false);  		   
				}
			}

			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				/**
				 * @author Anil @since 10-12-2021
				 */
				if(isPopUpAppMenubar()){
					 menus=GeneralViewDocumentPopup.applevelMenu;
				}
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Edit")||text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
							menus[k].setVisible(true); 
					
					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.QUICKSALESORDER,LoginPresenter.currentModule.trim());
		}

	/**
	 * Toogels the Application Process Level Menu as Per Application State
	 */
		public void toggleProcessLevelMenu()
		{
			SalesOrder entity=(SalesOrder) presenter.getModel();
			
			String status=entity.getStatus();

			for(int i=0;i<getProcesslevelBarNames().length;i++)
			{
				InlineLabel label=getProcessLevelBar().btnLabels[i];
				String text=label.getText().trim();

				if(status.equals(SalesOrder.CREATED))
				{
					if(text.contains(AppConstants.SUBMIT))
						label.setVisible(true);
					if(text.contains(AppConstants.CANCLESALESORDER))
						label.setVisible(false);
					if(text.contains(AppConstants.CANCELLATIONSUMMARY))
						label.setVisible(false);
					if(text.contains(AppConstants.WORKORDER))
						label.setVisible(false);
					if(text.contains(AppConstants.CREATEAMC))
						label.setVisible(false);
					
					if(text.contains(AppConstants.VIEWINVOICEPAYMENT))
						label.setVisible(false);
				}
				else if(status.equals(SalesOrder.APPROVED))
				{
					if(text.equals(AppConstants.Approve))
						label.setVisible(false);	
//					if((text.contains(AppConstants.CANCLESALESORDER)))
//						label.setVisible(true);
					
					/**
					 * rohan added this code for NBHC Changes ADMIN role should  
					 */
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
						if((text.contains(AppConstants.CANCLESALESORDER)))
							label.setVisible(true);
					}
					else
					{
						if((text.contains(AppConstants.CANCLESALESORDER)))
							label.setVisible(false);
					}
					if(text.contains(AppConstants.SUBMIT))
						label.setVisible(false);
					if((text.contains(AppConstants.CANCELLATIONSUMMARY)))
						label.setVisible(false);
					if((text.contains(AppConstants.WORKORDER)))
						label.setVisible(true);
					if(text.contains(AppConstants.CREATEAMC))
						label.setVisible(true);
					if(text.contains(AppConstants.VIEWINVOICEPAYMENT))
						label.setVisible(true);
				}

				else if(status.equals(SalesOrder.SALESORDERCANCEL))
				{
					if((text.contains(AppConstants.NEW)))
						label.setVisible(true);
					else
						label.setVisible(false);
					
					if((text.contains(AppConstants.CANCELLATIONSUMMARY)))
						label.setVisible(true);
					
					if((text.contains(AppConstants.CANCLESALESORDER)))
						label.setVisible(false);
					
					if(text.contains(AppConstants.SUBMIT))
						label.setVisible(false);
					
					if(text.contains(AppConstants.CREATEAMC))
						label.setVisible(false);
					
					if(text.contains(AppConstants.VIEWINVOICEPAYMENT))
						label.setVisible(false);
				}
				
//				else if(status.equals(SalesOrder.REQUESTED))
//				{
//					if((text.contains(ManageApprovals.APPROVALREQUEST)))
//						label.setVisible(false);
//					else
//						label.setVisible(true);
//					
//					if((text.contains(AppConstants.CANCLESALESORDER)))
//						label.setVisible(false);
//					if((text.contains(AppConstants.CANCELLATIONSUMMARY)))
//						label.setVisible(false);
//					if((text.contains(AppConstants.WORKORDER)))
//						label.setVisible(false);
//				}
				
//				else if(status.equals(SalesOrder.REJECTED))
//				{
//					if(text.contains(AppConstants.NEW))
//						label.setVisible(true);
//					else
//						label.setVisible(false);
//					
//					if((text.contains(AppConstants.CANCELLATIONSUMMARY)))
//						label.setVisible(false);
//					if((text.contains(AppConstants.CANCLESALESORDER)))
//						label.setVisible(false);
//				}
			}
		}
		/**
		 * Sets the Application Header Bar as Per Status
		 */

		public void setAppHeaderBarAsPerStatus()
		{
			SalesOrder entity=(SalesOrder) presenter.getModel();
			String status=entity.getStatus();
			
			if(status.equals(SalesOrder.SALESORDERCANCEL)||status.equals(SalesOrder.REJECTED)||status.equals(SalesOrder.APPROVED))
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")
							||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
					{
						menus[k].setVisible(true); 
						System.out.println("Value of text is "+menus[k].getText());
					}
					else
					{
						menus[k].setVisible(false);  
					}
				}
			}

			if(status.equals(SalesOrder.APPROVED))
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")||text.contains("Email")
							||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
					{
						menus[k].setVisible(true); 

					}
					else
					{
						menus[k].setVisible(false);  
					}
				}
			}
			
			if(status.equals(SalesOrder.REQUESTED))
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION))
					{
						menus[k].setVisible(true); 
					}
					else
					{
						menus[k].setVisible(false);  
					}
				}
			}
			
			if(status.equals(SalesOrder.REJECTED))
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains("Edit")||text.contains(AppConstants.NAVIGATION))
					{
						menus[k].setVisible(true); 
					}
					else
					{
						menus[k].setVisible(false);  
					}
				}
			}
		}
		
		
		/**
		 * The method is responsible for changing Application state as per 
		 * status
		 */
		public void setMenuAsPerStatus()
		{
			this.setAppHeaderBarAsPerStatus();
			this.toggleProcessLevelMenu();
		}

		
		/**
		 * sets the id textbox with the passed count value. 
		 */
		@Override
		public void setCount(int count)
		{
			tbContractId.setValue(count+"");
		}
		
		/****************************************On Click Mehtod*******************************************/
		/**
		 * This method is called on click of any widget.
		 * 
		 * 	Involves Payment Terms Button  (ADD)
		 *  Involves Add Charges Button (Plus symbol)
		 * 	Involves OtmComposite i.e Products which are added
		 */
		
		
		
		@Override
		public void onClick(ClickEvent event) 
		{
//			if(event.getSource().equals(addPaymentTerms)){
//				reactOnAddPaymentTerms();
//			}
			if(event.getSource().equals(addOtherCharges)){
				int size=chargesTable.getDataprovider().getList().size();
				if(this.doamtincltax.getValue()!=null){
					ProductOtherCharges prodCharges=new ProductOtherCharges();
					prodCharges.setAssessableAmount(this.getDoamtincltax().getValue());
					prodCharges.setFlagVal(false);
					prodCharges.setIndexCheck(size+1);
					this.chargesTable.getDataprovider().getList().add(prodCharges);
				}
			}
			if(event.getSource().equals(baddproducts)){
				boolean addressFlag=true;
//				if(personInfoComposite.getId().getValue().equals("")){
//	
//					showDialogMessage("Please add customer information!");
//				}
				
				
				
//				if(acshippingcomposite.getState().getSelectedIndex()==0&&acshippingcomposite.getCountry().getSelectedIndex()==0&&acshippingcomposite.getCity().getSelectedIndex()==0)
//				{
//					addressFlag=false;
//					showDialogMessage("Please select shipping address!");
//				}
				
				/**
				 * Date 12 july 2017 added by vijay for GST and C form manage
				 */
				if(dbsalesorderdate.getValue()==null){
					showDialogMessage("Please add Sales Order date first");
					return;
				}
				/**
				 * ends here
				 */

				if(!prodInfoComposite.getProdCode().getValue().equals("")){
//					boolean productValidation=validateProductDuplicate(prodInfoComposite.getProdCode().getValue().trim());

					if(personInfoComposite.getId().getValue().equals("") && this.tbFullName.getValue().equals("") && this.tbCompanyName.getValue().equals("")){
						showDialogMessage("Please add customer information!");
						return;
					}
					if(!this.tbFullName.getValue().equals("") || !this.tbCompanyName.getValue().equals("")  ){
						if(this.customerAddressComposite.getAdressline1().getValue().equals("") || this.customerAddressComposite.getCountry().getSelectedIndex()==0 || this.customerAddressComposite.getState().getSelectedIndex()==0 || this.customerAddressComposite.getCity().getSelectedIndex()==0 ){
							showDialogMessage("Please add new customer complete address!");
							return;
						}
						if(acshippingcomposite.getCountry().getSelectedIndex()==0 ||acshippingcomposite.getState().getSelectedIndex()==0 ||acshippingcomposite.getCity().getSelectedIndex()==0){
							showDialogMessage("Please add new customer shipping address");
							return;
						}
							
					}
					
					if(olbbBranch.getSelectedIndex() == 0){
						showDialogMessage("Please select Branch.");
						return;
					}
					
					/**
					 * Date 03-05-2018 By Vijay 
					 * for duplicate product allow to add
					 */
					
//					if(productValidation==true){
//						showDialogMessage("Product already exists!");
//					}
//					else{
						if(validateState()){
							this.ProdctType(this.prodInfoComposite.getProdCode().getValue().trim(),this.prodInfoComposite.getProdID().getValue().trim());
						}
						if(!validateState()&&validateCForm()){
							this.ProdctType(this.prodInfoComposite.getProdCode().getValue().trim(),this.prodInfoComposite.getProdID().getValue().trim());
						}
//					}
				}
			}
			
			if(event.getSource().equals(btnCopyAddress)){
				System.out.println("inside copy address");
				System.out.println(customerAddressComposite.getAdressline1().getValue());
//				if(salesorderlineitemtable.getDataprovider().getList().size()==0){
//					showDialogMessage("Please add product");
//				}
//				else if(customerAddressComposite.getAdressline1().getValue().equals("")){
//					showDialogMessage("Please Add new customer addess first");
//				}
				if(!this.tbFullName.getValue().equals("") || !this.tbCompanyName.getValue().equals("")  ){

					acshippingcomposite.clear();
					acshippingcomposite.setValue(customerAddressComposite.getValue());
				}
			}
			
			/**
			 * Date : 14-09-2017 By ANIL
			 */
				
			if(event.getSource().equals(addOthrChargesBtn)){
				Tax tax1=new Tax();
				Tax tax2=new Tax();
				OtherCharges prodCharges=new OtherCharges();
				prodCharges.setOtherChargeName("");
				prodCharges.setAmount(0);
				prodCharges.setTax1(tax1);
				prodCharges.setTax2(tax2);
				this.tblOtherCharges.getDataprovider().getList().add(prodCharges);
			}
			/**
			 * End
			 */
		}
		
		public boolean validateCForm()
		{
			
			/**
			 * Date 12 july 2017 added by vijay for GST
			 * below code is applicable only if quotation date is less than 1 july 2017 for Cform
			 * below if condition added by vijay
			 */
			if(dbsalesorderdate.getValue().before(date)){
				
			
			if(!validateState()&&this.getCbcformlis().getSelectedIndex()==0){
				showDialogMessage("Please select C Form");
				return false;
			}
			if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
				if(this.getOlbcstpercent().getSelectedIndex()==0){
					showDialogMessage("Please select CST Percent");
					return false;
				}
				else{
					return true;
				}
			}
			
			}
			return true;
			
			/**
			 * ends here
			 */
		}
		
		public boolean validateState()
		{
			

			/** Date 12 july 2017 added by vijay for GST
			 * below if condition code is applicable only if Sales order date is less than 1 july 2017 for Cform
			 * below if condition added by vijay and else block for no need this state validation for 1 july 2017 after sales order
			 */
			/* 27-09-2017  by sagar sore first line of if condition is added to avoid null pointer exception while redireting to quick order **/
			if(this.dbsalesorderdate.getValue()!=null&&this.dbsalesorderdate.getValue().before(date)){
				if(this.getAcshippingcomposite().getState().getValue().trim().equals(QuickSalesOrderPresenter.companyStateVal.trim())){
					return true;
				}
				return false;
			}else{
				return true;	
			}
		}

		
		
		public TextBox getstatustextbox() {
			return this.tbQuotationStatus;
		}

		/******************************React On Add Payment Terms Method*******************************/
		/**
		 * This method is called when add button of payment terms is clicked.
		 * This method involves the inclusion of payment terms in the corresponding table
		 */
		
//		public void reactOnAddPaymentTerms()
//		{
//			int payTrmDays=0;
//			double payTrmPercent=0;
//			String payTrmComment="";
//			boolean flag=true;
//			boolean payTermsFlag=true;
//			
//			if(tbcomment.getValue()==null||tbcomment.getValue().equals(""))
//			{
//				this.showDialogMessage("Comment cannot be empty!");
//			}
//			
//			boolean checkTermDays=this.validatePaymentTrmDays(ibdays.getValue());
//			if(checkTermDays==false)
//			{
//				if(chk==0){
//					showDialogMessage("Days already defined.Please enter unique days!");
//				}
////				clearPaymentTermsFields();
//				flag=false;
//			}
//			
//			if(ibdays.getValue()!=null&&ibdays.getValue()>=0){
//				payTrmDays=ibdays.getValue();
//			}
//			else{
//				showDialogMessage("Days cannot be empty!");
//				flag=false;
//			}
//			
//			
//			if(dopercent.getValue()!=null&&dopercent.getValue()>=0)
//			{
//				if(dopercent.getValue()>100){
//					showDialogMessage("Percent cannot be greater than 100");
//					dopercent.setValue(null);
//					flag=false;
//				}
//				else if(dopercent.getValue()==0){
//					showDialogMessage("Percent should be greater than 0!");
//					dopercent.setValue(null);
//				}
//				else{
//				payTrmPercent=dopercent.getValue();
//				}
//			}
//			else{
//				showDialogMessage("Percent cannot be empty!");
//			}
//			
//			if(tbcomment.getValue()!=null){
//				payTrmComment=tbcomment.getValue();
//			}
//			
//			System.out.println("Payment"+paymentTermsTable.getDataprovider().getList().size());
//			if(paymentTermsTable.getDataprovider().getList().size()>11)
//			{
//				showDialogMessage("Total number of payment terms allowed are 12");
//				payTermsFlag=false;
//			}
//			
//			if(!payTrmComment.equals("")&&payTrmPercent!=0&&flag==true&&payTermsFlag==true)
//			{
//				PaymentTerms payTerms=new PaymentTerms();
//				payTerms.setPayTermDays(payTrmDays);
//				payTerms.setPayTermPercent(payTrmPercent);
//				payTerms.setPayTermComment(payTrmComment);
//				paymentTermsTable.getDataprovider().getList().add(payTerms);
//				clearPaymentTermsFields();
//			}
//		}
		
		public boolean checkProducts(String pcode)
		{
			List<SalesLineItem> salesitemlis=salesorderlineitemtable.getDataprovider().getList();
			for(int i=0;i<salesitemlis.size();i++)
			{
				if(salesitemlis.get(i).getProductCode().equals(pcode)||salesitemlis.get(i).getProductCode()==pcode){
					return true;
				}
			}
			return false;
		}
		
		/*****************************************Add Product Taxes Method***********************************/
		
		/**
		 * This method is called when a product is added to SalesLineItem table.
		 * Taxes related to corresponding products will be added in ProductTaxesTable.
		 * @throws Exception
		 */
		public void addProdTaxes() throws Exception
		{
			List<SalesLineItem> salesLineItemLis=this.salesorderlineitemtable.getDataprovider().getList();
			List<ProductOtherCharges> taxList=this.prodTaxTable.getDataprovider().getList();
			NumberFormat nf=NumberFormat.getFormat("0.00");
			String cformval="";
			int cformindex=this.getCbcformlis().getSelectedIndex();
			if(cformindex!=0){
				cformval=this.getCbcformlis().getItemText(cformindex);
			}
			for(int i=0;i<salesLineItemLis.size();i++)
			{
				double priceqty=0,taxPrice=0,origPrice=0;
//				if(salesLineItemLis.get(i).getPercentageDiscount()==null){
//					taxPrice=this.getSalesorderlineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//					priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice)*salesLineItemLis.get(i).getQty();
//					priceqty=Double.parseDouble(nf.format(priceqty));
//				}
//				if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
//					taxPrice=this.getSalesorderlineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//					priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
//					priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
//					priceqty=priceqty*salesLineItemLis.get(i).getQty();
//					priceqty=Double.parseDouble(nf.format(priceqty));
//				}
				
				taxPrice=this.getSalesorderlineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
				
				origPrice=(salesLineItemLis.get(i).getPrice()-taxPrice);
				if((salesLineItemLis.get(i).getPercentageDiscount()==null && salesLineItemLis.get(i).getPercentageDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
						
						System.out.println("inside both 0 condition");
//						priceqty=origPrice*salesLineItemLis.get(i).getQty();
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=origPrice*squareArea;
							priceqty=Double.parseDouble(nf.format(priceqty));
							System.out.println("RRRRRRRRRRRRRR price == "+priceqty);
						}else{
							priceqty=origPrice*salesLineItemLis.get(i).getQty();

						}
					}
					
					else if((salesLineItemLis.get(i).getPercentageDiscount()!=null)&& (salesLineItemLis.get(i).getDiscountAmt()!=0)){
						
						System.out.println("inside both not null condition");
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=origPrice*squareArea;
							priceqty= priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							priceqty=Double.parseDouble(nf.format(priceqty));
						}else{
							priceqty=origPrice*salesLineItemLis.get(i).getQty();
							priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
						}
						
					}
					else 
					{
						System.out.println("inside oneof the null condition");
						
							if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
								System.out.println("inside getPercentageDiscount oneof the null condition");
								/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
								if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
									Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
									priceqty = origPrice*squareArea;
									priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
								}else{
									priceqty=origPrice*salesLineItemLis.get(i).getQty();
									priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
								}
							}
							else 
							{
								System.out.println("inside getDiscountAmt oneof the null condition");
								/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
								if(!salesLineItemLis.get(i).getArea().equals("NA")){
									Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
									priceqty = origPrice*squareArea;
									priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
								}else{
									priceqty=origPrice*salesLineItemLis.get(i).getQty();
									priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
								}
							}
							
//							total=total*object.getQty();
							
					}
				
				
				System.out.println("Total Prodcut"+priceqty);
				
				System.out.println("Total Prodcut"+priceqty);
				
				/**
				 * Date 20 july 2017 below code commented by vijay 
				 * for this below code by default showing vat and service tax percent 0 no nedd this if
				 * product master does not have any taxes
				 */
				
//				if(salesLineItemLis.get(i).getVatTax().getPercentage()==0&&cbcformlis.getSelectedIndex()==0){
//					ProductOtherCharges pocentity=new ProductOtherCharges();
//					if(validateState()){
//						pocentity.setChargeName(AppConstants.VAT);
//					}
//					if(!validateState()){
//						pocentity.setChargeName(AppConstants.CENTRALTAX);
//					}
//					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
//					pocentity.setIndexCheck(i+1);
//					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),AppConstants.VAT);
//					if(indexValue!=-1){
////						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(0);
//						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//					}
//					
//					if(indexValue==-1){
//						pocentity.setAssessableAmount(0);
////						pocentity.setAssessableAmount(priceqty);
//					}
//					this.prodTaxTable.getDataprovider().getList().add(pocentity);
//				}
//				
//				if(salesLineItemLis.get(i).getServiceTax().getPercentage()==0){
////					if(validateState()){
//					ProductOtherCharges pocentity=new ProductOtherCharges();
//					pocentity.setChargeName(AppConstants.SERVICETAX);
//					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
//					pocentity.setIndexCheck(i+1);
//					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
//					if(indexValue!=-1){
////						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(0);
//						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//					}
//					
//					if(indexValue==-1){
//						pocentity.setAssessableAmount(0);
////						pocentity.setAssessableAmount(priceqty);
//					}
//					this.prodTaxTable.getDataprovider().getList().add(pocentity);
////					}
//					
////					if(!validateState()&&getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.NO)){
////						ProductOtherCharges pocentity=new ProductOtherCharges();
////						pocentity.setChargeName("Service Tax");
////						pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
////						pocentity.setIndexCheck(i+1);
////						pocentity.setAssessableAmount(priceqty);
////						this.prodTaxTable.getDataprovider().getList().add(pocentity);
////					}
//					
//					
//				}
			/**
			 * Adding Vat Tax to table if the company state and delivery state is equal
			 */
				
			if(validateState()){
				if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
					
					/**
					 * Date 12 july 2017 below if condition added by vijay for GST old code added in else block
					 */
					if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals("")){
						
						System.out.println("GST VAT");
						ProductOtherCharges pocentity=new ProductOtherCharges();
						
						//Below old code commented by vijay
//						pocentity.setChargeName("VAT");
						// below code added by vijay for GST
						pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
						pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
						pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getVatTax().getTaxPrintName());
						
						if(indexValue!=-1){
							System.out.println("Index As Not -1");
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							System.out.println("VAt");
							pocentity.setAssessableAmount(priceqty);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
						
					}else{
						
						System.out.println("Vattx");
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName("VAT");
						pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
						pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
						
						if(indexValue!=-1){
							System.out.println("Index As Not -1");
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							System.out.println("VAt");
							pocentity.setAssessableAmount(priceqty);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
						
					
				}
			}
				
			/**
			 * Adding Vat Tax as CST if company state and delivery state is not equal and C form is submitted.
			 */
			
			/**
			 * Date 12 july 2017 added by vijay for GST
			 * below code is applicable only if Sales order date is less than 1 july 2017 for Cform
			 * below if condition added by vijay
			 */
			if(this.dbsalesorderdate.getValue()!=null && this.dbsalesorderdate.getValue().before(date)){
				
				if(this.getCbcformlis().getSelectedIndex()!=0&&AppConstants.YES.equals(cformval.trim())){
//					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
						System.out.println("Vattx");
						ProductOtherCharges pocentity=new ProductOtherCharges();
						TaxDetails cstPerc=olbcstpercent.getSelectedItem();
						pocentity.setChargeName("CST");
						pocentity.setChargePercent(cstPerc.getTaxChargePercent());
						pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(cstPerc.getTaxChargePercent(),"CST");
						
						if(indexValue!=-1){
							System.out.println("Index As Not -1");
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							pocentity.setAssessableAmount(priceqty);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
//					}
				}
			}
				
				/**
				 * Adding Vat Tax as CST if company state and delivery state is not equal and C form is not submitted.
				 */
				
			/**
			 * Date 12 july 2017 added by vijay for GST
			 * below code is applicable only if Sales order date is less than 1 july 2017 for Cform
			 * below if condition added by vijay
			 */
			if(this.dbsalesorderdate.getValue()!=null && this.dbsalesorderdate.getValue().before(date)){
				
				if(this.getCbcformlis().getSelectedIndex()!=0&&AppConstants.NO.equals(cformval.trim())){
					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
						System.out.println("Vattx");
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName("CST");
						pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
						pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
						
						if(indexValue!=-1){
							System.out.println("vsctIndex As Not -1");
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							System.out.println("vcst-1");
							pocentity.setAssessableAmount(priceqty);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}
				
			}
				/**
				 * Adding service tax to the table
				 */
				
				if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
					System.out.println("hi vijay ");
					System.out.println("ServiceTx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					
					/**
					 * Date 11 july 2017 if condition added by vijay for GST and old code added in else block 
					 */
					if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
					{
						pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					}else{
						pocentity.setChargeName("Service Tax");
					}
					/**
					 * ends here
					 */
					
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
//					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
					
					/**
					 * Date 12 july 2017 if condition for old code and else block for GST added by vijay
					 */
					int indexValue;
					if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
					{
						indexValue =this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					}else{
						indexValue =this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");

					}
					/**
					 * ends here
					 */
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						
						/**
						 * If validate state method is true
						 */
						
						if(validateState()){
							double assessValue=0;
							if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
								
								/**
								 * Date 12 july 2017 if condition added by vijay for GST and old code added in else block 
								 */
								if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals("")){
									System.out.println("For GST");
									assessValue=priceqty;
									pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
									System.out.println("price =="+assessValue);
									System.out.println(assessValue+taxList.get(indexValue).getAssessableAmount());
								 }else{
									 System.out.println("old code ");
									 	assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
										pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
								 }
								this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
							}
							if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
								pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
								this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
							}
						}
						
						/** Date 12 july 2017 added by vijay for GST
						 * below code is applicable only if Sales order date is less than 1 july 2017 for Cform
						 * below if condition added by vijay
						 */
						if(this.dbsalesorderdate.getValue()!=null && this.dbsalesorderdate.getValue().before(date)){
							
						/**
						 * If validate state method is true and cform is submittted.
						 */
						
						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
							double assessValue=0;
							if(this.getOlbcstpercent().getSelectedIndex()!=0){
								TaxDetails cstperc1=olbcstpercent.getSelectedItem();
								assessValue=priceqty+(priceqty*cstperc1.getTaxChargePercent()/100);
								pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
								this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
							}
						}
						
						/**
						 * If validate state method is true and cform is not submitted.
						 */
						
						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.NO)){
							double assessValue=0;
							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						
					  }
					/**
					 * ends here
					 */
					}
					if(indexValue==-1){
						System.out.println("Index As -1");
						
						if(validateState()){
							double assessVal=0;
							if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
								
								/**
								 * Date 12 july 2017 if condition added by vijay for GST and old code added into else block
								 */
								if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals("")){
									System.out.println("For GST");
									assessVal=priceqty;
									pocentity.setAssessableAmount(assessVal);
									
								}
								else{
									assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
									pocentity.setAssessableAmount(assessVal);
								}
								
							}
							if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
								pocentity.setAssessableAmount(priceqty);
							}
						}
						
						/** Date 12 july 2017 added by vijay for GST
						 * below code is applicable only if Sales order date is less than 1 july 2017 for Cform
						 * below if condition added by vijay
						 */
						if(this.dbsalesorderdate.getValue()!=null && this.dbsalesorderdate.getValue().before(date)){
							
						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.NO)){
							double assessVal=0;
							if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
								assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
								pocentity.setAssessableAmount(assessVal);
							}
							if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
								pocentity.setAssessableAmount(priceqty);
							}
						}
						
						if(!validateState()&&this.getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).equals(AppConstants.YES)){
							double assessVal=0;
							if(this.getOlbcstpercent().getSelectedIndex()!=0){
								TaxDetails cstperc2=olbcstpercent.getSelectedItem();
								assessVal=priceqty+(priceqty*cstperc2.getTaxChargePercent()/100);
								pocentity.setAssessableAmount(assessVal);
							}
						}
					   }
						/**
						 * ends here
						 */
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
		}
		
		/**
		 * Date : 15-09-2017 BY ANIL
		 * Adding calculation part for other charges
		 */
		public void addOtherChargesInTaxTbl(){
			List<ProductOtherCharges> taxList=this.prodTaxTable.getDataprovider().getList();
			for(OtherCharges otherCharges:tblOtherCharges.getDataprovider().getList()){
				/**
				 * If GST tax is not applicable then we will consider tax1 as vat tax field and tax2 as service tax field.
				 */
				if(otherCharges.getTax1().getTaxPrintName()!=null&&!otherCharges.getTax1().getTaxPrintName().equals("")){
					boolean updateTaxFlag=true;
					if(otherCharges.getTax1().getTaxPrintName().equalsIgnoreCase("SELECT")){
						updateTaxFlag=false;
//						return;
					}
					if(otherCharges.getTax1().getPercentage()==0){
						updateTaxFlag=false;
//						return;
					}
					if(updateTaxFlag){
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName(otherCharges.getTax1().getTaxPrintName());
						pocentity.setChargePercent(otherCharges.getTax1().getPercentage());
	//					pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(otherCharges.getTax1().getPercentage(),otherCharges.getTax1().getTaxPrintName());
						if(indexValue!=-1){
							pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							pocentity.setAssessableAmount(otherCharges.getAmount());
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}else{
					boolean updateTaxFlag=true;
					if(otherCharges.getTax1().getPercentage()==0){
						updateTaxFlag=false;
//						return;
					}
					if(updateTaxFlag){
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName("VAT");
						pocentity.setChargePercent(otherCharges.getTax1().getPercentage());
	//					pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(otherCharges.getTax1().getPercentage(),"VAT");
						if(indexValue!=-1){
							pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							pocentity.setAssessableAmount(otherCharges.getAmount());
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}
				
				if(otherCharges.getTax2().getTaxPrintName()!=null&&!otherCharges.getTax2().getTaxPrintName().equals("")){
					System.out.println("INSIDE GST TAX PRINT NAME : "+otherCharges.getTax2().getTaxPrintName());
					boolean updateTaxFlag=true;
					if(otherCharges.getTax2().getTaxPrintName().equalsIgnoreCase("SELECT")){
						updateTaxFlag=false;
//						return;
					}
					if(otherCharges.getTax2().getPercentage()==0){
						updateTaxFlag=false;
//						return;
					}
					if(updateTaxFlag){
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName(otherCharges.getTax2().getTaxPrintName());
						pocentity.setChargePercent(otherCharges.getTax2().getPercentage());
	//					pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(otherCharges.getTax2().getPercentage(),otherCharges.getTax2().getTaxPrintName());
						if(indexValue!=-1){
							pocentity.setAssessableAmount(otherCharges.getAmount()+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							pocentity.setAssessableAmount(otherCharges.getAmount());
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}else{
					System.out.println("ST OR NON GST");
					boolean updateTaxFlag=true;
					if(otherCharges.getTax2().getPercentage()==0){
						updateTaxFlag=false;
//						return;
					}
					if(otherCharges.getTax1().getTaxPrintName()!=null&&!otherCharges.getTax1().getTaxPrintName().equals("")){
						updateTaxFlag=false;
//						return;
					}
					if(updateTaxFlag){
						ProductOtherCharges pocentity=new ProductOtherCharges();
						pocentity.setChargeName("Service Tax");
						pocentity.setChargePercent(otherCharges.getTax2().getPercentage());
	//					pocentity.setIndexCheck(i+1);
						int indexValue=this.checkTaxPercent(otherCharges.getTax2().getPercentage(),"Service");
						if(indexValue!=-1){
						 	double assessValue=otherCharges.getAmount()+(otherCharges.getAmount()*otherCharges.getTax1().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(indexValue==-1){
							double assessValue=otherCharges.getAmount()+(otherCharges.getAmount()*otherCharges.getTax1().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue);
						}
						this.prodTaxTable.getDataprovider().getList().add(pocentity);
					}
				}
			}
		}

		/*********************************Check Tax Percent*****************************************/
		/**
		 * This method returns 
		 * @param taxValue
		 * @param taxName
		 * @return
		 */
		
		public int checkTaxPercent(double taxValue,String taxName)
		{
			System.out.println("Tav Value"+taxValue);
			List<ProductOtherCharges> taxesList=this.prodTaxTable.getDataprovider().getList();
			
			for(int i=0;i<taxesList.size();i++)
			{
				double listval=taxesList.get(i).getChargePercent();
				int taxval=(int)listval;
				
				if(taxName.equals("Service")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
						System.out.println("zero");
						return i;
					}
				}
				if(taxName.equals("VAT")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
						System.out.println("zero");
						return i;
					}
				}
				if(taxName.equals("CST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxesList.get(i).getChargeName().trim().equals("CST")){
						return i;
					}
				}
			//  vijay added this for GST on 12 july 2017
				if(taxName.equals("SGST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
						return i;
					}
				}
				
				//  vijay added this for GST on 12 july 2017
				if(taxName.equals("CGST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
						return i;
					}
				}
				
				//  vijay added this for GST on 12 july 2017
				if(taxName.equals("IGST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
						return i;
					}
				}
			}
			return -1;
		}
		
		
		
		/*************************Update Charges Table****************************************/
		/**
		 * This method updates the charges table.
		 * The table is updated when any row value is changed through field updater in table i.e
		 * row change event is fired
		 */
		
		public void updateChargesTable()
		{
			List<ProductOtherCharges> prodOtrLis=this.chargesTable.getDataprovider().getList();
			ArrayList<ProductOtherCharges> prodOtrArr=new ArrayList<ProductOtherCharges>();
			prodOtrArr.addAll(prodOtrLis);
			
			this.chargesTable.connectToLocal();
			System.out.println("prodo"+prodOtrArr.size());
			for(int i=0;i<prodOtrArr.size();i++)
			{
				ProductOtherCharges prodChargesEnt=new ProductOtherCharges();
				prodChargesEnt.setChargeName(prodOtrArr.get(i).getChargeName());
				prodChargesEnt.setChargePercent(prodOtrArr.get(i).getChargePercent());
				prodChargesEnt.setChargeAbsValue(prodOtrArr.get(i).getChargeAbsValue());
				//prodChargesEnt.setAssessableAmount(prodOtrArr.get(i).getAssessableAmount());
				prodChargesEnt.setFlagVal(prodOtrArr.get(i).getFlagVal());
				prodChargesEnt.setIndexCheck(prodOtrArr.get(i).getIndexCheck());
				if(prodOtrArr.get(i).getFlagVal()==false){
					prodChargesEnt.setAssessableAmount(this.getDoamtincltax().getValue());
				}
				if(prodOtrArr.get(i).getFlagVal()==true){
					double assesableVal=this.retrieveSurchargeAssessable(prodOtrArr.get(i).getIndexCheck());
					prodChargesEnt.setAssessableAmount(assesableVal);
				}
				
				this.chargesTable.getDataprovider().getList().add(prodChargesEnt);
			}
		}
		
		public double retrieveSurchargeAssessable(int indexValue)
		{
			List<ProductOtherCharges> otrChrgLis=this.chargesTable.getDataprovider().getList();
			ArrayList<ProductOtherCharges> otrChrgArr=new ArrayList<ProductOtherCharges>();
			otrChrgArr.addAll(otrChrgLis);
		
			double getAssessVal=0;
			for(int i=0;i<otrChrgArr.size();i++)
			{
				if(otrChrgArr.get(i).getFlagVal()==false&&otrChrgArr.get(i).getIndexCheck()==indexValue)
				{
					if(otrChrgArr.get(i).getChargePercent()!=0){
						getAssessVal=otrChrgArr.get(i).getAssessableAmount()*otrChrgArr.get(i).getChargePercent()/100;
					}
					if(otrChrgArr.get(i).getChargeAbsValue()!=0){
						getAssessVal=otrChrgArr.get(i).getChargeAbsValue();
					}
				}
			}
			return getAssessVal;
		}

		/************************************Overridden Methods*******************************************/


		@Override
		public void setEnable(boolean state) {
			super.setEnable(state);
			getstatustextbox().setEnabled(false);
			this.tbContractId.setEnabled(false);
			this.tbLeadId.setEnabled(false);
			this.tbQuotatinId.setEnabled(false);
			this.salesorderlineitemtable.setEnable(state);
//			this.paymentTermsTable.setEnable(state);
			this.chargesTable.setEnable(state);
			this.prodTaxTable.setEnable(state);
			cbcheckaddress.setValue(false);
			this.doamtincltax.setEnabled(false);
			this.donetpayamt.setEnabled(false);
			this.dototalamt.setEnabled(false);
			this.olbcstpercent.setEnabled(false);
			this.cbcformlis.setEnabled(false);
			this.acshippingcomposite.setEnable(false);
			tbremark.setEnabled(false);
			dbbalancepayment.setEnabled(false);
			
			/**
			 * Date : 20-09-2017 BY ANIL
			 */
			this.tblOtherCharges.setEnable(state);
			dbOtherChargesTotal.setEnabled(false);
			/**
			 * end
			 */
			if(tbContractId.getValue()!=null&&!tbContractId.getValue().equals("")){
				this.olbcNumberRange.setEnabled(false);
			}
			
			 /*
	  		 * Ashwini Patil
	  		 * Date:14-06-2024
	  		 * Ultima search want to map number range directly from branch so they want this field read only
	  		 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
	  		{	olbcNumberRange.setEnabled(false);
	  		}
		}

		@Override
		public void setToViewState() {
		
			super.setToViewState();
			setMenuAsPerStatus();
		
			SuperModel model=new SalesOrder();
			model=salesorderobj;
			
			System.out.println("Hi vijay view State"+customerEditFlag);
			if(customerEditFlag==true){
				QuickSalesOrderPresenter.initalize();
			}
			
			
			AppUtility.addDocumentToHistoryTable(AppConstants.SALESMODULE,AppConstants.QUICKSALESORDER, salesorderobj.getCount(), salesorderobj.getCinfo().getCount(),salesorderobj.getCinfo().getFullName(),salesorderobj.getCinfo().getCellNumber(), false, model, null);
			
			String mainScreenLabel="Sales Order";
			if(salesorderobj!=null&&salesorderobj.getCount()!=0){
				mainScreenLabel=salesorderobj.getCount()+" "+"/"+" "+salesorderobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(salesorderobj.getCreationDate());
			}
				fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		}
		@Override
		public void setToEditState() {
		
			super.setToEditState();
			this.processLevelBar.setVisibleFalse(false);
			
			if(olbcstpercent.getValue()!=null){
				olbcstpercent.setEnabled(true);
			}
			if(cbcformlis.getSelectedIndex()!=0){
				cbcformlis.setEnabled(true);
			}
			
			if(!tbQuotatinId.getValue().equals(""))
			{
				baddproducts.setEnabled(false);
				addOtherCharges.setEnabled(false);
			}
			this.olbcNumberRange.setEnabled(false);
			changeStatusToCreated();
			
			String mainScreenLabel="Sales Order";
			if(salesorderobj!=null&&salesorderobj.getCount()!=0){
				mainScreenLabel=salesorderobj.getCount()+" "+"/"+" "+salesorderobj.getStatus()+" "+"/"+" "+AppUtility.parseDate(salesorderobj.getCreationDate());
			}
				fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		}
		
		public void changeStatusToCreated()
		{
			SalesOrder entity=(SalesOrder) presenter.getModel();
			String status=entity.getStatus();
			
			if(SalesOrder.REJECTED.equals(status.trim()))
			{
				this.tbQuotationStatus.setValue(SalesOrder.CREATED);
			}
		}
		
		@Override
		public void clear() {
			super.clear();
			getstatustextbox().setText(SalesOrder.CREATED);
		}
		
		
		
		/****************************************Validation Method(Overridden)**********************************/
		/**
		 * Validates form fields
		 */
		
		@Override
		 public boolean validate()
		 {
			 boolean superValidate = super.validate();
			 if(superValidate==false){
	        	 return false;
	        }
	       
			if(salesorderlineitemtable.getDataprovider().getList().size()==0)
	        {
	            showDialogMessage("Please Select at least one Product!");
	            return false;
	        }
	        
//	        boolean valPayPercent=this.validatePaymentTermPercent();
//	        if(valPayPercent==false)
//	        {
//	        	showDialogMessage("Payment Term Not 100%, Please Correct!");
//	        	return false;
//	        }
	        
			//Ashwini Patil Date: 11-03-2022 
//	        if(this.getAcshippingcomposite().getState().getSelectedIndex()!=0){
//	        	String companyStateVal=QuickSalesOrderPresenter.companyStateVal.trim();
//	        	System.out.println("comp"+companyStateVal);
//	        	Console.log(companyStateVal);
//	        	if(!getAcshippingcomposite().getState().getValue().trim().equals(companyStateVal)&&getCbcformlis().getSelectedIndex()==0){
//	        		showDialogMessage("C Form is Mandatory!");
//	        		return false;
//	        	}
//	        }
	        
	        /**
			 * Date 4 august 2017 if condition added by vijay for quick sales no need this if needed then process config
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "WarehouseMondatory")){
				if(validateWarehouse()==false){
					showDialogMessage("Please Select Warehouse Details.");
					return false;
				}
			}
			
	        if(getCbcformlis().getItemText(getCbcformlis().getSelectedIndex()).trim().equals(AppConstants.YES)&&getOlbcstpercent().getSelectedIndex()==0){
	        	showDialogMessage("Please select CST Percent!");
	        	return false;
	        }
	        
	        
	        if(validateSalesOrderPrice()>0){
	        	showDialogMessage("Please enter product price greater than 0!");
	        	return false;
	        }
	        
	        
	        if(!validateSalesOrderProdQty()){
				showDialogMessage("Please enter appropriate quantity required!");
				return false;
			}
	        
	        /**
			 * Date 03-07-2017 added by vijay tax validation
			 */
			List<ProductOtherCharges> prodtaxlist = this.prodTaxTable.getDataprovider().getList();
			for(int i=0;i<prodtaxlist.size();i++){
				
				if(dbsalesorderdate.getValue()!=null && dbsalesorderdate.getValue().before(date)){
					if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CGST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("SGST")
							|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("IGST")){
						showDialogMessage("As sales order date "+fmt.format(dbsalesorderdate.getValue())+" GST Tax is not applicable");
						return false;
					}
				}
				else if(dbsalesorderdate.getValue()!=null && dbsalesorderdate.getValue().equals(date) || dbsalesorderdate.getValue().after(date)){
					if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("Service Tax")
							|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("VAT")){
						showDialogMessage("As sales order date "+fmt.format(dbsalesorderdate.getValue())+" VAT/CST/Service Tax is not applicable");
						return false;
					}
				}
			}
			/**
			 * ends here
			 */
			
			
			/**
			  * Date 26-07-2018 By Vijay
			  * Des:- GSTIN Number can not be greater than 15 characters Validation
			  */
			if(tbGSTNumber.getValue().trim()!=null && !tbGSTNumber.getValue().trim().equals("")){
				if(tbGSTNumber.getValue().trim().length()>15 || tbGSTNumber.getValue().trim().length()<15){
					showDialogMessage("GSTIN number must be 15 characters!");
					return false;
				}
			}
			/**
			 * ends here
			 */
	        
			/***
			 * Date 07-08-2018 By Vijay
			 * Des :- if warehouse selected then qty must not greater than available qty validation
			 */
			List<SalesLineItem> list = this.salesorderlineitemtable.getDataprovider().getList();
			for(int i=0;i<list.size();i++){
				if( list.get(i).getItemProductWarehouseName()!=null && !list.get(i).getItemProductWarehouseName().equals("") && list.get(i).getQty()>list.get(i).getItemProductAvailableQty()){
					showDialogMessage("Qty should not be greater than available Quantity ");
					return false;
				}
			}
			/**
			 * ends here
			 */
			
	        return true;
		 }

		/********************************Clear Payment Terms Fields*******************************************/
		
		
		/**
		 * Date 4-07-2017 added by vijay for warehouse validation
		 */
		
		private boolean validateWarehouse() {

			
			List<SalesLineItem> list = this.salesorderlineitemtable.getDataprovider().getList();
			
			for(int i=0;i<list.size();i++){
				if(list.get(i).getItemProductWarehouseName()!=null && !list.get(i).getItemProductWarehouseName().equals("") && 
				   list.get(i).getItemProductStrorageLocation()!=null && !list.get(i).getItemProductStrorageLocation().equals("") &&
						   list.get(i).getItemProductStorageBin()!=null && !list.get(i).getItemProductStorageBin().equals("")){
					return false;
				}
			}
			
			return true;
		}
		
		
		
		public boolean validateSalesOrderProdQty()
		{
			List<SalesLineItem> orderlis=salesorderlineitemtable.getDataprovider().getList();
			int ctr=0;
			for(int i=0;i<orderlis.size();i++)
			{
				if(orderlis.get(i).getQty()==0)
				{
					ctr=ctr+1;
				}
			}
			
			if(ctr==0){
				return true;
			}
			else{
				return false;
			}
		}
		
		
//		public void clearPaymentTermsFields(){
//			ibdays.setValue(null);
//			dopercent.setValue(null);
//			tbcomment.setValue("");
//		}
		
		/*******************************Validate Payment Term Percent*****************************************/
		/**
		 * This method validates payment term percent. i.e the total of percent should be equal to 100
		 * @return
		 */
		
//		public boolean validatePaymentTermPercent()
//		{
//			List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
//			double totalPayPercent=0;
//			for(int i=0;i<payTermsLis.size();i++)
//			{
//				totalPayPercent=totalPayPercent+payTermsLis.get(i).getPayTermPercent();
//			}
//			System.out.println("Validation of Payment Percent as 100"+totalPayPercent);
//			
//			if(totalPayPercent!=100)
//			{
//				return false;
//			}
//			
//			return true;
//		}
		
		/********************************Validate Payment Terms Days******************************************/
		/**
		 * This method represents the validation of payment terms days
		 * @param retrDays
		 * @return
		 */
		
		int chk=0;
//		public boolean validatePaymentTrmDays(int retrDays)
//		{
//			List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
//			for(int i=0;i<payTermsLis.size();i++)
//			{
//				if(retrDays==payTermsLis.get(i).getPayTermDays())
//				{
//					return false;
//				}
//				if(retrDays<payTermsLis.get(i).getPayTermDays())
//				{
//					showDialogMessage("Days should be defined in increasing order!");
//					chk=1;
//					return false;
//				}
//				else{
//					chk=0;
//				}
//				if(retrDays>10000){
//					showDialogMessage("Days cannot be greater than 4 digits!");
//					return false;
//				}
//			}
//			return true;
//		}
		
		/**
		 * 
		 * @param pcode
		 * @return
		 */
		
		public boolean validateProductDuplicate(String pcode)
		{
			List<SalesLineItem> salesItemLis=this.getSalesorderlineitemtable().getDataprovider().getList();
			for(int i=0;i<salesItemLis.size();i++)
			{
				System.out.println("salesl"+salesItemLis.get(i).getProductCode());
				if(salesItemLis.get(i).getProductCode().trim().equals(pcode)){
					System.out.println("In");
					return true;
				}
			}
			System.out.println("Out");
			return false;
		}
		
		public int validateSalesOrderPrice()
		{
			List<SalesLineItem> salesItemLis=this.getSalesorderlineitemtable().getDataprovider().getList();
			int ctr=0;
			for(int i=0;i<salesItemLis.size();i++)
			{
				if(salesItemLis.get(i).getPrice()==0){
					ctr=ctr+1;
				}
			}
			
			return ctr;
		}
		
		public void ProdctType(String pcode,String productId)
		{
			final GenricServiceAsync genasync=GWT.create(GenricService.class);
			
			int prodId=Integer.parseInt(productId);
			
			Vector<Filter> filtervec=new Vector<Filter>();
			final MyQuerry querry=new MyQuerry();
			
			Filter filter=new Filter();
			filter.setQuerryString("productCode");
			filter.setStringValue(pcode.trim());
			filtervec.add(filter);
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(prodId);
			filtervec.add(filter);
			
			filter=new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(true);
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new SuperProduct());
			showWaitSymbol();
			 Timer timer=new Timer() 
        	 {
 				@Override
 				public void run() {
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
						showDialogMessage("An Unexpected error occurred!");
					}
		
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						if(result.size()==0)
						{
							showDialogMessage("Please check whether product status is active or not!");
						}
						
						Branch branchEntity = olbbBranch.getSelectedItem();
						boolean gstApplicable = false;
						if(olbcNumberRange.getSelectedIndex()!=0){
							Config configEntity = olbcNumberRange.getSelectedItem();
							if(configEntity!=null)
							gstApplicable = configEntity.isGstApplicable();
						}
						else{
							gstApplicable = true;
						}
						
						for(SuperModel model:result)
						{
							SuperProduct superProdEntity = (SuperProduct)model;
//							final SalesLineItem lis=AppUtility.ReactOnAddProductCompositeForSales(superProdEntity);
							final SalesLineItem lis=AppUtility.ReactOnAddProductCompositeForSales(superProdEntity,cust,branchEntity,gstApplicable);

							/***************** vijay********************/
							Date warrantyuntildate = getWarrantyEndDate(dbsalesorderdate.getValue(),lis.getWarrantyInMonth());
							lis.setWarrantyUntil(warrantyuntildate);
							//Date 21 july 2017 added by vijay for valid until calculation
							if(dbsalesorderdate.getValue()!=null)
							salesorderDate = dbsalesorderdate.getValue();
                            //for hvac {
							//showDialogMessage("category :" + lis.getPrduct().getProductCategory());
							cList.clear();
							if(lis.getPrduct().getProductCategory().equalsIgnoreCase("ASSET")){
							final ArrayList<CompanyAsset> list = new ArrayList<CompanyAsset>();
							MyQuerry querry = new MyQuerry();
						  	Company c = new Company();
						  	Vector<Filter> filtervec=new Vector<Filter>();
						  	Filter filter = null;
						  	filter = new Filter();
						  	filter.setQuerryString("companyId");
							filter.setLongValue(c.getCompanyId());
							filtervec.add(filter);
							filter = new Filter();
							filter.setQuerryString("status");
							filter.setBooleanvalue(true);
							filtervec.add(filter);
							filter = new Filter();
							filter.setQuerryString("amcStatus");
							filter.setStringValue("Allocated");
							filtervec.add(filter);
							filter = new Filter();
						  	filter.setQuerryString("productId");
							filter.setIntValue(lis.getPrduct().getCount());
							filtervec.add(filter);
							if(getOlbeSalesPerson().getSelectedIndex()!=0){
							filter = new Filter();
						  	filter.setQuerryString("referenceNumber");
							filter.setStringValue(getOlbeSalesPerson().getValue());
							filtervec.add(filter);
							}
							List<String> branchList = new ArrayList<String>();
							for(Branch b : LoginPresenter.globalBranch){
								branchList.add(b.getBusinessUnitName());
							}
							filter = new Filter();
							filter.setQuerryString("branch IN");
							filter.setList(branchList);
							filtervec.add(filter);
							querry.setFilters(filtervec);
							querry.setQuerryObject(new CompanyAsset());
							
							genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
								
								@Override
								public void onFailure(Throwable caught) {
								}
								@Override
								public void onSuccess(ArrayList<SuperModel> result) {
									System.out.println("RS"+result.size());
									cList.clear();
									//ArrayList<String> cList = new ArrayList<String>();
									for(SuperModel superModel  : result){
										CompanyAsset c =  (CompanyAsset)superModel;
										cList.add(c.getName());
									}
										//lis.setCompanyAsset(cList);

								}
										
							});		
							}
							
							/** Date 03-05-2018 By vijay for Product SrNo and allow to add same product **/
							lis.setProductSrNo(productSrNo+1);
							
							getSalesorderlineitemtable().getDataprovider().getList().add(lis);

							/** Date 03-05-2018 By vijay for Product SrNo **/
						    productSrNo++;
						}
						}
					 });
					hideWaitSymbol();
	 				}
		 	  };
	          timer.schedule(3000); 
		}
		
		private Date getWarrantyEndDate(Date salesorderDate,int warrantyInMonth) {
			
		    CalendarUtil.addMonthsToDate(salesorderDate, warrantyInMonth);
			return salesorderDate;
		}
		
		public void initializePercentCst()
		{
			Timer timer=new Timer() 
		    {
				@Override
				public void run() 
				{
					MyQuerry querry=new MyQuerry();
					Company comEntity=new Company();
					Vector<Filter> filter=new Vector<Filter>();
					Filter temp=null;
					
					temp=new Filter();
					temp.setQuerryString("companyId");
					temp.setLongValue(comEntity.getCompanyId());
					filter.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("isCentralTax");
					temp.setBooleanvalue(true);
					filter.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("taxChargeStatus");
					temp.setBooleanvalue(true);
					filter.add(temp);
					
					querry.setFilters(filter);
					querry.setQuerryObject(new TaxDetails());
					olbcstpercent.MakeLive(querry);
				}
	    	 };
	    	 timer.schedule(1500);
		}
		
		
		/**********************************Change Widgets Method******************************************/
		/**
		 * This method represents changing the widgets style. Used for modifying payment composite
		 * widget fields
		 */
		
		private void changeWidgets()
		{
			//  For changing the text alignments.
			this.changeTextAlignment(dototalamt);
			this.changeTextAlignment(donetpayamt);
			this.changeTextAlignment(doamtincltax);
			
			//  For changing widget widths.
			this.changeWidth(dototalamt, 200, Unit.PX);
			this.changeWidth(donetpayamt, 200, Unit.PX);
			this.changeWidth(doamtincltax, 200, Unit.PX);
			
			/**
			 * Date : 19-09-2017 By ANIL
			 */
			this.changeTextAlignment(dbOtherChargesTotal);
			this.changeWidth(dbOtherChargesTotal, 200, Unit.PX);
		}
		
		/*******************************Text Alignment Method******************************************/
		/**
		 * Method used for align of payment composite fields
		 * @param widg
		 */
		
		private void changeTextAlignment(Widget widg)
		{
			widg.getElement().getStyle().setTextAlign(TextAlign.CENTER);
		}
		
		/*****************************Change Width Method**************************************/
		/**
		 * Method used for changing width of fields
		 * @param widg
		 * @param size
		 * @param unit
		 */
		
		private void changeWidth(Widget widg,double size,Unit unit)
		{
			widg.getElement().getStyle().setWidth(size, unit);
		}

		protected void checkCustomerStatus(int cCount)
		{
			MyQuerry querry=new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(c.getCompanyId());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("count");
			temp.setIntValue(cCount);
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("An Unexpected error occurred!");
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
						for(SuperModel model:result)
						{
							Customer custEntity = (Customer)model;
							
							if(custEntity.getNewCustomerFlag()==true){
								showDialogMessage("Please fill customer information by editing customer");
								personInfoComposite.clear();
							}
							else if(AppConstants.ACTIVE.equals(custEntity.getStatus().trim())&&(custEntity.getNewCustomerFlag()==false)){
								 getPersonInfoComposite().getId().getElement().addClassName("personactive");
								 getPersonInfoComposite().getName().getElement().addClassName("personactive");
								 getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
								 getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
							}
							
							cust = custEntity;
							
						}
						if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT || AppMemory.getAppMemory().currentState==ScreeenState.NEW){
							updateTaxesDetails(cust);
						}
					}
				 });
			}
		
		/*******************************************Type Drop Down Logic**************************************/
		
		@Override
		public void onChange(ChangeEvent event) {
			if(event.getSource().equals(olbSalesOrderCategory))
			{
				if(olbSalesOrderCategory.getSelectedIndex()!=0){
					ConfigCategory cat=olbSalesOrderCategory.getSelectedItem();
					if(cat!=null){
						AppUtility.makeLiveTypeDropDown(olbSalesOrderType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
					}
				}
			}
			
			String payMethodVal=null;
			if(this.olbcPaymentMethods.getSelectedIndex()!=0)
			{
				payMethodVal=olbcPaymentMethods.getValue(olbcPaymentMethods.getSelectedIndex()).trim();
				if(payMethodVal.equalsIgnoreCase("Cash"))
				{
					clearFields();
					this.tbbankAccNo.setEnabled(false);
					this.tbbankName.setEnabled(false);
					this.tbbankBranch.setEnabled(false);
					this.tbchequeNo.setEnabled(false);
					this.dbChequeDate.setEnabled(false);
					this.tbreferenceno.setEnabled(false);
					this.dbtransferDate.setEnabled(false);
					// Date 02-04-2018 By vijay
					this.tbChequeIssuedBy.setEnabled(false);
				}
				else if(payMethodVal.equalsIgnoreCase("Cheque"))
				{
					clearFields();
					this.tbbankAccNo.setEnabled(true);
					this.tbbankName.setEnabled(true);
					this.tbbankBranch.setEnabled(true);
					this.tbchequeNo.setEnabled(true);
					this.dbChequeDate.setEnabled(true);
					this.tbreferenceno.setEnabled(false);
					this.dbtransferDate.setEnabled(false);
					// Date 02-04-2018 By vijay
					this.tbChequeIssuedBy.setEnabled(true);
				}
				else if(payMethodVal.equalsIgnoreCase("ECS"))
				{
					clearFields();
					this.tbreferenceno.setEnabled(true);
					this.dbtransferDate.setEnabled(true);
					this.tbbankAccNo.setEnabled(true);
					this.tbbankName.setEnabled(true);
					this.tbbankBranch.setEnabled(true);
					this.tbchequeNo.setEnabled(false);
					this.dbChequeDate.setEnabled(false);
					// Date 02-04-2018 By vijay
					this.tbChequeIssuedBy.setEnabled(false);
				}
				else if(payMethodVal.equalsIgnoreCase("NEFT"))
				{
					clearFields();
					this.tbbankAccNo.setEnabled(false);
					this.tbbankName.setEnabled(false);
					this.tbbankBranch.setEnabled(false);
					this.tbchequeNo.setEnabled(false);
					this.dbChequeDate.setEnabled(false);
					this.tbreferenceno.setEnabled(true);
					this.dbtransferDate.setEnabled(true);
					// Date 02-04-2018 By vijay
					this.tbChequeIssuedBy.setEnabled(false);
				}
			}		
			else
			{
				clearFields();
				this.tbbankAccNo.setEnabled(false);
				this.tbbankName.setEnabled(false);
				this.tbbankBranch.setEnabled(false);
				this.tbchequeNo.setEnabled(false);
				this.dbChequeDate.setEnabled(false);
				this.tbreferenceno.setEnabled(false);
				this.dbtransferDate.setEnabled(false);
			}
			
			if(event.getSource().equals(olbcNumberRange)){
				updateTaxesDetails(cust);
			}
			if(event.getSource().equals(olbbBranch)){
				if(olbbBranch.getSelectedIndex()!=0) {
					/*
					 * Ashwini Patil 
					 * Date:13-05-2024 
					 * Added for ultima search. 
					 * If number range is selected on branch then it will automatically get selected on contract screen.
					 */
					
					Branch branchEntity =olbbBranch.getSelectedItem();
					if(branchEntity!=null) {
						Console.log("branch entity not null");
						Console.log("branch "+branchEntity.getBusinessUnitName()+"number range="+branchEntity.getNumberRange());
						if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
							olbcNumberRange.setValue(branchEntity.getNumberRange());
							Console.log("Number range set to drop down by default");
						}else {
							Console.log("in else Number range");
							String range=LoginPresenter.branchWiseNumberRangeMap.get(branchEntity.getBusinessUnitName());
							if(range!=null&&!range.equals("")) {
								olbcNumberRange.setValue(range);
								Console.log("in else Number range set to drop down by default");							
							}else {
								olbcNumberRange.setSelectedIndex(0);
							}
						}
					}else
						Console.log("branch entity null");
				updateTaxesDetails(cust);
				}
			}
		}
		
		
		private void clearFields()
		{
			this.tbbankAccNo.setValue("");
			this.tbbankName.setValue("");
			this.tbbankBranch.setValue("");
			this.tbchequeNo.setValue(null);
			this.dbChequeDate.setValue(null);
			this.dbtransferDate.setValue(null);
			this.tbreferenceno.setValue("");
		}
		
		/***********************************Getters And Setters************************************/
		
		public ObjectListBox<Branch> getOlbbBranch() {
			return olbbBranch;
		}

		public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
			this.olbbBranch = olbbBranch;
		}

		
		public TextBox getTbReferenceNumber() {
			return tbReferenceNumber;
		}

		public void setTbReferenceNumber(TextBox tbReferenceNumber) {
			this.tbReferenceNumber = tbReferenceNumber;
		}

		public ObjectListBox<Config> getOlbcPaymentMethods() {
			return olbcPaymentMethods;
		}

		public void setOlbcPaymentMethods(ObjectListBox<Config> olbcPaymentMethods) {
			this.olbcPaymentMethods = olbcPaymentMethods;
		}

		public TextBox getTbContractId() {
			return tbContractId;
		}

		public void setTbContractId(TextBox tbContractId) {
			this.tbContractId = tbContractId;
		}

		public TextBox getTbRefeRRedBy() {
			return tbRefeRRedBy;
		}

		public void setTbRefeRRedBy(TextBox tbRefeRRedBy) {
			this.tbRefeRRedBy = tbRefeRRedBy;
		}

		public TextBox getTbQuotationStatus() {
			return tbQuotationStatus;
		}

		public void setTbQuotationStatus(TextBox tbQuotationStatus) {
			this.tbQuotationStatus = tbQuotationStatus;
		}

		public TextArea getTaDescription() {
			return taDescription;
		}

		public void setTaDescription(TextArea taDescription) {
			this.taDescription = taDescription;
		}

		public UploadComposite getUcUploadTAndCs() {
			return ucUploadTAndCs;
		}

		public void setUcUploadTAndCs(UploadComposite ucUploadTAndCs) {
			this.ucUploadTAndCs = ucUploadTAndCs;
		}

		public PersonInfoComposite getPersonInfoComposite() {
			return personInfoComposite;
		}

		public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
			this.personInfoComposite = personInfoComposite;
		}

		public ObjectListBox<Employee> getOlbeSalesPerson() {
			return olbeSalesPerson;
		}

		public void setOlbeSalesPerson(ObjectListBox<Employee> olbeSalesPerson) {
			this.olbeSalesPerson = olbeSalesPerson;
		}

		public ObjectListBox<Employee> getOlbApproverName() {
			return olbApproverName;
		}

		public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
			this.olbApproverName = olbApproverName;
		}

		public ObjectListBox<Config> getOlbSalesOrderGroup() {
			return olbSalesOrderGroup;
		}

		public void setOlbSalesOrderGroup(ObjectListBox<Config> olbSalesOrderGroup) {
			this.olbSalesOrderGroup = olbSalesOrderGroup;
		}

		public ObjectListBox<Type> getOlbSalesOrderType() {
			return olbSalesOrderType;
		}

		public void setOlbSalesOrderType(ObjectListBox<Type> olbSalesOrderType) {
			this.olbSalesOrderType = olbSalesOrderType;
		}

		public ObjectListBox<ConfigCategory> getOlbSalesOrderCategory() {
			return olbSalesOrderCategory;
		}

		public void setOlbSalesOrderCategory(
				ObjectListBox<ConfigCategory> olbSalesOrderCategory) {
			this.olbSalesOrderCategory = olbSalesOrderCategory;
		}

		public DoubleBox getDototalamt() {
			return dototalamt;
		}

		public void setDototalamt(DoubleBox dototalamt) {
			this.dototalamt = dototalamt;
		}

		public DoubleBox getDoamtincltax() {
			return doamtincltax;
		}

		public void setDoamtincltax(DoubleBox doamtincltax) {
			this.doamtincltax = doamtincltax;
		}

		public DoubleBox getDonetpayamt() {
			return donetpayamt;
		}

		public void setDonetpayamt(DoubleBox donetpayamt) {
			this.donetpayamt = donetpayamt;
		}

		public TextBox getTbLeadId() {
			return tbLeadId;
		}

		public void setTbLeadId(TextBox tbLeadId) {
			this.tbLeadId = tbLeadId;
		}

		public TextBox getTbQuotatinId() {
			return tbQuotatinId;
		}

		public void setTbQuotatinId(TextBox tbQuotatinId) {
			this.tbQuotatinId = tbQuotatinId;
		}

//		public IntegerBox getIbdays() {
//			return ibdays;
//		}
//
//		public void setIbdays(IntegerBox ibdays) {
//			this.ibdays = ibdays;
//		}
//
//		public DoubleBox getDopercent() {
//			return dopercent;
//		}
//
//		public void setDopercent(DoubleBox dopercent) {
//			this.dopercent = dopercent;
//		}
//
//		public TextBox getTbcomment() {
//			return tbcomment;
//		}
//
//		public void setTbcomment(TextBox tbcomment) {
//			this.tbcomment = tbcomment;
//		}
//
//		public PaymentTermsTable getPaymentTermsTable() {
//			return paymentTermsTable;
//		}
//
//		public void setPaymentTermsTable(PaymentTermsTable paymentTermsTable) {
//			this.paymentTermsTable = paymentTermsTable;
//		}

		public SalesOrderLineItemTable getSalesorderlineitemtable() {
			return salesorderlineitemtable;
		}

		public void setSalesorderlineitemtable(
				SalesOrderLineItemTable salesorderlineitemtable) {
			this.salesorderlineitemtable = salesorderlineitemtable;
		}

		public ProductChargesTable getChargesTable() {
			return chargesTable;
		}

		public void setChargesTable(ProductChargesTable chargesTable) {
			this.chargesTable = chargesTable;
		}

		public ProductTaxesTable getProdTaxTable() {
			return prodTaxTable;
		}

		public void setProdTaxTable(ProductTaxesTable prodTaxTable) {
			this.prodTaxTable = prodTaxTable;
		}

		public DateBox getDbdeliverydate() {
			return dbdeliverydate;
		}

		public void setDbdeliverydate(DateBox dbdeliverydate) {
			this.dbdeliverydate = dbdeliverydate;
		}

		public AddressComposite getAcshippingcomposite() {
			return acshippingcomposite;
		}

		public void setAcshippingcomposite(AddressComposite acshippingcomposite) {
			this.acshippingcomposite = acshippingcomposite;
		}

		public CheckBox getCbcheckaddress() {
			return cbcheckaddress;
		}

		public void setCbcheckaddress(CheckBox cbcheckaddress) {
			this.cbcheckaddress = cbcheckaddress;
		}

		public ListBox getCbcformlis() {
			return cbcformlis;
		}

		public void setCbcformlis(ListBox cbcformlis) {
			this.cbcformlis = cbcformlis;
		}

		public ProductInfoComposite getProdInfoComposite() {
			return prodInfoComposite;
		}

		public void setProdInfoComposite(ProductInfoComposite prodInfoComposite) {
			this.prodInfoComposite = prodInfoComposite;
		}

		public ObjectListBox<TaxDetails> getOlbcstpercent() {
			return olbcstpercent;
		}

		public void setOlbcstpercent(ObjectListBox<TaxDetails> olbcstpercent) {
			this.olbcstpercent = olbcstpercent;
		}

		public IntegerBox getIbCreditPeriod() {
			return ibCreditPeriod;
		}

		public void setIbCreditPeriod(IntegerBox ibCreditPeriod) {
			this.ibCreditPeriod = ibCreditPeriod;
		}

//		public Button getAddPaymentTerms() {
//			return addPaymentTerms;
//		}
//
//		public void setAddPaymentTerms(Button addPaymentTerms) {
//			this.addPaymentTerms = addPaymentTerms;
//		}

		public Button getAddOtherCharges() {
			return addOtherCharges;
		}

		public void setAddOtherCharges(Button addOtherCharges) {
			this.addOtherCharges = addOtherCharges;
		}

		public Button getBaddproducts() {
			return baddproducts;
		}

		public void setBaddproducts(Button baddproducts) {
			this.baddproducts = baddproducts;
		}

		public TextBox getTbremark() {
			return tbremark;
		}

		public void setTbremark(TextBox tbremark) {
			this.tbremark = tbremark;
		}

		public DoubleBox getDbpaymentrecieved() {
			return dbpaymentrecieved;
		}

		public void setDbpaymentrecieved(DoubleBox dbpaymentrecieved) {
			this.dbpaymentrecieved = dbpaymentrecieved;
		}

		public DoubleBox getDbbalancepayment() {
			return dbbalancepayment;
		}

		public void setDbbalancepayment(DoubleBox dbbalancepayment) {
			this.dbbalancepayment = dbbalancepayment;
		}

		public TextBox getTbFullName() {
			return tbFullName;
		}

		public void setTbFullName(TextBox tbFullName) {
			this.tbFullName = tbFullName;
		}

		public TextBox getTbCompanyName() {
			return tbCompanyName;
		}

		public void setTbCompanyName(TextBox tbCompanyName) {
			this.tbCompanyName = tbCompanyName;
		}

		public TextBox getTbGSTNumber() {
			return tbGSTNumber;
		}

		public void setTbGSTNumber(TextBox tbGSTNumber) {
			this.tbGSTNumber = tbGSTNumber;
		}

		public AddressComposite getCustomerAddressComposite() {
			return customerAddressComposite;
		}

		public void setCustomerAddressComposite(
				AddressComposite customerAddressComposite) {
			this.customerAddressComposite = customerAddressComposite;
		}
		
		/**
		 * @author Priyanka
		 * @since 31-12-2020
		 * While when created directly from Quick Sales Order then this field are non-editable.
		 * raised by Rahul Tiwari
		 */
		
		public PhoneNumberBox getPnbCellNo() {
			return pnbCellNo;
		}

		public void setPnbCellNo(PhoneNumberBox pnbCellNo) {
			this.pnbCellNo = pnbCellNo;
		}

		public EmailTextBox getEtbEmail() {
			return etbEmail;
		}

		public void setEtbEmail(EmailTextBox etbEmail) {
			this.etbEmail = etbEmail;
		}

		@Override
		public void refreshTableData() {
			// TODO Auto-generated method stub
			super.refreshTableData();
			prodTaxTable.getTable().redraw();
			chargesTable.getTable().redraw();
			salesorderlineitemtable.getTable().redraw();
			tblOtherCharges.getTable().redraw();
			
		}
		

		protected void updateTaxesDetails(Customer cust) {
			List<SalesLineItem> prodlist = salesorderlineitemtable.getDataprovider().getList();
			if(prodlist.size()>0){
				Branch branchEntity =olbbBranch.getSelectedItem();
				
				boolean gstApplicable = false;
				if(olbcNumberRange.getSelectedIndex()!=0){
					Config configEntity = olbcNumberRange.getSelectedItem();
					if(configEntity!=null)
					gstApplicable = configEntity.isGstApplicable();
				}
				else{
					gstApplicable = true;
				}
				
				if(cust!=null){
					System.out.println("customer onselection");
					 String customerBillingaddressState = cust.getAdress().getState();
					 List<SalesLineItem> productlist = AppUtility.updateTaxesDetails(prodlist, branchEntity, customerBillingaddressState,gstApplicable);
					 salesorderlineitemtable.getDataprovider().setList(productlist);
					 salesorderlineitemtable.getTable().redraw();
					RowCountChangeEvent.fire(salesorderlineitemtable.getTable(), salesorderlineitemtable.getDataprovider().getList().size(), true);
					setEnable(true);
				}
				
			}
		}
}
