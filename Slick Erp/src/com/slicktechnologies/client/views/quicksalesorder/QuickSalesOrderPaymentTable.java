package com.slicktechnologies.client.views.quicksalesorder;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

public class QuickSalesOrderPaymentTable extends SuperTable<CustomerPayment>{

	TextColumn<CustomerPayment> getColumnSalesOrderId;
	TextColumn<CustomerPayment> getColumnPaymentId;
	TextColumn<CustomerPayment> getColumnCustomerName;
	TextColumn<CustomerPayment> getColumnNetPayable;
	TextColumn<CustomerPayment> getColumnStatus;
	TextColumn<CustomerPayment> getColumnPaymentDate;
	TextColumn<CustomerPayment> getColumnPaymentReceivedAmt;
	
	//Ashwini Patil Date:26-12-2022
	TextColumn<CustomerPayment> getColumnInvoiceId;
	TextColumn<CustomerPayment> getColumnInvoiceDate;
	TextColumn<CustomerPayment> getColumnBillingPeriod;
	NumberFormat df=NumberFormat.getFormat("0.00");
	public QuickSalesOrderPaymentTable(){
		createGui();
		setHeight("350px");
	}
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		
		addColumnInvoiceId();
		addColumnInvoiceDate();
		addColumnBillingPeriod();
		addColumnSalesOrderCustomerName();
		addColumnSalesOrderPaymentDate();
		addColumnSalesOrderTotalAmount();
		addColumnSalesOrderReceivedAmount();
		addColumnSalesOrderStatus();
		addColumnSalesOrderId();
		addColumnSalesOrderInvoiceId();
	}

	private void addColumnSalesOrderReceivedAmount() {

		getColumnPaymentReceivedAmt = new TextColumn<CustomerPayment>() {
			
			@Override
			public String getValue(CustomerPayment object) {
				return object.getPaymentReceived()+"";
			}
		};
		table.addColumn(getColumnPaymentReceivedAmt,"Amount Received");
		table.setColumnWidth(getColumnPaymentReceivedAmt, 110,Unit.PX);
	}

	private void addColumnSalesOrderPaymentDate() {

		getColumnPaymentDate = new TextColumn<CustomerPayment>() {
			
			@Override
			public String getValue(CustomerPayment object) {
				return AppUtility.parseDate(object.getPaymentDate());
			}
		};
		table.addColumn(getColumnPaymentDate,"PaymentDate");
		table.setColumnWidth(getColumnPaymentDate, 110,Unit.PX);
	}
	
	private void addColumnSalesOrderStatus() {
		getColumnStatus = new TextColumn<CustomerPayment>() {
			
			@Override
			public String getValue(CustomerPayment object) {
				return object.getStatus();
			}
		};
		table.addColumn(getColumnStatus,"Status");
		table.setColumnWidth(getColumnStatus, 110, Unit.PX);
	}


	private void addColumnSalesOrderId() {

		getColumnSalesOrderId = new TextColumn<CustomerPayment>() {
			
			@Override
			public String getValue(CustomerPayment object) {

				return object.getContractCount()+"";
			}
		};
		table.addColumn(getColumnSalesOrderId,"Order Id");
		table.setColumnWidth(getColumnSalesOrderId, 130,Unit.PX);
	}

	private void addColumnSalesOrderInvoiceId() {

		getColumnPaymentId = new TextColumn<CustomerPayment>() {
			
			@Override
			public String getValue(CustomerPayment object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getColumnPaymentId,"Payment Id");
		table.setColumnWidth(getColumnPaymentId, 130,Unit.PX);
	}

	private void addColumnSalesOrderCustomerName() {

		getColumnCustomerName = new TextColumn<CustomerPayment>() {
			
			@Override
			public String getValue(CustomerPayment object) {
				if(object.getPersonInfo()!=null)
					return object.getPersonInfo().getFullName();
				return "";
			}
		};
		table.addColumn(getColumnCustomerName,"Customer Name");
		table.setColumnWidth(getColumnCustomerName, 130,Unit.PX);
	}

	private void addColumnSalesOrderTotalAmount() {

		getColumnNetPayable = new TextColumn<CustomerPayment>() {
			
			@Override
			public String getValue(CustomerPayment object) {
				
				//return object.getTotalBillingAmount()+"";
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CONTRACT", "PC_NO_ROUNDOFF_INVOICE_CONTRACT")){
					return df.format(object.getPaymentAmtInDecimal())+"";
				}else {
					return object.getPaymentAmt()+"";
				}
				
			}
		};
		//table.addColumn(getColumnNetPayable,"NetPayable"); //Net Receivable
		table.addColumn(getColumnNetPayable,"Net Receivable"); //Added by priyanka - for creative pest -raised by Rahul
		table.setColumnWidth(getColumnNetPayable, 100,Unit.PX);
	}

	private void addColumnInvoiceId() {

		getColumnInvoiceId = new TextColumn<CustomerPayment>() {
			
			@Override
			public String getValue(CustomerPayment object) {
				return object.getInvoiceCount()+"";
			}
		};
		table.addColumn(getColumnInvoiceId,"Invoice Id");
		table.setColumnWidth(getColumnInvoiceId, 130,Unit.PX);
	}
	
	private void addColumnInvoiceDate() {

		getColumnInvoiceDate = new TextColumn<CustomerPayment>() {
			
			@Override
			public String getValue(CustomerPayment object) {
				return AppUtility.parseDate(object.getInvoiceDate())+"";
			}
		};
		table.addColumn(getColumnInvoiceDate,"Invoice Date");
		table.setColumnWidth(getColumnInvoiceDate, 130,Unit.PX);
	}
	
	private void addColumnBillingPeriod() {

		getColumnBillingPeriod = new TextColumn<CustomerPayment>() {
			
			@Override
			public String getValue(CustomerPayment object) {
				if(object.getBillingPeroidFromDate()!=null&&object.getBillingPeroidToDate()!=null)
					return AppUtility.parseDate(object.getBillingPeroidFromDate())+" - "+AppUtility.parseDate(object.getBillingPeroidToDate());
				else
					return "";
			}
		};
		table.addColumn(getColumnBillingPeriod,"Billing Period");
		table.setColumnWidth(getColumnBillingPeriod, 130,Unit.PX);
	}
	
	
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
