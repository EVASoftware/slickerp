package com.slicktechnologies.client.views.quicksalesorder;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;

public class QuickSalesOrderDeliveryNoteTable extends SuperTable<DeliveryNote> {

	
	TextColumn<DeliveryNote> getColumnSalesOrderId;
	TextColumn<DeliveryNote> getColumnDeliveryNoteId;
	TextColumn<DeliveryNote> getColumnCustomerName;
	TextColumn<DeliveryNote> getColumnNetPayable;
	TextColumn<DeliveryNote> getColumnStatus;
	TextColumn<DeliveryNote> getColumnDeliveryNoteDate;
	
	public QuickSalesOrderDeliveryNoteTable(){
		createGui();
		setHeight("350px");
	}
	
	@Override
	public void createTable() {

		addColumnSalesOrderId();
		addColumnSalesOrderDeliveryNoteId();
		addColumnSalesOrderCustomerName();
		addColumnSalesOrderDeliveryNoteDate();
		addColumnSalesOrderNetPayable();
		addColumnSalesOrderDeliveryNoteStatus();
	}

	
	private void addColumnSalesOrderDeliveryNoteDate() {

		getColumnDeliveryNoteDate = new TextColumn<DeliveryNote>() {
			
			@Override
			public String getValue(DeliveryNote object) {
				return AppUtility.parseDate(object.getDeliveryDate());
			}
		};
		table.addColumn(getColumnDeliveryNoteDate,"Delivery Note Date");
		table.setColumnWidth(getColumnDeliveryNoteDate, 110,Unit.PX);
	}
	
	private void addColumnSalesOrderDeliveryNoteStatus() {
		getColumnStatus = new TextColumn<DeliveryNote>() {
			
			@Override
			public String getValue(DeliveryNote object) {
				return object.getStatus();
			}
		};
		table.addColumn(getColumnStatus,"Status");
		table.setColumnWidth(getColumnStatus, 110, Unit.PX);
	}


	private void addColumnSalesOrderId() {

		getColumnSalesOrderId = new TextColumn<DeliveryNote>() {
			
			@Override
			public String getValue(DeliveryNote object) {

				return object.getSalesOrderCount()+"";
			}
		};
		table.addColumn(getColumnSalesOrderId,"Order Id");
		table.setColumnWidth(getColumnSalesOrderId, 130,Unit.PX);
	}

	private void addColumnSalesOrderDeliveryNoteId() {

		getColumnDeliveryNoteId = new TextColumn<DeliveryNote>() {
			
			@Override
			public String getValue(DeliveryNote object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getColumnDeliveryNoteId,"Delivery Note Id");
		table.setColumnWidth(getColumnDeliveryNoteId, 130,Unit.PX);
	}

	private void addColumnSalesOrderCustomerName() {

		getColumnCustomerName = new TextColumn<DeliveryNote>() {
			
			@Override
			public String getValue(DeliveryNote object) {
				if(object.getCinfo()!=null)
					return object.getCinfo().getFullName();
				return "";
			}
		};
		table.addColumn(getColumnCustomerName,"Customer Name");
		table.setColumnWidth(getColumnCustomerName, 130,Unit.PX);
	}

	private void addColumnSalesOrderNetPayable() {

		getColumnNetPayable = new TextColumn<DeliveryNote>() {
			
			@Override
			public String getValue(DeliveryNote object) {
				
				return object.getNetpayable()+"";
			}
		};
		table.addColumn(getColumnNetPayable,"Net Payable");
		table.setColumnWidth(getColumnNetPayable, 100,Unit.PX);
	}

	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
