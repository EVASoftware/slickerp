package com.slicktechnologies.client.views.quicksalesorder;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class QuickSalesOrderInvoiceTable extends SuperTable<Invoice>{

	
	TextColumn<Invoice> getColumnSalesOrderId;
	TextColumn<Invoice> getColumnInvoiceId;
	TextColumn<Invoice> getColumnCustomerName;
	TextColumn<Invoice> getColumnNetPayable;
	TextColumn<Invoice> getColumnInvoiceType;
	TextColumn<Invoice> getColumnPInvoiceId; //Ashwini Patil Date:27-12-2022
	TextColumn<Invoice> getColumnStatus;
	TextColumn<Invoice> getColumnInvoiceDate;
	
	public QuickSalesOrderInvoiceTable(){
		createGui();
		setHeight("350px");
	}
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		
		addColumnSalesOrderId();
		addColumnSalesOrderInvoiceId();
		addColumnSalesOrderCustomerName();
		addColumnSalesOrderInvoiceDate();
		addColumnInvoiceType();
		addColumnSalesOrderNetPayable();
		addColumnSalesOrderPInvoiceId();
		addColumnSalesOrderStatus();
	}

	
	private void addColumnInvoiceType() {

		getColumnInvoiceType = new TextColumn<Invoice>() {
			
			@Override
			public String getValue(Invoice object) {
				if(object.getInvoiceType()!=null)
				return object.getInvoiceType();
				return "";
			}
		};
		table.addColumn(getColumnInvoiceType,"Invoice Type");
		table.setColumnWidth(getColumnInvoiceType, 110,Unit.PX);
	}


	private void addColumnSalesOrderInvoiceDate() {

		getColumnInvoiceDate = new TextColumn<Invoice>() {
			
			@Override
			public String getValue(Invoice object) {
				return AppUtility.parseDate(object.getInvoiceDate());
			}
		};
		table.addColumn(getColumnInvoiceDate,"InvoiceDate");
		table.setColumnWidth(getColumnInvoiceDate, 110,Unit.PX);
	}
	
	private void addColumnSalesOrderStatus() {
		getColumnStatus = new TextColumn<Invoice>() {
			
			@Override
			public String getValue(Invoice object) {
				return object.getStatus();
			}
		};
		table.addColumn(getColumnStatus,"Status");
		table.setColumnWidth(getColumnStatus, 110, Unit.PX);
	}


	private void addColumnSalesOrderId() {

		getColumnSalesOrderId = new TextColumn<Invoice>() {
			
			@Override
			public String getValue(Invoice object) {

				return object.getContractCount()+"";
			}
		};
		table.addColumn(getColumnSalesOrderId,"Order Id");
		table.setColumnWidth(getColumnSalesOrderId, 130,Unit.PX);
	}

	private void addColumnSalesOrderInvoiceId() {

		getColumnInvoiceId = new TextColumn<Invoice>() {
			
			@Override
			public String getValue(Invoice object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getColumnInvoiceId,"Invoice Id");
		table.setColumnWidth(getColumnInvoiceId, 130,Unit.PX);
	}
	private void addColumnSalesOrderPInvoiceId() {

		getColumnPInvoiceId = new TextColumn<Invoice>() {
			
			@Override
			public String getValue(Invoice object) {
				if(object.getInvoiceType().equals("Tax Invoice")) {
					if(object.getProformaCount()!=null)
						return object.getProformaCount()+"";
					else
						return "";				
				}else {
					return "";
				}
			}
		};
		table.addColumn(getColumnPInvoiceId,"Proforma Invoice Id");
		table.setColumnWidth(getColumnPInvoiceId, 130,Unit.PX);
	}

	private void addColumnSalesOrderCustomerName() {

		getColumnCustomerName = new TextColumn<Invoice>() {
			
			@Override
			public String getValue(Invoice object) {
				if(object.getPersonInfo()!=null)
					return object.getPersonInfo().getFullName();
				return "";
			}
		};
		table.addColumn(getColumnCustomerName,"Customer Name");
		table.setColumnWidth(getColumnCustomerName, 130,Unit.PX);
	}

	private void addColumnSalesOrderNetPayable() {

		getColumnNetPayable = new TextColumn<Invoice>() {
			
			@Override
			public String getValue(Invoice object) {
				
				return object.getNetPayable()+"";
			}
		};
		table.addColumn(getColumnNetPayable,"NetPayable");
		table.setColumnWidth(getColumnNetPayable, 100,Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
