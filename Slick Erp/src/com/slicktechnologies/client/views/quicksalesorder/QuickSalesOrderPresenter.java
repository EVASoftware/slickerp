package com.slicktechnologies.client.views.quicksalesorder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;





import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummary;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummaryPopUp;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummaryPopUpTable;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenterSearchProxy;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenterTableProxy;
import com.slicktechnologies.client.views.salesorder.SalesOrderService;
import com.slicktechnologies.client.views.salesorder.SalesOrderServiceAsync;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter.SalesOrderPresenterSearch;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter.SalesOrderPresenterTable;
import com.slicktechnologies.client.views.workorder.WorkOrderForm;
import com.slicktechnologies.client.views.workorder.WorkOrderPresenter;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cancelsummary.CancelSummary;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.workorder.WorkOrder;
import com.smartgwt.client.widgets.tile.events.SelectionChangedEvent;

public class QuickSalesOrderPresenter extends FormScreenPresenter<SalesOrder> implements RowCountChangeEvent.Handler, SelectionHandler<Suggestion>, ChangeHandler {



	public static QuickSalesOrderForm form;
	final SalesOrderServiceAsync async = GWT.create(SalesOrderService.class);
	EmailServiceAsync emailService = GWT.create(EmailService.class);
	final CsvServiceAsync csvservice = GWT.create(CsvService.class);
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	NumberFormat nf=NumberFormat.getFormat("0.00");
	String retrCompnyState=retrieveCompanyState();
	static String companyStateVal=""; 
	ConditionDialogBox conditionPopup=new ConditionDialogBox("Are you sure you want to change address. All products will be deleted!",AppConstants.YES,AppConstants.NO);
	PopupPanel panel, preprintPanel;
	
	ConditionDialogBox conditionPopupforCreateAMC=new ConditionDialogBox("AMC has been already created for this Sales Order Do you want to create annother one",AppConstants.YES,AppConstants.NO);
	
	
//  rohan added this cnt for preprint functionality 
	int cnt=0;
	ConditionDialogBox conditionPopupforPreprint=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
	
	List<CancellationSummary> cancellis=new ArrayList<CancellationSummary>();
	CancellationSummaryPopUp summaryPopup = new CancellationSummaryPopUp();
	PopupPanel summaryPanel;
	DocumentCancellationPopUp cancelpopup = new DocumentCancellationPopUp();
	PopupPanel cancelPanel ;
			
	ViewInvoicePaymentDeliveryNotePopup invoicePaymentPopup;
	PopupPanel invoicePaymentPanel;

	NewEmailPopUp emailpopup = new NewEmailPopUp();

	public QuickSalesOrderPresenter(FormScreen<SalesOrder> view, SalesOrder model) {
		super(view, model);
		form = (QuickSalesOrderForm) view;
		form.setPresenter(this);
		form.getSalesorderlineitemtable().getTable().addRowCountChangeHandler(this);
		form.getChargesTable().getTable().addRowCountChangeHandler(this);
		form.getCbcheckaddress().addClickHandler(this);
		form.getPersonInfoComposite().getId().addSelectionHandler(this);
		form.getPersonInfoComposite().getName().addSelectionHandler(this);
		form.getPersonInfoComposite().getPhone().addSelectionHandler(this);
		form.getCbcformlis().addChangeHandler(this);
		form.getOlbcstpercent().addChangeHandler(this);
		form.getAcshippingcomposite().getState().addChangeHandler(this);
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		
		cancelpopup.getBtnOk().addClickHandler(this);
		cancelpopup.getBtnCancel().addClickHandler(this);
		summaryPopup.getBtnOk().addClickHandler(this);
		summaryPopup.getBtnDownload().addClickHandler(this);
		
		conditionPopupforCreateAMC.getBtnOne().addClickHandler(this);
		conditionPopupforCreateAMC.getBtnTwo().addClickHandler(this);
		
		
		conditionPopupforPreprint.getBtnOne().addClickHandler(this);
		conditionPopupforPreprint.getBtnTwo().addClickHandler(this);
		
		form.dbpaymentrecieved.addChangeHandler(this);
		form.getTbFullName().addChangeHandler(this);
		form.getTbCompanyName().addChangeHandler(this);
		
		
		invoicePaymentPopup = new ViewInvoicePaymentDeliveryNotePopup();
		invoicePaymentPopup.btnClose.addClickHandler(this);
		/**
		 * Date : 15-09-2017 BY ANIL
		 * Adding row count change handler on other charges table
		 */
		form.tblOtherCharges.getTable().addRowCountChangeHandler(this);
		/**
		 * End
		 */
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.QUICKSALESORDER,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals(AppConstants.NEW)) {
			reactToNew();
		}
		if(text.equals(AppConstants.CANCLESALESORDER))
		{
			cancelpopup.getPaymentDate().setValue(new Date()+"");
			cancelpopup.getPaymentID().setValue(form.getTbContractId().getValue().trim());
			cancelpopup.getPaymentStatus().setValue(form.getstatustextbox().getValue().trim());
			cancelPanel=new PopupPanel(true);
			cancelPanel.add(cancelpopup);
			cancelPanel.show();
			cancelPanel.center();
			
		}
		if(text.equals("Email")){
			reactOnEmail();
		}
		if(text.equals(AppConstants.CANCELLATIONSUMMARY)){
			getSummaryFromSalesOrderID();
		}
		if(text.equals(AppConstants.WORKORDER)){
			reactOnWorkOrder();
		}

		if(text.equals("Create AMC")){
			checkContractStatusAgainsSalesOrder();
		}
		
		//Date 14 july 2017 added by vijay 
		if(text.equals(AppConstants.SUBMIT)){
			reactonSubmit();
		}
		
		if(text.equals(AppConstants.VIEWINVOICEPAYMENT)){
			reatconViewInvoicePaymentAndDeliveryNote();
		}
	}

	private void reatconViewInvoicePaymentAndDeliveryNote() {
		
		MyQuerry query = getQuery(model);
		query.setQuerryObject(new BillingDocument());

		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
			
				System.out.println("Result size=="+result.size());
				ArrayList<BillingDocument> billinglist = new ArrayList<BillingDocument>();
				for(SuperModel model : result){
					BillingDocument billingEntity = (BillingDocument) model;
					
					billinglist.add(billingEntity);
				}

				invoicePaymentPopup.companyId=model.getCompanyId();
				invoicePaymentPopup.salesOrderId=model.getCount();
				invoicePaymentPopup.getTabBillingTable().getDataprovider().setList(billinglist);
				invoicePaymentPopup.getTabBillingTable().getTable().redraw();
				invoicePaymentPanel = new PopupPanel();
				
				invoicePaymentPanel.add(invoicePaymentPopup);
				invoicePaymentPanel.center();
				invoicePaymentPanel.show();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Unexpected Error Occurred");

			}
		});
		
	}

	private MyQuerry getQuery(SalesOrder model) {

		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		
		Filter filter = new Filter();
		filter.setIntValue(model.getCount());
		filter.setQuerryString("contractCount");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setLongValue(model.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setStringValue("Sales Order");
		filter.setQuerryString("typeOfOrder");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		return querry;
	}

	/**
	 * Date 14 july 2017 added by vijay for sales order approved and 
	 */
	
	private void reactonSubmit() {
		
		GeneralServiceAsync genAsync = GWT.create(GeneralService.class);
		form.showWaitSymbol();
		genAsync.quickSalesSumbit(model, new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(final String result) {
				// TODO Auto-generated method stub
				
				
				model.setStatus(SalesOrder.APPROVED);
				form.tbQuotationStatus.setValue(SalesOrder.APPROVED);
				form.setMenuAsPerStatus();
				Timer time = new Timer() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
							form.showDialogMessage(result);
							form.hideWaitSymbol();
					}
				};
				time.schedule(3000);
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Unexpected Error Occured");
				form.hideWaitSymbol();
			}
		});
	}
	/**
	 * ends here
	 */

	private void checkContractStatusAgainsSalesOrder() {
		// TODO Auto-generated method stub
		System.out.println("hiiiiiiiiiii");
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = new Filter();
		
		
		filter.setStringValue(model.getCount()+"");
		filter.setQuerryString("refNo");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setLongValue(model.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
		
		genasync.getSearchResult(querry, new  AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("Result====="+result.size());
				
				if(result.size()>0){
					System.out.println("result size >0");
					panel=new PopupPanel(true);
					panel.add(conditionPopupforCreateAMC);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
				}
				
				if(result.size()==0){
					System.out.println("result size == 0");
					reactOnCreateAMC();
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	private void reactOnCreateAMC() {
		
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract",Screen.CONTRACT);
		final ContractForm form=ContractPresenter.initalize();
		final Contract contract = new Contract();
		
		PersonInfo pinfo = new PersonInfo();
		
		pinfo.setCount(model.getCustomerId());
		pinfo.setFullName(model.getCustomerFullName());
		pinfo.setCellNumber(model.getCustomerCellNumber());
		pinfo.setPocName(model.getCustomerFullName());
		
		contract.setCinfo(pinfo);
		
		contract.setRefNo(model.getCount()+"");
		System.out.println("Hi =="+model.getCount()+"");
		contract.setRefDate(model.getSalesOrderDate());
		contract.setBranch(model.getBranch());
		contract.setEmployee(model.getEmployee());
		
		form.showWaitSymbol();
		Timer timer = new Timer() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				form.setToNewState();
				form.updateView(contract);
				form.getTbReferenceNumber().setEnabled(false);
				form.getDbrefernceDate().setEnabled(false);
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);

	}
	
	
	@Override
	public void reactOnPrint() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("SalesOrder");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}			

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result set size +++++++"+result.size());
				
				List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
				
				if(result.size()==0){
					
					final String url = GWT.getModuleBaseURL() + "pdfsales"+"?Id="+model.getId()+"&"+"type="+"so"+"&"+"preprint="+"plane";
					 Window.open(url, "test", "enabled");
					
				}
				/**
				 * @author Vijay Chougule
				 * Des :- added sales order code here to print updated PDF
				 */
				else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "POPDFV1")){
					final String url = GWT.getModuleBaseURL() + "pdfsales"+"?Id="+model.getId()+"&"+"type="+"so"+"&"+"preprint="+"plane"+"&"+"subtype="+"POPDFV1";
					 Window.open(url, "test", "enabled");
				}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "SalesOrderUpdatedPdf")){
					final String url = GWT.getModuleBaseURL() + "pdfsales"+"?Id="+model.getId()+"&"+"type="+"so"+"&"+"preprint="+"plane"+"&"+"subtype="+"SalesPdfUpdated";
					 Window.open(url, "test", "enabled");
				}//SalesPdfUpdated
				else{
				
					for(SuperModel model:result)
					{
						ProcessConfiguration processConfig=(ProcessConfiguration)model;
						processList.addAll(processConfig.getProcessList());
						
					}
				
				for(int k=0;k<processList.size();k++){	
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
					
					cnt=cnt+1;
				
				}
				}

				if(cnt>0){
					System.out.println("in side react on prinnt cnt");
					preprintPanel=new PopupPanel(true);
					preprintPanel.add(conditionPopupforPreprint);
					preprintPanel.setGlassEnabled(true);
					preprintPanel.show();
					preprintPanel.center();
				}
				else
				{
					
					final String url = GWT.getModuleBaseURL() + "pdfsales"+"?Id="+model.getId()+"&"+"type="+"so"+"&"+"preprint="+"plane";
					 Window.open(url, "test", "enabled");
				
				}
				
			
			}
			}
		});
		
		
	}
	
	@Override
	public void reactOnDownload() {
		ArrayList<SalesOrder> salesorderarray=new ArrayList<SalesOrder>();
		List<SalesOrder> list=(List<SalesOrder>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		salesorderarray.addAll(list);
		
		csvservice.setSalesOrderList(salesorderarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				Console.log("RPC call Failed"+caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+61;
				Window.open(url, "test", "enabled");
			}
		});
	}

	public void reactOnEmail()
	{
//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true) {
//			emailService.initiateSalesOrderEmail((Sales) model,new AsyncCallback<Void>() {
//
//				@Override
//				public void onFailure(Throwable caught) {
//					Window.alert("Resource Quota Ended ");
//					caught.printStackTrace();
//				}
//
//				@Override
//				public void onSuccess(Void result) {
//					Window.alert("Email Sent Sucessfully !");
//					
//				}
//			});
//		}
		
		/**
		 * @author Vijay Date 19-03-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData();
		
		/**
		 * ends here
		 */
	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model=new SalesOrder();
	}

	public static QuickSalesOrderForm initalize()
	{			
				QuickSalesOrderForm form=new  QuickSalesOrderForm();
				SalesOrderPresenterTable gentable=new SalesOrderPresenterTableProxy();
				gentable.setView(form);
				gentable.applySelectionModle();
				SalesOrderPresenterSearch.staticSuperTable=gentable;
				SalesOrderPresenterSearch searchpopup=new SalesOrderPresenterSearchProxy();
				form.setSearchpopupscreen(searchpopup);
				QuickSalesOrderPresenter  presenter=new QuickSalesOrderPresenter(form,new SalesOrder());
				AppMemory.getAppMemory().stickPnel(form);
				
		return form;
	}

//	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.salesorder.SalesOrder")
//	public static  class SalesOrderPresenterSearch extends SearchPopUpScreen<SalesOrder>{
//
//		@Override
//		public MyQuerry getQuerry() {
//			// TODO Auto-generated method stub
//			return null;
//		}
//
//		@Override
//		public boolean validate() {
//			// TODO Auto-generated method stub
//			return true;
//		}};

//		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.salesorder.SalesOrder")
//		public static class SalesOrderPresenterTable extends SuperTable<SalesOrder> implements GeneratedVariableRefrence{
//
//			@Override
//			public Object getVarRef(String varName) {
//				// TODO Auto-generated method stub
//				return null;
//			}
//
//			@Override
//			public void createTable() {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			protected void initializekeyprovider() {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void addFieldUpdater() {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void setEnable(boolean state) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void applyStyle() {
//				// TODO Auto-generated method stub
//
//			}} ;
			
			
			private void reactToCancel()
			{
				MyQuerry querry=new MyQuerry();
				Vector<Filter> filtervec=new Vector<Filter>();
				
				Filter temp=null;
				
				temp=new Filter();
				temp.setLongValue(model.getCompanyId());
				temp.setQuerryString("companyId");
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setIntValue(model.getCount());
				temp.setStringValue("contractCount");
				filtervec.add(temp);
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Invoice());
				
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected error occurred!");
					}
		
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						if(result.size()==0){
							validateDeliveryNote();
						}
						
						if(result.size()>0){
							int count=0;
							for(SuperModel imodel:result)
							{
								Invoice inv=(Invoice)imodel;
								if(!inv.getStatus().equals(Invoice.CANCELLED)){
//									form.showDialogMessage("Please cancel invoice document before cancelling sales order");
									count=count+1;
								}
								
//								if(!inv.getStatus().equals(Invoice.REQUESTED)&&!inv.getStatus().equals(Invoice.APPROVED)&&!inv.getStatus().equals(Invoice.CREATED)){
//									validateDeliveryNote();
//								}
							}
							
							if(count!=0){
								form.showDialogMessage("Please cancel invoice document before cancelling sales order");
							}
							if(count==0){
								validateDeliveryNote();
							}
						}
						
						}
					 });
				
			}
			
			private void reactToNew()
			{
				form.setToNewState();
				initalize();
			}
			
			private void validateDeliveryNote()
			{
				MyQuerry querryd=new MyQuerry();
				Vector<Filter> filtervecd=new Vector<Filter>();
				
				Filter tempd=null;
				
				tempd=new Filter();
				tempd.setLongValue(model.getCompanyId());
				tempd.setQuerryString("companyId");
				filtervecd.add(tempd);
				
				tempd=new Filter();
				tempd.setIntValue(model.getCount());
				tempd.setStringValue("salesOrderCount");
				filtervecd.add(tempd);
				
				querryd.setFilters(filtervecd);
				querryd.setQuerryObject(new DeliveryNote());
				
				genasync.getSearchResult(querryd, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected error occurred!");
					}
		
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						int count=0;
						
						if(result.size()==0){
							model.setStatus(SalesOrder.SALESORDERCANCEL);
							form.getTbQuotationStatus().setText(SalesOrder.SALESORDERCANCEL);
							changeStatus("Failed To Cancel SalesOrder !","Sales Order Cancelled !");
							form.setAppHeaderBarAsPerStatus();
							form.setMenuAsPerStatus();
						}
						
						for(SuperModel dmodel:result){
							DeliveryNote delNote=(DeliveryNote) dmodel;
							

							if(!delNote.getStatus().trim().equals(ConcreteBusinessProcess.CREATED)&&!delNote.getStatus().trim().equals(ConcreteBusinessProcess.CANCELLED)){
								count=count+1;
							}
							
							if(count!=0){
								form.showDialogMessage("Please cancel delivery note before cancelling sales order!");
							}
							if(count==0){
								model.setStatus(SalesOrder.SALESORDERCANCEL);
								form.getTbQuotationStatus().setText(SalesOrder.SALESORDERCANCEL);
								changeStatus("Failed To Cancel SalesOrder !","Sales Order Cancelled !");
								form.setAppHeaderBarAsPerStatus();
								form.setMenuAsPerStatus();
							}
							}
						}
					 });
			}
			
			private void changeStatus(final String failureMessage,final String successMessage)
			{
				async.changeStatus(model,new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage(failureMessage);
					}

					@Override
					public void onSuccess(Void result) {
						form.showDialogMessage(successMessage);
					}
				});
			}
			
			
			@Override
			public void onRowCountChange(RowCountChangeEvent event) 
			{
				if(event.getSource().equals(form.getSalesorderlineitemtable().getTable()))
				{
					this.productsOnChange();
			    }
				
				if(event.getSource().equals(form.getChargesTable().getTable())){
					if(form.getDoamtincltax().getValue()!=null){
						double newnetpay=form.getDoamtincltax().getValue()+form.getChargesTable().calculateNetPayable();
						newnetpay=Math.round(newnetpay);
						int netPayAmt=(int) newnetpay;
						newnetpay=netPayAmt;
						form.getDonetpayamt().setValue(Double.parseDouble(nf.format(newnetpay)));
						
						/******************** vijay  for quick sales on 14 july 2017 *******************/
						if(form.getDbpaymentrecieved().getValue()!=null){
							double balancePayment=newnetpay-form.getDbpaymentrecieved().getValue();
							System.out.println("balance payment ===="+balancePayment);
							form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(balancePayment)));
						}else{
							form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(newnetpay)));
						}
					}
				}
				
				/**
				 * Date : 16-09-2017 By ANIL
				 */
				if(event.getSource().equals(form.tblOtherCharges.getTable())){
					form.prodTaxTable.connectToLocal();
					try {
						form.addProdTaxes();
						form.addOtherChargesInTaxTbl();
						form.dbOtherChargesTotal.setValue(form.tblOtherCharges.calculateOtherChargesSum());
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(form.getDoamtincltax().getValue()!=null){
						double totalIncludingTax=form.getDototalamt().getValue()+form.getProdTaxTable().calculateTotalTaxes();
						totalIncludingTax=totalIncludingTax+form.dbOtherChargesTotal.getValue();
						form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
						form.updateChargesTable();
					}else{
						double totalIncludingTax=form.getProdTaxTable().calculateTotalTaxes();
						totalIncludingTax=totalIncludingTax+form.dbOtherChargesTotal.getValue();
						form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
						form.updateChargesTable();
					}
					
					double netPay=fillNetPayable(form.getDoamtincltax().getValue());
					netPay=Math.round(netPay);
					int netPayable=(int) netPay;
					netPay=netPayable;
					form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
					
					/******************** vijay  for quick sales on 14 july 2017 *******************/
					if(form.getDbpaymentrecieved().getValue()!=null){
						double balancePayment=netPay-form.getDbpaymentrecieved().getValue();
						System.out.println("balance payment ===="+balancePayment);
						form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(balancePayment)));
					}else{
						form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(netPay)));
					}
					/**
					 * ends here
					 */
					
					String approverName="";
					if(form.olbApproverName.getValue()!=null){
						approverName=form.olbApproverName.getValue();
					}
					AppUtility.getApproverListAsPerTotalAmount(form.olbApproverName, "Sales Order", approverName, form.getDonetpayamt().getValue());
				}
				/**
				 * End
				 */
			}
			
			private double fillNetPayable(double amtincltax)
			{
				double amtval=0;
				if(form.getChargesTable().getDataprovider().getList().size()==0)
				{
					amtval=amtincltax;
				}
				if(form.getChargesTable().getDataprovider().getList().size()!=0)
				{
					amtval=amtincltax+form.getChargesTable().calculateNetPayable();
				}
				amtval=Math.round(amtval);
				int retAmtVal=(int) amtval;
				amtval=retAmtVal;
				return amtval;
			}

			@Override
			public void onClick(ClickEvent event) {
				super.onClick(event);
				
				
				if(event.getSource().equals(conditionPopupforPreprint.getBtnOne()))
				{
					
					if(cnt >0){
						System.out.println("in side one yes");
						
						final String url = GWT.getModuleBaseURL() + "pdfsales"+"?Id="+model.getId()+"&"+"type="+"so"+"&"+"preprint="+"yes";
						 Window.open(url, "test", "enabled");
						 preprintPanel.hide();
					}
				}
				
				
				if(event.getSource().equals(conditionPopupforPreprint.getBtnTwo()))
				{
					if(cnt >0){
						System.out.println("inside two no");
						
						final String url = GWT.getModuleBaseURL() + "pdfsales"+"?Id="+model.getId()+"&"+"type="+"so"+"&"+"preprint="+"no";
						 Window.open(url, "test", "enabled");
						 
						 preprintPanel.hide();
					}
				}
				
				
				if(event.getSource().equals(form.getCbcheckaddress())){
//					if(!form.getPersonInfoComposite().getId().getValue().equals("")&&form.getSalesorderlineitemtable().getDataprovider().getList().size()>0)
//					{
//						panel=new PopupPanel(true);
//						panel.add(conditionPopup);
//						panel.setGlassEnabled(true);
//						panel.show();
//						panel.center();
//					}
					
					if(!form.getPersonInfoComposite().getId().getValue().equals(""))
					{
						if(form.getCbcheckaddress().getValue()==false)
						{
							form.getAcshippingcomposite().setEnable(false);
							try {
								retrieveShippingAddress();
							} 
							catch (Exception e) {
								e.printStackTrace();
							}
						}
						
						if(form.getCbcheckaddress().getValue()==true)
						{
							form.getAcshippingcomposite().setEnable(true);
							form.getAcshippingcomposite().getAdressline1().setValue(null);
							form.getAcshippingcomposite().getAdressline2().setValue(null);
							form.getAcshippingcomposite().getLocality().setSelectedIndex(0);
							form.getAcshippingcomposite().getLandMark().setText(null);
							form.getAcshippingcomposite().getPin().setText(null);
							form.getAcshippingcomposite().getCity().setSelectedIndex(0);
							form.getAcshippingcomposite().getCountry().setSelectedIndex(0);
							form.getAcshippingcomposite().getState().setSelectedIndex(0);
						}
						// date 20-08-2017 added by vijay
						else if(form.getCbcheckaddress().getValue()==false){
							
							form.getAcshippingcomposite().setEnable(false);
							try {
								retrieveShippingAddress();
							} 
							catch (Exception e) {
								e.printStackTrace();
							}
						}
					}else { 					
						// date 20-08-2017 else added by vijay for new customer shipping address is editable
						if(form.getCbcheckaddress().getValue()==true)
						{
							form.getAcshippingcomposite().setEnable(true);
							form.getAcshippingcomposite().getAdressline1().setValue(null);
							form.getAcshippingcomposite().getAdressline2().setValue(null);
							form.getAcshippingcomposite().getLocality().setSelectedIndex(0);
							form.getAcshippingcomposite().getLandMark().setText(null);
							form.getAcshippingcomposite().getPin().setText(null);
							form.getAcshippingcomposite().getCity().setSelectedIndex(0);
							form.getAcshippingcomposite().getCountry().setSelectedIndex(0);
							form.getAcshippingcomposite().getState().setSelectedIndex(0);
						}
						if(form.getCbcheckaddress().getValue()==false)
						{
							form.getAcshippingcomposite().setEnable(false);
						}
					}
				}
				
				
				if(event.getSource().equals(conditionPopup.getBtnOne())&&form.getCbcheckaddress().getValue()==true)
				{
					clearProductsAndAddress();
						panel.hide();
				}
				
				if(event.getSource().equals(conditionPopup.getBtnOne())&&form.getCbcheckaddress().getValue()==false)
				{
					if(!form.getPersonInfoComposite().getId().getValue().equals("")){
							try {
								
								retrieveShippingAddress();
							} catch (Exception e) {
								e.printStackTrace();
							}
					}
//					clearProductsAndAddress();
//					form.getProdTaxTable().connectToLocal();
//					form.getChargesTable().connectToLocal();
//					form.getSalesorderlineitemtable().connectToLocal();
//					form.getDoamtincltax().setValue(null);
//					form.getDonetpayamt().setValue(null);
//					form.getDototalamt().setValue(null);
//					form.getCbcformlis().setSelectedIndex(0);
//					form.getOlbcstpercent().setSelectedIndex(0);
//					form.getCbcformlis().setEnabled(false);
//					form.getOlbcstpercent().setEnabled(false);
					form.getAcshippingcomposite().setEnable(false);
					form.getAcshippingcomposite().getAdressline1().setValue(null);
					form.getAcshippingcomposite().getAdressline2().setValue(null);
					form.getAcshippingcomposite().getLocality().setSelectedIndex(0);
					form.getAcshippingcomposite().getLandMark().setText(null);
					form.getAcshippingcomposite().getPin().setText(null);
					form.getAcshippingcomposite().getCity().setSelectedIndex(0);
					form.getAcshippingcomposite().getCountry().setSelectedIndex(0);
					form.getAcshippingcomposite().getState().setSelectedIndex(0);
					panel.hide();
				}
				
				if(event.getSource().equals(conditionPopup.getBtnTwo()))
				{
					boolean flag=false;
					if(form.getCbcheckaddress().getValue()==true)
					{
						System.out.println("Two True");
						form.getAcshippingcomposite().setEnable(false);
								try {
									retrieveShippingAddress();
									flag=true;
									form.getCbcheckaddress().setValue(false);
								} catch (Exception e) {
									e.printStackTrace();
								}
						
						panel.hide();
					}
					
					if(form.getCbcheckaddress().getValue()==false&&flag==false)
					{
						form.getAcshippingcomposite().setEnable(true);
						form.getAcshippingcomposite().getAdressline1().setValue(null);
						form.getAcshippingcomposite().getAdressline2().setValue(null);
						form.getAcshippingcomposite().getLocality().setSelectedIndex(0);
						form.getAcshippingcomposite().getLandMark().setText(null);
						form.getAcshippingcomposite().getPin().setText(null);
						form.getAcshippingcomposite().getCity().setSelectedIndex(0);
						form.getAcshippingcomposite().getCountry().setSelectedIndex(0);
						form.getAcshippingcomposite().getState().setSelectedIndex(0);
						form.getCbcheckaddress().setValue(true);
						panel.hide();
					}
				}
				
				

				if(event.getSource().equals(cancelpopup.getBtnOk())){
					cancelPanel.hide();
					if(form.getstatustextbox().getValue().equals("Approved")){
						chequeForDeliveryNoteStatus();
					}
				}
				if(event.getSource().equals(cancelpopup.getBtnCancel())){
					cancelPanel.hide();
					cancelpopup.remark.setValue("");
				}
				
				
				if(event.getSource()==summaryPopup.getBtnOk()){
					summaryPanel.hide();
					
				}
				if(event.getSource()==summaryPopup.getBtnDownload()){
					summaryPanel.hide();
					reactOnCancelSummaryDownload();
					
				}
				
				if(event.getSource() == invoicePaymentPopup.btnClose){
//					this.initalize();
//					
//					Timer time = new Timer() {
//						@Override
//						public void run() {
//							form.updateView(model);
//				        	form.setToViewState();
//							invoicePaymentPanel.hide();
//						}
//					};
//					time.schedule(3000);
					/** Date 09-03-2021 by Vijay updated code for smooth execution **/
					/** Date 09-03-2021 by Vijay updated code for smooth execution **/
					AppMemory.getAppMemory().currentScreen = Screen.QUICKSALESORDER;
					if(form.getProcesslevelBarNames()!=null)
					form.setProcesslevelBarNames(form.getProcesslevelBarNames());
					if(form.getProcessLevelBar()!=null)
					form.setProcessLevelBar(form.getProcessLevelBar());
					if(form.getProcesslevel()!=null)
					form.setProcesslevel(form.getProcesslevel());

					if(!AppMemory.getAppMemory().enableMenuBar||FormStyle.DEFAULT==form.formstyle){
						form.content.add(form.processLevelBar.content);	
					}else{
						form.addClickEventOnActionAndNavigationMenus();
					}
					
					form.setToViewState();
					invoicePaymentPanel.hide();
					setEventHandeling();
				}
			}
			
			private void productsOnChange()
			{
				
				form.showWaitSymbol();
				Timer timer=new Timer() 
			     {
					@Override
					public void run() 
					{
				    double totalExcludingTax=form.getSalesorderlineitemtable().calculateTotalExcludingTax();
				    totalExcludingTax=Double.parseDouble(nf.format(totalExcludingTax));
				    form.getDototalamt().setValue(totalExcludingTax);
				    
					boolean chkSize=form.getSalesorderlineitemtable().removeChargesOnDelete();
					if(chkSize==false){
						form.getChargesTable().connectToLocal();
					}
					
					try {
						form.prodTaxTable.connectToLocal();
						form.addProdTaxes();
						
						/**
						 * Date : 15-09-2017 By ANIL
						 * This method is added to calculate other charges and tax on it.
						 */
						form.addOtherChargesInTaxTbl();
						form.dbOtherChargesTotal.setValue(form.tblOtherCharges.calculateOtherChargesSum());
						/**
						 * End
						 */
						//Date 20 july 2017 added by vijay for vattax and service tax dont display  old code added in if conditon
						if(form.getProdTaxTable().getDataprovider().getList().size()!=0){
							double totalIncludingTax=form.getDototalamt().getValue()+form.getProdTaxTable().calculateTotalTaxes();
							/**
							 * Date : 20-09-2017 By ANIL
							 */
							totalIncludingTax=totalIncludingTax+form.dbOtherChargesTotal.getValue();
							/**
							 * End
							 */
							form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
						}else{
							form.getDoamtincltax().setValue(form.getDototalamt().getValue());
						}
						
						form.updateChargesTable();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					double netPay=fillNetPayable(form.getDoamtincltax().getValue());
					netPay=Math.round(netPay);
					int netPayable=(int) netPay;
					netPay=netPayable;
					form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));
					
					String approverName="";
					if(form.olbApproverName.getValue()!=null){
						approverName=form.olbApproverName.getValue();
					}
					AppUtility.getApproverListAsPerTotalAmount(form.olbApproverName, "Sales Order", approverName, form.getDonetpayamt().getValue());
				
					
					/******************** vijay  for quick sales on 14 july 2017 *******************/
					if(form.getDbpaymentrecieved().getValue()!=null){
						double balancePayment=netPay-form.getDbpaymentrecieved().getValue();
						System.out.println("balance payment ===="+balancePayment);
						form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(balancePayment)));
					}else{
						form.getDbbalancepayment().setValue(Double.parseDouble(nf.format(netPay)));
					}
					/**
					 * ends here
					 */
					
					
					form.hideWaitSymbol();
					}
		    	 };
		    	 timer.schedule(3000);
		    
			}
			
			private void retrieveShippingAddress()
			{
				form.showWaitSymbol();
				 Timer timer=new Timer() 
			     {
					@Override
					public void run() 
					{
					MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter tempfilter=new Filter();
					tempfilter=new Filter();
					tempfilter.setQuerryString("count");
					int custId=Integer.parseInt(form.getPersonInfoComposite().getId().getValue());
					tempfilter.setIntValue(custId);
					filtervec.add(tempfilter);
					tempfilter=new Filter();
					tempfilter.setQuerryString("companyId");
					tempfilter.setLongValue(model.getCompanyId());
					filtervec.add(tempfilter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());
					genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
							
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected error occurred!");
								form.hideWaitSymbol();
							}
				
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								System.out.println("Size Of Result In SalesQuotation Presenter"+result.size());
								for(SuperModel model:result)
								{
									Customer custentity = (Customer)model;
									form.getAcshippingcomposite().setValue(custentity.getSecondaryAdress());
									
									if(!checkCompanyState()){
										form.getCbcformlis().setEnabled(true);
									}
									
									
									form.customerAddressComposite.setEnable(false);
									form.tbCompanyName.setValue("");
									form.tbCompanyName.setEnabled(false);
									form.tbFullName.setValue("");
									form.tbFullName.setEnabled(false);
									form.pnbCellNo.setValue(0l);
									form.pnbCellNo.setEnabled(false);
									form.etbEmail.setValue("");
									form.etbEmail.setEnabled(false);
									form.olbbCustomerBranch.setEnabled(false);
									
									form.hideWaitSymbol();
								}
								}
							 });
//					form.hideWaitSymbol();
					}
		    	 };
		    	 timer.schedule(3000);		
		    	 form.hideWaitSymbol();
			}

			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				
				if(event.getSource().equals(form.getPersonInfoComposite().getId())||event.getSource().equals(form.getPersonInfoComposite().getName())||event.getSource().equals(form.getPersonInfoComposite().getPhone())){
					form.checkCustomerStatus(form.getPersonInfoComposite().getIdValue());
					
					form.btnCopyAddress.setEnabled(false);
					try {
						retrieveShippingAddress();
						/**
						 * @author Priyanka , Date : 31-12-2020
						 * Loading customer branch
						 */
						form.loadCustomerBranch(null, form.getPersonInfoComposite().getIdValue());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
//				if(event.getSource()==form.getPersonInfoComposite().getId())
//				{
//					if(!form.getPersonInfoComposite().getId().getValue().equals("")){
//						retrieveShippingAddress();
//					}
//				}
//				
//				if(event.getSource()==form.getPersonInfoComposite().getName())
//				{
//					if(!form.getPersonInfoComposite().getName().getValue().equals("")){
//						retrieveShippingAddress();
//					}
//				}
//				
//				if(event.getSource()==form.getPersonInfoComposite().getPhone())
//				{
//					if(!form.getPersonInfoComposite().getPhone().getValue().equals("")){
//						retrieveShippingAddress();
//					}
//				}
			}

			@Override
			public void onChange(ChangeEvent event) {
				/**************************************************************************************/
				if(event.getSource().equals(form.getCbcformlis()))
				{
					if(form.cbcformlis.getSelectedIndex()!=0){
						int index=form.cbcformlis.getSelectedIndex();
						if(AppConstants.YES.equals(form.cbcformlis.getItemText(index).trim())){
							form.getOlbcstpercent().setEnabled(true);
						}
						if(AppConstants.NO.equals(form.cbcformlis.getItemText(index).trim())){
							productsOnChange();
							form.getOlbcstpercent().setEnabled(false);
							form.getOlbcstpercent().setSelectedIndex(0);
						}
					}
					if(form.getCbcformlis().getSelectedIndex()==0){
						form.getOlbcstpercent().setSelectedIndex(0);
						form.getOlbcstpercent().setEnabled(false);
						productsOnChange();
					}
				}
				
				/**************************************************************************************/
				
				if(event.getSource().equals(form.getOlbcstpercent())){
					if(form.getOlbcstpercent().getSelectedIndex()!=0){
						productsOnChange();
					}
				}
				
				/**************************************************************************************/
				
				if(event.getSource().equals(form.getAcshippingcomposite().getState())){
					if(!checkCompanyState()&&form.getAcshippingcomposite().getState().getSelectedIndex()!=0)
					{
						form.getCbcformlis().setEnabled(true);
					}
					if(checkCompanyState()&&form.getAcshippingcomposite().getState().getSelectedIndex()!=0){
						form.getCbcformlis().setEnabled(false);
					}
					if(form.getAcshippingcomposite().getState().getSelectedIndex()==0){
						form.getCbcformlis().setEnabled(false);
						form.getOlbcstpercent().setEnabled(false);
						form.getCbcformlis().setSelectedIndex(0);
						form.getOlbcstpercent().setValue(null);
					}
				}
				
				/**
				 * Date 14 july 2017 added by vijay for Quick Sales Order
				 */
				
				if(event.getSource().equals(form.dbpaymentrecieved)){
					System.out.println("you are here on change");
					
					System.out.println(" net payment ==== "+form.donetpayamt.getValue());
					System.out.println("hi recievd == "+form.dbpaymentrecieved.getValue());
					System.out.println("balance =="+form.dbbalancepayment.getValue());
					if(form.dbpaymentrecieved.getValue()==null){
						form.dbpaymentrecieved.setValue(0d);
						form.dbbalancepayment.setValue(form.donetpayamt.getValue());
					}
						
					
					boolean valid =	validation(form.donetpayamt.getValue(),form.dbpaymentrecieved.getValue());
					if(valid == false){
						form.showDialogMessage("Payment recieved amount not greater than net payable amount");
						form.dbpaymentrecieved.setText(null);
						form.dbbalancepayment.setValue(form.donetpayamt.getValue());
					}
					if(valid){
						form.dbbalancepayment.setValue(form.donetpayamt.getValue()-form.dbpaymentrecieved.getValue());
						System.out.println(" if chnged value then == "+ form.dbbalancepayment.getValue());
					}
					
				}
						
				if(event.getSource().equals(form.getTbFullName())){
					System.out.println("hi vijay");
					validateCustomer();
				}
				
				if(event.getSource().equals(form.getTbCompanyName())){
					validateClientName();
				}
				
				/**
				 * ends here
				 */
				
			}
				
			private boolean validation(Double netpayable, Double recievedamt) {
				if(recievedamt>netpayable){
					return false;
			    }
			  return true;
		    }
			
			private void validateClientName() {


				if(form.getTbCompanyName().getValue().trim()!=null){
					final MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter temp=null;
					
					temp=new Filter();
					temp.setQuerryString("isCompany");
					temp.setBooleanvalue(true);
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("companyName");
					temp.setStringValue(form.getTbCompanyName().getValue().toUpperCase().trim());
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("companyId");
					temp.setLongValue(model.getCompanyId());
					filtervec.add(temp);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());
					genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
								
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								System.out.println("Success"+result.size());
								if(result.size()!=0){
//									clientflag=false;
									form.showDialogMessage("Company Name Already Exists!");
									form.tbCompanyName.setValue("");
								}
								if(result.size()==0){
//									clientflag=true;
								}
							}
							
						});
				}

			
			}

			private void validateCustomer() {

				

//				if(!form.getTbFirstName().getValue().equals("")&&!form.getTbLastName().getValue().equals("")){
					if(!form.getTbFullName().getValue().equals("")){
					String custnameval=form.getTbFullName().getValue();
					
					custnameval=custnameval.toUpperCase().trim();
					if(custnameval!=null){

							final MyQuerry querry=new MyQuerry();
							Vector<Filter> filtervec=new Vector<Filter>();
							Filter temp=null;
							
							temp=new Filter();
							temp.setQuerryString("isCompany");
							if(!form.getTbCompanyName().getValue().equals("")){
								System.out.println(" value =="+form.getTbCompanyName().getValue());
								temp.setBooleanvalue(true);
							}
							if(form.getTbCompanyName().getValue().equals("")){
								temp.setBooleanvalue(false);
								System.out.println("valueeeee =="+form.getTbCompanyName().getValue());
							}
							filtervec.add(temp);
							
							temp=new Filter();
							temp.setQuerryString("companyId");
							temp.setLongValue(model.getCompanyId());
							filtervec.add(temp);
							
							temp=new Filter();
							temp.setQuerryString("fullname");
							temp.setStringValue(custnameval);
							filtervec.add(temp);
							
							querry.setFilters(filtervec);
							querry.setQuerryObject(new Customer());
							genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
										
									}
									@Override
									public void onSuccess(ArrayList<SuperModel> result) {
										System.out.println("Success"+result.size());
										if(result.size()!=0){
											form.showDialogMessage("Customer Name Already Exists");
											form.tbFullName.setValue("");
											
										}
									}
								});
					}
				}
			
			}

			
			
			/**
			 *   This method returns the name of state in which the company is located.
			 *   It is retrieved form Company address in Company Entity
			 * @return  It returns company state
			 */
			
			public String retrieveCompanyState()
			{
				final MyQuerry querry=new MyQuerry();
				Filter tempfilter=new Filter();
				tempfilter.setQuerryString("companyId");
				tempfilter.setLongValue(model.getCompanyId());
				querry.getFilters().add(tempfilter);
				querry.setQuerryObject(new Company());
				Timer timer=new Timer() 
			    {
					@Override
					public void run() 
					{
					genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected error occurred!");
						}
			
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							for(SuperModel model:result)
							{
								Company compentity = (Company)model;
								retrCompnyState=compentity.getAddress().getState();
								System.out.println("Retrieved "+retrCompnyState);
							}
							companyStateVal=retrCompnyState.trim();
							System.out.println("sysouStatc"+companyStateVal);
							}
						 });
					}
		    	 };
		    	 timer.schedule(3000);
		    	 
		    	return retrCompnyState;
			}

			/**
			 * This method is used for verifying whether the name of the state where delivery is to be done
			 * is same as that of company state.
			 * @return it returns true if company state is same as that of customer and false if it is different.
			 */
			public boolean checkCompanyState()
			{
				//String companyState=retrieveCompanyState().trim();
				System.out.println("Company State"+retrCompnyState);
				
				String custState="";
				if(form.getAcshippingcomposite().getState().getSelectedIndex()!=0){
					custState=form.getAcshippingcomposite().getState().getValue().trim();
					System.out.println("Customer State"+custState);
				}
				if(!custState.equals("")&&custState.trim().equals(retrCompnyState.trim())&&!retrCompnyState.equals("")){
					return true;
				}
				return false;
			}

			protected void clearProductsAndAddress()
			{
				form.getProdTaxTable().connectToLocal();
				form.getChargesTable().connectToLocal();
				form.getSalesorderlineitemtable().connectToLocal();
				form.getDoamtincltax().setValue(null);
				form.getDonetpayamt().setValue(null);
				form.getDototalamt().setValue(null);
				form.getCbcformlis().setSelectedIndex(0);
				form.getOlbcstpercent().setSelectedIndex(0);
				form.getCbcformlis().setEnabled(false);
				form.getOlbcstpercent().setEnabled(false);
				form.getAcshippingcomposite().setEnable(true);
				form.getAcshippingcomposite().getAdressline1().setValue(null);
				form.getAcshippingcomposite().getAdressline2().setValue(null);
				form.getAcshippingcomposite().getLocality().setSelectedIndex(0);
				form.getAcshippingcomposite().getLandMark().setText(null);
				form.getAcshippingcomposite().getPin().setText(null);
				form.getAcshippingcomposite().getCity().setSelectedIndex(0);
				form.getAcshippingcomposite().getCountry().setSelectedIndex(0);
				form.getAcshippingcomposite().getState().setSelectedIndex(0);
			}
			
			
			

			private void chequeForDeliveryNoteStatus(){
				final int salesOrderId=Integer.parseInt(form.getTbContractId().getValue().trim());
				final String salesOrderStatus=form.getTbQuotationStatus().getValue().trim();
				
				MyQuerry querry = new MyQuerry();
			  	Company c = new Company();
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
			  	filter = new Filter();
			  	filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("salesOrderCount");
				filter.setIntValue(salesOrderId);
				filtervec.add(filter);
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new DeliveryNote());
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						ArrayList<DeliveryNote> delnotelist=new ArrayList<DeliveryNote>();
						int pic=0;
						int request=0;
						for(SuperModel model:result){
							
							DeliveryNote deliveryNoteEntity=(DeliveryNote)model;
							deliveryNoteEntity.setStatus(deliveryNoteEntity.getStatus());
							deliveryNoteEntity.setCount(deliveryNoteEntity.getCount());
							delnotelist.add(deliveryNoteEntity);
							
							CancellationSummary table = new CancellationSummary();
							table.setDocID(deliveryNoteEntity.getCount());
							table.setDocType(AppConstants.APPROVALDELIVERYNOTE);
							table.setRemark(cancelpopup.getRemark().getValue().trim());
							table.setLoggedInUser(LoginPresenter.loggedInUser);
							table.setOldStatus(deliveryNoteEntity.getStatus());
							table.setNewStatus(DeliveryNote.CANCELLED);
							table.setSrNo(1);
							cancellis.add(table);
						}
						
						for(int i=0;i<delnotelist.size();i++){
						
						if(delnotelist.get(i).getStatus().equals(DeliveryNote.APPROVED))
						{
							form.showDialogMessage("Delivery note status is approved. Cancel delivery note first ");
						}
						else if(delnotelist.get(i).getStatus().equals(DeliveryNote.DELIVERYCOMPLETED))
						{
							form.showDialogMessage("Delivery note status is Completed. Can't Proceed");
						}
						
						else if(delnotelist.get(i).getStatus().equals(DeliveryNote.CREATED)||delnotelist.get(i).getStatus().equals(DeliveryNote.REJECTED))
						{
							pic=pic+1;
						}	
						else if(delnotelist.get(i).getStatus().equals(DeliveryNote.REQUESTED))
						{
							request=request+1;
						}
						
						}
							if((pic>0)||(request>0)){
								if(request>0)
								{
									for(int i=0;i<request;i++){
										
										MyQuerry querry = new MyQuerry();
									  	Company c = new Company();
									  	Vector<Filter> filtervec=new Vector<Filter>();
									  	Filter filter = null;
									  	filter = new Filter();
									  	filter.setQuerryString("companyId");
										filter.setLongValue(c.getCompanyId());
										filtervec.add(filter);
										filter = new Filter();
										filter.setQuerryString("businessprocessId");
										filter.setIntValue(delnotelist.get(i).getCount());
										filtervec.add(filter);
										filter = new Filter();
										filter.setQuerryString("businessprocesstype");
										filter.setStringValue(ApproverFactory.DELIVERYNOTE);
										filtervec.add(filter);
										querry.setFilters(filtervec);
										querry.setQuerryObject(new Approvals());
										genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
								  			
								  			@Override
								  			public void onFailure(Throwable caught) {
								  			}

								  			@Override
											public void onSuccess(ArrayList<SuperModel> result) {
								  				
								  				for(SuperModel smodel:result)
												{
								  					Approvals approval = (Approvals)smodel;
								  					
								  					approval.setStatus(Approvals.CANCELLED);
								  					approval.setRemark("Sales Order Id ="+salesOrderId+" "+"Sales Order status ="+salesOrderStatus+"\n"+
												   "has been cancelled by "+LoginPresenter.loggedInUser+" with remark"+"\n"+"Remark ="+cancelpopup.getRemark().getValue().trim());
													
													genasync.save(approval,new AsyncCallback<ReturnFromServer>() {
														
														@Override
														public void onFailure(Throwable caught) {
															form.showDialogMessage("An Unexpected Error occured !");
														}
														@Override
														public void onSuccess(ReturnFromServer result) {
														}
													});
												}
								  			}
								  			});
									}
								}
						
							//****************************************************
							System.out.println("inside Cheque For Billing Details Status");
							MyQuerry querry = new MyQuerry();
						  	Company c = new Company();
						  	Vector<Filter> filtervec=new Vector<Filter>();
						  	Filter filter = null;
						  	filter = new Filter();
						  	filter.setQuerryString("companyId");
							filter.setLongValue(c.getCompanyId());
							filtervec.add(filter);
							filter = new Filter();
							filter.setQuerryString("contractCount");
							filter.setIntValue(salesOrderId);
							filtervec.add(filter);
							filter = new Filter();
							filter.setQuerryString("typeOfOrder");
							filter.setStringValue(AppConstants.ORDERTYPEFORSALES);
							filtervec.add(filter);
							querry.setFilters(filtervec);
							querry.setQuerryObject(new BillingDocument());
							
							genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
								
								@Override
								public void onFailure(Throwable caught) {
								}

								@Override
								public void onSuccess(ArrayList<SuperModel> result) {
									int req=0;
									
									ArrayList<BillingDocument> slist=new ArrayList<BillingDocument>();
									
									for(SuperModel model:result)
									{
										BillingDocument billingEntity = (BillingDocument)model;
										billingEntity.setStatus(billingEntity.getStatus());
									
										slist.add(billingEntity);
										
										if(BillingDocument.BILLINGINVOICED.equals(billingEntity.getStatus().trim()))
										{
											CancellationSummary table1 = new CancellationSummary();
											
											table1.setDocID(billingEntity.getCount());
											table1.setDocType(AppConstants.APPROVALBILLINGDETAILS);
											table1.setRemark(cancelpopup.getRemark().getValue().trim());
											table1.setLoggedInUser(LoginPresenter.loggedInUser);
											table1.setOldStatus(billingEntity.getStatus());
											table1.setNewStatus(BillingDocument.BILLINGINVOICED);
											table1.setSrNo(2);
											
											cancellis.add(table1);
										}
										else
										{
											CancellationSummary table1 = new CancellationSummary();
											
											table1.setDocID(billingEntity.getCount());
											table1.setDocType(AppConstants.APPROVALBILLINGDETAILS);
											table1.setRemark(cancelpopup.getRemark().getValue().trim());
											table1.setLoggedInUser(LoginPresenter.loggedInUser);
											table1.setOldStatus(billingEntity.getStatus());
											table1.setNewStatus(BillingDocument.CANCELLED);
											table1.setSrNo(2);
											
											cancellis.add(table1);
										}
									}
									
									
									int cnt=0;
									int flag=0;
									for(int i=0;i<slist.size();i++){
										
										System.out.println("forloop runs for times ===="+i+1);
										if(slist.get(i).getStatus().equals(BillingDocument.APPROVED))
										{
											flag=flag+1;
										}
										else if(slist.get(i).getStatus().equals(BillingDocument.BILLINGINVOICED))
										{
											cnt=cnt+1;
										}
										else if(slist.get(i).getStatus().equals(BillingDocument.CREATED))
										{
											flag=flag+1;
										}
										else if(slist.get(i).getStatus().equals(BillingDocument.REJECTED))
										{
											flag=flag+1;
										}
										else if(slist.get(i).getStatus().equals(BillingDocument.REQUESTED))
										{
											req=req+1;
										}
									}	
									
									if(cnt>0){
										
										//**************************************************************
										MyQuerry querry = new MyQuerry();
									  	Company c = new Company();
									  	Vector<Filter> filtervec=new Vector<Filter>();
									  	Filter filter = null;
									  	filter = new Filter();
									  	filter.setQuerryString("companyId");
										filter.setLongValue(c.getCompanyId());
										filtervec.add(filter);
										filter = new Filter();
										filter.setQuerryString("contractCount");
										filter.setIntValue(salesOrderId);
										filtervec.add(filter);
										filter = new Filter();
										filter.setQuerryString("typeOfOrder");
										filter.setStringValue(AppConstants.ORDERTYPEFORSALES);
										filtervec.add(filter);
										querry.setFilters(filtervec);
										querry.setQuerryObject(new Invoice());
										genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
								  			
								  			@Override
								  			public void onFailure(Throwable caught) {
								  			}

								  			@Override
											public void onSuccess(ArrayList<SuperModel> result) {
								  				int cnt=0;
								  				int requested=0;
								  				ArrayList<Invoice> slist=new ArrayList<Invoice>();
								  				System.out.println("Result size is ===="+result.size());

								  				for(SuperModel model:result)
												{
								  					Invoice invoiceEntity= (Invoice)model;
													
								  					invoiceEntity.setStatus(invoiceEntity.getStatus());
													System.out.println("invoice count===="+ invoiceEntity.getCount());
													invoiceEntity.setContractCount(invoiceEntity.getContractCount());
													invoiceEntity.setInvoiceCount(invoiceEntity.getCount());
													invoiceEntity.setStatus(invoiceEntity.getStatus());
													slist.add(invoiceEntity);
													
													if(Invoice.APPROVED.equals(invoiceEntity.getStatus().trim()))
													{
														CancellationSummary table2 = new CancellationSummary();
														table2.setDocID(invoiceEntity.getCount());
														table2.setDocType(AppConstants.APPROVALINVOICEDETAILS);
														table2.setRemark(cancelpopup.getRemark().getValue().trim());
														table2.setLoggedInUser(LoginPresenter.loggedInUser);
														table2.setOldStatus(invoiceEntity.getStatus());
														table2.setNewStatus(Invoice.APPROVED);
														table2.setSrNo(3);
														
														cancellis.add(table2);
													}
													else
													{
														CancellationSummary table2 = new CancellationSummary();
														table2.setDocID(invoiceEntity.getCount());
														table2.setDocType(AppConstants.APPROVALINVOICEDETAILS);
														table2.setRemark(cancelpopup.getRemark().getValue().trim());
														table2.setLoggedInUser(LoginPresenter.loggedInUser);
														table2.setOldStatus(invoiceEntity.getStatus());
														table2.setNewStatus(Invoice.CANCELLED);
														table2.setSrNo(3);
														
														cancellis.add(table2);
													}
												}
								  				
								  				
								  				for(int i=0;i<slist.size();i++)
								  				{
													if(salesOrderId==slist.get(i).getContractCount());
													{
														 if(slist.get(i).getStatus().equals(Invoice.CREATED))
														{
															System.out.println("slist.get(i).getStatus().equals in create===="+slist.get(i).getStatus());
															
														}
														 else if(slist.get(i).getStatus().equals(Invoice.APPROVED))
														{
															cnt=cnt+1;
															System.out.println("slist.get(i).getStatus().equals in Approved ==="+slist.get(i).getStatus());
														}
														 else if(slist.get(i).getStatus().equals(Invoice.REJECTED))
															{
																System.out.println("slist.get(i).getStatus().equals in rejected ==="+slist.get(i).getStatus());
															}
														 else if(slist.get(i).getStatus().equals(Invoice.REQUESTED))
															{
																requested=requested+1;
																System.out.println("slist.get(i).getStatus().equals in requested ==="+slist.get(i).getStatus());
															}
														 
													}
												} 
												System.out.println("count value===="+cnt);
												if(cnt>0){
										
										MyQuerry querry = new MyQuerry();
									  	Company c = new Company();
									  	Vector<Filter> filtervec=new Vector<Filter>();
									  	Filter filter = null;
									  	filter = new Filter();
									  	filter.setQuerryString("companyId");
										filter.setLongValue(c.getCompanyId());
										filtervec.add(filter);
										filter = new Filter();
										filter.setQuerryString("contractCount");
										filter.setIntValue(salesOrderId);
										filtervec.add(filter);
										querry.setFilters(filtervec);
										querry.setQuerryObject(new CustomerPayment());
										genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
								  			
								  			@Override
								  			public void onFailure(Throwable caught) {
								  			}

								  			@Override
											public void onSuccess(ArrayList<SuperModel> result) {
								  				int cnt=0;
								  			
								  				ArrayList<CustomerPayment> slist=new ArrayList<CustomerPayment>();
								  				
								  				System.out.println("Result size is ===="+result.size());
												for(SuperModel model:result)
												{
													CustomerPayment custPayEntity= (CustomerPayment)model;
													
													custPayEntity.setStatus(custPayEntity.getStatus());
													System.out.println("invoice count===="+ custPayEntity.getInvoiceCount());
													custPayEntity.setInvoiceCount(custPayEntity.getInvoiceCount());
													custPayEntity.setContractCount(custPayEntity.getContractCount());
													slist.add(custPayEntity);
													
													if(custPayEntity.getStatus().equals(CustomerPayment.PAYMENTCLOSED)){
													CancellationSummary table3= new CancellationSummary();
													
													table3.setDocID(custPayEntity.getCount());
													table3.setDocType("Payment Details");
													table3.setRemark(cancelpopup.getRemark().getValue().trim());
													table3.setLoggedInUser(LoginPresenter.loggedInUser);
													table3.setOldStatus(custPayEntity.getStatus());
													table3.setNewStatus(CustomerPayment.PAYMENTCLOSED);
													table3.setSrNo(4);
													cancellis.add(table3);
													}
													else{
														
														CancellationSummary table3= new CancellationSummary();
														
														table3.setDocID(custPayEntity.getCount());
														table3.setDocType("Payment Details");
														table3.setRemark(cancelpopup.getRemark().getValue().trim());
														table3.setLoggedInUser(LoginPresenter.loggedInUser);
														table3.setOldStatus(custPayEntity.getStatus());
														table3.setNewStatus(Invoice.CANCELLED);
														table3.setSrNo(4);
														cancellis.add(table3);
													}
													}
									
												
												for(int i=0;i<slist.size();i++){
													
													
												if(salesOrderId==slist.get(i).getContractCount());
												{
													 if(slist.get(i).getStatus().equals(CustomerPayment.CREATED))
													{
														System.out.println("slist.get(i).getStatus().equals in create===="+slist.get(i).getStatus());
														cnt=cnt+1;
													}
													
													 else if(slist.get(i).getStatus().equals(CustomerPayment.PAYMENTCLOSED))
													{
														
														System.out.println("slist.get(i).getStatus().equals in closed ==="+slist.get(i).getStatus());
													}
												}
												}
												
												if(cnt>0){
													
													cancleDeliveryNote();
													cancleBillingDocument();
													cancelInvoiceDetails();
													canclePaymentDetails();
													createNewSalesOrder();
													
												}
												else
												{
													cancleDeliveryNote();
													cancleBillingDocument();
													cancelInvoiceDetails();
													canclePaymentDetails();
													createNewSalesOrder();
												}
								  			}
										});
												}
												else
												{
													if(requested>0)
													{
													createNewSalesOrder();
													cancleDeliveryNote();
													cancleBillingDocument();
													cancelInvoiceDetails();
													
													}
													else
													{
													createNewSalesOrder();
													cancleDeliveryNote();
													cancleBillingDocument();
													cancelInvoiceDetails();
													
													}													
												}
								  			}
										});
										//**************************************************************
									}
									else {
									
									
									if(flag>0||req>0)
									{	
										createNewSalesOrder();
										cancleDeliveryNote();
										cancleBillingDocument();
										
									}
									
									}
								}
							});
						}
					}
				});
				
			}
			
			
			private void cancleDeliveryNote(){
				
				final int salesOrderId=Integer.parseInt(form.getTbContractId().getValue().trim());
				final String salesOrderStatus=form.getstatustextbox().getValue().trim();
				
				
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
		
			filter = new Filter();
			filter.setQuerryString("salesOrderCount");
			filter.setIntValue(salesOrderId);
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new DeliveryNote());
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					for(final SuperModel smodel:result)
					{
						DeliveryNote delNote = (DeliveryNote)smodel;
						
						if(delNote.getStatus().equals(DeliveryNote.CREATED))
	  					{
						delNote.setStatus(ConcreteBusinessProcess.CANCELLED);
						delNote.setDescription("Sales Order Id ="+salesOrderId+" "+"Sales Order status ="+salesOrderStatus+"\n"+
					   "has been cancelled by "+LoginPresenter.loggedInUser+" with remark"+"\n"+"Remark ="+cancelpopup.getRemark().getValue().trim());
						
						genasync.save(delNote,new AsyncCallback<ReturnFromServer>() {
							
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								
							}
						});
	  					}
						
						if(delNote.getStatus().equals(DeliveryNote.REJECTED))
	  					{
						delNote.setStatus(ConcreteBusinessProcess.CANCELLED);
						delNote.setDescription("Sales Order Id ="+salesOrderId+" "+"Sales Order status ="+salesOrderStatus+"\n"+
					   "has been cancelled by "+LoginPresenter.loggedInUser+" with remark"+"\n"+"Remark ="+cancelpopup.getRemark().getValue().trim());
						
						genasync.save(delNote,new AsyncCallback<ReturnFromServer>() {
							
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								
							
							}
						});
	  					}
						
						if(delNote.getStatus().equals(DeliveryNote.REQUESTED))
	  					{
						delNote.setStatus(ConcreteBusinessProcess.CANCELLED);
						delNote.setDescription("Sales Order Id ="+salesOrderId+" "+"Sales Order status ="+salesOrderStatus+"\n"+
					   "has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelpopup.getRemark().getValue().trim());
						
						genasync.save(delNote,new AsyncCallback<ReturnFromServer>() {
							
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								
//								CancellationSummary table = new CancellationSummary();
//								
//								table.setDocID(smodel.getCount());
//								table.setDocType(AppConstants.APPROVALDELIVERYNOTE);
//								table.setRemark(popup.getRemark().getValue().trim());
//								table.setLoggedInUser(LoginPresenter.loggedInUser);
//								table.setOldStatus(DeliveryNote.REQUESTED);
//								table.setNewStatus(model.getStatus());
//								table.setDocDate(new Date());
//								table.setSrNo(1);
//								
//								cancellis.add(table);
							
							}
						});
	  					}
				}
				}
			});
			}
			
			
			
			
			private void cancleBillingDocument(){
				final int salesOrderId=Integer.parseInt(form.getTbContractId().getValue().trim());
				final String salesOrderStatus=form.getstatustextbox().getValue().trim();
				
				System.out.println("in side cancle billind document ");
				
				MyQuerry querry = new MyQuerry();
			  	Company c = new Company();
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
			  	filter = new Filter();
			  	filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("contractCount");
				filter.setIntValue(salesOrderId);
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("typeOfOrder");
				filter.setStringValue(AppConstants.ORDERTYPEFORSALES);
				filtervec.add(filter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new BillingDocument());
				System.out.println("querry completed......");
				
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		  			
		  			@Override
		  			public void onFailure(Throwable caught) {
		  			}

		  			@Override
					public void onSuccess(ArrayList<SuperModel> result) {
		  				System.out.println("in side cancle billind document result set is=="+result.size());
		  				for(SuperModel smodel:result)
						{
		  					BillingDocument bill = (BillingDocument)smodel;
		  					
		  					if(bill.getStatus().equals(BillingDocument.CREATED)){
			  					
		  						bill.setStatus(BillingDocument.CANCELLED);
		  						bill.setComment("Sales Order Id ="+salesOrderId+" "+"Sales Order status ="+salesOrderStatus+"\n"+
								"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelpopup.getRemark().getValue().trim());
		  						genasync.save(bill,new AsyncCallback<ReturnFromServer>() {
										
										@Override
										public void onFailure(Throwable caught) {
											form.showDialogMessage("An Unexpected Error occured !");
//											
										}
										@Override
										public void onSuccess(ReturnFromServer result) {
											
										}
									});
						}
		  					
		  					if(bill.getStatus().equals(BillingDocument.APPROVED)){
			  					
		  						bill.setStatus(BillingDocument.CANCELLED);
		  						bill.setComment("Sales Order Id ="+salesOrderId+" "+"Sales Order status ="+salesOrderStatus+"\n"+
								"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelpopup.getRemark().getValue().trim());
		  						genasync.save(bill,new AsyncCallback<ReturnFromServer>() {
										
										@Override
										public void onFailure(Throwable caught) {
											form.showDialogMessage("An Unexpected Error occured !");
//											
										}
										@Override
										public void onSuccess(ReturnFromServer result) {
										}
									});
						}
		  					
		  					if(bill.getStatus().equals(BillingDocument.BILLINGINVOICED)){
			  					bill.setRenewFlag(true);
		  						bill.setStatus(BillingDocument.BILLINGINVOICED);
		  						bill.setComment("Sales Order Id ="+salesOrderId+" "+"Sales Order status ="+salesOrderStatus+"\n"+
								"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelpopup.getRemark().getValue().trim());
		  						genasync.save(bill,new AsyncCallback<ReturnFromServer>() {
										
										@Override
										public void onFailure(Throwable caught) {
											form.showDialogMessage("An Unexpected Error occured !");
//											
										}
										@Override
										public void onSuccess(ReturnFromServer result) {
										}
							});
						}
		  					
		  					if(bill.getStatus().equals(BillingDocument.REJECTED)){
			  					
		  						bill.setStatus(BillingDocument.CANCELLED);
		  						bill.setComment("Sales Order Id ="+salesOrderId+" "+"Sales Order status ="+salesOrderStatus+"\n"+
								"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelpopup.getRemark().getValue().trim());
		  						genasync.save(bill,new AsyncCallback<ReturnFromServer>() {
										
										@Override
										public void onFailure(Throwable caught) {
											form.showDialogMessage("An Unexpected Error occured !");
										}
										@Override
										public void onSuccess(ReturnFromServer result) {
											
										}
							});
						}	
		  					
		  					
		  					if(bill.getStatus().equals(BillingDocument.REQUESTED)){
			  					
		  						bill.setStatus(BillingDocument.CANCELLED);
		  						bill.setComment("Sales Order Id ="+salesOrderId+" "+"Sales Order status ="+salesOrderStatus+"\n"+
								"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelpopup.getRemark().getValue().trim());
		  						genasync.save(bill,new AsyncCallback<ReturnFromServer>() {
										
										@Override
										public void onFailure(Throwable caught) {
											form.showDialogMessage("An Unexpected Error occured !");
//											
										}
										@Override
										public void onSuccess(ReturnFromServer result) {
											
										}
							});
		  						
		  						
		  						MyQuerry querry = new MyQuerry();
							  	Company c = new Company();
							  	Vector<Filter> filtervec=new Vector<Filter>();
							  	Filter filter = null;
							  	filter = new Filter();
							  	filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocessId");
								filter.setIntValue(bill.getCount());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocesstype");
								filter.setStringValue(ApproverFactory.BILLINGDETAILS);
								filtervec.add(filter);
								querry.setFilters(filtervec);
								querry.setQuerryObject(new Approvals());
								System.out.println("querry completed......");
								genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						  			
						  			@Override
						  			public void onFailure(Throwable caught) {
						  			}

						  			@Override
									public void onSuccess(ArrayList<SuperModel> result) {
						  				
						  				for(SuperModel smodel:result)
										{
						  					Approvals approval = (Approvals)smodel;
						  					
						  					approval.setStatus(Approvals.CANCELLED);
						  					approval.setRemark("Sales Order Id ="+salesOrderId+" "+"Sales Order status ="+salesOrderStatus+"\n"+
										   "has been cancelled by "+LoginPresenter.loggedInUser+" with remark"+"\n"+"Remark ="+cancelpopup.getRemark().getValue().trim());
											
											genasync.save(approval,new AsyncCallback<ReturnFromServer>() {
												
												@Override
												public void onFailure(Throwable caught) {
													form.showDialogMessage("An Unexpected Error occured !");
												}
												@Override
												public void onSuccess(ReturnFromServer result) {
												
												}
											});
						  					
						  				
										}
						  			}
						  			});
						}	
		  					
		  			}
		  			}
				});
			}
			
			private void createNewSalesOrder() {
				
				int salesOrderId=Integer.parseInt(form.getTbContractId().getValue().trim());
				String salesOrderStatus=form.getstatustextbox().getValue().trim();
				
				  model.setStatus(SalesOrder.CANCELLED);
				  model.setDescription(model.getDescription()+"\n"+"Sales Order Id ="+salesOrderId+" "+"Sales Order Status ="+
						  salesOrderStatus+"\n"+
					"has been cancelled by "+LoginPresenter.loggedInUser+" With remark."+"\n"
				  +"Remark ="+cancelpopup.getRemark().getValue().trim());
		    	
				  genasync.save(model,new AsyncCallback<ReturnFromServer>() {
						
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
//							
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
							
							CancellationSummary table5 = new CancellationSummary();
							
							table5.setDocID(model.getCount());
							table5.setDocType(AppConstants.APPROVALSALESORDER);
							table5.setRemark(cancelpopup.getRemark().getValue().trim());
							table5.setLoggedInUser(LoginPresenter.loggedInUser);
							table5.setOldStatus(SalesOrder.APPROVED);
							table5.setNewStatus(model.getStatus());
							table5.setSrNo(5);
							
							  cancellis.add(table5);
							  CancellationSummaryPopUpTable popupTable=new CancellationSummaryPopUpTable();
							  popupTable.setValue(cancellis);
							  
							  List<CancellationSummary> cancellistsetting=new ArrayList<CancellationSummary>();
							  Date canceldate=null;
							  for(int i=0;i<cancellis.size();i++){
								  CancellationSummary sum= new CancellationSummary();
								  sum.setSrNo(i+1);
								  sum.setDocID(cancellis.get(i).getDocID());
								  sum.setDocType(cancellis.get(i).getDocType());
								  sum.setLoggedInUser(cancellis.get(i).getLoggedInUser());
								  sum.setOldStatus(cancellis.get(i).getOldStatus());
								  sum.setNewStatus(cancellis.get(i).getNewStatus());
								  sum.setRemark(cancellis.get(i).getRemark());
								  cancellistsetting.add(sum);
							  }
							  
							  summaryPanel=new PopupPanel(true);
							  summaryPanel.add(summaryPopup);
							  summaryPanel.show();
							  summaryPanel.center();
							  summaryPopup.setSummaryTable(cancellistsetting);
							  
							  System.out.println("Summary Pop Up");
							  CancelSummary cancelSummaryEntity=new CancelSummary();
							  cancelSummaryEntity.setDocID(model.getCount());
							  cancelSummaryEntity.setDocType(AppConstants.APPROVALSALESORDER);
							  cancelSummaryEntity.setCancelLis(cancellistsetting);
							
							 
							  try
							  {
								  genasync.save(cancelSummaryEntity,new AsyncCallback<ReturnFromServer>() {
										
										@Override
										public void onFailure(Throwable caught) {
											form.showDialogMessage("An Unexpected Error occured for saving Summary !");
										}
										@Override
										public void onSuccess(ReturnFromServer result) {
										
										}
									});
							  }
							  catch(Exception e)
							  {
								  e.printStackTrace();
							  }
							 
								
							form.getTbQuotationStatus().setValue(model.getStatus());
							form.getTaDescription().setValue(model.getDescription());
							form.toggleProcessLevelMenu();
								
						}
					});
				
//				  form.showDialogMessage("Sales order cancelled...........!!!!");
				  
				 
			}
			
			private void cancelInvoiceDetails()
			{
				
				final int salesOrderId=Integer.parseInt(form.getTbContractId().getValue().trim());
				final String salesOrderStatus=form.getstatustextbox().getValue().trim();
	
	
				MyQuerry querry = new MyQuerry();
			  	Company c = new Company();
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
			  	filter = new Filter();
			  	filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("contractCount");
				filter.setIntValue(salesOrderId);
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("typeOfOrder");
				filter.setStringValue(AppConstants.ORDERTYPESALES);
				filtervec.add(filter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Invoice());
				System.out.println("querry completed......");
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		  			
		  			@Override
		  			public void onFailure(Throwable caught) {
		  			}

		  			@Override
					public void onSuccess(ArrayList<SuperModel> result) {
		  				
		  				for(SuperModel smodel:result)
							{
		  					//**************inset login user name in comment *******************
								
		  					final Invoice invoice = (Invoice)smodel;
		  					if(invoice.getStatus().equals(Invoice.CREATED))
		  					{
		  						invoice.setStatus(Invoice.CANCELLED);
		  						invoice.setComment("Sales Order Id ="+salesOrderId+" "+"Sales Order Status ="+salesOrderStatus+"\n"
								+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelpopup.getRemark().getValue());
		  						genasync.save(invoice,new AsyncCallback<ReturnFromServer>() {
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
										
									}
								});
							}
		  					
		  					if(invoice.getStatus().equals(Invoice.APPROVED))
		  					{
		  						invoice.setRenewFlag(true);
		  						invoice.setStatus(Invoice.APPROVED);
		  						invoice.setComment("Sales Order Id ="+salesOrderId+" "+"Sales Order Status ="+salesOrderStatus+"\n"
								+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelpopup.getRemark().getValue());
		  						genasync.save(invoice,new AsyncCallback<ReturnFromServer>() {
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
									}
								});
							}
		  					if(invoice.getStatus().equals(Invoice.REJECTED))
		  					{
		  						invoice.setStatus(Invoice.CANCELLED);
		  						invoice.setComment("Sales Order Id ="+salesOrderId+" "+"Sales Order Status ="+salesOrderStatus+"\n"
								+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelpopup.getRemark().getValue());
		  						genasync.save(invoice,new AsyncCallback<ReturnFromServer>() {
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
										
										
									}
								});
							}
		  					
		  					if(invoice.getStatus().equals(Invoice.REQUESTED))
		  					{
		  						invoice.setStatus(Invoice.CANCELLED);
		  						invoice.setComment("Sales Order Id ="+salesOrderId+" "+"Sales Order Status ="+salesOrderStatus+"\n"
								+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelpopup.getRemark().getValue());
		  						genasync.save(invoice,new AsyncCallback<ReturnFromServer>() {
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
										
									}
								});
		  						
		  						MyQuerry querry = new MyQuerry();
							  	Company c = new Company();
							  	Vector<Filter> filtervec=new Vector<Filter>();
							  	Filter filter = null;
							  	filter = new Filter();
							  	filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocessId");
								filter.setIntValue(invoice.getCount());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocesstype");
								filter.setStringValue(ApproverFactory.INVOICEDETAILS);
								filtervec.add(filter);
								querry.setFilters(filtervec);
								querry.setQuerryObject(new Approvals());
								System.out.println("querry completed......");
								genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						  			
						  			@Override
						  			public void onFailure(Throwable caught) {
						  			}

						  			@Override
									public void onSuccess(ArrayList<SuperModel> result) {
						  				
						  				for(SuperModel smodel:result)
										{
						  					Approvals approval = (Approvals)smodel;
						  					
						  					approval.setStatus(Approvals.CANCELLED);
						  					approval.setRemark("Sales Order Id ="+salesOrderId+" "+"Sales Order Status ="+salesOrderStatus+"\n"+
										   "has been cancelled by "+LoginPresenter.loggedInUser+" with remark  "+"\n"+"Remark ="+cancelpopup.getRemark().getValue().trim());
											
											genasync.save(approval,new AsyncCallback<ReturnFromServer>() {
												
												@Override
												public void onFailure(Throwable caught) {
													form.showDialogMessage("An Unexpected Error occured !");
												}
												@Override
												public void onSuccess(ReturnFromServer result) {
												
												}
											});
						  					
						  				
										}
						  			}
						  			});
							}
							}
		  			}
				});
				
			}
			
			
			private void canclePaymentDetails() {
				
				final int salesOrderId=Integer.parseInt(form.getTbContractId().getValue().trim());
				final String salesOrderStatus=form.getstatustextbox().getValue().trim();
				
				MyQuerry querry = new MyQuerry();
			  	Company c = new Company();
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
			  	filter = new Filter();
			  	filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("contractCount");
				filter.setIntValue(salesOrderId);
				filtervec.add(filter);
				filter = new Filter();
				filter.setQuerryString("typeOfOrder");
				filter.setStringValue(AppConstants.ORDERTYPESALES);
				filtervec.add(filter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new CustomerPayment());
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		  			
		  			@Override
		  			public void onFailure(Throwable caught) {
		  			}

		  			@Override
					public void onSuccess(ArrayList<SuperModel> result) {
		  				
		  				for(SuperModel smodel:result)
							{
		  					//**************inset login user name in comment *******************
								
		  					final CustomerPayment custpay = (CustomerPayment)smodel;
		  					if(custpay.getStatus().equals(CustomerPayment.CREATED))
		  					{
								custpay.setStatus(CustomerPayment.CANCELLED);
								custpay.setComment("Sales Order Id ="+salesOrderId+" "+"Sales Order Status ="+salesOrderStatus+"\n"
								+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelpopup.getRemark().getValue());
								genasync.save(custpay,new AsyncCallback<ReturnFromServer>() {
		//
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
//										
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
										
										
									}
								});
							}
		  					
		  					if(custpay.getStatus().equals(CustomerPayment.PAYMENTCLOSED))
		  					{
		  						custpay.setRenewFlag(true);
								custpay.setStatus(CustomerPayment.PAYMENTCLOSED);
								custpay.setComment("Sales Order Id ="+salesOrderId+" "+"Sales Order Status ="+salesOrderStatus+"\n"
								+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+cancelpopup.getRemark().getValue());
								genasync.save(custpay,new AsyncCallback<ReturnFromServer>() {
		//
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
//										
									}
									@Override
									public void onSuccess(ReturnFromServer result) {
										
										
									}
								});
							}
		  					
							}
		  				
		  			}
				
			
				});
			}
			
			
			private void getSummaryFromSalesOrderID()
			{
				
				System.out.println(Integer.parseInt(form.getTbContractId().getValue().trim()));
				
				MyQuerry querry = new MyQuerry();
			  	Company c = new Company();
			  	Vector<Filter> filtervec=new Vector<Filter>();
			  	Filter filter = null;
			  	
			  	filter = new Filter();
			  	filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("docID");
				filter.setIntValue(Integer.parseInt(form.getTbContractId().getValue().trim()));
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("docType");
				filter.setStringValue(AppConstants.APPROVALSALESORDER);
				filtervec.add(filter);
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new CancelSummary());
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		  			
		  			@Override
		  			public void onFailure(Throwable caught) {
		  			}

		  			@Override
					public void onSuccess(ArrayList<SuperModel> result) {
		  				for(SuperModel smodel:result)
						{
		  					CancelSummary cancelEntity = (CancelSummary)smodel;
			  				summaryPanel=new PopupPanel(true);
						    summaryPanel.add(summaryPopup);
							summaryPanel.show();
							summaryPanel.center();
							summaryPopup.setSummaryTable(cancelEntity.getCancelLis());
		  			}
		  			}	
				});
			}
			
			
			private void reactOnCancelSummaryDownload()
			{
				ArrayList<CancelSummary> cancelsummarylist=new ArrayList<CancelSummary>();
				List<CancellationSummary> cancel= summaryPopup.getSummaryTable().getDataprovider().getList();
				CancelSummary cancelEntity =new CancelSummary();
				cancelEntity.setCancelLis(cancel);
				
				cancelsummarylist.add(cancelEntity);
				csvservice.setCancelSummaryDetails(cancelsummarylist, new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error Occurred!");
					}

					@Override
					public void onSuccess(Void result) {
						String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url=gwt + "csvservlet"+"?type="+82;
						Window.open(url, "test", "enabled");
					}
				});
			}
			
			
			private void reactOnWorkOrder() {
				final WorkOrderForm form = WorkOrderPresenter.initialize();
				final WorkOrder wo = new WorkOrder();
				
				wo.setOrderId(model.getCount());
				wo.setOrderDate(model.getSalesOrderDate());
				if(model.getRefNo()!=null)
					wo.setRefNum(model.getRefNo()+"");
				if(model.getReferenceDate()!=null)
					wo.setRefDate(model.getReferenceDate());
				wo.setOrderDeliveryDate(model.getDeliveryDate());
				wo.setBranch(model.getBranch());
				wo.setSalesPerson(model.getEmployee());
				wo.setApproverName(model.getApproverName());
				
				List<SalesLineItem> productList = model.getItems();
				wo.setBomTable(productList);
				
				
				
				form.showWaitSymbol();
				Timer t = new Timer() {
					@Override
					public void run() {
						form.setToNewState();
						form.updateView(wo);
						form.getIbOrderId().setEnabled(false);
						form.getDbOrderDate().setEnabled(false);
						form.getOblSalesPerson().setEnabled(false);
						form.getOblWoBranch().setEnabled(false);
						form.getOblSalesPerson().setEnabled(false);
						form.getUpload().getCancel().setVisible(false);
						form.getOblWoApproverName().setEnabled(false);
						form.getProdInfoComp().setEnabled(false);
						if(model.getRefNo()!=null)
							form.getTbRefNumId().setEnabled(false);
						if(model.getReferenceDate()!=null)
							form.getDbRefDate().setEnabled(false);
						form.getLbComProdId().clear();
						form.getLbComProdId().addItem("--SELECT--");
						for(int i=0;i<model.getItems().size();i++){
							form.flage=false;
							form.getBillOfProductMaterial(model.getItems().get(i).getPrduct().getCount(), model.getItems().get(i).getProductName(),model.getItems().get(i).getQty(),false);
							form.getLbComProdId().addItem(model.getItems().get(i).getPrduct().getCount()+"");
						}
						form.hideWaitSymbol();
					}
				};
				t.schedule(5000);
			}
			
			
			private void setEmailPopUpData() {
				form.showWaitSymbol();
				String customerEmail = "";

				String branchName = form.olbbBranch.getValue();
				String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
				customerEmail = AppUtility.getCustomerEmailid(model.getCinfo().getCount());
				
				String customerName = model.getCinfo().getFullName();
				if(!customerName.equals("") && !customerEmail.equals("")){
					label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),customerName,customerEmail,null);
					emailpopup.taToEmailId.setValue(customerEmail);

				}
				else{
			
					MyQuerry querry = AppUtility.getcustomerQuerry(model.getCinfo().getCount());
					genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							// TODO Auto-generated method stub
							for(SuperModel smodel : result){
								Customer custEntity  = (Customer) smodel;
								System.out.println("customer Name = =="+custEntity.getFullname());
								System.out.println("Customer Email = == "+custEntity.getEmail());
								
								if(custEntity.getEmail()!=null){
									label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
									emailpopup.taToEmailId.setValue(custEntity.getEmail());

								}
								else{
									label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
								}
								break;
							}
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
						}
					});
				}
				
				Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
				Console.log("screenName "+screenName);
				AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,LoginPresenter.currentModule.trim(),screenName);
				emailpopup.taFromEmailId.setValue(fromEmailId);
				emailpopup.smodel = model;
				form.hideWaitSymbol();
				
			}

}
