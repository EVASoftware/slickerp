package com.slicktechnologies.client.views.quicksalesorder;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class QuickSalesOrderBillingTable  extends SuperTable<BillingDocument>{

	
	TextColumn<BillingDocument> getColumnSalesOrderId;
	TextColumn<BillingDocument> getColumnBillingId;
	TextColumn<BillingDocument> getColumnCustomerName;
	TextColumn<BillingDocument> getColumnNetPayable;
	TextColumn<BillingDocument> getColumnStatus;
	TextColumn<BillingDocument> getColumnBillingDate;
	NumberFormat numberFormat=NumberFormat.getFormat("0.00");
	
	public QuickSalesOrderBillingTable(){
		createGui();
		setHeight("350px");
	}
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		
		addColumnSalesOrderId();
		addColumnSalesOrderBillingId();
		addColumnSalesOrderCustomerName();
		addColumnSalesOrderInvoiceDate();
		addColumnSalesOrderNetPayable();
		addColumnSalesOrderStatus();
	}

	

	private void addColumnSalesOrderInvoiceDate() {

		getColumnBillingDate = new TextColumn<BillingDocument>() {
			
			@Override
			public String getValue(BillingDocument object) {
				return AppUtility.parseDate(object.getBillingDate());
			}
		};
		table.addColumn(getColumnBillingDate,"Billing Date");
		table.setColumnWidth(getColumnBillingDate, 110,Unit.PX);
	}
	
	private void addColumnSalesOrderStatus() {
		getColumnStatus = new TextColumn<BillingDocument>() {
			
			@Override
			public String getValue(BillingDocument object) {
				return object.getStatus();
			}
		};
		table.addColumn(getColumnStatus,"Status");
		table.setColumnWidth(getColumnStatus, 110, Unit.PX);
	}


	private void addColumnSalesOrderId() {

		getColumnSalesOrderId = new TextColumn<BillingDocument>() {
			
			@Override
			public String getValue(BillingDocument object) {

				return object.getContractCount()+"";
			}
		};
		table.addColumn(getColumnSalesOrderId,"Order Id");
		table.setColumnWidth(getColumnSalesOrderId, 130,Unit.PX);
	}

	private void addColumnSalesOrderBillingId() {

		getColumnBillingId = new TextColumn<BillingDocument>() {
			
			@Override
			public String getValue(BillingDocument object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getColumnBillingId,"Billing Id");
		table.setColumnWidth(getColumnBillingId, 130,Unit.PX);
	}

	private void addColumnSalesOrderCustomerName() {

		getColumnCustomerName = new TextColumn<BillingDocument>() {
			
			@Override
			public String getValue(BillingDocument object) {
				if(object.getPersonInfo()!=null)
					return object.getPersonInfo().getFullName();
				return "";
			}
		};
		table.addColumn(getColumnCustomerName,"Customer Name");
		table.setColumnWidth(getColumnCustomerName, 130,Unit.PX);
	}

	private void addColumnSalesOrderNetPayable() {

		getColumnNetPayable = new TextColumn<BillingDocument>() {
			
			@Override
			public String getValue(BillingDocument object) {
				
				return numberFormat.format(object.getTotalBillingAmount())+"";
			}
		};
		table.addColumn(getColumnNetPayable,"Total Billing Amount");
		table.setColumnWidth(getColumnNetPayable, 100,Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
