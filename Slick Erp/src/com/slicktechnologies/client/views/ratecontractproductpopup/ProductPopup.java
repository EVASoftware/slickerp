package com.slicktechnologies.client.views.ratecontractproductpopup;

import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;

public class ProductPopup  extends AbsolutePanel{

	

	Button btnOk,btnCancel;
	ServiceProductTable productTable;
	InlineLabel lbnetamt;

	public ProductPopup(){
		
		
		productTable = new ServiceProductTable();
		productTable.connectToLocal();
		add(productTable.getTable(),10,30);
		
		btnOk = new Button("Ok");
		add(btnOk, 460, 350);
		
		btnCancel = new Button("Cancel");
		add(btnCancel,500,350);
	
//		setSize("600px", "400px");
		setSize("1030px", "400px");/** size of table is increased by sheetal on 08-12-2021**/
		this.getElement().setId("form");
		
		
	}
	
	
	

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public ServiceProductTable getProductTable() {
		return productTable;
	}

	public void setProductTable(ServiceProductTable productTable) {
		this.productTable = productTable;
	}
	
	
}
