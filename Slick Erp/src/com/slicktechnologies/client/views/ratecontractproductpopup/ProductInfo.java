package com.slicktechnologies.client.views.ratecontractproductpopup;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;

@Embed
public class ProductInfo implements Serializable{

	
	
	private static final long serialVersionUID = 7956136982424485491L;
	

	protected String productCode;
	protected String productName;
	protected boolean selectedProduct;
	protected String serviceTaxEdit;
	protected Date serviceDate;
	private Double price;
	private ServiceProduct serviceProduct;
	
	private double totalAmount;
	
	//  rohan added this code 
	
	private String areaprSqFeet;
	//  rohan added this qty 
	private double ProductQty;
	
	protected String vatTaxEdit;
	
	/**
	 * Date : 30-10-2017 BY ANIL
	 * this field is added to check the srNo,if same product is added more than once.
	 */
	protected int srNo;
	
	public ProductInfo(){
		
		serviceProduct=new ServiceProduct();
	}
	
	
	
	
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public boolean isSelectedProduct() {
		return selectedProduct;
	}
	public void setSelectedProduct(boolean selectedProduct) {
		this.selectedProduct = selectedProduct;
	}
	public String getServiceTaxEdit() {
		return serviceTaxEdit;
	}
	public void setServiceTaxEdit(String serviceTaxEdit) {
		this.serviceTaxEdit = serviceTaxEdit;
	}

	public Date getServiceDate() {
		return serviceDate;
	}
	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	public Tax getServiceTax() {
			return serviceProduct.getServiceTax();
	}
	
	public void setServiceTax(Tax serviceTax) {
			serviceProduct.setServiceTax(serviceTax);
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public double getProductQty() {
		return ProductQty;
	}





	public void setProductQty(double productQty) {
		ProductQty = productQty;
	}





	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getAreaprSqFeet() {
		return areaprSqFeet;
	}

	public void setAreaprSqFeet(String areaprSqFeet) {
		this.areaprSqFeet = areaprSqFeet;
	}

	
	public Tax getVatTax() {
		return serviceProduct.getVatTax();
	}

	public void setVatTax(Tax vatTax) {
		serviceProduct.setVatTax(vatTax);
	}


	public String getVatTaxEdit() {
		return vatTaxEdit;
	}

	public void setVatTaxEdit(String vatTaxEdit) {
		this.vatTaxEdit = vatTaxEdit;
	}

	public int getSrNo() {
		return srNo;
	}
	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	
	/**
	 * Date 06/06/2018 by vijay
	 * Des :- Warranty Period of service product
	 * Requirement :- Neatedge Services
	 */
	public int getWarrantyPeriod(){
		if(serviceProduct!=null){
			return serviceProduct.getWarrantyPeriod();
		}else{
			return 0;
		}
	}
	
	public void setWarrantyPeriod(int warrantyPeriod){
			 serviceProduct.setWarrantyPeriod(warrantyPeriod);
	}
	
	/**
	 * ends here
	 */
	
	
	
}
