package com.slicktechnologies.client.views.ratecontractproductpopup;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.productlayer.Tax;

public class ServiceProductTableForMultipleBranches extends SuperTable<ProductInfo>{

	
	TextColumn<ProductInfo> getColumnProductCode;
	TextColumn<ProductInfo> getColumnProductName;
	private Column<ProductInfo, Boolean> getColumnselectProduct;
	Column<ProductInfo,Date> getColumnServiceDate;
	TextColumn<ProductInfo> viewselectProduct;
	Column<ProductInfo,String> getColumnServiceTax,vatColumn;
	Column<ProductInfo,String> getColumnPrice;
//	Column<ProductInfo,String> getColumnTotalAmt;
	public  ArrayList<TaxDetails> servicetaxlist;
	public  ArrayList<String> list;
	
	// rohan added this code for NBHC 
	
//	Column<ProductInfo,String> getColumnArea;
	TextColumn<ProductInfo> getColumnArea;
	
	// rohan added this qty for NBHC 
	
	Column<ProductInfo,String> getColumnProdQty;
	
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	
	//Date 09-08-2017 added by vijay for GST and vat
	public  ArrayList<TaxDetails> vattaxlist;
	public  ArrayList<String> vatlist;
	
	TextColumn<ProductInfo> getColumnTotalAmt;
	
	/**
	 * Date : 07-11-2017 BY ANIL
	 * Making quantity and price column read only
	 * for NBHC
	 */
	TextColumn<ProductInfo> getViewColumnQty;
	TextColumn<ProductInfo> getViewColumnPrice;
	/**
	 * End
	 */
	
	
	/**
	 * Date 06-06-2018 By vijay
	 * Des :- warranty of the product
	 * Requirement :- Neatedge Services
	 */
	Column<ProductInfo, String> getcolWarrantyPeriod;
	TextColumn<ProductInfo> viewcolWarrantyPeriod;
	/**
	 * ends here
	 */
	
	public ServiceProductTableForMultipleBranches(){
		super();
		setHeight("300px");
	}
	
	@Override
	public void createTable() {
		addColumnChechbox();
		addColumnProductId();
		addColumnProductName();
		addColumnDate();
//		addColumnProdQty();
		getViewColumnQty();	
		addColumnAreaPerSqFeet();
//		addcolumnPrice();
		getViewColumnPrice();
		retriveServiceTax();
		
	}

	private void getViewColumnPrice() {
		getViewColumnPrice = new TextColumn<ProductInfo>() {
			@Override
			public String getValue(ProductInfo object) {
//				if (object.getPrice() != 0) {
					return object.getPrice() + "";
//				} else {
//					return "NA";
//				}
			}
		};
		table.addColumn(getViewColumnPrice, "Price");
		table.setColumnWidth(getViewColumnPrice, 100, Unit.PX);
		
	}

	private void getViewColumnQty() {
		getViewColumnQty = new TextColumn<ProductInfo>() {
			@Override
			public String getValue(ProductInfo object) {
				if (object.getProductQty() != 0) {
					return object.getProductQty() + "";
				} else {
					return "NA";
				}
			}
		};
		table.addColumn(getViewColumnQty, "No. of Branches");//Qty
		table.setColumnWidth(getViewColumnQty, 100, Unit.PX);
		
	}

	private void addColumnProdQty() {
		
		
		EditTextCell editCell=new EditTextCell();
		
		getColumnProdQty = new Column<ProductInfo, String>(editCell) {
			
			@Override
			public String getValue(ProductInfo object) {
				if(object.getProductQty()!=0){
					return object.getProductQty()+"";
				}else{
					return "NA";
				}
			}
		};
		
		table.addColumn(getColumnProdQty,"# Qty");
		table.setColumnWidth(getColumnProdQty, 100,Unit.PX);
	}

	private void addColumnAreaPerSqFeet() {		
		
		getColumnArea = new TextColumn<ProductInfo>() {
			
			@Override
			public String getValue(ProductInfo object) {
				if(object.getAreaprSqFeet()!=null){
					return object.getAreaprSqFeet();
				}else{
					return "NA";
				}
				
			}
		};
		
		table.addColumn(getColumnArea,"# Qty");//# Area
		table.setColumnWidth(getColumnArea, 100,Unit.PX);
	}

	private void addColumnTotalAmt() {
//		EditTextCell editCell=new EditTextCell();
//		getColumnTotalAmt = new Column<ProductInfo, String>(editCell) {
//			
//			@Override
//			public String getValue(ProductInfo object) {
//				
//				double totalAmount = 0;
//				
//				System.out.println("Get value from area =="+ object.getAreaprSqFeet());
//				if( (object.getAreaprSqFeet().equals(null) && object.getAreaprSqFeet().equals("")) || 
//						object.getAreaprSqFeet().equals("0")){
//					System.out.println(" vijay mmmmmmm ");
//					object.setAreaprSqFeet("NA");
//				}
//				
//				System.out.println("area == "+object.getAreaprSqFeet());
//					if(!object.getAreaprSqFeet().equalsIgnoreCase("NA") ){
//					double area = Double.parseDouble(object.getAreaprSqFeet());
//					totalAmount =  object.getPrice()*area;
//					System.out.println(" Final TOTAL if no discount per & no discount Amt ==="+totalAmount);
//					}else{
//						totalAmount = object.getPrice();
//					}
//				
//				
//				System.out.println(" table price =="+object.getPrice());
//				if(object.getServiceTax().getPercentage()!=0.0 || object.getServiceTax().getPercentage()!=0){
//					System.out.println(" inside service tax calculation");
//					double servicetax = totalAmount * object.getServiceTax().getPercentage() /100;
//					
//					totalAmount = totalAmount+servicetax;
//					System.out.println("after service tax total value =="+totalAmount);
//					object.setTotalAmount(totalAmount);
//				}
//				return totalAmount+"";
//			}
//		};
		
		getColumnTotalAmt = new TextColumn<ProductInfo>() {
			
			@Override
			public String getValue(ProductInfo object) {
				double totalAmount = 0;
				if( (object.getAreaprSqFeet()==null|| object.getAreaprSqFeet().equals("")) || object.getAreaprSqFeet().equals("0")) {
					object.setAreaprSqFeet("NA");
				}
				if (!object.getAreaprSqFeet().equalsIgnoreCase("NA")) {
					double area = Double.parseDouble(object.getAreaprSqFeet());
					totalAmount = object.getPrice() * area;
				} else {
					totalAmount = object.getPrice();
				}
				
				//Date 09-08-2017 added by vijay for GST and VAT
				double baseAmount = totalAmount;
				
				if(object.getVatTax().getPercentage()!=0.0 || object.getVatTax().getPercentage()!=0){
					System.out.println("Inside vat");
					double vatTax = totalAmount * object.getVatTax().getPercentage()/100;
					totalAmount = totalAmount+vatTax;
					object.setTotalAmount(totalAmount);
				}
				System.out.println("Total AMOUNT =="+totalAmount);
				
				System.out.println(" table price =="+object.getPrice());
				if(object.getServiceTax().getPercentage()!=0.0 || object.getServiceTax().getPercentage()!=0){
					System.out.println(" inside service tax calculation");
					

					if(object.getVatTax().getTaxPrintName()!=null && !object.getVatTax().getTaxPrintName().equals("")){
						System.out.println("GST");
						double gstTax = baseAmount * object.getServiceTax().getPercentage() /100;
						 totalAmount = totalAmount+gstTax;
						 System.out.println("GST AMOUNT =="+totalAmount);
					}else{
						
						double servicetax = totalAmount * object.getServiceTax().getPercentage() /100;
						totalAmount = totalAmount+servicetax;
						System.out.println("after service tax total value =="+totalAmount);
					}
					
					
					object.setTotalAmount(totalAmount);
				}
				return totalAmount+"";
			}
		};
				
		table.addColumn(getColumnTotalAmt, "Total");
		table.setColumnWidth(getColumnTotalAmt,90,Unit.PX);
	}




	private void addcolumnPrice() {
		EditTextCell editCell=new EditTextCell();
		getColumnPrice = new Column<ProductInfo, String>(editCell) {
			
			@Override
			public String getValue(ProductInfo object) {
				return object.getPrice()+"";
			}
		};
		
		table.addColumn(getColumnPrice, "#Price");
		table.setColumnWidth(getColumnPrice,90,Unit.PX);
	}


	private void retriveServiceTax() {


		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

		
			public void onSuccess(ArrayList<SuperModel> result) {
				
				System.out.println("result size of tax "+result);
				list=new ArrayList<String>();
				servicetaxlist=new ArrayList<TaxDetails>();
				
				vattaxlist = new ArrayList<TaxDetails>();
				vatlist=new ArrayList<String>();
				
				List<TaxDetails> backlist=new ArrayList<TaxDetails>();
				
				for (SuperModel model : result) {
					TaxDetails entity = (TaxDetails) model;
					
					backlist.add(entity);
					servicetaxlist.add(entity);
					vattaxlist.add(entity);
				}
				
				for(int i=0;i<backlist.size();i++){
//					if(backlist.get(i).getServiceTax().equals(true)){
						list.add(backlist.get(i).getTaxChargeName());
						System.out.println("list size"+list.size());
						
						vatlist.add(backlist.get(i).getTaxChargeName());

//					}
					}
				
				addEditColumnVatTax();
				addEditColumnServiceTax();
				addColumnTotalAmt();
				
				/**
				 * Date 06-06-2018 By vijay
				 */
				getColWarrantyPeriod();
				/**
				 * ends here
				 */
				
				addFieldUpdater();
			}


			
		});
	
	}

	
	/**
	 * Date 06-06-2018 By vijay
	 * Des :- Warranty period of the product
	 */
	private void getColWarrantyPeriod() {
		EditTextCell editCell = new EditTextCell();
		getcolWarrantyPeriod = new Column<ProductInfo, String>(editCell) {
			
			@Override
			public String getValue(ProductInfo object) {
				if(object.getWarrantyPeriod()!=0){
					return object.getWarrantyPeriod()+"";
				}
				return 0+"";
			}
		};
		table.addColumn(getcolWarrantyPeriod,"#Warranty Period");
		table.setColumnWidth(getcolWarrantyPeriod, 100,Unit.PX);
		
	}
	
	/**
	 * ends here
	 */

	private void addEditColumnVatTax() {

		SelectionCell employeeselection= new SelectionCell(vatlist);
		vatColumn = new Column<ProductInfo, String>(employeeselection) {
			
			@Override
			public String getValue(ProductInfo object) {
				if (object.getVatTax().getTaxConfigName()!= null
						&&!object.getVatTax().getTaxConfigName().equals("")) {
					return object.getVatTax().getTaxConfigName();
				} else
					return "NA";
			}
		};
		
		table.addColumn(vatColumn, "# Tax 1");
		table.setColumnWidth(vatColumn, 100,Unit.PX);
	}

	protected void addEditColumnServiceTax() {

		SelectionCell taxlelection= new SelectionCell(list);
		getColumnServiceTax = new Column<ProductInfo, String>(taxlelection) {
			
			@Override
			public String getValue(ProductInfo object) {
				if (object.getServiceTax().getTaxConfigName()!= null) {
					return object.getServiceTax().getTaxConfigName();
				} else
					return "N/A";
			}
		};
		table.addColumn(getColumnServiceTax, "# Tax 2");
		table.setColumnWidth(getColumnServiceTax,90,Unit.PX);
	}




	private void addColumnChechbox() {
		CheckboxCell checkCell= new CheckboxCell();
		getColumnselectProduct = new Column<ProductInfo, Boolean>(checkCell) {
			
			@Override
			public Boolean getValue(ProductInfo object) {
				return object.isSelectedProduct();
			}
		};
		table.addColumn(getColumnselectProduct,"Select");
		table.setColumnWidth(getColumnselectProduct, 60,Unit.PX);
	}




	private void addColumnProductId() {

		getColumnProductCode = new TextColumn<ProductInfo>() {
			
			@Override
			public String getValue(ProductInfo object) {
				return object.getProductCode();
			}
		};
		table.addColumn(getColumnProductCode,"Product Code");
		table.setColumnWidth(getColumnProductCode, 90,Unit.PX);
	}




	private void addColumnProductName() {

		getColumnProductName = new TextColumn<ProductInfo>() {
			
			@Override
			public String getValue(ProductInfo object) {
				return object.getProductName();
			}
		};
		table.addColumn(getColumnProductName,"Product Name");
		table.setColumnWidth(getColumnProductName, 100,Unit.PX);
	}

	private void addColumnDate() {

		
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date= new DatePickerCell(fmt);
		
		getColumnServiceDate = new Column<ProductInfo, Date>(date) {
			
			@Override
			public Date getValue(ProductInfo object) {
				return fmt.parse(fmt.format(object.getServiceDate()));
			}
		};
		
		table.addColumn(getColumnServiceDate,"# Service Date");
		table.setColumnWidth(getColumnServiceDate, 90,Unit.PX);
		
	}

	@Override
	protected void initializekeyprovider() {

		
	}

	@Override
	public void addFieldUpdater() {
		System.out.print("Ashiwini ServiceProductTable addFieldUpdater");
		createFieldUpdatorSelectProduct();
		updateServiceTaxColumn();
		createFildUpdatorServiceDate();
//		createFieldUpdatorPrice();
//		createFieldUpdatorArea();//commented by ashwini patil
//		createFieldProdQty();
		
		updateVatTaxColumn();
	}

	

	public void updateVatTaxColumn()
	{

		vatColumn.setFieldUpdater(new FieldUpdater<ProductInfo, String>() {
			
			@Override
			public void update(int index, ProductInfo object, String value) {
				
				try {
					String val1 = (value.trim());
					object.setVatTaxEdit(val1);
					System.out.println("val1 vat "+val1);
					Tax tax = object.getVatTax();
					for(int i=0;i<vattaxlist.size();i++)
					{
						if(val1.trim().equals(vattaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(vattaxlist.get(i).getTaxChargePercent());
							/**
							 * Vijay added this for GST implementation Date :09-08-2017
							 */
							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());
							tax.setTaxConfigName(vattaxlist.get(i).getTaxChargeName());

							break;
						}
					}
					object.setVatTax(tax);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	private void createFieldProdQty() {
		
		getColumnProdQty.setFieldUpdater(new FieldUpdater<ProductInfo, String>() {
			
			@Override
			public void update(int index, ProductInfo object, String value) {
				
				try{
//					Double val1=Double.parseDouble(value.trim());
					object.setProductQty(Double.parseDouble(value));
//					productIdCount=object.getPrduct().getCount();
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			
			}
		});
	}

	private void createFieldUpdatorArea() {
		
			getColumnArea.setFieldUpdater(new FieldUpdater<ProductInfo, String>() {
				
				@Override
				public void update(int index, ProductInfo object, String value) {
					
					try{
//						Double val1=Double.parseDouble(value.trim());
						object.setAreaprSqFeet(value);
//						productIdCount=object.getPrduct().getCount();
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					}
					catch (NumberFormatException e)
					{

					}
					table.redrawRow(index);
				
				}
			});
	}

	private void createFieldUpdatorPrice() {

		getColumnPrice.setFieldUpdater(new FieldUpdater<ProductInfo, String>() {
			
			@Override
			public void update(int index, ProductInfo object, String value) {

				object.setPrice(Double.parseDouble(value));
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	
	}


	private void createFildUpdatorServiceDate() {

		getColumnServiceDate.setFieldUpdater(new FieldUpdater<ProductInfo, Date>() {
			
			@Override
			public void update(int index, ProductInfo object, Date value) {
				object.setServiceDate(value);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
		
	}




	private void updateServiceTaxColumn() {
		
		getColumnServiceTax.setFieldUpdater(new FieldUpdater<ProductInfo, String>() {
			
			@Override
			public void update(int index, ProductInfo object, String value) {
				try {
					String val1 = (value.trim());
					object.setServiceTaxEdit(val1);
				
						System.out.println(" val1 is of priceeeeee == =="+val1);
					Tax tax = new Tax();
					for(int i=0;i<servicetaxlist.size();i++)
					{
						System.out.println(" list value =="+servicetaxlist.get(i).getTaxChargeName());
						if(val1.trim().equals(servicetaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(servicetaxlist.get(i).getTaxChargePercent());
							
							 /** Vijay added this for GST implementation Date :09-08-2017
							 */
							tax.setTaxPrintName(servicetaxlist.get(i).getTaxPrintName());
							tax.setTaxConfigName(servicetaxlist.get(i).getTaxChargeName());
							break;
						}
					}
					object.setServiceTax(tax);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
				
			}
		});
		
	}




	private void createFieldUpdatorSelectProduct() {
		getColumnselectProduct.setFieldUpdater(new FieldUpdater<ProductInfo, Boolean>() {
			
			@Override
			public void update(int index, ProductInfo object, Boolean value) {

				try {
					System.out.println(" check box Value changed ==== "+value);
					Boolean val1=(value);
					object.setSelectedProduct(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				} catch (Exception e) {
				}
				table.redrawRow(index);
			}
		});
		
	}




	@Override
	public void setEnable(boolean state) {

	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
