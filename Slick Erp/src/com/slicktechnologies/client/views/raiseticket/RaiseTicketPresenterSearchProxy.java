package com.slicktechnologies.client.views.raiseticket;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.raiseticket.RaiseTicketPresenter.RaiseTicketPresenterSearch;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.supportlayer.RaiseTicket;

public class RaiseTicketPresenterSearchProxy extends RaiseTicketPresenterSearch{
	public IntegerBox ibTicketId;
	public TextBox tbTitle;
	public ListBox lbStatus;
	public DateComparator dateComparator;
	public ObjectListBox<ConfigCategory> olbConfigRaiseTicketCategory;
	public ObjectListBox<Type> olbConfigRaiseTicketType;
	public ObjectListBox<Config> olbConfigRaiseTicketLevel,olbConfigRaiseTicketPriority;
	public ObjectListBox<Type> olbDocumentName;
	public TextBox tbDocumentId;
	public ObjectListBox<ConfigCategory> olbModule;

	
	public Object getVarRef(String varName)
	{


		if(varName.equals("lbStatus"))
			return this.lbStatus;
		if(varName.equals("dateComparator"))
			return this.dateComparator;
		if(varName.equals("ibTicketId"))
			return this.ibTicketId;
		if(varName.equals("tbTitle"))
			return this.tbTitle;
		if(varName.equals("tbDocumentId"))
			return this.tbDocumentId;
		if(varName.equals("olbDocumentName"))
			return this.olbDocumentName;
		if(varName.equals("olbModule"))
			return this.olbModule;
		if(varName.equals("olbConfigRaiseTicketCategory"))
			return this.olbConfigRaiseTicketCategory;
		if(varName.equals("olbConfigRaiseTicketLevel"))
			return this.olbConfigRaiseTicketLevel;
		if(varName.equals("olbConfigRaiseTicketPriority"))
			return this.olbConfigRaiseTicketPriority;
		if(varName.equals("olbConfigRaiseTicketType"))
			return this.olbConfigRaiseTicketType;
		
		return null ;
	}
	
	
public RaiseTicketPresenterSearchProxy() {
	super();
	createGui();
}
	public void initWidget()
	{
		
		ibTicketId=new IntegerBox();
		tbTitle=new TextBox();
		tbDocumentId=new TextBox();
		dateComparator=new DateComparator("raiseTicketDate",new RaiseTicket());
		olbModule=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbModule, Screen.MODULENAME);
		olbDocumentName=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbDocumentName, Screen.DOCUMENTNAME);
		olbConfigRaiseTicketLevel=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbConfigRaiseTicketLevel, Screen.TICKETLEVEL);
		olbConfigRaiseTicketPriority=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbConfigRaiseTicketPriority, Screen.TICKETPRIORITY);
		olbConfigRaiseTicketCategory=new ObjectListBox<ConfigCategory>();
		olbConfigRaiseTicketType=new ObjectListBox<Type>();
		setOneToManyCombo();
		lbStatus=new ListBox();
		AppUtility.setStatusListBox(lbStatus, RaiseTicket.getStatusList());
	}


	public void setOneToManyCombo() {
		CategoryTypeFactory.initiateOneManyFunctionality(this.olbConfigRaiseTicketCategory,this.olbConfigRaiseTicketType);
		AppUtility.makeTypeListBoxLive(this.olbConfigRaiseTicketType, Screen.TICKETCATEGORY);
		
	}


	public void setModuleCombo() {
		CategoryTypeFactory.initiateOneManyFunctionality(this.olbModule,this.olbDocumentName);
		 AppUtility.makeTypeListBoxLive(this.olbDocumentName,Screen.MODULENAME);
		
	}
	
	@Override
	public void createScreen() {
		initWidget();
FormFieldBuilder builder;
builder=new FormFieldBuilder("Ticket Id",ibTicketId);
FormField fibTicketId=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
builder=new FormFieldBuilder("Title",tbTitle);
FormField ftbTitle=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
builder = new FormFieldBuilder("From Date",dateComparator.getFromDate());
FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
builder = new FormFieldBuilder("To Date",dateComparator.getToDate());
FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
builder=new FormFieldBuilder("Category",olbConfigRaiseTicketCategory);
FormField folbCategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
builder=new FormFieldBuilder("Type",olbConfigRaiseTicketType);
FormField folbType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
builder=new FormFieldBuilder("Priority",olbConfigRaiseTicketPriority);
FormField folbPriority= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
builder=new FormFieldBuilder("Level",olbConfigRaiseTicketLevel);
FormField folbLevel= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
builder=new FormFieldBuilder("Staus",lbStatus);
FormField flbStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
builder=new FormFieldBuilder("Module",olbModule);
FormField folbModule= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
builder=new FormFieldBuilder("Document Name",olbDocumentName);
FormField folbDocumentName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
builder=new FormFieldBuilder("Document Id",tbDocumentId);
FormField ftbDocumentId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
this.fields=new FormField[][]{
		
		{fibTicketId,ftbTitle,fdateComparator,fdateComparator1},
		{folbCategory,folbType,folbPriority,folbLevel},
		{flbStatus,folbModule,folbDocumentName,ftbDocumentId},
		};
			
		}

	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(ibTicketId.getValue()!=null)
		{  
			temp=new Filter();
			temp.setIntValue(ibTicketId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}

		if(!tbTitle.getValue().equals(""))
		{
			temp=new Filter();
			temp.setStringValue(tbTitle.getValue().trim());
			temp.setQuerryString("raiseTicketTitle");
			filtervec.add(temp);
		}
		
		if(!tbDocumentId.getValue().equals(""))
		{

			temp=new Filter();
			temp.setStringValue(tbDocumentId.getValue().trim());
			temp.setQuerryString("raiseTicketDocumentId");
			filtervec.add(temp);
		}
		
		
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		
		
		if(lbStatus.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(lbStatus.getItemText(lbStatus.getSelectedIndex()).trim());
			temp.setQuerryString("raiseTicketStatus");
			filtervec.add(temp);
		}
		if(olbConfigRaiseTicketType.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbConfigRaiseTicketType.getValue().trim());
			temp.setQuerryString("raiseTicketType");
			filtervec.add(temp);
		}

		if(olbConfigRaiseTicketCategory.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbConfigRaiseTicketCategory.getValue().trim());
			temp.setQuerryString("raiseTicketCategory");
			filtervec.add(temp);
		}
		

		if(olbDocumentName.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbDocumentName.getValue().trim());
			temp.setQuerryString("raiseTicketDocumentName");
			filtervec.add(temp);
		}
		

		if(olbModule.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbModule.getValue().trim());
			temp.setQuerryString("raiseTicketModule");
			filtervec.add(temp);
		}

		if(olbConfigRaiseTicketLevel.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbConfigRaiseTicketLevel.getValue().trim());
			temp.setQuerryString("raiseTicketLevel");
			filtervec.add(temp);
		}
		

		if(olbConfigRaiseTicketPriority.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbConfigRaiseTicketPriority.getValue().trim());
			temp.setQuerryString("raiseTicketPriority");
			filtervec.add(temp);
		}
			
			
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new RaiseTicket());
		return querry;
	}


	@Override
	public boolean validate() {
		return super.validate();
	}

	
	
}


