package com.slicktechnologies.client.views.raiseticket;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.raiseticket.RaiseTicketPresenter.RaiseTicketPresenterTable;
import com.slicktechnologies.shared.common.supportlayer.RaiseTicket;

public class RaiseTicketPresenterTableProxy extends RaiseTicketPresenterTable{
	
	TextColumn<RaiseTicket> getCountColumn;
	TextColumn<RaiseTicket> getTitleColumn;
	TextColumn<RaiseTicket> getCreationDateColumn;
	TextColumn<RaiseTicket> getReferenceNoColumn;
	TextColumn<RaiseTicket> getEmpNameColumn;
	TextColumn<RaiseTicket> getCellNoColumn;
	TextColumn<RaiseTicket> getBranchColumn;
	TextColumn<RaiseTicket> getPriorityColumn;
	TextColumn<RaiseTicket> getLevelColumn;
	TextColumn<RaiseTicket> getCategoryColumn;
	TextColumn<RaiseTicket> getTypeColumn;
	TextColumn<RaiseTicket> getStatusColumn;
	TextColumn<RaiseTicket> getModuleColumn;
	TextColumn<RaiseTicket> getDocumentNameColumn;
	TextColumn<RaiseTicket> getDocumentIdColumn;

	public RaiseTicketPresenterTableProxy() {
	super();
	}
	@Override
	public void createTable() {
	
	addColumngetCount();
	addColumngetTitle();
	addColumngetCreationDate();  
	addColumngetEmployeeName();
	addColumngetCellNumber();
	addColumngetModule();
	addColumngetDocumentId();
	addColumngetTicketPriority();
	addColumngetCategory();            
	addColumngetType();
	addColumngetTicketLevel();
	addColumngetBranch();
	addColumngetModule();
	addColumngetDocumentName();
	addColumngetReferenceNo();
	addColumngetStatus();
}

public void addColumnSorting(){
	 
	addSortinggetCount();
	addSortingetTitle();
	addSortingetCreationDate();  
	addSortinggetEmployeeName();
	addSortinggetCellNumber();
	addSortinggetModule();
	addSortinggetDocumentId();
	addSortinggetTicketPriority();
	addSortinggetCategory();            
	addSortinggetType();
	addSortinggetTicketLevel();
	addSortinggetBranch();
	addSortinggetStatus();
	addSortinggetModule();
	addSortinggetDocumentName();
	addSortinggetReferenceNo();
}


public void addSortinggetDocumentName() {
	
	
}
public void addSortinggetStatus() {
	List<RaiseTicket> list=getDataprovider().getList();
	  columnSort=new ListHandler<RaiseTicket>(list);
	  columnSort.setComparator(getStatusColumn, new Comparator<RaiseTicket>()
	  {
	  @Override
	  public int compare(RaiseTicket e1,RaiseTicket e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getRaiseTicketStatus()!=null && e2.getRaiseTicketStatus()!=null){
	  return e1.getRaiseTicketStatus().compareTo(e2.getRaiseTicketStatus());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  
	
}


public void addSortinggetBranch() {

	  List<RaiseTicket> list=getDataprovider().getList();
	  columnSort=new ListHandler<RaiseTicket>(list);
	  columnSort.setComparator(getBranchColumn, new Comparator<RaiseTicket>()
	  {
	  @Override
	  public int compare(RaiseTicket e1,RaiseTicket e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getRaiseTicketBranch()!=null && e2.getRaiseTicketBranch()!=null){
	  return e1.getRaiseTicketBranch().compareTo(e2.getRaiseTicketBranch());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  
}


public void addSortinggetTicketLevel() {
	List<RaiseTicket> list=getDataprovider().getList();
	  columnSort=new ListHandler<RaiseTicket>(list);
	  columnSort.setComparator(getLevelColumn, new Comparator<RaiseTicket>()
	  {
	  @Override
	  public int compare(RaiseTicket e1,RaiseTicket e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getRaiseTicketLevel()!=null && e2.getRaiseTicketLevel()!=null){
	  return e1.getRaiseTicketLevel().compareTo(e2.getRaiseTicketLevel());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
}


public void addSortinggetType() {
	List<RaiseTicket> list=getDataprovider().getList();
	  columnSort=new ListHandler<RaiseTicket>(list);
	  columnSort.setComparator(getTypeColumn, new Comparator<RaiseTicket>()
	  {
	  @Override
	  public int compare(RaiseTicket e1,RaiseTicket e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getRaiseTicketType()!=null && e2.getRaiseTicketType()!=null){
	  return e1.getRaiseTicketType().compareTo(e2.getRaiseTicketType());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  
	
}
public void addSortinggetCategory() {
	 List<RaiseTicket> list=getDataprovider().getList();
	  columnSort=new ListHandler<RaiseTicket>(list);
	  columnSort.setComparator(getCategoryColumn, new Comparator<RaiseTicket>()
	  {
	  @Override
	  public int compare(RaiseTicket e1,RaiseTicket e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getRaiseTicketCategory()!=null && e2.getRaiseTicketCategory()!=null){
	  return e1.getRaiseTicketCategory().compareTo(e2.getRaiseTicketCategory());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	 
	
}
public void addSortinggetTicketPriority() {
	
	 List<RaiseTicket> list=getDataprovider().getList();
	  columnSort=new ListHandler<RaiseTicket>(list);
	  columnSort.setComparator(getPriorityColumn, new Comparator<RaiseTicket>()
	  {
	  @Override
	  public int compare(RaiseTicket e1,RaiseTicket e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getRaiseTicketPriority()!=null && e2.getRaiseTicketPriority()!=null){
	  return e1.getRaiseTicketPriority().compareTo(e2.getRaiseTicketPriority());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);		  
	
}


public void addSortinggetReferenceNo() {

	  List<RaiseTicket> list=getDataprovider().getList();
	  columnSort=new ListHandler<RaiseTicket>(list);
	  columnSort.setComparator(getReferenceNoColumn, new Comparator<RaiseTicket>()
	  {
	  @Override
	  public int compare(RaiseTicket e1,RaiseTicket e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getRaiseTicketReferneceNo()!=null && e2.getRaiseTicketReferneceNo()!=null){
	  return e1.getRaiseTicketReferneceNo().compareTo(e2.getRaiseTicketReferneceNo());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);		  
	
}

public void addSortinggetCount() {
	List<RaiseTicket> list=getDataprovider().getList();
	  columnSort=new ListHandler<RaiseTicket>(list);
	  columnSort.setComparator(getCountColumn, new Comparator<RaiseTicket>()
	  {
	  @Override
	  public int compare(RaiseTicket e1,RaiseTicket e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if(e1.getCount()== e2.getCount()){
	  return 0;}
	  if(e1.getCount()> e2.getCount()){
	  return 1;}
	  else{
	  return -1;}
	  }
	  else{
	  return 0;}
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	
}


public void addSortinggetModule() {
	
	List<RaiseTicket> list=getDataprovider().getList();
	  columnSort=new ListHandler<RaiseTicket>(list);
	  columnSort.setComparator(getModuleColumn, new Comparator<RaiseTicket>()
	  {
	  @Override
	  public int compare(RaiseTicket e1,RaiseTicket e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getRaiseTicketModule()!=null && e2.getRaiseTicketModule()!=null){
	  return e1.getRaiseTicketModule().compareTo(e2.getRaiseTicketModule());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  	
}
public void addSortinggetCellNumber() {

	List<RaiseTicket> list=getDataprovider().getList();
	columnSort=new ListHandler<RaiseTicket>(list);
	columnSort.setComparator(getCellNoColumn, new Comparator<RaiseTicket>()
			{
		@Override
		public int compare(RaiseTicket e1,RaiseTicket e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getRaiseTicketCell()== e2.getRaiseTicketCell()){
					return 0;}
				if(e1.getRaiseTicketCell()> e2.getRaiseTicketCell()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}



public void addSortinggetEmployeeName() {
	
	List<RaiseTicket> list=getDataprovider().getList();
	  columnSort=new ListHandler<RaiseTicket>(list);
	  columnSort.setComparator(getEmpNameColumn, new Comparator<RaiseTicket>()
	  {
	  @Override
	  public int compare(RaiseTicket e1,RaiseTicket e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getRaiseTicketEmpName()!=null && e2.getRaiseTicketEmpName()!=null){
	  return e1.getRaiseTicketEmpName().compareTo(e2.getRaiseTicketEmpName());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  
}



public void addSortingetCreationDate() {
	
	List<RaiseTicket> list=getDataprovider().getList();
	  columnSort=new ListHandler<RaiseTicket>(list);
	  columnSort.setComparator(getCreationDateColumn, new Comparator<RaiseTicket>()
	  {
	  @Override
	  public int compare(RaiseTicket e1,RaiseTicket e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getRaiseTicketDate()!=null && e2.getRaiseTicketDate()!=null){
	  return e1.getRaiseTicketDate().compareTo(e2.getRaiseTicketDate());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  	
}



public void addSortingetTitle() {
	 
	  List<RaiseTicket> list=getDataprovider().getList();
	  columnSort=new ListHandler<RaiseTicket>(list);
	  columnSort.setComparator(getTitleColumn, new Comparator<RaiseTicket>()
	  {
	  @Override
	  public int compare(RaiseTicket e1,RaiseTicket e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getRaiseTicketTitle()!=null && e2.getRaiseTicketTitle()!=null){
	  return e1.getRaiseTicketTitle().compareTo(e2.getRaiseTicketTitle());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	 
	 
	
}
public void addSortinggetDocumentId() {
	List<RaiseTicket> list=getDataprovider().getList();
	  columnSort=new ListHandler<RaiseTicket>(list);
	  columnSort.setComparator(getDocumentIdColumn, new Comparator<RaiseTicket>()
	  {
	  @Override
	  public int compare(RaiseTicket e1,RaiseTicket e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if(e1.getRaiseTicketDocumentId()== e2.getRaiseTicketDocumentId()){
	  return 0;}
	  if(e1.getRaiseTicketDocumentId()> e2.getRaiseTicketDocumentId()){
	  return 1;}
	  else{
	  return -1;}
	  }
	  else{
	  return 0;}
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	
}



protected void addColumngetCount()
{
getCountColumn=new TextColumn<RaiseTicket>()
{
@Override
public String getValue(RaiseTicket object)
{
if( object.getCount()==-1)
return "N.A";
else return object.getCount()+"";
}
};
table.addColumn(getCountColumn,"ID");
table.setColumnWidth(getCountColumn, 100, Unit.PX);
getCountColumn.setSortable(true);
}



public void addColumngetTitle() {
	

	  getTitleColumn=new TextColumn<RaiseTicket>()
	  {
	  @Override
	  public String getValue(RaiseTicket object)
	  {
	  return object.getRaiseTicketTitle()+"";
	  }
	  };
	  table.addColumn(getTitleColumn,"Title");
	  table.setColumnWidth(getTitleColumn, 110, Unit.PX);
	  getTitleColumn.setSortable(true);
	  
}


public void addColumngetCreationDate() {

	  getCreationDateColumn=new TextColumn<RaiseTicket>()
	  {
	  @Override
	  public String getValue(RaiseTicket object)
	  {
	  if(object.getRaiseTicketDate()!=null){
	  return AppUtility.parseDate(object.getRaiseTicketDate());}
	  else{
	  object.setRaiseTicketDate(new Date());
	  return "N.A";
	  }
	  }
	  };
	  table.addColumn(getCreationDateColumn,"Date");
	  table.setColumnWidth(getCreationDateColumn, 100, Unit.PX);
	  getCreationDateColumn.setSortable(true);

}



protected void addColumngetCellNumber()
{
getCellNoColumn=new TextColumn<RaiseTicket>()
{
@Override
public String getValue(RaiseTicket object)
{
return object.getRaiseTicketCell()+"";
}
};
table.addColumn(getCellNoColumn,"Cell Number");
table.setColumnWidth(getCellNoColumn, 110, Unit.PX);
getCellNoColumn.setSortable(true);
}



protected void addColumngetEmployeeName()
{
getEmpNameColumn=new TextColumn<RaiseTicket>()
{
@Override
public String getValue(RaiseTicket object)
{
return object.getRaiseTicketEmpName()+"";
}
};
table.addColumn(getEmpNameColumn,"Employee Name");
table.setColumnWidth(getEmpNameColumn, 120, Unit.PX);
getEmpNameColumn.setSortable(true);
}

public  void addColumngetBranch() {
	{
		  getBranchColumn=new TextColumn<RaiseTicket>()
				  {
				  @Override
				  public String getValue(RaiseTicket object)
				  {
				  return object.getRaiseTicketBranch()+"";
				  }
				  };
				  table.addColumn(getBranchColumn,"Branch");
				  table.setColumnWidth(getBranchColumn, 100, Unit.PX);
				  getBranchColumn.setSortable(true);
	}
	
}


public void addColumngetTicketPriority() {

	  getPriorityColumn=new TextColumn<RaiseTicket>()
	  {
	  @Override
	  public String getValue(RaiseTicket object)
	  {
	  return object.getRaiseTicketPriority()+"";
	  }
	  };
	  table.addColumn(getPriorityColumn,"Priority");
	  table.setColumnWidth(getPriorityColumn, 110, Unit.PX);
	  getPriorityColumn.setSortable(true);
	  
	
}


protected void addColumngetCategory()
{
getCategoryColumn=new TextColumn<RaiseTicket>()
{
@Override
public String getValue(RaiseTicket object)
{
return object.getRaiseTicketCategory()+"";
}
};
table.addColumn(getCategoryColumn,"Category");
table.setColumnWidth(getCategoryColumn, 110, Unit.PX);
getCategoryColumn.setSortable(true);
}


protected void addColumngetType()
{
	getTypeColumn=new TextColumn<RaiseTicket>()
{
@Override
public String getValue(RaiseTicket object)
{
return object.getRaiseTicketType()+"";
}
};
table.addColumn(getTypeColumn,"Type");
table.setColumnWidth(getTypeColumn, 100, Unit.PX);
getTypeColumn.setSortable(true);
}


protected void addColumngetTicketLevel()
{
getLevelColumn=new TextColumn<RaiseTicket>()
{
@Override
public String getValue(RaiseTicket object)
{
return object.getRaiseTicketLevel()+"";
}
};
table.addColumn(getLevelColumn,"Level");
table.setColumnWidth(getLevelColumn, 100, Unit.PX);
getLevelColumn.setSortable(true);
}



protected void addColumngetStatus()
{
getStatusColumn=new TextColumn<RaiseTicket>()
{
@Override
public String getValue(RaiseTicket object)
{
return object.getRaiseTicketStatus()+"";
}
};
table.addColumn(getStatusColumn,"Status");
table.setColumnWidth(getStatusColumn, 100, Unit.PX);
getStatusColumn.setSortable(true);
}



public void addColumngetModule() {
	getModuleColumn=new TextColumn<RaiseTicket>()
			{
			@Override
			public String getValue(RaiseTicket object)
			{
			return object.getRaiseTicketModule()+"";
			}
			};
			table.addColumn(getModuleColumn,"Module");
			table.setColumnWidth(getModuleColumn, 110, Unit.PX);
			getModuleColumn.setSortable(true);

	
}

public void addColumngetDocumentName() {
	getDocumentNameColumn=new TextColumn<RaiseTicket>()
			{
			@Override
			public String getValue(RaiseTicket object)
			{
			return object.getRaiseTicketDocumentName()+"";
			}
			};
			table.addColumn(getDocumentNameColumn,"Document Name");
			table.setColumnWidth(getDocumentNameColumn, 110, Unit.PX);
			getDocumentNameColumn.setSortable(true);

	
}



public void addColumngetDocumentId() {
	

	  getDocumentIdColumn=new TextColumn<RaiseTicket>()
	  {
	  @Override
	  public String getValue(RaiseTicket object)
	  {
	  return object.getRaiseTicketDocumentId()+"";
	  }
	  };
	  table.addColumn(getDocumentIdColumn,"Document Id");
	  table.setColumnWidth(getDocumentIdColumn, 110, Unit.PX);
	  getDocumentIdColumn.setSortable(true);
	  
}




public void addColumngetReferenceNo() {
	

	  getReferenceNoColumn=new TextColumn<RaiseTicket>()
	  {
	  @Override
	  public String getValue(RaiseTicket object)
	  {
	  return object.getRaiseTicketTitle();
	  }
	  };
	  table.addColumn(getReferenceNoColumn,"Reference No.");
	  table.setColumnWidth(getReferenceNoColumn, 100, Unit.PX);
	  getReferenceNoColumn.setSortable(true);
	  
}



}
