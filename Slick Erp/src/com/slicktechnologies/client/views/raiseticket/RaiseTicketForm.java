package com.slicktechnologies.client.views.raiseticket;

import java.util.Date;
import java.util.Vector;

import com.google.gwt.dom.builder.shared.FormBuilder;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.config.categoryconfig.CategoryTypes;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.supportlayer.RaiseTicket;

public class RaiseTicketForm extends FormScreen<RaiseTicket> implements ChangeHandler {

	// Token to add the varialble declarations
	TextBox ibId;
	TextBox tbTitle;
	MyLongBox lbCompanyId;
	TextBox tbEmailId;
	TextBox tbSubmittedBy;
	TextBox tbFixedBy;
	TextBox tbClosedBy;
	TextBox tbAssignTo;
	DateBox dbSubmittedDate;
	DateBox dbFixedDate;
	DateBox dbClosedDate;
	TextBox tbTicketReferenceNo;
	DateBox dbDate;
	TextBox tbEmpName;
	MyLongBox tbCell;
	TextBox tbBranch;
	ObjectListBox<Config> olbLevel, olbPriority;
	ObjectListBox<Type> olbTicketType;
	ObjectListBox<ConfigCategory> olbTicketCategory;
	TextBox tbStatus;
	ObjectListBox<ConfigCategory> olbModule;
	ObjectListBox<Type> olbDocumentName;
	TextBox tbDocumentId;
	TextArea taDescription;
	UploadComposite ucChoosefile;
	TextBox tbCompanyName;

	public RaiseTicketForm() {
		super();
		createGui();
	}

	public RaiseTicketForm(String[] processlevel, FormField[][] fields,
			FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
	}

	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget() {
		ibId = new TextBox();
		tbCompanyName = new TextBox();
		tbEmailId = new TextBox();
		dbSubmittedDate = new DateBox();
		dbClosedDate = new DateBox();
		dbFixedDate = new DateBox();
		tbAssignTo = new TextBox();
		tbSubmittedBy = new TextBox();
		tbFixedBy = new TextBox();
		tbClosedBy = new TextBox();
		lbCompanyId = new MyLongBox();
		tbTicketReferenceNo = new TextBox();
		tbTitle = new TextBox();
		taDescription = new TextArea();
		tbEmpName = new TextBox();
		tbCell = new MyLongBox();
		tbStatus = new TextBox();
		ucChoosefile = new UploadComposite();
		dbDate = new DateBox();
		tbBranch = new TextBox();
		tbDocumentId = new TextBox();
		olbModule = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(getOlbModule(), Screen.MODULENAME);
		olbModule.addChangeHandler(this);
		olbDocumentName = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(this.olbDocumentName,Screen.DOCUMENTNAME);
		
		olbLevel = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbLevel, Screen.TICKETLEVEL);
		olbPriority = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbPriority, Screen.TICKETPRIORITY);
		olbTicketCategory = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbTicketCategory, Screen.TICKETCATEGORY);
		olbTicketCategory.addChangeHandler(this);
		olbTicketType = new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbTicketType, Screen.TICKETTYPE);
//		setOneToManyCombo();
		tbStatus.setValue(RaiseTicket.TICKETCREATED);
		tbStatus.setEnabled(false);
		tbEmpName.setEnabled(false);
		tbBranch.setEnabled(false);
		dbDate.setValue(new Date());
		dbDate.setEnabled(false);
		tbCell.setEnabled(false);
		ibId.setEnabled(false);
		lbCompanyId.setEnabled(false);
		tbEmailId.setEnabled(false);
		tbSubmittedBy.setEnabled(false);
		tbFixedBy.setEnabled(false);
		tbClosedBy.setEnabled(false);
		dbSubmittedDate.setEnabled(false);
		dbFixedDate.setEnabled(false);
		dbClosedDate.setEnabled(false);
		tbCompanyName.setEnabled(false);
	}

	
	@Override
	public void createScreen() {
	
		initalizeWidget();

		// Token to initialize the processlevel menus.
		this.processlevelBarNames = new String[] { AppConstants.SUBMIT,AppConstants.TICKETFIXED, AppConstants.TICKETCLOSE };

		// ////////////////////////// Form Field
		// Initialization////////////////////////////////////////////////////
		// Token to initialize formfield
		FormFieldBuilder fBuilder;
		fBuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeInformation = fBuilder.setlabel("Employee Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fBuilder = new FormFieldBuilder("Company ID", lbCompanyId);
		FormField flbCompanyId = fBuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fBuilder = new FormFieldBuilder("Company Name", tbCompanyName);
		FormField ftbCompanyName = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("Employee Name", tbEmpName);
		FormField ftbEmpName = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("Cell No", tbCell);
		FormField ftbCellNo = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("Branch", tbBranch);
		FormField ftbBranch = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("Email-Id", tbEmailId);
		FormField ftbEmailId = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder();
		FormField fgroupingTicketInformation = fBuilder
				.setlabel("Ticket Information").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();
		fBuilder = new FormFieldBuilder("Ticket ID", ibId);
		FormField fibTicketId = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("* Title", tbTitle);
		FormField ftbTitle = fBuilder.setMandatory(true)
				.setMandatoryMsg("Title is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("Date", dbDate);
		FormField ftbDate = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("Reference No.", tbTicketReferenceNo);
		FormField fibReferenceNo = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("* Priority", olbPriority);
		FormField folbPriority = fBuilder.setMandatory(true)
				.setMandatoryMsg("Priority is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("* Level", olbLevel);
		FormField folbLevel = fBuilder.setMandatory(true)
				.setMandatoryMsg("Level is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("* Ticket Category", olbTicketCategory);
		FormField folbCategory = fBuilder.setMandatory(true)
				.setMandatoryMsg("Ticket Category is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("* Ticket Type", olbTicketType);
		FormField folbType = fBuilder.setMandatory(true)
				.setMandatoryMsg("Ticket Type is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("Status", tbStatus);
		FormField ftbStatus = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("* Module", olbModule);
		FormField folbModule = fBuilder.setMandatory(true)
				.setMandatoryMsg("Module is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("* Document Name", olbDocumentName);
		FormField folbDocument = fBuilder.setMandatory(true)
				.setMandatoryMsg("Document Name is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("* Document ID", tbDocumentId);
		FormField ftbDocummentId = fBuilder.setMandatory(true)
				.setMandatoryMsg("Document ID is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fBuilder = new FormFieldBuilder("Description", taDescription);
		FormField ftaDescription = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		fBuilder = new FormFieldBuilder();
		FormField fgroupingDocumentUpload = fBuilder
				.setlabel("Document Upload").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();

		fBuilder = new FormFieldBuilder("Choose File", ucChoosefile);
		FormField fucChooseFile = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fBuilder = new FormFieldBuilder();
		FormField fgroupingTicketLog = fBuilder.setlabel("Ticket Log")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();

		fBuilder = new FormFieldBuilder("Submitted By", tbSubmittedBy);
		FormField ftbSubmittedBy = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fBuilder = new FormFieldBuilder("Submitted Date", dbSubmittedDate);
		FormField fdbSubmittedDate = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fBuilder = new FormFieldBuilder("Fixed By", tbFixedBy);
		FormField ftbFixedBy = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fBuilder = new FormFieldBuilder("Fixed Date", dbFixedDate);
		FormField fdbFixedDate = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fBuilder = new FormFieldBuilder("Closed By", tbClosedBy);
		FormField ftbClosedBy = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fBuilder = new FormFieldBuilder("Closed Date", dbClosedDate);
		FormField fdbClosedDate = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fBuilder = new FormFieldBuilder("Assign To", tbAssignTo);
		FormField ftbAssignTo = fBuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// //////////////////////////////Lay Out Making
		// Code///////////////////////////////////////////////////////////////

		FormField[][] formFields = { { fgroupingEmployeeInformation },
				{ flbCompanyId, ftbCompanyName, ftbEmpName, ftbCellNo },
				{ ftbBranch, ftbEmailId }, { fgroupingTicketInformation },
				{ fibTicketId, ftbTitle, ftbDate, fibReferenceNo },
				{ folbPriority, folbLevel, folbCategory, folbType },
				{ folbModule, folbDocument, ftbDocummentId, ftbStatus },
				{ ftaDescription }, { fgroupingDocumentUpload },
				{ fucChooseFile }, { fgroupingTicketLog },
				{ ftbSubmittedBy, fdbSubmittedDate, ftbFixedBy, fdbFixedDate },
				{ ftbClosedBy, fdbClosedDate, ftbAssignTo }

		};

		this.fields = formFields;
	}

	/**
	 * method template to update the model with token entity name
	 */

	@Override
	public void updateModel(RaiseTicket model) {

		if (dbDate.getValue() != null)
			model.setRaiseTicketDate(dbDate.getValue());

		if (dbSubmittedDate.getValue() != null)
			model.setRaiseTicketSubmittedDate(dbSubmittedDate.getValue());

		if (dbFixedDate.getValue() != null)
			model.setRaiseTicketFixedDate(dbFixedDate.getValue());

		if (dbClosedDate.getValue() != null)
			model.setRaiseTicketClosedDate(dbClosedDate.getValue());

		if (tbAssignTo.getValue() != null)
			model.setRaiseTicketAssignTo(tbAssignTo.getValue());

		if (tbCompanyName.getValue() != null)
			model.setRaiseTicketCompanyName(tbCompanyName.getValue());

		if (tbSubmittedBy.getValue() != null)
			model.setRaiseTicketSubmittedBy(tbSubmittedBy.getValue());

		if (tbFixedBy.getValue() != null)
			model.setRaiseTicketFixedBy(tbFixedBy.getValue());
		if (tbClosedBy.getValue() != null)
			model.setRaiseTicketClosedBy(tbClosedBy.getValue());

		if (tbEmailId.getValue() != null)
			model.setRaiseTicketEmailId(tbEmailId.getValue());

		if (tbTitle.getValue() != null)
			model.setRaiseTicketTitle(tbTitle.getValue());

		if (tbEmpName.getValue() != null)
			model.setRaiseTicketEmpName(tbEmpName.getValue());

		if (tbBranch.getValue() != null)
			model.setRaiseTicketBranch(tbBranch.getValue());

		if (tbTicketReferenceNo.getValue() != null)
			model.setRaiseTicketReferneceNo(tbTicketReferenceNo.getValue());

		if (tbCell.getValue() != null)
			model.setRaiseTicketCell(tbCell.getValue());

		if (lbCompanyId.getValue() != null)
			model.setRaiseTicketCompanyId(lbCompanyId.getValue());

		if (ucChoosefile.getValue() != null)
			model.setUploadDocument(ucChoosefile.getValue());

		if (taDescription.getValue() != null)
			model.setRaiseTicketDescription(taDescription.getValue());

		if (tbStatus.getValue() != null)
			model.setRaiseTicketStatus(tbStatus.getValue());

		if (!tbDocumentId.getValue().equals(""))
			model.setRaiseTicketDocumentId(Integer.parseInt(tbDocumentId.getValue()));

		if (olbTicketType.getValue() != null)
			model.setRaiseTicketType(olbTicketType.getValue(olbTicketType.getSelectedIndex()));

		if (olbTicketCategory.getValue() != null) {
			model.setRaiseTicketCategory(olbTicketCategory.getValue());
		}
		
		if (olbModule.getValue() != null) {
			model.setRaiseTicketModule(olbModule.getValue());
		}
		if (olbDocumentName.getValue() != null)
			model.setRaiseTicketDocumentName(olbDocumentName.getValue(olbDocumentName.getSelectedIndex()));
		if (olbPriority.getValue() != null)
			model.setRaiseTicketPriority(olbPriority.getValue());
		if (olbLevel.getValue() != null)
			model.setRaiseTicketLevel(olbLevel.getValue());

		presenter.setModel(model);

	}

	@Override
	public void updateView(RaiseTicket view) {

		ibId.setValue(view.getCount()+"");
		if (view.getRaiseTicketTitle() != null)
			tbTitle.setValue(view.getRaiseTicketTitle());
		if (view.getRaiseTicketDate() != null)
			dbDate.setValue(view.getRaiseTicketDate());
		if (view.getRaiseTicketEmpName() != null)
			tbEmpName.setValue(view.getRaiseTicketEmpName());
		if (view.getRaiseTicketBranch() != null)
			tbBranch.setValue(view.getRaiseTicketBranch());
		if (view.getRaiseTicketEmailId() != null)
			tbEmailId.setValue(view.getRaiseTicketEmailId());
		if (view.getRaiseTicketCompanyName() != null)
			tbCompanyName.setValue(view.getRaiseTicketCompanyName());

		if (view.getRaiseTicketReferneceNo() != null)
			tbTicketReferenceNo.setValue(view.getRaiseTicketReferneceNo());
		if (view.getRaiseTicketCell() != null)
			tbCell.setValue(view.getRaiseTicketCell());
		if (view.getRaiseTicketCompanyId() != null)
			lbCompanyId.setValue(view.getRaiseTicketCompanyId());

		if (view.getUploadDocument() != null)
			ucChoosefile.setValue(view.getUploadDocument());
		if (view.getRaiseTicketDescription() != null)
			taDescription.setValue(view.getRaiseTicketDescription());
		if (view.getRaiseTicketStatus() != null)
			tbStatus.setValue(view.getRaiseTicketStatus());
		if (view.getRaiseTicketDocumentId() != 0)
			tbDocumentId.setValue(view.getRaiseTicketDocumentId()+"");
		if (view.getRaiseTicketType() != null)
			olbTicketType.setValue(view.getRaiseTicketType());
		if (view.getRaiseTicketCategory() != null) {
			olbTicketCategory.setValue(view.getRaiseTicketCategory());
		}
		if (view.getRaiseTicketModule() != null) {
			olbModule.setValue(view.getRaiseTicketModule());
		}
		if (view.getRaiseTicketDocumentName() != null)
			olbDocumentName.setValue(view.getRaiseTicketDocumentName());
		
		if (view.getRaiseTicketPriority() != null)
			olbPriority.setValue(view.getRaiseTicketPriority());
		if (view.getRaiseTicketLevel() != null)
			olbLevel.setValue(view.getRaiseTicketLevel());

		if (view.getRaiseTicketSubmittedDate() != null)
			dbSubmittedDate.setValue(view.getRaiseTicketSubmittedDate());
		if (view.getRaiseTicketFixedDate() != null)
			dbFixedDate.setValue(view.getRaiseTicketFixedDate());
		if (view.getRaiseTicketClosedDate() != null)
			dbClosedDate.setValue(view.getRaiseTicketClosedDate());

		if (view.getRaiseTicketSubmittedBy() != null)
			tbSubmittedBy.setValue(view.getRaiseTicketSubmittedBy());
		if (view.getRaiseTicketClosedDate() != null)
			tbClosedBy.setValue(view.getRaiseTicketClosedBy());
		if (view.getRaiseTicketFixedBy() != null)
			tbFixedBy.setValue(view.getRaiseTicketFixedBy());

		if (view.getRaiseTicketAssignTo() != null)
			tbAssignTo.setValue(view.getRaiseTicketAssignTo());

		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */
	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard")
						|| text.equals("Search")) {
					menus[k].setVisible(true);
				} else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Discard") || text.equals("Edit")
						|| text.equals("Search"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.RAISETICKET,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		dbDate.setEnabled(false);
		tbCell.setEnabled(false);
		tbBranch.setEnabled(false);
		tbStatus.setEnabled(false);
		tbEmpName.setEnabled(false);
		ibId.setEnabled(false);
		lbCompanyId.setEnabled(false);
		tbEmailId.setEnabled(false);
		tbCompanyName.setEnabled(false);

		tbSubmittedBy.setEnabled(false);
		tbFixedBy.setEnabled(false);
		tbClosedBy.setEnabled(false);
		dbSubmittedDate.setEnabled(false);
		dbFixedDate.setEnabled(false);
		dbClosedDate.setEnabled(false);
	}


	public void setAppHeaderBarAsPerStatus() {
		RaiseTicket entity = (RaiseTicket) presenter.getModel();
		String status = entity.getRaiseTicketStatus();
		if (status.equals(RaiseTicket.TICKETSUBMITTED)) 
		{
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")||text.equals("Email")) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		
		if (status.equals(RaiseTicket.TICKETFIXED)) 
		{
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		
		if (status.equals(RaiseTicket.TICKETCLOSED)) 
		{
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
	}

	public void setOneToManyCombo() {
		CategoryTypeFactory.initiateOneManyFunctionality(this.olbTicketCategory, this.olbTicketType);
		AppUtility.makeTypeListBoxLive(this.olbTicketType,Screen.TICKETCATEGORY);
	}
	

	@Override
	public void setToViewState() {
		super.setToViewState();
		setAppHeaderBarAsPerStatus();
		toggleProcessLevelMenu();
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		setAppHeaderBarAsPerStatus();
		this.processLevelBar.setVisibleFalse(false);
	}
	
	@Override
	public void setCount(int count)
	{
		ibId.setValue(count+"");
	}
	
	
	 public void toggleProcessLevelMenu()
		{
			RaiseTicket entity=(RaiseTicket) presenter.getModel();
			String status=entity.getRaiseTicketStatus();

			for(int i=0;i<getProcesslevelBarNames().length;i++)
			{
				InlineLabel label=getProcessLevelBar().btnLabels[i];
				String text=label.getText().trim();
								
				if(status.equals(RaiseTicket.TICKETSUBMITTED))
				{
					if(text.equals(AppConstants.SUBMIT))
						label.setVisible(false);
					if(text.equals(AppConstants.TICKETCLOSE))
						label.setVisible(false);
					if(text.equals(AppConstants.TICKETFIXED))
						label.setVisible(true);
				}
				
				if(status.equals(RaiseTicket.TICKETFIXED)){
					if(text.equals(AppConstants.TICKETFIXED))
						label.setVisible(false);
					if(text.equals(AppConstants.SUBMIT))
						label.setVisible(false);
					if(text.equals(AppConstants.TICKETCLOSE))
						label.setVisible(true);
				}
				
				if(status.equals(RaiseTicket.TICKETCLOSED)){
					if(text.equals(AppConstants.TICKETFIXED))
						label.setVisible(false);
					if(text.equals(AppConstants.SUBMIT))
						label.setVisible(false);
					if(text.equals(AppConstants.TICKETCLOSE))
						label.setVisible(false);
				}
				
			}
		}


	 
	 @Override
		public void onChange(ChangeEvent event) {
			if(event.getSource().equals(olbModule))
			{
				if(olbModule.getSelectedIndex()!=0){
					ConfigCategory cat=olbModule.getSelectedItem();
					if(cat!=null){
						AppUtility.makeLiveTypeDropDown(olbDocumentName, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
					}
				}
			}
			
			if(event.getSource().equals(olbTicketCategory))
			{
				if(olbTicketCategory.getSelectedIndex()!=0){
					ConfigCategory cat=olbTicketCategory.getSelectedItem();
					if(cat!=null){
						AppUtility.makeLiveTypeDropDown(olbTicketType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
					}
				}
			}
		}
		
	 
	
	
	/************************************** Getters And Setters *********************************************/

	

	public TextBox getTbTitle() {
		return tbTitle;
	}

	public TextBox getIbId() {
		return ibId;
	}

	public void setIbId(TextBox ibId) {
		this.ibId = ibId;
	}

	public void setTbTitle(TextBox tbTitle) {
		this.tbTitle = tbTitle;
	}

	public TextBox getTbTicketRefernceNo() {
		return tbTicketReferenceNo;
	}

	public void setTbTicketRefernceNo(TextBox tbTicketRefernceNo) {
		this.tbTicketReferenceNo = tbTicketRefernceNo;
	}

	public DateBox getDbDate() {
		return dbDate;
	}

	public void setDbDate(DateBox dbDate) {
		this.dbDate = dbDate;
	}

	public TextBox getTbEmpName() {
		return tbEmpName;
	}

	public void setTbEmpName(TextBox tbEmpName) {
		this.tbEmpName = tbEmpName;
	}

	public MyLongBox getTbCell() {
		return tbCell;
	}

	public void setTbCell(MyLongBox tbCell) {
		this.tbCell = tbCell;
	}

	public TextBox getTbBranch() {
		return tbBranch;
	}

	public void setTbBranch(TextBox tbBranch) {
		this.tbBranch = tbBranch;
	}

	public ObjectListBox<Config> getOlbLevel() {
		return olbLevel;
	}

	public void setOlbLevel(ObjectListBox<Config> olbLevel) {
		this.olbLevel = olbLevel;
	}

	public ObjectListBox<Config> getOlbPriority() {
		return olbPriority;
	}

	public void setOlbPriority(ObjectListBox<Config> olbPriority) {
		this.olbPriority = olbPriority;
	}

	public ObjectListBox<Type> getOlbTicketType() {
		return olbTicketType;
	}

	public void setOlbTicketType(ObjectListBox<Type> olbTicketType) {
		this.olbTicketType = olbTicketType;
	}

	public ObjectListBox<ConfigCategory> getOlbTicketCategory() {
		return olbTicketCategory;
	}

	public void setOlbTicketCategory(
			ObjectListBox<ConfigCategory> olbTicketCategory) {
		this.olbTicketCategory = olbTicketCategory;
	}

	public TextBox getTbStatus() {
		return tbStatus;
	}

	public void setTbStatus(TextBox tbStatus) {
		this.tbStatus = tbStatus;
	}

	public ObjectListBox<ConfigCategory> getOlbModule() {
		return olbModule;
	}

	public void setOlbModule(ObjectListBox<ConfigCategory> olbModule) {
		this.olbModule = olbModule;
	}

	public ObjectListBox<Type> getOlbDocumentName() {
		return olbDocumentName;
	}

	public void setOlbDocumentName(ObjectListBox<Type> olbDocumentName) {
		this.olbDocumentName = olbDocumentName;
	}

	public TextBox getTbDocumentId() {
		return tbDocumentId;
	}

	public void setTbDocumentId(TextBox tbDocumentId) {
		this.tbDocumentId = tbDocumentId;
	}

	public TextArea getTaDescription() {
		return taDescription;
	}

	public void setTaDescription(TextArea taDescription) {
		this.taDescription = taDescription;
	}

	public UploadComposite getUcChoosefile() {
		return ucChoosefile;
	}

	public void setUcChoosefile(UploadComposite ucChoosefile) {
		this.ucChoosefile = ucChoosefile;
	}

	public MyLongBox getLbCompanyId() {
		return lbCompanyId;
	}

	public void setLbCompanyId(MyLongBox lbCompanyId) {
		this.lbCompanyId = lbCompanyId;
	}

	public TextBox getTbEmailId() {
		return tbEmailId;
	}

	public void setTbEmailId(TextBox tbEmailId) {
		this.tbEmailId = tbEmailId;
	}

	public TextBox getTbTicketReferenceNo() {
		return tbTicketReferenceNo;
	}

	public void setTbTicketReferenceNo(TextBox tbTicketReferenceNo) {
		this.tbTicketReferenceNo = tbTicketReferenceNo;
	}

	public TextBox getTbSubmittedBy() {
		return tbSubmittedBy;
	}

	public void setTbSubmittedBy(TextBox tbSubmittedBy) {
		this.tbSubmittedBy = tbSubmittedBy;
	}

	public TextBox getTbFixedBy() {
		return tbFixedBy;
	}

	public void setTbFixedBy(TextBox tbFixedBy) {
		this.tbFixedBy = tbFixedBy;
	}

	public TextBox getTbClosedBy() {
		return tbClosedBy;
	}

	public void setTbClosedBy(TextBox tbClosedBy) {
		this.tbClosedBy = tbClosedBy;
	}

	public TextBox getTbAssignTo() {
		return tbAssignTo;
	}

	public void setTbAssignTo(TextBox tbAssignTo) {
		this.tbAssignTo = tbAssignTo;
	}

	public DateBox getDbSubmittedDate() {
		return dbSubmittedDate;
	}

	public void setDbSubmittedDate(DateBox dbSubmittedDate) {
		this.dbSubmittedDate = dbSubmittedDate;
	}

	public DateBox getDbFixedDate() {
		return dbFixedDate;
	}

	public void setDbFixedDate(DateBox dbFixedDate) {
		this.dbFixedDate = dbFixedDate;
	}

	public DateBox getDbClosedDate() {
		return dbClosedDate;
	}

	public void setDbClosedDate(DateBox dbClosedDate) {
		this.dbClosedDate = dbClosedDate;
	}

	public TextBox getTbCompanyName() {
		return tbCompanyName;
	}

	public void setTbCompanyName(TextBox tbCompanyName) {
		this.tbCompanyName = tbCompanyName;
	}

	

}
