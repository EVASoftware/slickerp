package com.slicktechnologies.client.views.raiseticket;

import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.supportlayer.RaiseTicket;

public class RaiseTicketPresenter extends FormScreenPresenter<RaiseTicket>
		implements ChangeHandler {
	
	public RaiseTicketForm form;
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	final GenricServiceAsync async = GWT.create(GenricService.class);

	public RaiseTicketPresenter(FormScreen<RaiseTicket> view, RaiseTicket model) {
		super(view, model);
		form = (RaiseTicketForm) view;
		form.setPresenter(this);
		form.getOlbModule().addChangeHandler(this);

		EmployeeInfo info = UserConfiguration.getInfo();
		form.lbCompanyId.setValue(info.getCompanyId());
		form.tbEmpName.setValue(info.getFullName());
		form.tbBranch.setValue(info.getBranch());
		form.tbCell.setValue(info.getCellNumber());
		
		getCompanyName(info.getCompanyId());
		getEmployeeEmailId(info.getFullName());
		
		
	}

	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals(AppConstants.SUBMIT)){
			reactToSubmit();
		}
		if (text.equals(AppConstants.TICKETCLOSE)){
			reactToClose();
		}
		if (text.equals(AppConstants.TICKETFIXED)){
			reactToFixed();
		}
		if (text.equals("Email")){
			reactOnEmail();
		}

	}

	
	@Override
	protected void makeNewModel() {
		model = new RaiseTicket();

	}

	

	public static void initalize() {
		AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		RaiseTicketForm form = new RaiseTicketForm();
		RaiseTicketPresenterTable gentable = new RaiseTicketPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		RaiseTicketPresenterSearch.staticSuperTable = gentable;
		RaiseTicketPresenterSearch searchpopup = new RaiseTicketPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		RaiseTicketPresenter presenter = new RaiseTicketPresenter(form,new RaiseTicket());
		AppMemory.getAppMemory().stickPnel(form);
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.supportlayer.RaiseTicket")
	public static class RaiseTicketPresenterSearch extends SearchPopUpScreen<RaiseTicket> {

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.supportlayer.RaiseTicket")
	public static class RaiseTicketPresenterTable extends
			SuperTable<RaiseTicket> implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub

		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub

		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub

		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub

		}

	}

	
	public void reactToFixed() {
		model.setRaiseTicketStatus(RaiseTicket.TICKETFIXED);
		model.setRaiseTicketFixedBy(model.getRaiseTicketEmpName());
		Date date = new Date();
		model.setRaiseTicketFixedDate(date);
		async.save(model, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");

			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				form.showDialogMessage("Ticket Fixed!");
				form.tbStatus.setValue(model.getRaiseTicketStatus());
				form.tbFixedBy.setValue(model.getRaiseTicketFixedBy());
				form.dbFixedDate.setValue(model.getRaiseTicketFixedDate());
				form.setToViewState();
			}
		});

	}

	public void reactToClose() {
		model.setRaiseTicketStatus(RaiseTicket.TICKETCLOSED);
		model.setRaiseTicketClosedBy(model.getRaiseTicketEmpName());
		Date date = new Date();
		model.setRaiseTicketClosedDate(date);
		async.save(model, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");

			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				form.showDialogMessage("Ticket Closed!");
				form.tbStatus.setValue(model.getRaiseTicketStatus());
				form.tbClosedBy.setValue(model.getRaiseTicketClosedBy());
				form.dbClosedDate.setValue(model.getRaiseTicketClosedDate());
				form.setToViewState();
			}
		});

	}

	public void reactToSubmit() {

		model.setRaiseTicketStatus(RaiseTicket.TICKETSUBMITTED);
		model.setRaiseTicketSubmittedBy(model.getRaiseTicketEmpName());
		Date date = new Date();
		model.setRaiseTicketSubmittedDate(date);
		async.save(model, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");

			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				form.showDialogMessage("Ticket Submitted!");
				form.tbStatus.setValue(model.getRaiseTicketStatus());
				form.tbSubmittedBy.setValue(model.getRaiseTicketSubmittedBy());
				form.dbSubmittedDate.setValue(model.getRaiseTicketSubmittedDate());
				form.setToViewState();

			}
		});

	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reactOnEmail()
	{
		boolean conf = Window.confirm("Do you really want to send email?");
		if (conf == true) {
		emailService.initiateSupportEmail(model,new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Resource Quota Ended ");
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(Void result) {
				Window.alert("Email Sent Sucessfully !");
			}
		});
		}
	}

	
	@Override
	public void onChange(ChangeEvent event) {
		if (form.getOlbModule().getSelectedIndex() != 0) {
			ConfigCategory configCat = form.getOlbModule().getSelectedItem();
			MyQuerry querry = new MyQuerry();
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter temp = null;

			temp = new Filter();
			temp.setIntValue(configCat.getInternalType());
			temp.setQuerryString("internalType");
			filtervec.add(temp);

			temp = new Filter();
			temp.setStringValue(configCat.getCategoryName().trim());
			temp.setQuerryString("categoryName");
			filtervec.add(temp);

			temp = new Filter();
			temp.setLongValue(model.getCompanyId());
			temp.setQuerryString("companyId");
			filtervec.add(temp);

			querry.setFilters(filtervec);
			querry.setQuerryObject(new Type());
			form.getOlbDocumentName().MakeLive(querry);
		}

		if (form.getOlbModule().getSelectedIndex() == 0) {
			form.getOlbDocumentName().setSelectedIndex(0);
		}
	}

	public void getCompanyName(long id) {
		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(id);
		querry.getFilters().add(filter);
		querry.setQuerryObject(new Company());

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for (SuperModel smodel : result) {
							Company com = (Company) smodel;
							form.tbCompanyName.setValue(com.getBusinessUnitName());
						}
					}

					@Override
					public void onFailure(Throwable caught) {

					}
				});
	}
	
	public void getEmployeeEmailId(String empname) {
		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("fullname");
		filter.setStringValue(empname.trim());
		querry.getFilters().add(filter);
		querry.setQuerryObject(new Employee());

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for (SuperModel smodel : result) {
							Employee emp = (Employee) smodel;
							form.tbEmailId.setValue(emp.getEmail());
							System.out.println("Email  :"+ emp.getEmail());
						}
					}

					@Override
					public void onFailure(Throwable caught) {

					}
				});
	}

}
