package com.slicktechnologies.client.views.loggedinhistory;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;

public class LoggedInHistoryTableProxy extends SuperTable<LoggedIn> {
	
	TextColumn<LoggedIn>userIdColumn;
	TextColumn<LoggedIn>userNameColumn;
	TextColumn<LoggedIn>loginDateColumn;
	TextColumn<LoggedIn>loginTimeColumn;
	TextColumn<LoggedIn>logoutDateColumn;
	TextColumn<LoggedIn>logoutTimeColumn;
	TextColumn<LoggedIn>processNameColumn;
	TextColumn<LoggedIn>remarkColumn;
	TextColumn<LoggedIn>sessionIdColumn;
	TextColumn<LoggedIn>statusColumn;

	@Override
	public void createTable() {
		
		addColumngetuserId();
		addColumngetuserName();
		addColumngetloginDate();
		addColumngetloginTime();
		addColumngetlogoutDate();
		addColumngetlogoutTime();
		addColumngetprocessName();
		addColumngetremark();
		addColumngetsessionId();
		addColumngetstatus();
	}

	@Override
	public void addColumnSorting(){
		
		addSortinggetuserId();
		addSortinggetuserName();
		addSortinggetloginDate();
		addSortinggetloginTime();
		addSortinggetlogoutDate();
		addSortinggetlogoutTime();
		addSortinggetprocessName();
		addSortinggetremark();
		addSortinggetsessionId();
		addSortinggetstatus();
	}

	private void addColumngetuserId() {
		userIdColumn=new TextColumn<LoggedIn>() {

			@Override
			public String getValue(LoggedIn object) {
				if(object.getUserName()!=null)
				return object.getUserName();
				else
					return "";
			}
		};
		
		table.addColumn(userIdColumn,"User Id");
		table.setColumnWidth(userIdColumn, 150, Unit.PX);
		userIdColumn.setSortable(true);	
	}
	

	private void addSortinggetuserId() {
		List<LoggedIn> list=getDataprovider().getList();
		columnSort=new ListHandler<LoggedIn>(list);
		columnSort.setComparator(userIdColumn, new Comparator<LoggedIn>()
				{
			@Override
			public int compare(LoggedIn e1,LoggedIn e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getUserName()!=null && e2.getUserName()!=null){
						return e1.getUserName().compareTo(e2.getUserName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumngetstatus() {
		statusColumn =new TextColumn <LoggedIn>() {

			@Override
			public String getValue(LoggedIn object) {
				if(object.getStatus()!=null)
				return object.getStatus();
				else
					return "";
			}
		};
		
		table.addColumn(statusColumn,"Status");
		table.setColumnWidth(statusColumn, 90, Unit.PX);
		statusColumn.setSortable(true);
	}

	private void addSortinggetstatus() {
		List<LoggedIn> list = getDataprovider().getList();
		columnSort = new ListHandler<LoggedIn>(list);
		columnSort.setComparator(statusColumn,
				new Comparator<LoggedIn>() {
					@Override
					public int compare(LoggedIn e1, LoggedIn e2) {
						if (e1 != null && e2 != null) {
							if (e1.getStatus() == e2.getStatus()) {
								return 0;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);	
	}
	
	private void addColumngetsessionId() {
		sessionIdColumn=new TextColumn<LoggedIn>() {

			@Override
			public String getValue(LoggedIn object) {
				if(object.getSessionId()!=0)
				return object.getSessionId()+"";
				else
					return "";
			}
		};
		
		table.addColumn(sessionIdColumn,"Session ID");
		table.setColumnWidth(sessionIdColumn, 100, Unit.PX);
		sessionIdColumn.setSortable(true);
	}
	
	private void addSortinggetsessionId() {
		List<LoggedIn> list=getDataprovider().getList();
		columnSort=new ListHandler<LoggedIn>(list);
		columnSort.setComparator(sessionIdColumn, new Comparator<LoggedIn>()
				{
			@Override
			public int compare(LoggedIn e1,LoggedIn e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getSessionId()== e2.getSessionId()){
						return 0;}
					if(e1.getSessionId()> e2.getSessionId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);	
	}
	
	private void addColumngetremark(){
		remarkColumn=new TextColumn<LoggedIn>() {

			@Override
			public String getValue(LoggedIn object) {
				if(object.getRemark()!=null)
				return object.getRemark();
				else
					return "";
			}
		};
		
		table.addColumn(remarkColumn,"Remark");
		table.setColumnWidth(remarkColumn, 150, Unit.PX);
		remarkColumn.setSortable(true);	
	}
	
	
	private void addSortinggetremark(){
		
		List<LoggedIn> list=getDataprovider().getList();
		columnSort=new ListHandler<LoggedIn>(list);
		columnSort.setComparator(remarkColumn, new Comparator<LoggedIn>()
				{
			@Override
			public int compare(LoggedIn e1,LoggedIn e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getRemark()!=null && e2.getRemark()!=null){
						return e1.getRemark().compareTo(e2.getRemark());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);	
	}


	private void addColumngetprocessName() {
		processNameColumn=new TextColumn<LoggedIn>() {

			@Override
			public String getValue(LoggedIn object) {
				if(object.getProcessName()!=null)
				return object.getProcessName();
				else
					return "";
			}
		};
		
		table.addColumn(processNameColumn,"Process Name");
		table.setColumnWidth(processNameColumn, 150, Unit.PX);
		processNameColumn.setSortable(true);	
	}
	
	
	private void addSortinggetprocessName() {
		List<LoggedIn> list=getDataprovider().getList();
		columnSort=new ListHandler<LoggedIn>(list);
		columnSort.setComparator(processNameColumn, new Comparator<LoggedIn>()
				{
			@Override
			public int compare(LoggedIn e1,LoggedIn e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getProcessName()!=null && e2.getProcessName()!=null){
						return e1.getProcessName().compareTo(e2.getProcessName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);		
	}


	private void addColumngetlogoutDate() {
		logoutDateColumn=new TextColumn<LoggedIn>() {

			@Override
			public String getValue(LoggedIn object) {
				if(object.getLogoutDate()!=null)
				{
				    String Date=AppUtility.parseDate(object.getLogoutDate());
				    return Date;
				}
				return "";
			}
		};
		
		table.addColumn(logoutDateColumn,"Logout Date");
		table.setColumnWidth(logoutDateColumn, 120, Unit.PX);
		logoutDateColumn.setSortable(true);	
	}
	
	
	private void addSortinggetlogoutDate() {
		List<LoggedIn> list=getDataprovider().getList();
		columnSort=new ListHandler<LoggedIn>(list);
		columnSort.setComparator(logoutDateColumn, new Comparator<LoggedIn>()
				{
			@Override
			public int compare(LoggedIn e1,LoggedIn e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getLogoutDate()!=null && e2.getLogoutDate()!=null){
						return e1.getLogoutDate().compareTo(e2.getLogoutDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);		
	}

	private void addColumngetloginDate() {
		loginDateColumn=new TextColumn<LoggedIn>() {

			@Override
			public String getValue(LoggedIn object) {
				if(object.getLoginDate()!=null)
				{
				    String Date=AppUtility.parseDate(object.getLoginDate());
				    return Date;
				}
				return "";
			}
		};
		
		table.addColumn(loginDateColumn,"Login Date");
		table.setColumnWidth(loginDateColumn, 100, Unit.PX);
		loginDateColumn.setSortable(true);	
	}
	
	
	private void addSortinggetloginDate() {
		List<LoggedIn> list=getDataprovider().getList();
		columnSort=new ListHandler<LoggedIn>(list);
		columnSort.setComparator(loginDateColumn, new Comparator<LoggedIn>()
				{
			@Override
			public int compare(LoggedIn e1,LoggedIn e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getLoginDate()!=null && e2.getLoginDate()!=null){
						return e1.getLoginDate().compareTo(e2.getLoginDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);	
	}


	private void addColumngetlogoutTime() {
		logoutTimeColumn=new TextColumn<LoggedIn>() {

			@Override
			public String getValue(LoggedIn object) {
				if(object.getLogoutTime()!=null)
				{
				    return object.getLogoutTime();
				}
				return "";
			}
		};
		
		table.addColumn(logoutTimeColumn,"Logout Time");
		table.setColumnWidth(logoutTimeColumn, 110, Unit.PX);
		logoutTimeColumn.setSortable(true);	
	}
	

	private void addSortinggetlogoutTime() {
		List<LoggedIn> list=getDataprovider().getList();
		columnSort=new ListHandler<LoggedIn>(list);
		columnSort.setComparator(logoutTimeColumn, new Comparator<LoggedIn>()
				{
			@Override
			public int compare(LoggedIn e1,LoggedIn e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getLogoutTime()!=null && e2.getLogoutTime()!=null){
						return e1.getLogoutTime().compareTo(e2.getLogoutTime());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);	
	}



	private void addColumngetloginTime() {
		loginTimeColumn=new TextColumn<LoggedIn>() {

			@Override
			public String getValue(LoggedIn object) {
				
				return object.getLoginTime()+"";
			}
		};
		
		table.addColumn(loginTimeColumn,"Login Time");
		table.setColumnWidth(loginTimeColumn, 110, Unit.PX);
		loginTimeColumn.setSortable(true);	
	}
	
	private void addSortinggetloginTime() {
		List<LoggedIn> list=getDataprovider().getList();
		columnSort=new ListHandler<LoggedIn>(list);
		columnSort.setComparator(loginTimeColumn, new Comparator<LoggedIn>()
				{
			@Override
			public int compare(LoggedIn e1,LoggedIn e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getLoginTime()!=null && e2.getLoginTime()!=null){
						return e1.getLoginTime().compareTo(e2.getLoginTime());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);	
	}


	private void addColumngetuserName() {
		userNameColumn=new TextColumn<LoggedIn>() {

			@Override
			public String getValue(LoggedIn object) {
				if(object.getEmployeeName()!=null)
				return object.getEmployeeName();
				else
					return "";
			}
		};
		
		table.addColumn(userNameColumn,"User Name");
		table.setColumnWidth(userNameColumn, 150, Unit.PX);
		userNameColumn.setSortable(true);	
	}
	
	
	private void addSortinggetuserName() {
		List<LoggedIn> list=getDataprovider().getList();
		columnSort=new ListHandler<LoggedIn>(list);
		columnSort.setComparator(userNameColumn, new Comparator<LoggedIn>()
				{
			@Override
			public int compare(LoggedIn e1,LoggedIn e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeeName()!=null && e2.getEmployeeName()!=null){
						return e1.getEmployeeName().compareTo(e2.getEmployeeName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);		
	}
	
	/***********************************************************************************************************/

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}
}
