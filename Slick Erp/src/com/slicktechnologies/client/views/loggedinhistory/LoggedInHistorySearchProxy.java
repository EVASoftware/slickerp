package com.slicktechnologies.client.views.loggedinhistory;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;

public class LoggedInHistorySearchProxy extends SearchPopUpScreen<LoggedIn> {
	
	DateComparator dateComparator;
	TextBox userName;
	ListBox status;
	IntegerBox sessionId;
	
	
	
	public LoggedInHistorySearchProxy() {
		super();
		createGui();
	}
	
	public LoggedInHistorySearchProxy(boolean b) {
		super(b);
		createGui();
	}

	
private void initializewidget() {
		
		status = new ListBox();
		status.addItem("SELECT");
		status.addItem(AppConstants.ACTIVE);
		status.addItem(AppConstants.INACTIVE);
		userName = new TextBox();
		sessionId = new IntegerBox();
		dateComparator= new DateComparator("loginDate",new LoggedIn());
	}
	
	
	public void createScreen(){
		initializewidget();
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder("UserId",userName);
		FormField userName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		builder = new FormFieldBuilder("SessionId",sessionId);
		FormField sessionId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		builder = new FormFieldBuilder("Status",status);
		FormField status= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		builder = new FormFieldBuilder("From Date(LoginDate)",dateComparator.getFromDate());
		FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date(LoginDate)",dateComparator.getToDate());
		FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		this.fields=new FormField[][]{
				{userName,sessionId,fdateComparator,fdateComparator1,status},
		};
	}

	@Override
	public MyQuerry getQuerry() {
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		
		 if(!userName.getValue().equals(""))
			  {
			  temp=new Filter();
			  temp.setStringValue(userName.getValue().trim());
			  temp.setQuerryString("userName");
			  filtervec.add(temp);
			  }
		 
		 
		 if(sessionId.getValue()!=null)
			  {
			  temp=new Filter();
			  temp.setIntValue(sessionId.getValue());
			  temp.setQuerryString("sessionId");
			  filtervec.add(temp);
			  }
		
		if(status.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(status.getItemText(status.getSelectedIndex()).trim());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new LoggedIn());					
		return querry;
		
	}

	@Override
	public boolean validate() {
		return true;
	}
	
	
}
