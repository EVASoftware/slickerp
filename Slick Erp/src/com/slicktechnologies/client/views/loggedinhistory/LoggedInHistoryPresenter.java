package com.slicktechnologies.client.views.loggedinhistory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.SessionService;
import com.slicktechnologies.client.services.SessionServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;

public class LoggedInHistoryPresenter extends TableScreenPresenter<LoggedIn> {
	
	TableScreen<LoggedIn>form;
	final GenricServiceAsync async=GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	//  rohan added this code for removing all session from array and logged in entity
	
	private SessionServiceAsync sessionService=GWT.create(SessionService.class);
	
	String str="";
	int loginlist=0;
	String userId="";
	LoggedIn loggedIn= null;
	
	public LoggedInHistoryPresenter(TableScreen<LoggedIn> view,LoggedIn model) {

		super(view, model);
		form=view;
		view.retriveTable(getLoggedInHistoryQuery());
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		reactOnLoginRecordSelect();
	}
	
	public LoggedInHistoryPresenter(TableScreen<LoggedIn> view,LoggedIn model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		reactOnLoginRecordSelect();
	}
	
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("Terminate Session")){
			validateOnTerminateSession();
		}
		
		if(text.equals("Terminate All Session")){
			terminateAllSession();
		}
	}
	
	
	private void terminateAllSession() {
		
		final List<LoggedIn>loggedinlist = form.getSuperTable().getDataprovider().getList();
		ArrayList<LoggedIn> loginArrayList=new ArrayList<LoggedIn>();
		loginArrayList.addAll(loggedinlist);
		
		System.out.println("loggedinlist"+loginArrayList.size()+"company id"+model.getCompanyId());
		
		sessionService.clearAllLoggedInUsers(loginArrayList,model.getCompanyId(), new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("Failuer...!");
			}

			@Override
			public void onSuccess(Void result) {
				
					form.showDialogMessage("User remove successfully..!!");
			}

			
		} );
		
	}

	@Override
	public void reactOnDownload() {
		ArrayList<LoggedIn> loggedinarray=new ArrayList<LoggedIn>();
		List<LoggedIn> list=(List<LoggedIn>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		loggedinarray.addAll(list);
		
		csvservice.setLoggedInHistoryDetails(loggedinarray, new AsyncCallback<Void>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+81;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	
	
	@Override
	protected void makeNewModel() {
		model=new LoggedIn();		
	}
	
	
	public static void initalize()
	{
		LoggedInHistoryTableProxy table=new LoggedInHistoryTableProxy();
		LoggedInHistoryForm form=new LoggedInHistoryForm(table);
		
		LoggedInHistorySearchProxy.staticSuperTable=table;
		LoggedInHistorySearchProxy searchPopUp=new LoggedInHistorySearchProxy(false);
		form.setSearchpopupscreen(searchPopUp);
		
		LoggedInHistoryPresenter presenter=new LoggedInHistoryPresenter(form, new LoggedIn());
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	public static void initalize(MyQuerry querry)
	{
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Settings/LoggedIn History",Screen.LOGGEDINHISTORY);
		LoggedInHistoryTableProxy table=new LoggedInHistoryTableProxy();
		LoggedInHistoryForm form=new LoggedInHistoryForm(table);
		
		LoggedInHistorySearchProxy.staticSuperTable=table;
		LoggedInHistorySearchProxy searchPopUp=new LoggedInHistorySearchProxy(false);
		form.setSearchpopupscreen(searchPopUp);
		
		LoggedInHistoryPresenter presenter=new LoggedInHistoryPresenter(form, new LoggedIn(),querry);
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	
	public MyQuerry getLoggedInHistoryQuery()
	{
		MyQuerry quer=new MyQuerry();
		Vector<Filter> filters = new Vector<Filter>();
		Filter temp = null;
		temp = new Filter();
		temp.setLongValue(model.getCompanyId());
		temp.setQuerryString("companyId");
		filters.add(temp);
		temp = new Filter();
		temp.setStringValue(AppConstants.ACTIVE);
		temp.setQuerryString("status");
		filters.add(temp);
		quer.setFilters(filters);
		quer.setQuerryObject(new LoggedIn());
		return quer;
	}
	
	
	public void validateOnTerminateSession()
	{
		String str1 = UserConfiguration.getUserconfig().getUser().getUserName();
		if(!userId.trim().equals("")){
			if(str1.trim().equals(userId.trim())&&str.equals(AppConstants.ACTIVE)){
				form.showDialogMessage("Invalid attempt!");
			}
			
			if(str.equals(AppConstants.INACTIVE)){
				form.showDialogMessage("Session already Inactive!");
			}
			
			if(str.equals(AppConstants.ACTIVE)&&!str1.trim().equals(userId.trim())){
				reactOnSessionTermination();
			}
		}
	}
	
	public void reactOnSessionTermination(){
		
		final List<LoggedIn>loggedinlist = form.getSuperTable().getDataprovider().getList();
		
		if(str.equals(AppConstants.ACTIVE)){
			
			String saveTime=DateTimeFormat.getShortTimeFormat().format(new Date());
			Date todaydate = new Date();
			
			String str2 = UserConfiguration.getUserconfig().getUser().getUserName();
			String remark = "User Session was terminated by "+str2;
			loggedIn.setRemark(remark);
			loggedIn.setStatus(AppConstants.INACTIVE);
			loggedIn.setLogoutTime(saveTime);
			loggedIn.setLogoutDate(todaydate);
			async.save(loggedIn,new AsyncCallback<ReturnFromServer>(){
				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("An unexpected error occurred!");
				}
	
				@Override
				public void onSuccess(ReturnFromServer result) {
					
					for(int i=0;i<loggedinlist.size();i++){
						if(loggedinlist.get(i).getCount()==loginlist){
							loggedinlist.remove(i);
						}
					}
					
					form.getSuperTable().connectToLocal();
					form.getSuperTable().getDataprovider().setList(loggedinlist);
				}
			});
		}
	}
	
	public void reactOnLoginRecordSelect(){
		
		final NoSelectionModel<LoggedIn> selectionModelMyObj = new NoSelectionModel<LoggedIn>();
	     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 final LoggedIn entity=selectionModelMyObj.getLastSelectedObject();
	        	 
	        	 loginlist=entity.getCount();
	        	 str=entity.getStatus();
	        	 userId = entity.getUserName();
	        	 loggedIn = entity;
	         }
	     };
    // Add the handler to the selection model
    selectionModelMyObj.addSelectionChangeHandler( tableHandler );
    // Add the selection model to the table
    form.getSuperTable().getTable().setSelectionModel(selectionModelMyObj);
	}
	
	
	public void reactOnIncreaseLicenseDate(){
		
		Company c = new Company();
		MyQuerry query = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		query.getFilters().add(filter);
		query.setQuerryObject(new Company());
		
		 async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					for(SuperModel model:result){
						
						Company company = (Company)model;
						Date date = company.getLicenseStartDate();
						CalendarUtil.addDaysToDate(date, 7);
						company.setLicenseStartDate(date);
						async.save(company,new AsyncCallback<ReturnFromServer>(){

							@Override
							public void onFailure(Throwable caught) {
								
							}

							@Override
							public void onSuccess(ReturnFromServer result) {
								
							}
							
						});
					}
					}
	});
	}
	
	
	public void reactOnDecreaseLicenseDate(){
		
		Company c = new Company();
		MyQuerry query = new MyQuerry();
		Vector<Filter> filterobj = new Vector<Filter>();
		Filter filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		query.getFilters().add(filter);
		query.setQuerryObject(new Company());
		
		 async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					
					for(SuperModel model:result){
						
						Company company = (Company)model;
						Date date = company.getLicenseStartDate();
						CalendarUtil.addDaysToDate(date, -7);
						company.setLicenseStartDate(date);
						async.save(company,new AsyncCallback<ReturnFromServer>(){

							@Override
							public void onFailure(Throwable caught) {
							}

							@Override
							public void onSuccess(ReturnFromServer result) {
							}
							
						});
					}
						
					}
	});
	}
	
	
	
	
	
	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}
}
