package com.slicktechnologies.client.views.licenseinfo;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class LicenseInfoForm extends FormScreen<Company> {
	
	
	TextBox tbcompanyName;
	IntegerBox ibNoOfLicenses;
	DateBox dbLicenseStartDate,dbLicenseEndDate;
	
	/**
	 * Date :04-04-2018 BY  ANIL
	 * 
	 */
	LicenseDetailsTable licenseDetTable;
	HorizontalPanel hp;
	
	/**
	 * Date : 06-04-2018 BY ANIL
	 */
	IntegerBox ibContrcatId;
	TextBox tbLicenseType;
	IntegerBox ibNoOfLicense;
	DateBox dbStartDate;
	IntegerBox ibDuration;
	DateBox dbEndDate;
	CheckBox cbStatus;
	Button btnAdd;
	
	
	public  LicenseInfoForm() {
		super();
		createGui();
	}

	public LicenseInfoForm(String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}

	
	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget()
	{
		tbcompanyName=new TextBox();
		tbcompanyName.setEnabled(false);
		ibNoOfLicenses=new IntegerBox();
		ibNoOfLicenses.setEnabled(false);
		dbLicenseStartDate=new DateBox();
		dbLicenseStartDate.setEnabled(false);
		dbLicenseEndDate=new DateBox();
		dbLicenseEndDate.setEnabled(false);
		
		
		
		
		licenseDetTable=new LicenseDetailsTable();
		licenseDetTable.setEnable(false);
		hp=new HorizontalPanel();
		
		/**
		 * Date : 12-04-2018 BY ANIL
		 * For manually updating license
		 * START
		 * COMMENT FORM HERE
		 */
		
//		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy"); 
//		
//		licenseDetTable.setEnable(true);
//		VerticalPanel vp1=new VerticalPanel();
//		InlineLabel lbl1=new InlineLabel("Contract Id");
//		ibContrcatId=new IntegerBox();
//		ibContrcatId.getElement().getStyle().setHeight(22, Unit.PX);
//		vp1.add(lbl1);
//		vp1.add(ibContrcatId);
//		hp.add(vp1);
//		
//		VerticalPanel vp2=new VerticalPanel();
//		InlineLabel lbl2=new InlineLabel("License Type");
//		tbLicenseType=new TextBox();
//		vp2.add(lbl2);
//		vp2.add(tbLicenseType);
//		hp.add(vp2);
//		
//		VerticalPanel vp3=new VerticalPanel();
//		InlineLabel lbl3=new InlineLabel("No. Of License");
//		ibNoOfLicense=new IntegerBox();
//		ibNoOfLicense.getElement().getStyle().setHeight(22, Unit.PX);
//		vp3.add(lbl3);
//		vp3.add(ibNoOfLicense);
//		hp.add(vp3);
//		
//		
//		VerticalPanel vp4=new VerticalPanel();
//		InlineLabel lbl4=new InlineLabel("License Start Date");
//		dbStartDate=new DateBoxWithYearSelector();
//		dbStartDate.setFormat(new DateBox.DefaultFormat(dateFormat));
//		vp4.add(lbl4);
//		vp4.add(dbStartDate);
//		hp.add(vp4);
//		
//		VerticalPanel vp5=new VerticalPanel();
//		InlineLabel lbl5=new InlineLabel("Duration");
//		ibDuration=new IntegerBox();
//		ibDuration.getElement().getStyle().setHeight(22, Unit.PX);
//		vp5.add(lbl5);
//		vp5.add(ibDuration);
//		hp.add(vp5);
//		
//		VerticalPanel vp6=new VerticalPanel();
//		InlineLabel lbl6=new InlineLabel("License End Date");
//		dbEndDate=new DateBoxWithYearSelector();
//		dbEndDate.setFormat(new DateBox.DefaultFormat(dateFormat));
//		vp6.add(lbl6);
//		vp6.add(dbEndDate);
//		hp.add(vp6);
//		
//		VerticalPanel vp7=new VerticalPanel();
//		InlineLabel lbl7=new InlineLabel("Status");
//		cbStatus=new CheckBox();
//		vp7.add(lbl7);
//		vp7.add(cbStatus);
//		hp.add(vp7);
//		
//		VerticalPanel vp8=new VerticalPanel();
//		InlineLabel lbl8=new InlineLabel(".");
//		btnAdd=new Button("Add");
//		vp8.add(lbl8);
//		vp8.add(btnAdd);
//		hp.add(vp8);
//		
//		
//		btnAdd.addClickHandler(new ClickHandler() {
//			@SuppressWarnings("unchecked")
//			@Override
//			public void onClick(ClickEvent event) {
//				GenricServiceAsync async=GWT.create(GenricService.class);
//				if(presenter.getModel()!=null){
//					Company company=(Company) presenter.getModel();
//					
//					if(!tbLicenseType.getValue().equals("")){
//						LicenseDetails licObj=new LicenseDetails();
//						licObj.setContractCount(ibContrcatId.getValue());
//						licObj.setLicenseType(tbLicenseType.getValue());
//						licObj.setNoOfLicense(ibNoOfLicense.getValue());
//						licObj.setStartDate(dbStartDate.getValue());
//						licObj.setDuration(ibDuration.getValue());
//						licObj.setEndDate(dbEndDate.getValue());
//						licObj.setStatus(cbStatus.getValue());
//						licenseDetTable.getDataprovider().getList().add(licObj);
//					}
//					company.setLicenseDetailsList(licenseDetTable.getValue());
//					if(tbLicenseType.getValue().equals("")&&dbStartDate.getValue()!=null&&dbEndDate.getValue()!=null&&ibNoOfLicense.getValue()!=null){
//						company.setLicenseStartDate(dbStartDate.getValue());
//						company.setLicenseEndingDate(dbEndDate.getValue());
//						company.setNoOfUser(ibNoOfLicense.getValue());
//						company.setLicenseDetailsList(null);
//						company.setAccessUrl("127");
//					}
//					if(company.getLicenseDetailsList()!=null){
//						for(int i=0;i<company.getLicenseDetailsList().size();i++){
//							if(company.getLicenseDetailsList().get(i).getContractCount()==0
//									&&company.getLicenseDetailsList().get(i).getDuration()==0){
//								company.getLicenseDetailsList().remove(i);
//								i--;
//							}
//						}
//					}
//					
//					presenter.setModel(company);
//					
//					showWaitSymbol();
//					async.save(company, new AsyncCallback<ReturnFromServer>() {
//						@Override
//						public void onSuccess(ReturnFromServer result) {
//							hideWaitSymbol();
//							showDialogMessage("License Updated Sucessfully.");
//						}
//						@Override
//						public void onFailure(Throwable caught) {
//							hideWaitSymbol();
//						}
//					});
//				}
//			}
//		});
		
		/**
		 * END
		 */
		
	}
	
	
	@Override
	public void createScreen() {
	    
		initalizeWidget();

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCompanyInformation=fbuilder.setlabel("License Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Company Name",tbcompanyName);
		FormField ftbcompanyName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("No Of Licenses",ibNoOfLicenses);
		FormField fibNoOfLicenses= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("License Start Date",dbLicenseStartDate);
		FormField fdbLicenseStartDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("License End Date",dbLicenseEndDate);
		FormField fdblicenseEndDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",licenseDetTable.getTable());
		FormField flicenseDetTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder(" ",hp);
		FormField fhp= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		  FormField[][] formfield = {
			
		  {fgroupingCompanyInformation},
		  {ftbcompanyName,fibNoOfLicenses},
		  {fdbLicenseStartDate,fdblicenseEndDate},
		  {fhp},
		  {flicenseDetTable},
		  };

		  this.fields=formfield;
	}
	
	
	@Override
	public void updateModel(Company model) 
	{
	}

	@Override
	public void updateView(Company view) 
	{
	}

	

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.LICENSEMANAGEMENT,LoginPresenter.currentModule.trim());
	}
	
	
	@Override
	public void setCount(int count)
	{
	}
	
	

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		dbLicenseStartDate.setEnabled(false);
		tbcompanyName.setEnabled(false);
		dbLicenseEndDate.setEnabled(false);
		ibNoOfLicenses.setEnabled(false);
		licenseDetTable.setEnable(false);
	}

	
	/*************************************Getters And Setters*************************************/
	
	public TextBox getTbcompanyName() {
		return tbcompanyName;
	}

	public void setTbcompanyName(TextBox tbcompanyName) {
		this.tbcompanyName = tbcompanyName;
	}

	public IntegerBox getIbNoOfLicenses() {
		return ibNoOfLicenses;
	}

	public void setIbNoOfLicenses(IntegerBox ibNoOfLicenses) {
		this.ibNoOfLicenses = ibNoOfLicenses;
	}

	public DateBox getDbLicenseStartDate() {
		return dbLicenseStartDate;
	}

	public void setDbLicenseStartDate(DateBox dbLicenseStartDate) {
		this.dbLicenseStartDate = dbLicenseStartDate;
	}

	public DateBox getDbLicenseEndDate() {
		return dbLicenseEndDate;
	}

	public void setDbLicenseEndDate(DateBox dbLicenseEndDate) {
		this.dbLicenseEndDate = dbLicenseEndDate;
	}


	

}
