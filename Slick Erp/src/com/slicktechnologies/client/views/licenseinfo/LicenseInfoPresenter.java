package com.slicktechnologies.client.views.licenseinfo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Calendar;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;

public class LicenseInfoPresenter extends FormScreenPresenter<Company> {
	
	public LicenseInfoForm form;
	GenricServiceAsync async =GWT.create(GenricService.class);
	
	public LicenseInfoPresenter(FormScreen<Company> view, Company model) {
		super(view, model);
		form=(LicenseInfoForm) view;
		
		form.showWaitSymbol();
		Timer timer=new Timer() {
				
				@Override
				public void run() {
					retriveLicenseDetails();
			    
			             
				}
			};
			 timer.schedule(5000); 
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
	InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
	}

	@Override
	public void reactOnPrint() {
	}

	@Override
	public void reactOnEmail() {
		
	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
	}
	
	public void reactOnDownload()
    {
    }
	
	
	public static void initalize()
	{
		LicenseInfoForm  form=new  LicenseInfoForm();
		LicenseInfoPresenter  presenter=new LicenseInfoPresenter(form,new Company());
		AppMemory.getAppMemory().stickPnel(form);
	}

	private void retriveLicenseDetails()
	{
		  MyQuerry myQuer= new MyQuerry();
		  myQuer.setQuerryObject(new Company());
		  
		  async.getSearchResult(myQuer,new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				model=(Company) result.get(0);
				form.getTbcompanyName().setValue(model.getBusinessUnitName());
				form.getIbNoOfLicenses().setValue(model.getNoOfUser());
				form.getDbLicenseStartDate().setValue(model.getLicenseStartDate());
				if(model.getLicenseEndingDate()!=null){
					form.getDbLicenseEndDate().setValue(model.getLicenseEndingDate());
				}
				else{
					int weeks=model.getNoOfWeeks();
					Date startDate=model.getLicenseStartDate();
					CalendarUtil.addDaysToDate(startDate, weeks);
					form.getDbLicenseEndDate().setValue(startDate);
				}
				
				/**
				 * Date : 05-04-2018 By ANIL
				 */
//				if(model.getLicenseDetailsList()==null||model.getLicenseDetailsList().size()==0){
//					LicenseDetails license=new LicenseDetails();
//					license.setLicenseType("EVA ERP License");
//					license.setNoOfLicense(5);
//					license.setStartDate(new Date());
//					license.setDuration(365);
//					Date endDate=new Date();
//					CalendarUtil.addDaysToDate(endDate, 365);
//					license.setEndDate(endDate);
//					model.getLicenseDetailsList().add(license);
//					service.save(model, new AsyncCallback<ReturnFromServer>() {
//						@Override
//						public void onFailure(Throwable caught) {
//						}
//						@Override
//						public void onSuccess(ReturnFromServer result) {
//							
//						}
//					});
//					
//				}
				if(model.getLicenseDetailsList()!=null){
//					form.licenseDetTable.getDataprovider().setList(model.getLicenseDetailsList());
					/**Added by Sheetal : 14-03-2022
					 * Des : Loading only active licenses information in license details table **/
					
                    ArrayList<LicenseDetails> licenseDetailslist= new  ArrayList<LicenseDetails>();
                    licenseDetailslist=model.getLicenseDetailsList();
                    ArrayList<LicenseDetails> activelicenseDetailslist =new  ArrayList<LicenseDetails>();
                    Date todaysdate=new Date();
                    for(LicenseDetails licenseDetails : licenseDetailslist){
                  
                    Console.log("license end date== "+licenseDetails.getEndDate());
                    Console.log("todays date== "+todaysdate);
                    if(licenseDetails.getEndDate().equals(todaysdate)||licenseDetails.getEndDate().after(todaysdate)){
                    	
                    	activelicenseDetailslist.add(licenseDetails);
                    }
                    Console.log("inside if of active license== "+activelicenseDetailslist.size());
                    }
                    form.licenseDetTable.getDataprovider().setList(activelicenseDetailslist);
				}/**end**/
				
				/**
				 * End
				 */
			}
		});
	}
	
	
	
}
