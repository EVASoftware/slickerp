package com.slicktechnologies.client.views.licenseinfo;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;

public class LicenseDetailsTable extends SuperTable<LicenseDetails>{
	
	TextColumn<LicenseDetails> getColLicenseType;
	TextColumn<LicenseDetails> getColNoOfLicense;
	TextColumn<LicenseDetails> getColStartDate;
	TextColumn<LicenseDetails> getColDuration;
	TextColumn<LicenseDetails> getColEndDate;
	
	TextColumn<LicenseDetails> getColLicenseStatus;
	TextColumn<LicenseDetails> getColContractCount;
	
	Column<LicenseDetails,String> getColDelete;

	@Override
	public void createTable() {
		getColContractCount();
		getColLicenseType();
		getColNoOfLicense();
		getColStartDate();
		getColDuration();
		getColEndDate();
		getColLicenseStatus();
		getColDelete();
		
	}

	private void getColDelete() {
		ButtonCell cell=new ButtonCell();
		getColDelete=new Column<LicenseDetails, String>(cell) {
			@Override
			public String getValue(LicenseDetails object) {
				// TODO Auto-generated method stub
				return "Delete";
			}
		};
		table.addColumn(getColDelete,"");
		
		getColDelete.setFieldUpdater(new FieldUpdater<LicenseDetails, String>() {
			
			@Override
			public void update(int index, LicenseDetails object, String value) {
				getDataprovider().getList().remove(index);
				table.redraw();
			}
		});
	}

	private void getColLicenseStatus() {
		getColLicenseStatus=new TextColumn<LicenseDetails>() {
			@Override
			public String getValue(LicenseDetails object) {
				if(object.getStatus()==true){
					return "Active";
				}
				return "Inactive";
			}
		};
		table.addColumn(getColLicenseStatus,"Status");
	}

	private void getColContractCount() {
		getColContractCount=new TextColumn<LicenseDetails>() {
			@Override
			public String getValue(LicenseDetails object) {
				if(object.getContractCount()!=0){
					return object.getContractCount()+"";
				}
				return "";
			}
		};
		table.addColumn(getColContractCount,"Contract Id");
	}

	private void getColLicenseType() {
		getColLicenseType=new TextColumn<LicenseDetails>() {
			@Override
			public String getValue(LicenseDetails object) {
				if(object.getLicenseType()!=null){
					return object.getLicenseType();
				}
				return "";
			}
		};
		table.addColumn(getColLicenseType,"License Type");
		table.setColumnWidth(getColLicenseType, "120px");

	}

	private void getColNoOfLicense() {
		getColNoOfLicense=new TextColumn<LicenseDetails>() {
			@Override
			public String getValue(LicenseDetails object) {
				if(object.getNoOfLicense()!=0){
					return object.getNoOfLicense()+"";
				}
				return "";
			}
		};
		table.addColumn(getColNoOfLicense,"No. Of License");
	}

	private void getColStartDate() {
		getColStartDate=new TextColumn<LicenseDetails>() {
			@Override
			public String getValue(LicenseDetails object) {
				if(object.getStartDate()!=null){
					return AppUtility.parseDate(object.getStartDate());
				}
				return "";
			}
		};
		table.addColumn(getColStartDate,"License Start Date");
	}

	private void getColDuration() {
		getColDuration=new TextColumn<LicenseDetails>() {
			@Override
			public String getValue(LicenseDetails object) {
				if(object.getDuration()!=0){
					return object.getDuration()+"";
				}
				return "";
			}
		};
		table.addColumn(getColDuration,"Duration");
	}

	private void getColEndDate() {
		getColEndDate=new TextColumn<LicenseDetails>() {
			@Override
			public String getValue(LicenseDetails object) {
				if(object.getEndDate()!=null){
					return AppUtility.parseDate(object.getEndDate());
				}
				return "";
			}
		};
		table.addColumn(getColEndDate,"License End Date");
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true){
			getColContractCount();
			getColLicenseType();
			getColNoOfLicense();
			getColStartDate();
			getColDuration();
			getColEndDate();
			getColLicenseStatus();
			getColDelete();
		}
		if (state == false){
			getColContractCount();
			getColLicenseType();
			getColNoOfLicense();
			getColStartDate();
			getColDuration();
			getColEndDate();
			getColLicenseStatus();
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	

}
