package com.slicktechnologies.client.views.shipping.packingmethod;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.views.shipping.shippingmethod.ShippingMethodForm;
import com.slicktechnologies.client.views.shipping.shippingmethod.ShippingMethodPresenter;
import com.slicktechnologies.client.views.shipping.shippingmethod.ShippingMethodPresenterTableProxy;
import com.slicktechnologies.client.views.shipping.shippingmethod.ShippingMethodPresenter.ShippingMethodPresenterTable;
import com.slicktechnologies.shared.common.shippingpackingprocess.PackingMethod;
import com.slicktechnologies.shared.common.shippingpackingprocess.ShippingMethod;

public class PackingMethodPresenter extends FormTableScreenPresenter<PackingMethod> {
	
	PackingMethodForm form;
	
	public PackingMethodPresenter (FormTableScreen<PackingMethod> view,
			PackingMethod model) {
		super(view, model);
		form=(PackingMethodForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getPackingMethodQuery());
		form.setPresenter(this);
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel lbl= (InlineLabel) e.getSource();
		
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
	}
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new PackingMethod();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getPackingMethodQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new PackingMethod());
		return quer;
	}
	
	public void setModel(PackingMethod entity)
	{
		model=entity;
	}
	
	public static void initalize()
	{
		PackingMethodPresenterTable gentableScreen=new PackingMethodPresenterTableProxy();
		PackingMethodForm  form=new  PackingMethodForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		PackingMethodPresenter  presenter=new  PackingMethodPresenter(form,new PackingMethod());
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.shippingpackingprocess.PackingMethod")
		 public static class PackingMethodPresenterTable extends SuperTable<PackingMethod> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;


}
