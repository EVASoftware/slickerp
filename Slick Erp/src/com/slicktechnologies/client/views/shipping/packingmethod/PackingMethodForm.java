package com.slicktechnologies.client.views.shipping.packingmethod;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;
import com.slicktechnologies.shared.common.shippingpackingprocess.PackingMethod;

public class PackingMethodForm extends FormTableScreen<PackingMethod> {
	
	IntegerBox ibpackingMethodId;
	TextBox tbpackingMethodName;
	ObjectListBox<CheckListType> olbchecklisttype;
	CheckBox cbshippingStatus;
	TextArea tadescription;
	
	public PackingMethodForm(SuperTable<PackingMethod> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
	}

	private void initalizeWidget()
	{
		ibpackingMethodId=new IntegerBox();
		ibpackingMethodId.setEnabled(false);
		tbpackingMethodName=new TextBox();
		tadescription=new TextArea();
		cbshippingStatus=new CheckBox();
		cbshippingStatus.setValue(true);
		olbchecklisttype=new ObjectListBox<CheckListType>();
		CheckListType.initializeTypeName(olbchecklisttype);
	}
	
	
	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingMethodInformation=fbuilder.setlabel("Shipping Method Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Packing Method ID",ibpackingMethodId);
		FormField fibpackingMethodId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Packing Method Name",tbpackingMethodName);
		FormField ftbpackingMethodName= fbuilder.setMandatory(true).setMandatoryMsg("Packing Method Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("CheckList Type",olbchecklisttype);
		FormField folbchecklisttype= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbshippingStatus);
		FormField fcbshippingStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Description",tadescription);
		FormField ftadescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();


		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingMethodInformation},
				{fibpackingMethodId,ftbpackingMethodName,folbchecklisttype,fcbshippingStatus},
				{ftadescription},
		};

		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(PackingMethod model) 
	{
		if(tbpackingMethodName.getValue()!=null)
			model.setPackingMethodName(tbpackingMethodName.getValue());
		if(olbchecklisttype.getValue()!=null)
			model.setCheckListName(olbchecklisttype.getValue());
		if(cbshippingStatus.getValue()!=null)
			model.setStatus(cbshippingStatus.getValue());
		if(tadescription.getValue()!=null)
			model.setDescription(tadescription.getValue());
		
		presenter.setModel(model);
 	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(PackingMethod view) 
	{
		ibpackingMethodId.setValue(view.getCount());
		if(view.getPackingMethodName()!=null)
			tbpackingMethodName.setValue(view.getPackingMethodName());
		if(view.getDescription()!=null)
			tadescription.setValue(view.getDescription());
		if(view.getCheckListName()!=null)
			olbchecklisttype.setValue(view.getCheckListName());
		if(view.isStatus()!=null)
			cbshippingStatus.setValue(view.isStatus());
		

		presenter.setModel(view);

	}
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CHECKLISTTYPE,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibpackingMethodId.setEnabled(false);
		cbshippingStatus.setValue(true);
	}

}
