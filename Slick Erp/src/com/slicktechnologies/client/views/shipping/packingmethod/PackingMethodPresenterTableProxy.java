package com.slicktechnologies.client.views.shipping.packingmethod;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.views.shipping.packingmethod.PackingMethodPresenter.PackingMethodPresenterTable;
import com.slicktechnologies.shared.common.shippingpackingprocess.PackingMethod;

public class PackingMethodPresenterTableProxy extends PackingMethodPresenterTable {
	
	TextColumn<PackingMethod> getCountColumn;
	TextColumn<PackingMethod> getPackingMethodName;
	TextColumn<PackingMethod> getChecklistName;
	TextColumn<PackingMethod> getStatusColumn;
	
	
	public PackingMethodPresenterTableProxy()
	{
		super();
	}
	
	@Override public void createTable() {
		addColumngetCount();
		addColumngetMethodName();
		addColumngetChecklistName();
		addColumngetStatus();
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<PackingMethod>()
				{
			@Override
			public Object getKey(PackingMethod item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	
	@Override
	public void setEnable(boolean state)
	{
	}
	
	@Override
	public void applyStyle()
	{
	}
	
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetMethodName();
		addSortinggetChecklistName();
		addSortinggetStatus();
	}
	
	protected void addSortinggetCount()
	{
		List<PackingMethod> list=getDataprovider().getList();
		columnSort=new ListHandler<PackingMethod>(list);
		columnSort.setComparator(getCountColumn, new Comparator<PackingMethod>()
				{
			@Override
			public int compare(PackingMethod e1,PackingMethod e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<PackingMethod>()
				{
			@Override
			public String getValue(PackingMethod object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				getCountColumn.setSortable(true);
	}
	
	
	protected void addSortinggetMethodName()
	{
		List<PackingMethod> list=getDataprovider().getList();
		columnSort=new ListHandler<PackingMethod>(list);
		columnSort.setComparator(getPackingMethodName, new Comparator<PackingMethod>()
				{
			@Override
			public int compare(PackingMethod e1,PackingMethod e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPackingMethodName()!=null && e2.getPackingMethodName()!=null){
						return e1.getPackingMethodName().compareTo(e2.getPackingMethodName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetMethodName()
	{
		getPackingMethodName=new TextColumn<PackingMethod>()
		{
			@Override
			public String getValue(PackingMethod object)
			{
				return object.getPackingMethodName();
			}
				};
				table.addColumn(getPackingMethodName,"Packing Method Name");
				getPackingMethodName.setSortable(true);
	}
	
	
	protected void addSortinggetChecklistName()
	{
		List<PackingMethod> list=getDataprovider().getList();
		columnSort=new ListHandler<PackingMethod>(list);
		columnSort.setComparator(getChecklistName, new Comparator<PackingMethod>()
				{
			@Override
			public int compare(PackingMethod e1,PackingMethod e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCheckListName()!=null && e2.getCheckListName()!=null){
						return e1.getCheckListName().compareTo(e2.getCheckListName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetChecklistName()
	{
		getChecklistName=new TextColumn<PackingMethod>()
		{
			@Override
			public String getValue(PackingMethod object)
			{
				return object.getCheckListName();
			}
				};
				table.addColumn(getChecklistName,"CheckList Name");
				getChecklistName.setSortable(true);
	}
	
	protected void addSortinggetStatus()
	{
		List<PackingMethod> list=getDataprovider().getList();
		columnSort=new ListHandler<PackingMethod>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<PackingMethod>()
				{
			@Override
			public int compare(PackingMethod e1,PackingMethod e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.isStatus()== e2.isStatus()){
						return 0;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<PackingMethod>()
				{
			@Override
			public String getValue(PackingMethod object)
			{
				if( object.isStatus()==true)
					return "ACTIVE";
				else 
					return "INACTIVE";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				getStatusColumn.setSortable(true);
	}

}
