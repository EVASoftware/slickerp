package com.slicktechnologies.client.views.shipping.inspectionmethod;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.shippingpackingprocess.InspectionMethod;

public class InspectionMethodPresenter extends FormTableScreenPresenter<InspectionMethod> {
	
	InspectionMethodForm form;
	
	public InspectionMethodPresenter (FormTableScreen<InspectionMethod> view,
			InspectionMethod model) {
		super(view, model);
		form=(InspectionMethodForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getShippingMethodQuery());
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.INSPECTIONMETHOD,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel lbl= (InlineLabel) e.getSource();
		
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
	}
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new InspectionMethod();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getShippingMethodQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new InspectionMethod());
		return quer;
	}
	
	
	public static void initalize()
	{
		InspectionMethodPresenterTable gentableScreen=new InspectionMethodPresenterTableProxy();
		InspectionMethodForm  form=new  InspectionMethodForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		InspectionMethodPresenter  presenter=new  InspectionMethodPresenter(form,new InspectionMethod());
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.shippingpackingprocess.InspectionMethod")
		 public static class InspectionMethodPresenterTable extends SuperTable<InspectionMethod> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
	
	
	
}
