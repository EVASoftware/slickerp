package com.slicktechnologies.client.views.shipping.inspectionmethod;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.views.shipping.inspectionmethod.InspectionMethodPresenter.InspectionMethodPresenterTable;
import com.slicktechnologies.shared.common.shippingpackingprocess.InspectionMethod;

public class InspectionMethodPresenterTableProxy extends InspectionMethodPresenterTable {
	
	TextColumn<InspectionMethod> getCountColumn;
	TextColumn<InspectionMethod> getInspectionMethodName;
	TextColumn<InspectionMethod> getChecklistName;
	TextColumn<InspectionMethod> getStatusColumn;
	
	
	public InspectionMethodPresenterTableProxy()
	{
		super();
	}
	
	@Override public void createTable() {
		addColumngetCount();
		addColumngetMethodName();
		addColumngetChecklistName();
		addColumngetStatus();
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<InspectionMethod>()
				{
			@Override
			public Object getKey(InspectionMethod item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	
	@Override
	public void setEnable(boolean state)
	{
	}
	
	@Override
	public void applyStyle()
	{
	}
	
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetMethodName();
		addSortinggetChecklistName();
		addSortinggetStatus();
	}
	
	protected void addSortinggetCount()
	{
		List<InspectionMethod> list=getDataprovider().getList();
		columnSort=new ListHandler<InspectionMethod>(list);
		columnSort.setComparator(getCountColumn, new Comparator<InspectionMethod>()
				{
			@Override
			public int compare(InspectionMethod e1,InspectionMethod e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<InspectionMethod>()
				{
			@Override
			public String getValue(InspectionMethod object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				getCountColumn.setSortable(true);
	}
	
	
	protected void addSortinggetMethodName()
	{
		List<InspectionMethod> list=getDataprovider().getList();
		columnSort=new ListHandler<InspectionMethod>(list);
		columnSort.setComparator(getInspectionMethodName, new Comparator<InspectionMethod>()
				{
			@Override
			public int compare(InspectionMethod e1,InspectionMethod e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInspectionMethodName()!=null && e2.getInspectionMethodName()!=null){
						return e1.getInspectionMethodName().compareTo(e2.getInspectionMethodName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetMethodName()
	{
		getInspectionMethodName=new TextColumn<InspectionMethod>()
		{
			@Override
			public String getValue(InspectionMethod object)
			{
				return object.getInspectionMethodName();
			}
				};
				table.addColumn(getInspectionMethodName,"Inspection Method Name");
				getInspectionMethodName.setSortable(true);
	}
	
	
	protected void addSortinggetChecklistName()
	{
		List<InspectionMethod> list=getDataprovider().getList();
		columnSort=new ListHandler<InspectionMethod>(list);
		columnSort.setComparator(getChecklistName, new Comparator<InspectionMethod>()
				{
			@Override
			public int compare(InspectionMethod e1,InspectionMethod e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCheckListName()!=null && e2.getCheckListName()!=null){
						return e1.getCheckListName().compareTo(e2.getCheckListName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetChecklistName()
	{
		getChecklistName=new TextColumn<InspectionMethod>()
		{
			@Override
			public String getValue(InspectionMethod object)
			{
				return object.getCheckListName();
			}
				};
				table.addColumn(getChecklistName,"CheckList Name");
				getChecklistName.setSortable(true);
	}
	
	protected void addSortinggetStatus()
	{
		List<InspectionMethod> list=getDataprovider().getList();
		columnSort=new ListHandler<InspectionMethod>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<InspectionMethod>()
				{
			@Override
			public int compare(InspectionMethod e1,InspectionMethod e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.isStatus()== e2.isStatus()){
						return 0;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<InspectionMethod>()
				{
			@Override
			public String getValue(InspectionMethod object)
			{
				if( object.isStatus()==true)
					return "ACTIVE";
				else 
					return "INACTIVE";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				getStatusColumn.setSortable(true);
	}


}
