package com.slicktechnologies.client.views.shipping.checkliststep;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListStep;

public class CheckListStepForm extends FormTableScreen<CheckListStep> {
	
	IntegerBox ibcheckListId;
	IntegerBox ibStepNumber;
	TextBox tbStepCode,tbStepName;
	TextArea taStepDesc;
	CheckBox cbStatus,cbMandatory;


	public CheckListStepForm(SuperTable<CheckListStep> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
	}

	private void initalizeWidget()
	{
		ibcheckListId=new IntegerBox();
		ibcheckListId.setEnabled(false);
		ibStepNumber=new IntegerBox();
		tbStepCode=new TextBox();
		tbStepName=new TextBox();
		taStepDesc=new TextArea();
		cbStatus=new CheckBox();
		cbStatus.setValue(true);
		cbMandatory=new CheckBox();
	}
	
	
	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCheckListInformation=fbuilder.setlabel("Checklist Step Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Checklist Step ID",ibcheckListId);
		FormField fibcheckListId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Step Number",ibStepNumber);
		FormField fibStepNumber= fbuilder.setMandatory(true).setMandatoryMsg("Step Number is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Step Code",tbStepCode);
		FormField ftbStepCode= fbuilder.setMandatory(true).setMandatoryMsg("Step Code is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Step Name",tbStepName);
		FormField ftbStepName= fbuilder.setMandatory(true).setMandatoryMsg("Step Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Is Mandatory",cbMandatory);
		FormField fcbMandatory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Descripition",taStepDesc);
		FormField ftaStepDesc= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();


		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingCheckListInformation},
				{fibcheckListId,fibStepNumber,ftbStepCode,ftbStepName},
				{fcbMandatory,fcbStatus},
				{ftaStepDesc},
		};

		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(CheckListStep model) 
	{

			model.setStepNumber(ibStepNumber.getValue());
		if(tbStepCode.getValue()!=null)
			model.setStepCode(tbStepCode.getValue());
		if(tbStepName.getValue()!=null)
			model.setStepName(tbStepName.getValue());
		if(cbStatus.getValue()!=null)
			model.setStepStatus(cbStatus.getValue());
		if(cbMandatory.getValue()!=null)
			model.setStepMandatory(cbMandatory.getValue());
		if(taStepDesc.getValue()!=null)
			model.setStepDescription(taStepDesc.getValue());

		presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(CheckListStep view) 
	{
		ibcheckListId.setValue(view.getCount());
		ibStepNumber.setValue(view.getStepNumber());
		if(view.getStepCode()!=null){
			tbStepCode.setValue(view.getStepCode());
		}
		if(view.getStepName()!=null){
			tbStepName.setValue(view.getStepName());
		}
		if(view.getStepDescription()!=null){
			taStepDesc.setValue(view.getStepDescription());
		}
		if(view.isStepStatus()!=null){
			cbStatus.setValue(view.isStepStatus());
		}
		if(view.isStepMandatory()!=null){
			cbMandatory.setValue(view.isStepMandatory());
		}

		presenter.setModel(view);

	}

	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CHECKLISTSTEP,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibcheckListId.setEnabled(false);
		cbStatus.setValue(true);
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		processLevelBar.setVisibleFalse(false);
		
	}

}
