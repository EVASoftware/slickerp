package com.slicktechnologies.client.views.shipping.checkliststep;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.views.shipping.checkliststep.CheckListStepPresenter.CheckListStepPresenterTable;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListStep;

public class CheckListStepPresenterTableProxy extends CheckListStepPresenterTable {
	
	TextColumn<CheckListStep> getCountColumn;
	TextColumn<CheckListStep> getStepCodeColumn;
	TextColumn<CheckListStep> getStepNameColumn;
	TextColumn<CheckListStep> getStepMandatoryColumn;
	TextColumn<CheckListStep> getStepStatusColumn;
	TextColumn<CheckListStep> getStepNumberColumn;
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		if(varName.equals("getStepCodeColumn"))
			return this.getStepCodeColumn;
		if(varName.equals("getStepNameColumn"))
			return this.getStepNameColumn;
		if(varName.equals("getStepMandatoryColumn"))
			return this.getStepMandatoryColumn;
		if(varName.equals("getStepStatusColumn"))
			return this.getStepStatusColumn;
		if(varName.equals("getStepNumberColumn"))
			return this.getStepNumberColumn;
		return null ;
	}
	
	public CheckListStepPresenterTableProxy()
	{
		super();
	}
	
	@Override public void createTable() {
		addColumngetCount();
		addColumngetStepNumber();
		addColumngetStepCode();
		addColumngetStepName();
		addColumngetStepMandatory();
		addColumngetStatus();
	}
	
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<CheckListStep>()
				{
			@Override
			public Object getKey(CheckListStep item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	
	@Override
	public void setEnable(boolean state)
	{
	}
	
	@Override
	public void applyStyle()
	{
	}
	
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetStepNumber();
		addSortinggetStepCode();
		addSortinggetStepName();
		addSortinggetStepMandatory();
		addSortinggetStatus();
	}
	
	protected void addSortinggetCount()
	{
		List<CheckListStep> list=getDataprovider().getList();
		columnSort=new ListHandler<CheckListStep>(list);
		columnSort.setComparator(getCountColumn, new Comparator<CheckListStep>()
				{
			@Override
			public int compare(CheckListStep e1,CheckListStep e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<CheckListStep>()
				{
			@Override
			public String getValue(CheckListStep object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				getCountColumn.setSortable(true);
	}
	
	
	protected void addSortinggetStepNumber()
	{
		List<CheckListStep> list=getDataprovider().getList();
		columnSort=new ListHandler<CheckListStep>(list);
		columnSort.setComparator(getStepNumberColumn, new Comparator<CheckListStep>()
				{
			@Override
			public int compare(CheckListStep e1,CheckListStep e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetStepNumber()
	{
		getStepNumberColumn=new TextColumn<CheckListStep>()
				{
			@Override
			public String getValue(CheckListStep object)
			{
				if( object.getStepNumber()==0)
					return "";
				else return object.getStepNumber()+"";
			}
				};
				table.addColumn(getStepNumberColumn,"Step Number");
				getStepNumberColumn.setSortable(true);
	}
	
	protected void addSortinggetStepCode()
	{
		List<CheckListStep> list=getDataprovider().getList();
		columnSort=new ListHandler<CheckListStep>(list);
		columnSort.setComparator(getStepCodeColumn, new Comparator<CheckListStep>()
				{
			@Override
			public int compare(CheckListStep e1,CheckListStep e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStepCode()!=null && e2.getStepCode()!=null){
						return e1.getStepCode().compareTo(e2.getStepCode());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStepCode()
	{
		getStepCodeColumn=new TextColumn<CheckListStep>()
				{
			@Override
			public String getValue(CheckListStep object)
			{
				return object.getStepCode();
			}
				};
				table.addColumn(getStepCodeColumn,"Step Code");
				getStepCodeColumn.setSortable(true);
	}
	
	protected void addSortinggetStepName()
	{
		List<CheckListStep> list=getDataprovider().getList();
		columnSort=new ListHandler<CheckListStep>(list);
		columnSort.setComparator(getStepNameColumn, new Comparator<CheckListStep>()
				{
			@Override
			public int compare(CheckListStep e1,CheckListStep e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStepName()!=null && e2.getStepName()!=null){
						return e1.getStepName().compareTo(e2.getStepName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStepName()
	{
		getStepNameColumn=new TextColumn<CheckListStep>()
				{
			@Override
			public String getValue(CheckListStep object)
			{
				return object.getStepName()+"";
			}
				};
				table.addColumn(getStepNameColumn,"Step Name");
				getStepNameColumn.setSortable(true);
	}
	
	protected void addSortinggetStepMandatory()
	{
		List<CheckListStep> list=getDataprovider().getList();
		columnSort=new ListHandler<CheckListStep>(list);
		columnSort.setComparator(getStepMandatoryColumn, new Comparator<CheckListStep>()
				{
			@Override
			public int compare(CheckListStep e1,CheckListStep e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.isStepMandatory()== e2.isStepMandatory()){
						return 0;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStepMandatory()
	{
		getStepMandatoryColumn=new TextColumn<CheckListStep>()
				{
			@Override
			public String getValue(CheckListStep object)
			{
				if( object.isStepMandatory()==true)
					return "YES";
				else 
					return "NO";
			}
				};
				table.addColumn(getStepMandatoryColumn,"Is Mandatory");
				getStepMandatoryColumn.setSortable(true);
	}
	
	protected void addSortinggetStatus()
	{
		List<CheckListStep> list=getDataprovider().getList();
		columnSort=new ListHandler<CheckListStep>(list);
		columnSort.setComparator(getStepStatusColumn, new Comparator<CheckListStep>()
				{
			@Override
			public int compare(CheckListStep e1,CheckListStep e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.isStepStatus()== e2.isStepStatus()){
						return 0;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStepStatusColumn=new TextColumn<CheckListStep>()
				{
			@Override
			public String getValue(CheckListStep object)
			{
				if( object.isStepStatus()==true)
					return "ACTIVE";
				else 
					return "INACTIVE";
			}
				};
				table.addColumn(getStepStatusColumn,"Status");
				getStepStatusColumn.setSortable(true);
	}


}
