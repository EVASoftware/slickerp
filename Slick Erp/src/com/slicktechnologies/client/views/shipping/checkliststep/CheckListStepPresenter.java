package com.slicktechnologies.client.views.shipping.checkliststep;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListStep;

public class CheckListStepPresenter extends FormTableScreenPresenter<CheckListStep> {
	
	 CheckListStepForm form;
		
		public CheckListStepPresenter (FormTableScreen<CheckListStep> view,
				CheckListStep model) {
			super(view, model);
			form=(CheckListStepForm) view;
			form.getSupertable().connectToLocal();
			form.retriveTable(getChecklistStepQuery());
			form.setPresenter(this);
		}

		/**
		 * Method template to set the processBar events
		 */
		@Override
		public void reactToProcessBarEvents(ClickEvent e) 
	   {
			InlineLabel lbl= (InlineLabel) e.getSource();
			
			if(lbl.getText().contains("New"))
			{
				form.setToNewState();
				this.initalize();
			}
		}
		
		

		@Override
		public void reactOnPrint() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void reactOnEmail() {
			// TODO Auto-generated method stub
			
		}

		/**
		 * Method template to set new model
		 */
		@Override
		protected void makeNewModel() {
			
			model=new CheckListStep();
		}
		
		/*
		 * Method template to set Myquerry object
		 */
		public MyQuerry getChecklistStepQuery()
		{
			MyQuerry quer=new MyQuerry(new Vector<Filter>(), new CheckListStep());
			return quer;
		}
		
		public void setModel(CheckListStep entity)
		{
			model=entity;
		}
		
		public static void initalize()
		{
				CheckListStepPresenterTable gentableScreen=new CheckListStepPresenterTableProxy();
				CheckListStepForm  form=new  CheckListStepForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
				gentableScreen.setView(form);
				gentableScreen.applySelectionModle();
				CheckListStepPresenter  presenter=new  CheckListStepPresenter(form,new CheckListStep());
				AppMemory.getAppMemory().stickPnel(form);
		}
		
		
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.shippingpackingprocess.CheckListStep")
			 public static class  CheckListStepPresenterTable extends SuperTable<CheckListStep> implements GeneratedVariableRefrence{

				@Override
				public Object getVarRef(String varName) {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public void createTable() {
					// TODO Auto-generated method stub
					
				}

				@Override
				protected void initializekeyprovider() {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void addFieldUpdater() {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void setEnable(boolean state) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void applyStyle() {
					// TODO Auto-generated method stub
					
				}} ;
				

}
