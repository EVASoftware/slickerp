package com.slicktechnologies.client.views.shipping.shippingmethod;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;
import com.slicktechnologies.shared.common.shippingpackingprocess.ShippingMethod;

public class ShippingMethodForm extends FormTableScreen<ShippingMethod> {
	
	IntegerBox ibshippingMethodId;
	TextBox tbshippingMethodName;
	ObjectListBox<CheckListType> olbchecklisttype;
	CheckBox cbshippingStatus;
	TextArea tadescription;
	
	public ShippingMethodForm(SuperTable<ShippingMethod> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
	}

	private void initalizeWidget()
	{
		ibshippingMethodId=new IntegerBox();
		ibshippingMethodId.setEnabled(false);
		tbshippingMethodName=new TextBox();
		tadescription=new TextArea();
		cbshippingStatus=new CheckBox();
		cbshippingStatus.setValue(true);
		olbchecklisttype=new ObjectListBox<CheckListType>();
		CheckListType.initializeTypeName(olbchecklisttype);
	}
	
	
	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingMethodInformation=fbuilder.setlabel("Shipping Method Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Shipping Method ID",ibshippingMethodId);
		FormField fibshippingMethodId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Shipping Method Name",tbshippingMethodName);
		FormField ftbshippingMethodName= fbuilder.setMandatory(true).setMandatoryMsg("Shipping Method Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("CheckList Type",olbchecklisttype);
		FormField folbchecklisttype= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbshippingStatus);
		FormField fcbshippingStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Description",tadescription);
		FormField ftadescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();


		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingMethodInformation},
				{fibshippingMethodId,ftbshippingMethodName,folbchecklisttype,fcbshippingStatus},
				{ftadescription},
		};

		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(ShippingMethod model) 
	{
		if(tbshippingMethodName.getValue()!=null)
			model.setShippingMethodName(tbshippingMethodName.getValue());
		if(olbchecklisttype.getValue()!=null)
			model.setCheckListName(olbchecklisttype.getValue());
		if(cbshippingStatus.getValue()!=null)
			model.setStatus(cbshippingStatus.getValue());
		if(tadescription.getValue()!=null)
			model.setDescription(tadescription.getValue());
		
		presenter.setModel(model);
 	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(ShippingMethod view) 
	{
		ibshippingMethodId.setValue(view.getCount());
		if(view.getShippingMethodName()!=null)
			tbshippingMethodName.setValue(view.getShippingMethodName());
		if(view.getDescription()!=null)
			tadescription.setValue(view.getDescription());
		if(view.getCheckListName()!=null)
			olbchecklisttype.setValue(view.getCheckListName());
		if(view.isStatus()!=null)
			cbshippingStatus.setValue(view.isStatus());
		

		presenter.setModel(view);

	}
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SHIPPINGMETHOD,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibshippingMethodId.setEnabled(false);
		cbshippingStatus.setValue(true);
	}

}
