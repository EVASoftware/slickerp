package com.slicktechnologies.client.views.shipping.shippingmethod;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.views.shipping.shippingmethod.ShippingMethodPresenter.ShippingMethodPresenterTable;
import com.slicktechnologies.shared.common.shippingpackingprocess.ShippingMethod;

public class ShippingMethodPresenterTableProxy extends ShippingMethodPresenterTable {

	TextColumn<ShippingMethod> getCountColumn;
	TextColumn<ShippingMethod> getShippingMethodName;
	TextColumn<ShippingMethod> getChecklistName;
	TextColumn<ShippingMethod> getStatusColumn;
	
	
	public ShippingMethodPresenterTableProxy()
	{
		super();
	}
	
	@Override public void createTable() {
		addColumngetCount();
		addColumngetMethodName();
		addColumngetChecklistName();
		addColumngetStatus();
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<ShippingMethod>()
				{
			@Override
			public Object getKey(ShippingMethod item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	
	@Override
	public void setEnable(boolean state)
	{
	}
	
	@Override
	public void applyStyle()
	{
	}
	
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetMethodName();
		addSortinggetChecklistName();
		addSortinggetStatus();
	}
	
	protected void addSortinggetCount()
	{
		List<ShippingMethod> list=getDataprovider().getList();
		columnSort=new ListHandler<ShippingMethod>(list);
		columnSort.setComparator(getCountColumn, new Comparator<ShippingMethod>()
				{
			@Override
			public int compare(ShippingMethod e1,ShippingMethod e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<ShippingMethod>()
				{
			@Override
			public String getValue(ShippingMethod object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				getCountColumn.setSortable(true);
	}
	
	
	protected void addSortinggetMethodName()
	{
		List<ShippingMethod> list=getDataprovider().getList();
		columnSort=new ListHandler<ShippingMethod>(list);
		columnSort.setComparator(getShippingMethodName, new Comparator<ShippingMethod>()
				{
			@Override
			public int compare(ShippingMethod e1,ShippingMethod e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getShippingMethodName()!=null && e2.getShippingMethodName()!=null){
						return e1.getShippingMethodName().compareTo(e2.getShippingMethodName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetMethodName()
	{
		getShippingMethodName=new TextColumn<ShippingMethod>()
		{
			@Override
			public String getValue(ShippingMethod object)
			{
				return object.getShippingMethodName();
			}
				};
				table.addColumn(getShippingMethodName,"Shipping Method Name");
				getShippingMethodName.setSortable(true);
	}
	
	
	protected void addSortinggetChecklistName()
	{
		List<ShippingMethod> list=getDataprovider().getList();
		columnSort=new ListHandler<ShippingMethod>(list);
		columnSort.setComparator(getChecklistName, new Comparator<ShippingMethod>()
				{
			@Override
			public int compare(ShippingMethod e1,ShippingMethod e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCheckListName()!=null && e2.getCheckListName()!=null){
						return e1.getCheckListName().compareTo(e2.getCheckListName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetChecklistName()
	{
		getChecklistName=new TextColumn<ShippingMethod>()
		{
			@Override
			public String getValue(ShippingMethod object)
			{
				return object.getCheckListName();
			}
				};
				table.addColumn(getChecklistName,"CheckList Name");
				getChecklistName.setSortable(true);
	}
	
	protected void addSortinggetStatus()
	{
		List<ShippingMethod> list=getDataprovider().getList();
		columnSort=new ListHandler<ShippingMethod>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<ShippingMethod>()
				{
			@Override
			public int compare(ShippingMethod e1,ShippingMethod e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.isStatus()== e2.isStatus()){
						return 0;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<ShippingMethod>()
				{
			@Override
			public String getValue(ShippingMethod object)
			{
				if( object.isStatus()==true)
					return "ACTIVE";
				else 
					return "INACTIVE";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				getStatusColumn.setSortable(true);
	}

	
}
