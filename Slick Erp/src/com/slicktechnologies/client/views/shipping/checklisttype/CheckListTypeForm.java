package com.slicktechnologies.client.views.shipping.checklisttype;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListStep;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;
import com.slicktechnologies.shared.common.shippingpackingprocess.StepsDetails;

public class CheckListTypeForm extends FormTableScreen<CheckListType> implements ClickHandler {
	
	TextBox ibcheckListTypeId;
	TextBox tbCheckListTypeName;
	TextArea taTypeDesc;
	CheckBox cbTypeStatus;
	CheckListTypeStepsTable stepsTable;
	ObjectListBox<CheckListStep> olbStepName;
	Button addSteps;
	
	public CheckListTypeForm(SuperTable<CheckListType> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		stepsTable.connectToLocal();
	}

	private void initalizeWidget()
	{
		ibcheckListTypeId=new TextBox();
		ibcheckListTypeId.setEnabled(false);
		tbCheckListTypeName=new TextBox();
		taTypeDesc=new TextArea();
		cbTypeStatus=new CheckBox();
		cbTypeStatus.setValue(true);
		stepsTable=new CheckListTypeStepsTable();
		olbStepName=new ObjectListBox<CheckListStep>();
		CheckListStep.initializeStepName(olbStepName);
		addSteps=new Button("ADD");
		addSteps.addClickHandler(this);
	}
	
	
	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		this.processlevelBarNames=new String[]{"New"};

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCheckListInformation=fbuilder.setlabel("Checklist Type Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("Checklist Type ID",ibcheckListTypeId);
		FormField fibcheckListTypeId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Checklist Type Name",tbCheckListTypeName);
		FormField ftbCheckListTypeName= fbuilder.setMandatory(true).setMandatoryMsg("CheckList Type Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbTypeStatus);
		FormField fcbTypeStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Step Name",olbStepName);
		FormField folbStepName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",addSteps);
		FormField faddSteps= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",stepsTable.getTable());
		FormField fstepsTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();


		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingCheckListInformation},
				{fibcheckListTypeId,ftbCheckListTypeName,fcbTypeStatus},
				{folbStepName,faddSteps},
				{fstepsTable}
		};

		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(CheckListType model) 
	{
		if(tbCheckListTypeName.getValue()!=null)
			model.setCheckListName(tbCheckListTypeName.getValue());
		if(taTypeDesc.getValue()!=null)
			model.setCheckListDesc(taTypeDesc.getValue());
		if(cbTypeStatus.getValue()!=null)
			model.setStatus(cbTypeStatus.getValue());
		List<StepsDetails> lisSteps=stepsTable.getDataprovider().getList();
		ArrayList<StepsDetails> arrSteps=new ArrayList<StepsDetails>();
		arrSteps.addAll(lisSteps);
		model.setStepsInfo(arrSteps);
		
		
		presenter.setModel(model);
 	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(CheckListType view) 
	{
		ibcheckListTypeId.setValue(view.getCount()+"");
		if(view.getCheckListDesc()!=null)
			taTypeDesc.setValue(view.getCheckListDesc());
		if(view.getCheckListName()!=null)
			tbCheckListTypeName.setValue(view.getCheckListName());
		if(view.isStatus()!=null)
			cbTypeStatus.setValue(view.isStatus());
		
		stepsTable.setValue(view.getStepsInfo());

		presenter.setModel(view);

	}
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CHECKLISTTYPE,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibcheckListTypeId.setEnabled(false);
		cbTypeStatus.setValue(true);
	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource().equals(addSteps)){
		if(validateShipingSteps()){
			if(olbStepName.getSelectedIndex()!=0){
				StepsDetails stepEntity=new StepsDetails();
				CheckListStep checkEntity=olbStepName.getSelectedItem();
				stepEntity.setStepDetailsNumber(checkEntity.getStepNumber());
				stepEntity.setStepDetailsCode(checkEntity.getStepCode());
				stepEntity.setStepDetailsName(checkEntity.getStepName());
				if(checkEntity.isStepMandatory()==true){
					stepEntity.setStepDetailsMandatory(AppConstants.YES);
				}
				else{
					stepEntity.setStepDetailsMandatory(AppConstants.NO);
				}
				stepsTable.getDataprovider().getList().add(stepEntity);
			}
			if(olbStepName.getSelectedIndex()==0){
				showDialogMessage("Please select Step Name!");
			}
			
		}
		}
	}
	

	
	public boolean validateShipingSteps(){
		if(stepsTable.getDataprovider().getList().size()!=0){
			for(int i=0;i<stepsTable.getDataprovider().getList().size();i++){
				if(stepsTable.getDataprovider().getList().get(i).getStepDetailsName().trim().equals(olbStepName.getValue(olbStepName.getSelectedIndex()).trim())){
					this.showDialogMessage("Can not add duplicate steps.");
					return false;
				}
			}
		}
		else{
			return true;
		}
		return true;
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
	}
	
	
	@Override
	public void clear() {
			super.clear();
			stepsTable.clear();
	}
	
	
	
	
	
	
	

}
