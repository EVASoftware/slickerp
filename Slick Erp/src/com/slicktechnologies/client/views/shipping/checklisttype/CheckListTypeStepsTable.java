package com.slicktechnologies.client.views.shipping.checklisttype;

import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.shippingpackingprocess.StepsDetails;

public class CheckListTypeStepsTable extends SuperTable<StepsDetails> {
	
	TextColumn<StepsDetails> getstepDetailsNumberColumn;
	TextColumn<StepsDetails> getStepDetailsCodeColumn;
	TextColumn<StepsDetails> getStepDetailsNameColumn;
	TextColumn<StepsDetails> getStepDetailsMandatoryColumn;
	
	
	public CheckListTypeStepsTable()
	{
		super();
	}
	
	@Override public void createTable() {
		addColumngetStepNumber();
		addColumngetStepsCode();
		addColumngetStepName();
		addColumngetStepMandatory();
	}
	
	
	protected void addColumngetStepNumber()
	{
		getstepDetailsNumberColumn=new TextColumn<StepsDetails>()
				{
			@Override
			public String getValue(StepsDetails object)
			{
				if( object.getStepDetailsNumber()==0)
					return "";
				else return object.getStepDetailsNumber()+"";
			}
				};
		table.addColumn(getstepDetailsNumberColumn,"Step Number");
	}
	
	protected void addColumngetStepsCode()
	{
		getStepDetailsCodeColumn=new TextColumn<StepsDetails>()
				{
			@Override
			public String getValue(StepsDetails object)
			{
				return object.getStepDetailsCode();
			}
				};
		table.addColumn(getStepDetailsCodeColumn,"Step Code");
	}
	
	protected void addColumngetStepName()
	{
		getStepDetailsNameColumn=new TextColumn<StepsDetails>()
				{
			@Override
			public String getValue(StepsDetails object)
			{
				return object.getStepDetailsName();
			}
				};
		table.addColumn(getStepDetailsNameColumn,"Step Name");
	}
	
	protected void addColumngetStepMandatory()
	{
		getStepDetailsMandatoryColumn=new TextColumn<StepsDetails>()
				{
			@Override
			public String getValue(StepsDetails object)
			{
				return object.getStepDetailsMandatory();
			}
				};
		table.addColumn(getStepDetailsMandatoryColumn,"Is Mandatory");
	}
	

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	

}
