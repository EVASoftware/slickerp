package com.slicktechnologies.client.views.shipping.checklisttype;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.views.shipping.checklisttype.CheckListTypePresenter.CheckListTypePresenterTable;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;

public class CheckListTypePresenterTableProxy extends CheckListTypePresenterTable {
	
	TextColumn<CheckListType> getCountColumn;
	TextColumn<CheckListType> getTypeNameColumn;
	TextColumn<CheckListType> getTypeStatusColumn;
	
	
	public CheckListTypePresenterTableProxy()
	{
		super();
	}
	
	@Override public void createTable() {
		addColumngetCount();
		addColumngetTypeName();
		addColumngetStatus();
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<CheckListType>()
				{
			@Override
			public Object getKey(CheckListType item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	
	@Override
	public void setEnable(boolean state)
	{
	}
	
	@Override
	public void applyStyle()
	{
	}
	
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetTypeName();
		addSortinggetStatus();
	}
	
	protected void addSortinggetCount()
	{
		List<CheckListType> list=getDataprovider().getList();
		columnSort=new ListHandler<CheckListType>(list);
		columnSort.setComparator(getCountColumn, new Comparator<CheckListType>()
				{
			@Override
			public int compare(CheckListType e1,CheckListType e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<CheckListType>()
				{
			@Override
			public String getValue(CheckListType object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				getCountColumn.setSortable(true);
	}
	
	protected void addSortinggetTypeName()
	{
		List<CheckListType> list=getDataprovider().getList();
		columnSort=new ListHandler<CheckListType>(list);
		columnSort.setComparator(getTypeNameColumn, new Comparator<CheckListType>()
				{
			@Override
			public int compare(CheckListType e1,CheckListType e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCheckListName()!=null && e2.getCheckListName()!=null){
						return e1.getCheckListName().compareTo(e2.getCheckListName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetTypeName()
	{
		getTypeNameColumn=new TextColumn<CheckListType>()
				{
			@Override
			public String getValue(CheckListType object)
			{
				return object.getCheckListName();
			}
				};
				table.addColumn(getTypeNameColumn,"Type Name");
				getTypeNameColumn.setSortable(true);
	}
	
	
	protected void addSortinggetStatus()
	{
		List<CheckListType> list=getDataprovider().getList();
		columnSort=new ListHandler<CheckListType>(list);
		columnSort.setComparator(getTypeStatusColumn, new Comparator<CheckListType>()
				{
			@Override
			public int compare(CheckListType e1,CheckListType e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.isStatus()== e2.isStatus()){
						return 0;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getTypeStatusColumn=new TextColumn<CheckListType>()
				{
			@Override
			public String getValue(CheckListType object)
			{
				if( object.isStatus()==true)
					return "ACTIVE";
				else 
					return "INACTIVE";
			}
				};
				table.addColumn(getTypeStatusColumn,"Status");
				getTypeStatusColumn.setSortable(true);
	}


}
