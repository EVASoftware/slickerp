package com.slicktechnologies.client.views.inventoryTranscationErrorUpdate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNoteForm;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNotePresenter;
import com.slicktechnologies.client.views.materialconsumptionreport.MaterialConsumptionReportForm;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;

public class InventoryTransactionErrorUpdatePresenter extends FormScreenPresenter<DummyEntityOnlyForScreen> implements ClickHandler,ValueChangeHandler<String>{

	



	public static  InventoryTransactionErrorUpdateForm form;
	
	ArrayList<MaterialProduct> minProductList = new ArrayList<MaterialProduct>();
	MaterialIssueNote minEntity= null;
	UpdateServiceAsync updateAsync = GWT.create(UpdateService.class);
	
	public InventoryTransactionErrorUpdatePresenter(UiScreen<DummyEntityOnlyForScreen> view,DummyEntityOnlyForScreen model) {
		super(view, model);
		form =(InventoryTransactionErrorUpdateForm)view;
		form.getBtnAdd().addClickHandler(this);
		form.getBtTransactions().addClickHandler(this);
		form.getBtUpdateTRansactionListIndex().addClickHandler(this);
		form.getIbMinMrnId().addValueChangeHandler(this);
		form.getBtUpdateTransaction().addClickHandler(this);
		
	}
	
	
	
	public static InventoryTransactionErrorUpdateForm initialize() {
		
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory Transaction Error Handling",Screen.INVENTORYTRANSCATIONERRORUPDATE); 
		InventoryTransactionErrorUpdateForm form=new  InventoryTransactionErrorUpdateForm();
		
		InventoryTransactionErrorUpdatePresenter  presenter=new InventoryTransactionErrorUpdatePresenter(form,new DummyEntityOnlyForScreen());
			AppMemory.getAppMemory().stickPnel(form);
		return form;
		
	}

	
	
	/**
	 * Rohan added this button for cancelling records and reversing stock to in 
	 * productInventory list and view (NBHC issue and stock update)
	 * Date : 12/1/2017 
	 */
	
		@Override
	public void reactToProcessBarEvents(ClickEvent e) {
			InlineLabel label=(InlineLabel) e.getSource();
			String text=label.getText().trim();
		
	}
	
	/**
	 * ends here 
	 */
	
	
	/**
	 * Rohan added this button for canceling records and reversing stock to in 
	 * productInventory list and view (NBHC issue and stock update)
	 * Date : 12/1/2017 
	 */
//	private void cancelRecordsAndUpdateStock() {
//		 final List<ProductInventoryTransaction> listsel=getAllSelectedRecords();
//		 
//		 Comparator<ProductInventoryTransaction> compare = new Comparator<ProductInventoryTransaction>() {
//			
//			@Override
//			public int compare(ProductInventoryTransaction o1,ProductInventoryTransaction o2) {
//				
//				Integer l1=o1.getCount();
//				Integer l2=o2.getCount();
//				
//				return l1.compareTo(l2);
//			}
//		};
//		 
//		Collections.sort(listsel, compare);
//	}
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if (event.getSource() == form.getBtnAdd()) {
			
			
			
			if(form.getIbMinMrnId().getValue().trim()!= null && !form.getIbMinMrnId().getValue().trim().equals(""))
			{
				form.getSubProductTableMin().connectToLocal();
				findMINProductDetailsFromMINid();
				
			}
			else
			{
				form.showDialogMessage("Please add MIN id to proceed..!");
			}
			
		}
		
		
		if (event.getSource() == form.getBtTransactions()) {
			
			if(form.getIbMinMrnId().getValue().trim()!= null && !form.getIbMinMrnId().getValue().trim().equals(""))
			{
				form.getProductTransactionTable().connectToLocal();
				getProductInventoryTransactionDetails();
			}
			else
			{
				form.showDialogMessage("Please add MIN id to proceed..!");
			}
		}
		
		if (event.getSource() == form.getBtUpdateTRansactionListIndex()) {
			
			updateProductInventorytransactionIndexes();
		}
		
		if (event.getSource() == form.getBtUpdateTransaction()) {
			updateProductInventoryList();
		}
	}

	/**
	 * This method is used for making inventory product data indexes Details by task queue
	 * Date : 12-01-2017
	 */

	private void updateProductInventorytransactionIndexes() {
		
		updateAsync.updateProductInventoryListIndex(UserConfiguration.getCompanyId(), new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(Void result) {
				
				form.showDialogMessage("Records Updation sucessfull");
			}
		});
	}

/**
 * ends here 
 */

	ArrayList<ProductInventoryTransaction> productInventoryList =new ArrayList<ProductInventoryTransaction>();
	private void getProductInventoryTransactionDetails() {
		form.showWaitSymbol();
		System.out.println("min product list  "+minProductList.size());
//		for (int i = 0; i < minProductList.size(); i++) {
			
			MyQuerry querry = new MyQuerry();
		  	Company c = new Company();
		  	Vector<Filter> filtervec=new Vector<Filter>();
		  	Filter filter = null;
		  	
		  	filter = new Filter();
		  	filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("documentId");
			filter.setIntValue(Integer.parseInt(form.getIbMinMrnId().getValue().trim()));
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("documentType");
			filter.setStringValue("MIN");
			filtervec.add(filter);
//			
//			filter = new Filter();
//			filter.setQuerryString("storageLocation");
//			filter.setStringValue(minProductList.get(i).getMaterialProductStorageLocation());
//			filtervec.add(filter);
//			
//			filter = new Filter();
//			filter.setQuerryString("storageBin");
//			filter.setStringValue(minProductList.get(i).getMaterialProductStorageBin());
//			filtervec.add(filter);
			
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ProductInventoryTransaction());
			
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
				}			

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					System.out.println(" result set size +++++++"+result.size());
					
					
					
					if(result.size()==0){
						
						form.showDialogMessage("Transactions does not exist..!");
					}
					else{
					
						for(SuperModel model:result)
						{
							ProductInventoryTransaction transactionEntity=(ProductInventoryTransaction)model;
							productInventoryList.add(transactionEntity);
							
							
							Comparator<ProductInventoryTransaction> compare = new Comparator<ProductInventoryTransaction>() {
								
								@Override
								public int compare(ProductInventoryTransaction o1,ProductInventoryTransaction o2) {
									Integer l1=o1.getProductId();
									Integer l2=o1.getProductId();
									
									return l1.compareTo(l2);
								}
							};
							
							Collections.sort(productInventoryList, compare);
							
							
							form.getProductTransactionTable().getDataprovider().setList(productInventoryList);
							form.hideWaitSymbol();
						}
					}
				}
				});
		
		
	
		
	}



	private void updateProductInventoryList() {
		form.showWaitSymbol();
		updateAsync.updateInventoryProductListStock(productInventoryList,minEntity,new AsyncCallback<ArrayList<ProductInventoryTransaction>>() {

			@Override
			public void onFailure(Throwable caught) {
				
				form.showDialogMessage("Failure in Inventory Stock Updated");
			}


			@Override
			public void onSuccess(ArrayList<ProductInventoryTransaction> result) {

				form.showDialogMessage("Updation Successful");
				form.getProductTransactionTable().connectToLocal();
				
				form.hideWaitSymbol();
			}
		});
	}



	private void findMINProductDetailsFromMINid() {
		form.showWaitSymbol();
		int minID = Integer.parseInt(form.getIbMinMrnId().getValue().trim());
		
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(minID);
		filtervec.add(filter);
		
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new MaterialIssueNote());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}			

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result set size +++++++"+result.size());
				
				
				
				if(result.size()==0){
					
					form.showDialogMessage("MIN does not exist..!");
				}
				else{
				
					for(SuperModel model:result)
					{
						MaterialIssueNote MINEntity=(MaterialIssueNote)model;
						minEntity = MINEntity;
						minProductList.addAll(minEntity.getSubProductTablemin());
						form.getSubProductTableMin().getDataprovider().setList(minEntity.getSubProductTablemin());
						form.hideWaitSymbol();
					}
				}
			}
			});
		
		
		System.out.println("product list"+minProductList.size());
		
	}



	@Override
	public void reactOnEmail() {
		
	}

	@Override
	protected void makeNewModel() {
	}




	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		
		if(event.getSource()== form.getIbMinMrnId()){
			form.getSubProductTableMin().connectToLocal();
		}
	}
}
