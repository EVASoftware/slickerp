package com.slicktechnologies.client.views.inventoryTranscationErrorUpdate;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.slicktechnologies.client.views.inventory.materialreuestnote.SubProductTableMrn;
import com.slicktechnologies.client.views.inventory.productinventorytransaction.InventoryProductTransactionTable;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;

public class InventoryTransactionErrorUpdateForm extends FormScreen<DummyEntityOnlyForScreen>{

	
	TextBox ibMinMrnId;
	Button btnAdd,btTransactions,btUpdateTransaction,btUpdateTRansactionListIndex;
	SubProductTableMrn subProductTableMin;
	InventoryProductTransactionTable productTransactionTable;
	
	
	
	
	public InventoryTransactionErrorUpdateForm() {
		super();
		createGui();
	}

	private void initializeWidget(){
		ibMinMrnId=new TextBox();
		subProductTableMin=new SubProductTableMrn();
		productTransactionTable = new InventoryProductTransactionTable();
		
		btnAdd = new Button("Find MIN Products");
		btTransactions = new Button("Find Transacations");
		btUpdateTRansactionListIndex= new Button("Update Product Inventory Transacations Indexs");
		btUpdateTransaction = new Button("Update Transaction");
	}

	@Override
	public void createScreen() {
		
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		FormField fMinRefereceDetailGrouping = fbuilder.setlabel("MIN Details")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("MIN ID", ibMinMrnId);
		FormField fibMinMrnId = fbuilder.setMandatory(true).setRowSpan(0)
				.setColSpan(1).build();
		
		
		fbuilder = new FormFieldBuilder("", btTransactions);
		FormField fbtTransactions = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();

		fbuilder = new FormFieldBuilder("", subProductTableMin.getTable());
		FormField fsubProductTableMin= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", productTransactionTable.getTable());
		FormField fproductTransactionTable= fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fbtnAdd = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", btUpdateTRansactionListIndex);
		FormField fbtUpdateTRansactionListIndex = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(2).build();
		
		
		fbuilder = new FormFieldBuilder("", btUpdateTransaction);
		FormField fbtUpdateTransaction = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(1).build();
		
		FormField ftranscationDetails = fbuilder.setlabel("Product Inventory Transcation Details")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
			
			FormField[][] formfield = { 
					{ fMinRefereceDetailGrouping },
					{fbtUpdateTRansactionListIndex},
					{ fibMinMrnId,fbtnAdd},
					{ fsubProductTableMin},
					{fbtTransactions,fbtUpdateTransaction},
					{ftranscationDetails},
					{fproductTransactionTable}
			};
			this.fields = formfield;
	}

	
	
	
	
	
	
	@Override
	public void toggleAppHeaderBarMenu() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateView(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

	public TextBox getIbMinMrnId() {
		return ibMinMrnId;
	}

	public void setIbMinMrnId(TextBox ibMinMrnId) {
		this.ibMinMrnId = ibMinMrnId;
	}

	public Button getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(Button btnAdd) {
		this.btnAdd = btnAdd;
	}

	public SubProductTableMrn getSubProductTableMin() {
		return subProductTableMin;
	}

	public void setSubProductTableMin(SubProductTableMrn subProductTableMin) {
		this.subProductTableMin = subProductTableMin;
	}

	public Button getBtTransactions() {
		return btTransactions;
	}

	public void setBtTransactions(Button btTransactions) {
		this.btTransactions = btTransactions;
	}

	public Button getBtUpdateTRansactionListIndex() {
		return btUpdateTRansactionListIndex;
	}

	public void setBtUpdateTRansactionListIndex(Button btUpdateTRansactionListIndex) {
		this.btUpdateTRansactionListIndex = btUpdateTRansactionListIndex;
	}

	public InventoryProductTransactionTable getProductTransactionTable() {
		return productTransactionTable;
	}

	public void setProductTransactionTable(
			InventoryProductTransactionTable productTransactionTable) {
		this.productTransactionTable = productTransactionTable;
	}

	public Button getBtUpdateTransaction() {
		return btUpdateTransaction;
	}

	public void setBtUpdateTransaction(Button btUpdateTransaction) {
		this.btUpdateTransaction = btUpdateTransaction;
	}

	
	
}
