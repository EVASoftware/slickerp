package com.slicktechnologies.client.views.home;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogPopUp;
import com.slicktechnologies.client.views.lead.LeadPresenter.LeadPresenterTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;

public class LeadPresenterTableProxy extends LeadPresenterTable implements ClickHandler
{TextColumn<Lead> getTitleColumn;
TextColumn<Lead> getPriorityColumn;
TextColumn<Lead> getCustomerIdColumn;
TextColumn<Lead> getCustomerCellNumberColumn;
TextColumn<Lead> getCustomerFullNameColumn;
/**
* Updated By: Viraj
* Date: 08-01-2019
* Description: To add customer point of contact
*/
TextColumn<Lead> getCustomerPocColumn;
/** Ends **/
TextColumn<Lead> getBranchColumn;
TextColumn<Lead> getEmployeeColumn;
TextColumn<Lead> getStatusColumn;
TextColumn<Lead> getCreationDateColumn;
TextColumn<Lead> getGroupColumn;
TextColumn<Lead> getCategoryColumn;
TextColumn<Lead> getTypeColumn;
TextColumn<Lead> getCountColumn;
/** date 12/10/2017 added by komal for followup date**/
TextColumn<Lead> getFollowUpDateColumn;
Column<Lead , String> followUpDateButton;
PopupPanel communicationPanel;
CommunicationLogPopUp communicationLogPopUp = new CommunicationLogPopUp();
CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
int rowIndex ;
Lead lead;
final GWTCAlert alert = new GWTCAlert(); 
final GWTCGlassPanel glassPanel = new GWTCGlassPanel();
final GenricServiceAsync service = GWT.create(GenricService.class);
static boolean followUpFlag =  false;


public Object getVarRef(String varName)
{
	if(varName.equals("getTitleColumn"))
		return this.getTitleColumn;
	if(varName.equals("getPriorityColumn"))
		return this.getPriorityColumn;
	if(varName.equals("getCustomerCellNumberColumn"))
		return this.getCustomerCellNumberColumn;
	if(varName.equals("getCustomerFullNameColumn"))
		return this.getCustomerFullNameColumn;
	if(varName.equals("getTypeColumn"))
		return this.getTypeColumn;
	if(varName.equals("getBranchColumn"))
		return this.getBranchColumn;
	if(varName.equals("getEmployeeColumn"))
		return this.getEmployeeColumn;
	if(varName.equals("getStatusColumn"))
		return this.getStatusColumn;
	if(varName.equals("getCreationDateColumn"))
		return this.getCreationDateColumn;
	if(varName.equals("getGroupColumn"))
		return this.getGroupColumn;
	if(varName.equals("getCategoryColumn"))
		return this.getCategoryColumn;
	if(varName.equals("getTypeColumn"))
		return this.getTypeColumn;
	if(varName.equals("getCountColumn"))
		return this.getCountColumn;
	/** date 12/10/2017 added by komal for followup date**/
	if(varName.equals("getFollowUpDateColumn"))
		return this.getFollowUpDateColumn;
	/**
	 * Updated By: Viraj
	 * Date: 08-01-2019
	 * Description: To add customer point of contact
	 */
	if(varName.equals("getCustomerPocColumn"))
		return this.getCustomerPocColumn;
	/** Ends **/
	return null ;
}

public LeadPresenterTableProxy()
{
	super();
	/** 12/10/2017 added by komal for communication log popup **/
	communicationLogPopUp.getBtnOk().addClickHandler(this);
	communicationLogPopUp.getBtnCancel().addClickHandler(this);
	communicationLogPopUp.getBtnAdd().addClickHandler(this);
}

@Override public void createTable() {
	addColumngetCount();
    /** Date 12/10/2017 added by komal for follow-up date button **/
	
	createColumnFollowupDate();
	updateColumnFollowupDate();
	addColumngetFollowUpDate();
	
	
	addColumngetCreationDate();
	addColumngetPriority();
	addColumngetCustomerId();
	addColumngetCustomerFullName();
	/**
	 * Updated By: Viraj
	 * Date: 08-01-2019
	 * Description: To add customer point of contact
	 */
	addColumngetCustomerPOC();
	/** Ends **/
	addColumngetCustomerCellNumber();
	addColumngetEmployee();
	
	addColumngetCategory();
	addColumngetType();
	addColumngetGroup();
	
	addColumngetBranch();
	addColumngetStatus();
	
	
	
	//addColumngetTitle();
	
	
	addColumnSorting();
}
@Override
protected void initializekeyprovider() {
	keyProvider= new ProvidesKey<Lead>()
			{
		@Override
		public Object getKey(Lead item)
		{
			if(item==null)
			{
				return null;
			}
			else
				return item.getId();
		}
			};
}
@Override
public void setEnable(boolean state)
{
}
@Override
public void applyStyle()
{
}
public void addColumnSorting(){
	addSortinggetCount();
	addSortinggetBranch();
	addSortinggetEmployee();
	addSortinggetStatus();
	addSortinggetType();
	addSortinggetGroup();
	addSortinggetCategory();
	addSortinggetType();
	addSortinggetTitle();
	addSortinggetCustomerFullName();
	/**
	 * Updated By: Viraj
	 * Date: 08-01-2019
	 * Description: To add sorting for customer point of contact
	 */
	addSortingGetCustomerPOC();
	/** Ends **/
	addSortinggetCustomerCellNumber();
	addSortinggetPriority();
	addSortinggetCustomerId();
	addSortinggetCreationDate();
	addSortinggetFollowUpDate();
}
@Override public void addFieldUpdater() {
}

protected void addColumngetCount()
{
	getCountColumn=new TextColumn<Lead>()
			{
		@Override
		public String getValue(Lead object)
		{
			return object.getCount()+"";
		}
			};
			table.addColumn(getCountColumn,"ID");
			table.setColumnWidth(getCountColumn, 100,Unit.PX);
			getCountColumn.setSortable(true);
}
@SuppressWarnings("deprecation")
protected void addColumngetCreationDate()
{
	DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
	DatePickerCell date= new DatePickerCell(fmt);
	
//	final Date sub7Days=new Date();
//	System.out.println("Todays Date ::: "+sub7Days);
//	CalendarUtil.addDaysToDate(sub7Days,-7);
//	System.out.println("Date - 7 Days ::: "+sub7Days);
//	sub7Days.setHours(23);
//	sub7Days.setMinutes(59);
//	sub7Days.setSeconds(59);
//	System.out.println("Converted Date ::: "+sub7Days);
	
	getCreationDateColumn=new TextColumn<Lead>()
	{
//		@Override
//		public String getCellStyleNames(Context context, Lead object) {
//			 if(object.getCreationDate().before(sub7Days)&&object.getStatus().equals("Created")){
//				 return "red";
//			 }
//			 else if(object.getStatus().equals("Closed")||object.getStatus().equals("Successful")||object.getStatus().equals("Unsuccessful")){
//				 return "green";
//			 }
//			 else{
//				 return "black";
//			 }
//		}
		
		@Override
		public String getValue(Lead object)
		{
			if(object.getCreationDate()!=null){
				return AppUtility.parseDate(object.getCreationDate());}
			else{
				object.setCreationDate(new Date());
				return " ";
			}
		}
	};
	table.addColumn(getCreationDateColumn,"Creation Date");
	table.setColumnWidth(getCreationDateColumn, 120,Unit.PX);
	getCreationDateColumn.setSortable(true);
}


//////////////////////////////////////////////// Date IST Format //////////////////////////////////
//public  Date getDateWithTimeZone(String timeZone,Date date)
//{
//	 SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
//	 isoFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
//	    try {
//		  date = isoFormat.parse(isoFormat.format(date));
//		  } catch (ParseException e) {
//			e.printStackTrace();
//		}
//	    
//	    return date;
//}

//////////////////////////////////////////////////////////////////////////////////////////////////////

protected void addSortinggetCreationDate()
  {
  List<Lead> list=getDataprovider().getList();
  columnSort=new ListHandler<Lead>(list);
  columnSort.setComparator(getCreationDateColumn, new Comparator<Lead>()
  {
  @Override
  public int compare(Lead e1,Lead e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
  return e1.getCreationDate().compareTo(e2.getCreationDate());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }

protected void addSortinggetBranch()
{
	List<Lead> list=getDataprovider().getList();
	columnSort=new ListHandler<Lead>(list);
	columnSort.setComparator(getBranchColumn, new Comparator<Lead>()
			{
		@Override
		public int compare(Lead e1,Lead e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getBranch()!=null && e2.getBranch()!=null){
					return e1.getBranch().compareTo(e2.getBranch());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addSortinggetCount()
  {
  List<Lead> list=getDataprovider().getList();
  columnSort=new ListHandler<Lead>(list);
  columnSort.setComparator(getCountColumn, new Comparator<Lead>()
  {
  @Override
  public int compare(Lead e1,Lead e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCount()== e2.getCount()){
  return 0;}
  if(e1.getCount()> e2.getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }

protected void addColumngetBranch()
{
	getBranchColumn=new TextColumn<Lead>()
			{
		@Override
		public String getValue(Lead object)
		{
			return object.getBranch()+"";
		}
			};
			table.addColumn(getBranchColumn,"Branch");
			table.setColumnWidth(getBranchColumn, 90,Unit.PX);
			getBranchColumn.setSortable(true);
}

protected void addSortinggetEmployee()
{
	List<Lead> list=getDataprovider().getList();
	columnSort=new ListHandler<Lead>(list);
	columnSort.setComparator(getEmployeeColumn, new Comparator<Lead>()
			{
		@Override
		public int compare(Lead e1,Lead e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getEmployee()!=null && e2.getEmployee()!=null){
					return e1.getEmployee().compareTo(e2.getEmployee());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetEmployee()
{
	getEmployeeColumn=new TextColumn<Lead>()
			{
		@Override
		public String getValue(Lead object)
		{
			return object.getEmployee()+"";
		}
			};
			table.addColumn(getEmployeeColumn,"Sales Person");
			table.setColumnWidth(getEmployeeColumn, 120,Unit.PX);
			getEmployeeColumn.setSortable(true);
}

protected void addSortinggetStatus()
{
	List<Lead> list=getDataprovider().getList();
	columnSort=new ListHandler<Lead>(list);
	columnSort.setComparator(getStatusColumn, new Comparator<Lead>()
			{
		@Override
		public int compare(Lead e1,Lead e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getStatus()!=null && e2.getStatus()!=null){
					return e1.getStatus().compareTo(e2.getStatus());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetStatus()
{
	getStatusColumn=new TextColumn<Lead>()
			{
		@Override
		public String getValue(Lead object)
		{
			return object.getStatus()+"";
		}
			};
			table.addColumn(getStatusColumn,"Status");
			table.setColumnWidth(getStatusColumn, 90,Unit.PX);
			getStatusColumn.setSortable(true);
}

protected void addSortinggetGroup()
{
	List<Lead> list=getDataprovider().getList();
	columnSort=new ListHandler<Lead>(list);
	columnSort.setComparator(getGroupColumn, new Comparator<Lead>()
			{
		@Override
		public int compare(Lead e1,Lead e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getGroup()!=null && e2.getGroup()!=null){
					return e1.getGroup().compareTo(e2.getGroup());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetGroup()
{
	getGroupColumn=new TextColumn<Lead>()
			{
		@Override
		public String getValue(Lead object)
		{
			return object.getGroup()+"";
		}
			};
			table.addColumn(getGroupColumn,"Group");
			table.setColumnWidth(getGroupColumn, 90,Unit.PX);
			getGroupColumn.setSortable(true);
}
protected void addSortinggetCategory()
{
	List<Lead> list=getDataprovider().getList();
	columnSort=new ListHandler<Lead>(list);
	columnSort.setComparator(getCategoryColumn, new Comparator<Lead>()
			{
		@Override
		public int compare(Lead e1,Lead e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getCategory()!=null && e2.getCategory()!=null){
					return e1.getCategory().compareTo(e2.getCategory());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetCategory()
{
	getCategoryColumn=new TextColumn<Lead>()
			{
		@Override
		public String getValue(Lead object)
		{
			return object.getCategory()+"";
		}
			};
			table.addColumn(getCategoryColumn,"Category");
			table.setColumnWidth(getCategoryColumn, 90,Unit.PX);
			getCategoryColumn.setSortable(true);
}
protected void addSortinggetType()
{
	List<Lead> list=getDataprovider().getList();
	columnSort=new ListHandler<Lead>(list);
	columnSort.setComparator(getTypeColumn, new Comparator<Lead>()
			{
		@Override
		public int compare(Lead e1,Lead e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getType()!=null && e2.getType()!=null){
					return e1.getType().compareTo(e2.getType());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetType()
{
	getTypeColumn=new TextColumn<Lead>()
			{
		@Override
		public String getValue(Lead object)
		{
			return object.getType()+"";
		}
			};
			table.addColumn(getTypeColumn,"Type");
			table.setColumnWidth(getTypeColumn, 90,Unit.PX);
			getTypeColumn.setSortable(true);
}
protected void addSortinggetTitle()
{
	List<Lead> list=getDataprovider().getList();
	columnSort=new ListHandler<Lead>(list);
	columnSort.setComparator(getTitleColumn, new Comparator<Lead>()
			{
		@Override
		public int compare(Lead e1,Lead e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getTitle()!=null && e2.getTitle()!=null){
					return e1.getTitle().compareTo(e2.getTitle());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetTitle()
{
	getTitleColumn=new TextColumn<Lead>()
			{
		@Override
		public String getValue(Lead object)
		{
			return object.getTitle()+"";
		}
			};
			table.addColumn(getTitleColumn,"Lead Title");
			table.setColumnWidth(getTitleColumn, 120,Unit.PX);
			getTitleColumn.setSortable(true);
}
protected void addSortinggetCustomerFullName()
{
	List<Lead> list=getDataprovider().getList();
	columnSort=new ListHandler<Lead>(list);
	columnSort.setComparator(getCustomerFullNameColumn, new Comparator<Lead>()
			{
		@Override
		public int compare(Lead e1,Lead e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getCustomerFullName()!=null && e2.getCustomerFullName()!=null){
					return e1.getCustomerFullName().compareTo(e2.getCustomerFullName());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetCustomerFullName()
{
	getCustomerFullNameColumn=new TextColumn<Lead>()
			{
		@Override
		public String getValue(Lead object)
		{
			return object.getCustomerFullName()+"";
		}
			};
			table.addColumn(getCustomerFullNameColumn,"Customer Name");
			table.setColumnWidth(getCustomerFullNameColumn, 120,Unit.PX);
			getCustomerFullNameColumn.setSortable(true);
}
protected void addSortinggetCustomerCellNumber()
{
	List<Lead> list=getDataprovider().getList();
	columnSort=new ListHandler<Lead>(list);
	columnSort.setComparator(getCustomerCellNumberColumn, new Comparator<Lead>()
			{
		@Override
		public int compare(Lead e1,Lead e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getCustomerCellNumber()== e2.getCustomerCellNumber()){
					return 0;}
				if(e1.getCustomerCellNumber()> e2.getCustomerCellNumber()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}

/**
 * Updated By: Viraj
 * Date: 08-01-2019
 * Description: To add customer point of contact
 */
private void addColumngetCustomerPOC() {
	getCustomerPocColumn=new TextColumn<Lead>() {
		@Override
		public String getValue(Lead object) {
			if(object.getPersonInfo().getPocName() == null) {
				return "";
			}
			else {
				return object.getPersonInfo().getPocName();
			}
		}
	};
	table.addColumn(getCustomerPocColumn,"Customer POC");
	table.setColumnWidth(getCustomerPocColumn, 120,Unit.PX);
	getCustomerPocColumn.setSortable(true);
}

private void addSortingGetCustomerPOC() {
	List<Lead> list=getDataprovider().getList();
	columnSort=new ListHandler<Lead>(list);
	columnSort.setComparator(getCustomerPocColumn, new Comparator<Lead>() {
		@Override
		public int compare(Lead e1,Lead e2)
		{
			if(e1!=null && e2!=null) {
				if( e1.getCustomerPOC()!=null && e2.getCustomerPOC()!=null) {
					return e1.getCustomerPOC().compareTo(e2.getCustomerPOC());
				}
			}
			else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

/** Ends **/

protected void addColumngetCustomerCellNumber()
{
	
	getCustomerCellNumberColumn=new TextColumn<Lead>()
			{
		@Override
		public String getValue(Lead object)
		{
			if(object.getCustomerCellNumber()==0){
				return 0+"";}
			else{
				return object.getCustomerCellNumber()+"";}
		}
			};
			table.addColumn(getCustomerCellNumberColumn,"Customer Cell");
			table.setColumnWidth(getCustomerCellNumberColumn, 120,Unit.PX);
			getCustomerCellNumberColumn.setSortable(true);
}
protected void addFieldUpdatergetCustomerCellNumber()
{
	
}
protected void addSortinggetPriority()
{
	List<Lead> list=getDataprovider().getList();
	columnSort=new ListHandler<Lead>(list);
	columnSort.setComparator(getPriorityColumn, new Comparator<Lead>()
			{
		@Override
		public int compare(Lead e1,Lead e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getPriority()!=null && e2.getPriority()!=null){
					return e1.getPriority().compareTo(e2.getPriority());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}
protected void addColumngetCustomerId()
{
	getCustomerIdColumn=new TextColumn<Lead>()
			{
		@Override
		public String getValue(Lead object)
		{
			return object.getPersonInfo().getCount()+"";
		}
			};
			table.addColumn(getCustomerIdColumn,"Customer Id");
			table.setColumnWidth(getCustomerIdColumn, 120,Unit.PX);
			getCustomerIdColumn.setSortable(true);
}


protected void addSortinggetCustomerId()
  {
  List<Lead> list=getDataprovider().getList();
  columnSort=new ListHandler<Lead>(list);
  columnSort.setComparator(getCustomerIdColumn, new Comparator<Lead>()
  {
  @Override
  public int compare(Lead e1,Lead e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
  return 0;}
  if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
protected void addColumngetPriority()
{
	getPriorityColumn=new TextColumn<Lead>()
			{
		@Override
		public String getValue(Lead object)
		{
			return object.getPriority()+"";
		}
			};
			table.addColumn(getPriorityColumn,"Priority");
			table.setColumnWidth(getPriorityColumn, 120,Unit.PX);
			getPriorityColumn.setSortable(true);
}

/** Date 12/10/2017 added by komal for follow-up date column**/
protected void addColumngetFollowUpDate()
{
	/** date 18/10/2017 added by komal to add style of follow-up date column **/
	
	final Date sub7Days=new Date();
	System.out.println("Todays Date ::: "+sub7Days);
	CalendarUtil.addDaysToDate(sub7Days,-7);
	System.out.println("Date - 7 Days ::: "+sub7Days);
	sub7Days.setHours(23);
	sub7Days.setMinutes(59);
	sub7Days.setSeconds(59);
	System.out.println("Converted Date ::: "+sub7Days);
	
	getFollowUpDateColumn=new TextColumn<Lead>()
			{
		/** date 18/10/2017 added by komal to add style of follow-up date column **/
		@Override
		public String getCellStyleNames(Context context, Lead object) {
			 if(object.getFollowUpDate().before(sub7Days)&&object.getStatus().equals("Created")){
				 return "red";
			 }
			 else if(object.getStatus().equals("Closed")||object.getStatus().equals("Successful")||object.getStatus().equals("Unsuccessful") || object.getStatus().equals("Sucessful") || object.getStatus().equals("Unsucessful")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
	
		@Override
		public String getValue(Lead object)
		{
			if(object.getFollowUpDate()!=null){
				return AppUtility.parseDate(object.getFollowUpDate());}
			else{
				//object.setFollowUpDate(null);
				return "N.A.";
			}
		}
			};
			table.addColumn(getFollowUpDateColumn,"Follow-up Date");
			table.setColumnWidth(getFollowUpDateColumn, 110, Unit.PX);
			getFollowUpDateColumn.setSortable(true);
}

protected void addSortinggetFollowUpDate()
{
List<Lead> list=getDataprovider().getList();
columnSort=new ListHandler<Lead>(list);
columnSort.setComparator(getFollowUpDateColumn, new Comparator<Lead>()
{
@Override
public int compare(Lead e1,Lead e2)
{
if(e1!=null && e2!=null)
{
	 if(e1.getFollowUpDate() == null && e2.getFollowUpDate()==null){
	        return 0;
	    }
	    if(e1.getFollowUpDate() == null) return 1;
	    if(e2.getFollowUpDate() == null) return -1;
	    
	    return e1.getFollowUpDate().compareTo(e2.getFollowUpDate());
}
return 0;
}
});
table.addColumnSortHandler(columnSort);
}


protected void createColumnFollowupDate()
{
	ButtonCell btnCell = new ButtonCell();
   followUpDateButton = new Column<Lead, String>(btnCell) {
		
		@Override
		public String getValue(Lead object) {
			// TODO Auto-generated method stub
			return "Follow-up Date";
		}
	};                 
	
	table.addColumn(followUpDateButton, "");
	table.setColumnWidth(followUpDateButton,130,Unit.PX);
	
}


protected void updateColumnFollowupDate()
{
	// TODO Auto-generated method stub
	followUpDateButton.setFieldUpdater(new FieldUpdater<Lead, String>() {
		
				@Override
				public void update(int index, final Lead object, String value) {
					
					
					rowIndex=index;
					followUpFlag = true;
					lead= object;
					getLogDetails(object);
				}
			});
	
}

private void getLogDetails(Lead lead)
{
	glassPanel.show();
	MyQuerry querry = AppUtility.communicationLogQuerry(lead.getCount(), lead.getCompanyId(),AppConstants.SERVICEMODULE,AppConstants.LEAD);
	
	service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		
		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			
			ArrayList<InteractionType> list = new ArrayList<InteractionType>();
			
			for(SuperModel model : result){
				
				InteractionType interactionType = (InteractionType) model;
				list.add(interactionType);
				
			}
			/**
			 * Date : 08-11-2017 BY Komal
			 */
			Comparator<InteractionType> comp=new Comparator<InteractionType>() {
				@Override
				public int compare(InteractionType e1, InteractionType e2) {
//					Integer coutn1=o1.getCount();
//					Integer count2=o2.getCount();
//					return ;
//					if (e1.getCount() == e2.getCount()) {
//						return 0;
//					}
//					if (e1.getCount() > e2.getCount()) {
//						return 1;
//					} else {
//						return -1;
//					}
					/**
					 * Updated By: Viraj
					 * Date: 09-02-2018
					 * Description: To sort communication log
					 */
					Integer ee1=e1.getCount();
					Integer ee2=e2.getCount();
					return ee2.compareTo(ee1);
					/** Ends **/
				}
			};
			Collections.sort(list,comp);
            
			glassPanel.hide();
			communicationLogPopUp.getRemark().setValue("");
			communicationLogPopUp.getDueDate().setValue(null);
			communicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
			communicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
			communicationPanel = new PopupPanel(true);
			communicationPanel.add(communicationLogPopUp);
			
			communicationPanel.center();
			communicationPanel.show();
				
		}
		
		@Override
		public void onFailure(Throwable caught) {
			glassPanel.hide();
		}
	});
	
}

@Override
public void onClick(ClickEvent event) {
	if(event.getSource() == communicationLogPopUp.getBtnOk()){
	
		glassPanel.show();
	    List<InteractionType> list = communicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
	   
	    ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
	    interactionlist.addAll(list);
	    
	    boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);
	    
	    if(checkNewInteraction==false){
	    	alert.alert("Please add new interaction details");
	    	glassPanel.hide();
	    }	
	    else{	
	    	
	    	final Date followUpDate = interactionlist.get(interactionlist.size()-1).getInteractionDueDate();
	    	 communicationService.saveCommunicationLog(interactionlist, new AsyncCallback<Void>() {
	    		 
					@Override
					public void onFailure(Throwable caught) {
						alert.alert("Unexpected Error");
						glassPanel.hide();
						LoginPresenter.communicationLogPanel.hide();
					}

					@Override
					public void onSuccess(Void result) {
						alert.alert("Data Save Successfully");
						//komal
						//model.setFollowUpDate(followUpDate);
						//form.getDbleaddate().setValue(followUpDate); 
						List<Lead> list = getDataprovider().getList();
						list.get(rowIndex).setFollowUpDate(followUpDate);
						Lead leadObj = list.get(rowIndex);
						saveLead(leadObj);
						followUpFlag = false;
//						getDataprovider().getList().clear();
//						getDataprovider().getList().addAll(list);
						table.redraw();
						glassPanel.hide();
						communicationPanel.hide();
					} 
				});
	    }
	   
	}
	if(event.getSource() == communicationLogPopUp.getBtnCancel()){
//		communicationPanel.hide();
		followUpFlag = false;
		communicationPanel.hide();
	}
	if(event.getSource() == communicationLogPopUp.getBtnAdd()){
			glassPanel.show();
			String remark = communicationLogPopUp.getRemark().getValue();
			Date dueDate = communicationLogPopUp.getDueDate().getValue();
			String interactiongGroup =null;
			if(communicationLogPopUp.getOblinteractionGroup().getSelectedIndex()!=0)
				interactiongGroup=communicationLogPopUp.getOblinteractionGroup().getValue(communicationLogPopUp.getOblinteractionGroup().getSelectedIndex());
			boolean validationFlag = AppUtility.validateCommunicationlog(remark,dueDate);
			
			List<InteractionType> list = communicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
			/**
			 * Date : 08-11-2017 BY Komal
			 */
			Comparator<InteractionType> comp=new Comparator<InteractionType>() {
				@Override
				public int compare(InteractionType e1, InteractionType e2) {
//					Integer coutn1=o1.getCount();
//					Integer count2=o2.getCount();
//					return ;
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				}
			};
			Collections.sort(list,comp);
            
			
			if(validationFlag){
				InteractionType communicationLog =  AppUtility.getCommunicationLog(AppConstants.SERVICEMODULE,AppConstants.LEAD,lead.getCount(),lead.getEmployee(),remark,dueDate,lead.getPersonInfo(),null,interactiongGroup, lead.getBranch(),"");
				list.add(communicationLog);
				communicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);;
				communicationLogPopUp.getRemark().setValue("");
				communicationLogPopUp.getDueDate().setValue(null);
				communicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
			}
			glassPanel.hide();
	}
	
	
	
}
	private void saveLead(Lead leadObj) {
		service.save(leadObj,new AsyncCallback<ReturnFromServer>() {
		@Override
		public void onFailure(Throwable caught) {
			glassPanel.hide();
			
		     alert.alert("An Unexpected error occurred!");
		}
		@Override
		public void onSuccess(ReturnFromServer result) {
			glassPanel.hide();
			
		     alert.alert("Saved successfully!");
		}
	});
		
	}
}


