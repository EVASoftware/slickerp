package com.slicktechnologies.client.views.home;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.MyInlineLabel;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.personlayer.Employee;

// TODO: Auto-generated Javadoc
/**
 * The Form represents a dashboard maninly for reporting purposes.It shows a summary for the business processes.
 * The summary can be configured depending upon the querry conditions.
 * The class and its presenter can be a common component for which we may write parser.
 * 
 */

public class HomeForm extends ViewContainer implements ClickHandler
{
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	/** String array repersenting the table header menus*/
	String[] tableheaderbarnames={"New","Download"};
	
	/** Flow panel for holding the Table header menus */
	FlowPanel tableheader;
	
	/** Lead Table which can come on the form, this component can be made generic */
	public LeadPresenterTableProxy leadTable;
	
	/** Quotation Table which can come on form component can be made generic */
	public QuotationPresenterTableProxy quotationTable;
	
	/** Contract table which can come on form component can be made generic */
	public ContractPresenterTableProxy contractTable;
	/** Service table which can come on form component can be made generic */
	public ServiceTable serviceTable;
	
	//**************************rohan added this fields in dashboard for searching by branch and sales person  
	
			ObjectListBox<Branch> olbbranch=new ObjectListBox<Branch>();
			ObjectListBox<Employee> olbsalesPerson = new ObjectListBox<Employee>();
//			/**
//			 * Updated By: Viraj
//			 * Date: 10-01-2019
//			 * Description: To add Status filter in dashboard
//			 */
//			ObjectListBox<ConfigCategory> olbLeadStatus;
//			/** Ends **/
		
		//************************************changes ends here ********************
		
	
	
	DateBox dbFromDate;
	DateBox dbToDate;
	Button btnGo;
		
	//public HomeSearchCriteria criteria;
	//private final PersistentServiceAsync persistentService = GWT.create(PersistentService.class);
	
	/**
	 * Instantiates a new home form.
	 */
	public HomeForm()
	{
		//Toggle the App header bar menu , In this case only search should come
		toggleAppHeaderBarMenu();
		//Create the Gui, actual Gui gets created here.
		createGui();
		//Sets the event handling
  }	
	
	private void initializeWidget(){
		
		//***********************rohan changes here for searching for by branch and sales person ****
		
				olbbranch =new ObjectListBox<Branch>();
				AppUtility.makeBranchListBoxLive(olbbranch);
				
				olbsalesPerson =new ObjectListBox<Employee>();
				AppUtility.makeSalesPersonListBoxLive(olbsalesPerson);
				
//				/**
//				 * Updated By: Viraj
//				 * Date: 10-01-2019
//				 * Description: To add Status filter in dashboard
//				 */
//				olbLeadStatus= new ObjectListBox<ConfigCategory>();
//				AppUtility.MakeLiveConfig(olbLeadStatus,Screen.HOME);
//				/** Ends **/
		// ******************************changescomplete ********************************
		
		
		dbFromDate=new DateBoxWithYearSelector();
		dbToDate=new DateBoxWithYearSelector();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		dbFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		dbToDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		btnGo=new Button("GO");
	}
			
	/**
	 * Toggles the app header bar menu.
	 */
	private void toggleAppHeaderBarMenu() {
		InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
		for(int k=0;k<menus.length;k++)
		{
			String text=menus[k].getText();
			if(text.contains("Search"))
			{
				menus[k].setVisible(true); 
			}
			else
				menus[k].setVisible(false);  		   
			
		}
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.ViewContainer#createGui()
	 */
	@Override
	public void createGui() 
	{
		
		initializeWidget();
		
		FlowPanel panel= new FlowPanel();
		
		panel.getElement().getStyle().setMarginTop(10, Unit.PX);
		panel.getElement().getStyle().setMarginLeft(75, Unit.PX);
		panel.getElement().getStyle().setMarginRight(75, Unit.PX);
		
		FlowPanel innerpanel= new FlowPanel();
		
		InlineLabel blank = new InlineLabel("    ");
		
		
		//************rohan added here search by branch and sales person***************  
		
				InlineLabel branch = new InlineLabel("  Branch  ");
//				branch.setSize("90px", "20px");
				innerpanel.add(branch);
				innerpanel.add(olbbranch);
		//		innerpanel.add(blank);
		//		innerpanel.add(blank);
		//		innerpanel.add(blank);
		//		innerpanel.add(blank);
		
				InlineLabel salesPerson = new InlineLabel(" Sales Person  ");
//				salesPerson.setSize("90px", "20px");
				innerpanel.add(salesPerson);
				innerpanel.add(olbsalesPerson);
//				innerpanel.add(blank);
//				innerpanel.add(blank);
//				innerpanel.add(blank);
//				innerpanel.add(blank);
				
//				
		//**************************changes ends here ********************************
		
		InlineLabel fromDate = new InlineLabel("  From Date  ");
		fromDate.setSize("90px", "20px");
		innerpanel.add(fromDate);
		innerpanel.add(dbFromDate);
		
		InlineLabel toDate = new InlineLabel("  To Date  ");
		/**
		 * Updated By: Viraj
		 * Date: 10-01-2019
		 * Description: To add Status filter in dashboard
		 */
//		InlineLabel status = new InlineLabel("Status");
		innerpanel.add(toDate);
		innerpanel.add(dbToDate);
//		innerpanel.add(status);
//		innerpanel.add(olbLeadStatus);
		/** Ends **/
		innerpanel.add(blank);
		innerpanel.add(blank);
		innerpanel.add(btnGo);
		
		panel.add(innerpanel);
		content.add(panel);
		
		
		
		//Create the Four Scroll Panel To hold four types of Screen
		ScrollPanel[] scrollPanes=new ScrollPanel[4];
		//String array to hold captions
		String[]captions={"Leads","Quotations","Contracts","Services"};
		//Screen array to hold new Screen object for the purpose of Redirection
		Screen[]screens={Screen.LEAD,Screen.QUOTATION,Screen.CONTRACT,Screen.SERVICE};	
		
		//Create  Flow Panel,Inside Flow Panel 
		for(int i=0;i<captions.length;i++)
		{
			FlowPanel holder= new FlowPanel();
			tableheader= new FlowPanel();
			tableheader.getElement().setId("tableheader");
			// Provides Table Header which is responsible for new or Download Buttons
			setTableHeaderBar(screens[i]);
			
			tableheader.getElement().getStyle().setMarginTop(10, Unit.PX);
			tableheader.getElement().getStyle().setMarginBottom(10, Unit.PX);
			
			scrollPanes[i]= new ScrollPanel();
			scrollPanes[i].setHeight("150px");
		
			holder.add(tableheader);
			holder.add(scrollPanes[i]);
			//Caption Panel inside which a Flow Panel is added
			CaptionPanel cap= new CaptionPanel(captions[i]);
			cap.add(holder);
			cap.getElement().setClassName("tablecaption");
			//Add caption Panel on Content
		    content.add(cap);
		}
		//Add Lead Table inside the Scroll Panel
		leadTable = new LeadPresenterTableProxy();
		
		scrollPanes[0].add(leadTable.content);
		//Add Quotation Table inside the Scroll Panel
		quotationTable = new QuotationPresenterTableProxy();
		scrollPanes[1].add(quotationTable.content);
		//Add Contract Panel inside the table
		contractTable = new ContractPresenterTableProxy();
		scrollPanes[2].add(contractTable.content);
		//Add Service Table
		serviceTable = new ServiceTable();
		scrollPanes[3].add(serviceTable.content);
		
		content.getElement().setId("homeview");
	  }
	
	/**
	 * ******************************.
	 *
	 * @param screen the new table header bar
	 */
	/**
	 * Method sticks the header bar on tables depending upon their type.To differntiate between tables
	 * app header bar is introduced.
	 * @param screen
	 */
	
	public void setTableHeaderBar(Screen screen)
	{
		if(screen == Screen.SERVICE)
		{
			MyInlineLabel lbl;
			lbl=new MyInlineLabel("Download",screen);
			getTableheader().add(lbl);
			lbl.getElement().setId("tableheadermenu");
			lbl.addClickHandler(this);
			return;
		}
		
		for(int j=0;j<getTableheaderbarnames().length;j++)
		{
			MyInlineLabel lbl;
			lbl=new MyInlineLabel(getTableheaderbarnames()[j],screen);
            getTableheader().add(lbl);
			lbl.getElement().setId("tableheadermenu");
			lbl.addClickHandler(this);
		}
	}
 
/* (non-Javadoc)
 * @see com.simplesoftwares.client.library.appstructure.ViewContainer#applyStyle()
 */
@Override
public void applyStyle() {
	
}

/**
 * Gets the tableheaderbarnames.
 *
 * @return the tableheaderbarnames
 */
public String[] getTableheaderbarnames() {
	return tableheaderbarnames;
}

/**
 * Sets the tableheaderbarnames.
 *
 * @param tableheaderbarnames the new tableheaderbarnames
 */
public void setTableheaderbarnames(String[] tableheaderbarnames) {
	this.tableheaderbarnames = tableheaderbarnames;
}

/**
 * Gets the tableheader.
 *
 * @return the tableheader
 */
public FlowPanel getTableheader() {
	return tableheader;
}

/**
 * Sets the tableheader.
 *
 * @param tableheader the new tableheader
 */
public void setTableheader(FlowPanel tableheader) {
	this.tableheader = tableheader;
}

/**
 * Gets the lead table.
 *
 * @return the lead table
 */
public LeadPresenterTableProxy getLeadTable() {
	return leadTable;
}

public ServiceTable getServiceTable() {
	return serviceTable;
}

public void setServiceTable(ServiceTable serviceTable) {
	this.serviceTable = serviceTable;
}

/**
 * Sets the lead table.
 *
 * @param leadTable the new lead table
 */
public void setLeadTable(LeadPresenterTableProxy leadTable) {
	this.leadTable = leadTable;
}

/**
 * Gets the quotation table.
 *
 * @return the quotation table
 */
public QuotationPresenterTableProxy getQuotationTable() {
	return quotationTable;
}

/**
 * Sets the quotation table.
 *
 * @param quotationTable the new quotation table
 */
public void setQuotationTable(QuotationPresenterTableProxy quotationTable) {
	this.quotationTable = quotationTable;
}

/**
 * Gets the contract table.
 *
 * @return the contract table
 */
public ContractPresenterTableProxy getContractTable() {
	return contractTable;
}

/**
 * Sets the contract table.
 *
 * @param contractTable the new contract table
 */
public void setContractTable(ContractPresenterTableProxy contractTable) {
	this.contractTable = contractTable;
}

/* (non-Javadoc)
 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
 */
@Override
public void onClick(ClickEvent event) {
	if(event.getSource() instanceof MyInlineLabel)
	{
		MyInlineLabel lbl=(MyInlineLabel) event.getSource();
		redirectOnScreens(lbl);
		
	}
}

private void redirectOnScreens(MyInlineLabel lbl)
{
	Screen screen=(Screen) lbl.redirectScreen;
	String title=lbl.getText().trim();
	switch(screen)
	{
	  case LEAD:
		  
			if(title.equals("Download"))
			{
				reactOnLeadDownLoad();
			}
			
			if(title.equals("New"))
			{
				/**
				 * Date : 28-10-2017 BY ANIL
				 * Loading Lead Screen from dashboard
				 */
				AppMemory.getAppMemory().currentState=ScreeenState.NEW;
				AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Lead", Screen.LEAD);
				screen.changeScreen();
				/**
				 * End
				 */
				LeadPresenter.initalize();
			}
			
			break;
			
	  case QUOTATION:
		  
			if(title.equals("Download"))
			{
				reactOnQuotationDownLoad();
			}
			
			if(title.equals("New"))
			{
				QuotationPresenter.initalize();    		  
			}
			
			break;
			
	  case CONTRACT:
		  
			if(title.equals("Download"))
			{
				reactOnContractDownLoad();
			}
			
			if(title.equals("New"))
			{
				ContractPresenter.initalize();
			}
			
			break;
			
	  case SERVICE:
		 
			if(title.equals("Download"))
			{
				reactOnServiceDownLoad();
			}
			break;
	default:
		break;
			
	}

}

private void reactOnLeadDownLoad()
{
	ArrayList<Lead> custarray=new ArrayList<Lead>();
	List<Lead> list=(List<Lead>) leadTable.getListDataProvider().getList();
	
	custarray.addAll(list);
	
	csvservice.setleadslist(custarray, new AsyncCallback<Void>() {

		@Override
		public void onFailure(Throwable caught) {
			System.out.println("RPC call Failed"+caught);
			
		}

		@Override
		public void onSuccess(Void result) {
			

			String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
			final String url=gwt + "csvservlet"+"?type="+3;
			Window.open(url, "test", "enabled");
			
		}
	});
	 
}


private void reactOnServiceDownLoad()
{
	ArrayList<Service> custarray=new ArrayList<Service>();
	List<Service> list=(List<Service>) serviceTable.getListDataProvider().getList();
	
	custarray.addAll(list);
	
	csvservice.setservice(custarray, new AsyncCallback<Void>() {

		@Override
		public void onFailure(Throwable caught) {
			System.out.println("RPC call Failed"+caught);
			
		}

		@Override
		public void onSuccess(Void result) {
			

			String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
			final String url=gwt + "csvservlet"+"?type="+5;
			Window.open(url, "test", "enabled");
			
		}
	});
	 
}


private void reactOnContractDownLoad()
{
	ArrayList<Contract> custarray=new ArrayList<Contract>();
	List<Contract> list=(List<Contract>) contractTable.getDataprovider().getList();
	
	custarray.addAll(list);
	
	csvservice.setcontractlist(custarray, new AsyncCallback<Void>() {

		@Override
		public void onFailure(Throwable caught) {
			System.out.println("RPC call Failed"+caught);
			
		}

		@Override
		public void onSuccess(Void result) {
//			String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//			final String url=gwt + "csvservlet"+"?type="+6;
//			Window.open(url, "test", "enabled");
			
			String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
			/**
			 * Date : 24-10-2017 BY ANIL
			 * New Contract Report format for NBHC
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractReport")){
				final String url = gwt + "csvservlet" + "?type=" + 127;
				Window.open(url, "test", "enabled");
			}else{
				final String url = gwt + "csvservlet" + "?type=" + 6;
				Window.open(url, "test", "enabled");
			}
		}
	});
	 
}

private void reactOnQuotationDownLoad()
{
	ArrayList<Quotation> custarray=new ArrayList<Quotation>();
	List<Quotation> list=(List<Quotation>) quotationTable.getDataprovider().getList();
	
	custarray.addAll(list);
	
	csvservice.setquotationlist(custarray, new AsyncCallback<Void>() {
		

		@Override
		public void onFailure(Throwable caught) {
			System.out.println("RPC call Failed"+caught);
			
		}

		@Override
		public void onSuccess(Void result) {
			String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
			final String url=gwt + "csvservlet"+"?type="+2;
			Window.open(url, "test", "enabled");
		}
	});
	 
}

	

		public DateBox getDbFromDate() {
			return dbFromDate;
		}
		public void setDbFromDate(DateBox dbFromDate) {
			this.dbFromDate = dbFromDate;
		}
		public DateBox getDbToDate() {
			return dbToDate;
		}
		public void setDbToDate(DateBox dbToDate) {
			this.dbToDate = dbToDate;
		}
		public Button getBtnGo() {
			return btnGo;
		}
		public void setBtnGo(Button btnGo) {
			this.btnGo = btnGo;
		}

		public ObjectListBox<Branch> getOlbbranch() {
			return olbbranch;
		}

		public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
			this.olbbranch = olbbranch;
		}

		public ObjectListBox<Employee> getOlbsalesPerson() {
			return olbsalesPerson;
		}

		public void setOlbsalesPerson(ObjectListBox<Employee> olbsalesPerson) {
			this.olbsalesPerson = olbsalesPerson;
		}
		
//		/**
//		 * Updated By: Viraj
//		 * Date: 10-01-2019
//		 * Description: To add Status filter in dashboard
//		 */
//		public ObjectListBox<ConfigCategory> getOlbLeadStatus() {
//			return olbLeadStatus;
//		}
//
//		public void setOlbLeadStatus(ObjectListBox<ConfigCategory> olbLeadStatus) {
//			this.olbLeadStatus = olbLeadStatus;
//		}
//		/** Ends **/




}

		
	


