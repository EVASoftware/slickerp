package com.slicktechnologies.client.views.home;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;

public class ServiceTable extends SuperTable<Service> {TextColumn<Service> getCountColumn;
TextColumn<Service> getCreationDateColumn;


//   rohan commented this column because of duplication on 12/2/2016
//TextColumn<Service> getServiceCountColumn;
TextColumn<Service> getContractCountColumn;

TextColumn<Service> getCustomerIdColumn;
TextColumn<Service> getCustomerFullNameColumn;
TextColumn<Service> getCustomerCellNumberColumn;

TextColumn<Service> getGroupColumn;
TextColumn<Service> getCategoryColumn;
TextColumn<Service> getTypeColumn;

TextColumn<Service> getEmployeeColumn;
TextColumn<Service> getApproverName;
TextColumn<Service> getBranchColumn;
TextColumn<Service> getStatusColumn;

public Object getVarRef(String varName) {
	if (varName.equals("getCustomerCellNumberColumn"))
		return this.getCustomerCellNumberColumn;
	if (varName.equals("getCustomerFullNameColumn"))
		return this.getCustomerFullNameColumn;
	if (varName.equals("getCustomerIdColumn"))
		return this.getCustomerIdColumn;
//	if (varName.equals("getServiceCountColumn"))
//		return this.getServiceCountColumn;
	if (varName.equals("getBranchColumn"))
		return this.getBranchColumn;
	if (varName.equals("getEmployeeColumn"))
		return this.getEmployeeColumn;
	if (varName.equals("getStatusColumn"))
		return this.getStatusColumn;
	if (varName.equals("getCreationDateColumn"))
		return this.getCreationDateColumn;
	if (varName.equals("getGroupColumn"))
		return this.getGroupColumn;
	if (varName.equals("getCategoryColumn"))
		return this.getCategoryColumn;
	if (varName.equals("getTypeColumn"))
		return this.getTypeColumn;
	if (varName.equals("getCountColumn"))
		return this.getCountColumn;
	return null;
}

public ServiceTable() {
	super();
}

@Override
public void createTable() {
	
	addColumngetCount();
	addColumngetCreationDate();
//	addColumngetServiceCount();
	addColumngetContractCount();

	addColumngetCustomerId();
	addColumngetCustomerFullName();
	addColumngetCustomerCellNumber();

	addColumngetEmployee();
//	addColumngetApprover();
	addColumngetBranch();
	addColumngetStatus();

	// addColumngetCategory();
	// addColumngetType();
	// addColumngetGroup();

}

protected void addColumngetApprover() {
	getApproverName = new TextColumn<Service>() {
		@Override
		public String getValue(Service object) {
			return object.getApproverName();
		}
	};
	table.addColumn(getApproverName, "Approver");
	table.setColumnWidth(getApproverName, 120, Unit.PX);
	getApproverName.setSortable(true);
}

@Override
protected void initializekeyprovider() {
	keyProvider = new ProvidesKey<Service>() {
		@Override
		public Object getKey(Service item) {
			if (item == null) {
				return null;
			} else
				return item.getId();
		}
	};
}

@Override
public void setEnable(boolean state) {
}

@Override
public void applyStyle() {
}

public void addColumnSorting() {
	addSortinggetCount();
	addSortinggetCreationDate();
//	addSortinggetServiceCount();
	addSortinggetContractCount();

	addSortinggetCustomerId();
	addSortinggetCustomerFullName();
	addSortinggetCustomerCellNumber();

	addSortinggetApproverName();
	addSortinggetBranch();
	addSortinggetEmployee();
	addSortinggetStatus();

	// addSortinggetType();
	// addSortinggetGroup();
	// addSortinggetCategory();
}

protected void addSortinggetApproverName() {
	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(getApproverName, new Comparator<Service>() {
		@Override
		public int compare(Service e1, Service e2) {
			if (e1 != null && e2 != null) {
				if (e1.getApproverName() != null
						&& e2.getApproverName() != null) {
					return e1.getApproverName().compareTo(
							e2.getApproverName());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}



@Override
public void addFieldUpdater() {
}

protected void addSortinggetCount() {
	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(getCountColumn, new Comparator<Service>() {
		@Override
		public int compare(Service e1, Service e2) {
			if (e1 != null && e2 != null) {
				if (e1.getCount() == e2.getCount()) {
					return 0;
				}
				if (e1.getCount() > e2.getCount()) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCount() {
	getCountColumn = new TextColumn<Service>() {
		@Override
		public String getValue(Service object) {
			if (object.getCount() == -1)
				return " ";
			else
				return object.getCount() + "";
		}
	};
	table.addColumn(getCountColumn, "ID");

	table.setColumnWidth(getCountColumn, 100,Unit.PX);
	getCountColumn.setSortable(true);
}

protected void addColumngetCreationDate() {
	DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
	DatePickerCell date = new DatePickerCell(fmt);
//	final Date add7Days=new Date();
//	CalendarUtil.addDaysToDate(add7Days,7);
//	System.out.println("Date + 7 Days ::: "+add7Days);
	
//	final Date sub30Days=new Date();
//	DateFormatUtilityClient.getDateWithTimeZone("IST", sub30Days);
//	System.out.println("Todays Date ::: "+sub30Days);
//	
//	Calendar calBeg=DateFormatUtilityClient.setTimeToBeginningOfDay(DateFormatUtilityClient.getCalendarForDate(sub30Days));
	final Date todaysDate=new Date();
	CalendarUtil.addDaysToDate(todaysDate,-1);
	todaysDate.setHours(23);
	todaysDate.setMinutes(59);
	todaysDate.setSeconds(59);
	System.out.println("Todays Date Converted ::: "+todaysDate);
	
	getCreationDateColumn = new TextColumn<Service>() {
		@Override
		public String getCellStyleNames(Context context, Service object) {
			if((object.getServiceDate().before(todaysDate))&&(object.getStatus().equals("Scheduled"))){
				 return "red";
			 }
			else if(object.getStatus().equals("Completed")){
				 return "green";
			 }
			else if(object.getStatus().trim().equals("Rescheduled")){
				 return "blue";
			 }
			 else{
				 return "black";
			 }
		}

		@Override
		public String getValue(Service object) {
			if (object.getServiceDate() != null)
				return AppUtility.parseDate(object.getServiceDate());
			else
				return " ";
		}
	};
	table.addColumn(getCreationDateColumn, "Service Date");
	table.setColumnWidth(getCreationDateColumn, 120, Unit.PX);
	getCreationDateColumn.setSortable(true);
}

protected void addSortinggetCreationDate() {
	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(getCreationDateColumn,
			new Comparator<Service>() {
				@Override
				public int compare(Service e1, Service e2) {
					if (e1 != null && e2 != null) {
						if (e1.getServiceDate() != null
								&& e2.getServiceDate() != null) {
							return e1.getServiceDate().compareTo(
									e2.getServiceDate());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addSortinggetBranch() {
	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(getBranchColumn, new Comparator<Service>() {
		@Override
		public int compare(Service e1, Service e2) {
			if (e1 != null && e2 != null) {
				if (e1.getBranch() != null && e2.getBranch() != null) {
					return e1.getBranch().compareTo(e2.getBranch());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetBranch() {
	getBranchColumn = new TextColumn<Service>() {
		@Override
		public String getValue(Service object) {
			return object.getBranch() + "";
		}
	};
	table.addColumn(getBranchColumn, "Branch");
	table.setColumnWidth(getBranchColumn, 90,Unit.PX);
	getBranchColumn.setSortable(true);
}

protected void addSortinggetEmployee() {
	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(getEmployeeColumn, new Comparator<Service>() {
		@Override
		public int compare(Service e1, Service e2) {
			if (e1 != null && e2 != null) {
				if (e1.getEmployee() != null && e2.getEmployee() != null) {
					return e1.getEmployee().compareTo(e2.getEmployee());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetEmployee() {
	getEmployeeColumn = new TextColumn<Service>() {
		@Override
		public String getValue(Service object) {
			return object.getEmployee() + "";
		}
	};
	table.addColumn(getEmployeeColumn, "Technician");
	getEmployeeColumn.setSortable(true);
	table.setColumnWidth(getEmployeeColumn, 100, Unit.PX);

}

protected void addSortinggetStatus() {
	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(getStatusColumn, new Comparator<Service>() {
		@Override
		public int compare(Service e1, Service e2) {
			if (e1 != null && e2 != null) {
				if (e1.getStatus() != null && e2.getStatus() != null) {
					return e1.getStatus().compareTo(e2.getStatus());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetStatus() {
	getStatusColumn = new TextColumn<Service>() {
		@Override
		public String getValue(Service object) {
			return object.getStatus() + "";
		}
	};
	table.addColumn(getStatusColumn, "Status");
	getStatusColumn.setSortable(true);
	table.setColumnWidth(getStatusColumn, 90, Unit.PX);

}

 protected void addSortinggetContractCount()
 {
	 List<Service> list=getDataprovider().getList();
	 columnSort=new ListHandler<Service>(list);
	 columnSort.setComparator(getContractCountColumn, new Comparator<Service>()
	 {
		 @Override
		 public int compare(Service e1,Service e2)
		 {
			 if(e1!=null && e2!=null)
			 {
				 if(e1.getContractCount()== e2.getContractCount()){
					 return 0;}
				 if(e1.getContractCount()> e2.getContractCount()){
					 return 1;}
				 else{
					 return -1;}
			 }
			 else{
				 return 0;}
		 }
 });
 table.addColumnSortHandler(columnSort);
 }

 protected void addColumngetContractCount()
 {
	 getContractCountColumn=new TextColumn<Service>()
	 {
		 @Override
		 public String getValue(Service object)
		 {
			 if( object.getContractCount()==-1)
				 return "  ";
			 else return object.getContractCount()+"";
		 }
	 };
	 table.addColumn(getContractCountColumn,"Contract Id");
	 table.setColumnWidth(getContractCountColumn,90,Unit.PX);
	 getContractCountColumn.setSortable(true);
 }

protected void addSortinggetGroup() {
	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(getGroupColumn, new Comparator<Service>() {
		@Override
		public int compare(Service e1, Service e2) {
			if (e1 != null && e2 != null) {
				if (e1.getGroup() != null && e2.getGroup() != null) {
					return e1.getGroup().compareTo(e2.getGroup());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetGroup() {
	getGroupColumn = new TextColumn<Service>() {
		@Override
		public String getValue(Service object) {
			return object.getGroup() + "";
		}
	};
	table.addColumn(getGroupColumn, "Group");
	table.setColumnWidth(getGroupColumn, 120, Unit.PX);
	getGroupColumn.setSortable(true);
}

// protected void addSortinggetQuotationCount()
// {
// List<Service> list=getDataprovider().getList();
// columnSort=new ListHandler<Service>(list);
// columnSort.setComparator(getQuotationCountColumn, new
// Comparator<Service>()
// {
// @Override
// public int compare(Service e1,Service e2)
// {
// if(e1!=null && e2!=null)
// {
// if(e1.get== e2.getQuotationCount()){
// return 0;}
// if(e1.getQuotationCount()> e2.getQuotationCount()){
// return 1;}
// else{
// return -1;}
// }
// else{
// return 0;}
// }
// });
// table.addColumnSortHandler(columnSort);
// }
//
// protected void addColumngetQuotationCount()
// {
// getQuotationCountColumn=new TextColumn<Service>()
// {
// @Override
// public String getValue(Service object)
// {
// if( object.getQuotationCount()==-1)
// return "N.A";
// else return object.getQuotationCount()+"";
// }
// };
// table.addColumn(getQuotationCountColumn,"Quotation Id");
// table.setColumnWidth(getQuotationCountColumn,90,Unit.PX);
// getQuotationCountColumn.setSortable(true);
// }

protected void addSortinggetCategory() {
	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(getCategoryColumn, new Comparator<Service>() {
		@Override
		public int compare(Service e1, Service e2) {
			if (e1 != null && e2 != null) {
				if (e1.getCategory() != null && e2.getCategory() != null) {
					return e1.getCategory().compareTo(e2.getCategory());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCategory() {
	getCategoryColumn = new TextColumn<Service>() {
		@Override
		public String getValue(Service object) {
			return object.getCategory() + "";
		}
	};
	table.addColumn(getCategoryColumn, "Category");
	table.setColumnWidth(getCategoryColumn, "120px");
	getCategoryColumn.setSortable(true);
}

protected void addSortinggetCustomerId() {
	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(getCustomerIdColumn,
			new Comparator<Service>() {
				@Override
				public int compare(Service e1, Service e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCustomerId() == e2.getCustomerId()) {
							return 0;
						}
						if (e1.getCustomerId() > e2.getCustomerId()) {
							return 1;
						} else {
							return -1;
						}
					} else {
						return 0;
					}
				}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCustomerId() {
	getCustomerIdColumn = new TextColumn<Service>() {
		@Override
		public String getValue(Service object) {
			if (object.getCustomerId() == -1)
				return " ";
			else
				return object.getCustomerId() + "";
		}
	};
	table.addColumn(getCustomerIdColumn, "Customer Id");
	table.setColumnWidth(getCustomerIdColumn, 100, Unit.PX);
	getCustomerIdColumn.setSortable(true);
}

protected void addSortinggetType() {
	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(getTypeColumn, new Comparator<Service>() {
		@Override
		public int compare(Service e1, Service e2) {
			if (e1 != null && e2 != null) {
				if (e1.getType() != null && e2.getType() != null) {
					return e1.getType().compareTo(e2.getType());
				}
			} else {
				return 0;
			}
			return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetType() {
	getTypeColumn = new TextColumn<Service>() {
		@Override
		public String getValue(Service object) {
			return object.getType() + "";
		}
	};
	table.addColumn(getTypeColumn, "Type");
	table.setColumnWidth(getTypeColumn, 100, Unit.PX);
	getTypeColumn.setSortable(true);
}

protected void addSortinggetCustomerFullName() {
	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(getCustomerFullNameColumn,
			new Comparator<Service>() {
				@Override
				public int compare(Service e1, Service e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCustomerName() != null
								&& e2.getCustomerName() != null) {
							return e1.getCustomerName().compareTo(
									e2.getCustomerName());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
	table.addColumnSortHandler(columnSort);

}

protected void addColumngetCustomerFullName() {
	getCustomerFullNameColumn = new TextColumn<Service>() {
		@Override
		public String getValue(Service object) {
			return object.getCustomerName() + "";
		}
	};
	table.addColumn(getCustomerFullNameColumn, "Customer Name");
	table.setColumnWidth(getCustomerFullNameColumn, 120, Unit.PX);
	getCustomerFullNameColumn.setSortable(true);
}

protected void addSortinggetCustomerCellNumber() {
	List<Service> list = getDataprovider().getList();
	columnSort = new ListHandler<Service>(list);
	columnSort.setComparator(getCustomerCellNumberColumn,
			new Comparator<Service>() {
				@Override
				public int compare(Service e1, Service e2) {
					if (e1 != null && e2 != null) {
						if (e1.getCellNumber() == e2.getCellNumber()) {
							return 0;
						}
						if (e1.getCellNumber() > e2.getCellNumber()) {
							return 1;
						} else {
							return -1;
						}
					} else {
						return 0;
					}
				}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCustomerCellNumber() {
	getCustomerCellNumberColumn = new TextColumn<Service>() {
		@Override
		public String getValue(Service object) {
			return object.getCellNumber() + "";
		}
	};
	table.addColumn(getCustomerCellNumberColumn, "Customer Cell");
	table.setColumnWidth(getCustomerCellNumberColumn, 120, Unit.PX);
	getCustomerCellNumberColumn.setSortable(true);
}
//
//protected void addSortinggetServiceCount() {
//	List<Service> list = getDataprovider().getList();
//	columnSort = new ListHandler<Service>(list);
//	columnSort.setComparator(getServiceCountColumn,
//			new Comparator<Service>() {
//				@Override
//				public int compare(Service e1, Service e2) {
//					if (e1 != null && e2 != null) {
//						if (e1.getCount() == e2.getCount()) {
//							return 0;
//						}
//						if (e1.getCount() > e2.getCount()) {
//							return 1;
//						} else {
//							return -1;
//						}
//					} else {
//						return 0;
//					}
//				}
//			});
//	table.addColumnSortHandler(columnSort);
//}

//protected void addColumngetServiceCount() {
//	getServiceCountColumn = new TextColumn<Service>() {
//		@Override
//		public String getValue(Service object) {
//			if (object.getCount() == -1)
//				return "N.A";
//			else
//				return object.getCount() + "";
//		}
//	};
//	table.addColumn(getServiceCountColumn, "Service Id");
//	table.setColumnWidth(getServiceCountColumn, 90, Unit.PX);
//
//	getServiceCountColumn.setSortable(true);
//}
}
