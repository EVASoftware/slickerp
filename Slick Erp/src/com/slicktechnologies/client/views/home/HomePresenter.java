package com.slicktechnologies.client.views.home;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.MyInlineLabel;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.SalesLineItemTable;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.client.views.quotation.SalesLineItemQuotationTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;

public class HomePresenter implements ClickHandler
{
	
HomeForm form;
HomePresenterSearchProxy searchpopup;
protected GenricServiceAsync service=GWT.create(GenricService.class);

/**
 * Updated By: Viraj
 * Date: 17-01-2019
 * Description: To fill data of lead,quotation and contract in popup
 */
public static PopupPanel docLeadPanel = null;
VerticalPanel docVerticalPanel = null;
FlowPanel docflowpanel = null;
VerticalPanel mainDocVerticalPanel = null;
/** Ends **/

public  HomePresenter(HomeForm form,HomePresenterSearchProxy searchpopup)
{
	this.form=form;
	this.searchpopup=searchpopup;
	SetEventHandling();
	searchpopup.applyHandler(this);
	form.getBtnGo().addClickHandler(this);
	
	Date date=new Date();
	System.out.println("Today Date ::::::::: "+date);
	// **** Here We Add 7 Days to current date and pass this date to default search.
	CalendarUtil.addDaysToDate(date,7);
    System.out.println("Changed Date + 7 ::::::::: "+date);
	
    /**
     * Updated By: Viraj
     * Date: 17-01-2019
     * Description: To fill data of lead,quotation and contract in popup
     */
    docLeadPanel = new PopupPanel(true);
    docflowpanel = new FlowPanel();
    mainDocVerticalPanel = new VerticalPanel();
    
    mainDocVerticalPanel.add(docflowpanel);
	
	docVerticalPanel = new VerticalPanel();
	mainDocVerticalPanel.add(docVerticalPanel);
			
	docLeadPanel.add(mainDocVerticalPanel);
	/** Ends **/
    
	Vector<Filter> filtervec=null;
	Filter filter = null;
	MyQuerry querry;
//	List<String> statusList;
	
	filtervec=new Vector<Filter>();
	
	/**
	 * Date : 02-08-2017 By ANIL
	 * Loading all the status of lead except Successful,Unsuccessful,Cancelled,Cancel,Rejected and Closed.
	 */
	List<String> statusList=new ArrayList<String>();
	statusList=AppUtility.getConfigValue(6);
	System.out.println("STATUS LIST SIZE:  "+statusList.size());
	
	for(int i=0;i<statusList.size();i++){
		if(statusList.get(i).trim().equalsIgnoreCase(AppConstants.Sucessful)
				/**
				 * UPdated By: Viraj
				 * Date: 10-01-2019
				 * Description: To remove successful status
				 */
				||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Successful)
				||statusList.get(i).trim().equalsIgnoreCase("successfull")
//				||statusList.get(i).trim().equalsIgnoreCase("unsuccessfull")
				/** Ends **/
				||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Unsucessful)
				||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CANCELLED)
				||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CANCEL)
				||statusList.get(i).trim().equalsIgnoreCase(AppConstants.REJECTED)
				||statusList.get(i).trim().equalsIgnoreCase(AppConstants.CLOSED)
				||statusList.get(i).trim().equalsIgnoreCase("Completed")
				||statusList.get(i).trim().equalsIgnoreCase(AppConstants.Successful)
				||statusList.get(i).trim().equalsIgnoreCase("Quotation Sent")
				||statusList.get(i).trim().equalsIgnoreCase("Quotation Created")
				||statusList.get(i).trim().equalsIgnoreCase("Revised")){
			statusList.remove(i);
			i--;
		}
	}
	System.out.println("AFTER STATUS LIST SIZE:  "+statusList.size());
	filter = new Filter();
	filter.setQuerryString("status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	/**
	 * End
	 */
	
	
	/**
	 * Old COde
	 */
//	filter = new Filter();
//	filter.setQuerryString("status");
//	filter.setStringValue(Lead.CREATED);
//	filtervec.add(filter);
	
//	/**
//	 * New Code 
//	 * Developed by : Rohan  Bhagde.
//	 * reason :added this because of lead status is configurable.
//	 * so we have to load that in dashboard 
//	 * Now it will load all the status except these ('Successful','Cancelled,''Cancel','Rejected') 
//	 */
//	List<String> leadStatusList;
//	leadStatusList=new ArrayList<String>();
//	leadStatusList.add("Successful");
//	leadStatusList.add("Cancelled");
//	leadStatusList.add("Rejected");
//	leadStatusList.add("Cancel");
//	
//	filter = new Filter();
//	filter.setQuerryString("status OUT");
//	filter.setList(leadStatusList);
//	filtervec.add(filter);
//	/**
//	 * ends here  
//	 */
	
	
	
	/**
	 * date 18/10/2017 added by komal to show records according to follow-up
	 * date in lead
	 **/
	
//	filter = new Filter();
//	filter.setQuerryString("creationDate <=");
//	filter.setDateValue(date);
//	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("followUpDate <=");
	filter.setDateValue(date);
	filtervec.add(filter);
	
	/**
	 * End
	 */
	
	/**
	 * Developed by : Rohan added this branch  
	 */
		List<String> branchList ;
		branchList= new ArrayList<String>();
		System.out.println("item count in branch "+LoginPresenter.globalBranch.size());
		for (int i = 0; i < LoginPresenter.globalBranch.size(); i++) {
			branchList.add(LoginPresenter.globalBranch.get(i).getBusinessUnitName());
		}
		System.out.println("branchList list "+branchList.size());
	
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
	/**
	 * ends here 
	 */
	
	querry=new MyQuerry();
	querry.setQuerryObject(new Lead());
	querry.setFilters(filtervec);
	this.retriveTable(querry,form.getLeadTable());
	
	
	
	////////////////////////////////////Create Querry For Quotation///////////////////////////// 
	
	
	filtervec=new Vector<Filter>();
	statusList=new ArrayList<String>();
	statusList.add(Quotation.CREATED);
	statusList.add(Quotation.REQUESTED);
	statusList.add(Quotation.APPROVED);
	
	filter = new Filter();
	filter.setQuerryString("status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	
	
	/**
	 * Developed by : Rohan added this branch  
	 */
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
	
	/**
	 * ends here 
	 */
		
		/**
		 * date 18/10/2017 added by komal to show records according to followup
		 * date in quotation
		 **/
		filter = new Filter();
		filter.setQuerryString("followUpDate <=");
		filter.setDateValue(date);
		filtervec.add(filter);
		
	//	filter = new Filter();
	//	filter.setQuerryString("creationDate <=");
	//	filter.setDateValue(date);
	//	filtervec.add(filter);
		
		/**
		 * End
		 */
	

	
    querry=new MyQuerry();
	querry.setQuerryObject(new Quotation());
	querry.setFilters(filtervec);
	retriveTable(querry,form.getQuotationTable());
	
////////////////////////////////////Create Querry For Contract///////////////////////////// 
	
	filtervec=new Vector<Filter>();
	statusList=new ArrayList<String>();
	statusList.add(Contract.CREATED);
	statusList.add(Contract.REQUESTED);
	
	filter = new Filter();
	filter.setQuerryString("status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	
	/**
	 * Developed by : Rohan added this branch  
	 */
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
	/**
	 * ends here 
	 */
	
	filter = new Filter();
	filter.setQuerryString("startDate <=");
	filter.setDateValue(date);
	filtervec.add(filter);
	
	querry=new MyQuerry();
	querry.setQuerryObject(new Contract());
	querry.setFilters(filtervec);
	retriveTable(querry,form.getContractTable());
	
	//////////////////////////////////// Create Query For Contract ///////////////////////////////////////
	
	filtervec=new Vector<Filter>();
//	statusList=new ArrayList<String>();
//	statusList.add(Contract.APPROVED);
	
	filter = new Filter();
	filter.setQuerryString("status");
	filter.setStringValue(Contract.APPROVED);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("customerInterestFlag");
	filter.setBooleanvalue(false);
	filtervec.add(filter);
	
	
	/**
	 * Developed by : Rohan added this branch  
	 */
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
	/**
	 * ends here 
	 */
	
	filter = new Filter();
	filter.setQuerryString("endDate <=");
	filter.setDateValue(date);
	filtervec.add(filter);
	
	querry=new MyQuerry();
	querry.setQuerryObject(new Contract());
	querry.setFilters(filtervec);
	retriveTable(querry,form.getContractTable());
	
	//////////////////////////////////// Create Query For Service //////////////////////////////////////////
	
	filtervec=new Vector<Filter>();
	statusList=new ArrayList<String>();
	statusList.add(Service.SERVICESTATUSRESCHEDULE);
	statusList.add(Service.SERVICESTATUSSCHEDULE);
	
	filter = new Filter();
	filter.setQuerryString("status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	
	
	/**
	 * Developed by : Rohan added this branch  
	 */
		filter = new Filter();
		filter.setQuerryString("branch IN");
		filter.setList(branchList);
		filtervec.add(filter);
	/**
	 * ends here 
	 */
	
	filter = new Filter();
	filter.setQuerryString("serviceDate <=");
	filter.setDateValue(date);
	filtervec.add(filter);
	
	
	
	
	querry=new MyQuerry();
	querry.setQuerryObject(new Service());
	querry.setFilters(filtervec);
	retriveTable(querry,form.getServiceTable());
	
	
	setTableSelectionOnLeads();
	setTableSelectionOnContract();
	setTableQuotation();
	setTableSelectionOnService();
	}

public static void initalize()
{
	HomeForm homeForm = new HomeForm();
	AppMemory.getAppMemory().currentScreen=Screen.HOME;
	AppMemory.getAppMemory().skeleton.getProcessName().setText("Dashboard/Home");
	HomePresenterSearchProxy searchpopup=new HomePresenterSearchProxy();
	HomePresenter presenter= new HomePresenter(homeForm,searchpopup);
	AppMemory.getAppMemory().stickPnel(homeForm);	
}




@Override
public void onClick(ClickEvent event) {
	
	if(event.getSource() instanceof InlineLabel)
	{
	InlineLabel lbl= (InlineLabel) event.getSource();
	String text= lbl.getText();
	if(text.equals("Search"))
		searchpopup.showPopUp();
	
	if(text.equals("Go"))
		reactOnGo();
	}
	
	if(event.getSource().equals(form.getBtnGo())){
		if(form.getOlbbranch().getSelectedIndex()==0&&
		form.getOlbsalesPerson().getSelectedIndex()==0&&
		form.getDbFromDate().getValue()==null&&form.getDbToDate().getValue()==null){
	
		final GWTCAlert alert = new GWTCAlert();
		alert.alert("Select atleast one field to search");
	}
		
	else{	
		
		if(form.getOlbsalesPerson().getSelectedIndex()!=0&&form.getOlbbranch().getSelectedIndex()==0
		   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
			
			System.out.println("salesperson ");
			searchByFromToDate();
			
		}
		else if(form.getOlbsalesPerson().getSelectedIndex()==0&&form.getOlbbranch().getSelectedIndex()!=0
				   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
			
			
			System.out.println("branch ");
			searchByFromToDate();
		}
		else if(form.getOlbsalesPerson().getSelectedIndex()!=0&&form.getOlbbranch().getSelectedIndex()!=0
				   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
			
			System.out.println("for both sales person and branch");
			searchByFromToDate();
		}
		else{
		System.out.println("Remaining all situations ");
		
		if(form.getDbFromDate().getValue()==null||form.getDbToDate().getValue()==null){
			final GWTCAlert alert = new GWTCAlert();
			alert.alert("Select From and To Date.");
		}
		 if(form.getDbFromDate().getValue()!=null&&form.getDbToDate()!=null){
		Date formDate=form.getDbFromDate().getValue();
		Date toDate=form.getDbToDate().getValue();
		if(toDate.equals(formDate)||toDate.after(formDate)){
			System.out.println("From Date ::::::::: "+formDate);
			CalendarUtil.addDaysToDate(formDate,7);
			System.out.println("Changed Date ::::::::: "+formDate);
			System.out.println("To Date ::::::::: "+toDate);
		
			if(toDate.before(formDate)){
				System.out.println("Inside Date Condition"); 
				searchByFromToDate();
			}
			else{
				final GWTCAlert alert = new GWTCAlert();
				alert.alert("From Date and To Date Difference Can Not Be More Than 7 Days.");
			}
		}
		else{
			final GWTCAlert alert = new GWTCAlert();
			alert.alert("To Date should be greater than From Date.");
		}
		}
		}
	}
	}
		
}

/**
 * Sets the event handling.
 */
public void SetEventHandling()
{
	 InlineLabel label[] =AppMemory.getAppMemory().skeleton.getMenuLabels();
	  for(int k=0;k<label.length;k++)
	  {
		 if( AppMemory.getAppMemory().skeleton.registration[k]!=null)
		    AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
		 
			 
	  }
	 
	  for( int k=0;k<label.length;k++)
	  {
		  
		  AppMemory.getAppMemory().skeleton.registration[k]= label[k].addClickHandler(this);
	  }
}

public void reactOnGo()
{ 

	
	if(!searchpopup.personInfo.getId().getValue().equals("")&&!searchpopup.personInfo.getName().getValue().equals("")&&!searchpopup.personInfo.getPhone().getValue().equals(""))
	{
		MyQuerry quer=createSalesFilter(new Contract());
		 this.form.contractTable.connectToLocal();
		retriveTable(quer, form.getContractTable());
		 
		quer=createLeadFilter();
		 this.form.leadTable.connectToLocal();
		 retriveTable(quer, form.getLeadTable());
		 
		 quer=createSalesFilter(new Quotation());
		 this.form.quotationTable.connectToLocal();
		 retriveTable(quer, form.getQuotationTable());
		 
		 quer=createServiceFilter(new Service());
		 this.form.serviceTable.connectToLocal();
		 retriveTable(quer, form.getServiceTable());
		 searchpopup.hidePopUp();
	}
	
	
	if(searchpopup.personInfo.getId().getValue().equals("")&&searchpopup.personInfo.getName().getValue().equals("")&&searchpopup.personInfo.getPhone().getValue().equals(""))
	{
		GWTCAlert alertMsg=new GWTCAlert();
		alertMsg.alert("Please enter customer information");
	}
	
}

private MyQuerry createLeadFilter()
{
	Vector<Filter>filterVec = new Vector<Filter>();
	 Filter temp;
	 
	 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
			temp.setQuerryString("personInfo.count");
			filterVec.add(temp);	
		}
		
		if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.personInfo.getName().getValue());
			temp.setQuerryString("personInfo.fullName");
			filterVec.add(temp);	
		}
		
		if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
			temp.setQuerryString("personInfo.cellNumber");
			filterVec.add(temp);	
		}
		
		MyQuerry lead=new MyQuerry();
		lead.setQuerryObject(new Lead());
		lead.setFilters(filterVec);
		
		return lead;
}

private MyQuerry createSalesFilter(Sales sales)
{
	Vector<Filter>filterVec = new Vector<Filter>();
	 Filter temp;
	 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
			temp.setQuerryString("cinfo.count");
			filterVec.add(temp);	
		}
		
		if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.personInfo.getName().getValue());
			temp.setQuerryString("cinfo.fullName");
			filterVec.add(temp);	
		}
		
		if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
			temp.setQuerryString("cinfo.cellNumber");
			filterVec.add(temp);	
		}
		
		MyQuerry quotation=new MyQuerry();
		quotation.setFilters(filterVec);
		quotation.setQuerryObject(sales);
		
		return quotation;
}



private MyQuerry createServiceFilter(Service sales)
{
	Vector<Filter>filterVec = new Vector<Filter>();
	 Filter temp;
	 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
			temp.setQuerryString("personInfo.count");
			filterVec.add(temp);	
		}
		
		if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.personInfo.getName().getValue());
			temp.setQuerryString("personInfo.fullName");
			filterVec.add(temp);	
		}
		
		if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
			temp.setQuerryString("personInfo.cellNumber");
			filterVec.add(temp);	
		}
		
		MyQuerry quotation=new MyQuerry();
		quotation.setFilters(filterVec);
		quotation.setQuerryObject(sales);
		
		return quotation;
}

public void setContractRedirection()
 {
	    
 }

 public void setLeadRedirection()
 {
	 
 }
 
 public void setServiceRedirection()
 {
	 	
		
 }
 
 public void setQuotationRedirection()
 {
	    
		
 }	
/**
 * Updated By: Viraj
 * Date: 17-01-2019
 * Description: To remove items added in popup
 */
 void removePopObjects() {
		try {

			for (Widget wi : docVerticalPanel) {
				boolean flag = docLeadPanel.remove(wi);
				wi.removeFromParent();
				System.out.println("get item remove status " + flag);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
/** Ends **/
//Big Patch fix search 
 public void setTableSelectionOnLeads()
 {final NoSelectionModel<Lead> selectionModelMyObj = new NoSelectionModel<Lead>();

	SelectionChangeEvent.Handler tableHandler = new SelectionChangeEvent.Handler() {
		 @Override
      public void onSelectionChange(SelectionChangeEvent event) 
      {
     	 
     	  Timer timer1 = new Timer(){

				@Override
				public void run() {
					if(LeadPresenterTableProxy.followUpFlag == false){
						/**
						 * Updated By:Viraj
						 * Date: 17-01-2019
						 * Description: To display lead in popup
						 */
						 removePopObjects();
						 
//						 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Lead",Screen.LEAD);
			        	 final Lead entity=selectionModelMyObj.getLastSelectedObject();
//			        	 final LeadForm form = LeadPresenter.initalize();
			        	 final LeadForm form = new LeadForm(); 
			        	 
			        	 form.content.getElement().getStyle().setHeight(570, Unit.PX);
			 			 form.content.getElement().getStyle().setWidth(950, Unit.PX);
			 			 docVerticalPanel.add(form.content);
						 docLeadPanel.setHeight("600px");
						 docLeadPanel.show();
						 docLeadPanel.center();
							form.showWaitSymbol();
							Timer timer=new Timer() {
			 				
			 				@Override
			 				public void run() {
			 					form.hideWaitSymbol();
							     form.updateView(entity);
							     form.setProcessLevelBar(null);
							     form.setToViewState();
							     form.getPic().setEnabled(false);
			 				}
			 				/** Ends **/
			 			};
			             timer.schedule(5000); 
					}else{
						LeadPresenterTableProxy.followUpFlag = false;
					}
				}
     	  };
     	  timer1.schedule(1000);
		}
	};
	// Add the handler to the selection model
	selectionModelMyObj.addSelectionChangeHandler(tableHandler);
	// Add the selection model to the table
	form.leadTable.getTable().setSelectionModel(selectionModelMyObj);}
 
 private void setTableSelectionOnService()
 {
	 final NoSelectionModel<Service> selectionModelMyObj = new NoSelectionModel<Service>();
     
     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
     {
         @Override
         public void onSelectionChange(SelectionChangeEvent event) 
         {
        	 setContractRedirection();
        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Service",Screen.SERVICE);
        	 final Service entity=selectionModelMyObj.getLastSelectedObject();
        	 final DeviceForm form = DevicePresenter.initalize();
				
				form.showWaitSymbol();
				AppMemory.getAppMemory().stickPnel(form);
				Timer timer=new Timer() {
 				
 				@Override
 				public void run() {
 					 form.hideWaitSymbol();
 					 form.getPoc().setEnable(false);
				     form.updateView(entity);
				     form.setToViewState();
 				}
 			};
            
             timer.schedule(5000); 
                         
          }
     };
     // Add the handler to the selection model
     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
     // Add the selection model to the table
     form.serviceTable.getTable().setSelectionModel(selectionModelMyObj);

 }
 public void setTableSelectionOnContract()
 {
	 final NoSelectionModel<Contract> selectionModelMyObj = new NoSelectionModel<Contract>();
     
     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
     {
         @Override
         public void onSelectionChange(SelectionChangeEvent event) 
         {

        	 Timer timer1 = new Timer(){

				@Override
					public void run() {
						/**
						 * 4.1.2019 added by komal to differentiate button click
						 **/
						if (ContractPresenterTableProxy.communicationFlag == false) {
							setContractRedirection();
							/**
							 * Updated By:Viraj
							 * Date: 17-01-2019
							 * Description: To display Contract in popup
							 */
							 removePopObjects();
							 
//							AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract", Screen.CONTRACT);
							final Contract entity = selectionModelMyObj.getLastSelectedObject();
							SalesLineItemTable.newModeFlag = true;
							final ContractForm form = new ContractForm();
//							final ContractForm form = ContractPresenter.initalize();
							form.content.getElement().getStyle().setHeight(570, Unit.PX);
				 			form.content.getElement().getStyle().setWidth(950, Unit.PX);
				 			docVerticalPanel.add(form.content);
							docLeadPanel.setHeight("600px");
							docLeadPanel.show();
							docLeadPanel.center();
							
							form.showWaitSymbol();
//							AppMemory.getAppMemory().stickPnel(form);
							Timer timer = new Timer() {

								@Override
								public void run() {
									form.hideWaitSymbol();
									form.getPersonInfoComposite().setEnable(false);
									form.updateView(entity);
									form.setProcessLevelBar(null);
									form.setToViewState();
								}
								/** Ends **/
							};

							timer.schedule(5000);
						} else {
							ContractPresenterTableProxy.communicationFlag = false;
						}
					}
        	 };
        	 timer1.schedule(1000);
          
        	 
          }
     };
     // Add the handler to the selection model
     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
     // Add the selection model to the table
     form.contractTable.getTable().setSelectionModel(selectionModelMyObj);
 }
 
 
 public void setTableQuotation()
 {
		final NoSelectionModel<Quotation> selectionModelMyObj = new NoSelectionModel<Quotation>();
		 SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	     {
	         @Override
	         public void onSelectionChange(SelectionChangeEvent event) 
	         {
	        	 Timer timer1 = new Timer(){

					@Override
					public void run() {
						if(QuotationPresenterTableProxy.followUpFlag == false){
							
				        	 setQuotationRedirection();
				        	 /**
							 * Updated By:Viraj
							 * Date: 17-01-2019
							 * Description: To display Quotation in popup
							 */
							 removePopObjects();
							 
//				             AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quotation",Screen.QUOTATION);
				             final Quotation entity=selectionModelMyObj.getLastSelectedObject();
				             SalesLineItemQuotationTable.newModeFlag=true;
				             final QuotationForm form = new QuotationForm();
//				             final QuotationForm form = QuotationPresenter.initalize();
				             
				             form.content.getElement().getStyle().setHeight(570, Unit.PX);
			 				 form.content.getElement().getStyle().setWidth(950, Unit.PX);
				 			 docVerticalPanel.add(form.content);
							 docLeadPanel.setHeight("600px");
							 docLeadPanel.show();
							 docLeadPanel.center();
				             
				             form.showWaitSymbol();
				             Timer timer=new Timer() {
								@Override
								public void run() {
								     form.hideWaitSymbol();
								     form.getPersonInfoComposite().setEnable(false);
								     form.updateView(entity);
								     form.setProcessLevelBar(null);
								     form.setToViewState();
								}
								/** Ends **/
							};
				            timer.schedule(5000); 
					}
					else
					 {
						 QuotationPresenterTableProxy.followUpFlag = false;
					 }	
					}
	        	 };
	        	 timer1.schedule(1000);
	          }
	     };
		// Add the handler to the selection model
		selectionModelMyObj.addSelectionChangeHandler(tableHandler);
		// Add the selection model to the table
		form.quotationTable.getTable().setSelectionModel(selectionModelMyObj);}
 
 
 private void searchByFromToDate(){
		
		
		System.out.println("Hiiiiiiiiiiii........");
		Long compId=UserConfiguration.getCompanyId();
		System.out.println("Company Id :: "+compId);
		    
		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		List<String> statusList;
		
		filtervec=new Vector<Filter>();
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
//		/**
//		 * Updated By: Viraj
//		 * Date: 10-01-2019
//		 * Description: To add Status filter in dashboard
//		 */
//		if(form.getOlbLeadStatus().getSelectedIndex() != 0) {
//			filter = new Filter();
//			filter.setQuerryString("status");
//			filter.setStringValue(form.getOlbLeadStatus().getValue());
//			filtervec.add(filter);
//		} else {
			filter = new Filter();
			filter.setQuerryString("status");
			filter.setStringValue(Lead.CREATED);
			filtervec.add(filter);
//		}
		/** Ends **/
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate >=");
		filter.setDateValue(form.getDbFromDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
			
		filter = new Filter();
		filter.setQuerryString("creationDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
		System.out.println("inside lead***********");
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);
			
		}
		
		if(form.getOlbsalesPerson().getSelectedIndex()!=0){
			System.out.println("for service    ");
			filter = new Filter();
			filter.setQuerryString("employee");
			filter.setStringValue(form.getOlbsalesPerson().getValue());
			filtervec.add(filter);
		}
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Lead());
		querry.setFilters(filtervec);
		this.retriveTable(querry,form.getLeadTable());
		
		
		////////////////////////////////////Create Querry For Quotation///////////////////////////// 
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(Quotation.CREATED);
		statusList.add(Quotation.REQUESTED);
		statusList.add(Quotation.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate >=");
		filter.setDateValue(form.getDbFromDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("creationDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
//		/**
//		 * Updated By: Viraj
//		 * Date: 10-01-2019
//		 * Description: To add Status filter in dashboard
//		 */
//		if(form.getOlbLeadStatus().getSelectedIndex() != 0) {
//			filter = new Filter();
//			filter.setQuerryString("status");
//			filter.setStringValue(form.getOlbLeadStatus().getValue());
//			filtervec.add(filter);
//		} else {
			filter = new Filter();
			filter.setQuerryString("status IN");
			filter.setList(statusList);
			filtervec.add(filter);
//		}
		/** Ends **/
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("for quotation ******");
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);
			
		}
		
		if(form.getOlbsalesPerson().getSelectedIndex()!=0){
			System.out.println("for service    ");
			filter = new Filter();
			filter.setQuerryString("employee");
			filter.setStringValue(form.getOlbsalesPerson().getValue());
			filtervec.add(filter);
		}
		
		
	    querry=new MyQuerry();
		querry.setQuerryObject(new Quotation());
		querry.setFilters(filtervec);
		retriveTable(querry,form.getQuotationTable());
		
		///////////Create Querry For Contract/////////////////////////////////////////////////////
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(Contract.CREATED);
		statusList.add(Contract.REQUESTED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
//		/**
//		 * Updated By: Viraj
//		 * Date: 10-01-2019
//		 * Description: To add Status filter in dashboard
//		 */
//		if(form.getOlbLeadStatus().getSelectedIndex() != 0) {
//			filter = new Filter();
//			filter.setQuerryString("status");
//			filter.setStringValue(form.getOlbLeadStatus().getValue());
//			filtervec.add(filter);
//		} else {
			filter = new Filter();
			filter.setQuerryString("status IN");
			filter.setList(statusList);
			filtervec.add(filter);
//		}
		/** Ends **/
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("endDate >=");
		filter.setDateValue(form.getDbFromDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("endDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("for contract ****");
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);
			
		}
		
		
		if(form.getOlbsalesPerson().getSelectedIndex()!=0){
			System.out.println("for service    ");
			filter = new Filter();
			filter.setQuerryString("employee");
			filter.setStringValue(form.getOlbsalesPerson().getValue());
			filtervec.add(filter);
		}
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Contract());
		querry.setFilters(filtervec);
		retriveTable(querry,form.getContractTable());
		
		////////////////////Create Querry For Service//////////////////////
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(Service.SERVICESTATUSRESCHEDULE);
		statusList.add(Service.SERVICESTATUSSCHEDULE);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
//		/**
//		 * Updated By: Viraj
//		 * Date: 10-01-2019
//		 * Description: To add Status filter in dashboard
//		 */
//		if(form.getOlbLeadStatus().getSelectedIndex() != 0) {
//			filter = new Filter();
//			filter.setQuerryString("status");
//			filter.setStringValue(form.getOlbLeadStatus().getValue());
//			filtervec.add(filter);
//		} else {
			filter = new Filter();
			filter.setQuerryString("status IN");
			filter.setList(statusList);
			filtervec.add(filter);
//		}
		/** Ends **/
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("serviceDate >=");
		filter.setDateValue(form.getDbFromDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("serviceDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}	 
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("for service    ");
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(form.getOlbbranch().getValue());
			filtervec.add(filter);
		}
		
		if(form.getOlbsalesPerson().getSelectedIndex()!=0){
			System.out.println("for service    ");
			filter = new Filter();
			filter.setQuerryString("employee");
			filter.setStringValue(form.getOlbsalesPerson().getValue());
			filtervec.add(filter);
		}
		
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Service());
		querry.setFilters(filtervec);
		retriveTable(querry,form.getServiceTable());
	}
 
 
 
 
 public <T> void retriveTable(MyQuerry querry,final SuperTable<T>table)
 {
	 
			table.connectToLocal();
			service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				
				@SuppressWarnings("unchecked")
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					for(SuperModel model:result)
					{
						table.getListDataProvider().getList().add((T) model);
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
				caught.printStackTrace();
					
				}
			}); 
 }
 
//*************************rohan added thismethod for search by branch ******************
 
//	public MyQuerry getQuerry()
//	{
//		Vector<Filter> filtervec=new Vector<Filter>();
//		Filter temp=null;
//		if(dateComparator.getValue()!=null)
//		{
//			filtervec.addAll(dateComparator.getValue());
//		}
//		if(olbEmployee.getSelectedIndex()!=0){
//			temp=new Filter();temp.setStringValue(olbEmployee.getValue().trim());
//			temp.setQuerryString("employee");
//			filtervec.add(temp);
//		}
//		if(olbBranch.getSelectedIndex()!=0){
//			temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
//			temp.setQuerryString("branch");
//			filtervec.add(temp);
//		}
//		if(personInfo.getIdValue()!=-1)
//		  {
//		  temp=new Filter();
//		  temp.setIntValue(personInfo.getIdValue());
//		  temp.setQuerryString("cinfo.count");
//		  filtervec.add(temp);
//		  }
//		  
//		  if(!(personInfo.getFullNameValue().equals("")))
//		  {
//		  temp=new Filter();
//		  temp.setStringValue(personInfo.getFullNameValue());
//		  temp.setQuerryString("cinfo.fullName");
//		  filtervec.add(temp);
//		  }
//		  if(personInfo.getCellValue()!=-1l)
//		  {
//		  temp=new Filter();
//		  temp.setLongValue(personInfo.getCellValue());
//		  temp.setQuerryString("cinfo.cellNumber");
//		  filtervec.add(temp);
//		  }
//		  if(tbQuotationId.getValue()!=null){
//				temp=new Filter();
//				temp.setIntValue(tbQuotationId.getValue());
//				filtervec.add(temp);
//				temp.setQuerryString("quotationCount");
//				filtervec.add(temp);
//			}
//			if(tbLeadId.getValue()!=null){
//				temp=new Filter();
//				temp.setIntValue(tbLeadId.getValue());
//				filtervec.add(temp);
//				temp.setQuerryString("leadCount");
//				filtervec.add(temp);
//			}
//			if(tbContractId.getValue()!=null){
//				temp=new Filter();
//				temp.setIntValue(tbContractId.getValue());
//				filtervec.add(temp);
//				temp.setQuerryString("count");
//				filtervec.add(temp);
//			}
//			
//		
//		if(olbContractGroup.getSelectedIndex()!=0){
//			temp=new Filter();
//			temp.setStringValue(olbContractGroup.getValue().trim());
//			temp.setQuerryString("group");
//			filtervec.add(temp);
//		}
//		
//		if(olbContractCategory.getSelectedIndex()!=0){
//			temp=new Filter();temp.setStringValue(olbContractCategory.getValue().trim());
//			temp.setQuerryString("category");
//			filtervec.add(temp);
//		}
//		
//		if(olbContractType.getSelectedIndex()!=0){
//			temp=new Filter();temp.setStringValue(olbContractType.getValue().trim());
//			temp.setQuerryString("type");
//			filtervec.add(temp);
//		}
//		
//		if(olbContractStatus.getSelectedIndex()!=0){
//			temp=new Filter();temp.setStringValue(olbContractStatus.getValue(olbContractStatus.getSelectedIndex()).trim());
//			temp.setQuerryString("status");
//			filtervec.add(temp);
//		}
//		
//		
//		MyQuerry querry= new MyQuerry();
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new SalesOrder());
//		return querry;
//	}
 
 
 
 

}
