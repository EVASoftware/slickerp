package com.slicktechnologies.client.views.home;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogPopUp;
import com.slicktechnologies.client.views.contract.ContractPresenter.ContractPresenterTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;

public class ContractPresenterTableProxy extends ContractPresenterTable implements ClickHandler {
	
	Column<Contract, Number> getbalanceColumn;
	TextColumn<Contract> getCustomerCellNumberColumn;
	TextColumn<Contract> getCustomerFullNameColumn;
	TextColumn<Contract> getCustomerIdColumn;
	TextColumn<Contract> getContractCountColumn;
	TextColumn<Contract> getQuotationCountColumn;
	TextColumn<Contract> getLeadCountColumn;
	TextColumn<Contract> getBranchColumn;
	TextColumn<Contract> getEmployeeColumn;
	TextColumn<Contract> getStatusColumn;
	TextColumn<Contract> getCreationDateColumn;
	TextColumn<Contract> getGroupColumn;
	TextColumn<Contract> getCategoryColumn;
	TextColumn<Contract> getTypeColumn;
	TextColumn<Contract> getCountColumn;
	
	TextColumn<Contract> getApproverName;
	TextColumn<Contract> getStartDate;
	TextColumn<Contract> getEndDate;
	/**
	 * nidhi
	 * 9-06-2018
	 * for salessourse
	 */
	TextColumn<Contract> getSalesSource;
	/** date 4.1.19 added by komal for followup date**/
	Column<Contract , String> communicationLogButton;
	PopupPanel communicationPanel;
	CommunicationLogPopUp communicationLogPopUp = new CommunicationLogPopUp();
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	int rowIndex ;
	Contract contract;
	final GWTCAlert alert = new GWTCAlert(); 
	final GWTCGlassPanel glassPanel = new GWTCGlassPanel();
	final GenricServiceAsync service = GWT.create(GenricService.class);
	static boolean communicationFlag =  false;
	
	public Object getVarRef(String varName) {
		if (varName.equals("getbalanceColumn"))
			return this.getbalanceColumn;
		if (varName.equals("getCustomerCellNumberColumn"))
			return this.getCustomerCellNumberColumn;
		if (varName.equals("getCustomerFullNameColumn"))
			return this.getCustomerFullNameColumn;
		if (varName.equals("getCustomerIdColumn"))
			return this.getCustomerIdColumn;
		if (varName.equals("getContractCountColumn"))
			return this.getContractCountColumn;
		if (varName.equals("getQuotationCountColumn"))
			return this.getQuotationCountColumn;
		if (varName.equals("getLeadCountColumn"))
			return this.getLeadCountColumn;
		if (varName.equals("getBranchColumn"))
			return this.getBranchColumn;
		if (varName.equals("getEmployeeColumn"))
			return this.getEmployeeColumn;
		if (varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if (varName.equals("getCreationDateColumn"))
			return this.getCreationDateColumn;
		if (varName.equals("getGroupColumn"))
			return this.getGroupColumn;
		if (varName.equals("getCategoryColumn"))
			return this.getCategoryColumn;
		if (varName.equals("getTypeColumn"))
			return this.getTypeColumn;
		if (varName.equals("getCountColumn"))
			return this.getCountColumn;
		/**
		 * nidhi
		 * 9-06-2018
		 */
		if(varName.equals("getSalesSource"))
			return this.getSalesSource;
		
		return null;
	}
	
	public ContractPresenterTableProxy() {
		super();
		/** 4.1.2019 added by komal for communication log popup **/
		communicationLogPopUp.getBtnOk().addClickHandler(this);
		communicationLogPopUp.getBtnCancel().addClickHandler(this);
		communicationLogPopUp.getBtnAdd().addClickHandler(this);
	}
	
	@Override
	public void createTable() {
	
		addColumngetCount();		
		 /** Date 12/10/2017 added by komal for communication log button **/	
		createColumnCommunicationLog();
		updateColumnCommunicationLog();
		
		addColumngetCreationDate();
		addColumngetStartDate();
		addColumngetEndDate();
	
		// addColumngetLeadCount();
		/**
		 * nidhi
		 * 9-06-2018
		 * for sales sources
		 */
		addColumngetSalessource();
		addColumngetCustomerId();
		addColumngetCustomerFullName();
		addColumngetCustomerCellNumber();
	
		addColumngetEmployee();
		addColumngetApprover();
	
		addColumngetCategory();
		addColumngetType();
		addColumngetGroup();
	
		// addColumngetbalance();
	
		addColumngetBranch();
		addColumngetQuotationCount();
		addColumngetStatus();
		/**
		 * nidhi
		 * 9-06-2018
		 * 
		 */
		addSortinggetSalesSource();
		System.out.println("Ajay-----------");
	}
	
	protected void addColumngetApprover() {
		getApproverName = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				return object.getApproverName();
			}
		};
		table.addColumn(getApproverName, "Approver");
		table.setColumnWidth(getApproverName, 120, Unit.PX);
		getApproverName.setSortable(true);
	}
	
	protected void addColumngetStartDate() {
		
		final Date sub7Days=new Date();
		System.out.println("Todays Date ::: "+sub7Days);
		CalendarUtil.addDaysToDate(sub7Days,-7);
		System.out.println("Date - 7 Days ::: "+sub7Days);
		sub7Days.setHours(23);
		sub7Days.setMinutes(59);
		sub7Days.setSeconds(59);
		System.out.println("Converted Date ::: "+sub7Days);
		
	
		
		getStartDate = new TextColumn<Contract>() {
			
			@Override
			public String getCellStyleNames(Context context, Contract object) {
				 if((object.getStartDate().before(sub7Days))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
					 return "red";
				 }
				 else if(object.getStatus().equals("Approved")){
					 return "green";
				 }
				 else{
					 return "black";
				 }
			}
			
			@Override
			public String getValue(Contract object) {
				if (object.getStartDate() != null)
					return AppUtility.parseDate(object.getStartDate());
				else
					return " ";
	
			}
		};
		table.addColumn(getStartDate, "Contract Start Date");
		table.setColumnWidth(getStartDate, 140, Unit.PX);
		getStartDate.setSortable(true);
	}
	
	protected void addColumngetEndDate() {
		final Date sub30Days=new Date();
		System.out.println("Todays Date ::: "+sub30Days);
		
		CalendarUtil.addDaysToDate(sub30Days,-30);
		System.out.println("Date - 30 Days ::: "+sub30Days);
		sub30Days.setHours(23);
		sub30Days.setMinutes(59);
		sub30Days.setSeconds(59);
		System.out.println("WITHOUT USE OF CALENDER ::: "+sub30Days);
		
	//	final Date add30Days=new Date();
	//	System.out.println("Todays Date ::: "+add30Days);
	//	
	//	CalendarUtil.addDaysToDate(add30Days,+29);
	//	System.out.println("Date + 30 Days ::: "+add30Days);
	//	add30Days.setHours(23);
	//	add30Days.setMinutes(59);
	//	add30Days.setSeconds(59);
	//	System.out.println("WITHOUT USE OF CALENDER ::: "+add30Days);
		
		
		final Date todaysDate=new Date();
		CalendarUtil.addDaysToDate(todaysDate,-1);
		todaysDate.setHours(23);
		todaysDate.setMinutes(59);
		todaysDate.setSeconds(59);
		System.out.println("Todays Date Converted ::: "+todaysDate);
		
		
		getEndDate = new TextColumn<Contract>() {
			
			@Override
			public String getCellStyleNames(Context context, Contract object) {
				 if((object.getEndDate().before(todaysDate)&&object.getEndDate().after(sub30Days))&&(object.getStatus().equals("Approved")&&(object.isRenewFlag()!=true))){
					 return "red";
				 }
				 else if((object.getStatus().equals("Approved")&&(object.isRenewFlag()==true))){
					 return "green";
				 }
				 else{
					 return "black";
				 }
			}
			@Override
			public String getValue(Contract object) {
				if (object.getEndDate() != null)
					return AppUtility.parseDate(object.getEndDate());
				else
					return " ";
	
			}
		};
		table.addColumn(getEndDate, "Contract End Date");
		table.setColumnWidth(getEndDate, 140, Unit.PX);
		getEndDate.setSortable(true);
	}
	/***
	 * nidhi
	 * 9-06-2018
	 * for sales source
	 * 
	 */
	protected void addColumngetSalessource() {
		getSalesSource = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				if (object.getContractPeriod()==null)
					return "N.A";
				else
					return object.getContractPeriod() + "";
			}
		};
		table.addColumn(getSalesSource, "Sales Source");
		table.setColumnWidth(getSalesSource, 100, Unit.PX);
		getSalesSource.setSortable(true);
	}
	
	////////////////////////////////////////////////Date IST Format //////////////////////////////////
	
	//public Date getDateWithTimeZone(String timeZone,Date date)
	//{
	//SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
	//isoFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
	//try {
	//	date = isoFormat.parse(isoFormat.format(date));
	//} catch (ParseException e) {
	//	e.printStackTrace();
	//}
	//return date;
	//}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	
	////////////////// Date Method To Convert Date To IST ////////////////////////////////////
	
	
	//public Date getDateWithTimeZone(String timeZone,Date date)
	//{
	//	 SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
	//	 isoFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
	//	    try {
	//		  date = isoFormat.parse(isoFormat.format(date));
	//		  } catch (ParseException e) {
	//			e.printStackTrace();
	//		}
	//	    
	//	    return date;
	//}
	//private Calendar getCalendarForDate(Date date) {
	//    Calendar calendar = GregorianCalendar.getInstance();
	//    calendar.setTime(date);
	//    return calendar;
	//}
	//
	//private Calendar setTimeToBeginningOfDay(Calendar calendar) {
	//    calendar.set(Calendar.HOUR_OF_DAY, 0);
	//    calendar.set(Calendar.MINUTE, 0);
	//    calendar.set(Calendar.SECOND, 0);
	//    calendar.set(Calendar.MILLISECOND, 0);
	//    return calendar;
	//}
	//
	//private Calendar setTimeToEndofDay(Calendar calendar) {
	//    calendar.set(Calendar.HOUR_OF_DAY, 23);
	//    calendar.set(Calendar.MINUTE, 59);
	//    calendar.set(Calendar.SECOND, 59);
	//    calendar.set(Calendar.MILLISECOND, 999);
	//    return calendar;
	//}
	
	
	//////////////////////////////////////////////////////////////////////////////////////////
	
	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<Contract>() {
			@Override
			public Object getKey(Contract item) {
				if (item == null) {
					return null;
				} else
					return item.getId();
			}
		};
	}
	
	@Override
	public void setEnable(boolean state) {
	}
	
	@Override
	public void applyStyle() {
	}
	
	public void addColumnSorting() {
		addSortinggetCount();
		addSortinggetBranch();
		addSortinggetEmployee();
		addSortinggetStatus();
		addSortinggetLeadCount();
		addSortinggetGroup();
		addSortinggetQuotationCount();
		addSortinggetCategory();
		addSortinggetCustomerId();
		addSortinggetType();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortinggetContractCount();
		addSortinggetCreationDate();
		addSortinggetbalance();
	
		addSortinggetApproverName();
		addSortinggetStartDate();
		addSortinggetEndDate();
	
	}
	
	protected void addSortinggetApproverName() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getApproverName, new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getApproverName() != null
							&& e2.getApproverName() != null) {
						return e1.getApproverName().compareTo(
								e2.getApproverName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetStartDate() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getStartDate, new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStartDate() != null && e2.getStartDate() != null) {
						return e1.getStartDate().compareTo(e2.getStartDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetEndDate() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getEndDate, new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEndDate() != null && e2.getEndDate() != null) {
						return e1.getEndDate().compareTo(e2.getEndDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	@Override
	public void addFieldUpdater() {
	}
	
	protected void addSortinggetCount() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetCount() {
		getCountColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				if (object.getCount() == -1)
					return "N.A";
				else
					return object.getCount() + "";
			}
		};
		table.addColumn(getCountColumn, "ID");
	
		table.setColumnWidth(getCountColumn, 100, Unit.PX);
		getCountColumn.setSortable(true);
	}
	
	protected void addColumngetCreationDate() {
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date = new DatePickerCell(fmt);
		
	//	final Date add7Days=new Date();
	//	CalendarUtil.addDaysToDate(add7Days,7);
	//	System.out.println("Date + 7 Days ::: "+add7Days);
	//	
	//	final Date sub7Days=new Date();
	//	CalendarUtil.addDaysToDate(sub7Days,-7);
	//	System.out.println("Date + 7 Days ::: "+sub7Days);
		
		getCreationDateColumn = new TextColumn<Contract>() {
	//		@Override
	//		public String getCellStyleNames(Context context, Contract object) {
	//			 if((object.getCreationDate().before(add7Days)&&object.getCreationDate().after(new Date()))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
	//				 return "red";
	//			 }
	//			 if((object.getCreationDate().before(sub7Days))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
	//				 return "red";
	//			 }
	//			 else{
	//				 return "black";
	//			 }
	//		}
			
			@Override
			public String getValue(Contract object) {
				if (object.getCreationDate() != null)
					return AppUtility.parseDate(object.getCreationDate());
				else
					return "N.A";
	
			}
		};
		table.addColumn(getCreationDateColumn, "Creation Date");
		table.setColumnWidth(getCreationDateColumn, 100, Unit.PX);
		getCreationDateColumn.setSortable(true);
	}
	
	protected void addSortinggetCreationDate() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getCreationDateColumn,
				new Comparator<Contract>() {
					@Override
					public int compare(Contract e1, Contract e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCreationDate() != null
									&& e2.getCreationDate() != null) {
								return e1.getCreationDate().compareTo(
										e2.getCreationDate());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetBranch() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null && e2.getBranch() != null) {
						return e1.getBranch().compareTo(e2.getBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetBranch() {
		getBranchColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				return object.getBranch();
			}
		};
		table.addColumn(getBranchColumn, "Branch");
		table.setColumnWidth(getBranchColumn, 100, Unit.PX);
		getBranchColumn.setSortable(true);
	}
	
	protected void addSortinggetEmployee() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getEmployeeColumn, new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmployee() != null && e2.getEmployee() != null) {
						return e1.getEmployee().compareTo(e2.getEmployee());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetEmployee() {
		getEmployeeColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				return object.getEmployee() + "";
			}
		};
		table.addColumn(getEmployeeColumn, "Sales Person");
		getEmployeeColumn.setSortable(true);
		table.setColumnWidth(getEmployeeColumn, 100, Unit.PX);
	
	}
	
	protected void addSortinggetStatus() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null && e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetStatus() {
		getStatusColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				return object.getStatus() + "";
			}
		};
		table.addColumn(getStatusColumn, "Status");
		getStatusColumn.setSortable(true);
		table.setColumnWidth(getStatusColumn, 80, Unit.PX);
	
	}
	
	protected void addSortinggetSalesSource() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getSalesSource,
				new Comparator<Contract>() {
					@Override
					public int compare(Contract e1, Contract e2) {
						if (e1 != null && e2 != null) {
							if (e1.getContractPeriod() != null && e2.getContractPeriod() != null) {
								return e1.getContractPeriod().compareTo(e2.getContractPeriod());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetLeadCount() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getLeadCountColumn,
				new Comparator<Contract>() {
					@Override
					public int compare(Contract e1, Contract e2) {
						if (e1 != null && e2 != null) {
							if (e1.getLeadCount() == e2.getLeadCount()) {
								return 0;
							}
							if (e1.getLeadCount() > e2.getLeadCount()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetLeadCount() {
		getLeadCountColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				if (object.getLeadCount() == -1)
					return "N.A";
				else
					return object.getLeadCount() + "";
			}
		};
		table.addColumn(getLeadCountColumn, "Lead Id");
		table.setColumnWidth(getLeadCountColumn, 80, Unit.PX);
	
		getLeadCountColumn.setSortable(true);
	}
	
	protected void addSortinggetGroup() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getGroupColumn, new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getGroup() != null && e2.getGroup() != null) {
						return e1.getGroup().compareTo(e2.getGroup());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetGroup() {
		getGroupColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				return object.getGroup() + "";
			}
		};
		table.addColumn(getGroupColumn, "Group");
		table.setColumnWidth(getGroupColumn, 100, Unit.PX);
		getGroupColumn.setSortable(true);
	}
	
	protected void addSortinggetQuotationCount() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getQuotationCountColumn,
				new Comparator<Contract>() {
					@Override
					public int compare(Contract e1, Contract e2) {
						if (e1 != null && e2 != null) {
							if (e1.getQuotationCount() == e2
									.getQuotationCount()) {
								return 0;
							}
							if (e1.getQuotationCount() > e2.getQuotationCount()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	
	}
	
	protected void addColumngetQuotationCount() {
		getQuotationCountColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				if (object.getQuotationCount() == -1)
					return "N.A";
				else
					return object.getQuotationCount() + "";
			}
		};
		table.addColumn(getQuotationCountColumn, "Quotation Id");
		table.setColumnWidth(getQuotationCountColumn, 90, Unit.PX);
		getQuotationCountColumn.setSortable(true);
	}
	
	protected void addSortinggetCategory() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getCategoryColumn, new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCategory() != null && e2.getCategory() != null) {
						return e1.getCategory().compareTo(e2.getCategory());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetCategory() {
		getCategoryColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				return object.getCategory() + "";
			}
		};
		table.addColumn(getCategoryColumn, "Category");
		table.setColumnWidth(getCategoryColumn, "120px");
		getCategoryColumn.setSortable(true);
	}
	
	protected void addSortinggetCustomerId() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getCustomerIdColumn,
				new Comparator<Contract>() {
					@Override
					public int compare(Contract e1, Contract e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCustomerId() == e2.getCustomerId()) {
								return 0;
							}
							if (e1.getCustomerId() > e2.getCustomerId()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetCustomerId() {
		getCustomerIdColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				if (object.getCustomerId() == -1)
					return "N.A";
				else
					return object.getCustomerId() + "";
			}
		};
		table.addColumn(getCustomerIdColumn, "Customer Id");
		table.setColumnWidth(getCustomerIdColumn, 100, Unit.PX);
		getCustomerIdColumn.setSortable(true);
	}
	
	protected void addSortinggetType() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getTypeColumn, new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getType() != null && e2.getType() != null) {
						return e1.getType().compareTo(e2.getType());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetType() {
		getTypeColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				return object.getType() + "";
			}
		};
		table.addColumn(getTypeColumn, "Type");
		table.setColumnWidth(getTypeColumn, 100, Unit.PX);
		getTypeColumn.setSortable(true);
	}
	
	protected void addSortinggetCustomerFullName() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getCustomerFullNameColumn,
				new Comparator<Contract>() {
					@Override
					public int compare(Contract e1, Contract e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCustomerFullName() != null
									&& e2.getCustomerFullName() != null) {
								return e1.getCustomerFullName().compareTo(
										e2.getCustomerFullName());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	
	}
	
	protected void addColumngetCustomerFullName() {
		getCustomerFullNameColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				return object.getCustomerFullName() + "";
			}
		};
		table.addColumn(getCustomerFullNameColumn, "Customer Name");
		table.setColumnWidth(getCustomerFullNameColumn, 130, Unit.PX);
		getCustomerFullNameColumn.setSortable(true);
	}
	
	protected void addSortinggetCustomerCellNumber() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getCustomerCellNumberColumn,
				new Comparator<Contract>() {
					@Override
					public int compare(Contract e1, Contract e2) {
						if (e1 != null && e2 != null) {
							if (e1.getCustomerCellNumber() == e2
									.getCustomerCellNumber()) {
								return 0;
							}
							if (e1.getCustomerCellNumber() > e2
									.getCustomerCellNumber()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetCustomerCellNumber() {
		getCustomerCellNumberColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				return object.getCustomerCellNumber() + "";
			}
		};
		table.addColumn(getCustomerCellNumberColumn, "Customer Cell");
		table.setColumnWidth(getCustomerCellNumberColumn, 130, Unit.PX);
		getCustomerCellNumberColumn.setSortable(true);
	}
	
	protected void addSortinggetContractCount() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getContractCountColumn,
				new Comparator<Contract>() {
					@Override
					public int compare(Contract e1, Contract e2) {
						if (e1 != null && e2 != null) {
							if (e1.getContractCount() == e2.getContractCount()) {
								return 0;
							}
							if (e1.getContractCount() > e2.getContractCount()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetContractCount() {
		getContractCountColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				if (object.getContractCount() == -1)
					return "N.A";
				else
					return object.getContractCount() + "";
			}
		};
		table.addColumn(getContractCountColumn, "Contract Id");
		table.setColumnWidth(getContractCountColumn, 150, Unit.PX);
	
		getContractCountColumn.setSortable(true);
	}
	
	protected void addSortinggetbalance() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getbalanceColumn, new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getbalance() == e2.getbalance()) {
						return 0;
					}
					if (e1.getbalance() > e2.getbalance()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetbalance() {
		NumberCell editCell = new NumberCell();
		getbalanceColumn = new Column<Contract, Number>(editCell) {
			@Override
			public Number getValue(Contract object) {
				if (object.getbalance() == 0) {
					return 0;
				} else {
					return object.getbalance();
				}
			}
		};
		table.addColumn(getbalanceColumn, "Balance");
		table.setColumnWidth(getbalanceColumn, 110, Unit.PX);
		getbalanceColumn.setSortable(true);
	
	}
	
	protected void addFieldUpdatergetbalance() {
		getbalanceColumn.setFieldUpdater(new FieldUpdater<Contract, Number>() {
			@Override
			public void update(int index, Contract object, Number value) {
				Double val1 = (Double) value;
				object.setbalance(val1);
				table.redrawRow(index);
			}
		});
	}
	
/** date 4.1.2019 added by komal to add communication log button **/
protected void createColumnCommunicationLog()
{
	ButtonCell btnCell = new ButtonCell();
	communicationLogButton = new Column<Contract, String>(btnCell) {
		
		@Override
		public String getValue(Contract object) {
			// TODO Auto-generated method stub
			return "Communication Log";
		}
	};                 
	
	table.addColumn(communicationLogButton, "");
	table.setColumnWidth(communicationLogButton,130,Unit.PX);
	
}

/** date 4.1.2019 added by komal to add communication log button **/
protected void updateColumnCommunicationLog()
{
	// TODO Auto-generated method stub
	communicationLogButton.setFieldUpdater(new FieldUpdater<Contract, String>() {
		
				@Override
				public void update(int index, final Contract object, String value) {
					
					
					rowIndex=index;
					communicationFlag = true;
					contract= object;
					getContractDetails(object);
				}
			});
	
}
private void getContractDetails(Contract contract)
{
	glassPanel.show();
	MyQuerry querry = AppUtility.communicationLogQuerry(contract.getCount(), contract.getCompanyId(),AppConstants.SERVICEMODULE,AppConstants.CONTRACT);
	
	service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
		
		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			
			ArrayList<InteractionType> list = new ArrayList<InteractionType>();
			
			for(SuperModel model : result){
				
				InteractionType interactionType = (InteractionType) model;
				list.add(interactionType);
				
			}
			/**
			 * Date : 08-11-2017 BY Komal
			 */
			Comparator<InteractionType> comp=new Comparator<InteractionType>() {
				@Override
				public int compare(InteractionType e1, InteractionType e2) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return -1;
					} else {
						return 1;
					}
				}
			};
			Collections.sort(list,comp);
            
			glassPanel.hide();
			communicationLogPopUp.getRemark().setValue("");
			communicationLogPopUp.getDueDate().setValue(null);
			communicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
			communicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
			communicationPanel = new PopupPanel(true);
			communicationPanel.add(communicationLogPopUp);
			
			communicationPanel.center();
			communicationPanel.show();
				
		}
		
		@Override
		public void onFailure(Throwable caught) {
			glassPanel.hide();
		}
	});
	
}

@Override
public void onClick(ClickEvent event) {
	if(event.getSource() == communicationLogPopUp.getBtnOk()){
	
		glassPanel.show();
	    List<InteractionType> list = communicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
	   
	    ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
	    interactionlist.addAll(list);
	    
	    boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);
	    
	    if(checkNewInteraction==false){
	    	alert.alert("Please add new interaction details");
	    	glassPanel.hide();
	    }	
	    else{	
	    	
	    	 communicationService.saveCommunicationLog(interactionlist, new AsyncCallback<Void>() {
	    		 
					@Override
					public void onFailure(Throwable caught) {
						alert.alert("Unexpected Error");
						glassPanel.hide();
						LoginPresenter.communicationLogPanel.hide();
					}

					@Override
					public void onSuccess(Void result) {
						alert.alert("Data Save Successfully");
						communicationFlag = false;
						table.redraw();
						glassPanel.hide();
						communicationPanel.hide();
					} 
				});
	    }
	   
	}
	if(event.getSource() == communicationLogPopUp.getBtnCancel()){
		communicationFlag = false;
		communicationPanel.hide();
	}
	if(event.getSource() == communicationLogPopUp.getBtnAdd()){
			glassPanel.show();
			String remark = communicationLogPopUp.getRemark().getValue();
			Date dueDate = communicationLogPopUp.getDueDate().getValue();
			String interactiongGroup =null;
			if(communicationLogPopUp.getOblinteractionGroup().getSelectedIndex()!=0)
				interactiongGroup=communicationLogPopUp.getOblinteractionGroup().getValue(communicationLogPopUp.getOblinteractionGroup().getSelectedIndex());
			boolean validationFlag = AppUtility.validateCommunicationlog(remark,dueDate);
			
			List<InteractionType> list = communicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
			
			Comparator<InteractionType> comp=new Comparator<InteractionType>() {
				@Override
				public int compare(InteractionType e1, InteractionType e2) {
//					if (e1.getCount() == e2.getCount()) {
//						return 0;
//					}
//					if (e1.getCount() > e2.getCount()) {
//						return -1;
//					} else {
//						return 1;
//					}
					/**
					 * Updated By: Viraj
					 * Date: 09-02-2018
					 * Description: To sort communication log
					 */
					Integer ee1=e1.getCount();
					Integer ee2=e2.getCount();
					return ee2.compareTo(ee1);
					/** Ends **/
				}
			};
			Collections.sort(list,comp);
            
			
			if(validationFlag){
				InteractionType communicationLog =  AppUtility.getCommunicationLog(AppConstants.SERVICEMODULE,AppConstants.CONTRACT,contract.getCount(),contract.getEmployee(),remark,dueDate,contract.getCinfo(),null,interactiongGroup, contract.getBranch(),"");
				list.add(communicationLog);
				communicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);;
				communicationLogPopUp.getRemark().setValue("");
				communicationLogPopUp.getDueDate().setValue(null);
				communicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
			}
			glassPanel.hide();
	}
		
}
	
}
