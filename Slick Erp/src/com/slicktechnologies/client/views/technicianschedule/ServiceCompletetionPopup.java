package com.slicktechnologies.client.views.technicianschedule;

import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;

public class ServiceCompletetionPopup {

	Window	 winModal;
	DateItem dbserviceCompletionDate;
	IButton btnOk;
	IButton btnCancel;
	ListBox servicetime;
	ListBox p_servicemin;
	ListBox p_ampm;
	IntegerBox ibserviceDuration;
	ListBox lbRating;
	TextBox tbRemark;
	DoubleBox dbserviceQuantity;
	ObjectListBox<Config> olbUnitOfMeasurement;
	
	private ListBox to_servicehours;
	private ListBox to_servicemin;
	private ListBox to_ampm;
	
	
	// material return qty popup
	
	Window materialReturnpPopup;
	DoubleBox dbmaterialReturnQty;
	IButton btnReturnQtySave;
	IButton btnReturnQtyCancel;

	
	public ServiceCompletetionPopup(){
		
		// service completion popup
		winModal = new Window();
//		winModal.setAutoSize(true);
		winModal.setWidth(570);
		winModal.setHeight(350);
        winModal.setTitle("Service Mark Complete");
        winModal.setShowMinimizeButton(false);
        winModal.setIsModal(true);
        winModal.setShowModalMask(true);
        winModal.centerInPage();
        winModal.setMargin(10);
        
        createServiceCompletionPopUp();
        
        
	}
	
	// RETURN MATERIAL POPUP
	public ServiceCompletetionPopup(boolean flag){
		createMaterialReturnPopUp();
	}

	private void createMaterialReturnPopUp() {
		
		materialReturnpPopup = new Window();
		materialReturnpPopup.setAutoSize(true);
		materialReturnpPopup.setTitle("Schedule");
		materialReturnpPopup.setShowMinimizeButton(false);
		materialReturnpPopup.setIsModal(true);
		materialReturnpPopup.setShowModalMask(true);
		materialReturnpPopup.centerInPage();
		materialReturnpPopup.setMargin(10);
        
		materialReturnpPopup.setAlign(Alignment.CENTER);

		
		Label lblReturnQty = new Label("Return Quantity :");
		lblReturnQty.setWidth("130px");
		lblReturnQty.setStyleName("bold");

		final HLayout lablesLaybout = new HLayout ();
        lablesLaybout.setPadding(10);
        lablesLaybout.setAutoHeight();
        lablesLaybout.setAutoWidth();
        lablesLaybout.setMembersMargin(10);
        lablesLaybout.setAlign(Alignment.CENTER);
       
        lablesLaybout.addMember(lblReturnQty);
		
		final HLayout hlayoutReturnQty = new HLayout ();
		hlayoutReturnQty.setPadding(10);
		hlayoutReturnQty.setAutoHeight();
		hlayoutReturnQty.setAutoWidth();
		hlayoutReturnQty.setMembersMargin(10);
		hlayoutReturnQty.setAlign(Alignment.CENTER);
		dbmaterialReturnQty = new DoubleBox();
		hlayoutReturnQty.addMember(dbmaterialReturnQty);
        
		
		final HLayout hlayoutbuttons = new HLayout ();
		hlayoutbuttons.setPadding(10);
		hlayoutbuttons.setAutoHeight();
		hlayoutbuttons.setAutoWidth();
		hlayoutbuttons.setMembersMargin(10);
		
		btnReturnQtySave = new IButton();
		btnReturnQtySave.setTitle("Save");
		btnReturnQtyCancel = new IButton();
		btnReturnQtyCancel.setTitle("Cancel");
        
        Label blank = new Label();
        
        hlayoutbuttons.addMember(btnReturnQtySave);
//        hlayoutbuttons.addMember(blank);
        hlayoutbuttons.addMember(btnReturnQtyCancel);
	        
        materialReturnpPopup.addItem(lablesLaybout);
        materialReturnpPopup.addItem(hlayoutReturnQty);
        materialReturnpPopup.addItem(hlayoutbuttons);
	}

	private void createServiceCompletionPopUp() {

		final HLayout hlServiceDuration = new HLayout ();
		hlServiceDuration.setPadding(10);
		hlServiceDuration.setAutoHeight();
		hlServiceDuration.setAutoWidth();
		hlServiceDuration.setMembersMargin(10);
		
		
		 Label lblServiceDuration =null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
			lblServiceDuration= new Label("* Service Duration (In Minutes)");
		}
		else{
			lblServiceDuration = new Label("Service Duration (In Minutes)");
		}
		
		 lblServiceDuration.setSize("100px", "15px");
		 lblServiceDuration.setStyleName("bold");
		 
		 ibserviceDuration = new IntegerBox();
		 ibserviceDuration.setSize("115px", "13px");
	
		 Label lblCustomerFeedback = new Label("Customer Feedback");
		 lblCustomerFeedback.setSize("100px", "22px");
		 lblCustomerFeedback.setStyleName("bold"); 
		 
	 	lbRating = new ListBox();
		lbRating.addItem("--SELECT--");
		lbRating.addItem("Excellent");
		lbRating.addItem("Very Good");
		lbRating.addItem("Good");
		lbRating.addItem("Satisfactory");
		lbRating.addItem("Poor");
		 
		 hlServiceDuration.addMember(lblServiceDuration);
	     hlServiceDuration.addMember(ibserviceDuration);
	     hlServiceDuration.addMember(lblCustomerFeedback);
	     hlServiceDuration.addMember(lbRating);
	     
	     winModal.addItem(hlServiceDuration); 
	        
	     
	    final HLayout hlserviceFrmTimeToTime = new HLayout ();
        hlserviceFrmTimeToTime.setPadding(10);
        hlserviceFrmTimeToTime.setAutoHeight();
        hlserviceFrmTimeToTime.setAutoWidth();
        hlserviceFrmTimeToTime.setMembersMargin(10);
	        
        Label lblServiceTime = new Label("From Time (HH:MM)");
	    lblServiceTime.setSize("70px", "20px"); //95
	    lblServiceTime.setStyleName("bold");
	    
        servicetime = new ListBox();
        servicetime.setSize("45px", "20px");
        servicetime.insertItem("--", 0);
        servicetime.insertItem("01", 1);
        servicetime.insertItem("02", 2);
        servicetime.insertItem("03", 3);
        servicetime.insertItem("04", 4);
		servicetime.insertItem("05", 5);
		servicetime.insertItem("06", 6);
		servicetime.insertItem("07", 7);
		servicetime.insertItem("08", 8);
		servicetime.insertItem("09", 9);
		servicetime.insertItem("10", 10);
		servicetime.insertItem("11", 11);
		servicetime.insertItem("12", 12);
        
		
		p_servicemin=new ListBox();
		p_servicemin.setSize("40px", "20px");
		p_servicemin.insertItem("--", 0);
		p_servicemin.insertItem("00", 1);
		p_servicemin.insertItem("15", 2);
		p_servicemin.insertItem("30", 3);
		p_servicemin.insertItem("45", 4);
		
		p_ampm = new ListBox();
		p_ampm.setSize("45px", "20px");
		p_ampm.insertItem("--", 0);
		p_ampm.insertItem("AM", 1);
		p_ampm.insertItem("PM", 2);
        
       
        
        Label lblToTime = new Label("To Time (HH:MM)");
        lblToTime.setSize("85px", "20px"); //100
        lblToTime.setStyleName("bold");
	    
    	to_servicehours=new ListBox();
		to_servicehours.setSize("40px", "20px");
		to_servicehours.insertItem("--", 0);
		to_servicehours.insertItem("00", 1);
		to_servicehours.insertItem("01", 2);
		to_servicehours.insertItem("02", 3);
		to_servicehours.insertItem("03", 4);
		to_servicehours.insertItem("04", 5);
		to_servicehours.insertItem("05", 6);
		to_servicehours.insertItem("06", 7);
		to_servicehours.insertItem("07", 8);
		to_servicehours.insertItem("08", 9);
		to_servicehours.insertItem("09", 10);
		to_servicehours.insertItem("10", 11);
		to_servicehours.insertItem("11", 12);
		
		
		to_servicemin=new ListBox();
		to_servicemin.setSize("40px", "20px");
		to_servicemin.insertItem("--", 0);
		to_servicemin.insertItem("00", 1);
		to_servicemin.insertItem("15", 2);
		to_servicemin.insertItem("30", 3);
		to_servicemin.insertItem("45", 4);
		
		to_ampm = new ListBox();
		to_ampm.setSize("45px", "20px");
		to_ampm.insertItem("--", 0);
		to_ampm.insertItem("AM", 1);
		to_ampm.insertItem("PM", 2);
		
		
		hlserviceFrmTimeToTime.addMember(lblServiceTime);
		hlserviceFrmTimeToTime.addMember(servicetime);
		hlserviceFrmTimeToTime.addMember(p_servicemin);
		hlserviceFrmTimeToTime.addMember(p_ampm);
		
		hlserviceFrmTimeToTime.addMember(lblToTime);
		hlserviceFrmTimeToTime.addMember(to_servicehours);
		hlserviceFrmTimeToTime.addMember(to_servicemin);
		hlserviceFrmTimeToTime.addMember(to_ampm);

		winModal.addItem(hlserviceFrmTimeToTime); 
		
		
		
		final HLayout hlcompletionDateRemark = new HLayout ();
		hlcompletionDateRemark.setPadding(10);
		hlcompletionDateRemark.setAutoHeight();
		hlcompletionDateRemark.setAutoWidth();
		hlcompletionDateRemark.setMembersMargin(10);
	        
		Label lblServicecompletionDate = new Label("Service Completion Date");
		lblServicecompletionDate.setSize("90px", "20px");
		lblServicecompletionDate.setStyleName("bold");
        
		DynamicForm dynamicform = new DynamicForm();
	    dbserviceCompletionDate = new DateItem();
	    dbserviceCompletionDate.setWidth(122);
	    dbserviceCompletionDate.setUseTextField(true);
	    dbserviceCompletionDate.setTitle("");
	      
	    dynamicform.setFields(dbserviceCompletionDate);
	    
	    Label lblRemark = new Label("Remark");
	    lblRemark.setSize("100px", "20px");
	    lblRemark.setStyleName("bold");
		
	    tbRemark = new TextBox();
	    tbRemark.setSize("100px", "20px");
	    
	    hlcompletionDateRemark.addMember(lblServicecompletionDate);
	    hlcompletionDateRemark.addMember(dynamicform);
	    hlcompletionDateRemark.addMember(lblRemark);
	    hlcompletionDateRemark.addMember(tbRemark);

	    winModal.addItem(hlcompletionDateRemark);
	    
        
    	final HLayout hlserviceQtyUOM = new HLayout ();
    	hlserviceQtyUOM.setPadding(10);
    	hlserviceQtyUOM.setAutoHeight();
    	hlserviceQtyUOM.setAutoWidth();
    	hlserviceQtyUOM.setMembersMargin(10);
    	
    	Label lblQty = new Label("Quantity");
    	lblQty.setSize("100px", "20px");
    	lblQty.setStyleName("bold");
    	
    	dbserviceQuantity = new DoubleBox();
    	dbserviceQuantity.setSize("115px", "15px");
    	
    	Label lblUOM = new Label("Unit of Measurement");
    	lblUOM.setSize("100px", "20px");
    	lblUOM.setStyleName("bold");
    	
    	olbUnitOfMeasurement=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbUnitOfMeasurement, Screen.UNITOFMEASUREMENT);
		
		hlserviceQtyUOM.addMember(lblQty);
		hlserviceQtyUOM.addMember(dbserviceQuantity);
		hlserviceQtyUOM.addMember(lblUOM);
		hlserviceQtyUOM.addMember(olbUnitOfMeasurement);

        winModal.addItem(hlserviceQtyUOM);

        
        final HLayout hlbtnOkCancel = new HLayout ();
	    hlbtnOkCancel.setPadding(10);
	    hlbtnOkCancel.setAutoHeight();
	    hlbtnOkCancel.setAutoWidth();
	    hlbtnOkCancel.setMembersMargin(25);
	    
        btnOk = new IButton();
        btnOk.setTitle("OK");
        btnCancel = new IButton();
        btnCancel.setTitle("Cancel");
        
        Label lblbtnBlank = new Label();
        
        hlbtnOkCancel.addMember(lblbtnBlank);
        hlbtnOkCancel.addMember(btnOk);
        hlbtnOkCancel.addMember(btnCancel);
        
        winModal.addItem(hlbtnOkCancel);

	}
	
	public void showPopUp(){
		winModal.show();
	}
	
	public void hidePopUp(){
		winModal.hide();
	}


	public IButton getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(IButton btnOk) {
		this.btnOk = btnOk;
	}

	public IButton getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(IButton btnCancel) {
		this.btnCancel = btnCancel;
	}
	
	public DateItem getDbserviceCompletionDate() {
		return dbserviceCompletionDate;
	}

	public void setDbserviceCompletionDate(DateItem dbserviceCompletionDate) {
		this.dbserviceCompletionDate = dbserviceCompletionDate;
	}

	public ListBox getServicetime() {
		return servicetime;
	}

	public void setServicetime(ListBox servicetime) {
		this.servicetime = servicetime;
	}

	public ListBox getP_servicemin() {
		return p_servicemin;
	}

	public void setP_servicemin(ListBox p_servicemin) {
		this.p_servicemin = p_servicemin;
	}

	public ListBox getP_ampm() {
		return p_ampm;
	}

	public void setP_ampm(ListBox p_ampm) {
		this.p_ampm = p_ampm;
	}

	

	public IntegerBox getIbserviceDuration() {
		return ibserviceDuration;
	}

	public void setIbserviceDuration(IntegerBox ibserviceDuration) {
		this.ibserviceDuration = ibserviceDuration;
	}

	public ListBox getLbRating() {
		return lbRating;
	}

	public void setLbRating(ListBox lbRating) {
		this.lbRating = lbRating;
	}

	public TextBox getTbRemark() {
		return tbRemark;
	}

	public void setTbRemark(TextBox tbRemark) {
		this.tbRemark = tbRemark;
	}

	public ListBox getTo_servicehours() {
		return to_servicehours;
	}

	public void setTo_servicehours(ListBox to_servicehours) {
		this.to_servicehours = to_servicehours;
	}

	public ListBox getTo_servicemin() {
		return to_servicemin;
	}

	public void setTo_servicemin(ListBox to_servicemin) {
		this.to_servicemin = to_servicemin;
	}

	public ListBox getTo_ampm() {
		return to_ampm;
	}

	public void setTo_ampm(ListBox to_ampm) {
		this.to_ampm = to_ampm;
	}

	
	public DoubleBox getDbserviceQuantity() {
		return dbserviceQuantity;
	}

	public void setDbserviceQuantity(DoubleBox dbserviceQuantity) {
		this.dbserviceQuantity = dbserviceQuantity;
	}

	public ObjectListBox<Config> getOlbUnitOfMeasurement() {
		return olbUnitOfMeasurement;
	}

	public void setOlbUnitOfMeasurement(ObjectListBox<Config> olbUnitOfMeasurement) {
		this.olbUnitOfMeasurement = olbUnitOfMeasurement;
	}

	/**************** Return Qty popup getter setter  ********************************/
	
	public void showReturnQtyPopUp(){
		materialReturnpPopup.show();
	}
	
	public void hideReturnQtyPopUp(){
		materialReturnpPopup.hide();
	}
	
	
	
	

	public IButton getBtnReturnQtySave() {
		return btnReturnQtySave;
	}

	public void setBtnReturnQtySave(IButton btnReturnQtySave) {
		this.btnReturnQtySave = btnReturnQtySave;
	}

	public IButton getBtnReturnQtyCancel() {
		return btnReturnQtyCancel;
	}

	public void setBtnReturnQtyCancel(IButton btnReturnQtyCancel) {
		this.btnReturnQtyCancel = btnReturnQtyCancel;
	}

	public DoubleBox getDbmaterialReturnQty() {
		return dbmaterialReturnQty;
	}

	public void setDbmaterialReturnQty(DoubleBox dbmaterialReturnQty) {
		this.dbmaterialReturnQty = dbmaterialReturnQty;
	}
	
	
	
	
	
	
	
}
