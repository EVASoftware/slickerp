package com.slicktechnologies.client.views.technicianschedule;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.eclipse.jetty.security.UserAuthentication;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.technicianscheduling.TechnicianMaterialInfo;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class TechnicianPlanPopUp implements ClickHandler, ChangeHandler, com.smartgwt.client.widgets.events.ClickHandler {

	
	final GenricServiceAsync genasync=GWT.create(GenricService.class);

	
	ObjectListBox<TeamManagement> olbTeam;
	ObjectListBox<Employee> olbEmployee;
	
	TechnicianTable technicianTable;
	Button btnTechnicianAdd;
	Button btnToolAdd;
	Button btnproductAdd;
	TechnicianToolTable toolTable;
	
	MaterialProductTable materialProductTable;
	
	ObjectListBox<ItemProduct> olbitemProduct;
    ObjectListBox<CompanyAsset> olbTool;
	ObjectListBox<WareHouse> olbWarehouseName;
	ObjectListBox<StorageLocation> olbStorageLocation;
	ObjectListBox<Storagebin> olbstorageBin;
	DoubleBox dbQuantity;
	
	

	IButton btnOk;
	DateItem dbserviceDate;
	ListBox servicetime;
	ListBox p_servicemin;
	ListBox p_ampm;
	ObjectListBox<Config> olbSpecificReason;
	
	
	DynamicForm dynamicform;
	
	Service model=null;
	long companyId = UserConfiguration.getCompanyId();
	
	final GeneralServiceAsync technicianPlanInfoSync = GWT.create(GeneralService.class);
	
	ListGridRecord selectedRecord=null;
	
	Button btnReschedulePopUp;				//Updated By:Viraj Date: 03-01-2019 Description: Pop reschedule name changed due to duplicate names
	TextBox tbRecheduleReason;
	
	IButton btnComplete;
	
	ObjectListBox<Invoice> olbInvoiceId;
	ObjectListBox<Invoice> olbInvoiceDate;
	TextArea tbOther;
	
	/**
	 * Updated By: Viraj 
	 * Date: 26-12-2018
	 * Description: Adding Tabular Structure to popup
	 */
	TabPanel tabPanel = new TabPanel();
	FlowPanel flowPanel1;
	FlowPanel flowPanel2;
	FlowPanel flowPanel3;
	FlowPanel flowPanel4;
	VerticalPanel verticalPanel;
	VerticalPanel verticalPanel1;
	VerticalPanel verticalPanel2;
	VerticalPanel verticalPanel3;
	int tabId;
	HorizontalPanel h1,h2,h3;
	HorizontalPanel h4,h5,h6;
	HorizontalPanel h7,h8,h9;			// Date: 04-01-2019
	VerticalPanel v1,v2,v3;				// Date: 04-01-2019
	/** Ends **/
	
	Window winModal ;
	
	public TechnicianPlanPopUp(){
		
		winModal = new Window();
		winModal.setWidth("70%");
        winModal.setHeight("75%"); //700
        winModal.setTitle("Schedule Plan");					//updated by Viraj Date:25-12-2018
        winModal.setShowMinimizeButton(false);
        winModal.setIsModal(true);
        winModal.setShowModalMask(true);
        winModal.centerInPage();
        winModal.setMargin(4);
        
        CreatePopup();
        /**
         * Updated By: Viraj 
         * Date: 31-12-2018
         * Description: For selection of tabs
         */
        tabPanel.addStyleName("gwt-TabPanelHeight");
        tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
       		@Override
       		public void onSelection(SelectionEvent<Integer> event) {
       			tabId = event.getSelectedItem();
       			System.out.println("TAB ID "+tabId); 
       			 	
       			 if(tabId==0) { 
       			 } else if(tabId==1) {
       				technicianTable.getTable().redraw();
       			 } else if(tabId==2) {
       				materialProductTable.getTable().redraw();
       			 } else if(tabId==3) {
       				toolTable.getTable().redraw();
       			 } else if(tabId==4) {   				 
       			 }
       		}
       	});
        /** Ends **/
	}

	private void CreatePopup() {

		
		/********* Technician Info strart here ********************/
		
		btnTechnicianAdd = new Button("Add");
	    btnTechnicianAdd.addClickHandler(this);
	    
	    Label lblServiceDate = new Label("Service Date");
	    lblServiceDate.setSize("105px", "30px");
	    lblServiceDate.setStyleName("bold");
	    
	    Label lblServiceTime = new Label("Service Time(HH:MM)");
	    lblServiceTime.setSize("135px", "30px");
	    lblServiceTime.setStyleName("bold");
	    
	    Label lblblankdate = new Label();
	    lblblankdate.setSize("105px", "30px");
	    
	    Label lblReason = new Label("Reason for change");
		lblReason.setSize("140px", "30px");
		lblReason.setStyleName("bold");
		
		Label specificReasonlbl = new Label("Specific Reason");
		specificReasonlbl.setSize("110px", "30px");
		specificReasonlbl.setStyleName("bold");
		
		Label lblbank = new Label();
		lblbank.setSize("155px", "30px");
		
		Label lblOther = new Label("Remark");					//Date:26-12-2018 updated by: Viraj Des: Changed others to remark 
		lblOther.setSize("80px", "30px");
		lblOther.setStyleName("bold");
		
		tbOther = new TextArea();
		tbOther.setSize("700px", "30px");
		 
		Label lblblankfrbtn = new Label();
		lblblankfrbtn.setSize("155px", "30px");
		
		/**
		 * Updated By: Viraj
		 * Date: 03-12-2018
		 * Description: To Convert smartGWT layout to normal GWT layout
		 */
		
//		h1 = new HorizontalPanel();
//		h1.add(lblServiceTime);
//		h1.add(lblbank);
//		h1.add(specificReasonlbl);
//		h1.add(lblReason);
//		h1.add(lblblankfrbtn);
//		h1.add(lblbank);
//		h1.add(lblOther);
		
		
//		final HLayout hlLblReschedule = new HLayout ();
//		hlLblReschedule.setPadding(10);
//		hlLblReschedule.setAutoHeight();
//		hlLblReschedule.setAutoWidth();
//		hlLblReschedule.setMembersMargin(10);
//		
//		hlLblReschedule.addMember(lblServiceDate);
//		hlLblReschedule.addMember(lblServiceTime);
//        hlLblReschedule.addMember(specificReasonlbl);
//        hlLblReschedule.addMember(lblReason);
//        
//        hlLblReschedule.addMember(lblblankfrbtn);
//        hlLblReschedule.addMember(lblbank);
//
//        hlLblReschedule.addMember(lblOther);

		 
        DynamicForm dynamicform = new DynamicForm();
        dbserviceDate = new DateItem();
        dbserviceDate.setWidth(90);
        dbserviceDate.setUseTextField(true);
        dbserviceDate.setTitle("");
        
        dynamicform.setFields(dbserviceDate);
        
        servicetime = new ListBox();
        servicetime.setSize("55px", "30px");
        servicetime.insertItem("--", 0);
        servicetime.insertItem("01", 1);
        servicetime.insertItem("02", 2);
        servicetime.insertItem("03", 3);
        servicetime.insertItem("04", 4);
		servicetime.insertItem("05", 5);
		servicetime.insertItem("06", 6);
		servicetime.insertItem("07", 7);
		servicetime.insertItem("08", 8);
		servicetime.insertItem("09", 9);
		servicetime.insertItem("10", 10);
		servicetime.insertItem("11", 11);
		servicetime.insertItem("12", 12);
        
		
		p_servicemin=new ListBox();
		p_servicemin.setSize("55px", "30px");
		p_servicemin.insertItem("--", 0);
		p_servicemin.insertItem("00", 1);
		p_servicemin.insertItem("15", 2);
		p_servicemin.insertItem("30", 3);
		p_servicemin.insertItem("45", 4);
		
		p_ampm = new ListBox();
		p_ampm.setSize("55px", "30px");
		p_ampm.insertItem("--", 0);
		p_ampm.insertItem("AM", 1);
		p_ampm.insertItem("PM", 2);
		
		
		tbRecheduleReason = new TextBox();
		tbRecheduleReason.setSize("120px", "20px");
		
		btnReschedulePopUp = new Button("Reschedule");
		btnReschedulePopUp.addClickHandler(this);
		
		
		olbSpecificReason=new ObjectListBox<Config>();
		olbSpecificReason.setSize("100px", "30px");
		AppUtility.MakeLiveConfig(olbSpecificReason, Screen.SPECIFICREASONFORRESCHEDULING);
		olbSpecificReason.addChangeHandler(this);
		
		 
//		final HLayout hlReschedule = new HLayout ();
//		hlReschedule.setPadding(10);
//		hlReschedule.setAutoHeight();
//		hlReschedule.setAutoWidth();
//		hlReschedule.setMembersMargin(10);
//		hlReschedule.addMember(dynamicform);
//		 hlReschedule.addMember(servicetime);
//		 hlReschedule.addMember(p_servicemin);
//		 hlReschedule.addMember(p_ampm);
//		 hlReschedule.addMember(olbSpecificReason);
//		 hlReschedule.addMember(tbRecheduleReason);
//		 hlReschedule.addMember(btnReschedule);	  
//		 hlReschedule.addMember(lblblankfrbtn);	  
//		 hlReschedule.addMember(tbOther);	 
		 
//		h2 = new HorizontalPanel();
//		h2.add(dynamicform);
//		h2.add(servicetime);
//		h2.add(p_servicemin);
//		h2.add(p_ampm);
//		h2.add(olbSpecificReason);
//		h2.add(tbRecheduleReason);
//		h2.add(btnReschedulePopUp);
//		h2.add(lblblankfrbtn);
//		h2.add(tbOther);
		
		/**
		 * Updated By: Viraj
		 * Date: 04-01-2019
		 * Description: To change the structure of service tab
		 */
		
		h1 = new HorizontalPanel();
		h1.add(lblServiceDate);
		h1.add(specificReasonlbl);
		h1.add(lblReason);
		
		h2 = new HorizontalPanel();
		h2.add(dynamicform);
		h2.add(olbSpecificReason);
		h2.add(tbRecheduleReason);
		h2.add(btnReschedulePopUp);
		
		v1 = new VerticalPanel();
		v1.add(h1);
		v1.add(h2);
		v1.add(lblbank);
		
		h7 = new HorizontalPanel();
		h7.add(lblServiceTime);
		
		h8 = new HorizontalPanel();
		h8.add(servicetime);
		h8.add(p_servicemin);
		h8.add(p_ampm);
		
		v2 = new VerticalPanel();
		v2.add(h7);
		v2.add(h8);
		v2.add(lblblankfrbtn);
		
		h9 = new HorizontalPanel();
		h9.add(lblOther);
		h9.add(tbOther);
		
		
		v3 = new VerticalPanel();
		v3.add(v1);
		v3.add(v2);
		v3.add(h9);
		
		
		/** Ends **/
		
		
		 /**
		 * Updated By: Viraj 
		 * Date: 26-12-2018
		 * Description: Adding Tabular Structure to popup
		 */
		 flowPanel1 = new FlowPanel();
		 verticalPanel = new VerticalPanel();
		 verticalPanel.add(v3);
//		 verticalPanel.add(h2);
		 flowPanel1.add(verticalPanel);
//		 flowPanel1.add(hlLblReschedule);
//		 flowPanel1.add(hlReschedule);
//		 winModal.addChild(flowPanel1);
		 /** Ends **/
		 
//		 winModal.addItem(hlLblReschedule);
//	     winModal.addItem(hlReschedule);
	     
	     
//	 	final HLayout hlayoutTechnician = new HLayout ();
//		hlayoutTechnician.setPadding(10);
//		hlayoutTechnician.setAutoHeight();
//		hlayoutTechnician.setAutoWidth();
//		hlayoutTechnician.setMembersMargin(10);
//		hlayoutTechnician.setTop(2);
		
		
		Label lblTeam = new Label("Team");
		lblTeam.setSize("60px", "20px");
		lblTeam.setStyleName("bold");

		Label lblTechnician = new Label("Technician");
		lblTechnician.setSize("60px", "20px");
		lblTechnician.setStyleName("bold");
		
		
		olbTeam = new ObjectListBox<TeamManagement>();
		olbTeam.setTitle("Team");
		olbTeam.setSize("110px", "20px");
		this.initalizeTeamListBox();

		
		olbEmployee=new ObjectListBox<Employee>();
        olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.TECHNICIANSCEDULINGLIST, "Service Engineer");
        olbEmployee.setSize("110px", "20px");
        
	     
         Label lblInvoiceId = new Label("Invoice Id");
	     lblInvoiceId.setSize("80px", "20px");
	     lblInvoiceId.setStyleName("bold");
	     
	     olbInvoiceId = new ObjectListBox<Invoice>();
	     olbInvoiceId.addItem("--SELECT--");
	     olbInvoiceId.setSize("110px", "20px");
	     olbInvoiceId.addChangeHandler(this);

	     
	     Label lblInvoiceDate = new Label("Invoice Date");
	     lblInvoiceDate.setSize("80px", "20px");
	     lblInvoiceDate.setStyleName("bold");
	     
	     olbInvoiceDate = new ObjectListBox<Invoice>();
	     olbInvoiceDate.addItem("--SELECT--");
	     olbInvoiceDate.setSize("110px", "20px");
	     olbInvoiceDate.addChangeHandler(this);
        
	     Label lblblank2 = new Label();
	     
//         hlayoutTechnician.addMember(lblTeam);
//         hlayoutTechnician.addMember(olbTeam);
//         hlayoutTechnician.addMember(lblTechnician);
//		 hlayoutTechnician.addMember(olbEmployee);
//		 hlayoutTechnician.addMember(btnTechnicianAdd);
//		 hlayoutTechnician.addMember(lblblank2);
//		 hlayoutTechnician.addMember(lblInvoiceId);
//		 hlayoutTechnician.addMember(olbInvoiceId);
//		 hlayoutTechnician.addMember(lblInvoiceDate);
//		 hlayoutTechnician.addMember(olbInvoiceDate);
		 
	     h3 = new HorizontalPanel();
	     h3.add(lblTeam);
	     h3.add(olbTeam);
	     h3.add(lblTechnician);
	     h3.add(olbEmployee);
	     h3.add(btnTechnicianAdd);
	     h3.add(lblblank2);
	     h3.add(lblInvoiceId);
	     h3.add(olbInvoiceId);
	     h3.add(lblInvoiceDate);
	     h3.add(olbInvoiceDate);
	     
	     
		 technicianTable = new TechnicianTable();
	     technicianTable.connectToLocal();
	     /**
		 * Updated By: Viraj 
		 * Date: 26-12-2018
		 * Description: Adding Tabular Structure to popup
		 */
	     verticalPanel1 = new VerticalPanel();
	     verticalPanel1.add(h3);
	     verticalPanel1.add(technicianTable.getTable());
	     flowPanel2 = new FlowPanel();
	     flowPanel2.add(verticalPanel1);
//	     winModal.addItem(flowPanel2);
	     /** Ends **/
	     
//	     winModal.addItem(hlayoutTechnician);
//	     winModal.addItem(technicianTable.getTable());
	     
	     final HLayout layputBlank = new HLayout ();
	     layputBlank.setPadding(5);
	     layputBlank.setAutoHeight();
	     layputBlank.setAutoWidth();
	     winModal.addItem(layputBlank);
	     
		 /*********************** Technician info ends here *******************/

	     /****************** Material info start here ***********************/
	     
//	     final HLayout lblservicewarehouse = new HLayout ();
//	        lblservicewarehouse.setPadding(10);
//	        lblservicewarehouse.setAutoHeight();
//	        lblservicewarehouse.setAutoWidth();
//	        lblservicewarehouse.setMembersMargin(10);
	        
	        Label lblMaterial = new Label("Material Name");
	        lblMaterial.setStyleName("bold");
	        lblMaterial.setSize("120px", "20px");
	        Label lblQuantity = new Label("Quantity");
	        lblQuantity.setStyleName("bold");
	        lblQuantity.setSize("120px", "20px");
	        Label lblWarehouse = new Label("Warehouse");
	        lblWarehouse.setStyleName("bold");
	        lblWarehouse.setSize("130px", "20px");
	        Label lblStorageLocation = new Label("Storage Location");
	        lblStorageLocation.setStyleName("bold");
	        lblStorageLocation.setSize("130px", "20px");
	        Label lblStorageBin = new Label("Storage Bin");
	        lblStorageBin.setStyleName("bold");
	        lblStorageBin.setSize("130px", "20px");
//	        lblservicewarehouse.addMember(lblMaterial);
//	        lblservicewarehouse.addMember(lblQuantity);
//	        lblservicewarehouse.addMember(lblWarehouse);
//	        lblservicewarehouse.addMember(lblStorageLocation);
//	        lblservicewarehouse.addMember(lblStorageBin);

	        h4 = new HorizontalPanel();
	        h4.add(lblMaterial);
	        h4.add(lblQuantity);
	        h4.add(lblWarehouse);
	        h4.add(lblStorageLocation);
	        h4.add(lblStorageBin);
	        
		     olbitemProduct = new ObjectListBox<ItemProduct>();
			 olbitemProduct.addItem("--SELECT--");
			 olbitemProduct.setSize("120px", "20px");
			 initializeItemProdcut(companyId,olbitemProduct);
			 olbitemProduct.addChangeHandler(this);
			
	        
	        dbQuantity = new DoubleBox();
	        dbQuantity.setSize("120px", "20px");
	        
	        olbWarehouseName = new ObjectListBox<WareHouse>();
	        olbWarehouseName.addItem("--SELECT--");
	        olbWarehouseName.setSize("130px", "20px");

//	        initializeWarehouse(olbWarehouseName,companyId);
	        olbWarehouseName.addChangeHandler(this);
	        
	        olbStorageLocation = new ObjectListBox<StorageLocation>();
	        olbStorageLocation.addItem("--SELECT--");
	        olbStorageLocation.setSize("130px", "20px");
	        olbStorageLocation.addChangeHandler(this);

	        olbstorageBin = new ObjectListBox<Storagebin>();
	        olbstorageBin.addItem("--SELECT--");
	        olbstorageBin.setSize("130px", "20px");
	        
	        btnproductAdd = new Button("Add");
	        btnproductAdd.addClickHandler(this);

//	        final HLayout warehouselayout = new HLayout ();
//	        warehouselayout.setPadding(10);
//	        warehouselayout.setAutoHeight();
//	        warehouselayout.setAutoWidth();
//	        warehouselayout.setMembersMargin(10);
//	        warehouselayout.addMember(olbitemProduct);
//	        warehouselayout.addMember(dbQuantity);
//	        warehouselayout.addMember(olbWarehouseName);
//	        warehouselayout.addMember(olbStorageLocation);
//	        warehouselayout.addMember(olbstorageBin);
//	        warehouselayout.addMember(btnproductAdd);
	        
	        h5 = new HorizontalPanel();
	        h5.add(olbitemProduct);
	        h5.add(dbQuantity);
	        h5.add(olbWarehouseName);
	        h5.add(olbStorageLocation);
	        h5.add(olbstorageBin);
	        h5.add(btnproductAdd);

	        materialProductTable = new MaterialProductTable();
	        materialProductTable.connectToLocal();
	        
	        /**
			 * Updated By: Viraj 
			 * Date: 26-12-2018
			 * Description: Adding Tabular Structure to popup
			 */
	         materialProductTable.getTable().setWidth("950px");
		     verticalPanel2 = new VerticalPanel();
		     verticalPanel2.add(h4);
		     verticalPanel2.add(h5);
		     verticalPanel2.add(materialProductTable.getTable());
		     flowPanel3 = new FlowPanel();
		     flowPanel3.add(verticalPanel2);
//		     winModal.addItem(flowPanel3);
		     /** Ends **/
	        
//	        winModal.addItem(lblservicewarehouse);
//	        winModal.addItem(warehouselayout);
//	        winModal.addItem(materialProductTable.getTable());
	     
	        /****************** Material info ends here ***********************/
	     
	     /****************** Tool info start here ***********************/
	     
	     
	     Label lblTool = new Label("Tool");
	     lblTool.setSize("60px", "20px");
	     lblTool.setStyleName("bold");

	     
//	     final HLayout layoutTool = new HLayout ();
//	     layoutTool.setAutoHeight();
//	     layoutTool.setAutoWidth();
//	     layoutTool.setMembersMargin(10);
//	     layoutTool.setPadding(10);
	     
	     btnToolAdd = new Button("Add");  
	     btnToolAdd.addClickHandler(this);
	     
	     olbTool = new ObjectListBox<>();
	     olbTool.setSize("150px", "20px");
	     AppUtility.makeCompanyAssetListBoxLive(olbTool);
	     
	     
	     Label lblblankTool = new Label();
	     
//		 layoutTool.addMember(lblTool);
//		 layoutTool.addMember(olbTool);
//		 layoutTool.addMember(btnToolAdd); 
		 
	     h6 = new HorizontalPanel();
	     h6.add(lblTool);
	     h6.add(olbTool);
	     h6.add(btnToolAdd);
		 
	     toolTable = new TechnicianToolTable();
	     toolTable.connectToLocal();
	     toolTable.getTable().setWidth("900px");
	     /** Ends **/
	     
	     /**
		 * Updated By: Viraj 
		 * Date: 26-12-2018
		 * Description: Adding Tabular Structure to popup
		 */
	     verticalPanel3 = new VerticalPanel();
	     verticalPanel3.add(h6);
	     verticalPanel3.add(toolTable.getTable());
	     flowPanel4 = new FlowPanel();
	     flowPanel4.add(verticalPanel3);
//	     winModal.addItem(flowPanel4);
	     
//	     winModal.addItem(layoutTool);
//	     winModal.addItem(toolTable.getTable());
//	     tabPanel = new TabPanel();
//	     VLayout v1 = new VLayout();
//	     v1.addMember(hlLblReschedule);
//	     v1.addMember(hlReschedule);
//	     flowPanel1.add(v1);
	   //  flowPanel1.addStyleName("gwt-SuggestBoxPopup");
	     tabPanel.add(flowPanel1 , "Service Details");
         tabPanel.add(flowPanel2 , "Technician Details");
         tabPanel.add(flowPanel3 , "Material Details");
         tabPanel.add(flowPanel4 , "Tools");
         tabPanel.selectTab(0);
//         tabPanel.setSize("100%","80% !important");
         
         
	     winModal.addItem(tabPanel);
	     /** Ends **/
	     
//	     final HLayout layputBlank2 = new HLayout ();
//	     layputBlank2.setPadding(5);
//	     layputBlank2.setAutoHeight();
//	     layputBlank2.setAutoWidth();
//	     winModal.addItem(layputBlank2);
	     
	     /***************** Tool info ends here ***********************/
        
       final HLayout okbtnlayout = new HLayout ();
       okbtnlayout.setPadding(10);
       okbtnlayout.setAlign(Alignment.CENTER);
       btnOk = new IButton("Save");
       btnOk.addClickHandler(this);
       
       Label lblblank = new Label();
       Label lblblank6 = new Label();
       Label lblblank3 = new Label();
       
       Label lblblank4 = new Label();
       lblblank4.setSize("30px", "10px");
       
       Label lblblank5 = new Label();
       lblblank5.setSize("30px", "10px");
       
       btnComplete = new IButton();
       btnComplete.setTitle("Complete Service");

//       okbtnlayout.addMember(lblblank);
//       okbtnlayout.addMember(lblblank4);

       okbtnlayout.addMember(btnComplete);
       okbtnlayout.addMember(lblblank3);
       okbtnlayout.addMember(lblblank5);
       okbtnlayout.addMember(btnOk);
       
       final HLayout hlblank = new HLayout ();
       hlblank.setPadding(10);
       
//       winModal.addItem(hlblank);
       winModal.addItem(okbtnlayout);
       	
	}
	

	public void showPopUp(){
		olbEmployee.setSelectedIndex(0);
		olbitemProduct.setSelectedIndex(0);
		olbTeam.setSelectedIndex(0);
		olbTool.setSelectedIndex(0);
		olbWarehouseName.setSelectedIndex(0);
		olbStorageLocation.setSelectedIndex(0);
		olbstorageBin.setSelectedIndex(0);
		
		 winModal.show();
	}
	
	public void hidePopUp(){
		winModal.hide();
	}
	
	private void initalizeTeamListBox() {
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new TeamManagement());
		olbTeam.MakeLive(querry);
	}

	private void initializeItemProdcut(Long companyId, final ObjectListBox<ItemProduct> olbitemProduct) {
		// TODO Auto-generated method stub
		
		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec=new Vector<Filter>();
		
		Filter filter =null;
		
		filter= new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(companyId);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ItemProduct());
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				
				List<ItemProduct> list = new ArrayList<ItemProduct>();
				for(SuperModel model : result){
					ItemProduct item = (ItemProduct) model;
					
					olbitemProduct.addItem(item.getProductName());
					list.add(item);
				}
				olbitemProduct.setItems(list);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	private void initializeWarehouse(final ObjectListBox<WareHouse> olbWarehouseName, Long companyId) {
		// TODO Auto-generated method stub
		
		MyQuerry query = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(companyId);
		filtervec.add(filter);
	
		query.setFilters(filtervec);
		query.setQuerryObject(new WareHouse());
		
		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				for(SuperModel model :result){
					WareHouse warehouse = (WareHouse) model;
					olbWarehouseName.addItem(warehouse.getBusinessUnitName());
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	@Override
	public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
		// TODO Auto-generated method stub
		System.out.println("Inside onClick method" );
		
		if(event.getSource()==btnTechnicianAdd){
			System.out.println("btn add tech ");
			if(ValidateTechnician()){
				
				Employee empEntity=olbEmployee.getSelectedItem();

				if(olbEmployee.getSelectedIndex()!=0){
					/** date 15/11/2017 added by komal to add employee only once **/
				    List<EmployeeInfo> empList = technicianTable.getDataprovider().getList();
				    for(EmployeeInfo empInfo : empList){
				    	if(empEntity.getCount() == empInfo.getEmpCount()){
				    		showDialogMessage("Technician is already present in Table..Pleease select different one");
				    		return;
				    	}
				    }
			  		EmployeeInfo empInfoEntity = new EmployeeInfo();
			  		
			  		empInfoEntity.setEmpCount(empEntity.getCount());
			  		empInfoEntity.setFullName(empEntity.getFullName());
			      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
			      	empInfoEntity.setDesignation(empEntity.getDesignation());
			      	empInfoEntity.setDepartment(empEntity.getDepartMent());
			      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
			      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
			      	empInfoEntity.setBranch(empEntity.getBranchName());
			      	empInfoEntity.setCountry(empEntity.getCountry());
			      	
			      	technicianTable.getDataprovider().getList().add(empInfoEntity);
				}
				else if(olbTeam.getSelectedIndex()!=0){
//					EmployeeInfo empInfoEntity = new EmployeeInfo();
//					empInfoEntity.setFullName(olbTeam.getValue());
					
					ArrayList<EmployeeInfo> list = new ArrayList<EmployeeInfo>();
					TeamManagement teamManangementEntity = olbTeam.getSelectedItem();
					for(int i=0;i<teamManangementEntity.getTeamList().size();i++){
						
						EmployeeInfo empInfoEntity = new EmployeeInfo();
				  		
				  		empInfoEntity.setFullName(teamManangementEntity.getTeamList().get(i).getEmployeeName());
				     
				      	
				      	list.add(empInfoEntity);
					}
					
//					technicianTable.getDataprovider().getList().add(empInfoEntity);
					
					
					technicianTable.getDataprovider().setList(list);
					
				}
			}
		}
		
		if(event.getSource()==btnToolAdd){
			System.out.println("btnToolAdd ==");
			if(olbTool.getSelectedIndex()!=0){
				System.out.println("btnToolAdd == 222222222");

				CompanyAsset companyAsset = olbTool.getSelectedItem();
//				CompanyAsset techniciantoolinfo = new CompanyAsset();
//				techniciantoolinfo.setName(olbTool.getValue(olbTool.getSelectedIndex()));
//				techniciantoolinfo.setCount(companyAsset.getCount());
				toolTable.getDataprovider().getList().add(companyAsset);
				
			}else{
				System.out.println("btnToolAdd else");
				showDialogMessage("Please Select Tool");
			}
			
		}
		
		if(event.getSource()==btnproductAdd){
			
			System.out.println("btnproductAdd ==");

			if(ValidateWarehouse()){
				System.out.println("btnproductAdd == 222222222222");
				
				ItemProduct product = olbitemProduct.getSelectedItem();
//				TechnicianMaterialInfo technicianMaterialIfno = new TechnicianMaterialInfo();
				ProductGroupList technicianMaterialIfno  = new ProductGroupList();
				technicianMaterialIfno.setName(olbitemProduct.getValue(olbitemProduct.getSelectedIndex()));
				if(dbQuantity.getValue()!=null)
				technicianMaterialIfno.setQuantity(dbQuantity.getValue());
				if(olbWarehouseName.getSelectedIndex()!=0)
					technicianMaterialIfno.setWarehouse(olbWarehouseName.getValue(olbWarehouseName.getSelectedIndex()));
				if(olbStorageLocation.getSelectedIndex()!=0)
					technicianMaterialIfno.setStorageLocation(olbStorageLocation.getValue(olbStorageLocation.getSelectedIndex()));	
				if(olbstorageBin.getSelectedIndex()!=0)
					technicianMaterialIfno.setStorageBin(olbstorageBin.getValue(olbstorageBin.getSelectedIndex()));	

				technicianMaterialIfno.setProduct_id(product.getCount());
				technicianMaterialIfno.setCode(product.getProductCode());
				technicianMaterialIfno.setUnit(product.getUnitOfMeasurement());
				
				materialProductTable.getDataprovider().getList().add(technicianMaterialIfno);
			}
		}
		
	}
	
	private boolean validateRechedule() {

		if(dbserviceDate.getValueAsDate()==null){
			showDialogMessage("Please add Service Date");
			return false;
		}
		if(tbRecheduleReason.getValue()==null || tbRecheduleReason.getValue().equals("")){
			showDialogMessage("Please add resaon for Rechedule");
			return false;
		}
		
		return true;
	}

	private boolean ValidatePopupData() {
		
		if(technicianTable.getDataprovider().getList().size()==0){
			showDialogMessage("Please add Technician ");
			return false;
		}
		if(materialProductTable.getDataprovider().getList().size()==0){
			showDialogMessage("Please add Material Info ");
			return false;
		}
		if(servicetime.getSelectedIndex()==0){
			showDialogMessage("Please Select Service Time");
			return false;
		}
		if(dbserviceDate.getValue()==null){
			showDialogMessage("Service Date can not be blank");
			return false;
		}
		
		return true;
	}

	private boolean ValidateWarehouse() {

		if(olbitemProduct.getSelectedIndex()==0){
			System.out.println("olbitemProduct");
			showDialogMessage("Please select Material information");
			return false;
		}
		
//		if(olbitemProduct.getSelectedIndex()!=0 && dbQuantity.getValue()==null ){
//			showDialogMessage("Please add quantity");
//			return false;
//		}
		
		List<ProductGroupList> list = materialProductTable.getDataprovider().getList();

		if(olbWarehouseName.getSelectedIndex()!=0){
			if(olbStorageLocation.getSelectedIndex()==0){
				showDialogMessage("Please select storage location");
				return false;
			}
			if(olbstorageBin.getSelectedIndex()==0){
				showDialogMessage("Please select storage Bin");
				return false;
			}
			
			if(olbitemProduct.getSelectedIndex()!=0 && dbQuantity.getValue()==null ){
				showDialogMessage("Please add quantity");
				return false;
			}
			
			for(int i=0;i<list.size();i++){
				if(list.get(i).getWarehouse()==null){
					showDialogMessage("Please add material either with warehouse or without warehouse");
					return false;
				}
			}
		}
		
		if(olbWarehouseName.getSelectedIndex()==0){
			
			for(int i=0;i<list.size();i++){
				if(list.get(i).getWarehouse()!=null && !list.get(i).getWarehouse().equals("") ){
					showDialogMessage("Please Select Warehouse details!");
					return false;
				}
			}
		}
		
		
		return true;
	}

	private boolean ValidateTechnician() {

		if(olbEmployee.getSelectedIndex()==0  && olbTeam.getSelectedIndex()==0){
			showDialogMessage("Please Select either Team or Technician");
			return false;
		}
		else if(olbEmployee.getSelectedIndex()!=0 && olbTeam.getSelectedIndex()!=0){
			showDialogMessage("Please Select either Team or Technician");
			return false;
		}
		
		
		return true;
	}

	/**** Smart GWT dialog message popup *****************/
	public static void showDialogMessage(String Message)
	{
		SC.say(Message);
	}


	@Override
	public void onChange(ChangeEvent event) {

		if(event.getSource() == olbitemProduct){
			
			if(olbitemProduct.getSelectedIndex()==0){
				olbWarehouseName.setSelectedIndex(0);
				olbStorageLocation.setSelectedIndex(0);
				olbstorageBin.setSelectedIndex(0);
			}else{
				ItemProduct product = olbitemProduct.getSelectedItem();
				initializeProductWarehouse(product.getCount());
			}
		}
		if(event.getSource()==olbWarehouseName){
			if(olbWarehouseName.getSelectedIndex()==0){
				olbStorageLocation.setSelectedIndex(0);
				olbstorageBin.setSelectedIndex(0);
			}else{
				initializeStoragelocation(olbWarehouseName.getValue(olbWarehouseName.getSelectedIndex()));
			}
		}
		if(event.getSource()==olbStorageLocation){
			initializeBin(olbStorageLocation.getValue(olbStorageLocation.getSelectedIndex()));
		}
		
		if(event.getSource()==olbSpecificReason){
			if(olbSpecificReason.getValue()!=null){
				tbRecheduleReason.setValue("");
				tbRecheduleReason.setValue(olbSpecificReason.getValue());
			}else{
				tbRecheduleReason.setValue("");
			}
		}
		
		
		if(event.getSource() == olbInvoiceId){
			setinvoiceDateDropDown();
		}
		if(event.getSource() == olbInvoiceDate){
			setInvoiceIdDroDown();
		}
	}
	

	private void setInvoiceIdDroDown() {

		if(olbInvoiceDate.getSelectedIndex()!=0){
//			olbInvoiceId.removeAllItems();
//			olbInvoiceId.addItem("--SELECT--");
			Invoice invoiceEntity = olbInvoiceDate.getSelectedItem();
			olbInvoiceId.setValue(invoiceEntity.getCount()+"");
			
		}else{
			olbInvoiceId.setSelectedIndex(0);
		}
	}

	private void setinvoiceDateDropDown() {
		if(olbInvoiceId.getSelectedIndex()!=0){
//			olbInvoiceDate.removeAllItems();
//			olbInvoiceDate.addItem("--SELECT--");
			Invoice invoicentity = olbInvoiceId.getSelectedItem();
			olbInvoiceDate.setValue(DateTimeFormat.getShortDateFormat().format(invoicentity.getInvoiceDate()));
		}else{
			olbInvoiceDate.setSelectedIndex(0);
		}
	}

	private void initializeProductWarehouse(int prodId) {

		olbWarehouseName.removeAllItems();
		
		MyQuerry query = new MyQuerry();
//		Company c=new Company();
//		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
//		filter = new Filter();
//		filter.setQuerryString("companyId");
//		filter.setLongValue(companyId);
//		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("productinfo.prodID");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new ProductInventoryView());
		
		olbWarehouseName.addItem("--SELECT--");
		
		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("Warehouse size as per product"+result.size());
				for(SuperModel model : result){
					ProductInventoryView product = (ProductInventoryView) model;
					for(int i=0;i<product.getDetails().size();i++){
						olbWarehouseName.addItem(product.getDetails().get(i).getWarehousename().trim());
						System.out.println("hiiiiiiiiiiiiiii");
					}
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	private void initializeBin(String storageLocation) {
		
		olbstorageBin.removeAllItems();

		MyQuerry query = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(companyId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("storagelocation");
		filter.setStringValue(storageLocation);
		filtervec.add(filter);
	
		query.setFilters(filtervec);
		query.setQuerryObject(new Storagebin());
		
		olbstorageBin.addItem("--SELECT--");
		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("result of storage bin =="+result.size());
				for(SuperModel model :result){
					Storagebin storagebin = (Storagebin) model;
					olbstorageBin.addItem(storagebin.getBinName());
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	private void initializeStoragelocation(String warehouseName) {

		olbStorageLocation.removeAllItems();
		MyQuerry query = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(companyId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("warehouseName");
		filter.setStringValue(warehouseName);
		filtervec.add(filter);
	
		query.setFilters(filtervec);
		query.setQuerryObject(new StorageLocation());
		
		olbStorageLocation.addItem("--SELECT--");
		
		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				
				
				for(SuperModel model :result){
					StorageLocation storageLocation = (StorageLocation) model;
					olbStorageLocation.addItem(storageLocation.getBusinessUnitName());
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	
	public void SetEnableWidgets(boolean state){
		
		System.out.println("Hi vijay widgets enabled method"+ state);
		tbRecheduleReason.setEnabled(state);
		tbOther.setEnabled(state);
		technicianTable.setEnable(state);
		toolTable.setEnable(state);
		materialProductTable.setEnable(state);
		
		olbEmployee.setEnabled(state);
		olbTeam.setEnabled(state);
		
		olbTool.setEnabled(state);
		servicetime.setEnabled(state);
		p_servicemin.setEnabled(state);
		p_ampm.setEnabled(state);
		olbSpecificReason.setEnabled(state);
		olbitemProduct.setEnabled(state);
		dbQuantity.setEnabled(state);
		olbWarehouseName.setEnabled(state);
		olbStorageLocation.setEnabled(state);
		olbstorageBin.setEnabled(state);
		olbInvoiceId.setEnabled(state);
		olbInvoiceDate.setEnabled(state);
		
		
		dbserviceDate.setDisabled(!state);
		btnComplete.setDisabled(!state);
		btnOk.setDisabled(!state);
		btnproductAdd.setEnabled(state);
		btnReschedulePopUp.setEnabled(state);
		btnTechnicianAdd.setEnabled(state);
		btnToolAdd.setEnabled(state);
	}
	
	/****************************** Getter Setter ****************************/
	
	
	public IButton getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(IButton btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnReschedule() {
		return btnReschedulePopUp;
	}

	public void setBtnReschedule(Button btnReschedule) {
		this.btnReschedulePopUp = btnReschedule;
	}

	public IButton getBtnComplete() {
		return btnComplete;
	}

	public void setBtnComplete(IButton btnComplete) {
		this.btnComplete = btnComplete;
	}

	public ListBox getP_servicemin() {
		return p_servicemin;
	}

	public void setP_servicemin(ListBox p_servicemin) {
		this.p_servicemin = p_servicemin;
	}

	public ListBox getP_ampm() {
		return p_ampm;
	}

	public void setP_ampm(ListBox p_ampm) {
		this.p_ampm = p_ampm;
	}

	public ListBox getServicetime() {
		return servicetime;
	}

	public void setServicetime(ListBox servicetime) {
		this.servicetime = servicetime;
	}

	public ObjectListBox<Invoice> getOlbInvoiceId() {
		return olbInvoiceId;
	}

	public void setOlbInvoiceId(ObjectListBox<Invoice> olbInvoiceId) {
		this.olbInvoiceId = olbInvoiceId;
	}

	public ObjectListBox<Invoice> getOlbInvoiceDate() {
		return olbInvoiceDate;
	}

	public void setOlbInvoiceDate(ObjectListBox<Invoice> olbInvoiceDate) {
		this.olbInvoiceDate = olbInvoiceDate;
	}

	public TextArea getTbOther() {
		return tbOther;
	}

	public void setTbOther(TextArea tbOther) {
		this.tbOther = tbOther;
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}
	
}
