package com.slicktechnologies.client.views.technicianschedule;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

import org.apache.poi.ss.usermodel.HorizontalAlignment;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
//import com.google.gwt.event.dom.client.ClickEvent;
//import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.CustomerProjectService;
import com.slicktechnologies.client.services.CustomerProjectServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.ServiceListService;
import com.slicktechnologies.client.services.ServiceListServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.ExpenseReturnAmountHistoryPopup;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.device.CreateSingleServiceService;
import com.slicktechnologies.client.views.device.CreateSingleServiceServiceAsync;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.device.ServicReschdulePopUp;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.popups.ContractDetailsPopup;
import com.slicktechnologies.client.views.popups.ServicelistServiceAssignTechnicianPopup;
import com.slicktechnologies.client.views.popups.ServicelistServiceMarkCompleptePopup;
import com.slicktechnologies.client.views.project.concproject.ProjectForm;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.service.ServiceSchedulePopup;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.server.CreateSingleServiceImpl;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;
import com.slicktechnologies.shared.common.technicianscheduling.TechnicianMaterialInfo;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.MultipleAppearance;
import com.smartgwt.client.types.Positioning;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Dialog;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
//import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;


public class TechnicianScheduleListPresenter implements ClickHandler, RecordClickHandler, CellClickHandler, ChangeHandler, com.google.gwt.event.dom.client.ClickHandler {
	
	static TechnicianScheduleListForm form;
	protected AppMemory mem;
	
	
	
	public static ArrayList<Service> globalServicelist;
	ArrayList<Customer>customerServicelist;//Added by Ashwini
	
	protected GenricServiceAsync genservice=GWT.create(GenricService.class);
	CustomerProjectServiceAsync projectAsync=GWT.create(CustomerProjectService.class);
	
	/*
	 * Date:12/08/2018
	 * Developer:Ashwini
	 */
	public final GenricServiceAsync service = GWT.create(GenricService.class);
	
	public final  CreateSingleServiceServiceAsync customerId=GWT.create(CreateSingleServiceService.class);
	  final ArrayList<Customer> customerMainlist=new ArrayList<Customer>();
	/*
	 * End by Ashwini
	 */
	 
	Service model=null;
	 boolean antipestFlag = false;
	 
	 TechnicianSchedulingSearchProxy searchPopup = new TechnicianSchedulingSearchProxy();
	 /** date 15/11/2017 added by komal for service date format **/
	 DateTimeFormat format= DateTimeFormat.getFormat("MM dd yyyy");
	 DateTimeFormat format1= DateTimeFormat.getFormat("dd-MMM-yyyy");
	 DateTimeFormat format2= DateTimeFormat.getFormat("MM/dd/yyyy");

	
	 /** date 13/11/2017 added by komal for download **/
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	/**
	 * Date 16-03-2018 by vijay
	 * 
	 */
	ListGridRecord selectedRecord=null;
	TechnicianPlanPopUp planPopUp = new TechnicianPlanPopUp();
	ServiceCompletetionPopup completeServicePopUp = new ServiceCompletetionPopup();
	
	
	/*
	 * Added by Ashwini
	 */
     ContractDetailsPopup contactpopup = new ContractDetailsPopup();
     ArrayList<Customer> custList = new ArrayList<Customer>();
	/*
	 * End by Ashwini
	 */
	
	/**
	 * Added By Komal
	 * Date : 18 Nov 2017
	 * Description : Used for technician validation
	 */
	boolean technianValidateflag = true;
//	DateTimeFormat format1= DateTimeFormat.getFormat("dd-MMM-yyyy");
	/**
	 * Ends For Komal
	 */
	TechnicianSchedulePrintPopUp reschedulePopup = new TechnicianSchedulePrintPopUp(true);
	public final ServiceListServiceAsync servicelistservice=GWT.create(ServiceListService.class);
	final GeneralServiceAsync technicianPlanInfoSync = GWT.create(GeneralService.class);
	final GeneralServiceAsync servicerecords = GWT.create(GeneralService.class);

	/****** For SMS *****/
	 private long cellNo;
	 private String companyName;
	 private String actualMsg;
	final SmsServiceAsync smsserviceAsync=GWT.create(SmsService.class);
	/**
	 * ends here
	 */
	/**
	 * Updated By: Viraj
	 * Date: 27-12-2018
	 * Description: To add the functionalities of customer service list
	 */
	PopupPanel panel=new PopupPanel(true);
	DocumentCancellationPopUp cancellationPopup = new DocumentCancellationPopUp();
	UpdateServiceAsync updateService=GWT.create(UpdateService.class);
	final ServiceListServiceAsync servicelistserviceAsync = GWT.create(ServiceListService.class);
	Service serviceObject;
	ServicelistServiceMarkCompleptePopup serviceCompletionPopup = new ServicelistServiceMarkCompleptePopup();
	ServicelistServiceAssignTechnicianPopup serviceTechnicianPopup = new ServicelistServiceAssignTechnicianPopup();
	ServicReschdulePopUp reschedule=new ServicReschdulePopUp();
	/** Ends **/
	
	public TechnicianScheduleListPresenter(TechnicianScheduleListForm homeForm) {
		this.form=homeForm;
		
		mem=AppMemory.getAppMemory();
		setEventHandeling();
		
		form.grid.addRecordClickHandler(this);
		form.grid.addCellClickHandler(this);
		
		searchPopup.getBtnGo().addClickHandler(this);
		
		/**
		 * Date 16-03-2018 by vijay
		 * 
		 */
		planPopUp.btnOk.addClickHandler(this);
		planPopUp.btnReschedulePopUp.addClickHandler(this);
		planPopUp.getBtnComplete().addClickHandler(this);
		
		completeServicePopUp.getBtnOk().addClickHandler(this);
		completeServicePopUp.getBtnCancel().addClickHandler(this);
		
		reschedulePopup.getBtnReschedule().addClickHandler(this);
		reschedulePopup.getBtnReshceduleCancel().addClickHandler(this);
		/**
		 * ends here
		 */
		
		contactpopup.getLblOk().addClickHandler(this);//Added by Ashwini
		/**
		 * Updated By: Viraj
		 * Date: 27-12-2018
		 * Description: To add the functionalities of customer service list
		 */
		cancellationPopup.getBtnOk().addClickHandler(this);
		cancellationPopup.getBtnCancel().addClickHandler(this);
		
		form.btnCancelServ.addClickHandler(this);
		form.btnSuspendServ.addClickHandler(this);
		form.btnResumeServ.addClickHandler(this);
		form.btnViewContract.addClickHandler(this);
		form.btnViewDoc.addClickHandler(this);
		
		form.btnMarkComplete.addClickHandler(this);
		serviceCompletionPopup.getLblOk().addClickHandler(this);
		serviceCompletionPopup.getLblCancel().addClickHandler(this);
		
		form.btnAssignTech.addClickHandler(this);
		serviceTechnicianPopup.getLblOk().addClickHandler(this);
		serviceTechnicianPopup.getLblCancel().addClickHandler(this);
		
		form.btnReschedule.addClickHandler(this);
		reschedule.getBtnReschedule().addClickHandler(this);
		reschedule.getBtnCancel().addClickHandler(this);
		
		/** Ends **/
		
	}
	
	
	/**
	 * Updated By: Viraj
	 * Date: 27-12-2018
	 * Description: to add all the functionalities of customer service list
	 */
	
	private boolean validSelectedRecord() {
		String message="";
		ArrayList<Service> selectedlist = getAllSelectedRecords();
		
		if(selectedlist.size()==0){
			form.showDialogMessage("Please select records!");
			return false;
		}
		for(Service object:selectedlist){
			/**
			 * Added validation on selected record for cancellation(NBHC CCPM)
			 */		
			if(object.getStatus().equals(Service.SERVICESTATUSCANCELLED)
					||object.getStatus().equals(Service.SERVICESTATUSCLOSED)
					||object.getStatus().equals(Service.SERVICESTATUSCOMPLETED)
					||object.getStatus().equals(Service.SERVICESTATUSREPORTED)
					||object.getStatus().equals(Service.SERVICESTATUSPENDING)
					||object.getStatus().equals(Service.SERVICESTATUSTECHCOMPLETED)
					||object.getStatus().equals(Service.SERVICESUSPENDED)){
				message=message+"Service status should be Scheduled/Rescheduled.";
				form.showDialogMessage("Service status should be Scheduled/Rescheduled!");
				return false;
			}
		}
		
		return true;
	}
	
	private boolean validSelectedRecordForSuspend() {
		String message="";
		ArrayList<Service> selectedlist = getAllSelectedRecords();
		
		if(selectedlist.size()==0){
			form.showDialogMessage("Please select records!");
			return false;
		}
		for(Service object:selectedlist){
			/**
			 * Added validation on selected record for cancellation(NBHC CCPM)
			 */		
			if(object.getStatus().equals(Service.SERVICESTATUSCANCELLED)
					||object.getStatus().equals(Service.SERVICESTATUSCLOSED)
					||object.getStatus().equals(Service.SERVICESTATUSCOMPLETED)
					||object.getStatus().equals(Service.SERVICESTATUSREPORTED)
					||object.getStatus().equals(Service.SERVICESTATUSPENDING)
					||object.getStatus().equals(Service.SERVICESTATUSTECHCOMPLETED)
					||object.getStatus().equals(Service.SERVICESTATUSSCHEDULE)
					||object.getStatus().equals(Service.SERVICESTATUSRESCHEDULE))
			{
				message=message+"Service status should be Suspended.";
				form.showDialogMessage("Service status should be Suspended");
				return false;
			}
		}
		
		return true;
	}
	
	private ArrayList<Service> getAllSelectedRecords() {
		ListGridRecord[] record = form.grid.getSelection();
		System.out.println("table===="+ record.length);
		 ArrayList<Service> selectedlist=new ArrayList<Service>();
		 if(record.length != 0) {
			 for(int i=0;i< record.length;i++) {
				 ListGridRecord listGridRecord = record[i];
				 int ServiceId =  Integer.parseInt(listGridRecord.getAttribute("ServiceId"));
				 model = getServiceModel(ServiceId);
				 selectedlist.add(model);
			 }
			 
		 }
			 return selectedlist;
	}
	
	public void cancelServices(ArrayList<Service>list) {
		
		for(Service ser:list){
			ser.setReasonForChange("Service ID ="+ser.getCount()+" "+"Service status ="+ser.getStatus().trim()+"\n"
				+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"
				+"Remark ="+cancellationPopup.getRemark().getValue().trim()+" "+"Cancellation Date ="+AppUtility.parseDate(new Date()));
			ser.setStatus(Service.SERVICESTATUSCANCELLED);
			ser.setRecordSelect(false);
		}
//		form.showDialogMessage(Message);
		updateService.cancelServices(list,cancellationPopup.getRemark().getValue().trim() ,LoginPresenter.loggedInUser, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
//				form.hideWaitSymbol();
				form.showDialogMessage("An Unexpected Error occured !");
			}
			@Override
			public void onSuccess(String result) {
//				form.hideWaitSymbol();
				form.showDialogMessage(result);
				form.grid.redraw();
				
			}
		});
	}
	
	private void reactOnViewContract(ArrayList<Service> selectedList) {

		MyQuerry querry = new MyQuerry();
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter filter;
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(selectedList.get(0).getContractCount());
		filterVec.add(filter);
		
		querry.setFilters(filterVec);
		querry.setQuerryObject(new Contract());
//		form.showWaitSymbol();
		genservice.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
//				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("Contract Document Not Found!");
				}else{
					final Contract contract = (Contract) result.get(0);
					final ContractForm form = ContractPresenter.initalize();
					Timer timer = new Timer() {
						
						@Override
						public void run() {
							form.updateView(contract);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
//				form.hideWaitSymbol();
			}
		});
	}
	
	private void viewSelectedDocument() {
	
		ArrayList<Service> selectedlist = getAllSelectedRecords();
		final Service entity = selectedlist.get(0);
		AppMemory.getAppMemory().currentState = ScreeenState.VIEW;
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Service", Screen.DEVICE);
		DeviceForm.flag = false;
		final DeviceForm form = DevicePresenter.initalize();
		form.showWaitSymbol();
		AppMemory.getAppMemory().stickPnel(form);
		Timer timer = new Timer() {
			@Override
			public void run() {
				form.hideWaitSymbol();
				form.updateView(entity);
				form.setToViewState();
			}
		};
		timer.schedule(3000);
	}
	
	public void setTechnicianToServices(){
		final ArrayList<Service> selectedlist = getAllSelectedRecords();
//		form.showWaitSymbol();
		final String technician = serviceTechnicianPopup.getOlbEmployee().getValue();
		servicelistserviceAsync.setServiceTechnicianToServices(selectedlist, "", technician,UserConfiguration.getCompanyId(), new AsyncCallback<Integer>() {
			
			@Override
			public void onSuccess(Integer result) {
				if(result == selectedlist.size()){
					
					ArrayList<Service> list = getAllSelectedRecords();
					for(int i=0;i<selectedlist.size();i++){
						for(int j=0;j<list.size();j++){
							if(selectedlist.get(i).getCount() == list.get(j).getCount()){
								list.get(j).setEmployee(technician);
								/*** Date 27-11-2018 By Vijay for selected records set false for untick **/
								list.get(j).setRecordSelect(false);

							}
						}
						
					}
//					form.showDialogMessage("Services Mark Completed successfully!");
//					form.getSuperTable().getDataprovider().setList(list);
					form.setDataToListGrid(globalServicelist);
					serviceTechnicianPopup.hidePopUp();
					form.grid.redraw();
					
					
					form.showDialogMessage("Data updated sucessfully");
				}else{
					form.showDialogMessage("Data not updated.!");
				}
//				form.hideWaitSymbol();
			}
			@Override
			public void onFailure(Throwable caught) {
				serviceTechnicianPopup.hidePopUp();
//				form.hideWaitSymbol();
			}
		});
	}
	
	private void reactOnReschedule() {
		reschedule.getP_seviceBranch().setVisible(false);
		reschedule.getDateBox().setValue(new Date());
		   panel=new PopupPanel(true);
		   panel.add(reschedule);
		   reschedule.clear();
		   panel.addStyleName("gwt-SuggestBoxPopup");
		   panel.show();
		   panel.center();
	}
	
	public boolean validateReschedule()
	{
		Date rescheduleDate = reschedule.getDateBox().getValue();
		String reasonForChange=reschedule.getTextArea().getText().trim();
		if(rescheduleDate==null)
		{
			form.showDialogMessage("Reschedule Date Cannot be empty !");
			return false;
		}
		
		
		
		/**
		 * nidhi
		 * 21-06-20118
		 * for reschedule service for same month validate
		 * Date :- 23-07-2018 By Vijay as discussed with Vaishali mam this validation not for Admin level
		 */
			boolean reScheduleValide  = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceRescheduleWithInMonthOnly");
			
			if(reScheduleValide && !LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")){
					Date serDate = reschedule.getDateBox().getValue();
					ArrayList<Service> selectedlist = getAllSelectedRecords();
					for(int i=0;i<selectedlist.size();i++){
						
						Date preFirstDate = CalendarUtil.copyDate(selectedlist.get(i).getServiceDate());
						Date preLastDate = CalendarUtil.copyDate(selectedlist.get(i).getServiceDate());
						
						CalendarUtil.addMonthsToDate(preLastDate, 1);
						
						preLastDate.setDate(1);
						CalendarUtil.addDaysToDate(preLastDate, -1);
						
						/**
						 * Date 10-Aug-2018 By Vijay
						 * Des :- Reschedule date only allow service date month only.
						 */
						Date firstDateServiceDate = CalendarUtil.copyDate(selectedlist.get(i).getServiceDate());
						CalendarUtil.setToFirstDayOfMonth(firstDateServiceDate);
						
						if(serDate.after(preLastDate) || serDate.before(firstDateServiceDate) ){
							form.showDialogMessage("Service date can be Reschedule With in Service date's month only.");
							return false;
						}
						
//						if(serDate.before(form.getTbServiceDate().getValue())){
//							form.showDialogMessage("Reschedule service date should be after current service date.");
//							return false;
//						}
						
						/**
						 * ends here and above old code commented because reschedule date allow for the same month date. 
						 */
					}
					
					
			}
		
		
		if(reasonForChange.equals(""))
		{
			form.showDialogMessage("Reason For Change Cannot be Empty !");
			return false;
		}
		
		return true;
	  
	
	}
	
	/** Ends **/
	
	private void setEventHandeling() {
		
	  
		for(int k=0;k<mem.skeleton.menuLabels.length;k++)
		  {
			 if( mem.skeleton.registration[k]!=null)
				 		mem.skeleton.registration[k].removeHandler();
		  }
		 
		  for( int k=0;k<mem.skeleton.menuLabels.length;k++)
		  
			 mem.skeleton.registration[k]= mem.skeleton.menuLabels[k].addClickHandler(this);	  
	  
	}
	/**Date :27/12/2017 BY :Manisha
	 * Add argument in below method to separate the assessment services from n
	 * @param isassesmentflag
	 */

	public static void initialize(boolean isassesmentflag)
	{
		System.out.println("INSIDE CHART INITIALIZATION");
		/**Date :27/12/2017 BY :Manisha
		 * Add argument in below method to separate the assessment services from n
		 * @param isassesmentflag
		 */
		loadService(isassesmentflag);
		TechnicianScheduleListForm homeForm = new TechnicianScheduleListForm();
		AppMemory.getAppMemory().currentScreen=Screen.HOME;
		/**Date :27/12/2017 BY :Manisha
		 * Add argument in below method to separate the assessment services from n
		 * @param isassesmentflag
		 */
		
		if(isassesmentflag){
			AppMemory.getAppMemory().skeleton.getProcessName().setText(AppConstants.SERVICEMODULE+"/"+"Assessment Scheduling List");
		}
		else{
			AppMemory.getAppMemory().skeleton.getProcessName().setText(AppConstants.SERVICEMODULE+"/"+"Technician Scheduling list");
		}
		/**End**/
	
//		AppMemory.getAppMemory().skeleton.getProcessName().setText(AppConstants.SERVICEMODULE+"/"+"Technician Scheduling list");
		TechnicianScheduleListPresenter presenter= new TechnicianScheduleListPresenter(homeForm);
		AppMemory.getAppMemory().stickPnel(homeForm);	
		
	}

	public static void loadService(boolean isassesmentflag) {
		if (LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")) {
			/**Date :27/12/2017 BY :Manisha
			 * Add argument in below method to separate the assessment services from normal services
			 * @param isassesmentflag
			 */
			loadAllServices(isassesmentflag);
		}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BRANCHLEVELRESTRICTION")){

			HashSet<String> branchSet=new HashSet<String>();
			for(Employee emp:LoginPresenter.globalEmployee){
				if(UserConfiguration.getInfo()!=null&&UserConfiguration.getInfo().getEmpCount()==emp.getCount()){
					if(emp.getEmpBranchList()!=null&&emp.getEmpBranchList().size()!=0){
						for(EmployeeBranch empBranch:emp.getEmpBranchList()){
							branchSet.add(empBranch.getBranchName().trim());
						}
					}
				}
			}
			List<String> list=new ArrayList<String>();
			list.addAll(branchSet);
			
			loadServiceswithBranchLevelRestriction(list,isassesmentflag);
		}else{
			
			loadAllServices(isassesmentflag);
		}
		
	}
	
	private static void loadServiceswithBranchLevelRestriction(List<String> list,boolean isassessflag) {

		GenricServiceAsync service=GWT.create(GenricService.class);
		
		ArrayList<String> statuslist = new ArrayList<String>();
		statuslist.add("Scheduled");
//		statuslist.add("Completed");
		statuslist.add("Rescheduled");

		// plus seven days to date
		Date plusSevenDate = new Date();
		CalendarUtil.addDaysToDate(plusSevenDate, 1);
		
		// minus 7 days to date
		Date minusSevenDate = new Date();
		CalendarUtil.addDaysToDate(minusSevenDate, -1);
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		MyQuerry querry;
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statuslist);
		filtervec.add(filter);
		
		if(list.size()!=0){
			filter=new Filter();
			filter.setQuerryString("branch IN");
			filter.setList(list);
			filtervec.add(filter);
		}
		if(list.size()==0){
			String loginEmployee = LoginPresenter.loggedInUser;
			String branchName ="";
			for(int i=0;i<LoginPresenter.globalEmployee.size();i++){
				if(loginEmployee.trim().equals(LoginPresenter.globalEmployee.get(i).getFullName().trim())){
					branchName = LoginPresenter.globalEmployee.get(i).getBranchName();
				}
			}
			if(!branchName.equals("")){
				filter=new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(branchName);
				filtervec.add(filter);
			}
			
		}
		
		filter = new Filter();
		filter.setQuerryString("serviceDate >=");
		filter.setDateValue(minusSevenDate);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("serviceDate <=");
		filter.setDateValue(plusSevenDate);
		filtervec.add(filter);
		
		/**Date :27/12/2017 BY :Manisha
		 * To get the assessment services on scheduling list
		 * @param isassesmentflag
		 */
		if(isassessflag==true)
		{
		filter = new Filter();
		filter.setQuerryString("assessmentServiceFlag");
		filter.setBooleanvalue(isassessflag);
		filtervec.add(filter);
		}
		/**End**/
		querry=new MyQuerry();
		querry.setQuerryObject(new Service());
		querry.setFilters(filtervec);
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("RESULT SIZE with branch level restriction: "+result.size());
				
				 globalServicelist=new ArrayList<Service>();
				
				for(SuperModel model:result){
					Service customerEntity=(Service) model;
					globalServicelist.add(customerEntity);
				}
				form.setDataToListGrid(globalServicelist);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Failed");
				Console.log("RPC Failed");
			}
		});
		
	}

	private static void loadAllServices(boolean isassessflag) {

		GenricServiceAsync service=GWT.create(GenricService.class);
		
		ArrayList<String> statuslist = new ArrayList<String>();
		statuslist.add("Scheduled");
//		statuslist.add("Completed");
		statuslist.add("Rescheduled");

		// plus seven days to date
		Date plusSevenDate = new Date();
		CalendarUtil.addDaysToDate(plusSevenDate, 1);
		
		// minus 7 days to date
		Date minusSevenDate = new Date();
		CalendarUtil.addDaysToDate(minusSevenDate, -1);
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		MyQuerry querry;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statuslist);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("serviceDate >=");
		filter.setDateValue(minusSevenDate);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("serviceDate <=");
		filter.setDateValue(plusSevenDate);
		filtervec.add(filter);
		
		/**Date :27/12/2017 BY :Manisha
		 * To get the assessment services on scheduling list
		 * @param isassesmentflag
		 */
		if(isassessflag==true){
		filter = new Filter();
		filter.setQuerryString("assessmentServiceFlag");
		filter.setBooleanvalue(isassessflag);
		filtervec.add(filter);
		}
		/**End**/
		
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Service());
		querry.setFilters(filtervec);
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("RESULT SIZE : "+result.size());
				
				 globalServicelist=new ArrayList<Service>();
				
				for(SuperModel model:result){
					Service customerEntity=(Service) model;
					globalServicelist.add(customerEntity);
				}
				form.setDataToListGrid(globalServicelist);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Failed");
				Console.log("RPC Failed");
			}
		});
		
	}


	@Override
	public void onRecordClick(RecordClickEvent event) {

		System.out.println("On record click");
//		event.getRecord()
		System.out.println("Service Id =="+event.getRecord().getAttribute("ServiceId"));
	    model =getServiceModel(Integer.parseInt(event.getRecord().getAttribute("ServiceId").trim()));

	}

	private Service getServiceModel(int serviceId) {
		
		for(Service service:globalServicelist){
			if(service.getCount()==serviceId){
				return service;
			}
		}
		
		return null;
	}

	@Override
	public void onCellClick(CellClickEvent event) {
		
		ListGridRecord record =  event.getRecord();
        int colNum = event.getColNum();
        ListGridField field = form.grid.getField(colNum);
        String fieldName = form.grid.getFieldName(colNum);
        String fieldTitle = field.getTitle();
        
        Console.log("Clicked " + fieldTitle + ":" + record.getAttribute(fieldName) + " Clien Name: " + record.getAttribute("clientName"));
        System.out.println("Clicked " + fieldTitle + ":" + record.getAttribute(fieldName) + "  Allocation Id :: " + record.getAttribute("allocationId"));
        
        
//        final AllocateBooking model=getBookingDetails(Integer.parseInt(record.getAttribute("allocationId").trim()));
        if (fieldName.equals("Plan")) { 
			
						System.out.println("hi vijay plan");
						
						selectedRecord = record;
					    model =getServiceModel(Integer.parseInt(record.getAttribute("ServiceId").trim()));

					    getProjectDetails(model,record);
					
          }else if(fieldName.equals("Complete")){
        	
        	 selectedRecord = record;
			    model =getServiceModel(Integer.parseInt(record.getAttribute("ServiceId").trim()));
			    
			    if(record.getAttribute("Status").equals("Completed")){
			    	showDialogMessage("Service already completed");
			    }
			    else if(validateCompletion()){
					Date todayDate=new Date();
					if(todayDate.before(model.getServiceDate())){
						System.out.println("Service Date Exceeds");
						showDialogMessage("Service Date exceeds current date. Please reschedule the date");
						
					}else{
						ShowCompleteServicePopUp();
						
					}
				}
        } else if(fieldName.equals("Reschedule")){
        	
        	selectedRecord = record;
			 System.out.println("Slected record  Reschedule ==  ServiceId "+selectedRecord.getAttribute("ServiceId"));
		    model =getServiceModel(Integer.parseInt(record.getAttribute("ServiceId").trim()));

		    if(record.getAttribute("Status").equals("Completed")){
		    	showDialogMessage("Service already completed");
		    }else{
		    	
		    	reschedulePopup.makeCustBranchListBox(model.getPersonInfo().getCount(), model.getCompanyId(),model.getServiceBranch());
		    	reschedulePopup.dbserviceDate.setValue(model.getServiceDate());
		    	reschedulePopup.showReschedulePopup();
		    }
        }
        
        
        /*
         * Date:13/08/2018
         * Added by Ashwini
         * Des:To add contact details in Technician Scheduling list
         */
        else if (fieldName.equals("ContactDetail")){
        	
        	selectedRecord = record;
		    model =getServiceModel(Integer.parseInt(record.getAttribute("ServiceId").trim()));
		    getContactDetail(model,record);
		    contactpopup.getPopup().addStyleName("dateBoxPopup");
	    contactpopup.showPopUp();
        	
        }   
	}


	/**
	 * Date : 21-03-2017 By Anil
	 * this method checks service complete
	 */
	private boolean validateCompletion() {
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
			System.out.println("Config true");
			if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
				System.out.println("Not an admin");
				DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
				Date serviceDate=model.getServiceDate();
				Date lastValidDate=CalendarUtil.copyDate(serviceDate);
				Console.log("Service Date : "+format.format(serviceDate));
				System.out.println("Service Date : "+format.format(serviceDate));
				if(serviceDate!=null){
					CalendarUtil.addMonthsToDate(lastValidDate, 1);
					lastValidDate.setDate(5);
					Console.log("Valid/Deadline date "+format.format(lastValidDate));
					System.out.println("Valid/Deadline date "+format.format(lastValidDate));
					Date todaysDate=new Date();
					lastValidDate=format.parse(format.format(lastValidDate));
					todaysDate=format.parse(format.format(todaysDate));
					Console.log("Todays Date : "+format.format(todaysDate));
					System.out.println("Todays Date : "+format.format(todaysDate));
					if(todaysDate.after(lastValidDate)){
						showDialogMessage("Service completion of previous month is not allowed..!");
						return false;
					}
				}else{
					showDialogMessage("Service date is null");
					return false;
				}
			}
			return true;
		}else{
			System.out.println("Config false");
			return true;
		}
	}
	//SmartGWT Click event

	@Override
	public void onClick(ClickEvent event) {

	if(event.getSource()==planPopUp.btnOk){
			
			if(ValidatePopupData()){
				
				final ArrayList<EmployeeInfo> technicianlist = new ArrayList<EmployeeInfo>();
				technicianlist.addAll(planPopUp.technicianTable.getDataprovider().getList());
				
				final String TechnicianName = getTechnicianName(planPopUp.technicianTable.getDataprovider().getList());

				final ArrayList<CompanyAsset> technicianToollist = new ArrayList<CompanyAsset>();
				technicianToollist.addAll(planPopUp.toolTable.getDataprovider().getList());
				
				final ArrayList<ProductGroupList> materialInfo = new ArrayList<ProductGroupList>();
				materialInfo.addAll(planPopUp.materialProductTable.getDataprovider().getList());
				
				
				DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
				selectedRecord.setAttribute("ServiceDate", format.format(planPopUp.dbserviceDate.getValueAsDate()));
				
				String serviceHrs=planPopUp.servicetime.getItemText(planPopUp.servicetime.getSelectedIndex());
				String serviceMin=planPopUp.p_servicemin.getItemText(planPopUp.p_servicemin.getSelectedIndex());
				String serviceAmPm=planPopUp.p_ampm.getItemText(planPopUp.p_ampm.getSelectedIndex());
				String totalTime = "";
				final String calculatedServiceTime;
				
				totalTime = serviceHrs+":"+serviceMin+""+serviceAmPm;
				if(totalTime.equalsIgnoreCase("--:----")){
					calculatedServiceTime = "Flexible";
				}else{
					calculatedServiceTime = totalTime;
				}
				
				selectedRecord.setAttribute("ServiceTime", calculatedServiceTime);
				

				final int InvoiceId ;
				if(planPopUp.getOlbInvoiceId().getSelectedIndex()!=0){
					InvoiceId  = Integer.parseInt(planPopUp.getOlbInvoiceId().getValue(planPopUp.getOlbInvoiceId().getSelectedIndex()));
					selectedRecord.setAttribute("InvoiceId", InvoiceId);
				}else{
					InvoiceId = 0;
				}
				final String invoiceDate;
				if(planPopUp.getOlbInvoiceDate().getSelectedIndex()!=0){
					invoiceDate = planPopUp.getOlbInvoiceDate().getValue(planPopUp.getOlbInvoiceDate().getSelectedIndex());
					selectedRecord.setAttribute("InvoiceDate", invoiceDate);
                 }else{
                	 invoiceDate = null;
                 }
				final int projectId = selectedRecord.getAttributeAsInt("ProjectId");
				System.out.println("Project Id=="+projectId);
				
				if(planPopUp.getTbOther().getValue()!=null&&!planPopUp.getTbOther().getValue().equals("")){
					model.setTechPlanOtherInfo(planPopUp.getTbOther().getValue());
				}
				
				System.out.println("flag value :" + technianValidateflag);
				
					System.out.println("all komal : " + technicianlist.size() +" "+ planPopUp.dbserviceDate.getValueAsDate() +" " + calculatedServiceTime);
					ArrayList<String> statuslist = new ArrayList<String>();
					statuslist.add("Scheduled");
//					statuslist.add("Completed");
					statuslist.add("Rescheduled");
				
					// plus seven days to date
					Date date = new Date();
					
					// add 30 days to date
					Date addThirtyDate = new Date();
					CalendarUtil.addDaysToDate(addThirtyDate, 15);
					
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter filter = null;
					MyQuerry querry;
					
					filter = new Filter();
					filter.setQuerryString("companyId");
					filter.setLongValue(UserConfiguration.getCompanyId());
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("status IN");
					filter.setList(statuslist);
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("serviceDate >=");
					filter.setDateValue(date);
					filtervec.add(filter);
					
					filter = new Filter();
					filter.setQuerryString("serviceDate <=");
					filter.setDateValue(addThirtyDate);
					filtervec.add(filter);
					
					querry=new MyQuerry();
					querry.setQuerryObject(new Service());
					querry.setFilters(filtervec);
					
					genservice.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							System.out.println("RESULT SIZE : "+result.size());		
							label : for(SuperModel model:result){
								Service customerEntity=(Service) model;	
								if(!(calculatedServiceTime.equalsIgnoreCase("Flexible")))
								{
								for(EmployeeInfo emp : technicianlist){
									System.out.println("All values komal 1:" + emp.getFullName() +" " +customerEntity.getEmployee() +" "+format1.format(planPopUp.dbserviceDate.getValueAsDate())+" "+format1.format(customerEntity.getServiceDate())  +" " +calculatedServiceTime +" " +customerEntity.getServiceTime());
									
									if(emp.getFullName().equals(customerEntity.getEmployee()) && format1.format(planPopUp.dbserviceDate.getValueAsDate()).equals(format1.format(customerEntity.getServiceDate())) && calculatedServiceTime.equals(customerEntity.getServiceTime())){
										System.out.println("All values komal 2:" + emp.getFullName() +" " +customerEntity.getEmployee() +" "+format1.format(planPopUp.dbserviceDate.getValueAsDate())+" "+format1.format(customerEntity.getServiceDate()) +" " +calculatedServiceTime +" " +customerEntity.getServiceTime());
										technianValidateflag = false;
										break label;
										
									}
									}
								}
								
							}
							
							System.out.println("Flag value :" + technianValidateflag);
							if (technianValidateflag == false) {
								showDialogMessage("Selected technician is already assigned to other service at inserted date and time");
								technianValidateflag = true;
								} else {
							    technicianPlanInfoSync.TechnicianSchedulingPlan(model,technicianlist,technicianToollist,materialInfo,planPopUp.dbserviceDate.getValueAsDate(),calculatedServiceTime,InvoiceId,invoiceDate,TechnicianName,projectId, new AsyncCallback<Integer>() {
								
								@Override
								public void onSuccess(Integer result) {
									// TODO Auto-generated method stub
									System.out.println("iddddiiddd - - "+result);
									selectedRecord.setAttribute("ProjectId", result);
									form.grid.redraw();
									showDialogMessage("Saved Succesfully");
									planPopUp.hidePopUp();
									
								}
				
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									planPopUp.hidePopUp();
								}
							});
							
						}
						}
						
						@Override
						public void onFailure(Throwable caught) {
							System.out.println("Failed");
							Console.log("RPC Failed");
						}
					});

				
			}
			
			
		}
		
		System.out.println("Hi vijay on click === plan complete");
		// plan popup complete button
		if(event.getSource() == planPopUp.getBtnComplete()){
			System.out.println("plan popup btn complete");
			
			  if(validateCompletion()){
					Date todayDate=new Date();
					if(todayDate.before(model.getServiceDate())){
						System.out.println("Service Date Exceeds");
						showDialogMessage("Service Date exceeds current date. Please reschedule the date");
						
					}else{
						ShowCompleteServicePopUp();
						
					}
				}
		}
		/**
		 * Updated By: Viraj
		 * Date: 03-01-2019
		 * Description: Since it is normal button commented code in onclick of smartGWT.  
		 */
		// plan poup rechedule button
//		if(event.getSource() == planPopUp.btnReschedulePopUp){
//			if(validateRechedule()){
//				selectedRecord.setAttribute("Status", "Rescheduled");
//				
//				/** Date 22-03-2018 By vijay for formated Date properly **/
//				DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
//				selectedRecord.setAttribute("ServiceDate", format.format(planPopUp.dbserviceDate.getValueAsDate()));
//					
//				
//				String serviceHrs =  planPopUp.getServicetime().getValue(planPopUp.servicetime.getSelectedIndex());
//				String serviceMin = planPopUp.getP_servicemin().getValue(planPopUp.getP_servicemin().getSelectedIndex());
//				String serviceAmPm = planPopUp.getP_ampm().getValue(planPopUp.getP_ampm().getSelectedIndex());
//				String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
//				
//				selectedRecord.setAttribute("ServiceTime", calculatedServiceTime);
//				saveReschedule(model,planPopUp.tbRecheduleReason.getValue(),planPopUp.dbserviceDate.getValueAsDate(),calculatedServiceTime,null );
//				
//				form.grid.redraw();
//				showDialogMessage("Service Rescheduled Successfully");
//				planPopUp.olbSpecificReason.setSelectedIndex(0);
//				planPopUp.tbRecheduleReason.setValue("");
//				/** date 11/11/2017 added by komal to hide popup after rescheduling through plan popup **/
//				planPopUp.hidePopUp();
//			}
//		}
		/** Ends **/
		//complete service popup
		if(event.getSource() == completeServicePopUp.getBtnOk()){
			
			if(completeServicePopUp.getDbserviceCompletionDate().getValueAsDate()==null){
				showDialogMessage("Please add Completetion Date");
			}
			else if(model.isRateContractService()){
				if(completeServicePopUp.getDbserviceQuantity().getValue()==null){
					showDialogMessage("Please add Quantity");
				}
				else if(completeServicePopUp.getOlbUnitOfMeasurement().getSelectedIndex()==0){
					showDialogMessage("Please Select Unit Of Measurement ");
				}
				
			}
			else{
				ReactOnServiceCompletionPopUp(event);
			}
			
		}
		if(event.getSource() == completeServicePopUp.getBtnCancel()){
			planPopUp.hidePopUp();
			completeServicePopUp.hidePopUp();
		}
		
		
		// Reschedule popup buttons click
				if(event.getSource() == reschedulePopup.getBtnReschedule()){
					if(validateReschedulePopup()){
						
						selectedRecord.setAttribute("Status", "Rescheduled");
						
						/** Date 22-03-2018 By vijay for formated Date properly **/
						DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
						selectedRecord.setAttribute("ServiceDate", format.format(reschedulePopup.dbserviceDate.getValueAsDate()));
					
						String serviceHrs =  reschedulePopup.getServicetime().getValue(reschedulePopup.servicetime.getSelectedIndex());
						String serviceMin = reschedulePopup.getP_servicemin().getValue(reschedulePopup.getP_servicemin().getSelectedIndex());
						String serviceAmPm = reschedulePopup.getP_ampm().getValue(reschedulePopup.getP_ampm().getSelectedIndex());
						String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
						
						selectedRecord.setAttribute("ServiceTime", calculatedServiceTime);
						String servicingBranch = null;
						if(reschedulePopup.p_seviceBranch.getSelectedIndex()!=0)
							servicingBranch = reschedulePopup.p_seviceBranch.getValue(reschedulePopup.p_seviceBranch.getSelectedIndex());
						
						saveReschedule(model,reschedulePopup.tbReason.getValue(),reschedulePopup.dbserviceDate.getValueAsDate(),calculatedServiceTime,servicingBranch);
						
						form.grid.redraw();
						showDialogMessage("Service Rescheduled Successfully");
						reschedulePopup.rescheduleWinModal.hide();
					}
				}
				if(event.getSource() == reschedulePopup.getBtnReshceduleCancel()){
					reschedulePopup.rescheduleWinModal.hide();
				}
				
				
				/*
				 * Date:11/08/2018
				 * Added by Ashwini
				 */
				if(event.getSource() == contactpopup.getLblOk() ){
					
					contactpopup.hidePopUp();
				}
			
					/*
					 * End by Ashwini
					 */
		
	}

	
	private boolean validateReschedulePopup() {

		if(reschedulePopup.getTbReason().getValue().equals("")){
			showDialogMessage("Reason for change Can not be blank !");
			return false;
		}
		
		return true;
	}
	
	/** Service wise complete button popup **************/
	private void ShowCompleteServicePopUp() {
		// TODO Auto-generated method stub
		completeServicePopUp.getLbRating().setSelectedIndex(0);
		completeServicePopUp.getServicetime().setSelectedIndex(0);
		completeServicePopUp.getP_servicemin().setSelectedIndex(0);
		completeServicePopUp.getP_ampm().setSelectedIndex(0);
		completeServicePopUp.getTo_servicehours().setSelectedIndex(0);
		completeServicePopUp.getTo_servicemin().setSelectedIndex(0);
		completeServicePopUp.getTo_ampm().setSelectedIndex(0);
		completeServicePopUp.getDbserviceCompletionDate().setValue(new Date());
		completeServicePopUp.getTbRemark().setValue("");
		/** date 15/11/2017 added by komal to clear previous value of service duration **/
		completeServicePopUp.getIbserviceDuration().setValue(0);
		completeServicePopUp.showPopUp();
		
		addDefaultValueInMarkCompletePopUp(completeServicePopUp);
	}
	
	
	private void addDefaultValueInMarkCompletePopUp(ServiceCompletetionPopup markcompletepopup2) {
		
		Console.log("Inside addDefaultValue");
		Console.log("model.getServiceCompleteDuration() :::"+model.getServiceCompleteDuration());
		Console.log("model.getServiceCompletionDate():::"+model.getServiceCompletionDate());
		Console.log("model.getServiceCompleteRemark():::"+model.getServiceCompleteRemark());

		if (model.getServiceCompleteDuration() != 0)
			markcompletepopup2.getIbserviceDuration().setValue(
					model.getServiceCompleteDuration());

		if (model.getServiceCompletionDate() != null) {
			DateTimeFormat sdf = DateTimeFormat.getFormat("dd/MM/yyyy");

			markcompletepopup2.getDbserviceCompletionDate().setValue(
					sdf.parse(sdf.format(model.getServiceCompletionDate())));

		}

		if (model.getServiceCompleteRemark() != null
				&& !model.getServiceCompleteRemark().equalsIgnoreCase("")) {
			markcompletepopup2.getTbRemark().setValue(
					model.getServiceCompleteRemark().trim());
		}

		if (model.getCustomerFeedback() != null
				&& !model.getCustomerFeedback().equalsIgnoreCase("")) {
			int index = 0;
			for (int i = 0; i < markcompletepopup2.getLbRating().getItemCount(); i++) {
				if (model
						.getCustomerFeedback()
						.trim()
						.equalsIgnoreCase(
								markcompletepopup2.getLbRating().getValue(i))) {
					index = i;
				}
			}
			
			Console.log("index:::"+index);
			markcompletepopup2.getLbRating().setSelectedIndex(index);
		}
		
	}

	private void getProjectDetails(final Service model, final ListGridRecord record) {

		final int contractId = model.getContractCount();
		
		 MyQuerry querry = new MyQuerry();
		 Vector<Filter> filterVec = new Vector<Filter>();
		 
		 Filter filter =null;
		 filter=new Filter();
		 filter.setIntValue(model.getCount());
		 filter.setQuerryString("serviceId");
		 filterVec.add(filter);
		 System.out.println("proj =="+record.getAttribute("ProjectId"));
		 int projectId = Integer.parseInt(record.getAttribute("ProjectId"));
		 if(projectId!=0){
			 System.out.println("PROJECTttt");
			 filter=new Filter();
			 filter.setIntValue(projectId);
			 filter.setQuerryString("count");
			 filterVec.add(filter);
		 }
		 
		 querry.setFilters(filterVec);
		 querry.setQuerryObject(new ServiceProject());
		 
		 genservice.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("result ="+result.size());
				
				loadInvoiceDropDown(contractId);
				
				planPopUp.technicianTable.getDataprovider().getList().clear();
				planPopUp.toolTable.getDataprovider().getList().clear();
				planPopUp.materialProductTable.getDataprovider().getList().clear();
				planPopUp.dbQuantity.setValue(0d);
				if(record.getAttribute("Status").equals("Completed")){
					planPopUp.SetEnableWidgets(false);
				}else{
					planPopUp.SetEnableWidgets(true);
				}
				
				for(SuperModel model :result){
					ServiceProject project = (ServiceProject) model;
					planPopUp.technicianTable.getDataprovider().setList(project.getTechnicians());
					planPopUp.toolTable.getDataprovider().setList(project.getTooltable());
					planPopUp.materialProductTable.getDataprovider().setList(project.getProdDetailsList());
					
					MaterialProductTable.projectId=project.getCount();
				}
				planPopUp.dbserviceDate.setValue(model.getServiceDate());
//				setValue(model.getServiceTime(),planPopUp.servicetime);
				
				setpopupServiceTime(selectedRecord.getAttribute("ServiceTime"));
				
				if(model.getTechPlanOtherInfo()!=null)
				planPopUp.getTbOther().setValue(model.getTechPlanOtherInfo());;
				
			  // get invoice id and date from table
//				if(invoiceId!=0){
//					planPopUp.getOlbInvoiceId().setValue(invoiceId+"");
//				}else{
//					planPopUp.getOlbInvoiceId().setSelectedIndex(0);
//				}
//				if(invoiceDate!=null){
//					planPopUp.getOlbInvoiceDate().setValue(DateTimeFormat.getShortDateFormat().format(invoiceDate));	
//				}else{
//					planPopUp.getOlbInvoiceDate().setSelectedIndex(0);
//				}
	
				/**
				 * Date 22-03-2018 by vijay 
				 * for if condition if there is no plan and technician name exist in service then technician name adding to plan popup by default 
				 */
				System.out.println("result.size() =="+result.size());
				if(result.size()==0){
					String technicianName = record.getAttribute("Technician");
					getTechnicianInfo(planPopUp,technicianName);
				}else{
					planPopUp.showPopUp();

				}
			}
			
		


			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				SC.say("Unexpected error occurred");
			}
		});
		 
	}
	
	
	/*
	 * Added by Ashwini
	 * Date:13/08/2018
	 * Des:To add contact detail in Technician Scheduling list
	 */
	private void getContactDetail(final Service model, final ListGridRecord record) {
		
		Service object = new Service();
				 MyQuerry querry = new MyQuerry();
				 Vector<Filter> filterVec = new Vector<Filter>();
				 
				 Filter filter =null;
				 filter=new Filter();
				 filter.setIntValue(model.getPersonInfo().getCount());
				 System.out.println ("Count for contact Detail"+ model.getPersonInfo().getCount());
				 filter.setQuerryString("count");
				 filterVec.add(filter);
				 querry.setFilters(filterVec);
				 querry.setQuerryObject(new Customer());
				 service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						System.out.println("contact result==" +result.size());
						contactpopup.getPopup().addStyleName("dateBoxPopup");
						contactpopup.showPopUp();
						for(SuperModel model : result){
							Customer customer = (Customer) model;
							contactpopup.getLandLine().setValue(customer.getLandline()+"");
							contactpopup.getCellNumber2().setValue(customer.getCellNumber2()+"");
							
							contactpopup.getCellNumber1().setValue(customer.getCellNumber1()+"");
							contactpopup.getEmail().setValue(customer.getEmail()+"");
							contactpopup.setEnable(false);
							contactpopup.getLblCancel().setVisible(false);
							break;
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						contactpopup.showPopUp();
					}
				});
	}	 

	/*
	 * End by Ashwini
	 */

	
	/**
	 * Date 22-03-2018 By vijay For Technician adding to Plan By default 
	 * @param planPopUp
	 * @param technicianName
	 */

	private void getTechnicianInfo(final TechnicianPlanPopUp planPopUp, String technicianName) {
		// TODO Auto-generated method stub
		
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter =null;
		
		filter= new Filter();
		filter.setQuerryString("fullname");
		filter.setStringValue(technicianName);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Employee());
		
		genservice.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("result.size() === =="+result.size());
				if(result.size()!=0){
					ArrayList<EmployeeInfo> technicianlist = new ArrayList<EmployeeInfo>();
					for(SuperModel model : result){
						Employee empEntity = (Employee) model;
						EmployeeInfo empInfoEntity = new EmployeeInfo();
				  		empInfoEntity.setEmpCount(empEntity.getCount());
				  		empInfoEntity.setFullName(empEntity.getFullName());
				      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
				      	empInfoEntity.setDesignation(empEntity.getDesignation());
				      	empInfoEntity.setDepartment(empEntity.getDepartMent());
				      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
				      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
				      	empInfoEntity.setBranch(empEntity.getBranchName());
				      	empInfoEntity.setCountry(empEntity.getCountry());
				      	technicianlist.add(empInfoEntity);
				      	break;
					}
					if(technicianlist.size()!=0)
					planPopUp.technicianTable.getDataprovider().setList(technicianlist);

				}
				planPopUp.showPopUp();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				planPopUp.showPopUp();
			}
		});
	}
	
	private void loadInvoiceDropDown(int contractId) {

		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter =null;
		
		filter= new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(contractId);
		filtervec.add(filter);
		
		filter= new Filter();
		filter.setQuerryString("status");
		filter.setStringValue("Approved");
		filtervec.add(filter); 
		
		filter= new Filter();
		filter.setQuerryString("typeOfOrder");
		filter.setStringValue(AppConstants.ORDERTYPESERVICE);
		filtervec.add(filter);
		
		filter= new Filter();
		filter.setQuerryString("accountType");
		filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAR);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Invoice());
		
		planPopUp.olbInvoiceId.removeAllItems();
		planPopUp.olbInvoiceDate.removeAllItems();
		
		genservice.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				ArrayList<Invoice> list = new ArrayList<Invoice>();
				Console.log("Invoice size=="+result.size());
				
				planPopUp.olbInvoiceId.addItem("--SELECT--");
				planPopUp.olbInvoiceDate.addItem("--SELECT--");
				for(SuperModel model : result){
					Invoice invoice = (Invoice) model;
					planPopUp.olbInvoiceId.addItem(invoice.getCount()+"");
					planPopUp.olbInvoiceDate.addItem(DateTimeFormat.getShortDateFormat().format(invoice.getInvoiceDate()));
					list.add(invoice);
				}
				planPopUp.olbInvoiceId.setItems(list);
				planPopUp.olbInvoiceDate.setItems(list);
				
				System.out.println("INVOICE IDDDDD ="+selectedRecord.getAttributeAsInt("InvoiceId"));
				System.out.println("INVOICE DATEEEE ="+selectedRecord.getAttributeAsDate("InvoiceDate"));

				int invoiceId = selectedRecord.getAttributeAsInt("InvoiceId");
				Date invoiceDate = selectedRecord.getAttributeAsDate("InvoiceDate");
				
				setpopUpInvoiceIdDate(invoiceId,invoiceDate);
			}
			
		

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
//				SC.say("Unexpected Error");
			}
		});
	}
	
	
	private void setpopUpInvoiceIdDate(int invoiceId, Date invoiceDate) {
		// TODO Auto-generated method stub
		int invoiceIdcount  = planPopUp.getOlbInvoiceId().getItemCount();
		String invoiceid = invoiceId+"";
		if(invoiceId!=0){
			for(int i=0;i<invoiceIdcount;i++){
				if(invoiceid.trim().equals(planPopUp.getOlbInvoiceId().getItemText(i))){
					planPopUp.getOlbInvoiceId().setSelectedIndex(i);
					break;
				}
			}
		}
		
		int invoiceIDDateCount = planPopUp.getOlbInvoiceDate().getItemCount();
		
		if(invoiceDate!=null){
			String invoiceIdDate = DateTimeFormat.getShortDateFormat().format(invoiceDate);
			for(int i=0;i<invoiceIDDateCount;i++){
				if(invoiceIdDate.trim().equals(planPopUp.getOlbInvoiceDate().getItemText(i))){
					planPopUp.getOlbInvoiceDate().setSelectedIndex(i);
					break;
				}
			}
		}
	}
	
	private void setpopupServiceTime(String serviceTime) {

		if(!serviceTime.equals("")&&!serviceTime.equals("Flexible")){
			
			String[] time=serviceTime.split(":");
			System.out.println(" 0 "+time[0]);
			System.out.println(" 1 "+time[1]);
			String hrs=time[0];
			String min;
			String ampm;
			if(time[1].contains("PM")){
				min=time[1].replace("PM", "");
				ampm="PM";
			}else{
				min=time[1].replace("AM", "");
				ampm="AM";
			}
			System.out.println(" 2 "+hrs+" "+min+" "+ampm);
			
			int count = planPopUp.servicetime.getItemCount();
			String item=hrs.toString();
			System.out.println("Hours =="+item);
			for(int i=0;i<count;i++)
			{
				System.out.println("HTRRSSS =="+planPopUp.servicetime.getItemText(i).trim());
				System.out.println(item.trim().equals(planPopUp.servicetime.getItemText(i).trim()));
				if(item.trim().equals(planPopUp.servicetime.getItemText(i).trim()))
				{
					planPopUp.servicetime.setSelectedIndex(i);
					break;
				}
			}
			
			int count1 = planPopUp.p_servicemin.getItemCount();
			String item1=min.toString();
			for(int i=0;i<count1;i++)
			{
				if(item1.trim().equals(planPopUp.p_servicemin.getItemText(i).trim()))
				{
					planPopUp.p_servicemin.setSelectedIndex(i);
					break;
				}
			}
			
			int count2 = planPopUp.p_ampm.getItemCount();
			String item2=ampm.toString();
			for(int i=0;i<count2;i++)
			{
				if(item2.trim().equals(planPopUp.p_ampm.getItemText(i).trim()))
				{
					planPopUp.p_ampm.setSelectedIndex(i);
					break;
				}
			}
			
		}else{
			planPopUp.servicetime.setSelectedIndex(0);
			planPopUp.p_servicemin.setSelectedIndex(0);
			planPopUp.p_ampm.setSelectedIndex(0);
		}
	}
	
	
	/**** Smart GWT dialog message popup *****************/
	public static void showDialogMessage(String Message)
	{
		SC.say(Message);
	}

	@Override
	public void onChange(ChangeEvent event) {

	}

	@Override
	public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
		// TODO Auto-generated method stub
		System.out.println("App menu click");
		/**
		 * Updated By: Viraj
		 * Date: 27-12-2018
		 * Description: To add the functionalities of customer service list
		 */
		if (event.getSource() == form.btnCancelServ) {
			if(validSelectedRecord()){
				panel=new PopupPanel(true);
//				cancellationPopup.clear();
				cancellationPopup.getPaymentDate().setValue(AppUtility.parseDate(new Date())+"");
				panel.add(cancellationPopup);
				panel.center();
				panel.addStyleName("gwt-SuggestBoxPopup");
				panel.show();
			}
		}
		
		if(event.getSource()==cancellationPopup.getBtnOk()){
			System.out.println("POPUP OK CLICK");
			cancelServices(getAllSelectedRecords());
			panel.hide();
			
		}
		
		if(event.getSource().equals(cancellationPopup.getBtnCancel())){
			System.out.println("POPUP CANCEL CLICK");
			panel.hide();
		}
		
		if(event.getSource() == form.btnSuspendServ){
//			form.showWaitSymbol();
			ArrayList<Service> selectedlist = getAllSelectedRecords();
			
			if(validSelectedRecord()){
				ArrayList<SuperModel> servicelist=new ArrayList<SuperModel>();
				for(Service service:selectedlist){
					if(service.getStatus().equals(Service.SERVICESTATUSSCHEDULE)||
							service.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)){
						service.setStatus(Service.SERVICESUSPENDED);
						
						}
					servicelist.add(service);
				}
			
				genservice.save(servicelist, new AsyncCallback<ArrayList<ReturnFromServer>>() {

						@Override
						public void onFailure(Throwable caught) {
//							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(ArrayList<ReturnFromServer> result) {
							
//							form.hideWaitSymbol();
							DeviceForm deviceform=new DeviceForm();
							deviceform.tbStatus.setValue(model.getStatus());
							try {
								
								form.showDialogMessage("Services suspend Succesfully");
								form.grid.redraw();
							} catch (Exception e2) {
								System.out.println(e2);
							}
							
						}

										});
				}
			
//			form.hideWaitSymbol();
			}
		
		if(event.getSource() == form.btnResumeServ) {
//			form.showWaitSymbol();
			
			ArrayList<Service> selectedlist = getAllSelectedRecords();
			
			if(validSelectedRecordForSuspend()){
				ArrayList<SuperModel> servicelist=new ArrayList<SuperModel>();
				for(Service service:selectedlist){
					if(service.getStatus().equals(Service.SERVICESUSPENDED)){
						service.setStatus(Service.SERVICESTATUSRESCHEDULE);
						}
					
					servicelist.add(service);
				}
				
				
				genservice.save(servicelist, new AsyncCallback<ArrayList<ReturnFromServer>>() {

					@Override
					public void onFailure(Throwable caught) {
						
					}

					@Override
					public void onSuccess(ArrayList<ReturnFromServer> result) {
						
//						form.hideWaitSymbol();
						DeviceForm deviceform=new DeviceForm();
						deviceform.tbStatus.setValue(model.getStatus());
						try {
							
							form.showDialogMessage("Services Reschedule Succesfully");
							form.grid.redraw();
						} catch (Exception e2) {
							System.out.println(e2);
						}
						
					}

									});
			}
//			form.hideWaitSymbol();
		}
		
		if(event.getSource() == form.btnViewContract) {
			ArrayList<Service> selectedList = getAllSelectedRecords();
			if(selectedList.size()==0){
				form.showDialogMessage("Please select record!");
				return;
			}
			if(selectedList.size()>=2){
				form.showDialogMessage("Please select only one record!");
				return;
			}
			reactOnViewContract(selectedList);
		}
		
		if(event.getSource() == form.btnViewDoc) {
			ListGridRecord[] record = form.grid.getSelection();
			if(record.length == 1){
				System.out.println("NOT NULLLLLL");
				viewSelectedDocument();
			}
			else{
				form.showDialogMessage("Please select atleast one record!");
			}
		}
		
		if(event.getSource() == form.btnMarkComplete){
			ArrayList<Service> selectedlist = getAllSelectedRecords();
			if(selectedlist.size()==0){
				form.showDialogMessage("Please select records!");
				return;
			}
			for(int i=0;i<selectedlist.size();i++){
				if(selectedlist.get(i).getStatus().equalsIgnoreCase("Completed") 
						|| selectedlist.get(i).getStatus().equalsIgnoreCase(Service.CANCELLED)
						|| selectedlist.get(i).getStatus().equalsIgnoreCase(Service.SERVICESUSPENDED)){
					form.showDialogMessage("Please select Scheduled and Rescheduled Services only!");
					return;
				}
			}
			serviceCompletionPopup.getCbServiceDateAsServiceCompletionDate().setValue(false);
			serviceCompletionPopup.getPopup().addStyleName("gwt-SuggestBoxPopup");
			serviceCompletionPopup.showPopUp();
		}
		
		if(event.getSource()==serviceCompletionPopup.getLblOk()){
			final ArrayList<Service> selectedlist = getAllSelectedRecords();
			for(Service service : selectedlist){
				if(service.getServiceDate().after(new Date())){
					form.showDialogMessage("Service Date exceeds current date. Please reschedule the date");
					serviceCompletionPopup.hidePopUp();
					return;
				}
			}
			System.out.println("serviceCompletionPopup.getCbServiceDateAsServiceCompletionDate().getValue() =="+serviceCompletionPopup.getDbServiceCompletionDate().getValue());
			if(serviceCompletionPopup.getCbServiceDateAsServiceCompletionDate().getValue()==true && serviceCompletionPopup.getDbServiceCompletionDate().getValue()!=null
				|| serviceCompletionPopup.getCbServiceDateAsServiceCompletionDate().getValue()==false && serviceCompletionPopup.getDbServiceCompletionDate().getValue() == null ){
				form.showDialogMessage("Please select Either Service Completion Date OR Service Date As Service completion Date");
			}
			else if(serviceCompletionPopup.getTaRemark().getValue().equals("") ){
				form.showDialogMessage("Please add Remark!");
			}
			else{
				
				final String remark = serviceCompletionPopup.getTaRemark().getValue();
				Date serviceCompletionDate = null;
				if(serviceCompletionPopup.getDbServiceCompletionDate().getValue()!=null){
					 serviceCompletionDate = serviceCompletionPopup.getDbServiceCompletionDate().getValue();
				}
				final Date completionDate = serviceCompletionDate;
//				form.showWaitSymbol();
				servicelistserviceAsync.markCompleteServicesWithBulk(selectedlist, remark, serviceCompletionDate, new AsyncCallback<Integer>() {
					
					@Override
					public void onSuccess(Integer result) {
						// TODO Auto-generated method stub
						if(result==1){
							ArrayList<Service> list = getAllSelectedRecords();
							for(int i=0;i<selectedlist.size();i++){
								for(int j=0;j<list.size();j++){
									if(selectedlist.get(i).getCount() == list.get(j).getCount()){
										list.get(j).setStatus("Completed");
										list.get(j).setServiceCompleteRemark(remark);
										if(completionDate!=null){
											list.get(j).setServiceCompletionDate(completionDate);
										}
										else{
											list.get(j).setServiceCompletionDate(list.get(j).getServiceDate());
										}
										/*** Date 1-12-2018 By Vijay for selected records set false for untick **/
										list.get(j).setRecordSelect(false);

									}
								}
								
							}
							form.showDialogMessage("Services Mark Completed successfully!");
//							form.getSuperTable().getDataprovider().setList(list);
							form.setDataToListGrid(globalServicelist);
							serviceCompletionPopup.hidePopUp();
							form.grid.redraw();
							
						}else if(result==-1){
							form.showDialogMessage("Please try after 5 minutes! Last task is InProcess");
							serviceCompletionPopup.hidePopUp();

						}
						/**
						 * @author Vijay Chougule
						 * Date 30-11-2018
						 * Des :- if service wise billing process config is active then only allow
						 * to complete services with bulk of 100 only not more than this validation
						 */
						else if(result==-2){
							form.showDialogMessage("Please select services less than 100 ");
						}
//						form.hideWaitSymbol();
						serviceCompletionPopup.hidePopUp();

					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
//						form.hideWaitSymbol();

					}
				});
			}
			
		}
		
		if(event.getSource()==serviceCompletionPopup.getLblCancel()){
			serviceCompletionPopup.hidePopUp();
		}
		
		if(event.getSource() == form.btnAssignTech){
			ArrayList<Service> selectedlist = getAllSelectedRecords();
			if(selectedlist.size()==0){
				form.showDialogMessage("Please select records!");
				return;
			}
			for(int i=0;i<selectedlist.size();i++){
				if(!selectedlist.get(i).getStatus().equalsIgnoreCase("Scheduled") 
						&& !selectedlist.get(i).getStatus().equalsIgnoreCase("Rescheduled") ){
					
					form.showDialogMessage("Please select Scheduled and Rescheduled Services only!");
					return;
				}
			}
			serviceTechnicianPopup.getPopup().addStyleName("gwt-SuggestBoxPopup");
			serviceTechnicianPopup.showPopUp();
		}
		
		if(event.getSource()== serviceTechnicianPopup.getLblOk()){
			if(serviceTechnicianPopup.getOlbEmployee().getSelectedIndex()!=0){
				setTechnicianToServices();
			}else{
				form.showDialogMessage("Please Select Technician.");
			}
		}
		
		if(event.getSource()== serviceTechnicianPopup.getLblCancel()){
			serviceTechnicianPopup.hidePopUp();
		}
		
		if(event.getSource() == form.btnReschedule) {
			ArrayList<Service> selectedlist = getAllSelectedRecords();
			if(selectedlist.size()==0){
				form.showDialogMessage("Please select records!");
				return;
			}
			if(selectedlist.size()>=200){
				form.showDialogMessage("Please select Less than 200 records only!");
				return;
			}
			for(int i=0;i<selectedlist.size();i++){
				if(selectedlist.get(i).getStatus().equalsIgnoreCase("Completed")){
					form.showDialogMessage("Please select Scheduled and Rescheduled Services only!");
					return;
				}
			}
			reactOnReschedule();
		}
		
		if(event.getSource()==reschedule.getBtnReschedule()) {
			boolean val=validateReschedule();
			if(val==true)
			{
				ArrayList<Service> selectedlist = getAllSelectedRecords();
				ArrayList<SuperModel> servicelist = new ArrayList<SuperModel>();
				for(Service service:selectedlist){
				ModificationHistory history=new ModificationHistory();
				history.oldServiceDate=service.getServiceDate();
				history.resheduleDate=reschedule.getDateBox().getValue();
				
				String serviceHrs=reschedule.getP_servicehours().getItemText(reschedule.getP_servicehours().getSelectedIndex());
				String serviceMin=reschedule.getP_servicemin().getItemText(reschedule.getP_servicemin().getSelectedIndex());
				String serviceAmPm=reschedule.getP_ampm().getItemText(reschedule.getP_ampm().getSelectedIndex());
				String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
				if(serviceHrs.equals("--") || serviceMin.equals("--")){
					history.resheduleTime="Flexible";
				}else{
					history.resheduleTime=calculatedServiceTime;
				}
				history.reason=reschedule.getTextArea().getText();
				history.userName=LoginPresenter.loggedInUser;
				history.systemDate=new Date();
				Date reschDate=reschedule.getDateBox().getValue();
				service.getListHistory().add(history);
				service.setServiceDate(reschDate);
				
				servicelist.add(service);
				
				}
				genservice.save(servicelist, new AsyncCallback<ArrayList<ReturnFromServer>>() {
					
					@Override
					public void onSuccess(ArrayList<ReturnFromServer> result) {
						// TODO Auto-generated method stub
						
						ArrayList<Service> list = getAllSelectedRecords();
						ArrayList<Service> selectedlist = getAllSelectedRecords();
						for(int i=0;i<selectedlist.size();i++){
							for(int j=0;j<list.size();j++){
								if(selectedlist.get(i).getCount() == list.get(j).getCount()){
									list.get(j).setStatus("Rescheduled");
									list.get(j).setServiceDate(reschedule.getDateBox().getValue());
									String serviceHrs=reschedule.getP_servicehours().getItemText(reschedule.getP_servicehours().getSelectedIndex());
									String serviceMin=reschedule.getP_servicemin().getItemText(reschedule.getP_servicemin().getSelectedIndex());
									String serviceAmPm=reschedule.getP_ampm().getItemText(reschedule.getP_ampm().getSelectedIndex());
									String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
									if(serviceHrs.equals("--") || serviceMin.equals("--")){
										list.get(j).setServiceTime("Flexible");
									}else{
										list.get(j).setServiceTime(calculatedServiceTime);
									}
								}
							}
							
						}
					form.showDialogMessage("Services Rescheduled successfully!");
//					form.getSuperTable().getDataprovider().setList(list);
					form.setDataToListGrid(globalServicelist);
					 panel.hide();
//					 form.hideWaitSymbol();
					 form.grid.redraw();
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
			}
		}
		if(event.getSource()==reschedule.getBtnCancel()){
			panel.hide();
		}
		/**
		 * Updated By: Viraj
		 * Date: 03-01-2019
		 * Description: Since it is normal button commented code in onclick of smartGWT.  
		 */
		if(event.getSource() == planPopUp.btnReschedulePopUp){
			if(validateRechedule()){
				selectedRecord.setAttribute("Status", "Rescheduled");
				
				/** Date 22-03-2018 By vijay for formated Date properly **/
				DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
				selectedRecord.setAttribute("ServiceDate", format.format(planPopUp.dbserviceDate.getValueAsDate()));
					
				
				String serviceHrs =  planPopUp.getServicetime().getValue(planPopUp.servicetime.getSelectedIndex());
				String serviceMin = planPopUp.getP_servicemin().getValue(planPopUp.getP_servicemin().getSelectedIndex());
				String serviceAmPm = planPopUp.getP_ampm().getValue(planPopUp.getP_ampm().getSelectedIndex());
				String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
				
				selectedRecord.setAttribute("ServiceTime", calculatedServiceTime);
				saveReschedule(model,planPopUp.tbRecheduleReason.getValue(),planPopUp.dbserviceDate.getValueAsDate(),calculatedServiceTime,null );
				
				form.grid.redraw();
				showDialogMessage("Service Rescheduled Successfully");
				planPopUp.olbSpecificReason.setSelectedIndex(0);
				planPopUp.tbRecheduleReason.setValue("");
				/** date 11/11/2017 added by komal to hide popup after rescheduling through plan popup **/
				planPopUp.hidePopUp();
			}
		}
		
		/** Ends **/
		
		if(event.getSource() instanceof InlineLabel)
		{
			InlineLabel lbl= (InlineLabel) event.getSource();
			if(lbl.getText().equals("Print"))
			{
				System.out.println("Print click");
				reactOnPrint();
			}
			if(lbl.getText().equals("Search")){
				System.out.println("Serach click");
				reactOnSearch();
			}
			
			/** date 11/11/2017 added by komal for download button **/
			if(lbl.getText().equals("Download")){
				System.out.println("Download click");
				
				 getCustomerList(form.getGrid());
				 
			}
		}
		
		if(event.getSource() == searchPopup.getBtnGo()){
			System.out.println("Go btnn click");
			System.out.println(searchPopup.getIbContractId().getValue());
			if(validateSerachPopup()){
				reactOnGo();
			}
		}
		
		
		/*
		 * Added by Ashwini
		 */
		if(event.getSource() == contactpopup.getLblOk() ){
			
			System.out.println("Ashwini");
			contactpopup.hidePopUp();
		}
	
			/*
			 * end by Ashwini
			 */
	}
	
	
	
private void ReactOnServiceCompletionPopUp(ClickEvent event) {

		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
			
				if(completeServicePopUp.getIbserviceDuration().getValue()!=null){
					if(completeServicePopUp.getIbserviceDuration().getValue()!=0){
						
					
						String formTime = completeServicePopUp.getServicetime().getItemText(completeServicePopUp.getServicetime().getSelectedIndex())+":"+
								completeServicePopUp.getP_servicemin().getItemText(completeServicePopUp.getP_servicemin().getSelectedIndex())+":"+
								completeServicePopUp.getP_ampm().getItemText(completeServicePopUp.getP_ampm().getSelectedIndex());
						
						System.out.println("formTime form popup "+formTime);
						
						String toTime = completeServicePopUp.getTo_servicehours().getItemText(completeServicePopUp.getTo_servicehours().getSelectedIndex())+":"+
								completeServicePopUp.getTo_servicemin().getItemText(completeServicePopUp.getTo_servicemin().getSelectedIndex())+":"+
								completeServicePopUp.getTo_ampm().getItemText(completeServicePopUp.getTo_ampm().getSelectedIndex());
						System.out.println("toTime form popup "+toTime);
						
					/**
					 * Added By Rahul Verma
					 * This method is only for nbhc. and it will check whether there stock current status is>0 and they have service or not.
					 * 
					 */
					if(model.getEmployee().trim().equalsIgnoreCase("Snehal Abhishek Palav") && model.getStackDetailsList().size()!=0){
						createExtraServiceIfStackZero(model);
					}
					/**
					 * ends here 
					 */
					
					reactOnComplete(completeServicePopUp.getDbserviceCompletionDate().getValueAsDate(),formTime,toTime);
					
					// ********vaishnavi****************
					createProjectonMarkComplete();
				}else{
					showDialogMessage("Please add Service Duration (In Minutes)");
				}
		}else{
			showDialogMessage("Service Duration (In Minutes) is mandetory..!");
		}
		}
		else
		{
	
			String formTime = completeServicePopUp.getServicetime().getItemText(completeServicePopUp.getServicetime().getSelectedIndex())+":"+
					completeServicePopUp.getP_servicemin().getItemText(completeServicePopUp.getP_servicemin().getSelectedIndex())+":"+
					completeServicePopUp.getP_ampm().getItemText(completeServicePopUp.getP_ampm().getSelectedIndex());
			
			System.out.println("formTime form popup "+formTime);
			
			String toTime = completeServicePopUp.getTo_servicehours().getItemText(completeServicePopUp.getTo_servicehours().getSelectedIndex())+":"+
					completeServicePopUp.getTo_servicemin().getItemText(completeServicePopUp.getTo_servicemin().getSelectedIndex())+":"+
					completeServicePopUp.getTo_ampm().getItemText(completeServicePopUp.getTo_ampm().getSelectedIndex());
			System.out.println("toTime form popup "+toTime);
			
		
		/**
		 * Added By Rahul Verma
		 * This method is only for nbhc. and it will check whether there stock current status is>0 and they have service or not.
		 * 
		 */
		if(model.getEmployee().trim().equalsIgnoreCase("Snehal Abhishek Palav") && model.getStackDetailsList().size()!=0){
			createExtraServiceIfStackZero(model);
		}
		/**
		 * ends here 
		 */
		
		reactOnComplete(completeServicePopUp.getDbserviceCompletionDate().getValueAsDate(),formTime,toTime);
		
		// ********vaishnavi****************
		createProjectonMarkComplete();
		
	}

		
	if(event.getSource()==completeServicePopUp.getBtnCancel()){
		completeServicePopUp.hidePopUp();
	}

	}


		private void createProjectonMarkComplete() {
			Timer timer = new Timer() {
				@Override
				public void run() {
		
					projectAsync.createCustomerProject(model.getCompanyId(),
							model.getContractCount(), model.getCount(),model.getStatus(),
							new AsyncCallback<Integer>() {
		
								@Override
								public void onFailure(Throwable caught) {
									showDialogMessage("An Unexpected Error occured !");
								}
		
								@Override
								public void onSuccess(Integer result) {
									System.out.println("project created .......!!");
								}
							});
				}
			};
			timer.schedule(3000);
		}
	private void reactOnComplete(Date completionDt,String formTime,String toTime) {
		model.setStatus(Service.SERVICESTATUSCOMPLETED);
		System.out.println("in side if");
		model.setServiceCompletionDate(completionDt);
		
	if(completeServicePopUp.getTbRemark().getValue()!=null){
		model.setServiceCompleteRemark(completeServicePopUp.getTbRemark().getValue());
	}
	
	if(completeServicePopUp.getIbserviceDuration().getValue()!=null){
		model.setServiceCompleteDuration(completeServicePopUp.getIbserviceDuration().getValue());
	}
	
	if(!formTime.equals("")){
		model.setFromTime(formTime);
	}
	
	if(!toTime.equals("") ){
		model.setToTime(toTime);
	}
	
	if(completeServicePopUp.lbRating.getSelectedIndex()!=0){
		model.setCustomerFeedback(completeServicePopUp.lbRating.getValue(completeServicePopUp.lbRating.getSelectedIndex()));
	}
	
	if(completeServicePopUp.getDbserviceQuantity().getValue()!=null && completeServicePopUp.getDbserviceQuantity().getValue()!=0){
		model.setQuantity(completeServicePopUp.getDbserviceQuantity().getValue());
	}
	if(completeServicePopUp.getOlbUnitOfMeasurement().getSelectedIndex()!=0){
		model.setUom(completeServicePopUp.getOlbUnitOfMeasurement().getValue());
	}
	manageProcessLevelBar("Marked Completed !","Failed To Mark Complete !",Service.SERVICESTATUSCOMPLETED, 
			null, null,null,null);
	
	}
	
	
	public void manageProcessLevelBar(final String sucesssMessage,
			final String failureMessage,
			final String statusText,final ModificationHistory history,
			final Date ServiceDate,final String serviceTime,final String serviceBranch)
	{
		ArrayList<ProductGroupList> materialList = new ArrayList<ProductGroupList>();
		materialList.addAll(planPopUp.materialProductTable.getDataprovider().getList());
		
		servicerecords.saveServiceProjectStatus(model, new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				showDialogMessage(sucesssMessage);
				selectedRecord.setAttribute("Status", "Completed");
				form.grid.redraw();
				completeServicePopUp.hidePopUp();
				planPopUp.hidePopUp();
//				sendSMSLogic();
				sendSMS();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
	
		// MIN Creating 
		
		Console.log("Before Calling MIN  ");
			if(!selectedRecord.getAttribute("Status").equals("Completed")){
			int projectId = selectedRecord.getAttributeAsInt("ProjectId");
			System.out.println("project Id =="+projectId);
				servicelistservice.TechnicianSchedulingListMarkcompleteservice(model,LoginPresenter.loggedInUser,projectId,materialList, new AsyncCallback<ArrayList<Integer>>() {
				
				@Override
				public void onSuccess(ArrayList<Integer> result) {
					// TODO Auto-generated method stub
					Console.log("MIN on success");
					if(result.contains(2)){
						showDialogMessage("Service Completed Sucessfully");
						
						selectedRecord.setAttribute("Status", "Completed");
						form.grid.redraw();
					}
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					showDialogMessage("An Unexpected error occurred!");
	
				}
			});
		}
		
	}


	/**
	 * Description: This is for NBHC.
	 * By Rahul Verma
	 * Date : 25/01/2017
	 * @param model 
	 */
	private void createExtraServiceIfStackZero(Service model) {
		// TODO Auto-generated method stub
		Console.log("Inside Create Extra Service");
		for (int i = 0; i < model.getStackDetailsList().size(); i++) {
			if(model.getStackDetailsList().get(i).getQauntity()!=0){
				Console.log("Quantity not eQAUL to zero");
				createAnExtraService(model);
				break;
			}else{
				System.out.println("Do not create service");
			}
		}
	}
	
	private void createAnExtraService(final Service model) {
		// TODO Auto-generated method stub
		Console.log("Calling RPC to complete services");
		final CreateSingleServiceServiceAsync createSingleServiceAsync = GWT
				.create(CreateSingleServiceService.class);
		Timer timer=new Timer() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				createSingleServiceAsync.createSingleService(model.getCount(),model.getServiceDate(),model.getServiceIndexNo(),model.getServiceSerialNo(),model.getContractCount(),model.getCompanyId(),new AsyncCallback<ArrayList<Integer>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Console.log("Failed"+caught);
						showDialogMessage("An Unexpected Error");
					}

					@Override
					public void onSuccess(ArrayList<Integer> result) {
						// TODO Auto-generated method stub
						if(result.contains(-1)){
							showDialogMessage("Unexpected Error");
						}else if(result.contains(-2)){
							showDialogMessage("Parsing Previous Date Error");
						}else if(result.contains(-3)){
							showDialogMessage("Service Date Parsing Error");
						}else if(result.contains(1)){
							showDialogMessage("Successfull created Upcoming Services");
						}
					}
				});
			}
		};
		timer.schedule(3000);
		
	}

	
/********************************************Sms Method*****************************************/
	
	
	private void sendSMSLogic()
	{
		Vector<Filter> filtrvec = new Vector<Filter>();
		Filter filtr = null;
		
		filtr = new Filter();
		filtr.setLongValue(model.getCompanyId());
		filtr.setQuerryString("companyId");
		filtrvec.add(filtr);
		
		filtr = new Filter();
		filtr.setBooleanvalue(true);
		filtr.setQuerryString("status");
		filtrvec.add(filtr);
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtrvec);
		querry.setQuerryObject(new SmsConfiguration());
//		form.showWaitSymbol();
		
		genservice.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if(result.size()!=0){
					for(SuperModel model:result){
						SmsConfiguration smsconfig = (SmsConfiguration) model;
						boolean smsStatus = smsconfig.getStatus();
						String accountsid = smsconfig.getAccountSID();
						String authotoken = smsconfig.getAuthToken();
						String fromnumber = smsconfig.getPassword();
						
						if(smsStatus==true){
//						 sendSMS(accountsid, authotoken, fromnumber);
						}
						else{
							showDialogMessage("SMS Configuration is InActive");
						}
					}
				}
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});
		
	}
	

	private void sendSMS() {
		
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filtr = null;
		filtr = new Filter();
		filtr.setLongValue(model.getCompanyId());
		filtr.setQuerryString("companyId");
		filtervec.add(filtr);
		
		System.out.println("SERVICE STATUS :: "+model.getStatus());
		
		if (model.getStatus().equals(Service.SERVICESTATUSCOMPLETED)) {

			filtr = new Filter();
			filtr.setStringValue("Service Completion");
			filtr.setQuerryString("event");
			filtervec.add(filtr);
		} else if (model.getStatus().equals(Service.SERVICESTATUSSCHEDULE)) {

			filtr = new Filter();
			filtr.setStringValue("Service Schedule");
			filtr.setQuerryString("event");
			filtervec.add(filtr);
		}else if (model.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)) {

			filtr = new Filter();
			filtr.setStringValue("Service Reschedule");
			filtr.setQuerryString("event");
			filtervec.add(filtr);
		}else{
			filtr = new Filter();
			filtr.setStringValue("");
			filtr.setQuerryString("event");
			filtervec.add(filtr);
		}
		
		filtr = new Filter();
		filtr.setBooleanvalue(true);
		filtr.setQuerryString("status");
		filtervec.add(filtr);
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SmsTemplate());
//		form.showWaitSymbol();
		
		genservice.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					for(SuperModel smodel:result){
						final SmsTemplate sms =(SmsTemplate) smodel;
						final String templateMsgwithbraces = new String(sms.getMessage()); 
//						String custName = form.getPoc().getName().getValue();
						final String prodName = model.getProductName();
						final String serNo = model.getServiceSerialNo()+"";
						System.out.println("Service No===="+serNo);
						
						cellNo = model.getPersonInfo().getCellNumber();
						final String serDate = AppUtility.parseDate(model.getServiceDate());
						
						
						/**
						 * Date - 29 jun 2017 added by vijay for Eco Friendly getting customer name or customer correspondence name
						 */
						
						if(LoginPresenter.bhashSMSFlag){

							MyQuerry myquerry = AppUtility.getcustomerName(model.getPersonInfo().getCount(), model.getCompanyId());
							genservice.getSearchResult(myquerry, new AsyncCallback<ArrayList<SuperModel>>() {
								
								@Override
								public void onSuccess(ArrayList<SuperModel> result) {
									// TODO Auto-generated method stub
									for(SuperModel model :result){
										Customer customer = (Customer) model;
										 String custName;
										 Console.log("Currespondence=="+customer.getCustPrintableName());
										if(!customer.getCustPrintableName().equals("")){
											custName = customer.getCustPrintableName();
											Console.log("Customer correspondence Name =="+custName);
										}else{
											custName = customer.getFullname();
											Console.log("Customer  Name =="+custName);
										}
										String cutomerName = templateMsgwithbraces.replace("{CustomerName}", custName);
										String productName = cutomerName.replace("{ProductName}", prodName);
										String serviceDate =  productName.replace("{ServiceDate}", serDate);
										companyName = Slick_Erp.businessUnitName;
										actualMsg = serviceDate.replace("{companyName}", companyName);

										System.out.println("Actual MSG:"+actualMsg);
//										smsserviceAsync.sendSmsToClient(actualMsg, cellNo, accountsid, authotoken,fromnumber,model.getCompanyId(), new AsyncCallback<Integer>(){
//
//											@Override
//											public void onFailure(Throwable caught) {
//											}
//
//											@Override
//											public void onSuccess(Integer result) {
//												if(result==1)
//												{
//													showDialogMessage("SMS Sent Successfully!");
//													saveSmsHistory();
//
//												}
//											}} );
										
										smsserviceAsync.sendMessage(AppConstants.SERVICEMODULE, AppConstants.SERVICE, sms.getEvent(), model.getCompanyId(), actualMsg, cellNo,false, new AsyncCallback<String>() {
											
											@Override
											public void onSuccess(String result) {
												// TODO Auto-generated method stub
												showDialogMessage("Message Sent Successfully!");

											}
											
											@Override
											public void onFailure(Throwable caught) {
												// TODO Auto-generated method stub
												
											}
										});
									}
								}
								
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									
								}
							});
							
							
							
							
						}else{
						/**
						 * ends here
						 */
						
						String custName = model.getPersonInfo().getFullName();

						String cutomerName = templateMsgwithbraces.replace("{CustomerName}", custName);
						String productName = cutomerName.replace("{ProductName}", prodName);
						String serviceNo = productName.replace("{ServiceNo}", serNo);
						String serviceDate =  serviceNo.replace("{ServiceDate}", serDate);
						companyName = Slick_Erp.businessUnitName;
						actualMsg = serviceDate.replace("{companyName}", companyName);

						System.out.println("Actual MSG:"+actualMsg);
						
						System.out.println("Company Name========================:"+companyName);
						
//						smsserviceAsync.sendSmsToClient(actualMsg, cellNo, accountsid, authotoken,fromnumber,model.getCompanyId(), new AsyncCallback<Integer>(){
//
//							@Override
//							public void onFailure(Throwable caught) {
//							}
//
//							@Override
//							public void onSuccess(Integer result) {
//								if(result==1)
//								{
//									showDialogMessage("SMS Sent Successfully!");
////									saveSmsHistory();
//
//								}
//							}} );
						
						smsserviceAsync.sendMessage(AppConstants.SERVICEMODULE, AppConstants.SERVICE, sms.getEvent(), model.getCompanyId(), actualMsg, cellNo,false, new AsyncCallback<String>() {
							
							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								showDialogMessage("Message Sent Successfully!");

							}
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								
							}
						});
						
					}
					}
				}
			
				
			}
			@Override
			public void onFailure(Throwable caught) {
			}
		});

	}	
	
	private void saveSmsHistory (){

		SmsHistory smshistory = new SmsHistory();
		smshistory.setDocumentId(model.getCount());
		smshistory.setDocumentType("Service");
		smshistory.setSmsText(actualMsg);
		smshistory.setName(model.getPersonInfo().getFullName());
		smshistory.setCellNo(cellNo);
		smshistory.setUserId(LoginPresenter.loggedInUser);
		System.out.println("UserID"+LoginPresenter.loggedInUser);
		System.out.println("Doc Id"+model.getCount());
		genservice.save(smshistory, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				System.out.println("Data Saved SuccessFully");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				System.out.println("Data Saved UnsuccessFully");

			}
		});		
	}
	
	
private void saveReschedule(Service model, String rescheduleReason, Date RescheduleDate, String serviceTime, String servicingBranch) {

		
		model.setServiceTime(serviceTime);
		
		ModificationHistory history=new ModificationHistory();
		
		history.oldServiceDate=model.getServiceDate();
		history.resheduleDate=RescheduleDate;
	
		
		history.reason=rescheduleReason;
		history.userName=LoginPresenter.loggedInUser;
		history.systemDate=new Date();
		history.resheduleTime=serviceTime;
		if(servicingBranch!=null)
		history.resheduleBranch = servicingBranch;
		
		model.getListHistory().add(history);
		
		model.setServiceDate(RescheduleDate);
		
		
		genservice.save(model, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	private boolean validateRechedule() {

		if(planPopUp.dbserviceDate.getValueAsDate()==null){
			showDialogMessage("Please add Service Date");
			return false;
		}
		if(planPopUp.tbRecheduleReason.getValue()==null || planPopUp.tbRecheduleReason.getValue().equals("")){
			showDialogMessage("Please add resaon for Rechedule");
			return false;
		}
		if(planPopUp.servicetime.getSelectedIndex()==0){
			showDialogMessage("Please select Service Time!");
			return false;
		}
		
		Date rescheduleDate = planPopUp.dbserviceDate.getValueAsDate();
		Date contractEndDate = model.getContractEndDate();
		Date contractStartDate = model.getContractStartDate();
		
		/**
		 * rohan added this code to restrict service date in between contract period only contract end date
		 * Date : 18 /1/2017
		 */
		int flag =rescheduleDate.compareTo(contractEndDate);
		if(flag > 0)
		{
			showDialogMessage("Reschedule date should not be greater than contract period..!");
			return false;
		}
		
		int cnt =rescheduleDate.compareTo(contractStartDate);
		if(cnt <= 0)
		{
			showDialogMessage("Reschedule date should be after contract start date..!");
			return false;
		}
		
		/**
		 * ends here 
		 */
		
		return true;
	}
	private boolean ValidatePopupData() {
		
		if(planPopUp.technicianTable.getDataprovider().getList().size()==0){
			showDialogMessage("Please add Technician ");
			return false;
		}
		/** date 11/11/2017 commmented by komal to remove validation on service time **/
//		if(planPopUp.servicetime.getSelectedIndex()==0){
//			showDialogMessage("Please Select Service Time");
//			return false;
//		}
		if(planPopUp.dbserviceDate.getValue()==null){
			showDialogMessage("Service Date can not be blank");
			return false;
		}
		
		return true;
	}
	
	private String getTechnicianName(List<EmployeeInfo> list) {

		String TechnicianName ="";
		
		for(int i=0;i<planPopUp.olbTeam.getItems().size();i++){
			if(planPopUp.olbTeam.getItems().get(i).getTeamList().get(0).getEmployeeName().equals(list.get(0).getFullName())){
				System.out.println("Team employee name matched");
				selectedRecord.setAttribute("Technician", list.get(0).getFullName());
				form.grid.redraw();
				TechnicianName = list.get(0).getFullName();
				return TechnicianName;
			}
		}
		
		for(int i=0;i<planPopUp.olbEmployee.getItems().size();i++){
			if(planPopUp.olbEmployee.getItems().get(i).getFullName().equals(list.get(0).getFullName())){
				System.out.println("technician Name matched");
				selectedRecord.setAttribute("Technician", list.get(0).getFullName());
				form.grid.redraw();
				TechnicianName = list.get(0).getFullName();
				
			}
		}
		return TechnicianName;
		
//		selectedRecord.setAttribute("ServiceTime", planPopUp.servicetime.getValue(planPopUp.servicetime.getSelectedIndex()));

	}
	
	private boolean validateSerachPopup() {
		System.out.println("Search - "+searchPopup.personInfo.getValue());
		/**
		 * updated by: Viraj 
		 * Date: 25-12-2018
		 * Description: Added validation for locality and technician
		 */
		if(searchPopup.getDbFromDate().getValueAsDate()==null && 
			searchPopup.getDbToDate().getValueAsDate() == null &&
			searchPopup.getOlbBranch().getSelectedIndex()==0 &&
			searchPopup.getIbContractId().getValue()==null && 
			searchPopup.personInfo.getValue()==null &&
			searchPopup.getOlbEmployee().getValue() == null &&
			searchPopup.getLocality().getValue() == null){
			showDialogMessage("Select atleast one field to search! ");
			return false;
		}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BRANCHLEVELRESTRICTION")){
			if (!LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")) {
			if(searchPopup.getOlbBranch().getSelectedIndex()==0){
				showDialogMessage("Select Branch!");
				return false;
			}
			}
		}
		
		return true;
	}

	private void reactOnGo() {
		// TODO Auto-generated method stub
		searchPopup.getWinModal().hide();
		
		GenricServiceAsync service=GWT.create(GenricService.class);

		
		ArrayList<String> statuslist = new ArrayList<String>();
		statuslist.add("Scheduled");
//		statuslist.add("Completed");
		statuslist.add("Rescheduled");
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		MyQuerry querry;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statuslist);
		filtervec.add(filter);
		
		if(searchPopup.getDbFromDate().getValueAsDate()!=null){
			filter = new Filter();
			filter.setQuerryString("serviceDate >=");
			filter.setDateValue(searchPopup.getDbFromDate().getValueAsDate());
			filtervec.add(filter);
		}
		
		if(searchPopup.getDbToDate().getValueAsDate()!=null){
		filter = new Filter();
		filter.setQuerryString("serviceDate <=");
		filter.setDateValue(searchPopup.getDbToDate().getValueAsDate());
		filtervec.add(filter);
		}
		
		if(searchPopup.getOlbBranch().getSelectedIndex()!=0){
			filter=new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(searchPopup.getOlbBranch().getValue(searchPopup.getOlbBranch().getSelectedIndex()));
			filtervec.add(filter);
		}
		
		if(searchPopup.getIbContractId().getValue()!=null){
			filter=new Filter();
			filter.setQuerryString("contractCount");
			filter.setIntValue(searchPopup.getIbContractId().getValue());
			filtervec.add(filter);
		}
		
		if(searchPopup.personInfo.getIdValue()!=-1)
		  {
			filter=new Filter();
			filter.setIntValue(searchPopup.personInfo.getIdValue());
			filter.setQuerryString("personInfo.count");
			filtervec.add(filter);
		  }
		
		/**
		 * Updated by: Viraj
		 * Date: 25-12-2018
		 * Description: to get records according to technician and locality
		 */
		if(searchPopup.getLocality().getSelectedIndex()!=0){
			filter=new Filter();
			filter.setQuerryString("address.locality");
			filter.setStringValue(searchPopup.getLocality().getValue(searchPopup.getLocality().getSelectedIndex()));
			filtervec.add(filter);
		}
		
		if(searchPopup.getOlbEmployee().getSelectedIndex()!=0){
			filter=new Filter();
			filter.setQuerryString("employee");
			filter.setStringValue(searchPopup.getOlbEmployee().getValue(searchPopup.getOlbEmployee().getSelectedIndex()));
			filtervec.add(filter);
		}
		/** Ends **/
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Service());
		querry.setFilters(filtervec);
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("RESULT SIZE : "+result.size());
				
				 globalServicelist=new ArrayList<Service>();
				
				for(SuperModel model:result){
					Service customerEntity=(Service) model;
					globalServicelist.add(customerEntity);
				}
				form.setDataToListGrid(globalServicelist);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Failed");
				Console.log("RPC Failed");
				showDialogMessage("No Result TO Display");
			}
		});
		

		
	}

	private void reactOnSearch() {

		
		searchPopup.showPopup();
	}

	private void reactOnPrint() {
		System.out.println("Inside print");

		if(model==null){
			
			showDialogMessage("Please Select record then click on print");
			model=null;
			
		}else{
			
			
			// ********************************************
			MyQuerry querry = new MyQuerry();
			Company c = new Company();

			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter = null;
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);

			filter = new Filter();
			filter.setQuerryString("processName");
			filter.setStringValue("Service");
			filtervec.add(filter);

			filter = new Filter();
			filter.setQuerryString("processList.status");
			filter.setBooleanvalue(true);
			filtervec.add(filter);

			querry.setFilters(filtervec);
			querry.setQuerryObject(new ProcessConfiguration());

			genservice.getSearchResult(querry,
					new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							System.out.println(" result set size +++++++"
									+ result.size());

							List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();

							int antipest = 0;
							if (result.size() == 0) {

							} else {

								for (SuperModel model : result) {
									ProcessConfiguration processConfig = (ProcessConfiguration) model;
									processList.addAll(processConfig.getProcessList());

								}

								for (int k = 0; k < processList.size(); k++) {
									if (processList.get(k).getProcessType().trim().equalsIgnoreCase("JobCompletionCertificate")	&& processList.get(k).isStatus() == true) {
										antipest = antipest + 1;
									}

									if (antipest > 0) {
										antipestFlag = true;
									}
									System.out.println("Flag value++++++++++++ "+ antipestFlag);
								}
							}
						}
					});
			
			/**
			 * Date 13 Feb 2017
			 * by Vijay
			 * Description :- if condition for nbhc service engineer is mondatory if we click on print then validation will
			 * display message if service engineer is blank in service
			 * else for all clients service engineer non mondatory
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","OnlyForNBHC"))
			{
				if(model.getEmployee()==null && model.getEmployee().equals("")){
					form.showDialogMessage("Please add Service engineer first!");
				}
				else{
					 createProjectCorrespondingToService("Print");			
					 }
				
				
			}else{
				 createProjectCorrespondingToService("Print");		
				 }
			
			/**
			 * end here
			 */
		}

		
	
	}
	
	private void createProjectCorrespondingToService(final String typeOfTransaction)
	{
    		Timer timer=new Timer() 
        	 {
    				@Override
    				public void run() {
		
						projectAsync.createCustomerProject(model.getCompanyId(), model.getContractCount(), model.getCount(),model.getStatus(), new AsyncCallback<Integer>(){
				
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An Unexpected Error occured !");
							}
				
							@Override
							public void onSuccess(Integer result) {
								if(result==1){
									if(typeOfTransaction.trim().equals("Button"))
									{
										reactOnAddProject();
									}
									if(typeOfTransaction.trim().equals("Print"))
									{
										if (antipestFlag == true) {
											final String url1 = GWT.getModuleBaseURL()+ "pdfjobcerti"+ "?Id="+ model.getId();
											Window.open(url1, "test", "enabled");
											
										} else {
										
//										final String url = GWT.getModuleBaseURL() + "customerserpdf"+"?Id="+model.getId();
//										Window.open(url, "test", "enabled");
										
										final String url = GWT.getModuleBaseURL()+"pdfserjob"+ "?Id="+ model.getId();
										Window.open(url, "test", "enabled");
										
										}
									}
								}
							//  vijay addded this code
								if(result==-1){
									if(typeOfTransaction.trim().equals("Button"))
									{
										form.showDialogMessage("Project deos not exist for this Service Service already Completed!");

									}else{
										form.showDialogMessage("Project deos not exist for this Service Can Not print Job Card!");
									}
								}
								
								if(result==0){
									form.showDialogMessage("An unexpected error occurred. Please try again!");
								}
							}
						});
    				}
  			};
             timer.schedule(3000);
 		
	}
	
	private void reactOnAddProject()
	{
		  MyQuerry querry=new MyQuerry();
		  Vector<Filter> filtervec=new Vector<Filter>();
	      Filter filter=null;
	      filter=new Filter();
	      filter.setQuerryString("serviceId");
	      filter.setIntValue(model.getCount());
	      filtervec.add(filter);
	      
	      filter=new Filter();
	      filter.setQuerryString("companyId");
	      filter.setLongValue(model.getCompanyId());
	      filtervec.add(filter);
	      
	      querry.setFilters(filtervec);
	      querry.setQuerryObject(new ServiceProject());
	        
    	  ProjectForm form=ProjectPresenter.initalize(querry);
    	  form.setToNewState();
		  AppMemory.getAppMemory().stickPnel(form);
          
	        
          filter=new Filter();
	        filter.setQuerryString("personInfo.count");
	        filter.setIntValue(model.getPersonInfo().getCount());
	        System.out.println("count::"+model.getPersonInfo().getCount());
	        querry=new MyQuerry();
	        querry.setQuerryObject(new ClientSideAsset());
	        querry.getFilters().add(filter);
	        form.olbClientSideAsset.MakeLive(querry);
            
            if(model.getStatus().equals(Service.SERVICESTATUSCOMPLETED)
            		||model.getStatus().equals(Service.SERVICESTATUSCLOSED)
            		||model.getStatus().equals(Service.SERVICESTATUSCANCELLED))
            {
            	form.setToNewState();
            }
	}
	
	
	/*
	 * Date:30/08/2018
	 * Developer:Ashwini
	 * Des:To add cell no.2 and Landline no.
	 */
	
	private void getCustomerList(ListGrid listGrid){
		
		ArrayList<Integer> custId = new ArrayList<Integer>();
	    HashSet<Integer> custCount = new HashSet<Integer>();
		ListGridRecord[] record = listGrid.getRecords();
		Long compy  = (long) 0;
		for (int i = 0; i < record.length; i++){
			
			ListGridRecord listGridRecord = record[i];
			ListGridField[] listGridFields = listGrid.getFields();
			model =getServiceModel(Integer.parseInt(listGridRecord.getAttribute("ServiceId").trim()));
			custCount.add(model.getPersonInfo().getCount());
			compy = model.getCompanyId();
		}
		custId.addAll(custCount);
		System.out.println("customerlist value::"+custId.toString());

		customerId.createSinglecustomer(0, custId,compy , new AsyncCallback<ArrayList<Customer>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<Customer> result) {
				
				// TODO Auto-generated method stub
				for(SuperModel model:result){
					Customer customerEntity=(Customer) model;
					System.out.println("customerlist value::"+customerEntity.getCount());
					custList.add(customerEntity);
				}
				System.out.println("gegt cust list -- " + custList.size());
				downloadServiceList();
				
			}
		});
		
		
	}
	
	void downloadServiceList(){

		// TODO Auto-generated method stub
		StringBuilder exportedCSV = reactOnDownload(form.getGrid());

		csvservice.setSchedulingList(exportedCSV.toString(), new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+129;
				Window.open(url, "test", "enabled");
			}
		});	
	
	}
	
	/*
	 * End by Ashwini
	 */
	
	/**
	 * Created By : Komal 
	 * Date: 18 Nov 2017
	 * Description : Download Technician Scheduling List
	 * @param listGrid
	 * @return
	 */
	private  StringBuilder reactOnDownload(ListGrid listGrid) {
   
		final StringBuilder stringBuilder = new StringBuilder(); // csv data in here
		
		
		 
		
	
		// column names
		ListGridField[] fields = listGrid.getFields();
//		for (int i = 4; i < fields.length; i++) {
			for (int i = 4; i < fields.length-1; i++) {
				
			ListGridField listGridField = fields[i];
			stringBuilder.append("\"");
			stringBuilder.append(listGridField.getName());
			stringBuilder.append("\",");
			System.out.println("listGridField Name::"+listGridField.getName());
		
	}
			
			/*
			 * Date:25/08/2018
			 * Developer:Ashwini
			 * Des:To add cell no.2 and landline no .
			 */
			
			    stringBuilder.append("\"");
			    stringBuilder.append("Cell no.1");
			    stringBuilder.append("\",");
			
				stringBuilder.append("\"");
				stringBuilder.append("Cell no.2");
				stringBuilder.append("\",");
				
				stringBuilder.append("\"");
				stringBuilder.append("Landline no");
				stringBuilder.append("\",");
				

				stringBuilder.append("\"");
				stringBuilder.append("Email Id");
				stringBuilder.append("\",");
				
				
		stringBuilder.deleteCharAt(stringBuilder.length() - 1); // remove last ","
		stringBuilder.append("\n");
		
		// column data
		ListGridRecord[] records = listGrid.getRecords();
//		for (int i = 0; i < records.length; i++) {
			for (int i = 0; i < records.length; i++) {
			ListGridRecord listGridRecord = records[i];
			ListGridField[] listGridFields = listGrid.getFields();
			
//			for (int j = 4; j < listGridFields.length; j++) {
				for (int j = 4; j < listGridFields.length-1; j++) {
				ListGridField listGridField = listGridFields[j];
				
				model =getServiceModel(Integer.parseInt(listGridRecord.getAttribute("ServiceId").trim()));
				stringBuilder.append("\"");
				if(listGridField.getName().equalsIgnoreCase("ServiceDate")){
					
				Date date = format2.parse(getDateString(listGridRecord.getAttribute(listGridField.getName())));
				
				stringBuilder.append(format2.format(date));

				}
//				else if(j==listGridFields.length || j==listGridFields.length+1){
//					System.out.println("hi Ash");
//				}
				else{
				stringBuilder.append(listGridRecord.getAttribute(listGridField.getName()));
				}
				
				stringBuilder.append("\",");
			}
				
			/*
			 * date:25/08/2018
			 * Developer:Ashwini
			 * Des:To add cell no.2 &Landline no.	
			 */
				
					int size=custList.size();
					System.out.println("Customer list::"+custList.size());
					boolean getflag = false;
				for(int k=0; k<size; k++)	{
					
					 if(model.getPersonInfo().getCount()==custList.get(k).getCount()){
						 getflag = true;
						 System.out.println("seviceId :::"+model.getPersonInfo().getCount());
						 System.out.println("customer list id:::"+custList.get(k).getCount());
						
						 stringBuilder.append("\"");
					     stringBuilder.append(custList.get(k).getCellNumber1());
					     stringBuilder.append("\",");
							
						stringBuilder.append("\"");
						stringBuilder.append(custList.get(k).getCellNumber2());
						stringBuilder.append("\",");
						
						stringBuilder.append("\"");
						stringBuilder.append(custList.get(k).getLandline());
						stringBuilder.append("\",");
						
						stringBuilder.append("\"");
						stringBuilder.append(custList.get(k).getEmail());
						stringBuilder.append("\",");
						break;
						
					}
					 
				}
				if(!getflag)
				 {
						stringBuilder.append("\"");
						stringBuilder.append(" ");
						stringBuilder.append("\",");
						stringBuilder.append("\"");
						stringBuilder.append(" ");
						stringBuilder.append("\",");
				}
				
				/*
				 * End by Ashwini
				 */
			stringBuilder.deleteCharAt(stringBuilder.length() - 1); // remove last ","
			stringBuilder.append("\n");
			
		}
		return stringBuilder;
	
}

	private String getDateString(String dateString){
		
		String str = "";
		for(int i=0;i<dateString.length();i++){
			
			
			str = str + dateString.charAt(i);
			System.out.println("str"+str);
			if(dateString.charAt(i) == ':'){
				System.out.println("String is : "+ str.substring(4, i-3));
				return str.substring(4, i-3).trim();
			}
			
			
		}
		
		return dateString;
		
		
	}

	
	
}
