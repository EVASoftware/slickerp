package com.slicktechnologies.client.views.technicianschedule;

import java.util.ArrayList;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
//import com.slicktechnologies.shared.common.technicianscheduling.TechnicianMaterialInfo;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;

public class MaterialProductTable extends SuperTable<ProductGroupList> implements ClickHandler {

	TextColumn<ProductGroupList> getColumnProductName;
	TextColumn<ProductGroupList> getColumnQuantity;
	TextColumn<ProductGroupList> getColumnWarehouseName;
	TextColumn<ProductGroupList> getColumnStorageLocationName;
	TextColumn<ProductGroupList> getColumnStorageBinName;
	
	TextColumn<ProductGroupList> getColumnReturnQuantity;
	Column<ProductGroupList, String> returnQuantity;
	
//	TextColumn<ProductGroupList> getcolumnProjectId;
	
	Column<ProductGroupList, String> btnreturnQuantity;
	Column<ProductGroupList, String> deleteColumn;
	
	/** date 11/11/2017 added by komal to add UOM column **/
	TextColumn<ProductGroupList> getColumnUOM;
	
	public static int projectId;
	
	int rowindex=0;
	int productId=0;
	final GenricServiceAsync genasync=GWT.create(GenricService.class);

	ServiceCompletetionPopup returnQtyPopUp = new ServiceCompletetionPopup(true);
	
	public MaterialProductTable(){
		super();
		setHeight("100px");
		
		returnQtyPopUp.getBtnReturnQtySave().addClickHandler(this);
		returnQtyPopUp.getBtnReturnQtyCancel().addClickHandler(this);
	}
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addColumnProductName();
		/** date 11/11/2017 added by komal to add UOM column **/
		addColumnUOM();
		addColumnQuantity();
		addColumnWarehosueName();
		addColumnStorageLocation();
		addColumnStorageBin();
//		addColumnReturnQuantity();
		addColumnViewReturnQuantity();
		addColumnReturnQuantity();
		addColumnDelete();
		addFieldUpdater();
	}
	/** date 11/11/2017 added by komal to add UOM column **/
	private void addColumnUOM(){
      getColumnUOM = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				return object.getUnit();
			}
		};
		table.addColumn(getColumnUOM, "UOM");
		table.setColumnWidth(getColumnUOM, 50, Unit.PX);
		
	}

//	private void addColumnProjectId() {
//
//		getcolumnProjectId = new TextColumn<ProductGroupList>() {
//			
//			@Override
//			public String getValue(ProductGroupList object) {
//				// TODO Auto-generated method stub
//				return "";
//			}
//		};
//		
//	}

	private void addColumnReturnQuantity() {

//		EditTextCell editCell=new EditTextCell();
		
		ButtonCell btnceell = new ButtonCell();

		returnQuantity = new Column<ProductGroupList, String>(btnceell) {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				return "Return Qty";
			}
		};
		table.addColumn(returnQuantity, "Return");
		table.setColumnWidth(returnQuantity, 100, Unit.PX);
	}

	private void addColumnViewReturnQuantity() {

		getColumnReturnQuantity = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				return object.getReturnQuantity()+"";
			}
		};
		table.addColumn(getColumnReturnQuantity, "Return Quantity");
		table.setColumnWidth(getColumnReturnQuantity, 100, Unit.PX);
	}

	private void addColumnProductName() {

		getColumnProductName = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				return object.getName();
			}
		};
		table.addColumn(getColumnProductName, "Material Name");
		table.setColumnWidth(getColumnProductName, 100, Unit.PX);
	}

	private void addColumnQuantity() {

		getColumnQuantity = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				return object.getQuantity()+"";
			}
		};
		table.addColumn(getColumnQuantity, "Quantity");
		table.setColumnWidth(getColumnQuantity, 100, Unit.PX);
		
	}

	private void addColumnWarehosueName() {

		getColumnWarehouseName = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				return object.getWarehouse();
			}
		};
		table.addColumn(getColumnWarehouseName, "Warehouse Name");
		table.setColumnWidth(getColumnWarehouseName, 100, Unit.PX);
	}

	private void addColumnStorageLocation() {

		getColumnStorageLocationName = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				return object.getStorageLocation();
			}
		};
		table.addColumn(getColumnStorageLocationName, "Storage Location");
		table.setColumnWidth(getColumnStorageLocationName, 100, Unit.PX);
	}

	private void addColumnStorageBin() {

		getColumnStorageBinName = new TextColumn<ProductGroupList>() {
			
			@Override
			public String getValue(ProductGroupList object) {
				// TODO Auto-generated method stub
				return object.getStorageBin();
			}
			
		};
		table.addColumn(getColumnStorageBinName, "Storage Bin");
		table.setColumnWidth(getColumnStorageBinName, 100, Unit.PX);
	}

	private void addColumnDelete() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<ProductGroupList, String>(btnCell) {
			@Override
			public String getValue(ProductGroupList object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn, 100, Unit.PX);
	}
	
	

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterdeleteColumn();
		createFieldUpdaterReturnQtyColumn();
	}
		
	private void createFieldUpdaterReturnQtyColumn() {

		returnQuantity.setFieldUpdater(new FieldUpdater<ProductGroupList, String>() {
			
			@Override
			public void update(int index, ProductGroupList object, String value) {
				// TODO Auto-generated method stub
				
//				double returnQty = Double.parseDouble(value);
//				object.setReturnQuantity(returnQty);
//				table.redrawRow(index);
				rowindex = index;
				productId = object.getProduct_id();
				
				returnQtyPopUp.showReturnQtyPopUp();
//				saveintoProject(returnQty,object.getProduct_id());
			}

			
		});
	}

	protected void createFieldUpdaterdeleteColumn() {
		deleteColumn.setFieldUpdater(new FieldUpdater<ProductGroupList, String>() {
			@Override
			public void update(int index, ProductGroupList object, String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
		 
		 if(state==true){
			 createTable();
		 }else{
			 	addColumnProductName();
				addColumnQuantity();
				addColumnWarehosueName();
				addColumnStorageLocation();
				addColumnStorageBin();
				addColumnViewReturnQuantity();
		 }
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	private void saveintoProject(final double returnQty) {
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		
		Filter filter = null;
		
		filter = new Filter();
		filter.setIntValue(projectId);
		filter.setQuerryString("count");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ServiceProject());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				for(SuperModel model : result){
					System.out.println("Resultttt =="+result.size());
					final ServiceProject project = (ServiceProject) model;
					for(int i=0;i<project.getProdDetailsList().size();i++){
						System.out.println("project Product id =="+project.getProdDetailsList().get(i).getProduct_id());
						System.out.println("product id"+productId);
						if(project.getProdDetailsList().get(i).getProduct_id().equals(productId)){
							System.out.println("Done here");
							project.getProdDetailsList().get(i).setReturnQuantity(returnQty);
						}
					}
					
					Timer timer = new Timer() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							SaveProject(project);
						}
					};
					timer.schedule(1000);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				System.out.println("Failed");
			}
		});
		
	}
	
	
	private void SaveProject(ServiceProject project) {
		// TODO Auto-generated method stub
		
		genasync.save(project, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				// TODO Auto-generated method stub
				System.out.println("On save success");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				System.out.println("Failed");
			}
		});
	}

	@Override
	public void onClick(ClickEvent event) {

		// return material qty
				if(event.getSource() == returnQtyPopUp.getBtnReturnQtySave()){
					if(returnQtyPopUp.getDbmaterialReturnQty().getValue()==null){
						showDialogMessage("Please add return Quantity");
					}
					else if(returnQtyPopUp.getDbmaterialReturnQty().getValue()==0){
						showDialogMessage("Return Quantity can not be zero");
					}else{
						ReactOnreturnQuantity(returnQtyPopUp.getDbmaterialReturnQty().getValue());
					}
				}
				if(event.getSource() == returnQtyPopUp.getBtnReturnQtyCancel()){
					returnQtyPopUp.hideReturnQtyPopUp();
				}
	}
	
	private void ReactOnreturnQuantity(Double returnQty) {
		
		ArrayList<ProductGroupList> list = new ArrayList<ProductGroupList>();
		list.addAll(getDataprovider().getList());
		
		list.get(rowindex).setReturnQuantity(returnQty);
		
		getDataprovider().getList().clear();
		getDataprovider().getList().addAll(list);
		
		returnQtyPopUp.hideReturnQtyPopUp();
		
		saveintoProject(returnQty);
	}

	/**** Smart GWT dialog message popup (vijay *****************/
	public static void showDialogMessage(String Message)
	{
		SC.say(Message);
	}
}
