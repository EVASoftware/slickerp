package com.slicktechnologies.client.views.technicianschedule;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CustomerProjectService;
import com.slicktechnologies.client.services.CustomerProjectServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.ServiceListService;
import com.slicktechnologies.client.services.ServiceListServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.assesmentreport.AssesmentReportPresenter;
import com.slicktechnologies.client.views.device.CreateSingleServiceService;
import com.slicktechnologies.client.views.device.CreateSingleServiceServiceAsync;
import com.slicktechnologies.client.views.device.SaveService;
import com.slicktechnologies.client.views.device.SaveServiceAsync;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;
import com.slicktechnologies.shared.common.technicianscheduling.TechnicianMaterialInfo;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
//import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class TechnicianScheduleListForm extends ViewContainer implements ClickHandler, ChangeHandler, com.google.gwt.event.dom.client.ClickHandler  {

	
	ListGrid grid;
	DataSource ds;
	
	/** DATE :27/12/2017 Manisha added button assesss**/
	
	Button btnInvoice,btnschedule,btnprint,btnViewDetails,btnassess;
	/**
	 * Updated By: Viraj
	 * Date: 26-12-2018
	 * Description: to add all the functionalities of customer service list
	 */
	Button btnViewContract,btnViewDoc,btnCancelServ,btnSuspendServ ;
	Button btnResumeServ,btnMarkComplete,btnAssignTech,btnReschedule,btnPrtSummary ;
	
	HorizontalPanel horizontalpanel;
	
	VerticalPanel verticalpanel;
	
//	HorizontalPanel hpccustomerComposite;
//	HorizontalPanel hpsearchFields;

	
	
	final GeneralServiceAsync servicerecords = GWT.create(GeneralService.class);

	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	
	TechnicianSchedulePrintPopUp schedulePrintPopup = new TechnicianSchedulePrintPopUp();

	/**
	 * Date 16-03-2018 By vijay below code moved to presenter 
	 * due to buttons technician list too slow so i have added images and moved all the code in presenter
	 */

//	SaveServiceAsync asyncService=GWT.create(SaveService.class);
//	TechnicianPlanPopUp planPopUp = new TechnicianPlanPopUp();
//	ServiceCompletetionPopup completeServicePopUp = new ServiceCompletetionPopup();
//	final GeneralServiceAsync technicianPlanInfoSync = GWT.create(GeneralService.class);
//	Service model=null;
//	ListGridRecord selectedRecord=null;
//	public final ServiceListServiceAsync servicelistservice=GWT.create(ServiceListService.class);
//	TechnicianSchedulePrintPopUp reschedulePopup = new TechnicianSchedulePrintPopUp(true);
//	
//	/****** For SMS *****/
//	 private long cellNo;
//	 private String companyName;
//	 private String actualMsg;
//	final SmsServiceAsync smsserviceAsync=GWT.create(SmsService.class);
//
//	
//	CustomerProjectServiceAsync projectAsync=GWT.create(CustomerProjectService.class);
//
//	//serach fields
//	public PersonInfoComposite personInfo;
//	public ObjectListBox<Branch> olbBranch;
//	DateItem dbFromDate;
//	DateItem dbToDate;
//	DoubleBox dbContractId;
//	Button btnGo;
//	/**
//	 * Added By Komal
//	 * Date : 18 Nov 2017
//	 * Description : Used for technician validation
//	 */
//	boolean technianValidateflag = true;
//	DateTimeFormat format1= DateTimeFormat.getFormat("dd-MMM-yyyy");
//	/**
//	 * Ends For Komal
//	 */
	
	/**
	 * ends here
	 */
	
	public TechnicianScheduleListForm() {
		super();
		createGui();
		
		schedulePrintPopup.getBtnOk().addClickHandler(this);
		schedulePrintPopup.getBtnCancel().addClickHandler(this);
		/**
		 * Date 16-03-2018 by vijay
		 */
//		completeServicePopUp.getBtnOk().addClickHandler(this);
//		completeServicePopUp.getBtnCancel().addClickHandler(this);
//		reschedulePopup.getBtnReschedule().addClickHandler(this);
//		reschedulePopup.getBtnReshceduleCancel().addClickHandler(this);
		/**
		 * ends here
		 */
		
		
	}
	
	@Override
	protected void createGui() {
		System.out.println("CREATE GUI");
		
		btnschedule = new Button("Schedule");
		btnschedule.addClickHandler(this);
		btnprint = new Button("Plan Print");
		btnprint.addClickHandler(this);
		btnViewDetails = new Button("View Details");
		btnViewDetails.addClickHandler(this);
		
		/**
		 * Updated By: Viraj
		 * Date: 26-12-2018
		 * Description: to add all the functionalities of customer service list
		 */
		btnAssignTech = new Button("Assign Technician");
		btnCancelServ = new Button("Cancel Services");
		btnMarkComplete = new Button("Mark Complete");
		btnViewContract = new Button("View Contract");
		btnViewDoc = new Button("View Document");
		btnSuspendServ = new Button("Suspend Services");
		btnResumeServ = new Button("Resume Services");
		btnReschedule = new Button("Reschedule");
		/** Ends **/
		
		
		
		/** DATE :27/12/2017 Manisha added button assesss**/
		btnassess= new Button("View Assessment Report");
		btnassess.addClickHandler(this);
		/**End**/
		
		horizontalpanel = new HorizontalPanel();

		horizontalpanel.add(btnschedule);
		horizontalpanel.add(btnprint);
		/**
		 * Updated By: Viraj
		 * Date: 26-12-2018
		 * Description: to add all the functionalities of customer service list
		 */
		horizontalpanel.add(btnViewContract);
		horizontalpanel.add(btnViewDoc);
		horizontalpanel.add(btnCancelServ);
		horizontalpanel.add(btnSuspendServ);
		horizontalpanel.add(btnResumeServ);
		horizontalpanel.add(btnMarkComplete);
		horizontalpanel.add(btnAssignTech);
		horizontalpanel.add(btnReschedule);
		
		/** Ends **/
		
		/** DATE :27/12/2017 Manisha added button assesss**/
		if(AppMemory.getAppMemory().currentScreen==Screen.ASSESSMENTSCHEDULING){
		horizontalpanel.add(btnassess);
		}
		/**End**/
		horizontalpanel.setSpacing(10);
		horizontalpanel.getElement().addClassName("technicianscheduleButtons");
		
		toggleAppHeaderBarMenu();
		
		verticalpanel = new VerticalPanel();
		
		/**
		 * Date 16-03-2018 by vijay
		 */
//		
//		planPopUp.btnOk.addClickHandler(this);
//		planPopUp.btnReschedule.addClickHandler(this);
//		planPopUp.getBtnComplete().addClickHandler(this);
//		
//		grid = new ListGrid(){
//
//			@Override
//			protected Canvas createRecordComponent(final ListGridRecord record,
//					Integer colNum) {
//				
//
//				 String fieldName = this.getFieldName(colNum);
//				 if (fieldName.equals("Plan")) { 
//					
//	                    IButton button = new IButton();  
//	                    button.setHeight(26);  
//	                    button.setWidth(70);
//	                    button.setShowRollOver(true);  
//	                    button.setShowDown(true);  
//
//	                    button.setTitle("Plan");  
//	                    button.addClickHandler(new ClickHandler() {
//							
//							@Override
//							public void onClick(ClickEvent event) {
//								System.out.println("hi vijay plan");
//								
//								selectedRecord = record;
//							    model =getServiceModel(Integer.parseInt(record.getAttribute("ServiceId").trim()));
//
//							    getProjectDetails(model,record);
//							
//							}
//							
//						});
//	                    return button;  
//	              }
//				 else if(fieldName.equals("Complete")){
//					 
//					 
//					 IButton btnComplete = new IButton();  
//					 btnComplete.setHeight(26);  
//					 btnComplete.setWidth(70);
//					 btnComplete.setTitle("Complete");
//	                    
//					 btnComplete.addClickHandler(new ClickHandler() {
//							
//							@Override
//							public void onClick(ClickEvent event) {
//								// TODO Auto-generated method stub
//								
//								selectedRecord = record;
//							    model =getServiceModel(Integer.parseInt(record.getAttribute("ServiceId").trim()));
//							    
//							    if(record.getAttribute("Status").equals("Completed")){
//							    	showDialogMessage("Service already completed");
//							    }
//							    else if(validateCompletion()){
//									Date todayDate=new Date();
//									if(todayDate.before(model.getServiceDate())){
//										System.out.println("Service Date Exceeds");
//										showDialogMessage("Service Date exceeds current date. Please reschedule the date");
//										
//									}else{
//										ShowCompleteServicePopUp();
//										
//									}
//								}
//								
//							}
//
//							
//
//							
//						});
//	                    return btnComplete;
//				 }
//				 else if(fieldName.equals("Reschedule")){
//					 
//					 IButton btnReschedule = new IButton();  
//					 btnReschedule.setHeight(26);  
//					 btnReschedule.setWidth(70);
//					 btnReschedule.setTitle("Reschedule");
//					 btnReschedule.addClickHandler(new ClickHandler() {
//						
//						@Override
//						public void onClick(ClickEvent event) {
//							// TODO Auto-generated method stub
//							
//							selectedRecord = record;
//							 System.out.println("Slected record  Reschedule ==  ServiceId "+selectedRecord.getAttribute("ServiceId"));
//						    model =getServiceModel(Integer.parseInt(record.getAttribute("ServiceId").trim()));
//
//						    if(record.getAttribute("Status").equals("Completed")){
//						    	showDialogMessage("Service already completed");
//						    }else{
//						    	
//						    	reschedulePopup.makeCustBranchListBox(model.getPersonInfo().getCount(), model.getCompanyId(),model.getServiceBranch());
//						    	reschedulePopup.dbserviceDate.setValue(model.getServiceDate());
//						    	reschedulePopup.showReschedulePopup();
//						    }
//						    
//						}
//					});
//	                    return btnReschedule;
//				 }
//				 
//				 else {  
//	                    return null;  
//	                }  
//			}
//
//			
//		};
		
		grid = new  ListGrid();
		
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		setGridProperty(grid);
		grid.setFields(getGridFields());
		
//		grid.setCanEdit(true);
//		grid.setEditEvent(ListGridEditEvent.CLICK);  
//		grid.setEditByCell(true); 
		 grid.setStyleName("GridList");
		ds= new DataSource();
		ds.setClientOnly(true);
		setDataSourceField(ds);
		grid.setDataSource(ds);
		
		verticalpanel.getElement().addClassName("varticalGridPanel");
		grid.getElement().addClassName("technicianSchedule");
		verticalpanel.add(grid);
		
		content.add(horizontalpanel);
		content.add(verticalpanel);
		
		
		
	}
	
	
//	/**
//	 * Date : 21-03-2017 By Anil
//	 * this method checks service complete
//	 */
//	private boolean validateCompletion() {
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
//			System.out.println("Config true");
//			if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
//				System.out.println("Not an admin");
//				DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
//				Date serviceDate=model.getServiceDate();
//				Date lastValidDate=CalendarUtil.copyDate(serviceDate);
//				Console.log("Service Date : "+format.format(serviceDate));
//				System.out.println("Service Date : "+format.format(serviceDate));
//				if(serviceDate!=null){
//					CalendarUtil.addMonthsToDate(lastValidDate, 1);
//					lastValidDate.setDate(5);
//					Console.log("Valid/Deadline date "+format.format(lastValidDate));
//					System.out.println("Valid/Deadline date "+format.format(lastValidDate));
//					Date todaysDate=new Date();
//					lastValidDate=format.parse(format.format(lastValidDate));
//					todaysDate=format.parse(format.format(todaysDate));
//					Console.log("Todays Date : "+format.format(todaysDate));
//					System.out.println("Todays Date : "+format.format(todaysDate));
//					if(todaysDate.after(lastValidDate)){
//						showDialogMessage("Service completion of previous month is not allowed..!");
//						return false;
//					}
//				}else{
//					showDialogMessage("Service date is null");
//					return false;
//				}
//			}
//			return true;
//		}else{
//			System.out.println("Config false");
//			return true;
//		}
//	}
	
	
//	private void getProjectDetails(final Service model, final ListGridRecord record) {
//
//		final int contractId = model.getContractCount();
//		
//		 MyQuerry querry = new MyQuerry();
//		 Vector<Filter> filterVec = new Vector<Filter>();
//		 
//		 Filter filter =null;
//		 filter=new Filter();
//		 filter.setIntValue(model.getCount());
//		 filter.setQuerryString("serviceId");
//		 filterVec.add(filter);
//		 System.out.println("proj =="+record.getAttribute("ProjectId"));
//		 int projectId = Integer.parseInt(record.getAttribute("ProjectId"));
//		 if(projectId!=0){
//			 System.out.println("PROJECTttt");
//			 filter=new Filter();
//			 filter.setIntValue(projectId);
//			 filter.setQuerryString("count");
//			 filterVec.add(filter);
//		 }
//		 
//		 querry.setFilters(filterVec);
//		 querry.setQuerryObject(new ServiceProject());
//		 
//		 genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				// TODO Auto-generated method stub
//				System.out.println("result ="+result.size());
//				
//				loadInvoiceDropDown(contractId);
//				
//				planPopUp.technicianTable.getDataprovider().getList().clear();
//				planPopUp.toolTable.getDataprovider().getList().clear();
//				planPopUp.materialProductTable.getDataprovider().getList().clear();
//				planPopUp.dbQuantity.setValue(0d);
//				if(record.getAttribute("Status").equals("Completed")){
//					planPopUp.SetEnableWidgets(false);
//				}else{
//					planPopUp.SetEnableWidgets(true);
//				}
//				
//				for(SuperModel model :result){
//					ServiceProject project = (ServiceProject) model;
//					planPopUp.technicianTable.getDataprovider().setList(project.getTechnicians());
//					planPopUp.toolTable.getDataprovider().setList(project.getTooltable());
//					planPopUp.materialProductTable.getDataprovider().setList(project.getProdDetailsList());
//					
//					MaterialProductTable.projectId=project.getCount();
//				}
//				planPopUp.dbserviceDate.setValue(model.getServiceDate());
////				setValue(model.getServiceTime(),planPopUp.servicetime);
//				
//				setpopupServiceTime(selectedRecord.getAttribute("ServiceTime"));
//				
//				if(model.getTechPlanOtherInfo()!=null)
//				planPopUp.getTbOther().setValue(model.getTechPlanOtherInfo());;
//				
//			  // get invoice id and date from table
////				if(invoiceId!=0){
////					planPopUp.getOlbInvoiceId().setValue(invoiceId+"");
////				}else{
////					planPopUp.getOlbInvoiceId().setSelectedIndex(0);
////				}
////				if(invoiceDate!=null){
////					planPopUp.getOlbInvoiceDate().setValue(DateTimeFormat.getShortDateFormat().format(invoiceDate));	
////				}else{
////					planPopUp.getOlbInvoiceDate().setSelectedIndex(0);
////				}
//				
//				planPopUp.showPopUp();
//			}
//			
//		
//
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				SC.say("Unexpected error occurred");
//			}
//		});
//		 
//	}
	
	
//	private void setpopUpInvoiceIdDate(int invoiceId, Date invoiceDate) {
//		// TODO Auto-generated method stub
//		int invoiceIdcount  = planPopUp.getOlbInvoiceId().getItemCount();
//		String invoiceid = invoiceId+"";
//		if(invoiceId!=0){
//			for(int i=0;i<invoiceIdcount;i++){
//				if(invoiceid.trim().equals(planPopUp.getOlbInvoiceId().getItemText(i))){
//					planPopUp.getOlbInvoiceId().setSelectedIndex(i);
//					break;
//				}
//			}
//		}
//		
//		int invoiceIDDateCount = planPopUp.getOlbInvoiceDate().getItemCount();
//		
//		if(invoiceDate!=null){
//			String invoiceIdDate = DateTimeFormat.getShortDateFormat().format(invoiceDate);
//			for(int i=0;i<invoiceIDDateCount;i++){
//				if(invoiceIdDate.trim().equals(planPopUp.getOlbInvoiceDate().getItemText(i))){
//					planPopUp.getOlbInvoiceDate().setSelectedIndex(i);
//					break;
//				}
//			}
//		}
//	}
	
//	private void loadInvoiceDropDown(int contractId) {
//
//		MyQuerry querry = new MyQuerry();
//		Vector<Filter> filtervec=new Vector<Filter>();
//		Filter filter =null;
//		
//		filter= new Filter();
//		filter.setQuerryString("contractCount");
//		filter.setIntValue(contractId);
//		filtervec.add(filter);
//		
//		filter= new Filter();
//		filter.setQuerryString("status");
//		filter.setStringValue("Approved");
//		filtervec.add(filter); 
//		
//		filter= new Filter();
//		filter.setQuerryString("typeOfOrder");
//		filter.setStringValue(AppConstants.ORDERTYPESERVICE);
//		filtervec.add(filter);
//		
//		filter= new Filter();
//		filter.setQuerryString("accountType");
//		filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAR);
//		filtervec.add(filter);
//		
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new Invoice());
//		
//		planPopUp.olbInvoiceId.removeAllItems();
//		planPopUp.olbInvoiceDate.removeAllItems();
//		
//		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				// TODO Auto-generated method stub
//				ArrayList<Invoice> list = new ArrayList<Invoice>();
//				Console.log("Invoice size=="+result.size());
//				
//				planPopUp.olbInvoiceId.addItem("--SELECT--");
//				planPopUp.olbInvoiceDate.addItem("--SELECT--");
//				for(SuperModel model : result){
//					Invoice invoice = (Invoice) model;
//					planPopUp.olbInvoiceId.addItem(invoice.getCount()+"");
//					planPopUp.olbInvoiceDate.addItem(DateTimeFormat.getShortDateFormat().format(invoice.getInvoiceDate()));
//					list.add(invoice);
//				}
//				planPopUp.olbInvoiceId.setItems(list);
//				planPopUp.olbInvoiceDate.setItems(list);
//				
//				System.out.println("INVOICE IDDDDD ="+selectedRecord.getAttributeAsInt("InvoiceId"));
//				System.out.println("INVOICE DATEEEE ="+selectedRecord.getAttributeAsDate("InvoiceDate"));
//
//				int invoiceId = selectedRecord.getAttributeAsInt("InvoiceId");
//				Date invoiceDate = selectedRecord.getAttributeAsDate("InvoiceDate");
//				
//				setpopUpInvoiceIdDate(invoiceId,invoiceDate);
//			}
//			
//		
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
////				SC.say("Unexpected Error");
//			}
//		});
//	}
	
	
	
//	private void setpopupServiceTime(String serviceTime) {
//
//		if(!serviceTime.equals("")&&!serviceTime.equals("Flexible")){
//			
//			String[] time=serviceTime.split(":");
//			System.out.println(" 0 "+time[0]);
//			System.out.println(" 1 "+time[1]);
//			String hrs=time[0];
//			String min;
//			String ampm;
//			if(time[1].contains("PM")){
//				min=time[1].replace("PM", "");
//				ampm="PM";
//			}else{
//				min=time[1].replace("AM", "");
//				ampm="AM";
//			}
//			System.out.println(" 2 "+hrs+" "+min+" "+ampm);
//			
//			int count = planPopUp.servicetime.getItemCount();
//			String item=hrs.toString();
//			System.out.println("Hours =="+item);
//			for(int i=0;i<count;i++)
//			{
//				System.out.println("HTRRSSS =="+planPopUp.servicetime.getItemText(i).trim());
//				System.out.println(item.trim().equals(planPopUp.servicetime.getItemText(i).trim()));
//				if(item.trim().equals(planPopUp.servicetime.getItemText(i).trim()))
//				{
//					planPopUp.servicetime.setSelectedIndex(i);
//					break;
//				}
//			}
//			
//			int count1 = planPopUp.p_servicemin.getItemCount();
//			String item1=min.toString();
//			for(int i=0;i<count1;i++)
//			{
//				if(item1.trim().equals(planPopUp.p_servicemin.getItemText(i).trim()))
//				{
//					planPopUp.p_servicemin.setSelectedIndex(i);
//					break;
//				}
//			}
//			
//			int count2 = planPopUp.p_ampm.getItemCount();
//			String item2=ampm.toString();
//			for(int i=0;i<count2;i++)
//			{
//				if(item2.trim().equals(planPopUp.p_ampm.getItemText(i).trim()))
//				{
//					planPopUp.p_ampm.setSelectedIndex(i);
//					break;
//				}
//			}
//			
//		}else{
//			planPopUp.servicetime.setSelectedIndex(0);
//			planPopUp.p_servicemin.setSelectedIndex(0);
//			planPopUp.p_ampm.setSelectedIndex(0);
//		}
//	}
	
	public void setValue(String value, ListBox servicetime) {
		int count = servicetime.getItemCount();
		String item=value.toString();
		System.out.println("Item ===");
		for(int i=0;i<count;i++)
		{
			if(item.trim().equals(servicetime.getItemText(i).trim()))
			{	System.out.println("Time set ------");
				servicetime.setSelectedIndex(i);
				break;
			}
		}
	}
	
	protected List<TechnicianMaterialInfo> getproductList(ServiceProject project) {
		List<TechnicianMaterialInfo> list = new ArrayList<TechnicianMaterialInfo>();
		for(int i=0;i<project.getTooltable().size();i++){
			
			TechnicianMaterialInfo materialInfo = new TechnicianMaterialInfo();
			materialInfo.setProductName(project.getProdDetailsList().get(i).getName());
			materialInfo.setQuantity(project.getProdDetailsList().get(i).getQuantity());
			materialInfo.setWarehouseName(project.getProdDetailsList().get(i).getWarehouse());
			materialInfo.setStorageLocation(project.getProdDetailsList().get(i).getStorageLocation());
			materialInfo.setStorageBin(project.getProdDetailsList().get(i).getStorageBin());
			
			list.add(materialInfo);
		}

		System.out.println("list =="+list.size());	
		return list;
	}

	
//	/** Service wise complete button popup **************/
//	private void ShowCompleteServicePopUp() {
//		// TODO Auto-generated method stub
//		completeServicePopUp.getLbRating().setSelectedIndex(0);
//		completeServicePopUp.getServicetime().setSelectedIndex(0);
//		completeServicePopUp.getP_servicemin().setSelectedIndex(0);
//		completeServicePopUp.getP_ampm().setSelectedIndex(0);
//		completeServicePopUp.getTo_servicehours().setSelectedIndex(0);
//		completeServicePopUp.getTo_servicemin().setSelectedIndex(0);
//		completeServicePopUp.getTo_ampm().setSelectedIndex(0);
//		completeServicePopUp.getDbserviceCompletionDate().setValue(new Date());
//		completeServicePopUp.getTbRemark().setValue("");
//		/** date 15/11/2017 added by komal to clear previous value of service duration **/
//		completeServicePopUp.getIbserviceDuration().setValue(0);
//		completeServicePopUp.showPopUp();
//		
//		addDefaultValueInMarkCompletePopUp(completeServicePopUp);
//	}
	
//	private void addDefaultValueInMarkCompletePopUp(ServiceCompletetionPopup markcompletepopup2) {
//		
//		Console.log("Inside addDefaultValue");
//		Console.log("model.getServiceCompleteDuration() :::"+model.getServiceCompleteDuration());
//		Console.log("model.getServiceCompletionDate():::"+model.getServiceCompletionDate());
//		Console.log("model.getServiceCompleteRemark():::"+model.getServiceCompleteRemark());
//
//		if (model.getServiceCompleteDuration() != 0)
//			markcompletepopup2.getIbserviceDuration().setValue(
//					model.getServiceCompleteDuration());
//
//		if (model.getServiceCompletionDate() != null) {
//			DateTimeFormat sdf = DateTimeFormat.getFormat("dd/MM/yyyy");
//
//			markcompletepopup2.getDbserviceCompletionDate().setValue(
//					sdf.parse(sdf.format(model.getServiceCompletionDate())));
//
//		}
//
//		if (model.getServiceCompleteRemark() != null
//				&& !model.getServiceCompleteRemark().equalsIgnoreCase("")) {
//			markcompletepopup2.getTbRemark().setValue(
//					model.getServiceCompleteRemark().trim());
//		}
//
//		if (model.getCustomerFeedback() != null
//				&& !model.getCustomerFeedback().equalsIgnoreCase("")) {
//			int index = 0;
//			for (int i = 0; i < markcompletepopup2.getLbRating().getItemCount(); i++) {
//				if (model
//						.getCustomerFeedback()
//						.trim()
//						.equalsIgnoreCase(
//								markcompletepopup2.getLbRating().getValue(i))) {
//					index = i;
//				}
//			}
//			
//			Console.log("index:::"+index);
//			markcompletepopup2.getLbRating().setSelectedIndex(index);
//		}
//		
//	}
	
	public void setDataToListGrid(ArrayList<Service> list){
		System.out.println("FORM SETTING DATA....");
		try {
			ds.setTestData(getGridData(list));
			grid.setDataSource(ds);
			grid.setFields(getGridFields());
			grid.fetchData();
			
		} catch (Exception e) {
			System.out.println("grid"+grid);
			System.out.println("Exception =="+e);
			System.out.println(e.getMessage());
		}
		
		
	}
	
	private static ListGridRecord[] getGridData(ArrayList<Service> servicelist) {
		 
		 ListGridRecord[] records = null; 
		 records = new ListGridRecord[servicelist.size()];
		 
		 DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");

		 for(int i=0;i<servicelist.size();i++){
			 ListGridRecord record = new ListGridRecord();
				 
			 Service service=servicelist.get(i);
			 
			 record.setAttribute("Number", service.getSchedulingNumber());
			 record.setAttribute("CustomerName", service.getPersonInfo().getFullName());
			 record.setAttribute("SrNo", service.getServiceSerialNo());

			 record.setAttribute("ServiceName", service.getProductName());
			 record.setAttribute("Technician", service.getEmployee()); 
			 record.setAttribute("ServiceDate", format.format(service.getServiceDate()));
			 record.setAttribute("ServiceTime", service.getServiceTime());
			 
			record.setAttribute("CustomerCell", service.getPersonInfo().getCellNumber());
			 record.setAttribute("Locality",service.getLocality());
			 record.setAttribute("Status", service.getStatus());
			 record.setAttribute("Address", service.getAddress().getCompleteAddress());
			 record.setAttribute("RefNo", service.getRefNo());

			 record.setAttribute("ServiceId", service.getCount());

			 record.setAttribute("ContractId", service.getContractCount());
			 record.setAttribute("ServiceType",service.getServiceType());

			 
			 record.setAttribute("ProjectId", service.getProjectId());
			 record.setAttribute("InvoiceId", service.getInvoiceId());
			 if(service.getInvoiceDate()!=null){
				 record.setAttribute("InvoiceDate", service.getInvoiceDate());

			 }else{
				 record.setAttribute("InvoiceDate", "");

			 }
			 if(service.getServiceSerialNo()==1){
				 record.setCustomStyle("technicianSchedulingRecord");
			 }
			 /** date 15/11/2017 added by komal to show complaint services in red color **/
			 Console.log("ticket number :" + service.getTicketNumber()) ;
			 if(service.getTicketNumber() != -1){
				 Console.log("my ticket number :" + service.getTicketNumber()) ;
				 record.setCustomStyle("technicianSchedulingRecord1");
			 }
			 
			 record.setAttribute("Plan", "Plan.png");
			 record.setAttribute("Complete", "Complete.png");
			 record.setAttribute("Reschedule", "Reschedule.png");
			 
			 record.setAttribute("ContactDetail", "ContactDetail.png");//Added by Ashwini
			 
			 records[i]=record;
		 }
		 System.out.println("RECORDS LENGTH : "+records.length);
		 
		 return records;
	}
		

		//fields
		private static ListGridField[] getGridFields() {	   
			   
		   ListGridField SrNo = new ListGridField("SrNo","Sr No",80); //,40
		   SrNo.setWrap(true);
		   SrNo.setAlign(Alignment.CENTER);
		   SrNo.setCanEdit(false);
		  
		   
		   
		   ListGridField ServiceDate = new ListGridField("ServiceDate","Service Date",100);
		   ServiceDate.setWrap(true);
		   ServiceDate.setAlign(Alignment.CENTER);
		   ServiceDate.setCanEdit(false);
		   
		   ListGridField ServiceTime = new ListGridField("ServiceTime","Service Time",80); //,70
		   ServiceTime.setWrap(true);
		   ServiceTime.setAlign(Alignment.CENTER);
		   ServiceTime.setCanEdit(false);
		   
		   ListGridField CustomerName = new ListGridField("CustomerName","Customer Name",90); //,80
		   CustomerName.setWrap(true);
		   CustomerName.setAlign(Alignment.CENTER);
		   CustomerName.setCanEdit(false);
		   
		   ListGridField field5 = new ListGridField("CustomerCell","Customer Cell",80); //,80
		   field5.setWrap(true);
		   field5.setAlign(Alignment.CENTER);
		   field5.setCanEdit(false);
		   field5.setHidden(true);//added by Ashwini
		   
		   ListGridField field6 = new ListGridField("Locality","Locality",80); //,80
		   field6.setWrap(true);
		   field6.setAlign(Alignment.CENTER);
		   field6.setCanEdit(false);
		   
		   ListGridField field7 = new ListGridField("Status","Status",70); //,70
		   field7.setWrap(true);
		   field7.setAlign(Alignment.CENTER);
		   field7.setCanEdit(false);
		   
		   ListGridField field8 = new ListGridField("Address","Address",100); //,130
		   field8.setWrap(true);
		   field8.setAlign(Alignment.CENTER);
		   field8.setCanEdit(false);
		   
		   ListGridField field10 = new ListGridField("RefNo","Ref No",80); //,80 
		   field10.setWrap(true);
		   field10.setAlign(Alignment.CENTER);
		   field10.setCanEdit(false);
		   
		   
//		   ListGridField btnschedule = new ListGridField("Plan","Plan",80); //,80 
//		   btnschedule.setWrap(true);
//		   btnschedule.setAlign(Alignment.CENTER);
//		   btnschedule.setCanEdit(false);
		   
		   
		   ListGridField plan = new ListGridField("Plan","Plan",80);
		   plan.setWrap(true);
		   plan.setType(ListGridFieldType.IMAGE);
		   plan.setAlign(Alignment.CENTER);
		   plan.setDefaultValue("Plan.png");
		   plan.setCanEdit(false);
		   plan.setImageSize(80);
		   plan.setImageHeight(30);
		   

//		   ListGridField btnComplete = new ListGridField("Complete","Complete",80); //,80 
//		   btnComplete.setWrap(true);
//		   btnComplete.setAlign(Alignment.CENTER);
//		   btnComplete.setCanEdit(false);
		   
		   ListGridField btnComplete = new ListGridField("Complete","Complete",110);
		   btnComplete.setWrap(true);
		   btnComplete.setType(ListGridFieldType.IMAGE);
		   btnComplete.setAlign(Alignment.CENTER);
		   btnComplete.setDefaultValue("Complete.png");
		   btnComplete.setCanEdit(false);
		   btnComplete.setImageSize(110);
		   btnComplete.setImageHeight(30);
		   
//		   ListGridField btnReschedule = new ListGridField("Reschedule","Reschedule",90); //,100 
//		   btnReschedule.setWrap(true);
//		   btnReschedule.setAlign(Alignment.CENTER);
//		   btnReschedule.setCanEdit(false); 
		   
		   
		   ListGridField btnReschedule = new ListGridField("Reschedule","Reschedule",120);
		   btnReschedule.setWrap(true);
		   btnReschedule.setType(ListGridFieldType.IMAGE);
		   btnReschedule.setAlign(Alignment.CENTER);
		   btnReschedule.setDefaultValue("Reschedule.png");
		   btnReschedule.setCanEdit(false);
		   btnReschedule.setImageSize(120);
		   btnReschedule.setImageHeight(30);
		   
		   /*
		    * Date:11/08/2018
		    * Developer:Ashwini
		    * Des:to add contact details(cell no.2 and landline no)
		    */
		   ListGridField btnContact = new ListGridField("ContactDetail","ContactDetail",120);
		   btnContact.setWrap(true);
		   btnContact.setType(ListGridFieldType.IMAGE);
		   btnContact.setAlign(Alignment.CENTER);
		   btnContact.setDefaultValue("ContactDetail.png");
		   btnContact.setCanEdit(false);
		   btnContact.setImageSize(120);
		   btnContact.setImageHeight(30);
		   
		   /*
		    * End by Ashwini
		    */
		   
		   ListGridField ServiceName = new ListGridField("ServiceName","Service Name",100); //,100 
		   ServiceName.setWrap(true);
		   ServiceName.setAlign(Alignment.CENTER);
		   ServiceName.setCanEdit(false);
		   
		   
		   ListGridField ServiceId = new ListGridField("ServiceId","Service Id",80); //,80 
		   ServiceId.setWrap(true);
		   ServiceId.setAlign(Alignment.CENTER);
		   ServiceId.setCanEdit(false);
		   
		  
		   
		   ListGridField Number = new ListGridField("Number","Number",80); //,80 
		   Number.setWrap(true);
		   Number.setAlign(Alignment.CENTER);
		   Number.setCanEdit(false);
		   
		   ListGridField Employee = new ListGridField("Technician","Technician",80); //,80 
		   Employee.setWrap(true);
		   Employee.setAlign(Alignment.CENTER);
		   Employee.setCanEdit(false);
		   
		   ListGridField ContractId = new ListGridField("ContractId","ContractId",80); //,80 
		   ContractId.setWrap(true);
		   ContractId.setAlign(Alignment.CENTER);
		   ContractId.setCanEdit(false);
		   
		   ListGridField ServiceType = new ListGridField("ServiceType","ServiceType",80); //,80 
		   ServiceType.setWrap(true);
		   ServiceType.setAlign(Alignment.CENTER);
		   ServiceType.setCanEdit(false);
		   
		   ListGridField ProjectId = new ListGridField("ProjectId","ProjectId",80); //,80 
		   ProjectId.setWrap(true);
		   ProjectId.setAlign(Alignment.CENTER);
		   ProjectId.setCanEdit(false);
		   ProjectId.setHidden(true);
		   
		   ListGridField InvoiceId = new ListGridField("InvoiceId","InvoiceId",80); //,80 
		   InvoiceId.setWrap(true);
		   InvoiceId.setAlign(Alignment.CENTER);
		   InvoiceId.setCanEdit(false);
		   InvoiceId.setHidden(true);
		   
		   ListGridField InvoiceDate = new ListGridField("InvoiceDate","InvoiceDate",80); //,80 
		   InvoiceDate.setWrap(true);
		   InvoiceDate.setAlign(Alignment.CENTER);
		   InvoiceDate.setCanEdit(false);
		   InvoiceDate.setHidden(true);
		   
		 
		   
		   return new ListGridField[] { 
//				   plan,btnComplete,btnReschedule,Number,CustomerName,SrNo,ServiceName,Employee, ServiceDate ,ServiceTime,field5,field6,field7,field8,
//				   field10,ServiceId, ContractId,ServiceType,ProjectId,InvoiceId,InvoiceDate };
				  
				   /*
				    * Add by Ashwini
				    */
				   
				   plan,btnComplete,btnReschedule,Number,CustomerName,SrNo,ServiceName,Employee, ServiceDate ,ServiceTime,field5,field6,field7,field8,
				   field10,ServiceId, ContractId,ServiceType,btnContact,ProjectId,InvoiceId,InvoiceDate   };
		   
}   
		
		//data source field
		protected static void setDataSourceField(DataSource dataSource) {
		
			DataSourceIntegerField pkField = new DataSourceIntegerField("pk");  
	         pkField.setHidden(true);  
	         pkField.setPrimaryKey(true);
		    
		    DataSourceField number = new DataSourceField("Number", FieldType.TEXT);
			DataSourceField customerName = new DataSourceField("CustomerName", FieldType.TEXT);

		   DataSourceField SrNo = new DataSourceField("SrNo", FieldType.TEXT);
		   DataSourceField serviceDate = new DataSourceField("ServiceDate", FieldType.TEXT);
		   DataSourceField field3 = new DataSourceField("ServiceTime", FieldType.TEXT);
		   DataSourceField field5 = new DataSourceField("CustomerCell", FieldType.TEXT);
		   DataSourceField field6 = new DataSourceField("Locality", FieldType.TEXT);
		   DataSourceField field7 = new DataSourceField("Status", FieldType.TEXT);
		   DataSourceField field8 = new DataSourceField("Address", FieldType.TEXT);
		   DataSourceField field10 = new DataSourceField("RefNo", FieldType.TEXT);
		   DataSourceField field11 = new DataSourceField("State", FieldType.TEXT);
		   
		   DataSourceField serviceName = new DataSourceField("ServiceName", FieldType.TEXT);

		   DataSourceField serviceId = new DataSourceField("ServiceId", FieldType.TEXT);
		   
		   
		   
		   DataSourceField employee = new DataSourceField("Technician", FieldType.TEXT);

		   DataSourceField ProjectId = new DataSourceField("ProjectId", FieldType.TEXT);

		   DataSourceField ContractId = new DataSourceField("ContractId", FieldType.TEXT);
		   
		   DataSourceField ServiceType = new DataSourceField("ServiceType", FieldType.TEXT);

		   DataSourceField InvoiceId = new DataSourceField("InvoiceId", FieldType.TEXT);
		
		   DataSourceDateField InvoiceDate = new DataSourceDateField("InvoiceDate");
		   
		  
		   
		   DataSourceField btnschedule = new DataSourceField("Plan", FieldType.IMAGE);
		   DataSourceField Reschedule = new DataSourceField("Reschedule", FieldType.IMAGE);
		   DataSourceField btnComplete = new DataSourceField("Complete", FieldType.IMAGE);
		   
		   DataSourceField btnContact = new DataSourceField("ContactDetail", FieldType.IMAGE);//added by ashwini
		   

//		   dataSource.setFields(pkField,  btnschedule, Reschedule,btnComplete,  number, customerName,SrNo,serviceName, employee, serviceDate ,field3,field5,field6,field7,field8
//				   ,field10,field11,serviceId,ContractId,ServiceType,ProjectId,InvoiceId,InvoiceDate);
		   
		   /*
		    * Added by Ashwini
		    */
		   dataSource.setFields(pkField,  btnschedule, Reschedule,btnComplete,  number, customerName,SrNo,serviceName, employee, serviceDate ,field3,field5,field6,field7,field8
				   ,field10,field11,serviceId,ContractId,ServiceType,btnContact,ProjectId,InvoiceId,InvoiceDate);
		}
		
		
		//grid property
		private static void setGridProperty(ListGrid grid) {
		   grid.setWidth("83%");
		   grid.setHeight("63%");
		   
//		   grid.setWidth("70%");
//		   grid.setHeight("50%");
		   
		   grid.setAutoFetchData(true);
		   grid.setAlternateRecordStyles(true);
		   grid.setShowAllRecords(true);
		   grid.setShowFilterEditor(true);
		   grid.setFilterOnKeypress(true);
		 
//		   grid.setBodyOverflow(Overflow.SCROLL);
//		   grid.setOverflow(Overflow.SCROLL);
		   grid.setWrapCells(true);	
		   grid.setFixedRecordHeights(false);
//		   grid.setAutoFitData(Autofit.BOTH);
//		   grid.setAutoFitData(Autofit.HORIZONTAL);	
//		   grid.setAutoFitWidthApproach(AutoFitWidthApproach.BOTH);	
		   grid.setCanResizeFields(true);
		   
		   grid.setCanFreezeFields(true);
		   
		   // for animation
		   grid.setShowRecordComponents(true);
		   grid.setShowRecordComponentsByCell(true);
		   
		   /**
		    * @author Anil
		    * @since 07-01-2021
		    * for resizing smartgrid
		    */
		   if(AppMemory.getAppMemory().enableMenuBar){
			   grid.setWidth("100%");
		   }
//		   
//		   grid.setShowRollOverCanvas(true);
//		   grid.setAnimateRollUnder(true);  
//		   
//		   final Canvas rollUnderCanvasProperties = new Canvas();  
//	        rollUnderCanvasProperties.setAnimateFadeTime(1000);  
//	        rollUnderCanvasProperties.setAnimateShowEffect(AnimationEffect.FADE);  
//	        rollUnderCanvasProperties.setBackgroundColor("#00ffff");  
//	        rollUnderCanvasProperties.setOpacity(50);  
//	        //can also override ListGrid.getRollUnderCanvas(int rowNum, int colNum)  
//	        grid.setRollUnderCanvasProperties(rollUnderCanvasProperties);  
//	  
//	        grid.setShowSelectionCanvas(true);
//	        grid.setAnimateSelectionUnder(true);  
//	       
//	  
//	        final Canvas selectionUnderCanvasProperties = new Canvas();  
//	        selectionUnderCanvasProperties.setAnimateShowEffect(AnimationEffect.FADE);  
//	        selectionUnderCanvasProperties.setAnimateFadeTime(1000);  
//	        selectionUnderCanvasProperties.setBackgroundColor("#ffff40");  
//	        grid.setSelectionUnderCanvasProperties(selectionUnderCanvasProperties);  
	  
		}
				
	 /**
	 * Toggles the app header bar menus as per screen state
	 */
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				/** date 11/11/2017 added by komal for download button **/
				if(text.equals("Print")||text.equals("Search") || text.equals("Download"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		  		
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Print")||text.equals("Search")|| text.equals("Download") )
					menus[k].setVisible(true); 
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Print")||text.equals("Search")|| text.equals("Download"))
					menus[k].setVisible(true); 
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.TECHNICIANSCHEDULNG,LoginPresenter.currentModule.trim());
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	
//	private void showServiceDetailsPopup() {
//
//		Window	 winModal = new Window();
//		winModal.setWidth(700);
//        winModal.setHeight(700);
//		winModal.setAutoSize(true);
//        winModal.setTitle("Schedule");
//        winModal.setShowMinimizeButton(false);
//        winModal.setIsModal(true);
//        winModal.setShowModalMask(true);
//        winModal.centerInPage();
//        winModal.setMargin(10);
//        
//        
//	}

	private Service getServiceModel(int serviceId) {
		
//		getServiceList(serviceId);
		
		for(Service service:TechnicianScheduleListPresenter.globalServicelist){
			if(service.getCount()==serviceId){
				return service;
			}
		}
		
		return null;
	}


	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		

//		if(event.getSource()==planPopUp.btnOk){
//			
//			if(ValidatePopupData()){
//				
//				final ArrayList<EmployeeInfo> technicianlist = new ArrayList<EmployeeInfo>();
//				technicianlist.addAll(planPopUp.technicianTable.getDataprovider().getList());
//				
//				final String TechnicianName = getTechnicianName(planPopUp.technicianTable.getDataprovider().getList());
//
//				final ArrayList<CompanyAsset> technicianToollist = new ArrayList<CompanyAsset>();
//				technicianToollist.addAll(planPopUp.toolTable.getDataprovider().getList());
//				
//				final ArrayList<ProductGroupList> materialInfo = new ArrayList<ProductGroupList>();
//				materialInfo.addAll(planPopUp.materialProductTable.getDataprovider().getList());
//				
//				selectedRecord.setAttribute("ServiceDate", planPopUp.dbserviceDate.getValueAsDate());
//				
//				String serviceHrs=planPopUp.servicetime.getItemText(planPopUp.servicetime.getSelectedIndex());
//				String serviceMin=planPopUp.p_servicemin.getItemText(planPopUp.p_servicemin.getSelectedIndex());
//				String serviceAmPm=planPopUp.p_ampm.getItemText(planPopUp.p_ampm.getSelectedIndex());
//				String totalTime = "";
//				final String calculatedServiceTime;
//				
//				totalTime = serviceHrs+":"+serviceMin+""+serviceAmPm;
//				if(totalTime.equalsIgnoreCase("--:----")){
//					calculatedServiceTime = "Flexible";
//				}else{
//					calculatedServiceTime = totalTime;
//				}
//				
//				selectedRecord.setAttribute("ServiceTime", calculatedServiceTime);
//				
//
//				final int InvoiceId ;
//				if(planPopUp.getOlbInvoiceId().getSelectedIndex()!=0){
//					InvoiceId  = Integer.parseInt(planPopUp.getOlbInvoiceId().getValue(planPopUp.getOlbInvoiceId().getSelectedIndex()));
//					selectedRecord.setAttribute("InvoiceId", InvoiceId);
//				}else{
//					InvoiceId = 0;
//				}
//				final String invoiceDate;
//				if(planPopUp.getOlbInvoiceDate().getSelectedIndex()!=0){
//					invoiceDate = planPopUp.getOlbInvoiceDate().getValue(planPopUp.getOlbInvoiceDate().getSelectedIndex());
//					selectedRecord.setAttribute("InvoiceDate", invoiceDate);
//                 }else{
//                	 invoiceDate = null;
//                 }
//				final int projectId = selectedRecord.getAttributeAsInt("ProjectId");
//				System.out.println("Project Id=="+projectId);
//				
//				if(planPopUp.getTbOther().getValue()!=null&&!planPopUp.getTbOther().getValue().equals("")){
//					model.setTechPlanOtherInfo(planPopUp.getTbOther().getValue());
//				}
//				
//				System.out.println("flag value :" + technianValidateflag);
//				
//					System.out.println("all komal : " + technicianlist.size() +" "+ planPopUp.dbserviceDate.getValueAsDate() +" " + calculatedServiceTime);
//					ArrayList<String> statuslist = new ArrayList<String>();
//					statuslist.add("Scheduled");
////					statuslist.add("Completed");
//					statuslist.add("Rescheduled");
//				
//					// plus seven days to date
//					Date date = new Date();
//					
//					// add 30 days to date
//					Date addThirtyDate = new Date();
//					CalendarUtil.addDaysToDate(addThirtyDate, 15);
//					
//					Vector<Filter> filtervec=new Vector<Filter>();
//					Filter filter = null;
//					MyQuerry querry;
//					
//					filter = new Filter();
//					filter.setQuerryString("companyId");
//					filter.setLongValue(UserConfiguration.getCompanyId());
//					filtervec.add(filter);
//					
//					filter = new Filter();
//					filter.setQuerryString("status IN");
//					filter.setList(statuslist);
//					filtervec.add(filter);
//					
//					filter = new Filter();
//					filter.setQuerryString("serviceDate >=");
//					filter.setDateValue(date);
//					filtervec.add(filter);
//					
//					filter = new Filter();
//					filter.setQuerryString("serviceDate <=");
//					filter.setDateValue(addThirtyDate);
//					filtervec.add(filter);
//					
//					querry=new MyQuerry();
//					querry.setQuerryObject(new Service());
//					querry.setFilters(filtervec);
//					
//					genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//						@Override
//						public void onSuccess(ArrayList<SuperModel> result) {
//							System.out.println("RESULT SIZE : "+result.size());		
//							label : for(SuperModel model:result){
//								Service customerEntity=(Service) model;	
//								if(!(calculatedServiceTime.equalsIgnoreCase("Flexible")))
//								{
//								for(EmployeeInfo emp : technicianlist){
//									System.out.println("All values komal 1:" + emp.getFullName() +" " +customerEntity.getEmployee() +" "+format1.format(planPopUp.dbserviceDate.getValueAsDate())+" "+format1.format(customerEntity.getServiceDate())  +" " +calculatedServiceTime +" " +customerEntity.getServiceTime());
//									
//									if(emp.getFullName().equals(customerEntity.getEmployee()) && format1.format(planPopUp.dbserviceDate.getValueAsDate()).equals(format1.format(customerEntity.getServiceDate())) && calculatedServiceTime.equals(customerEntity.getServiceTime())){
//										System.out.println("All values komal 2:" + emp.getFullName() +" " +customerEntity.getEmployee() +" "+format1.format(planPopUp.dbserviceDate.getValueAsDate())+" "+format1.format(customerEntity.getServiceDate()) +" " +calculatedServiceTime +" " +customerEntity.getServiceTime());
//										technianValidateflag = false;
//										break label;
//										
//									}
//									}
//								}
//								
//							}
//							
//							System.out.println("Flag value :" + technianValidateflag);
//							if (technianValidateflag == false) {
//								showDialogMessage("Selected technician is already assigned to other service at inserted date and time");
//								technianValidateflag = true;
//								} else {
//							    technicianPlanInfoSync.TechnicianSchedulingPlan(model,technicianlist,technicianToollist,materialInfo,planPopUp.dbserviceDate.getValueAsDate(),calculatedServiceTime,InvoiceId,invoiceDate,TechnicianName,projectId, new AsyncCallback<Integer>() {
//								
//								@Override
//								public void onSuccess(Integer result) {
//									// TODO Auto-generated method stub
//									System.out.println("iddddiiddd - - "+result);
//									selectedRecord.setAttribute("ProjectId", result);
//									grid.redraw();
//									showDialogMessage("Saved Succesfully");
//									planPopUp.hidePopUp();
//									
//								}
//				
//								@Override
//								public void onFailure(Throwable caught) {
//									// TODO Auto-generated method stub
//									planPopUp.hidePopUp();
//								}
//							});
//							
//						}
//						}
//						
//						@Override
//						public void onFailure(Throwable caught) {
//							System.out.println("Failed");
//							Console.log("RPC Failed");
//						}
//					});
//
//				
//			}
//			
//			
//		}
		
		System.out.println("Hi vijay on click === plan complete");
		// plan popup complete button
//		if(event.getSource() == planPopUp.getBtnComplete()){
//			System.out.println("plan popup btn complete");
//			
//			  if(validateCompletion()){
//					Date todayDate=new Date();
//					if(todayDate.before(model.getServiceDate())){
//						System.out.println("Service Date Exceeds");
//						showDialogMessage("Service Date exceeds current date. Please reschedule the date");
//						
//					}else{
//						ShowCompleteServicePopUp();
//						
//					}
//				}
//		}
		
		// plan poup rechedule button
//		if(event.getSource() == planPopUp.btnReschedule){
//			if(validateRechedule()){
//				selectedRecord.setAttribute("Status", "Rescheduled");
//				selectedRecord.setAttribute("ServiceDate", planPopUp.dbserviceDate.getValueAsDate());
//				
//				String serviceHrs =  planPopUp.getServicetime().getValue(planPopUp.servicetime.getSelectedIndex());
//				String serviceMin = planPopUp.getP_servicemin().getValue(planPopUp.getP_servicemin().getSelectedIndex());
//				String serviceAmPm = planPopUp.getP_ampm().getValue(planPopUp.getP_ampm().getSelectedIndex());
//				String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
//				
//				selectedRecord.setAttribute("ServiceTime", calculatedServiceTime);
//				saveReschedule(model,planPopUp.tbRecheduleReason.getValue(),planPopUp.dbserviceDate.getValueAsDate(),calculatedServiceTime,null );
//				
//				grid.redraw();
//				showDialogMessage("Service Rescheduled Successfully");
//				planPopUp.olbSpecificReason.setSelectedIndex(0);
//				planPopUp.tbRecheduleReason.setValue("");
//				/** date 11/11/2017 added by komal to hide popup after rescheduling through plan popup **/
//				planPopUp.hidePopUp();
//			}
//		}
		
		//complete service popup
//		if(event.getSource() == completeServicePopUp.getBtnOk()){
//			
//			if(completeServicePopUp.getDbserviceCompletionDate().getValueAsDate()==null){
//				showDialogMessage("Please add Completetion Date");
//			}
//			else if(model.isRateContractService()){
//				if(completeServicePopUp.getDbserviceQuantity().getValue()==null){
//					showDialogMessage("Please add Quantity");
//				}
//				else if(completeServicePopUp.getOlbUnitOfMeasurement().getSelectedIndex()==0){
//					showDialogMessage("Please Select Unit Of Measurement ");
//				}
//				
//			}
//			else{
//				ReactOnServiceCompletionPopUp(event);
//			}
//			
//		}
//		if(event.getSource() == completeServicePopUp.getBtnCancel()){
//			planPopUp.hidePopUp();
//			completeServicePopUp.hidePopUp();
//		}
		
		//print schedule popup btns click
		
		/** DATE :27/12/2017 By : Manisha
		 *  To do navigation from assessmentschduling list to assessmentReport
		 */
		if(event.getSource()==btnassess){
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Assessment Report",Screen.ASSESMENTREPORT);
			AssesmentReportPresenter.initalize();
		}
		/**End**/
		
		if(event.getSource() == schedulePrintPopup.getBtnOk()){
			if(validateSchedulePrintPopup()){

				// write code for selected filter for PDF
				
				/**
				 * Date 25/10/2017
				 * By Jayshree 
				 * Changes are made to call the intech pest control technician schedule pdf
				 */
				schedulePrintPopup.getWinModal().hide();
//				String technicianName = schedulePrintPopup.getOlbEmployee().getValue(schedulePrintPopup.getOlbEmployee().getSelectedIndex());
				Employee emp = schedulePrintPopup.getOlbEmployee().getSelectedItem();
				String technicianId = emp.getCount()+"";
				String team="";
				if(schedulePrintPopup.getOlbTeam().getSelectedIndex()!=0)
				team=schedulePrintPopup.getOlbTeam().getValue(schedulePrintPopup.getOlbTeam().getSelectedIndex());
				
				System.out.println("TEAM -"+team);
				System.out.println("From Date =="+schedulePrintPopup.getDbformDate().getValueAsDate());
				System.out.println("To Date "+schedulePrintPopup.getDbToDate().getValueAsDate());
				String frmDate=AppUtility .parseDate(schedulePrintPopup.getDbformDate().getValueAsDate());
				String toDate= AppUtility .parseDate(schedulePrintPopup.getDbToDate().getValueAsDate());
				String companyid=UserConfiguration.getCompanyId()+"";
				System.out.println("companyid==="+UserConfiguration.getCompanyId()+"");
				
				
				final String url = GWT.getModuleBaseURL()+"intechPestControlServlet"+"?"+"techTeam="+team+
						"&"+"technicianId="+technicianId+"&"+"fromDate="+frmDate+"&"+"toDate="+toDate+"&"+"companyId="+companyid;
				Window.open(url, "test", "enabled");
				// write code for selected filter for PDF
				
				System.out.println("Form Date"+schedulePrintPopup.dbformDate.getValueAsDate());
				System.out.println("Do Date "+schedulePrintPopup.dbToDate.getValueAsDate());
				schedulePrintPopup.getWinModal().hide();
			}
		}
		if(event.getSource() == schedulePrintPopup.getBtnCancel()){
			schedulePrintPopup.getWinModal().hide();
		}
		
		
		
		
		// Reschedule popup buttons click
//		if(event.getSource() == reschedulePopup.getBtnReschedule()){
//			if(validateReschedulePopup()){
//				
//				selectedRecord.setAttribute("Status", "Rescheduled");
//				selectedRecord.setAttribute("ServiceDate", reschedulePopup.dbserviceDate.getValueAsDate());
//				
//				String serviceHrs =  reschedulePopup.getServicetime().getValue(reschedulePopup.servicetime.getSelectedIndex());
//				String serviceMin = reschedulePopup.getP_servicemin().getValue(reschedulePopup.getP_servicemin().getSelectedIndex());
//				String serviceAmPm = reschedulePopup.getP_ampm().getValue(reschedulePopup.getP_ampm().getSelectedIndex());
//				String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
//				
//				selectedRecord.setAttribute("ServiceTime", calculatedServiceTime);
//				String servicingBranch = null;
//				if(reschedulePopup.p_seviceBranch.getSelectedIndex()!=0)
//					servicingBranch = reschedulePopup.p_seviceBranch.getValue(reschedulePopup.p_seviceBranch.getSelectedIndex());
//				
//				saveReschedule(model,reschedulePopup.tbReason.getValue(),reschedulePopup.dbserviceDate.getValueAsDate(),calculatedServiceTime,servicingBranch);
//				
//				grid.redraw();
//				showDialogMessage("Service Rescheduled Successfully");
//				reschedulePopup.rescheduleWinModal.hide();
//			}
//		}
//		if(event.getSource() == reschedulePopup.getBtnReshceduleCancel()){
//			reschedulePopup.rescheduleWinModal.hide();
//		}
		
		
	}

	
//	private String getTechnicianName(List<EmployeeInfo> list) {
//
//		String TechnicianName ="";
//		
//		for(int i=0;i<planPopUp.olbTeam.getItems().size();i++){
//			if(planPopUp.olbTeam.getItems().get(i).getTeamList().get(0).getEmployeeName().equals(list.get(0).getFullName())){
//				System.out.println("Team employee name matched");
//				selectedRecord.setAttribute("Technician", list.get(0).getFullName());
//				grid.redraw();
//				TechnicianName = list.get(0).getFullName();
//				return TechnicianName;
//			}
//		}
//		
//		for(int i=0;i<planPopUp.olbEmployee.getItems().size();i++){
//			if(planPopUp.olbEmployee.getItems().get(i).getFullName().equals(list.get(0).getFullName())){
//				System.out.println("technician Name matched");
//				selectedRecord.setAttribute("Technician", list.get(0).getFullName());
//				grid.redraw();
//				TechnicianName = list.get(0).getFullName();
//				
//			}
//		}
//		return TechnicianName;
//		
////		selectedRecord.setAttribute("ServiceTime", planPopUp.servicetime.getValue(planPopUp.servicetime.getSelectedIndex()));
//
//	}

//	private boolean validateReschedulePopup() {
//
//		if(reschedulePopup.getTbReason().getValue().equals("")){
//			showDialogMessage("Reason for change Can not be blank !");
//			return false;
//		}
//		
//		return true;
//	}

	private boolean validateSchedulePrintPopup() {
		if(schedulePrintPopup.getOlbTeam().getSelectedIndex()==0 && schedulePrintPopup.getOlbEmployee().getSelectedIndex()==0){
			showDialogMessage("Please Select Team or Technician");
			return false;
		}
		if(schedulePrintPopup.getOlbTeam().getSelectedIndex()!=0 && schedulePrintPopup.getOlbEmployee().getSelectedIndex()!=0){
			showDialogMessage("Please Select Either Team Or Technician");
			return false;
		}
		if(schedulePrintPopup.getDbformDate().getValueAsDate()==null && schedulePrintPopup.getDbToDate().getValueAsDate()==null){
			showDialogMessage("Please Select Form Date and To Date");
			return false;
		}
		
		return true;
	}

	private void saveReschedule(Service model, String rescheduleReason, Date RescheduleDate, String serviceTime, String servicingBranch) {

		
		model.setServiceTime(serviceTime);
		
		ModificationHistory history=new ModificationHistory();
		
		history.oldServiceDate=model.getServiceDate();
		history.resheduleDate=RescheduleDate;
	
		
		history.reason=rescheduleReason;
		history.userName=LoginPresenter.loggedInUser;
		history.systemDate=new Date();
		history.resheduleTime=serviceTime;
		if(servicingBranch!=null)
		history.resheduleBranch = servicingBranch;
		
		model.getListHistory().add(history);
		
		model.setServiceDate(RescheduleDate);
		
		
		genasync.save(model, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

//	private void ReactOnServiceCompletionPopUp(ClickEvent event) {
//
//		
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
//			
//				if(completeServicePopUp.getIbserviceDuration().getValue()!=null){
//					if(completeServicePopUp.getIbserviceDuration().getValue()!=0){
//						
//					
//						String formTime = completeServicePopUp.getServicetime().getItemText(completeServicePopUp.getServicetime().getSelectedIndex())+":"+
//								completeServicePopUp.getP_servicemin().getItemText(completeServicePopUp.getP_servicemin().getSelectedIndex())+":"+
//								completeServicePopUp.getP_ampm().getItemText(completeServicePopUp.getP_ampm().getSelectedIndex());
//						
//						System.out.println("formTime form popup "+formTime);
//						
//						String toTime = completeServicePopUp.getTo_servicehours().getItemText(completeServicePopUp.getTo_servicehours().getSelectedIndex())+":"+
//								completeServicePopUp.getTo_servicemin().getItemText(completeServicePopUp.getTo_servicemin().getSelectedIndex())+":"+
//								completeServicePopUp.getTo_ampm().getItemText(completeServicePopUp.getTo_ampm().getSelectedIndex());
//						System.out.println("toTime form popup "+toTime);
//						
//					/**
//					 * Added By Rahul Verma
//					 * This method is only for nbhc. and it will check whether there stock current status is>0 and they have service or not.
//					 * 
//					 */
//					if(model.getEmployee().trim().equalsIgnoreCase("Snehal Abhishek Palav") && model.getStackDetailsList().size()!=0){
//						createExtraServiceIfStackZero(model);
//					}
//					/**
//					 * ends here 
//					 */
//					
//					reactOnComplete(completeServicePopUp.getDbserviceCompletionDate().getValueAsDate(),formTime,toTime);
//					
//					// ********vaishnavi****************
//					createProjectonMarkComplete();
//				}else{
//					showDialogMessage("Please add Service Duration (In Minutes)");
//				}
//		}else{
//			showDialogMessage("Service Duration (In Minutes) is mandetory..!");
//		}
//}
//else
//{
//	
//			String formTime = completeServicePopUp.getServicetime().getItemText(completeServicePopUp.getServicetime().getSelectedIndex())+":"+
//					completeServicePopUp.getP_servicemin().getItemText(completeServicePopUp.getP_servicemin().getSelectedIndex())+":"+
//					completeServicePopUp.getP_ampm().getItemText(completeServicePopUp.getP_ampm().getSelectedIndex());
//			
//			System.out.println("formTime form popup "+formTime);
//			
//			String toTime = completeServicePopUp.getTo_servicehours().getItemText(completeServicePopUp.getTo_servicehours().getSelectedIndex())+":"+
//					completeServicePopUp.getTo_servicemin().getItemText(completeServicePopUp.getTo_servicemin().getSelectedIndex())+":"+
//					completeServicePopUp.getTo_ampm().getItemText(completeServicePopUp.getTo_ampm().getSelectedIndex());
//			System.out.println("toTime form popup "+toTime);
//			
//		
//		/**
//		 * Added By Rahul Verma
//		 * This method is only for nbhc. and it will check whether there stock current status is>0 and they have service or not.
//		 * 
//		 */
//		if(model.getEmployee().trim().equalsIgnoreCase("Snehal Abhishek Palav") && model.getStackDetailsList().size()!=0){
//			createExtraServiceIfStackZero(model);
//		}
//		/**
//		 * ends here 
//		 */
//		
//		reactOnComplete(completeServicePopUp.getDbserviceCompletionDate().getValueAsDate(),formTime,toTime);
//		
//		// ********vaishnavi****************
//		createProjectonMarkComplete();
//		
//	}
//
//		
//	if(event.getSource()==completeServicePopUp.getBtnCancel()){
//		completeServicePopUp.hidePopUp();
//	}
//
//	}

	
//	private void reactOnComplete(Date completionDt,String formTime,String toTime) {
//		model.setStatus(Service.SERVICESTATUSCOMPLETED);
//		System.out.println("in side if");
//		model.setServiceCompletionDate(completionDt);
//		
//	if(completeServicePopUp.getTbRemark().getValue()!=null){
//		model.setServiceCompleteRemark(completeServicePopUp.getTbRemark().getValue());
//	}
//	
//	if(completeServicePopUp.getIbserviceDuration().getValue()!=null){
//		model.setServiceCompleteDuration(completeServicePopUp.getIbserviceDuration().getValue());
//	}
//	
//	if(!formTime.equals("")){
//		model.setFromTime(formTime);
//	}
//
//	if(!toTime.equals("") ){
//		model.setToTime(toTime);
//	}
//	
//	if(completeServicePopUp.lbRating.getSelectedIndex()!=0){
//		model.setCustomerFeedback(completeServicePopUp.lbRating.getValue(completeServicePopUp.lbRating.getSelectedIndex()));
//	}
//	
//	if(completeServicePopUp.getDbserviceQuantity().getValue()!=null && completeServicePopUp.getDbserviceQuantity().getValue()!=0){
//		model.setQuantity(completeServicePopUp.getDbserviceQuantity().getValue());
//	}
//	if(completeServicePopUp.getOlbUnitOfMeasurement().getSelectedIndex()!=0){
//		model.setUom(completeServicePopUp.getOlbUnitOfMeasurement().getValue());
//	}
//	manageProcessLevelBar("Marked Completed !","Failed To Mark Complete !",Service.SERVICESTATUSCOMPLETED, 
//			null, null,null,null);
//	
//}
	
	
//	public void manageProcessLevelBar(final String sucesssMessage,
//			final String failureMessage,
//			final String statusText,final ModificationHistory history,
//			final Date ServiceDate,final String serviceTime,final String serviceBranch)
//	{
//		
//		servicerecords.saveServiceProjectStatus(model, new AsyncCallback<String>() {
//			
//			@Override
//			public void onSuccess(String result) {
//				// TODO Auto-generated method stub
//				showDialogMessage(sucesssMessage);
//				selectedRecord.setAttribute("Status", "Completed");
//				grid.redraw();
//				completeServicePopUp.hidePopUp();
//				planPopUp.hidePopUp();
//				sendSMSLogic();
//			}
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
//		
//		
//
//		// MIN Creating 
//		
//		Console.log("Before Calling MIN  ");
// 		if(!selectedRecord.getAttribute("Status").equals("Completed")){
//			int projectId = selectedRecord.getAttributeAsInt("ProjectId");
//			System.out.println("project Id =="+projectId);
// 			servicelistservice.TechnicianSchedulingListMarkcompleteservice(model,LoginPresenter.loggedInUser,projectId, new AsyncCallback<ArrayList<Integer>>() {
//				
//				@Override
//				public void onSuccess(ArrayList<Integer> result) {
//					// TODO Auto-generated method stub
//					Console.log("MIN on success");
//					if(result.contains(2)){
//						showDialogMessage("Service Completed Sucessfully");
//						
//						selectedRecord.setAttribute("Status", "Completed");
//						grid.redraw();
//					}
//					
//				}
//				
//				@Override
//				public void onFailure(Throwable caught) {
//					// TODO Auto-generated method stub
//					showDialogMessage("An Unexpected error occurred!");
//
//				}
//			});
//		}
//		
//	}
	
	
//	private boolean validateRechedule() {
//
//		if(planPopUp.dbserviceDate.getValueAsDate()==null){
//			showDialogMessage("Please add Service Date");
//			return false;
//		}
//		if(planPopUp.tbRecheduleReason.getValue()==null || planPopUp.tbRecheduleReason.getValue().equals("")){
//			showDialogMessage("Please add resaon for Rechedule");
//			return false;
//		}
//		if(planPopUp.servicetime.getSelectedIndex()==0){
//			showDialogMessage("Please select Service Time!");
//			return false;
//		}
//		
//		Date rescheduleDate = planPopUp.dbserviceDate.getValueAsDate();
//		Date contractEndDate = model.getContractEndDate();
//		Date contractStartDate = model.getContractStartDate();
//		
//		/**
//		 * rohan added this code to restrict service date in between contract period only contract end date
//		 * Date : 18 /1/2017
//		 */
//		int flag =rescheduleDate.compareTo(contractEndDate);
//		if(flag > 0)
//		{
//			showDialogMessage("Reschedule date should not be greater than contract period..!");
//			return false;
//		}
//		
//		int cnt =rescheduleDate.compareTo(contractStartDate);
//		if(cnt <= 0)
//		{
//			showDialogMessage("Reschedule date should be after contract start date..!");
//			return false;
//		}
//		
//		/**
//		 * ends here 
//		 */
//		
//		return true;
//	}
	
	
//	private boolean ValidatePopupData() {
//		
//		if(planPopUp.technicianTable.getDataprovider().getList().size()==0){
//			showDialogMessage("Please add Technician ");
//			return false;
//		}
//		/** date 11/11/2017 commmented by komal to remove validation on service time **/
////		if(planPopUp.servicetime.getSelectedIndex()==0){
////			showDialogMessage("Please Select Service Time");
////			return false;
////		}
//		if(planPopUp.dbserviceDate.getValue()==null){
//			showDialogMessage("Service Date can not be blank");
//			return false;
//		}
//		
//		return true;
//	}
	
	/**** Smart GWT dialog message popup (vijay)*****************/
	public static void showDialogMessage(String Message)
	{
		SC.say(Message);
	}
	
	@Override	
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

	
	// GWT click handler
	
	@Override
	public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
		// TODO Auto-generated method stub
		
		if(event.getSource()==btnschedule){
			 final ListGridRecord [] list = grid.getSelection();
			if(list.length==0){
				showDialogMessage("Please select Planned Services");
			}else{
				ReactOnSchedule();
			}
			
		}
		
		if(event.getSource() == btnViewDetails){
			System.out.println("hi vijay show popup");
//			showServiceDetailsPopup();
		}
		
		if(event.getSource() == btnprint){
			System.out.println("hi onclick scedule print btn");
			schedulePrintPopup.showTechnicianPrintPopUp();
		}
		
		
	
	}

	private void ReactOnSchedule() {
		System.out.println("HI vijay");
		
		 final ListGridRecord [] list = grid.getSelection();
		 System.out.println("Hi vijay selected records size =="+list.length);
		 
		 final ArrayList<Service> servicelist = new ArrayList<Service>();
		 
		 for(int i=0;i<list.length;i++){
			 int ServiceId = Integer.parseInt(list[i].getAttribute("ServiceId"));
			 servicelist.add(getServiceModel(ServiceId));
		 } 
		 
		 long CompanyId =UserConfiguration.getCompanyId();
		 
		 servicerecords.saveSelectedService(servicelist, CompanyId, new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				
				long uniqueNumber = Long.parseLong(result);
				 for(int i=0;i<list.length;i++){
					 list[i].setAttribute("Number", uniqueNumber);
				 }
				 grid.redraw();
				 
				System.out.println("Sucesfully");
				SC.say("Plan Scheduled successfully!");
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				System.out.println("Failed");
				
				SC.say("Unexpected error occurred!");
			}
		});
		 
		 
	}
	


	/********************************************Sms Method*****************************************/
	
	
//	private void sendSMSLogic()
//	{
//		Vector<Filter> filtrvec = new Vector<Filter>();
//		Filter filtr = null;
//		
//		filtr = new Filter();
//		filtr.setLongValue(model.getCompanyId());
//		filtr.setQuerryString("companyId");
//		filtrvec.add(filtr);
//		
//		filtr = new Filter();
//		filtr.setBooleanvalue(true);
//		filtr.setQuerryString("status");
//		filtrvec.add(filtr);
//		
//		MyQuerry querry = new MyQuerry();
//		querry.setFilters(filtrvec);
//		querry.setQuerryObject(new SmsConfiguration());
////		form.showWaitSymbol();
//		
//		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				if(result.size()!=0){
//					for(SuperModel model:result){
//						SmsConfiguration smsconfig = (SmsConfiguration) model;
//						boolean smsStatus = smsconfig.getStatus();
//						String accountsid = smsconfig.getAccountSID();
//						String authotoken = smsconfig.getAuthToken();
//						String fromnumber = smsconfig.getPassword();
//						
//						if(smsStatus==true){
//						 sendSMS(accountsid, authotoken, fromnumber);
//						}
//						else{
//							showDialogMessage("SMS Configuration is InActive");
//						}
//					}
//				}
//			}
//
//			@Override
//			public void onFailure(Throwable caught) {
//			}
//		});
//		
//	}
	

//	private void sendSMS(final String accountsid, final String authotoken,final String fromnumber) {
//		
//		Vector<Filter> filtervec = new Vector<Filter>();
//		Filter filtr = null;
//		filtr = new Filter();
//		filtr.setLongValue(model.getCompanyId());
//		filtr.setQuerryString("companyId");
//		filtervec.add(filtr);
//		
//		System.out.println("SERVICE STATUS :: "+model.getStatus());
//		
//		if (model.getStatus().equals(Service.SERVICESTATUSCOMPLETED)) {
//
//			filtr = new Filter();
//			filtr.setStringValue("Service Completion");
//			filtr.setQuerryString("event");
//			filtervec.add(filtr);
//		} else if (model.getStatus().equals(Service.SERVICESTATUSSCHEDULE)) {
//
//			filtr = new Filter();
//			filtr.setStringValue("Service Schedule");
//			filtr.setQuerryString("event");
//			filtervec.add(filtr);
//		}else if (model.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)) {
//
//			filtr = new Filter();
//			filtr.setStringValue("Service Reschedule");
//			filtr.setQuerryString("event");
//			filtervec.add(filtr);
//		}else{
//			filtr = new Filter();
//			filtr.setStringValue("");
//			filtr.setQuerryString("event");
//			filtervec.add(filtr);
//		}
//		
//		filtr = new Filter();
//		filtr.setBooleanvalue(true);
//		filtr.setQuerryString("status");
//		filtervec.add(filtr);
//		
//		MyQuerry querry = new MyQuerry();
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new SmsTemplate());
////		form.showWaitSymbol();
//		
//		genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				if (result.size() != 0) {
//					for(SuperModel smodel:result){
//						SmsTemplate sms =(SmsTemplate) smodel;
//						final String templateMsgwithbraces = new String(sms.getMessage()); 
////						String custName = form.getPoc().getName().getValue();
//						final String prodName = model.getProductName();
//						final String serNo = model.getServiceSerialNo()+"";
//						System.out.println("Service No===="+serNo);
//						
//						cellNo = model.getPersonInfo().getCellNumber();
//						final String serDate = AppUtility.parseDate(model.getServiceDate());
//						
//						
//						/**
//						 * Date - 29 jun 2017 added by vijay for Eco Friendly getting customer name or customer correspondence name
//						 */
//						
//						if(LoginPresenter.bhashSMSFlag){
//
//							MyQuerry myquerry = AppUtility.getcustomerName(model.getPersonInfo().getCount(), model.getCompanyId());
//							genasync.getSearchResult(myquerry, new AsyncCallback<ArrayList<SuperModel>>() {
//								
//								@Override
//								public void onSuccess(ArrayList<SuperModel> result) {
//									// TODO Auto-generated method stub
//									for(SuperModel model :result){
//										Customer customer = (Customer) model;
//										 String custName;
//										 Console.log("Currespondence=="+customer.getCustPrintableName());
//										if(!customer.getCustPrintableName().equals("")){
//											custName = customer.getCustPrintableName();
//											Console.log("Customer correspondence Name =="+custName);
//										}else{
//											custName = customer.getFullname();
//											Console.log("Customer  Name =="+custName);
//										}
//										String cutomerName = templateMsgwithbraces.replace("{CustomerName}", custName);
//										String productName = cutomerName.replace("{ProductName}", prodName);
//										String serviceDate =  productName.replace("{ServiceDate}", serDate);
//										companyName = Slick_Erp.businessUnitName;
//										actualMsg = serviceDate.replace("{companyName}", companyName);
//
//										System.out.println("Actual MSG:"+actualMsg);
//										smsserviceAsync.sendSmsToClient(actualMsg, cellNo, accountsid, authotoken,fromnumber,LoginPresenter.bhashSMSFlag, new AsyncCallback<Integer>(){
//
//											@Override
//											public void onFailure(Throwable caught) {
//											}
//
//											@Override
//											public void onSuccess(Integer result) {
//												if(result==1)
//												{
//													showDialogMessage("SMS Sent Successfully!");
//													saveSmsHistory();
//
//												}
//											}} );
//									}
//								}
//								
//								@Override
//								public void onFailure(Throwable caught) {
//									// TODO Auto-generated method stub
//									
//								}
//							});
//							
//							
//							
//							
//						}else{
//						/**
//						 * ends here
//						 */
//						
//						String custName = model.getPersonInfo().getFullName();
//
//						String cutomerName = templateMsgwithbraces.replace("{CustomerName}", custName);
//						String productName = cutomerName.replace("{ProductName}", prodName);
//						String serviceNo = productName.replace("{ServiceNo}", serNo);
//						String serviceDate =  serviceNo.replace("{ServiceDate}", serDate);
//						companyName = Slick_Erp.businessUnitName;
//						actualMsg = serviceDate.replace("{companyName}", companyName);
//
//						System.out.println("Actual MSG:"+actualMsg);
//						
//						System.out.println("Company Name========================:"+companyName);
//						
//						smsserviceAsync.sendSmsToClient(actualMsg, cellNo, accountsid, authotoken,fromnumber,LoginPresenter.bhashSMSFlag, new AsyncCallback<Integer>(){
//
//							@Override
//							public void onFailure(Throwable caught) {
//							}
//
//							@Override
//							public void onSuccess(Integer result) {
//								if(result==1)
//								{
//									showDialogMessage("SMS Sent Successfully!");
////									saveSmsHistory();
//
//								}
//							}} );
//					}
//					}
//				}
//			
//				
//			}
//			@Override
//			public void onFailure(Throwable caught) {
//			}
//		});
//
//	}	
	
//	private void saveSmsHistory (){
//
//		SmsHistory smshistory = new SmsHistory();
//		smshistory.setDocumentId(model.getCount());
//		smshistory.setDocumentType("Service");
//		smshistory.setSmsText(actualMsg);
//		smshistory.setName(model.getPersonInfo().getFullName());
//		smshistory.setCellNo(cellNo);
//		smshistory.setUserId(LoginPresenter.loggedInUser);
//		System.out.println("UserID"+LoginPresenter.loggedInUser);
//		System.out.println("Doc Id"+model.getCount());
//		genasync.save(smshistory, new AsyncCallback<ReturnFromServer>() {
//			
//			@Override
//			public void onSuccess(ReturnFromServer result) {
//				System.out.println("Data Saved SuccessFully");
//			}
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				System.out.println("Data Saved UnsuccessFully");
//
//			}
//		});		
//	}
	
//	private void createProjectonMarkComplete() {
//		Timer timer = new Timer() {
//			@Override
//			public void run() {
//
//				projectAsync.createCustomerProject(model.getCompanyId(),
//						model.getContractCount(), model.getCount(),model.getStatus(),
//						new AsyncCallback<Integer>() {
//
//							@Override
//							public void onFailure(Throwable caught) {
//								showDialogMessage("An Unexpected Error occured !");
//							}
//
//							@Override
//							public void onSuccess(Integer result) {
//								System.out.println("project created .......!!");
//							}
//						});
//			}
//		};
//		timer.schedule(3000);
//	}
	
	/**
	 * Description: This is for NBHC.
	 * By Rahul Verma
	 * Date : 25/01/2017
	 * @param model 
	 */
	private void createExtraServiceIfStackZero(Service model) {
		// TODO Auto-generated method stub
		Console.log("Inside Create Extra Service");
		for (int i = 0; i < model.getStackDetailsList().size(); i++) {
			if(model.getStackDetailsList().get(i).getQauntity()!=0){
				Console.log("Quantity not eQAUL to zero");
				createAnExtraService(model);
				break;
			}else{
				System.out.println("Do not create service");
			}
		}
	}
	
	private void createAnExtraService(final Service model) {
		// TODO Auto-generated method stub
		Console.log("Calling RPC to complete services");
		final CreateSingleServiceServiceAsync createSingleServiceAsync = GWT
				.create(CreateSingleServiceService.class);
		Timer timer=new Timer() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				createSingleServiceAsync.createSingleService(model.getCount(),model.getServiceDate(),model.getServiceIndexNo(),model.getServiceSerialNo(),model.getContractCount(),model.getCompanyId(),new AsyncCallback<ArrayList<Integer>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Console.log("Failed"+caught);
						showDialogMessage("An Unexpected Error");
					}

					@Override
					public void onSuccess(ArrayList<Integer> result) {
						// TODO Auto-generated method stub
						if(result.contains(-1)){
							showDialogMessage("Unexpected Error");
						}else if(result.contains(-2)){
							showDialogMessage("Parsing Previous Date Error");
						}else if(result.contains(-3)){
							showDialogMessage("Service Date Parsing Error");
						}else if(result.contains(1)){
							showDialogMessage("Successfull created Upcoming Services");
						}
					}
				});
			}
		};
		timer.schedule(3000);
		
	}

	public ListGrid getGrid() {
		return grid;
	}

	public void setGrid(ListGrid grid) {
		this.grid = grid;
	}
	
	
	
}
