package com.slicktechnologies.client.views.technicianschedule;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.layout.HLayout;

public class TechnicianSchedulePrintPopUp implements ChangeHandler {

	
	Window	 winModal;
	DateItem dbformDate,dbToDate;
	IButton btnOk;
	IButton btnCancel;
	
	ObjectListBox<TeamManagement> olbTeam;
	ObjectListBox<Employee> olbEmployee;
	
	// Reschedule popup widgets
	Window rescheduleWinModal;
	DateItem dbserviceDate;
	
	ListBox servicetime;
	ListBox p_servicemin;
	ListBox p_ampm;
	ObjectListBox<Config> olbSpecificReason;
	TextBox tbReason;
	ListBox p_seviceBranch;
	final GenricServiceAsync genAsync = GWT.create(GenricService.class);

	IButton btnReschedule;
	IButton btnReshceduleCancel;
	
	TechnicianSchedulePrintPopUp(){
		
		
		// service completion popup
		winModal = new Window();
		winModal.setAutoSize(true);
        winModal.setTitle("Schedule Print");
        winModal.setShowMinimizeButton(false);
        winModal.setIsModal(true);
        winModal.setShowModalMask(true);
        winModal.centerInPage();
        winModal.setMargin(10);
        
        createSchedulePrintPopUp();
	}
	
	TechnicianSchedulePrintPopUp(boolean flag){
		
		
		// service completion popup
		rescheduleWinModal = new Window();
//		rescheduleWinModal.setAutoSize(true);
		rescheduleWinModal.setWidth(570);
		rescheduleWinModal.setHeight(280);
		rescheduleWinModal.setTitle("Service Reschedule");
		rescheduleWinModal.setShowMinimizeButton(false);
		rescheduleWinModal.setIsModal(true);
		rescheduleWinModal.setShowModalMask(true);
		rescheduleWinModal.centerInPage();
		rescheduleWinModal.setMargin(10);
        
        createServiceReschedulePopUp();
	}

	private void createServiceReschedulePopUp() {

		final HLayout hlServiceDateTime = new HLayout ();
		hlServiceDateTime.setPadding(10);
		hlServiceDateTime.setAutoHeight();
		hlServiceDateTime.setAutoWidth();
		hlServiceDateTime.setMembersMargin(10);
		
		
		 Label lblServiceDate = new Label("New Service Date:");
		 lblServiceDate.setSize("110px", "20px");
		 lblServiceDate.setStyleName("bold");

		DynamicForm dynamicform = new DynamicForm();
        dbserviceDate = new DateItem();
        dbserviceDate.setWidth(100);
        dbserviceDate.setUseTextField(true);
        dbserviceDate.setTitle("");
        
        dynamicform.setFields(dbserviceDate);
		 
        
        Label lblServiceTime = new Label("Service Time (HH:MM):");
	    lblServiceTime.setSize("110px", "20px");
	    lblServiceTime.setStyleName("bold");
	    
        
        servicetime = new ListBox();
        servicetime.setSize("45px", "20px");
        servicetime.insertItem("--", 0);
        servicetime.insertItem("01", 1);
        servicetime.insertItem("02", 2);
        servicetime.insertItem("03", 3);
        servicetime.insertItem("04", 4);
		servicetime.insertItem("05", 5);
		servicetime.insertItem("06", 6);
		servicetime.insertItem("07", 7);
		servicetime.insertItem("08", 8);
		servicetime.insertItem("09", 9);
		servicetime.insertItem("10", 10);
		servicetime.insertItem("11", 11);
		servicetime.insertItem("12", 12);
        
		
		p_servicemin=new ListBox();
		p_servicemin.setSize("45px", "20px");
		p_servicemin.insertItem("--", 0);
		p_servicemin.insertItem("00", 1);
		p_servicemin.insertItem("15", 2);
		p_servicemin.insertItem("30", 3);
		p_servicemin.insertItem("45", 4);
		
		p_ampm = new ListBox();
		p_ampm.setSize("47px", "20px");
		p_ampm.insertItem("--", 0);
		p_ampm.insertItem("AM", 1);
		p_ampm.insertItem("PM", 2);
        
        hlServiceDateTime.addMember(lblServiceDate);
        hlServiceDateTime.addMember(dynamicform);
        hlServiceDateTime.addMember(lblServiceTime);
        hlServiceDateTime.addMember(servicetime);
        hlServiceDateTime.addMember(p_servicemin);
        hlServiceDateTime.addMember(p_ampm);
        
        rescheduleWinModal.addItem(hlServiceDateTime);
        
        final HLayout hlServiceBranchSpecificReason = new HLayout ();
        hlServiceBranchSpecificReason.setPadding(10);
        hlServiceBranchSpecificReason.setAutoHeight();
        hlServiceBranchSpecificReason.setAutoWidth();
        hlServiceBranchSpecificReason.setMembersMargin(10);
        
        	
        Label lblServicingBranch = new Label("New Service Branch :");
        lblServicingBranch.setSize("110px", "20px");
        lblServicingBranch.setStyleName("bold");
        	
        p_seviceBranch = new ListBox();
        p_seviceBranch.setSize("110px", "20px");
        p_seviceBranch.addItem("--SELECT--");
		
        Label lblspecificReason = new Label("Specific Reason :");
        lblspecificReason.setSize("110px", "20px");
        lblspecificReason.setStyleName("bold");
		
		olbSpecificReason=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbSpecificReason, Screen.SPECIFICREASONFORRESCHEDULING);
		olbSpecificReason.addChangeHandler(this);
		olbSpecificReason.setSize("100px", "20px");

		
		hlServiceBranchSpecificReason.addMember(lblServicingBranch);
		hlServiceBranchSpecificReason.addMember(p_seviceBranch);
		hlServiceBranchSpecificReason.addMember(lblspecificReason);
		hlServiceBranchSpecificReason.addMember(olbSpecificReason);

		rescheduleWinModal.addItem(hlServiceBranchSpecificReason);
		
		
		final HLayout hlReason = new HLayout ();
		hlReason.setPadding(10);
		hlReason.setAutoHeight();
		hlReason.setAutoWidth();
		hlReason.setMembersMargin(10);
		
		
		Label lblReason = new Label("Reason For Change :");
		lblReason.setSize("110px", "20px");
		lblReason.setStyleName("bold");
		
		tbReason = new TextBox() ;
		tbReason.setSize("110px", "20px");
		
		hlReason.addMember(lblReason);
		hlReason.addMember(tbReason);

		
		rescheduleWinModal.addItem(hlReason);

		
		
	   final HLayout okbtnlayout = new HLayout ();
	   okbtnlayout.setPadding(10);
	   okbtnlayout.setAlign(Alignment.CENTER);
	   btnOk = new IButton("OK");
       
       Label lblblank = new Label();
       lblblank.setSize("100px", "20px");
       Label lblblank2 = new Label();
       Label lblblank3 = new Label();
       lblblank3.setSize("10px", "20px");
       
       btnReschedule = new IButton("Reschedule");
		btnReshceduleCancel = new IButton("Cancel");

//       okbtnlayout.addMember(lblblank);
//       okbtnlayout.addMember(lblblank2);
       okbtnlayout.addMember(btnReschedule);
       okbtnlayout.addMember(lblblank3);
       okbtnlayout.addMember(btnReshceduleCancel);
       
       rescheduleWinModal.addItem(okbtnlayout);
		
	}

	private void createSchedulePrintPopUp() {

		final HLayout hlServiceEngineer = new HLayout ();
		hlServiceEngineer.setPadding(10);
		hlServiceEngineer.setAutoHeight();
		hlServiceEngineer.setAutoWidth();
		hlServiceEngineer.setMembersMargin(10);
		
		
		 Label lblTeam = new Label("Team :");
		 lblTeam.setSize("60px", "20px");
	     lblTeam.setStyleName("bold");

		olbTeam = new ObjectListBox<TeamManagement>();
		olbTeam.setTitle("Team :");
		olbTeam.setSize("90px", "20px");
		this.initalizeTeamListBox();
		
		 Label lblTechnician = new Label("Technician:");
		 lblTechnician.setSize("60px", "20px");
		 lblTechnician.setStyleName("bold");
		
		olbEmployee=new ObjectListBox<Employee>();
        olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.TECHNICIANSCEDULINGLIST, "Service Engineer");
        olbEmployee.setSize("90px", "20px");
        
        hlServiceEngineer.addMember(lblTeam);
        hlServiceEngineer.addMember(olbTeam);
        hlServiceEngineer.addMember(lblTechnician);
        hlServiceEngineer.addMember(olbEmployee);
        
        winModal.addItem(hlServiceEngineer);
        
        
        
        final HLayout hlformToDate = new HLayout ();
        hlformToDate.setPadding(10);
        hlformToDate.setAutoHeight();
        hlformToDate.setAutoWidth();
        hlformToDate.setMembersMargin(10);
        
        DynamicForm dynamicformDate = new DynamicForm();
        dbformDate = new DateItem();
        dbformDate.setWidth(95);
        dbformDate.setUseTextField(true);
        dbformDate.setTitle("");
        
        Label lblfromDate = new Label("From Date:");
        lblfromDate.setSize("60px", "20px");
        lblfromDate.setStyleName("bold");
        
        dynamicformDate.setFields(dbformDate);
        
        
        Label lblToDate = new Label("To Date:");
        lblToDate.setSize("60px", "20px");
        lblToDate.setStyleName("bold");
        
        DynamicForm dyformToDate = new DynamicForm();
        dbToDate = new DateItem();
        dbToDate.setWidth(95);
        dbToDate.setUseTextField(true);
        dbToDate.setTitle("To Date");
        
        dyformToDate.setFields(dbToDate);
        
        hlformToDate.addMember(lblfromDate);
        hlformToDate.addMember(dynamicformDate);
        hlformToDate.addMember(lblToDate);
        hlformToDate.addMember(dyformToDate);

        
        winModal.addItem(hlformToDate);
        
        final HLayout okbtnlayout = new HLayout ();
        okbtnlayout.setPadding(10);
        okbtnlayout.setAlign(Alignment.CENTER);
        btnOk = new IButton("OK");
        
        Label lblblank3 = new Label();
        lblblank3.setSize("30px", "20px");
        
        Label lblblank = new Label();
        lblblank3.setSize("10px", "20px");
        
        btnCancel = new IButton("Cancel");

        okbtnlayout.addMember(lblblank);
        okbtnlayout.addMember(btnOk);
        okbtnlayout.addMember(lblblank3);
        okbtnlayout.addMember(btnCancel);
        
        winModal.addItem(okbtnlayout);
	}
	

	private void initalizeTeamListBox() {
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new TeamManagement());
		olbTeam.MakeLive(querry);
	}
	
	public void showTechnicianPrintPopUp() {
		olbTeam.setSelectedIndex(0);
		olbEmployee.setSelectedIndex(0);
		dbformDate.setValue("");
		dbToDate.setValue("");
		winModal.show();
	}


	
	public void showReschedulePopup(){
		servicetime.setSelectedIndex(0);
		p_servicemin.setSelectedIndex(0);
		p_ampm.setSelectedIndex(0);
		p_seviceBranch.setSelectedIndex(0);
		
		tbReason.setValue("");
		olbSpecificReason.setSelectedIndex(0);
		
		rescheduleWinModal.show();
	}
	
	
	
	
	
	public DateItem getDbformDate() {
		return dbformDate;
	}

	public void setDbformDate(DateItem dbformDate) {
		this.dbformDate = dbformDate;
	}

	public DateItem getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateItem dbToDate) {
		this.dbToDate = dbToDate;
	}

	public IButton getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(IButton btnOk) {
		this.btnOk = btnOk;
	}

	public IButton getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(IButton btnCancel) {
		this.btnCancel = btnCancel;
	}

	public ObjectListBox<TeamManagement> getOlbTeam() {
		return olbTeam;
	}

	public void setOlbTeam(ObjectListBox<TeamManagement> olbTeam) {
		this.olbTeam = olbTeam;
	}

	public ObjectListBox<Employee> getOlbEmployee() {
		return olbEmployee;
	}

	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}

	public Window getWinModal() {
		return winModal;
	}

	public void setWinModal(Window winModal) {
		this.winModal = winModal;
	}
	
	public ListBox getP_seviceBranch() {
		return p_seviceBranch;
	}

	public void setP_seviceBranch(ListBox p_seviceBranch) {
		this.p_seviceBranch = p_seviceBranch;
	}
	

	@Override
	public void onChange(ChangeEvent event) {

		if(event.getSource()==olbSpecificReason){
			if(olbSpecificReason.getValue()!=null){
				tbReason.setValue("");
				tbReason.setValue(olbSpecificReason.getValue());
			}else{
				tbReason.setValue("");
			}
		}
	}
	
	
	public void makeCustBranchListBox(int custmorID,long companyId,final String serBranch){
		MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter=null;
		filter=new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(custmorID);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(companyId);
		filtervec.add(filter);
		
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());
		
		genAsync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

		
			public void onSuccess(ArrayList<SuperModel> result) {
				p_seviceBranch.clear();
				p_seviceBranch.addItem("--SELECT--");
				p_seviceBranch.addItem("Service Address");
				
				for (SuperModel model : result) {
					CustomerBranchDetails entity = (CustomerBranchDetails) model;
					p_seviceBranch.addItem(entity.getBusinessUnitName());
				}
				
				for(int i=0;i<getP_seviceBranch().getItemCount();i++){
					
					if(getP_seviceBranch().getItemText(i).equals(serBranch)){
						
						getP_seviceBranch().setSelectedIndex(i);
					}
				}
			}
		});
		
		
		
	}

	

	/******************* getter setter for Reschedule widgets *************************/
	
	
	public Window getRescheduleWinModal() {
		return rescheduleWinModal;
	}

	public void setRescheduleWinModal(Window rescheduleWinModal) {
		this.rescheduleWinModal = rescheduleWinModal;
	}

	public DateItem getDbserviceDate() {
		return dbserviceDate;
	}

	public void setDbserviceDate(DateItem dbserviceDate) {
		this.dbserviceDate = dbserviceDate;
	}

	public ListBox getServicetime() {
		return servicetime;
	}

	public void setServicetime(ListBox servicetime) {
		this.servicetime = servicetime;
	}

	public ListBox getP_servicemin() {
		return p_servicemin;
	}

	public void setP_servicemin(ListBox p_servicemin) {
		this.p_servicemin = p_servicemin;
	}

	public ListBox getP_ampm() {
		return p_ampm;
	}

	public void setP_ampm(ListBox p_ampm) {
		this.p_ampm = p_ampm;
	}

	public ObjectListBox<Config> getOlbSpecificReason() {
		return olbSpecificReason;
	}

	public void setOlbSpecificReason(ObjectListBox<Config> olbSpecificReason) {
		this.olbSpecificReason = olbSpecificReason;
	}

	public TextBox getTbReason() {
		return tbReason;
	}

	public void setTbReason(TextBox tbReason) {
		this.tbReason = tbReason;
	}

	public IButton getBtnReschedule() {
		return btnReschedule;
	}

	public void setBtnReschedule(IButton btnReschedule) {
		this.btnReschedule = btnReschedule;
	}

	public IButton getBtnReshceduleCancel() {
		return btnReshceduleCancel;
	}

	public void setBtnReshceduleCancel(IButton btnReshceduleCancel) {
		this.btnReshceduleCancel = btnReshceduleCancel;
	}
	
	
	
	
	
	
}
