package com.slicktechnologies.client.views.technicianschedule;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;

public class TechnicianSchedulingSearchProxy {

	
		public PersonInfoComposite personInfo;
		public ObjectListBox<Branch> olbBranch;
		DateItem dbFromDate;
		DateItem dbToDate;
		IntegerBox ibContractId;
		Button btnGo;
		/**
		 * Updated by: Viraj
		 * Date: 25-12-2018
		 * Description: to get records according to technician and locality
		 */
		public ObjectListBox<Locality>  locality;
		ObjectListBox<Employee> olbEmployee;
		/** Ends **/
		
		Window winModal ;
		
		public TechnicianSchedulingSearchProxy(){
			
			winModal = new Window();
			winModal.setWidth(400);
	        winModal.setHeight(270);
			winModal.setAutoSize(true);
	        winModal.setTitle(" ");
	        winModal.setShowMinimizeButton(false);
	        winModal.setIsModal(true);
	        winModal.setShowModalMask(true);
	        winModal.centerInPage();
	        winModal.setMargin(4);
	        
	        CreatePopup();
		}

		private void CreatePopup() {

			
			final HLayout hlpersonInfo = new HLayout ();
			hlpersonInfo.setPadding(10);
			hlpersonInfo.setAutoHeight();
			hlpersonInfo.setAutoWidth();
			hlpersonInfo.setMembersMargin(10);
			
	        
			
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(new Customer());
			personInfo=new PersonInfoComposite(querry,false,true,false);
			personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
			personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
			personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
			personInfo.getElement().getStyle().setWidth(500, Unit.PX);
			personInfo.getCustomerId().getHeaderLabel().setStyleName("bold");
			personInfo.getCustomerName().getHeaderLabel().setStyleName("bold");
			personInfo.getCustomerCell().getHeaderLabel().setStyleName("bold");
			personInfo.getPocName().getHeaderLabel().setStyleName("bold");
			hlpersonInfo.addMember(personInfo);
			
			winModal.addItem(hlpersonInfo);
			
//			final HLayout hlbranchFrmDateTodate = new HLayout ();
//			hlbranchFrmDateTodate.setPadding(10);
//			hlbranchFrmDateTodate.setAutoHeight();
//			hlbranchFrmDateTodate.setAutoWidth();
//			hlbranchFrmDateTodate.setMembersMargin(10);
//			hlbranchFrmDateTodate.setLeft(5);
			
			
			 Label lblBranch = new Label("Branch");
			 lblBranch.setSize("110px", "20px");
			 lblBranch.setStyleName("bold");
			
			 Label lblFromDate = new Label("From Date");
			 lblFromDate.setSize("110px", "20px");
			 lblFromDate.setStyleName("bold");

			 
			 Label lblToDate = new Label("To Date");
			 lblToDate.setSize("110px", "20px");
			 lblToDate.setStyleName("bold");

			 
			 Label lblcontractId = new Label("Contract Id");
			 lblcontractId.setSize("110px", "20px");
			 lblcontractId.setStyleName("bold");

			 /**
			 * Updated by: Viraj
			 * Date: 25-12-2018
			 * Description: to get records according to technician and locality
			 */
			 Label lblLocality = new Label("Locality");
			 lblLocality.setSize("110px", "20px");
			 lblLocality.setStyleName("bold");
			 
			 Label lblTechnician = new Label("Technician");
			 lblTechnician.setSize("60px", "20px");
			 lblTechnician.setStyleName("bold");
			 /** Ends **/
			 
			final HLayout lablesLaybout = new HLayout ();
	        lablesLaybout.setPadding(10);
	        lablesLaybout.setAutoHeight();
	        lablesLaybout.setAutoWidth();
	        lablesLaybout.setMembersMargin(10);
	        
	        Label lblbankk = new Label();
	        lblbankk.setSize("0px", "10px");
	        
	        lablesLaybout.addMember(lblbankk);
	        lablesLaybout.addMember(lblFromDate);
	        lablesLaybout.addMember(lblToDate);
	        lablesLaybout.addMember(lblBranch);
	        lablesLaybout.addMember(lblcontractId);
	        
	        winModal.addItem(lablesLaybout);
			
			olbBranch= new ObjectListBox<Branch>();
			AppUtility.makeBranchListBoxLive(olbBranch);
			olbBranch.setSize("110px", "22px");
			
			/**
			 * Updated by: Viraj
			 * Date: 25-12-2018
			 * Description: to get records according to technician and locality
			 */
			locality=new ObjectListBox<Locality>();
			Locality.MakeObjectListBoxLive(locality);
			locality.setSize("110px", "22px");
			
			olbEmployee=new ObjectListBox<Employee>();
	        olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.TECHNICIANSCEDULINGLIST, "Service Engineer");
	        olbEmployee.setSize("90px", "20px");
			
			/** Ends **/
	        
			DynamicForm dynamicform = new DynamicForm();
	        dbFromDate = new DateItem();
	        dbFromDate.setWidth(100);
	        dbFromDate.setUseTextField(true);
	        dbFromDate.setTitle("");
	        
	        dynamicform.setFields(dbFromDate);
			
	        
			DynamicForm dfToDate = new DynamicForm();
	        dbToDate = new DateItem();
	       dbToDate.setWidth(100);
	       dbToDate.setUseTextField(true);
	       dbToDate.setTitle("");
	       dfToDate.setFields(dbToDate);
	       
	       ibContractId = new IntegerBox();
	       ibContractId.setSize("110px", "18px");
			
			 
			final HLayout lblfromDateToDatefields = new HLayout ();
			lblfromDateToDatefields.setPadding(10);
			lblfromDateToDatefields.setAutoHeight();
			lblfromDateToDatefields.setAutoWidth();
			lblfromDateToDatefields.setMembersMargin(10); // marginTop
			lblfromDateToDatefields.setStyleName("marginTop");
			
			lblfromDateToDatefields.addMember(dynamicform);
			lblfromDateToDatefields.addMember(dfToDate);
			lblfromDateToDatefields.addMember(olbBranch);
			lblfromDateToDatefields.addMember(ibContractId);
			
			winModal.addItem(lblfromDateToDatefields);
			
			/**
			 * Updated By: Viraj
			 * Date: 25-12-2018
			 * Description: To create Separate Row for Technician and Locality
			 */
			
			final HLayout lablesLaybout1 = new HLayout ();
	        lablesLaybout1.setPadding(10);
	        lablesLaybout1.setAutoHeight();
	        lablesLaybout1.setAutoWidth();
	        lablesLaybout1.setMembersMargin(10);
	        
	        Label lblbankk1 = new Label();
	        lblbankk1.setSize("0px", "10px");
			
	        lablesLaybout1.addMember(lblbankk1);
	        lablesLaybout1.addMember(lblLocality);			
	        lablesLaybout1.addMember(lblTechnician);
	        
	        winModal.addItem(lablesLaybout1);
	        
			final HLayout hlbtnTech = new HLayout ();
			hlbtnTech.setPadding(10);
			hlbtnTech.setAutoHeight();
			hlbtnTech.setAutoWidth();
			hlbtnTech.setMembersMargin(10);
			hlbtnTech.setAlign(Alignment.CENTER);
			
			hlbtnTech.addMember(locality);
			hlbtnTech.addMember(olbEmployee);
			
			winModal.addItem(hlbtnTech);
			/** Ends **/
			
			final HLayout hlbtnGo = new HLayout ();
			hlbtnGo.setPadding(10);
			hlbtnGo.setAutoHeight();
			hlbtnGo.setAutoWidth();
			hlbtnGo.setMembersMargin(10);
			hlbtnGo.setAlign(Alignment.CENTER);
			
			btnGo = new Button("Go");
			
			Label lbblak = new Label();
			Label lbblak2 = new Label();

			hlbtnGo.addMember(lbblak);
			hlbtnGo.addMember(lbblak2);

			hlbtnGo.addMember(btnGo);
			
			winModal.addItem(hlbtnGo);


		}

		
	public void showPopup(){
		dbFromDate.clearValue();
		dbToDate.clearValue();
		olbBranch.setSelectedIndex(0);
		locality.setSelectedIndex(0);  			//Added by Viraj Date 25-12-2018
		olbEmployee.setSelectedIndex(0);  		//Added by Viraj Date 25-12-2018
		personInfo.clear();
//		ibContractId.setValue();
		winModal.show();
	}
	
	public void hidePopup(){
		winModal.hide();
	}
	
	/**
	 * Updated by: Viraj
	 * Date: 25-12-2018
	 * Description: to get records according to technician and locality
	 */
	public ObjectListBox<Locality> getLocality() {
		return locality;
	}

	public void setLocality(ObjectListBox<Locality> locality) {
		this.locality = locality;
	}
	
	
	public ObjectListBox<Employee> getOlbEmployee() {
		return olbEmployee;
	}

	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}

	/** Ends **/

	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}

	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}

	public DateItem getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateItem dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateItem getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateItem dbToDate) {
		this.dbToDate = dbToDate;
	}


	public IntegerBox getIbContractId() {
		return ibContractId;
	}

	public void setIbContractId(IntegerBox ibContractId) {
		this.ibContractId = ibContractId;
	}

	public Button getBtnGo() {
		return btnGo;
	}

	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}

	public Window getWinModal() {
		return winModal;
	}

	public void setWinModal(Window winModal) {
		this.winModal = winModal;
	}
	
	
	
	
}
