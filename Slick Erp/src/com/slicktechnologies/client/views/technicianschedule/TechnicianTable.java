package com.slicktechnologies.client.views.technicianschedule;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;

public class TechnicianTable extends SuperTable<EmployeeInfo>{

	TextColumn<EmployeeInfo> getColumnTechnicanTeamName;
	Column<EmployeeInfo, String> deleteColumn;
	TextColumn<EmployeeInfo> getcolumnBlank;
	
	public TechnicianTable(){
		super();
		setHeight("100px");
	}
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addColumnTechnicianTeamName();
		addeColumndeleteColumn();
		addFieldUpdater();
	}


	protected void addeColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<EmployeeInfo, String>(btnCell) {
			@Override
			public String getValue(EmployeeInfo object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn, 80, Unit.PX);
		
	}

	
	private void addColumnTechnicianTeamName() {
		// TODO Auto-generated method stub
		getColumnTechnicanTeamName = new TextColumn<EmployeeInfo>() {
			
			@Override
			public String getValue(EmployeeInfo object) {
				// TODO Auto-generated method stub
				return object.getFullName();
			}
		};
		table.addColumn(getColumnTechnicanTeamName,"Technician/Team Name");
		table.setColumnWidth(getColumnTechnicanTeamName, 80, Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		System.out.println("111111111111111");
		createFieldUpdaterdeleteColumn();

	}

	protected void createFieldUpdaterdeleteColumn() {
		System.out.println("Hi vijay -- == -- = ");
		deleteColumn.setFieldUpdater(new FieldUpdater<EmployeeInfo, String>() {
			
			@Override
			public void update(int index, EmployeeInfo object, String value) {
				// TODO Auto-generated method stub
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		 for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
		 System.out.println("hi vijay technician =="+state);
		 if(state==true){
			 createTable();
		 }else{
			 addColumnTechnicianTeamName();
			 addcolumnBlank();
		 }
	}
	
	private void addcolumnBlank() {
		// TODO Auto-generated method stub
		getcolumnBlank = new TextColumn<EmployeeInfo>() {
			
			@Override
			public String getValue(EmployeeInfo object) {
				// TODO Auto-generated method stub
				return "";
			}
		};
		table.addColumn(getcolumnBlank, "");
		table.setColumnWidth(getcolumnBlank, 100, Unit.PX);
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
