package com.slicktechnologies.client.views.technicianschedule;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

public class TechnicianToolTable extends SuperTable<CompanyAsset>{

	TextColumn<CompanyAsset> getColumnToolName;
	Column<CompanyAsset, String> deleteColumn;

	TextColumn<CompanyAsset> getcolumnBlank;
	
	public TechnicianToolTable(){
		super();
		setHeight("100px");
	}
	
	@Override
	public void createTable() {
		addcolumnToolName();
		addColumnDelete();
		addFieldUpdater();
	}

	private void addcolumnToolName() {
		getColumnToolName = new TextColumn<CompanyAsset>() {
			
			@Override
			public String getValue(CompanyAsset object) {
				return object.getName();
			}
		};
		table.addColumn(getColumnToolName, "Tool Name");
		table.setColumnWidth(getColumnToolName, 80, Unit.PX);
	}

	private void addColumnDelete() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<CompanyAsset, String>(btnCell) {
			@Override
			public String getValue(CompanyAsset object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn, 80, Unit.PX);
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		createFieldUpdaterdeleteColumn();
	}
	
	protected void createFieldUpdaterdeleteColumn() {
		deleteColumn.setFieldUpdater(new FieldUpdater<CompanyAsset, String>() {
			@Override
			public void update(int index, CompanyAsset object, String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
		for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
		 
		 if(state==true){
			 createTable();
		 }else{
				addcolumnToolName();
				addcolumnBlank();
		 }
	}

	private void addcolumnBlank() {
		// TODO Auto-generated method stub
		getcolumnBlank = new TextColumn<CompanyAsset>() {
			
			@Override
			public String getValue(CompanyAsset object) {
				// TODO Auto-generated method stub
				return "";
			}
		};
		table.addColumn(getcolumnBlank, "");
		table.setColumnWidth(getcolumnBlank, 100, Unit.PX);
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
