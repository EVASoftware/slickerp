package com.slicktechnologies.client.views.franchise;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.AutoFitWidthApproach;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class InventoryListForm extends ViewContainer{

	ListGrid grid;
	DataSource ds;
	PersonInfoComposite pic;
	Button btnGo;
	HorizontalPanel horizontalpanel;
	VerticalPanel verticalpanel;
	
	public InventoryListForm() {
		super();
		createGui();
	}
	
	
	@Override
	protected void createGui() {
		pic=AppUtility.customerInfoComposite(new Customer());
		pic.getCustomerId().getHeaderLabel().setText("* Customer ID");
		pic.getCustomerName().getHeaderLabel().setText("* Customer Name");
		pic.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		
		btnGo = new Button("Search");
		
		horizontalpanel = new HorizontalPanel();
		horizontalpanel.setSpacing(10);
		horizontalpanel.getElement().addClassName("technicianscheduleButtons");
		
		horizontalpanel.add(pic);
		horizontalpanel.add(btnGo);
		
		verticalpanel = new VerticalPanel();
		
		grid = new ListGrid();
		grid.setStyleName("GridList");
		setGridProperty(grid);
		grid.setFields(getGridFields());
		
		ds= new DataSource();
		ds.setClientOnly(true);
		setDataSourceField(ds);
		grid.setDataSource(ds);
		
		verticalpanel.getElement().addClassName("varticalGridPanel");
		grid.getElement().addClassName("technicianSchedule");
		verticalpanel.add(grid);
		
		content.add(horizontalpanel);
		content.add(verticalpanel);
	}
	
	public void setDataToListGrid(ArrayList<ProductInventoryViewDetails> pivList){
		System.out.println("FORM SETTING DATA....");
		
		ds.setTestData(getGridData(pivList));
		grid.setDataSource(ds);
		grid.setFields(getGridFields());
		grid.fetchData();
	}
	
	private static void setGridProperty(ListGrid grid) {
		   grid.setWidth("83%");
		   grid.setHeight("70%");
		   grid.setAutoFetchData(true);
		   grid.setAlternateRecordStyles(true);
		   grid.setShowAllRecords(true);
		   grid.setShowFilterEditor(true);
		   grid.setFilterOnKeypress(true);
		   
		   grid.setWrapCells(true);	
		   grid.setFixedRecordHeights(false);
		   grid.setAutoFitData(Autofit.HORIZONTAL);	
		   grid.setAutoFitWidthApproach(AutoFitWidthApproach.BOTH);	
		   grid.setCanResizeFields(true);
		}
	
	private static ListGridField[] getGridFields() {
		ListGridField field1 = new ListGridField("id","Id");
		field1.setWrap(true);
	    field1.setAlign(Alignment.CENTER);
		
	    ListGridField field2 = new ListGridField("pCode","Product Code");
	    field2.setWrap(true);
	    field2.setAlign(Alignment.CENTER);
	    
	    ListGridField field3 = new ListGridField("pName","Product Name");
	    field3.setWrap(true);
	    field3.setAlign(Alignment.CENTER);
	    
	    ListGridField field4 = new ListGridField("warehouse","Warehouse");
	    field4.setWrap(true);
	    field4.setAlign(Alignment.CENTER);
	    
	    ListGridField field5 = new ListGridField("sLocation","Storage Location");
	    field5.setWrap(true);
	    field5.setAlign(Alignment.CENTER);
	    
	    ListGridField field6 = new ListGridField("sBin","Storage Bin");
	    field6.setWrap(true);
	    field6.setAlign(Alignment.CENTER);
	    
	    ListGridField field7 = new ListGridField("availableQty","Available Qty");
	    field7.setWrap(true);
	    field7.setAlign(Alignment.CENTER);
	    
	    ListGridField field8 = new ListGridField("minQty","Min Qty");
	    field8.setWrap(true);
	    field8.setAlign(Alignment.CENTER);
	    
	    ListGridField field9 = new ListGridField("maxQty","Max Qty");
	    field9.setWrap(true);
	    field9.setAlign(Alignment.CENTER);
	    
	    ListGridField field10 = new ListGridField("reOrderQty","Reorder Qty");
	    field10.setWrap(true);
	    field10.setAlign(Alignment.CENTER);
	    
	    ListGridField field11 = new ListGridField("pPrice","Purchase Price");
	    field11.setWrap(true);
	    field11.setAlign(Alignment.CENTER);
	    
		return new ListGridField[] { 
				   field1, field2, field3, field4, field5, field6, field7, field8, field9,
				   field10,field11
				   };	
	}

	protected static void setDataSourceField(DataSource dataSource) {
		DataSourceIntegerField pkField = new DataSourceIntegerField("pk");  
        pkField.setHidden(true);  
        pkField.setPrimaryKey(true);
        
        DataSourceField field1 = new DataSourceField("id", FieldType.TEXT);
        DataSourceField field2 = new DataSourceField("pCode", FieldType.TEXT);
        DataSourceField field3 = new DataSourceField("pName", FieldType.TEXT);
        DataSourceField field4 = new DataSourceField("warehouse", FieldType.TEXT);
        DataSourceField field5 = new DataSourceField("sLocation", FieldType.TEXT);
        DataSourceField field6 = new DataSourceField("sBin", FieldType.TEXT);
        DataSourceField field7 = new DataSourceField("availableQty", FieldType.TEXT);
        DataSourceField field8 = new DataSourceField("minQty", FieldType.TEXT);
        DataSourceField field9 = new DataSourceField("maxQty", FieldType.TEXT);
        DataSourceField field10 = new DataSourceField("reOrderQty", FieldType.TEXT);
        DataSourceField field11 = new DataSourceField("pPrice", FieldType.TEXT);
        
        dataSource.setFields(pkField,field1,field2,field3,field4,field5,field6,field7,field8,field9,field10,field11); 
	}
	
	private static ListGridRecord[] getGridData(ArrayList<ProductInventoryViewDetails> pivList) {
		ListGridRecord[] records = null; 
		records = new ListGridRecord[pivList.size()];
		String prodName="";
		String prodCode="";
		String prodWarehouse="";
		String prodLocation="";
		String prodBin="";
		Console.log("pivList size: "+pivList.size());
		for(int i=0;i<pivList.size();i++){
			 ListGridRecord record = new ListGridRecord();
			 ProductInventoryViewDetails piv = pivList.get(i);
			 Console.log("pivList name: "+piv.getProdname());
			 
			 record.setAttribute("id",piv.getProdid()+"");
			 if(piv.getProdcode() != null){
				 prodCode=piv.getProdcode();
			 }
			 record.setAttribute("pCode",prodCode);
			 
			 if(piv.getProdname() != null){
				 prodName=piv.getProdname();
			 }
			 record.setAttribute("pName",prodName);
			 
			 if(piv.getWarehousename() != null){
				 prodWarehouse=piv.getWarehousename();
			 }
			 record.setAttribute("warehouse",prodWarehouse);
			 
			 if(piv.getStoragelocation() != null){
				 prodLocation=piv.getStoragelocation();
			 }
			 record.setAttribute("sLocation",prodLocation);
			 
			 if(piv.getStoragebin() != null){
				 prodBin=piv.getStoragebin();
			 }
			 record.setAttribute("sBin",prodBin);
			 
			 record.setAttribute("availableQty",piv.getAvailableqty()+"");
			 record.setAttribute("minQty",piv.getMinqty()+"");
			 record.setAttribute("maxQty",piv.getMaxqty()+"");
			 record.setAttribute("reOrderQty",piv.getReorderqty()+"");
			 record.setAttribute("pPrice",piv.getPurchasePrice()+"");
			 records[i]=record;
		}
		return records;
	}
	
	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}


	public Button getBtnGo() {
		return btnGo;
	}


	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}


	public PersonInfoComposite getPic() {
		return pic;
	}


	public void setPic(PersonInfoComposite pic) {
		this.pic = pic;
	}


	public ListGrid getGrid() {
		return grid;
	}


	public void setGrid(ListGrid grid) {
		this.grid = grid;
	}


	public DataSource getDs() {
		return ds;
	}


	public void setDs(DataSource ds) {
		this.ds = ds;
	}


	public HorizontalPanel getHorizontalpanel() {
		return horizontalpanel;
	}


	public void setHorizontalpanel(HorizontalPanel horizontalpanel) {
		this.horizontalpanel = horizontalpanel;
	}


	public VerticalPanel getVerticalpanel() {
		return verticalpanel;
	}


	public void setVerticalpanel(VerticalPanel verticalpanel) {
		this.verticalpanel = verticalpanel;
	}
	
	

}
