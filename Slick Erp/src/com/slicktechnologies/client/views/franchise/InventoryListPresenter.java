package com.slicktechnologies.client.views.franchise;

import java.util.ArrayList;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;

public class InventoryListPresenter implements ClickHandler {

	InventoryListForm form;
	protected AppMemory mem;
	final static GeneralServiceAsync async=GWT.create(GeneralService.class);
	ArrayList<ProductInventoryViewDetails> pivList;
	
	public InventoryListPresenter(InventoryListForm form) {
		this.form = form;
		
		mem=AppMemory.getAppMemory();
		form.getBtnGo().addClickHandler(this);
	}

	public static void initialize() {
		System.out.println("INSIDE CHART INITIALIZATION");
		InventoryListForm form = new InventoryListForm();
		InventoryListPresenter presenter = new InventoryListPresenter(form);
		
		AppMemory.getAppMemory().stickPnel(form);
	}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource().equals(form.getBtnGo())) {
			long companyId = UserConfiguration.getCompanyId();
			
			async.fetchCustomerProductViewList(companyId, form.getPic().getIdValue(), new AsyncCallback<ArrayList<ProductInventoryViewDetails>>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("Something went wrong: "+ caught);
					
				}

				@Override
				public void onSuccess(
						ArrayList<ProductInventoryViewDetails> result) {
					pivList = result;
					Console.log("pivList: "+ pivList);
					if(pivList != null) {
						form.setDataToListGrid(pivList);
					}
				}
			});
		}
		
	}

}
