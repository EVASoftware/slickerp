package com.slicktechnologies.client.views.licensemanagement;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.helperlayer.LicenseManagement;

public class LicenseManagementTable extends SuperTable<LicenseManagement> {
	
	TextColumn<LicenseManagement> countColumn;
	TextColumn<LicenseManagement> companyNameColumn;
	TextColumn<LicenseManagement> companyIdColumn;
	TextColumn<LicenseManagement> prevEndDateColumn;
	TextColumn<LicenseManagement> prevUserCountColumn;
	TextColumn<LicenseManagement> currentUserCountColumn;
	TextColumn<LicenseManagement> currentEndDateColumn;
	TextColumn<LicenseManagement> licenseStartDateColumn;
	TextColumn<LicenseManagement> updationDateColumn;
	TextColumn<LicenseManagement> updationByColumn;
	
	public LicenseManagementTable() {
		super();
	}

	@Override
	public void createTable() {
		addCountColumn();
		addColumnCompanyNameColumn();
		addColumnCompanyIdColumn();
		addColumnStartDateColumn();
//		addColumnPrevEndDateColumn();
		addColumnCurrentEndDateColumn();
//		addColumnPrevUserCountColumn();
		addColumnCurrentCountColumn();
		addColumnUpdationDate();
		addColumnUpdatedBy();
	}
	
	public void addCountColumn()
	{
		countColumn=new TextColumn<LicenseManagement>() {

			@Override
			public String getValue(LicenseManagement object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(countColumn,"ID");
		table.setColumnWidth(countColumn, 100, Unit.PX);
		countColumn.setSortable(true);
	}


	public void addColumnCompanyNameColumn()
	{
		companyNameColumn=new TextColumn<LicenseManagement>() {
			
			@Override
			public String getValue(LicenseManagement object) {
				return object.getCompanyName();
			}
		};
		table.addColumn(companyNameColumn,"Company Name");
		table.setColumnWidth(companyNameColumn, 150, Unit.PX);
		companyNameColumn.setSortable(true);
	}
	
	public void addColumnCompanyIdColumn()
	{
		companyIdColumn=new TextColumn<LicenseManagement>() {

			@Override
			public String getValue(LicenseManagement object) {
				return object.getCompanyCount()+"";
			}
		};
		table.addColumn(companyIdColumn,"Company ID");
		table.setColumnWidth(companyIdColumn, 150, Unit.PX);
		companyIdColumn.setSortable(true);
	}
	
	public void addColumnPrevEndDateColumn()
	{
		prevEndDateColumn=new TextColumn<LicenseManagement>() {

			@Override
			public String getValue(LicenseManagement object) {
				if(object.getPreviousEndDate()!=null)
				{
					return AppUtility.parseDate(object.getPreviousEndDate());
				}
				else{
					return "N.A";
				}
			}
		};
		table.addColumn(prevEndDateColumn,"Prev End Date");
		table.setColumnWidth(prevEndDateColumn, 120, Unit.PX);
		prevEndDateColumn.setSortable(true);
	}
	
	public void addColumnPrevUserCountColumn()
	{
		
		prevUserCountColumn=new TextColumn<LicenseManagement>() {

			@Override
			public String getValue(LicenseManagement object) {
				return object.getPreviousUserCount()+"";
			}
		};
         table.addColumn(prevUserCountColumn,"Prev User Count");
         table.setColumnWidth(prevUserCountColumn, 130, Unit.PX);
         prevUserCountColumn.setSortable(true);
	}
	
	public void addColumnCurrentCountColumn()
	{
		currentUserCountColumn=new TextColumn<LicenseManagement>() {

			@Override
			public String getValue(LicenseManagement object) {
				return object.getCurrentUserCount()+"";
			}
		};
		table.addColumn(currentUserCountColumn,"Current User Count");
		 table.setColumnWidth(currentUserCountColumn, 140, Unit.PX);
		 currentUserCountColumn.setSortable(true);
	}
	
	public void addColumnCurrentEndDateColumn()
	{
		currentEndDateColumn=new TextColumn<LicenseManagement>() {

			@Override
			public String getValue(LicenseManagement object) {
				if(object.getCurrentEndDate()!=null){
					return AppUtility.parseDate(object.getCurrentEndDate());
				}
				else{
					return "N.A";
				}
			}
		};
		table.addColumn(currentEndDateColumn,"Current End Date");
		table.setColumnWidth(currentEndDateColumn, 120, Unit.PX);
		currentEndDateColumn.setSortable(true);
	}
	
	public void addColumnStartDateColumn()
	{
		licenseStartDateColumn=new TextColumn<LicenseManagement>() {

			@Override
			public String getValue(LicenseManagement object) {
				if(object.getLicenseStartDate()!=null){
					return AppUtility.parseDate(object.getLicenseStartDate());
				}
				else{
					return "N.A";
				}
			}
		};
		table.addColumn(licenseStartDateColumn,"License Start Date");
		table.setColumnWidth(licenseStartDateColumn, 150, Unit.PX);
		licenseStartDateColumn.setSortable(true);
	}
	
	public void addColumnUpdationDate()
	{
		updationDateColumn=new TextColumn<LicenseManagement>() {

			@Override
			public String getValue(LicenseManagement object) {
				if(object.getUpdationDate()!=null){
					return AppUtility.parseDate(object.getUpdationDate());
				}
				else{
					return "N.A";
				}
			}
		};
		table.addColumn(updationDateColumn,"Updation Date");
		table.setColumnWidth(updationDateColumn, 120, Unit.PX);
		updationDateColumn.setSortable(true);
	}
	
	public void addColumnUpdatedBy()
	{
		updationByColumn=new TextColumn<LicenseManagement>() {
			
			@Override
			public String getValue(LicenseManagement object) {
				if(object.getCreatedBy()!=null){
					return object.getCreatedBy();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(updationByColumn,"Updated By");
		table.setColumnWidth(updationByColumn, 100, Unit.PX);
		updationByColumn.setSortable(true);
	}
	
	
	public void addColumnSorting()
	{
		addSortinggetCount();
		addSortinggetCompanyName();
		addSortinggetCompanyId();
		addSortinggetprevEndDate();
		addSortinggetprevUserCount();
		addSortinggetcurrentEndDate();
		addSortingcurrentUserCount();
		addSortingStartDate();
		addSortinggetupdationDate();
		addSortingupdatedBy();
	}
	
	protected void addSortinggetCount()
	{
		List<LicenseManagement> list=getDataprovider().getList();
		columnSort=new ListHandler<LicenseManagement>(list);
		columnSort.setComparator(countColumn, new Comparator<LicenseManagement>()
				{
			@Override
			public int compare(LicenseManagement e1,LicenseManagement e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCompanyName()
	{
		List<LicenseManagement> list=getDataprovider().getList();
		columnSort=new ListHandler<LicenseManagement>(list);
		columnSort.setComparator(companyNameColumn, new Comparator<LicenseManagement>()
				{
			@Override
			public int compare(LicenseManagement e1,LicenseManagement e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCompanyName()!=null && e2.getCompanyName()!=null){
						return e1.getCompanyName().compareTo(e2.getCompanyName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetCompanyId()
	{
		List<LicenseManagement> list=getDataprovider().getList();
		columnSort=new ListHandler<LicenseManagement>(list);
		columnSort.setComparator(companyIdColumn, new Comparator<LicenseManagement>()
				{
			@Override
			public int compare(LicenseManagement e1,LicenseManagement e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCompanyCount()== e2.getCompanyCount()){
						return 0;}
					if(e1.getCompanyCount()> e2.getCompanyCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	
	protected void addSortinggetprevEndDate()
	{
		List<LicenseManagement> list=getDataprovider().getList();
		columnSort=new ListHandler<LicenseManagement>(list);
		columnSort.setComparator(prevEndDateColumn, new Comparator<LicenseManagement>()
				{
			@Override
			public int compare(LicenseManagement e1,LicenseManagement e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPreviousEndDate()!=null && e2.getPreviousEndDate()!=null){
						return e1.getPreviousEndDate().compareTo(e2.getPreviousEndDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetprevUserCount()
	{
		List<LicenseManagement> list=getDataprovider().getList();
		columnSort=new ListHandler<LicenseManagement>(list);
		columnSort.setComparator(prevUserCountColumn, new Comparator<LicenseManagement>()
				{
			@Override
			public int compare(LicenseManagement e1,LicenseManagement e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getPreviousUserCount()== e2.getPreviousUserCount()){
						return 0;}
					if(e1.getPreviousUserCount()> e2.getPreviousUserCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetcurrentEndDate()
	{
		List<LicenseManagement> list=getDataprovider().getList();
		columnSort=new ListHandler<LicenseManagement>(list);
		columnSort.setComparator(currentEndDateColumn, new Comparator<LicenseManagement>()
				{
			@Override
			public int compare(LicenseManagement e1,LicenseManagement e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCurrentEndDate()!=null && e2.getCurrentEndDate()!=null){
						return e1.getCurrentEndDate().compareTo(e2.getCurrentEndDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingcurrentUserCount()
	{
		List<LicenseManagement> list=getDataprovider().getList();
		columnSort=new ListHandler<LicenseManagement>(list);
		columnSort.setComparator(currentUserCountColumn, new Comparator<LicenseManagement>()
				{
			@Override
			public int compare(LicenseManagement e1,LicenseManagement e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCurrentUserCount()== e2.getCurrentUserCount()){
						return 0;}
					if(e1.getCurrentUserCount()> e2.getCurrentUserCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingStartDate()
	{
		List<LicenseManagement> list=getDataprovider().getList();
		columnSort=new ListHandler<LicenseManagement>(list);
		columnSort.setComparator(licenseStartDateColumn, new Comparator<LicenseManagement>()
				{
			@Override
			public int compare(LicenseManagement e1,LicenseManagement e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getLicenseStartDate()!=null && e2.getLicenseStartDate()!=null){
						return e1.getLicenseStartDate().compareTo(e2.getLicenseStartDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetupdationDate()
	{
		List<LicenseManagement> list=getDataprovider().getList();
		columnSort=new ListHandler<LicenseManagement>(list);
		columnSort.setComparator(updationDateColumn, new Comparator<LicenseManagement>()
				{
			@Override
			public int compare(LicenseManagement e1,LicenseManagement e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getUpdationDate()!=null && e2.getUpdationDate()!=null){
						return e1.getUpdationDate().compareTo(e2.getUpdationDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortingupdatedBy()
	{
		List<LicenseManagement> list=getDataprovider().getList();
		columnSort=new ListHandler<LicenseManagement>(list);
		columnSort.setComparator(updationByColumn, new Comparator<LicenseManagement>()
				{
			@Override
			public int compare(LicenseManagement e1,LicenseManagement e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreatedBy()!=null && e2.getCreatedBy()!=null){
						return e1.getCreatedBy().compareTo(e2.getCreatedBy());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	
	
	@Override
	protected void initializekeyprovider() {
	}

	@Override
	public void addFieldUpdater() {
	}

	@Override
	public void setEnable(boolean state) {
	}

	@Override
	public void applyStyle() {
	}

}
