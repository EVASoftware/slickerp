package com.slicktechnologies.client.views.licensemanagement;

import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.LicenseUpdationService;
import com.slicktechnologies.client.services.LicenseUpdationServiceAsync;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.LicenseManagement;

public class LicenseManagementPresenter extends FormScreenPresenter<LicenseManagement> implements ValueChangeHandler<String> {
	
	LicenseManagementForm form;
	final GenricServiceAsync async=GWT.create(GenricService.class);
	final LicenseUpdationServiceAsync licenseAsync=GWT.create(LicenseUpdationService.class);
	Company compEntityObj=null;
	
	public LicenseManagementPresenter(FormScreen<LicenseManagement> view, LicenseManagement model) {
		super(view, model);
		form=(LicenseManagementForm) view;
		form.getTbcompanyId().addValueChangeHandler(this);
		form.getBtnLicense().addClickHandler(this);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
	InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
	}

	@Override
	public void reactOnPrint() {
	}

	@Override
	public void reactOnEmail() {
		
	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model=new LicenseManagement();
	}
	
	public void reactOnDownload()
    {
    }
	
	
	public static void initalize()
	{
		AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		LicenseManagementForm  form=new  LicenseManagementForm();
		LicenseManagementPresenter  presenter=new LicenseManagementPresenter(form,new LicenseManagement());
		AppMemory.getAppMemory().stickPnel(form);
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		if(event.getSource().equals(form.getTbcompanyId()))
		{
			form.getLicenseTable().connectToLocal();
			compEntityObj=null;
			if(!form.getTbcompanyId().getValue().equals(""))
			{
				form.showWaitSymbol();
				licenseAsync.retrieveCompanyInfo(Long.parseLong(form.getTbcompanyId().getValue()), new AsyncCallback<Company>() {

					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						form.showDialogMessage("An unexpected error occurred!");
					}

					@Override
					public void onSuccess(Company result) {
						if(result!=null){
							compEntityObj=result;
							form.getTbcompanyName().setValue(result.getBusinessUnitName());
							form.getDbLicenseStartDate().setValue(result.getLicenseStartDate());
							if(result.getLicenseEndingDate()!=null){
								form.getDblicenseEndDate().setValue(result.getLicenseEndingDate());
							}
							else{
								int weeks=result.getNoOfWeeks();
								Date startDate=result.getLicenseStartDate();
								CalendarUtil.addDaysToDate(startDate, weeks);
								form.getDblicenseEndDate().setValue(startDate);
							}
							form.getIbnoofusers().setValue(result.getNoOfUser());
							retrieveUpdationHistory(Long.parseLong(form.getTbcompanyId().getValue()));
						}
						else
						{
							form.getTbcompanyId().setValue("");
							form.getDbLicenseStartDate().setValue(null);
							form.getDblicenseEndDate().setValue(null);
							form.getTbcompanyName().setValue(null);
							form.getIbnoofusers().setValue(0);
							form.showDialogMessage("Unable to load data. Please try again!");
						}
						form.hideWaitSymbol();
					}
					
				});
				
			}
			
			if(form.getTbcompanyId().getValue().equals(""))
			{
				form.getTbcompanyName().setValue(null);
				form.getIbnoofusers().setValue(null);
				form.getDbLicenseStartDate().setValue(null);
				form.getDblicenseEndDate().setValue(null);
			}
		}
	}
	
	private void retrieveUpdationHistory(long companyCount)
	{
		MyQuerry querry =new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("companyCount");
		temp.setLongValue(Long.parseLong(form.getTbcompanyId().getValue()));
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new LicenseManagement());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("An unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for(SuperModel modelEntity:result)
				{
					LicenseManagement licenseEntity=(LicenseManagement) modelEntity;
					LicenseManagement history=new LicenseManagement();
					history.setCount(licenseEntity.getCount());
					history.setCompanyCount(licenseEntity.getCompanyCount());
					history.setPreviousEndDate(licenseEntity.getPreviousEndDate());
					history.setCurrentEndDate(licenseEntity.getCurrentEndDate());
					history.setCurrentUserCount(licenseEntity.getCurrentUserCount());
					history.setPreviousUserCount(licenseEntity.getPreviousUserCount());
					history.setCompanyName(licenseEntity.getCompanyName());
					history.setLicenseStartDate(licenseEntity.getLicenseStartDate());
					history.setUpdationDate(licenseEntity.getUpdationDate());
					
					form.getLicenseTable().getDataprovider().getList().add(history);
				}
				form.hideWaitSymbol();
			}
		});
		
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource().equals(form.getBtnLicense()))
		{
			System.out.println("One");
			boolean endDateFlag=false;
			if(form.getTbcompanyId().getValue().equals(""))
			{
				form.showDialogMessage("Please enter Company Id!");
			}
			if(form.getDblicenseEndDate().getValue()==null)
			{
				form.showDialogMessage("Please enter license end date!");
			}
			
			if(form.getIbnoofusers().getValue()==null)
			{
				form.showDialogMessage("Please enter no of licenses!");
			}
			
			if(form.getDbLicenseStartDate().getValue()!=null&&form.getDblicenseEndDate().getValue()!=null)
			{
				Date startDate=form.getDbLicenseStartDate().getValue();
				Date endDate=form.getDblicenseEndDate().getValue();
				
				if(startDate.after(endDate))
				{
					endDateFlag=true;
					form.showDialogMessage("Please enter appropriate end date!");
				}
			}
			
			if(endDateFlag==false&&!form.getTbcompanyId().getValue().equals("")&&form.getDblicenseEndDate().getValue()!=null&&form.getIbnoofusers().getValue()>0)
			{
				reactOnUpdation();
			}
		}
	}
	
	protected void reactOnUpdation()
	{
		form.showWaitSymbol();
		licenseAsync.updateLicenses(Long.parseLong(form.getTbcompanyId().getValue()), form.getIbnoofusers().getValue(), form.getDblicenseEndDate().getValue(), new AsyncCallback<Integer>(){

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("An unexpected error occurred!");
			}

			@Override
			public void onSuccess(Integer result) {
				if(result==1){
					
					System.out.println("Conpany iddd = = "+form.getTbcompanyId().getValue());
					
					saveUpdationHistory();
				}
				if(result==0){
					form.showDialogMessage("Updation Un Successfull");
					form.getTbcompanyId().setValue(null);
					form.getTbcompanyName().setValue(null);
					form.getDbLicenseStartDate().setValue(null);
					form.getDblicenseEndDate().setValue(null);
					form.getIbnoofusers().setValue(null);
				}
				form.hideWaitSymbol();
			}});
	}
	
	private void saveUpdationHistory()
	{
		Company c=new Company();
		final LicenseManagement licenseManage=new LicenseManagement();
		licenseManage.setCompanyCount(Long.parseLong(form.getTbcompanyId().getValue()));
		licenseManage.setCompanyId(c.getCompanyId());
		licenseManage.setCompanyName(form.getTbcompanyName().getValue());
		licenseManage.setCurrentEndDate(form.getDblicenseEndDate().getValue());
		licenseManage.setCurrentUserCount(form.getIbnoofusers().getValue());
		licenseManage.setPreviousUserCount(compEntityObj.getNoOfUser());
		if(compEntityObj.getLicenseEndingDate()!=null){
			licenseManage.setPreviousEndDate(compEntityObj.getLicenseEndingDate());
		}
		else{
			int weeks=compEntityObj.getNoOfWeeks();
			Date startDate=compEntityObj.getLicenseStartDate();
			CalendarUtil.addDaysToDate(startDate, weeks);
			licenseManage.setPreviousEndDate(startDate);
		}
		licenseManage.setLicenseStartDate(form.getDbLicenseStartDate().getValue());
		licenseManage.setUpdationDate(new Date());
		licenseManage.setCreatedBy(LoginPresenter.loggedInUser);
		
		async.save(licenseManage, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("An unexpected error occurred!");
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				licenseManage.setCount(result.count);
				form.getLicenseTable().getDataprovider().getList().add(licenseManage);
				form.showDialogMessage("Updation Successfull");
				form.getTbcompanyId().setValue(null);
				form.getTbcompanyName().setValue(null);
				form.getDbLicenseStartDate().setValue(null);
				form.getDblicenseEndDate().setValue(null);
				form.getIbnoofusers().setValue(null);
				form.getLicenseTable().connectToLocal();
				form.hideWaitSymbol();
			}
		});
		
		
		
		
	}

}
