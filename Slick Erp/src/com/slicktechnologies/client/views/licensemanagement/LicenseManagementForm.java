package com.slicktechnologies.client.views.licensemanagement;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.LicenseManagement;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class LicenseManagementForm extends FormScreen<LicenseManagement> {
	
	TextBox tbcompanyName;
	TextBox tbcompanyId;
	DateBox dbLicenseStartDate;
	DateBox dblicenseEndDate;
	IntegerBox ibnoofusers;
	Button btnLicense;
	LicenseManagementTable licenseTable;
	
	
	public  LicenseManagementForm() {
		super();
		createGui();
	}

	public LicenseManagementForm(String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}

	
	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget()
	{
		tbcompanyId=new TextBox();
		tbcompanyName=new TextBox();
		tbcompanyName.setEnabled(false);
		btnLicense=new Button("OK");
		dbLicenseStartDate=new DateBox();
		dbLicenseStartDate.setEnabled(false);
		dblicenseEndDate=new DateBox();
		ibnoofusers=new IntegerBox();
		licenseTable=new LicenseManagementTable();
		licenseTable.connectToLocal();
	}
	
	
	@Override
	public void createScreen() {
	    
		initalizeWidget();

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCompanyInformation=fbuilder.setlabel("Company Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("Enter Company ID",tbcompanyId);
		FormField ftbcompanyId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Company Name",tbcompanyName);
		FormField ftbcompanyName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingLicenseInformation=fbuilder.setlabel("License Date Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("License Start Date",dbLicenseStartDate);
		FormField fdbLicenseStartDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("License End Date",dblicenseEndDate);
		FormField fdblicenseEndDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnLicense);
		FormField fbtnLicense= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingUserInformation=fbuilder.setlabel("No Of User Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("No Of Licenses",ibnoofusers);
		FormField fibnoofusers= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingUpdateHistory=fbuilder.setlabel("Updation History").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("",licenseTable.getTable());
		FormField flicenseTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		  FormField[][] formfield = {
			
		  {fgroupingCompanyInformation},
		  {ftbcompanyId,ftbcompanyName},
		  {fgroupingLicenseInformation},
		  {fdbLicenseStartDate,fdblicenseEndDate},
		  {fgroupingUserInformation},
		  {fibnoofusers},
		  {fbtnLicense},
		  {fgroupingUpdateHistory},
		  {flicenseTable}
  
		  };

		  this.fields=formfield;
	}
	
	
	@Override
	public void updateModel(LicenseManagement model) 
	{
	}

	@Override
	public void updateView(LicenseManagement view) 
	{
	}
	
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.LICENSEMANAGEMENT,LoginPresenter.currentModule.trim());
	}
	
	
	@Override
	public void setCount(int count)
	{
	}
	
	

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		dbLicenseStartDate.setEnabled(false);
		tbcompanyName.setEnabled(false);
	}


	
	/***************************************Getters And Setters********************************************/
	
	
	public TextBox getTbcompanyId() {
		return tbcompanyId;
	}

	public void setTbcompanyId(TextBox tbcompanyId) {
		this.tbcompanyId = tbcompanyId;
	}

	public DateBox getDbLicenseStartDate() {
		return dbLicenseStartDate;
	}

	public void setDbLicenseStartDate(DateBox dbLicenseStartDate) {
		this.dbLicenseStartDate = dbLicenseStartDate;
	}

	public DateBox getDblicenseEndDate() {
		return dblicenseEndDate;
	}

	public void setDblicenseEndDate(DateBox dblicenseEndDate) {
		this.dblicenseEndDate = dblicenseEndDate;
	}

	public Button getBtnLicense() {
		return btnLicense;
	}

	public void setBtnLicense(Button btnLicense) {
		this.btnLicense = btnLicense;
	}

	public TextBox getTbcompanyName() {
		return tbcompanyName;
	}

	public void setTbcompanyName(TextBox tbcompanyName) {
		this.tbcompanyName = tbcompanyName;
	}

	public IntegerBox getIbnoofusers() {
		return ibnoofusers;
	}

	public void setIbnoofusers(IntegerBox ibnoofusers) {
		this.ibnoofusers = ibnoofusers;
	}

	public LicenseManagementTable getLicenseTable() {
		return licenseTable;
	}

	public void setLicenseTable(LicenseManagementTable licenseTable) {
		this.licenseTable = licenseTable;
	}
	
	
	
	
	
	

}
