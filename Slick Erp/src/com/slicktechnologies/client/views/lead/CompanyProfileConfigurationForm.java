package com.slicktechnologies.client.views.lead;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.datastudioreports.DsReportTable;
import com.slicktechnologies.shared.common.DsReport;
import com.slicktechnologies.shared.common.businessprocesslayer.CompanyProfileConfiguration;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class CompanyProfileConfigurationForm extends FormTableScreen<CompanyProfileConfiguration>{

	
	CheckBox chkStatus;
	ObjectListBox<ConfigCategory> olbLeadCategory;
	UploadComposite companyProfile;		
	CompanyProfileConfiguration compProfileObj;
	
	
	public CompanyProfileConfigurationForm(SuperTable<CompanyProfileConfiguration> table, int mode, boolean captionmode){
		super(table, mode, captionmode);
		createGui();
	}
	
	
	
	private void initalizeWidget()
	{
		companyProfile = new UploadComposite();
		chkStatus = new CheckBox();
		chkStatus.setValue(true);
		olbLeadCategory= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbLeadCategory, Screen.LEADCATEGORY);
	}
	
	
	
	
	

	@Override
	public void createScreen() {
		super.createScreen();
		initalizeWidget();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingReportDetails=fbuilder.setlabel("Company Profile Configuration Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Lead Category",olbLeadCategory);
		FormField folbeLeadCategory= fbuilder.setMandatory(false).setMandatoryMsg("Lead Category is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status",chkStatus);
		FormField fchkStatus = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Company Profile",companyProfile);
		FormField fucCompanyProfile= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = { 
				{fgroupingReportDetails},
				{folbeLeadCategory,fucCompanyProfile, fchkStatus },
		};
		this.fields=formfield;
	}

	
	

	@Override
	public void updateModel(CompanyProfileConfiguration model) {
		if(olbLeadCategory.getValue()!=null)
			model.setLeadCategory(olbLeadCategory.getValue());
		
		
		if(chkStatus.getValue()!=null)
			model.setStatus(chkStatus.getValue());
		
		if(companyProfile.getValue()!=null)
		model.setCompProfileConfig(companyProfile.getValue());
		
		
		compProfileObj=model;
		presenter.setModel(model);
		
	}

	@Override
	public void updateView(CompanyProfileConfiguration view) {
		compProfileObj=view;
		
		if(view.getLeadCategory()!=null)
			olbLeadCategory.setValue(view.getLeadCategory());
		
		chkStatus.setValue(view.isStatus());
		
		if(view.getCompProfileConfig()!=null)
			companyProfile.setValue(view.getCompProfileConfig());
		
		presenter.setModel(view);
		
	}

	
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.REPORTSCONFIGURATIONS,LoginPresenter.currentModule.trim());
	}


	@Override
	public void clear() 
	{	
		super.clear();
		chkStatus.setValue(true);
	}
}
