package com.slicktechnologies.client.views.lead;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummary;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;

public  class ProductStockDetailTableView  extends SuperTable<ProductInventoryViewDetails> {
	
	 TextColumn<ProductInventoryViewDetails> prodid;
	 TextColumn<ProductInventoryViewDetails> prodname;
	 TextColumn<ProductInventoryViewDetails> prodcode;
	 TextColumn<ProductInventoryViewDetails> warehousename;
	 TextColumn<ProductInventoryViewDetails> storagelocation;
	 TextColumn<ProductInventoryViewDetails> storagebin;
	 TextColumn<ProductInventoryViewDetails> availableqty;

	public ProductStockDetailTableView(){
		setHeight("350px");
	}
	
	public void createTable(){
		addColumngetproId();
		addColumngetproCode();
		addColumngetproName();
		addColumnGetAvailableQty();
		addColumnGetWareHouseName();
		addColumnGetStorageLocation();
		addColumnGetStorageBin();
	}
	
	private void addColumngetproId(){
		prodid = new  TextColumn<ProductInventoryViewDetails>() {
			public String getValue(ProductInventoryViewDetails object)
			{
					return object.getProdid()+"";
			}
		};
		table.addColumn(prodid,"Product Id" );
		table.setColumnWidth(prodid, 100, Unit.PX);
	}
	
	private void addColumngetproCode(){
		prodcode = new  TextColumn<ProductInventoryViewDetails>() {
			public String getValue(ProductInventoryViewDetails object)
			{
				if(object.getProdcode()!=null)
				{
					return object.getProdcode()+"";
				}
				else{
					return "";
				}
				
			}
		};
		table.addColumn(prodcode,"Product Code");
		table.setColumnWidth(prodcode, 100, Unit.PX);
	}
	
	private void addColumngetproName(){
		prodname = new  TextColumn<ProductInventoryViewDetails>() {
			public String getValue(ProductInventoryViewDetails object)
			{
				if(object.getProdname()!=null){
					return object.getProdname()+"";
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(prodname,"Product Name" );
		table.setColumnWidth(prodname, 100, Unit.PX);
	}
	
	private void addColumnGetWareHouseName(){
		warehousename = new  TextColumn<ProductInventoryViewDetails>() {
			public String getValue(ProductInventoryViewDetails object)
			{
				if(object.getWarehousename()!=null){
					return object.getWarehousename()+"";
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(warehousename,"WareHouse" );
		table.setColumnWidth(warehousename,100, Unit.PX);
	}
	
	private void addColumnGetStorageLocation(){
		storagelocation = new  TextColumn<ProductInventoryViewDetails>() {
			public String getValue(ProductInventoryViewDetails object)
			{
				if(object.getStoragelocation()!=null){
					return object.getStoragelocation()+"";
					}
					else{
						return "";
					}
			}
		};
		table.addColumn(storagelocation,"Storage Location" );
		table.setColumnWidth(storagelocation, 100, Unit.PX);
	}
	
	
	private void addColumnGetStorageBin(){
		storagebin = new  TextColumn<ProductInventoryViewDetails>() {
			public String getValue(ProductInventoryViewDetails object)
			{
				if(object.getStoragebin()!=null){
					return object.getStoragebin()+"";
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(storagebin,"Storage Bin" );
		table.setColumnWidth(storagebin, 100, Unit.PX);
	}
	
	private void addColumnGetAvailableQty(){
		availableqty = new  TextColumn<ProductInventoryViewDetails>() {
			public String getValue(ProductInventoryViewDetails object)
			{
					return object.getAvailableqty()+"";
			}
		};
		table.addColumn(availableqty,"Available Qty" );
		table.setColumnWidth(availableqty, 70, Unit.PX);
	}
	@Override
	public void setEnable(boolean state) {

	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}
}
