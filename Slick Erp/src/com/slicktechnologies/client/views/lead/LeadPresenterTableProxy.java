package com.slicktechnologies.client.views.lead;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.client.views.lead.LeadPresenter.LeadPresenterTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;

public class LeadPresenterTableProxy extends LeadPresenterTable 
{
	TextColumn<Lead> getTitleColumn;
	TextColumn<Lead> getPriorityColumn;
	TextColumn<Lead> getCustomerIdColumn;
	TextColumn<Lead> getCustomerCellNumberColumn;
	TextColumn<Lead> getCustomerFullNameColumn;

	TextColumn<Lead> getBranchColumn;
	TextColumn<Lead> getEmployeeColumn;
	TextColumn<Lead> getStatusColumn;
	TextColumn<Lead> getCreationDateColumn;
	TextColumn<Lead> getGroupColumn;
	TextColumn<Lead> getCategoryColumn;
	TextColumn<Lead> getTypeColumn;
	TextColumn<Lead> getCountColumn;
	
	/**
	 * rohan added this code for NBHC for 
	 */

	TextColumn<Lead> getCreatedByColumn;
	/**	  
	 *added by komal for followup date
	 */
	 TextColumn<Lead> getFollowUpDateColumn;
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("getTitleColumn"))
			return this.getTitleColumn;
		if(varName.equals("getPriorityColumn"))
			return this.getPriorityColumn;
		if(varName.equals("getCustomerCellNumberColumn"))
			return this.getCustomerCellNumberColumn;
		if(varName.equals("getCustomerFullNameColumn"))
			return this.getCustomerFullNameColumn;
		if(varName.equals("getTypeColumn"))
			return this.getTypeColumn;
		if(varName.equals("getBranchColumn"))
			return this.getBranchColumn;
		if(varName.equals("getEmployeeColumn"))
			return this.getEmployeeColumn;
		if(varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if(varName.equals("getCreationDateColumn"))
			return this.getCreationDateColumn;
		if(varName.equals("getGroupColumn"))
			return this.getGroupColumn;
		if(varName.equals("getCategoryColumn"))
			return this.getCategoryColumn;
		if(varName.equals("getTypeColumn"))
			return this.getTypeColumn;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		/**	  
		 *added by komal for followup date
		 */
		if(varName.equals("getFollowUpDateColumn"))
			return this.getFollowUpDateColumn;
		return null ;
	}
	public LeadPresenterTableProxy()
	{
		super();
	}
	@Override public void createTable() {
		addColumngetCount();
		/**	  
		 *added by komal for followup date
		 */
		addColumngetFollowUpDate();
		addColumngetCustomerId();
		addColumngetCustomerFullName();
		addColumngetCustomerCellNumber();
		addColumngetEmployee();
		addColumngetCreationDate();
		addColumngetGroup();
		addColumngetCategory();
		addColumngetType();
		addColumngetPriority();
		addColumngetBranch();
		addColumngetStatus();
		addColumngetCreatedBy();
		addColumnSorting();
	}
	
	
	
	
	private void addColumngetCreatedBy() {
		
		getCreatedByColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				return object.getCreatedBy();
			}
				};
				table.addColumn(getCreatedByColumn,"Created By");
				table.setColumnWidth(getCreatedByColumn, 110, Unit.PX);
				getCreatedByColumn.setSortable(true);
	}
	
	
	protected void addSortinggetCreatedBy()
	{
		List<Lead> list=getDataprovider().getList();
		columnSort=new ListHandler<Lead>(list);
		columnSort.setComparator(getCreatedByColumn, new Comparator<Lead>()
				{
			@Override
			public int compare(Lead e1,Lead e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreatedBy()!=null && e2.getCreatedBy()!=null){
						return e1.getCreatedBy().compareTo(e2.getCreatedBy());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Lead>()
				{
			@Override
			public Object getKey(Lead item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetBranch();
		addSortinggetEmployee();
		addSortinggetStatus();
		addSortinggetType();
		addSortinggetGroup();
		addSortinggetCategory();
		addSortinggetType();
		addSortinggetTitle();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortinggetPriority();
		addSortinggetCustomerId();
		addSortinggetCreationDate();
		addSortinggetCreatedBy();
		/**	  
		 *added by komal for followup date
		 */
		addSortinggetFollowUpDate();
	}
	@Override public void addFieldUpdater() {
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				table.setColumnWidth(getCountColumn, 100, Unit.PX);
				getCountColumn.setSortable(true);
	}
	protected void addColumngetCreationDate()
	{
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date= new DatePickerCell(fmt);
		getCreationDateColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				if(object.getCreationDate()!=null){
					return AppUtility.parseDate(object.getCreationDate());}
				else{
					object.setCreationDate(new Date());
					return "N.A";
				}
			}
				};
				table.addColumn(getCreationDateColumn,"Creation Date");
				table.setColumnWidth(getCreationDateColumn, 110, Unit.PX);
				getCreationDateColumn.setSortable(true);
	}
	
	protected void addSortinggetCreationDate()
	  {
	  List<Lead> list=getDataprovider().getList();
	  columnSort=new ListHandler<Lead>(list);
	  columnSort.setComparator(getCreationDateColumn, new Comparator<Lead>()
	  {
	  @Override
	  public int compare(Lead e1,Lead e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
	  return e1.getCreationDate().compareTo(e2.getCreationDate());}
	  }
	  else{
	  return 0;}
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  }
	
	protected void addSortinggetBranch()
	{
		List<Lead> list=getDataprovider().getList();
		columnSort=new ListHandler<Lead>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<Lead>()
				{
			@Override
			public int compare(Lead e1,Lead e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetCount()
 {
		List<Lead> list = getDataprovider().getList();
		columnSort = new ListHandler<Lead>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Lead>() {
			@Override
			public int compare(Lead e1, Lead e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				return object.getBranch()+"";
			}
				};
				table.addColumn(getBranchColumn,"Branch");
				table.setColumnWidth(getBranchColumn, 110, Unit.PX);
				getBranchColumn.setSortable(true);
	}
	protected void addSortinggetEmployee()
	{
		List<Lead> list=getDataprovider().getList();
		columnSort=new ListHandler<Lead>(list);
		columnSort.setComparator(getEmployeeColumn, new Comparator<Lead>()
				{
			@Override
			public int compare(Lead e1,Lead e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployee()
	{
		getEmployeeColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				return object.getEmployee()+"";
			}
				};
				table.addColumn(getEmployeeColumn,"Sales Person");
				table.setColumnWidth(getEmployeeColumn, 120, Unit.PX);
				getEmployeeColumn.setSortable(true);
	}
	protected void addSortinggetStatus()
	{
		List<Lead> list=getDataprovider().getList();
		columnSort=new ListHandler<Lead>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Lead>()
				{
			@Override
			public int compare(Lead e1,Lead e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				table.setColumnWidth(getStatusColumn,"100px");
				getStatusColumn.setSortable(true);
	}

	protected void addSortinggetGroup()
	{
		List<Lead> list=getDataprovider().getList();
		columnSort=new ListHandler<Lead>(list);
		columnSort.setComparator(getGroupColumn, new Comparator<Lead>()
				{
			@Override
			public int compare(Lead e1,Lead e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getGroup()!=null && e2.getGroup()!=null){
						return e1.getGroup().compareTo(e2.getGroup());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetGroup()
	{
		getGroupColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				return object.getGroup()+"";
			}
				};
				table.addColumn(getGroupColumn,"Group");
				table.setColumnWidth(getGroupColumn, 100, Unit.PX);
				getGroupColumn.setSortable(true);
	}
	protected void addSortinggetCategory()
	{
		List<Lead> list=getDataprovider().getList();
		columnSort=new ListHandler<Lead>(list);
		columnSort.setComparator(getCategoryColumn, new Comparator<Lead>()
				{
			@Override
			public int compare(Lead e1,Lead e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCategory()!=null && e2.getCategory()!=null){
						return e1.getCategory().compareTo(e2.getCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCategory()
	{
		getCategoryColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				return object.getCategory()+"";
			}
				};
				table.addColumn(getCategoryColumn,"Category");
				table.setColumnWidth(getCategoryColumn, 110, Unit.PX);
				getCategoryColumn.setSortable(true);
	}
	protected void addSortinggetType()
	{
		List<Lead> list=getDataprovider().getList();
		columnSort=new ListHandler<Lead>(list);
		columnSort.setComparator(getTypeColumn, new Comparator<Lead>()
				{
			@Override
			public int compare(Lead e1,Lead e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getType()!=null && e2.getType()!=null){
						return e1.getType().compareTo(e2.getType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetType()
	{
		getTypeColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				return object.getType()+"";
			}
				};
				table.addColumn(getTypeColumn,"Type");
				table.setColumnWidth(getTypeColumn, 110, Unit.PX);
				getTypeColumn.setSortable(true);
	}
	protected void addSortinggetTitle()
	{
		List<Lead> list=getDataprovider().getList();
		columnSort=new ListHandler<Lead>(list);
		columnSort.setComparator(getTitleColumn, new Comparator<Lead>()
				{
			@Override
			public int compare(Lead e1,Lead e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getTitle()!=null && e2.getTitle()!=null){
						return e1.getTitle().compareTo(e2.getTitle());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetTitle()
	{
		getTitleColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				return object.getTitle()+"";
			}
				};
				table.addColumn(getTitleColumn,"Lead Title");
				getTitleColumn.setSortable(true);
	}
	protected void addSortinggetCustomerFullName()
	{
		List<Lead> list=getDataprovider().getList();
		columnSort=new ListHandler<Lead>(list);
		columnSort.setComparator(getCustomerFullNameColumn, new Comparator<Lead>()
				{
			@Override
			public int compare(Lead e1,Lead e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCustomerFullName()!=null && e2.getCustomerFullName()!=null){
						return e1.getCustomerFullName().compareTo(e2.getCustomerFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerFullName()
	{
		getCustomerFullNameColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				return object.getCustomerFullName()+"";
			}
				};
				table.addColumn(getCustomerFullNameColumn,"Customer Name");
				table.setColumnWidth(getCustomerFullNameColumn, 110, Unit.PX);
				getCustomerFullNameColumn.setSortable(true);
	}
	protected void addSortinggetCustomerCellNumber()
	{
		List<Lead> list=getDataprovider().getList();
		columnSort=new ListHandler<Lead>(list);
		columnSort.setComparator(getCustomerCellNumberColumn, new Comparator<Lead>()
				{
			@Override
			public int compare(Lead e1,Lead e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCustomerCellNumber()== e2.getCustomerCellNumber()){
						return 0;}
					if(e1.getCustomerCellNumber()> e2.getCustomerCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerCellNumber()
	{
		
		getCustomerCellNumberColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				if(object.getCustomerCellNumber()==0){
					return 0+"";}
				else{
					return object.getCustomerCellNumber()+"";}
			}
				};
				table.addColumn(getCustomerCellNumberColumn,"Customer Cell");
				table.setColumnWidth(getCustomerCellNumberColumn, 110, Unit.PX);
				getCustomerCellNumberColumn.setSortable(true);
	}
	protected void addFieldUpdatergetCustomerCellNumber()
	{
		
	}
	protected void addSortinggetPriority()
	{
		List<Lead> list=getDataprovider().getList();
		columnSort=new ListHandler<Lead>(list);
		columnSort.setComparator(getPriorityColumn, new Comparator<Lead>()
				{
			@Override
			public int compare(Lead e1,Lead e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPriority()!=null && e2.getPriority()!=null){
						return e1.getPriority().compareTo(e2.getPriority());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerId()
	{
		getCustomerIdColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				return object.getPersonInfo().getCount()+"";
			}
				};
				table.addColumn(getCustomerIdColumn,"Customer ID");
				table.setColumnWidth(getCustomerIdColumn,100,Unit.PX);
				getCustomerIdColumn.setSortable(true);
	}
	
	
	protected void addSortinggetCustomerId()
	  {
	  List<Lead> list=getDataprovider().getList();
	  columnSort=new ListHandler<Lead>(list);
	  columnSort.setComparator(getCustomerIdColumn, new Comparator<Lead>()
	  {
	  @Override
	  public int compare(Lead e1,Lead e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
	  return 0;}
	  if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
	  return 1;}
	  else{
	  return -1;}
	  }
	  else{
	  return 0;}
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  }
	protected void addColumngetPriority()
	{
		getPriorityColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				return object.getPriority()+"";
			}
				};
				table.addColumn(getPriorityColumn,"Lead Priority");
				table.setColumnWidth(getPriorityColumn, 110, Unit.PX);
				getPriorityColumn.setSortable(true);
	}
	
	/**	  
	 *added by komal for followup date
	 */
	protected void addColumngetFollowUpDate()
	{
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date= new DatePickerCell(fmt);
		getFollowUpDateColumn=new TextColumn<Lead>()
				{
			@Override
			public String getValue(Lead object)
			{
				if(object.getFollowUpDate()!=null){
					return AppUtility.parseDate(object.getFollowUpDate());}
				else{
					//object.setFollowUpDate(null);
					return "N.A.";
				}
			}
				};
				table.addColumn(getFollowUpDateColumn,"Follow-up Date");
				table.setColumnWidth(getFollowUpDateColumn, 110, Unit.PX);
				getFollowUpDateColumn.setSortable(true);
	}

	protected void addSortinggetFollowUpDate()
	  {
	  List<Lead> list=getDataprovider().getList();
	  columnSort=new ListHandler<Lead>(list);
	  columnSort.setComparator(getFollowUpDateColumn, new Comparator<Lead>()
	  {
	  @Override
	  public int compare(Lead e1,Lead e2)
	  {
	  if(e1!=null && e2!=null)
	  {
		  if(e1.getFollowUpDate() == null && e2.getFollowUpDate()==null){
		        return 0;
		    }
		    if(e1.getFollowUpDate() == null) return 1;
		    if(e2.getFollowUpDate() == null) return -1;
		    return e1.getFollowUpDate().compareTo(e2.getFollowUpDate());
	  }
	  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  }
}
