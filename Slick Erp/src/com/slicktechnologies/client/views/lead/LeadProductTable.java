package com.slicktechnologies.client.views.lead;

import java.util.ArrayList;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.contract.SalesLineItemTable;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.client.utils.Console;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.client.views.ratecontractproductpopup.ProductInfo;
import com.slicktechnologies.shared.common.productlayer.Tax;

public class LeadProductTable extends SuperTable<SalesLineItem> {
	
	TextColumn<SalesLineItem> productSrNo;
	Column<SalesLineItem,String> deleteColumn;
	TextColumn<SalesLineItem> prodIdColumn,prodCodeColumn,prodNameColumn,viewdurationColumn;
	TextColumn<SalesLineItem> unitOfMeasurementColumn,prodCategoryColumn;
	Column<SalesLineItem, String> priceColumn;
	
	//  rohan added this for changing product name in lead screen Date : 10-04-2017
	Column<SalesLineItem,String> prodNameEditColumn;
	
	// vijay
		Column<SalesLineItem,String> getColumnServiceTax,vatColumn;
		Column<SalesLineItem,String> getColumnTotalAmt;
		
		public  ArrayList<TaxDetails> servicetaxlist;
		public  ArrayList<TaxDetails> vattaxlist;
		
		public  ArrayList<String> list;
		public  ArrayList<String> vatlist;

		TextColumn<SalesLineItem> viewServiceTaxColumn;
		TextColumn<SalesLineItem> viewVatTaxColumn;
	
	NumberFormat nf=NumberFormat.getFormat("0.00");
	TextColumn<SalesLineItem> viewPriceColumn;
	
	/** date 27/10/2017 added by komal  to add quantity column **/
	Column<SalesLineItem,String> getColumnEditQuantity;
	TextColumn<SalesLineItem> getColumnViewQuantity;
	
	
	private ServiceProduct serviceProduct;
	/**11-3-2019 added by Amol for No of Services columnS**/
	Column<SalesLineItem,String>getEditedNoOfServices;
	TextColumn<SalesLineItem> getNoOfServices;
	
	/**Date 21-9-2019 by Amol added discount column and percent discount column raised by Rahul T.**/
	TextColumn<SalesLineItem> viewColumnDiscountInPercent;
	Column<SalesLineItem,String>getColumnDiscountInPercent;
	TextColumn<SalesLineItem>viewColumnDiscountAmount;
	Column<SalesLineItem,String>getColumnDiscountAmount;
	int productIdCount=0;
	int productSrNoCount=0;
	GWTCAlert alert = new GWTCAlert();
	
	/**
	 * @author Anil ,Date : 16-11-2019
	 * Make price column non editable and make editable only price is zero
	 * raised by rahul tiwari for orkins
	 */
	boolean disablePriceCol=false;
	
	Column<SalesLineItem,Date> startDateColumn;
	TextColumn<SalesLineItem> viewStartDateColumn;
	
	Column<SalesLineItem,Date> serviceEndDateCol;
	TextColumn<SalesLineItem> viewServiceEndDateCol;
	Column<SalesLineItem,String> durationColumn;
	
	Column<SalesLineItem,Boolean> getColIsTaxInclusive;
	TextColumn<SalesLineItem> viewColIsTaxInclusive;
	
	public LeadProductTable()
	{
		super();
		table.setHeight("300px");
	}
	public LeadProductTable(UiScreen<SalesLineItem> view)
	{
		super(view);
		table.setHeight("300px");
	}
	
	
	public void createTable()
	{
		Console.log("LEAD CREATE TABLE : ");
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "MakePriceColumnDisable")){
			disablePriceCol=true;
			Console.log("dis flag value : "+disablePriceCol);
		}
		createColumnprodSrNoColumn();
		createColumnprodNameEditColumn();
//		createColumnprodNameColumn();
		createColumnUnitOfMeasurement();
		/**Date 9-3-2019 added by Amol to add No of Services***/
		/*** Date 20-11-2020 added by vijay for sign up fuctionality start date duration end date***/
  		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		if(screenName.toString().trim().equals(AppConstants.SCREENTYPESERVICELEAD))
		{
			createColumnStartDate();
			createColumnEndDate();
			createColumnDuration();
			createColumnEditedNoOfServices();
		}
		
		/** date 27/10/2017 added by komal  to add quantity column **/
		createColumnEditQuantity();
		
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "MakePriceColumnDisable")){
//			createViewColumnpriceColumn();
//		}else{
			createColumnpriceColumn();
//		}
		/**Date 21-9-2019 by Amol added a Discount Amount**/
		createColumnPercDiscount();
		createColumnDiscountAmt();
		// vijay
		retriveServiceTax();
		
		
//		createColumndeleteColumn();
//		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);	
		table.setWidth("auto");
		
	}
	
	

	
	

	private void createviewDiscountAmountInPercent() {
		viewColumnDiscountInPercent=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getPercentageDiscount()==0){
					return 0+"";}
				else{
					return object.getPercentageDiscount()+"";}
			}
				};
				table.addColumn(viewColumnDiscountInPercent,"#% Disc");
				table.setColumnWidth(viewColumnDiscountInPercent, 100,Unit.PX);
	}
	
	protected void createColumnPercDiscount()
	{
		EditTextCell editCell=new EditTextCell();
		getColumnDiscountInPercent=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getPercentageDiscount()==0||object.getPercentageDiscount()==null){
					return 0+"";}
				else{
					return object.getPercentageDiscount()+"";}
			}
				};
				table.addColumn(getColumnDiscountInPercent,"#% Disc");
				table.setColumnWidth(getColumnDiscountInPercent, 100,Unit.PX);
               
	}
	
	
	
	private void createViewDiscountAmount() {
		viewColumnDiscountAmount=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getDiscountAmt()==0){
					return 0+"";}
				else{
					return object.getDiscountAmt()+"";}
			}
				};
				table.addColumn(viewColumnDiscountAmount,"#Disc Amt");
				table.setColumnWidth(viewColumnDiscountAmount, 100,Unit.PX);
	}
	
	private void createColumnDiscountAmt() {
		EditTextCell editCell=new EditTextCell();
		getColumnDiscountAmount=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getDiscountAmt()==0){
					return 0+"";}
				else{
					return object.getDiscountAmt()+"";}
			}
				};
				table.addColumn(getColumnDiscountAmount,"#Disc Amt");
				table.setColumnWidth(getColumnDiscountAmount, 100,Unit.PX);
	}
	
	
	
	private void retriveServiceTax() {


		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

		
			public void onSuccess(ArrayList<SuperModel> result) {
				
				System.out.println("result size of tax "+result);
				list=new ArrayList<String>();
				vatlist=new ArrayList<String>();
				servicetaxlist=new ArrayList<TaxDetails>();
				vattaxlist = new ArrayList<TaxDetails>();
				List<TaxDetails> backlist=new ArrayList<TaxDetails>();
				
				for (SuperModel model : result) {
					TaxDetails entity = (TaxDetails) model;
					
					backlist.add(entity);
					servicetaxlist.add(entity);
					vattaxlist.add(entity);
				}
				
				for(int i=0;i<backlist.size();i++){
//					System.out.println("old condition ==="+backlist.get(i).getServiceTax().equals(true));
//					if(backlist.get(i).getServiceTax()==true){
						list.add(backlist.get(i).getTaxChargeName());
						System.out.println("service tax list size"+list.size());
//					}
//					 if(backlist.get(i).getVatTax()==true){
						vatlist.add(backlist.get(i).getTaxChargeName());
						System.out.println("valist size"+vatlist.size());
//					}
					
					}
				
				
				addColumnTotalAmt();
				addEditColumnVatTax();
				addEditColumnServiceTax();
				
				getColIsTaxInclusive();
				
//				createColumnProdCategory();
//				createColumnprodCodeColumn();
//				createColumnprodIdColumn();
				createColumndeleteColumn();
				addFieldUpdater();
				
			}

		});
	
	}
	
	private void addEditColumnServiceTax() {


		SelectionCell employeeselection= new SelectionCell(list);
		getColumnServiceTax = new Column<SalesLineItem, String>(employeeselection) {
			@Override
			public String getValue(SalesLineItem object) {
			
				if (object.getServiceTax().getTaxConfigName()!= null) {
					object.setServiceTaxEdit(object.getServiceTax().getTaxConfigName());
					return object.getServiceTax().getTaxConfigName();
				} else
					return "N/A";
				
//				if (object.getServiceTaxEdit()!= null) {
//					System.out.println(" value in edittaxname ==="+object.getServiceTaxEdit());
//					System.out.println(" namee =="+object.getServiceTax().getTaxConfigName());
//					
//					return object.getServiceTaxEdit();
//				} else
//					return "N/A";
			}

			
		};
		table.addColumn(getColumnServiceTax, "# Tax 2");
		table.setColumnWidth(getColumnServiceTax,140,Unit.PX);
	
	}
	protected void addEditColumnVatTax() {

		SelectionCell employeeselection= new SelectionCell(vatlist);
		vatColumn = new Column<SalesLineItem, String>(employeeselection) {
			@Override
			public String getValue(SalesLineItem object) {
				
				if (object.getVatTax().getTaxConfigName()!= null) {
					object.setVatTaxEdit(object.getVatTax().getTaxConfigName());
					return object.getVatTax().getTaxConfigName();
				} else
					return "N/A";
			}
		};
		table.addColumn(vatColumn, "# Tax 1");
		table.setColumnWidth(vatColumn,130,Unit.PX);
		
	
		
	}
	protected void addColumnTotalAmt() {


		TextCell editCell=new TextCell();
		getColumnTotalAmt=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				SuperProduct product=object.getPrduct();
				double tax=removeAllTaxes(product);
				double origPrice=object.getPrice()-tax;
				
				/** date 27/10/2017 added by komal to calculate new price =quantity*price **/
//				origPrice = origPrice * object.getQty();
				
//				return nf.format(origPrice);
				return nf.format(calculateAmount(object));
				
			}
				};
				table.addColumn(getColumnTotalAmt,"Total");
				table.setColumnWidth(getColumnTotalAmt, 100,Unit.PX);
	
	}
	
	private void createColumnprodSrNoColumn() {
		
		productSrNo=new TextColumn<SalesLineItem>()
			{
				@Override
				public String getValue(SalesLineItem object)
				{
						return object.getProductSrNo()+"";}
			};
			/**
			 *  Date : 15-01-2021 By Priyanka.
			 *  Rename prodSrNo as "Sr Nr" as per Nitin sir Instru.
			 */
			table.addColumn(productSrNo,"Sr Nr");
			table.setColumnWidth(productSrNo, 120,Unit.PX);
}
	
	protected void createColumnprodIdColumn()
	{
		prodIdColumn=new TextColumn<SalesLineItem>()
		{
			@Override public String getValue(SalesLineItem object){
				return object.getPrduct().getCount()+"";
			}
		};
		table.addColumn(prodIdColumn,"Product ID");
		table.setColumnWidth(prodIdColumn, 130,Unit.PX);
	}
	
	protected void createColumnprodCodeColumn()
	{
		prodCodeColumn=new TextColumn<SalesLineItem>()
		{
		@Override public String getValue(SalesLineItem object){
			if(object.getProductCode()!=null){
				return object.getProductCode();
			}
			else{
				return "";
			}
		}
		};
		table.addColumn(prodCodeColumn,"Product Code");
		table.setColumnWidth(prodCodeColumn, 130,Unit.PX);
	}
	
	
	protected void createColumnprodNameEditColumn()
	{
		EditTextCell editCell=new EditTextCell();
		prodNameEditColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				return object.getProductName();
			}
				};
				table.addColumn(prodNameEditColumn,"#Name");
				table.setColumnWidth(prodNameEditColumn, 120,Unit.PX);

	}
	
	
	protected void createColumnprodNameColumn()
	{
		prodNameColumn=new TextColumn<SalesLineItem>()
		{
			@Override public String getValue(SalesLineItem object){
				if(object.getProductName()!=null){
					return object.getProductName();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(prodNameColumn,"#Name");
		table.setColumnWidth(prodNameColumn, 120,Unit.PX);
	}
	
	protected void createColumnProdCategory()
	{
		prodCategoryColumn=new TextColumn<SalesLineItem>()
		{
			@Override public String getValue(SalesLineItem object){
				if(object.getProductCategory()!=null){
					return object.getProductCategory();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(prodCategoryColumn,"Category");
		table.setColumnWidth(prodCategoryColumn, 130,Unit.PX);
	}
	
	protected void createColumnUnitOfMeasurement()
	{
		unitOfMeasurementColumn=new TextColumn<SalesLineItem>()
		{
			@Override public String getValue(SalesLineItem object){
				if(object.getUnitOfMeasurement()!=null){
					return object.getUnitOfMeasurement();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(unitOfMeasurementColumn,"UOM");
		table.setColumnWidth(unitOfMeasurementColumn, 90, Unit.PX);
	}
	
	protected void createColumnpriceColumn() {

		EditTextCell editCell = new EditTextCell();
		priceColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				SuperProduct product = object.getPrduct();
				double tax = removeAllTaxes(product);
				double origPrice = object.getPrice() - tax;
				return nf.format(origPrice);
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "MakePriceColumnDisable")){
					disablePriceCol=true;
				}
				
				Console.log("dis flag : "+disablePriceCol+" price : "+object.getPrice());
				if(disablePriceCol==false||(disablePriceCol==true&&object.getPrice()==0)){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(priceColumn, "#Price");
		table.setColumnWidth(priceColumn, 100, Unit.PX);
	}
	
	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<SalesLineItem,String>(btnCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
				table.setColumnWidth(deleteColumn, 100, Unit.PX);
	}
	
	private void createColumnNoOfServices(){
		getNoOfServices=new TextColumn<SalesLineItem>()
		{
			@Override public String  getValue(SalesLineItem object){
				if(object.getNumberOfServices()!=0){
					return object.getNumberOfServices()+"";
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getNoOfServices,"No Of Services");
		table.setColumnWidth(getNoOfServices, 90, Unit.PX);
	}
	
	
   private void createColumnEditedNoOfServices(){

		EditTextCell editCell = new EditTextCell();
		getEditedNoOfServices = new Column<SalesLineItem, String>(editCell) {
			public String getValue(SalesLineItem object) {
				return object.getNumberOfServices() + "";
			}
		};
		table.addColumn(getEditedNoOfServices, "#No Of Services");
		table.setColumnWidth(getEditedNoOfServices, 120, Unit.PX);
		
		getEditedNoOfServices.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override
               public void update(int index,SalesLineItem object,String value)
			    {
				try{
					
					int val1=Integer.parseInt(value);
					System.out.println("value"+val1);
					object.setNumberOfService(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				}
				catch (NumberFormatException e)
				{

				}
				
				table.redrawRow(index);
			}
				});
	}
	
	@Override 
	public void addFieldUpdater() 
	{
		createFieldUpdaterproductNameColumn();
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "MakePriceColumnDisable")){
//			createFieldUpdaterViewpriceColumn();	
//		}else{
		createFieldUpdaterpriceColumn();
//		}
		createFieldUpdaterdeleteColumn();
		createFieldUpdatorVatTaxColumn();
		createFieldUpdatorServiceTaxColumn();
		/** date 27/10/2017 added by komal to update quantity **/
  		createFieldUpdaterQuantityColumn();
  		createFieldUpdaterDiscountAmtColumn();
  		createFieldUpdaterDiscountAmtInPercent();
  		addFieldUpdaterTotal();
  		
  		
  		
  		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		if(screenName.toString().trim().equals(AppConstants.SCREENTYPESERVICELEAD)){
			addFieldUpdateronStartDate();
	  		addFieldUpdaterOnServiceEndDateColumn();
	  		createFieldUpdaterprodDurationColumn();
		}
	}
	
	private void createFieldUpdaterViewpriceColumn() {
		viewPriceColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{
				try
				{
					Double val1=Double.parseDouble(value.trim());
			
				object.setPrice(val1);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				
				}
				catch(Exception e)
				{
				}
				table.redrawRow(index);
			}
				});
	}
	
	private void createFieldUpdaterDiscountAmtInPercent() {
		getColumnDiscountInPercent.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setPercentageDiscount(val1);
//					
//					productIdCount=object.getPrduct().getCount();
//					productSrNoCount = object.getProductSrNo();
					
					/**28-09-2017 sagar sore to store total amount**/
					calculateAmount(object);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (Exception e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
	private void addFieldUpdaterTotal() {

		getColumnTotalAmt.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				double oldtotalAmt = object.getTotalAmount();
				double totalAmt = 0;
				try {
					totalAmt = Double.parseDouble(value);
					object.setTotalAmount(totalAmt);
				} catch (NumberFormatException e) {
					alert.alert("Only Numric values are allowed !");
					object.setTotalAmount(oldtotalAmt);
				}
				
				try {
					double area = Double.parseDouble(object.getArea());
					double price = totalAmt/area;
					object.setPrice(price);
						
				} catch (Exception e) {
					alert.alert("Please Define Area first for reverse calculation with Area!");
					object.setTotalAmount(object.getTotalAmount());
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
				setEnable(true);
			}
		});
	}
	private void createFieldUpdaterDiscountAmtColumn() {
		
		Console.log("Inside Discount Amount Updater method");
		getColumnDiscountAmount.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override
			public void update(int index,SalesLineItem object,String value)
			{
				try{
					Double val1=Double.parseDouble(value.trim());
					object.setDiscountAmt(val1);
//					productIdCount=object.getPrduct().getCount();
//					productSrNoCount = object.getProductSrNo();
					calculateAmount(object);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				catch (Exception e)
				{
				}
				table.redrawRow(index);
			}
				});
	}
	
	
	public double calculateAmount(SalesLineItem object){
		double total=0;
		Console.log("INSIDE CALCULATE TOTALAMOUNT");
//		double origPrice=object.getPrice();
		
		double tax = removeAllTaxes(object.getPrduct());
		double origPrice = object.getPrice() - tax;
		
		
		double qty=object.getQty();
		if(object.getDiscountAmt()!=0||object.getPercentageDiscount()!=null||object.getPercentageDiscount()!=0){
			total=origPrice*qty;
			total=total-(total*object.getPercentageDiscount()/100);
			total=total-object.getDiscountAmt();
		}
		object.setTotalAmount(total);
		return total;
	}
	
	
	protected void createFieldUpdaterproductNameColumn()
	{
		
		
		prodNameEditColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					System.out.println("value"+value);
					object.setProductName(value);
				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
	
	private void createFieldUpdatorVatTaxColumn() {

		vatColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				try {
					String val1 = (value.trim());
					object.setVatTaxEdit(val1);
					Tax tax = object.getVatTax();
					for(int i=0;i<vattaxlist.size();i++)
					{
//						if(val1.trim().equals(vattaxlist.get(i).getTaxChargeName())){
//							tax.setTaxName(val1);
//							tax.setPercentage(vattaxlist.get(i).getTaxChargePercent());
//							break;
//						}
						
						if(val1.trim().equals(vattaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(vattaxlist.get(i).getTaxChargePercent());
							/**
							 * rohan added this for GST implementation Date :23-06-2017
							 */
							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());
							
							/**
							 * Date : 03-08-2017 By ANIL
							 */
							tax.setTaxConfigName(val1);
							/**
							 * End
							 */
							/**
							 * nidhi
							 * 8-06-2018
							 * for tax selection option
							 */
//							checkTaxValue(tax,object);
							Tax taxDt =  AppUtility.checkTaxValue(tax, object.getServiceTax(), vattaxlist);
							if(taxDt != null){
								object.setServiceTax(taxDt);
							}
							/**
							 * end
							 */
							
							/**
							 * Date 28-07-2018 By Vijay
							 * Des :- If we select IGST then second tax box will be disable.if we select other than IGST then second tax box will be visible
							 */
							if(vattaxlist.get(i).getTaxPrintName().equals("IGST") || vattaxlist.get(i).getTaxPrintName().equals("IGST@18") || vattaxlist.get(i).getTaxPrintName().equals("SEZ")){
								getColumnServiceTax.setCellStyleNames("hideVisibility");
							}else{
								getColumnServiceTax.setCellStyleNames("showVisibility");
							}
							/**
							 * ends here
							 */
							
							break;
						}
					}
					object.setVatTax(tax);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	private void createFieldUpdatorServiceTaxColumn() {

		getColumnServiceTax.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				try {
					String val1 = (value.trim());
					object.setServiceTaxEdit(val1);
					Tax tax = object.getServiceTax();
					for(int i=0;i<servicetaxlist.size();i++)
					{
//						if(val1.trim().equals(servicetaxlist.get(i).getTaxChargeName())){
//							tax.setTaxName(val1);
//							tax.setPercentage(servicetaxlist.get(i).getTaxChargePercent());
//							break;
//						}
						
						if(val1.trim().equals(servicetaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(servicetaxlist.get(i).getTaxChargePercent());
							/**
							 * rohan added this for GST implementation Date :23-06-2017
							 */
							tax.setTaxPrintName(servicetaxlist.get(i).getTaxPrintName());
							/**
							 * Date : 03-08-2017 By ANIL
							 */
							tax.setTaxConfigName(val1);
							/**
							 * End
							 */
							/**
							 * nidhi
							 * 8-06-2018
							 */
							Tax vatTax =  AppUtility.checkTaxValue(tax, object.getVatTax(), vattaxlist);
							if(vatTax!=null){
								object.setVatTax(vatTax);
							}
							/**
							 * end
							 */
							
							/**
							 * Date 28-07-2018 By Vijay
							 * Des :- If we select IGST then second tax box will be disable.if we select other than IGST then second tax box will be visible
							 */
							
							if(servicetaxlist.get(i).getTaxPrintName().equals("IGST") || servicetaxlist.get(i).getTaxPrintName().equals("IGST@18") || servicetaxlist.get(i).getTaxPrintName().equals("SEZ")){
								vatColumn.setCellStyleNames("hideVisibility");
							}else{
								vatColumn.setCellStyleNames("showVisibility");
							}
							/**
							 * ends here
							 */
							
							break;
						}
					}
					object.setServiceTax(tax);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	
	}

	protected void createFieldUpdaterpriceColumn()
	{
		priceColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{
				try
				{
					Double val1=Double.parseDouble(value.trim());
			
				object.setPrice(val1);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				
				}
				catch(Exception e)
				{
				}
				table.redrawRow(index);
				setEnable(true); 

			}
				});
	}
	
	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<SalesLineItem,String>()
		{
			@Override
			public void update(int index,SalesLineItem object,String value)
			{
				getDataprovider().getList().remove(object);

				table.redrawRow(index);
			}
		});
	}
	
	@Override
	public void setEnable(boolean state)
	{
		Console.log("LEAD setEnable : "+state);
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "MakePriceColumnDisable")){
			disablePriceCol=true;
			Console.log("dis flag value : "+disablePriceCol);
		}
        for(int i=table.getColumnCount()-1;i>-1;i--)
    	  table.removeColumn(i); 
        	  
          if(state==true)
          {
        	  createColumnprodSrNoColumn();
        	createColumnprodIdColumn();
        	createColumnProdCategory();
      		createColumnprodCodeColumn();
      		createColumnprodNameEditColumn();
      		createColumnUnitOfMeasurement();
      		/** date 27/10/2017 added by komal to add quantuty edit column **/
      		createColumnEditQuantity();
      		
    		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
    		if(screenName.toString().trim().equals(AppConstants.SCREENTYPESERVICELEAD))
    		{
    			createColumnStartDate();
    			createColumnEndDate();
    			createColumnDuration();
    			createColumnEditedNoOfServices();
    		}
//      		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "MakePriceColumnDisable")){
//    			createViewColumnpriceColumn();
//    		}else{
    			createColumnpriceColumn();
//    		}
      		createColumnDiscountAmt();
      		createColumnPercDiscount();
      		retriveServiceTax();
      		
//      		createColumndeleteColumn();
//      		addFieldUpdater();
      		}
 
          else
          {
        	  createColumnprodSrNoColumn();
        	createColumnprodIdColumn();
        	createColumnProdCategory();
      		createColumnprodCodeColumn();
      		createColumnprodNameColumn();
      		createColumnUnitOfMeasurement();
      		
      		
      		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
    		if(screenName.toString().trim().equals(AppConstants.SCREENTYPESERVICELEAD))
    		{
    			createColumnStartDate();
    			createColumnEndDate();
    			createColumnDuration();
    			createColumnEditedNoOfServices();
    		}
    		
    		/** date 27/10/2017 added by komal to add quantuty edit column **/
      		createColumnViewQuantity();
      		createColumnpriceColumn();
      		
      		/**Date 21-9-2019 by Amol to add discount amount column**/
      		createViewDiscountAmount();
      		createviewDiscountAmountInPercent();
      		createViewVatTaxColumn();
      		createViewServiceTaxColumn();
      		addColumnTotalAmt();
      		
      		viewColIsTaxInclusive();
      		
//      		createColumnNoOfServices();
          }
          table.setWidth("auto");
	}

	private void createViewServiceTaxColumn() {

		viewServiceTaxColumn=new TextColumn<SalesLineItem>() {
			public String getValue(SalesLineItem object)
			{
				System.out.println(" vijay service tax name ==== "+object.getServiceTaxEdit());
				return object.getServiceTaxEdit();
			}
		};
		table.addColumn(viewServiceTaxColumn,"# Tax 2" );
//		table.addColumn(viewServiceTaxColumn,"#Service Tax" );
		table.setColumnWidth(viewServiceTaxColumn, 120,Unit.PX);
	
	}
	private void createViewVatTaxColumn() {

		viewVatTaxColumn=new TextColumn<SalesLineItem>() {
			public String getValue(SalesLineItem object)
			{
				return object.getVatTaxEdit();
			}
		};
		table.addColumn(viewVatTaxColumn,"# Tax 1" );
//		table.addColumn(viewVatTaxColumn,"#Vat Tax" );
		table.setColumnWidth(viewVatTaxColumn, 120,Unit.PX);
	
	}
	
	protected void createViewColumnpriceColumn(){

		viewPriceColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getPrice() == 0) {
					return 0 + "";
				} else {
					SuperProduct product = object.getPrduct();
					double tax = removeAllTaxes(product);
					double origPrice = object.getPrice() - tax;
					return nf.format(origPrice);
				}
			}
		};
		table.addColumn(viewPriceColumn, "#Price");
		table.setColumnWidth(viewPriceColumn, 100, Unit.PX);
	}
	
	public double removeAllTaxes(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				System.out.println("service tax isinclusive");
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				System.out.println("vat tax isinclusive");
				vat=prod.getVatTax().getPercentage();
			}
		}
		System.out.println("service"+service);
		System.out.println("vat"+vat);

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			System.out.println("vat!=0&&service==0 "+vat);
			retrVat=entity.getPrice()/(1+(vat/100));
			System.out.println("retrVat "+retrVat);
			retrVat=entity.getPrice()-retrVat;
			System.out.println("retrVat == "+retrServ);
		}
		if(service!=0&&vat==0){
			System.out.println("service!=0&&vat==0"+service);
			retrServ=entity.getPrice()/(1+service/100);
			System.out.println("retrServ "+retrServ);
			retrServ=entity.getPrice()-retrServ;
			System.out.println("retrServ == "+retrServ);

		}
		if(service!=0&&vat!=0){
//			// Here if both are inclusive then first remove service tax and then on that amount
//			// calculate vat.
//			double removeServiceTax=(entity.getPrice()/(1+service/100));
//						
//			//double taxPerc=service+vat;
//			//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
//			retrServ=(removeServiceTax/(1+vat/100));
//			retrServ=entity.getPrice()-retrServ;
			
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
//					retrVat=0;

				}
				else{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					System.out.println("dot "+dot);
					retrServ=(entity.getPrice()/(1+dot/100));
					System.out.println("retrServ "+retrServ);
					retrServ=entity.getPrice()-retrServ;
					System.out.println("retrServ "+retrServ);

				}
				else
				{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
		}
		System.out.println("retrVat "+retrVat);
		System.out.println("retrServ "+retrServ);
		tax=retrVat+retrServ;
		
		return tax;
	}
	
	@Override
	public void applyStyle()
	{

	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<SalesLineItem>()
				{
			@Override
			public Object getKey(SalesLineItem item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	
	public double calculateTotalExcludingTax() {

     Console.log("INSIDE TOTAL EXCLUSING TAX METHOD ");
		List<SalesLineItem>list=getDataprovider().getList();
		double sum=0,priceVal=0;
		for(int i=0;i<list.size();i++)
		{
			SalesLineItem entity=list.get(i);
			SuperProduct prod=entity.getPrduct();
			double taxAmt=removeAllTaxes(prod);
			  /** date 28/10/2017 added by komal to calculate total amount **/
			priceVal=(entity.getPrice()-taxAmt) * entity.getQty();
			if(entity.getDiscountAmt()!=0||entity.getPercentageDiscount()!=null){
			priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
			priceVal=priceVal-entity.getDiscountAmt();
			}
			//priceVal=entity.getPrice()-taxAmt;
			sum=sum+priceVal;
		}
		return sum;

	}
	
/** date 27/10/2017 added by komal for quantity column **/
	
	protected void createFieldUpdaterQuantityColumn()
	{
		getColumnEditQuantity.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override
               public void update(int index,SalesLineItem object,String value)
			    {
				try{
					System.out.println("value"+value);
					object.setQuantity(Double.parseDouble(value));
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				}
				catch (NumberFormatException e)
				{

				}
				
				table.redrawRow(index);
			}
				});
	}

	/** date 27/10/2017 added by komal for quantity column **/
	protected void createColumnEditQuantity() {

		EditTextCell editCell = new EditTextCell();
		getColumnEditQuantity = new Column<SalesLineItem, String>(editCell) {
			public String getValue(SalesLineItem object) {
				return object.getQty() + "";
			}
		};
//		table.addColumn(getColumnEditQuantity, "#Quantity"); //Commented by Ashwini
		table.addColumn(getColumnEditQuantity, "#Qty");//Added by Ashwini
		table.setColumnWidth(getColumnEditQuantity, 120, Unit.PX);
	}
	
	/** date 27/10/2017 added by komal for quantity column **/
	protected void createColumnViewQuantity() {

		getColumnViewQuantity = new TextColumn<SalesLineItem>() {
			public String getValue(SalesLineItem object) {
				return object.getQty() + "";
			}
		};
		table.addColumn(getColumnViewQuantity, "#Quantity");
		table.setColumnWidth(getColumnViewQuantity, 120, Unit.PX);
	}
	
	private void createColumnEndDate() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		serviceEndDateCol = new Column<SalesLineItem, Date>(date) {
			@Override
			public Date getValue(SalesLineItem object) {
				if (object.getEndDate() != null) {
					return fmt.parse(fmt.format(object.getEndDate()));
				} else {
					Date date=CalendarUtil.copyDate(object.getStartDate());
					CalendarUtil.addDaysToDate(date,object.getDuration()-1);
					object.setEndDate(date);
					return fmt.parse(fmt.format(date));
				}
			}
		};
		table.addColumn(serviceEndDateCol, "#Service End Date");
		table.setColumnWidth(serviceEndDateCol, 100, Unit.PX);
		
	
	}
	protected void addFieldUpdaterOnServiceEndDateColumn()
	{
		serviceEndDateCol.setFieldUpdater(new FieldUpdater<SalesLineItem, Date>() {
			@Override
			public void update(int index, SalesLineItem object, Date value) {
				object.setEndDate(value);
				int duration=CalendarUtil.getDaysBetween(object.getStartDate(), object.getEndDate());
				duration++;
				object.setDuration(duration);
				System.out.println("Updated End Date : "+value+" ST DATE : "+object.getStartDate()+" EN DATE : "+object.getEndDate());
				System.out.println("Contract Duration : "+duration+"  Dur "+object.getDuration());
				productIdCount = object.getPrduct().getCount();
				productSrNoCount = object.getProductSrNo();
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}
	
	protected void createViewServiceEndDateColumn() {
		viewServiceEndDateCol = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getEndDate() != null) {
					return AppUtility.parseDate(object.getEndDate());
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(viewServiceEndDateCol, "#Service End Date");
		table.setColumnWidth(viewServiceEndDateCol, 100, Unit.PX);
	}
	private void createColumnStartDate() {

		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date= new DatePickerCell(fmt);
		startDateColumn=new Column<SalesLineItem,Date>(date)
				{
			@Override
			public Date getValue(SalesLineItem object)
			{
				if(object.getStartDate()!=null){
						return fmt.parse(fmt.format(object.getStartDate()));
					}
				else{
				object.setStartDate(new Date());
					return fmt.parse(fmt.format(new Date()));
				}
			}
			
			
			};
			table.addColumn(startDateColumn,"#Service Start Date");
			table.setColumnWidth(startDateColumn, 100,Unit.PX);
	
	}

	protected void createViewStartDateColumn() {
		viewStartDateColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getStartDate() != null) {

					return AppUtility.parseDate(object.getStartDate());
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(viewStartDateColumn, "#Service Start Date");
		table.setColumnWidth(viewStartDateColumn, 100, Unit.PX);
	}
	
	protected void addFieldUpdateronStartDate(){

		startDateColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, Date>()
				{
			@Override
			public void update(int index,SalesLineItem object,Date value)
			{
				object.setStartDate(value);
				Date date=CalendarUtil.copyDate(object.getStartDate());
				CalendarUtil.addDaysToDate(date, object.getDuration()-1);	
				object.setEndDate(date);
				System.out.println("Updated Start Date : "+value+" ST DATE : "+object.getStartDate()+" EN DATE : "+object.getEndDate());
				productIdCount=object.getPrduct().getCount();
				productSrNoCount = object.getProductSrNo();
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
				});
	
	}
	
	private void createColumnDuration() {
		EditTextCell editCell = new EditTextCell();
		durationColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getDuration() == 0) {
					return 0 + "";
				} else {
					return object.getDuration() + "";
				}
			}
		};
		table.addColumn(durationColumn, "#Duration");
		table.setColumnWidth(durationColumn, 100, Unit.PX);
	}

	protected void createColumnViewDuration() {

		viewdurationColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getDuration() == 0)
					return "N.A";
				else {
					return object.getDuration() + "";
				}
			}
		};
		table.addColumn(viewdurationColumn, "#Duration");
		table.setColumnWidth(viewdurationColumn, 100, Unit.PX);
	}
	
	protected void createFieldUpdaterprodDurationColumn()
	{
		durationColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Integer val1=Integer.parseInt(value.trim());
					object.setDuration(val1);
					Date date=CalendarUtil.copyDate(object.getStartDate());
					CalendarUtil.addDaysToDate(date, object.getDuration()-1);
					object.setEndDate(date);
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (Exception e)
				{
				}
				table.redrawRow(index);
			}
		});
	}
	
private void getColIsTaxInclusive() {
		
		
		CheckboxCell cell=new CheckboxCell();
		getColIsTaxInclusive=new Column<SalesLineItem, Boolean>(cell) {
			@Override
			public Boolean getValue(SalesLineItem object) {
				
				if(object.getVatTax()!=null&&object.getServiceTax()!=null){
					if(object.getVatTax().isInclusive()==true&&object.getServiceTax().isInclusive()==true){
						object.setInclusive(true);
					}else if(object.getVatTax().isInclusive()==false&&object.getServiceTax().isInclusive()==false){
						object.setInclusive(false);
					}else{
						object.setInclusive(false);
					}
				}
				
				return object.isInclusive();
			}
		};
		table.addColumn(getColIsTaxInclusive, "Is Inclusive");
		table.setColumnWidth(getColIsTaxInclusive,80,Unit.PX);
		
		getColIsTaxInclusive.setFieldUpdater(new FieldUpdater<SalesLineItem, Boolean>() {
			@Override
			public void update(int index, SalesLineItem object, Boolean value) {
				object.setInclusive(value);
				object.getServiceTax().setInclusive(value);
				object.getVatTax().setInclusive(value);
				
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redraw();
				
				setEnable(true);
			}
		});
	}
	
	private void viewColIsTaxInclusive() {
		viewColIsTaxInclusive=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				// TODO Auto-generated method stub
				
				if(object.isInclusive()==true){
					return "Yes";
				}else{
					return "No";
				}
			}
		};
		table.addColumn(viewColIsTaxInclusive, "Is Inclusive");
		table.setColumnWidth(viewColIsTaxInclusive,80,Unit.PX);
	}
}
