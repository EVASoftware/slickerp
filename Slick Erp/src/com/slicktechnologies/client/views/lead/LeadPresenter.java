package com.slicktechnologies.client.views.lead;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.gwt.view.client.RowCountChangeEvent;
import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AbstractScreen;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.CopyDataConfig;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.services.XlsxService;
import com.slicktechnologies.client.services.XlsxServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.cnc.CNCForm;
import com.slicktechnologies.client.views.cnc.CNCPresenter;
import com.slicktechnologies.client.views.complain.NewCustomerPopup;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentlist.PaymentListPresenter;
import com.slicktechnologies.client.views.popups.LatestNewCustomerPopup;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.popups.SMSPopUp;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoForm;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoPresenter;
import com.slicktechnologies.client.views.quickcontract.QuickContractForm;
import com.slicktechnologies.client.views.quickcontract.QuickContractPresentor;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderForm;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderPresenter;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationForm;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationPresenter;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.ProjectCoordinatorDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogPopUp;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
/**
 * FormScreen presenter template.
 */
public class LeadPresenter extends FormScreenPresenter<Lead> implements RowCountChangeEvent.Handler , SelectionHandler<Suggestion>{

	//Token to set the concrete FormScreen class name
	public static LeadForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	//For retrieving Shipping address based on customer id
	Address addComposite=new Address();
	String retrCompnyState=retrieveCompanyState();
	ArrayList<CustomerBranchDetails> lisCustomerBranch=new ArrayList<CustomerBranchDetails>();
	EmailServiceAsync emailService=GWT.create(EmailService.class);
//	NewCustomerPopup custpoppup=new NewCustomerPopup();  //commented by Ashwini
	LatestNewCustomerPopup custpoppup = new LatestNewCustomerPopup (); //Added by Ashwini
	PopupPanel panel = new PopupPanel(true);
	
	//Date : 11-1-2022 By: Ashwini Patil
	GeneralViewDocumentPopup generalViewDocumentPopup=new GeneralViewDocumentPopup();
	
	SmsServiceAsync smsService = GWT.create(SmsService.class);
	
	/**
	 * Date 15 April 2017 addded by vijay for communication log
	 */
	CommunicationLogPopUp CommunicationLogPopUp = new CommunicationLogPopUp();
//	PopupPanel communicationPanel;
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	/**
	 * ends here
	 */
	boolean disablecellNovalidation=false;
	/***Date 29-8-2020 remove the email mandatory validation in 
	 * custpopup using this flag
	 */
	boolean disableEmailValidation=false;
	/**
	 * Date : 06-11-2017 BY ANIL
	 * Adding cancellation popup 
	 */
	DocumentCancellationPopUp cancelPopup = new DocumentCancellationPopUp();
	/**
	 * nidhi
	 * 3-08-2018
	 */
	DocumentCancellationPopUp unSucessfullPopup = new DocumentCancellationPopUp(true);
	PopupPanel cancelpanel ;
	/**
	 * UPdated By: Viraj
	 * Date: 16-04-2019
	 * Description: 
	 */
	GWTCAlert alert = new GWTCAlert();
	/** Ends **/
	/**
	 * End
	 */
	
	CommonServiceAsync commonservice = GWT.create(CommonService.class);
	NewEmailPopUp emailpopup = new NewEmailPopUp();
	
	SMSPopUp smspopup = new SMSPopUp();

	
//	int srno =0;
	public LeadPresenter  (FormScreen<Lead> view, Lead model) 
	{
		super(view, model);
		form=(LeadForm) view;
		form.setPresenter(this);
		
		form.getLeadProductTable().getTable().addRowCountChangeHandler(this);
		
		form.getPic().getId().addSelectionHandler(this);
		form.getPic().getName().addSelectionHandler(this);
		form.getPic().getPhone().addSelectionHandler(this);
		form.getAddProducts().addClickHandler(this);
		
		CommunicationLogPopUp.getBtnOk().addClickHandler(this);
		CommunicationLogPopUp.getBtnCancel().addClickHandler(this);
		CommunicationLogPopUp.getBtnAdd().addClickHandler(this);
		
		Screen scr=(Screen) AppMemory.getAppMemory().currentScreen;
		if(AppConstants.SCREENTYPESALESLEAD.equals(scr.toString().trim()))
		{
			boolean isDownload1=AuthorizationHelper.getDownloadAuthorization(Screen.SALESLEAD,AppConstants.SALESMODULE);
			if(isDownload1==false){
				form.getSearchpopupscreen().getDwnload().setVisible(false);
			}
		}
		
		if(AppConstants.SCREENTYPESERVICELEAD.equals(scr.toString().trim()))
		{
			boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.LEAD,AppConstants.SERVICEMODULE);
			if(isDownload==false){
				form.getSearchpopupscreen().getDwnload().setVisible(false);
			}
		}
		form.getNewCustomer().addClickHandler(this);
		custpoppup.getLblCancel().addClickHandler(this);
		custpoppup.getLblOk().addClickHandler(this);
		
		/**
		 * Date 26-5-2018 by jayshree 
		 */
		
		custpoppup.getBtnbillingaddr().addClickHandler(this);
		custpoppup.getBtnserviceaddr().addClickHandler(this);
		
		/** date 25/10/2017 added by komal for project coordinator add button **/
		form.getAddPCButton().addClickHandler(this);
		
		/**
		 * Date : 06-11-2017 BY ANIL
		 */
		cancelPopup.getBtnOk().addClickHandler(this);
		cancelPopup.getBtnCancel().addClickHandler(this);
		
		unSucessfullPopup.getBtnOk().addClickHandler(this);
		unSucessfullPopup.getBtnCancel().addClickHandler(this);
		/**
		 * End
		 */
	}

	
	
	
	
	@Override
	public void reactOnSearch() {
		super.reactOnSearch();
		LeadPresenterSearchProxy.makeSalesPersonEnable();
	}





	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
	{
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();

		if(text.equals("Create Quotation")){
			if(form.isPopUpAppMenubar()){
				Console.log("LEAD POPUP : Create Quotation clicked!!");
				return;
			}
			reactToCreateQuotation();
		}
		if(text.equals("Create Order")){
			if(form.isPopUpAppMenubar()){
				Console.log("LEAD POPUP : Create Order clicked!!");
				return;
			}
			/**
			 * Date 31-01-2019 by Vijay For NBHC CCPM Lead and quotation mandatory so contract creation not allowed 
			 * from here if process config is active 
			 * Date 17-04-2019 by Vijay Updated For Admin dont need this restriction.
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MakeLeadAndQuotationIdMandatory")
					&& !LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin")){
				form.showDialogMessage("Please create quotation first! contract creation not allow from here!");
			}else{
				reactToCreateContract();
			}
		}
		if(text.equals("View Services"))
			reactToViewServices();
		/**@Sheetal : 15-02-2022
		 *   Added View Contract button**/
		if(text.equals(AppConstants.VIEWORDER)){
			if(form.isPopUpAppMenubar()){
				Console.log("LEAD POPUP : View Order clicked!!");
				return;
			}
			viewOrderDocument();
			Console.log("inside view contract from lead");
		}
		if(text.equals("New")){
			if(form.isPopUpAppMenubar()){
				Console.log("LEAD POPUP : New clicked!!");
				return;
			}
			reactToNew();
		}
		if(text.equals("Email"))
			reactOnEmail();
		if(text.equals("Send SMS")){
			reactonSendSMS();
		}
		/** 26-09-2017 sagar sore Redirecting to either Quick contract or Quick order on 'Quick Order' button click **/
		if (text.equals("Quick Order")) {
			if(form.isPopUpAppMenubar()){
				Console.log("LEAD POPUP : Quick Order clicked!!");
				return;
			}
			Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
			String currentScrType = s.toString();
			if (AppConstants.SCREENTYPESERVICELEAD.equals(currentScrType)) {
				if (form.validate() == true) {
					reactToQuickContract();
				} else {
					form.showDialogMessage("Please fill mandatory fields.");
				}
			}
			if (AppConstants.SCREENTYPESALESLEAD.equals(currentScrType)) {
				if (form.validate() == true) {
					reactToQuickOrder();
				} else {
					form.showDialogMessage("Please fill mandatory fields.");
				}
			}
		}
		/**
		 * Date : 06-11-2017 BY ANIL
		 * cancellation with remark
		 * for NBHC
		 */
		if(text.equals(AppConstants.CANCEL)){
			cancelPopup.getPaymentDate().setValue(AppUtility.parseDate(new Date())+"");
			cancelPopup.getPaymentID().setValue(model.getCount()+"");
			cancelPopup.getPaymentStatus().setValue(model.getStatus());
			cancelpanel=new PopupPanel(true);
			cancelpanel.addStyleName("gwt-SuggestBoxPopup");
			cancelpanel.add(cancelPopup);
			cancelpanel.show();
			cancelpanel.center();
		}
		
		if(text.equals(AppConstants.Unsucessful)){
			unSucessfullPopup.getPaymentDate().setValue(AppUtility.parseDate(new Date())+"");
			unSucessfullPopup.getPaymentID().setValue(model.getCount()+"");
			unSucessfullPopup.getPaymentStatus().setValue(model.getStatus());
			cancelpanel=new PopupPanel(true);
//			cancelpanel.addStyleName("gwt-SuggestBoxPopup");
			cancelpanel.add(unSucessfullPopup);
			cancelpanel.show();
			cancelpanel.center();
		}
		/**
		 * End
		 */
		if(text.equals(AppConstants.LEADCUSTOMERINTRODUCTORY)){
			reactOnCustomerLeadMail();
		}
		if(text.equals("Send Company Profile")){
			reactOnSendCompanyProfile();
		}
		
		/**
		 * @author Vijay Chougule Date :- 07-09-2020
		 * Des :- TO Sign Up Payment GateWay
		 */
		if(text.equals(AppConstants.SIGNUP)){
			reactonSignUp();
		}
	}

	private void viewOrderDocument() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(Integer.parseInt(model.getContractCount()));
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Contract());
		Console.log("Query executed");
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No order document found.");
					return;
				}
				else{
					final Contract billDocument=(Contract) result.get(0);
					final ContractForm form=ContractPresenter.initalize();
					Console.log("Document list>0: "+result.size());
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}
				
			}
		});
		}


	private void reactOnSendCompanyProfile() {

			Console.log("in side reactOnSendCompanyProfile");
			boolean conf = Window.confirm("Do you really want to send email?");
			if (conf == true) {
				emailService.sendCompanyProfileToLead(model,new AsyncCallback<String>() {
		
					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Resource Quota Ended ");
						caught.printStackTrace();
						
					}
		
					@Override
					public void onSuccess(String result) {
						Window.alert(result);						
					}
				});
			}
	}





	/** Date :- 18 Feb 2017
	 * by Vijay
	 * Lead Enquiry SMS will send to customer.
	 * lead SMS needed by Ankita pest control
	 * 
	 */
	private void reactonSendSMS() {

		System.out.println("Inside send SMS method");
		form.showWaitSymbol();
		/**
		 * Updated By: Viraj
		 * Date: 12-04-2019
		 * Description: changed lead sms send method 
		 */
//		boolean ankitapestleadSms = true;
//		ArrayList<SalesLineItem> productlist = new ArrayList<SalesLineItem>();
//		productlist.addAll(model.getLeadProducts());
		String companyName = Slick_Erp.businessUnitName;
//		String description = model.getDescription();
//		String SingleServiceAmt[] = description.split("-");
//		try {
//			System.out.println(SingleServiceAmt[1].trim());
//		    int singleSerAmt = Integer.parseInt(SingleServiceAmt[1].trim());
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			Console.log("Exception :- "+e);
//			form.showDialogMessage("Description must have Single Service Amount - amount -");
//			ankitapestleadSms=false;
//			form.hideWaitSymbol();
//		}
		
//		if(ankitapestleadSms){
			
			smsService.checkSMSConfigAndSendSMS(model.getCompanyId(), model.getPersonInfo().getCellNumber(), model.getCount(), companyName,model.getPersonInfo().getCount(), new AsyncCallback<ArrayList<Integer>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Unexpected Error");
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<Integer> result) {
					// TODO Auto-generated method stub
					if(result.contains(-1)){
						form.showDialogMessage("You cant send SMS if you want send SMS Contact to EVA Software Solutions!");
					}
					else if(result.contains(2)){
						form.showDialogMessage("SMS Configuration is InActive");
					}
					else if(result.contains(3)){
						form.showDialogMessage("Please define SMS Template");
					}
					else if(result.contains(4)){
						form.showDialogMessage("SMS Template is InActive");
					}
					else if(result.contains(5)){
						form.showDialogMessage("Can not send SMS customer DND status is active");
					}
					else if(result.contains(1)){
						form.showDialogMessage("SMS Sent Sucessfully");
					}
					
					form.hideWaitSymbol();
				}
			});
//		}
		
	}
/** Ends **/
	
	/**
	 * End here
	 */
	
	@Override
	public void reactOnPrint() {


	}

	@Override
	public void reactOnDownload() 
	{
		final ArrayList<Lead> custarray=new ArrayList<Lead>();
		List<Lead> list=(List<Lead>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		custarray.addAll(list);
//		boolean communicationlogflag = false;
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "CommunicationLogColumn")){
//			communicationlogflag = true;
//		}
//		if(communicationlogflag){
//			ArrayList<SuperModel> superlist = new ArrayList<SuperModel>();
//			superlist.addAll(custarray);
//			csvservice.getData(superlist, AppConstants.LEAD, new AsyncCallback<ArrayList<SuperModel>>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				System.out.println("RPC call Failed"+caught);
//				
//			}
//
//				@Override
//				public void onSuccess(ArrayList<SuperModel> result) {
//					// TODO Auto-generated method stub
//					ArrayList<InteractionType> interactiontypelist = new ArrayList<InteractionType>();
//					for(SuperModel model : result){
//						InteractionType interactionType = (InteractionType) model;
//						interactiontypelist.add(interactionType);
//					}
//					downloadData(custarray,true,interactiontypelist);
//
//				}
//			});
//		}
//		else{
//			downloadData(custarray,false,null);
//		}
		csvservice.setleadslist(custarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+3;
				Window.open(url, "test", "enabled");
				
//				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//				final String url=gwt + "CreateXLXSServlet"+"?type="+10;
//				Window.open(url, "test", "enabled");
				
			}
		});

	}

	@Override
	public void reactOnEmail() {
		System.out.println("in side email method");
//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true) {
//			emailService.emailToSalesPersonFromLead(model,new AsyncCallback<Void>() {
//	
//				@Override
//				public void onFailure(Throwable caught) {
//					Window.alert("Resource Quota Ended ");
//					caught.printStackTrace();
//					
//				}
//	
//				@Override
//				public void onSuccess(Void result) {
//					Window.alert("Email Sent Sucessfully !");
//					
//				}
//			});
//		}
		
		/**
		 * @author Vijay Date 19-03-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData();
		
		/**
		 * ends here
		 */
	
		
	}

	





	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model=new Lead();
	}

	/** 26-09-2017 sagar sore reactToQuickContract function added to create Quick contract from Lead **/
	private void reactToQuickContract() {
		try {
			/**
			 * Updated By: Viraj
			 * Date: 18-03-2019
			 * Description: To Hide popup from lead dashboard
			 */
			if(form.isPopUpAppMenubar()) {
//				if(GeneralViewDocumentPopup.viewDocumentPanel != null) {
//					GeneralViewDocumentPopup.viewDocumentPanel.hide();
//				}
				if(generalViewDocumentPopup.viewDocumentPanel!=null){
					generalViewDocumentPopup.viewDocumentPanel.hide();
				}
			}
			/** Ends **/
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quick Contract", Screen.QUICKCONTRACT);
			final QuickContractForm form = QuickContractPresentor.initalize();
			form.showWaitSymbol();
			Timer t = new Timer() {
				@Override
				public void run() {
					form.setToNewState();
					form.hideWaitSymbol();
					PersonInfo personInfo = new PersonInfo();
					personInfo.setCount(model.getCount());
					if (model.getStatus().equals(AppConstants.ACTIVE)) {
						form.getPersonInfoComposite().getId().getElement().addClassName("personactive");
						form.getPersonInfoComposite().getName().getElement().addClassName("personactive");
						form.getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
						form.getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
					}

					form.checkCustomerBranch(model.getCount());
					form.getPersonInfoComposite().setValue(model.getPersonInfo());
					form.getOlbbBranch().setValue(model.getBranch());
					form.getOlbeSalesPerson().setValue(model.getEmployee());
					form.getTbStatus().setValue(Contract.CREATED);
					form.getTbQuotatinId().setText("");
					form.getTbContractId().setText("");
					form.getPersonInfoComposite().setEnabled(false);
					form.getCustomerAddress(model.getCount());
					form.getCustomerAddressComposite().setEnable(false);
					if (model.getLeadProducts() != null)
						form.getSaleslineitemtable().setValue(model.getLeadProducts());
					form.getTbCompanyName().setValue("");
					form.getTbCompanyName().setEnabled(false);
					form.getTbFullName().setValue("");
					form.getTbFullName().setEnabled(false);
					form.getPnbCellNo().setValue(0l);
					form.getPnbCellNo().setEnabled(false);
					form.getEtbEmail().setValue("");
					form.getEtbEmail().setEnabled(false);
					form.getTbLeadId().setValue(model.getCount()+"");
					form.getTbLeadId().setEnabled(false);
					
					
					if(model.getNumberRange()!=null && !model.getNumberRange().equals("")){
				    	  form.getOlbcNumberRange().setValue(model.getNumberRange());
			    	  }
					form.getOlbbBranch().setValue(model.getBranch());
			    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
			    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
							form.olbcNumberRange.setValue(branchEntity.getNumberRange());
							Console.log("Number range set to drop down by default");
			    	  }else {
			    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
					  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
							if(range!=null&&!range.equals("")) {
								form.olbcNumberRange.setValue(range);
								Console.log("in else Number range set to drop down by default");							
							}				
			    	  }
					form.hideWaitSymbol();
				}
			};
			t.schedule(3000);
		} catch (Exception e) {
			Console.log("" + e);
		}
	}
	/** 26-09-2017 sagar sore reactToQuickOrder function to create Quick order from Lead **/
	private void reactToQuickOrder() {
		try {
			/**
			 * Updated By: Viraj
			 * Date: 18-03-2019
			 * Description: To Hide popup from lead dashboard
			 */
			if(form.isPopUpAppMenubar()) {
//				if(GeneralViewDocumentPopup.viewDocumentPanel != null) {
//					GeneralViewDocumentPopup.viewDocumentPanel.hide();
//				}
				if(generalViewDocumentPopup.viewDocumentPanel!=null){
					generalViewDocumentPopup.viewDocumentPanel.hide();
				}
			}
			/** Ends **/
			
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Quick Order", Screen.QUICKSALESORDER);
			final QuickSalesOrderForm form = QuickSalesOrderPresenter.initalize();
			form.showWaitSymbol();
			System.out.println("Starting timer");
			Timer t = new Timer() {
				@Override
				public void run() {
					System.out.println("Inside run");
					form.setToNewState();
					form.hideWaitSymbol();
					PersonInfo personInfo = new PersonInfo();
					personInfo.setCount(model.getCount());
					if (model.getStatus().equals(AppConstants.ACTIVE)) {
						form.getPersonInfoComposite().getId().getElement().addClassName("personactive");
						form.getPersonInfoComposite().getName().getElement().addClassName("personactive");
						form.getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
						form.getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
					}
					form.getPersonInfoComposite().setValue(model.getPersonInfo());
					form.getSalesorderlineitemtable().setValue(model.getLeadProducts());
					form.getOlbbBranch().setValue(model.getBranch());
					form.getOlbeSalesPerson().setValue(model.getEmployee());
					form.getTbQuotatinId().setText("");
					form.getTbContractId().setText("");
					form.getPersonInfoComposite().setEnabled(false);
					form.getTbCompanyName().setValue("");
					form.getTbCompanyName().setEnabled(false);
					form.getTbFullName().setValue("");
					form.getTbFullName().setEnabled(false);
					form.getOlbbBranch().setValue(model.getBranch());
			    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
			    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
							form.olbcNumberRange.setValue(branchEntity.getNumberRange());
							Console.log("Number range set to drop down by default");
			    	  }else {
			    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
					  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
							if(range!=null&&!range.equals("")) {
								form.olbcNumberRange.setValue(range);
								Console.log("in else Number range set to drop down by default");							
							}else {
								form.olbcNumberRange.setSelectedIndex(0);
								Console.log("could not set number range");
							}					
			    	  }
				}
			};
			t.schedule(3000);
		} catch (Exception e) {
			Console.log("" + e);
		}
	}

	public static LeadForm initalize()
	{		
				
				LeadForm form=new  LeadForm();
				LeadPresenterTable gentable=new LeadPresenterTableProxy();
				gentable.setView(form);
				gentable.applySelectionModle();
				LeadPresenterSearch.staticSuperTable=gentable;
				LeadPresenterSearch searchpopup=new LeadPresenterSearchProxy();
				form.setSearchpopupscreen(searchpopup);
				LeadPresenter  presenter=new LeadPresenter(form,new Lead());
//				form.setToNewState();
				Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
				Console.log("screenName =="+screenName);
				if(screenName.toString().trim().equals(AppConstants.SCREENTYPESALESLEAD))
				{
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Lead",Screen.SALESLEAD);

				}
				else{
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Lead",Screen.LEAD);
				}
				AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.Lead")
	public static  class LeadPresenterSearch extends SearchPopUpScreen<Lead>{

		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};

		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.Lead")
		public static class LeadPresenterTable extends SuperTable<Lead> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub

			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub

			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub

			}

			@Override
			public void setEnable(boolean state) {

			}

			@Override
			public void applyStyle() {

			}} ;

			
			
			
			
			
			private void reactToCreateQuotation()
			{
				boolean validateCopyingForData=false;
				Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
				String currentScrType=s.toString();
				/**
				 * Updated By: Viraj
				 * Date: 18-03-2019
				 * Description: To Hide popup from lead dashboard
				 */
				if(form.isPopUpAppMenubar()) {
//					if(GeneralViewDocumentPopup.viewDocumentPanel != null) {
//						GeneralViewDocumentPopup.viewDocumentPanel.hide();
//					}
					if(generalViewDocumentPopup.viewDocumentPanel!=null){
						generalViewDocumentPopup.viewDocumentPanel.hide();
					}
				}
				Console.log("current screen in lead: "+currentScrType);
				/** Ends **/
				if(AppConstants.SCREENTYPESALESLEAD.equals(currentScrType)){
					validateCopyingForData=CopyDataConfig.validateDataCopying(AppConstants.SALESQUOTATIONCOPYDATA);
				}

				if(AppConstants.SCREENTYPESERVICELEAD.equals(currentScrType)){
					validateCopyingForData=CopyDataConfig.validateDataCopying(AppConstants.QUOTATIONCOPYDATA);
				}
				
				boolean categoryDataVal=false;
				boolean typeDataVal=false;
				boolean groupDataVal=false;
				boolean priorityDataVal=false;
				Console.log("validateCopyingForData: "+ validateCopyingForData);
				
				if(validateCopyingForData==true)
				{
					if(AppConstants.SCREENTYPESALESLEAD.equals(currentScrType)){
						categoryDataVal=CopyDataConfig.validateData(AppConstants.COPYCATEGORYDATA, model.getCategory(), Screen.SALESQUOTATIONCATEGORY);
						typeDataVal=CopyDataConfig.validateData(AppConstants.COPYTYPEDATA, model.getType(), Screen.SALESQUOTATIONTYPE);
						groupDataVal=CopyDataConfig.validateData(AppConstants.COPYGROUPDATA, model.getGroup(), Screen.SALESQUOTATIONGROUP);
						priorityDataVal=CopyDataConfig.validateData(AppConstants.COPYPRIORITYDATA, model.getPriority(), Screen.SALESQUOTATIONPRIORITY);
					}
					
					if(AppConstants.SCREENTYPESERVICELEAD.equals(currentScrType)){
						categoryDataVal=CopyDataConfig.validateData(AppConstants.COPYCATEGORYDATA, model.getCategory(), Screen.QUOTATIONCATEGORY);
						typeDataVal=CopyDataConfig.validateData(AppConstants.COPYTYPEDATA, model.getType(), Screen.QUOTATIONTYPE);
						groupDataVal=CopyDataConfig.validateData(AppConstants.COPYGROUPDATA, model.getGroup(), Screen.QUOTATIONGROUP);
						priorityDataVal=CopyDataConfig.validateData(AppConstants.COPYPRIORITYDATA, model.getPriority(), Screen.QUOTATIONPRIORITY);
					}
					
					if(categoryDataVal==false){
						form.showDialogMessage("Quotation Category Value does not exists in Lead Category!");
					}
					
					if(categoryDataVal==true&&typeDataVal==false){
						form.showDialogMessage("Quotation Type value does not exists in Lead Type!");
					}
					
					if(groupDataVal==false&&typeDataVal==true&&categoryDataVal==true){
						form.showDialogMessage("Quotation Group value does not exists in Lead Group!");
					}
					
					if(priorityDataVal==false&&groupDataVal==true&&typeDataVal==true&&categoryDataVal==true){
						form.showDialogMessage("Quotation Priority value does not exists in Lead Priority!");
					}
					
					if(priorityDataVal==true&&groupDataVal==true&&typeDataVal==true&&categoryDataVal==true){
						switchToSalesQuotationScreen(validateCopyingForData);
					}
					
				}
				else{
					if(validateQuotationLeadProduct())
					{
						switchToSalesQuotationScreen(validateCopyingForData);
					}
				}
				
				
				//  rohan added this method 
				findMaxServiceSrNumberQuotation("Quotation");
				
			}
			
			private boolean validateQuotationLeadProduct()
			{
				Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
				String currentScrType=s.toString();
				Console.log("Screen Type in validateQuotationLeadProduct"+currentScrType);
				
				boolean flagProduct=true;
				
				if(model.getLeadProducts().size()!=0)
				{
					if(model.getLeadProducts().get(0).getPrduct() instanceof ItemProduct)
					{
						if(AppConstants.SCREENTYPESERVICELEAD.equals(currentScrType))
						{
							alert.addStyleName("attandancePopIndex");
							alert.alert("You cannot create Quotation using Item Products");
							flagProduct=false;
						}
					}
					if(model.getLeadProducts().get(0).getPrduct() instanceof ServiceProduct)
					{
						
						if(AppConstants.SCREENTYPESALESLEAD.equals(currentScrType))
						{
							alert.addStyleName("attandancePopIndex");
							alert.alert("You cannot create Quotation using Service Products");
							flagProduct=false;
						}
					}
				}
				Console.log("flagProduct: "+flagProduct);
				return flagProduct;
			}
			
			private boolean validateContractLeadProduct()
			{
				Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
				String currentScrType=s.toString();
				System.out.println("Screen Type"+currentScrType);
				
				boolean flagProduct=true;
				
				if(model.getLeadProducts().size()!=0)
				{
					if(model.getLeadProducts().get(0).getPrduct() instanceof ItemProduct)
					{
						if(AppConstants.SCREENTYPESERVICELEAD.equals(currentScrType))
						{
							alert.addStyleName("attandancePopIndex");
							alert.alert("You cannot create Contract using Item Products");
							flagProduct=false;
						}
					}
					if(model.getLeadProducts().get(0).getPrduct() instanceof ServiceProduct)
					{
						
						if(AppConstants.SCREENTYPESALESLEAD.equals(currentScrType))
						{
							alert.addStyleName("attandancePopIndex");
							alert.alert("You cannot create Sales Order using Service Products");
							flagProduct=false;
						}
					}
				}
				return flagProduct;
			}
			
			private void switchToSalesQuotationScreen(final boolean statusValue)
			{
				Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
				String currentScrType=s.toString();
				Console.log("Screen Type in switchToSalesQuotationScreen"+currentScrType);
				
				
				
				if(AppConstants.SCREENTYPESALESLEAD.equals(currentScrType)){
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Quotation",Screen.SALESQUOTATION);
					final SalesQuotationForm form=SalesQuotationPresenter.initalize();
					
					form.showWaitSymbol();
					final SalesQuotation squot=new SalesQuotation();
					squot.setBranch(model.getBranch());
					squot.setEmployee(model.getEmployee());
					squot.setCinfo(model.getPersonInfo());
					squot.setStatus(SalesQuotation.CREATED);
					squot.setLeadCount(model.getCount());
					
					Timer t = new Timer() {
					      @Override
					      public void run() {
					    	  form.setToNewState();
					    	  form.hideWaitSymbol();
					    	  form.updateView(squot);
					    	  
					    	  genasync.getSearchResult(queryForAddress(model.getPersonInfo().getCount()), new AsyncCallback<ArrayList<SuperModel>>() {
									
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected error occurred!");
									}
						
									@Override
									public void onSuccess(ArrayList<SuperModel> result) {
										 Console.log("Size Of Result In SalesQuotation Presenter"+result.size());
										 List<SalesLineItem> leadProductsLis=LeadPresenter.this.model.getLeadProducts();
										 Console.log("Size For Lis"+leadProductsLis.size());
										for(SuperModel model:result)
										{
												Customer custentity = (Customer)model;
												addComposite.setAddrLine1(custentity.getSecondaryAdress().getAddrLine1());
												addComposite.setLocality(custentity.getSecondaryAdress().getLocality());
												addComposite.setPin(custentity.getSecondaryAdress().getPin());
												addComposite.setCity(custentity.getSecondaryAdress().getCity());
												addComposite.setState(custentity.getSecondaryAdress().getState());
												addComposite.setCountry(custentity.getSecondaryAdress().getCountry());
												if(addComposite.getAddrLine2()!=null){
													addComposite.setAddrLine2(custentity.getSecondaryAdress().getAddrLine2());
												}
												if(custentity.getSecondaryAdress().getLandmark()!=null){
													addComposite.setLandmark(custentity.getSecondaryAdress().getLandmark());
												}
												 if(!retrCompnyState.trim().equals(custentity.getSecondaryAdress().getState().trim())){
										    		  form.getCbcformlis().setEnabled(true);
										    	  }
												 /**
													 * Date 27/10/2017 added by komal to set
													 * lead quantity into quotation quantity
													 **/
													for (SalesLineItem saleObj : leadProductsLis) {
														saleObj.setQuantity(saleObj
																.getQty());
													}
												
												 if(leadProductsLis.size()!=0){
													 form.getLineitemsalesquotationtable().setValue(leadProductsLis);
												 }
												 
												 form.getAcshippingcomposite().setValue(addComposite);
												 
												 form.cust = custentity;
										}
										}
									 });
					    	  
					    	 // final Address addCom=retrieveShippingAddress(model.getPersonInfo().getCount());
					    	  Console.log("RetrState"+retrCompnyState);
					    	  
					    	  
					    	  if(statusValue==true)
					    	  {
					    		  form.getOlbQuotationCategory().setValue(model.getCategory());
					    		  form.getOlbQuotationGroup().setValue(model.getGroup());
					    		  form.getOlbQuotationType().setValue(model.getType());
					    		  form.getOlbcPriority().setValue(model.getPriority());
					    	  }
					    	  
					    	  //System.out.println("Shipping Addr"+addCom.getState().trim());
//					    	  if(!retrCompnyState.trim().equals(addCom.getState().trim())){
//					    		  form.getCbcformlis().setEnabled(true);
//					    	  }
					    	  form.getTbQuotatinId().setText("");
					    	  form.getTbContractId().setText("");
					    	  form.getDbdeliverydate().setValue(null);
//					    	  form.getAcshippingcomposite().setValue(addCom);
					    	  form.getAcshippingcomposite().setEnable(false);
					    	  form.getPersonInfoComposite().setEnabled(false);
					    	  if(LeadPresenter.this.form.isSalesPersonRestrictionFlag())
					    	  {
					    		  form.getOlbeSalesPerson().setEnabled(false);
					    	  }
					    	  else
					    	  {
					    		  form.getOlbeSalesPerson().setEnabled(true);
					    	  }
					    	  
					    	  /**
					    	   * Date 09-06-2018
					    	   * By Vijay for round off amt
					    	   */
					    	  if(model.getRoundOffAmt()!=0){
					    		  form.getTbroundoffAmt().setValue(model.getRoundOffAmt()+"");
					    	  }
					    	  /**
					    	   * ends here
					    	   */
								AppUtility.makeCustomerBranchLive(form.olbcustBranch,squot.getCinfo().getCount());

					    	  Console.log("end of sales lead in switchToSalesQuotationScreen");
					      }
					    };
					    t.schedule(3000);
				}
				
				if(AppConstants.SCREENTYPESERVICELEAD.equals(currentScrType)){
					final String branch =form.olbbBranch.getValue(form.olbbBranch.getSelectedIndex());
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quotation",Screen.QUOTATION);
					final QuotationForm form=QuotationPresenter.initalize();
					form.showWaitSymbol();
					final Quotation quot=new Quotation();
					quot.setBranch(model.getBranch());
					quot.setEmployee(model.getEmployee());
					quot.setCinfo(model.getPersonInfo());
					quot.setStatus(Quotation.CREATED);
					quot.setLeadCount(model.getCount());
					/***Date 12-9-2019 by Amol map type ,grop and category**/
					quot.setGroup(model.getGroup());
					quot.setCategory(model.getCategory());
					quot.setType(model.getType());
					
					
					
					
					
					/**
					 * Date : 24-08-2017 BY ANIL
					 * For loading customer branches if any
					 */
					form.checkCustomerBranch(model.getPersonInfo().getCount());
					/**
					 * End
					 */
					
					Timer t = new Timer() {
					      @Override
					      public void run() {
					    	  form.setToNewState();
					    	  form.hideWaitSymbol();
					    	  form.updateView(quot);
//					    	  form.checkCustomerBranchFromLead(form.getPersonInfoComposite().getIdValue());
					    	  List<SalesLineItem> lineItemLis=model.getLeadProducts();
					    	  if(statusValue==true)
					    	  {
					    		  form.getOlbQuotationCategory().setValue(model.getCategory());
					    		  form.getOlbQuotationGroup().setValue(model.getGroup());
					    		  form.getOlbQuotationType().setValue(model.getType());
					    		  form.getOlbcPriority().setValue(model.getPriority());
					    	  }
					    	  QuotationPresenter.custcount=form.getPersonInfoComposite().getIdValue();

								/**
								 * Date 27/10/2017 added by komal to set lead quantity into
								 * area
								 **/
								for(SalesLineItem saleObj : lineItemLis) {
									if(saleObj.getQty()!=0){
										saleObj.setArea(saleObj.getQty() + "");
									}else{
										saleObj.setArea("NA");
									}
									saleObj.setTotalAmount(saleObj.getTotalAmount());
									
									/**
									 * @author Anil
									 * @since 12-06-2020
									 * Storing customer branch details in hashmap
									 */
									ArrayList<BranchWiseScheduling> branchlist = AppUtility.getCustomerBranchSchedulinglistInfo(form.customerbranchlist,model.getBranch(),"",0);
									Integer prodSrNo = saleObj.getProductSrNo();
									HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
									customerBranchlist.put(prodSrNo, branchlist);
									saleObj.setCustomerBranchSchedulingInfo(customerBranchlist);
								}
					    	  
					    	// vijay added for set quantity 1 becs we are not getting quantity in lead
					    	  for(int i=0;i<lineItemLis.size();i++){
					    		  lineItemLis.get(i).setQuantity(1.0);
					    	  }
					    	  
					    	  if(lineItemLis.size()!=0){
					    		  form.getSaleslineitemquotationtable().setValue(lineItemLis);
					    	  }
					    	 
					    	  if(form.getSaleslineitemquotationtable().getDataprovider().getList().size() == 0){
									
									 List<ServiceSchedule> popuplist=reactfordefaultTable(lineItemLis,branch,form.customerbranchlist);
									 form.getServiceScheduleTable().getDataprovider().setList(popuplist);
									 
							  }else{
									 List<ServiceSchedule> privlist=form.getServiceScheduleTable().getDataprovider().getList();
											 
									 List<ServiceSchedule> popuplist=reactfordefaultTable(lineItemLis,branch,form.customerbranchlist);
									 List<ServiceSchedule> popuplist1=new ArrayList<ServiceSchedule>();
									 
									 popuplist1.addAll(privlist);
									 popuplist1.addAll(popuplist);
									 form.getServiceScheduleTable().getDataprovider().setList(popuplist1);
									 
							  }
					    	  
					    	  
					    	  // vijay
					    	  form.getProdTaxTable().setValue(model.getProductTaxes());
					    	  
					    	  form.getTbQuotatinId().setText("");
					    	  form.getTbContractId().setText("");
					    	  form.getPersonInfoComposite().setEnabled(false);
					    	  if(LeadPresenter.this.form.isSalesPersonRestrictionFlag())
					    	  {
					    		  form.getOlbeSalesPerson().setEnabled(false);
					    	  }
					    	  else
					    	  {
					    		  form.getOlbeSalesPerson().setEnabled(true);
					    	  }
					    	  /** Date 05-09-2017 added by vijay for as per new code discount and round off ****/
					    	  form.getDbDiscountAmt().setValue(model.getDiscountAmt());
					    	  form.getTbroundOffAmt().setValue(model.getRoundOffAmt()+"");
					    	  
					    	  if(model.getNumberRange()!=null && !model.getNumberRange().equals("")){
						    	  form.getOlbcNumberRange().setValue(model.getNumberRange());
					    	  }
					    	  
					    	  form.getOlbbBranch().setValue(model.getBranch());
					    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
					    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
									form.olbcNumberRange.setValue(branchEntity.getNumberRange());
									Console.log("Number range set to drop down by default");
					    	  }else {
					    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
							  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
									if(range!=null&&!range.equals("")) {
										form.olbcNumberRange.setValue(range);
										Console.log("in else Number range set to drop down by default");							
									}					
					    	  }
					    	  form.hideWaitSymbol();
					      }
					    };
					    t.schedule(5000);

				}
			}
			
			
			/**
			 *  This method is used for setting data to schedule popup in quotation  and contract
			 * Date : 31st Aug 2016
			 * Developed by : Vijay Chougule
			 * 
			 */
			private List<ServiceSchedule> reactfordefaultTable(List<SalesLineItem> lineItemLis, String branch, ArrayList<CustomerBranchDetails> customerbranchlist) {

				
				/**
				 * Date 02-05-2018 
				 * Developer : Vijay
				 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
				 * so i have updated below code using hashmap. 
				 */
				/**
				 * Date : 24-08-2017 BY ANIL
				 * setting customers branch information at product level if any
				 */
				for(SalesLineItem lis:lineItemLis){
					
//					String branchArray[]=AppUtility.getCustomersBranchsForSchedulingInfo(customerbranchlist,branch);
//					if(branchArray[0]!=null){
//						lis.setBranchSchedulingInfo(branchArray[0]);
//					}
//					if(branchArray[1]!=null){
//						lis.setBranchSchedulingInfo1(branchArray[1]);
//					}
//					if(branchArray[2]!=null){
//						lis.setBranchSchedulingInfo2(branchArray[2]);
//					}
//					if(branchArray[3]!=null){
//						lis.setBranchSchedulingInfo3(branchArray[3]);
//					}
//					if(branchArray[4]!=null){
//						lis.setBranchSchedulingInfo4(branchArray[4]);
//					}
//					if(branchArray[5]!=null){
//						lis.setBranchSchedulingInfo5(branchArray[5]);
//					}
//					if(branchArray[6]!=null){
//						lis.setBranchSchedulingInfo6(branchArray[6]);
//					}
//					if(branchArray[7]!=null){
//						lis.setBranchSchedulingInfo7(branchArray[7]);
//					}
//					if(branchArray[8]!=null){
//						lis.setBranchSchedulingInfo8(branchArray[8]);
//					}
//					if(branchArray[9]!=null){
//						lis.setBranchSchedulingInfo9(branchArray[9]);
//					}

					/**
					 * Date 02-05-2018 By vijay updated code using hashmap for customer branches scheduling services
					 * above old code commented
					 */
					ArrayList<BranchWiseScheduling> branchlist = AppUtility.getCustomerBranchSchedulinglistInfo(customerbranchlist,branch,form.cust.getUnitOfMeasurement(),form.cust.getArea());
					Integer prodSrNo = lis.getProductSrNo();
					HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
					customerBranchlist.put(prodSrNo, branchlist);
					lis.setCustomerBranchSchedulingInfo(customerBranchlist);
					
					/**
					 * ends here	
					 */
				
				}

				/**
				 * End
				 */
				
				
				
				List<SalesLineItem> salesitemlis=new ArrayList<SalesLineItem>();
				salesitemlis.addAll(lineItemLis);
				ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
				serschelist.clear();
				
				for(int i=0;i<salesitemlis.size();i++){
				
					int qty=0;
					qty=(int) salesitemlis.get(i).getQty();
					for(int k=0;k < qty;k++){
				
					long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
					int noOfdays=salesitemlis.get(i).getDuration();
					int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
					Date servicedate=new Date();
					Date d=new Date(servicedate.getTime());
					Date productEndDate= new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate=new Date(productEndDate.getTime());
					productEndDate=prodenddate;
					Date tempdate=new Date();
					for(int j=0;j<noServices;j++){
			
						if(j>0){
							CalendarUtil.addDaysToDate(d, interval);
							tempdate=new Date(d.getTime());
						}
						
						ServiceSchedule ssch=new ServiceSchedule();
						
					//  rohan added this 1 field 
						System.out.println("In side method RRRRRRRRRRRRR"+salesitemlis.get(i).getProductSrNo());
						ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						
						Date Stardaate=new Date();
						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
						ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
						ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(j+1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(productEndDate);
						
						ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
						ssch.setServicingBranch(branch);
						System.out.println("SERVICING TIME ::: "+salesitemlis.get(i).getServicingTIme());
						System.out.println("BRANCH ::: "+branch);
						
						String str="Flexible";
						ssch.setScheduleServiceTime(str);
						if(j==0){
							ssch.setScheduleServiceDate(servicedate);
							}
						if(j!=0){
							if(productEndDate.before(d)==false){
								ssch.setScheduleServiceDate(tempdate);
							}else{
								ssch.setScheduleServiceDate(productEndDate);
							}
						}
						
						if(customerbranchlist.size()==qty){
							ssch.setScheduleProBranch(customerbranchlist.get(k).getBusinessUnitName());
						}else if(customerbranchlist.size()==0){
							ssch.setScheduleProBranch("Service Address");
						}else{
							ssch.setScheduleProBranch("Service Address");
						}
						
						ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						
						serschelist.add(ssch);
					}
				}
				}
				return serschelist;
			
			}
			
			
			private void reactToCreateContract()
			{
				boolean validateCopyingForData=false;
				Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
				String currentScrType=s.toString();
				
				/**
				 * Updated By: Viraj
				 * Date: 18-03-2019
				 * Description: To Hide popup from lead dashboard
				 */
				if(form.isPopUpAppMenubar()) {
//					if(GeneralViewDocumentPopup.viewDocumentPanel != null) {
//						GeneralViewDocumentPopup.viewDocumentPanel.hide();
//					}
					if(generalViewDocumentPopup.viewDocumentPanel!=null){
						generalViewDocumentPopup.viewDocumentPanel.hide();
					}
				}
				/** Ends **/
				
				if(AppConstants.SCREENTYPESALESLEAD.equals(currentScrType)){
					validateCopyingForData=CopyDataConfig.validateDataCopying(AppConstants.SALESORDERCOPYDATA);
				}

				if(AppConstants.SCREENTYPESERVICELEAD.equals(currentScrType)){
					validateCopyingForData=CopyDataConfig.validateDataCopying(AppConstants.CONTRACTCOPYDATA);
				}
				
				
				boolean categoryDataVal=false;
				boolean typeDataVal=false;
				boolean groupDataVal=false;
				
				if(validateCopyingForData==true)
				{
					
					if(AppConstants.SCREENTYPESALESLEAD.equals(currentScrType)){
						categoryDataVal=CopyDataConfig.validateData(AppConstants.COPYCATEGORYDATA, model.getCategory(), Screen.SALESORDERCATEGORY);
						typeDataVal=CopyDataConfig.validateData(AppConstants.COPYTYPEDATA, model.getType(), Screen.SALESORDERTYPE);
						groupDataVal=CopyDataConfig.validateData(AppConstants.COPYGROUPDATA, model.getGroup(), Screen.SALESORDERGROUP);
					}
					
					if(AppConstants.SCREENTYPESERVICELEAD.equals(currentScrType)){
						categoryDataVal=CopyDataConfig.validateData(AppConstants.COPYCATEGORYDATA, model.getCategory(), Screen.CONTRACTCATEGORY);
						typeDataVal=CopyDataConfig.validateData(AppConstants.COPYTYPEDATA, model.getType(), Screen.CONTRACTTYPE);
						groupDataVal=CopyDataConfig.validateData(AppConstants.COPYGROUPDATA, model.getGroup(), Screen.CONTRACTGROUP);
					}
					
					
					
					if(categoryDataVal==false){
						form.showDialogMessage("Quotation Category Value does not exists in Lead Category!");
					}
					
					if(categoryDataVal==true&&typeDataVal==false){
						form.showDialogMessage("Quotation Type value does not exists in Lead Type!");
					}
					
					if(groupDataVal==false&&typeDataVal==true&&categoryDataVal==true){
						form.showDialogMessage("Quotation Group value does not exists in Lead Group!");
					}
					
					if(groupDataVal==true&&typeDataVal==true&&categoryDataVal==true){
						switchToOrderScreen(validateCopyingForData);
					}
					
				}
				else{
					if(validateContractLeadProduct())
					{
						switchToOrderScreen(validateCopyingForData);
					}
				}
				
				findMaxServiceSrNumberQuotation("Contract");
			}
			
			
			private void switchToOrderScreen(final boolean statusValue)
			{
				Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
				String currentScrType=s.toString();
				System.out.println("Screen Type"+currentScrType);
				
				if(AppConstants.SCREENTYPESALESLEAD.equals(currentScrType)){
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Sales Order",Screen.SALESORDER);
					final SalesOrderForm form=SalesOrderPresenter.initalize();
					final SalesOrder salesOrderEnt=new SalesOrder();
					salesOrderEnt.setBranch(model.getBranch());
					salesOrderEnt.setEmployee(model.getEmployee());
					salesOrderEnt.setCinfo(model.getPersonInfo());
					salesOrderEnt.setStatus(SalesOrder.CREATED);
					salesOrderEnt.setLeadCount(model.getCount());
					
					form.showWaitSymbol();
					Timer t = new Timer() {
					      @Override
					      public void run() {
					    	  form.setToNewState();
					    	  form.hideWaitSymbol();
					    	  form.updateView(salesOrderEnt);
					    	  
					    	  genasync.getSearchResult(queryForAddress(model.getPersonInfo().getCount()), new AsyncCallback<ArrayList<SuperModel>>() {
									
									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected error occurred!");
									}
						
									@Override
									public void onSuccess(ArrayList<SuperModel> result) {
										System.out.println("Size Of Result In SalesQuotation Presenter"+result.size());
										List<SalesLineItem> lineItemLis=model.getLeadProducts();
										for(SuperModel model:result)
										{
												Customer custentity = (Customer)model;
												addComposite.setAddrLine1(custentity.getSecondaryAdress().getAddrLine1());
												addComposite.setLocality(custentity.getSecondaryAdress().getLocality());
												addComposite.setPin(custentity.getSecondaryAdress().getPin());
												addComposite.setCity(custentity.getSecondaryAdress().getCity());
												addComposite.setState(custentity.getSecondaryAdress().getState());
												addComposite.setCountry(custentity.getSecondaryAdress().getCountry());
												if(addComposite.getAddrLine2()!=null){
													addComposite.setAddrLine2(custentity.getSecondaryAdress().getAddrLine2());
												}
												if(custentity.getSecondaryAdress().getLandmark()!=null){
													addComposite.setLandmark(custentity.getSecondaryAdress().getLandmark());
												}
												 if(!retrCompnyState.trim().equals(custentity.getSecondaryAdress().getState().trim())){
										    		  form.getCbcformlis().setEnabled(true);
										    	  }
												 /**
													 * Date 27/10/2017 added by komal to set lead quantity into quantity
													 **/
													for (SalesLineItem saleObj : lineItemLis) {
														saleObj.setQuantity(saleObj.getQty());
													}
												 
												 form.getAcshippingcomposite().setValue(addComposite);
												 
												 if(!retrCompnyState.trim().equals(custentity.getSecondaryAdress().getState().trim())){
										    		  form.getCbcformlis().setEnabled(true);
										    	  }
												 form.getSalesorderlineitemtable().setValue(lineItemLis);
												 
												 form.getAcshippingcomposite().setValue(addComposite);
												
										}
										}
									 });
					    	  
					    	  
//					    	  if(!retrCompnyState.trim().equals(addCom.getState().trim())){
//					    		  form.getCbcformlis().setEnabled(true);
//					    	  }
					    	  
					    	  if(statusValue==true)
					    	  {
					    		  form.getOlbSalesOrderCategory().setValue(model.getCategory());
					    		  form.getOlbSalesOrderGroup().setValue(model.getGroup());
					    		  form.getOlbSalesOrderType().setValue(model.getType());
					    	  }
					    	  
					    	  
					    	  form.getTbQuotatinId().setText("");
					    	  form.getTbContractId().setText("");
					    	  form.getDbdeliverydate().setValue(null);
					    	  form.getTbReferenceNumber().setText("");
					    	  form.getPersonInfoComposite().setEnabled(false);
					    	  
					    	  form.getAcshippingcomposite().setEnable(false);
					    	  
					    	  form.getOlbbBranch().setValue(model.getBranch());
					    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
					    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
									form.olbcNumberRange.setValue(branchEntity.getNumberRange());
									Console.log("Number range set to drop down by default");
					    	  }else {
					    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
							  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
									if(range!=null&&!range.equals("")) {
										form.olbcNumberRange.setValue(range);
										Console.log("in else Number range set to drop down by default");							
									}else {
										form.olbcNumberRange.setSelectedIndex(0);
										Console.log("could not set number range");
									}					
					    	  }
					    	  
					        
					      }
					    };
					    t.schedule(5000);
				}
				
				if(AppConstants.SCREENTYPESERVICELEAD.equals(currentScrType)){
					final String branch =form.olbbBranch.getValue(form.olbbBranch.getSelectedIndex());
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract",Screen.CONTRACT);
					final ContractForm form=ContractPresenter.initalize();
					final Contract contrct=new Contract();
					contrct.setBranch(model.getBranch());
					contrct.setEmployee(model.getEmployee());
					
					contrct.setCinfo(model.getPersonInfo());
					contrct.setStatus(Contract.CREATED);
					contrct.setLeadCount(model.getCount());
					/**Date 12-9-2019 by Amol map the type,group,category raised by Rohan Sir**/
					contrct.setType(model.getType());
					contrct.setCategory(model.getCategory());
					contrct.setGroup(model.getGroup());
					
					
					
					
					/**
					 * Date : 24-08-2017 BY ANIL
					 * For loading customer branches if any
					 */
					form.checkCustomerBranch(model.getPersonInfo().getCount());
					/**
					 * End
					 */
					
					form.showWaitSymbol();
					Timer t = new Timer() {
					      @Override
					      public void run() {
					    	  form.setToNewState();
					    	  form.hideWaitSymbol();
					    	  form.updateView(contrct);
//					    	  form.checkCustomerBranch(form.getPersonInfoComposite().getIdValue());
					    	  List<SalesLineItem> lineItemLis=model.getLeadProducts();
					    	  if(statusValue==true)
					    	  {
					    		  form.getOlbContractCategory().setValue(model.getCategory());
					    		  form.getOlbContractGroup().setValue(model.getGroup());
					    		  form.getOlbContractType().setValue(model.getType());
					    	  }
					    	  
					    	  // rohan added this for loading customer branches Date : 3/2/2017 
					    	  ContractPresenter.custcount=form.getPersonInfoComposite().getIdValue();
					    	  // ends here
					    	  
					    	  	/**
								 * Date : 18-07-2017 by Anil
								 * added this code for mapping customer category to segment field for NBHC CCPM
								 */
								form.checkCustomerStatus(model.getPersonInfo().getCount(),true,true);
								/**
								 * End
								 */
					    	  if(lineItemLis.size()>0){
					    		  form.getSaleslineitemtable().setValue(lineItemLis);
					    	  }
					    	  
					    	  /**
								 * Date 27/10/2017 added by komal to set lead quantity into
								 * area
								 **/
								for(SalesLineItem saleObj : lineItemLis) {
									if(saleObj.getQty()!=0){
										saleObj.setArea(saleObj.getQty() + "");	
									}else{
										saleObj.setArea("NA");	
									}
									saleObj.setArea(saleObj.getQty() + "");
									saleObj.setTotalAmount(saleObj.getTotalAmount());
									/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
									if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableServiceBranch")){
										ArrayList<BranchWiseScheduling> branchArrayList = new ArrayList<BranchWiseScheduling>();
										for(Branch branch : LoginPresenter.customerScreenBranchList){
											BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
											branchWiseScheduling.setBranchName(branch.getBusinessUnitName());
											branchWiseScheduling.setArea(0);
											branchWiseScheduling.setCheck(false);
											branchArrayList.add(branchWiseScheduling);
										}
										HashMap<Integer, ArrayList<BranchWiseScheduling>> serviceBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
										serviceBranchlist.put(saleObj.getProductSrNo(), branchArrayList);
										saleObj.setServiceBranchesInfo(serviceBranchlist);
									}
									/** end komal **/
								
									/*** Date 17-05-2019 by Vijay bug branches added to if not added then contract approved then 
									 * services created but contract in created state so again approve then services created twice
									 */
									
									ArrayList<BranchWiseScheduling> branchlist = AppUtility.getCustomerBranchSchedulinglistInfo(form.customerbranchlist,model.getBranch(),saleObj.getUnitOfMeasurement(),0);
									Integer prodSrNo = saleObj.getProductSrNo();
									HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
									customerBranchlist.put(prodSrNo, branchlist);
									saleObj.setCustomerBranchSchedulingInfo(customerBranchlist);
									
									/**
									 * ends here
									 */
								}
//					    	// vijay added for set quantity 1 becs we are not getting quantity in lead
					    	  for(int i=0;i<lineItemLis.size();i++){
					    		  lineItemLis.get(i).setQuantity(1.0);
					    	  }
					    	  
					    	  // vijay
					    	  form.getProdTaxTable().setValue(model.getProductTaxes());
					    	  if(lineItemLis.size()>0){
					    		  form.getSaleslineitemtable().setValue(lineItemLis);
					    	  }
					    	  ContractPresenter.custcount=form.getPersonInfoComposite().getIdValue();
					    	 
					    	  if(form.getSaleslineitemtable().getDataprovider().getList().size() == 0){
									
									 List<ServiceSchedule> popuplist=reactfordefaultTable(lineItemLis,branch,form.customerbranchlist);
									 form.getServiceScheduleTable().getDataprovider().setList(popuplist);
									 
							  }else{
									 List<ServiceSchedule> privlist=form.getServiceScheduleTable().getDataprovider().getList();
											 
									 List<ServiceSchedule> popuplist=reactfordefaultTable(lineItemLis,branch,form.customerbranchlist);
									 List<ServiceSchedule> popuplist1=new ArrayList<ServiceSchedule>();
									 
									 popuplist1.addAll(privlist);
									 popuplist1.addAll(popuplist);
									 form.getServiceScheduleTable().getDataprovider().setList(popuplist1);
									 
							  }
					    	  
					    	  form.getTbQuotatinId().setText("");
					    	  form.getTbContractId().setText("");
					    	  form.getTbReferenceNumber().setText("");
					    	  form.getPersonInfoComposite().setEnabled(false);
					    	  form.getChkbillingatBranch().setValue(false);
					    	  form.getChkservicewithBilling().setValue(false);
					    	  if(LeadPresenter.this.form.isSalesPersonRestrictionFlag())
					    	  {
					    		  form.getOlbeSalesPerson().setEnabled(false);
					    	  }
					    	  else
					    	  {
					    		  form.getOlbeSalesPerson().setEnabled(true);
					    	  }
					    	  
					    	  
					    	  /** Date 05-09-2017 added by vijay for as per new code discount and round off ****/
					    	  form.getDbDiscountAmt().setValue(model.getDiscountAmt());
					    	  form.getTbroundOffAmt().setValue(model.getRoundOffAmt()+"");
					    	  
					    	  /**
					    	   * Date : 30-10-2017 BY ANIL
					    	   */
					    	  form.getCbServiceWiseBilling().setValue(false);
					    	  /**
					    	   * End
					    	   */
					    	  /**
					    	   * Date : 24-02-2018
					    	   * map  contact list
					    	   * 
					    	   */
					    	  form.getCustomerContactList(model.getCount(),null);
					    	  /**
					    	   * end
					    	   */
					    	  
					    	  /**
						  		 * Date 28-09-2018 By Vijay 
						  		 * Des:- NBHC CCPM for every contract this must be true by default it does not allow to change for user
						  		 */
						  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
						  			form.cbStartOfPeriod.setValue(true);
						  			form.cbStartOfPeriod.setEnabled(false);
						  		}
						  		/**
						  		 * ends here
						  		 */
						  		/**
						  		 * @author Vijay Date :- 28-01-2021
						  		 * Des :- if Free Material process config is active then stationed Service flag will true 
						  		 * or else it will false. if this flag is true then service will create with Automatic Scheduling 
						  		 */
						  		form.getChkStationedService().setValue(false);
//						  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.PC_FREEMATERIAL)){
//							  		form.getChkStationedService().setValue(true);
//								}
//						  		else{
//							  		form.getChkStationedService().setValue(false);
//						  		}
						  		
						  		if(model.getNumberRange()!=null && !model.getNumberRange().equals("")){
							    	  form.getOlbcNumberRange().setValue(model.getNumberRange());
						    	  }
						  		form.getOlbbBranch().setValue(model.getBranch());
						    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
						    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
										form.olbcNumberRange.setValue(branchEntity.getNumberRange());
										Console.log("Number range set to drop down by default");
						    	  }else {
						    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
								  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
										if(range!=null&&!range.equals("")) {
											form.olbcNumberRange.setValue(range);
											Console.log("in else Number range set to drop down by default");							
										}					
						    	  }
						  		form.hideWaitSymbol();
					      }
					    };
					    t.schedule(5000);
				}
				
			}
			
			private void reactToViewServices()
			{
			}
			
			private void reactToNew()
			{
				/**
				 * Updated By: Viraj
				 * Date: 18-03-2019
				 * Description: To Hide popup from lead dashboard
				 */
				if(form.isPopUpAppMenubar()) {
//					if(GeneralViewDocumentPopup.viewDocumentPanel != null) {
//						GeneralViewDocumentPopup.viewDocumentPanel.hide();
//					}
					if(generalViewDocumentPopup.viewDocumentPanel!=null){
						generalViewDocumentPopup.viewDocumentPanel.hide();
					}
				}
				/** Ends **/
				
				form.setToNewState();
				this.initalize();
				form.toggleAppHeaderBarMenu();
				
			}
			
			
			/**
			 * Method returns the shipping address of customer.
			 * The address is required in sales quotation and sales order.
			 */
			
			public Address retrieveShippingAddress(final int custId)
			{
				MyQuerry querry=new MyQuerry();
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter tempfilter=new Filter();
				tempfilter=new Filter();
				tempfilter.setQuerryString("count");
				System.out.println("Cust ID"+custId);
				tempfilter.setIntValue(custId);
				filtervec.add(tempfilter);
				tempfilter=new Filter();
				tempfilter.setQuerryString("companyId");
				tempfilter.setLongValue(model.getCompanyId());
				filtervec.add(tempfilter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Customer());
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected error occurred!");
						}
			
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							for(SuperModel model:result)
							{
									Customer custentity = (Customer)model;
									addComposite.setAddrLine1(custentity.getSecondaryAdress().getAddrLine1());
									addComposite.setLocality(custentity.getSecondaryAdress().getLocality());
									addComposite.setPin(custentity.getSecondaryAdress().getPin());
									addComposite.setCity(custentity.getSecondaryAdress().getCity());
									addComposite.setState(custentity.getSecondaryAdress().getState());
									addComposite.setCountry(custentity.getSecondaryAdress().getCountry());
									if(addComposite.getAddrLine2()!=null){
										addComposite.setAddrLine2(custentity.getSecondaryAdress().getAddrLine2());
									}
									if(custentity.getSecondaryAdress().getLandmark()!=null){
										addComposite.setLandmark(custentity.getSecondaryAdress().getLandmark());
									}
							}
							}
						 });
				return addComposite;				
			}
			
			private MyQuerry queryForAddress(int custId)
			{
				MyQuerry querry=new MyQuerry();
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter tempfilter=new Filter();
				tempfilter=new Filter();
				tempfilter.setQuerryString("count");
				tempfilter.setIntValue(custId);
				filtervec.add(tempfilter);
				tempfilter=new Filter();
				tempfilter.setQuerryString("companyId");
				tempfilter.setLongValue(model.getCompanyId());
				filtervec.add(tempfilter);
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Customer());
				
				return querry;
			}
			
			
			/**
			 *   This method returns the name of state in which the company is located.
			 *   It is retrieved form Company address in Company Entity
			 * @return  It returns company state
			 */
			
			public String retrieveCompanyState()
			{
				final MyQuerry querry=new MyQuerry();
				Filter tempfilter=new Filter();
				tempfilter.setQuerryString("companyId");
				tempfilter.setLongValue(model.getCompanyId());
				querry.getFilters().add(tempfilter);
				querry.setQuerryObject(new Company());
				Timer timer=new Timer() 
			    {
					@Override
					public void run() 
					{
					genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected error occurred!");
						}
			
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
								for(SuperModel model:result)
								{
									Company compentity = (Company)model;
									retrCompnyState=compentity.getAddress().getState();
									System.out.println("Retrieved "+retrCompnyState);
								}
							}
						 });
					}
		    	 };
		    	 timer.schedule(3000);
		    	 
		    	return retrCompnyState;
			}

			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				if(event.getSource().equals(form.getPic().getId())||event.getSource().equals(form.getPic().getName())||event.getSource().equals(form.getPic().getPhone())){
					form.checkCustomerStatus(form.getPic().getIdValue(), null);
					/**
					 * date 26/10/2017 added by komal for lead Project coordinator
					 * dropdowns
					 **/
					if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","LeadProjectCoordinator")) {
						form.getOlbArchitect().removeAllItems();
						form.getOlbPMC().removeAllItems();
						form.getOlbContractor().removeAllItems();
						AppUtility.makeContactPersonListBoxLive(form.getOlbArchitect(),"Architect", form.getPic().getIdValue());
						AppUtility.makeContactPersonListBoxLive(form.getOlbPMC(),"PMC", form.getPic().getIdValue());
						AppUtility.makeContactPersonListBoxLive(form.getOlbContractor(), "Contractor", form.getPic().getIdValue());
					}
				}
			}

			@Override
			public void onClick(ClickEvent event) {
				super.onClick(event);
				
				if (event.getSource().equals(form.getAddProducts())) {
					if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","ChangeProductComposite")) {
						reactOnAddServiceProduct();
						Console.log("Inside OnClick");
					} else {
						reactOnAddProducts();
					}
				}
				
				if(event.getSource().equals(form.getNewCustomer())){
					
					/*
					 * commented by ashwini
					 */
//					System.out.println("INSIDE POPUP CUSTOMER!!!");
//					panel=new PopupPanel(true);
//					panel.add(custpoppup);
//					panel.center();
//					custpoppup.clear();
//					panel.show();
					
					/*
					 * @author Ashwini
					 * Date:10-01-2018
					 * Des:to add branch and sales person in popup if selected on lead screen.
					 */
					custpoppup.getLblCancel().setText("Close");
					custpoppup.showPopUp();
					
					
					if(form.getOlbbBranch().getValue()!=null ||form.getOlbeSalesPerson().getValue()!=null ){
					custpoppup.getOlbbBranch().setValue(form.getOlbbBranch().getValue());
					
					custpoppup.getOlbsalesPerson().setValue(form.getOlbeSalesPerson().getValue());
					
					}
					
					/*
					 * @author Ashwini
					 * Date:10-01-2018
					 * Des:to add branch and sales person in popup if selected on lead screen.
					 */
					custpoppup.showPopUp();
					
					if(form.getOlbbBranch().getValue()!=null  && form.getOlbeSalesPerson().getValue()!=null){
						custpoppup.getOlbbBranch().setValue(form.getOlbbBranch().getValue());
						custpoppup.getOlbsalesPerson().setValue(form.getOlbeSalesPerson().getValue());
						
					}else if(form.getOlbbBranch().getValue()!=null){
						
						custpoppup.getOlbbBranch().setValue(form.getOlbbBranch().getValue());
					}else{
						custpoppup.getOlbsalesPerson().setValue(form.getOlbeSalesPerson().getValue());
					}
					
					
//					if(form.getOlbbBranch().getValue()!=null  || form.getOlbbBranch().getValue().equals("")){
//					custpoppup.getOlbbBranch().setValue(form.getOlbbBranch().getValue());
//					
//					}
//					
//					if(form.getOlbeSalesPerson().getValue()!=null ||form.getOlbeSalesPerson().getValue().equals("")){
//						custpoppup.getOlbsalesPerson().setValue(form.getOlbeSalesPerson().getValue());
//					}
				}
				if(event.getSource()==custpoppup.getLblOk()){
					if(validateCustomer()){
						System.out.println("YOU ARE ON TRACK.....!!!!");
						saveNewCustomer();
						custpoppup.hidePopUp();
					}
				}
				if(event.getSource()==custpoppup.getLblCancel()){
					custpoppup.hidePopUp();
				}
				/** added by vijay for communication log
				 */
				if(event.getSource() == CommunicationLogPopUp.getBtnOk()){
					form.showWaitSymbol();
				    List<InteractionType> list = CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
				    ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
				    interactionlist.addAll(list);
				    boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);
				    if(checkNewInteraction==false){
				    	form.showDialogMessage("Please add new interaction details");
				    	form.hideWaitSymbol();
				    }else{	
				    	/**
				    	 * Date : 07-11-2017 KOMAL
				    	 */
				    	final Date followUpDate = interactionlist.get(interactionlist.size()-1).getInteractionDueDate();
				    	 communicationService.saveCommunicationLog(interactionlist, new AsyncCallback<Void>() {
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("Unexpected Error");
								form.hideWaitSymbol();
								LoginPresenter.communicationLogPanel.hide();
							}

							@Override
							public void onSuccess(Void result) {
								form.showDialogMessage("Data Save Successfully");
								form.hideWaitSymbol();
								model.setFollowUpDate(followUpDate);
								form.getDbleaddate().setValue(followUpDate);
								LoginPresenter.communicationLogPanel.hide();
							}
						});
				    }
				}
				if(event.getSource() == CommunicationLogPopUp.getBtnCancel()){
//					communicationPanel.hide();
					LoginPresenter.communicationLogPanel.hide();
				}
				if(event.getSource() == CommunicationLogPopUp.getBtnAdd()){
						form.showWaitSymbol();
						String remark = CommunicationLogPopUp.getRemark().getValue();
						Date dueDate = CommunicationLogPopUp.getDueDate().getValue();
						String interactiongGroup =null;
						if(CommunicationLogPopUp.getOblinteractionGroup().getSelectedIndex()!=0)
							interactiongGroup=CommunicationLogPopUp.getOblinteractionGroup().getValue(CommunicationLogPopUp.getOblinteractionGroup().getSelectedIndex());
						boolean validationFlag = AppUtility.validateCommunicationlog(remark,dueDate);
						if(validationFlag){
							InteractionType communicationLog =  AppUtility.getCommunicationLog(AppConstants.SERVICEMODULE,AppConstants.LEAD,model.getCount(),model.getEmployee(),remark,dueDate,model.getPersonInfo(),null,interactiongGroup, model.getBranch(),model.getStatus());
							CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().getList().add(communicationLog);
							CommunicationLogPopUp.getRemark().setValue("");
							CommunicationLogPopUp.getDueDate().setValue(null);
							CommunicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
						}
						form.hideWaitSymbol();
				}
				/**ends here
				 */
				/**
				 * date 24/10/2017 below code added by komal for project coordinator add
				 * button
				 **/

				if (event.getSource().equals(form.getAddPCButton())) {
					form.showWaitSymbol();
					reactOnAddPCButton();
					form.hideWaitSymbol();
					form.getOlbArchitect().setSelectedIndex(0);
					form.getOlbContractor().setSelectedIndex(0);
					form.getOlbPMC().setSelectedIndex(0);
				}
				/**
				 * Date : 06-11-2017 BY ANIL
				 */
				if (event.getSource() == cancelPopup.getBtnOk()) {
					cancelpanel.hide();
					cancelDocument(AppConstants.CANCELLED,cancelPopup);
				}
				if (event.getSource() == cancelPopup.getBtnCancel()) {
					cancelpanel.hide();
					cancelPopup.remark.setValue("");
				}
				
				if (event.getSource() == unSucessfullPopup.getBtnOk()) {
					cancelpanel.hide();
					cancelDocument(AppConstants.Unsucessful,unSucessfullPopup);
				}
		
				if (event.getSource() == unSucessfullPopup.getBtnCancel()) {
					cancelpanel.hide();
					cancelPopup.remark.setValue("");
					/**
					 * nidhi
					 * 1-08-2018
					 */
					unSucessfullPopup.remark.setValue("");
					/**
					 * end
					 */
				}
				/**
				 * End
				 */
				
				/**
				 * Date 26-5-2018 by jayshree
				 * add the 
				 */
				if(event.getSource()==custpoppup.getBtnserviceaddr()){
					custpoppup.getAddressComp().clear();
					custpoppup.getAddressComp().setValue(custpoppup.getAddressCompservice().getValue());
				}
				if(event.getSource()==custpoppup.getBtnbillingaddr()){
					custpoppup.getAddressCompservice().clear();
					custpoppup.getAddressCompservice().setValue(custpoppup.getAddressComp().getValue());
				}
				
				/**
				 * End
				 */
				
				
			}
			
//Date 18-6-2018 by jayshree add new parameter 
			
			private void cancelDocument(final String status,DocumentCancellationPopUp obj) {
				Console.log("object"+ obj);				
				model.setStatus(status);
				model.setDescription(model.getDescription()+"\n"+"Lead ID ="+model.getCount()+" "+"Lead status ="+form.getOlbLeadStatus().getValue(form.getOlbLeadStatus().getSelectedIndex()).trim()+"\n"
						+"has been "+status+"by "+LoginPresenter.loggedInUser+" with remark "+"\n"+"Remark ="+obj.getRemark().getValue().trim()
						+" "+" Date ="+AppUtility.parseDate(new Date())+"Reason ="+"\n"+obj.getReason().getValue(obj.getReason().getSelectedIndex()).trim());//Date 16-6-2018 by jayshree add reson
//						System.out.println("cancelPopup.getReason().getValue()"+cancelPopup.getReason().getValue().trim());
				form.showWaitSymbol();
				/**
				 * nidhi
				 * 2227 for store reason for unsucessful 
				 */
				model.setDocStatusReason(unSucessfullPopup.getReason().getValue(unSucessfullPopup.getReason().getSelectedIndex()));
				/**
				 * end
				 */
				service.save(model, new AsyncCallback<ReturnFromServer>() {
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(ReturnFromServer result) {
						form.hideWaitSymbol();
						form.olbLeadStatus.setValue(status);
						form.getTaLeadDescription().setValue(model.getDescription());
						form.showDialogMessage("Document updated sucessfully!");
						
						sendSMS(status,model);{
							smsService.sendSMSToCustomer(model, new AsyncCallback<String>(){

								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									
								}

								@Override
								public void onSuccess(String result) {
									// TODO Auto-generated method stub
									
								}
								
							});
						}
					}

					private void sendSMS(String status, Lead model) {
						
						
					}
				});
				
       }





			protected void reactOnAddProducts(){
				if(form.getProdComposite().getProdID().getValue().equals(""))
				{
					form.showDialogMessage("Please select product!");
				}
				
				if(!form.getProdComposite().getProdID().getValue().equals(""))
				{
					boolean productValidation=validateProductDuplicate(Integer.parseInt(form.prodComposite.getProdID().getValue().trim()),form.prodComposite.getProdCode().getValue());
					if(productValidation==true){
						form.showDialogMessage("Product already exists!");
					}
					else{
						addProductsToTable(form.getProdComposite().getProdCode().getValue(),Integer.parseInt(form.getProdComposite().getProdID().getValue().trim()));
						/**
						 * Date : 12-10-2017 BY ANIL
						 * After adding product ,product composite should be clear
						 */
						form.getProdComposite().clear();
					}
				}
			}
			
	private void reactOnAddServiceProduct() {
		Console.log("Inside reactOnService product");
		if (form.olbProductName.getSelectedIndex() == 0
				&& form.olbProductGroup.getSelectedIndex() == 0) {
			Console.log("Inside please select Product");
			form.showDialogMessage("Please select Product Group And Product Name!");
		}
		
        if(form.olbProductGroup.getSelectedIndex()!=0&&form.olbProductName.getSelectedIndex() == 0){
        	form.showDialogMessage("Please Select Product Name");
        }
        if(form.olbProductGroup.getSelectedIndex()==0&&form.olbProductName.getSelectedIndex()!=0){
        	form.showDialogMessage("Please Select Product Group");
        }
		ServiceProduct serProduct = form.olbProductName.getSelectedItem();

		if(form.olbbBranch.getSelectedIndex()==0){
			form.showDialogMessage("Please select branch!");
			return;
		}
		Console.log("serice product code" + serProduct);

		if (form.olbProductName.getSelectedIndex() != 0&&form.olbProductGroup.getSelectedIndex() != 0) {
			Console.log("Inside the add method");
			boolean productValidation = validateProductDuplicate(
					serProduct.getCount(), serProduct.getProductCode());
			if (productValidation == true) {
				form.showDialogMessage("Product already exists!");
			} else {
				Console.log("Inside Else");
				addProductsToTable(serProduct.getProductCode(),
						serProduct.getCount());
				
			}
		}
	}
			
			
				public void addProductsToTable(String pcode,int productId)
				{
					Console.log("Inside ADD PRODUCT TABLE");
					final GenricServiceAsync genasync=GWT.create(GenricService.class);
					final MyQuerry querry=new MyQuerry();
					
					Vector<Filter> filtervec=new Vector<Filter>();
					
					Filter filter=new Filter();
					filter.setQuerryString("productCode");
					filter.setStringValue(pcode.trim());
					filtervec.add(filter);
					
					filter=new Filter();
					filter.setQuerryString("count");
					filter.setIntValue(productId);
					filtervec.add(filter);
					
					filter=new Filter();
					filter.setQuerryString("status");
					filter.setBooleanvalue(true);
					filtervec.add(filter);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new SuperProduct());
					form.showWaitSymbol();
					 Timer timer=new Timer() 
		        	 {
		 				@Override
		 				public void run() {
						genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
							
							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
								form.showDialogMessage("An Unexpected error occurred!");
							}
				
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								
								if(result.size()==0)
								{
									form.showDialogMessage("Please check whether product status is active or not!");
								}
								Branch branchEntity = form.olbbBranch.getSelectedItem();

								for(SuperModel model:result)
								{
									/**
									 * @author Vijay Date- 19-11-2021
									 * Des :- when lead created from priora and then if we add product from erp then duplicate
									 * SR number added so updated code here to get proper SR No.
									 */
									form.findMaxServiceSrNumber();
									
									SuperProduct superProdEntity = (SuperProduct)model;
									SalesLineItem lis = null;
									Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
									if(screenName.toString().trim().equals(AppConstants.SCREENTYPESALESLEAD))
									{
//										lis=AppUtility.ReactOnAddProductCompositeForSales(superProdEntity);
										 lis=AppUtility.ReactOnAddProductCompositeForSales(superProdEntity,form.cust,branchEntity,true);

									}
									if(screenName.toString().trim().equals(AppConstants.SCREENTYPESERVICELEAD))
									{
										 lis=AppUtility.ReactOnAddProductComposite(superProdEntity,form.cust,branchEntity,true);
									}
									
//									srno++;
//									lis.setProductSrNo(srno);
									form.productSrNo++;
									lis.setProductSrNo(form.productSrNo);
									/** date 28/10/2017 added by komal to ser quantity as 1 **/
									lis.setQuantity(1.0);
									form.getLeadProductTable().getDataprovider().getList().add(lis);
								}
								}
							 });
							form.hideWaitSymbol();
			 				}
				 	  };
			          timer.schedule(3000); 
				}
			
			
			public boolean validateProductDuplicate(int prodId,String pcode)
			{
				Console.log("Inside Valid product duplicate");
				List<SalesLineItem> salesItemLis=form.getLeadProductTable().getDataprovider().getList();
				for(int i=0;i<salesItemLis.size();i++)
				{
					if(salesItemLis.get(i).getProductCode().trim().equals(pcode.trim())&&salesItemLis.get(i).getPrduct().getCount()==prodId){
						return true;
					}
				}
				return false;
			}
			
			
			
			public boolean validateCustomer() {

				//  rohan added these validations 
				/** date 06/03/2018 added by komal for nbhc **/
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","makeCustomerPopupCompanyNameMandatory")){
					if(custpoppup.getTbCompanyName().getValue()==null || custpoppup.getTbCompanyName().getValue().equals("")){
					form.showDialogMessage("Compnay name is mandatory.");
					return false;
					}
				}
				/**
				 * end komal
				 */
				if(custpoppup.getTbFullName().getValue().equals("")){
					form.showDialogMessage("Please add Customer Fullname.");
					return false;
				}
				
				if(custpoppup.getOlbbBranch().getSelectedIndex()==0){
					form.showDialogMessage("Branch is mandatory.");
					return false;
				}
				
				/*
				 * Date:17-12-2018
				 * Developer:Ashwini
				 * Des:To make segment mandatory for nbhc
				 */
				
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "CustomerTypeForNbhc")){
					if(custpoppup.getolbcustcategory().getSelectedIndex()==0){
						form.showDialogMessage("Segment is mandatory ");
						return false;
					}
				}
				

				
				
				/**
				 * Date 26-5-2018 by jayshree
				 * des.add this to make field mandatory
				 */
//				if(custpoppup.getAddressCompservice().getAdressline1().getValue()==null||custpoppup.getAddressCompservice().getAdressline1().getValue().equals("")){
//					form.showDialogMessage("Address line one is mandatory");
//					return false;
//				}
//				if(custpoppup.getAddressCompservice().getCity().getValue()==null||custpoppup.getAddressCompservice().getCity().getValue().equals("")){
//					form.showDialogMessage("City is mandatory");
//					return false;
//				}
//				if(custpoppup.getAddressCompservice().getCountry().getValue()==null||custpoppup.getAddressCompservice().getCountry().getValue().equals("")){
//					form.showDialogMessage("Country is mandatory");
//					return false;
//				}
//				if(custpoppup.getAddressCompservice().getState().getValue()==null||custpoppup.getAddressCompservice().getState().getValue().equals("")){
//					form.showDialogMessage("State is mandatory");
//					return false;
//				}
//				if(custpoppup.getAddressComp().getAdressline1().getValue()==null||custpoppup.getAddressCompservice().getAdressline1().getValue().equals("")){
//					form.showDialogMessage("Address line one is mandatory");
//					return false;
//				}
//				if(custpoppup.getAddressComp().getCity().getValue()==null||custpoppup.getAddressCompservice().getCity().getValue().equals("")){
//					form.showDialogMessage("City is mandatory");
//					return false;
//				}
//				if(custpoppup.getAddressComp().getCountry().getValue()==null||custpoppup.getAddressCompservice().getCountry().getValue().equals("")){
//					form.showDialogMessage("Country is mandatory");
//					return false;
//				}
//				if(custpoppup.getAddressComp().getState().getValue()==null||custpoppup.getAddressCompservice().getState().getValue().equals("")){
//					form.showDialogMessage("State is mandatory");
//					return false;
//				}
				
				
				/** date 06/03/2018 added by komal for nbhc **/
				
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","makeCustomerPopupEmailMandatory")){
					if(custpoppup.getEtbEmail().getValue().equals("")){
					form.showDialogMessage("Customer email is mandatory.");
					return false;
					}
				}
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","makeCustomerPopupPhoneNumberMandatory")){
					if(custpoppup.getPnbLandlineNo().getValue().equals("")){
					form.showDialogMessage("Customer cell is mandatory.");
					return false;
					}
				}
			
				/**
				 * end komal
				 */
				
				
				/**
				 * @author Abhinav Bihade
				 * @since 03/02/2020
				 * As Per Nitin Sir's Requirement On Lead screen in ERP,when creating customer in popup in case mobile number is not correct
				 * or space in between system doesnt show any error message but stays on the screen with no message
				 */
//				if(custpoppup.getPnbLandlineNo().getValue().trim()!=null && !custpoppup.getPnbLandlineNo().getValue().trim().equals("")){
//					if(custpoppup.getPnbLandlineNo().getValue().trim().length()>10 || custpoppup.getPnbLandlineNo().getValue().length()<10){
//					form.showDialogMessage("Check Mobile Number Entered!");
//					System.out.println("Customer Popuppup For LandLine");
//					return false;
//				}
//				}
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "DisableCellNumberValidation")){
					disablecellNovalidation=true;
				}
				if(!disablecellNovalidation) {
					Console.log("inside cell no validation in popup");
				if(custpoppup.getPnbLandlineNo().getValue()!=null){
				int cellCurrLen = custpoppup.getPnbLandlineNo().getValue().toString().length();
				if(AppUtility.validateFieldRange(cellCurrLen, 10, 10, "Enter 10 Digit Mobile Number!", "Enter 10 Digit Mobile Number!")==false){
					return false;
				}
				}
				}
				
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "DisableEmailIdValidation")){
					disableEmailValidation=true;
				}
				Console.log("disableEmailValidation value "+disableEmailValidation);
				if(!disableEmailValidation){
					if(custpoppup.getEtbEmail().getValue().trim().equals("")){
						
						form.showDialogMessage("Please Fill Customer Email.");
						return false;
					}	
				}
				
				
//				if(!custpoppup.getTbfName().getValue().equals("")&&custpoppup.getTblName().getValue().equals("")){
//					form.showDialogMessage("Please Fill Customer Last Name!");
//					return false;
//				}
//				if(custpoppup.getTbfName().getValue().equals("")&&!custpoppup.getTblName().getValue().equals("")){
//					form.showDialogMessage("Please Fill Customer First Name!");
//					return false;
//				}
//				
//				if((!custpoppup.getTbfName().getValue().equals("")&&!custpoppup.getTblName().getValue().equals(""))&&(custpoppup.getPnbLandlineNo().getValue().equals(""))){
//					form.showDialogMessage("Please Fill customer cell!");
//					return false;
//				}
				
				/**
				  * Date 26-07-2018 By Vijay
				  * Des:- GSTIN Number can not be greater than 15 characters Validation
				  */
				if(custpoppup.getTbGstNumber().getValue().trim()!=null && !custpoppup.getTbGstNumber().getValue().trim().equals("")){
					if(custpoppup.getTbGstNumber().getValue().trim().length()>15 || custpoppup.getTbGstNumber().getValue().trim().length()<15){
						form.showDialogMessage("GSTIN number must be 15 characters!");
						return false;
					}
				}
				/**
				 * ends here
				 */
				
				return true;
			}
			
			private void saveNewCustomer() {
				final Customer cust=new Customer();
				
				if(!custpoppup.getTbFullName().getValue().trim().equals("")){
					cust.setFullname(custpoppup.getTbFullName().getValue().toUpperCase().trim());
				}
				/**Date 7-8-2020 by Amol added this salutation raised by amol**/
				if(custpoppup.getOlbSalutation().getSelectedIndex()!=0){
					cust.setSalutation(custpoppup.getOlbSalutation().getValue(custpoppup.getOlbSalutation().getSelectedIndex()).trim());
				}
				
//				if(!custpoppup.getTbmName().getValue().equals("")){
//					cust.setMiddleName(custpoppup.getTbmName().getValue().toUpperCase());		
//				}
//				if(!custpoppup.getTblName().getValue().equals("")){
//					cust.setLastName(custpoppup.getTblName().getValue().toUpperCase());
//				}
				/**Date 31-8-2019 by Amol for orkin raised by Rahul**/
				if(custpoppup.getOlbCustomerGroup().getSelectedIndex()!=0){
					cust.setGroup(custpoppup.getOlbCustomerGroup().getValue(custpoppup.getOlbCustomerGroup().getSelectedIndex()).trim());
				}
				
				if(custpoppup.getOlbCustomerType().getSelectedIndex()!=0){
					cust.setType(custpoppup.getOlbCustomerType().getValue(custpoppup.getOlbCustomerType().getSelectedIndex()).trim());
				}
				
				/**Date 25-9-2019 by Amol Map the Customer type to Lead Category for orkin**/
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","MapCustomerTypeToLeadCategory")){
				if(custpoppup.getOlbCustomerType().getSelectedIndex()!=0){
					form.olbLeadCategory.setValue(custpoppup.getOlbCustomerType().getValue(custpoppup.getOlbCustomerType().getSelectedIndex()).trim());
					ConfigCategory cat=form.olbLeadCategory.getSelectedItem();
					if(cat!=null){
					AppUtility.makeLiveTypeDropDown(form.olbcLeadType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
					}
				}
				}
				
				/**
				 * @author Abhinav Bihade
				 * @since 26/12/2019
				 * Orkin : On  Lead page if we create new customer or select any existing customer customer,
				 * then Customer Group should map in Lead Group
				 * 
				 */
				if(custpoppup.getOlbCustomerGroup().getSelectedIndex()!=0){
					form.olbLeadGroup.setValue(custpoppup.getOlbCustomerGroup().getValue(custpoppup.getOlbCustomerGroup().getSelectedIndex()).trim());
				}
				
			    /**Added by sheetal:07-12-2021
			       Des: If new customer is created Branch,Sales person,customer category
			       and customer type should get mapped in lead screen**/
				
				if(custpoppup.getOlbbBranch().getSelectedIndex()!=0){
					form.olbbBranch.setValue(custpoppup.getOlbbBranch().getValue(custpoppup.getOlbbBranch().getSelectedIndex()).trim());
				}
				
				if(custpoppup.getolbcustcategory().getSelectedIndex()!=0){
					form.olbLeadCategory.setValue(custpoppup.getolbcustcategory().getValue(custpoppup.getolbcustcategory().getSelectedIndex()).trim());
				}
				
				if(custpoppup.getOlbCustomerType().getSelectedIndex()!=0){
					form.olbcLeadType.setValue(custpoppup.getOlbCustomerType().getValue(custpoppup.getOlbCustomerType().getSelectedIndex()).trim());
				}
				
				if(custpoppup.getOlbsalesPerson().getSelectedIndex()!=0){
					form.olbeSalesPerson.setValue(custpoppup.getOlbsalesPerson().getValue(custpoppup.getOlbsalesPerson().getSelectedIndex()).trim());
				}
				
				/**end**/
				
				if(!custpoppup.getTbCompanyName().getValue().trim().equals(""))
				{
					System.out.println("Inside if condition");
					cust.setCompanyName(custpoppup.getTbCompanyName().getValue().toUpperCase().trim());
					cust.setCompany(true);
				}
				
				
				if(!custpoppup.getPnbLandlineNo().getValue().equals("")){
					cust.setCellNumber1(custpoppup.getPnbLandlineNo().getValue());
				}
				if(!custpoppup.getEtbEmail().getValue().equals("")){
					cust.setEmail(custpoppup.getEtbEmail().getValue().trim());
				}
				//  rohan added this code for setting branch in customer 
				
				if(custpoppup.getOlbbBranch().getSelectedIndex()!=0){
					cust.setBranch(custpoppup.getOlbbBranch().getValue(custpoppup.getOlbbBranch().getSelectedIndex()).trim());
				}
				
				/*
				 * Date:13-12-2018
				 * @author Ashwini
				  */
				
		
				System.out.println("lead presenter cust category123::"+custpoppup.getolbcustcategory().getValue(custpoppup.getolbcustcategory().getSelectedIndex()));
				if(custpoppup.getolbcustcategory().getSelectedIndex()!=0){
				
					cust.setCategory(custpoppup.getolbcustcategory().getValue(custpoppup.getolbcustcategory().getSelectedIndex()));
					
				    
				}
				
//				if(custpoppup.getOlbsalesPerson().getSelectedIndex()!=0){
//					cust.setEmployee(custpoppup.getOlbsalesPerson().getValue(custpoppup.getOlbsalesPerson().getSelectedIndex()));
//				}
				/*
				 * end by Ashwini
				 */
				

				
				/**
				 * Date : 08-08-2017 by ANIL
				 */
				boolean addressFlag=false;
//				if(custpoppup.getAddressComp().getValue()!=null){
//					cust.setAdress(custpoppup.getAddressComp().getValue());
//					cust.setSecondaryAdress(custpoppup.getAddressComp().getValue());
//					addressFlag=true;
//				}
				
				/**
				 * Date 27-4-2018
				 * By jayshree
				 * add 
				 */
				if((custpoppup.getAddressComp().getAdressline1()!=null
					&&custpoppup.getAddressComp().getCity().getValue()!=null 
					&&custpoppup.getAddressComp().getState().getValue()!=null 
					&&custpoppup.getAddressComp().getCountry().getValue()!=null)
					
					&&(custpoppup.getAddressCompservice().getAdressline1()!=null
					&&custpoppup.getAddressCompservice().getCity().getValue()!=null
					&&custpoppup.getAddressCompservice().getState().getValue()!=null
					&&custpoppup.getAddressCompservice().getCountry().getValue()!=null))
				{
					
					System.out.println("both true");
					cust.setAdress(custpoppup.getAddressComp().getValue());
					cust.setSecondaryAdress(custpoppup.getAddressCompservice().getValue());
					addressFlag=true;
				}
				
				else if(custpoppup.getAddressComp().getValue()!=null ){//&&(custpoppup.getAddressCompservice().getValue()==null||custpoppup.getAddressCompservice().getValue().equals(""))
					System.out.println("one true");
					cust.setAdress(custpoppup.getAddressComp().getValue());
					cust.setSecondaryAdress(custpoppup.getAddressComp().getValue());
					addressFlag=true;
				}
				
				else if(custpoppup.getAddressCompservice().getValue()!=null){//&& (custpoppup.getAddressComp().getValue()==null ||custpoppup.getAddressComp().getValue().equals(""))
					System.out.println("two true");
					cust.setAdress(custpoppup.getAddressCompservice().getValue());
					cust.setSecondaryAdress(custpoppup.getAddressCompservice().getValue());
					addressFlag=true;
				}
				
				//End by jayshree
				
				if(custpoppup.getTbGstNumber().getValue()!=null&&!custpoppup.getTbGstNumber().getValue().equals("")){
					ArticleType articalType=new ArticleType();
					articalType.setDocumentName("Invoice");
					articalType.setArticleTypeName("GSTIN");
					articalType.setArticleTypeValue(custpoppup.getTbGstNumber().getValue());
					articalType.setArticlePrint("Yes");
					cust.getArticleTypeDetails().add(articalType);
				}
				
				if(addressFlag){
					cust.setNewCustomerFlag(false);
				}else{
					cust.setNewCustomerFlag(true);
				}
				/**
				 * End
				 */
				/**
				 * Date 31-01-2019 by Vijay
				 * Des :- added Sales Person in customer  
				 */
				if(custpoppup.getOlbsalesPerson().getSelectedIndex()!=0){
					cust.setEmployee(custpoppup.getOlbsalesPerson().getValue());
				}
				/**
				 * ends here
				 */
				
				form.showWaitSymbol();
				genasync.save(cust,new AsyncCallback<ReturnFromServer>() {
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						form.showDialogMessage("An unexpected error occurred");
					}
					@Override
					public void onSuccess(ReturnFromServer result) {
						System.out.println("BEFORRERERRERERERRERE");
						panel.hide(); 
						
						cust.setCount(result.count);
						setCustomerDetails(cust);
						
						
						form.hideWaitSymbol();
						
						/*
						 *  nidhi
						 *  	22-07-2017
						 *  	commented this code for stop reloading customer and vender
						 *  
						 */
//						MyQuerry querry=new MyQuerry();
//						querry.setQuerryObject(new Customer());
//						form.pic = new PersonInfoComposite(querry, false);
//					    System.out.println("AFTERRRRERERRERRERERER");
//					    
//						Timer t = new Timer() {
//						      @Override
//						      public void run() {
//								System.out.println("INSIDE SUCCESS !!!");
//								Lead lead=getFilledFormDetails();
//								setNewCustomerDetails(lead);
////								form.hideWaitSymbol();
//						      }
//						};
//					    t.schedule(2000);
					}
				});
			}
			
			public void setCustomerDetails(Customer entity){
				try {
//					Customer entity=(Customer) model;
					PersonInfo info=new PersonInfo();
					
					if(entity.isCompany()==true)
					{
						info.setCount(entity.getCount());
						info.setFullName(entity.getCompanyName());
						info.setCellNumber(entity.getCellNumber1());
						info.setPocName(entity.getFullname());
						form.tbSegment.setValue(custpoppup.getolbcustcategory().getValue(custpoppup.getolbcustcategory().getSelectedIndex()));//Added by Ashwini
					}
					else
					{
						info.setCount(entity.getCount());
						info.setFullName(entity.getFullname());
						info.setCellNumber(entity.getCellNumber1());
						info.setPocName(entity.getFullname());
						form.tbSegment.setValue(custpoppup.getolbcustcategory().getValue(custpoppup.getolbcustcategory().getSelectedIndex()));//Added by Ashwini
					}
					
					/**
					 * @author Anil @since 15-09-2021
					 * When customer screen is created from summary screen lead screen then customer 
					 * details not mapped on composite
					 */
					try{
					    LeadPresenterSearchProxy search = (LeadPresenterSearchProxy) form.getSearchpopupscreen();
					    search.personInfo.setPersonInfoHashMap(info);
					}catch(Exception e){
						
					}
					
					/**
					 *  customer object set in personInfoComposite and 
					 *   and new entered value displayed in form using  
					 *   setNewCustomerDetails Method in LeadForm. java class
					 */
					
					PersonInfoComposite.globalCustomerArray.add(info);
//					lead.setPersonInfo(info);
					System.out.println("NEW CUSTOMER NAME ::: "+entity.getFullname());
				//	lead.setPersonInfo(info);
					
//					form.pic.setValue(info);
					form.setNewCustomerDetails(info);
//					form.updateView(lead);
					form.showDialogMessage("Customer Saved Successfully!");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
			}
			
			private Lead getFilledFormDetails() {
				Lead model=new Lead();
				
				if(!form.getTbLeadId().getValue().equals(""))
				{
					model.setCount(Integer.parseInt(form.getTbLeadId().getValue()));
				}
				
				if(form.tbLeadTitle.getValue()!=null)
					model.setTitle(form.tbLeadTitle.getValue());
				if(form.taLeadDescription.getValue()!=null)
					model.setDescription(form.taLeadDescription.getValue());
				if(form.olbcLeadPriority.getValue()!=null)
					model.setPriority(form.olbcLeadPriority.getValue());
				if(form.olbbBranch.getValue()!=null)
					model.setBranch(form.olbbBranch.getValue());
				if(form.olbeSalesPerson.getValue()!=null)
					model.setEmployee(form.olbeSalesPerson.getValue());
				if(form.olbLeadStatus.getValue()!=null)
					model.setStatus(form.olbLeadStatus.getValue());
				if(form.olbcLeadType.getValue()!=null)
					model.setType(form.olbcLeadType.getValue(form.olbcLeadType.getSelectedIndex()));
				if(form.olbLeadGroup.getValue()!=null)
					model.setGroup(form.olbLeadGroup.getValue());
				if(form.olbLeadCategory.getValue()!=null)
					model.setCategory(form.olbLeadCategory.getValue());
				if(form.dbleaddate.getValue()!=null)
					model.setCreationDate(form.dbleaddate.getValue());
				if(form.pic.getValue()!=null)
					model.setPersonInfo(form.pic.getValue());

				List<SalesLineItem> saleslis=form.getLeadProductTable().getDataprovider().getList();
				ArrayList<SalesLineItem> arrItems=new ArrayList<SalesLineItem>();
				arrItems.addAll(saleslis);
				model.setLeadProducts(arrItems);
				
				Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
				String currentScrType=s.toString();
				
				if(currentScrType.equals(AppConstants.SCREENTYPESALESLEAD)){
					model.setLeadScreenType(AppConstants.SALESTYPECUST);
				}
				
				if(currentScrType.equals(AppConstants.SCREENTYPESERVICELEAD)){
					model.setLeadScreenType(AppConstants.SERVICETYPECUST);
				}
				
				return model;
			}
			
			private void setNewCustomerDetails(final Lead lead){
				if(!custpoppup.getTbFullName().getValue().equals("")){
					String custnameval=custpoppup.getTbFullName().getValue().trim();
					custnameval=custnameval.toUpperCase().trim();
					if(custnameval!=null){
							final MyQuerry querry=new MyQuerry();
							Vector<Filter> filtervec=new Vector<Filter>();
							Filter temp=null;
							
							temp=new Filter();
							temp.setQuerryString("companyId");
							temp.setLongValue(UserConfiguration.getCompanyId());
							filtervec.add(temp);
							
							temp=new Filter();
							temp.setQuerryString("fullname");
							temp.setStringValue(custnameval);
							filtervec.add(temp);
							
							querry.setFilters(filtervec);
							querry.setQuerryObject(new Customer());
							genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
								@Override
								public void onFailure(Throwable caught) {
									final GWTCAlert alert = new GWTCAlert(); 
									alert.alert("An Unexpected Error occured !");
									form.hideWaitSymbol();
								}
								@Override
								public void onSuccess(final ArrayList<SuperModel> result) {
									System.out.println("Success"+result.size());
									
									
									if(lead.equals(AppConstants.SCREENTYPESALESLEAD)){
										AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Lead",Screen.SALESLEAD);
									}
									
									if(lead.equals(AppConstants.SCREENTYPESERVICELEAD)){
										AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Lead",Screen.LEAD);
									}
									form.hideWaitSymbol();
									final LeadForm form = LeadPresenter.initalize();
									form.showWaitSymbol();
									Timer t = new Timer() {
										@Override
									    public void run() {
											if(result.size()!=0){
												for(SuperModel model:result){
													
													Customer entity=(Customer) model;
													PersonInfo info=new PersonInfo();
													
													if(entity.isCompany()==true)
													{
														info.setCount(entity.getCount());
														info.setFullName(entity.getCompanyName());
														info.setCellNumber(entity.getCellNumber1());
														info.setPocName(entity.getFullname());
													}
													else
													{
														info.setCount(entity.getCount());
														info.setFullName(entity.getFullname());
														info.setCellNumber(entity.getCellNumber1());
														info.setPocName(entity.getFullname());
													}
													
													System.out.println("NEW CUSTOMER NAME ::: "+entity.getFullname());
													lead.setPersonInfo(info);
													form.updateView(lead);
													form.showDialogMessage("Customer Saved Successfully!");
//													hideScreen();
													form.hideWaitSymbol();
												}
											}
									    }
									};
								    t.schedule(2000);
								    System.out.println(" hi vijay wait symbol");
								    form.hideWaitSymbol();
								}
							});
					}
				}
			}

			@Override
			public void onRowCountChange(RowCountChangeEvent event) {
				
			//   rohan added this method for product serial number 
//							setProductSrNoMethod();
				
				NumberFormat nf=NumberFormat.getFormat("0.00");
				if(event.getSource().equals(form.getLeadProductTable().getTable()))
				{
					Console.log("LEAD PRODUCT TBL - ON ROW COUNT CHANGE");
					this.reactOnLineItem();
					
				}
			}
			
			private void reactOnLineItem() {


				form.showWaitSymbol();
				Timer timer=new Timer() 
			     {
					@Override
					public void run() 
					{
						NumberFormat nf=NumberFormat.getFormat("0.00");
					    double totalExcludingTax=form.getLeadProductTable().calculateTotalExcludingTax();
					    totalExcludingTax=Double.parseDouble(nf.format(totalExcludingTax));
					    form.getDototalamt().setValue(totalExcludingTax);
					    
					    /**
					     * Date 31-08-2017 added by vijay for final total amount after discount
					     */
					    if(form.getDbDiscountAmt().getValue()!=null && form.getDbDiscountAmt().getValue()!=0){
				    		if(form.getDbDiscountAmt().getValue()>totalExcludingTax){
					    		form.getDbDiscountAmt().setValue(0d);
				    		}
				    		else if(form.getDbDiscountAmt().getValue()<totalExcludingTax){
						    	form.getDbFinalTotalAmt().setValue(totalExcludingTax - form.getDbDiscountAmt().getValue());
				    		}
				    		else{
					    		form.getDbFinalTotalAmt().setValue(totalExcludingTax);
				    		}
				    	}
					    else{
					    	form.getDbFinalTotalAmt().setValue(totalExcludingTax);
				    	}
					    
					    /**
					     * ends here
					     */
					    
						try {
							form.prodTaxTable.connectToLocal();
							form.addProdTaxes();
//							double totalIncludingTax=form.getDototalamt().getValue()+form.getProdTaxTable().calculateTotalTaxes();
							/** Date 31-08-2017 above old code commented and new code below line added by vijay for final total amount after discount **/
							double totalIncludingTax=form.getDbFinalTotalAmt().getValue()+form.getProdTaxTable().calculateTotalTaxes();

							double netPay = Double.parseDouble(nf.format(totalIncludingTax));
							netPay = Math.round(netPay);
							
							/** Date 31-08-2017 added by vijay for final netpayable amount after discount **/

							if(!form.getTbroundOffAmt().getValue().equals("") && form.getTbroundOffAmt().getValue()!=null){
								String roundOff = form.getTbroundOffAmt().getValue();
								double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
								if(roundoffAmt!=0){
									form.getDonetpayamt().setValue(roundoffAmt);

								}else{
									form.getDonetpayamt().setValue(roundoffAmt);
									form.getTbroundOffAmt().setValue("");
								}
								
							}else{
								form.getDonetpayamt().setValue(netPay);
							}
							if(form.getLeadProductTable().getDataprovider().getList().size()==0){
								form.getDbDiscountAmt().setValue(0d);
								form.getTbroundOffAmt().setValue(0+"");
								form.getDonetpayamt().setValue(0d);

							}
							/** ends here *******/
//							form.getDonetpayamt().setValue(netPay);

						} catch (Exception e) {
							e.printStackTrace();
						}
						form.hideWaitSymbol();
					}
			     };
		    	 timer.schedule(3000);
			
			}
			
//			private void setProductSrNoMethod() {
//				
//				if(form.getLeadProductTable().getDataprovider().getList().size()!=0){
//				List<SalesLineItem> tableList = form.getLeadProductTable().getDataprovider().getList();
//				for (int i=0;i<tableList.size();i++)
//				{
//					tableList.get(i).setProductSrNo(i+1);
//				}
//				
//				form.getLeadProductTable().getDataprovider().setList(tableList);
//			}
//			}
//			public void hideScreen(){
//				form.hideWaitSymbol();
//			}	
			
			
			//  rohan added this method
			private void findMaxServiceSrNumberQuotation(String DocType) {
				
				int maxSrNumber = 0 ;
				Console.log("rohan in side max service number");
				
				List<SalesLineItem> list = form.leadProductTable.getDataprovider().getList();
				Console.log("mazza list size "+list.size());
				
				Comparator<SalesLineItem> compare = new  Comparator<SalesLineItem>() {
					
					@Override
					public int compare(SalesLineItem o1, SalesLineItem o2) {
						// TODO Auto-generated method stub
						return Integer.compare(o2.getProductSrNo(), o1.getProductSrNo());
					}
				};
				Collections.sort(list,compare);
				
					System.out.println("product sr number "+list.get(0).getProductSrNo());
					maxSrNumber = list.get(0).getProductSrNo();
					if(DocType.equals("Quotation"))
					{
						QuotationForm.productSrNo = maxSrNumber;
					}
					else
					{
						ContractForm.productSrNo = maxSrNumber;
					}
			}
			
			@Override
			public void reactOnCommunicationLog() {

				form.showWaitSymbol();
				MyQuerry querry = AppUtility.communicationLogQuerry(model.getCount(), model.getCompanyId(),AppConstants.SERVICEMODULE,AppConstants.LEAD);
				
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						
						ArrayList<InteractionType> list = new ArrayList<InteractionType>();
						
						for(SuperModel model : result){
							
							InteractionType interactionType = (InteractionType) model;
							list.add(interactionType);
						}
						form.hideWaitSymbol();
						/**
						 * Date : 08-11-2017 BY Komal
						 */
						Comparator<InteractionType> comp=new Comparator<InteractionType>() {
							@Override
							public int compare(InteractionType e1, InteractionType e2) {
//								Integer coutn1=o1.getCount();
//								Integer count2=o2.getCount();
//								return ;
//								if (e1.getCount() == e2.getCount()) {
//									return 0;
//								}
//								if (e1.getCount() > e2.getCount()) {
//									return 1;
//								} else {
//									return -1;
//								}
								/**
								 * Updated By: Viraj
								 * Date: 06-02-2018
								 * Description: To sort communication log
								 */
								Integer ee1=e1.getCount();
								Integer ee2=e2.getCount();
								return ee2.compareTo(ee1);
								/** Ends **/
								
							}
						};
						Collections.sort(list,comp);
						/**
						 * End
						 */
						CommunicationLogPopUp.getRemark().setValue("");
						CommunicationLogPopUp.getDueDate().setValue(null);
						CommunicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
						CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
//						communicationPanel = new PopupPanel(true);
//						communicationPanel.add(CommunicationLogPopUp);
//						communicationPanel.show();
//						communicationPanel.center();
						
						LoginPresenter.communicationLogPanel = new PopupPanel(true);
						LoginPresenter.communicationLogPanel.add(CommunicationLogPopUp);
						LoginPresenter.communicationLogPanel.show();
						LoginPresenter.communicationLogPanel.center();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
				
				
				
			}	
			
			/**
			 * date 23/10/2017 added by komal to add project coordinator details in
			 * table
			 **/
			public void reactOnAddPCButton() {
				boolean validateFlag;
				List<ProjectCoordinatorDetails> list = form
						.getLeadProjectCoordinatorTable().getDataprovider().getList();
				ArrayList<ProjectCoordinatorDetails> arrList = new ArrayList<ProjectCoordinatorDetails>();
				arrList.addAll(list);
				ProjectCoordinatorDetails pcDetails = null;

				if (form.getOlbArchitect().getSelectedIndex() == 0
						&& form.getOlbPMC().getSelectedIndex() == 0
						&& form.getOlbContractor().getSelectedIndex() == 0) {
					form.showDialogMessage("Please select at least one project coordinator");
				}

				if (form.getOlbArchitect().getSelectedIndex() != 0) {
					ContactPersonIdentification architect = form.getOlbArchitect()
							.getSelectedItem();
					validateFlag = validateInput(architect.getCount());

					pcDetails = new ProjectCoordinatorDetails();
					pcDetails.setCount(architect.getCount());
					pcDetails.setName(architect.getName());
					pcDetails.setPocName(architect.getPocName());
					pcDetails.setRole(architect.getRole());
					pcDetails.setEmail(architect.getEmail());
					pcDetails.setCell(architect.getCell());
					pcDetails.setKdf(0.0);
					if (validateFlag) {
						arrList.add(pcDetails);
					} else {
						form.showDialogMessage("Architect Already exists!");
					}
				}
				if (form.getOlbPMC().getSelectedIndex() != 0) {
					ContactPersonIdentification pmc = form.getOlbPMC()
							.getSelectedItem();
					validateFlag = validateInput(pmc.getCount());

					pcDetails = new ProjectCoordinatorDetails();
					pcDetails.setCount(pmc.getCount());
					pcDetails.setName(pmc.getName());
					pcDetails.setPocName(pmc.getPocName());
					pcDetails.setRole(pmc.getRole());
					pcDetails.setEmail(pmc.getEmail());
					pcDetails.setCell(pmc.getCell());
					pcDetails.setKdf(0.0);
					if (validateFlag) {
						arrList.add(pcDetails);
					} else {
						form.showDialogMessage("PMC Already exists!");
					}
				}
				if (form.getOlbContractor().getSelectedIndex() != 0) {
					ContactPersonIdentification contractor = form.getOlbContractor()
							.getSelectedItem();
					validateFlag = validateInput(contractor.getCount());

					pcDetails = new ProjectCoordinatorDetails();
					pcDetails.setCount(contractor.getCount());
					pcDetails.setName(contractor.getName());
					pcDetails.setPocName(contractor.getPocName());
					pcDetails.setRole(contractor.getRole());
					pcDetails.setEmail(contractor.getEmail());
					pcDetails.setCell(contractor.getCell());
					pcDetails.setKdf(0.0);
					if (validateFlag) {
						arrList.add(pcDetails);
					} else {
						form.showDialogMessage("Contractor Already exists!");
					}
				}

				form.getLeadProjectCoordinatorTable().getDataprovider()
						.setList(arrList);

			}

			/** date 25/10/2017 added by komal to validate project coordinator details **/
			public boolean validateInput(int pcCount) {
				List<ProjectCoordinatorDetails> list = form
						.getLeadProjectCoordinatorTable().getDataprovider().getList();
				for (ProjectCoordinatorDetails cpObj : list) {
					if (cpObj.getCount() == pcCount) {
						return false;
					}
				}

				return true;
			}
			
			void reactOnCustomerLeadMail(){
				try {
					System.out.println("in side email method");
					
					boolean conf = Window.confirm("Do you really want to send email?");
					if(model.getDocument()!=null){
						if (conf == true) {
							emailService.emailToCustomerFromLead(model,new AsyncCallback<Void>() {
					
								@Override
								public void onFailure(Throwable caught) {
									Window.alert("Resource Quota Ended ");
									caught.printStackTrace();
									
								}
					
								@Override
								public void onSuccess(Void result) {
									Window.alert("Email Sent Sucessfully !");
									
								}
							});
						}
					}else{
						form.showDialogMessage("Please upload Document file.");
					}
				
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
					Console.log("reactOnCustomerLeadMail method -- " + e.getMessage());
				}
			}
			
			
			private void reactonSignUp() {
				List<SalesLineItem> list = form.leadProductTable.getDataprovider().getList();
				boolean conf = true;
				if(form.olbLeadStatus.getValue()!=null && form.olbLeadStatus.getValue().equalsIgnoreCase(Lead.SUCCESSFULL) || form.olbLeadStatus.getValue().equalsIgnoreCase("Successful") ) {
					form.showDialogMessage("Lead is already sucessful");
					return;
				}
				for(SalesLineItem lineItem : list) {
					if(lineItem.getNumberOfServices()<=0) {
						conf =  Window.confirm("No of services is 0 , Do you want to continue?");
					}
				}
				Console.log("conf "+conf);
				if(conf) {
					String Url = com.google.gwt.core.client.GWT.getModuleBaseURL(); 
					String Appid = "";
					if(Url.contains("-dot-")){
						String [] urlarray = Url.split("-dot-");
						 Appid = urlarray[1];
					}
					else{
						String [] urlarray = Url.split("\\.");
						 Appid = urlarray[1];
					}
					Console.log("Appid "+Appid);
					form.showWaitSymbol();
//					commonservice.CallPaymentGatewayAPI(model,Url,Appid, new AsyncCallback<String>() {
//						
//						@Override
//						public void onSuccess(String result) {
//							// TODO Auto-generated method stub
////							form.showDialogMessage(result);
//							if(result.contains("@Error")){
//								String error[] = result.split("@Error");
//								form.showDialogMessage(error[0]);
//
//							}
//							else{
//								form.showDialogMessage("Sign Up Initiated And SMS Sent To Customer");
//								form.getTbpaymentgatewayid().setValue(result);
//								
//							}
//								form.hideWaitSymbol();
//						}
//						
//						@Override
//						public void onFailure(Throwable caught) {
//							// TODO Auto-generated method stub
//							form.hideWaitSymbol();
//							form.showDialogMessage("Please try again!");
//						}
//					});
					commonservice.CallPaymentGatewayAPI(model.getCount(), AppConstants.LEAD, Url, Appid, model.getCompanyId(), model.getPersonInfo().getCount(),null, new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
							form.showDialogMessage("Please try again!");
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							if(result.contains("@Error")){
								String error[] = result.split("@Error");
								form.showDialogMessage(error[0]);

							}
							else{
								form.showDialogMessage("Sign Up Initiated And SMS Sent To Customer");
								form.getTbpaymentgatewayid().setValue(result);
								
							}
								form.hideWaitSymbol();
						}
					});
				}
				
			}
			
			
			private void setEmailPopUpData() {
				form.showWaitSymbol();
				String customerEmail = "";

				String branchName = form.olbbBranch.getValue();
				String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
				customerEmail = AppUtility.getCustomerEmailid(model.getPersonInfo().getCount());
				
				String customerName = model.getPersonInfo().getFullName();
				if(!customerName.equals("") && !customerEmail.equals("")){
					label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),customerName,customerEmail,null);
					emailpopup.taToEmailId.setValue(customerEmail);
				}
				else{
			
					MyQuerry querry = AppUtility.getcustomerQuerry(model.getPersonInfo().getCount());
					genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							// TODO Auto-generated method stub
							for(SuperModel smodel : result){
								Customer custEntity  = (Customer) smodel;
								System.out.println("customer Name = =="+custEntity.getFullname());
								System.out.println("Customer Email = == "+custEntity.getEmail());
								
								if(custEntity.getEmail()!=null){
									label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
									emailpopup.taToEmailId.setValue(custEntity.getEmail());
								}
								else{
									label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
								}
								break;
							}
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
						}
					});
				}
				
				Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
				Console.log("screenName "+screenName);
				
				//Ashwini Patil wrote conditions for summary screen
				if(screenName.name().equals("SALESLEAD"))
					AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.SALESMODULE,screenName);				
				else if(screenName.name().equals("LEAD"))
					AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.SERVICE,screenName);
				
				emailpopup.taFromEmailId.setValue(fromEmailId);
				emailpopup.smodel = model;
				form.hideWaitSymbol();
				
			}





			@Override
			public void reactOnSMS() {
				
				smspopup.showPopUp();
				loadSMSPopUpData();
				
			}


			private void loadSMSPopUpData() {

				form.showWaitSymbol();
				String customerCellNo = model.getPersonInfo().getCellNumber()+"";

				String customerName = model.getPersonInfo().getFullName();
				if(!customerName.equals("") && !customerCellNo.equals("")){
					AppUtility.loadContactPersonlist(smspopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),customerName,customerCellNo,null,true);
				}
				
				Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
				Console.log("screenName "+screenName);
				//Ashwini Patil wrote conditions for summary screen
				if(screenName.name().equals("SALESLEAD"))
					AppUtility.makeLiveSmsTemplateConfig(smspopup.oblsmsTemplate,AppConstants.SALESMODULE,screenName,true,AppConstants.SMS);//LoginPresenter.currentModule.trim()								
				else if(screenName.name().equals("LEAD"))
					AppUtility.makeLiveSmsTemplateConfig(smspopup.oblsmsTemplate,AppConstants.SERVICE,screenName,true,AppConstants.SMS);//LoginPresenter.currentModule.trim()
				
				smspopup.getRbSMS().setValue(true);
				smspopup.smodel = model;
				form.hideWaitSymbol();
				
			
			}
			
			
			private void downloadData(ArrayList<Lead> leadArray, boolean communicationLogFlag, ArrayList<InteractionType> interactiontypelist) {
				
				 ArrayList<String> leadlist  = new ArrayList<String>();
					leadlist.add("LEAD ID");
					leadlist.add("FOLLOW-UP DATE");
					leadlist.add("CREATED BY");
					leadlist.add("CUSTOMER ID");
					leadlist.add("CUSTOMER NAME");
					leadlist.add("CUSTOMER CELL");
					leadlist.add("LEAD TITLE");
					leadlist.add("SALES PERSON");
					leadlist.add("GROUP");
					leadlist.add("CATEGORY");
					leadlist.add("TYPE");
					leadlist.add("PRIORITY");
					leadlist.add("BRANCH");
					leadlist.add("DESCRIPTION");
					leadlist.add("STATUS");
					leadlist.add("Lead Unsuccessfull Reason");
					leadlist.add("CREATION DATE");
					leadlist.add("TOTAL AMOUNT");
					leadlist.add("DISCOUNT AMOUNT");
					leadlist.add("AFTER DISCOUNT TOTAL AMOUNT");
					leadlist.add("ROUND OFF");
					leadlist.add("NET PAYABLE");
					if(communicationLogFlag) {
						leadlist.add("COMMUNICATION LOG");
					}
//					else{
//						leadlist.add("");
//					}
					int columncount = leadlist.size();
					
					for(Lead lead : leadArray){
						leadlist.add(lead.getCount()+"");
						if(lead.getFollowUpDate()!=null)
							leadlist.add(AppUtility.parseDate(lead.getFollowUpDate(),"dd-MMM-yyyy"));
						else
							leadlist.add("");
						
						leadlist.add(lead.getCreatedBy());
						leadlist.add(lead.getPersonInfo().getCount()+"");
						leadlist.add(lead.getPersonInfo().getFullName());
						leadlist.add(lead.getPersonInfo().getCellNumber()+"");
						leadlist.add(lead.getTitle());
						leadlist.add(lead.getEmployee());
						leadlist.add(lead.getGroup());
						leadlist.add(lead.getCategory());
						leadlist.add(lead.getType());
						leadlist.add(lead.getPriority());
						leadlist.add(lead.getBranch());
						leadlist.add(lead.getDescription());
						leadlist.add(lead.getStatus());
						System.out.println("leadlist size "+leadlist.size());
						if(lead.getDocStatusReason()!=null&&!lead.getDocStatusReason().equalsIgnoreCase("--SELECT--")){
							leadlist.add(lead.getDocStatusReason());
							System.out.println("lead.getDocStatusReason()"+lead.getDocStatusReason());
						}
						else
							leadlist.add("");
						System.out.println("after blank added  leadlist size "+leadlist.size());

						if(lead.getFollowUpDate()!=null)
							leadlist.add(AppUtility.parseDate(lead.getCreationDate(),"dd-MMM-yyyy"));
						else
							leadlist.add("");
						leadlist.add(lead.getTotalAmount()+"");
						leadlist.add(lead.getDiscountAmt()+"");
						leadlist.add(lead.getFinalTotalAmt()+"");
						leadlist.add(lead.getRoundOffAmt()+"");
						leadlist.add(lead.getNetpayable()+"");
						
						if(communicationLogFlag) {
				        	  ArrayList<InteractionType> list=getInteractionList(lead.getCount(),interactiontypelist);
				        	  Console.log("commulog Interaction type size"+list.size());
				        	  String communicationLog="";
				        	  if(list!=null&&list.size()!=0){
				        		  for(InteractionType interactiontype:list){
				        			  communicationLog+=" "+AppUtility.parseDate(interactiontype.getinteractionCreationDate(),"dd-MM-yyyy")+":"+interactiontype.getInteractionPurpose();

				        		  } 
				        		  leadlist.add(communicationLog);
				        	  }else{
				      			leadlist.add("");
				        	  }	        	  
						}
//						else {
//			      			leadlist.add("");
//						}
					}
					
					
					csvservice.setExcelData(leadlist, columncount, AppConstants.LEAD, new AsyncCallback<String>() {
						
						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							
							String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
							final String url=gwt + "CreateXLXSServlet"+"?type="+10;
							Window.open(url, "test", "enabled");
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
					});
			}



			private ArrayList<InteractionType> getInteractionList(int leadId, ArrayList<InteractionType> interactiontypelist) {

				ArrayList<InteractionType> interactionList=new ArrayList<InteractionType>();
				
				for(InteractionType interactiontype:interactiontypelist){
					
					if(interactiontype.getInteractionDocumentId()==leadId){
						interactionList.add(interactiontype);
					}
				}
				
				Comparator<InteractionType> datecomparator = new Comparator<InteractionType>() {  
					@Override  
					public int compare(InteractionType o1, InteractionType o2) {
						Date date1=o1.getinteractionCreationDate();
						Date date2=o2.getinteractionCreationDate();
						return date1.compareTo(date2);  
					}  
				};  
				Collections.sort(interactionList,datecomparator);
				
				
				return interactionList;
				
			}
			
			
			
}
