package com.slicktechnologies.client.views.lead;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Window;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.DsReport;
import com.slicktechnologies.shared.common.businessprocesslayer.CompanyProfileConfiguration;

public class CompanyProfileDetailsTable extends SuperTable<CompanyProfileConfiguration> {
    
	TextColumn<CompanyProfileConfiguration> leadCategoryColumn;
	TextColumn<CompanyProfileConfiguration> companyProfileColumn;
	TextColumn<CompanyProfileConfiguration> statusColumn;
	
	public CompanyProfileDetailsTable()
	{
		super();
		table.setWidth("100%");
		table.setHeight("700px");
		
	}
	
	
	@Override
	public void createTable() {
		addColumnLeadCategory();
		addColumnComapnyProfile();
		addColumnStatus();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		
	}
	

	private void addColumnComapnyProfile() {
		
		companyProfileColumn = new TextColumn<CompanyProfileConfiguration>() {
			@Override
			public String getValue(CompanyProfileConfiguration object) {
				return object.getCompProfileConfig().getName()+" ";
			}
		};
		table.addColumn(companyProfileColumn, "Company Profile");
		companyProfileColumn.setSortable(true);
	}


	private void addColumnLeadCategory() {
		
		leadCategoryColumn = new TextColumn<CompanyProfileConfiguration>() {
			@Override
			public String getValue(CompanyProfileConfiguration object) {
				return object.getLeadCategory()+" ";
			}
		};
		table.addColumn(leadCategoryColumn, "Lead Category");
		leadCategoryColumn.setSortable(true);
	}


	private void addColumnStatus() {
		
		statusColumn = new TextColumn<CompanyProfileConfiguration>() {
			
			@Override
			public String getValue(CompanyProfileConfiguration object) {
				if(object.isStatus())
				return "Active";
				else return "InActive";
			}
		};
		
		table.addColumn(statusColumn,"Status");
		statusColumn.setSortable(true);
	}

	
	








	

	

	

	

	

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
	}


}



