package com.slicktechnologies.client.views.lead;
//import java.io.Console;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.ss.formula.CollaboratingWorkbooksEnvironment;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ProductTaxesTable;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.ProjectCoordinatorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;


// TODO: Auto-generated Javadoc
/**
 * View For Lead.
 * To Do : Pradnya add a Follow Up Activity table in Lead
 */
public class LeadForm extends FormScreen<Lead> implements ClickHandler, ChangeHandler {

	
	final GenricServiceAsync async = GWT.create(GenricService.class);
	//Token to add the varialble declarations
	/** The olbb branch. */
	ObjectListBox<Branch> olbbBranch;
	
	/** The olbc lead priority. */
	ObjectListBox<Config> olbcLeadPriority;
	
	/** The tb lead title. */
	TextBox tbLeadTitle;
	
	/** The tb lead id. */
	TextBox tbLeadId;
	
	/** The etb EMAIL. */
	EmailTextBox etbEmail;
	
	/** The ta lead description. */
	TextArea taLeadDescription;
	
	/** The pic. */
	PersonInfoComposite pic;
	
	/** The olbe sales person. */
	ObjectListBox<Employee> olbeSalesPerson;
	
	/** The olb lead group. */
	ObjectListBox<Config>olbLeadGroup;
	
	/** The olbc lead type. */
	ObjectListBox<Type> olbcLeadType;
	
	/** The olb lead category. */
	ObjectListBox<ConfigCategory> olbLeadCategory;
	
	ObjectListBox<ConfigCategory> olbLeadStatus;
	
	DateBox dbleaddate;
	ProductInfoComposite prodComposite;
	Button addProducts;
	LeadProductTable leadProductTable;
	FormField fgroupingLeadInformation;
	
	
	Button newCustomer;
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	
	// vijay
	ProductTaxesTable prodTaxTable;
	DoubleBox donetpayamt,dototalamt,doamtincltax;
	
	Lead leadObject;
	
	/**
	 * rohan added this flag for checking process configuration
	 */
	
		boolean salesPersonRestrictionFlag= false;
	
	/**
	 * ends here 
	 */
	
	
	/**
	 * Rohan added created By field for NBHC to know who is going to create/ generate interaction
	 * date : 03/11/2016
	 */
		TextBox tbCreatedBy;
	
	/**
	 * ends here 
	 */
		
		/*
		 * Date:17-12-2018
		 * @author Ashwini
		 * Des:To add customer type as a segment for NBHC
		 */
		
		TextBox tbSegment;
		
		
		/*
		 * end by Ashwini
		 */
		
	/**
	 * Date 1-07-2017 added by vijay for Tax validation
	 */
	Date date = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss").parse("2017-07-01 00:00:00");
	final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");

	/**
	 * ends here
	 */
	
	/** Date 29-08-2017 added by vijay for discount on total and round off **/
	DoubleBox dbDiscountAmt;
	DoubleBox dbFinalTotalAmt;
	TextBox tbroundOffAmt;
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	/** date 23/10/2017 added by komal for project coordinator details **/
	ObjectListBox<ContactPersonIdentification> olbArchitect;
	ObjectListBox<ContactPersonIdentification> olbPMC;
	ObjectListBox<ContactPersonIdentification> olbContractor;
	LeadProjectCoordinatorTable leadProjectCoordinatorTable;
	Button addPCButton;


	/**
	 *  nidhi
	 *  20-11-2017
	 *  upload option for send email to customer id
	 */
	UploadComposite ucUploadTAndCs;
	/**
	 * end
	 */
	/**
	 * nidhi
	 * 1-08-2018
	 * for status reason
	 */
	TextBox tbDocStatusreason ;
	/**
	 * Instantiates a new lead form.
	 */
	/**
	 *  nidhi 
	 *   *:*:*
	 */
	public Customer cust;
	/**Date 25-9-2019 by Amol Changes the Sequence of screen using process config for Orkin Raised by Rahul**/
	boolean mapTypeToLead=false;
	
	/**Date 23-10-2019 by Amol**/
	ObjectListBox<Config> olbProductGroup;
	ObjectListBox<ServiceProduct>olbProductName;
	ArrayList<ServiceProduct> serviceproductList;
	boolean mapCustomerType=false;
	boolean changeProductComposites=false;
	
	/**
	 * @author Anil ,Date : 26-12-2019
	 * Adding follow up time text box
	 */
	TextBox tbFollowUpTime;
	TextBox tbContractId;
	
	
	TextBox tbpaymentgatewayid;
	
	
	ListBox followupmin;
	ListBox followuphours;
	
	
	/**
	 * @author Anil @since 16-03-2021
	 */
	public int productSrNo;
	
	/**
	 * @author Vijay Date :- 19-07-2021
	 * Des :- added Number Range functionality for Payment Gateway functionality
	 */
	public ObjectListBox<Config> olbcNumberRange;
	
	public  LeadForm() {
		super();
		createGui();
		this.tbLeadId.setEnabled(false);
		leadProductTable.connectToLocal();
		prodTaxTable.connectToLocal();
		tbCreatedBy.setEnabled(false);
		
		/** Date 31-08-2017 added by vijay for discount amount change handler ***/
		dbDiscountAmt.addChangeHandler(this);
		tbroundOffAmt.addChangeHandler(this);
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","NewCustomerButtonSetEnable"))
		{
			newCustomer.setEnabled(false);
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
			
			salesPersonRestrictionFlag=true;
			olbeSalesPerson.setValue(LoginPresenter.loggedInUser);
			olbeSalesPerson.setEnabled(false);
		}
		
		
		
		
		
	}

	
	/**
	 * Instantiates a new lead form.
	 *
	 * @param processlevel the processlevel
	 * @param fields the fields
	 * @param formstyle the formstyle
	 */
	public LeadForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
		this.tbLeadId.setEnabled(false);
		tbCreatedBy.setEnabled(false);

	}
	

	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget()
	{
		olbcLeadPriority=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcLeadPriority,Screen.LEADPRIORITY);
		olbbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbBranch);
		olbbBranch.addChangeHandler(this);
		
		tbLeadId=new TextBox();
		tbLeadTitle=new TextBox();
		taLeadDescription=new TextArea();
		olbLeadGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbLeadGroup, Screen.LEADGROUP);
		
		
		olbLeadCategory= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbLeadCategory, Screen.LEADCATEGORY);
		olbLeadCategory.addChangeHandler(this);
		olbcLeadType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbcLeadType, Screen.LEADTYPE);
		
		olbLeadStatus= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveConfig(olbLeadStatus,Screen.LEADSTATUS);
		/**
		 * Date : 04-11-2017 By ANIL
		 * for loading all customer if customer approval process is active
		 * FOR NBHC
		 */
//		pic=AppUtility.customerInfoComposite(new Customer());
		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		pic=new PersonInfoComposite(querry,true,true,true);
		/**
		 * End
		 */
		System.out.println("rohan 1 "+LoginPresenter.loggedInUser);
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","SalesPersonRestriction")&&
				UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
			
			salesPersonRestrictionFlag = true;
			olbeSalesPerson=new ObjectListBox<Employee>();
//			AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
			olbeSalesPerson.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.LEAD, "Sales Person");
			
			Timer t = new Timer() {
				
				@Override
				public void run() {

					makeSalesPersonEnable();
				}

				
			};t.schedule(3000);
		}
		else
		{
			olbeSalesPerson=new ObjectListBox<Employee>();
//			AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
			olbeSalesPerson.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.LEAD, "Sales Person");
		}
		pic.getCustomerId().getHeaderLabel().setText("* Customer ID");
		pic.getCustomerName().getHeaderLabel().setText("* Customer Name");
		pic.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		
//		dbleaddate=new DateBox();
//		dbleaddate.setEnabled(false);
//		dbleaddate.setValue(new Date());
		/** Date 06-09-2017 above old code commented by vijay because tax validations per on date so lead date must be editable **/
//		dbleaddate=new DateBox();
		dbleaddate=new DateBoxWithYearSelector();
		
		tbContractId=new TextBox();
		
		
		
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		if(screenName.toString().trim().equals(AppConstants.SCREENTYPESALESLEAD))
		{
			prodComposite=AppUtility.initiateSalesProductComposite(new SuperProduct());
		}
		if(screenName.toString().trim().equals(AppConstants.SCREENTYPESERVICELEAD))
		{
			prodComposite=AppUtility.initiateServiceProductComposite(new SuperProduct());
		}
		leadProductTable=new LeadProductTable();
		addProducts=new Button("ADD");
		
		/**
		 * Done By : Rohan Bhagde
		 * Date    : 29/11/2016
		 * Reason  : This is required by NBHC (Vaishali mam)
		 */
		
		newCustomer=new Button("New Customer");
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","NewCustomerButtonSetEnableFromLead"))
		{
			newCustomer.setEnabled(false);
		}
	
//		DateTimeFormat dateFormat = DateTimeFormat.getFormat("MM/dd/yyyy");
//	      DateBox dateBox = new DateBox();
//	      dateBox.setFormat(new DateBox.DefaultFormat(dateFormat));

		//vijay
		prodTaxTable=new ProductTaxesTable();
		donetpayamt=new DoubleBox();
		donetpayamt.setEnabled(false);
		
		dototalamt=new DoubleBox();
		dototalamt.setEnabled(false);
				
				
		tbCreatedBy = new TextBox();
		tbCreatedBy.setEnabled(false);
		tbCreatedBy.setValue(UserConfiguration.getUserconfig().getUser().getEmployeeName().trim());		
		
		/** Date 31-08-2017 added by vijay for discount amt round off total amt***/
		dbDiscountAmt = new DoubleBox();
		dbFinalTotalAmt = new DoubleBox();
		dbFinalTotalAmt.setEnabled(false);
		tbroundOffAmt = new TextBox();
		
		/** date 23/10/2017 added by komal for project coordinator details **/
		olbArchitect = new ObjectListBox<ContactPersonIdentification>();
		olbArchitect.addItem("--SELECT--");
		olbPMC = new ObjectListBox<ContactPersonIdentification>();
		olbPMC.addItem("--SELECT--");
	    olbContractor = new ObjectListBox<ContactPersonIdentification>();
	    olbContractor.addItem("--SELECT--");
	    
	    leadProjectCoordinatorTable = new LeadProjectCoordinatorTable();
		addPCButton = new Button("ADD");
				
		/**
		 *  Created By : Nidhi
		 *  Date : 20-11-2017
		 *  for Upload email document for customer
		 */
		ucUploadTAndCs=new UploadComposite();
		/**
		 * End
		 */
		/**
		 * nidhi
		 * 1-08-2018
		 * for status reason
		 */
		tbDocStatusreason = new TextBox();
		tbDocStatusreason.setEnabled(false);
		/*
		 * end.
		 */
		
		/*
		 * Date:17-12-2018
		 * @author Ashwini
		 * Des:17-12-2018
		 */
		
		tbSegment = new TextBox();
		tbSegment.setEnabled(false);
		
		/*
		 * end by Ashwini
		 */
		olbProductGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbProductGroup, Screen.PRODUCTGROUP);
		olbProductGroup.addChangeHandler(this);
		olbProductName=new ObjectListBox<ServiceProduct>();
		AppUtility.initializeServiceProduct(olbProductName);
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","MapCustomerTypeToLeadCategory")){
			mapCustomerType=true;
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","ChangeProductComposite")){
			changeProductComposites=true;
		}
		
		tbFollowUpTime=new TextBox();
		
		
		
		
		
		InlineLabel tohhtime=new InlineLabel("To Time HH");
		add(tohhtime,190,80);
		InlineLabel tocoluntime=new InlineLabel(":");
		add(tocoluntime,260,80);
		InlineLabel tommtime=new InlineLabel("MM");
		add(tommtime,265,80);
		
		
		followuphours=new ListBox();
		add(followuphours,190,100);
		followuphours.setSize("45px", "24px");
		followuphours.insertItem("00", 0);
		followuphours.insertItem("01", 1);
		followuphours.insertItem("02", 2);
		followuphours.insertItem("03", 3);
		followuphours.insertItem("04", 4);
		followuphours.insertItem("05", 5);
		followuphours.insertItem("06", 6);
		followuphours.insertItem("07", 7);
		followuphours.insertItem("08", 8);
		followuphours.insertItem("09", 9);
		followuphours.insertItem("10", 10);
		followuphours.insertItem("11", 11);
		followuphours.insertItem("12", 12);
		followuphours.insertItem("13", 13);
		followuphours.insertItem("14", 14);
		followuphours.insertItem("15", 15);
		followuphours.insertItem("16", 16);
		followuphours.insertItem("17", 17);
		followuphours.insertItem("18", 18);
		followuphours.insertItem("19", 19);
		followuphours.insertItem("20", 20);
		followuphours.insertItem("21", 21);
		followuphours.insertItem("22", 22);
		followuphours.insertItem("23", 23);
		
		/** Ends **/
		InlineLabel tocolon=new InlineLabel(":");
		add(tocolon,260,100);
		
		followupmin=new ListBox();
		add(followupmin,270,100);
		followupmin.setSize("45px", "24px");
		followupmin.insertItem("00", 0);
		followupmin.insertItem("15", 1);
		followupmin.insertItem("30", 2);
		followupmin.insertItem("45", 3);
		
//		hp.add(followuphours);
//		hp.add(followupmin);
		
		tbpaymentgatewayid = new TextBox();
		tbpaymentgatewayid.setEnabled(false);
		
		olbcNumberRange = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange, Screen.NUMBERRANGE);
		
		/*
  		 * Ashwini Patil
  		 * Date:14-06-2024
  		 * Ultima search want to map number range directly from branch so they want this field read only
  		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
  		{	olbcNumberRange.setEnabled(false);
  		}
	}
     
	
	
	private void add(ListBox to_servicemin2, int i, int j) {
		// TODO Auto-generated method stub
		
	}




	private void add(InlineLabel tocolon, int i, int j) {
		// TODO Auto-generated method stub
		
	}




	/**
	 * rohan added this method for setting value to sales person and and make it non editable
	 */
	private void makeSalesPersonEnable() {
	
		olbeSalesPerson.setValue(LoginPresenter.loggedInUser);
		olbeSalesPerson.setEnabled(false);
	}
	
	
	/**
	 * method template to create screen formfields.
	 */
	@Override
	public void createScreen() {


		initalizeWidget();

//		//Token to initialize the processlevel menus.
//		/**
//		 * Date : 06-11-2017 BY ANIL
//		 * Added unsucessfull  button
//		 */
//		/**
//		 *  nidhi
//		 *  20-11-2017
//		 *  All button for customer Email 
//		 */
//		this.processlevelBarNames=new String[]{"Create Quotation","Create Order","New","Send SMS","Quick Order",AppConstants.CANCEL,AppConstants.Unsucessful,AppConstants.LEADCUSTOMERINTRODUCTORY,"Send Company Profile",AppConstants.SIGNUP};

		/**
		 *  28-12-2020 Added by Priyanka
		 *  Des - Change Navigationflow as per requiremnet raised by Nitin Sir 
		 **/
				
		this.processlevelBarNames=new String[]{AppConstants.Unsucessful,AppConstants.CANCEL,"New","Create Quotation","Create Order","Quick Order",AppConstants.VIEWORDER,AppConstants.LEADCUSTOMERINTRODUCTORY,"Send SMS","Send Company Profile",AppConstants.SIGNUP};


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		
		String mainScreenLabel="Lead Details";
		if(leadObject!=null&&leadObject.getCount()!=0){
			mainScreenLabel=leadObject.getCount()+" "+"/"+" "+leadObject.getStatus()+" "+"/"+" "+AppUtility.parseDate(leadObject.getCreationDate());
		}
		
		//fgroupingLeadInformation.setLabel(mainScreenLabel);
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingLeadInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).setKeyField(true).build();
		fbuilder = new FormFieldBuilder("Lead Id",tbLeadId);
		FormField ftbLeadId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Title",tbLeadTitle);
		FormField ftbLeadTitle= fbuilder.setMandatory(true).setMandatoryMsg("Lead Title is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Lead Description (Max 500 characters)",taLeadDescription);
		FormField ftaLeadDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		//FormField fgroupingSalesInformation=fbuilder.setlabel("Sales Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fgroupingSalesInformation=fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Priority",olbcLeadPriority);
		FormField folbcLeadPriority= fbuilder.setMandatory(false)./**setMandatoryMsg("LeadPriority is mandatory!").**/setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Branch",olbbBranch);
		FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(0).build();
		/**@Sheetal:01-02-2022
        Des : Making Sales person non mandatory,requirement by UDS Water**/
		fbuilder = new FormFieldBuilder("Sales Person",olbeSalesPerson);
		FormField folbeSalesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Lead Status",olbLeadStatus);
		FormField folbeLeadStatus= fbuilder.setMandatory(true).setMandatoryMsg("Lead Status is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Type",olbcLeadType);
		FormField folbeLeadType= fbuilder.setMandatory(true).setMandatoryMsg("Lead Type is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		/** date 06.03.2018 added by komal for nbhc **/
		FormField folbeLeadGroup;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","makeGroupMandatory")){
			fbuilder = new FormFieldBuilder("* Group",olbLeadGroup);
			folbeLeadGroup= fbuilder.setMandatory(true).setMandatoryMsg("Lead Group is Mandatory").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder(" Group",olbLeadGroup);
			folbeLeadGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/** 
		 * end komal
		 */
		fbuilder = new FormFieldBuilder("* Category",olbLeadCategory);
		FormField folbeLeadCategory= fbuilder.setMandatory(true).setMandatoryMsg("Lead Category is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",pic);
		FormField fpic= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		/** date 10/10/2017 added by komal for followup date **/
		fbuilder = new FormFieldBuilder("* Follow-up Date", dbleaddate);
		FormField fdbleaddate= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		/** old lead date code **/
//		fbuilder = new FormFieldBuilder("* Lead Date",dbleaddate);
//		FormField fdbleaddate= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingGenralInformation=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fgroupingProductInformation=fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",prodComposite);
		FormField fprodComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("",addProducts);
		FormField faddProducts= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",leadProductTable.getTable());
		FormField fleadProductTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",newCustomer );
		FormField fbtnnewCustomer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingGenralInformation1=fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		/*
		 * Date:17-12-2018
		 * @author Ashwini
		 * Des:To add segment which is mandatory for nbhc
		 */
		
		FormField fbtbSegment;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "CustomerTypeForNbhc")){
		fbuilder = new FormFieldBuilder("*Segment",tbSegment);
		 fbtbSegment= fbuilder.setMandatory(true).setMandatoryMsg("Segment is Mandatory ").setRowSpan(0).setColSpan(0).build();
		}else{
			
			fbuilder = new FormFieldBuilder("Segment",tbSegment);
			 fbtbSegment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/*
		 * End by AShwini
		 */

		//vijay
		
		fbuilder = new FormFieldBuilder("",prodTaxTable.getTable());
		FormField fprodTaxTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupone=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Net Payable",donetpayamt);
		FormField fpayCompNetPay= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Total Amount",dototalamt);
		FormField fpayCompTotalAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Created By", tbCreatedBy);
		FormField ftbCreatedBy = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** Date 31-08-2017 added by vijay for discount amount final total amount and round off ***/
		
		fbuilder = new FormFieldBuilder("Discount Amount", dbDiscountAmt);
		FormField fdbDiscountAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Amount", dbFinalTotalAmt);
		FormField fdbFinalTotalAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Round Off", tbroundOffAmt);
		FormField fdbroundOffAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 23/10/2017 added by komal for project coordinator details **/
		fbuilder = new FormFieldBuilder();
		FormField fgroupingProjectCoordinator = fbuilder.setlabel("Project Coordinator").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Architect" , olbArchitect );
		FormField folbArchitect = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("PMC" , olbPMC );
		FormField folbPMC = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contractor" , olbContractor);
		FormField folbContractor = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",addPCButton );
		FormField fbtnaddPCButton = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("" , leadProjectCoordinatorTable.getTable());
		FormField fleadProjectCoordinatorTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		/**
		 *  28-11-2020 added by PRIYANKA.
		 *  DES -For email.
		 */
		FormField fetbEmail= fbuilder.setMandatory(true).setMandatoryMsg("Email is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Email",etbEmail);
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "makeEmailMandatory")){
			fbuilder = new FormFieldBuilder("* Email",etbEmail);
			fetbEmail= fbuilder.setMandatory(true).setMandatoryMsg("Email is Mandatory").setRowSpan(0).setColSpan(0).build();
			
		}else{
			fbuilder = new FormFieldBuilder("Email",etbEmail);
			fetbEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		
		
		/**
		 * nidhi
		 *  20-11-2017
		 *  send email introductory info to Customer
		 */
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDocuments=fbuilder.setlabel("Documents").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Upload T&Cs",ucUploadTAndCs);
		FormField fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("Product Group",olbProductGroup);
		FormField folbProductGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Product",olbProductName);
		FormField folbProductName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * nidhi
		 * 1-08-208
		 */
		fbuilder = new FormFieldBuilder("Unsuccessful Reason" , tbDocStatusreason);
		FormField ftbdocstatusreson = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 * end
		 */
		 
		fbuilder = new FormFieldBuilder("Follow up time",tbFollowUpTime);
		FormField ftbFollowUpTime= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contract Id",tbContractId);
		FormField ftbContractId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
//		fbuilder = new FormFieldBuilder("Follow up Time(MM)",followupmin);
//		FormField ftbfollowupmin= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
//		fbuilder = new FormFieldBuilder("Follow up Time(HH)",followuphours);
//		FormField ftbfollowuphours= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("Follow up Time(HH)",hp);
//		FormField ftbfollowuphours= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		HorizontalPanel hPanel=new HorizontalPanel();
		InlineLabel colLbl=new InlineLabel();
		colLbl.setText(":");
		hPanel.add(followuphours);
		hPanel.add(colLbl);
		hPanel.add(followupmin);
		
		fbuilder = new FormFieldBuilder("Follow up time",hPanel);
		FormField ftbFollowUpTime1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Payment Gateway id",tbpaymentgatewayid);
		FormField ftbpaymentgatewayid= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * @author Anil @since 05-10-2021
		 * Earlier Number range mandatory section is checked with process name 'NumberRange' and was used throughout the project
		 * if we disable it at one place it got disabled from everywhere
		 * So as discussed with Nitin And Poonam we are creating new process configuration for it
		 * Raised for pecopp by Pooname and Nitin Sir
		 */
		FormField folbcNumberRange;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "PC_MakeNumberRangeMandatory")){
			fbuilder = new FormFieldBuilder("* Number Range",olbcNumberRange);								 	
			folbcNumberRange= fbuilder.setMandatory(true).setMandatoryMsg("Number Range is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Number Range",olbcNumberRange);
			folbcNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/*
		 * Ashwini Patil
		 * Date:29-05-2024
		 * Ultima search want to map number range directly from branch so they want this field read only
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
			olbcNumberRange.setEnabled(false);
		/**@Sheetal : 16-02-2022 
		 *      Des : Hiding number range with below process configuration requirement by pecop**/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","PC_LEAD_HIDE_NUMBER_RANGE")){
			Console.log("Inside hide number range");
			FormField[][] formfield = { 
					   /**LEAD INFO DETAILS**/
					      {fgroupingLeadInformation},
					      {fpic},//fbtnnewCustomer
					      {ftbLeadTitle,folbbBranch,folbeSalesPerson,fdbleaddate,ftbFollowUpTime1},
					      {folbeLeadGroup,folbeLeadCategory,folbeLeadType,folbeLeadStatus,ftbdocstatusreson},
					      
					      /**PRODUCT INFO DETAILS**/		
					      {fgroupingProductInformation},
					      {fprodComposite,faddProducts},
					      {fleadProductTable},
					      {fblankgroup,fblankgroupone,fpayCompTotalAmt},
					      {fblankgroup,fblankgroupone,fdbDiscountAmt},
					      {fblankgroup,fblankgroupone,fdbFinalTotalAmt},
					      {fblankgroup,fprodTaxTable},
					      {fblankgroup,fblankgroupone,fdbroundOffAmt},
					      {fblankgroup,fblankgroupone,fpayCompNetPay},
					      
					      /**CLASSFICATION DETAILS**/		
					      {fgroupingSalesInformation},
					      {folbcLeadPriority,fbtbSegment},
					      
					      
					      /**refernce/genral DETAILS**/
					      {fgroupingGenralInformation},
					      {ftbContractId,ftbCreatedBy,ftbpaymentgatewayid},
					      {ftaLeadDescription},
					      /**Attachment**/
					      {fgroupingGenralInformation1},
					      {fucUploadTAndCs}
					      
					
				};
			   this.fields=formfield;
			
		}/** date 23/10/2017 added by komal for project coordinator tab **/
		else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","LeadProjectCoordinator")){
			
//		FormField[][] formfield = {  
//				{fgroupingLeadInformation},
////			{ftbLeadId,ftbLeadTitle,fdbleaddate,ftbFollowUpTime,ftbfollowupmin,ftbfollowuphours},
//				{ftbLeadId,ftbLeadTitle,fdbleaddate,ftbFollowUpTime1},
//				{ftaLeadDescription},
//				{fgroupingDocuments},
//				{fucUploadTAndCs},
//				{fgroupingSalesInformation},
//				{folbeLeadStatus,folbcLeadPriority,folbbBranch,folbeSalesPerson},
//				{folbeLeadGroup,folbeLeadCategory,folbeLeadType,ftbCreatedBy},
//				{ftbdocstatusreson},// for show lead status reason
//				{fgroupingCustomerInformation},
//				{fbtnnewCustomer},
//				{fpic},
//				{fbtbSegment}, //Added by Ashwini
//				{fgroupingProjectCoordinator},
//				{folbArchitect,folbPMC , folbContractor , fbtnaddPCButton},
//				{fleadProjectCoordinatorTable},
//				
//				{fgroupingProductInformation},
//				
//				{fprodComposite,faddProducts},
//				{fleadProductTable},
//				{fblankgroup,fblankgroupone,fpayCompTotalAmt},
//				{fblankgroup,fblankgroupone,fdbDiscountAmt},
//				{fblankgroup,fblankgroupone,fdbFinalTotalAmt},
//				{fblankgroup,fprodTaxTable},
//				{fblankgroup,fblankgroupone,fdbroundOffAmt},
//				{fblankgroup,fblankgroupone,fpayCompNetPay}
//		};
//
//		this.fields=formfield;
			
			FormField[][] formfield = { 
					   /**LEAD INFO DETAILS**/
					      {fgroupingLeadInformation},
					      {fpic},//fbtnnewCustomer
					      {ftbLeadTitle,folbbBranch,folbeSalesPerson,fdbleaddate,ftbFollowUpTime1},
					      {folbeLeadGroup,folbeLeadCategory,folbeLeadType,folbeLeadStatus,ftbdocstatusreson},
					      {folbcNumberRange},
					      /**PRODUCT INFO DETAILS**/		
					      {fgroupingProductInformation},
					      {fprodComposite,faddProducts},
					      {fleadProductTable},
					      {fblankgroup,fblankgroupone,fpayCompTotalAmt},
					      {fblankgroup,fblankgroupone,fdbDiscountAmt},
					      {fblankgroup,fblankgroupone,fdbFinalTotalAmt},
					      {fblankgroup,fprodTaxTable},
					      {fblankgroup,fblankgroupone,fdbroundOffAmt},
					      {fblankgroup,fblankgroupone,fpayCompNetPay},
					      /**CLASSFICATION DETAILS**/		
					      {fgroupingSalesInformation},
					      {folbcLeadPriority,fbtbSegment},
					      /**refernce/genral DETAILS**/
					      {fgroupingGenralInformation},
					      {ftbContractId,ftbCreatedBy,ftbpaymentgatewayid},
					      {ftaLeadDescription},
					      /**Attachment**/
					      {fgroupingGenralInformation1},
					      {fucUploadTAndCs}
							
			};

			this.fields=formfield;

		
		
		}else if(changeProductComposites&&mapCustomerType){ 
		
////	        Console.log("Inside changeProductComposites and mapCustomerType ");
//	        FormField[][] formfield = {   
//	        		{fgroupingLeadInformation},
////			{ftbLeadId,ftbLeadTitle,fdbleaddate,ftbFollowUpTime,ftbfollowupmin,ftbfollowuphours},
//			{ftbLeadId,ftbLeadTitle,fdbleaddate,ftbFollowUpTime1},
//			{ftaLeadDescription},
//			{fgroupingDocuments},
//			{fucUploadTAndCs},
//			
//			{fgroupingCustomerInformation},
//			{fbtnnewCustomer},
//			{fpic},
//			{fbtbSegment},//Added by Ashwini
//			
//			{fgroupingSalesInformation},
//			{folbeLeadStatus,folbcLeadPriority,folbbBranch,folbeSalesPerson},
//			{folbeLeadGroup,folbeLeadCategory,folbeLeadType,ftbCreatedBy},
//			{ftbdocstatusreson},// for show lead status reason
//			
//			{fgroupingProductInformation},
////			{fprodComposite,faddProducts},
//			{folbProductGroup,folbProductName,faddProducts},
//			{fleadProductTable},
//			{fblankgroup,fblankgroupone,fpayCompTotalAmt},
//			{fblankgroup,fblankgroupone,fdbDiscountAmt},
//			{fblankgroup,fblankgroupone,fdbFinalTotalAmt},
//			{fblankgroup,fprodTaxTable},
//			{fblankgroup,fblankgroupone,fdbroundOffAmt},
//			{fblankgroup,fblankgroupone,fpayCompNetPay}
//	};
			
			// Console.log("Inside changeProductComposites and mapCustomerType ");
			FormField[][] formfield = { 
					   /**LEAD INFO DETAILS**/
					      {fgroupingLeadInformation},
					      {fpic},//fbtnnewCustomer
					      {ftbLeadTitle,folbbBranch,folbeSalesPerson,fdbleaddate,ftbFollowUpTime1},
					      {folbeLeadGroup,folbeLeadCategory,folbeLeadType,folbeLeadStatus,ftbdocstatusreson},
					      {folbcNumberRange},
					      
					      /**PRODUCT INFO DETAILS**/		
					      {fgroupingProductInformation},
					      {fprodComposite,faddProducts},
					      {fleadProductTable},
					      {fblankgroup,fblankgroupone,fpayCompTotalAmt},
					      {fblankgroup,fblankgroupone,fdbDiscountAmt},
					      {fblankgroup,fblankgroupone,fdbFinalTotalAmt},
					      {fblankgroup,fprodTaxTable},
					      {fblankgroup,fblankgroupone,fdbroundOffAmt},
					      {fblankgroup,fblankgroupone,fpayCompNetPay},
					      /**CLASSFICATION DETAILS**/		
					      {fgroupingSalesInformation},
					      {folbcLeadPriority,fbtbSegment},
					      /**refernce/genral DETAILS**/
					      {fgroupingGenralInformation},
					      {ftbContractId,ftbCreatedBy,ftbpaymentgatewayid},
					      {ftaLeadDescription},
					      /**Attachment**/
					      {fgroupingGenralInformation1},
					      {fucUploadTAndCs}
  
							
			};

	this.fields=formfield;
   }else if(changeProductComposites){ 
//	   Console.log("Inside changeProductComposites ");
		
//       FormField[][] formfield = {   {fgroupingLeadInformation},
////		{ftbLeadId,ftbLeadTitle,fdbleaddate,ftbFollowUpTime,ftbfollowupmin,ftbfollowuphours},
//    		   {ftbLeadId,ftbLeadTitle,fdbleaddate,ftbFollowUpTime1},
//		{ftaLeadDescription},
//		{fgroupingDocuments},
//		{fucUploadTAndCs},
//		
//		{fgroupingCustomerInformation},
//		{fbtnnewCustomer},
//		{fpic},
//		{fbtbSegment},//Added by Ashwini
//		
//		{fgroupingSalesInformation},
//		{folbeLeadStatus,folbcLeadPriority,folbbBranch,folbeSalesPerson},
//		{folbeLeadGroup,folbeLeadCategory,folbeLeadType,ftbCreatedBy},
//		{ftbdocstatusreson},// for show lead status reason
//		
//		{fgroupingProductInformation},
////		{fprodComposite,faddProducts},
//		{folbProductGroup,folbProductName,faddProducts},
//		{fleadProductTable},
//		{fblankgroup,fblankgroupone,fpayCompTotalAmt},
//		{fblankgroup,fblankgroupone,fdbDiscountAmt},
//		{fblankgroup,fblankgroupone,fdbFinalTotalAmt},
//		{fblankgroup,fprodTaxTable},
//		{fblankgroup,fblankgroupone,fdbroundOffAmt},
//		{fblankgroup,fblankgroupone,fpayCompNetPay}
//  };
	   
	   FormField[][] formfield = { 
			   /**LEAD INFO DETAILS**/
			      {fgroupingLeadInformation},
			      {fpic},//fbtnnewCustomer
			      {ftbLeadTitle,folbbBranch,folbeSalesPerson,fdbleaddate,ftbFollowUpTime1},
			      {folbeLeadGroup,folbeLeadCategory,folbeLeadType,folbeLeadStatus,ftbdocstatusreson},
			      {folbcNumberRange},

			      /**PRODUCT INFO DETAILS**/		
			      {fgroupingProductInformation},
			      {fprodComposite,faddProducts},
			      {fleadProductTable},
			      {fblankgroup,fblankgroupone,fpayCompTotalAmt},
			      {fblankgroup,fblankgroupone,fdbDiscountAmt},
			      {fblankgroup,fblankgroupone,fdbFinalTotalAmt},
			      {fblankgroup,fprodTaxTable},
			      {fblankgroup,fblankgroupone,fdbroundOffAmt},
			      {fblankgroup,fblankgroupone,fpayCompNetPay},
			      
			      /**CLASSFICATION DETAILS**/		
			      {fgroupingSalesInformation},
			      {folbcLeadPriority,fbtbSegment},
			      
			      
			      {fgroupingGenralInformation},
			      {ftbContractId,ftbCreatedBy,ftbpaymentgatewayid},
			      {ftaLeadDescription},
			      /**Attachment**/
			      {fgroupingGenralInformation1},
			      {fucUploadTAndCs}
			      
		};
    this.fields=formfield;
  }
			else if(mapCustomerType){ 
////				 Console.log("Inside mapCustomerType ");
//	
//	        FormField[][] formfield = {   {fgroupingLeadInformation},
////			{ftbLeadId,ftbLeadTitle,fdbleaddate,ftbFollowUpTime,ftbfollowupmin,ftbfollowuphours},
//	        		{ftbLeadId,ftbLeadTitle,fdbleaddate,ftbFollowUpTime1},
//			{ftaLeadDescription},
//			{fgroupingDocuments},
//			{fucUploadTAndCs},
//			
//			{fgroupingCustomerInformation},
//			{fbtnnewCustomer},
//			{fpic},
//			{fbtbSegment},//Added by Ashwini
//			
//			{fgroupingSalesInformation},
//			{folbeLeadStatus,folbcLeadPriority,folbbBranch,folbeSalesPerson},
//			{folbeLeadGroup,folbeLeadCategory,folbeLeadType,ftbCreatedBy},
//			{ftbdocstatusreson},// for show lead status reason
//			
//			{fgroupingProductInformation},
////			{fprodComposite,faddProducts},
//			{folbProductGroup,folbProductName,faddProducts},
//			{fleadProductTable},
//			{fblankgroup,fblankgroupone,fpayCompTotalAmt},
//			{fblankgroup,fblankgroupone,fdbDiscountAmt},
//			{fblankgroup,fblankgroupone,fdbFinalTotalAmt},
//			{fblankgroup,fprodTaxTable},
//			{fblankgroup,fblankgroupone,fdbroundOffAmt},
//			{fblankgroup,fblankgroupone,fpayCompNetPay}
//	};
				FormField[][] formfield = { 
						   /**LEAD INFO DETAILS**/
						      {fgroupingLeadInformation},
						      {fbtnnewCustomer,fpic},
						      {ftbLeadTitle,folbbBranch,folbeSalesPerson,fdbleaddate,ftbFollowUpTime1},
						      {folbeLeadGroup,folbeLeadCategory,folbeLeadType,folbeLeadStatus,ftbdocstatusreson},
						      {folbcNumberRange},

						      /**PRODUCT INFO DETAILS**/		
						      {fgroupingProductInformation},
						      {fprodComposite,faddProducts},
						      {fleadProductTable},
						      {fblankgroup,fblankgroupone,fpayCompTotalAmt},
						      {fblankgroup,fblankgroupone,fdbDiscountAmt},
						      {fblankgroup,fblankgroupone,fdbFinalTotalAmt},
						      {fblankgroup,fprodTaxTable},
						      {fblankgroup,fblankgroupone,fdbroundOffAmt},
						      {fblankgroup,fblankgroupone,fpayCompNetPay},
						      
						      /**CLASSFICATION DETAILS**/		
						      {fgroupingSalesInformation},
						      {folbcLeadPriority,fbtbSegment},
						      /**refernce/genral DETAILS**/
						      {fgroupingGenralInformation},
						      {ftbContractId,ftbCreatedBy,ftbpaymentgatewayid},
						      {ftaLeadDescription},
						      /**Attachment**/
						      {fgroupingGenralInformation1},
						      {fucUploadTAndCs}
					      
								
				};

	this.fields=formfield;
   } else
		{          
////	   Console.log("Inside else ");
//			        FormField[][] formfield = {   {fgroupingLeadInformation},
////					{ftbLeadId,ftbLeadTitle,fdbleaddate,ftbFollowUpTime,ftbfollowupmin,ftbfollowuphours},
//			        		{ftbLeadId,ftbLeadTitle,fdbleaddate,ftbFollowUpTime1},
//					{ftaLeadDescription},
//					{fgroupingDocuments},
//					{fucUploadTAndCs,ftbContractId},
//					{fgroupingSalesInformation},
//					{folbeLeadStatus,folbcLeadPriority,folbbBranch,folbeSalesPerson},
//					{folbeLeadGroup,folbeLeadCategory,folbeLeadType,ftbCreatedBy},
//					{ftbdocstatusreson},// for show lead status reason
//					{fgroupingCustomerInformation},
//					{fbtnnewCustomer},
//					{fpic},
//					{fbtbSegment},  //Added by Ashwini
//					{fgroupingProductInformation},
////					{folbProductGroup},
//					{fprodComposite,faddProducts},
////					{folbProductGroup,folbProductName,faddProducts},
//					{fleadProductTable},
//					{fblankgroup,fblankgroupone,fpayCompTotalAmt},
//					{fblankgroup,fblankgroupone,fdbDiscountAmt},
//					{fblankgroup,fblankgroupone,fdbFinalTotalAmt},
//					{fblankgroup,fprodTaxTable},
//					{fblankgroup,fblankgroupone,fdbroundOffAmt},
//					{fblankgroup,fblankgroupone,fpayCompNetPay}
//			};
//
//			this.fields=formfield;
	   
	   FormField[][] formfield = { 
			   /**LEAD INFO DETAILS**/
			      {fgroupingLeadInformation},
			      {fpic},//fbtnnewCustomer
			      {ftbLeadTitle,folbbBranch,folbeSalesPerson,fdbleaddate,ftbFollowUpTime1},
			      {folbeLeadGroup,folbeLeadCategory,folbeLeadType,folbeLeadStatus,ftbdocstatusreson},
			      {folbcNumberRange},

			      /**PRODUCT INFO DETAILS**/		
			      {fgroupingProductInformation},
			      {fprodComposite,faddProducts},
			      {fleadProductTable},
			      {fblankgroup,fblankgroupone,fpayCompTotalAmt},
			      {fblankgroup,fblankgroupone,fdbDiscountAmt},
			      {fblankgroup,fblankgroupone,fdbFinalTotalAmt},
			      {fblankgroup,fprodTaxTable},
			      {fblankgroup,fblankgroupone,fdbroundOffAmt},
			      {fblankgroup,fblankgroupone,fpayCompNetPay},
			      
			      /**CLASSFICATION DETAILS**/		
			      {fgroupingSalesInformation},
			      {folbcLeadPriority,fbtbSegment},
			      
			      
			      /**refernce/genral DETAILS**/
			      {fgroupingGenralInformation},
			      {ftbContractId,ftbCreatedBy,ftbpaymentgatewayid},
			      {ftaLeadDescription},
			      /**Attachment**/
			      {fgroupingGenralInformation1},
			      {fucUploadTAndCs}
			      
			
		};
	   this.fields=formfield;
	}

}

	/**
	 * method template to update the model with token entity name.
	 *
	 * @param model the model
	 */
	@Override
	public void updateModel(Lead model) 
	{

		if(tbLeadTitle.getValue()!=null)
			model.setTitle(tbLeadTitle.getValue());
		
		if(taLeadDescription.getValue()!=null)
			model.setDescription(taLeadDescription.getValue());
		if(olbcLeadPriority.getValue()!=null)
			model.setPriority(olbcLeadPriority.getValue());
		
		System.out.println("branch value "+olbbBranch.getSelectedIndex());
		System.out.println("branch value "+olbbBranch.getValue(olbbBranch.getSelectedIndex()));
		if(olbbBranch.getValue(olbbBranch.getSelectedIndex())!=null)
			model.setBranch(olbbBranch.getValue(olbbBranch.getSelectedIndex()));
		/**** Date:- 15-10-2018 By Vijay Updated to code for reflecting sales person Name ***/
		if(olbeSalesPerson.getSelectedIndex()!=0)
			model.setEmployee(olbeSalesPerson.getValue());
		if(olbLeadStatus.getValue()!=null)
			model.setStatus(olbLeadStatus.getValue());
		if(olbcLeadType.getValue()!=null)
		model.setType(olbcLeadType.getValue(olbcLeadType.getSelectedIndex()));
		if(olbLeadGroup.getValue()!=null)
			model.setGroup(olbLeadGroup.getValue());
		if(olbLeadCategory.getValue()!=null)
			model.setCategory(olbLeadCategory.getValue());
//		if(dbleaddate.getValue()!=null)
//			model.setCreationDate(dbleaddate.getValue());
		
		/** date 10/10/2017 added by komal for followup date **/
		if (dbleaddate.getValue() != null)
			model.setFollowUpDate(dbleaddate.getValue());
		
		if(pic.getValue()!=null)
			model.setPersonInfo(pic.getValue());
		
		/*
		 * Date:17-12-2018
		 * @author Ashwini
		 * Des:to add segment 
		 */
		
		if (tbSegment.getValue()!=null){
			model.setSegment(tbSegment.getValue());
			System.out.println("segment1245677:::"+model.getSegment());
		}
			
		
		/*
		 * end by Ashwini
		 */

		List<SalesLineItem> saleslis=this.getLeadProductTable().getDataprovider().getList();
		ArrayList<SalesLineItem> arrItems=new ArrayList<SalesLineItem>();
		arrItems.addAll(saleslis);
		model.setLeadProducts(arrItems);
		
		// vijay 
		List<ProductOtherCharges> productTaxLis=this.prodTaxTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> productTaxArr=new ArrayList<ProductOtherCharges>();
		productTaxArr.addAll(productTaxLis);
		model.setProductTaxes(productTaxArr);
		
		/** date 23/10/2017 added by komal for project coordinators **/
		List<ProjectCoordinatorDetails> productCoordinatorLis = this.leadProjectCoordinatorTable.getDataprovider().getList();
		ArrayList<ProjectCoordinatorDetails> productCoordinatorArr=new ArrayList<ProjectCoordinatorDetails>();
		productCoordinatorArr.addAll(productCoordinatorLis);
		model.setProjectCoordinators(productCoordinatorArr);
		
		Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
		String currentScrType=s.toString();
		
		if(currentScrType.equals(AppConstants.SCREENTYPESALESLEAD)){
			model.setLeadScreenType(AppConstants.SALESTYPECUST);
		}
		
		if(currentScrType.equals(AppConstants.SCREENTYPESERVICELEAD)){
			model.setLeadScreenType(AppConstants.SERVICETYPECUST);
		}
		
		if(dototalamt.getValue()!=null)
			model.setTotalAmount(dototalamt.getValue());
		if(donetpayamt.getValue()!=null)
			model.setNetpayable(donetpayamt.getValue());
	

		/** Date 31-08-2017 added by vijay for discount amount final total amount and round off ***/
		if(dbDiscountAmt.getValue()!=null)
			model.setDiscountAmt(dbDiscountAmt.getValue());
		if(dbFinalTotalAmt.getValue() !=null)
			model.setFinalTotalAmt(dbFinalTotalAmt.getValue());
		if(tbroundOffAmt.getValue()!=null && !tbroundOffAmt.getValue().equals(""))
			model.setRoundOffAmt(Double.parseDouble(tbroundOffAmt.getValue()));
		

		/**
		 *   Created By : nidhi
		 * Date : 20-11-2017
		 * for  send email introductory info to Customer
		 */
		model.setDocument(ucUploadTAndCs.getValue());
		/**
		 *  end
		 */
		/**
		 * nidhi
		 * 1-08-2018
		 */
		model.setDocStatusReason(tbDocStatusreason.getValue());
		/**
		 * end
		 */
		
		if(olbProductGroup.getValue()!=null)
		  model.setProductGroup(olbProductGroup.getValue());
		
//		if(tbFollowUpTime.getValue()!=null){
//			String  follomini = getFolloupdate(followupmin,followuphours);
//			model.setFollowUpTime(tbFollowUpTime.getValue());
//		}
//		
//		if(followuphours.getValue(0)!=null && followupmin.getValue(0)!=null){
			
		if(followuphours.getSelectedIndex()!=0 || followupmin.getSelectedIndex()!=0){
			String followptime="";
			if(followuphours.getSelectedIndex()!=0){
				followptime = followuphours.getValue(followuphours.getSelectedIndex());
			}
			if(followupmin.getSelectedIndex()!=0){
				followptime = followptime +":"+followupmin.getValue(followupmin.getSelectedIndex());
			}
			model.setFollowUpTime(followptime);

		}
		if(tbContractId.getValue()!=null){
			model.setContractCount(tbContractId.getValue());
		}
		
		if(tbpaymentgatewayid.getValue()!=null){
			model.setPaymentGatewayUniqueId(tbpaymentgatewayid.getValue());
		}
		
		
		/**date 25-11-2020 by Amol added previous salesperson
		 * 
		 */
		if(leadObject!=null){
			if(!leadObject.getEmployee().equals(model.getEmployee())){
				Console.log("sales person "+leadObject.getEmployee());
				model.setPreviousSalesPerson(leadObject.getEmployee());
			}
		}
		
		/**
		 * @author Vijay Date 30-12-2020
		 * Des :- added to differentiate Sales lead and Service Lead.
		 * For sales its value Sales and for service its value Service
		 */
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		if(model.getId()==null) {
		if(screenName.toString().trim().equals(AppConstants.SCREENTYPESALESLEAD))
		{
			String orderType = "Sales";
			model.setOrderType(orderType);
		}
		if(screenName.toString().trim().equals(AppConstants.SCREENTYPESERVICELEAD))
		{
			String orderType = "Service";
			model.setOrderType(orderType);
		}
		}
		if(olbcNumberRange.getSelectedIndex()!=0){
			model.setNumberRange(olbcNumberRange.getValue());
		}
		else{
			model.setNumberRange("");
		}
		
		leadObject=model;
		presenter.setModel(model);

	}

	private String getFolloupdate(ListBox followupmin2, ListBox followuphours2) {
		String followuptime="";
		String hours ="";
		String  mini ="";
//		logger.log(Level.SEVERE,"Followup :");
		
		if(followuphours2.getSelectedIndex()!=0 ){
			hours= followuphours2.getSelectedIndex() +"";
			}
//		logger.log(Level.SEVERE,"Followup Hours:" +hours);
		
		if(followupmin2.getSelectedIndex()!=0 ){
			mini= followupmin2.getSelectedIndex() +"";
			}
//		logger.log(Level.SEVERE,"Followup mini:" +mini);
		
		followuptime=hours+":"+mini;
//		logger.log(Level.SEVERE,"FollowupTime:" +followuptime);
		return followuptime;
	}
	



	/**
	 * method template to update the view with token entity name.
	 *
	 * @param view the view
	 */
	@Override
	public void updateView(Lead view) 
	{
		leadObject=view;
		tbLeadId.setValue(view.getCount()+"");
		
		this.checkCustomerStatus(view.getPersonInfo().getCount(), view);
		
		if(view.getTitle()!=null)
			tbLeadTitle.setValue(view.getTitle());
		if(view.getDescription()!=null)
			taLeadDescription.setValue(view.getDescription());
		if(view.getPriority()!=null)
			olbcLeadPriority.setValue(view.getPriority());
		if(view.getBranch()!=null)
			olbbBranch.setValue(view.getBranch());
		if(view.getEmployee()!=null)
			olbeSalesPerson.setValue(view.getEmployee());
		
		if(view.getStatus()!=null)
			olbLeadStatus.setValue(view.getStatus());
		
		if(view.getCategory()!=null)
			olbLeadCategory.setValue(view.getCategory());
		//if(view.getType()!=null)
		//olbcLeadType.setValue(view.getType());
		if(view.getGroup()!=null)
			olbLeadGroup.setValue(view.getGroup());
		
		if(view.getPersonInfo()!=null)
			pic.setValue(view.getPersonInfo());
		
		/*
		 * Date:`17-12-2018
		 * @author Ashwini
		 */
		
		if(view.getSegment()!=null)
			tbSegment.setValue(view.getSegment());
		
	    System.out.println(" value of segment is::"+view.getSegment());
		
		/*
		 * end by Ashwini
		 */
		
		/** date 10/10/2017 added by komal for followup date **/
		if (view.getFollowUpDate() != null)
			dbleaddate.setValue(view.getFollowUpDate());
		
//		if(view.getCreationDate()!=null)
//			dbleaddate.setValue(view.getCreationDate());
		if(view.getLeadProducts()!=null){
			leadProductTable.setValue(view.getLeadProducts());
		}
		prodTaxTable.setValue(view.getProductTaxes());
		dototalamt.setValue(view.getTotalAmount());
		donetpayamt.setValue(view.getNetpayable());
		
		if (view.getCreatedBy() != null)
			tbCreatedBy.setValue(view.getCreatedBy());
		
		/** Date 31-08-2017 added by vijay for discount amount final total amount and round off ***/
		dbDiscountAmt.setValue(view.getDiscountAmt());
		dbFinalTotalAmt.setValue(view.getFinalTotalAmt());
		tbroundOffAmt.setValue(view.getRoundOffAmt()+"");
		/** date 23/10/2017 added by komal for project coordinators ***/
		if(view.getProjectCoordinators() != null){
			leadProjectCoordinatorTable.setValue(view.getProjectCoordinators());
		}
		/**
		 *   Created By : nidhi
		 * Date : 20-11-2017
		 * for  send email introductory info to Customer
		 */
		if(view.getDocument()!=null)
			ucUploadTAndCs.setValue(view.getDocument());
		/**
		 *  end
		 */
		/**
		 * nidhi
		 * 1-08-2018
		 */
		tbDocStatusreason.setValue(view.getDocStatusReason());
		/**
		 * end
		 */
		if(view.getProductGroup()!=null)
			olbProductGroup.setValue(view.getProductGroup());
		
//		if(view.getFollowUpTime()!=null){
//			tbFollowUpTime.setValue(view.getFollowUpTime());
//		}
		
		if(view.getFollowUpTime()!=null){
			System.out.println("view.getFollowUpTime() "+view.getFollowUpTime());
//			String arr[]= getfollowuptime(view.getFollowUpTime());
//			String hours = arr[0];
//			String mini = arr[1];
//			System.out.println("hours"+hours);
//			System.out.println("mins"+mini);
//			followuphours.setValue(0,hours);
//			followupmin.setValue(0,mini);
				setfollowupTime(view.getFollowUpTime());
			}
//		
//		if(view.getFollowupmin()!=null)
//			followupmin.setValue(0, view.getFollowupmin());
//		
//		if(view.getFollowuphours()!=null)
//			followuphours.setValue(0, view.getFollowuphours());
		
		
		if(view.getContractCount()!=null){
			tbContractId.setValue(view.getContractCount());
		}
		
		if(view.getPaymentGatewayUniqueId()!=null){
			tbpaymentgatewayid.setValue(view.getPaymentGatewayUniqueId());
		}
		
		if(view.getNumberRange()!=null && !view.getNumberRange().equals("")){
			olbcNumberRange.setValue(view.getNumberRange());
		}
		
		if(presenter != null) {
			presenter.setModel(view);
		}
	}

	
	private void setfollowupTime(String followUpTime) {
            try{
			String[] time=getfollowuptime(followUpTime);
			System.out.println(" 0 "+time[0]);
			System.out.println(" 1 "+time[1]);
			String hrs=time[0];
			String min=time[1];
			
			int count = followuphours.getItemCount();
			System.out.println("Hours =="+hrs);
			for(int i=0;i<count;i++)
			{
				System.out.println("HTRRSSS =="+followuphours.getItemText(i).trim());
				System.out.println(hrs.trim().equals(followuphours.getItemText(i).trim()));
				if(hrs.trim().equals(followuphours.getItemText(i).trim()))
				{
					followuphours.setSelectedIndex(i);
					break;
				}
			}
			
			int count1 = followupmin.getItemCount();
			for(int i=0;i<count1;i++)
			{
				if(min.trim().equals(followupmin.getItemText(i).trim()))
				{
					followupmin.setSelectedIndex(i);
					break;
				}
			}
            }
			 catch (Exception e) {
	    			e.printStackTrace();
	    			//System.out.println(e.getMessage());
	    		
	            }
			
	}


	private String[] getfollowuptime(String followUpTime) {
	 String arr[] =followUpTime.split(":");
	return arr;
}


	public void setNewCustomerDetails(PersonInfo pInfo){
		try {
			
			
			pic.getId().setValue(pInfo.getCount()+"");
			pic.getName().setValue(pInfo.getFullName());
			pic.getTbpocName().setValue(pInfo.getPocName());
			pic.getPhone().setValue(pInfo.getCellNumber()+"");
			System.out.println(" set customer values  "+pInfo.getFullName());
			
			pic.setPersonInfoHashMap(pInfo);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	// Hand written code shift in presenter


	/**
	 * Toggles the app header bar menus as per screen state.
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Anil @since 15-07-2021
			 * for summary screen
			 */
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			/**
			 * Updated By: Viraj
			 * Date: 16-03-2019
			 * Description: To add edit on popup in lead dashboard
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			} else {
				menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			/** Ends **/		
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION) || text.contains(AppConstants.COMMUNICATIONLOG))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			/**
			 * Updated By: Viraj
			 * Date: 16-03-2019
			 * Description: To add edit on popup in lead dashboard
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			} else {
				menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			/** Ends **/
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				/**
				 * Date 20-04-2019 by Vijay
				 * Des :- when we make quotation and contract from lead then lead should not allow to edit
				 * with process config for NBHC CCPM
				 * 
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "EnableFreezeLead")){
					Lead lead = (Lead) presenter.getModel();
					String leadStatus = lead.getStatus();
					if( leadStatus.equals("Quotation Created") || leadStatus.equals("Quotation Sent") || leadStatus.equals(Quotation.QUOTATIONSUCESSFUL)){
						if(text.contains("Email")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION)|| text.contains(AppConstants.COMMUNICATIONLOG) || text.contains(AppConstants.MESSAGE))
							menus[k].setVisible(true); 

						else
							menus[k].setVisible(false);  
					}
					else{
						if(text.contains("Edit")||text.contains("Email")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION)|| text.contains(AppConstants.COMMUNICATIONLOG) || text.contains(AppConstants.MESSAGE))
							menus[k].setVisible(true); 

						else
							menus[k].setVisible(false);  	
					}
				}else{
				//  rohan added email option in lead for PCAMB and all  
					if(text.contains("Edit")||text.contains("Email")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION)|| text.contains(AppConstants.COMMUNICATIONLOG) || text.contains(AppConstants.MESSAGE))
						menus[k].setVisible(true); 

					else
						menus[k].setVisible(false);  	
				}
				
					   
			}
		}
		
		String typeofModule=LoginPresenter.currentModule;
		Screen scr=(Screen) AppMemory.getAppMemory().currentScreen;

		if(AppConstants.SCREENTYPESALESLEAD.equals(scr.toString().trim()))
		{
			AuthorizationHelper.setAsPerAuthorization(Screen.SALESLEAD,typeofModule);
		}
		
		if(AppConstants.SCREENTYPESERVICELEAD.equals(scr.toString().trim()))
		{
			AuthorizationHelper.setAsPerAuthorization(Screen.LEAD,typeofModule);
		}
	}

	/**
	 * sets the id textbox with the passed count value.
	 *
	 * @param count the new count
	 */
	@Override
	public void setCount(int count)
	{
		tbLeadId.setValue(count+"");
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbLeadId.setEnabled(false);
		dbleaddate.setEnabled(state);
		leadProductTable.setEnable(state);
		tbCreatedBy.setEnabled(false);
		
		//Date 31-08-2017 added by vijay for total amount discount and roundoff
		this.dbDiscountAmt.setEnabled(state);
		this.dbFinalTotalAmt.setEnabled(false);
		this.tbroundOffAmt.setEnabled(state);
				
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","NewCustomerButtonSetEnableFromLead"))
		{
			newCustomer.setEnabled(false);
		}
		
		/*
		 * Date:17-12-2018
		 * @author Ashwini
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "CustomerTypeForNbhc")){
			tbSegment.setEnabled(false);
		}
		
		/*
		 * End by Ashwini
		 */
		
		
		/** date 25/10/2017 added by komal **/
		leadProjectCoordinatorTable.setEnable(state);
		
		/**
		 * nidhi
		 * 31-07-2018
		 * for set  quotaion created employee 
		 */
		this.tbCreatedBy.setEnabled(false);
		if(salesPersonRestrictionFlag)
		{
			olbeSalesPerson.setEnabled(false);
		}
		
		followuphours.setEnabled(state);
		followupmin.setEnabled(state);
		
		
		if(tbLeadId.getValue()!=null&&!tbLeadId.getValue().equals("")){
			this.olbcNumberRange.setEnabled(false);
		}
		
		/*
		 * Ashwini Patil
		 * Date:29-05-2024
		 * Ultima search want to map number range directly from branch so they want this field read only
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
			olbcNumberRange.setEnabled(false);
	}
	

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		
		
		/**Date 27-12-2019 by Amol if lead status is successfful and Contract ID is Present 
		 * then all the fields should be Non-Editable raised by Nitin Sir for Bitco Pest
		 */
		if(tbLeadId.getValue()!=null&&!tbLeadId.getValue().equals("")&&olbLeadStatus.getValue().equals(AppConstants.Successful)){
			
			form.setEnabled(false);
		}
		
		
		//  rohan added this code for sales person restriction 
		if(salesPersonRestrictionFlag)
		{
			olbeSalesPerson.setEnabled(false);
		}
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {

	}
	
	
	/********************************************************************************************************/
	
	protected void checkCustomerStatus(int cCount, final Lead View)
	{
		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(cCount);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for(SuperModel model:result)
				{
					Customer custEntity = (Customer)model;
					if(AppConstants.ACTIVE.equals(custEntity.getStatus().trim())){
						 getPic().getId().getElement().addClassName("personactive");
			    		 getPic().getName().getElement().addClassName("personactive");
			    		 getPic().getPhone().getElement().addClassName("personactive");
			    		 getPic().getTbpocName().getElement().addClassName("personactive");
					}
					
					/*
					 * Date:17-12-
					 */
//						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("lead", "CustomerTypeForNbhc")){
					tbSegment.setValue(custEntity.getCategory());
					
					System.out.println( "segment Value::"+tbSegment);
//						}
					/**date 10-10-2019 by Amol Set a customer type to lead category***/
		             olbLeadCategory.setValue(custEntity.getType());
		             
		             /**
		              * @author Abhinav Bihade
		              * @since 27/12/2019
		              * As per Orkin: On Lead page if we create new customer or select any existing customer customer,
		              * then Customer Group,Branch, Sales person should map in Lead Group
		              */
		             olbLeadGroup.setValue(custEntity.getGroup());
//		             olbbBranch.setValue(custEntity.getBranch());
		             
		             if(View!=null){
		            	 olbbBranch.setValue(View.getBranch());
					}else{
						 olbbBranch.setValue(custEntity.getBranch());
					}
		             
//				             olbeSalesPerson.setValue(custEntity.getEmployee());
		            /**
					 * @author Anil , Date : 23-01-2020
					 * In case if we are updating sales person from priora app ,then also it map the sales person from customer
					 */
					if(View!=null){
						 olbeSalesPerson.setValue(View.getEmployee());
					}else{
						 olbeSalesPerson.setValue(custEntity.getEmployee());
					}
		             
		            ConfigCategory cat=olbLeadCategory.getSelectedItem();
					if(cat!=null){
						AppUtility.makeLiveTypeDropDown(olbcLeadType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
					}
						
						

		             /**
		              * @author Abhinav Bihade
		              * @since 04/02/2019
		              * As per Rahul Tiwari's Orkin's Reqiurement,If I save a lead by adding Lead Category and Lead Type,
		              * then after sometime I open the same lead then Lead Type disappear
		              */
					if(View!=null && View.getType()!=null){
						olbcLeadType.setValue(View.getType());
					}
							
							
					/**
					 * nidhi
					 *  10-10-2018
					 */
					cust = custEntity;
				}
				
				if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT || AppMemory.getAppMemory().currentState==ScreeenState.NEW){
					updateTaxesDetails(cust);
				}
				
			}
		 });
	}
	
	
	protected void updateTaxesDetails(Customer cust) {
		List<SalesLineItem> prodlist = leadProductTable.getDataprovider().getList();
		if(prodlist.size()>0){
			Branch branchEntity =olbbBranch.getSelectedItem();
			
			if(cust!=null){
				System.out.println("customer onselection");
				 String customerBillingaddressState = cust.getAdress().getState();
				 List<SalesLineItem> productlist = AppUtility.updateTaxesDetails(prodlist, branchEntity, customerBillingaddressState, true);
				 leadProductTable.getDataprovider().setList(productlist);
				 leadProductTable.getTable().redraw();
				RowCountChangeEvent.fire(leadProductTable.getTable(), leadProductTable.getDataprovider().getList().size(), true);
				leadProductTable.setEnable(true);
			}
		}
	}


	/**************************************Type Drop Down Logic****************************************/

	@Override
	public void onChange(ChangeEvent event) {
		
		if(event.getSource().equals(olbProductGroup)){
			retrieveServiceProduct();
		}
		
		if(event.getSource().equals(olbLeadCategory)){
			
			if(olbLeadCategory.getSelectedIndex()!=0){
				
				ConfigCategory cat=olbLeadCategory.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(olbcLeadType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
		
		/**
	     * Date 31-08-2017 added by vijay for final total amount and net payable after discount
	     */
		
		if(event.getSource().equals(dbDiscountAmt)){
			System.out.println("on change amt ==");

			if(this.getDbDiscountAmt().getValue()==null){
				this.getDbFinalTotalAmt().setValue(this.getDototalamt().getValue());
				
				this.prodTaxTable.connectToLocal();
				this.addProdTaxes();
				
				double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getProdTaxTable().calculateTotalTaxes();

				double netPay = Double.parseDouble(nf.format(totalIncludingTax));
				netPay = Math.round(netPay);
				
				if(!tbroundOffAmt.getValue().equals("") && tbroundOffAmt.getValue()!=null){
					
					String roundOff = tbroundOffAmt.getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					if(roundoffAmt!=0){
						this.getDonetpayamt().setValue(roundoffAmt);
					}else{
						this.getDonetpayamt().setValue(netPay);
					}
					
				}else{
					this.getDonetpayamt().setValue(netPay);
				}
			}
			else if(this.getDbDiscountAmt().getValue() > this.getDototalamt().getValue()){
				showDialogMessage("Discount Amount can not be greater than total amount");
				this.getDbDiscountAmt().setValue(0d);
			}else{
				reactOnDiscountAmtChange();
			}
			
		}
		/**
		 * ends here
		 */
		
		/**
		 * Date 01-09-2017 added by vijay for roundoff and managing net payable
		 */
		if(event.getSource().equals(tbroundOffAmt)){

			double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getProdTaxTable().calculateTotalTaxes();
			double netPay = Double.parseDouble(nf.format(totalIncludingTax));
			netPay = Math.round(netPay);
			
			if(tbroundOffAmt.getValue()==null){
				this.getDonetpayamt().setValue(netPay);
				
			}else{
				if(!tbroundOffAmt.getValue().equals("")&& tbroundOffAmt.getValue()!=null){
					String roundOff = tbroundOffAmt.getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					if(roundoffAmt!=0){
						this.getDonetpayamt().setValue(roundoffAmt);
					}else{
						this.getDonetpayamt().setValue(netPay);
						this.getTbroundOffAmt().setValue("");
					}
				}else{
					this.getDonetpayamt().setValue(netPay);
				}
				
			}
		}
		/**
		 * ends here
		 */
		
		if(event.getSource().equals(olbbBranch)){
			if(olbbBranch.getSelectedIndex()!=0) {
				/*
				 * Ashwini Patil 
				 * Date:13-05-2024 
				 * Added for ultima search. 
				 * If number range is selected on branch then it will automatically get selected on contract screen.
				 */
				
				Branch branchEntity =olbbBranch.getSelectedItem();
				if(branchEntity!=null) {
					Console.log("branch entity not null");
					Console.log("branch "+branchEntity.getBusinessUnitName()+"number range="+branchEntity.getNumberRange());
					if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
						olbcNumberRange.setValue(branchEntity.getNumberRange());
						Console.log("Number range set to drop down by default");
					}else {
						Console.log("in else Number range");
						String range=LoginPresenter.branchWiseNumberRangeMap.get(branchEntity.getBusinessUnitName());
						if(range!=null&&!range.equals("")) {
							olbcNumberRange.setValue(range);
							Console.log("in else Number range set to drop down by default");							
						}else {
							olbcNumberRange.setSelectedIndex(0);
						}
					}
				}else
					Console.log("branch entity null");
			updateTaxesDetails(cust);
			}
		}
	}
	
	private void retrieveServiceProduct() {
       
	
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
//		Console.log("product group query "+olbProductGroup.getSelectedIndex());
		
		if(olbProductGroup.getSelectedIndex()!=0){
//		Console.log("Inside product group query");
		filter = new Filter();
		filter.setQuerryString("productGroup");
		filter.setStringValue(olbProductGroup.getValue(olbProductGroup.getSelectedIndex()));
		filtervec.add(filter);
		}
		//else{
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		//}
		query.setFilters(filtervec);
		query.setQuerryObject(new ServiceProduct());
		
		async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				olbProductName.clear();
				olbProductName.removeAllItems();
				
				olbProductName.addItem("--SELECT--");
				List<ServiceProduct> list = new ArrayList<ServiceProduct>();
				for (SuperModel model : result) { 
					ServiceProduct entity = (ServiceProduct) model;
					if(olbProductGroup.getValue().equals(entity.getProductGroup())){
					  list.add(entity);
					//	olbProductName.addItem(entity.toString());
					//	setListItems();
					//Console.log("Inside set product name");
					  
					}
				}
				olbProductName.setListItems(list);
			}
		});
	
	}


	/**
     * Date 31-08-2017 added by vijay for final total amount and net payable after discount
     */
	
	private void reactOnDiscountAmtChange() {
				showWaitSymbol();
				NumberFormat nf=NumberFormat.getFormat("0.00");
				double discountAmt = this.getDbDiscountAmt().getValue();
				double finalAmt = this.getDototalamt().getValue()-discountAmt;
			    this.getDbFinalTotalAmt().setValue(finalAmt);
			    
				try {
					this.prodTaxTable.connectToLocal();
					this.addProdTaxes();
					double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getProdTaxTable().calculateTotalTaxes();
					double netPay = Double.parseDouble(nf.format(totalIncludingTax));
					netPay = Math.round(netPay);
					if(!tbroundOffAmt.getValue().equals("") && tbroundOffAmt.getValue()!=null){
						
						String roundOff = tbroundOffAmt.getValue();
						double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
						
						if(roundoffAmt!=0){
							this.getDonetpayamt().setValue(roundoffAmt);
						}else{
							this.getDonetpayamt().setValue(netPay);
							this.getTbroundOffAmt().setValue("");
						}
						
					}else{
						this.getDonetpayamt().setValue(netPay);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				this.hideWaitSymbol();
	}

	/**
	 * ends here
	 */
	
	public void addProdTaxes() {

		List<SalesLineItem> salesLineItemLis=this.leadProductTable.getDataprovider().getList();
		List<ProductOtherCharges> taxList=this.prodTaxTable.getDataprovider().getList();
		NumberFormat nf=NumberFormat.getFormat("0.00");
		for(int i=0;i<salesLineItemLis.size();i++)
		{
			double priceqty=0,taxPrice=0;
			
			taxPrice=this.getLeadProductTable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
			System.out.println(" tax valueeeee ==="+taxPrice);
			/**
			 * date 28/10/2017 added by komal to calculate tax according to quantity 
			 */
			priceqty= (salesLineItemLis.get(i).getPrice()-taxPrice)*salesLineItemLis.get(i).getQty();
			/**Date 24-9-2019 by Amol**/
			if(salesLineItemLis.get(i).getPercentageDiscount()!=null||salesLineItemLis.get(i).getDiscountAmt()!=0){
			priceqty= priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
			priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
			}
			//priceqty=salesLineItemLis.get(i).getPrice()-taxPrice;
			priceqty=Double.parseDouble(nf.format(priceqty));
			System.out.println(" product price ==="+priceqty);
			
			
			/**
		     * Date 31-08-2017 added by vijay for if discount amount is there then taxes calculation based on after
		     * discount Total amount
		     */
			if(dbDiscountAmt.getValue()!=null && dbDiscountAmt.getValue()!=0){
//				priceqty = dbFinalTotalAmt.getValue();
				
				/**
				 * Date : 23-09-2017 BY ANIL
				 * 
				 */
				if(isValidForDiscount()){
					priceqty = dbFinalTotalAmt.getValue()/leadProductTable.getDataprovider().getList().size();
					System.out.println("hi vijay inside discount amount "+priceqty);
				}else{
					dbDiscountAmt.setValue(0.0);
					dbFinalTotalAmt.setValue(dototalamt.getValue());
				}
			}
			
			/**
			 * ends here
			 */
			
			if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
				if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals("")){
					System.out.println(" vat GST -"+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
					
				}
				else{
					System.out.println("Vattx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("VAT");
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
					
				}
			}
			
			if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
				if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals("")){
					System.out.println(" service GST - "+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						double assessValue=0;
//						if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
//							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getServiceTax().getPercentage()/100);
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//						}
//						if(salesLineItemLis.get(i).getServiceTax().getPercentage()==0){
//							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//						}
					}
					if(indexValue==-1){
						System.out.println("Index As -1");
						double assessVal=0;
//						if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
//							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getServiceTax().getPercentage()/100);
//							pocentity.setAssessableAmount(assessVal);
//						}
//						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty);
//						}
						
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
					System.out.println("DONE ....");
				}
				else{
					System.out.println("ServiceTx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("Service Tax");
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						double assessValue=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
					}
					if(indexValue==-1){
						System.out.println("Index As -1");
						double assessVal=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessVal);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty);
						}
						
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
					System.out.println("DONE ....");
				}
			}
			
		}
	
	}
	
	public int checkTaxPercent(double taxValue,String taxName)
	{
		List<ProductOtherCharges> taxesList=this.prodTaxTable.getDataprovider().getList();
		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("Service")){
				
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					System.out.println("zero");
					return i;
				}
			}
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					System.out.println("zero");
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("CGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("SGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("IGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
			}
		}
		return -1;
	}
	
	/*****************************************Getters And Setters*****************************************/

	/**
	 * Gets the olbb branch.
	 *
	 * @return the olbb branch
	 */
	public ObjectListBox<Branch> getOlbbBranch() {
		return olbbBranch;
	}

	/**
	 * Sets the olbb branch.
	 *
	 * @param olbbBranch the new olbb branch
	 */
	public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
		this.olbbBranch = olbbBranch;
	}

	/**
	 * Gets the olbc lead priority.
	 *
	 * @return the olbc lead priority
	 */
	public ObjectListBox<Config> getOlbcLeadPriority() {
		return olbcLeadPriority;
	}

	/**
	 * Sets the olbc lead priority.
	 *
	 * @param olbcLeadPriority the new olbc lead priority
	 */
	public void setOlbcLeadPriority(ObjectListBox<Config> olbcLeadPriority) {
		this.olbcLeadPriority = olbcLeadPriority;
	}

	/**
	 * Gets the tb lead id.
	 *
	 * @return the tb lead id
	 */
	public TextBox getTbLeadId() {
		return tbLeadId;
	}

	/**
	 * Sets the tb lead id.
	 *
	 * @param tbLeadId the new tb lead id
	 */
	public void setTbLeadId(TextBox tbLeadId) {
		this.tbLeadId = tbLeadId;
	}

	/**
	 * Gets the tb lead title.
	 *
	 * @return the tb lead title
	 */
	public TextBox getTbLeadTitle() {
		return tbLeadTitle;
	}

	/**
	 * Sets the tb lead title.
	 *
	 * @param tbLeadTitle the new tb lead title
	 */
	public void setTbLeadTitle(TextBox tbLeadTitle) {
		this.tbLeadTitle = tbLeadTitle;
	}

	/**
	 * Gets the ta lead description.
	 *
	 * @return the ta lead description
	 */
	public TextArea getTaLeadDescription() {
		return taLeadDescription;
	}

	/**
	 * Sets the ta lead description.
	 *
	 * @param taLeadDescription the new ta lead description
	 */
	public void setTaLeadDescription(TextArea taLeadDescription) {
		this.taLeadDescription = taLeadDescription;
	}

	/**
	 * Gets the pic.
	 *
	 * @return the pic
	 */
	public PersonInfoComposite getPic() {
		return pic;
	}

	/**
	 * Sets the pic.
	 *
	 * @param pic the new pic
	 */
	public void setPic(PersonInfoComposite pic) {
		this.pic = pic;
	}

	/**
	 * Gets the olbe sales person.
	 *
	 * @return the olbe sales person
	 */
	public ObjectListBox<Employee> getOlbeSalesPerson() {
		return olbeSalesPerson;
	}

	/**
	 * Sets the olbe sales person.
	 *
	 * @param olbeSalesPerson the new olbe sales person
	 */
	public void setOlbeSalesPerson(ObjectListBox<Employee> olbeSalesPerson) {
		this.olbeSalesPerson = olbeSalesPerson;
	}

	public ObjectListBox<Config> getOlbLeadGroup() {
		return olbLeadGroup;
	}

	public void setOlbLeadGroup(ObjectListBox<Config> olbLeadGroup) {
		this.olbLeadGroup = olbLeadGroup;
	}

	public ObjectListBox<Type> getOlbcLeadType() {
		return olbcLeadType;
	}

	public void setOlbcLeadType(ObjectListBox<Type> olbcLeadType) {
		this.olbcLeadType = olbcLeadType;
	}

	public ObjectListBox<ConfigCategory> getOlbLeadCategory() {
		return olbLeadCategory;
	}

	public void setOlbLeadCategory(ObjectListBox<ConfigCategory> olbLeadCategory) {
		this.olbLeadCategory = olbLeadCategory;
	}

	public ObjectListBox<ConfigCategory> getOlbLeadStatus() {
		return olbLeadStatus;
	}

	public void setOlbLeadStatus(ObjectListBox<ConfigCategory> olbLeadStatus) {
		this.olbLeadStatus = olbLeadStatus;
	}

	public ProductInfoComposite getProdComposite() {
		return prodComposite;
	}

	public void setProdComposite(ProductInfoComposite prodComposite) {
		this.prodComposite = prodComposite;
	}

	public Button getAddProducts() {
		return addProducts;
	}

	public void setAddProducts(Button addProducts) {
		this.addProducts = addProducts;
	}

	public LeadProductTable getLeadProductTable() {
		return leadProductTable;
	}

	public void setLeadProductTable(LeadProductTable leadProductTable) {
		this.leadProductTable = leadProductTable;
	}

	public Button getNewCustomer() {
		return newCustomer;
	}

	public void setNewCustomer(Button newCustomer) {
		this.newCustomer = newCustomer;
	}
	
	
	
	
	
	
	@Override
	public void setToViewState() {
		super.setToViewState();
	
		/**
		 * Date 20-04-2019 by Vijay
		 * Des :- when we make quotation from lead then lead should not allow to edit
		 * with process config for NBHC CCPM
		 * 
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "EnableFreezeLead")){
			 Lead lead  = (Lead) presenter.getModel();
			 if(lead.getStatus().equals("Quotation Created")|| lead.getStatus().equals("Quotation Sent") || lead.getStatus().equals(Quotation.QUOTATIONSUCESSFUL)){
				 this.getProcessLevelBar().setVisibleFalse(false);
			}
		}
		/**
		 * ends here
		 */
			
		String typeofModule=LoginPresenter.currentModule;
		Screen scr=(Screen) AppMemory.getAppMemory().currentScreen;

		if(AppConstants.SCREENTYPESALESLEAD.equals(scr.toString().trim()))
		{
			SuperModel model=new Lead();
			model=leadObject;
			AppUtility.addDocumentToHistoryTable(AppConstants.SALESMODULE,AppConstants.LEAD, leadObject.getCount(),Integer.parseInt(pic.getId().getValue()),pic.getName().getValue(),Long.parseLong(pic.getPhone().getValue()), false, model, null);
		}
		
		if(AppConstants.SCREENTYPESERVICELEAD.equals(scr.toString().trim()))
		{
			SuperModel model=new Lead();
			model=leadObject;
			AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.LEAD, leadObject.getCount(),Integer.parseInt(pic.getId().getValue()),pic.getName().getValue(),Long.parseLong(pic.getPhone().getValue()), false, model, null);
		}
		
		
		/**
		 * Added by Priyanka - 27-11-2020
		 * 
		 */

		String mainScreenLabel="Lead Details";
		if(leadObject!=null&&leadObject.getCount()!=0){
			mainScreenLabel=leadObject.getCount()+" "+"/"+" "+leadObject.getStatus()+" "+ "/"+" "+AppUtility.parseDate(leadObject.getCreationDate());
		}
		
//		fgroupingCustomerInformation.setLabel(mainScreenLabel);
		fgroupingLeadInformation.getHeaderLabel().setText(mainScreenLabel);
		
		/**
		 *  end
		 * 
		 */
		
		//Ashwini Patil Date:20-09-2022
		if(isHideNavigationButtons()) {
			Console.log("in changeProcessLevel isHideNavigationButtons");
			for(int i=0;i<getProcesslevelBarNames().length;i++)
			{
				InlineLabel label=getProcessLevelBar().btnLabels[i];
				String text=label.getText().trim();			
				
				if(text.equalsIgnoreCase("New")||text.equalsIgnoreCase("Create Quotation")||text.equalsIgnoreCase("Create Order")||text.equalsIgnoreCase("Quick Order")||text.equalsIgnoreCase(AppConstants.VIEWORDER))
					label.setVisible(false);					
			}
		
		}
	}
    
	
	/*
	 * Date:17-12-2018
	 * @author Ashwini
	 */
	public TextBox getTbSegment() {
		return tbSegment;
	}


	public void setTbSegment(TextBox tbSegment) {
		this.tbSegment = tbSegment;
	}
   
	/*
	 * end by Ashwini
	 */

	public ProductTaxesTable getProdTaxTable() {
		return prodTaxTable;
	}

	public void setProdTaxTable(ProductTaxesTable prodTaxTable) {
		this.prodTaxTable = prodTaxTable;
	}

	public DoubleBox getDonetpayamt() {
		return donetpayamt;
	}

	public void setDonetpayamt(DoubleBox donetpayamt) {
		this.donetpayamt = donetpayamt;
	}

	public DoubleBox getDototalamt() {
		return dototalamt;
	}

	public void setDototalamt(DoubleBox dototalamt) {
		this.dototalamt = dototalamt;
	}

	public DoubleBox getDoamtincltax() {
		return doamtincltax;
	}

	public void setDoamtincltax(DoubleBox doamtincltax) {
		this.doamtincltax = doamtincltax;
	}


	public boolean isSalesPersonRestrictionFlag() {
		return salesPersonRestrictionFlag;
	}


	public void setSalesPersonRestrictionFlag(boolean salesPersonRestrictionFlag) {
		this.salesPersonRestrictionFlag = salesPersonRestrictionFlag;
	}

	@Override
	public boolean validate() {
		/**
		 * Date : 26-08-2017 By ANIL
		 */
		boolean superValidate = super.validate();
		 /**
		 * Date 03-07-2017 added by vijay tax validation
		 */
		List<ProductOtherCharges> prodtaxlist = this.prodTaxTable.getDataprovider().getList();
		for(int i=0;i<prodtaxlist.size();i++){
			
			if(dbleaddate.getValue()!=null && dbleaddate.getValue().before(date)){
				if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CGST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("SGST")
						|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("IGST")){
					showDialogMessage("As Lead date "+fmt.format(dbleaddate.getValue())+" GST Tax is not applicable");
					return false;
				}
			}
			/**
			 * @author Anil @since 21-07-2021
			 * Issue raised by Rahul for Innovative Pest(Thailand) 
			 * Vat is applicable there so system should not through validation for VAT
			 */
//			else if(dbleaddate.getValue()!=null && dbleaddate.getValue().equals(date) || dbleaddate.getValue().after(date)){
//				if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("Service Tax")
//						|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("VAT")){
//					showDialogMessage("As Lead date "+fmt.format(dbleaddate.getValue())+" VAT/CST/Service Tax is not applicable");
//					return false;
//				}
//			}
		}
		/**
		 * ends here
		 */
		
		return true&&superValidate;
		
	}

	public DoubleBox getDbDiscountAmt() {
		return dbDiscountAmt;
	}


	public void setDbDiscountAmt(DoubleBox dbDiscountAmt) {
		this.dbDiscountAmt = dbDiscountAmt;
	}


	public DoubleBox getDbFinalTotalAmt() {
		return dbFinalTotalAmt;
	}


	public void setDbFinalTotalAmt(DoubleBox dbFinalTotalAmt) {
		this.dbFinalTotalAmt = dbFinalTotalAmt;
	}


	public TextBox getTbroundOffAmt() {
		return tbroundOffAmt;
	}


	public void setTbroundOffAmt(TextBox tbroundOffAmt) {
		this.tbroundOffAmt = tbroundOffAmt;
	}
	
	/**
	 * Date :23-09-2017 BY ANIL
	 * This method checks whether we should apply discount or not
	 * if taxes applied on all products then we can give discount
	 */
	public boolean isValidForDiscount(){
		String tax1="",tax2="";
		if(leadProductTable.getDataprovider().getList().size()==0){
			return false;
		}
		tax1=leadProductTable.getDataprovider().getList().get(0).getVatTax().getTaxConfigName().trim();
		tax2=leadProductTable.getDataprovider().getList().get(0).getServiceTax().getTaxConfigName().trim();
		for(SalesLineItem obj:leadProductTable.getDataprovider().getList()){
			if(!obj.getVatTax().getTaxConfigName().trim().equals(tax1)
					||!obj.getServiceTax().getTaxConfigName().trim().equals(tax2)){
				showDialogMessage("Discount is applicable only if same taxes are applied to all products.");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * End
	 */
	
	 /** date 24/10/2017 added by komal for project coordinator table on lead form **/
		public LeadProjectCoordinatorTable getLeadProjectCoordinatorTable() {
			return leadProjectCoordinatorTable;
		}


		public void setLeadProjectCoordinatorTable(
				LeadProjectCoordinatorTable leadProjectCoordinatorTable) {
			    this.leadProjectCoordinatorTable = leadProjectCoordinatorTable;
		}

		/** date 24/10/2017 added by komal for project coordinator add button on lead form **/

		public Button getAddPCButton() {
			return addPCButton;
		}


		public void setAddPCButton(Button addPCButton) {
			this.addPCButton = addPCButton;
		}


		public ObjectListBox<ContactPersonIdentification> getOlbArchitect() {
			return olbArchitect;
		}


		public void setOlbArchitect(
				ObjectListBox<ContactPersonIdentification> olbArchitect) {
			this.olbArchitect = olbArchitect;
		}


		public ObjectListBox<ContactPersonIdentification> getOlbPMC() {
			return olbPMC;
		}


		public void setOlbPMC(ObjectListBox<ContactPersonIdentification> olbPMC) {
			this.olbPMC = olbPMC;
		}


		public ObjectListBox<ContactPersonIdentification> getOlbContractor() {
			return olbContractor;
		}


		public void setOlbContractor(
				ObjectListBox<ContactPersonIdentification> olbContractor) {
			this.olbContractor = olbContractor;
		}


		public DateBox getDbleaddate() {
			return dbleaddate;
		}


		public void setDbleaddate(DateBox dbleaddate) {
			this.dbleaddate = dbleaddate;
		}


		public TextBox getTbCreatedBy() {
			return tbCreatedBy;
		}


		public void setTbCreatedBy(TextBox tbCreatedBy) {
			this.tbCreatedBy = tbCreatedBy;
		}


		@Override
		public void refreshTableData() {
			// TODO Auto-generated method stub
			super.refreshTableData();
			prodTaxTable.getTable().redraw();
			leadProductTable.getTable().redraw();
			leadProjectCoordinatorTable.getTable().redraw();
		}


		public TextBox getTbpaymentgatewayid() {
			return tbpaymentgatewayid;
		}


		public void setTbpaymentgatewayid(TextBox tbpaymentgatewayid) {
			this.tbpaymentgatewayid = tbpaymentgatewayid;
		}


		public ListBox getFollowuphours() {
			return followuphours;
		}


		public void setFollowuphours(ListBox followuphours) {
			this.followuphours = followuphours;
		}


		public ListBox getFollowupmin() {
			return followupmin;
		}


		public void setFollowupmin(ListBox followupmin) {
			this.followupmin = followupmin;
		}
		
		public void findMaxServiceSrNumber() {
			int maxSrNumber = 0 ;
			System.out.println("rohan in side max service number");
			
			List<SalesLineItem> list = this.leadProductTable.getDataprovider().getList();
			if(list != null && list.size() > 0){
			System.out.println("mazza list size "+list.size());
			
			Comparator<SalesLineItem> compare = new  Comparator<SalesLineItem>() {
				
				@Override
				public int compare(SalesLineItem o1, SalesLineItem o2) {
					return Integer.compare(o2.getProductSrNo(), o1.getProductSrNo());
				}
			};
			Collections.sort(list,compare);
			
				System.out.println("product sr number "+list.get(0).getProductSrNo());
				maxSrNumber = list.get(0).getProductSrNo();
				productSrNo = maxSrNumber;
			}
		}
		
}
