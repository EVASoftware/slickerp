package com.slicktechnologies.client.views.lead;

import java.util.Vector;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.lead.LeadPresenter.LeadPresenterSearch;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class LeadPresenterSearchProxy extends LeadPresenterSearch {
	public PersonInfoComposite personInfo;
	public ObjectListBox<Config> olbConfigLeadPriority;
	public IntegerBox tbLeadId;
	public ObjectListBox<Config>olbLeadGroup;
	public ObjectListBox<ConfigCategory>olbLeadCategory;
	public ObjectListBox<Type> olbcLeadType;
	public ObjectListBox<Config>olbLeadStatus;
	public ObjectListBox<Branch> olbBranch;
	public static ObjectListBox<Employee> olbEmployee;
	
	/**
	 * rohan added created by 
	 */
	
		public ObjectListBox<Employee> olbCreatedBy;
	
	/**
	 * 
	 */
	
	public DateComparator dateComparator;
	/**
	 * date 11/10/2017 added by komal for followup date 
	 */
	public DateComparator dateComparatorFollowUp;
	
	TextBox tbpaymentgatewayid;

	
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("personInfo"))
			return this.personInfo;
		if(varName.equals("olbConfigLeadPriority"))
			return this.olbConfigLeadPriority;
		if(varName.equals("tbLeadId"))
			return this.tbLeadId;
		if(varName.equals("olbcLeadType"))
			return this.olbcLeadType;
		if(varName.equals("olbBranch"))
			return this.olbBranch;
		if(varName.equals("olbEmployee"))
			return this.olbEmployee;
		if(varName.equals("dateComparator"))
			return this.dateComparator;
		/**
		 * date 11/10/2017 added by komal for followup date 
		 */
		if(varName.equals("dateComparatorFollowUp"))
			return this.dateComparatorFollowUp;
		return null ;
	}
	
	public LeadPresenterSearchProxy()
	{
		super();
		createGui();
		System.out.println("in side sdjklamasdsfdds");
	}
	public void initWidget()
	{
		System.out.println("inside initialize methods");
		
		/**
		 * Date : 04-11-2017 By ANIL
		 * for loading all customer if customer approval process is active
		 * FOR NBHC
		 */
		
//		MyQuerry querry=new MyQuerry();
//		querry.setQuerryObject(new Customer());
//		personInfo=new PersonInfoComposite(querry,false);
		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false,true,false);
		/**
		 * End
		 */
		
		
		olbConfigLeadPriority= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbConfigLeadPriority,Screen.LEADPRIORITY);
		olbLeadStatus=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbLeadStatus,Screen.LEADSTATUS);
		tbLeadId= new IntegerBox();
		olbLeadGroup = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbLeadGroup,Screen.LEADGROUP);
		olbLeadCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbLeadCategory, Screen.LEADCATEGORY);
		
		olbcLeadType= new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbcLeadType, Screen.QUOTATIONTYPE);
//		CategoryTypeFactory.initiateOneManyFunctionality(olbLeadCategory,olbcLeadType);
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
//		if(ProjectPresenter.checkForProcessConfigurartionIsActiveOrNot("Lead","SalesPersonRestriction"))
//		{
//			System.out.println("inside process config true");
//			olbEmployee=new ObjectListBox<Employee>();
//			AppUtility.makeSalesPersonListBoxLive(olbEmployee);
//		
//			Timer t = new Timer() {
//			
//			@Override
//			public void run() {
//
//				makeSalesPersonEnable();
//			}
//
//			
//		};t.schedule(4000);
//	}
//	else
//	{
		olbEmployee= new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		
		/**
		 * Date 19-01-2019 By Vijay
		 * Des :- For Zonal Head Login in search pop who employee are reporting to him they all are show in Sales person
		 * Drop Down including all branches
		 * Requirement :- NBHC CCPM
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "EnableSalesPersonAsPerReportingTo")
				&& LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Zonal Head")){
				olbEmployee.makeEmployeeLiveAsperReportingTo(LoginPresenter.loggedInUser);
		}else{
			olbEmployee.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.LEAD, "Sales Person");
		}
		
//	}
		
		olbCreatedBy= new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbCreatedBy);
		
		
		dateComparator= new DateComparator(new Lead());

		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");

		AppUtility.makeTypeListBoxLive(olbcLeadType,Screen.LEADTYPE);
		
		/**
		 * date 11/10/2017 added by komal for followup date 
		 */
		dateComparatorFollowUp= new DateComparator("followUpDate", new Lead());
		
		tbpaymentgatewayid = new TextBox();
	}
	
	
	/**
	 * rohan added this method for setting value to sales person and and make it non editable
	 */
	
	
	public static void makeSalesPersonEnable() {
	
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
			{
				System.out.println("login user"+LoginPresenter.loggedInUser);
				olbEmployee.setValue(LoginPresenter.loggedInUser);
				olbEmployee.setEnabled(false);
			}
	}
	
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		
		/**
		 * Date 12-01-2018 By Vijay 
		 * differentiated date combination search and other filter search
		 */
		builder = new FormFieldBuilder();
		FormField fgroupingLeadDateFilterInformation=builder.setlabel("Date Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		builder = new FormFieldBuilder();
		FormField fgroupingLeadOtherFilterInformation=builder.setlabel("Other Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		/**
		 * ends here
		 */
		
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		builder = new FormFieldBuilder("Lead Priority",olbConfigLeadPriority);
		FormField folbConfigLeadPriority= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Lead Id",tbLeadId);
		FormField ftbLeadId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Lead Type",olbcLeadType);
		FormField folbcLeadType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Sales Person",olbEmployee);
		FormField folbEmployee= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date",dateComparator.getFromDate());
		FormField ffromDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date",dateComparator.getToDate());
		FormField ffToDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Lead Group",olbLeadGroup);
		FormField fleadgroup= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Lead Category",olbLeadCategory);
		FormField fleadcategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Lead Status",olbLeadStatus);
		FormField fleadstatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		
		builder = new FormFieldBuilder("Created By",olbCreatedBy);
		FormField folbCreatedBy= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * added by komal for followup date
		 */
		builder = new FormFieldBuilder("From Date(Follow-up)",dateComparatorFollowUp.getFromDate());
		FormField followupfromDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date(Follow-up)",dateComparatorFollowUp.getToDate());
		FormField followupToDate= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Payment Gateway Id",tbpaymentgatewayid);
		FormField ftbpaymentgatewayid= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				/** Date 12-01-2018 Date combination Search grouping **/
				{fgroupingLeadDateFilterInformation},
				{ffromDate,ffToDate,followupfromDate,followupToDate, },
				{folbBranch,fleadstatus,folbEmployee,fleadgroup},
				/** Date 12-01-2018 Other Search filter group ***/
				{fgroupingLeadOtherFilterInformation},
				{fleadcategory,folbcLeadType,ftbLeadId,folbConfigLeadPriority },
				{folbCreatedBy,ftbpaymentgatewayid},
				{fpersonInfo},
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		if(olbcLeadType.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbcLeadType.getValue().trim());
			temp.setQuerryString("type");
			filtervec.add(temp);
		}
		if(olbEmployee.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbEmployee.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		
		
		//   rohan added this for branch level restriction
		if(LoginPresenter.branchRestrictionFlag==true && olbBranch.getSelectedIndex()==0)
		{
//			/**
//			 * Date 19-01-2019 By Vijay
//			 * Des :- added if condition if branch value is nothing then should not add filter of branch
//			 * NBHC CCPM when zonal head login then for sales person filter not required to branch restriction
//			 * so if branch selected then add filter
//			 */
//			if(!olbBranch.getValue(olbBranch.getSelectedIndex()).trim().equals("--SELECT--")){
//				temp=new Filter();
//				temp.setStringValue(olbBranch.getValue(olbBranch.getSelectedIndex()).trim());
//				temp.setQuerryString("branch");
//				filtervec.add(temp);
//			}
				temp=new Filter();
				temp.setList(AppUtility.getbranchlist());
				temp.setQuerryString("branch IN");
				filtervec.add(temp);
			
		}
		else
		{
			if(olbBranch.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbBranch.getValue().trim());
				temp.setQuerryString("branch");
				filtervec.add(temp);
			}
		}
		
		
	
		if(olbConfigLeadPriority.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbConfigLeadPriority.getValue().trim());
			temp.setQuerryString("priority");
			filtervec.add(temp);
		}


		if(personInfo.getIdValue()!=-1)
		{
			temp=new Filter();
			temp.setIntValue(personInfo.getIdValue());
			temp.setQuerryString("personInfo.count");
			filtervec.add(temp);
		}

		if(!(personInfo.getFullNameValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(personInfo.getFullNameValue());
			temp.setQuerryString("personInfo.fullName");
			filtervec.add(temp);
		}
		if(personInfo.getCellValue()!=-1l)
		{
			temp=new Filter();
			temp.setLongValue(personInfo.getCellValue());
			temp.setQuerryString("personInfo.cellNumber");
			filtervec.add(temp);
		}
		if(tbLeadId.getValue()!=null){
			temp=new Filter();temp.setIntValue(tbLeadId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}

		if(olbLeadGroup.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbLeadGroup.getValue().trim());
			temp.setQuerryString("group");
			filtervec.add(temp);
		}

		if(olbLeadCategory.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbLeadCategory.getValue().trim());
			temp.setQuerryString("category");
			filtervec.add(temp);
		}

		if(olbcLeadType.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbcLeadType.getValue().trim());
			temp.setQuerryString("type");
			filtervec.add(temp);
		}

		if(olbLeadStatus.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbLeadStatus.getValue());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		
		
		if(olbCreatedBy.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbCreatedBy.getValue());
			temp.setQuerryString("createdBy");
			filtervec.add(temp);
		}
		
//		Screen scrType=(Screen)AppMemory.getAppMemory().currentScreen;
//		String screenType=scrType.toString();
//		
//		if(screenType.equals(AppConstants.SCREENTYPESALESLEAD))
//		{
//			temp=new Filter();
//			temp.setQuerryString("leadScreenType");
//			temp.setStringValue(AppConstants.SALESTYPECUST);
//			filtervec.add(temp);
//		}
//		
//		if(screenType.equals(AppConstants.SCREENTYPESERVICELEAD))
//		{
//			temp=new Filter();
//			temp.setQuerryString("leadScreenType");
//			temp.setStringValue(AppConstants.SERVICETYPECUST);
//			filtervec.add(temp);
//		}
//		

		/**
		 * added by komal for followup date
		 */
		if(dateComparatorFollowUp.getValue()!=null)
		{
			filtervec.addAll(dateComparatorFollowUp.getValue());
		}
		
		if(tbpaymentgatewayid.getValue()!=null && !tbpaymentgatewayid.getValue().equals("")){
			temp=new Filter();
			temp.setStringValue(tbpaymentgatewayid.getValue());
			temp.setQuerryString("paymentGatewayUniqueId");
			filtervec.add(temp);
		}
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Lead());
		return querry;
	}
	@Override
	public boolean validate() {
		
		/**
		 * @author Vijay Date :- 08-12-2020
		 * Des :- below code commented becuase as per nitin sir this logic is not required
		 * we updated new logic whatever branches assigned to Employee in that data will search with Branch IN query.
		 */
//		if(LoginPresenter.branchRestrictionFlag)
//		{
//			/**
//			 * Date 19-01-2019 By Vijay
//			 * Des :- NBHC CCPM when zonal head login then if he selected sales person name in the search filter
//			 * then branch restriction not required 
//			 */
//			boolean flag = true;
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "EnableSalesPersonAsPerReportingTo")
//					&& LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Zonal Head")){
//				if(olbEmployee.getSelectedIndex()!=0){
//					flag = false;
//				}
//			}
//			/**
//			 * ends here
//			 */
//			if(flag && olbBranch.getSelectedIndex()==0)
//			{
//				showDialogMessage("Select Branch");
//				return false;
//			}
//		}

		/**
		 * Date 12-01-2018 By Vijay 
		 * for Date combination validation
		 */
		String msg = "Wrong filter selection with From Date and To Date ! Please select proper filters with Date";
		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null ){
			if(olbcLeadType.getSelectedIndex()!=0 || olbConfigLeadPriority.getSelectedIndex()!=0 ||
			   personInfo.getIdValue()!=-1	|| !(personInfo.getFullNameValue().equals(""))  || 
			   personInfo.getCellValue()!=-1l || tbLeadId.getValue()!=null || olbLeadCategory.getSelectedIndex()!=0 ||
			   olbcLeadType.getSelectedIndex()!=0 || olbCreatedBy.getSelectedIndex()!=0){
				showDialogMessage(msg);
				return false;
			}
		}
		if(dateComparatorFollowUp.getFromDate().getValue()!=null || dateComparatorFollowUp.getToDate().getValue()!=null ){
			if(olbcLeadType.getSelectedIndex()!=0 || olbConfigLeadPriority.getSelectedIndex()!=0 ||
					   personInfo.getIdValue()!=-1	|| !(personInfo.getFullNameValue().equals(""))  || 
					   personInfo.getCellValue()!=-1l || tbLeadId.getValue()!=null || olbLeadCategory.getSelectedIndex()!=0 ||
					   olbcLeadType.getSelectedIndex()!=0 || olbCreatedBy.getSelectedIndex()!=0){
						showDialogMessage(msg);
						return false;
					}
		}
		/**
		 * ends here
		 */
		//Ashwini Patil Date:27-09-2023 
		

		boolean filterAppliedFlag=false;
		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null||dateComparatorFollowUp.getFromDate().getValue()!=null || dateComparatorFollowUp.getToDate().getValue()!=null
				||olbBranch.getSelectedIndex()!=0||olbLeadStatus.getSelectedIndex()!=0||olbEmployee.getSelectedIndex()!=0 ||olbLeadGroup.getSelectedIndex()!=0 ||olbcLeadType.getSelectedIndex()!=0 || olbConfigLeadPriority.getSelectedIndex()!=0 ||
			   personInfo.getIdValue()!=-1	|| !(personInfo.getFullNameValue().equals(""))  || 
			   personInfo.getCellValue()!=-1l || tbLeadId.getValue()!=null || olbLeadCategory.getSelectedIndex()!=0 ||
			   olbcLeadType.getSelectedIndex()!=0 || olbCreatedBy.getSelectedIndex()!=0||(tbpaymentgatewayid.getValue()!=null&&!tbpaymentgatewayid.getValue().equals("")))
				{
					filterAppliedFlag=true;
				}
				
		if(!filterAppliedFlag){
			showDialogMessage("Please apply at least one filter!");
			return false;
		}
			
		if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)){
			int diffdays = AppUtility.getDifferenceDays(dateComparator.getFromDate().getValue(), dateComparator.getToDate().getValue());
			Console.log("diffdays "+diffdays);
			if(diffdays>31){
				showDialogMessage("Please select from date and to date range within 30 days");
				return false;
			}
		}
		if((dateComparatorFollowUp.getFromDate().getValue()!=null && dateComparatorFollowUp.getToDate().getValue()!=null)){
			int diffdays = AppUtility.getDifferenceDays(dateComparatorFollowUp.getFromDate().getValue(), dateComparatorFollowUp.getToDate().getValue());
			Console.log("diffdays "+diffdays);
			if(diffdays>31){
				showDialogMessage("Please select from date and to date range within 30 days");
				return false;
			}
		}
		return super.validate();
	}

	
	
}
