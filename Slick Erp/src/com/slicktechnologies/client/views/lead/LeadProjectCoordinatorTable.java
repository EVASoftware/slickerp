package com.slicktechnologies.client.views.lead;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.ProjectCoordinatorDetails;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;



public class LeadProjectCoordinatorTable extends SuperTable<ProjectCoordinatorDetails>{

	TextColumn<ProjectCoordinatorDetails> getColumnRoleName;
	TextColumn<ProjectCoordinatorDetails> getColumnName;
	TextColumn<ProjectCoordinatorDetails> getColumnPocName;
	TextColumn<ProjectCoordinatorDetails> getColumnCell;
	TextColumn<ProjectCoordinatorDetails> getColumnEmail;
	Column<ProjectCoordinatorDetails,String> getColumnEditKDF;
	TextColumn<ProjectCoordinatorDetails> getColumnViewKDF;
	Column<ProjectCoordinatorDetails,String> deleteColumn;
	
	public LeadProjectCoordinatorTable() {
		super();
	}

	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		createColumnRoleName();
		createColumnName();
		createColumnPocName();
		createColumnCell();
		createColumnEmail();
		createColumnEditKDF();
		createColumndeleteButton();
	    addFieldUpdater();
	
				
		table.setWidth("auto");
	}
	

	
	private void createColumnRoleName() {

		getColumnRoleName = new TextColumn<ProjectCoordinatorDetails>() {
			
			@Override
			public String getValue(ProjectCoordinatorDetails object) {
				if(object.getRole()!=null)
					return object.getRole();
				return "";
			}
		};
		 table.addColumn(getColumnRoleName,"Role");

	}
	
	private void createColumnName() {

		getColumnName = new TextColumn<ProjectCoordinatorDetails>() {
			
			@Override
			public String getValue(ProjectCoordinatorDetails object) {
				if(object.getName()!=null)
					return object.getName();
				return "";
			}
		};
		 table.addColumn(getColumnName,"Name");

	}
	
	private void createColumnPocName() {

		getColumnPocName = new TextColumn<ProjectCoordinatorDetails>() {
			
			@Override
			public String getValue(ProjectCoordinatorDetails object) {
				if(object.getPocName()!=null)
					return object.getPocName();
				return "";
			}
		};
		 table.addColumn(getColumnPocName,"Co-Name");

	}
	private void createColumnCell() {

		getColumnCell = new TextColumn<ProjectCoordinatorDetails>() {
			
			@Override
			public String getValue(ProjectCoordinatorDetails object) {
				if(object.getCell()!=null)
					return object.getCell()+"";
				return "";
			}
		};
		 table.addColumn(getColumnCell,"Cell");

	}
	
	private void createColumnEmail() {

		getColumnEmail = new TextColumn<ProjectCoordinatorDetails>() {
			
			@Override
			public String getValue(ProjectCoordinatorDetails object) {
				if(object.getEmail()!=null)
					return object.getEmail();
				return "";
			}
		};
		 table.addColumn(getColumnEmail,"Email");

	}
	
	private void createColumnEditKDF(){
		
	
	EditTextCell editCell=new EditTextCell();
	getColumnEditKDF = new Column<ProjectCoordinatorDetails,String>(editCell)
			{
		@Override
		public String getValue(ProjectCoordinatorDetails object)
		{
			
			
			return object.getKdf()+"";
		}
			};
			table.addColumn(getColumnEditKDF,"#KDF");
	}
	
	protected void createFieldUpdaterKDFColumn()
	{
		getColumnEditKDF.setFieldUpdater(new FieldUpdater<ProjectCoordinatorDetails, String>()
				{
			@Override
               public void update(int index,ProjectCoordinatorDetails object,String value)
			    {
				try{
					System.out.println("value"+value);
					object.setKdf(Double.parseDouble(value));
				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
	
	private void createColumndeleteButton() {

		
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<ProjectCoordinatorDetails, String>(btnCell) {
					
			@Override
			public String getValue(ProjectCoordinatorDetails object) {    

                    	return "Delete";
			}

		};
		 
		table.addColumn(deleteColumn,"Delete");
	}
	
	public void createColumnViewKDF(){
        getColumnViewKDF = new TextColumn<ProjectCoordinatorDetails>() {
			
			@Override
			public String getValue(ProjectCoordinatorDetails object) {
				if(object.getEmail()!=null)
					return object.getEmail();
				return "";
			}
		};
		 table.addColumn(getColumnViewKDF,"#KDF");
		

	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		createFieldUpdaterdeleteColumn();
		createFieldUpdaterKDFColumn();
	}

	private void createFieldUpdaterdeleteColumn() {
		deleteColumn.setFieldUpdater(new FieldUpdater<ProjectCoordinatorDetails, String>() {
			
			@Override
			public void update(int index, ProjectCoordinatorDetails object, String value) {
				
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
		
		RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

	}
	@Override
	public void setEnable(boolean state) {
		for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
	        	  
	          if(state==true)
	          {
	        	  createTable();
	      	  }
	 
	          else
	          {
	        	// TODO Auto-generated method stub
	      		createColumnRoleName();
	      		createColumnName();
	      		createColumnPocName();
	      		createColumnCell();
	      		createColumnEmail();
	      		createColumnViewKDF();
	      		table.setWidth("auto");
	      		
	          }
	          
		

		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}

