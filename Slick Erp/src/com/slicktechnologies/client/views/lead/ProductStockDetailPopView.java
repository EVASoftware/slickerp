package com.slicktechnologies.client.views.lead;

import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Button;

public class ProductStockDetailPopView extends AbsolutePanel {
	ProductStockDetailTableView productStockDetail;
	
	public ProductStockDetailTableView getProductStockDetail() {
		return productStockDetail;
	}

	public void setProductStockDetail(ProductStockDetailTableView productStockDetail) {
		this.productStockDetail = productStockDetail;
	}

	public void initializeWidget(){
	}
	
	public ProductStockDetailPopView() {
		
		productStockDetail = new ProductStockDetailTableView();
		productStockDetail.connectToLocal();
		add(productStockDetail.getTable(),10,30);
		productStockDetail.getTable().setHeight("300px");
		
		
		setSize("600px", "400px");
		this.getElement().setId("form");
		
	}
	
	public void clear(){
		productStockDetail.connectToLocal();
	}
	

}
