package com.slicktechnologies.client.views.scheduleservice;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.ContractService;
import com.slicktechnologies.client.views.contract.ContractServiceAsync;
import com.slicktechnologies.client.views.popups.ServiceBillofMaterialListPopup;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

public class ServiceScheduleTable extends SuperTable<ServiceSchedule> implements ClickHandler {

	TextColumn<ServiceSchedule> startdayeColumn;
	TextColumn<ServiceSchedule> prod_idColumn;
	TextColumn<ServiceSchedule> prod_NameColumn;
	TextColumn<ServiceSchedule> durationColumn;
	TextColumn<ServiceSchedule> noOfserviceColumn;
	TextColumn<ServiceSchedule> serviceColumn;
	Column<ServiceSchedule,Date> dateColumn;
	TextColumn<ServiceSchedule> dateViewColumn;
	TextColumn<ServiceSchedule> timeViewColumn;
	TextColumn<ServiceSchedule> timeColumn;
	Column<ServiceSchedule,String> settimeColumn;
	TextColumn<ServiceSchedule> startdateViewColumn;
	TextColumn<ServiceSchedule> enddateViewColumn;
	TextColumn<ServiceSchedule> proQtyViewColumn;
	
	Column<ServiceSchedule, String> proBranchColumn;
	TextColumn<ServiceSchedule> branchViewColumn;
	
	
	//Added By Anil 16/11/2015
	
	Column<ServiceSchedule,String> servicingBranch;
	Column<ServiceSchedule,String> totalservicingTime;
	
	TextColumn<ServiceSchedule> viewservicingBranch;
	TextColumn<ServiceSchedule> viewtotalservicingTime;
	
	//   Rahul on 03/02/2016
	Column<ServiceSchedule, String> premisesDetails;
	TextColumn<ServiceSchedule> premisesDetailsViewColumn;
	
	
	public ArrayList<String> branchList;
	public  ArrayList<String> binList;
	public  ArrayList<CustomerBranchDetails> customerBranchList;
	public ArrayList<Branch> branchlist;
	public int flagForDay=0;
	
	/**
	 * rohan added this code
	 */
	
	public ArrayList<CustomerBranchDetails> servicingBranchAndCustomerBranchList;
	
	/**
	 * ends here 
	 */
	Date oldDate=null;
	int rowIndex;
	int custid;
	String str;
	ServiceScheduleTimePopUp servicetime=new ServiceScheduleTimePopUp();
	PopupPanel panel;
	DateTimeFormat format = DateTimeFormat.getFormat("c");
	
	int specifiday1=0;
	
	
	TextColumn<ServiceSchedule> serviceSrNo;
	
/*************** vijay added column for QuickContract ****************/ 
	
	private Column<ServiceSchedule, Boolean> checkService;
	TextColumn<ServiceSchedule> viewcheckService;
	
	/****************** vijay *****************************/
	
	/**
	 * Date : 24-01-2017 By Anil
	 * This Column display the week no. of service date (PCAAMB)
	 */
	TextColumn<ServiceSchedule> getWeekNo;
	
	/*
	 * Date : 07 Feb 2017 by Vijay
	 * This Column display Service Remark
	 */
	TextColumn<ServiceSchedule> serviceRemark;
	
	
	private static final int ISO_THURSDAY = 4;
    private static final int MAX_DAY_OF_WEEK = 6;
    private static final int DAYS_IN_WEEK = 7;
    private static final long MILLIS_DAY = 86400000;
	    
    ContractServiceAsync contratSer=GWT.create(ContractService.class);
    int week;
    
    
	/**
	 * End
	 */
	
	
    /**
     *  *:*:*
	 * nidhi
	 * 19-09-2018
	 * for show bill of material
	 * @param documentId
	 * @param documentDate
	 */
	Column<ServiceSchedule, String> getServiceBtnColumn;
	ServiceBillofMaterialListPopup serProListPopup=new ServiceBillofMaterialListPopup();
	
	public ServiceScheduleTable() {
		super();
		setHeight("450px");
		
		servicetime.getBtnOk().addClickHandler(this);
		servicetime.getBtnCancel().addClickHandler(this);
		
		/**
		 *  *:*:*
			nidhi
		 */
		serProListPopup.getPopup().getElement().addClassName("popupCustomerLog");
		serProListPopup.getLblOk().addClickHandler(this);
		serProListPopup.getLblCancel().addClickHandler(this);
		loadBranch();
	}
	
	
	private void loadBranch() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query1 = new MyQuerry();
		Company c1=new Company();
		
		Vector<Filter> filtervec1=new Vector<Filter>();
		Filter filter1 = null;
		
		filter1 = new Filter();
		filter1.setQuerryString("companyId");
		filter1.setLongValue(c1.getCompanyId());
		filtervec1.add(filter1);
		
		
		query1.setFilters(filtervec1);
		query1.setQuerryObject(new Branch());
		
		service.getSearchResult(query1,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

		
			public void onSuccess(ArrayList<SuperModel> result) {
				branchList=new ArrayList<String>();
				branchList.add("SELECT");
				for (SuperModel model : result) {
					Branch entity = (Branch) model;
					branchList.add(entity.getBusinessUnitName());
				}
			}
			
		});
	
	}


	@Override
	public void createTable() {
		if(LoginPresenter.billofMaterialActive){
			addServiceProductListButton();
		}
//		addColumnServicingBranch();
		addViewColumnServicingBranch();
//		addColumnProServicingBranch();
		addColumnServiceSrNodate();
		addColumnstartdate();
		addColumnprodid();
		addColumnprodname();
		addColumnduration();
		addColumnnoOfservice();
		addColumnserviceno();
		addColumndate();
		
		getWeekNo();
		
		addColumntime();
		addColumntimeset();
		addColumnTotalServicingTime();
		addColumnpremisesDetails();
		addColumnprobranch();
		
	}
	
	
	private void addColumnserviceRemark() {

		serviceRemark = new TextColumn<ServiceSchedule>() {
			
			@Override
			public String getValue(ServiceSchedule object) {
				if(object.getServiceRemark()!=null)
				return object.getServiceRemark();
				else return "";
			}
		};
		table.addColumn(serviceRemark,"Remark");
		table.setColumnWidth(serviceRemark, 300, Unit.PX);
	}
	
	private void addColumnCheckBox() {

		CheckboxCell checkCell= new CheckboxCell();
		checkService = new Column<ServiceSchedule, Boolean>(checkCell) {
			
			@Override
			public Boolean getValue(ServiceSchedule object) {
				// TODO Auto-generated method stub
				return object.isCheckService();
			}
		};
		table.addColumn(checkService,"Select");
		table.setColumnWidth(checkService, 60, Unit.PX);
	
	}

	public void addEditColumn(){
//		addColumnServicingBranch();
		/**
		 * nidhi
		 * 19-09-2018
		 * for map serive material
		 */
		if(LoginPresenter.billofMaterialActive){
			addServiceProductListButton();
		}
		/**
		 * end
		 */
		addColumnProServicingBranch();
		addColumnServiceSrNodate();
		addColumnstartdate();
		addColumnprodid();
		addColumnprodname();
		addColumnduration();
		addColumnnoOfservice();
		addColumnserviceno();
		addColumndate();
		getWeekNo();
		addColumntime();
		addColumntimeset();
		addColumnTotalServicingTime();
		addColumnpremisesDetails();
		addColumnprobranch();
		createFieldUpdaterServicingBranchColumn();
		addFieldUpdater();
	}
	


	


	public void addViewColumn() {
		/**
		 * nidhi
		 * 19-09-2018
		 * for map serive material
		 */
		if(LoginPresenter.billofMaterialActive){
			addServiceProductListButton();
		}
		/**
		 * end
		 */
		
		addViewColumnServicingBranch();
		addColumnServiceSrNodate();
		addColumnstartdate();
		addColumnprodid();
		addColumnprodname();
		addColumnduration();
		addColumnnoOfservice();
		addColumnserviceno();
		addViewColumndate();
		getWeekNo();
		addViewColumntime();
		addViewColumnTotalServicingTime();
		addViewColumnpremisesDetails();
		addViewColumnBranch();
		addViewColumnServiceCheck();
		addColumnserviceRemark();
		/**
		 * nidhi
		 * 19-09-2018
		 * for map serive material
		 */
		if(LoginPresenter.billofMaterialActive){
			addFieldUpdater();
		}
	}
	
	private void addViewColumnServiceCheck() {
		// TODO Auto-generated method stub
		viewcheckService = new TextColumn<ServiceSchedule>() {
			
			@Override
			public String getValue(ServiceSchedule object) {
				// TODO Auto-generated method stub
				return object.isCheckService()+"";
			}
		};
		
		table.addColumn(viewcheckService,"Select");
		table.setColumnWidth(viewcheckService, 60, Unit.PX);
		viewcheckService.setSortable(true);	
	}

	
	private void addColumnServiceSrNodate() {
		serviceSrNo=new TextColumn<ServiceSchedule>() {

			@Override
			public String getValue(ServiceSchedule object) {
				return object.getSerSrNo()+"";
			}
		};
		table.addColumn(serviceSrNo,"Service SrNo");
		table.setColumnWidth(serviceSrNo, 150, Unit.PX);
		serviceSrNo.setSortable(true);
	}


	private void addColumnTotalServicingTime() {
		EditTextCell editCell=new EditTextCell();
		totalservicingTime=new Column<ServiceSchedule,String>(editCell)
		{
			@Override
			public String getValue(ServiceSchedule object)
			{
				if(object.getTotalServicingTime()!=null){
//					return 0+"";
//				}
//				else{
					return object.getTotalServicingTime()+"";
				}
				else{
					return 0+"";
				}
			}
		};
		table.addColumn(totalservicingTime,"#Total Servicing Time");
		table.setColumnWidth(totalservicingTime, 180, Unit.PX);
		totalservicingTime.setSortable(true);	
	}


//	private void addColumnServicingBranch() {
////		EditTextCell editCell=new EditTextCell();
//		viewservicingBranch=new TextColumn<ServiceSchedule>()
//		{
//			@Override
//			public String getValue(ServiceSchedule object)
//			{
//				if(object.getServicingBranch()!=null){
////					return "";
////				}
////				else{
//					return object.getServicingBranch()+"";
//				}
//				else{
//					return "";
//				}
//			}
//		};
//		table.addColumn(viewservicingBranch,"Servicing Branch");
//		table.setColumnWidth(viewservicingBranch, 180, Unit.PX);
//	}
	
	
	private void addColumnProServicingBranch() {
		SelectionCell employeeselection= new SelectionCell(branchList);
		servicingBranch = new Column<ServiceSchedule, String>(employeeselection) {
			@Override
			public String getValue(ServiceSchedule object) {
				if (object.getScheduleProdId() != 0) {
					return object.getServicingBranch();
				} else
					return "N/A";
			}
		};
		table.addColumn(servicingBranch, "Servicing Branch");
		table.setColumnWidth(servicingBranch,180,Unit.PX);
		servicingBranch.setSortable(true);	
	}
	
	private void addViewColumnTotalServicingTime() {
		viewtotalservicingTime=new TextColumn<ServiceSchedule>() {
			@Override
			public String getValue(ServiceSchedule object) {
				if(object.getTotalServicingTime()!=null){
					return object.getTotalServicingTime()+"";
				}
				return " ";
			}
		};
		table.addColumn(viewtotalservicingTime,"Total Servicing Time");
		table.setColumnWidth(viewtotalservicingTime, 180, Unit.PX);
		viewtotalservicingTime.setSortable(true);	
	}


	private void addViewColumnServicingBranch() {
		viewservicingBranch=new TextColumn<ServiceSchedule>() {
			@Override
			public String getValue(ServiceSchedule object) {
				if(object.getServicingBranch()!=null){
					return object.getServicingBranch();
				}
				return " ";
			}
		};
		table.addColumn(viewservicingBranch,"Servicing Branch");
		table.setColumnWidth(viewservicingBranch, 180, Unit.PX);
		viewservicingBranch.setSortable(true);	
	}

	
	private void addViewColumnBranch() {
		branchViewColumn=new TextColumn<ServiceSchedule>() {
			@Override
			public String getValue(ServiceSchedule object) {
				if(object.getScheduleProBranch()!=null){
					return object.getScheduleProBranch();
				}
				return " ";
			}
		};
		table.addColumn(branchViewColumn,"Branch");
		table.setColumnWidth(branchViewColumn, 100, Unit.PX);
		branchViewColumn.setSortable(true);	
	}


	private void addColumntimeset() {

		ButtonCell btnCell= new ButtonCell();
		settimeColumn=new Column<ServiceSchedule,String>(btnCell)
				{
			@Override
			public String getValue(ServiceSchedule object)
			{
				return  "Set" ;
			}
				};
				table.addColumn(settimeColumn,"Set Time");
				table.setColumnWidth(settimeColumn, 80, Unit.PX);
				settimeColumn.setSortable(true);	
				settimeColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
	}
	

	private void addViewColumntime() {
		
		timeViewColumn=new TextColumn<ServiceSchedule>() {

			
			@Override
			public String getValue(ServiceSchedule object) {
				if(object.getScheduleServiceTime()!=null){
					return object.getScheduleServiceTime();
				}
				return " ";
			}
		};
		table.addColumn(timeViewColumn,"Service Time");
		table.setColumnWidth(timeViewColumn, 100, Unit.PX);
		timeViewColumn.setSortable(true);	
		
		
	}


	private void addViewColumndate() {

		final DateTimeFormat fmt1 = DateTimeFormat.getFormat(PredefinedFormat.DATE_FULL);
		dateViewColumn=new TextColumn<ServiceSchedule>() {

			@Override
            public String getCellStyleNames(Context context, ServiceSchedule  object) {

				if(object.getScheduleProdEndDate().equals(object.getScheduleServiceDate())){
					 return "red";
				}else{
					 return "black";
				 }
              }  
			
			@Override
			public String getValue(ServiceSchedule object) {
				if(object.getScheduleServiceDate()!=null){
					return fmt1.format(object.getScheduleServiceDate());
				}
				return " ";
			}
		};
		table.addColumn(dateViewColumn,"Service Date");
		table.setColumnWidth(dateViewColumn, 180, Unit.PX);
		dateViewColumn.setSortable(true);	
	}


	private void addColumnstartdate() {
		startdayeColumn=new TextColumn<ServiceSchedule>() {
			@Override
			public String getValue(ServiceSchedule object) {
				if(object.getScheduleStartDate()!=null){
					return AppUtility.parseDate(object.getScheduleStartDate());
				}else{
					return null;
				}
			}
		};
		table.addColumn(startdayeColumn,"Start Date");
		table.setColumnWidth(startdayeColumn, 100, Unit.PX);
		startdayeColumn.setSortable(true);	
	}




	private void addColumnduration() {
		durationColumn=new TextColumn<ServiceSchedule>() {

			@Override
			public String getValue(ServiceSchedule object) {
				return object.getScheduleDuration()+"";
			}
		};
		
		table.addColumn(durationColumn,"Duration");
		table.setColumnWidth(durationColumn, 80, Unit.PX);
		durationColumn.setSortable(true);	
		durationColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
	}


	private void addColumnprodid() {
		
		prod_idColumn=new TextColumn<ServiceSchedule>() {

			@Override
			public String getValue(ServiceSchedule object) {
				return object.getScheduleProdId()+"";
			}
		};
		table.addColumn(prod_idColumn,"Product ID");
		table.setColumnWidth(prod_idColumn, 100, Unit.PX);
		prod_idColumn.setSortable(true);
	}


	private void addColumnprodname() {
		prod_NameColumn=new TextColumn<ServiceSchedule>() {

			@Override
			public String getValue(ServiceSchedule object) {
				return object.getScheduleProdName();
			}
		};
		table.addColumn(prod_NameColumn,"Product Name");
		table.setColumnWidth(prod_NameColumn, 150, Unit.PX);
		prod_NameColumn.setSortable(true);
		
		
	}
	
	
	
	private void addColumnnoOfservice() {

		noOfserviceColumn=new TextColumn<ServiceSchedule>() {

			@Override
			public String getValue(ServiceSchedule object) {
				return object.getScheduleNoOfServices()+"";
			}
		};
		
		table.addColumn(noOfserviceColumn,"Services");
		table.setColumnWidth(noOfserviceColumn, 80, Unit.PX);
		noOfserviceColumn.setSortable(true);
		noOfserviceColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		
	}



	private void addColumnserviceno() {
		
		
		serviceColumn=new TextColumn<ServiceSchedule>() {

			@Override
			public String getValue(ServiceSchedule object) {
				return object.getScheduleServiceNo()+"";
			}
		};
		
		table.addColumn(serviceColumn,"Service No");
		table.setColumnWidth(serviceColumn, 90, Unit.PX);
		serviceColumn.setSortable(true);
		serviceColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		
	}


	private void addColumndate() {
		
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_FULL);
		DatePickerCell date= new DatePickerCell(fmt);
		dateColumn=new Column<ServiceSchedule,Date>(date)
				{
			@Override
			public Date getValue(ServiceSchedule object)
			{
				if(object.getScheduleServiceDate()!=null){
					
					oldDate=object.getScheduleServiceDate();
					return object.getScheduleServiceDate();
					
				}
				else{
					object.setScheduleServiceDate(new Date());
					oldDate=new Date();
					return new Date();
				}
			}
				};
				table.addColumn(dateColumn,"Service Date");
				table.setColumnWidth(dateColumn, 180, Unit.PX);
				dateColumn.setSortable(true);
	}

	
	private void addColumntime() {
		timeColumn=new TextColumn<ServiceSchedule>() {

			@Override
			public String getValue(ServiceSchedule object) {
				return object.getScheduleServiceTime();
			}
		};
		
		table.addColumn(timeColumn,"Service Time");
		table.setColumnWidth(timeColumn, 100, Unit.PX);
		timeColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		
	}
	
	private void addColumnprobranch() {
		

		int custmorid=QuotationPresenter.custcount;
		if(custmorid==0){
			custmorid=ContractPresenter.custcount;
		}
		final GenricServiceAsync service = GWT.create(GenricService.class);
		
		
		MyQuerry query1 = new MyQuerry();
		Company c1=new Company();
		
		Vector<Filter> filtervec1=new Vector<Filter>();
		Filter filter1 = null;
		
		filter1 = new Filter();
		filter1.setQuerryString("companyId");
		filter1.setLongValue(c1.getCompanyId());
		filtervec1.add(filter1);
		
		filter1=new Filter();
		filter1.setQuerryString("cinfo.count");
		filter1.setIntValue(custmorid);
		filtervec1.add(filter1);
		
		filter1=new Filter();
		filter1.setQuerryString("status");
		filter1.setBooleanvalue(true);
		filtervec1.add(filter1);
		
		query1.setFilters(filtervec1);
		query1.setQuerryObject(new CustomerBranchDetails());
		
		service.getSearchResult(query1,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

		
			public void onSuccess(ArrayList<SuperModel> result) {
				binList=new ArrayList<String>();
				customerBranchList=new ArrayList<CustomerBranchDetails>();
				/**
				 * rohan added this code for setting servicing branch on selection of customer branch 
				 */
				binList.add("SELECT");
				binList.add("Service Address");
				for (SuperModel model : result) {
					CustomerBranchDetails entity = (CustomerBranchDetails) model;
					binList.add(entity.getBusinessUnitName());
					customerBranchList.add(entity);
				}
				addEditColumnbranch();
				
				addColumnCheckBox();
				addColumnserviceRemark();
				addFieldUpdater();
			}

			
		});
	
		
		
		
	}
	
	private void addEditColumnbranch() {

		
		SelectionCell employeeselection= new SelectionCell(binList);
		proBranchColumn = new Column<ServiceSchedule, String>(employeeselection) {
			@Override
			public String getValue(ServiceSchedule object) {
				if (object.getScheduleProdId() != 0) {
					return object.getScheduleProBranch();
				} else
					return "N/A";
			}

			
		};
		table.addColumn(proBranchColumn, "Branch");
		table.setColumnWidth(proBranchColumn,153,Unit.PX);
		
	}
	


	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
//		createFieldUpdaterServicingBranchColumn();
		createFieldUpdaterdateColumn();
		createFieldUpdatertimeColumn();
		createFieldUpdatersettimeColumn();
		createFieldUpdaterTotalServicingTimeColumn();
		createFieldUpdatepremisesDetailsColumn();
		createFieldUpdatebranchColumn();
		
		createFieldUpdateCheckservice();
		
		if(LoginPresenter.billofMaterialActive){
			createFieldUpdaterOnServiceProductColumn();
		}
	}

	private void createFieldUpdateCheckservice() {
		checkService.setFieldUpdater(new FieldUpdater<ServiceSchedule, Boolean>() {
			
			@Override
			public void update(int index, ServiceSchedule object, Boolean value) {
				// TODO Auto-generated method stub
				try {
					Boolean val1=(value);
					object.setCheckService(val1);
				} catch (Exception e) {
				}
				table.redrawRow(index);
			}
		});
			
	
	}
	
	private void createFieldUpdaterServicingBranchColumn() {
		servicingBranch.setFieldUpdater(new FieldUpdater<ServiceSchedule, String>()
		{
			@Override
			public void update(int index,ServiceSchedule object,String value)
			{
				try{
					object.setServicingBranch(value);
				}
				catch (NumberFormatException e)
				{
				}
				table.redrawRow(index);
			}
		});
	}


	private void createFieldUpdaterTotalServicingTimeColumn() {
		totalservicingTime.setFieldUpdater(new FieldUpdater<ServiceSchedule, String>() {
			@Override
			public void update(int index, ServiceSchedule object,String value) {
				try {
					double val=Double.parseDouble(value);
					object.setTotalServicingTime(val);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
		
	}


	protected void createFieldUpdatebranchColumn() {

		
		proBranchColumn.setFieldUpdater(new FieldUpdater<ServiceSchedule, String>()
				{
			@Override

			public void update(int index,ServiceSchedule object,String value)
			{

				try{
					
					
					object.setScheduleProBranch(value);
					
					if(customerBranchList.size()!=0)
					{
						for (int i = 0; i < customerBranchList.size(); i++) {
							
							if(value.equalsIgnoreCase(customerBranchList.get(i).getBusinessUnitName().trim()))
							{
								if(customerBranchList.get(i).getBranch()!=null)
								{
									object.setServicingBranch(customerBranchList.get(i).getBranch());
									break;
								}
							}
						}
					}

				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
		
		
		
//		proBranchColumn.setFieldUpdater(new FieldUpdater<ServiceSchedule, String>(){
//			
//			@Override
//			public void update(int index, ServiceSchedule object, String value) {
//				try {
//					String val1 = (value.trim());
//					object.setScheduleProBranch(val1);
//					int pgid=0;
//					for(int i=0;i<groupList.size();i++)
//					{
//						if(val1.trim().equals(groupList.get(i).getBusinessUnitName().trim())){
//							pgid=groupList.get(i).getCount();
//							
//						}
//					}
//					object.setScheduleProdId(pgid);
//					
//					
//					
//					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//					
//				} catch (NumberFormatException e) {
//				}
//				table.redrawRow(index);
//			}
//		});
	
		
		
		
	}


	private void createFieldUpdatersettimeColumn() {

		settimeColumn.setFieldUpdater(new FieldUpdater<ServiceSchedule, String>() {
			@Override
			public void update(int index, ServiceSchedule object, String value) {
				
				str=object.getScheduleServiceTime();
				servicetime.getTbtime().setValue(str);
				servicetime.getP_servicehours().setSelectedIndex(0);
				servicetime.getP_servicemin().setSelectedIndex(0);
				servicetime.getP_ampm().setSelectedIndex(0);
				
				panel=new PopupPanel(true);
				panel.add(servicetime);
				
				panel.show();
				panel.center();
				
				rowIndex=index;
			}
		});
	
		
		
		
		
		
		
	}


	private void createFieldUpdatertimeColumn() {

		

		timeColumn.setFieldUpdater(new FieldUpdater<ServiceSchedule, String>()
				{
			@Override

			public void update(int index,ServiceSchedule object,String value)
			{

				try{
					
					
					
					object.setScheduleServiceTime(value);
					

				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	
		
		
		
	}


	private void createFieldUpdaterdateColumn() {

		dateColumn.setFieldUpdater(new FieldUpdater<ServiceSchedule, Date>()
				{
			@Override
			public void update(int index,ServiceSchedule object,Date value)
			{
				flagForDay=0;
				oldDate=object.getScheduleServiceDate();
				object.setScheduleServiceDate(value);
				/**
				 * Date : 16-06-2017 BY ANIL
				 * Setting Service Day
				 */
				object.setScheduleServiceDay(ContractForm.serviceDay(value));
				/**
				 * End
				 */
				boolean validDate=compareDateWithContractEndDate(index,value,object);
				
				if(validDate){
					
				int	specifiday=QuotationPresenter.specificday;
					
					if(specifiday==0){
						specifiday=ContractPresenter.specificday;
					}
					
					int listsize=getDataprovider().getList().size()-1;
					
					if(specifiday==1){
					
						specifiday1=specifiday;
						
					if(listsize==index){
						
						Date atdate=getDataprovider().getList().get(index-1).getScheduleServiceDate();
						
						String dayOfWeek2 = format.format(value);
						String dayOfWeek3 = format.format(atdate);
						
						int adate1=Integer.parseInt(dayOfWeek2);
						int adate2=Integer.parseInt(dayOfWeek3);
						
						
						if(adate1==adate2){
							specifiday1=1;
						}
						
						if(specifiday1==0){
//							flagForDay=1;
							ServiceSchedulePopUp.p_mondaytofriday.setSelectedIndex(0);
							GWTCAlert alrt=new GWTCAlert();
							alrt.alert("You Selected Specific Day is Remove");
							
							specifiday=0;
							QuotationPresenter.specificday=0;
							ContractPresenter.specificday=0;
							specifiday1=0;
						}
						
					}else
					if(listsize>index){
						
						Date b4date=getDataprovider().getList().get(index+1).getScheduleServiceDate();
						
						String dayOfWeek1 = format.format(b4date);
						String dayOfWeek2 = format.format(value);
						
						int adate=Integer.parseInt(dayOfWeek1);
						int adate1=Integer.parseInt(dayOfWeek2);
						
						
						if(adate==adate1){
							specifiday1=0;
						}
						
						if(specifiday1==1){
//							flagForDay=1;
							
							ServiceSchedulePopUp.p_mondaytofriday.setSelectedIndex(0);
							GWTCAlert alrt=new GWTCAlert();
							alrt.alert("You Selected Specific Day is Remove");
							
							specifiday=0;
							QuotationPresenter.specificday=0;
							ContractPresenter.specificday=0;
							specifiday1=0;
						}
						
					}else{
						
						
						Date b4date=getDataprovider().getList().get(index-1).getScheduleServiceDate();
						Date atdate=getDataprovider().getList().get(index+1).getScheduleServiceDate();
						
						String dayOfWeek1 = format.format(b4date);
						String dayOfWeek2 = format.format(value);
						String dayOfWeek3 = format.format(atdate);
						
						int adate=Integer.parseInt(dayOfWeek1);
						int adate1=Integer.parseInt(dayOfWeek2);
						int adate2=Integer.parseInt(dayOfWeek3);
						
						
						if(adate==adate1&&adate1==adate2){
							specifiday1=0;
						}
						
						if(specifiday1==1){
//							flagForDay=1;
							
							ServiceSchedulePopUp.p_mondaytofriday.setSelectedIndex(0);
							
							GWTCAlert alrt=new GWTCAlert();
							alrt.alert("You Selected Specific Day is Remove");
							specifiday=0;
							QuotationPresenter.specificday=0;
							ContractPresenter.specificday=0;
							specifiday1=0;
						}
					}
					
					}
					
					/**
					 * Date 02-04-2017 added by vijay for week number
					 */
					int weaknumber = getweeknumber(value);
					object.setWeekNo(weaknumber);
					
					/**
					 * ends here
					 */
					
					object.setScheduleServiceDate(value);
				
				}
				else{
					GWTCAlert alrt=new GWTCAlert();
					alrt.alert("Service Date Should be in between Service Previous date and Next Date");
					object.setScheduleServiceDate(oldDate);
				}
				
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					table.redrawRow(index);
					
			}

//			private boolean compareDateWithContractEndDate(int index,Date value, ServiceSchedule object) {
//				if(object.getProductEndDate().after(value)&&object.getProductStartDate().before(value)){
//				
//					if(index>1){
//						
//						System.out.println("service    == "+getDataprovider().getList().get(index-1).getService_Date());
//						System.out.println("value    ==  "+value);
//						if(getDataprovider().getList().get(index-1).getService_Date().after(value)){
//							System.out.println("in if============");
//							return true;
//						}
//						else{
//							return false;
//						}
//							
//						
//					}else{
//						return false;
//					}
//				
//				}else{
//				
//					return false;
//			
//				}
//			}
			
			private boolean compareDateWithContractEndDate(int index,Date value, ServiceSchedule object) {
				
				
				Date startdate=object.getScheduleProdStartDate();
				
				Date pStartdate=new Date(startdate.getTime());
				
				CalendarUtil.addDaysToDate(pStartdate, -1);
				
				
				
				if(object.getScheduleProdEndDate().after(value)&&pStartdate.before(value)){
//					if(index>=1&&object.getScheduleServiceNo()>=1){
//						if(getDataprovider().getList().get(index-1).getScheduleServiceDate().before(value)){
//							return true;
//						}
//					}
//					else if(index==0){
//						return true;
//					}else if(object.getScheduleServiceNo()==1){
//						return true;
//					}
					
					
				if(object.getScheduleServiceNo()==1&&object.getScheduleNoOfServices()>1&&index==0){
					if(getDataprovider().getList().size()>index){
						Date nextdate=getDataprovider().getList().get(index+1).getScheduleServiceDate();
						Date nDate=new Date(nextdate.getTime());
						CalendarUtil.addDaysToDate(nDate, -1);
						if(object.getScheduleServiceDate().before(nDate)){
							return true;
						}
						else{
							return false;
						}
					}
					else{
						return false;
					}
				}
				
				
				
				if(object.getScheduleServiceNo()==object.getScheduleNoOfServices()){
						Date b4date=getDataprovider().getList().get(index-1).getScheduleServiceDate();
						Date nDate=new Date(b4date.getTime());
						if(object.getScheduleServiceDate().after(nDate)){
							return true;
						}
						else{
							return false;
						}
					}
				
				
				
				if(object.getScheduleServiceNo()==1&&object.getScheduleNoOfServices()>1&&index>=1){
					if(getDataprovider().getList().size()>index){
						Date nextdate=object.getScheduleServiceDate();
						Date nDate=new Date(nextdate.getTime());
						CalendarUtil.addDaysToDate(nDate, 1);
						if(nDate.before(getDataprovider().getList().get(index+1).getScheduleServiceDate())){
							return true;
						}
						else{
							return false;
						}
					}
					else{
						return false;
					}
				}
				if(object.getScheduleServiceNo()==1&&object.getScheduleNoOfServices()==1){
					return true;
				}
				
				if(object.getScheduleServiceNo()>1){
					if(getDataprovider().getList().size()>index){
						Date nextdate=getDataprovider().getList().get(index+1).getScheduleServiceDate();
						Date nDate=new Date(nextdate.getTime());
						CalendarUtil.addDaysToDate(nDate, -1);
						
					if((object.getScheduleServiceDate().after(getDataprovider().getList().get(index-1).getScheduleServiceDate())&&object.getScheduleServiceDate().before(nDate))
							&&(object.getScheduleServiceNo()>getDataprovider().getList().get(index-1).getScheduleServiceNo())&&object.getScheduleServiceNo()<getDataprovider().getList().get(index+1).getScheduleServiceNo())
					{
						return true;
//					}else if(object.getScheduleServiceDate().before(getDataprovider().getList().get(index+1).getScheduleServiceDate())&&object.getScheduleServiceNo()<getDataprovider().getList().get(index+1).getScheduleServiceNo()){
//						
//						System.out.println("true 2");
//						return true;
					}
					else{
						return false;
					}
					}
				}
					
				/********************************************************************/	
				}else{
				
					return false;
			
				}
				return false;
			}
		});
	}
	
	
	
	
	
//	private boolean compareDateWithContractEndDate(Date value) {
//		if(AppUtility.calculateQuotationEndDate(salesitemlis).before(value)&&AppUtility.calculateQuotationStartDate(salesitemlis).after(value)){
//			
//			System.out.println("Start date  === "+AppUtility.calculateQuotationStartDate(salesitemlis));
//			System.out.println("End date  === "+AppUtility.calculateQuotationEndDate(salesitemlis));
//			
//			
//			
//		}else{
//			return false;
//		}
//		
//		
//	}

	@Override
	public void setEnable(boolean state) {

		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addEditColumn();
		if (state == false)
			addViewColumn();
	}

	@Override
	public void applyStyle() {
		
	}


	public Column<ServiceSchedule, Date> getDateColumn() {
		return dateColumn;
	}


	public void setDateColumn(Column<ServiceSchedule, Date> dateColumn) {
		this.dateColumn = dateColumn;
	}


	public Column<ServiceSchedule, String> getProBranchColumn() {
		return proBranchColumn;
	}


	public void setProBranchColumn(Column<ServiceSchedule, String> proBranchColumn) {
		this.proBranchColumn = proBranchColumn;
	}


	@Override
	public void onClick(ClickEvent event) {

		if(event.getSource()==servicetime.getBtnOk()){
			
			
			if((servicetime.getP_servicehours().getSelectedIndex()!=0&&servicetime.getP_servicemin().getSelectedIndex()!=0&&servicetime.getP_ampm().getSelectedIndex()!=0)
					||(servicetime.getP_servicehours().getSelectedIndex()==0&&servicetime.getP_servicemin().getSelectedIndex()==0&&servicetime.getP_ampm().getSelectedIndex()==0)){
					
			
			
			if(servicetime.getP_servicehours().getSelectedIndex()!=0&&servicetime.getP_servicemin().getSelectedIndex()!=0&&servicetime.getP_ampm().getSelectedIndex()!=0){
				ArrayList<ServiceSchedule> list=new ArrayList<ServiceSchedule>();
				if(getDataprovider().getList().size()!=0){
					list.addAll(getDataprovider().getList());
						
						String serviceHrs=servicetime.getP_servicehours().getItemText(servicetime.getP_servicehours().getSelectedIndex());
						String serviceMin=servicetime.getP_servicemin().getItemText(servicetime.getP_servicemin().getSelectedIndex());
						String serviceAmPm=servicetime.getP_ampm().getItemText(servicetime.getP_ampm().getSelectedIndex());
						String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
						
						list.get(rowIndex).setScheduleServiceTime(calculatedServiceTime);
						getDataprovider().getList().clear();
						getDataprovider().getList().addAll(list);
				}
				panel.hide();
			}
			}else{
				
				GWTCAlert alrt=new GWTCAlert();
				alrt.alert("Set Time Properly");
			}
			
		}
		
		if(event.getSource()==servicetime.getBtnCancel()){
			panel.hide();
		}
		
		if(event.getSource()== serProListPopup.getLblOk()){
			serProListPopup.hidePopUp();
			
		}
		if(event.getSource() == serProListPopup.getLblCancel()){
			serProListPopup.hidePopUp();
		}
	}

	public int valdiateDay()
	{
		return flagForDay;
	}

	
	
	// *********************added by rahul for premise details *******************
	
	private void addColumnpremisesDetails() {
		// TODO Auto-generated method stub

		EditTextCell editCell=new EditTextCell();
		premisesDetails=new Column<ServiceSchedule,String>(editCell)
				{
			@Override
			public String getValue(ServiceSchedule object)
			{
				if(object.getPremisesDetails()!=null){
					return object.getPremisesDetails();
					}
				else{
					return "";
				}
			}
				};
				table.addColumn(premisesDetails,"#Premises Details");
				table.setColumnWidth(premisesDetails, 200, Unit.PX);
				premisesDetails.setSortable(true);
	}
	
	
	private void addViewColumnpremisesDetails() {
		// TODO Auto-generated method stub


		premisesDetailsViewColumn=new TextColumn<ServiceSchedule>() {

			
			@Override
			public String getValue(ServiceSchedule object) {
				if(object.getPremisesDetails()!=null){
					return object.getPremisesDetails();
				}
				return " ";
			}
		};
		table.addColumn(premisesDetailsViewColumn,"Premises Details");
		table.setColumnWidth(premisesDetailsViewColumn, 100, Unit.PX);
		
	}
	

	private void createFieldUpdatepremisesDetailsColumn() {
		premisesDetails.setFieldUpdater(new FieldUpdater<ServiceSchedule, String>()
				{
			@Override

			public void update(int index,ServiceSchedule object,String value)
			{

				try{
					object.setPremisesDetails(value);
				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
	
	
	//    rohan added this code for sorting on service schedule table 
	//    Date :10/11/2016
	
	public void addColumnSorting(){
		
//		addColumnServicingBranch();
		addViewColumnServicingBranchSorting();
//		addColumnProServicingBranch();
		addColumnServiceSrNodateSorting();
		addColumnstartdateSorting();
		addColumnprodidSorting();
		addColumnprodnameSorting();
		addColumndurationSorting();
		addColumnnoOfserviceSorting();
		addColumnservicenoSorting();
		addColumndateSorting();
//		addColumntimeSorting();
//		addColumntimesetSorting();
//		addColumnTotalServicingTimeSorting();
		addColumnpremisesDetailsSorting();
//		addColumnprobranchSorting();
	}

	
	protected void addViewColumnServicingBranchSorting()
	{
		List<ServiceSchedule> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceSchedule>(list);
		columnSort.setComparator(viewservicingBranch, new Comparator<ServiceSchedule>()
				{
			@Override
			public int compare(ServiceSchedule e1,ServiceSchedule e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getServicingBranch()!=null && e2.getServicingBranch()!=null){
						return e1.getServicingBranch().compareTo(e2.getServicingBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumnServiceSrNodateSorting()
	{
		List<ServiceSchedule> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceSchedule>(list);
		columnSort.setComparator(serviceSrNo, new Comparator<ServiceSchedule>()
				{
			@Override
			public int compare(ServiceSchedule e1,ServiceSchedule e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getSerSrNo()== e2.getSerSrNo()){
						return 0;}
					if(e1.getSerSrNo()> e2.getSerSrNo()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addColumnstartdateSorting()
	{
		List<ServiceSchedule> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceSchedule>(list);
		columnSort.setComparator(startdayeColumn, new Comparator<ServiceSchedule>()
				{
			@Override
			public int compare(ServiceSchedule e1,ServiceSchedule e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getScheduleStartDate()!=null && e2.getScheduleStartDate()!=null){
						return e1.getScheduleStartDate().compareTo(e2.getScheduleStartDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumnprodidSorting()
	{
		List<ServiceSchedule> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceSchedule>(list);
		columnSort.setComparator(prod_idColumn, new Comparator<ServiceSchedule>()
				{
			@Override
			public int compare(ServiceSchedule e1,ServiceSchedule e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getScheduleProdId()== e2.getScheduleProdId()){
						return 0;}
					if(e1.getScheduleProdId()> e2.getScheduleProdId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumnprodnameSorting()
	{
		List<ServiceSchedule> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceSchedule>(list);
		columnSort.setComparator(prod_NameColumn, new Comparator<ServiceSchedule>()
				{
			@Override
			public int compare(ServiceSchedule e1,ServiceSchedule e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getScheduleProdName()!=null && e2.getScheduleProdName()!=null){
						return e1.getScheduleProdName().compareTo(e2.getScheduleProdName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumndurationSorting()
	{
		List<ServiceSchedule> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceSchedule>(list);
		columnSort.setComparator(durationColumn, new Comparator<ServiceSchedule>()
				{
			@Override
			public int compare(ServiceSchedule e1,ServiceSchedule e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getScheduleDuration()== e2.getScheduleDuration()){
						return 0;}
					if(e1.getScheduleDuration()> e2.getScheduleDuration()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addColumnnoOfserviceSorting()
	{
		List<ServiceSchedule> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceSchedule>(list);
		columnSort.setComparator(noOfserviceColumn, new Comparator<ServiceSchedule>()
				{
			@Override
			public int compare(ServiceSchedule e1,ServiceSchedule e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getScheduleNoOfServices()== e2.getScheduleNoOfServices()){
						return 0;}
					if(e1.getScheduleNoOfServices()> e2.getScheduleNoOfServices()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	protected void addColumnservicenoSorting()
	{
		List<ServiceSchedule> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceSchedule>(list);
		columnSort.setComparator(serviceColumn, new Comparator<ServiceSchedule>()
				{
			@Override
			public int compare(ServiceSchedule e1,ServiceSchedule e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getScheduleServiceNo()== e2.getScheduleServiceNo()){
						return 0;}
					if(e1.getScheduleServiceNo()> e2.getScheduleServiceNo()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addColumndateSorting()
	{
		List<ServiceSchedule> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceSchedule>(list);
		columnSort.setComparator(dateColumn, new Comparator<ServiceSchedule>()
				{
			@Override
			public int compare(ServiceSchedule e1,ServiceSchedule e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getScheduleServiceDate()!=null && e2.getScheduleServiceDate()!=null){
						return e1.getScheduleServiceDate().compareTo(e2.getScheduleServiceDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	protected void addColumnpremisesDetailsSorting()
	{
		List<ServiceSchedule> list=getDataprovider().getList();
		columnSort=new ListHandler<ServiceSchedule>(list);
		columnSort.setComparator(premisesDetails, new Comparator<ServiceSchedule>()
				{
			@Override
			public int compare(ServiceSchedule e1,ServiceSchedule e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getPremisesDetails()!=null && e2.getPremisesDetails()!=null){
						return e1.getPremisesDetails().compareTo(e2.getPremisesDetails());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void getWeekNo() {
		
		
		getWeekNo=new TextColumn<ServiceSchedule>() {
			@Override
			public String getValue(ServiceSchedule object) {
				if(object.getScheduleServiceDate()!=null){
//					System.out.println("&&&&&&&& "+object.getScheduleServiceDate());
//					Calendar cal=Calendar.getInstance();
//					cal.setTime(object.getScheduleServiceDate());
//					int week=cal.get(Calendar.WEEK_OF_MONTH);
//					return week+"";
					
//					SimpleDateFormat fmt=new SimpleDateFormat("w");
//					int month=object.getScheduleServiceDate().getMonth();
//					int weekYearWise=Integer.parseInt(fmt.format(object.getScheduleServiceDate()));
//					int week=weekYearWise/month;
//					return week+"";
					
//					Date date = object.getScheduleServiceDate();
//					Date yearStart = new Date(date.getYear(), 0, 0);
//					int week = (int) (date.getTime() - yearStart.getTime())/(7 * 24 * 60 * 60 * 1000);
//					return week+"";
					
//					int month=object.getScheduleServiceDate().getMonth()+1;
//					int weekYearWise=getWeek(object.getScheduleServiceDate());
//					int week=weekYearWise/month;
//					return week+"";
					
//					CalendarModel cal=new CalendarModel();
//					cal.setCurrentMonth(object.getScheduleServiceDate());
//					int week=cal.WEEKS_IN_MONTH;
//					return week+"";
					
//					week=0;
//					contratSer.getWeek(object.getScheduleServiceDate(), new AsyncCallback<Integer>() {
//						@Override
//						public void onFailure(Throwable caught) {
//						}
//						@Override
//						public void onSuccess(Integer result) {
//							week= result;
//						}
//					});
//					return week+"";
					
//					DateTimeFormat fmt=DateTimeFormat.getFormat("E")
					
//					return getWeek(object.getScheduleServiceDate())+"";
					
					/**
					 * Date 29-03-2017
					 * below one line commented old code by vijay and new line added by vijay
					 * old for week of the year and new code for week of month
					 */
					return object.getWeekNo()+"";

				}
				return "";
			}
		};
		table.addColumn(getWeekNo, "Week");
		table.setColumnWidth(getWeekNo, 80, Unit.PX);
		dateColumn.setSortable(true);
	}

	public int getWeek(Date serviceDate) {
		final Date date = serviceDate;
		// ISO week day (Mon=1 to Sun=7)
		final int dayOfWeek = 1 + (date.getDay() + MAX_DAY_OF_WEEK)% DAYS_IN_WEEK;
		final Date nearestThu = addDays(date, ISO_THURSDAY - dayOfWeek);
		final int year = nearestThu.getYear();
		final Date jan1 = new Date(year, 0, 1);
		return 1 + dayDiff(nearestThu, jan1) / DAYS_IN_WEEK;
	}

	public static Date addDays(final Date sourceDate, final long days) {
		return new Date(sourceDate.getTime() + (days * MILLIS_DAY));
	}

	public static int dayDiff(final Date firstDate, final Date secondDate) {
		return (int) ((firstDate.getTime() - secondDate.getTime()) / MILLIS_DAY);
	}
	
	/**
	 * Date 02-04-2017
	 * added by vijay
	 * this method returns service date week number of month
	 */
	
	private int getweeknumber(Date scheduleServiceDate) {

	 Date serviceDatewithtime = new  Date();
	 serviceDatewithtime =  CalendarUtil.copyDate(scheduleServiceDate);
	 String strDate = DateTimeFormat.getFormat("dd/MM/yyyy").format(serviceDatewithtime);
	 Date serviceDate =  DateTimeFormat.getFormat("dd/MM/yyyy").parse(strDate);
	 
	 
	  Date date  = new Date();
	  date = CalendarUtil.copyDate(scheduleServiceDate);
	  
	  String dayOfWeek1 = format.format(date);
	  int dayNumber = Integer.parseInt(dayOfWeek1);
	  
	  CalendarUtil.setToFirstDayOfMonth(date);	
	  Date sdate =  getCalculatedDatewithWeek(date, dayNumber);
	  
//	  dayNumber=dayNumber+1;
	  if(sdate.equals(serviceDate)){
		  return 1;
	  }
	  CalendarUtil.addDaysToDate(date, 7);
	  sdate = getCalculatedDatewithWeek(date, dayNumber);
	  if(sdate.equals(serviceDate)){
		  return 2;
	  }
	  
	  CalendarUtil.addDaysToDate(date, 7);
	  sdate = getCalculatedDatewithWeek(date, dayNumber);
	  if(sdate.equals(serviceDate)){
		  return 3;
	  }
	  CalendarUtil.addDaysToDate(date, 7);
	  sdate = getCalculatedDatewithWeek(date, dayNumber);
	  if(sdate.equals(serviceDate)){
		  return 4;
	  }
	  
	  CalendarUtil.addDaysToDate(date, 7);
	  sdate = getCalculatedDatewithWeek(date, dayNumber);
	  if(sdate.equals(serviceDate)){
		  return 5;
	  }
	return 0;

	}
	
	/**
	 * date :- 03 March 2017
	 * added by vijay for week number when we change service date
	 * this method return date with specified day
	 */
	private Date getCalculatedDatewithWeek(Date d, int adday1value) {
		
		String dayOfWeek1 = format.format(d);
		int adate = Integer.parseInt(dayOfWeek1);
		int adday1 =adday1value;
		int adday = 1;
		if (adate <= adday1) {
			adday = (adday1 - adate);
		} else {
			adday = (7 - adate + adday1);
		}
		Date newDate = new Date(d.getTime());
		CalendarUtil.addDaysToDate(newDate, adday);

		Date day = new Date(newDate.getTime());
		
		return day;
	}
	protected void addServiceProductListButton() {
		ButtonCell btnCell = new ButtonCell();
		getServiceBtnColumn = new Column<ServiceSchedule, String>(btnCell) {
			@Override
			public String getValue(ServiceSchedule object) {
				return "Service Material";
			}
		};
		table.addColumn(getServiceBtnColumn, "Service Material");
		table.setColumnWidth(getServiceBtnColumn, 120, Unit.PX);
	}
	
	protected void createFieldUpdaterOnServiceProductColumn(){

		getServiceBtnColumn.setFieldUpdater(new FieldUpdater<ServiceSchedule, String>() {
			@Override
			public void update(int index, ServiceSchedule object,String value) {
				serProListPopup.getServiceProListTable().getDataprovider().getList().clear();
				serProListPopup.showPopUp();
				if(object.getServiceProductList()!=null){
					serProListPopup.getServiceProListTable().getDataprovider().getList().clear();
					serProListPopup.getServiceProListTable().getDataprovider().getList().addAll(object.getServiceProductList().get(0));
				}
			
				
				serProListPopup.getServiceProListTable().getTable().redraw();
				
				System.out.println("get table size --" +serProListPopup.getServiceProListTable().getDataprovider().getList().size());
				/*if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
					panel=new PopupPanel(true);
					advSchPopup.clear();
//					advSchPopup.getBranchTable().getDataprovider().getList().addAll(branchSchedulingList); // old code commented by vijay
					advSchPopup.getBranchTable().getDataprovider().setList(branchSchedulingList);
					panel.add(advSchPopup);
					panel.center();
					panel.show();
					rowIndex=index;
				}else{
					GWTCAlert alert=new GWTCAlert();
					alert.alert("Customer branch is not defined.");
				}*/
			}
		});
	
	}
}
