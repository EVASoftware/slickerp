package com.slicktechnologies.client.views.scheduleservice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

public class ServiceSchedulePopUp extends AbsolutePanel {

	private TextBox p_docid;
	private DateBox p_docdate;
	private IntegerBox p_interval;
	private RadioButton p_default;
	private CheckBox p_customise;
	public static ListBox p_mondaytofriday;
	private Button btnCancel,btnOk,btnReset;
	private ListBox p_servicehours;
	private ListBox p_servicemin;
	private ListBox p_ampm;
	ServiceScheduleTable popScheduleTable;
	ServiceSchedule serviceScheduleEntity;
	private ListBox p_serviceWeek;
	
	
	/**
	 * Date : 23-11-2017 BY ANIL
	 * Following are the filters for advance service scheduling
	 */
	private ListBox lbServices;
	private ObjectListBox<Branch> olbServicingBranch;
	private ObjectListBox<CustomerBranchDetails> olbCustomerBranch;
	private Button btnFilter;
	/**
	 * nidhi
	 * 21-05-2018
	 * for exclude day for service
	 * @param documentId
	 * @param documentDate
	 */
	public  ListBox e_mondaytofriday;
	
	public ObjectListBox<Calendar> olbcalendar;
	

	public ServiceSchedulePopUp(String documentId,String documentDate) {

		serviceScheduleEntity=new ServiceSchedule();
		
		InlineLabel laqoution=new InlineLabel("Service Schedule");
		laqoution.getElement().getStyle().setFontSize(20, Unit.PX);
		add(laqoution,320,10);
	
		InlineLabel ladocid=new InlineLabel(documentId);
		add(ladocid, 10, 50);
		p_docid = new TextBox();
		add(p_docid, 10, 70);
		p_docid.setEnabled(false);
		p_docid.setSize("100px", "16px");
		
		InlineLabel docstats=new InlineLabel(documentDate);
		add(docstats, 150, 50);
		
		p_docdate = new DateBoxWithYearSelector();
		DateTimeFormat format1=DateTimeFormat.getFormat("dd-MM-yyyy");
		DateBox.DefaultFormat defformat1=new DateBox.DefaultFormat(format1);
		p_docdate.setFormat(defformat1);
		add(p_docdate, 150, 70);
		p_docdate.setEnabled(false);
		p_docdate.setSize("100px", "16px");
	
		p_default = new RadioButton("Monthly");
		add(p_default,10,120);
		p_default.setSize("100px", "16px");
		p_default.setText("Equal");
		
		
		InlineLabel redate=new InlineLabel("Interval");
		add(redate,110,125);
		
		p_interval = new IntegerBox();
		add(p_interval,160,120);
		p_interval.setSize("60px", "16px");
		p_interval.setName("Interval");
		
		p_customise = new CheckBox();
		add(p_customise,260,120);
		p_customise.setSize("100px", "16px");
		p_customise.setText("Customise");
		
		
		InlineLabel reday=new InlineLabel("Day");
		add(reday,390,124);
		p_mondaytofriday = new ListBox();
		add(p_mondaytofriday,420,120);
		p_mondaytofriday.setSize("100px", "24px");
		
		p_mondaytofriday.insertItem("--Select--", 0);
		p_mondaytofriday.insertItem("Sunday", 1);
		p_mondaytofriday.insertItem("Monday", 2);
		p_mondaytofriday.insertItem("Tuesday", 3);
		p_mondaytofriday.insertItem("Wednesday", 4);
		p_mondaytofriday.insertItem("Thursday",5);
		p_mondaytofriday.insertItem("Friday", 6);
		p_mondaytofriday.insertItem("Saturday", 7);
		
		
		/**
		 * Date 8 march 2017
		 * added by vijay
		 */
		InlineLabel monthWeek=new InlineLabel("Week");
		add(monthWeek,538,124);
		p_serviceWeek = new ListBox();
		add(p_serviceWeek,578,120);
		p_serviceWeek.setSize("45px", "24px");
		p_serviceWeek.insertItem("--", 0);
		p_serviceWeek.insertItem("01", 1);
		p_serviceWeek.insertItem("02", 2);
		p_serviceWeek.insertItem("03", 3);
		p_serviceWeek.insertItem("04", 4);
		
		/**
		 * ends here
		 */

		InlineLabel retime=new InlineLabel("Time");
		add(retime,638,124);
		
		
		InlineLabel hhtime=new InlineLabel("HH");
		add(hhtime,678,124);
		InlineLabel coluntime=new InlineLabel(":");
		add(coluntime,700,124);
		InlineLabel mmtime=new InlineLabel("MM");
		add(mmtime,707,124); 
		
		
		p_servicehours=new ListBox();
		add(p_servicehours,731,120);
		p_servicehours.setSize("45px", "24px");
		p_servicehours.insertItem("--", 0);
		p_servicehours.insertItem("01", 1);
		p_servicehours.insertItem("02", 2);
		p_servicehours.insertItem("03", 3);
		p_servicehours.insertItem("04", 4);
		p_servicehours.insertItem("05", 5);
		p_servicehours.insertItem("06", 6);
		p_servicehours.insertItem("07", 7);
		p_servicehours.insertItem("08", 8);
		p_servicehours.insertItem("09", 9);
		p_servicehours.insertItem("10", 10);
		p_servicehours.insertItem("11", 11);
		p_servicehours.insertItem("12", 12);
		
		InlineLabel colon=new InlineLabel(":");
		add(colon,779,123);
		
		p_servicemin=new ListBox();
		add(p_servicemin,786,120);
		p_servicemin.setSize("45px", "24px");
		p_servicemin.insertItem("--", 0);
		p_servicemin.insertItem("00", 1);
		p_servicemin.insertItem("15", 2);
		p_servicemin.insertItem("30", 3);
		p_servicemin.insertItem("45", 4);
		
		
		
		
		p_ampm = new ListBox();
		add(p_ampm,846,120);
		p_ampm.setSize("47px", "24px");
		p_ampm.insertItem("--", 0);
		p_ampm.insertItem("AM", 1);
		p_ampm.insertItem("PM", 2);
		
		
		InlineLabel exDaye=new InlineLabel("Exclude Day:");
		add(exDaye,960,124); //900
		e_mondaytofriday = new ListBox();
		add(e_mondaytofriday,1050,120); //990
		e_mondaytofriday.setSize("100px", "24px");
		
		e_mondaytofriday.insertItem("--Select--", 0);
		e_mondaytofriday.insertItem("Sunday", 1);
		e_mondaytofriday.insertItem("Monday", 2);
		e_mondaytofriday.insertItem("Tuesday", 3);
		e_mondaytofriday.insertItem("Wednesday", 4);
		e_mondaytofriday.insertItem("Thursday",5);
		e_mondaytofriday.insertItem("Friday", 6);
		e_mondaytofriday.insertItem("Saturday", 7);
		e_mondaytofriday.insertItem(AppConstants.HOLIDAY, 8);

		
		btnReset = new Button("Reset");
		add(btnReset, 906, 117);
		
		popScheduleTable = new ServiceScheduleTable();
		popScheduleTable.connectToLocal();
		add(popScheduleTable.getTable(),10,160);
		
		btnCancel = new Button("Cancel");
		btnOk=new Button("Ok");
		
		add(btnOk, 370, 615);
		add(btnCancel,520,615);
		
		/*
		 * nidhi
		 *  5-08-2017
		 *  for make popup responsive
		 *  
		 */
		setSize("90%","90%");
		this.getElement().addClassName("scheduleformdiv");
		/**
		 * end
		 */
		this.getElement().setId("form");
	
	}

	
	
	public ServiceSchedulePopUp(String documentId,String documentDate,boolean filterFlag) {/*

		serviceScheduleEntity=new ServiceSchedule();
		
		InlineLabel laqoution=new InlineLabel("Service Schedule");
		laqoution.getElement().getStyle().setFontSize(20, Unit.PX);
		add(laqoution,320,10);
	
		InlineLabel ladocid=new InlineLabel(documentId);
		add(ladocid, 10, 50);
		p_docid = new TextBox();
		add(p_docid, 10, 70);
		p_docid.setEnabled(false);
		p_docid.setSize("100px", "16px");
		
		InlineLabel docstats=new InlineLabel(documentDate);
		add(docstats, 150, 50);
		
		p_docdate = new DateBoxWithYearSelector();
		DateTimeFormat format1=DateTimeFormat.getFormat("dd-MM-yyyy");
		DateBox.DefaultFormat defformat1=new DateBox.DefaultFormat(format1);
		p_docdate.setFormat(defformat1);
		add(p_docdate, 150, 70);
		p_docdate.setEnabled(false);
		p_docdate.setSize("100px", "16px");
	
		p_default = new RadioButton("Monthly");
		add(p_default,10,120);
		p_default.setSize("100px", "16px");
		p_default.setText("Equal");
		
		
		InlineLabel redate=new InlineLabel("Interval");
		add(redate,110,125);
		
		p_interval = new IntegerBox();
		add(p_interval,160,120);
		p_interval.setSize("60px", "16px");
		p_interval.setName("Interval");
		
		p_customise = new CheckBox();
		add(p_customise,260,120);
		p_customise.setSize("100px", "16px");
		p_customise.setText("Customise");
		
		
		InlineLabel reday=new InlineLabel("Day");
		add(reday,390,124);
		p_mondaytofriday = new ListBox();
		add(p_mondaytofriday,420,120);
		p_mondaytofriday.setSize("100px", "24px");
		
		p_mondaytofriday.insertItem("--Select--", 0);
		p_mondaytofriday.insertItem("Sunday", 1);
		p_mondaytofriday.insertItem("Monday", 2);
		p_mondaytofriday.insertItem("Tuesday", 3);
		p_mondaytofriday.insertItem("Wednesday", 4);
		p_mondaytofriday.insertItem("Thursday",5);
		p_mondaytofriday.insertItem("Friday", 6);
		p_mondaytofriday.insertItem("Saturday", 7);
		
		
		*//**
		 * Date 8 march 2017
		 * added by vijay
		 *//*
		InlineLabel monthWeek=new InlineLabel("Week");
		add(monthWeek,538,124);
		p_serviceWeek = new ListBox();
		add(p_serviceWeek,578,120);
		p_serviceWeek.setSize("45px", "24px");
		p_serviceWeek.insertItem("--", 0);
		p_serviceWeek.insertItem("01", 1);
		p_serviceWeek.insertItem("02", 2);
		p_serviceWeek.insertItem("03", 3);
		p_serviceWeek.insertItem("04", 4);
		
		*//**
		 * ends here
		 *//*

		InlineLabel retime=new InlineLabel("Time");
		add(retime,638,124);
		
		
		InlineLabel hhtime=new InlineLabel("HH");
		add(hhtime,678,124);
		InlineLabel coluntime=new InlineLabel(":");
		add(coluntime,700,124);
		InlineLabel mmtime=new InlineLabel("MM");
		add(mmtime,707,124); 
		
		
		p_servicehours=new ListBox();
		add(p_servicehours,731,120);
		p_servicehours.setSize("45px", "24px");
		p_servicehours.insertItem("--", 0);
		p_servicehours.insertItem("01", 1);
		p_servicehours.insertItem("02", 2);
		p_servicehours.insertItem("03", 3);
		p_servicehours.insertItem("04", 4);
		p_servicehours.insertItem("05", 5);
		p_servicehours.insertItem("06", 6);
		p_servicehours.insertItem("07", 7);
		p_servicehours.insertItem("08", 8);
		p_servicehours.insertItem("09", 9);
		p_servicehours.insertItem("10", 10);
		p_servicehours.insertItem("11", 11);
		p_servicehours.insertItem("12", 12);
		
		InlineLabel colon=new InlineLabel(":");
		add(colon,779,123);
		
		p_servicemin=new ListBox();
		add(p_servicemin,786,120);
		p_servicemin.setSize("45px", "24px");
		p_servicemin.insertItem("--", 0);
		p_servicemin.insertItem("00", 1);
		p_servicemin.insertItem("15", 2);
		p_servicemin.insertItem("30", 3);
		p_servicemin.insertItem("45", 4);
		
		
		
		
		p_ampm = new ListBox();
		add(p_ampm,846,120);
		p_ampm.setSize("47px", "24px");
		p_ampm.insertItem("--", 0);
		p_ampm.insertItem("AM", 1);
		p_ampm.insertItem("PM", 2);
		
		
		btnReset = new Button("Reset");
		add(btnReset, 906, 117);
		
		popScheduleTable = new ServiceScheduleTable();
		popScheduleTable.connectToLocal();
		add(popScheduleTable.getTable(),10,160);
		
		btnCancel = new Button("Cancel");
		btnOk=new Button("Ok");
		
		add(btnOk, 370, 615);
		add(btnCancel,520,615);
		
		
		 * nidhi
		 *  5-08-2017
		 *  for make popup responsive
		 *  
		 
		setSize("90%","90%");
		this.getElement().addClassName("scheduleformdiv");
		*//**
		 * end
		 *//*
		this.getElement().setId("form");
		
		
		
		if(filterFlag){
			btnFilter = new Button("Filter");
//			add(btnFilter,906,70);
			
			InlineLabel productsLabel=new InlineLabel("Services");
			add(productsLabel,270,50);
			lbServices = new ListBox();
			lbServices.addItem("--SELECT--");
			add(lbServices,270,70);
			lbServices.setSize("200px", "29px");
		
			InlineLabel branchLabel=new InlineLabel("Customer Branch");
			add(branchLabel,490,50);
			olbCustomerBranch = new ObjectListBox<CustomerBranchDetails>();
			olbCustomerBranch.addItem("--SELECT--");
			add(olbCustomerBranch,490,70);
			olbCustomerBranch.setSize("200px", "29px");
			
			
			InlineLabel servicingBranchLabel=new InlineLabel("Servicing Branch");
			add(servicingBranchLabel,710,50);
			olbServicingBranch = new ObjectListBox<Branch>();
			add(olbServicingBranch,710,70);
			olbServicingBranch.setSize("150px", "29px");
			AppUtility.makeBranchListBoxLive(olbServicingBranch);
		}
	
	*/
		


		serviceScheduleEntity=new ServiceSchedule();
		
		InlineLabel laqoution=new InlineLabel("Service Schedule");
		laqoution.getElement().getStyle().setFontSize(20, Unit.PX);
		add(laqoution,320,10);
	
		/*InlineLabel ladocid=new InlineLabel(documentId);
		add(ladocid, 10, 50);
		p_docid = new TextBox();
		add(p_docid, 10, 70);
		p_docid.setEnabled(false);
		p_docid.setSize("100px", "16px");
		
		InlineLabel docstats=new InlineLabel(documentDate);
		add(docstats, 150, 50);
		
		p_docdate = new DateBoxWithYearSelector();
		DateTimeFormat format1=DateTimeFormat.getFormat("dd-MM-yyyy");
		DateBox.DefaultFormat defformat1=new DateBox.DefaultFormat(format1);
		p_docdate.setFormat(defformat1);
		add(p_docdate, 150, 70);
		p_docdate.setEnabled(false);
		p_docdate.setSize("100px", "16px");
	
		p_default = new RadioButton("Monthly");
		add(p_default,10,120);
		p_default.setSize("100px", "16px");
		p_default.setText("Equal");
		
		
		InlineLabel redate=new InlineLabel("Interval");
		add(redate,110,125);
		
		p_interval = new IntegerBox();
		add(p_interval,160,120);
		p_interval.setSize("60px", "16px");
		p_interval.setName("Interval");
		
		p_customise = new CheckBox();
		add(p_customise,260,120);
		p_customise.setSize("100px", "16px");
		p_customise.setText("Customise");
		
		
		InlineLabel reday=new InlineLabel("Day");
		add(reday,390,124);
		p_mondaytofriday = new ListBox();
		add(p_mondaytofriday,420,120);
		p_mondaytofriday.setSize("100px", "24px");
		
		p_mondaytofriday.insertItem("--Select--", 0);
		p_mondaytofriday.insertItem("Sunday", 1);
		p_mondaytofriday.insertItem("Monday", 2);
		p_mondaytofriday.insertItem("Tuesday", 3);
		p_mondaytofriday.insertItem("Wednesday", 4);
		p_mondaytofriday.insertItem("Thursday",5);
		p_mondaytofriday.insertItem("Friday", 6);
		p_mondaytofriday.insertItem("Saturday", 7);
		
		
		*//**
		 * Date 8 march 2017
		 * added by vijay
		 *//*
		InlineLabel monthWeek=new InlineLabel("Week");
		add(monthWeek,538,124);
		p_serviceWeek = new ListBox();
		add(p_serviceWeek,578,120);
		p_serviceWeek.setSize("45px", "24px");
		p_serviceWeek.insertItem("--", 0);
		p_serviceWeek.insertItem("01", 1);
		p_serviceWeek.insertItem("02", 2);
		p_serviceWeek.insertItem("03", 3);
		p_serviceWeek.insertItem("04", 4);
		
		*//**
		 * ends here
		 *//*

		InlineLabel retime=new InlineLabel("Time");
		add(retime,638,124);
		
		
		InlineLabel hhtime=new InlineLabel("HH");
		add(hhtime,678,124);
		InlineLabel coluntime=new InlineLabel(":");
		add(coluntime,700,124);
		InlineLabel mmtime=new InlineLabel("MM");
		add(mmtime,707,124); 
		
		
		p_servicehours=new ListBox();
		add(p_servicehours,731,120);
		p_servicehours.setSize("45px", "24px");
		p_servicehours.insertItem("--", 0);
		p_servicehours.insertItem("01", 1);
		p_servicehours.insertItem("02", 2);
		p_servicehours.insertItem("03", 3);
		p_servicehours.insertItem("04", 4);
		p_servicehours.insertItem("05", 5);
		p_servicehours.insertItem("06", 6);
		p_servicehours.insertItem("07", 7);
		p_servicehours.insertItem("08", 8);
		p_servicehours.insertItem("09", 9);
		p_servicehours.insertItem("10", 10);
		p_servicehours.insertItem("11", 11);
		p_servicehours.insertItem("12", 12);
		
		InlineLabel colon=new InlineLabel(":");
		add(colon,779,123);
		
		p_servicemin=new ListBox();
		add(p_servicemin,786,120);
		p_servicemin.setSize("45px", "24px");
		p_servicemin.insertItem("--", 0);
		p_servicemin.insertItem("00", 1);
		p_servicemin.insertItem("15", 2);
		p_servicemin.insertItem("30", 3);
		p_servicemin.insertItem("45", 4);
		
		
		
		
		p_ampm = new ListBox();
		add(p_ampm,846,120);
		p_ampm.setSize("47px", "24px");
		p_ampm.insertItem("--", 0);
		p_ampm.insertItem("AM", 1);
		p_ampm.insertItem("PM", 2);
		
		*//**
		 * nidhi
		 * 21-05-2018
		 * for exclude day
		 *//*
		
		InlineLabel exDaye=new InlineLabel("Exclude Day");
		add(exDaye,900,120);
		e_mondaytofriday = new ListBox();
		add(e_mondaytofriday,900,120);
		e_mondaytofriday.setSize("100px", "24px");
		
		e_mondaytofriday.insertItem("--Select--", 0);
		e_mondaytofriday.insertItem("Sunday", 1);
		e_mondaytofriday.insertItem("Monday", 2);
		e_mondaytofriday.insertItem("Tuesday", 3);
		e_mondaytofriday.insertItem("Wednesday", 4);
		e_mondaytofriday.insertItem("Thursday",5);
		e_mondaytofriday.insertItem("Friday", 6);
		e_mondaytofriday.insertItem("Saturday", 7);
		
		
		
		btnReset = new Button("Reset");
		add(btnReset, 1000, 117);*/
		
		
		InlineLabel ladocid=new InlineLabel(documentId);
		add(ladocid, 10, 50);
		p_docid = new TextBox();
		add(p_docid, 10, 70);
		p_docid.setEnabled(false);
		p_docid.setSize("100px", "16px");
		
		InlineLabel docstats=new InlineLabel(documentDate);
		add(docstats, 150, 50);
		
		p_docdate = new DateBoxWithYearSelector();
		DateTimeFormat format1=DateTimeFormat.getFormat("dd-MM-yyyy");
		DateBox.DefaultFormat defformat1=new DateBox.DefaultFormat(format1);
		p_docdate.setFormat(defformat1);
		add(p_docdate, 150, 70);
		p_docdate.setEnabled(false);
		p_docdate.setSize("100px", "16px");
	
		p_default = new RadioButton("Monthly");
		add(p_default,10,120);
		p_default.setSize("100px", "16px");
		p_default.setText("Equal");
		
		
		InlineLabel redate=new InlineLabel("Interval");
		add(redate,110,125);
		
		p_interval = new IntegerBox();
		add(p_interval,160,120);
		p_interval.setSize("60px", "16px");
		p_interval.setName("Interval");
		
		p_customise = new CheckBox();
		add(p_customise,260,120);
		p_customise.setSize("100px", "16px");
		p_customise.setText("Customise");
		
		
		InlineLabel reday=new InlineLabel("Specific Day");
		add(reday,390,124);
		p_mondaytofriday = new ListBox();
		add(p_mondaytofriday,375,120);//460px
		p_mondaytofriday.setSize("100px", "24px");
		
		p_mondaytofriday.insertItem("--Select--", 0);
		p_mondaytofriday.insertItem("Sunday", 1);
		p_mondaytofriday.insertItem("Monday", 2);
		p_mondaytofriday.insertItem("Tuesday", 3);
		p_mondaytofriday.insertItem("Wednesday", 4);
		p_mondaytofriday.insertItem("Thursday",5);
		p_mondaytofriday.insertItem("Friday", 6);
		p_mondaytofriday.insertItem("Saturday", 7);
		
		
		/**
		 * Date 8 march 2017
		 * added by vijay
		 */
		InlineLabel monthWeek=new InlineLabel("Week");
		add(monthWeek,575,124);//538
		p_serviceWeek = new ListBox();
		add(p_serviceWeek,620,120);//578
		p_serviceWeek.setSize("45px", "24px");
		p_serviceWeek.insertItem("--", 0);
		p_serviceWeek.insertItem("01", 1);
		p_serviceWeek.insertItem("02", 2);
		p_serviceWeek.insertItem("03", 3);
		p_serviceWeek.insertItem("04", 4);
		
		/**
		 * ends here
		 */

		InlineLabel retime=new InlineLabel("Time");
		add(retime,678,124);//638 
		
		
		InlineLabel hhtime=new InlineLabel("HH");
		add(hhtime,715,124); //678 715px 708px 680px
		InlineLabel coluntime=new InlineLabel(":");
		add(coluntime,740,124); //740px 
		InlineLabel mmtime=new InlineLabel("MM");
		add(mmtime,750,124); //707
		
		
		p_servicehours=new ListBox();
		add(p_servicehours,790,120); //731
		p_servicehours.setSize("45px", "24px");
		p_servicehours.insertItem("--", 0);
		p_servicehours.insertItem("01", 1);
		p_servicehours.insertItem("02", 2);
		p_servicehours.insertItem("03", 3);
		p_servicehours.insertItem("04", 4);
		p_servicehours.insertItem("05", 5);
		p_servicehours.insertItem("06", 6);
		p_servicehours.insertItem("07", 7);
		p_servicehours.insertItem("08", 8);
		p_servicehours.insertItem("09", 9);
		p_servicehours.insertItem("10", 10);
		p_servicehours.insertItem("11", 11);
		p_servicehours.insertItem("12", 12);
		
		InlineLabel colon=new InlineLabel(":");
		add(colon,838,123); // 779
		
		p_servicemin=new ListBox();
		add(p_servicemin,845,120); //845px
		p_servicemin.setSize("45px", "24px");
		p_servicemin.insertItem("--", 0);
		p_servicemin.insertItem("00", 1);
		p_servicemin.insertItem("15", 2);
		p_servicemin.insertItem("30", 3);
		p_servicemin.insertItem("45", 4);
		
		
		
		
		p_ampm = new ListBox();
		add(p_ampm,895,120); //895px
		p_ampm.setSize("47px", "24px");
		p_ampm.insertItem("--", 0);
		p_ampm.insertItem("AM", 1);
		p_ampm.insertItem("PM", 2);
		
		
		InlineLabel calendar=new InlineLabel("Calendar:");
		add(calendar,960,124); //900
		olbcalendar = new ObjectListBox<Calendar>();
		olbcalendar.setSize("100px", "24px");
		olbcalendar.setListItems(LoginPresenter.globalCalendarlist);
//		AppUtility.makeliveCalendar(olbcalendar);
		add(olbcalendar,1020,120); //990

		Console.log("Calendar");
		
		InlineLabel exDaye=new InlineLabel("Exclude Day:");
		add(exDaye,1150,124); //900
		e_mondaytofriday = new ListBox();
		add(e_mondaytofriday,1230,120); //990
		e_mondaytofriday.setSize("100px", "24px");
		
		e_mondaytofriday.insertItem("--Select--", 0);
		e_mondaytofriday.insertItem("Sunday", 1);
		e_mondaytofriday.insertItem("Monday", 2);
		e_mondaytofriday.insertItem("Tuesday", 3);
		e_mondaytofriday.insertItem("Wednesday", 4);
		e_mondaytofriday.insertItem("Thursday",5);
		e_mondaytofriday.insertItem("Friday", 6);
		e_mondaytofriday.insertItem("Saturday", 7);
		e_mondaytofriday.insertItem(AppConstants.HOLIDAYS, 8);
		
		
		btnReset = new Button("Reset");
		add(btnReset, 1350, 117); //1160px
		
	/*	popScheduleTable = new ServiceScheduleTable();
		popScheduleTable.connectToLocal();
		add(popScheduleTable.getTable(),10,160);
		
		btnCancel = new Button("Cancel");
		btnOk=new Button("Ok");
		
		add(btnOk, 370, 615);
		add(btnCancel,520,615);
		*/
		popScheduleTable = new ServiceScheduleTable();
		popScheduleTable.connectToLocal();
		add(popScheduleTable.getTable(),10,160);
		
		btnCancel = new Button("Cancel");
		btnOk=new Button("Ok");
		
		add(btnOk, 370, 615);
		add(btnCancel,520,615);
		
		/*
		 * nidhi
		 *  5-08-2017
		 *  for make popup responsive
		 *  
		 */
		setSize("90%","90%");
		this.getElement().addClassName("scheduleformdiv");
		/**
		 * end
		 */
		this.getElement().setId("form");
		
		
		
		if(filterFlag){
			btnFilter = new Button("Filter");
//			add(btnFilter,906,70);
			
			InlineLabel productsLabel=new InlineLabel("Services");
			add(productsLabel,270,50);
			lbServices = new ListBox();
			lbServices.addItem("--SELECT--");
			add(lbServices,270,70);
			lbServices.setSize("200px", "29px");
		
			InlineLabel branchLabel=new InlineLabel("Customer Branch");
			add(branchLabel,490,50);
			olbCustomerBranch = new ObjectListBox<CustomerBranchDetails>();
			olbCustomerBranch.addItem("--SELECT--");
			add(olbCustomerBranch,490,70);
			olbCustomerBranch.setSize("200px", "29px");
			
			
			InlineLabel servicingBranchLabel=new InlineLabel("Servicing Branch");
			add(servicingBranchLabel,710,50);
			olbServicingBranch = new ObjectListBox<Branch>();
			add(olbServicingBranch,710,70);
			olbServicingBranch.setSize("150px", "29px");
			AppUtility.makeBranchListBoxLive(olbServicingBranch);
		}
	
	
	
	
	}
	
	/**
	 * Date : 23-11-2017 BY ANIL
	 * initializing customer branch listbox by passing customer list as parameter
	 */
	public void initializeCustomersBranch(ArrayList<CustomerBranchDetails> custList){
		olbCustomerBranch.clear();
		olbCustomerBranch.addItem("--SELECT--");
		if(custList.size()!=0){
			olbCustomerBranch.setListItems(custList);
		}
	}
	/**
	 * Date : 23-11-2017 BY ANIL
	 * initializing service listbox by passing item list as parameter
	 */
	public void initializeServiceListBox(List<SalesLineItem> list){
		lbServices.clear();
		lbServices.addItem("--SELECT--");
		if(list.size()!=0){
			HashSet<String> prodHs= new HashSet<>();
			for(SalesLineItem item:list){
				prodHs.add(item.getProductName());
			}
			ArrayList<String> prodLs=new ArrayList<String>(prodHs);
			for(String obj:prodLs){
				lbServices.addItem(obj);
			}
		}
	}
	
	
	/************************************Getter && Setter*****************************************/
	
		public TextBox getP_docid() {
			return p_docid;
		}
	
		public void setP_docid(TextBox p_docid) {
			this.p_docid = p_docid;
		}
	
		public DateBox getP_docdate() {
			return p_docdate;
		}
	
		public void setP_docdate(DateBox p_docdate) {
			this.p_docdate = p_docdate;
		}
	
		public RadioButton getP_default() {
			return p_default;
		}
	
		public void setP_default(RadioButton p_default) {
			this.p_default = p_default;
		}
	
		
	
		public CheckBox getP_customise() {
			return p_customise;
		}



		public void setP_customise(CheckBox p_customise) {
			this.p_customise = p_customise;
		}



		public Button getBtnCancel() {
			return btnCancel;
		}

		public void setBtnCancel(Button btnCancel) {
			this.btnCancel = btnCancel;
		}

		public Button getBtnOk() {
			return btnOk;
		}

		public void setBtnOk(Button btnOk) {
			this.btnOk = btnOk;
		}

		public ListBox getP_mondaytofriday() {
			return p_mondaytofriday;
		}

		public void setP_mondaytofriday(ListBox p_mondaytofriday) {
			this.p_mondaytofriday = p_mondaytofriday;
		}

		public Button getBtnReset() {
			return btnReset;
		}

		public void setBtnReset(Button btnReset) {
			this.btnReset = btnReset;
		}

		public IntegerBox getP_interval() {
			return p_interval;
		}

		public void setP_interval(IntegerBox p_interval) {
			this.p_interval = p_interval;
		}

		public ServiceScheduleTable getPopScheduleTable() {
			return popScheduleTable;
		}

		public void setPopScheduleTable(ServiceScheduleTable popScheduleTable) {
			this.popScheduleTable = popScheduleTable;
		}

		public ServiceSchedule getServiceScheduleEntity() {
			return serviceScheduleEntity;
		}

		public void setServiceScheduleEntity(ServiceSchedule serviceScheduleEntity) {
			this.serviceScheduleEntity = serviceScheduleEntity;
		}



		



		public ListBox getP_servicehours() {
			return p_servicehours;
		}



		public void setP_servicehours(ListBox p_servicehours) {
			this.p_servicehours = p_servicehours;
		}



		public ListBox getP_servicemin() {
			return p_servicemin;
		}



		public void setP_servicemin(ListBox p_servicemin) {
			this.p_servicemin = p_servicemin;
		}



		public ListBox getP_ampm() {
			return p_ampm;
		}



		public void setP_ampm(ListBox p_ampm) {
			this.p_ampm = p_ampm;
		}



		public ListBox getP_serviceWeek() {
			return p_serviceWeek;
		}



		public void setP_serviceWeek(ListBox p_serviceWeek) {
			this.p_serviceWeek = p_serviceWeek;
		}



		public ListBox getLbServices() {
			return lbServices;
		}



		public void setLbServices(ListBox lbServices) {
			this.lbServices = lbServices;
		}



		public ObjectListBox<Branch> getOlbServicingBranch() {
			return olbServicingBranch;
		}



		public void setOlbServicingBranch(ObjectListBox<Branch> olbServicingBranch) {
			this.olbServicingBranch = olbServicingBranch;
		}



		public ObjectListBox<CustomerBranchDetails> getOlbCustomerBranch() {
			return olbCustomerBranch;
		}



		public void setOlbCustomerBranch(
				ObjectListBox<CustomerBranchDetails> olbCustomerBranch) {
			this.olbCustomerBranch = olbCustomerBranch;
		}



		public Button getBtnFilter() {
			return btnFilter;
		}



		public void setBtnFilter(Button btnFilter) {
			this.btnFilter = btnFilter;
		}



		public  ListBox getE_mondaytofriday() {
			return e_mondaytofriday;
		}



		public  void setE_mondaytofriday(ListBox e_mondaytofriday) {
			e_mondaytofriday = e_mondaytofriday;
		}
		
		
		
		public ObjectListBox<Calendar> getOlbcalendar() {
			return olbcalendar;
		}



		public void setOlbcalendar(ObjectListBox<Calendar> olbcalendar) {
			this.olbcalendar = olbcalendar;
		}



	
}
