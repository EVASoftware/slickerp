package com.slicktechnologies.client.views.smsconfiguration;

import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class SmsConfigurationPresenter extends FormTableScreenPresenter<SmsConfiguration> {
	
	SmsConfigurationForm form;
	final GenricServiceAsync async=GWT.create(GenricService.class);
	public SmsConfigurationPresenter(FormTableScreen<SmsConfiguration> view,SmsConfiguration model) {
		super(view, model);
		
		form = (SmsConfigurationForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getsmsConfigQuery());
		form.setPresenter(this);
		form.btnNBHCDeleteProcess.addClickHandler(this);
		form.btnServiceCancelProcess.addClickHandler(this);
		form.btnProductPriceProcess.addClickHandler(this);
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SMSCONFIG,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	
	
	
	public MyQuerry getsmsConfigQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new SmsConfiguration());
		return quer;
	}
	
	
	
	public static SmsConfigurationForm initalize() {
		
		SmsConfigurationTable gentableScreen=new SmsConfigurationTable();
		
//		SMSConfigurationForm  form=new  SmsTemplateForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		SmsConfigurationForm form = new SmsConfigurationForm(gentableScreen, FormTableScreen.UPPER_MODE, true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		SmsConfigurationPresenter  presenter=new  SmsConfigurationPresenter(form,new SmsConfiguration());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
		
	}

	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {

		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
//		if(text.equals("New"))
//		{
//			form.setToNewState();
//			initalize();
//		}	
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		super.onClick(event);
		
		if(event.getSource().equals(form.btnNBHCDeleteProcess)){
			form.showDialogMessage("PRocess get..!!");
			reactOnServiceDeleteProcess();
		
		}else if(event.getSource().equals(form.btnServiceCancelProcess)){
			reactOnServiceCancelProcess();
		}else if(event.getSource().equals(form.btnProductPriceProcess)){
			reactOnProductItemProcess();
		}
	}
	@Override
	public void reactOnPrint() {
	}

	@Override
	public void reactOnEmail() {
	}

	@Override
	protected void makeNewModel() {
		model=new SmsConfiguration();
		
	}
	
	public void setModel(SmsConfiguration entity)
	{
		model=entity;
	}
	void reactOnServiceDeleteProcess(){
		//nbhcContractServiceCycleDelProcess
		
		
		async.nbhcContractServiceCycleDelProcess(model.getCompanyId(), new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				if(result.equals("Success")){
					form.showDialogMessage("Delete Process Started"+model.getCompanyId());
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
				form.showDialogMessage("Delete Process have some Problem can not delete recordds");
			}
		});
	}


	void reactOnServiceCancelProcess(){

		
		async.contractServiceCycleCanceltionProcess(model.getCompanyId(), new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				if(result.equals("Success")){
					form.showDialogMessage("Process Started");
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
				form.showDialogMessage(" Process have some Problem can not cancel recordds");
			}
		});
	
	}
	
	void reactOnProductItemProcess(){

		
		async.productPriceUtilty(model.getCompanyId(), new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				if(result.equals("Success")){
					form.showDialogMessage("Process Started");
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
				form.showDialogMessage(" Process have some Problem can not cancel recordds");
			}
		});
	
	}
	
}
