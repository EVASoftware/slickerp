package com.slicktechnologies.client.views.smsconfiguration;

import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class SmsConfigurationForm extends FormTableScreen<SmsConfiguration> {
	

	TextBox tbaccountSid, tbauthoToken;
	CheckBox chkStatus;
//	MyLongBox ibfromNumber;
	SmsConfiguration smsConfigObj;
	PasswordTextBox tbpassword;
	
	TextArea taSMSAPIUrl;
	TextBox tbsmsconfigId;
	
	/**
	 * nidhi
	 * 29-09-2017
	 * for delete un usefull service cycle Details
	 */
	Button btnNBHCDeleteProcess,btnServiceCancelProcess,btnProductPriceProcess;
	
	TextBox tbmobileprefix;
	/**
	 * end
	 */
	public SmsConfigurationForm(SuperTable<SmsConfiguration> table, int mode, boolean captionmode){
		super(table, mode, captionmode);
		createGui();
	}
	
	public void initalizeWidget(){
		
		tbaccountSid = new TextBox();
		tbauthoToken = new TextBox();
		chkStatus = new CheckBox();
		chkStatus.setValue(true);
		tbpassword=new PasswordTextBox();
		btnNBHCDeleteProcess=new Button("Service Cycle Delete");
		btnNBHCDeleteProcess.getElement().getStyle().setWidth(120, Unit.PX);
		btnServiceCancelProcess = new Button("Service Canceltion");
		btnServiceCancelProcess.getElement().getStyle().setWidth(120, Unit.PX);
		/**
		 * nidhi 6-11-2018 for product utilty
		 */
		btnProductPriceProcess = new Button("Product Price Utilty");
		btnProductPriceProcess.getElement().getStyle().setWidth(120, Unit.PX);
		
		taSMSAPIUrl = new TextArea();
		tbsmsconfigId = new TextBox();
		tbsmsconfigId.setEnabled(false);
		
		tbmobileprefix = new TextBox();
		
	}
	
	
	@Override
	public void createScreen() {
		super.createScreen();
		
		initalizeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingMessageInformation=fbuilder.setlabel("Message Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Account SID",tbaccountSid);
		FormField ftbaccountSid= fbuilder.setMandatory(true).setMandatoryMsg("Account SID is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Authonticatication Token",tbauthoToken);
		FormField ftbauthoToken = fbuilder.setMandatory(true).setMandatoryMsg("Authontication Token is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Password",tbpassword);
		FormField ftbpassword = fbuilder.setMandatory(true).setMandatoryMsg("From Number is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",chkStatus);
		FormField fchkStatus = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnNBHCDeleteProcess);
		FormField fbtnNBHCDeleteProcess= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnServiceCancelProcess);
		FormField fbtnServiceCancelProcess= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 * @author : nidhi 6-11-2018 for product utilty
		 */
		fbuilder = new FormFieldBuilder("",btnProductPriceProcess);
		FormField fbtnProductPriceProcess= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("SMS API URL",taSMSAPIUrl);
		FormField ftbSMSAPIUrl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("ID",tbsmsconfigId);
		FormField ftbsmsconfigId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Mobile No Prefix",tbmobileprefix);
		FormField ftbmobileprefix= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = { {fgroupingMessageInformation},
				  {ftbaccountSid,ftbauthoToken,ftbpassword,fchkStatus},
				  {fbtnNBHCDeleteProcess,fbtnServiceCancelProcess,fbtnProductPriceProcess},
				  {ftbSMSAPIUrl},
				  {ftbsmsconfigId,ftbmobileprefix},
				  };

		
		this.fields=formfield;		
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForNBHC"))
		{
			btnNBHCDeleteProcess.setVisible(true);
		}
		else{
			btnNBHCDeleteProcess.setVisible(false);

		}

		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ShowHideServiceCancelButton"))
		{
			btnServiceCancelProcess.setVisible(true);
		}
		else{
			btnServiceCancelProcess.setVisible(false);

		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ItemProduct","ShowHideProductPriceProcessButton"))
		{
			btnProductPriceProcess.setVisible(true);
		}
		else{
			btnProductPriceProcess.setVisible(false);

		}
	}
	
	
	
	
	
	@Override
	public void updateModel(SmsConfiguration model) 
	{
		if(tbaccountSid.getValue()!=null)
			model.setAccountSID(tbaccountSid.getValue());
		if(tbauthoToken.getValue()!=null)
			model.setAuthToken(tbauthoToken.getValue());
		if(tbpassword.getValue()!=null)
			model.setPassword(tbpassword.getValue());
		if(chkStatus.getValue()!=null)
			model.setStatus(chkStatus.getValue());
		if(taSMSAPIUrl.getValue()!=null)
			model.setSmsAPIUrl(taSMSAPIUrl.getValue());
		if(tbmobileprefix.getValue()!=null)
			model.setMobileNoPrefix(tbmobileprefix.getValue());
		smsConfigObj=model;
			presenter.setModel(model);
	}

	@Override
	public void updateView(SmsConfiguration view) {
		
		
		smsConfigObj=view;
		if(view.getAccountSID()!=null)
			tbaccountSid.setValue(view.getAccountSID());
		if(view.getAuthToken()!=null)
			tbauthoToken.setValue(view.getAuthToken());
		if(view.getPassword()!=null)
			tbpassword.setValue(view.getPassword());
		
		chkStatus.setValue(view.getStatus());
		if(view.getSmsAPIUrl()!=null)
			taSMSAPIUrl.setValue(view.getSmsAPIUrl());
		
		tbsmsconfigId.setValue(view.getCount()+"");
		
		tbmobileprefix.setValue(view.getMobileNoPrefix());
	
		presenter.setModel(view);
		
	}
	
	
	
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.SMSCONFIG,LoginPresenter.currentModule.trim());
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new SmsConfiguration();
		model=smsConfigObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.SMSCONFIGURATION, smsConfigObj.getCount(), null,null,null, false, model, null);
	
	}
	
	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(smsConfigObj!=null){
			SuperModel model=new SmsConfiguration();
			model=smsConfigObj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.SMSCONFIGURATION, smsConfigObj.getCount(), null,null,null, false, model, null);
		
		}
	}
	
	@Override
	public boolean validate() {
//		return super.validate();
		
		List<SmsConfiguration> list = this.supertable.getDataprovider().getList();
		if(this.chkStatus.getValue()){
			for(SmsConfiguration smsconfig : list){
				if(smsconfig.getStatus()){
					if(tbsmsconfigId.getValue()!=null && !tbsmsconfigId.getValue().equals("")){
						int count = Integer.parseInt(tbsmsconfigId.getValue());
						if(count!= smsconfig.getCount()){
							showDialogMessage("Make only one SMS Configuration Active");
							return false;
						}
					}
					else{
						showDialogMessage("Make only one SMS Configuration Active");
						return false;
					}
					
				}
			}
		}
		
		
		return AppUtility.checkLicenseStatus(AppConstants.LICENSETYPELIST.EVA_SMS_LICENSE);
	}

	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbsmsconfigId.setEnabled(false);
	}	

}
