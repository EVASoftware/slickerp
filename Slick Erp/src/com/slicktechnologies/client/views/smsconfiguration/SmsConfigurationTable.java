package com.slicktechnologies.client.views.smsconfiguration;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;

public class SmsConfigurationTable extends SuperTable<SmsConfiguration> {
	
	TextColumn<SmsConfiguration> getaccountSIDColum;
	TextColumn<SmsConfiguration> getauthoTokenColum;
	TextColumn<SmsConfiguration> getfromNumberColum;
	TextColumn<SmsConfiguration> getStatusColumn;
	TextColumn<SmsConfiguration> getSMSAPIUrlColumn;
	TextColumn<SmsConfiguration> getId;

	
	public SmsConfigurationTable() {
		super();
	}
	
	@Override
	public void createTable() {
		
		addColumngetCount();
		addColumngetAccountSID();
		addColumngetAuthoToken();
		addColumngetFromNumber();
		addColumngetSMSAPIURL();
		addColumngetStatus();
		
	}

	
	

	private void addColumngetCount() {
		
		getId = new TextColumn<SmsConfiguration>() {
			
			@Override
			public String getValue(SmsConfiguration object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getId,"ID");
		table.setColumnWidth(getId, 100,Unit.PX);
		
	
	}

	private void addColumngetAccountSID() {
		
		getaccountSIDColum = new TextColumn<SmsConfiguration>() {
			
			@Override
			public String getValue(SmsConfiguration object) {
				return object.getAccountSID();
			}
		};
		table.addColumn(getaccountSIDColum,"AccountSID");
		table.setColumnWidth(getaccountSIDColum, 100,Unit.PX);
		
	}

	private void addColumngetAuthoToken() {
		getauthoTokenColum = new TextColumn<SmsConfiguration>() {
			
			@Override
			public String getValue(SmsConfiguration object) {
				return object.getAuthToken();
			}
		};
		table.addColumn(getauthoTokenColum,"Authontication Token");
		table.setColumnWidth(getauthoTokenColum, 100,Unit.PX);
	}

	private void addColumngetFromNumber() {
		
		getfromNumberColum = new TextColumn<SmsConfiguration>() {
			
			@Override
			public String getValue(SmsConfiguration object) {
				if(object.getCount()==-1)
				return "N.A";
				else return object.getPassword();
			}
		};
		
		table.addColumn(getfromNumberColum,"From Number");
		table.setColumnWidth(getfromNumberColum, 100,Unit.PX);
		
		
	}
	
	private void addColumngetStatus() {
		
		getStatusColumn = new TextColumn<SmsConfiguration>() {
			
			@Override
			public String getValue(SmsConfiguration object) {
				if(object.getStatus()==true)
				return "Active";
				else return "InActive";
			}
		};
		
		table.addColumn(getStatusColumn,"Status");
		table.setColumnWidth(getStatusColumn,100,Unit.PX);
		
	}

	private void addColumngetSMSAPIURL() {
		
		getSMSAPIUrlColumn = new TextColumn<SmsConfiguration>() {
			
			@Override
			public String getValue(SmsConfiguration object) {
				if(object.getSmsAPIUrl()!=null){
					return object.getSmsAPIUrl();
				}
				else{
					return "N.A";
				}
				 
			}
		};
		
		table.addColumn(getSMSAPIUrlColumn,"SMS API");
		table.setColumnWidth(getSMSAPIUrlColumn, 100,Unit.PX);
		
		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}


}
