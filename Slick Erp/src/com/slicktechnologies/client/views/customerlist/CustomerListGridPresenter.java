package com.slicktechnologies.client.views.customerlist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
//import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.client.views.technicianschedule.TechnicianSchedulingSearchProxy;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;

public class CustomerListGridPresenter implements ClickHandler, com.google.gwt.event.dom.client.ClickHandler {

	static CustomerListGridForm form;
	protected GenricServiceAsync service=GWT.create(GenricService.class);
	
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	public static ArrayList<Customer> globalCustomerlist=new ArrayList<Customer>();

	protected AppMemory mem;
	
	CustomerListSearchProxy searchPopup = new CustomerListSearchProxy();
		
	
	public CustomerListGridPresenter(CustomerListGridForm homeForm) {
		this.form=homeForm;
		
		mem=AppMemory.getAppMemory();
		setEventHandeling();
		searchPopup.getBtnGo().addClickHandler(this);
		
		
	}
	
	private void setEventHandeling() {
		  for(int k=0;k<mem.skeleton.menuLabels.length;k++)
		  {
			 if( mem.skeleton.registration[k]!=null)
				 		mem.skeleton.registration[k].removeHandler();
		  }
		 
		  for( int k=0;k<mem.skeleton.menuLabels.length;k++)
		  
			 mem.skeleton.registration[k]= mem.skeleton.menuLabels[k].addClickHandler(this);	  
	  
	}

	public static void initialize()
	{
		System.out.println("INSIDE CHART INITIALIZATION");
		
		CustomerListGridForm homeForm = new CustomerListGridForm();
		AppMemory.getAppMemory().currentScreen=Screen.HOME;
		AppMemory.getAppMemory().skeleton.getProcessName().setText(AppConstants.SERVICEMODULE+"/"+"Customer List");
		CustomerListGridPresenter presenter= new CustomerListGridPresenter(homeForm);
		AppMemory.getAppMemory().stickPnel(homeForm);	
//		loadCustomer(); //Ashwini Patil Date:23-08-2024 to stop auto loading
	}
	
	
	
	public static void loadCustomer(){
		
		if(globalCustomerlist.size()==0){
			System.out.println("First time on screen click");
			if (LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")) {
			  
				loadAllCustomer();
				
			}else{
				boolean isBranchRestricted=false;
				if(LoginPresenter.globalProcessConfig.size()!=0){
					for(ProcessConfiguration proConf:LoginPresenter.globalProcessConfig){
						if(proConf.getProcessName().equals("Branch")&&proConf.isConfigStatus()==true){
							for(ProcessTypeDetails proTypDet:proConf.getProcessList()){
								if(proTypDet.getProcessType().equalsIgnoreCase("BRANCHLEVELRESTRICTION")&&proTypDet.isStatus()==true){
									isBranchRestricted= true;
									break;
								}
							}
						}
					}
				}
				if(isBranchRestricted){
					HashSet<String> branchSet=new HashSet<String>();
					for(Employee emp:LoginPresenter.globalEmployee){
						if(UserConfiguration.getInfo()!=null&&UserConfiguration.getInfo().getEmpCount()==emp.getCount()){
							if(emp.getEmpBranchList()!=null&&emp.getEmpBranchList().size()!=0){
								for(EmployeeBranch empBranch:emp.getEmpBranchList()){
									branchSet.add(empBranch.getBranchName().trim());
								}
							}
						}
					}
					List<String> list=new ArrayList<String>();
					list.addAll(branchSet);
					
					loadAllCustomerwithBranchLevelRestriction(list);
					
				}else{
					loadAllCustomer();
				}
			
			}	
			
		}else{
			System.out.println("for global customer already loaded"+globalCustomerlist.size());
			form.setDataToListGrid(globalCustomerlist);
		}
	}

	private static void loadAllCustomerwithBranchLevelRestriction(
			List<String> list) {

	
		GenricServiceAsync service=GWT.create(GenricService.class);
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		MyQuerry querry;
		
		if(list.size()!=0){
			filter=new Filter();
			filter.setQuerryString("businessProcess.branch IN");
			filter.setList(list);
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(UserConfiguration.getCompanyId());
			filtervec.add(filter);
		}	
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		querry.setFilters(filtervec);
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("Branch level restriction RESULT SIZE : "+result.size());

				//Date 10-04-2018 By vijay clearing global list
				globalCustomerlist.clear();
				
				for(SuperModel model:result){
					Customer customerEntity=(Customer) model;
					globalCustomerlist.add(customerEntity);
				}
				form.setDataToListGrid(globalCustomerlist);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Failed");
				Console.log("RPC Failed");
			}
		});
	}

	private static void loadAllCustomer() {

		GenricServiceAsync service=GWT.create(GenricService.class);
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		MyQuerry querry;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		querry.setFilters(filtervec);
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("RESULT SIZE : "+result.size());

				//Date 10-04-2018 By vijay clearing global list
				globalCustomerlist.clear();
				
				
				for(SuperModel model:result){
					Customer customerEntity=(Customer) model;
					globalCustomerlist.add(customerEntity);
				}
				form.setDataToListGrid(globalCustomerlist);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Failed");
				Console.log("RPC Failed");
			}
		});
		
	}

//	@Override
//	public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
//		// TODO Auto-generated method stub
//		Console.log("Inside onclick");
//
//		if(event.getSource() == searchPopup.getBtnGo()){
//			Console.log("in searchPopup.getBtnGo() click");
//			if(validateSerachPopup()){
//				reactOnGo();
//			}
//		}
//		if(event.getSource() instanceof InlineLabel)
//		{
//			InlineLabel lbl= (InlineLabel) event.getSource();
//			if(lbl.getText().equals("Download"))
//			{
//				reactOnDownload();
//			}
//			else if(lbl.getText().equals("Search")){
//				Console.log("Search click");
//				reactOnSearch();
//			}
//		}	
//		
//	}

	private void reactOnDownload() {

		System.out.println("hi vijay inside download button");
		
		 ArrayList<Customer> customerlist = new ArrayList<Customer>();
		 ListGridRecord[] allRecords = form.grid.getRecords();
	        if (allRecords != null) {
	            List<ListGridRecord> allRecordsList = Arrays.asList(allRecords);
	           System.out.println("allRecordsList =="+allRecordsList.size());
		   		 for(int i=0;i<allRecordsList.size();i++){
		   			 
		   			 final Customer model=getCustomerDetails(Integer.parseInt(allRecordsList.get(i).getAttribute("CustomerId").trim()));
		   			 if(model!=null){
		   				customerlist.add(model);
		   			 }
		   		 }
	            
	        }

		System.out.println("Customer list size=="+customerlist.size());
		
		csvservice.setcustomerlist(customerlist, new AsyncCallback<Void>(){
    	
    				@Override
    				public void onFailure(Throwable caught) {
    					System.out.println("RPC call Failed"+caught);
    				}
    				
    				@Override
    				public void onSuccess(Void result) {
    		
    					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
    					final String url=gwt + "csvservlet"+"?type="+1;
    					Window.open(url, "test", "enabled");
    				}
    			});
	}

	private Customer getCustomerDetails(int customerId) {

		for(Customer object:globalCustomerlist){
			if(object.getCount()==customerId){
				return object;
			}
		}
		return null;
	}
	
	private boolean validateSerachPopup() {
		Console.log("in validateSerachPopup");
		System.out.println("Search - "+searchPopup.personInfo.getValue());
		/**
		 * updated by: Viraj 
		 * Date: 25-12-2018
		 * Description: Added validation for locality and technician
		 */
		if(searchPopup.getDbFromDate().getValueAsDate()==null && 
			searchPopup.getDbToDate().getValueAsDate() == null &&
			searchPopup.getOlbBranch().getSelectedIndex()==0 &&
			searchPopup.getLbStatus().getSelectedIndex()==0 && 
			searchPopup.personInfo.getValue()==null &&
			searchPopup.getOlbSalesPerson().getValue() == null &&
			searchPopup.getOlbAccountManager().getValue() == null&&
			searchPopup.getOlbConfigCustomerCategory().getValue() == null&&
			searchPopup.getOlbConfigCustomerType().getValue() == null&&
			searchPopup.getOlbCustomerGroup().getValue() == null){
			
			showDialogMessage("Select atleast one field to search! ");
			return false;
		}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BRANCHLEVELRESTRICTION")){
			if (!LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN")) {
			if(searchPopup.getOlbBranch().getSelectedIndex()==0){
				showDialogMessage("Select Branch!");
				return false;
			}
			}
		}
		
		return true;
	}
	private void reactOnGo() {
		// TODO Auto-generated method stub
		Console.log("in reactOnGo");
		searchPopup.getWinModal().hide();
		
		GenricServiceAsync service=GWT.create(GenricService.class);

		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		MyQuerry querry;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);		
		
		
		if(searchPopup.getDbFromDate().getValueAsDate()!=null){
			filter = new Filter();
			filter.setQuerryString("businessProcess.creationDate >=");
			filter.setDateValue(searchPopup.getDbFromDate().getValueAsDate());
			filtervec.add(filter);
		}
		
		if(searchPopup.getDbToDate().getValueAsDate()!=null){
		filter = new Filter();
		filter.setQuerryString("businessProcess.creationDate <=");
		filter.setDateValue(searchPopup.getDbToDate().getValueAsDate());
		filtervec.add(filter);
		}
		
		if(searchPopup.getOlbBranch().getSelectedIndex()!=0){
			filter=new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(searchPopup.getOlbBranch().getValue(searchPopup.getOlbBranch().getSelectedIndex()));
			filtervec.add(filter);
		}
		if(searchPopup.getLbStatus().getSelectedIndex()!=0){
			filter = new Filter();
			filter.setQuerryString("businessProcess.status");
			filter.setStringValue(searchPopup.getLbStatus().getItemText(searchPopup.getLbStatus().getSelectedIndex()).trim());
			filtervec.add(filter);
		}
		
		if(searchPopup.personInfo.getIdValue()!=-1)
		  {
			filter=new Filter();
			filter.setIntValue(searchPopup.personInfo.getIdValue());
			filter.setQuerryString("count");
			filtervec.add(filter);
		  }
		
		if(searchPopup.getOlbConfigCustomerCategory().getSelectedIndex()!=0){
			filter=new Filter();
			filter.setQuerryString("businessProcess.category");
			filter.setStringValue(searchPopup.getOlbConfigCustomerCategory().getValue(searchPopup.getOlbConfigCustomerCategory().getSelectedIndex()));
			filtervec.add(filter);
		}
		if(searchPopup.getOlbConfigCustomerType().getSelectedIndex()!=0){
			filter=new Filter();
			filter.setQuerryString("businessProcess.type");
			filter.setStringValue(searchPopup.getOlbConfigCustomerType().getValue(searchPopup.getOlbConfigCustomerType().getSelectedIndex()));
			filtervec.add(filter);
		}
		if(searchPopup.getOlbCustomerGroup().getSelectedIndex()!=0){
			filter=new Filter();
			filter.setQuerryString("businessProcess.group");
			filter.setStringValue(searchPopup.getOlbCustomerGroup().getValue(searchPopup.getOlbCustomerGroup().getSelectedIndex()));
			filtervec.add(filter);
		}
		
		if(searchPopup.getOlbSalesPerson().getSelectedIndex()!=0){
			filter=new Filter();
			filter.setQuerryString("businessProcess.employee");
			filter.setStringValue(searchPopup.getOlbSalesPerson().getValue(searchPopup.getOlbSalesPerson().getSelectedIndex()));
			filtervec.add(filter);
		}
		if(searchPopup.getOlbAccountManager().getSelectedIndex()!=0){
			filter=new Filter();
			filter.setQuerryString("accountManager");
			filter.setStringValue(searchPopup.getOlbAccountManager().getValue(searchPopup.getOlbAccountManager().getSelectedIndex()));
			filtervec.add(filter);
		}
		/** Ends **/
		
		querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		querry.setFilters(filtervec);
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				Console.log("RESULT SIZE : "+result.size());
				
				globalCustomerlist=new ArrayList<Customer>();
				
				for(SuperModel model:result){
					Customer customerEntity=(Customer) model;
					globalCustomerlist.add(customerEntity);
				}
				form.setDataToListGrid(globalCustomerlist);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Failed");
				Console.log("RPC Failed");
				showDialogMessage("No Result TO Display");
			}
		});
		

		
	}

	private void reactOnSearch() {
		Console.log("in reactOnSearch");
		searchPopup.showPopup();
	}
	public static void showDialogMessage(String Message)
	{
		SC.say(Message);
	}

	@Override
	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
		// TODO Auto-generated method stub
				Console.log("in regular onclick");
	}

	@Override
	public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
		

		// TODO Auto-generated method stub
		Console.log("Inside onclick");

		if(event.getSource() == searchPopup.getBtnGo()){
			Console.log("in searchPopup.getBtnGo() click");
			if(validateSerachPopup()){
				reactOnGo();
			}
		}
		if(event.getSource() instanceof InlineLabel)
		{
			InlineLabel lbl= (InlineLabel) event.getSource();
			if(lbl.getText().equals("Download"))
			{
				reactOnDownload();
			}
			else if(lbl.getText().equals("Search")){
				Console.log("Search click");
				reactOnSearch();
			}
		}	
		
	
	
	}

}
