package com.slicktechnologies.client.views.customerlist;

import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.ViewContainer;

import java.util.ArrayList;
import com.google.gwt.user.client.ui.InlineLabel;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.AutoFitWidthApproach;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class CustomerListGridForm extends ViewContainer{


	ListGrid grid;
	DataSource ds;
	
	
	public CustomerListGridForm() {
		super();
		createGui();
	}
	
	@Override
	protected void createGui() {
		System.out.println("CREATE GUI");
		toggleAppHeaderBarMenu();
		grid = new ListGrid();
		setGridProperty(grid);
		grid.setFields(getGridFields());
		
		ds= new DataSource();
		ds.setClientOnly(true);
		setDataSourceField(ds);
		grid.setDataSource(ds);
		content.add(grid);
		
	}
	
	public void setDataToListGrid(ArrayList<Customer> list){
		System.out.println("FORM SETTING DATA....");
		
		ds.setTestData(getGridData(list));
		grid.setDataSource(ds);
		grid.setFields(getGridFields());
		grid.fetchData();
		
//		grid.redraw();
	}

	@Override
	public void applyStyle() {
		
	}
	
	//grid property
	private static void setGridProperty(ListGrid grid) {
	   grid.setWidth("83%");
	   grid.setHeight("70%");
	   grid.setAutoFetchData(true);
	   grid.setAlternateRecordStyles(true);
	   grid.setShowAllRecords(true);
	   grid.setShowFilterEditor(true);
	   grid.setFilterOnKeypress(true);
	   
	   grid.setWrapCells(true);	
	   grid.setFixedRecordHeights(false);
	   grid.setAutoFitData(Autofit.HORIZONTAL);	
	   grid.setAutoFitWidthApproach(AutoFitWidthApproach.BOTH);	
	   grid.setCanResizeFields(true);
	}
	
	//fields
	private static ListGridField[] getGridFields() {
	   ListGridField field1 = new ListGridField("CustomerId","Customer Id");
	   field1.setWrap(true);
	   field1.setAlign(Alignment.CENTER);
	   
	   ListGridField field2 = new ListGridField("CustomerName","Customer Name");
	   field2.setWrap(true);
	   field2.setAlign(Alignment.CENTER);
	   
	   ListGridField field3 = new ListGridField("CustomerCell","Customer Cell");
	   field3.setWrap(true);
	   field3.setAlign(Alignment.CENTER);
	   
	   ListGridField field4 = new ListGridField("POCName","POC Name");
	   field4.setWrap(true);
	   field4.setAlign(Alignment.CENTER);
	   
	   ListGridField field5 = new ListGridField("BillingAddress","Billing Address");
	   field5.setWrap(true);
	   field5.setAlign(Alignment.CENTER);
	   
	   ListGridField field6 = new ListGridField("BillingAddState","State");
	   field6.setWrap(true);
	   field6.setAlign(Alignment.CENTER);
	   
	   ListGridField field7 = new ListGridField("BillingAddCity","City");
	   field7.setWrap(true);
	   field7.setAlign(Alignment.CENTER);
	   
	   ListGridField field8 = new ListGridField("BillingAddLocality","Locality");
	   field8.setWrap(true);
	   field8.setAlign(Alignment.CENTER);
	   
//	   ListGridField field9 = new ListGridField("BillingAddPin","Pin");
//	   field9.setWrap(true);
//	   field9.setAlign(Alignment.CENTER);
	   
	   ListGridField field10 = new ListGridField("ServiceAddress","Service Address"); 
	   field10.setWrap(true);
	   field10.setAlign(Alignment.CENTER);
	   
	   ListGridField field11 = new ListGridField("State","State");
	   field11.setWrap(true);
	   field11.setAlign(Alignment.CENTER);
	   
	   ListGridField field12 = new ListGridField("City","City");
	   field12.setWrap(true);
	   field12.setAlign(Alignment.CENTER);
	   
	   ListGridField field13 = new ListGridField("Locality","Locality");
	   field13.setWrap(true);
	   field13.setAlign(Alignment.CENTER);
	   
//	   ListGridField field14 = new ListGridField("Pin","Pin");
//	   field14.setWrap(true);
//	   field14.setAlign(Alignment.CENTER);
	   
	   return new ListGridField[] { 
			   field1, field2 ,field3,field4,field5,field6,field7,field8,
			   field10,field11,field12,field13,
			   };
	}
	
	//data source field
	protected static void setDataSourceField(DataSource dataSource) {
		
	   DataSourceField field1 = new DataSourceField("CustomerId", FieldType.TEXT);
	   DataSourceField field2 = new DataSourceField("CustomerName", FieldType.TEXT);
	   DataSourceField field3 = new DataSourceField("CustomerCell", FieldType.TEXT);
	   DataSourceField field4 = new DataSourceField("POCName", FieldType.TEXT);
	   DataSourceField field5 = new DataSourceField("BillingAddress", FieldType.TEXT);
	   DataSourceField field6 = new DataSourceField("BillingAddState", FieldType.TEXT);
	   DataSourceField field7 = new DataSourceField("BillingAddCity", FieldType.TEXT);
	   DataSourceField field8 = new DataSourceField("BillingAddLocality", FieldType.TEXT);
//	   DataSourceField field9 = new DataSourceField("BillingAddPin", FieldType.TEXT);
	   DataSourceField field10 = new DataSourceField("ServiceAddress", FieldType.TEXT);
	   DataSourceField field11 = new DataSourceField("State", FieldType.TEXT);
	   DataSourceField field12 = new DataSourceField("City", FieldType.TEXT);
	   DataSourceField field13 = new DataSourceField("Locality", FieldType.TEXT);
//	   DataSourceField field14 = new DataSourceField("Pin", FieldType.TEXT);
	   
	   dataSource.setFields(field1, field2 ,field3,field4,field5,field6,field7,field8
			   ,field10,field11,field12,field13);
	}
	
	
	 private static ListGridRecord[] getGridData(ArrayList<Customer> customerlist) {
		 
		 ListGridRecord[] records = null; 
		 records = new ListGridRecord[customerlist.size()];
		 for(int i=0;i<customerlist.size();i++){
			 ListGridRecord record = new ListGridRecord();
			 Customer customer=customerlist.get(i);
			 
			 record.setAttribute("CustomerId", customer.getCount());
			 if(customer.isCompany()){
				 record.setAttribute("CustomerName", customer.getCompanyName());
				 record.setAttribute("POCName", customer.getFullname());
			 }else{
				 record.setAttribute("CustomerName", customer.getFullname());
				 record.setAttribute("POCName", customer.getFullname());
			 }
			
			 record.setAttribute("CustomerCell", customer.getCellNumber1());
			 String billingAddress = customer.getAdress().getAddrLine1()+" "+customer.getAdress().getAddrLine2()+" "+customer.getAdress().getLandmark()+" "+customer.getAdress().getLocality() +" "+ customer.getAdress().getPin();
			 System.out.println("billing Address =="+billingAddress);
			 record.setAttribute("BillingAddress", billingAddress);
			 record.setAttribute("BillingAddState",customer.getAdress().getState());
			 record.setAttribute("BillingAddCity", customer.getAdress().getCity());
			 record.setAttribute("BillingAddLocality", customer.getAdress().getLocality());
//			 record.setAttribute("BillingAddPin", customer.getAdress().getPin());
			 String serviceAddress = customer.getSecondaryAdress().getAddrLine1()+" "+customer.getSecondaryAdress().getAddrLine2()+" "+customer.getSecondaryAdress().getLandmark()+" "+customer.getSecondaryAdress().getLocality()+" "+customer.getSecondaryAdress().getPin();
			 record.setAttribute("ServiceAddress", serviceAddress);
			 record.setAttribute("State", customer.getSecondaryAdress().getState());
			 record.setAttribute("City", customer.getSecondaryAdress().getCity());
			 record.setAttribute("Locality", customer.getSecondaryAdress().getLocality());
//			 record.setAttribute("Pin", customer.getSecondaryAdress().getPin());
			 records[i]=record;
		 }
		 System.out.println("RECORDS LENGTH : "+records.length);
		 
		 return records;
	}
	 
	 /**
		 * Toggles the app header bar menus as per screen state
		 */
		public void toggleAppHeaderBarMenu() {
			if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download")||text.equals("Search"))
					{
						menus[k].setVisible(true); 
					}
					else
						menus[k].setVisible(false);  		  		
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download")||text.equals("Search"))
						menus[k].setVisible(true); 
					else
						menus[k].setVisible(false);  		   
				}
			}
			
			else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
			{
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.equals("Download")||text.equals("Search"))
						menus[k].setVisible(true); 
					else
						menus[k].setVisible(false);  		   
				}
			}
			AuthorizationHelper.setAsPerAuthorization(Screen.SERVICE,LoginPresenter.currentModule.trim());
		}

}
