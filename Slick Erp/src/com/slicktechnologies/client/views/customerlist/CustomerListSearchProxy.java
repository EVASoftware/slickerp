package com.slicktechnologies.client.views.customerlist;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;

public class CustomerListSearchProxy {

	
		public PersonInfoComposite personInfo;
		public ObjectListBox<Branch> olbBranch;
		DateItem dbFromDate;
		DateItem dbToDate;
		Button btnGo;
		
		public ObjectListBox<ConfigCategory> olbConfigCustomerCategory;
		public ObjectListBox<Config>olbCustomerGroup;
		public ObjectListBox<Type> olbConfigCustomerType;
		public static ObjectListBox<Employee> olbSalesPerson;
		public ListBox lbStatus;
		ObjectListBox<Employee> olbAccountManager;
		/** Ends **/
		
		Window winModal ;
		
		public CustomerListSearchProxy(){
			
			winModal = new Window();
			winModal.setWidth(400);
	        winModal.setHeight(270);
			winModal.setAutoSize(true);
	        winModal.setTitle(" ");
	        winModal.setShowMinimizeButton(false);
	        winModal.setIsModal(true);
	        winModal.setShowModalMask(true);
	        winModal.centerInPage();
	        winModal.setMargin(4);
	        
	        CreatePopup();
		}

		private void CreatePopup() {

			
			final HLayout hlpersonInfo = new HLayout ();
			hlpersonInfo.setPadding(10);
			hlpersonInfo.setAutoHeight();
			hlpersonInfo.setAutoWidth();
			hlpersonInfo.setMembersMargin(10);
			
	        
			
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(new Customer());
			personInfo=new PersonInfoComposite(querry,false,true,false);
			personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
			personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
			personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
			personInfo.getElement().getStyle().setWidth(500, Unit.PX);
			personInfo.getCustomerId().getHeaderLabel().setStyleName("bold");
			personInfo.getCustomerName().getHeaderLabel().setStyleName("bold");
			personInfo.getCustomerCell().getHeaderLabel().setStyleName("bold");
			personInfo.getPocName().getHeaderLabel().setStyleName("bold");
			hlpersonInfo.addMember(personInfo);
			
			winModal.addItem(hlpersonInfo);
			
//			final HLayout hlbranchFrmDateTodate = new HLayout ();
//			hlbranchFrmDateTodate.setPadding(10);
//			hlbranchFrmDateTodate.setAutoHeight();
//			hlbranchFrmDateTodate.setAutoWidth();
//			hlbranchFrmDateTodate.setMembersMargin(10);
//			hlbranchFrmDateTodate.setLeft(5);
			
			
			 Label lblBranch = new Label("Branch");
			 lblBranch.setSize("110px", "20px");
			 lblBranch.setStyleName("bold");
			
			 Label lblFromDate = new Label("From Date");
			 lblFromDate.setSize("110px", "20px");
			 lblFromDate.setStyleName("bold");

			 
			 Label lblToDate = new Label("To Date");
			 lblToDate.setSize("110px", "20px");
			 lblToDate.setStyleName("bold");

			 
			 Label lblStatus = new Label("Status");
			 lblStatus.setSize("110px", "20px");
			 lblStatus.setStyleName("bold");
			
			 Label lblCategory = new Label("Category");
			 lblCategory.setSize("110px", "20px");
			 lblCategory.setStyleName("bold");
			 
			 Label lblType = new Label("Type");
			 lblType.setSize("140px", "20px");
			 lblType.setStyleName("bold");
			 
			 Label lblGroup = new Label("Group");
			 lblGroup.setSize("110px", "20px");
			 lblGroup.setStyleName("bold");
			 /** Ends **/
			 
			final HLayout lablesLaybout = new HLayout ();
	        lablesLaybout.setPadding(10);
	        lablesLaybout.setAutoHeight();
	        lablesLaybout.setAutoWidth();
	        lablesLaybout.setMembersMargin(10);
	        
	        Label lblbankk = new Label();
	        lblbankk.setSize("0px", "10px");
	        
	        lablesLaybout.addMember(lblbankk);
	        lablesLaybout.addMember(lblFromDate);
	        lablesLaybout.addMember(lblToDate);
	        lablesLaybout.addMember(lblBranch);
	        lablesLaybout.addMember(lblStatus);
	        
	        winModal.addItem(lablesLaybout);
			
			olbBranch= new ObjectListBox<Branch>();
			AppUtility.makeBranchListBoxLive(olbBranch);
			olbBranch.setSize("110px", "22px");
			
			
			olbConfigCustomerCategory= new ObjectListBox<ConfigCategory>();
			AppUtility.MakeLiveCategoryConfig(olbConfigCustomerCategory, Screen.CUSTOMERCATEGORY);
			
			olbConfigCustomerType= new ObjectListBox<Type>();
			AppUtility.makeTypeListBoxLive(olbConfigCustomerType, Screen.CUSTOMERTYPE);
			
			olbCustomerGroup=new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(olbCustomerGroup,Screen.CUSTOMERGROUP);

			olbAccountManager=new ObjectListBox<Employee>();
			AppUtility.makeApproverListBoxLive(olbAccountManager,"Customer");
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","SalesPersonRestriction")
					&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
			{
//				salesPersonRestrictionFlag = true;
				olbSalesPerson=new ObjectListBox<Employee>();
				olbSalesPerson.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.CUSTOMER, "Sales Person");
				Timer t = new Timer() {
					@Override
					public void run() {
						makeSalesPersonEnable();
					}
				};t.schedule(3000);
			}
			else{
				olbSalesPerson=new ObjectListBox<Employee>();
				olbSalesPerson.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.CUSTOMER, "Sales Person");
			}
			
			/** Ends **/
	        
			DynamicForm dynamicform = new DynamicForm();
	        dbFromDate = new DateItem();
	        dbFromDate.setWidth(100);
	        dbFromDate.setUseTextField(true);
	        dbFromDate.setTitle("");
	        
	        dynamicform.setFields(dbFromDate);
			
	        
			DynamicForm dfToDate = new DynamicForm();
	        dbToDate = new DateItem();
	       dbToDate.setWidth(100);
	       dbToDate.setUseTextField(true);
	       dbToDate.setTitle("");
	       dfToDate.setFields(dbToDate);
	       
	       lbStatus= new ListBox();
	       AppUtility.setStatusListBox(lbStatus,Customer.getStatusList());			
	       lbStatus.setSize("110px", "18px");
			
			 
			final HLayout lblfromDateToDatefields = new HLayout ();
			lblfromDateToDatefields.setPadding(10);
			lblfromDateToDatefields.setAutoHeight();
			lblfromDateToDatefields.setAutoWidth();
			lblfromDateToDatefields.setMembersMargin(10); // marginTop
			lblfromDateToDatefields.setStyleName("marginTop");
			
			lblfromDateToDatefields.addMember(dynamicform);
			lblfromDateToDatefields.addMember(dfToDate);
			lblfromDateToDatefields.addMember(olbBranch);
			lblfromDateToDatefields.addMember(lbStatus);
			
			winModal.addItem(lblfromDateToDatefields);
			
			final HLayout lablesLaybout1 = new HLayout ();
	        lablesLaybout1.setPadding(10);
	        lablesLaybout1.setAutoHeight();
	        lablesLaybout1.setAutoWidth();
	        lablesLaybout1.setMembersMargin(10);
	        
	        Label lblbankk1 = new Label();
	        lblbankk1.setSize("0px", "10px");
			
	        lablesLaybout1.addMember(lblbankk1);
	        lablesLaybout1.addMember(lblCategory);			
	        lablesLaybout1.addMember(lblType);
	        lablesLaybout1.addMember(lblGroup);
	        winModal.addItem(lablesLaybout1);
	        
			final HLayout hlbtnTech = new HLayout ();
			hlbtnTech.setPadding(10);
			hlbtnTech.setAutoHeight();
			hlbtnTech.setAutoWidth();
			hlbtnTech.setMembersMargin(10);
			hlbtnTech.setAlign(Alignment.CENTER);
			
			hlbtnTech.addMember(olbConfigCustomerCategory);
			hlbtnTech.addMember(olbConfigCustomerType);
			hlbtnTech.addMember(olbCustomerGroup);
			
			winModal.addItem(hlbtnTech);
			
			final HLayout lablesLaybout2 = new HLayout ();
			lablesLaybout2.setPadding(10);
			lablesLaybout2.setAutoHeight();
			lablesLaybout2.setAutoWidth();
			lablesLaybout2.setMembersMargin(10);
	        
	        lblbankk1 = new Label();
	        lblbankk1.setSize("0px", "10px");
	        
	        Label lblSalesPerson = new Label("Sales Person");
	        lblSalesPerson.setSize("150px", "20px");
	        lblSalesPerson.setStyleName("bold");
			 
			Label lblAccountManager = new Label("Account Manager");
			lblAccountManager.setSize("110px", "20px");
			lblAccountManager.setStyleName("bold");
			
	        lablesLaybout2.addMember(lblbankk1);
	        lablesLaybout2.addMember(lblSalesPerson);			
	        lablesLaybout2.addMember(lblAccountManager);
	        winModal.addItem(lablesLaybout2);
	        
			final HLayout hlbtnTech1 = new HLayout ();
			hlbtnTech1.setPadding(10);
			hlbtnTech1.setAutoHeight();
			hlbtnTech1.setAutoWidth();
			hlbtnTech1.setMembersMargin(10);
			hlbtnTech1.setAlign(Alignment.CENTER);
			
			hlbtnTech1.addMember(olbSalesPerson);
			hlbtnTech1.addMember(olbAccountManager);
			
			winModal.addItem(hlbtnTech1);
			final HLayout hlbtnGo = new HLayout ();
			hlbtnGo.setPadding(10);
			hlbtnGo.setAutoHeight();
			hlbtnGo.setAutoWidth();
			hlbtnGo.setMembersMargin(10);
			hlbtnGo.setAlign(Alignment.CENTER);
			
			btnGo = new Button("Go");
			
			Label lbblak = new Label();
			Label lbblak2 = new Label();

			hlbtnGo.addMember(lbblak);
			hlbtnGo.addMember(lbblak2);

			hlbtnGo.addMember(btnGo);
			
			winModal.addItem(hlbtnGo);


		}

		
	public void showPopup(){
			Console.log(" in showPopup");
		dbFromDate.clearValue();
		dbToDate.clearValue();
		olbBranch.setSelectedIndex(0);
		olbAccountManager.setSelectedIndex(0);  		
		lbStatus.setSelectedIndex(0);  	
		olbConfigCustomerCategory.setSelectedIndex(0);  	
		olbConfigCustomerType.setSelectedIndex(0);  	
		olbCustomerGroup.setSelectedIndex(0);  	
		olbSalesPerson.setSelectedIndex(0);  	
		personInfo.clear();
//		ibContractId.setValue();
		winModal.show();
	}
	
	public void hidePopup(){
		winModal.hide();
	}
	

	/** Ends **/

	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}

	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}

	public DateItem getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateItem dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateItem getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateItem dbToDate) {
		this.dbToDate = dbToDate;
	}

	public Button getBtnGo() {
		return btnGo;
	}

	public void setBtnGo(Button btnGo) {
		this.btnGo = btnGo;
	}

	public Window getWinModal() {
		return winModal;
	}

	public void setWinModal(Window winModal) {
		this.winModal = winModal;
	}
	
	public ObjectListBox<ConfigCategory> getOlbConfigCustomerCategory() {
		return olbConfigCustomerCategory;
	}

	public void setOlbConfigCustomerCategory(ObjectListBox<ConfigCategory> olbConfigCustomerCategory) {
		this.olbConfigCustomerCategory = olbConfigCustomerCategory;
	}

	public ObjectListBox<Config> getOlbCustomerGroup() {
		return olbCustomerGroup;
	}

	public void setOlbCustomerGroup(ObjectListBox<Config> olbCustomerGroup) {
		this.olbCustomerGroup = olbCustomerGroup;
	}

	public ObjectListBox<Type> getOlbConfigCustomerType() {
		return olbConfigCustomerType;
	}

	public void setOlbConfigCustomerType(ObjectListBox<Type> olbConfigCustomerType) {
		this.olbConfigCustomerType = olbConfigCustomerType;
	}

	public static ObjectListBox<Employee> getOlbSalesPerson() {
		return olbSalesPerson;
	}

	public static void setOlbSalesPerson(ObjectListBox<Employee> olbSalesPerson) {
		CustomerListSearchProxy.olbSalesPerson = olbSalesPerson;
	}

	public ListBox getLbStatus() {
		return lbStatus;
	}

	public void setLbStatus(ListBox lbStatus) {
		this.lbStatus = lbStatus;
	}

	public ObjectListBox<Employee> getOlbAccountManager() {
		return olbAccountManager;
	}

	public void setOlbAccountManager(ObjectListBox<Employee> olbAccountManager) {
		this.olbAccountManager = olbAccountManager;
	}

	public static void makeSalesPersonEnable() {
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
			olbSalesPerson.setValue(LoginPresenter.loggedInUser);
			olbSalesPerson.setEnabled(false);
		}
	}
	
	
}

