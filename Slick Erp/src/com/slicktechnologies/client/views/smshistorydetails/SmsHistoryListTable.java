package com.slicktechnologies.client.views.smshistorydetails;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;

public class SmsHistoryListTable extends SuperTable<SmsHistory> {
	
	TextColumn<SmsHistory> getdocumentTypeColumn;
	TextColumn<SmsHistory> getdocumentIdColumn;
	TextColumn<SmsHistory> getNameColumn;
	TextColumn<SmsHistory> getcellNumberColumn;
	TextColumn<SmsHistory> getsmstextColumn;
	TextColumn<SmsHistory> gettransactionDateColumn;
	TextColumn<SmsHistory> gettransactionTimeColumn;
	TextColumn<SmsHistory> getuserIdColumn;
	
	public SmsHistoryListTable(){
		super();
		setHeight("300px");

	}
	
	@Override
	public void createTable() {
		addColumngetdocumentType();
		addColumngetdocumentId();
		addColumngetName();
		addColumngetCellNumber();
		addColumngetSmsText();
		addColumngetTransactionDate();
		addColumngetTransactiontime();
		addColumngetUserId();
		
		
	}

	
	
	
	
	
	private void addColumngetdocumentId() {
		
		getdocumentIdColumn = new TextColumn<SmsHistory>() {
			
			@Override
			public String getValue(SmsHistory object) {
				return object.getDocumentId()+"";
			}
		};
		table.addColumn(getdocumentIdColumn,"Documnet ID");
		table.setColumnWidth(getdocumentIdColumn, 100, Unit.PX);
		getdocumentIdColumn.setSortable(true);
	}



	private void addColumngetdocumentType() {
		
		getdocumentTypeColumn = new TextColumn<SmsHistory>() {

			@Override
			public String getValue(SmsHistory object) {
				return object.getDocumentType();
				
			}
		};
			table.addColumn(getdocumentTypeColumn,"Document Type");
			table.setColumnWidth(getdocumentTypeColumn, 100, Unit.PX);
			getdocumentTypeColumn.setSortable(true);
		
	}




	private void addColumngetName() {
		getNameColumn = new TextColumn<SmsHistory>() {
			
			@Override
			public String getValue(SmsHistory object) {
				return object.getName();
			}
		};
			table.addColumn(getNameColumn,"Name");
			table.setColumnWidth(getNameColumn, 100,Unit.PX);
			getNameColumn.setSortable(true);
	}
	



	private void addColumngetCellNumber() {
			getcellNumberColumn = new TextColumn<SmsHistory>() {
				
				@Override
				public String getValue(SmsHistory object) {
					return object.getCellNo()+"";
				}
			};
			
			table.addColumn(getcellNumberColumn,"Cell No");
			table.setColumnWidth(getcellNumberColumn, 100,Unit.PX);
			getcellNumberColumn.setSortable(true);
	}



	private void addColumngetSmsText() {

		getsmstextColumn = new TextColumn<SmsHistory>() {
			
			@Override
			public String getValue(SmsHistory object) {
				return object.getSmsText();
			}
		};
		table.addColumn(getsmstextColumn,"SMS Text");
		table.setColumnWidth(getsmstextColumn, 100,Unit.PX);
		getsmstextColumn.setSortable(true);
	}



	private void addColumngetTransactionDate() {
		gettransactionDateColumn=new TextColumn<SmsHistory>()
				{
			@Override
			public String getValue(SmsHistory object)
			{
				if(object.getTransactionDate()!=null){
					return AppUtility.parseDate(object.getTransactionDate());
				}
				else{
					return "";
				}
				
			}
				};
		table.addColumn(gettransactionDateColumn,"Transaction Date");
		table.setColumnWidth(gettransactionDateColumn, 110, Unit.PX);
		gettransactionDateColumn.setSortable(true);
	}



	private void addColumngetTransactiontime() {
		gettransactionTimeColumn = new TextColumn<SmsHistory>() {

			@Override
			public String getValue(SmsHistory object) {
				return object.getTransactionTime();
			}
		};
		table.addColumn(gettransactionTimeColumn,"Transaction Time");
		table.setColumnWidth(gettransactionTimeColumn, 100,Unit.PX);
		gettransactionTimeColumn.setSortable(true);
	}



	private void addColumngetUserId() {
		
		getuserIdColumn = new TextColumn<SmsHistory>() {
			
			@Override
			public String getValue(SmsHistory object) {
				return object.getUserId();
			}
		};
		
		table.addColumn(getuserIdColumn,"User ID");
		table.setColumnWidth(getuserIdColumn, 100, Unit.PX);
		getuserIdColumn.setSortable(true);
		
	}


	
	public void addColumnSorting(){
		
		addSortinggetDocumentId();
		addSortinggetDocumentType();
		addSortinggetName();
		addSortinggetCellNo();
		addSortinggetSmsText();
		addSortinggetDate();
		addSortinggetTime();
		addSotinggetUserId();
		
	}
	
	
	
	
	
	
	

	private void addSortinggetDocumentId() {

		List<SmsHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<SmsHistory>(list);
		columnSort.setComparator(getdocumentIdColumn, new Comparator<SmsHistory>()
				{
			@Override
			public int compare(SmsHistory e1,SmsHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getDocumentId()== e2.getDocumentId()){
						return 0;}
					if(e1.getDocumentId()> e2.getDocumentId()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}



	private void addSortinggetDocumentType() {
		
		List<SmsHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<SmsHistory>(list);
		columnSort.setComparator(getdocumentTypeColumn, new Comparator<SmsHistory>()
				{
			@Override
			public int compare(SmsHistory e1,SmsHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDocumentType()!=null && e2.getDocumentType()!=null){
						return e1.getDocumentType().compareTo(e2.getDocumentType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}



	private void addSortinggetName() {
		List<SmsHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<SmsHistory>(list);
		columnSort.setComparator(getNameColumn, new Comparator<SmsHistory>()
				{
			@Override
			public int compare(SmsHistory e1,SmsHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getName()!=null && e2.getName()!=null){
						return e1.getName().compareTo(e2.getName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
		
	}



	private void addSortinggetCellNo() {
		List<SmsHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<SmsHistory>(list);
		columnSort.setComparator(getcellNumberColumn, new Comparator<SmsHistory>()
				{
			@Override
			public int compare(SmsHistory e1,SmsHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCellNo()== e2.getCellNo()){
						return 0;}
					if(e1.getCellNo()> e2.getCellNo()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}



	private void addSortinggetSmsText() {
		List<SmsHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<SmsHistory>(list);
		columnSort.setComparator(getsmstextColumn, new Comparator<SmsHistory>()
				{
			@Override
			public int compare(SmsHistory e1,SmsHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getSmsText()!=null && e2.getSmsText()!=null){
						return e1.getSmsText().compareTo(e2.getSmsText());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
		
		
	}



	private void addSortinggetDate() {

		List<SmsHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<SmsHistory>(list);
		columnSort.setComparator(gettransactionDateColumn, new Comparator<SmsHistory>()
				{
			@Override
			public int compare(SmsHistory e1,SmsHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getTransactionDate()!=null && e2.getTransactionDate()!=null){
						return e1.getTransactionDate().compareTo(e2.getTransactionDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	
		
	}



	private void addSortinggetTime() {

		List<SmsHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<SmsHistory>(list);
		columnSort.setComparator(gettransactionTimeColumn, new Comparator<SmsHistory>()
				{
			@Override
			public int compare(SmsHistory e1,SmsHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getTransactionTime()!=null && e2.getTransactionTime()!=null){
						return e1.getTransactionTime().compareTo(e2.getTransactionTime());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	
	}



	private void addSotinggetUserId() {
		List<SmsHistory> list=getDataprovider().getList();
		columnSort=new ListHandler<SmsHistory>(list);
		columnSort.setComparator(getuserIdColumn, new Comparator<SmsHistory>()
				{
			@Override
			public int compare(SmsHistory e1,SmsHistory e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getUserId()!=null && e2.getUserId()!=null){
						return e1.getUserId().compareTo(e2.getUserId());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}



	@Override
	protected void initializekeyprovider() {
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}


}
