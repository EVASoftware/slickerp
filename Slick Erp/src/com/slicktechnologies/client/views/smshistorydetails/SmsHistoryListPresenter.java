package com.slicktechnologies.client.views.smshistorydetails;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;

public class SmsHistoryListPresenter extends TableScreenPresenter<SmsHistory> {
	
	TableScreen<SmsHistory>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);

	public SmsHistoryListPresenter(TableScreen<SmsHistory> view, SmsHistory model) {
		super(view, model);
		form=view;

		form.getSearchpopupscreen().getDwnload().setVisible(false);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SMSHISTORY,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}

	}
	
	
	public SmsHistoryListPresenter(TableScreen<SmsHistory> view, SmsHistory model,MyQuerry querry) {
		super(view, model);
		form=view;
		view.retriveTable(querry);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.SMSHISTORY,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("")){
			reactTo();
		}
	}
	
	
	
	@Override
	public void reactOnDownload() {
		ArrayList<SmsHistory> smsarray=new ArrayList<SmsHistory>();
		List<SmsHistory> list=form.getSuperTable().getListDataProvider().getList();
		
		smsarray.addAll(list); 
		
		csvservice.setSmsHistoryList(smsarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+91;
				Window.open(url, "test", "enabled");
			}
		});
	}

	private void reactTo() {
		// TODO Auto-generated method stub
		
	}

	/*
	 * Method template to make a new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new SmsHistory();
	}

	public static void initalize()
	{
		SmsHistoryListTable table=new SmsHistoryListTable();
		SmsHistoryListForm form=new SmsHistoryListForm(table);
		SmsHistorySearchPopUp.staticSuperTable=table;
		SmsHistorySearchPopUp searchPopUp=new SmsHistorySearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		SmsHistoryListPresenter presenter=new SmsHistoryListPresenter(form, new SmsHistory());
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	
	
	public static void initalize(MyQuerry querry)
	{
		SmsHistoryListTable table=new SmsHistoryListTable();
		SmsHistoryListForm form=new SmsHistoryListForm(table);
		SmsHistorySearchPopUp.staticSuperTable=table;
		SmsHistorySearchPopUp searchPopUp=new SmsHistorySearchPopUp(false);
		form.setSearchpopupscreen(searchPopUp);
		SmsHistoryListPresenter presenter=new SmsHistoryListPresenter(form, new SmsHistory(),querry);
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	
	/*
	 * Method template to set the query object in specific to search criteria and return the same
	 * @return MyQuerry object
	 */
	public MyQuerry getSmsHistoryQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new SmsHistory());
		return quer;
	}
	
	
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}


}
