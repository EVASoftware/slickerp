package com.slicktechnologies.client.views.smshistorydetails;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;

public class SmsHistorySearchPopUp extends SearchPopUpScreen<SmsHistory> {
	
	DateComparator datecomparator ;
	ListBox lbdocumentType;
	IntegerBox ibdocumenttId;
	MyLongBox ibcellNo;
	TextBox txtuserId;
	
	public SmsHistorySearchPopUp() {
		super();
		createGui();
	}
	
	public SmsHistorySearchPopUp(boolean b) {
		super(b);
		createGui();
	}
	

	public void initWidget(){
		datecomparator = new DateComparator("transactionDate",new SmsHistory());
		lbdocumentType = new ListBox();
		lbdocumentType.addItem("--SELECT--");
		lbdocumentType.addItem("Service");
		lbdocumentType.addItem("Delivery Note Approvable");
		lbdocumentType.addItem("Payment");
		lbdocumentType.addItem("Contract Renewal Due");
		lbdocumentType.addItem("Customer Payment Due");
		lbdocumentType.addItem("Customer Services Due");

		ibdocumenttId = new IntegerBox();
		ibcellNo = new MyLongBox();
		txtuserId = new TextBox();
		
		
	}
	
	
	public void createScreen(){
		initWidget();
		
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Document Id", ibdocumenttId);
		FormField fibdocumentId = builder.setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Documnet Type", lbdocumentType);
		FormField flbdocumenttype = builder.setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Cell No", ibcellNo);
		FormField fcellNo = builder.setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("From Date (Transaction Date)",datecomparator.getFromDate());
		FormField fdatecomparator = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (Transaction Date)",datecomparator.getToDate());
		FormField fdatecomparatorTodate = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("User Id",txtuserId);
		FormField ftxtuserId = builder.setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				{fdatecomparator,fdatecomparatorTodate,fibdocumentId},
				{flbdocumenttype,fcellNo,ftxtuserId},
		};
		
	}
	
	
	@Override
	public MyQuerry getQuerry() {
		
		Vector<Filter> filtrvec = new Vector<Filter>();
		Filter temp = null;
		
		if(datecomparator.getValue()!=null)
			filtrvec.addAll(datecomparator.getValue());
		
		
		if(ibdocumenttId.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(ibdocumenttId.getValue());
			temp.setQuerryString("documentId");
			filtrvec.add(temp);
		}
		
		if(ibcellNo.getValue()!=null){
			temp=new Filter();
			temp.setLongValue(ibcellNo.getValue());
			temp.setQuerryString("cellNo");
			filtrvec.add(temp);
		}
				
		if(lbdocumentType.getSelectedIndex()!=0){
			temp = new Filter();
			temp.setStringValue(lbdocumentType.getItemText(lbdocumentType.getSelectedIndex()));
			temp.setQuerryString("documentType");
			filtrvec.add(temp);
			
		}
		
		
		if(!txtuserId.getValue().trim().equals("")){
			temp = new Filter();
			temp.setStringValue(txtuserId.getValue().trim());
			temp.setQuerryString("userId");
			filtrvec.add(temp);
		}
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtrvec);
		querry.setQuerryObject(new SmsHistory());
		return querry;
	}

	@Override
	public boolean validate() {
		return true;
	}

}
