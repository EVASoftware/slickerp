package com.slicktechnologies.client.views.paymentmodes.vendorpayment;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.paymentmodelayer.vendorpayment.VendorPayment;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class VendorPaymentForm extends FormScreen<VendorPayment> {

	IntegerBox ibVendorPaymentComId;
	TextBox tbVendorPaymentComName, tbVendorPaymentBankName, tbVendorPaymentBranch,
			tbVendorPaymentPayableAt, tbVendorPaymentFavouring,tbVendorPaymentAcNo;
	AddressComposite VendorPaymentBankAddressComposite;
	TextBox tbVendorPaymentIFSCCode, tbVendorPaymentMICRCode, tbVendorPaymentSWIFTCode;
	TextArea taVendorPaymentInstruction;
	CheckBox cbStatus, cbPrint, cbDefault;
	PersonInfoComposite personInfoVendorComposite;

	public VendorPaymentForm() {
		super();
		createGui();
	}

	private void initiliazeWidget() {
		ibVendorPaymentComId = new IntegerBox();
		ibVendorPaymentComId.setEnabled(false);

		tbVendorPaymentComName = new TextBox();
		tbVendorPaymentComName.setEnabled(false);

		cbStatus = new CheckBox();
		cbStatus.setValue(true);

		cbPrint = new CheckBox();
		cbDefault = new CheckBox();

		tbVendorPaymentBankName = new TextBox();
		tbVendorPaymentBranch = new TextBox();
		tbVendorPaymentPayableAt = new TextBox();
		tbVendorPaymentFavouring = new TextBox();

		VendorPaymentBankAddressComposite = new AddressComposite();

		tbVendorPaymentAcNo = new TextBox();
		tbVendorPaymentIFSCCode = new TextBox();
		tbVendorPaymentMICRCode = new TextBox();
		tbVendorPaymentSWIFTCode = new TextBox();

		taVendorPaymentInstruction = new TextArea();
		
		
		MyQuerry vendorquerry = new MyQuerry();
		Filter vendorfilteer = new Filter();
		vendorfilteer.setQuerryString("status");
		vendorfilteer.setBooleanvalue(true);
		vendorquerry.setQuerryObject(new Vendor());
		personInfoVendorComposite = new PersonInfoComposite(vendorquerry);
	}
	
	

	@Override
	public void setCount(int count) {
		ibVendorPaymentComId.setValue(count);
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();

				if (text.equals("Save") || text.equals("Discard")
						|| text.equals("Search")) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
					.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();

				if (text.equals("Discard") || text.equals("Edit")
						|| text.equals("Print") || text.equals("Email")
						|| text.equals("Search") || text.equals(AppConstants.MESSAGE))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.VENDORPAYMENT,LoginPresenter.currentModule.trim());
	}

	@Override
	public void createScreen() {
		initiliazeWidget();

		this.processlevelBarNames = new String[] { "New" };

		// Initialization////////////////////////////////////////////////////
		// Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();

		FormField fgroupingCompanyInformation = fbuilder
				.setlabel("Vendor Information").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Paymnet Mode Id", ibVendorPaymentComId);
		FormField ftbCompanyId = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		//fbuilder = new FormFieldBuilder("* Company Name", tbVendorPaymentComName);
//		FormField ftbCompanyName = fbuilder.setMandatory(true)
//				.setMandatoryMsg("Company Name is mandatory!").setRowSpan(0)
//				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", personInfoVendorComposite);
		FormField fvendorInfoComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Active", cbStatus);
		FormField fcbstatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Print", cbPrint);
		FormField fcbprint = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Defualt", cbDefault);
		FormField fcbdefault = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder();

		FormField fgroupingBankDetails = fbuilder.setlabel("Bank Details")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("* Bank Name", tbVendorPaymentBankName);
		FormField ftbBankName = fbuilder.setMandatory(true)
				.setMandatoryMsg("Bank Name is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Branch", tbVendorPaymentBranch);
		FormField ftbBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch Name is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Payable At", tbVendorPaymentPayableAt);
		FormField ftbPayableAt = fbuilder.setMandatory(true)
				.setMandatoryMsg("Payable At is mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Favouring", tbVendorPaymentFavouring);
		FormField ftbFavouring = fbuilder.setMandatory(true)
				.setMandatoryMsg("Favouring is mandatory!").setRowSpan(0)
				.setColSpan(0).build();

		FormField fgroupingBankAddress = fbuilder.setlabel("Bank Address")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(5).build();

		fbuilder = new FormFieldBuilder("", VendorPaymentBankAddressComposite);
		FormField fVendorPaymentBankAddressComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(4).build();

		FormField fgroupingRTGSDetails = fbuilder
				.setlabel("RTGS / NEFT Details").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("* Account No.", tbVendorPaymentAcNo);
		FormField ftbAccountId = fbuilder.setMandatory(true)
				.setMandatoryMsg("Account Number is Mandatory!").setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("IFSC Code", tbVendorPaymentIFSCCode);
		FormField ftbIFSCCode = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("MICR Code", tbVendorPaymentMICRCode);
		FormField ftbMICRCode = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		fbuilder = new FormFieldBuilder("SWIFT Code", tbVendorPaymentSWIFTCode);
		FormField ftbSWIFTCode = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Instruction", taVendorPaymentInstruction);
		FormField ftbinstruction = fbuilder.setMandatory(false).setRowSpan(2)
				.setColSpan(2).build();

		// //////////////////////////////Lay Out Making
		// Code///////////////////////////////////////////////////////////////
		FormField[][] formfield = {
				{ fgroupingCompanyInformation },
				{ ftbCompanyId,fcbstatus, fcbprint, fcbdefault },
				{ fvendorInfoComposite},
				{ fgroupingBankDetails },
				{ ftbBankName, ftbBranch, ftbPayableAt, ftbFavouring },
				{ fgroupingBankAddress }, { fVendorPaymentBankAddressComposite },
				{ fgroupingRTGSDetails },
				{ ftbAccountId, ftbIFSCCode, ftbMICRCode, ftbSWIFTCode },
				{ ftbinstruction }, };

		this.fields = formfield;
	}

	@Override
	public void updateModel(VendorPayment model) {
		if (tbVendorPaymentBankName.getValue() != null)
			model.setVendorPaymentBankName(tbVendorPaymentBankName.getValue());
		if (cbStatus.getValue() != null)
			model.setVendorPaymentStatus(cbStatus.getValue());
		if (cbPrint.getValue() != null)
			model.setVendorPaymentPrint(cbPrint.getValue());
		if (cbDefault.getValue() != null)
			model.setVendorPaymentDefault(cbDefault.getValue());
		if (tbVendorPaymentBranch.getValue() != null)
			model.setVendorPaymentBranch(tbVendorPaymentBranch.getValue());
		if (tbVendorPaymentPayableAt.getValue() != null)
			model.setVendorPaymentPayableAt(tbVendorPaymentPayableAt.getValue());
		if (tbVendorPaymentFavouring.getValue() != null)
			model.setVendorPaymentFavouring(tbVendorPaymentFavouring.getValue());
		if (VendorPaymentBankAddressComposite.getValue() != null)
			model.setAddressInfo(VendorPaymentBankAddressComposite.getValue());
		if (tbVendorPaymentAcNo.getValue() != null)
			model.setVendorPaymentAccountNo(tbVendorPaymentAcNo.getValue());
		if (tbVendorPaymentIFSCCode.getValue() != null)
			model.setVendorPaymentIFSCcode(tbVendorPaymentIFSCCode.getValue());
		if (tbVendorPaymentMICRCode.getValue() != null)
			model.setVendorPaymentMICRcode(tbVendorPaymentMICRCode.getValue());
		if (tbVendorPaymentSWIFTCode.getValue() != null)
			model.setVendorPaymentSWIFTcode(tbVendorPaymentSWIFTCode.getValue());
		if (taVendorPaymentInstruction.getValue() != null)
			model.setVendorPaymentInstruction(taVendorPaymentInstruction.getValue());
		if (personInfoVendorComposite.getValue() != null) {
			model.setPersonVendorInfo(personInfoVendorComposite.getValue());
		}
	}

	@Override
	public void updateView(VendorPayment view) {
		ibVendorPaymentComId.setValue(view.getCount());
		if (view.getVendorPaymentBankName() != null)
			tbVendorPaymentBankName.setValue(view.getVendorPaymentBankName());
		cbStatus.setValue(view.isVendorPaymentStatus());
		cbPrint.setValue(view.isVendorPaymentPrint());
		cbDefault.setValue(view.isVendorPaymentDefault());
		if (view.getVendorPaymentBranch() != null)
			tbVendorPaymentBranch.setValue(view.getVendorPaymentBranch());
		if (view.getVendorPaymentPayableAt() != null)
			tbVendorPaymentPayableAt.setValue(view.getVendorPaymentPayableAt());
		if (view.getVendorPaymentFavouring() != null)
			tbVendorPaymentFavouring.setValue(view.getVendorPaymentFavouring());
		if (view.getAddressInfo() != null)
			VendorPaymentBankAddressComposite.setValue(view.getAddressInfo());
		if (view.getVendorPaymentAccountNo()!= null)
			tbVendorPaymentAcNo.setValue(view.getVendorPaymentAccountNo());
		if (view.getVendorPaymentIFSCcode() != null)
			tbVendorPaymentIFSCCode.setValue(view.getVendorPaymentIFSCcode());
		if (view.getVendorPaymentMICRcode() != null)
			tbVendorPaymentMICRCode.setValue(view.getVendorPaymentMICRcode());
		if (view.getVendorPaymentSWIFTcode() != null)
			tbVendorPaymentSWIFTCode.setValue(view.getVendorPaymentSWIFTcode());
		if (view.getVendorPaymentInstruction() != null)
			taVendorPaymentInstruction.setValue(view.getVendorPaymentInstruction());
		if (view.getPersonVendorInfo() != null)
			personInfoVendorComposite.setValue(view.getPersonVendorInfo());
		

	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibVendorPaymentComId.setEnabled(false);
		tbVendorPaymentComName.setEnabled(false);
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		Company com=new Company();
		tbVendorPaymentComName.setValue(com.getBusinessUnitName());
	}

	@Override
	public boolean validate() {
		return super.validate();
		
	}


}
