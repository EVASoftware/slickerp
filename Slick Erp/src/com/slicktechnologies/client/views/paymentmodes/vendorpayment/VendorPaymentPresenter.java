package com.slicktechnologies.client.views.paymentmodes.vendorpayment;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.paymentmodelayer.vendorpayment.VendorPayment;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class VendorPaymentPresenter extends FormScreenPresenter<VendorPayment> {
	/**
	 * 
	 */
	public static VendorPaymentForm form;
	String comName = "";
	final GenricServiceAsync async = GWT.create(GenricService.class);
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	/**
	 * Constructor
	 */
	public VendorPaymentPresenter(FormScreen<VendorPayment> view,
			VendorPayment model) {
		super(view, model);
		form = (VendorPaymentForm) view;
		form.setPresenter(this);

		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.VENDORPAYMENT,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	public static VendorPaymentForm initialize() {
		AppMemory.getAppMemory().currentState = ScreeenState.NEW;
		VendorPaymentForm form = new VendorPaymentForm();

		VendorPaymentPresenterTableProxy gentable = new VendorPaymentPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();

		VendorPaymentPresenterSearchProxy.staticSuperTable = gentable;

		VendorPaymentPresenterSearchProxy searchpopup = new VendorPaymentPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		VendorPaymentPresenter presenter = new VendorPaymentPresenter(form,
				new VendorPayment());
		AppMemory.getAppMemory().stickPnel(form);

		return form;

	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals("New")) {
			form.setToNewState();
			initialize();
		}
	}

	@Override
	public void reactOnPrint() {
		final String url = GWT.getModuleBaseURL() + "pdfvendorpayment" + "?Id="
				+ model.getId();
		Window.open(url, "test", "enabled");

	}

	@Override
	public void reactOnEmail() {

	}

	@Override
	protected void makeNewModel() {

	}

	@Override
	public void reactOnDownload() {
		ArrayList<VendorPayment> interactionarray = new ArrayList<VendorPayment>();
		List<VendorPayment> list = (List<VendorPayment>) form
				.getSearchpopupscreen().getSupertable().getDataprovider()
				.getList();

		interactionarray.addAll(list);

		csvservice.setVendorPaymentDetails(interactionarray,new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed" + caught);
					}

					@Override
					public void onSuccess(Void result) {
						String gwt = com.google.gwt.core.client.GWT
								.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 73;
						Window.open(url, "test", "enabled");
					}
				});
	}

}
