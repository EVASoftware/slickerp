package com.slicktechnologies.client.views.paymentmodes.vendorpayment;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.common.paymentmodelayer.vendorpayment.VendorPayment;

public class VendorPaymentPresenterSearchProxy extends SearchPopUpScreen<VendorPayment> {

	IntegerBox ibVendorPaymentComId, ibVendorPaymentId;
	TextBox tbVendorPaymentVendorName, tbVendorPaymentBankName,tbVendorPaymentBranch;
	TextBox tbVendorPaymentIFSCCode, tbVendorPaymentMICRCode,tbVendorPaymentSWIFTCode;

	public VendorPaymentPresenterSearchProxy() {
		super();
		createGui();
	}

	private void initiliazeWidget() {
		ibVendorPaymentComId = new IntegerBox();
		ibVendorPaymentId = new IntegerBox();
		tbVendorPaymentVendorName = new TextBox();
		tbVendorPaymentBankName = new TextBox();
		tbVendorPaymentBranch = new TextBox();
		tbVendorPaymentIFSCCode = new TextBox();
		tbVendorPaymentMICRCode = new TextBox();
		tbVendorPaymentSWIFTCode = new TextBox();
	}

	public void createScreen() {
		initiliazeWidget();

		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("Payment Mode Id", ibVendorPaymentComId);
		FormField fibPaymentModeId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Vendor Id", ibVendorPaymentId);
		FormField ftbVendorId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Vendor Name",tbVendorPaymentVendorName);
		FormField ftbCompanyName = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Bank Name", tbVendorPaymentBankName);
		FormField ftbBankName = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Branch", tbVendorPaymentBranch);
		FormField ftbBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("IFSC Code", tbVendorPaymentIFSCCode);
		FormField ftbIFSCCode = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("MICR Code", tbVendorPaymentMICRCode);
		FormField ftbMICRCode = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("SWIFT Code",tbVendorPaymentSWIFTCode);
		FormField ftbSWIFTCode = fbuilder.setMandatory(false).setMandatoryMsg("SWIFT Code is mandatory!").setRowSpan(0).setColSpan(0).build();


		
		FormField[][] formfield = {
				{ fibPaymentModeId, ftbVendorId,ftbCompanyName,ftbBankName },
				{ ftbBranch,ftbIFSCCode, ftbMICRCode, ftbSWIFTCode }, };
		this.fields = formfield;
	}

	@Override
	public MyQuerry getQuerry() {

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		if (ibVendorPaymentComId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibVendorPaymentComId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		if (ibVendorPaymentId.getValue() != null) {
			temp = new Filter();
			temp.setIntValue(ibVendorPaymentId.getValue());
			temp.setQuerryString("personVendorInfo.count");
			filtervec.add(temp);
		}
		if (tbVendorPaymentVendorName.getValue().trim().equals("") == false) {
			temp = new Filter();
			temp.setStringValue(tbVendorPaymentVendorName.getValue().trim());
			temp.setQuerryString("personVendorInfo.fullName");
			filtervec.add(temp);
		}
		if (tbVendorPaymentBankName.getValue().trim().equals("") == false) {
			temp = new Filter();
			temp.setStringValue(tbVendorPaymentBankName.getValue().trim());
			temp.setQuerryString("vendorPaymentBankName");
			filtervec.add(temp);
		}
		if (tbVendorPaymentBranch.getValue().trim().equals("") == false) {
			temp = new Filter();
			temp.setStringValue(tbVendorPaymentBranch.getValue().trim());
			temp.setQuerryString("vendorPaymentBranch");
			filtervec.add(temp);
		}
		if (tbVendorPaymentIFSCCode.getValue().trim().equals("") == false) {
			temp = new Filter();
			temp.setStringValue(tbVendorPaymentIFSCCode.getValue().trim());
			temp.setQuerryString("vendorPaymentIFSCcode");
			filtervec.add(temp);
		}
		if (tbVendorPaymentMICRCode.getValue().trim().equals("") == false) {
			temp = new Filter();
			temp.setStringValue(tbVendorPaymentMICRCode.getValue().trim());
			temp.setQuerryString("vendorPaymentMICRcode");
			filtervec.add(temp);
		}
		if (tbVendorPaymentSWIFTCode.getValue().trim().equals("") == false) {
			temp = new Filter();
			temp.setStringValue(tbVendorPaymentSWIFTCode.getValue().trim());
			temp.setQuerryString("vendorPaymentSWIFTcode");
			filtervec.add(temp);
		}

		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new VendorPayment());
		return querry;
	}

	@Override
	public boolean validate() {
		return true;
	}

}
