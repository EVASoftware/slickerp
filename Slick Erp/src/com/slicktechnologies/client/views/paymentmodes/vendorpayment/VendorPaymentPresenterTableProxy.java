package com.slicktechnologies.client.views.paymentmodes.vendorpayment;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.paymentmodelayer.vendorpayment.VendorPayment;

public class VendorPaymentPresenterTableProxy extends SuperTable<VendorPayment> {
	
	TextColumn<VendorPayment> getComIdColumn;
	TextColumn<VendorPayment> getVendorIdColumn;
	TextColumn<VendorPayment> getVendorNameColumn;
	
	TextColumn<VendorPayment> getBankNameColumn;
	TextColumn<VendorPayment> getBankBranchColumn;
	TextColumn<VendorPayment> getPayableAtColumn;
	TextColumn<VendorPayment> getFavouringColumn;
	
	TextColumn<VendorPayment> getMICRColumn;
	TextColumn<VendorPayment> getIFSCColumn;
	TextColumn<VendorPayment> getSWIFTColumn;
	
	public VendorPaymentPresenterTableProxy() {
		super();
	}

	@Override
	public void createTable() {
		addColumnComId();
		addColumnVendorId();
		addColumnVendorName();
		addColumnBankName();
		addColumnBranch();
		addColumnPayableAt();
		addColumnFavouring();
		addColumnIFSC();
		addColumnMICR();
		addColumnSWIFT();
	}

	private void addColumnVendorId() {
		getVendorIdColumn = new TextColumn<VendorPayment>() {
			@Override
			public String getValue(VendorPayment object) {
				return object.getPersonVendorInfo().getCount() + "";
			}
		};
		table.addColumn(getVendorIdColumn, "Vendor Id");
		getVendorIdColumn.setSortable(true);
		
	}

	private void addColumnSWIFT() {
		getSWIFTColumn = new TextColumn<VendorPayment>() {
			@Override
			public String getValue(VendorPayment object) {
				return object.getVendorPaymentSWIFTcode() + "";
			}
		};
		table.addColumn(getSWIFTColumn, "SWIFT Code");
		getSWIFTColumn.setSortable(true);
	}

	private void addColumnMICR() {
		getMICRColumn = new TextColumn<VendorPayment>() {
			@Override
			public String getValue(VendorPayment object) {
				return object.getVendorPaymentMICRcode() + "";
			}
		};
		table.addColumn(getMICRColumn, "MICR Code");
		getMICRColumn.setSortable(true);
	}

	private void addColumnIFSC() {
		getIFSCColumn = new TextColumn<VendorPayment>() {
			@Override
			public String getValue(VendorPayment object) {
				return object.getVendorPaymentIFSCcode() + "";
			}
		};
		table.addColumn(getIFSCColumn, "IFSC Code");
		getIFSCColumn.setSortable(true);
	}

	private void addColumnFavouring() {
		getFavouringColumn = new TextColumn<VendorPayment>() {
			@Override
			public String getValue(VendorPayment object) {
				return object.getVendorPaymentFavouring() + "";
			}
		};
		table.addColumn(getFavouringColumn, "Favouring");
		getFavouringColumn.setSortable(true);
	}

	private void addColumnPayableAt() {
		getPayableAtColumn = new TextColumn<VendorPayment>() {
			@Override
			public String getValue(VendorPayment object) {
				return object.getVendorPaymentPayableAt() + "";
			}
		};
		table.addColumn(getPayableAtColumn, "Payable At");
		getPayableAtColumn.setSortable(true);
	}

	private void addColumnBranch() {
		getBankBranchColumn = new TextColumn<VendorPayment>() {
			@Override
			public String getValue(VendorPayment object) {
				return object.getVendorPaymentBranch() + "";
			}
		};
		table.addColumn(getBankBranchColumn, "Branch");
		getBankBranchColumn.setSortable(true);
	}

	private void addColumnBankName() {
		getBankNameColumn = new TextColumn<VendorPayment>() {
			@Override
			public String getValue(VendorPayment object) {
				return object.getVendorPaymentBankName() + "";
			}
		};
		table.addColumn(getBankNameColumn, "Bank Name");
		getBankNameColumn.setSortable(true);
	}

	private void addColumnVendorName() {
		getVendorNameColumn = new TextColumn<VendorPayment>() {
			@Override
			public String getValue(VendorPayment object) {
				return object.getPersonVendorInfo().getFullName() + "";
			}
		};
		table.addColumn(getVendorNameColumn, "Vendor Name");
		getVendorNameColumn.setSortable(true);
	}

	private void addColumnComId() {
		getComIdColumn = new TextColumn<VendorPayment>() {
			@Override
			public String getValue(VendorPayment object) {
				return object.getCount() + "";
			}
		};
		table.addColumn(getComIdColumn, "Payment Mode Id");
		getComIdColumn.setSortable(true);
	}

	public void addColumnSorting(){
		addColumnPaymentIdSorting();
		addColumnVendorIdSorting();
		addColumnVendorNameSorting();
		addColumnBankNameSorting();
		addColumnBranchNameSorting();
		addColumnPayableAtSorting();
		addColumnFavouringSorting();
		addColumnIFSCSorting();
		addColumnMICRSorting();
		addColumnSWIFTSorting();
	}
	
	
	
	private void addColumnPaymentIdSorting() {
		List<VendorPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPayment>(list);
		Comparator<VendorPayment> quote = new Comparator<VendorPayment>() {
			@Override
			public int compare(VendorPayment e1, VendorPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount())
						return 0;
					else if (e1.getCount() < e2.getCount())
						return 1;
					else
						return -1;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getComIdColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnVendorIdSorting() {
		List<VendorPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPayment>(list);
		Comparator<VendorPayment> quote = new Comparator<VendorPayment>() {
			@Override
			public int compare(VendorPayment e1, VendorPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPersonVendorInfo().getCount() == e2.getPersonVendorInfo().getCount())
						return 0;
					else if (e1.getPersonVendorInfo().getCount() < e2.getPersonVendorInfo().getCount())
						return 1;
					else
						return -1;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getVendorIdColumn, quote);
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnVendorNameSorting() {
		List<VendorPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPayment>(list);
		Comparator<VendorPayment> quote = new Comparator<VendorPayment>() {
			@Override
			public int compare(VendorPayment e1, VendorPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPersonVendorInfo().getFullName() != null
							&& e2.getPersonVendorInfo().getFullName()!= null)
						return e1.getPersonVendorInfo().getFullName().compareTo(
								e2.getPersonVendorInfo().getFullName());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getVendorNameColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnBankNameSorting() {
		List<VendorPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPayment>(list);
		Comparator<VendorPayment> quote = new Comparator<VendorPayment>() {
			@Override
			public int compare(VendorPayment e1, VendorPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getVendorPaymentBankName() != null
							&& e2.getVendorPaymentBankName() != null)
						return e1.getVendorPaymentBankName().compareTo(
								e2.getVendorPaymentBankName());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getBankBranchColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	
	
	
	
	
	private void addColumnBranchNameSorting() {
		List<VendorPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPayment>(list);
		Comparator<VendorPayment> quote = new Comparator<VendorPayment>() {
			@Override
			public int compare(VendorPayment e1, VendorPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getVendorPaymentBranch()!= null
							&& e2.getVendorPaymentBranch() != null)
						return e1.getVendorPaymentBranch().compareTo(
								e2.getVendorPaymentBranch());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getBankNameColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnPayableAtSorting() {
		List<VendorPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPayment>(list);
		Comparator<VendorPayment> quote = new Comparator<VendorPayment>() {
			@Override
			public int compare(VendorPayment e1, VendorPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getVendorPaymentPayableAt() != null
							&& e2.getVendorPaymentPayableAt() != null)
						return e1.getVendorPaymentPayableAt().compareTo(
								e2.getVendorPaymentPayableAt());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getPayableAtColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnFavouringSorting() {
		List<VendorPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPayment>(list);
		Comparator<VendorPayment> quote = new Comparator<VendorPayment>() {
			@Override
			public int compare(VendorPayment e1, VendorPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getVendorPaymentFavouring() != null
							&& e2.getVendorPaymentFavouring() != null)
						return e1.getVendorPaymentFavouring().compareTo(
								e2.getVendorPaymentFavouring());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getFavouringColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnIFSCSorting() {
		List<VendorPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPayment>(list);
		Comparator<VendorPayment> quote = new Comparator<VendorPayment>() {
			@Override
			public int compare(VendorPayment e1, VendorPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getVendorPaymentIFSCcode() != null
							&& e2.getVendorPaymentIFSCcode() != null)
						return e1.getVendorPaymentIFSCcode().compareTo(
								e2.getVendorPaymentIFSCcode());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getIFSCColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnMICRSorting() {
		List<VendorPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPayment>(list);
		Comparator<VendorPayment> quote = new Comparator<VendorPayment>() {
			@Override
			public int compare(VendorPayment e1, VendorPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getVendorPaymentMICRcode() != null
							&& e2.getVendorPaymentMICRcode() != null)
						return e1.getVendorPaymentMICRcode().compareTo(
								e2.getVendorPaymentMICRcode());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getMICRColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSWIFTSorting() {
		List<VendorPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<VendorPayment>(list);
		Comparator<VendorPayment> quote = new Comparator<VendorPayment>() {
			@Override
			public int compare(VendorPayment e1, VendorPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getVendorPaymentSWIFTcode() != null
							&& e2.getVendorPaymentSWIFTcode() != null)
						return e1.getVendorPaymentSWIFTcode().compareTo(
								e2.getVendorPaymentSWIFTcode());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getSWIFTColumn, quote);
		table.addColumnSortHandler(columnSort);
	}
	
	
	

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
