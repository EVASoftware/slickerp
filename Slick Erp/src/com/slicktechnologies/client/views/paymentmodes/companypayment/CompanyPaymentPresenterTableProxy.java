package com.slicktechnologies.client.views.paymentmodes.companypayment;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.slicktechnologies.client.views.paymentmodes.companypayment.CompanyPaymentPresenter.CompanyPaymentPresenterTable;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;

public class CompanyPaymentPresenterTableProxy extends CompanyPaymentPresenterTable {
	
	TextColumn<CompanyPayment> getComIdColumn;
	TextColumn<CompanyPayment> getComNameColumn;
	
	TextColumn<CompanyPayment> getBankNameColumn;
	TextColumn<CompanyPayment> getBankBranchColumn;
	TextColumn<CompanyPayment> getPayableAtColumn;
	TextColumn<CompanyPayment> getFavouringColumn;
	
	TextColumn<CompanyPayment> getMICRColumn;
	TextColumn<CompanyPayment> getIFSCColumn;
	TextColumn<CompanyPayment> getSWIFTColumn;
	
	public CompanyPaymentPresenterTableProxy() {
		super();
	}

	@Override
	public void createTable() {
		addColumnComId();
		addColumnComName();
		addColumnBankName();
		addColumnBranch();
		addColumnPayableAt();
		addColumnFavouring();
		addColumnIFSC();
		addColumnMICR();
		addColumnSWIFT();
	}

	private void addColumnSWIFT() {
		getSWIFTColumn = new TextColumn<CompanyPayment>() {
			@Override
			public String getValue(CompanyPayment object) {
				return object.getPaymentSWIFTcode() + "";
			}
		};
		table.addColumn(getSWIFTColumn, "SWIFT Code");
		getSWIFTColumn.setSortable(true);
	}

	private void addColumnMICR() {
		getMICRColumn = new TextColumn<CompanyPayment>() {
			@Override
			public String getValue(CompanyPayment object) {
				return object.getPaymentMICRcode() + "";
			}
		};
		table.addColumn(getMICRColumn, "MICR Code");
		getMICRColumn.setSortable(true);
	}

	private void addColumnIFSC() {
		getIFSCColumn = new TextColumn<CompanyPayment>() {
			@Override
			public String getValue(CompanyPayment object) {
				return object.getPaymentIFSCcode() + "";
			}
		};
		table.addColumn(getIFSCColumn, "IFSC Code");
		getIFSCColumn.setSortable(true);
	}

	private void addColumnFavouring() {
		getFavouringColumn = new TextColumn<CompanyPayment>() {
			@Override
			public String getValue(CompanyPayment object) {
				return object.getPaymentFavouring() + "";
			}
		};
		table.addColumn(getFavouringColumn, "Favouring");
		getFavouringColumn.setSortable(true);
	}

	private void addColumnPayableAt() {
		getPayableAtColumn = new TextColumn<CompanyPayment>() {
			@Override
			public String getValue(CompanyPayment object) {
				return object.getPaymentPayableAt() + "";
			}
		};
		table.addColumn(getPayableAtColumn, "Payable At");
		getPayableAtColumn.setSortable(true);
	}

	private void addColumnBranch() {
		getBankBranchColumn = new TextColumn<CompanyPayment>() {
			@Override
			public String getValue(CompanyPayment object) {
				return object.getPaymentBranch() + "";
			}
		};
		table.addColumn(getBankBranchColumn, "Branch");
		getBankBranchColumn.setSortable(true);
	}

	private void addColumnBankName() {
		getBankNameColumn = new TextColumn<CompanyPayment>() {
			@Override
			public String getValue(CompanyPayment object) {
				return object.getPaymentBankName() + "";
			}
		};
		table.addColumn(getBankNameColumn, "Bank Name");
		getBankNameColumn.setSortable(true);
	}

	private void addColumnComName() {
		getComNameColumn = new TextColumn<CompanyPayment>() {
			@Override
			public String getValue(CompanyPayment object) {
				return object.getPaymentComName() + "";
			}
		};
		table.addColumn(getComNameColumn, "Comp Name");
		getComNameColumn.setSortable(true);
	}

	private void addColumnComId() {
		getComIdColumn = new TextColumn<CompanyPayment>() {
			@Override
			public String getValue(CompanyPayment object) {
				return object.getCount() + "";
			}
		};
		table.addColumn(getComIdColumn, "Payment Mode Id");
		getComIdColumn.setSortable(true);
	}

	
	public void addColumnSorting(){
		addColumnPaymentId();
		addColumnSortCompanyName();
		addColumnSortBankname();
		addColumnSortBranch();
		addColumnSortPayableAt();
		addColumnSortFavouring();
		addColumnSortIFSC();
		addColumnSortMICR();
		addColumnSortSwift();
	}
	
	
	
	private void addColumnSortSwift() {
		List<CompanyPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyPayment>(list);
		Comparator<CompanyPayment> quote = new Comparator<CompanyPayment>() {
			@Override
			public int compare(CompanyPayment e1, CompanyPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPaymentSWIFTcode() != null
							&& e2.getPaymentSWIFTcode() != null)
						return e1.getPaymentSWIFTcode().compareTo(
								e2.getPaymentSWIFTcode());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getSWIFTColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortMICR() {
		List<CompanyPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyPayment>(list);
		Comparator<CompanyPayment> quote = new Comparator<CompanyPayment>() {
			@Override
			public int compare(CompanyPayment e1, CompanyPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPaymentMICRcode() != null
							&& e2.getPaymentMICRcode() != null)
						return e1.getPaymentMICRcode().compareTo(
								e2.getPaymentMICRcode());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getMICRColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortIFSC() {
		List<CompanyPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyPayment>(list);
		Comparator<CompanyPayment> quote = new Comparator<CompanyPayment>() {
			@Override
			public int compare(CompanyPayment e1, CompanyPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPaymentIFSCcode() != null
							&& e2.getPaymentIFSCcode() != null)
						return e1.getPaymentIFSCcode().compareTo(
								e2.getPaymentIFSCcode());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getIFSCColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortFavouring() {
		List<CompanyPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyPayment>(list);
		Comparator<CompanyPayment> quote = new Comparator<CompanyPayment>() {
			@Override
			public int compare(CompanyPayment e1, CompanyPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPaymentFavouring() != null
							&& e2.getPaymentFavouring() != null)
						return e1.getPaymentFavouring().compareTo(
								e2.getPaymentFavouring());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getFavouringColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortPayableAt() {
		List<CompanyPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyPayment>(list);
		Comparator<CompanyPayment> quote = new Comparator<CompanyPayment>() {
			@Override
			public int compare(CompanyPayment e1, CompanyPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPaymentPayableAt() != null
							&& e2.getPaymentPayableAt() != null)
						return e1.getPaymentPayableAt().compareTo(
								e2.getPaymentPayableAt());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getPayableAtColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortBranch() {
		List<CompanyPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyPayment>(list);
		Comparator<CompanyPayment> quote = new Comparator<CompanyPayment>() {
			@Override
			public int compare(CompanyPayment e1, CompanyPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPaymentBranch() != null
							&& e2.getPaymentBranch() != null)
						return e1.getPaymentBranch().compareTo(
								e2.getPaymentBranch());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getBankBranchColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortBankname() {
		List<CompanyPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyPayment>(list);
		Comparator<CompanyPayment> quote = new Comparator<CompanyPayment>() {
			@Override
			public int compare(CompanyPayment e1, CompanyPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPaymentBankName() != null
							&& e2.getPaymentBankName() != null)
						return e1.getPaymentBankName().compareTo(
								e2.getPaymentBankName());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getBankNameColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnSortCompanyName() {
		List<CompanyPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyPayment>(list);
		Comparator<CompanyPayment> quote = new Comparator<CompanyPayment>() {
			@Override
			public int compare(CompanyPayment e1, CompanyPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPaymentComName() != null
							&& e2.getPaymentComName() != null)
						return e1.getPaymentComName().compareTo(
								e2.getPaymentComName());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getComNameColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnPaymentId() {
		List<CompanyPayment> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyPayment>(list);
		Comparator<CompanyPayment> quote = new Comparator<CompanyPayment>() {
			@Override
			public int compare(CompanyPayment e1, CompanyPayment e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount())
						return 0;
					else if (e1.getCount() < e2.getCount())
						return 1;
					else
						return -1;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getComIdColumn, quote);
		table.addColumnSortHandler(columnSort);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
