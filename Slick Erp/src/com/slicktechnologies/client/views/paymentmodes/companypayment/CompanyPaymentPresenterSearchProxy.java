package com.slicktechnologies.client.views.paymentmodes.companypayment;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.views.paymentmodes.companypayment.CompanyPaymentPresenter.CompanyPresenterPresenterSearch;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;

public class CompanyPaymentPresenterSearchProxy extends CompanyPresenterPresenterSearch{
	
	/**
	 * Form Component Defined Here.
	 */
	
	IntegerBox ibPaymentComId;
	TextBox tbPaymentBankName,tbPaymentBranch;
	TextBox tbPaymentIFSCCode,tbPaymentMICRCode,tbPaymentSWIFTCode;
	
	public CompanyPaymentPresenterSearchProxy() {
		super();
		createGui();
	}
	
	/**
	 * Form Component Initialization
	 */
	
	private void initiliazeWidget(){
		ibPaymentComId=new IntegerBox();
		tbPaymentBankName=new TextBox();
		tbPaymentBranch=new TextBox();
		tbPaymentIFSCCode=new TextBox();
		tbPaymentMICRCode=new TextBox();
		tbPaymentSWIFTCode=new TextBox();
	}
	
	/**
	 * Creating Form Screen.
	 */
	public void createScreen() {
		initiliazeWidget();
		
				FormFieldBuilder fbuilder;
				fbuilder = new FormFieldBuilder();
				
				fbuilder = new FormFieldBuilder("Payment Mode Id",ibPaymentComId);
				FormField ftbCompanyId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
				
				fbuilder = new FormFieldBuilder("Bank Name",tbPaymentBankName);
				FormField ftbBankName= fbuilder.setMandatory(false).setMandatoryMsg("Bank Name is mandatory!").setRowSpan(0).setColSpan(0).build();
				fbuilder = new FormFieldBuilder("Branch",tbPaymentBranch);
				FormField ftbBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

				fbuilder = new FormFieldBuilder("IFSC Code",tbPaymentIFSCCode);
				FormField ftbIFSCCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
				fbuilder = new FormFieldBuilder("MICR Code",tbPaymentMICRCode);
				FormField ftbMICRCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
				fbuilder = new FormFieldBuilder("SWIFT Code",tbPaymentSWIFTCode);
				FormField ftbSWIFTCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
				

				////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
				FormField[][] formfield = { 
						{ftbCompanyId,ftbBankName,ftbBranch},
						{ftbIFSCCode,ftbMICRCode,ftbSWIFTCode},
				};

				this.fields=formfield;
	}
	
	/**
	 * This Method take one or more input and Returns Search Result.
	 */

	@Override
	public MyQuerry getQuerry() {
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		if(ibPaymentComId.getValue()!=null)
		{
			temp=new Filter();
			temp.setIntValue(ibPaymentComId.getValue());
			temp.setQuerryString("count");
			filtervec.add(temp);
		}
		if(tbPaymentBankName.getValue().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(tbPaymentBankName.getValue().trim());
			temp.setQuerryString("paymentBankName");
			filtervec.add(temp);
		}
		if(tbPaymentBranch.getValue().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(tbPaymentBranch.getValue().trim());
			temp.setQuerryString("paymentBranch");
			filtervec.add(temp);
		}
		if(tbPaymentIFSCCode.getValue().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(tbPaymentIFSCCode.getValue().trim());
			temp.setQuerryString("paymentIFSCcode");
			filtervec.add(temp);
		}
		if(tbPaymentMICRCode.getValue().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(tbPaymentMICRCode.getValue().trim());
			temp.setQuerryString("paymentMICRcode");
			filtervec.add(temp);
		}
		if(tbPaymentSWIFTCode.getValue().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(tbPaymentSWIFTCode.getValue().trim());
			temp.setQuerryString("paymentSWIFTcode");
			filtervec.add(temp);
		}
		
		MyQuerry querry=new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CompanyPayment());
		return querry;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return super.validate();
	}
	
	

}
