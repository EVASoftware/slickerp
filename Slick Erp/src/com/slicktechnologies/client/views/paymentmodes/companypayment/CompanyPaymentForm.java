package com.slicktechnologies.client.views.paymentmodes.companypayment;

import org.apache.poi.ss.usermodel.Textbox;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.articletypecomposite.ArticleTypeComposite;
import com.slicktechnologies.client.views.composites.articletypecomposite.ArticleTypeTable;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.BranchUsernamePassword;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class CompanyPaymentForm extends FormScreen<CompanyPayment> implements ClickHandler {
/**
 * Form Component Defined Here.
 */
	IntegerBox ibPaymentComId, ibGooglePayNumber;
	TextBox tbPaymentComName, tbPaymentBankName, tbPaymentBranch,
			tbPaymentPayableAt, tbPaymentFavouring,tbPaymentAcNo;
	AddressComposite paymentBankAddressComposite;
	TextBox tbPaymentIFSCCode, tbPaymentMICRCode, tbPaymentSWIFTCode, tbPaymentIBANNo;
	TextArea taPaymentInstruction;
	CheckBox cbStatus, cbPrint, cbDefault;

	UploadComposite uploadQrCode;
	
	ObjectListBox<Config> olbPaymentType;
	TextBox tbPaymentDetails;
	Button btnAddPaymentDetail;
	ArticleTypeTable articletable;
	public static boolean isCallFromPaymentMode=true;

	public CompanyPaymentForm() {
		super();
		createGui();
	}
	
	public CompanyPaymentForm(boolean popupFlag) {
		super(popupFlag);
		createGui();
	}
	
	public CompanyPaymentForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		
		
	}
/**
 * Form Component Initialization
 */ 
	private void initiliazeWidget() {
		ibPaymentComId = new IntegerBox();
		ibPaymentComId.setEnabled(false);

		tbPaymentComName = new TextBox();
		tbPaymentComName.setEnabled(false);

		cbStatus = new CheckBox();
		cbStatus.setValue(true);

		cbPrint = new CheckBox();
		cbPrint.addClickHandler(this);
		cbDefault = new CheckBox();
		cbDefault.addClickHandler(this);
		tbPaymentBankName = new TextBox();
		tbPaymentBranch = new TextBox();
		tbPaymentPayableAt = new TextBox();
		tbPaymentFavouring = new TextBox();

		paymentBankAddressComposite = new AddressComposite();

		tbPaymentAcNo = new TextBox();
		tbPaymentIFSCCode = new TextBox();
		tbPaymentMICRCode = new TextBox();
		tbPaymentSWIFTCode = new TextBox();
		/*Added by sheetal:30-11-2021 for IBAN No*/
		tbPaymentIBANNo = new TextBox();

		taPaymentInstruction = new TextArea();
		
		uploadQrCode = new UploadComposite();
		
		ibGooglePayNumber = new IntegerBox();
		
		olbPaymentType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbPaymentType,Screen.COMPANYPAYMENTTYPE);
		tbPaymentDetails = new TextBox();
		btnAddPaymentDetail=new Button("ADD");
		btnAddPaymentDetail.addClickHandler(this);
		isCallFromPaymentMode=true;
		articletable=new ArticleTypeTable();
		articletable.connectToLocal();
	}
	
	/**
	 * Creating Form Screen.
	 */
	@Override
	public void createScreen() {
		initiliazeWidget();

		this.processlevelBarNames = new String[] { "New" };

		// Initialization////////////////////////////////////////////////////
		// Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCompanyInformation = fbuilder.setlabel("Company Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Paymnet Mode Id", ibPaymentComId);
		FormField ftbCompanyId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Company Name", tbPaymentComName);
		FormField ftbCompanyName = fbuilder.setMandatory(true).setMandatoryMsg("Company Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Active", cbStatus);
		FormField fcbstatus = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Print", cbPrint);
		FormField fcbprint = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Defualt", cbDefault);
		FormField fcbdefault = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();

		FormField fgroupingBankDetails = fbuilder.setlabel("Bank Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("* Bank Name", tbPaymentBankName);
		FormField ftbBankName = fbuilder.setMandatory(true).setMandatoryMsg("Bank Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Branch", tbPaymentBranch);
		FormField ftbBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Payable At", tbPaymentPayableAt);
		FormField ftbPayableAt = fbuilder.setMandatory(true).setMandatoryMsg("Payable At is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Favouring", tbPaymentFavouring);
		FormField ftbFavouring = fbuilder.setMandatory(true).setMandatoryMsg("Favouring is mandatory!").setRowSpan(0).setColSpan(0).build();

		FormField fgroupingBankAddress = fbuilder.setlabel("Bank Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("", paymentBankAddressComposite);
		FormField fpaymentBankAddressComposite = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		FormField fgroupingRTGSDetails = fbuilder.setlabel("RTGS / NEFT Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		fbuilder = new FormFieldBuilder("* Account No.", tbPaymentAcNo);
		FormField ftbAccountId = fbuilder.setMandatory(true).setMandatoryMsg("Account Number is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("IFSC Code", tbPaymentIFSCCode);
		FormField ftbIFSCCode = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("MICR Code", tbPaymentMICRCode);
		FormField ftbMICRCode = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("SWIFT Code", tbPaymentSWIFTCode);
		FormField ftbSWIFTCode = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("IBAN No", tbPaymentIBANNo);
		FormField ftbIBANNo = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Instruction", taPaymentInstruction);
		FormField ftbinstruction = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();

		FormField fgroupingUploadQRCode = fbuilder.setlabel("Upload QR Code").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("(Size : 225x225 px)",uploadQrCode);
	    FormField fuploadQRCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	      
	    fbuilder = new FormFieldBuilder("Google/Phone Pay Number", ibGooglePayNumber);
		FormField fibGooglePayNumber = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	        
		fbuilder = new FormFieldBuilder("Payment Type",olbPaymentType);
        FormField folbPaymentType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        
        fbuilder = new FormFieldBuilder("Payment Details",tbPaymentDetails);
        FormField ftbPaymentDetails= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        
        fbuilder = new FormFieldBuilder("",btnAddPaymentDetail);
        FormField fbtnAddPaymentDetail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        
        fbuilder = new FormFieldBuilder("",articletable.getTable());
        FormField farticletable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
        
		// //////////////////////////////Lay Out Making
		// Code///////////////////////////////////////////////////////////////
		FormField[][] formfield = {
				{ fgroupingCompanyInformation },
				{ ftbCompanyId, ftbCompanyName, fcbstatus, fcbprint, fcbdefault },
				{ fgroupingBankDetails },
				{ ftbBankName, ftbBranch, ftbPayableAt, ftbFavouring },
				{fpaymentBankAddressComposite},
//				{ fgroupingBankAddress }, { fpaymentBankAddressComposite },
				{ fgroupingRTGSDetails },
				{ ftbAccountId, ftbIFSCCode, ftbMICRCode, ftbSWIFTCode,ftbIBANNo},
//				{fibGooglePayNumber},
				{folbPaymentType,ftbPaymentDetails,fbtnAddPaymentDetail},
				{farticletable},
				{ ftbinstruction },
				{fgroupingUploadQRCode},
				{fuploadQRCode},
				};

		this.fields = formfield;
	}
	
	/**
	 * Form Header That Content button in Blue Background.
	 */
	@Override
	public void toggleAppHeaderBarMenu() {
		if (AppMemory.getAppMemory().currentState == ScreeenState.NEW) {
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
//					.getMenuLabels();
			
			/**
			 * @author Vijay Date - 24-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();

				if (text.equals("Save") || text.equals("Discard")
						|| text.equals("Search")) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.EDIT) {
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
//					.getMenuLabels();
			/**
			 * @author Vijay Date - 24-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.equals("Save") || text.equals("Discard"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}

		else if (AppMemory.getAppMemory().currentState == ScreeenState.VIEW) {
//			InlineLabel[] menus = AppMemory.getAppMemory().skeleton
//					.getMenuLabels();
			/**
			 * @author Vijay Date - 24-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();

				if (text.equals("Discard") || text.equals("Edit")
						|| text.equals("Print")|| text.equals("Search"))
					menus[k].setVisible(true);

				else
					menus[k].setVisible(false);
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.COMPANYPAYMENT,LoginPresenter.currentModule.trim());
	}
	
	
	/**
	 * Saving Form Details
	 */
	@Override
	public void updateModel(CompanyPayment model) {
		if (tbPaymentComName.getValue() != null)
			model.setPaymentComName(tbPaymentComName.getValue());
		if (tbPaymentBankName.getValue() != null)
			model.setPaymentBankName(tbPaymentBankName.getValue());
		if (cbStatus.getValue() != null)
			model.setPaymentStatus(cbStatus.getValue());
		if (cbPrint.getValue() != null)
			model.setPaymentPrint(cbPrint.getValue());
		if (cbDefault.getValue() != null)
			model.setPaymentDefault(cbDefault.getValue());
		if (tbPaymentBranch.getValue() != null)
			model.setPaymentBranch(tbPaymentBranch.getValue());
		if (tbPaymentPayableAt.getValue() != null)
			model.setPaymentPayableAt(tbPaymentPayableAt.getValue());
		if (tbPaymentFavouring.getValue() != null)
			model.setPaymentFavouring(tbPaymentFavouring.getValue());
		if (paymentBankAddressComposite.getValue() != null)
			model.setAddressInfo(paymentBankAddressComposite.getValue());
		if (tbPaymentAcNo.getValue() != null)
			model.setPaymentAccountNo(tbPaymentAcNo.getValue());
		if (tbPaymentIFSCCode.getValue() != null)
			model.setPaymentIFSCcode(tbPaymentIFSCCode.getValue());
		if (tbPaymentMICRCode.getValue() != null)
			model.setPaymentMICRcode(tbPaymentMICRCode.getValue());
		if (tbPaymentSWIFTCode.getValue() != null)
			model.setPaymentSWIFTcode(tbPaymentSWIFTCode.getValue());
		if (tbPaymentIBANNo.getValue() != null)
			model.setPaymentIBANNo(tbPaymentIBANNo.getValue());
		
		if (taPaymentInstruction.getValue() != null)
			model.setPaymentInstruction(taPaymentInstruction.getValue());
		
		if(uploadQrCode.getValue()!=null)
			model.setQrCodeDocument(uploadQrCode.getValue());
		
		if(ibGooglePayNumber.getValue()!=null) {
			model.setGooglePhonePayNo(ibGooglePayNumber.getValue());
		}
		
		if(articletable.getValue()!=null){
			model.setArticleTypeDetails(articletable.getValue());
		}
		
		presenter.setModel(model);
	}
	
	/**
	 * Set view 	
	 */
	@Override
	public void updateView(CompanyPayment view) {
		ibPaymentComId.setValue(view.getCount());
			tbPaymentComName.setValue(view.getPaymentComName());
		if (view.getPaymentBankName() != null)
			tbPaymentBankName.setValue(view.getPaymentBankName());
		cbStatus.setValue(view.isPaymentStatus());
		cbPrint.setValue(view.isPaymentPrint());
		cbDefault.setValue(view.isPaymentDefault());
		if (view.getPaymentBranch() != null)
			tbPaymentBranch.setValue(view.getPaymentBranch());
		if (view.getPaymentPayableAt() != null)
			tbPaymentPayableAt.setValue(view.getPaymentPayableAt());
		if (view.getPaymentFavouring() != null)
			tbPaymentFavouring.setValue(view.getPaymentFavouring());
		if (view.getAddressInfo() != null)
			paymentBankAddressComposite.setValue(view.getAddressInfo());
		if (view.getPaymentAccountNo()!= null)
			tbPaymentAcNo.setValue(view.getPaymentAccountNo());
		if (view.getPaymentIFSCcode() != null)
			tbPaymentIFSCCode.setValue(view.getPaymentIFSCcode());
		if (view.getPaymentMICRcode() != null)
			tbPaymentMICRCode.setValue(view.getPaymentMICRcode());
		if (view.getPaymentSWIFTcode() != null)
			tbPaymentSWIFTCode.setValue(view.getPaymentSWIFTcode());
		if (view.getPaymentIBANNo() != null)
			tbPaymentIBANNo.setValue(view.getPaymentIBANNo());
		if (view.getPaymentInstruction() != null)
			taPaymentInstruction.setValue(view.getPaymentInstruction());
		
		if(view.getQrCodeDocument()!=null)
			uploadQrCode.setValue(view.getQrCodeDocument());
		
		if(view.getGooglePhonePayNo()!=0){
			ibGooglePayNumber.setValue(view.getGooglePhonePayNo());
		}
		
		if(view.getArticleTypeDetails()!=null){
			articletable.setValue(view.getArticleTypeDetails());
		}
		
		presenter.setModel(view);

	}
	/**
	 * Disabling Required Form Fields.
	 */
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		ibPaymentComId.setEnabled(false);
		tbPaymentComName.setEnabled(false);
		articletable.setEnable(state);
	}
	
	/**
	 * Set ID in view state after saving form.
	 */
	@Override
	public void setCount(int count) {
		ibPaymentComId.setValue(count);
	}
	
	/**
	 * Printing Alert Message when clicking on Check box Button.
	 */
	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource() == cbPrint) {
			boolean cbPrintValue=this.cbPrint.getValue();
			
			if(cbPrintValue==true ){
				final GWTCAlert alert=new GWTCAlert();
				alert.alert("Enable To Print Bank In Correspondance.");
			}
			
		}
		if(event.getSource() == cbDefault){
			boolean cbDefaultValue=this.cbDefault.getValue();
			if(cbDefaultValue==true ){
				final GWTCAlert alert=new GWTCAlert();
				alert.alert("Default Account To Used In Correspondance.");
			}
		}
		
		if(event.getSource() == btnAddPaymentDetail){

			
			String msg="";
			if(olbPaymentType.getSelectedIndex()==0){
				showDialogMessage("Please select payment type");
				return;				
			}
			if(tbPaymentDetails.getText()==null||tbPaymentDetails.getText().equals(""))
			{
				showDialogMessage("Please enter payment detail");
				return;
			}
			
			
			ArticleType entry=new ArticleType();
			entry.setArticleTypeName(olbPaymentType.getValue());
			entry.setArticleTypeValue(tbPaymentDetails.getText());
			
			if(articletable.getValue()!=null&&articletable.getValue().size()!=0){
				for(ArticleType obj:articletable.getValue()){
					if(obj.getArticleTypeName().equals(olbPaymentType.getValue())){
						showDialogMessage("Payment type already added!");
						return;
					}
				}
			}
			
			articletable.getDataprovider().getList().add(entry);
		
		}
	}
   /*Added by sheetal:30-11-2021*/
	@Override
	public boolean validate() {
		boolean flag =  super.validate();

		if(tbPaymentIFSCCode.getValue()!=null && !tbPaymentIFSCCode.getValue().equals("")){
			if(tbPaymentIBANNo.getValue()!=null && !tbPaymentIBANNo.getValue().equals("")){
			  showDialogMessage("Please add either IFSC code or IBAN no!");
			  return false;
			}
		}
		return flag;
	}
	
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		
		articletable.getTable().redraw();
	}

	

			
}
