package com.slicktechnologies.client.views.paymentmodes.companypayment;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.humanresource.calendar.CalendarPresenter;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class CompanyPaymentPresenter extends FormScreenPresenter<CompanyPayment> {

	public CompanyPaymentForm form;
	String comName="";
	final GenricServiceAsync async = GWT.create(GenricService.class);
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	/**
	 * Constructor of Class
	 */
	public CompanyPaymentPresenter(FormScreen<CompanyPayment> view,CompanyPayment model) {
		super(view, model);
		System.out.println("Constructor");
		form = (CompanyPaymentForm) view;
		form.setPresenter(this);
		getCompanyName(model.getCompanyId());
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.COMPANYPAYMENT,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if (text.equals("New")) {
			initalize();
		}
	}
	
	@Override
	public void reactOnDownload() {
		ArrayList<CompanyPayment> comppayarray = new ArrayList<CompanyPayment>();
		List<CompanyPayment> list = (List<CompanyPayment>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();

		comppayarray.addAll(list);

		csvservice.setCompanyPaymentDetails(comppayarray,new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed" + caught);
					}

					@Override
					public void onSuccess(Void result) {
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 17;
						Window.open(url, "test", "enabled");
					}
				});
	}
	/**
	 * Method That set Company name When Form is Loaded.
	 * 
	 */
	private void getCompanyName(Long id) {
		
		Filter temp = new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(id);
		MyQuerry querry = new MyQuerry();
		querry.getFilters().add(temp);
		querry.setQuerryObject(new Company());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");

					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for (SuperModel smodel : result) {
							Company comentity=(Company)smodel;
							comName=comentity.getBusinessUnitName();
							System.out.println("Company Name is : "+comName);
							form.tbPaymentComName.setValue(comName);
						}
					}
				});

	}
	/**
	 * Form is Initialized From Here.
	 * 
	 */

	public static void initalize() {
		AppMemory.getAppMemory().currentState = ScreeenState.NEW;
		CompanyPaymentForm form = new CompanyPaymentForm();

		CompanyPaymentPresenterTable gentable = new CompanyPaymentPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();

		CompanyPresenterPresenterSearch.staticSuperTable = gentable;

		CompanyPresenterPresenterSearch searchpopup = new CompanyPaymentPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		 CompanyPaymentPresenter  presenter=new CompanyPaymentPresenter(form,new CompanyPayment());
		AppMemory.getAppMemory().stickPnel(form);

	}

	

	@Override
	public void reactOnPrint() {
		final String url = GWT.getModuleBaseURL() + "pdfpayment" + "?Id="+ model.getId();
		Window.open(url, "test", "enabled");

	}

	@Override
	public void reactOnEmail() {

	}

	@Override
	protected void makeNewModel() {
		model=new CompanyPayment();
	}

	
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment")
	 public static  class CompanyPresenterPresenterSearch extends SearchPopUpScreen<CompanyPayment>{

		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};
		
		
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment")
		 public static class CompanyPaymentPresenterTable extends SuperTable<CompanyPayment> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;

}
