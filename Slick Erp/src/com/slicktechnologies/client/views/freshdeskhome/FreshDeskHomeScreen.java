package com.slicktechnologies.client.views.freshdeskhome;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;

public class FreshDeskHomeScreen extends FormScreen<DummyEntityOnlyForScreen>  {
	HTMLPanel htmlPanel=null;
	public FreshDeskHomeScreen() {
		super(FormStyle.DEFAULT);
		createGui();
		form.getElement().getStyle().setHeight(100, Unit.PCT);
		form.getElement().getStyle().setMarginTop(0,Unit.PX);
		form.getElement().getStyle().setMarginBottom(0,Unit.PX);
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Home", Screen.HOME);
//		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Home",Screen.BILLINGDETAILS);
		AppMemory.getAppMemory().skeleton.getProcessName().setText("Home"); 
		AppMemory.getAppMemory().stickPnel(this);
		
	}

	@Override
	public void clear() {
		super.clear();
	}

	@Override
	public boolean validate() {
		return super.validate();
	}

	@Override
	public void applyStyle() {
		super.applyStyle();
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
	}

	@Override
	public void createScreen() {
		super.createScreen();
		
		int height=Window.getClientHeight();
		Console.log("Window Height : "+height);
		height=height*75/100;
		Console.log("Updated Height : "+height);
		
//		String htmlString="<iframe width=\"100%\" height="+height+" src=\"https://datastudio.google.com/embed/reporting/6dd9bfef-745b-4847-9fa1-a500b41b8f33/page/atm9B\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>";
		String htmlString=AppMemory.getAppMemory().globalDsReportList.get(0).getReportLink();
		Console.log("OG LINK : "+htmlString);
		htmlString=htmlString.replaceAll("width=\"600\"", "width=\"100%\"");
		htmlString=htmlString.replaceAll("height=\"450\"", "height="+"\""+height+"\"");
		Console.log("UP LINK : "+htmlString);
		htmlPanel = new HTMLPanel(htmlString);
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		FormField fgoupingFreshdeskHome=fbuilder.setlabel("Home").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",htmlPanel);
		FormField fhtmlpanel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = {
//				{ fgoupingFreshdeskHome },
				{ fhtmlpanel },
		};
		this.fields = formfield;
		Console.log("FORM COMPLETED..");
	}

	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		super.updateModel(model);
	}

	@Override
	public void updateView(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		super.updateView(model);
	}

	@Override
	public void setToNewState() {
		// TODO Auto-generated method stub
		super.setToNewState();
	}

	@Override
	public void setToViewState() {
		// TODO Auto-generated method stub
		super.setToViewState();
	}

	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
	}
	
	
}
