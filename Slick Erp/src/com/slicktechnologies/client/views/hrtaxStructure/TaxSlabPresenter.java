package com.slicktechnologies.client.views.hrtaxStructure;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.settings.user.UserForm;
import com.slicktechnologies.client.views.settings.user.UserPresenter;
import com.slicktechnologies.client.views.settings.user.UserPresenterTableProxy;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.hrtaxstructure.TaxSlab;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class TaxSlabPresenter extends FormTableScreenPresenter<TaxSlab> {

	//Token to set the concrete form
	TaxSlabForm form;
	CsvServiceAsync async=GWT.create(CsvService.class);
	
	public TaxSlabPresenter (FormTableScreen<TaxSlab> view,
			TaxSlab model) {
		super(view, model);
		form=(TaxSlabForm) view;
		
		Console.log("inside Presenter Const");
		form.getSupertable().connectToLocal();
		form.retriveTable(TaxSlab());
		form.setPresenter(this);
		
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		if(text.equalsIgnoreCase("New")){
			form.setToNewState();
		}
			
	}
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new TaxSlab();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry TaxSlab()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new TaxSlab());
		return quer;
	}
	
	public void setModel(TaxSlab entity)
	{
		model=entity;
	}
	
	public static TaxSlabForm initialize()
	{
			Console.log("Inside user form initialize");
//			 UserPresenterTable gentableScreen=GWT.create(UserPresenterTable.class);
			TaxSlabTableProxy gentableScreen=new TaxSlabTableProxy(); 
			TaxSlabForm  form=new  TaxSlabForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			
			Console.log("Inside user form initialize 2222222");
			TaxSlabPresenter  presenter=new  TaxSlabPresenter  (form,new TaxSlab());
			AppMemory.getAppMemory().stickPnel(form);
			Console.log("Inside user form initialize 333333333");
			return form;
			
			
		 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.hrtaxstructure.TaxSlab")
		 public static class  TaxSlabPresenterTable extends SuperTable<TaxSlab> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
	

	
	

	
	
}
