package com.slicktechnologies.client.views.hrtaxStructure.employeeinvestment;

import java.util.Vector;

import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.hrtaxStructure.employeeinvestment.EmployeeInvestmentPresenter.EmployeeInvestmentSearch;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.hrtaxstructure.EmployeeInvestment;

public class EmployeeInvestmentPresenterSearchProxy extends EmployeeInvestmentSearch {

	
	public EmployeeInfoComposite personInfo;
	

	public Object getVarRef(String varName)
	{
		
		if(varName.equals("personInfo"))
			return this.personInfo;
		return null ;
	}
	public EmployeeInvestmentPresenterSearchProxy()
	{
		super();
		getPopup().setHeight("300px");
		createGui();
	}
	public void initWidget()
	{
		personInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
	
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
	


		this.fields=new FormField[][]{
				{fpersonInfo},
				
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
			if(!personInfo.getId().getValue().equals(""))
			{  
				temp=new Filter();
				temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
				temp.setQuerryString("empId");
				filtervec.add(temp);
			}

			if(!(personInfo.getName().getValue().equals("")))
			{
				temp=new Filter();
				temp.setStringValue(personInfo.getName().getValue());
				temp.setQuerryString("empName");
				filtervec.add(temp);
			}
			if(!personInfo.getPhone().getValue().equals(""))
			{
				temp=new Filter();
				temp.setLongValue(personInfo.getCellNumber());
				temp.setQuerryString("empCellNo");
				filtervec.add(temp);
			}
			
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new EmployeeInvestment());
		return querry;
	}
	
	
	@Override
	public boolean validate() {
		
		return super.validate();
	}

}
