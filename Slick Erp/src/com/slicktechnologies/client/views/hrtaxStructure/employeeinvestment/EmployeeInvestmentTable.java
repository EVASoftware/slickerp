package com.slicktechnologies.client.views.hrtaxStructure.employeeinvestment;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.hrtaxstructure.EmployeeInvestmentBean;
import com.slicktechnologies.shared.common.hrtaxstructure.Investment;
import com.slicktechnologies.shared.common.hrtaxstructure.TaxSlab;

public class EmployeeInvestmentTable extends SuperTable<EmployeeInvestmentBean> {


	TextColumn<EmployeeInvestmentBean> getYearCol;
	TextColumn<EmployeeInvestmentBean> getSectionNameCol;
	TextColumn<EmployeeInvestmentBean> getUpperLimitCol;
	TextColumn<EmployeeInvestmentBean> getDeclaredInvestmentCol;
	TextColumn<EmployeeInvestmentBean> getActualInvestmentCol;
	Column<EmployeeInvestmentBean,String> getEditDeclaredInvestmentCol;
	Column<EmployeeInvestmentBean,String> getEditActualInvestmentCol;
	Column<EmployeeInvestmentBean,String> deleteColumn;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		getYearCol();
		getSectionNameCol();
		getUpperLimitCol();
		getEditDeclaredInvestmentCol();
		getEditActualInvestmentCol();
		createColumndeleteColumn();
		addFieldUpdater();
		
	}

	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<EmployeeInvestmentBean,String>(btnCell)
				{
			@Override
			public String getValue(EmployeeInvestmentBean object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
				table.setColumnWidth(deleteColumn, 100,Unit.PX);
	}

	
	private void getYearCol() {
		// TODO Auto-generated method stub
		getYearCol=new TextColumn<EmployeeInvestmentBean>() {
			@Override
			public String getValue(EmployeeInvestmentBean object) {
				return object.getYear();
			}
		};
		table.addColumn(getYearCol,"Year");
		table.setColumnWidth(getYearCol,90, Unit.PX);
		getYearCol.setSortable(true);
	}



	private void getSectionNameCol() {
		// TODO Auto-generated method stub
		getSectionNameCol=new TextColumn<EmployeeInvestmentBean>() {
			@Override
			public String getValue(EmployeeInvestmentBean object) {
				if(object.getSectionName()!=null){
					return object.getSectionName();
				}
				return "";
			}
		};
		table.addColumn(getSectionNameCol,"Section Name");
		table.setColumnWidth(getSectionNameCol,90, Unit.PX);
		getSectionNameCol.setSortable(true);
	}



	

	private void getUpperLimitCol() {
		// TODO Auto-generated method stub
		getUpperLimitCol=new TextColumn<EmployeeInvestmentBean>() {
			@Override
			public String getValue(EmployeeInvestmentBean object) {
				
				return object.getUpperLimit()+"";
			}
		};
		table.addColumn(getUpperLimitCol,"Upper Limit");
		table.setColumnWidth(getUpperLimitCol,90, Unit.PX);
		getUpperLimitCol.setSortable(true);
	}


	private void getDeclaredInvestmentCol() {
		// TODO Auto-generated method stub
		getDeclaredInvestmentCol=new TextColumn<EmployeeInvestmentBean>() {
			@Override
			public String getValue(EmployeeInvestmentBean object) {
				
				return object.getDeclaredInvestment()+"";
			}
		};
		table.addColumn(getDeclaredInvestmentCol,"Declared Investment");
		table.setColumnWidth(getDeclaredInvestmentCol,90, Unit.PX);
		getDeclaredInvestmentCol.setSortable(true);
	}

	private void getActualInvestmentCol() {
		// TODO Auto-generated method stub
		getActualInvestmentCol=new TextColumn<EmployeeInvestmentBean>() {
			@Override
			public String getValue(EmployeeInvestmentBean object) {
				
				return object.getActualInvestment()+"";
			}
		};
		table.addColumn(getActualInvestmentCol,"Actual Investment");
		table.setColumnWidth(getActualInvestmentCol,90, Unit.PX);
		getActualInvestmentCol.setSortable(true);
	}
	

	private void getEditDeclaredInvestmentCol() {
		// TODO Auto-generated method stub
		EditTextCell editcell = new EditTextCell();
		getEditDeclaredInvestmentCol=new Column<EmployeeInvestmentBean ,String>(editcell) {
			@Override
			public String getValue(EmployeeInvestmentBean object) {
				
				return object.getDeclaredInvestment()+"";
			}
		};
		table.addColumn(getEditDeclaredInvestmentCol,"Declared Investment");
		table.setColumnWidth(getEditDeclaredInvestmentCol,90, Unit.PX);
		getEditDeclaredInvestmentCol.setSortable(true);
	}

	private void getEditActualInvestmentCol() {
		// TODO Auto-generated method stub
		EditTextCell editcell = new EditTextCell();
		getEditActualInvestmentCol=new Column<EmployeeInvestmentBean , String>(editcell) {
			@Override
			public String getValue(EmployeeInvestmentBean object) {
				
				return object.getActualInvestment()+"";
			}
		};
		table.addColumn(getEditActualInvestmentCol,"Actual Investment");
		table.setColumnWidth(getEditActualInvestmentCol,90, Unit.PX);
		getEditActualInvestmentCol.setSortable(true);
	}


	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		
	}



	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		createFieldUpdaterActualInvestmentColumn();
		createFieldUpdaterDeclaredInvestmentColumn();
		createFieldUpdaterdeleteColumn();
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		 for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
		if(state == true){
			getYearCol();
			getSectionNameCol();
			getUpperLimitCol();
			getEditDeclaredInvestmentCol();
			getEditActualInvestmentCol();
			createColumndeleteColumn();
			addFieldUpdater();
		}else{
			table.setWidth("100%");
			getYearCol();
			getSectionNameCol();
			getUpperLimitCol();
			getDeclaredInvestmentCol();
			getActualInvestmentCol();
			
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	protected void createFieldUpdaterDeclaredInvestmentColumn()
	{
		getEditDeclaredInvestmentCol.setFieldUpdater(new FieldUpdater<EmployeeInvestmentBean, String>()
				{
			@Override

			public void update(int index,EmployeeInvestmentBean object,String value)
			{

				try{
					String val1=value.trim();
					double value1 = 0;
					if(!val1.equals("")){
						value1 = Double.parseDouble(val1);
					}
					object.setDeclaredInvestment(value1);				
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
	protected void createFieldUpdaterActualInvestmentColumn()
	{
		getEditActualInvestmentCol.setFieldUpdater(new FieldUpdater<EmployeeInvestmentBean, String>()
				{
			@Override

			public void update(int index,EmployeeInvestmentBean object,String value)
			{

				try{
					String val1=value.trim();
					double value1 = 0;
					if(!val1.equals("")){
						value1 = Double.parseDouble(val1);
					}
					object.setActualInvestment(value1);				
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}

	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<EmployeeInvestmentBean,String>()
				{
			@Override
			public void update(int index,EmployeeInvestmentBean object,String value)
			{
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}


}
