package com.slicktechnologies.client.views.hrtaxStructure.employeeinvestment;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.humanresource.salarySlip.SalaryComponentTable;
import com.slicktechnologies.client.views.settings.employee.EmployeeCTCTemplateTable;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.hrtaxstructure.EmployeeInvestment;
import com.slicktechnologies.shared.common.hrtaxstructure.EmployeeInvestmentBean;
import com.slicktechnologies.shared.common.hrtaxstructure.Investment;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public  class EmployeeInvestmentForm extends FormScreen<EmployeeInvestment> implements ClickHandler{
	
	EmployeeInfoComposite pic;
	ObjectListBox<Type> olbYear;
	ObjectListBox<Investment> olbSectionName;
	DoubleBox dbUpperLimit;
	DoubleBox dbDeclaredInvestment;
	DoubleBox dbActualInvestment;
	Button btnAdd;
	EmployeeInvestmentTable employeeInvestmentTable;
	EmployeeInvestment empInvestment;
	DoubleBox dbpreviousCompanyCTC;
	DoubleBox dbpreviousCompanyTax;
	GenricServiceAsync async = GWT.create(GenricService.class);
	
	public EmployeeInvestmentForm(){
		super();
		btnAdd.addClickHandler(this);
		createGui();
	}
	
	public EmployeeInvestmentForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		btnAdd.addClickHandler(this);
		createGui();
		
	}

	private void initalizeWidget()
	{
		pic=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		olbYear = new ObjectListBox<Type>();
		AppUtility.MakeLiveConfig(olbYear,Screen.TAXATIONYEAR);		
		olbSectionName = new ObjectListBox<Investment>();
		MyQuerry qu= new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		qu.setQuerryObject(new Investment());
		olbSectionName.MakeLive(qu);
		dbUpperLimit = new DoubleBox();
		dbDeclaredInvestment = new DoubleBox();
		dbActualInvestment = new DoubleBox();
		
		btnAdd = new Button("ADD");
		employeeInvestmentTable = new EmployeeInvestmentTable();
		employeeInvestmentTable.connectToLocal();
		dbpreviousCompanyCTC = new DoubleBox();
		dbpreviousCompanyTax = new DoubleBox();
		dbUpperLimit.setEnabled(false);

	}

	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames=new String[]{AppConstants.NEW};
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeInformation=fbuilder.setlabel("Employee Investment Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("",pic);
		FormField fpic= fbuilder.setMandatory(true).setMandatoryMsg("Employee information is mandatory").setRowSpan(0).setColSpan(6).build();
	
		fbuilder = new FormFieldBuilder("Limit",dbUpperLimit);
		FormField fdbUpperLimit= fbuilder.setMandatory(false).setMandatoryMsg("Limit is mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Declared Investment",dbDeclaredInvestment);
		FormField fdbDeclaredInvestment= fbuilder.setMandatory(false).setMandatoryMsg("Declared Investment is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Actual Investment",dbActualInvestment);
		FormField fdbActualInvestment= fbuilder.setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Year",olbYear);
		FormField folbYear= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Section Name",olbSectionName);
		FormField folbSectionName= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnAdd);
		FormField fbtnAdd= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",employeeInvestmentTable.getTable());
		FormField fTable= fbuilder.setRowSpan(0).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder("Previous Company CTC(For TDS)",dbpreviousCompanyCTC);
		FormField fdbpreviousCompanyCTC= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Previous Company Tax(For TDS)",dbpreviousCompanyTax);
		FormField fdbpreviousCompanyTax= fbuilder.setRowSpan(0).setColSpan(0).build();
				
		FormField[][] formfield = {   
				{fgroupingEmployeeInformation},
				{fpic},
				{fdbpreviousCompanyCTC , fdbpreviousCompanyTax},
				{folbYear , folbSectionName ,fdbUpperLimit , fdbDeclaredInvestment , fdbActualInvestment , fbtnAdd},
				{fTable}
				
		};

		this.fields=formfield;		

	}


	@Override
	public void updateModel(EmployeeInvestment model) 
	{
		if(pic.getValue()!=null)
		{
			model.setEmpId(pic.getEmployeeId());
			model.setEmpName(pic.getEmployeeName());
			model.setEmpCellNo(pic.getCellNumber());

		}
		if(employeeInvestmentTable.getDataprovider().getList().size()>0){
			List<EmployeeInvestmentBean> list = employeeInvestmentTable.getDataprovider().getList();
			model.setList(list);
		}
		if(dbpreviousCompanyCTC.getValue()!=null){
			model.setPreviousCompanyCTC(dbpreviousCompanyCTC.getValue());
		}
		if(dbpreviousCompanyTax.getValue()!=null){
			model.setPreviousCompanyTax(dbpreviousCompanyTax.getValue());
		}
		empInvestment=model;
		
		presenter.setModel(model);


	}


	@Override
	public void updateView(EmployeeInvestment view) 
	{
		empInvestment=view;
		
		pic.setEmployeeId(view.getEmpId());
		if(view.getEmpName()!=null)
			pic.setEmployeeName(view.getEmpName());	
			pic.setCellNumber(view.getEmpCellNo());
		if(view.getList().size()>0){
			employeeInvestmentTable.getDataprovider().setList(view.getList());
		}
		if(view.getPreviousCompanyCTC()!=null){
			dbpreviousCompanyCTC.setValue(view.getPreviousCompanyCTC());
		}
		if(view.getPreviousCompanyTax()!=null){
			dbpreviousCompanyTax.setValue(view.getPreviousCompanyTax());
		}
		presenter.setModel(view);
	}

	

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION) || text.equals("Search"))
				{
					menus[k].setVisible(true); 
				}
				else{
					menus[k].setVisible(false); 
				}
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Search")||text.equals("Print")||text.equals(AppConstants.NAVIGATION)||text.contains("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.EMPLOYEEINVESTMENT,LoginPresenter.currentModule.trim());
	}

	@Override
	public void setCount(int count)
	{
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		this.employeeInvestmentTable.setEnable(state);
		dbUpperLimit.setEnabled(false);
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new EmployeeInvestment();
		model=empInvestment;
		AppUtility.addDocumentToHistoryTable(AppConstants.HRMODULE,"Employee Investment", empInvestment.getCount(), empInvestment.getEmpId(),empInvestment.getEmpName(),empInvestment.getEmpCellNo(), false, model, null);		
	}
	

	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
			
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		boolean val= super.validate();
		if(val==false)
			return val;
		return true;
	}


	@Override
	public void clear() {
		super.clear();
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(btnAdd)){
		if(olbYear.getSelectedIndex()==0){
			showDialogMessage("Year is Mandatory.");
			return;
		}
		if(olbSectionName.getSelectedIndex()==0){
			showDialogMessage("Section Name is Mandatory.");
			return;
		}
		if(olbYear.getSelectedIndex()!=0 && olbSectionName.getSelectedIndex()!=0){
			List<EmployeeInvestmentBean> list = employeeInvestmentTable.getDataprovider().getList();
			for(EmployeeInvestmentBean bean1 : list){
				if(olbSectionName.getValue(olbSectionName.getSelectedIndex()).equalsIgnoreCase(bean1.getSectionName())){
					showDialogMessage("Section name already exist.");
					return;
				}
			}
			EmployeeInvestmentBean bean = new EmployeeInvestmentBean();
			bean.setYear(olbYear.getValue(olbYear.getSelectedIndex()));
			bean.setSectionName(olbSectionName.getValue(olbSectionName.getSelectedIndex()));
			if(dbUpperLimit.getValue()!=null){
				bean.setUpperLimit(dbUpperLimit.getValue());
			}
			if(dbDeclaredInvestment.getValue()!=null){
//				if(dbDeclaredInvestment.getValue() > dbUpperLimit.getValue()){
//					showDialogMessage("Declared investment should not be greter than upper limit.");
//					return;
//				}
				bean.setDeclaredInvestment(dbDeclaredInvestment.getValue());
			}
			if(dbActualInvestment.getValue()!=null){
//				if(dbActualInvestment.getValue() > dbUpperLimit.getValue()){
//					showDialogMessage("Actual investment should not be greter than upper limit.");
//			        return;	
//				}
				bean.setActualInvestment(dbActualInvestment.getValue());
			}
			list.add(bean);
			olbYear.setSelectedIndex(0);
			olbSectionName.setSelectedIndex(0);
			dbUpperLimit.setValue(0.0);
			dbDeclaredInvestment.setValue(0.0);
			dbActualInvestment.setValue(0.0);
		}
		}
	}

	public void retriveEmployee() {
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = new Filter();
		
		filter.setQuerryString("empId");
		filter.setIntValue(pic.getEmployeeId());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setLongValue(c.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new EmployeeInvestment());

		async.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							showDialogMessage("Employee Investment Already Exist!");
							pic.clear();
						}
					}
					@Override
					public void onFailure(Throwable caught) {
						System.out.println("No Record Found");
					}
				});
	}

//	@Override
//	public void onClick(ClickEvent event) {
//		
//	}
	
	


//	EmployeeInfoComposite pic;
//	ObjectListBox<Type> olbYear;
//	
//	public EmployeeInvestmentForm(){
//		super();
//		createGui();
//	}
//	private void initalizeWidget()
//	{
//		pic=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
//		olbYear = new ObjectListBox<Type>();
//		
//	}
//	@Override
//	public void toggleAppHeaderBarMenu() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void createScreen() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void updateModel(EmployeeInvestment model) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void updateView(EmployeeInvestment model) {
//		// TODO Auto-generated method stub
//		
//	}

}
