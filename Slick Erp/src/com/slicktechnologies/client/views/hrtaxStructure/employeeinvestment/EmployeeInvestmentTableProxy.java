package com.slicktechnologies.client.views.hrtaxStructure.employeeinvestment;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import java.util.List;
import com.slicktechnologies.shared.common.hrtaxstructure.EmployeeInvestment;

public class EmployeeInvestmentTableProxy extends SuperTable<EmployeeInvestment> {
	TextColumn<EmployeeInvestment> getCountColumn;
	TextColumn<EmployeeInvestment> getFullnameColumn;
	TextColumn<EmployeeInvestment> getCellNumber1Column;
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("getCellNumber1Column"))
			return this.getCellNumber1Column;
		if(varName.equals("getFullnameColumn"))
			return this.getFullnameColumn;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		return null ;
	}
	public EmployeeInvestmentTableProxy()
	{
		super();
	}
	@Override public void createTable() {
		addColumngetCount();
		addColumngetFullname();
		addColumngetCellNumber1();
		
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<EmployeeInvestment>()
				{
			@Override
			public Object getKey(EmployeeInvestment item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetFullname();
		addSortinggetCellNumber1();
	}
	
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetCount()
	{
		List<EmployeeInvestment> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeInvestment>(list);
		columnSort.setComparator(getCountColumn, new Comparator<EmployeeInvestment>()
				{
			@Override
			public int compare(EmployeeInvestment e1,EmployeeInvestment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getEmpId()== e2.getEmpId()){
						return 0;}
					if(e1.getEmpId()> e2.getEmpId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<EmployeeInvestment>()
				{
			@Override
			public String getValue(EmployeeInvestment object)
			{
				if( object.getEmpId()==-1)
					return "N.A";
				else return object.getEmpId()+"";
			}
				};
				table.addColumn(getCountColumn,"Id");
				getCountColumn.setSortable(true);
	}
	protected void addSortinggetFullname()
	{
		List<EmployeeInvestment> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeInvestment>(list);
		columnSort.setComparator(getFullnameColumn, new Comparator<EmployeeInvestment>()
				{
			@Override
			public int compare(EmployeeInvestment e1,EmployeeInvestment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmpName()!=null && e2.getEmpName()!=null){
						return e1.getEmpName().compareTo(e2.getEmpName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetFullname()
	{
		getFullnameColumn=new TextColumn<EmployeeInvestment>()
				{
			@Override
			public String getValue(EmployeeInvestment object)
			{
				return object.getEmpName();
			}
				};
				table.addColumn(getFullnameColumn,"Name");
				getFullnameColumn.setSortable(true);
	}
	protected void addSortinggetCellNumber1()
	{
		List<EmployeeInvestment> list=getDataprovider().getList();
		columnSort=new ListHandler<EmployeeInvestment>(list);
		columnSort.setComparator(getCellNumber1Column, new Comparator<EmployeeInvestment>()
				{
			@Override
			public int compare(EmployeeInvestment e1,EmployeeInvestment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getEmpCellNo()== e2.getEmpCellNo()){
						return 0;}
					if(e1.getEmpCellNo()> e2.getEmpCellNo()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCellNumber1()
	{
		getCellNumber1Column=new TextColumn<EmployeeInvestment>()
				{
			@Override
			public String getValue(EmployeeInvestment object)
			{
				return object.getEmpCellNo()+"";
			}
				};
				table.addColumn(getCellNumber1Column,"Phone");
				getCellNumber1Column.setSortable(true);
	}
	
}
