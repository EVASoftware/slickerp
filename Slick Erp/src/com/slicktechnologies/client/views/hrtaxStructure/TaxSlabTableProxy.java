package com.slicktechnologies.client.views.hrtaxStructure;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.hrtaxstructure.TaxSlab;

public class TaxSlabTableProxy extends SuperTable<TaxSlab> {

	TextColumn<TaxSlab> getYearCol;
	TextColumn<TaxSlab> getCategoryCol;
	TextColumn<TaxSlab> getUpperLimitCol;
	TextColumn<TaxSlab> getLowerLimitCol;
	TextColumn<TaxSlab> getTaxCol;
	TextColumn<TaxSlab> getEducationTaxCol;
	TextColumn<TaxSlab> getHSEducationCol;
	TextColumn<TaxSlab> getStatusCol;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		getYearCol();
		getCategoryCol();
		getLowerLimitCol();
		getUpperLimitCol();
		getTaxCol();
		getEducationTaxCol();
		getHSEducationCol();
		getStatusCol();
	}

	
	
	private void getYearCol() {
		// TODO Auto-generated method stub
		getYearCol=new TextColumn<TaxSlab>() {
			@Override
			public String getValue(TaxSlab object) {
				return object.getYear();
			}
		};
		table.addColumn(getYearCol,"Year");
		table.setColumnWidth(getYearCol,90, Unit.PX);
		getYearCol.setSortable(true);
	}



	private void getCategoryCol() {
		// TODO Auto-generated method stub
		getCategoryCol=new TextColumn<TaxSlab>() {
			@Override
			public String getValue(TaxSlab object) {
				if(object.getCategory()!=null){
					return object.getCategory();
				}
				return "";
			}
		};
		table.addColumn(getCategoryCol,"Category");
		table.setColumnWidth(getCategoryCol,90, Unit.PX);
		getCategoryCol.setSortable(true);
	}



	private void getUpperLimitCol() {
		// TODO Auto-generated method stub
		getUpperLimitCol=new TextColumn<TaxSlab>() {
			@Override
			public String getValue(TaxSlab object) {
				
				return object.getUpperLimit()+"";
			}
		};
		table.addColumn(getUpperLimitCol,"Upper Limit");
		table.setColumnWidth(getUpperLimitCol,90, Unit.PX);
		getUpperLimitCol.setSortable(true);
	}



	private void getLowerLimitCol() {
		// TODO Auto-generated method stub
		getLowerLimitCol =new TextColumn<TaxSlab>() {
			@Override
			public String getValue(TaxSlab object) {
				
				return object.getLowerLimit()+"";
				
			}
		};
		table.addColumn(getLowerLimitCol,"Lower Limit");
		table.setColumnWidth(getLowerLimitCol,110, Unit.PX);
		getLowerLimitCol.setSortable(true);
	}



	private void getTaxCol() {
		// TODO Auto-generated method stub
		getTaxCol=new TextColumn<TaxSlab>() {
			@Override
			public String getValue(TaxSlab object) {
				return object.getTax()+"";
			}
		};
		table.addColumn(getTaxCol,"Tax");
		table.setColumnWidth(getTaxCol,110, Unit.PX);
		getTaxCol.setSortable(true);
		
	}



	private void getEducationTaxCol() {
		// TODO Auto-generated method stub
		getEducationTaxCol=new TextColumn<TaxSlab>() {
			@Override
			public String getValue(TaxSlab object) {		
				return object.getEducationTax()+"";
			}
		};
		table.addColumn(getEducationTaxCol,"Education Cess");
		table.setColumnWidth(getEducationTaxCol,90, Unit.PX);
		getEducationTaxCol.setSortable(true);
	}

	private void getHSEducationCol() {
		// TODO Auto-generated method stub
		getHSEducationCol=new TextColumn<TaxSlab>() {
			@Override
			public String getValue(TaxSlab object) {		
				return object.getHsEducationTax()+"";
			}
		};
		table.addColumn(getHSEducationCol,"Secondary and Higher Education Cess");
		table.setColumnWidth(getHSEducationCol,90, Unit.PX);
		getHSEducationCol.setSortable(true);
	}
	

	private void getStatusCol() {
		// TODO Auto-generated method stub
		getStatusCol=new TextColumn<TaxSlab>() {
			@Override
			public String getValue(TaxSlab object) {
				if(object.getStatus() == true){
					return "Active";
				}
				return "Inactive";
			}
		};
		table.addColumn(getStatusCol,"Status");
		table.setColumnWidth(getStatusCol,110, Unit.PX);
		getStatusCol.setSortable(true);
	}



	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		getSortingYearCol();
		getSortingCategoryCol();
		getSortingLowerLimitCol();
		getSortingUpperLimitCol();
		getSortingTaxCol();
		getSortingEducationTaxCol();
		getSortingHSEducationCol();
		getSortingStatusCol();
	}



	private void getSortingYearCol() {
		List<TaxSlab> list=getDataprovider().getList();
		columnSort=new ListHandler<TaxSlab>(list);
		columnSort.setComparator(getYearCol, new Comparator<TaxSlab>()
		{
			@Override
			public int compare(TaxSlab e1,TaxSlab e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getYear()!=null && e2.getYear()!=null){
						return e1.getYear().compareTo(e2.getYear());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void getSortingCategoryCol() {
		List<TaxSlab> list=getDataprovider().getList();
		columnSort=new ListHandler<TaxSlab>(list);
		columnSort.setComparator(getCategoryCol, new Comparator<TaxSlab>()
		{
			@Override
			public int compare(TaxSlab e1,TaxSlab e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCategory()!=null && e2.getCategory()!=null){
						return e1.getCategory().compareTo(e2.getCategory());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	private void getSortingLowerLimitCol() {
		List<TaxSlab> list=getDataprovider().getList();
		columnSort=new ListHandler<TaxSlab>(list);
		columnSort.setComparator(getLowerLimitCol, new Comparator<TaxSlab>()
		{
			@Override
			public int compare(TaxSlab e1,TaxSlab e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getLowerLimit()== e2.getLowerLimit()){
						return 0;}
					if(e1.getLowerLimit()> e2.getLowerLimit()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	private void getSortingUpperLimitCol() {
		List<TaxSlab> list=getDataprovider().getList();
		columnSort=new ListHandler<TaxSlab>(list);
		columnSort.setComparator(getUpperLimitCol, new Comparator<TaxSlab>()
		{
			@Override
			public int compare(TaxSlab e1,TaxSlab e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getUpperLimit()== e2.getUpperLimit()){
						return 0;}
					if(e1.getUpperLimit()> e2.getUpperLimit()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void getSortingTaxCol() {
		List<TaxSlab> list=getDataprovider().getList();
		columnSort=new ListHandler<TaxSlab>(list);
		columnSort.setComparator(getTaxCol, new Comparator<TaxSlab>()
		{
			@Override
			public int compare(TaxSlab e1,TaxSlab e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getTax()== e2.getTax()){
						return 0;}
					if(e1.getTax()> e2.getTax()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void getSortingEducationTaxCol() {
		List<TaxSlab> list=getDataprovider().getList();
		columnSort=new ListHandler<TaxSlab>(list);
		columnSort.setComparator(getEducationTaxCol, new Comparator<TaxSlab>()
		{
			@Override
			public int compare(TaxSlab e1,TaxSlab e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getEducationTax()== e2.getEducationTax()){
						return 0;}
					if(e1.getEducationTax()> e2.getEducationTax()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void getSortingHSEducationCol() {
		List<TaxSlab> list=getDataprovider().getList();
		columnSort=new ListHandler<TaxSlab>(list);
		columnSort.setComparator(getHSEducationCol, new Comparator<TaxSlab>()
		{
			@Override
			public int compare(TaxSlab e1,TaxSlab e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getHsEducationTax()== e2.getHsEducationTax()){
						return 0;}
					if(e1.getHsEducationTax()> e2.getHsEducationTax()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void getSortingStatusCol() {
		List<TaxSlab> list=getDataprovider().getList();
		columnSort=new ListHandler<TaxSlab>(list);
		columnSort.setComparator(getStatusCol, new Comparator<TaxSlab>()
		{
			@Override
			public int compare(TaxSlab e1,TaxSlab e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
