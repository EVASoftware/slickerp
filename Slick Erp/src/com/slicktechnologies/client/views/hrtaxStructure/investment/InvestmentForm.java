package com.slicktechnologies.client.views.hrtaxStructure.investment;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.hrtaxstructure.Investment;
import com.slicktechnologies.shared.common.hrtaxstructure.TaxSlab;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class InvestmentForm extends  FormTableScreen<Investment>  implements ClickHandler{



	ObjectListBox<Type> olbYear;
	ObjectListBox<Type> olbSectionName;
	DoubleBox dbUpperLimit;
	CheckBox cbStatus;	
	Investment investmentObj;

	public InvestmentForm  (SuperTable<Investment> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();

	}

	private void initalizeWidget()
	{
		Console.log("Inside user form initialize4444444444444444");

		olbYear = new ObjectListBox<Type>();
		AppUtility.MakeLiveConfig(olbYear,Screen.TAXATIONYEAR);		
		olbSectionName = new ObjectListBox<Type>();
		AppUtility.MakeLiveConfig(olbSectionName,Screen.SECTIONNAME);
		dbUpperLimit = new DoubleBox();
		cbStatus = new CheckBox();	
		cbStatus.setEnabled(true);
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();
		this.processlevelBarNames=new String[]{"New"};


		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingInformation=fbuilder.setlabel("Tax Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("* Year",olbYear);
		FormField folbYear= fbuilder.setMandatory(true).setMandatoryMsg("Year is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Section Name", olbSectionName);
		FormField folbCategory= fbuilder.setMandatory(true).setMandatoryMsg("Section Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
	
		fbuilder = new FormFieldBuilder("* Upper Limit", dbUpperLimit);
		FormField fdbUpperLimit= fbuilder.setMandatory(true).setMandatoryMsg("Upper Limit is Mandatory").setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Status", cbStatus);
		FormField fcbStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingInformation},
				{folbYear ,folbCategory ,fdbUpperLimit, fcbStatus}

		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Investment model) 
	{
		Console.log("update model");
		if(olbYear.getSelectedIndex() !=0){
			model.setYear(olbYear.getValue(olbYear.getSelectedIndex()));
		}else{
			model.setYear("");
		}
		if(olbSectionName.getSelectedIndex()!=0){
			model.setSectionName(olbSectionName.getValue(olbSectionName.getSelectedIndex()));
		}else{
			model.setSectionName("");
		}
	
		if(dbUpperLimit.getValue()!=null){
			model.setUpperLimit(dbUpperLimit.getValue());
		}
		
		model.setStatus(cbStatus.getValue());
		investmentObj=model;
		presenter.setModel(model);


	}

	
	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Investment view) 
	{

		investmentObj=view;
		
		if(view.getYear() !=null){
			olbYear.setValue(view.getYear());
		}
		if(view.getSectionName()!=null){
			olbSectionName.setValue(view.getSectionName());
		}		
		dbUpperLimit.setValue(view.getUpperLimit());
		cbStatus.setValue(view.getStatus());
				
		presenter.setModel(view);
	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.INVESTENTCONFIGURATION,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{

	}


	@Override
	public void onClick(ClickEvent event) {

	}
	
	
	@Override
	public boolean validate()
	{
		 boolean superValidate = super.validate();
		 return superValidate;
	}


	@Override
	public void clear() {

		super.clear();
		cbStatus.setValue(true); 
	}


	@Override
	public void setToViewState() {
		super.setToViewState();

		SuperModel model=new Investment();
		model=investmentObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.HRCONFIG,"Investment", investmentObj.getCount(), null,null,null, false, model, null);
	
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(investmentObj!=null){
			SuperModel model=new Investment();
			model=investmentObj;
			AppUtility.addDocumentToHistoryTable(AppConstants.HRCONFIG, "Investment", investmentObj.getCount(), null,null,null, false, model, null);
		
		}
	}

}
