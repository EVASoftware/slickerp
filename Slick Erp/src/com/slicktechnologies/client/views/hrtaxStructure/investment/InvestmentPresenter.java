package com.slicktechnologies.client.views.hrtaxStructure.investment;

import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.hrtaxStructure.TaxSlabForm;
import com.slicktechnologies.client.views.hrtaxStructure.TaxSlabPresenter;
import com.slicktechnologies.client.views.hrtaxStructure.TaxSlabTableProxy;
import com.slicktechnologies.shared.common.hrtaxstructure.Investment;
import com.slicktechnologies.shared.common.hrtaxstructure.TaxSlab;

public class InvestmentPresenter extends FormTableScreenPresenter<Investment> {

	//Token to set the concrete form
	InvestmentForm form;
	CsvServiceAsync async=GWT.create(CsvService.class);
	
	public InvestmentPresenter (FormTableScreen<Investment> view,
			Investment model) {
		super(view, model);
		form=(InvestmentForm) view;
		
		Console.log("inside Presenter Const");
		form.getSupertable().connectToLocal();
		form.retriveTable(getQuerry());
		form.setPresenter(this);
		
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		if(text.equalsIgnoreCase("New")){
			form.setToNewState();
		}
			
	}
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new Investment();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getQuerry()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new Investment());
		return quer;
	}
	
	public void setModel(Investment entity)
	{
		model=entity;
	}
	
	public static InvestmentForm initialize()
	{
			Console.log("Inside user form initialize");
//			 UserPresenterTable gentableScreen=GWT.create(UserPresenterTable.class);
			InvestmentTableProxy gentableScreen=new InvestmentTableProxy(); 
			InvestmentForm  form=new  InvestmentForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			
			Console.log("Inside user form initialize 2222222");
			InvestmentPresenter  presenter=new  InvestmentPresenter  (form,new Investment());
			AppMemory.getAppMemory().stickPnel(form);
			Console.log("Inside user form initialize 333333333");
			return form;
			
			
		 
	}
	
	
	
}
