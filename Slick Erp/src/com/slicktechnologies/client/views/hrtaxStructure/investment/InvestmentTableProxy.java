package com.slicktechnologies.client.views.hrtaxStructure.investment;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.hrtaxstructure.Investment;
import com.slicktechnologies.shared.common.hrtaxstructure.TaxSlab;

public class InvestmentTableProxy extends SuperTable<Investment> {

	TextColumn<Investment> getYearCol;
	TextColumn<Investment> getSectionNameCol;
	TextColumn<Investment> getUpperLimitCol;
	TextColumn<Investment> getStatusCol;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		getYearCol();
		getSectionNameCol();
		getUpperLimitCol();
		getStatusCol();
	}

	
	
	private void getYearCol() {
		// TODO Auto-generated method stub
		getYearCol=new TextColumn<Investment>() {
			@Override
			public String getValue(Investment object) {
				return object.getYear();
			}
		};
		table.addColumn(getYearCol,"Year");
		table.setColumnWidth(getYearCol,90, Unit.PX);
		getYearCol.setSortable(true);
	}



	private void getSectionNameCol() {
		// TODO Auto-generated method stub
		getSectionNameCol=new TextColumn<Investment>() {
			@Override
			public String getValue(Investment object) {
				if(object.getSectionName()!=null){
					return object.getSectionName();
				}
				return "";
			}
		};
		table.addColumn(getSectionNameCol,"Section Name");
		table.setColumnWidth(getSectionNameCol,90, Unit.PX);
		getSectionNameCol.setSortable(true);
	}



	private void getUpperLimitCol() {
		// TODO Auto-generated method stub
		getUpperLimitCol=new TextColumn<Investment>() {
			@Override
			public String getValue(Investment object) {
				
				return object.getUpperLimit()+"";
			}
		};
		table.addColumn(getUpperLimitCol,"Upper Limit");
		table.setColumnWidth(getUpperLimitCol,90, Unit.PX);
		getUpperLimitCol.setSortable(true);
	}



	
	

	private void getStatusCol() {
		// TODO Auto-generated method stub
		getStatusCol=new TextColumn<Investment>() {
			@Override
			public String getValue(Investment object) {
				if(object.getStatus() == true){
					return "Active";
				}
				return "Inactive";
			}
		};
		table.addColumn(getStatusCol,"Status");
		table.setColumnWidth(getStatusCol,110, Unit.PX);
		getStatusCol.setSortable(true);
	}



	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		getSortingYearCol();
		getSortingSectionNameCol();
		getSortingUpperLimitCol();
		getSortingStatusCol();
	}



	private void getSortingYearCol() {
		List<Investment> list=getDataprovider().getList();
		columnSort=new ListHandler<Investment>(list);
		columnSort.setComparator(getYearCol, new Comparator<Investment>()
		{
			@Override
			public int compare(Investment e1,Investment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getYear()!=null && e2.getYear()!=null){
						return e1.getYear().compareTo(e2.getYear());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void getSortingSectionNameCol() {
		List<Investment> list=getDataprovider().getList();
		columnSort=new ListHandler<Investment>(list);
		columnSort.setComparator(getSectionNameCol, new Comparator<Investment>()
		{
			@Override
			public int compare(Investment e1,Investment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getSectionName()!=null && e2.getSectionName()!=null){
						return e1.getSectionName().compareTo(e2.getSectionName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void getSortingUpperLimitCol() {
		List<Investment> list=getDataprovider().getList();
		columnSort=new ListHandler<Investment>(list);
		columnSort.setComparator(getUpperLimitCol, new Comparator<Investment>()
		{
			@Override
			public int compare(Investment e1,Investment e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getUpperLimit()== e2.getUpperLimit()){
						return 0;}
					if(e1.getUpperLimit()> e2.getUpperLimit()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	

	private void getSortingStatusCol() {
		List<Investment> list=getDataprovider().getList();
		columnSort=new ListHandler<Investment>(list);
		columnSort.setComparator(getStatusCol, new Comparator<Investment>()
		{
			@Override
			public int compare(Investment e1,Investment e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}



}
