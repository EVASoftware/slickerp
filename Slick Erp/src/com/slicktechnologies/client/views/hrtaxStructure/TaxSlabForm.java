package com.slicktechnologies.client.views.hrtaxStructure;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.settings.user.IPAddressTable;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.hrtaxstructure.TaxSlab;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.UserRole;
import com.slicktechnologies.shared.common.helperlayer.Type;

public class TaxSlabForm extends FormTableScreen<TaxSlab>  implements ClickHandler{



	ObjectListBox<Type> olbYear;
	ObjectListBox<Type> olbCategory;;
	DoubleBox dbUpperLimit;
	DoubleBox dbLowerLimit;
	DoubleBox dbTax;
	DoubleBox dbEducationTax;
	DoubleBox dbHSEducationTax;
	CheckBox cbStatus;	
	TaxSlab taxObj;

	public TaxSlabForm  (SuperTable<TaxSlab> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		/**
		 * @author Anil , Date : 06-02-2020
		 */
		cbStatus.setEnabled(true);

	}

	private void initalizeWidget()
	{
		Console.log("Inside user form initialize4444444444444444");

		olbYear = new ObjectListBox<Type>();
		AppUtility.MakeLiveConfig(olbYear,Screen.TAXATIONYEAR);		
		olbCategory = new ObjectListBox<Type>();
		AppUtility.MakeLiveConfig(olbCategory,Screen.TAXCATEGORY);
		dbUpperLimit = new DoubleBox();
		dbLowerLimit = new DoubleBox();
		dbTax = new DoubleBox();
		dbEducationTax = new DoubleBox();
		dbHSEducationTax = new DoubleBox();
		cbStatus = new CheckBox();
		/**
		 * @author Anil , Date : 06-02-2020
		 */
		cbStatus.setEnabled(true);
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();
		this.processlevelBarNames=new String[]{"New"};


		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingInformation=fbuilder.setlabel("Tax Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("* Year",olbYear);
		FormField folbYear= fbuilder.setMandatory(true).setMandatoryMsg("Year is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Category", olbCategory);
		FormField folbCategory= fbuilder.setMandatory(true).setMandatoryMsg("Category is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Lower Limit", dbLowerLimit);
		FormField fdbLowerLimit= fbuilder.setMandatory(true).setMandatoryMsg("Lower Limit is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Upper Limit", dbUpperLimit);
		FormField fdbUpperLimit= fbuilder.setMandatory(true).setMandatoryMsg("Upper Limit is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Tax", dbTax);
		FormField fdbTax= fbuilder.setMandatory(true).setMandatoryMsg("Tax is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Education Tax", dbEducationTax);
		FormField fdbEducationTax= fbuilder.setMandatory(true).setMandatoryMsg("Education Tax is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Secondary and Higher Education Tax", dbHSEducationTax);
		FormField fdbHSEducationTax= fbuilder.setMandatory(true).setMandatoryMsg("Secondary and Higher Tax is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Status", cbStatus);
		FormField fcbStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingInformation},
				{folbYear ,folbCategory ,fdbLowerLimit ,fdbUpperLimit},
				{fdbTax , fdbEducationTax , fdbHSEducationTax , fcbStatus}

		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(TaxSlab model) 
	{
		Console.log("update model");
		if(olbYear.getSelectedIndex() !=0){
			model.setYear(olbYear.getValue(olbYear.getSelectedIndex()));
		}else{
			model.setYear("");
		}
		if(olbCategory.getSelectedIndex()!=0){
			model.setCategory(olbCategory.getValue(olbCategory.getSelectedIndex()));
		}else{
			model.setCategory("");
		}
		if(dbTax.getValue()!=null){
			model.setTax(dbTax.getValue());
		}
		if(dbLowerLimit.getValue()!=null){
			model.setLowerLimit(dbLowerLimit.getValue());
		}
		if(dbUpperLimit.getValue()!=null){
			model.setUpperLimit(dbUpperLimit.getValue());
		}
		if(dbEducationTax.getValue()!=null){
			model.setEducationTax(dbEducationTax.getValue());
		}
		if(dbHSEducationTax.getValue()!=null){
			model.setHsEducationTax(dbHSEducationTax.getValue());
		}
		model.setStatus(cbStatus.getValue());
		taxObj=model;
		presenter.setModel(model);


	}

	
	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(TaxSlab view) 
	{

		taxObj=view;
		
		if(view.getYear() !=null){
			olbYear.setValue(view.getYear());
		}
		if(view.getCategory()!=null){
			olbCategory.setValue(view.getCategory());
		}		
		dbLowerLimit.setValue(view.getLowerLimit());
		dbUpperLimit.setValue(view.getUpperLimit());
		dbTax.setValue(view.getTax());
		dbEducationTax.setValue(view.getEducationTax());
		dbHSEducationTax.setValue(view.getHsEducationTax());
		cbStatus.setValue(view.getStatus());
				
		presenter.setModel(view);
	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.TAXSLABCONFIGURATION,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{

	}


	@Override
	public void onClick(ClickEvent event) {

	}
	
	
	@Override
	public boolean validate()
	{
		 boolean superValidate = super.validate();
		 return superValidate;
	}


	@Override
	public void clear() {

		super.clear();
		cbStatus.setValue(true); 
	}


	@Override
	public void setToViewState() {
		super.setToViewState();

		SuperModel model=new TaxSlab();
		model=taxObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.HRCONFIG,"Tax Slab", taxObj.getCount(), null,null,null, false, model, null);
	
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(taxObj!=null){
			SuperModel model=new TaxSlab();
			model=taxObj;
			AppUtility.addDocumentToHistoryTable(AppConstants.HRCONFIG, "Tax Slab", taxObj.getCount(), null,null,null, false, model, null);
		
		}
	}

	

	public DoubleBox getDbUpperLimit() {
		return dbUpperLimit;
	}

	public void setDbUpperLimit(DoubleBox dbUpperLimit) {
		this.dbUpperLimit = dbUpperLimit;
	}

	public DoubleBox getDbLowerLimit() {
		return dbLowerLimit;
	}

	public void setDbLowerLimit(DoubleBox dbLowerLimit) {
		this.dbLowerLimit = dbLowerLimit;
	}

	public DoubleBox getDbTax() {
		return dbTax;
	}

	public void setDbTax(DoubleBox dbTax) {
		this.dbTax = dbTax;
	}

	public DoubleBox getDbEducationTax() {
		return dbEducationTax;
	}

	public void setDbEducationTax(DoubleBox dbEducationTax) {
		this.dbEducationTax = dbEducationTax;
	}

	public DoubleBox getDbHSEducationTax() {
		return dbHSEducationTax;
	}

	public void setDbHSEducationTax(DoubleBox dbHSEducationTax) {
		this.dbHSEducationTax = dbHSEducationTax;
	}

	public CheckBox getCbStatus() {
		return cbStatus;
	}

	public void setCbStatus(CheckBox cbStatus) {
		this.cbStatus = cbStatus;
	}

	public TaxSlab getTaxObj() {
		return taxObj;
	}

	public void setTaxObj(TaxSlab taxObj) {
		this.taxObj = taxObj;
	}

	public ObjectListBox<Type> getOlbYear() {
		return olbYear;
	}

	public void setOlbYear(ObjectListBox<Type> olbYear) {
		this.olbYear = olbYear;
	}

	public ObjectListBox<Type> getOlbCategory() {
		return olbCategory;
	}

	public void setOlbCategory(ObjectListBox<Type> olbCategory) {
		this.olbCategory = olbCategory;
	}



}
