package com.slicktechnologies.client.views.summaryscreen;

import java.awt.Color;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.apache.poi.hssf.record.DBCellRecord;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.elementparsers.IsEmptyParser;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.AutoFitWidthApproach;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.form.fields.DateRangeItem;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class SummaryForm extends FormScreen<DummyEntityOnlyForScreen> {
	
	
	/**
	 * Key fields or general filter
	 */
	Button newCustBtn;
	PersonInfoComposite personInfo;
	DateComparator dateComparator;
	ObjectListBox<Branch> olbBranch;
	ObjectListBox<Employee> olbSalesPerson;
	Button btnSearch,btnClear;
	
	
	/***************************** Sales Tab Panel *******************************/
	TabPanel salesTabPanel;
	
	// Lead Tab
	ListGrid dgSalesLead;
	DataSource dsSalesLead;
	VerticalPanel vpSalesLead;
	
	Button btnNewSalesLead;
	Button btnDownloadSalesLead;
	Label lbltotalSalesLeads, lbltotalSalesLeadAmt;
	
	//Quotation Tab
	ListGrid dgSalesQuotation;
	DataSource dsSalesQuotation;
	VerticalPanel vpSalesQuotation;
	
	Button btnNewSalesQuot;
	Button btnDownloadSalesQuot;
	Label lbltotalSalesQuots, lbltotalSalesQuotAmt;
	
	//Contract Tab
	ListGrid dgSalesOrder;
	DataSource dsSalesOrder;
	VerticalPanel vpSalesOrder;
	
	Button btnNewSalesOrder;
	Button btnDownloadSalesOrder;
	Label lbltotalSO, lbltotalSOAmt;
	
	//Delivery Note
	ListGrid dgDeliveryNote;
	DataSource dsDeliveryNote;
	VerticalPanel vpDeliveryNote;
	
	Button btnNewDeliveryNote;
	Button btnDownloadDeliveryNote;
	Label lbltotalDNotes;
	/***************************** Service Tab Panel ********************************/
	TabPanel serviceTabPanel;
	
	// Lead Tab
	ListGrid dgServiceLead;
	DataSource dsServiceLead;
	VerticalPanel vpServiceLead;
	
	Button btnNewSerLead;
	Button btnDownloadSerLead;
	Label lbltotalLeads, lbltotalLeadAmt;
	
	//Quotation Tab
	ListGrid dgServiceQuotation;
	DataSource dsServiceQuotation;
	VerticalPanel vpServiceQuotation;
	
	Button btnNewSerQuot;
	Button btnDownloadSerQuot;
	Label lbltotalQuots, lbltotalQuotAmt;
	
	//Contract Tab
	ListGrid dgContract;
	DataSource dsContract;
	VerticalPanel vpContract;
	
	Button btnNewContract;
	Button btnDownloadContract;
	
	Label lbltotalContracts, lbltotalConAmt;
	
	/************************************ Accounts Tab Panel ******************************/
	TabPanel accountsTabPanel;
	
	
	// Billing Tab
	ListGrid dgBill;
	DataSource dsBill;
	VerticalPanel vpBill;
	
	Button btnMergeBill;
	Button btnDownloadBill;
	Label lbltotalBills, lbltotalBillGrAmt;
	
	//Invoice Tab
	ListGrid dgInvoice;
	DataSource dsInvoice;
	VerticalPanel vpInvoice;
	
	Button btnDownloadInvoice;
	Label lbltotalInvoices, lbltotalInvGrAmt;
	
	//Payment Tab
	ListGrid dgPayment;
	DataSource dsPayment;
	VerticalPanel vpPaymentt;
	
	Button btnDownloadPayment;
	Label lbltotalPayments, lbltotalReceivable,lbltotalReceived;
	
	/************************************ Service Plan Tab **************************************/
	TabPanel servicePlanTabPanel;
	
	
	
	public List<String> lsLeadStatus;
	public List<String> lsQuotStatus;
	public List<String> lsOrderStatus;
	
	public List<String> lsInvoiceStatus;
	public List<String> lsPaymentStatus;//Ashwini Patil 9-9-2022 
	
	InlineLabel lblSerLead=null;
	InlineLabel lblSerQuot=null;
	InlineLabel lblContract=null;
	
	InlineLabel lblSerLead1=null;
	InlineLabel lblSerQuot1=null;
	InlineLabel lblContract1=null;
	
	
	
	public boolean defaultLoadingFlag=true;
	public List<Lead> globalSerLeadList;
	public List<Quotation> globalSerQuotList;
	public List<Contract> globalContractList;
	
	public List<BillingDocument> globalBillingDocList;
	public List<Invoice> globalInvoiceList;
	public List<CustomerPayment> globalPaymentList;
	
	
	public List<Lead> globalSalesLeadList;
	public List<SalesQuotation> globalSalesQuotList;
	public List<SalesOrder> globalSalesOrderList;
	public List<DeliveryNote> globalDeliveryNoteList;
	
	
	ListBox lbStaus;
	
	public static String className="technicianSchedule1";
	
	public List<String> tabNames,rollNames; //Ashwini Patil
	
	NumberFormat df=NumberFormat.getFormat("0.00");
	
	public  SummaryForm() {
		super();
		createGui();
	
		tabPanel.getWidget(0).getElement().getStyle().setMarginTop(0, Unit.PX);
		tabPanel.getWidget(1).getElement().getStyle().setMarginTop(0, Unit.PX);
		tabPanel.getWidget(2).getElement().getStyle().setMarginTop(0, Unit.PX);
		tabPanel.selectTab(1);
		
		
		
		tabPanel.getWidget(0).getElement().getStyle().setMarginBottom(23.9, Unit.PCT);
		tabPanel.getWidget(0).getElement().getStyle().setBorderWidth(0, Unit.PX);
		tabPanel.getWidget(1).getElement().getStyle().setMarginBottom(23.9, Unit.PCT);
		tabPanel.getWidget(1).getElement().getStyle().setBorderWidth(0, Unit.PX);
		tabPanel.getWidget(2).getElement().getStyle().setMarginBottom(23.9, Unit.PCT);
		tabPanel.getWidget(2).getElement().getStyle().setBorderWidth(0, Unit.PX);
		
//		Console.log("tabcount="+tabNames.size());
//		if(tabNames.size()>0) {
//		for(int i=0;i<tabNames.size();i++) {
//			tabPanel.getWidget(i).getElement().getStyle().setMarginTop(0, Unit.PX);			
//			tabPanel.getWidget(i).getElement().getStyle().setMarginBottom(23.9, Unit.PCT);
//			tabPanel.getWidget(i).getElement().getStyle().setBorderWidth(0, Unit.PX);
//		}
//		}
		//17-10-2022
		globalSerLeadList=new ArrayList<Lead>();
		globalSerQuotList=new ArrayList<Quotation>();
		globalContractList=new ArrayList<Contract>();
		
		globalBillingDocList=new ArrayList<BillingDocument>();
		globalInvoiceList=new ArrayList<Invoice>();
		globalPaymentList=new ArrayList<CustomerPayment>();
		
		
		globalSalesLeadList=new ArrayList<Lead>();
		globalSalesQuotList=new ArrayList<SalesQuotation>();
		globalSalesOrderList=new ArrayList<SalesOrder>();
		globalDeliveryNoteList=new ArrayList<DeliveryNote>();
			
	}
	
	public void loadLeadStatus(){
		lsLeadStatus=new ArrayList<String>();
		if(LoginPresenter.globalConfig!=null&&LoginPresenter.globalConfig.size()!=0){
			for(Config obj:LoginPresenter.globalConfig){
				if(!obj.getName().equals("Successful")&&!obj.getName().equals("Unsuccessful")&&!obj.getName().equals("Cancelled")&&obj.isStatus()==true&&obj.getType()==6){
					lsLeadStatus.add(obj.getName());
				}
			}
		}
	}
	private void initializeWidget(){
		loadLeadStatus();
		
		lsQuotStatus=new ArrayList<String>();
		lsQuotStatus.add("Created");
		lsQuotStatus.add("Requested");
		lsQuotStatus.add("Rejected");
		lsQuotStatus.add("Approved");
		lsOrderStatus=new ArrayList<String>();
		lsOrderStatus.add("Created");
		lsOrderStatus.add("Requested");
		lsOrderStatus.add("Invoiced");//21-09-2022
		lsOrderStatus.add("Rejected");
		lsOrderStatus.add("Approved");
		
		lsInvoiceStatus=new ArrayList<String>();
		lsInvoiceStatus.add("Created");
		lsInvoiceStatus.add("Requested");
		lsInvoiceStatus.add("Rejected");
		lsInvoiceStatus.add("PInvoice");
		lsInvoiceStatus.add("Invoiced");
		lsInvoiceStatus.add("Approved");//7-9-2022
		
		
		lsPaymentStatus=new ArrayList<String>();
		lsPaymentStatus.add("Created");
		lsPaymentStatus.add("Cancelled");
		lsPaymentStatus.add("Closed");
		
		newCustBtn=new Button("New Customer");
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,true,true,false);
		
//		MyQuerry querry=new MyQuerry();
//		querry.setQuerryObject(new Customer());
//		pic=new PersonInfoComposite(querry,true,true,true);
		
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		personInfo.getPanel().setWidth("100%");
		personInfo.getPanel().getWidget(0).setWidth("100%");
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		olbSalesPerson=new ObjectListBox<Employee>();
		olbSalesPerson.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.CUSTOMER, "Sales Person");
		
		dateComparator= new DateComparator("businessProcess.creationDate",new Customer());
		
		btnSearch=new Button("Go");
		btnClear=new Button("Clear");
		
		lbStaus=new ListBox();
		lbStaus.addItem("--SELECT--");
		lbStaus.addItem("Open");
		lbStaus.addItem("Closed");
		lbStaus.addItem("Cancelled");
//		newlbl=new Label("");
		
		lbltotalSalesLeads=new Label("");
		lbltotalSalesLeadAmt=new Label("");
		
		lbltotalSalesQuots=new Label("");
		lbltotalSalesQuotAmt=new Label("");
		
		lbltotalSO=new Label("");
		lbltotalSOAmt=new Label("");
		
		lbltotalDNotes=new Label("");
		
		lbltotalLeads=new Label("");
		lbltotalLeadAmt=new Label("");
		
		lbltotalQuots=new Label("");
		lbltotalQuotAmt=new Label("");
		
		lbltotalContracts=new Label("");
		lbltotalConAmt=new Label("");
		
		lbltotalBills=new Label("");
		lbltotalBillGrAmt=new Label("");
		
		lbltotalInvoices=new Label("");
		lbltotalInvGrAmt=new Label("");	
		
		lbltotalPayments=new Label("");
		lbltotalReceivable=new Label("");
		lbltotalReceived=new Label("");
		
		
		intializeSalesTabPanel();
		intializeServiceTabPanel();
		intializeAccountsTabPanel();
		intializeServicePlanTabPanel();
	}
	
	private void intializeSalesTabPanel() {
		salesTabPanel=new TabPanel();
		salesTabPanel.setWidth("100%");
		salesTabPanel.getDeckPanel().getElement().getStyle().setBorderWidth(0, Unit.PX);
		
		//Lead
		vpSalesLead=new VerticalPanel();
		vpSalesLead.setHeight("0%");
		vpSalesLead.setWidth("95%");
		vpSalesLead.add(intializeSalesLeadTabFilter()); 
		initializeSalesLeadDataGrid();
		salesTabPanel.add(vpSalesLead, "Lead");
		
		//Quotation
		vpSalesQuotation=new VerticalPanel();
		vpSalesQuotation.setHeight("0%");
		vpSalesQuotation.setWidth("95%");
		vpSalesQuotation.add(intializeSalesQuotTabFilter());
		initializeSalesQuotationDataGrid();
		salesTabPanel.add(vpSalesQuotation, "Quotation");
		
		//Contract
		vpSalesOrder=new VerticalPanel();
		vpSalesOrder.setHeight("0%");
		vpSalesOrder.setWidth("95%");
		vpSalesOrder.add(intializeSalesOrderTabFilter());
		initializeSalesOrderDataGrid();
		salesTabPanel.add(vpSalesOrder, "Sales Order");
		
		//Contract
		vpDeliveryNote=new VerticalPanel();
		vpDeliveryNote.setHeight("0%");
		vpDeliveryNote.setWidth("95%");
		vpDeliveryNote.add(intializeDeliveryNoteTabFilter());
		initializeDeliveryNoteDataGrid();
		salesTabPanel.add(vpDeliveryNote, "Delivery Note");
		
		salesTabPanel.selectTab(0);
	}

	private void initializeDeliveryNoteDataGrid() {
		// TODO Auto-generated method stub
		dgDeliveryNote = new ListGrid();
		setGridProperty(dgDeliveryNote);
		dgDeliveryNote.setFields(getDeliveryNoteGridFields());
		
		dsDeliveryNote= new DataSource();
		dsDeliveryNote.setClientOnly(true);
		setDeliveryNoteDataSourceField(dsDeliveryNote);
		dgDeliveryNote.setDataSource(dsDeliveryNote);
		dgDeliveryNote.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
		
		vpDeliveryNote.getElement().addClassName("varticalGridPanel1");
		dgDeliveryNote.getElement().addClassName("technicianSchedule2");
		vpDeliveryNote.add(dgDeliveryNote);
	}

	private HorizontalPanel intializeDeliveryNoteTabFilter() {
		btnNewDeliveryNote=new Button("<image src='/images/menubar/043-plus.png' width='25px' height='18px'/>");
		btnDownloadDeliveryNote=new Button("<image src='/images/menubar/146-download.png' width='25px' height='18px'/>");
		btnNewDeliveryNote.getElement().addClassName("buttonIcon");
		btnDownloadDeliveryNote.getElement().addClassName("buttonIcon");
		
		HorizontalPanel hPanel=new HorizontalPanel();
		
//		hPanel.add(btnNewDeliveryNote);
		hPanel.add(btnDownloadDeliveryNote);
		
		lbltotalDNotes.setVisible(false);
		lbltotalDNotes.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalDNotes.getElement().setClassName("processbaritems");
		lbltotalDNotes.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalDNotes.getElement().getStyle().setBackgroundImage(null);
		lbltotalDNotes.getElement().getStyle().setBackgroundColor("White");
		hPanel.add(lbltotalDNotes);
		
		hPanel.setSpacing(4);
		return hPanel;
	}

	private void intializeServiceTabPanel() {
		serviceTabPanel=new TabPanel();
		serviceTabPanel.setWidth("100%");
		serviceTabPanel.getDeckPanel().getElement().getStyle().setBorderWidth(0, Unit.PX);
		
		//Lead
		vpServiceLead=new VerticalPanel();
		vpServiceLead.setHeight("0%");
		vpServiceLead.setWidth("95%");
		vpServiceLead.add(intializeSerLeadTabFilter()); 
		initializeSerLeadDataGrid();
		serviceTabPanel.add(vpServiceLead, "Lead");
		
		//Quotation
		vpServiceQuotation=new VerticalPanel();
		vpServiceQuotation.setHeight("0%");
		vpServiceQuotation.setWidth("95%");
		vpServiceQuotation.add(intializeSerQuotTabFilter());
		initializeSerQuotationDataGrid();
		serviceTabPanel.add(vpServiceQuotation, "Quotation");
		
		//Contract
		vpContract=new VerticalPanel();
		vpContract.setHeight("0%");
		vpContract.setWidth("95%");
		vpContract.add(intializeServiceTabFilter());
		initializeContractDataGrid();
		serviceTabPanel.add(vpContract, "Contract");
		
		serviceTabPanel.selectTab(0);
	}
	
	private void initializeSerLeadDataGrid() {
		dgServiceLead = new ListGrid();
		setGridProperty(dgServiceLead);
		dgServiceLead.setFields(getServiceLeadGridFields());
		
		dsServiceLead= new DataSource();
		dsServiceLead.setClientOnly(true);
		setServiceLeadDataSourceField(dsServiceLead);
		dgServiceLead.setDataSource(dsServiceLead);
		dgServiceLead.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
		vpServiceLead.getElement().addClassName("varticalGridPanel1");
		dgServiceLead.getElement().addClassName("technicianSchedule2");//7-07-2022
//		dgServiceLead.getElement().addClassName(className);

		
		
		vpServiceLead.add(dgServiceLead);
	}
	
	private HorizontalPanel intializeSerLeadTabFilter() {
		btnNewSerLead=new Button("<image src='/images/menubar/043-plus.png' width='25px' height='18px'/>");
		btnDownloadSerLead=new Button("<image src='/images/menubar/146-download.png' width='25px' height='18px'/>");
		btnNewSerLead.getElement().addClassName("buttonIcon");
		btnDownloadSerLead.getElement().addClassName("buttonIcon");
		
		
		if(lblSerLead==null){
			lblSerLead=new InlineLabel("Total Lead : ");
			lblSerLead1=new InlineLabel("Total Amount : ");
			lblSerLead.setVisible(false);
			lblSerLead1.setVisible(false);
		}
		
//		lblSerLead.getElement().getStyle().setMarginLeft(1080, Unit.PX);
//		lblSerLead.setWidth("100px");
//		lblSerLead1.setWidth("160px");
//		lblSerLead.getElement().setClassName("processbaritems");
//		lblSerLead1.getElement().setClassName("processbaritems");
		
		HorizontalPanel hPanel=new HorizontalPanel();
		
		hPanel.add(btnNewSerLead);
		hPanel.add(btnDownloadSerLead);		
		
//		hPanel.add(lblSerLead);
//		hPanel.add(lblSerLead1);
		
		lbltotalLeads.setVisible(false);
		lbltotalLeads.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalLeads.getElement().setClassName("processbaritems");
		lbltotalLeads.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalLeads.getElement().getStyle().setBackgroundImage(null);
		lbltotalLeads.getElement().getStyle().setBackgroundColor("White");
		hPanel.add(lbltotalLeads);
				
		lbltotalLeadAmt.setVisible(false);
		lbltotalLeadAmt.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalLeadAmt.getElement().setClassName("processbaritems");
		lbltotalLeadAmt.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalLeadAmt.getElement().getStyle().setBackgroundImage(null);
		lbltotalLeadAmt.getElement().getStyle().setBackgroundColor("White");
		hPanel.add(lbltotalLeadAmt);
		
		hPanel.setSpacing(4);
		return hPanel;
	}

	private void initializeSerQuotationDataGrid() {
		dgServiceQuotation = new ListGrid();
		setGridProperty(dgServiceQuotation);
		dgServiceQuotation.setFields(getServiceQuotationGridFields());
		dsServiceQuotation= new DataSource();
		dsServiceQuotation.setClientOnly(true);
		setServiceQuotationDataSourceField(dsServiceQuotation);
		dgServiceQuotation.setDataSource(dsServiceQuotation);
		dgServiceQuotation.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
		vpServiceQuotation.getElement().addClassName("varticalGridPanel1");
		dgServiceQuotation.getElement().addClassName("technicianSchedule2");
		vpServiceQuotation.add(dgServiceQuotation);
	}

	private HorizontalPanel intializeSerQuotTabFilter() {
		btnNewSerQuot=new Button("<image src='/images/menubar/043-plus.png' width='25px' height='18px'/>");
		btnDownloadSerQuot=new Button("<image src='/images/menubar/146-download.png' width='25px' height='18px'/>");
		btnNewSerQuot.getElement().addClassName("buttonIcon");
		btnDownloadSerQuot.getElement().addClassName("buttonIcon");
		
//		lblSerQuot=new InlineLabel("Total Quotations : ");
//		lblSerQuot1=new InlineLabel("Total Amount : ");
//		lblSerQuot.setVisible(false);
//		lblSerQuot1.setVisible(false);
		
//		lblSerQuot.getElement().getStyle().setMarginLeft(1040, Unit.PX);
//		lblSerQuot.setWidth("140px");
//		lblSerQuot1.setWidth("160px");
//		lblSerQuot.getElement().setClassName("processbaritems");
//		lblSerQuot1.getElement().setClassName("processbaritems");
		
		HorizontalPanel hPanel=new HorizontalPanel();
		
		hPanel.add(btnNewSerQuot);
		hPanel.add(btnDownloadSerQuot);
		
//		hPanel.add(lblSerQuot);
//		hPanel.add(lblSerQuot1);
		
		lbltotalQuots.setVisible(false);
		lbltotalQuots.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalQuots.getElement().setClassName("processbaritems");
		lbltotalQuots.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalQuots.getElement().getStyle().setBackgroundImage(null);
		lbltotalQuots.getElement().getStyle().setBackgroundColor("White");
		hPanel.add(lbltotalQuots);
				
		lbltotalQuotAmt.setVisible(false);
		lbltotalQuotAmt.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalQuotAmt.getElement().setClassName("processbaritems");
		lbltotalQuotAmt.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalQuotAmt.getElement().getStyle().setBackgroundImage(null);
		lbltotalQuotAmt.getElement().getStyle().setBackgroundColor("White");
		hPanel.add(lbltotalQuotAmt);
		
		
		hPanel.setSpacing(4);
		return hPanel;
	}
	

	private HorizontalPanel intializeServiceTabFilter() {
		
		btnNewContract=new Button("<image src='/images/menubar/043-plus.png' width='25px' height='18px'/>");
		btnDownloadContract=new Button("<image src='/images/menubar/146-download.png' width='25px' height='18px'/>");
		btnNewContract.getElement().addClassName("buttonIcon");
		btnDownloadContract.getElement().addClassName("buttonIcon");
		
//		lblContract=new InlineLabel("Total Contract : ");
//		lblContract1=new InlineLabel("Total Amount : ");
//		lblContract.setVisible(false);
//		lblContract1.setVisible(false);
//		lblContract.getElement().getStyle().setMarginLeft(1050, Unit.PX);
//		lblContract.setWidth("125px");
//		lblContract1.setWidth("160px");
//		lblContract.getElement().setClassName("processbaritems");
//		lblContract1.getElement().setClassName("processbaritems");
		
		HorizontalPanel hPanel=new HorizontalPanel();
		
		hPanel.add(btnNewContract);
		hPanel.add(btnDownloadContract);
		
//		hPanel.add(lblContract);
//		hPanel.add(lblContract1);
		
		lbltotalContracts.setVisible(false);
		lbltotalContracts.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalContracts.getElement().setClassName("processbaritems");
		lbltotalContracts.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalContracts.getElement().getStyle().setBackgroundImage(null);
		lbltotalContracts.getElement().getStyle().setBackgroundColor("White");
		hPanel.add(lbltotalContracts);
				
		lbltotalConAmt.setVisible(false);
		lbltotalConAmt.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalConAmt.getElement().setClassName("processbaritems");
		lbltotalConAmt.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalConAmt.getElement().getStyle().setBackgroundImage(null);
		lbltotalConAmt.getElement().getStyle().setBackgroundColor("White");
		hPanel.add(lbltotalConAmt);
		
		hPanel.setSpacing(4);
		return hPanel;
	}

	private void initializeContractDataGrid() {
		dgContract = new ListGrid();
		setGridProperty(dgContract);
		dgContract.setFields(getContractGridFields());
		
		dsContract= new DataSource();
		dsContract.setClientOnly(true);
		setContractDataSourceField(dsContract);
		dgContract.setDataSource(dsContract);
		dgContract.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
		
		vpContract.getElement().addClassName("varticalGridPanel1");
		dgContract.getElement().addClassName("technicianSchedule2");
		vpContract.add(dgContract);
	}

	private void intializeAccountsTabPanel() {
		
		accountsTabPanel=new TabPanel();
		accountsTabPanel.setWidth("100%");
		accountsTabPanel.getDeckPanel().getElement().getStyle().setBorderWidth(0, Unit.PX);
		
		
		//Bill
		vpBill=new VerticalPanel();
		vpBill.setHeight("0%");
		vpBill.setWidth("95%");
		vpBill.add(intializeBillTabFilter());
		initializeBillDataGrid();
		accountsTabPanel.add(vpBill, "Bill");
		
		//Invoice
		vpInvoice=new VerticalPanel();
		vpInvoice.setHeight("0%");
		vpInvoice.setWidth("95%");
		vpInvoice.add(intializeInvoiceTabFilter());
		initializeInvoiceDataGrid();
		accountsTabPanel.add(vpInvoice, "Invoice");
		
		//Payment
		vpPaymentt=new VerticalPanel();
		vpPaymentt.setHeight("0%");
		vpPaymentt.setWidth("95%");
		vpPaymentt.add(intializePaymentTabFilter());
		initializePaymentDataGrid();
		accountsTabPanel.add(vpPaymentt, "Payment");
		
		
		
		
		
		
		accountsTabPanel.selectTab(0);
		
		
		
	}

	private void intializeServicePlanTabPanel() {
		VerticalPanel vpanel1=new VerticalPanel();
		
		servicePlanTabPanel=new TabPanel();
		servicePlanTabPanel.setWidth("100%");
		servicePlanTabPanel.add(vpanel1, "Service");
		servicePlanTabPanel.selectTab(0);
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		super.createScreen();
		
		//Ashwini Patil Date: 7-04-2022 for authorization
		
		tabNames=new ArrayList<String>();
		rollNames=new ArrayList<String>();
		List<ScreenAuthorization> moduleValidation = null;
		if(UserConfiguration.getRole()!=null){
			moduleValidation=UserConfiguration.getRole().getAuthorization();
		}
		for(int f=0;f<moduleValidation.size();f++)
		{
			Console.log("module access name="+moduleValidation.get(f).getModule().trim());						
			tabNames.add(moduleValidation.get(f).getModule().trim());
			tabNames.add(moduleValidation.get(f).getScreens().trim());
		}
//		tabNames.add("Sales");
//		tabNames.add("Quotation");
		Console.log("tabNames count="+tabNames.size());
		
		List<ScreenAuthorization> internalModuleValidation=UserConfiguration.getRole().getAuthorization();
		if(!UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)){
			Console.log("in internalModuleValidation" );	
			for(int f=0;f<internalModuleValidation.size();f++)
			{
				if(internalModuleValidation.get(f).getModule().trim().equals("Sales")){
					rollNames.add("Sales");
				}
				if(internalModuleValidation.get(f).getModule().trim().equals("Service")){
					rollNames.add("Service");
				}
				if(internalModuleValidation.get(f).getModule().trim().equals("Accounts")){
					rollNames.add("Accounts");
				}
				
			}
		}
		
		Console.log("rollNames count="+rollNames.size());
		initializeWidget();
		
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder();
		FormField fgroupingCustomerInformation=builder.setlabel("Home").widgetType(com.simplesoftwares.client.library.FieldType.Grouping).setMandatory(false).setColSpan(7).setKeyField(true).build();
		
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(6).build();
		
		builder = new FormFieldBuilder("",newCustBtn);
		FormField fnewCustBtn= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Sales Person",olbSalesPerson);
		FormField folbSalesPerson= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("From Date",dateComparator.getFromDate());
		FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date",dateComparator.getToDate());
		FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Status",lbStaus);
		FormField flbStaus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("",btnSearch);
		FormField fbtnSearch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("",btnClear);
		FormField fbtnClear= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder();
		FormField fSalesGrouping=builder.setlabel("Sales").widgetType(com.simplesoftwares.client.library.FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		builder = new FormFieldBuilder("",salesTabPanel);
		FormField fsalesTabPanel= builder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		builder = new FormFieldBuilder();
		FormField fServiceGrouping=builder.setlabel("Service").widgetType(com.simplesoftwares.client.library.FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		builder = new FormFieldBuilder("",serviceTabPanel);
		FormField fserviceTabPanel= builder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		builder = new FormFieldBuilder();
		FormField fAccountsGrouping=builder.setlabel("Accounts").widgetType(com.simplesoftwares.client.library.FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		builder = new FormFieldBuilder("",accountsTabPanel);
		FormField faccountsTabPanel= builder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		builder = new FormFieldBuilder();
		FormField fServicePlanGrouping=builder.setlabel("Service Plan").widgetType(com.simplesoftwares.client.library.FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		builder = new FormFieldBuilder("",servicePlanTabPanel);
		FormField fservicePlanTabPanel= builder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		
		this.fields=new FormField[][]{
				{fgroupingCustomerInformation},
				{fpersonInfo},
				{fdateComparator,fdateComparator1,folbBranch,folbSalesPerson,flbStaus,fbtnSearch,fbtnClear},
				{fSalesGrouping},
				{fsalesTabPanel},
				{fServiceGrouping},
				{fserviceTabPanel},
				{fAccountsGrouping},
				{faccountsTabPanel},
				{fServicePlanGrouping},
//				{fservicePlanTabPanel},
		};
		
//		this.fields=new FormField[10][10];
//		this.fields[0][0]=fgroupingCustomerInformation;
//		this.fields[1][0]=fpersonInfo;
//		this.fields[2][0]=fdateComparator;
//		this.fields[2][1]=fdateComparator1;
//		this.fields[2][2]=folbBranch;
//		this.fields[2][3]=folbSalesPerson;
//		this.fields[2][4]=flbStaus;
//		this.fields[2][5]=fbtnSearch;
//		this.fields[3][0]=fSalesGrouping;
//		this.fields[4][0]=fsalesTabPanel;
//		this.fields[5][0]=fServiceGrouping;
//		this.fields[6][0]=fserviceTabPanel;
//		this.fields[7][0]=fAccountsGrouping;
//		this.fields[8][0]=faccountsTabPanel;
//		this.fields[9][0]=fServicePlanGrouping;
	}
	
	@Override
	public void toggleAppHeaderBarMenu() {
		Console.log("in summary screen toggleAppHeaderBarMenu");
		super.toggleAppHeaderBarMenu();
		InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW){
			for(int k=0;k<menus.length;k++){
				String text=menus[k].getText();
				if(text.equals("Discard")){
					menus[k].setVisible(true); 
				}else{
					menus[k].setVisible(false); 
				}
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT){
			for(int k=0;k<menus.length;k++){
				String text=menus[k].getText();
				if(text.equals("Discard")){
					menus[k].setVisible(true); 
				}else{
					menus[k].setVisible(false);  
				}
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW){
			for(int k=0;k<menus.length;k++){
				String text=menus[k].getText();
				if(text.equals("Discard")){
					menus[k].setVisible(true); 
				}else{
					menus[k].setVisible(false);  
				}
			}
		}
		
		AuthorizationHelper.setAsPerAuthorization(Screen.SUMMARYSCREEN,"Home!");
	}

	@Override
	public boolean validate() {
		return super.validate();
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
	}

	@Override
	public void refreshTableData() {
		super.refreshTableData();
	}
	//ashwini check here for indexes issue
	public MyQuerry getQuerry(String tabName) {
		
		Date fromDate=new Date();
		Date toDate=new Date();
		
//		CalendarUtil.setToFirstDayOfMonth(fromDate);
//		CalendarUtil.addMonthsToDate(toDate, 1);
//		CalendarUtil.setToFirstDayOfMonth(toDate);
//		CalendarUtil.addDaysToDate(toDate, -1);
		
		
		//Ashwini Patil changing logic from month to week Date:17-08-2022
		CalendarUtil.addDaysToDate(fromDate, -6);
		
		Console.log("From Date : "+fromDate +" To Date : "+toDate+" Querry:- "+tabName+"defaultLoadingFlag="+defaultLoadingFlag);
		
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(temp);
		
		if(defaultLoadingFlag){
			/**
			 * @author Anil @since 15-09-2021
			 * mapping date and status when data is default loaded
			 * raised by Rahul Tiwari
			 */
			dateComparator.getFromDate().setValue(fromDate);
			dateComparator.getToDate().setValue(toDate);
			
			for(int i=0;i<lbStaus.getItemCount();i++){
//				if(lbStaus.getItemText(i).equals("Open")){
//					lbStaus.setSelectedIndex(i);
//					break;
//				}
//				
				lbStaus.setSelectedIndex(0); //Ashwini Patil setting default loading status to all Date:17-08-2022
			}
			
			Console.log("Default Loading.....!");
			if(tabName.equals("Service Lead")||tabName.equals("Sales Lead")){
				
				/**
				 * @author Anil @since 23-11-2021
				 */
				if(lsLeadStatus!=null&&lsLeadStatus.size()!=0){
					temp=new Filter();
					temp.setQuerryString("status IN");
					temp.setList(lsLeadStatus);
					filtervec.add(temp);
				}else{
					loadLeadStatus();
					if(lsLeadStatus!=null&&lsLeadStatus.size()!=0){
						temp=new Filter();
						temp.setQuerryString("status IN");
						temp.setList(lsLeadStatus);
						filtervec.add(temp);
					}
				}
				
				
				temp=new Filter();
				temp.setQuerryString("followUpDate"+" >=");
				temp.setDateValue(fromDate);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("followUpDate"+" <=");
				temp.setDateValue(toDate);
				filtervec.add(temp);
				
			}else if(tabName.equals("Service Quotation")||tabName.equals("Sales Quotation")){
				
				temp=new Filter();
				temp.setQuerryString("status IN");
				temp.setList(lsQuotStatus);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("quotationDate"+" >=");
				temp.setDateValue(fromDate);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("quotationDate"+" <=");
				temp.setDateValue(toDate);
				filtervec.add(temp);
				
			}else if(tabName.equals("Contract")){
				
				temp=new Filter();
				temp.setQuerryString("status IN");
				temp.setList(lsOrderStatus);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("contractDate"+" >=");
				temp.setDateValue(fromDate);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("contractDate"+" <=");
				temp.setDateValue(toDate);
				filtervec.add(temp);
			}else if(tabName.equals("Bill")){
				
				temp=new Filter();
				temp.setQuerryString("status IN");
				temp.setList(lsOrderStatus);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("billingDocumentInfo.billingDate"+" >=");
				temp.setDateValue(fromDate);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("billingDocumentInfo.billingDate"+" <=");
				temp.setDateValue(toDate);
				filtervec.add(temp);
				
			}else if(tabName.equals("Invoice")){
				
				temp=new Filter();
				temp.setQuerryString("status IN");
				temp.setList(lsInvoiceStatus);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("invoiceDate"+" >=");
				temp.setDateValue(fromDate);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("invoiceDate"+" <=");
				temp.setDateValue(toDate);
				filtervec.add(temp);
				
			}else if(tabName.equals("Payment")){
				
				temp=new Filter();
				temp.setQuerryString("status IN");
				temp.setList(lsPaymentStatus);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("paymentDate"+" >=");
				temp.setDateValue(fromDate);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("paymentDate"+" <=");
				temp.setDateValue(toDate);
				filtervec.add(temp);
				
			}else if(tabName.equals("Sales Order")){
				
				temp=new Filter();
				temp.setQuerryString("status IN");
				temp.setList(lsOrderStatus);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("salesOrderDate"+" >=");
				temp.setDateValue(fromDate);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("salesOrderDate"+" <=");
				temp.setDateValue(toDate);
				filtervec.add(temp);
			}else if(tabName.equals("Delivery Note")){
				
				temp=new Filter();
				temp.setQuerryString("status IN");
				temp.setList(lsOrderStatus);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("deliveryDate"+" >=");
				temp.setDateValue(fromDate);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("deliveryDate"+" <=");
				temp.setDateValue(toDate);
				filtervec.add(temp);
			}
			//Ashwini Patil Date:28-05-2024 Ultima search reported that branch level restriction is not working on summary screen so added this code
			if (LoginPresenter.branchRestrictionFlag == true) {
				temp = new Filter();
				
				temp.setList(AppUtility.getbranchlist());
				temp.setQuerryString("branch IN");
				filtervec.add(temp);
			} 
		}else{
			
			if(lbStaus.getSelectedIndex()!=0&&!isCustomerAndDateSelected()){
				Console.log("in if(lbStaus.getSelectedIndex()!=0&&!isCustomerAndDateSelected())");
				if(lbStaus.getValue(lbStaus.getSelectedIndex()).equals("Open")){
					Console.log("in if(lbStaus.getValue(lbStaus.getSelectedIndex()).equals(Open)");
					if(tabName.equals("Service Lead")||tabName.equals("Sales Lead")){
						temp=new Filter();
//						temp.setQuerryString("status IN");
//						temp.setList(lsLeadStatus);
						temp.setQuerryString("status");
						temp.setStringValue("Created");//Ashwini Patil
						filtervec.add(temp);
					}else if(tabName.equals("Service Quotation")||tabName.equals("Sales Quotation")){
						temp=new Filter();
//						temp.setQuerryString("status IN");
//						temp.setList(lsQuotStatus);
						temp.setQuerryString("status");
						temp.setStringValue("Created");//Ashwini Patil
						filtervec.add(temp);
					}else if(tabName.equals("Contract")||tabName.equals("Sales Order")){
						Console.log("in sales order if");
						temp=new Filter();
//						temp.setQuerryString("status IN");
//						temp.setList(lsOrderStatus);
						temp.setQuerryString("status");
						temp.setStringValue("Created");//Ashwini Patil
						filtervec.add(temp);
					}else if(tabName.equals("Bill")){
						temp=new Filter();
//						temp.setQuerryString("status IN");
//						temp.setList(lsOrderStatus);
						temp.setQuerryString("status");
						temp.setStringValue("Created");//Ashwini Patil
						filtervec.add(temp);
					}else if(tabName.equals("Invoice")){
						temp=new Filter();
//						temp.setQuerryString("status IN");
//						temp.setList(lsInvoiceStatus);
						temp.setQuerryString("status");
						temp.setStringValue("Created"); //Ashwini Patil
						filtervec.add(temp);
					}else if(tabName.equals("Payment")){
						temp=new Filter();
						temp.setQuerryString("status");
						temp.setStringValue("Created");
						filtervec.add(temp);
					}else if(tabName.equals("Delivery Note")){
						temp=new Filter();
						temp.setQuerryString("status");
						temp.setStringValue("Created");
						filtervec.add(temp);
					}
				}else{
					String status=lbStaus.getValue(lbStaus.getSelectedIndex());
					if(tabName.equals("Service Lead")||tabName.equals("Sales Lead")){
						temp=new Filter();
						temp.setQuerryString("status");
						temp.setStringValue(status);
						if(status.equals("Closed")){
							temp.setStringValue("Successful");
						}
						filtervec.add(temp);
					}else if(tabName.equals("Service Quotation")||tabName.equals("Sales Quotation")){
						temp=new Filter();
						temp.setQuerryString("status");
						temp.setStringValue(status);
						if(status.equals("Closed")){
							temp.setStringValue("Successful");
						}
						filtervec.add(temp);
					}else if(tabName.equals("Contract")||tabName.equals("Sales Order")){
						temp=new Filter();
						temp.setQuerryString("status");
						temp.setStringValue(status);
						if(status.equals("Closed")){
							temp.setStringValue("Approved");
						}
						filtervec.add(temp);
					}else if(tabName.equals("Bill")){
						temp=new Filter();
						temp.setQuerryString("status");
						temp.setStringValue(status);
						if(status.equals("Closed")){
							temp.setStringValue("Invoiced");
						}
						filtervec.add(temp);
					}else if(tabName.equals("Invoice")){
						temp=new Filter();
						temp.setQuerryString("status");
						temp.setStringValue(status);
						if(status.equals("Closed")){
							temp.setStringValue("Approved");
						}
						filtervec.add(temp);
					}else if(tabName.equals("Payment")){
						temp=new Filter();
						temp.setQuerryString("status");
						temp.setStringValue(status);
						filtervec.add(temp);
					}else if(tabName.equals("Delivery Note")){
						temp=new Filter();
						temp.setQuerryString("status");
						temp.setStringValue(status);
						filtervec.add(temp);
					}
				}
			}
			//Ashwini Patil Date:17-08-2022
//			-------------------
			if(lbStaus.getSelectedIndex()==0 && personInfo.getIdValue() <1 &&(dateComparator.getFromDate().getValue()!=null||dateComparator.getToDate().getValue()!=null)) {//14-10-2022
				Console.log("in else");
				if(tabName.equals("Contract")){
					temp=new Filter();
					temp.setQuerryString("status IN");
					temp.setList(lsOrderStatus);
					filtervec.add(temp);					
				}
				else if(tabName.equals("Service Quotation")||tabName.equals("Sales Quotation")){					
					temp=new Filter();
					temp.setQuerryString("status IN");
					temp.setList(lsQuotStatus);
					filtervec.add(temp);
				}
				else if(tabName.equals("Bill")){					
					temp=new Filter();
					temp.setQuerryString("status IN");
					temp.setList(lsOrderStatus);
					filtervec.add(temp);
				}
				else if(tabName.equals("Invoice")){
					
					temp=new Filter();
					temp.setQuerryString("status IN");
					temp.setList(lsInvoiceStatus);
					filtervec.add(temp);
				}
				else if(tabName.equals("Sales Order")){
					
					temp=new Filter();
					temp.setQuerryString("status IN");
					temp.setList(lsOrderStatus);
					filtervec.add(temp);
				}
				else if(tabName.equals("Delivery Note")){
					
					temp=new Filter();
					temp.setQuerryString("status IN");
					temp.setList(lsOrderStatus);
					filtervec.add(temp);
				}
				else if(tabName.equals("Payment")) {
					temp=new Filter();
					temp.setQuerryString("status IN");
					temp.setList(lsPaymentStatus);
					filtervec.add(temp);
				}
			}
//			--------------------
			
			String dtQueryString="creationDate";
			String custQueryString="personInfo.";
			if(tabName.equals("Service Lead")||tabName.equals("Sales Lead")){
				dtQueryString="followUpDate";
				custQueryString="personInfo.";
			}else if(tabName.equals("Service Quotation")||tabName.equals("Sales Quotation")||tabName.equals("Quotation")){
				dtQueryString="quotationDate";
				custQueryString="cinfo.";
			}else if(tabName.equals("Contract")){
				dtQueryString="contractDate";
				custQueryString="cinfo.";
			}else if(tabName.equals("Sales Order")){
				dtQueryString="salesOrderDate";
				custQueryString="cinfo.";
			}else if(tabName.equals("Delivery Note")){
				dtQueryString="deliveryDate";
				custQueryString="cinfo.";
			}else if(tabName.equals("Bill")){ //added by ashwini bill, invoice and payment if
				dtQueryString="billingDocumentInfo.billingDate";
				custQueryString="personInfo.";
			}else if(tabName.equals("Invoice")){
				dtQueryString="invoiceDate";
				custQueryString="personInfo.";
			}else if(tabName.equals("Payment")){
				dtQueryString="paymentDate";
				custQueryString="personInfo.";
			}
			
			
			if(dateComparator.getFromDate().getValue()!=null){
				Console.log("in if(dateComparator.getFromDate().getValue()!=null)");
				if(dateComparator.getToDate().getValue()==null){
					temp=new Filter();
					temp.setQuerryString(dtQueryString+" >=");
					temp.setDateValue(dateComparator.getFromDate().getValue());
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setQuerryString(dtQueryString+" <=");
					temp.setDateValue(dateComparator.getFromDate().getValue());
					filtervec.add(temp);
				}
				else{
					temp=new Filter();
					temp.setQuerryString(dtQueryString+" >=");
					temp.setDateValue(dateComparator.getFromDate().getValue());
					filtervec.add(temp);
				}
			}
			if(dateComparator.getToDate().getValue()!=null){
				temp=new Filter();
				temp.setQuerryString(dtQueryString+" <=");
				temp.setDateValue(dateComparator.getToDate().getValue());
				filtervec.add(temp);
			}
			
			if (olbSalesPerson.getSelectedIndex() != 0&&!isCustomerAndDateSelected()) {
				
				if((tabName.equals("Bill")||tabName.equals("Invoice")||tabName.equals("Payment"))){
					if(!isSalesPersonAndDateSelected()){
						temp = new Filter();
						temp.setStringValue(olbSalesPerson.getValue().trim());
						temp.setQuerryString("employee");
						filtervec.add(temp);
					}
				}else{
					temp = new Filter();
					temp.setStringValue(olbSalesPerson.getValue().trim());
					temp.setQuerryString("employee");
					filtervec.add(temp);
				}
			}
	
			if (LoginPresenter.branchRestrictionFlag == true&& olbBranch.getSelectedIndex() == 0&&!isCustomerAndDateSelected()) {
				temp = new Filter();
				temp.setList(AppUtility.getbranchlist());
				temp.setQuerryString("branch IN");
				filtervec.add(temp);
			} else {
				if (olbBranch.getSelectedIndex() != 0&&!isCustomerAndDateSelected()) {
					temp = new Filter();
					temp.setStringValue(olbBranch.getValue().trim());
					temp.setQuerryString("branch");
					filtervec.add(temp);
				}
			}
			
			if (personInfo.getIdValue() != -1) {
				temp = new Filter();
				temp.setIntValue(personInfo.getIdValue());
				temp.setQuerryString(custQueryString+"count");
				filtervec.add(temp);
			}
//			if (!(personInfo.getFullNameValue().equals(""))) {
//				temp = new Filter();
//				temp.setStringValue(personInfo.getFullNameValue());
//				temp.setQuerryString(custQueryString+"fullName");
//				filtervec.add(temp);
//			}
//			
//			if(tabName.equals("Service Lead")||tabName.equals("Sales Lead")){
//				if (personInfo.getCellValue() != -1l) {
//					temp = new Filter();
//					temp.setLongValue(personInfo.getCellValue());
//					temp.setQuerryString(custQueryString+"cellNumber");
//					filtervec.add(temp);
//				}
//			}
		}
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		
		if(tabName.equals("Service Lead")||tabName.equals("Sales Lead")){
			querry.setQuerryObject(new Lead());
		}else if(tabName.equals("Service Quotation")){
			querry.setQuerryObject(new Quotation());
		}else if(tabName.equals("Contract")){
			querry.setQuerryObject(new Contract());
		}else if(tabName.equals("Bill")){
			querry.setQuerryObject(new BillingDocument());
		}else if(tabName.equals("Invoice")){
			querry.setQuerryObject(new Invoice());
		}else if(tabName.equals("Payment")){
			querry.setQuerryObject(new CustomerPayment());
		}else if(tabName.equals("Sales Quotation")){
			querry.setQuerryObject(new SalesQuotation());
		}else if(tabName.equals("Sales Order")){
			querry.setQuerryObject(new SalesOrder());
		}else if(tabName.equals("Delivery Note")){
			querry.setQuerryObject(new DeliveryNote());
		}
		
		
		return querry;
	}

	private void setGridProperty(ListGrid grid) {
	   grid.setWidth("91.99%");
	   grid.setHeight("40%");
	   grid.setAutoFetchData(true);
	   grid.setAlternateRecordStyles(true);
	   grid.setShowAllRecords(true);
	   grid.setFixedRecordHeights(true);
	   grid.setAutoFitData(Autofit.HORIZONTAL);	
	   grid.setAutoFitWidthApproach(AutoFitWidthApproach.BOTH);	
	   grid.setCanResizeFields(true);
	   
	   grid.setShowFilterEditor(true);
	   grid.setFilterOnKeypress(true);
	   
	   grid.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
	  
	}
	
	private ListGridField[] getContractGridFields() {
	   ListGridField print = new ListGridField("print"," ");
	   print.setWrap(true);
	   print.setAlign(Alignment.CENTER);
	   print.setType(ListGridFieldType.IMAGE);
	   print.setDefaultValue("/images/menubar/119-printer.png");
	   print.setCanEdit(false);
	   print.setWidth(40);
	   
	   ListGridField email = new ListGridField("email"," ");
	   email.setWrap(true);
	   email.setAlign(Alignment.CENTER);
	   email.setType(ListGridFieldType.IMAGE);
	   email.setDefaultValue("/images/menubar/159-email.png");
	   email.setCanEdit(false);
	   email.setWidth(40);
	   
	   ListGridField orderDate = new ListGridField("orderDate","Order Date");
	   orderDate.setWrap(true);
	   orderDate.setAlign(Alignment.CENTER);
	   
	   ListGridField customer = new ListGridField("customer","Customer");
	   customer.setWrap(true);
	   customer.setAlign(Alignment.CENTER);
	   
	   ListGridField salesPerson = new ListGridField("salesPerson","Sales Person");
	   salesPerson.setWrap(true);
	   salesPerson.setAlign(Alignment.CENTER);
	   
	   ListGridField cellNumber = new ListGridField("cellNumber","Cell Number");
	   cellNumber.setWrap(true);
	   cellNumber.setAlign(Alignment.CENTER);
	   
	   ListGridField status = new ListGridField("status","Status");
	   status.setWrap(true);
	   status.setAlign(Alignment.CENTER);
	   
	   ListGridField amount = new ListGridField("amount","Amount");
	   amount.setWrap(true);
	   amount.setAlign(Alignment.CENTER);
	   
	   ListGridField branch = new ListGridField("branch","Branch");
	   branch.setWrap(true);
	   branch.setAlign(Alignment.CENTER);
	   
	   ListGridField approver = new ListGridField("approver","Approver");
	   approver.setWrap(true);
	   approver.setAlign(Alignment.CENTER);
	   
	   ListGridField numberRange = new ListGridField("numberRange","Number Range");
	   numberRange.setWrap(true);
	   numberRange.setAlign(Alignment.CENTER);
	   
	   ListGridField orderId = new ListGridField("orderId","Order Id"); 
	   orderId.setWrap(true);
	   orderId.setAlign(Alignment.CENTER);
	   //Updated By: Ashwini Patil Date: 25-12-2021 Description: to display orderId on grid with underline
	   orderId.setCellFormatter(new CellFormatter() {
		@Override
		public String format(Object value, ListGridRecord record, int rowNum,
				int colNum) {
			// TODO Auto-generated method stub
			String id = record.getAttributeAsString("orderId");
			return "<u>"+id+"</u>";
		}
	});
	   
	   ListGridField quotationId = new ListGridField("quotationId","Quotation Id");
	   quotationId.setWrap(true);
	   quotationId.setAlign(Alignment.CENTER);
	 //Updated By: Ashwini Patil Date: 25-12-2021 Description: to display orderId on grid with underline
	   quotationId.setCellFormatter(new CellFormatter() {
		@Override
		public String format(Object value, ListGridRecord record, int rowNum,
				int colNum) {
			// TODO Auto-generated method stub
			String id = record.getAttributeAsString("quotationId");
			return "<u>"+id+"</u>";
		}
	});
	   
	   ListGridField leadId = new ListGridField("leadId","Lead Id");
	   leadId.setWrap(true);
	   leadId.setAlign(Alignment.CENTER);
	 //Updated By: Ashwini Patil Date: 25-12-2021 Description: to display orderId on grid with underline
	   leadId.setCellFormatter(new CellFormatter() {
		@Override
		public String format(Object value, ListGridRecord record, int rowNum,
				int colNum) {
			// TODO Auto-generated method stub
			String id = record.getAttributeAsString("leadId");
			return "<u>"+id+"</u>";
		}
	});
	  
	   
	   return new ListGridField[] { 
			  print,email,orderId, orderDate,customer,salesPerson,cellNumber,status,amount,branch,approver,numberRange,
			   quotationId,leadId //Ashwini removed one comma after leadId
	   };

	}
	
	protected void setContractDataSourceField(DataSource dataSource) {
	   DataSourceField email = new DataSourceField("email", FieldType.IMAGE);
	   DataSourceField print = new DataSourceField("print", FieldType.IMAGE);
	   DataSourceDateField orderDate = new DataSourceDateField("orderDate");
	   orderDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
	   
	   DataSourceField customer = new DataSourceField("customer", FieldType.TEXT);
	   DataSourceField salesPerson = new DataSourceField("salesPerson", FieldType.TEXT);
	   DataSourceField cellNumber = new DataSourceField("cellNumber", FieldType.TEXT);
	   DataSourceField status = new DataSourceField("status", FieldType.TEXT);
	   DataSourceField amount = new DataSourceField("amount", FieldType.TEXT);
	   DataSourceField branch = new DataSourceField("branch", FieldType.TEXT);
	   DataSourceField approver = new DataSourceField("approver", FieldType.TEXT);
	   DataSourceField numberRange = new DataSourceField("numberRange", FieldType.TEXT);
	   
	   DataSourceField orderId = new DataSourceField("orderId", FieldType.INTEGER);
	   orderId.setPrimaryKey(true);
	   DataSourceField quotationId = new DataSourceField("quotationId", FieldType.INTEGER);
	   DataSourceField leadId = new DataSourceField("leadId", FieldType.INTEGER);
	   
	   dataSource.setFields(print,email,orderId,orderDate,customer,salesPerson,cellNumber,status,amount,branch,approver,numberRange,quotationId,leadId);
	}
	
	public void setDataToContractListGrid(List<Contract> list){
		//Ashwini Patil Date:6-09-2022
		Collections.sort(list, new Comparator<Contract>() {
				public int compare(Contract o1, Contract o2) {
						 return o2.getContractDate().compareTo(o1.getContractDate());
					}
			});
		dsContract.setTestData(getContractGridData(list));
		dgContract.setDataSource(dsContract);
		dgContract.setFields(getContractGridFields());
		dgContract.fetchData();
		
		
		int total=globalContractList.size();
		double totalamt=0;
		for(Contract obj:globalContractList){
			totalamt=totalamt+obj.getNetpayable();
		}
				
		lbltotalContracts.setText("Count: "+total);
		lbltotalConAmt.setText("Total Amount: "+totalamt);
		lbltotalContracts.setVisible(true);
		lbltotalConAmt.setVisible(true);
	}
	
	private ListGridRecord[] getContractGridData(List<Contract> contractList) {
		ListGridRecord[] records = null; 
		 records = new ListGridRecord[contractList.size()];
		 
		 for(int i=0;i<contractList.size();i++){
			 
			 ListGridRecord record = new ListGridRecord();
			 Contract contract=contractList.get(i);
			 
			 record.setAttribute("email", "/images/menubar/159-email.png");
			 record.setAttribute("print", "/images/menubar/119-printer.png");
			 
			 
			 if(contract.getContractDate()!=null){
				 record.setAttribute("orderDate", contract.getContractDate());
			 }else{
				 record.setAttribute("orderDate", "");
			 }
			 record.setAttribute("customer",contract.getCinfo().getFullName());
			 if(contract.getEmployee()!=null){
				 record.setAttribute("salesPerson", contract.getEmployee());
			 }else{
				 record.setAttribute("salesPerson", "");
			 }
			 record.setAttribute("cellNumber", contract.getCinfo().getCellNumber().toString());
			 record.setAttribute("status",contract.getStatus());
			 record.setAttribute("amount", contract.getNetpayable());
			 record.setAttribute("branch", contract.getBranch());
			 record.setAttribute("approver", contract.getApproverName());
			 if(contract.getNumberRange()!=null){
				 record.setAttribute("numberRange", contract.getNumberRange());
			 }else{
				 record.setAttribute("numberRange", "");
			 }
			 record.setAttribute("orderId", contract.getCount());
			 if(contract.getQuotationCount()!=0&&contract.getQuotationCount()!=-1){
				 record.setAttribute("quotationId", contract.getQuotationCount());
			 }else{
				 record.setAttribute("quotationId", "");
			 }
			 if(contract.getLeadCount()!=0&&contract.getLeadCount()!=-1){
				 record.setAttribute("leadId", contract.getLeadCount());
			 }else{
				 record.setAttribute("leadId", "");
			 }
			 records[i]=record;
		 }
		 return records;
	}
	
	public Record addDateToContractDG(Contract contract) {
		ListGridRecord record = new ListGridRecord();
		 record.setAttribute("email", "/images/menubar/159-email.png");
		 record.setAttribute("print", "/images/menubar/119-printer.png");
		 
		 
		 if(contract.getContractDate()!=null){
			 record.setAttribute("orderDate", contract.getContractDate());
		 }else{
			 record.setAttribute("orderDate", "");
		 }
		 record.setAttribute("customer",contract.getCinfo().getFullName());
		 if(contract.getEmployee()!=null){
			 record.setAttribute("salesPerson", contract.getEmployee());
		 }else{
			 record.setAttribute("salesPerson", "");
		 }
		 record.setAttribute("cellNumber", contract.getCinfo().getCellNumber());
		 record.setAttribute("status",contract.getStatus());
		 record.setAttribute("amount", contract.getNetpayable());
		 record.setAttribute("branch", contract.getBranch());
		 record.setAttribute("approver", contract.getApproverName());
		 if(contract.getNumberRange()!=null){
			 record.setAttribute("numberRange", contract.getNumberRange());
		 }else{
			 record.setAttribute("numberRange", "");
		 }
		 record.setAttribute("orderId", contract.getCount());
		 if(contract.getQuotationCount()!=0&&contract.getQuotationCount()!=-1){
			 record.setAttribute("quotationId", contract.getQuotationCount());
		 }else{
			 record.setAttribute("quotationId", "");
		 }
		 if(contract.getLeadCount()!=0&&contract.getLeadCount()!=-1){
			 record.setAttribute("leadId", contract.getLeadCount());
		 }else{
			 record.setAttribute("leadId", "");
		 }
		return record;
	}
	
	private ListGridField[] getServiceQuotationGridFields() {
	   ListGridField print = new ListGridField("print"," ");
	   print.setWrap(true);
	   print.setAlign(Alignment.CENTER);
	   print.setType(ListGridFieldType.IMAGE);
	   print.setDefaultValue("/images/menubar/119-printer.png");
	   print.setCanEdit(false);
	   print.setWidth(40);
	   
	   ListGridField email = new ListGridField("email"," ");
	   email.setWrap(true);
	   email.setAlign(Alignment.CENTER);
	   email.setType(ListGridFieldType.IMAGE);
	   email.setDefaultValue("/images/menubar/159-email.png");
	   email.setCanEdit(false);
	   email.setWidth(40);
	   
	   ListGridField followup = new ListGridField("followup"," ");
	   followup.setWrap(true);
	   followup.setAlign(Alignment.CENTER);
	   followup.setType(ListGridFieldType.IMAGE);
	   followup.setDefaultValue("/images/menubar/044-file.png");
	   followup.setCanEdit(false);
	   followup.setWidth(40);
	   
	   ListGridField followupDate = new ListGridField("followupDate","Follow-Date");
	   followupDate.setWrap(true);
	   followupDate.setAlign(Alignment.CENTER);
	   
	   ListGridField quotationDate = new ListGridField("quotationDate","Quotation Date");
	   quotationDate.setWrap(true);
	   quotationDate.setAlign(Alignment.CENTER);
	   quotationDate.setWidth(135);
	   
	   ListGridField customer = new ListGridField("customer","Customer");
	   customer.setWrap(true);
	   customer.setAlign(Alignment.CENTER);
	   
	   ListGridField salesPerson = new ListGridField("salesPerson","Sales Person");
	   salesPerson.setWrap(true);
	   salesPerson.setAlign(Alignment.CENTER);
	   
	   ListGridField cellNumber = new ListGridField("cellNumber","Cell Number");
	   cellNumber.setWrap(true);
	   cellNumber.setAlign(Alignment.CENTER);
	   
	   ListGridField status = new ListGridField("status","Status");
	   status.setWrap(true);
	   status.setAlign(Alignment.CENTER);
	   
	   ListGridField amount = new ListGridField("amount","Amount");
	   amount.setWrap(true);
	   amount.setAlign(Alignment.CENTER);
	   
	   ListGridField quotationId = new ListGridField("quotationId","Quotation Id");
	   quotationId.setWrap(true);
	   quotationId.setAlign(Alignment.CENTER);

	   //Updated By: Ashwini Patil Date: 25-12-2021 Description: to display quotationId on grid with underline
	   quotationId.setCellFormatter(new CellFormatter() {
	 		@Override
	 		public String format(Object value, ListGridRecord record, int rowNum,
	 				int colNum) {
	 			// TODO Auto-generated method stub
	 			String id = record.getAttributeAsString("quotationId");
	 			return "<u>"+id+"</u>";
	 		}
	 	});
	   
	   
	   ListGridField leadId = new ListGridField("leadId","Lead Id");
	   leadId.setWrap(true);
	   leadId.setAlign(Alignment.CENTER);
	 //Updated By: Ashwini Patil Date: 25-12-2021 Description: to display quotationId on grid with underline
	   leadId.setCellFormatter(new CellFormatter() {
	 		@Override
	 		public String format(Object value, ListGridRecord record, int rowNum,
	 				int colNum) {
	 			// TODO Auto-generated method stub
	 			String id = record.getAttributeAsString("leadId");
	 			return "<u>"+id+"</u>";
	 		}
	 	});
	  
	   
	   ListGridField orderId = new ListGridField("orderId","Order Id"); 
	   orderId.setWrap(true);
	   orderId.setAlign(Alignment.CENTER);
	 //Updated By: Ashwini Patil Date: 25-12-2021 Description: to display quotationId on grid with underline
	   orderId.setCellFormatter(new CellFormatter() {
	 		@Override
	 		public String format(Object value, ListGridRecord record, int rowNum,
	 				int colNum) {
	 			// TODO Auto-generated method stub
	 			String id = record.getAttributeAsString("orderId");
	 			return "<u>"+id+"</u>";
	 		}
	 	});
	  
	   
	   ListGridField branch = new ListGridField("branch","Branch");
	   branch.setWrap(true);
	   branch.setAlign(Alignment.CENTER);
	   
	   return new ListGridField[] {print,email,followup,quotationId,followupDate,quotationDate,customer,salesPerson,
			   cellNumber,status,amount,leadId,orderId,branch
	   };

	}
		
	protected void setServiceQuotationDataSourceField(DataSource dataSource) {
	   DataSourceField email = new DataSourceField("email", FieldType.IMAGE);
	   DataSourceField print = new DataSourceField("print", FieldType.IMAGE);
	   DataSourceField followup = new DataSourceField("followup", FieldType.IMAGE);
	   
	   DataSourceDateField followupDate = new DataSourceDateField("followupDate");
	   DataSourceDateField quotationDate = new DataSourceDateField("quotationDate");
	   DataSourceDateField validuntil = new DataSourceDateField("validuntil");
	   
	   followupDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
	   quotationDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
	   validuntil.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
	   
	   DataSourceField customer = new DataSourceField("customer", FieldType.TEXT);
	   DataSourceField salesPerson = new DataSourceField("salesPerson", FieldType.TEXT);
	   DataSourceField cellNumber = new DataSourceField("cellNumber", FieldType.TEXT);
	   DataSourceField status = new DataSourceField("status", FieldType.TEXT);
	   DataSourceField amount = new DataSourceField("amount", FieldType.TEXT);
	   
	   DataSourceField quotationId = new DataSourceField("quotationId", FieldType.INTEGER);
	   quotationId.setPrimaryKey(true);
	   DataSourceField leadId = new DataSourceField("leadId", FieldType.INTEGER);
	   DataSourceField orderId = new DataSourceField("orderId", FieldType.INTEGER);
	   DataSourceField branch = new DataSourceField("branch", FieldType.TEXT);
	   
	   
	   dataSource.setFields(print,email,followup,quotationId,followupDate,quotationDate,customer,salesPerson,cellNumber,status,amount,validuntil,leadId,orderId,branch);
	}
	
	public void setDataToSerQuotLG(List<Quotation> list){
		//Ashwini Patil Date:6-09-2022
		Collections.sort(list, new Comparator<Quotation>() {
						public int compare(Quotation o1, Quotation o2) {
								 return o2.getFollowUpDate().compareTo(o1.getFollowUpDate());
							}
					});		
		dsServiceQuotation.setTestData(getSerQuotGridData(list));
		dgServiceQuotation.setDataSource(dsServiceQuotation);
		dgServiceQuotation.setFields(getServiceQuotationGridFields());
		dgServiceQuotation.fetchData();
		
		int total=globalSerQuotList.size();
		double totalamt=0;
		for(Quotation obj:globalSerQuotList){
			totalamt=totalamt+obj.getNetpayable();
		}
						
		lbltotalQuots.setText("Count: "+total);
		lbltotalQuotAmt.setText("Total Amount: "+totalamt);
		lbltotalQuots.setVisible(true);
		lbltotalQuotAmt.setVisible(true);
		
	}
	
	private ListGridRecord[] getSerQuotGridData(List<Quotation> quotList) {
		ListGridRecord[] records = null; 
		 records = new ListGridRecord[quotList.size()];
		 
		 for(int i=0;i<quotList.size();i++){
			 
			 ListGridRecord record = new ListGridRecord();
			 Quotation quotation=quotList.get(i);
			 
			 record.setAttribute("email", "/images/menubar/159-email.png");
			 record.setAttribute("print", "/images/menubar/119-printer.png");
			 record.setAttribute("followup", "/images/menubar/044-file.png");
			 
			 if(quotation.getFollowUpDate()!=null){
				 record.setAttribute("followupDate", quotation.getFollowUpDate());
			 }else{
				 record.setAttribute("followupDate", "");
			 }
			 if(quotation.getQuotationDate()!=null){
				 record.setAttribute("quotationDate", quotation.getQuotationDate());
			 }else{
				 record.setAttribute("quotationDate", "");
			 }
			 record.setAttribute("customer",quotation.getCinfo().getFullName());
			 if(quotation.getEmployee()!=null){
				 record.setAttribute("salesPerson", quotation.getEmployee());
			 }else{
				 record.setAttribute("salesPerson", "");
			 }
			 record.setAttribute("cellNumber", quotation.getCinfo().getCellNumber().toString());
			 record.setAttribute("status",quotation.getStatus());
			 record.setAttribute("amount", quotation.getNetpayable());
			 record.setAttribute("branch", quotation.getBranch());
			
			 
			 record.setAttribute("quotationId", quotation.getCount());
			 if(quotation.getContractCount()!=0&&quotation.getContractCount()!=-1){
				 record.setAttribute("orderId", quotation.getContractCount());
			 }else{
				 record.setAttribute("orderId", "");
			 }
			 if(quotation.getLeadCount()!=0&&quotation.getLeadCount()!=-1){
				 record.setAttribute("leadId", quotation.getLeadCount());
			 }else{
				 record.setAttribute("leadId", "");
			 }
			 records[i]=record;
			 
			
		 }
		 return records;
	}
	
	public Record addDateToSerQuotDG(Quotation quotation) {
		 ListGridRecord record = new ListGridRecord();
		 record.setAttribute("email", "/images/menubar/159-email.png");
		 record.setAttribute("print", "/images/menubar/119-printer.png");
		 record.setAttribute("followup", "/images/menubar/044-file.png");
		 
		 if(quotation.getFollowUpDate()!=null){
			 record.setAttribute("followupDate", quotation.getFollowUpDate());
		 }else{
			 record.setAttribute("followupDate", "");
		 }
		 if(quotation.getQuotationDate()!=null){
			 record.setAttribute("quotationDate", quotation.getQuotationDate());
		 }else{
			 record.setAttribute("quotationDate", "");
		 }
		 record.setAttribute("customer",quotation.getCinfo().getFullName());
		 if(quotation.getEmployee()!=null){
			 record.setAttribute("salesPerson", quotation.getEmployee());
		 }else{
			 record.setAttribute("salesPerson", "");
		 }
		 record.setAttribute("cellNumber", quotation.getCinfo().getCellNumber());
		 record.setAttribute("status",quotation.getStatus());
		 record.setAttribute("amount", quotation.getNetpayable());
		 record.setAttribute("branch", quotation.getBranch());
		
		 
		 record.setAttribute("quotationId", quotation.getCount());
		 if(quotation.getContractCount()!=0&&quotation.getContractCount()!=-1){
			 record.setAttribute("orderId", quotation.getContractCount());
		 }else{
			 record.setAttribute("orderId", "");
		 }
		 if(quotation.getLeadCount()!=0&&quotation.getLeadCount()!=-1){
			 record.setAttribute("leadId", quotation.getLeadCount());
		 }else{
			 record.setAttribute("leadId", "");
		 }
		return record;
	}
	
	private ListGridField[] getServiceLeadGridFields() {
	   ListGridField followup = new ListGridField("followup"," ");
	   followup.setWrap(true);
	   followup.setAlign(Alignment.CENTER);
	   followup.setType(ListGridFieldType.IMAGE);
	   followup.setDefaultValue("/images/menubar/044-file.png");
	   followup.setCanEdit(false);
	   followup.setWidth(40);
	   
	   ListGridField followupDate = new ListGridField("followupDate","Follow-Date");
	   followupDate.setWrap(true);
	   followupDate.setAlign(Alignment.CENTER);
	   
	   ListGridField creationDate = new ListGridField("creationDate","Creation Date");
	   creationDate.setWrap(true);
	   creationDate.setAlign(Alignment.CENTER);
	   creationDate.setWidth(82);
	   
	   ListGridField customer = new ListGridField("customer","Customer");
	   customer.setWrap(true);
	   customer.setAlign(Alignment.LEFT);
	   
	   ListGridField salesPerson = new ListGridField("salesPerson","Sales Person");
	   salesPerson.setWrap(true);
	   salesPerson.setAlign(Alignment.LEFT);
	   
	   ListGridField cellNumber = new ListGridField("cellNumber","Cell Number");
	   cellNumber.setWrap(true);
	   cellNumber.setAlign(Alignment.CENTER);
	   
	   ListGridField status = new ListGridField("status","Status");
	   status.setWrap(true);
	   status.setAlign(Alignment.LEFT);
	   
	   ListGridField amount = new ListGridField("amount","Amount");
	   amount.setWrap(true);
	   amount.setAlign(Alignment.RIGHT);
	   
	   ListGridField leadId = new ListGridField("leadId","Lead Id");
	   leadId.setWrap(true);
	   leadId.setAlign(Alignment.CENTER);
	   
	   //Updated By: Ashwini Patil Date: 25-12-2021 Description: to display leadId on grid with underline
	   leadId.setCellFormatter(new CellFormatter() {
		@Override
		public String format(Object value, ListGridRecord record, int rowNum,
				int colNum) {
			// TODO Auto-generated method stub
			String id = record.getAttributeAsString("leadId");
			return "<u>"+id+"</u>";
		}
	});
	   
	   
	   ListGridField orderId = new ListGridField("orderId","Order Id"); 
	   orderId.setWrap(true);
	   orderId.setAlign(Alignment.CENTER);
	 //Updated By: Ashwini Patil Date: 24-08-2022 Description: to display leadId on grid with underline
	   orderId.setCellFormatter(new CellFormatter() {
		@Override
		public String format(Object value, ListGridRecord record, int rowNum,
				int colNum) {
			// TODO Auto-generated method stub
			String id = record.getAttributeAsString("orderId");
			return "<u>"+id+"</u>";
		}
	});
	   
	   
	   ListGridField branch = new ListGridField("branch","Branch");
	   branch.setWrap(true);
	   branch.setAlign(Alignment.LEFT);
	   
//	   return new ListGridField[] {followup,followupDate,creationDate,customer,salesPerson,
//			   cellNumber,status,amount,leadId,orderId,branch
//	   };
	   return new ListGridField[] {followup,leadId,followupDate,creationDate,customer,salesPerson,
			   cellNumber,status,amount,orderId,branch
	   }; //changed id position

	}
		
	protected void setSalesLeadDataSourceField(DataSource dataSource) {
//	   DataSourceField email = new DataSourceField("email", FieldType.IMAGE);
//	   DataSourceField print = new DataSourceField("print", FieldType.IMAGE);
	   DataSourceField followup = new DataSourceField("followup", FieldType.IMAGE);
	   
	   DataSourceDateField followupDate = new DataSourceDateField("followupDate");
	   DataSourceDateField creationDate = new DataSourceDateField("creationDate");
	   
	   followupDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
	   creationDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
	   
	   DataSourceField title = new DataSourceField("title", FieldType.TEXT);
	   DataSourceField customer = new DataSourceField("customer", FieldType.TEXT);
	   DataSourceField salesPerson = new DataSourceField("salesPerson", FieldType.TEXT);
	   DataSourceField cellNumber = new DataSourceField("cellNumber", FieldType.TEXT);
	   DataSourceField status = new DataSourceField("status", FieldType.TEXT);
	   DataSourceField amount = new DataSourceField("amount", FieldType.TEXT);
	   
//	   DataSourceField quotationId = new DataSourceField("quotationId", FieldType.INTEGER);
	   
	   DataSourceField group = new DataSourceField("group", FieldType.TEXT);
	   DataSourceField category = new DataSourceField("category", FieldType.TEXT);
	   DataSourceField type = new DataSourceField("type", FieldType.TEXT);
	   
	   DataSourceField leadId = new DataSourceField("leadId", FieldType.INTEGER);
	   leadId.setPrimaryKey(true);
	   DataSourceField orderId = new DataSourceField("orderId", FieldType.INTEGER);
	   DataSourceField branch = new DataSourceField("branch", FieldType.TEXT);
	   
	   dataSource.setFields(followup,leadId,followupDate,title,salesPerson,customer,cellNumber,status,amount,group,category,type,creationDate,orderId,branch);	
	}
	
		
	public void setDataToSerLeadLG(List<Lead> list){
		
		//Ashwini Patil Date:6-09-2022
		Collections.sort(list, new Comparator<Lead>() {
	        public int compare(Lead o1, Lead o2) {
	            return o2.getFollowUpDate().compareTo(o1.getFollowUpDate());
	        }
	    });

		Console.log("list sorted by name before"+list.get(0).getFollowUpDate());
		
		if(list!=null&&list.size()>0){
			dsServiceLead.setTestData(getSerLeadGridData(list));
			dgServiceLead.setDataSource(dsServiceLead);
		}else{
			dgServiceLead.setData(new ListGridRecord[] {});
		}
		
		dgServiceLead.setFields(getServiceLeadGridFields());
		dgServiceLead.fetchData();
		
		int total=globalSerLeadList.size();
		double totalamt=0;
		for(Lead obj:globalSerLeadList){
			totalamt=totalamt+obj.getNetpayable();
		}
						
		lbltotalLeads.setText("Count: "+total);
		lbltotalLeadAmt.setText("Total Amount: "+totalamt);
		lbltotalLeads.setVisible(true);
		lbltotalLeadAmt.setVisible(true);
		
		
//		lblSerLead.setText("Total Lead : "+totalLead);
//		lblSerLead1.setText("Total Amount : "+Math.round(totalAmount));
		
		
	}
	
	private ListGridRecord[] getSerLeadGridData(List<Lead> leadList) {
		 ListGridRecord[] records = null; 
		 records = new ListGridRecord[leadList.size()];
		 
		 for(int i=0;i<leadList.size();i++){
			 
			 ListGridRecord record = new ListGridRecord();
			 Lead lead=leadList.get(i);
			 
			 record.setAttribute("followup", "/images/menubar/044-file.png");
			 
			 if(lead.getFollowUpDate()!=null){
				 record.setAttribute("followupDate", lead.getFollowUpDate());
			 }else{
				 record.setAttribute("followupDate", "");
			 }
			 if(lead.getCreationDate()!=null){
				 record.setAttribute("creationDate", lead.getCreationDate());
			 }else{
				 record.setAttribute("creationDate", "");
			 }
			 record.setAttribute("customer",lead.getPersonInfo().getFullName());
			 if(lead.getEmployee()!=null){
				 record.setAttribute("salesPerson", lead.getEmployee());
			 }else{
				 record.setAttribute("salesPerson", "");
			 }
			 record.setAttribute("cellNumber", lead.getPersonInfo().getCellNumber().toString());//Ashwini Patil added toString()
			 record.setAttribute("status",lead.getStatus());
			 record.setAttribute("amount", lead.getNetpayable());
			 record.setAttribute("branch", lead.getBranch());
			
			 
			 if(lead.getContractCount()!=null){
				 record.setAttribute("orderId", lead.getContractCount());
			 }else{
				 record.setAttribute("orderId", "");
			 }
			 record.setAttribute("leadId", lead.getCount());
			 records[i]=record;
			 
		 }
		 
		 return records;
	}
	
	public ListGridRecord addDateToLeadDG(Lead lead) {
		 ListGridRecord record = new ListGridRecord();
		 record.setAttribute("followup", "/images/menubar/044-file.png");
		 
		 if(lead.getFollowUpDate()!=null){
			 record.setAttribute("followupDate", lead.getFollowUpDate());
		 }else{
			 record.setAttribute("followupDate", "");
		 }
		 if(lead.getCreationDate()!=null){
			 record.setAttribute("creationDate", lead.getCreationDate());
		 }else{
			 record.setAttribute("creationDate", "");
		 }
		 record.setAttribute("customer",lead.getPersonInfo().getFullName());
		 if(lead.getEmployee()!=null){
			 record.setAttribute("salesPerson", lead.getEmployee());
		 }else{
			 record.setAttribute("salesPerson", "");
		 }
		 record.setAttribute("cellNumber", lead.getPersonInfo().getCellNumber().toString());
		 record.setAttribute("status",lead.getStatus());
		 record.setAttribute("amount", lead.getNetpayable());
		 record.setAttribute("branch", lead.getBranch());
		 if(lead.getContractCount()!=null){
			 record.setAttribute("orderId", lead.getContractCount());
		 }else{
			 record.setAttribute("orderId", "");
		 }
		 record.setAttribute("leadId", lead.getCount());
		 return record;
	}
	
	
	private void initializeBillDataGrid() {
		dgBill = new ListGrid();
		setGridProperty(dgBill);
		dgBill.setFields(getBillingGridFields());
		
		dgBill.setSelectionType(SelectionStyle.SIMPLE);
		dgBill.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		
		dsBill= new DataSource();
		dsBill.setClientOnly(true);
		setBillingDataSourceField(dsBill);
		dgBill.setDataSource(dsBill);
		dgBill.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
		vpBill.getElement().addClassName("varticalGridPanel1");
		dgBill.getElement().addClassName("technicianSchedule2");
		vpBill.add(dgBill);
	}
	
	private HorizontalPanel intializeBillTabFilter() {
		btnMergeBill=new Button("<image src='/images/menubar/merge.png' width='25px' height='18px'/>");
		btnDownloadBill=new Button("<image src='/images/menubar/146-download.png' width='25px' height='18px'/>");
		btnMergeBill.getElement().addClassName("buttonIcon");
		btnDownloadBill.getElement().addClassName("buttonIcon");

		
		HorizontalPanel hPanel=new HorizontalPanel();
		
		hPanel.add(btnMergeBill);
		hPanel.add(btnDownloadBill);
		
		lbltotalBills.setVisible(false);
		lbltotalBills.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalBills.getElement().setClassName("processbaritems");
		lbltotalBills.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalBills.getElement().getStyle().setBackgroundImage(null);
		lbltotalBills.getElement().getStyle().setBackgroundColor("White");
		hPanel.add(lbltotalBills);
				
		lbltotalBillGrAmt.setVisible(false);
		lbltotalBillGrAmt.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalBillGrAmt.getElement().setClassName("processbaritems");
		lbltotalBillGrAmt.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalBillGrAmt.getElement().getStyle().setBackgroundImage(null);
		lbltotalBillGrAmt.getElement().getStyle().setBackgroundColor("White");
		hPanel.add(lbltotalBillGrAmt);
		
		
		hPanel.setSpacing(4);
		return hPanel;
	}
	
	private ListGridField[] getBillingGridFields() {
//	   ListGridField print = new ListGridField("print"," ");
//	   print.setWrap(true);
//	   print.setAlign(Alignment.CENTER);
//	   print.setType(ListGridFieldType.IMAGE);
//	   print.setDefaultValue("/images/menubar/119-printer.png");
//	   print.setCanEdit(false);
//	   print.setWidth(40);
//	   
//	   ListGridField email = new ListGridField("email"," ");
//	   email.setWrap(true);
//	   email.setAlign(Alignment.CENTER);
//	   email.setType(ListGridFieldType.IMAGE);
//	   email.setDefaultValue("/images/menubar/159-email.png");
//	   email.setCanEdit(false);
//	   email.setWidth(40);
	   
	   ListGridField invoiceDate = new ListGridField("invoiceDate","Invoice Date");
	   invoiceDate.setWrap(true);
	   invoiceDate.setAlign(Alignment.CENTER);
	   
	   ListGridField customer = new ListGridField("customer","Customer");
	   customer.setWrap(true);
	   customer.setAlign(Alignment.CENTER);
	   
	   ListGridField cellNumber = new ListGridField("cellNumber","Cell Number");
	   cellNumber.setWrap(true);
	   cellNumber.setAlign(Alignment.CENTER);
	   
	   ListGridField status = new ListGridField("status","Status");
	   status.setWrap(true);
	   status.setAlign(Alignment.CENTER);
	   
	   ListGridField amount = new ListGridField("amount","Gross Amount");
	   amount.setWrap(true);
	   amount.setAlign(Alignment.CENTER);
	   
	   ListGridField billingId = new ListGridField("billingId","Billing Id");
	   billingId.setWrap(true);
	   billingId.setAlign(Alignment.CENTER);
	   
	   //Updated By: Ashwini Patil Date: 25-12-2021 Description: to display billingId on grid with underline
	   billingId.setCellFormatter(new CellFormatter() {
		@Override
		public String format(Object value, ListGridRecord record, int rowNum,
				int colNum) {
			// TODO Auto-generated method stub
			String id = record.getAttributeAsString("billingId");
			return "<u>"+id+"</u>";
		}
	});
	   ListGridField branch = new ListGridField("branch","Branch");
	   branch.setWrap(true);
	   branch.setAlign(Alignment.CENTER);
	   
	   ListGridField paymentDate = new ListGridField("paymentDate","Payment Date");
	   paymentDate.setWrap(true);
	   paymentDate.setAlign(Alignment.CENTER);
	   
	   ListGridField approver = new ListGridField("approver","Approver");
	   approver.setWrap(true);
	   approver.setAlign(Alignment.CENTER);
	   
	   ListGridField invoiceId = new ListGridField("invoiceId","Invoice Id");
	   invoiceId.setWrap(true);
	   invoiceId.setAlign(Alignment.CENTER);
	 //Updated By: Ashwini Patil Date: 25-12-2021 Description: to display billingId on grid with underline
	   invoiceId.setCellFormatter(new CellFormatter() {
		@Override
		public String format(Object value, ListGridRecord record, int rowNum,
				int colNum) {
			// TODO Auto-generated method stub
			String id = record.getAttributeAsString("invoiceId");
			return "<u>"+id+"</u>";
		}
	});
	   
	   ListGridField orderId = new ListGridField("orderId","Order Id"); 
	   orderId.setWrap(true);
	   orderId.setAlign(Alignment.CENTER);
	   
	 //Updated By: Ashwini Patil Date: 25-12-2021 Description: to display billingId on grid with underline
	   orderId.setCellFormatter(new CellFormatter() {
		@Override
		public String format(Object value, ListGridRecord record, int rowNum,
				int colNum) {
			// TODO Auto-generated method stub
			String id = record.getAttributeAsString("orderId");
			return "<u>"+id+"</u>";
		}
	});
	   
	   
	   return new ListGridField[] { 
			   billingId,invoiceDate,customer,cellNumber,status,amount,branch,paymentDate,approver,
			  invoiceId,orderId //Removed comma
	   };

	}
		
	protected void setBillingDataSourceField(DataSource dataSource) {
//	   DataSourceField email = new DataSourceField("email", FieldType.IMAGE);
//	   DataSourceField print = new DataSourceField("print", FieldType.IMAGE);
	   DataSourceDateField invoiceDate = new DataSourceDateField("invoiceDate");
	   invoiceDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
	   DataSourceField customer = new DataSourceField("customer", FieldType.TEXT);
	   DataSourceField cellNumber = new DataSourceField("cellNumber", FieldType.TEXT);
	   DataSourceField status = new DataSourceField("status", FieldType.TEXT);
	   DataSourceField amount = new DataSourceField("amount", FieldType.TEXT);
	   DataSourceField billingId = new DataSourceField("billingId", FieldType.TEXT);
	   billingId.setPrimaryKey(true);
	   DataSourceField branch = new DataSourceField("branch", FieldType.TEXT);
	   DataSourceDateField paymentDate = new DataSourceDateField("paymentDate");
	   paymentDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
	   DataSourceField approver = new DataSourceField("approver", FieldType.TEXT);
	   DataSourceField invoiceId = new DataSourceField("invoiceId", FieldType.INTEGER);
	   DataSourceField orderId = new DataSourceField("orderId", FieldType.INTEGER);   
	   dataSource.setFields(billingId,invoiceDate,customer,cellNumber,status,amount,branch,paymentDate,approver,invoiceId,orderId);
	   
	}
		
	public void setDataToBillListGrid(List<BillingDocument> list){
		//Ashwini Patil Date:6-09-2022
		Collections.sort(list, new Comparator<BillingDocument>() {
			        public int compare(BillingDocument o1, BillingDocument o2) {
			            return o2.getInvoiceDate().compareTo(o1.getInvoiceDate());
			        }
			    });		
		
		dsBill.setTestData(getBillingGridData(list));
		dgBill.setDataSource(dsBill);
		dgBill.setFields(getBillingGridFields());
		dgBill.fetchData();
		
		int total=globalBillingDocList.size();
		double totalamt=0;
		for(BillingDocument obj:globalBillingDocList){
			totalamt=totalamt+obj.getGrandTotalAmount();
		}
						
		lbltotalBills.setText("Count: "+total);
		lbltotalBillGrAmt.setText("Total Amount: "+df.format(totalamt));
		lbltotalBills.setVisible(true);
		lbltotalBillGrAmt.setVisible(true);
		
	}
		
	private ListGridRecord[] getBillingGridData(List<BillingDocument> billList) {
		ListGridRecord[] records = null; 
		records = new ListGridRecord[billList.size()];
		 
		 for(int i=0;i<billList.size();i++){
			 ListGridRecord record = new ListGridRecord();
			 BillingDocument bill=billList.get(i);
			 
//			 record.setAttribute("print", "/images/menubar/119-printer.png");
//			 record.setAttribute("email", "/images/menubar/159-email.png");
			 if(bill.getInvoiceDate()!=null){
				 record.setAttribute("invoiceDate", bill.getInvoiceDate());
			 }else{
				 record.setAttribute("invoiceDate", "");
			 }
			 record.setAttribute("customer",bill.getPersonInfo().getFullName());
			 record.setAttribute("cellNumber", bill.getPersonInfo().getCellNumber().toString());
			 record.setAttribute("status",bill.getStatus());
			 record.setAttribute("amount",df.format(bill.getGrandTotalAmount()));//12-10-2022
			 record.setAttribute("billingId", bill.getCount());
			 record.setAttribute("branch", bill.getBranch());
			 record.setAttribute("approver", bill.getApproverName());
			 if(bill.getPaymentDate()!=null){
				 record.setAttribute("paymentDate", bill.getPaymentDate());
			 }else{
				 record.setAttribute("paymentDate", "");
			 }
			 
			 if(bill.getApproverName()!=null){
				 record.setAttribute("approver", bill.getApproverName());
			 }else{
				 record.setAttribute("approver", "");
			 }
			 
			 if(bill.getInvoiceCount()!=0&&bill.getInvoiceCount()!=-1){
				 record.setAttribute("invoiceId", bill.getInvoiceCount());
			 }else{
				 record.setAttribute("invoiceId", "");
			 }
			 record.setAttribute("orderId", bill.getContractCount());
			
			 records[i]=record;
		 }
		 return records;
	}
		
	public Record addDateToBillingDG(BillingDocument bill, ListGridRecord record) {
		
//		ListGridRecord record = new ListGridRecord();
		if(record==null){
			record = new ListGridRecord();
		}
		
		 
		 
//		 record.setAttribute("print", "/images/menubar/119-printer.png");
//		 record.setAttribute("email", "/images/menubar/159-email.png");
		 if(bill.getInvoiceDate()!=null){
			 record.setAttribute("invoiceDate", bill.getInvoiceDate());
		 }else{
			 record.setAttribute("invoiceDate", "");
		 }
		 record.setAttribute("customer",bill.getPersonInfo().getFullName());
		 record.setAttribute("cellNumber", bill.getPersonInfo().getCellNumber().toString());
		 record.setAttribute("status",bill.getStatus());		 
		 record.setAttribute("amount",df.format(bill.getGrandTotalAmount()));
		 record.setAttribute("billingId", bill.getCount());
		 record.setAttribute("branch", bill.getBranch());
		 record.setAttribute("approver", bill.getApproverName());
		 if(bill.getPaymentDate()!=null){
			 record.setAttribute("paymentDate", bill.getPaymentDate());
		 }else{
			 record.setAttribute("paymentDate", "");
		 }
		 
		 if(bill.getApproverName()!=null){
			 record.setAttribute("approver", bill.getApproverName());
		 }else{
			 record.setAttribute("approver", "");
		 }
		 
		 if(bill.getInvoiceCount()!=0&&bill.getInvoiceCount()!=-1){
			 record.setAttribute("invoiceId", bill.getInvoiceCount());
		 }else{
			 record.setAttribute("invoiceId", "");
		 }
		 record.setAttribute("orderId", bill.getContractCount());
		return record;
	}
	
	
	private void initializeInvoiceDataGrid() {
		dgInvoice = new ListGrid();
		setGridProperty(dgInvoice);
		dgInvoice.setFields(getInvoiceGridFields());
		
		dsInvoice= new DataSource();
		dsInvoice.setClientOnly(true);
		setInvoiceDataSourceField(dsInvoice);
		dgInvoice.setDataSource(dsInvoice);
		dgInvoice.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
		vpInvoice.getElement().addClassName("varticalGridPanel1");
		dgInvoice.getElement().addClassName("technicianSchedule2");
		vpInvoice.add(dgInvoice);
	}
	
	private HorizontalPanel intializeInvoiceTabFilter() {
		btnDownloadInvoice=new Button("<image src='/images/menubar/146-download.png' width='25px' height='18px'/>");
		btnDownloadInvoice.getElement().addClassName("buttonIcon");
		
//		if(lblSerLead==null){
//			lblSerLead=new InlineLabel("Total Lead : ");
//			lblSerLead1=new InlineLabel("Total Amount : ");
//			lblSerLead.setVisible(false);
//			lblSerLead1.setVisible(false);
//		}
//		lblSerLead.getElement().getStyle().setMarginLeft(1080, Unit.PX);
//		lblSerLead.setWidth("100px");
//		lblSerLead1.setWidth("160px");
//		lblSerLead.getElement().setClassName("processbaritems");
//		lblSerLead1.getElement().setClassName("processbaritems");
		
		HorizontalPanel hPanel=new HorizontalPanel();
		
		hPanel.add(btnDownloadInvoice);
//		hPanel.add(btnDownloadBill);
		
//		hPanel.add(lblSerLead);
//		hPanel.add(lblSerLead1);
		
		lbltotalInvoices.setVisible(false);
		lbltotalInvoices.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalInvoices.getElement().setClassName("processbaritems");
		lbltotalInvoices.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalInvoices.getElement().getStyle().setBackgroundImage(null);
		lbltotalInvoices.getElement().getStyle().setBackgroundColor("White");
		
		hPanel.add(lbltotalInvoices);
				
		lbltotalInvGrAmt.setVisible(false);
		lbltotalInvGrAmt.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalInvGrAmt.getElement().setClassName("processbaritems");
		lbltotalInvGrAmt.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalInvGrAmt.getElement().getStyle().setBackgroundImage(null);
		lbltotalInvGrAmt.getElement().getStyle().setBackgroundColor("White");
		
		hPanel.add(lbltotalInvGrAmt);
		
		hPanel.setSpacing(4);
		return hPanel;
	}
	
	private ListGridField[] getInvoiceGridFields() {
	   ListGridField print = new ListGridField("print"," ");
	   print.setWrap(true);
	   print.setAlign(Alignment.CENTER);
	   print.setType(ListGridFieldType.IMAGE);
	   print.setDefaultValue("/images/menubar/119-printer.png");
	   print.setCanEdit(false);
	   print.setWidth(40);
	   
	   ListGridField email = new ListGridField("email"," ");
	   email.setWrap(true);
	   email.setAlign(Alignment.CENTER);
	   email.setType(ListGridFieldType.IMAGE);
	   email.setDefaultValue("/images/menubar/159-email.png");
	   email.setCanEdit(false);
	   email.setWidth(40);
	   
	   ListGridField invoiceDate = new ListGridField("invoiceDate","Invoice Date");
	   invoiceDate.setWrap(true);
	   invoiceDate.setAlign(Alignment.CENTER);
	   
	   ListGridField customer = new ListGridField("customer","Customer");
	   customer.setWrap(true);
	   customer.setAlign(Alignment.CENTER);
	   
	   ListGridField cellNumber = new ListGridField("cellNumber","Cell Number");
	   cellNumber.setWrap(true);
	   cellNumber.setAlign(Alignment.CENTER);
	   
	   ListGridField status = new ListGridField("status","Status");
	   status.setWrap(true);
	   status.setAlign(Alignment.CENTER);
	   
	   ListGridField amount = new ListGridField("amount","Gross Amount");
	   amount.setWrap(true);
	   amount.setAlign(Alignment.CENTER);
	   
	   ListGridField branch = new ListGridField("branch","Branch");
	   branch.setWrap(true);
	   branch.setAlign(Alignment.CENTER);
	   
	   ListGridField refNum = new ListGridField("refNum","Ref No");
	   refNum.setWrap(true);
	   refNum.setAlign(Alignment.CENTER);
	   
	   ListGridField invRefNum = new ListGridField("invRefNum","Invoice Ref No");
	   invRefNum.setWrap(true);
	   invRefNum.setAlign(Alignment.CENTER);
	   
	   ListGridField paymentDate = new ListGridField("paymentDate","Payment Date");
	   paymentDate.setWrap(true);
	   paymentDate.setAlign(Alignment.CENTER);
	   
	   ListGridField approver = new ListGridField("approver","Approver");
	   approver.setWrap(true);
	   approver.setAlign(Alignment.CENTER);
	   
	   ListGridField invoiceId = new ListGridField("invoiceId","Invoice Id");
	   invoiceId.setWrap(true);
	   invoiceId.setAlign(Alignment.CENTER);
	   //Updated By: Ashwini Patil Date: 25-12-2021 Description: to display invoiceId on grid with underline
	   invoiceId.setCellFormatter(new CellFormatter() {
		@Override
		public String format(Object value, ListGridRecord record, int rowNum,
				int colNum) {
			// TODO Auto-generated method stub
			String id = record.getAttributeAsString("invoiceId");
			return "<u>"+id+"</u>";
		}
	});
	   
	   ListGridField orderId = new ListGridField("orderId","Order Id"); 
	   orderId.setWrap(true);
	   orderId.setAlign(Alignment.CENTER);
	   orderId.setCellFormatter(new CellFormatter() {
			@Override
			public String format(Object value, ListGridRecord record, int rowNum,
					int colNum) {
				// TODO Auto-generated method stub
				String id = record.getAttributeAsString("orderId");
				return "<u>"+id+"</u>";
			}
		});
	   
	   ListGridField invoiceType = new ListGridField("invoiceType","Invoice Type"); 
	   invoiceType.setWrap(true);
	   invoiceType.setAlign(Alignment.CENTER);//Ashwini Patil added this Date:18-08-2022
	   
	   
	   return new ListGridField[] { 
			  print,email,invoiceId, invoiceDate,customer,cellNumber,status,amount,branch,refNum,invRefNum,paymentDate,approver,
			  orderId,invoiceType //removed comma
	   };

	}
		
	protected void setInvoiceDataSourceField(DataSource dataSource) {
	   DataSourceField email = new DataSourceField("email", FieldType.IMAGE);
	   DataSourceField print = new DataSourceField("print", FieldType.IMAGE);
	   DataSourceDateField invoiceDate = new DataSourceDateField("invoiceDate");
	   invoiceDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
	   DataSourceField customer = new DataSourceField("customer", FieldType.TEXT);
	   DataSourceField cellNumber = new DataSourceField("cellNumber", FieldType.TEXT);
	   DataSourceField status = new DataSourceField("status", FieldType.TEXT);
	   DataSourceField amount = new DataSourceField("amount", FieldType.TEXT);
	   DataSourceField billingId = new DataSourceField("billingId", FieldType.TEXT);
	   DataSourceField branch = new DataSourceField("branch", FieldType.TEXT);
	   DataSourceField refNum = new DataSourceField("refNum", FieldType.TEXT);
	   DataSourceField invRefNum = new DataSourceField("InvRefNum", FieldType.TEXT);
	   DataSourceDateField paymentDate = new DataSourceDateField("paymentDate");
	   paymentDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
	   DataSourceField approver = new DataSourceField("approver", FieldType.TEXT);
	   DataSourceField invoiceId = new DataSourceField("invoiceId", FieldType.INTEGER);
	   invoiceId.setPrimaryKey(true);
	   DataSourceField orderId = new DataSourceField("orderId", FieldType.INTEGER);
	   DataSourceField invoiceType = new DataSourceField("invoiceType", FieldType.TEXT);//Ashwini Patil added this Date:18-08-2022
	   
	   dataSource.setFields(print,email,invoiceId,invoiceDate,customer,cellNumber,status,amount,billingId,branch,refNum,invRefNum,paymentDate,approver,orderId,invoiceType);
	}
		
	public void setDataToInvoiceListGrid(List<Invoice> list){
		//Ashwini Patil Date:6-09-2022
		Collections.sort(list, new Comparator<Invoice>() {
					        public int compare(Invoice o1, Invoice o2) {
					            return o2.getInvoiceDate().compareTo(o1.getInvoiceDate());
					        }
					    });
		dsInvoice.setTestData(getInvoiceGridData(list));
		dgInvoice.setDataSource(dsInvoice);
		dgInvoice.setFields(getInvoiceGridFields());
		dgInvoice.fetchData();
		
		int total=globalInvoiceList.size();
		double totalamt=0;
		for(Invoice obj:globalInvoiceList){
			totalamt=totalamt+obj.getNetPayable();
		}
						
		lbltotalInvoices.setText("Count: "+total);
		lbltotalInvGrAmt.setText("Total Amount: "+totalamt);
		lbltotalInvoices.setVisible(true);
		lbltotalInvGrAmt.setVisible(true);
	}
		
	private ListGridRecord[] getInvoiceGridData(List<Invoice> billList) {
		ListGridRecord[] records = null; 
		records = new ListGridRecord[billList.size()];
		 
		 for(int i=0;i<billList.size();i++){
			 ListGridRecord record = new ListGridRecord();
			 Invoice bill=billList.get(i);
			 
			 record.setAttribute("print", "/images/menubar/119-printer.png");
			 record.setAttribute("email", "/images/menubar/159-email.png");
			 if(bill.getInvoiceDate()!=null){
				 record.setAttribute("invoiceDate", bill.getInvoiceDate());
			 }else{
				 record.setAttribute("invoiceDate", "");
			 }
			 record.setAttribute("customer",bill.getPersonInfo().getFullName());
			 record.setAttribute("cellNumber", bill.getPersonInfo().getCellNumber().toString());
			 record.setAttribute("status",bill.getStatus());
			 record.setAttribute("amount", bill.getNetPayable());
			 record.setAttribute("billingId", bill.getCount());
			 record.setAttribute("branch", bill.getBranch());
			 record.setAttribute("approver", bill.getApproverName());
			 if(bill.getRefNumber()!=null){
				 record.setAttribute("refNum", bill.getRefNumber());
			 }else{
				 record.setAttribute("refNum", "");
			 }
			 if(bill.getInvRefNumber()!=null){
				 record.setAttribute("invRefNum", bill.getInvRefNumber());
			 }else{
				 record.setAttribute("invRefNum", "");
			 }
			 
			 if(bill.getPaymentDate()!=null){
				 record.setAttribute("paymentDate", bill.getPaymentDate());
			 }else{
				 record.setAttribute("paymentDate", "");
			 }
			 
			 if(bill.getApproverName()!=null){
				 record.setAttribute("approver", bill.getApproverName());
			 }else{
				 record.setAttribute("approver", "");
			 }
			 
			 record.setAttribute("invoiceId", bill.getCount()+"");
			 record.setAttribute("orderId", bill.getContractCount());
			 record.setAttribute("invoiceType", bill.getInvoiceType());//18-08-2022
			 records[i]=record;
		 }
		 return records;
	}
		
	public Record addDateToInvoiceDG(Invoice bill) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute("print", "/images/menubar/119-printer.png");
		 record.setAttribute("email", "/images/menubar/159-email.png");
		 if(bill.getInvoiceDate()!=null){
			 record.setAttribute("invoiceDate", bill.getInvoiceDate());
		 }else{
			 record.setAttribute("invoiceDate", "");
		 }
		 record.setAttribute("customer",bill.getPersonInfo().getFullName());
		 record.setAttribute("cellNumber", bill.getPersonInfo().getCellNumber().toString());
		 record.setAttribute("status",bill.getStatus());
		 record.setAttribute("amount", bill.getNetPayable());
		 record.setAttribute("billingId", bill.getCount());
		 record.setAttribute("branch", bill.getBranch());
		 record.setAttribute("approver", bill.getApproverName());
		 if(bill.getRefNumber()!=null){
			 record.setAttribute("refNum", bill.getRefNumber());
		 }else{
			 record.setAttribute("refNum", "");
		 }
		 if(bill.getInvRefNumber()!=null){
			 record.setAttribute("invRefNum", bill.getInvRefNumber());
		 }else{
			 record.setAttribute("invRefNum", "");
		 }
		 
		 if(bill.getPaymentDate()!=null){
			 record.setAttribute("paymentDate", bill.getPaymentDate());
		 }else{
			 record.setAttribute("paymentDate", "");
		 }
		 
		 if(bill.getApproverName()!=null){
			 record.setAttribute("approver", bill.getApproverName());
		 }else{
			 record.setAttribute("approver", "");
		 }
		 
		 record.setAttribute("invoiceId", bill.getCount()+"");
		 record.setAttribute("orderId", bill.getContractCount());
		return record;
	}
	
	private void initializePaymentDataGrid() {
		dgPayment = new ListGrid();
		setGridProperty(dgPayment);
		dgPayment.setFields(getPaymentGridFields());
		
		dsPayment= new DataSource();
		dsPayment.setClientOnly(true);
		setPaymentDataSourceField(dsPayment);
		dgPayment.setDataSource(dsPayment);
		dgPayment.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
		vpPaymentt.getElement().addClassName("varticalGridPanel1");
		dgPayment.getElement().addClassName("technicianSchedule2");
		vpPaymentt.add(dgPayment);
	}
	
	private HorizontalPanel intializePaymentTabFilter() {
		btnDownloadPayment=new Button("<image src='/images/menubar/146-download.png' width='25px' height='18px'/>");
		btnDownloadPayment.getElement().addClassName("buttonIcon");
		
//		if(lblSerLead==null){
//			lblSerLead=new InlineLabel("Total Lead : ");
//			lblSerLead1=new InlineLabel("Total Amount : ");
//			lblSerLead.setVisible(false);
//			lblSerLead1.setVisible(false);
//		}
//		lblSerLead.getElement().getStyle().setMarginLeft(1080, Unit.PX);
//		lblSerLead.setWidth("100px");
//		lblSerLead1.setWidth("160px");
//		lblSerLead.getElement().setClassName("processbaritems");
//		lblSerLead1.getElement().setClassName("processbaritems");
		
		HorizontalPanel hPanel=new HorizontalPanel();
		
		hPanel.add(btnDownloadPayment);
//		hPanel.add(btnDownloadBill);
		
//		hPanel.add(lblSerLead);
//		hPanel.add(lblSerLead1);
		
		lbltotalPayments.setVisible(false);
		lbltotalPayments.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalPayments.getElement().setClassName("processbaritems");
		lbltotalPayments.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalPayments.getElement().getStyle().setBackgroundImage(null);
		lbltotalPayments.getElement().getStyle().setBackgroundColor("White");
		
		hPanel.add(lbltotalPayments);
		
		lbltotalReceivable.setVisible(false);
		lbltotalReceivable.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalReceivable.getElement().setClassName("processbaritems");
		lbltotalReceivable.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalReceivable.getElement().getStyle().setBackgroundImage(null);
		lbltotalReceivable.getElement().getStyle().setBackgroundColor("White");
		
		hPanel.add(lbltotalReceivable);
		
		lbltotalReceived.setVisible(false);
		lbltotalReceived.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalReceived.getElement().setClassName("processbaritems");
		lbltotalReceived.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalReceived.getElement().getStyle().setBackgroundImage(null);
		lbltotalReceived.getElement().getStyle().setBackgroundColor("White");
		
		hPanel.add(lbltotalReceived);
		
		
		hPanel.setSpacing(4);
		return hPanel;
	}
	
	private ListGridField[] getPaymentGridFields() {
	   ListGridField print = new ListGridField("print"," ");
	   print.setWrap(true);
	   print.setAlign(Alignment.CENTER);
	   print.setType(ListGridFieldType.IMAGE);
	   print.setDefaultValue("/images/menubar/119-printer.png");
	   print.setCanEdit(false);
	   print.setWidth(40);
	   
	   ListGridField email = new ListGridField("email"," ");
	   email.setWrap(true);
	   email.setAlign(Alignment.CENTER);
	   email.setType(ListGridFieldType.IMAGE);
	   email.setDefaultValue("/images/menubar/159-email.png");
	   email.setCanEdit(false);
	   email.setWidth(40);
	   
	   ListGridField followup = new ListGridField("followup"," ");
	   followup.setWrap(true);
	   followup.setAlign(Alignment.CENTER);
	   followup.setType(ListGridFieldType.IMAGE);
	   followup.setDefaultValue("/images/menubar/044-file.png");
	   followup.setCanEdit(false);
	   followup.setWidth(40);
	   
	   ListGridField followupDate = new ListGridField("followupDate","Follow-Date");
	   followupDate.setWrap(true);
	   followupDate.setAlign(Alignment.CENTER);
	   
	   ListGridField paymentDate = new ListGridField("paymentDate","Payment Date");
	   paymentDate.setWrap(true);
	   paymentDate.setAlign(Alignment.CENTER);
	   
	   ListGridField customer = new ListGridField("customer","Customer");
	   customer.setWrap(true);
	   customer.setAlign(Alignment.CENTER);
	   
	   ListGridField cellNumber = new ListGridField("cellNumber","Cell Number");
	   cellNumber.setWrap(true);
	   cellNumber.setAlign(Alignment.CENTER);
	   
	   ListGridField status = new ListGridField("status","Status");
	   status.setWrap(true);
	   status.setAlign(Alignment.CENTER);
	   
	   ListGridField receivableAmount = new ListGridField("receivableAmount","Receivable Amount");
	   receivableAmount.setWrap(true);
	   receivableAmount.setAlign(Alignment.CENTER);
	   
	   ListGridField receivedAmount = new ListGridField("receivedAmount","Received Amount");
	   receivedAmount.setWrap(true);
	   receivedAmount.setAlign(Alignment.CENTER);
	   
	   ListGridField paymentId = new ListGridField("paymentId","Payment Id"); 
	   paymentId.setWrap(true);
	   paymentId.setAlign(Alignment.CENTER);
	   //Updated By: Ashwini Patil Date: 25-12-2021 Description: to display paymentId on grid with underline
	   paymentId.setCellFormatter(new CellFormatter() {
		@Override
		public String format(Object value, ListGridRecord record, int rowNum,
				int colNum) {
			// TODO Auto-generated method stub
			String id = record.getAttributeAsString("paymentId");
			return "<u>"+id+"</u>";
		}
	});
	   
	   ListGridField invoiceId = new ListGridField("invoiceId","Invoice Id");
	   invoiceId.setWrap(true);
	   invoiceId.setAlign(Alignment.CENTER);
	 //Updated By: Ashwini Patil Date: 25-12-2021 Description: to display paymentId on grid with underline
	   invoiceId.setCellFormatter(new CellFormatter() {
		@Override
		public String format(Object value, ListGridRecord record, int rowNum,
				int colNum) {
			// TODO Auto-generated method stub
			String id = record.getAttributeAsString("invoiceId");
			return "<u>"+id+"</u>";
		}
	});
	  
	   
	   ListGridField branch = new ListGridField("branch","Branch");
	   branch.setWrap(true);
	   branch.setAlign(Alignment.CENTER);
	   
	   return new ListGridField[] { 
			  print,email,followup,paymentId,followupDate,paymentDate,customer,cellNumber,status,receivableAmount,receivedAmount,
			  invoiceId,branch
	   };

	}
		
	protected void setPaymentDataSourceField(DataSource dataSource) {
	   DataSourceField email = new DataSourceField("email", FieldType.IMAGE);
	   DataSourceField print = new DataSourceField("print", FieldType.IMAGE);
	   DataSourceField followup = new DataSourceField("followup", FieldType.IMAGE);
	   DataSourceDateField followupDate = new DataSourceDateField("followupDate");
	   followupDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
	   DataSourceDateField paymentDate = new DataSourceDateField("paymentDate");
	   paymentDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
	   DataSourceField customer = new DataSourceField("customer", FieldType.TEXT);
	   DataSourceField cellNumber = new DataSourceField("cellNumber", FieldType.TEXT);
	   DataSourceField status = new DataSourceField("status", FieldType.TEXT);
	   DataSourceField receivableAmount = new DataSourceField("receivableAmount", FieldType.TEXT);
	   DataSourceField receivedAmount = new DataSourceField("receivedAmount", FieldType.TEXT);
	  
	   DataSourceField paymentId = new DataSourceField("paymentId", FieldType.TEXT);
	   paymentId.setPrimaryKey(true);
	  
	   DataSourceField invoiceId = new DataSourceField("invoiceId", FieldType.INTEGER);
	   
	   DataSourceField branch = new DataSourceField("branch", FieldType.TEXT);
	   
	   dataSource.setFields(print,email,followup,paymentId,followupDate,paymentDate,customer,cellNumber,status,receivableAmount,receivedAmount,invoiceId,branch);
	}
		
	public void setDataToPaymentListGrid(List<CustomerPayment> list){
		Console.log("in setDataToPaymentListGrid");
		//Ashwini Patil Date:6-09-2022 
		Collections.sort(list, new Comparator<CustomerPayment>() {
				public int compare(CustomerPayment o1, CustomerPayment o2) {
						 return o2.getPaymentDate().compareTo(o1.getPaymentDate());
					}
			});
		Console.log("List sorted");
		
		dsPayment.setTestData(getPaymentGridData(list));
		Console.log("setDataToPaymentListGrid step1");
		dgPayment.setDataSource(dsPayment);
		Console.log("setDataToPaymentListGrid step2");
		dgPayment.setFields(getPaymentGridFields());
		Console.log("setDataToPaymentListGrid step3");
		dgPayment.fetchData();
		Console.log("setDataToPaymentListGrid step4");
		
		
		int totalPayments=globalPaymentList.size();
		double totalReceivable=0;
		double totalReceived=0;
		for(CustomerPayment obj:globalPaymentList){
			totalReceivable=totalReceivable+obj.getPaymentAmt();
			totalReceived=totalReceived+obj.getPaymentReceived();
		}
		
		lbltotalPayments.setText("Count: "+totalPayments);
		lbltotalReceivable.setText("Total Receivable: "+totalReceivable);
		lbltotalReceived.setText("Total Received: "+totalReceived);
		lbltotalPayments.setVisible(true);
		lbltotalReceivable.setVisible(true);
		lbltotalReceived.setVisible(true);
		
	}

		
	private ListGridRecord[] getPaymentGridData(List<CustomerPayment> billList) {
		ListGridRecord[] records = null; 
		records = new ListGridRecord[billList.size()];
		 
		 for(int i=0;i<billList.size();i++){
			 ListGridRecord record = new ListGridRecord();
			 CustomerPayment bill=billList.get(i);
			 
			 record.setAttribute("print", "/images/menubar/119-printer.png");
			 record.setAttribute("email", "/images/menubar/159-email.png");
			 record.setAttribute("followup", "/images/menubar/044-file.png");
			 
			 if(bill.getFollowUpDate()!=null){
				 record.setAttribute("followupDate", bill.getFollowUpDate());
			 }else{
				 record.setAttribute("followupDate", "");
			 }
			 if(bill.getPaymentDate()!=null){
				 record.setAttribute("paymentDate", bill.getPaymentDate());
			 }else{
				 record.setAttribute("paymentDate", "");
			 }
			 record.setAttribute("customer",bill.getPersonInfo().getFullName());
			 record.setAttribute("cellNumber", bill.getPersonInfo().getCellNumber().toString());
			 record.setAttribute("status",bill.getStatus());
			 record.setAttribute("receivableAmount", bill.getPaymentAmt());
			 record.setAttribute("receivedAmount", bill.getPaymentReceived());
			 record.setAttribute("paymentId", bill.getCount());
			 
			 if(bill.getInvoiceCount()!=0&&bill.getInvoiceCount()!=-1){
				 record.setAttribute("invoiceId", bill.getInvoiceCount());
			 }else{
				 record.setAttribute("invoiceId", "");
			 }
			 record.setAttribute("branch", bill.getBranch());
			
			 records[i]=record;
		 }
		 Console.log("before returning records");
		 return records;
	}
		
	public Record addDateToPaymentDG(CustomerPayment bill) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute("print", "/images/menubar/119-printer.png");
		 record.setAttribute("email", "/images/menubar/159-email.png");
		 record.setAttribute("followup", "/images/menubar/044-file.png");
		 
		 if(bill.getFollowUpDate()!=null){
			 record.setAttribute("followupDate", bill.getFollowUpDate());
		 }else{
			 record.setAttribute("followupDate", "");
		 }
		 if(bill.getPaymentDate()!=null){
			 record.setAttribute("paymentDate", bill.getPaymentDate());
		 }else{
			 record.setAttribute("paymentDate", "");
		 }
		 record.setAttribute("customer",bill.getPersonInfo().getFullName());
		 record.setAttribute("cellNumber", bill.getPersonInfo().getCellNumber().toString());
		 record.setAttribute("status",bill.getStatus());
		 record.setAttribute("receivableAmount", bill.getPaymentAmt());
		 record.setAttribute("receivedAmount", bill.getPaymentReceived());
		 record.setAttribute("paymentId", bill.getCount());
		 
		 if(bill.getInvoiceCount()!=0&&bill.getInvoiceCount()!=-1){
			 record.setAttribute("invoiceId", bill.getInvoiceCount());
		 }else{
			 record.setAttribute("invoiceId", "");
		 }
		 record.setAttribute("branch", bill.getBranch());
		return record;
	}
	
	
	
	private void initializeSalesLeadDataGrid() {
		dgSalesLead = new ListGrid();
		setGridProperty(dgSalesLead);
		dgSalesLead.setFields(getSalesLeadGridFields());
		
		dsSalesLead= new DataSource();
		dsSalesLead.setClientOnly(true);
		setSalesLeadDataSourceField(dsSalesLead);
		dgSalesLead.setDataSource(dsSalesLead);
		dgSalesLead.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
		vpSalesLead.getElement().addClassName("varticalGridPanel1");
		dgSalesLead.getElement().addClassName("technicianSchedule2");//7-07-2022 technicianSchedule1
//		dgSalesLead.getElement().addClassName(className);
		
		vpSalesLead.add(dgSalesLead);
	}
	
	private HorizontalPanel intializeSalesLeadTabFilter() {
		btnNewSalesLead=new Button("<image src='/images/menubar/043-plus.png' width='25px' height='18px'/>");
		btnDownloadSalesLead=new Button("<image src='/images/menubar/146-download.png' width='25px' height='18px'/>");
		btnNewSalesLead.getElement().addClassName("buttonIcon");
		btnDownloadSalesLead.getElement().addClassName("buttonIcon");
		
		HorizontalPanel hPanel=new HorizontalPanel();
		
		hPanel.add(btnNewSalesLead);
		hPanel.add(btnDownloadSalesLead);
		
//		hPanel.add(lblSerLead);
//		hPanel.add(lblSerLead1);
		
		lbltotalSalesLeads.setVisible(false);
		lbltotalSalesLeads.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalSalesLeads.getElement().setClassName("processbaritems");
		lbltotalSalesLeads.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalSalesLeads.getElement().getStyle().setBackgroundImage(null);
		lbltotalSalesLeads.getElement().getStyle().setBackgroundColor("White");
		
		hPanel.add(lbltotalSalesLeads);
				
		lbltotalSalesLeadAmt.setVisible(false);
		lbltotalSalesLeadAmt.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalSalesLeadAmt.getElement().setClassName("processbaritems");
		lbltotalSalesLeadAmt.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalSalesLeadAmt.getElement().getStyle().setBackgroundImage(null);
		lbltotalSalesLeadAmt.getElement().getStyle().setBackgroundColor("White");
		
		hPanel.add(lbltotalSalesLeadAmt);
		
		hPanel.setSpacing(4);
		return hPanel;
	}

	private void initializeSalesQuotationDataGrid() {
		dgSalesQuotation = new ListGrid();
		setGridProperty(dgSalesQuotation);
		dgSalesQuotation.setFields(getSalesQuotationGridFields());
		dsSalesQuotation= new DataSource();
		dsSalesQuotation.setClientOnly(true);
		setServiceQuotationDataSourceField(dsSalesQuotation);
		dgSalesQuotation.setDataSource(dsSalesQuotation);
		dgSalesQuotation.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
		vpSalesQuotation.getElement().addClassName("varticalGridPanel1");
		dgSalesQuotation.getElement().addClassName("technicianSchedule2");
		vpSalesQuotation.add(dgSalesQuotation);
	}

	private HorizontalPanel intializeSalesQuotTabFilter() {
		btnNewSalesQuot=new Button("<image src='/images/menubar/043-plus.png' width='25px' height='18px'/>");
		btnDownloadSalesQuot=new Button("<image src='/images/menubar/146-download.png' width='25px' height='18px'/>");
		btnNewSalesQuot.getElement().addClassName("buttonIcon");
		btnDownloadSalesQuot.getElement().addClassName("buttonIcon");
		
		
		HorizontalPanel hPanel=new HorizontalPanel();
		
		hPanel.add(btnNewSalesQuot);
		hPanel.add(btnDownloadSalesQuot);
		
//		hPanel.add(lblSerQuot);
//		hPanel.add(lblSerQuot1);
		lbltotalSalesQuots.setVisible(false);
		lbltotalSalesQuots.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalSalesQuots.getElement().setClassName("processbaritems");
		lbltotalSalesQuots.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalSalesQuots.getElement().getStyle().setBackgroundImage(null);
		lbltotalSalesQuots.getElement().getStyle().setBackgroundColor("White");
		hPanel.add(lbltotalSalesQuots);
				
		lbltotalSalesQuotAmt.setVisible(false);
		lbltotalSalesQuotAmt.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalSalesQuotAmt.getElement().setClassName("processbaritems");
		lbltotalSalesQuotAmt.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalSalesQuotAmt.getElement().getStyle().setBackgroundImage(null);
		lbltotalSalesQuotAmt.getElement().getStyle().setBackgroundColor("White");
		hPanel.add(lbltotalSalesQuotAmt);
		
		hPanel.setSpacing(4);
		return hPanel;
	}
	

	private HorizontalPanel intializeSalesOrderTabFilter() {
		
		btnNewSalesOrder=new Button("<image src='/images/menubar/043-plus.png' width='25px' height='18px'/>");
		btnDownloadSalesOrder=new Button("<image src='/images/menubar/146-download.png' width='25px' height='18px'/>");
		btnNewSalesOrder.getElement().addClassName("buttonIcon");
		btnDownloadSalesOrder.getElement().addClassName("buttonIcon");
		
		HorizontalPanel hPanel=new HorizontalPanel();
		
		hPanel.add(btnNewSalesOrder);
		hPanel.add(btnDownloadSalesOrder);
		
		
		lbltotalSO.setVisible(false);
		lbltotalSO.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalSO.getElement().setClassName("processbaritems");
		lbltotalSO.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalSO.getElement().getStyle().setBackgroundImage(null);
		lbltotalSO.getElement().getStyle().setBackgroundColor("White");
		
		hPanel.add(lbltotalSO);
				
		lbltotalSOAmt.setVisible(false);
		lbltotalSOAmt.getElement().getStyle().setMarginLeft(10, Unit.PX);
		lbltotalSOAmt.getElement().setClassName("processbaritems");
		lbltotalSOAmt.getElement().getStyle().setBorderWidth(0, Unit.PX);
		lbltotalSOAmt.getElement().getStyle().setBackgroundImage(null);
		lbltotalSOAmt.getElement().getStyle().setBackgroundColor("White");
		
		hPanel.add(lbltotalSOAmt);
		
		hPanel.setSpacing(4);
		return hPanel;
	}

	private void initializeSalesOrderDataGrid() {
		dgSalesOrder = new ListGrid();
		setGridProperty(dgSalesOrder);
		dgSalesOrder.setFields(getSalesOrderGridFields());
		
		dsSalesOrder= new DataSource();
		dsSalesOrder.setClientOnly(true);
		setSalesOrderDataSourceField(dsSalesOrder);
		dgSalesOrder.setDataSource(dsSalesOrder);
		dgSalesOrder.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
		
		vpSalesOrder.getElement().addClassName("varticalGridPanel1");
		dgSalesOrder.getElement().addClassName("technicianSchedule2");
		vpSalesOrder.add(dgSalesOrder);
	}
	
	
	private ListGridField[] getSalesOrderGridFields() {
		   ListGridField print = new ListGridField("print"," ");
		   print.setWrap(true);
		   print.setAlign(Alignment.CENTER);
		   print.setType(ListGridFieldType.IMAGE);
		   print.setDefaultValue("/images/menubar/119-printer.png");
		   print.setCanEdit(false);
		   print.setWidth(40);
		   
		   ListGridField email = new ListGridField("email"," ");
		   email.setWrap(true);
		   email.setAlign(Alignment.CENTER);
		   email.setType(ListGridFieldType.IMAGE);
		   email.setDefaultValue("/images/menubar/159-email.png");
		   email.setCanEdit(false);
		   email.setWidth(40);
		   
		   ListGridField orderDate = new ListGridField("orderDate","Order Date");
		   orderDate.setWrap(true);
		   orderDate.setAlign(Alignment.CENTER);
		   
		   ListGridField salesPerson = new ListGridField("salesPerson","Sales Person");
		   salesPerson.setWrap(true);
		   salesPerson.setAlign(Alignment.CENTER);
		   
		   ListGridField customer = new ListGridField("customer","Customer");
		   customer.setWrap(true);
		   customer.setAlign(Alignment.CENTER);
		   
		   ListGridField cellNumber = new ListGridField("cellNumber","Cell Number");
		   cellNumber.setWrap(true);
		   cellNumber.setAlign(Alignment.CENTER);
		   
		   ListGridField status = new ListGridField("status","Status");
		   status.setWrap(true);
		   status.setAlign(Alignment.CENTER);
		   
		   ListGridField amount = new ListGridField("amount","Amount");
		   amount.setWrap(true);
		   amount.setAlign(Alignment.CENTER);
		   
//		   ListGridField approver = new ListGridField("approver","Approver");
//		   approver.setWrap(true);
//		   approver.setAlign(Alignment.CENTER);
		   
		   ListGridField orderId = new ListGridField("orderId","Order Id"); 
		   orderId.setWrap(true);
		   orderId.setAlign(Alignment.CENTER);
		   //Updated By: Ashwini Patil Date: 27-12-2021 Description: to display orderId on grid with underline
		   orderId.setCellFormatter(new CellFormatter() {
			@Override
			public String format(Object value, ListGridRecord record, int rowNum,
					int colNum) {
				// TODO Auto-generated method stub
				String id = record.getAttributeAsString("orderId");
				return "<u>"+id+"</u>";
			}
		});
		   
		   ListGridField quotationId = new ListGridField("quotationId","Quotation Id");
		   quotationId.setWrap(true);
		   quotationId.setAlign(Alignment.CENTER);
		   quotationId.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record, int rowNum,
						int colNum) {
					// TODO Auto-generated method stub
					String id = record.getAttributeAsString("quotationId");
					return "<u>"+id+"</u>";
				}
			});
		   
		   ListGridField leadId = new ListGridField("leadId","Lead Id");
		   leadId.setWrap(true);
		   leadId.setAlign(Alignment.CENTER);
		   leadId.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record, int rowNum,
						int colNum) {
					// TODO Auto-generated method stub
					String id = record.getAttributeAsString("leadId");
					return "<u>"+id+"</u>";
				}
			});
		   
		   ListGridField branch = new ListGridField("branch","Branch");
		   branch.setWrap(true);
		   branch.setAlign(Alignment.CENTER);
		   
		   ListGridField numberRange = new ListGridField("numberRange","Number Range");
		   numberRange.setWrap(true);
		   numberRange.setAlign(Alignment.CENTER);
		   
		   return new ListGridField[] { 
				  print,email,orderId,orderDate,salesPerson,customer,cellNumber,status,amount,
				   quotationId,leadId,branch,numberRange //removed comma
		   };

		}
		
		protected void setSalesOrderDataSourceField(DataSource dataSource) {
		   DataSourceField email = new DataSourceField("email", FieldType.IMAGE);
		   DataSourceField print = new DataSourceField("print", FieldType.IMAGE);
		   DataSourceDateField orderDate = new DataSourceDateField("orderDate");
		   orderDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		   
		   DataSourceField salesPerson = new DataSourceField("salesPerson", FieldType.TEXT);
		   DataSourceField customer = new DataSourceField("customer", FieldType.TEXT);
		  
		   DataSourceField cellNumber = new DataSourceField("cellNumber", FieldType.TEXT);
		   DataSourceField status = new DataSourceField("status", FieldType.TEXT);
		   DataSourceField amount = new DataSourceField("amount", FieldType.TEXT);
		  
		   
		   DataSourceField orderId = new DataSourceField("orderId", FieldType.INTEGER);
		   orderId.setPrimaryKey(true);
		   DataSourceField quotationId = new DataSourceField("quotationId", FieldType.INTEGER);
		   DataSourceField leadId = new DataSourceField("leadId", FieldType.INTEGER);
		   
		   DataSourceField branch = new DataSourceField("branch", FieldType.TEXT);
		   DataSourceField numberRange = new DataSourceField("numberRange", FieldType.TEXT);
		   
		   dataSource.setFields(print,email,orderId,orderDate,salesPerson,customer,cellNumber,status,amount,quotationId,leadId,branch,numberRange);
		}
		
		public void setDataToSalesOrderListGrid(List<SalesOrder> list){
			Console.log("before arranging salesorderlist");
			//Ashwini Patil Date:6-09-2022
			Collections.sort(list, new Comparator<SalesOrder>() {
					public int compare(SalesOrder o1, SalesOrder o2) {
							 return o2.getSalesOrderDate().compareTo(o1.getSalesOrderDate());
						}
				});
			Console.log("after arranging salesorderlist");
			dsSalesOrder.setTestData(getSalesOrderGridData(list));
			dgSalesOrder.setDataSource(dsSalesOrder);
			dgSalesOrder.setFields(getSalesOrderGridFields());
			dgSalesOrder.fetchData();
			
			int total=globalSalesOrderList.size();
			double totalamt=0;
			for(SalesOrder obj:globalSalesOrderList){
				totalamt=totalamt+obj.getNetpayable();
			}
							
			lbltotalSO.setText("Count: "+total);
			lbltotalSOAmt.setText("Total Amount: "+totalamt);
			lbltotalSO.setVisible(true);
			lbltotalSOAmt.setVisible(true);
		}
		
		private ListGridRecord[] getSalesOrderGridData(List<SalesOrder> list) {
			ListGridRecord[] records = null; 
			 records = new ListGridRecord[list.size()];
			 
			 for(int i=0;i<list.size();i++){
				 
				 ListGridRecord record = new ListGridRecord();
				 SalesOrder contract=list.get(i);
				 
				 record.setAttribute("email", "/images/menubar/159-email.png");
				 record.setAttribute("print", "/images/menubar/119-printer.png");
				 
				 
				 if(contract.getSalesOrderDate()!=null){
					 record.setAttribute("orderDate", contract.getSalesOrderDate());
				 }else{
					 record.setAttribute("orderDate", "");
				 }
				 record.setAttribute("customer",contract.getCinfo().getFullName());
				 if(contract.getEmployee()!=null){
					 record.setAttribute("salesPerson", contract.getEmployee());
				 }else{
					 record.setAttribute("salesPerson", "");
				 }
				 record.setAttribute("cellNumber", contract.getCinfo().getCellNumber().toString());
				 record.setAttribute("status",contract.getStatus());
				 record.setAttribute("amount", contract.getNetpayable());
				 record.setAttribute("branch", contract.getBranch());
				 record.setAttribute("approver", contract.getApproverName());
				 if(contract.getNumberRange()!=null){
					 record.setAttribute("numberRange", contract.getNumberRange());
				 }else{
					 record.setAttribute("numberRange", "");
				 }
				 record.setAttribute("orderId", contract.getCount());
				 if(contract.getQuotationCount()!=0&&contract.getQuotationCount()!=-1){
					 record.setAttribute("quotationId", contract.getQuotationCount());
				 }else{
					 record.setAttribute("quotationId", "");
				 }
				 if(contract.getLeadCount()!=0&&contract.getLeadCount()!=-1){
					 record.setAttribute("leadId", contract.getLeadCount());
				 }else{
					 record.setAttribute("leadId", "");
				 }
				 records[i]=record;
			 }
			 return records;
		}
		
		public Record addDateToSalesOrderDG(SalesOrder contract) {
			ListGridRecord record = new ListGridRecord();
			 record.setAttribute("email", "/images/menubar/159-email.png");
			 record.setAttribute("print", "/images/menubar/119-printer.png");
			 
			 
			 if(contract.getSalesOrderDate()!=null){
				 record.setAttribute("orderDate", contract.getSalesOrderDate());
			 }else{
				 record.setAttribute("orderDate", "");
			 }
			 record.setAttribute("customer",contract.getCinfo().getFullName());
			 if(contract.getEmployee()!=null){
				 record.setAttribute("salesPerson", contract.getEmployee());
			 }else{
				 record.setAttribute("salesPerson", "");
			 }
			 record.setAttribute("cellNumber", contract.getCinfo().getCellNumber().toString());
			 record.setAttribute("status",contract.getStatus());
			 record.setAttribute("amount", contract.getNetpayable());
			 record.setAttribute("branch", contract.getBranch());
			 record.setAttribute("approver", contract.getApproverName());
			 if(contract.getNumberRange()!=null){
				 record.setAttribute("numberRange", contract.getNumberRange());
			 }else{
				 record.setAttribute("numberRange", "");
			 }
			 record.setAttribute("orderId", contract.getCount());
			 if(contract.getQuotationCount()!=0&&contract.getQuotationCount()!=-1){
				 record.setAttribute("quotationId", contract.getQuotationCount());
			 }else{
				 record.setAttribute("quotationId", "");
			 }
			 if(contract.getLeadCount()!=0&&contract.getLeadCount()!=-1){
				 record.setAttribute("leadId", contract.getLeadCount());
			 }else{
				 record.setAttribute("leadId", "");
			 }
			return record;
		}
		
		private ListGridField[] getSalesQuotationGridFields() {
		   ListGridField print = new ListGridField("print"," ");
		   print.setWrap(true);
		   print.setAlign(Alignment.CENTER);
		   print.setType(ListGridFieldType.IMAGE);
		   print.setDefaultValue("/images/menubar/119-printer.png");
		   print.setCanEdit(false);
		   print.setWidth(40);
		   
		   ListGridField email = new ListGridField("email"," ");
		   email.setWrap(true);
		   email.setAlign(Alignment.CENTER);
		   email.setType(ListGridFieldType.IMAGE);
		   email.setDefaultValue("/images/menubar/159-email.png");
		   email.setCanEdit(false);
		   email.setWidth(40);
		   
		   ListGridField followup = new ListGridField("followup"," ");
		   followup.setWrap(true);
		   followup.setAlign(Alignment.CENTER);
		   followup.setType(ListGridFieldType.IMAGE);
		   followup.setDefaultValue("/images/menubar/044-file.png");
		   followup.setCanEdit(false);
		   followup.setWidth(40);
		   
		   ListGridField followupDate = new ListGridField("followupDate","Follow-Date");
		   followupDate.setWrap(true);
		   followupDate.setAlign(Alignment.CENTER);
		   
		   ListGridField quotationDate = new ListGridField("quotationDate","Quotation Date");
		   quotationDate.setWrap(true);
		   quotationDate.setAlign(Alignment.CENTER);
		   quotationDate.setWidth(135);
		   
		   ListGridField salesPerson = new ListGridField("salesPerson","Sales Person");
		   salesPerson.setWrap(true);
		   salesPerson.setAlign(Alignment.CENTER);
		   
		   ListGridField customer = new ListGridField("customer","Customer");
		   customer.setWrap(true);
		   customer.setAlign(Alignment.CENTER);
		   
		   ListGridField cellNumber = new ListGridField("cellNumber","Cell Number");
		   cellNumber.setWrap(true);
		   cellNumber.setAlign(Alignment.CENTER);
		   
		   ListGridField status = new ListGridField("status","Status");
		   status.setWrap(true);
		   status.setAlign(Alignment.CENTER);
		   
		   ListGridField amount = new ListGridField("amount","Amount");
		   amount.setWrap(true);
		   amount.setAlign(Alignment.CENTER);
		   
		   ListGridField approver = new ListGridField("approver","Approver");
		   approver.setWrap(true);
		   approver.setAlign(Alignment.CENTER);
		   
		   ListGridField validuntil = new ListGridField("validuntil","Valid Until");
		   validuntil.setWrap(true);
		   validuntil.setAlign(Alignment.CENTER);
		   
		   ListGridField quotationId = new ListGridField("quotationId","Quotation Id");
		   quotationId.setWrap(true);
		   quotationId.setAlign(Alignment.CENTER);
		   //Updated By: Ashwini Patil Date: 27-12-2021 Description: to display quotationId on grid with underline
		   quotationId.setCellFormatter(new CellFormatter() {
			@Override
			public String format(Object value, ListGridRecord record, int rowNum,
					int colNum) {
				// TODO Auto-generated method stub
				String id = record.getAttributeAsString("quotationId");
				return "<u>"+id+"</u>";
			}
		});
		   
		   ListGridField leadId = new ListGridField("leadId","Lead Id");
		   leadId.setWrap(true);
		   leadId.setAlign(Alignment.CENTER);
		   leadId.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record, int rowNum,
						int colNum) {
					// TODO Auto-generated method stub
					String id = record.getAttributeAsString("leadId");
					return "<u>"+id+"</u>";
				}
			});
		   
		   
		   ListGridField orderId = new ListGridField("orderId","Order Id"); 
		   orderId.setWrap(true);
		   orderId.setAlign(Alignment.CENTER);
		   orderId.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record, int rowNum,
						int colNum) {
					// TODO Auto-generated method stub
					String id = record.getAttributeAsString("orderId");
					return "<u>"+id+"</u>";
				}
			});
		   
		   ListGridField branch = new ListGridField("branch","Branch");
		   branch.setWrap(true);
		   branch.setAlign(Alignment.CENTER);
		   
		   return new ListGridField[] {print,email,followup,quotationId,followupDate,quotationDate,salesPerson,customer,
				   cellNumber,status,amount,approver,validuntil,leadId,orderId,branch
		   };

		}
			
		protected void setSalesQuotationDataSourceField(DataSource dataSource) {
		   DataSourceField email = new DataSourceField("email", FieldType.IMAGE);
		   DataSourceField print = new DataSourceField("print", FieldType.IMAGE);
		   DataSourceField followup = new DataSourceField("followup", FieldType.IMAGE);
		   
		   DataSourceDateField followupDate = new DataSourceDateField("followupDate");
		   DataSourceDateField quotationDate = new DataSourceDateField("quotationDate");
		   
		   followupDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		   quotationDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		   
		   DataSourceField customer = new DataSourceField("customer", FieldType.TEXT);
		   DataSourceField salesPerson = new DataSourceField("salesPerson", FieldType.TEXT);
		   DataSourceField cellNumber = new DataSourceField("cellNumber", FieldType.TEXT);
		   DataSourceField status = new DataSourceField("status", FieldType.TEXT);
		   DataSourceField amount = new DataSourceField("amount", FieldType.TEXT);
		   
		   DataSourceField approver = new DataSourceField("approver", FieldType.TEXT);
		   DataSourceField validuntil = new DataSourceField("validuntil", FieldType.TEXT);
		   
		   DataSourceField quotationId = new DataSourceField("quotationId", FieldType.INTEGER);
		   quotationId.setPrimaryKey(true);
		   DataSourceField leadId = new DataSourceField("leadId", FieldType.INTEGER);
		   DataSourceField orderId = new DataSourceField("orderId", FieldType.INTEGER);
		   DataSourceField branch = new DataSourceField("branch", FieldType.TEXT);
		   
		   
		   dataSource.setFields(print,email,followup,followupDate,quotationDate,customer,salesPerson,cellNumber,status,amount,approver,validuntil,quotationId,leadId,orderId,branch);
		}
		
		public void setDataToSalesQuotLG(List<SalesQuotation> list){
			//Ashwini Patil Date:6-09-2022
			Collections.sort(list, new Comparator<SalesQuotation>() {
					public int compare(SalesQuotation o1, SalesQuotation o2) {
							 return o2.getQuotationDate().compareTo(o1.getQuotationDate());
						}
				});
			dsSalesQuotation.setTestData(getSalesQuotGridData(list));
			dgSalesQuotation.setDataSource(dsSalesQuotation);
			dgSalesQuotation.setFields(getSalesQuotationGridFields());
			dgSalesQuotation.fetchData();
			
			int total=globalSalesQuotList.size();
			double totalamt=0;
			for(SalesQuotation obj:globalSalesQuotList){
				totalamt=totalamt+obj.getNetpayable();
			}
							
			lbltotalSalesQuots.setText("Count: "+total);
			lbltotalSalesQuotAmt.setText("Total Amount: "+totalamt);
			lbltotalSalesQuots.setVisible(true);
			lbltotalSalesQuotAmt.setVisible(true);
		}
		
		private ListGridRecord[] getSalesQuotGridData(List<SalesQuotation> list) {
			ListGridRecord[] records = null; 
			 records = new ListGridRecord[list.size()];
			 
			 for(int i=0;i<list.size();i++){
				 
				 ListGridRecord record = new ListGridRecord();
				 SalesQuotation quotation=list.get(i);
				 
				 record.setAttribute("email", "/images/menubar/159-email.png");
				 record.setAttribute("print", "/images/menubar/119-printer.png");
				 record.setAttribute("followup", "/images/menubar/044-file.png");
				 
				 if(quotation.getFollowUpDate()!=null){
					 record.setAttribute("followupDate", quotation.getFollowUpDate());
				 }else{
					 record.setAttribute("followupDate", "");
				 }
				 if(quotation.getQuotationDate()!=null){
					 record.setAttribute("quotationDate", quotation.getQuotationDate());
				 }else{
					 record.setAttribute("quotationDate", "");
				 }
				 record.setAttribute("customer",quotation.getCinfo().getFullName());
				 if(quotation.getEmployee()!=null){
					 record.setAttribute("salesPerson", quotation.getEmployee());
				 }else{
					 record.setAttribute("salesPerson", "");
				 }
				 record.setAttribute("cellNumber", quotation.getCinfo().getCellNumber().toString());
				 record.setAttribute("status",quotation.getStatus());
				 record.setAttribute("amount", quotation.getNetpayable());
				 record.setAttribute("branch", quotation.getBranch());
				
				 if(quotation.getValidUntill()!=null){
					 record.setAttribute("validuntil", quotation.getValidUntill());
				 }else{
					 record.setAttribute("validuntil", "");
				 }
				 
				 record.setAttribute("approver", quotation.getApproverName());
				 
				 record.setAttribute("quotationId", quotation.getCount());
				 if(quotation.getContractCount()!=0&&quotation.getContractCount()!=-1){
					 record.setAttribute("orderId", quotation.getContractCount());
				 }else{
					 record.setAttribute("orderId", "");
				 }
				 if(quotation.getLeadCount()!=0&&quotation.getLeadCount()!=-1){
					 record.setAttribute("leadId", quotation.getLeadCount());
				 }else{
					 record.setAttribute("leadId", "");
				 }
				 records[i]=record;
				 
				
			 }
			 return records;
		}
		
		public Record addDateToSalesQuotDG(SalesQuotation quotation) {
			 ListGridRecord record = new ListGridRecord();
			 record.setAttribute("email", "/images/menubar/159-email.png");
			 record.setAttribute("print", "/images/menubar/119-printer.png");
			 record.setAttribute("followup", "/images/menubar/044-file.png");
			 
			 if(quotation.getFollowUpDate()!=null){
				 record.setAttribute("followupDate", quotation.getFollowUpDate());
			 }else{
				 record.setAttribute("followupDate", "");
			 }
			 if(quotation.getQuotationDate()!=null){
				 record.setAttribute("quotationDate", quotation.getQuotationDate());
			 }else{
				 record.setAttribute("quotationDate", "");
			 }
			 record.setAttribute("customer",quotation.getCinfo().getFullName());
			 if(quotation.getEmployee()!=null){
				 record.setAttribute("salesPerson", quotation.getEmployee());
			 }else{
				 record.setAttribute("salesPerson", "");
			 }
			 record.setAttribute("cellNumber", quotation.getCinfo().getCellNumber().toString());
			 record.setAttribute("status",quotation.getStatus());
			 record.setAttribute("amount", quotation.getNetpayable());
			 record.setAttribute("branch", quotation.getBranch());
			
			 if(quotation.getValidUntill()!=null){
				 record.setAttribute("validuntil", quotation.getValidUntill());
			 }else{
				 record.setAttribute("validuntil", "");
			 }
			 
			 record.setAttribute("approver", quotation.getApproverName());
			 
			 record.setAttribute("quotationId", quotation.getCount());
			 if(quotation.getContractCount()!=0&&quotation.getContractCount()!=-1){
				 record.setAttribute("orderId", quotation.getContractCount());
			 }else{
				 record.setAttribute("orderId", "");
			 }
			 if(quotation.getLeadCount()!=0&&quotation.getLeadCount()!=-1){
				 record.setAttribute("leadId", quotation.getLeadCount());
			 }else{
				 record.setAttribute("leadId", "");
			 }
			return record;
		}
		
		private ListGridField[] getSalesLeadGridFields() {
		   ListGridField followup = new ListGridField("followup"," ");
		   followup.setWrap(true);
		   followup.setAlign(Alignment.CENTER);
		   followup.setType(ListGridFieldType.IMAGE);
		   followup.setDefaultValue("/images/menubar/044-file.png");
		   followup.setCanEdit(false);
		   followup.setWidth(40);
		   
		   ListGridField followupDate = new ListGridField("followupDate","Follow-Date");
		   followupDate.setWrap(true);
		   followupDate.setAlign(Alignment.CENTER);
		   followupDate.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
		   
		   DateRangeItem dateRangeItem = new DateRangeItem();  
		   dateRangeItem.setAllowRelativeDates(true);
		   dateRangeItem.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
		   followupDate.setFilterEditorType(dateRangeItem);
		   
		   ListGridField title = new ListGridField("title","Title");
		   title.setWrap(true);
		   title.setAlign(Alignment.LEFT);
		   
		   ListGridField salesPerson = new ListGridField("salesPerson","Sales Person");
		   salesPerson.setWrap(true);
		   salesPerson.setAlign(Alignment.LEFT);
		   
		   ListGridField customer = new ListGridField("customer","Customer");
		   customer.setWrap(true);
		   customer.setAlign(Alignment.LEFT);
		   
		   ListGridField cellNumber = new ListGridField("cellNumber","Cell Number");
		   cellNumber.setWrap(true);
		   cellNumber.setAlign(Alignment.CENTER);
		   
		   ListGridField status = new ListGridField("status","Status");
		   status.setWrap(true);
		   status.setAlign(Alignment.LEFT);
		   
		   ListGridField amount = new ListGridField("amount","Amount");
		   amount.setWrap(true);
		   amount.setAlign(Alignment.RIGHT);
		   
		   ListGridField group = new ListGridField("group","Group");
		   group.setWrap(true);
		   group.setAlign(Alignment.RIGHT);
		   
		   ListGridField category = new ListGridField("category","Category");
		   category.setWrap(true);
		   category.setAlign(Alignment.RIGHT);
		   
		   ListGridField type = new ListGridField("type","Type");
		   type.setWrap(true);
		   type.setAlign(Alignment.RIGHT);
		   
		   ListGridField creationDate = new ListGridField("creationDate","Creation Date");
		   creationDate.setWrap(true);
		   creationDate.setAlign(Alignment.CENTER);
		   creationDate.setWidth(82);
		   creationDate.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
		   
		   DateRangeItem dateRangeItem1 = new DateRangeItem();  
		   dateRangeItem1.setAllowRelativeDates(true);
		   dateRangeItem1.setDateFormatter(DateDisplayFormat.TOEUROPEANSHORTDATE);
		   creationDate.setFilterEditorType(dateRangeItem1);
		   
		   ListGridField leadId = new ListGridField("leadId","Lead Id");
		   leadId.setWrap(true);
		   leadId.setAlign(Alignment.CENTER);
		   //Updated By: Ashwini Patil Date: 27-12-2021 Description: to display leadId on grid with underline
		   leadId.setCellFormatter(new CellFormatter() {
			@Override
			public String format(Object value, ListGridRecord record, int rowNum,
					int colNum) {
				// TODO Auto-generated method stub
				String id = record.getAttributeAsString("leadId");
				return "<u>"+id+"</u>";
			}
		});
		   
		   ListGridField orderId = new ListGridField("orderId","Order Id"); 
		   orderId.setWrap(true);
		   orderId.setAlign(Alignment.CENTER);
		   orderId.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record, int rowNum,
						int colNum) {
					// TODO Auto-generated method stub
					String id = record.getAttributeAsString("orderId");
					return "<u>"+id+"</u>";
				}
			});
		   
		   ListGridField branch = new ListGridField("branch","Branch");
		   branch.setWrap(true);
		   branch.setAlign(Alignment.LEFT);
		   
		   return new ListGridField[] {followup,leadId,followupDate,title,salesPerson,customer,
				   cellNumber,status,amount,group,category,type,creationDate,orderId,branch
		   };

		}
			
		protected void setServiceLeadDataSourceField(DataSource dataSource) {
		   DataSourceField followup = new DataSourceField("followup", FieldType.IMAGE);
		   
		   DataSourceDateField followupDate = new DataSourceDateField("followupDate");
		   DataSourceDateField creationDate = new DataSourceDateField("creationDate");
		   
		   followupDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		   creationDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		   
		   
		   DataSourceField customer = new DataSourceField("customer", FieldType.TEXT);
		   DataSourceField salesPerson = new DataSourceField("salesPerson", FieldType.TEXT);
		   DataSourceField cellNumber = new DataSourceField("cellNumber", FieldType.TEXT);
		   DataSourceField status = new DataSourceField("status", FieldType.TEXT);
		   DataSourceField amount = new DataSourceField("amount", FieldType.TEXT);
		   
		   DataSourceField leadId = new DataSourceField("leadId", FieldType.INTEGER);
		   leadId.setPrimaryKey(true);
		   DataSourceField orderId = new DataSourceField("orderId", FieldType.INTEGER);
		   DataSourceField branch = new DataSourceField("branch", FieldType.TEXT);
		   
		   DataSourceField title = new DataSourceField("title", FieldType.TEXT);
		   DataSourceField group = new DataSourceField("group", FieldType.TEXT);
		   DataSourceField category = new DataSourceField("category", FieldType.TEXT);
		   DataSourceField type = new DataSourceField("type", FieldType.TEXT);
		   
//		   dataSource.setFields(followup,followupDate,creationDate,customer,salesPerson,cellNumber,status,amount,leadId,orderId,branch);
		   dataSource.setFields(followup,leadId,followupDate,creationDate,customer,salesPerson,cellNumber,status,amount,orderId,branch);//changed position of id
			
		   
		   //commented by ashwini
//		   dataSource.setFields(followup,followupDate,title,customer,cellNumber,status,amount,creationDate,group,category,type,leadId,orderId,branch);
		}
		
		public void setDataToSalesLeadLG(List<Lead> list){
			//Ashwini Patil Date:6-09-2022
			Collections.sort(list, new Comparator<Lead>() {
					public int compare(Lead o1, Lead o2) {
							 return o2.getFollowUpDate().compareTo(o1.getFollowUpDate());
						}
				});
			if(list!=null&&list.size()>0){
				dsSalesLead.setTestData(getSalesLeadGridData(list));
				dgSalesLead.setDataSource(dsSalesLead);
			}else{
				dgSalesLead.setData(new ListGridRecord[] {});
			}
			
			dgSalesLead.setFields(getSalesLeadGridFields());
			dgSalesLead.fetchData();
			
			
			int total=globalSalesLeadList.size();
			double totalamt=0;
			for(Lead obj:globalSalesLeadList){
				totalamt=totalamt+obj.getNetpayable();
			}
							
			lbltotalSalesLeads.setText("Count: "+total);
			lbltotalSalesLeadAmt.setText("Total Amount: "+totalamt);
			lbltotalSalesLeads.setVisible(true);
			lbltotalSalesLeadAmt.setVisible(true);
		}
		
		private ListGridRecord[] getSalesLeadGridData(List<Lead> leadList) {
			 ListGridRecord[] records = null; 
			 records = new ListGridRecord[leadList.size()];
			 
			 for(int i=0;i<leadList.size();i++){
				 
				 ListGridRecord record = new ListGridRecord();
				 Lead lead=leadList.get(i);
				 
				 record.setAttribute("followup", "/images/menubar/044-file.png");
				 
				 if(lead.getFollowUpDate()!=null){
					 record.setAttribute("followupDate", lead.getFollowUpDate());
				 }else{
					 record.setAttribute("followupDate", "");
				 }
				 if(lead.getCreationDate()!=null){
					 record.setAttribute("creationDate", lead.getCreationDate());
				 }else{
					 record.setAttribute("creationDate", "");
				 }
				 record.setAttribute("customer",lead.getPersonInfo().getFullName());
				 if(lead.getEmployee()!=null){
					 record.setAttribute("salesPerson", lead.getEmployee());
				 }else{
					 record.setAttribute("salesPerson", "");
				 }
				 record.setAttribute("cellNumber", lead.getPersonInfo().getCellNumber().toString());
				 record.setAttribute("status",lead.getStatus());
				 record.setAttribute("amount", lead.getNetpayable());
				 record.setAttribute("branch", lead.getBranch());
				
				 
				 if(lead.getContractCount()!=null){
					 record.setAttribute("orderId", lead.getContractCount());
				 }else{
					 record.setAttribute("orderId", "");
				 }
				 record.setAttribute("leadId", lead.getCount());
				 
				 if(lead.getTitle()!=null){
					 record.setAttribute("title", lead.getTitle());
				 }else{
					 record.setAttribute("title", "");
				 }
				 
				 if(lead.getGroup()!=null){
					 record.setAttribute("group", lead.getGroup());
				 }else{
					 record.setAttribute("group", "");
				 }
				 
				 if(lead.getCategory()!=null){
					 record.setAttribute("category", lead.getCategory());
				 }else{
					 record.setAttribute("category", "");
				 }
				 
				 if(lead.getType()!=null){
					 record.setAttribute("type", lead.getType());
				 }else{
					 record.setAttribute("type", "");
				 }
				 
				 records[i]=record;
				 
			 }
			 
			 return records;
		}
		
		public ListGridRecord addDateToSalesLeadDG(Lead lead) {
			 ListGridRecord record = new ListGridRecord();
			 record.setAttribute("followup", "/images/menubar/044-file.png");
			 
			 if(lead.getFollowUpDate()!=null){
				 record.setAttribute("followupDate", lead.getFollowUpDate());
			 }else{
				 record.setAttribute("followupDate", "");
			 }
			 if(lead.getCreationDate()!=null){
				 record.setAttribute("creationDate", lead.getCreationDate());
			 }else{
				 record.setAttribute("creationDate", "");
			 }
			 record.setAttribute("customer",lead.getPersonInfo().getFullName());
			 if(lead.getEmployee()!=null){
				 record.setAttribute("salesPerson", lead.getEmployee());
			 }else{
				 record.setAttribute("salesPerson", "");
			 }
			 record.setAttribute("cellNumber", lead.getPersonInfo().getCount());
			 record.setAttribute("status",lead.getStatus());
			 record.setAttribute("amount", lead.getNetpayable());
			 record.setAttribute("branch", lead.getBranch());
			 if(lead.getContractCount()!=null){
				 record.setAttribute("orderId", lead.getContractCount());
			 }else{
				 record.setAttribute("orderId", "");
			 }
			 record.setAttribute("leadId", lead.getCount());
			 
			 if(lead.getTitle()!=null){
				 record.setAttribute("title", lead.getTitle());
			 }else{
				 record.setAttribute("title", "");
			 }
			 
			 if(lead.getGroup()!=null){
				 record.setAttribute("group", lead.getGroup());
			 }else{
				 record.setAttribute("group", "");
			 }
			 
			 if(lead.getCategory()!=null){
				 record.setAttribute("category", lead.getCategory());
			 }else{
				 record.setAttribute("category", "");
			 }
			 
			 if(lead.getType()!=null){
				 record.setAttribute("type", lead.getType());
			 }else{
				 record.setAttribute("type", "");
			 }
			 return record;
		}
		
		
		private ListGridField[] getDeliveryNoteGridFields() {
			   ListGridField print = new ListGridField("print"," ");
			   print.setWrap(true);
			   print.setAlign(Alignment.CENTER);
			   print.setType(ListGridFieldType.IMAGE);
			   print.setDefaultValue("/images/menubar/119-printer.png");
			   print.setCanEdit(false);
			   print.setWidth(40);
			   
			   ListGridField email = new ListGridField("email"," ");
			   email.setWrap(true);
			   email.setAlign(Alignment.CENTER);
			   email.setType(ListGridFieldType.IMAGE);
			   email.setDefaultValue("/images/menubar/159-email.png");
			   email.setCanEdit(false);
			   email.setWidth(40);
			   
			   ListGridField deliveryDate = new ListGridField("deliveryDate","Delivery Date");
			   deliveryDate.setWrap(true);
			   deliveryDate.setAlign(Alignment.CENTER);
			   
			   ListGridField completionDate = new ListGridField("completionDate","Completion Date");
			   completionDate.setWrap(true);
			   completionDate.setAlign(Alignment.CENTER);
			   
			   ListGridField salesPerson = new ListGridField("salesPerson","Sales Person");
			   salesPerson.setWrap(true);
			   salesPerson.setAlign(Alignment.CENTER);
			   
			   ListGridField customer = new ListGridField("customer","Customer");
			   customer.setWrap(true);
			   customer.setAlign(Alignment.CENTER);
			   
			   ListGridField cellNumber = new ListGridField("cellNumber","Cell Number");
			   cellNumber.setWrap(true);
			   cellNumber.setAlign(Alignment.CENTER);
			   
			   ListGridField status = new ListGridField("status","Status");
			   status.setWrap(true);
			   status.setAlign(Alignment.CENTER);
			   
			   ListGridField deliveryNoteId = new ListGridField("deliveryNoteId","Delivery Note Id"); 
			   deliveryNoteId.setWrap(true);
			   deliveryNoteId.setAlign(Alignment.CENTER);
			   //Updated By: Ashwini Patil Date: 27-12-2021 Description: to display deliveryNoteId on grid with underline
			   deliveryNoteId.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record, int rowNum,
						int colNum) {
					// TODO Auto-generated method stub
					String id = record.getAttributeAsString("deliveryNoteId");
					return "<u>"+id+"</u>";
				}
			});
			   
			   ListGridField orderId = new ListGridField("orderId","Order Id"); 
			   orderId.setWrap(true);
			   orderId.setAlign(Alignment.CENTER);
			   orderId.setCellFormatter(new CellFormatter() {
					@Override
					public String format(Object value, ListGridRecord record, int rowNum,
							int colNum) {
						// TODO Auto-generated method stub
						String id = record.getAttributeAsString("orderId");
						return "<u>"+id+"</u>";
					}
				});
			
			   
			   ListGridField branch = new ListGridField("branch","Branch");
			   branch.setWrap(true);
			   branch.setAlign(Alignment.CENTER);
			   
			   return new ListGridField[] { 
					  print,email,deliveryNoteId, deliveryDate,completionDate,salesPerson,customer,cellNumber,status,
					  orderId,branch //removed comma
			   };

			}
			
			protected void setDeliveryNoteDataSourceField(DataSource dataSource) {
			   DataSourceField email = new DataSourceField("email", FieldType.IMAGE);
			   DataSourceField print = new DataSourceField("print", FieldType.IMAGE);
			   DataSourceDateField deliveryDate = new DataSourceDateField("deliveryDate");
			   deliveryDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
			   
			   DataSourceDateField completionDate = new DataSourceDateField("completionDate");
			   completionDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
			   
			   DataSourceField salesPerson = new DataSourceField("salesPerson", FieldType.TEXT);
			   DataSourceField customer = new DataSourceField("customer", FieldType.TEXT);
			  
			   DataSourceField cellNumber = new DataSourceField("cellNumber", FieldType.TEXT);
			   DataSourceField status = new DataSourceField("status", FieldType.TEXT);
			   DataSourceField amount = new DataSourceField("amount", FieldType.TEXT);
			  
			   DataSourceField deliveryNoteId = new DataSourceField("deliveryNoteId", FieldType.INTEGER);
			   deliveryNoteId.setPrimaryKey(true);
			   
			   DataSourceField orderId = new DataSourceField("orderId", FieldType.INTEGER);
			   
			   DataSourceField branch = new DataSourceField("branch", FieldType.TEXT);
			   
			   //this is different that getGridFields method
			   dataSource.setFields(print,email,deliveryDate,completionDate,salesPerson,customer,cellNumber,status,amount,orderId,branch);
			}
			
			public void setDataToDeliveryNoteListGrid(List<DeliveryNote> list){
				//Ashwini Patil Date:6-09-2022
				Collections.sort(list, new Comparator<DeliveryNote>() {
						public int compare(DeliveryNote o1, DeliveryNote o2) {
								 return o2.getDeliveryDate().compareTo(o1.getDeliveryDate());
							}
					});
				dsDeliveryNote.setTestData(getDeliveryNoteGridData(list));
				dgDeliveryNote.setDataSource(dsDeliveryNote);
				dgDeliveryNote.setFields(getDeliveryNoteGridFields());
				dgDeliveryNote.fetchData();
				
				int total=globalDeliveryNoteList.size();
								
				lbltotalDNotes.setText("Count: "+total);
				lbltotalDNotes.setVisible(true);
				
			}
			
			private ListGridRecord[] getDeliveryNoteGridData(List<DeliveryNote> list) {
				ListGridRecord[] records = null; 
				 records = new ListGridRecord[list.size()];
				 
				 for(int i=0;i<list.size();i++){
					 
					 ListGridRecord record = new ListGridRecord();
					 DeliveryNote contract=list.get(i);
					 
					 record.setAttribute("email", "/images/menubar/159-email.png");
					 record.setAttribute("print", "/images/menubar/119-printer.png");
					 
					 
					 if(contract.getDeliveryDate()!=null){
						 record.setAttribute("deliveryDate", contract.getDeliveryDate());
					 }else{
						 record.setAttribute("deliveryDate", "");
					 }
					 if(contract.getCompletionDate()!=null){
						 record.setAttribute("completionDate", contract.getCompletionDate());
					 }else{
						 record.setAttribute("completionDate", "");
					 }
					 
					 record.setAttribute("customer",contract.getCinfo().getFullName());
					 if(contract.getEmployee()!=null){
						 record.setAttribute("salesPerson", contract.getEmployee());
					 }else{
						 record.setAttribute("salesPerson", "");
					 }
					 record.setAttribute("cellNumber", contract.getCinfo().getCellNumber().toString());
					 record.setAttribute("status",contract.getStatus());
					 record.setAttribute("amount", contract.getNetpayable());
					 record.setAttribute("branch", contract.getBranch());
					 record.setAttribute("approver", contract.getApproverName());
					 
					 record.setAttribute("deliveryNoteId", contract.getCount());
					 if(contract.getSalesOrderCount()!=0&&contract.getSalesOrderCount()!=-1){
						 record.setAttribute("orderId", contract.getSalesOrderCount());
					 }else{
						 record.setAttribute("orderId", "");
					 }
					
					 records[i]=record;
				 }
				 return records;
			}
			
			public Record addDateToDeliveryNoteDG(DeliveryNote contract) {
				ListGridRecord record = new ListGridRecord();
				 record.setAttribute("email", "/images/menubar/159-email.png");
				 record.setAttribute("print", "/images/menubar/119-printer.png");
				 
				 
				 if(contract.getDeliveryDate()!=null){
					 record.setAttribute("deliveryDate", contract.getDeliveryDate());
				 }else{
					 record.setAttribute("deliveryDate", "");
				 }
				 if(contract.getCompletionDate()!=null){
					 record.setAttribute("completionDate", contract.getCompletionDate());
				 }else{
					 record.setAttribute("completionDate", "");
				 }
				 record.setAttribute("customer",contract.getCinfo().getFullName());
				 if(contract.getEmployee()!=null){
					 record.setAttribute("salesPerson", contract.getEmployee());
				 }else{
					 record.setAttribute("salesPerson", "");
				 }
				 record.setAttribute("cellNumber", contract.getCinfo().getCellNumber());
				 record.setAttribute("status",contract.getStatus());
				 record.setAttribute("amount", contract.getNetpayable());
				 record.setAttribute("branch", contract.getBranch());
				 record.setAttribute("approver", contract.getApproverName());
				
				 record.setAttribute("deliveryNoteId", contract.getCount());
				 if(contract.getSalesOrderCount()!=0&&contract.getSalesOrderCount()!=-1){
					 record.setAttribute("orderId", contract.getSalesOrderCount());
				 }else{
					 record.setAttribute("orderId", "");
				 }
				return record;
			}
			
			public boolean isCustomerAndDateSelected(){
				if (personInfo.getIdValue() != -1 && (dateComparator.getFromDate().getValue()!=null||dateComparator.getToDate().getValue()!=null)) {
					return true;
				}
				return false;
			}
			
			public boolean isSalesPersonAndDateSelected(){
				if (olbSalesPerson.getSelectedIndex() != 0 && (dateComparator.getFromDate().getValue()!=null||dateComparator.getToDate().getValue()!=null)) {
					return true;
				}
				return false;
			}
			
			public ArrayList<SuperModel> getFilteredData(ArrayList<SuperModel> list,String tabName){
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				try{
					if(list==null||list.size()==0){
						return list;
					}
					
					Console.log("Before Filteration : "+list.size());
					for(SuperModel model:list){
						if(tabName.equals("Service Lead")||tabName.equals("Sales Lead")){
							Lead obj=(Lead) model;
							if(olbSalesPerson.getValue()!=null){
								if(!obj.getEmployee().equals(olbSalesPerson.getValue())){
									continue;
								}
							}
							if(olbBranch.getValue()!=null){
								if(!obj.getBranch().equals(olbBranch.getValue())){
									continue;
								}
							}
							if(lbStaus.getSelectedIndex()!=0){
								if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Open")){
									if(!lsLeadStatus.contains(obj.getStatus())){
										continue;
									}
								}else if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Closed")){
									if(!obj.getStatus().equals("Successful")){
										continue;
									}
								}else{
									if(!obj.getStatus().equals(lbStaus.getItemText(lbStaus.getSelectedIndex()))){
										continue;
									}
								}
							}
							modelList.add(model);
						}else if(tabName.equals("Service Quotation")){
							Quotation obj=(Quotation) model;
							if(olbSalesPerson.getValue()!=null){
								if(!obj.getEmployee().equals(olbSalesPerson.getValue())){
									continue;
								}
							}
							if(olbBranch.getValue()!=null){
								if(!obj.getBranch().equals(olbBranch.getValue())){
									continue;
								}
							}
							if(lbStaus.getSelectedIndex()!=0){
								if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Open")){
									if(!lsQuotStatus.contains(obj.getStatus())){
										continue;
									}
								}else if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Closed")){
									if(!obj.getStatus().equals("Successful")){
										continue;
									}
								}else{
									if(!obj.getStatus().equals(lbStaus.getItemText(lbStaus.getSelectedIndex()))){
										continue;
									}
								}
							}
							modelList.add(model);
						}else if(tabName.equals("Contract")){
							Contract obj=(Contract) model;
							if(olbSalesPerson.getValue()!=null){
								if(!obj.getEmployee().equals(olbSalesPerson.getValue())){
									continue;
								}
							}
							if(olbBranch.getValue()!=null){
								if(!obj.getBranch().equals(olbBranch.getValue())){
									continue;
								}
							}
							if(lbStaus.getSelectedIndex()!=0){
								if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Open")){
									if(!lsOrderStatus.contains(obj.getStatus())){
										continue;
									}
								}else if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Closed")){
									if(!obj.getStatus().equals("Approved")){
										continue;
									}
								}else{
									if(!obj.getStatus().equals(lbStaus.getItemText(lbStaus.getSelectedIndex()))){
										continue;
									}
								}
							}
							modelList.add(model);
						}else if(tabName.equals("Bill")){
							BillingDocument obj=(BillingDocument) model;
							if(olbSalesPerson.getValue()!=null){
								if(!obj.getEmployee().equals(olbSalesPerson.getValue())){
									continue;
								}
							}
							if(olbBranch.getValue()!=null){
								if(!obj.getBranch().equals(olbBranch.getValue())){
									continue;
								}
							}
							if(lbStaus.getSelectedIndex()!=0){
								if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Open")){
									if(!lsOrderStatus.contains(obj.getStatus())){
										continue;
									}
								}else if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Closed")){
									if(!obj.getStatus().equals("Invoiced")){
										continue;
									}
								}else{
									if(!obj.getStatus().equals(lbStaus.getItemText(lbStaus.getSelectedIndex()))){
										continue;
									}
								}
							}
							modelList.add(model);
						}else if(tabName.equals("Invoice")){
							Invoice obj=(Invoice) model;
							if(olbSalesPerson.getValue()!=null){
								if(!obj.getEmployee().equals(olbSalesPerson.getValue())){
									continue;
								}
							}
							if(olbBranch.getValue()!=null){
								if(!obj.getBranch().equals(olbBranch.getValue())){
									continue;
								}
							}
							if(lbStaus.getSelectedIndex()!=0){
								if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Open")){
									if(!lsInvoiceStatus.contains(obj.getStatus())){
										continue;
									}
								}else if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Closed")){
									if(!obj.getStatus().equals("Approved")){
										continue;
									}
								}else{
									if(!obj.getStatus().equals(lbStaus.getItemText(lbStaus.getSelectedIndex()))){
										continue;
									}
								}
							}
							modelList.add(model);
						}else if(tabName.equals("Payment")){
							CustomerPayment obj=(CustomerPayment) model;
							if(olbSalesPerson.getValue()!=null){
								if(!obj.getEmployee().equals(olbSalesPerson.getValue())){
									continue;
								}
							}
							if(olbBranch.getValue()!=null){
								if(!obj.getBranch().equals(olbBranch.getValue())){
									continue;
								}
							}
							if(lbStaus.getSelectedIndex()!=0){
								if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Open")){
									if(!obj.getStatus().equals("Created")){
										continue;
									}
								}else{
									if(!obj.getStatus().equals(lbStaus.getItemText(lbStaus.getSelectedIndex()))){
										continue;
									}
								}
							}
							modelList.add(model);
						}else if(tabName.equals("Sales Quotation")){
							SalesQuotation obj=(SalesQuotation) model;
							if(olbSalesPerson.getValue()!=null){
								if(!obj.getEmployee().equals(olbSalesPerson.getValue())){
									continue;
								}
							}
							if(olbBranch.getValue()!=null){
								if(!obj.getBranch().equals(olbBranch.getValue())){
									continue;
								}
							}
							if(lbStaus.getSelectedIndex()!=0){
								if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Open")){
									if(!lsQuotStatus.contains(obj.getStatus())){
										continue;
									}
								}else if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Closed")){
									if(!obj.getStatus().equals("Successful")){
										continue;
									}
								}else{
									if(!obj.getStatus().equals(lbStaus.getItemText(lbStaus.getSelectedIndex()))){
										continue;
									}
								}
							}
							modelList.add(model);
						}else if(tabName.equals("Sales Order")){
							SalesOrder obj=(SalesOrder) model;
							if(olbSalesPerson.getValue()!=null){
								if(!obj.getEmployee().equals(olbSalesPerson.getValue())){
									continue;
								}
							}
							if(olbBranch.getValue()!=null){
								if(!obj.getBranch().equals(olbBranch.getValue())){
									continue;
								}
							}
							if(lbStaus.getSelectedIndex()!=0){
								if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Open")){
									if(!lsOrderStatus.contains(obj.getStatus())){
										continue;
									}
								}else if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Closed")){
									if(!obj.getStatus().equals("Approved")){
										continue;
									}
								}else{
									if(!obj.getStatus().equals(lbStaus.getItemText(lbStaus.getSelectedIndex()))){
										continue;
									}
								}
							}
							modelList.add(model);
						}else if(tabName.equals("Delivery Note")){
							DeliveryNote obj=(DeliveryNote) model;
							if(olbSalesPerson.getValue()!=null){
								if(!obj.getEmployee().equals(olbSalesPerson.getValue())){
									continue;
								}
							}
							if(olbBranch.getValue()!=null){
								if(!obj.getBranch().equals(olbBranch.getValue())){
									continue;
								}
							}
							if(lbStaus.getSelectedIndex()!=0){
								if(lbStaus.getItemText(lbStaus.getSelectedIndex()).equals("Open")){
									if(!obj.getStatus().equals("Created")){
										continue;
									}
								}else{
									if(!obj.getStatus().equals(lbStaus.getItemText(lbStaus.getSelectedIndex()))){
										continue;
									}
								}
							}
							modelList.add(model);
						}
					}
				}catch(Exception e){
					
				}
				
				Console.log("After Filteration : "+modelList.size());
				return modelList;
						
			}
			@Override
			public void setToViewState() {
				// TODO Auto-generated method stub
				super.setToViewState();
				personInfo.setEnable(true);
				olbBranch.setEnabled(true);
				olbSalesPerson.setEnabled(true);
				dateComparator.setEnable(true);
				btnClear.setEnabled(true);
				btnSearch.setEnabled(true);
				lbStaus.setEnabled(true);
			}

}
