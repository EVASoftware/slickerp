package com.slicktechnologies.client.views.summaryscreen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.cnc.CNCForm;
import com.slicktechnologies.client.views.complain.ServiceDateBoxPopup;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contractrenewal.ContractRenewalForm;
import com.slicktechnologies.client.views.dashboard.leaddashboard.CommunicationSmartPoup;
import com.slicktechnologies.client.views.deliverynote.DeliveryNoteForm;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNForm;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsForm;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.project.concproject.ProjectForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.quickcontract.NewCustomerPopup;
import com.slicktechnologies.client.views.quickcontract.QuickContractForm;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationForm;
import com.slicktechnologies.client.views.service.ServiceForm;
import com.slicktechnologies.client.views.workorder.WorkOrderForm;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Device;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.workorder.WorkOrder;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.twilio.sdk.resource.instance.Account;

public class SummaryPresenter extends FormScreenPresenter<DummyEntityOnlyForScreen> implements CellClickHandler {

	SummaryForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	GenricServiceAsync async=GWT.create(GenricService.class);
	
	CommonServiceAsync commonServ=GWT.create(CommonService.class);
	
	NewCustomerPopup newcustomerPopup = new NewCustomerPopup();
	int serTabSelIndex;
	int accTabSelIndex;
	int salTabSelIndex;
	
	CommunicationSmartPoup commSmrtPopup=new CommunicationSmartPoup();
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	int position;
	ListGridRecord selectedRecord;
	
	Lead leadObj=null;
	Quotation quotationObj=null;
	Contract contractObj=null;
	
	BillingDocument billObj=null;
	Invoice invoiceObj=null;
	CustomerPayment custPayObj=null;
	
	GeneralViewDocumentPopup genralInvoicePopup=new GeneralViewDocumentPopup();
	NewEmailPopUp emailpopup = new NewEmailPopUp();
	public boolean diffrentBranchflag = false;
	GeneralViewDocumentPopup billingPopUp=new GeneralViewDocumentPopup();
	GeneralViewDocumentPopup salesLeadPopup=new GeneralViewDocumentPopup();
	
	Lead salesLeadObj=null;
	SalesQuotation salesQuotObj=null;
	SalesOrder salesOrderObj=null;
	DeliveryNote deliveryNoteObj=null;
	
	//Ashwini Patil
	WorkOrder workOrderObj=null;
	PurchaseOrder purchaseOrderObj=null;
	Service serviceObj=null;
	Device deviceObj=null;
	boolean hideNavigationFlag=false;//Ashwini Patil Date:22-08-2022
	
	String viewedScreenName="";
	ServiceDateBoxPopup otpPopup=new ServiceDateBoxPopup("OTP"); //Ashwini Patil Date:5-07-2023 to restrict download with otp for hi tech client
	PopupPanel otppopupPanel;
	int generatedOTP;
	SmsServiceAsync smsService = GWT.create(SmsService.class);
	
	public SummaryPresenter(UiScreen<DummyEntityOnlyForScreen> view,DummyEntityOnlyForScreen model) {
		super(view, model);
		form=(SummaryForm) view;
		form.newCustBtn.addClickHandler(this);
		newcustomerPopup.getLblOk().addClickHandler(this);
		newcustomerPopup.getLblCancel().addClickHandler(this);
		
		form.btnSearch.addClickHandler(this);
		form.btnClear.addClickHandler(this);
		serTabSelIndex=-1;
		accTabSelIndex=-1;
		salTabSelIndex=-1;
		
		
		loadDefaultData();
		form.serviceTabPanel.selectTab(0);
		
		form.setPresenter(this);
		
//		List<ScreenAuthorization> moduleValidation = null;
//		if(UserConfiguration.getRole()!=null){
//			moduleValidation=UserConfiguration.getRole().getAuthorization();
//		}
//		for(int f=0;f<moduleValidation.size();f++)
//		{
//			Console.log("module access name form presenter="+moduleValidation.get(f).getModule().trim());		
//		}
		
		form.btnDownloadSerLead.addClickHandler(this);
		form.btnDownloadSerQuot.addClickHandler(this);
		form.btnDownloadContract.addClickHandler(this);
		
		form.btnDownloadBill.addClickHandler(this);
		form.btnDownloadInvoice.addClickHandler(this);
		form.btnDownloadPayment.addClickHandler(this);
		
		form.btnNewSerLead.addClickHandler(this);
		form.btnNewSerQuot.addClickHandler(this);
		form.btnNewContract.addClickHandler(this);
		
		form.btnMergeBill.addClickHandler(this);
		
		form.dgServiceLead.addCellClickHandler(this);
		form.dgServiceQuotation.addCellClickHandler(this);
		form.dgContract.addCellClickHandler(this);
		
		form.dgBill.addCellClickHandler(this);
		form.dgInvoice.addCellClickHandler(this);
		form.dgPayment.addCellClickHandler(this);
		
		commSmrtPopup.getBtnOk().addClickHandler(this);
		commSmrtPopup.getBtnCancel().addClickHandler(this);
		commSmrtPopup.getBtnAdd().addClickHandler(this);
		
		billingPopUp.btnClose.addClickHandler(this);
		
		form.btnDownloadSalesLead.addClickHandler(this);
		form.btnDownloadSalesQuot.addClickHandler(this);
		form.btnDownloadSalesOrder.addClickHandler(this);
		form.btnDownloadDeliveryNote.addClickHandler(this);
		
		form.btnNewSalesLead.addClickHandler(this);
		form.btnNewSalesQuot.addClickHandler(this);
		form.btnNewSalesOrder.addClickHandler(this);
		
		form.dgSalesLead.addCellClickHandler(this);
		form.dgSalesQuotation.addCellClickHandler(this);
		form.dgSalesOrder.addCellClickHandler(this);
		form.dgDeliveryNote.addCellClickHandler(this);
		
//		form.personInfo.getId().addSelectionHandler(this);
//		form.personInfo.getName().addSelectionHandler(this);
//		form.personInfo.getPhone().addSelectionHandler(this);
//		form.dateComparator.getFromDate().addValueChangeHandler(this);
//		form.dateComparator.getToDate().addValueChangeHandler(this);
		
		otpPopup.getBtnOne().addClickHandler(this);
		otpPopup.getBtnTwo().addClickHandler(this);
	}
	
	 private void loadDefaultData() {
		 Console.log("loadDefaultData called");
		 form.tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {

			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				// TODO Auto-generated method stub
				int tabNo=event.getSelectedItem();
				
				
				//New code
				String tabName=form.tabPanel.getTabBar().getTabHTML(form.tabPanel.getTabBar().getSelectedTab());
				if(tabName.contains("<div class=")){
					try{
						tabName=tabName.split(">")[1];
						tabName=tabName.split("<")[0];
					}catch(Exception e){
						
					}
				}
				Console.log("TAB name from summary presenter"+tabName);
				//end of new code
				
				if(tabName=="Sales"){ //if(tabNo==0)
					accTabSelIndex=-1;
					serTabSelIndex=-1;
					salTabSelIndex=0;
					form.salesTabPanel.selectTab(0);
				}else if(tabName=="Service"){ //if(tabNo==1)
					accTabSelIndex=-1;
					serTabSelIndex=0;
					salTabSelIndex=-1;
					form.serviceTabPanel.selectTab(0);
				}else if(tabName=="Accounts"){ //if(tabNo==2)
					serTabSelIndex=-1;
					accTabSelIndex=0;
					salTabSelIndex=-1;
					form.accountsTabPanel.selectTab(0);
				}
			}
		});
		 Console.log("tabnames" +form.tabNames.toString());
		 Console.log("rollNames" +form.rollNames.toString());
		
		 form.salesTabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
				@Override
				public void onSelection(SelectionEvent<Integer> event) {
					
					//Ashwini Patil
					String tabName=form.salesTabPanel.getTabBar().getTabHTML(form.salesTabPanel.getTabBar().getSelectedTab());
					if(tabName.contains("<div class=")){
						try{
							tabName=tabName.split(">")[1];
							tabName=tabName.split("<")[0];
						}catch(Exception e){
							
						}
					}
					
					accTabSelIndex=-1;
					serTabSelIndex=-1;
					salTabSelIndex = event.getSelectedItem();
					Console.log("in sales tab event"+tabName+"index"+salTabSelIndex);
					
					 if(form.tabNames.contains("Sales")&&form.rollNames.contains("Sales") ||UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)){
						 Console.log("in if(form.salesFlag)" );
						 if(salTabSelIndex == 0){
								if(form.globalSalesLeadList==null||form.globalSalesLeadList.size()==0){
									getData("Sales Lead");
								}else{
									loadDataForSalesLead(form.globalSalesLeadList);
								}
							
						}else if(salTabSelIndex == 1){
								if(form.globalSalesQuotList==null||form.globalSalesQuotList.size()==0){
									getData("Sales Quotation");
								}else{
									loadDataForSalesQuot(form.globalSalesQuotList);
								}
							
						}else if(salTabSelIndex == 2){
								if(form.globalSalesOrderList==null||form.globalSalesOrderList.size()==0){
									getData("Sales Order");
								}else{
									loadDataForSalesOrder(form.globalSalesOrderList);
								}

						}else if(salTabSelIndex == 3){
								if(form.globalDeliveryNoteList==null||form.globalDeliveryNoteList.size()==0){
									getData("Delivery Note");
								}else{
									loadDataForDeliveryNote(form.globalDeliveryNoteList);
								}													
						}
					
				}else
					 form.showDialogMessage("You are not authorized to use Sales module");
				}
				
			});
		 
		 	 
		 form.serviceTabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				accTabSelIndex=-1;
				salTabSelIndex=-1;
				serTabSelIndex = event.getSelectedItem();
				if(form.tabNames.contains("Service")&&form.rollNames.contains("Service")||UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)){
					if(serTabSelIndex == 0){
						if(form.globalSerLeadList==null||form.globalSerLeadList.size()==0){
							getData("Service Lead");
						}else{
							loadDataForSerLead(form.globalSerLeadList);
						}
					}else if(serTabSelIndex == 1){
						if(form.globalSerQuotList==null||form.globalSerQuotList.size()==0){
							getData("Service Quotation");
						}else{
							loadDataForSerQuot(form.globalSerQuotList);
						}
					}else if(serTabSelIndex == 2){
						if(form.globalContractList==null||form.globalContractList.size()==0){
							getData("Contract");
						}else{
							loadDataForContract(form.globalContractList);
						}
					}
				}else
					form.showDialogMessage("You are not authorized to use Service module");
				
			}
		});
		
			
		 form.accountsTabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
				@Override
				public void onSelection(SelectionEvent<Integer> event) {
					serTabSelIndex=-1;
					salTabSelIndex=-1;
					accTabSelIndex = event.getSelectedItem();
					if(form.tabNames.contains("Accounts")&&form.rollNames.contains("Accounts")||UserConfiguration.getRole().getRoleName().trim().equalsIgnoreCase(AppConstants.ADMINUSER)) {
						if(accTabSelIndex == 0){
							if(form.globalBillingDocList==null||form.globalBillingDocList.size()==0){
								getData("Bill");
							}else{
								form.setDataToBillListGrid(form.globalBillingDocList);
							}
						}else if(accTabSelIndex == 1){
							if(form.globalInvoiceList==null||form.globalInvoiceList.size()==0){
								getData("Invoice");
							}else{
								form.setDataToInvoiceListGrid(form.globalInvoiceList);
							}
						}else if(accTabSelIndex == 2){
							if(form.tabNames.contains("Payment Details")||form.tabNames.contains("Payment List")||form.tabNames.contains("PAYMENTDETAILS")||form.tabNames.contains("PAYMENTLIST")) {
								if(form.globalPaymentList==null||form.globalPaymentList.size()==0){
									getData("Payment");
								}else{
									form.setDataToPaymentListGrid(form.globalPaymentList);
								}							
							}else {
								form.showDialogMessage("You are not authorized to view this screen!");
							}
						}
					}else
						form.showDialogMessage("You are not authorized to use Accounts module");
				
				}
			});		
		 
		 Console.log("SER TAB - "+serTabSelIndex+" ACC TAB - "+accTabSelIndex);
	}

	protected void getData(final String tabName) {
		Console.log("In getData Selected Tab :- "+tabName);
		
		form.showWaitSymbol();
		service.getSearchResult(form.getQuerry(tabName), new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				Console.log("Failed...!!!");
			}

			public void onSuccess(ArrayList<SuperModel> result1) {
				form.hideWaitSymbol();
				Console.log("Success...!!!");
				if(tabName.equals("Service Lead")){
					form.globalSerLeadList=new ArrayList<Lead>();
					
					form.dgServiceLead.setData(new ListGridRecord[] {});
					form.dgServiceLead.redraw();
					Console.log("Service lead size "+form.globalSerLeadList.size());
					
				}else if(tabName.equals("Service Quotation")){
					form.globalSerQuotList=new ArrayList<Quotation>();
					
					form.dgServiceQuotation.setData(new ListGridRecord[] {});
					form.dgServiceQuotation.redraw();
					
				}else if(tabName.equals("Contract")){
					form.globalContractList=new ArrayList<Contract>();
					
					form.dgContract.setData(new ListGridRecord[] {});
					form.dgContract.redraw();
					
				}else if(tabName.equals("Bill")){
					form.globalBillingDocList=new ArrayList<BillingDocument>();
					
					form.dgBill.setData(new ListGridRecord[] {});
					form.dgBill.redraw();
					
				}else if(tabName.equals("Invoice")){
					form.globalInvoiceList=new ArrayList<Invoice>();
					
					form.dgInvoice.setData(new ListGridRecord[] {});
					form.dgInvoice.redraw();
					
				}else if(tabName.equals("Payment")){
					form.globalPaymentList=new ArrayList<CustomerPayment>();
					
					form.dgPayment.setData(new ListGridRecord[] {});
					form.dgPayment.redraw();
				}else if(tabName.equals("Sales Lead")){
					form.globalSalesLeadList=new ArrayList<Lead>();
					
					form.dgSalesLead.setData(new ListGridRecord[] {});
					form.dgSalesLead.redraw();
				}else if(tabName.equals("Sales Quotation")){
					form.globalSalesQuotList=new ArrayList<SalesQuotation>();
					
					form.dgSalesQuotation.setData(new ListGridRecord[] {});
					form.dgSalesQuotation.redraw();
				}else if(tabName.equals("Sales Order")){
					
					form.globalSalesOrderList=new ArrayList<SalesOrder>();
					
					form.dgSalesOrder.setData(new ListGridRecord[] {});
					form.dgSalesOrder.redraw();
				}else if(tabName.equals("Delivery Note")){
					form.globalDeliveryNoteList=new ArrayList<DeliveryNote>();
					
					form.dgDeliveryNote.setData(new ListGridRecord[] {});
					form.dgDeliveryNote.redraw();
				}
				
				if(result1!=null&&result1.size()!=0){
					ArrayList<SuperModel> result=new ArrayList<SuperModel>();
					if(form.isCustomerAndDateSelected()){
						Console.log("Customer and Date selected!!!");
						result.addAll(form.getFilteredData(result1, tabName));
					}else if(form.isSalesPersonAndDateSelected()){
						Console.log("Sales Person and Date selected!!!");
						result.addAll(form.getFilteredData(result1, tabName));
					}else{
						Console.log("Other than date, customer and salesperson selected!!!");
						result.addAll(result1);
					}
					
					
					int totalRecords=result.size();
					double totalAmount=0;
					
					Console.log("Success...!!! "+result.size());
					
					if(result.size()==0){
						return;
					}
					if(tabName.equals("Service Lead")){
						
						for(SuperModel model:result){
							Lead obj=(Lead) model;
							if(obj.getLeadScreenType()!=null) {
								if(obj.getLeadScreenType().equals("Service")) { //26-09-2022 to remove sales lead from list
									form.globalSerLeadList.add(obj);
									totalAmount=totalAmount+obj.getNetpayable();
								}
							}
//							else { 
//								form.globalSerLeadList.add(obj);
//								totalAmount=totalAmount+obj.getNetpayable();
//							}
						}
						
						if(form.lblSerLead==null&&form.lblSerLead1==null){
							form.lblSerLead=new InlineLabel("Total Lead : ");
							form.lblSerLead1=new InlineLabel("Total Amount : ");
						}
						Console.log(form.lblSerLead.getText()+" "+form.lblSerLead1.getText());
						Console.log(totalAmount+" aaa "+totalRecords);
						
						form.lblSerLead.setVisible(true);
						form.lblSerLead1.setVisible(true);
						
						form.lblSerLead.setText("Total Lead : "+totalRecords);
						form.lblSerLead1.setText("Total Amount : "+Math.round(totalAmount));
						
						Console.log(form.lblSerLead.getText()+" "+form.lblSerLead1.getText());
						
						loadDataForSerLead(form.globalSerLeadList);
						
					}else if(tabName.equals("Service Quotation")){
						for(SuperModel model:result){
							Quotation obj=(Quotation) model;
							form.globalSerQuotList.add(obj);
							totalAmount=totalAmount+obj.getNetpayable();
						}
						
						if(form.lblSerQuot==null&&form.lblSerQuot1==null){
							form.lblSerQuot=new InlineLabel("Total Quotation : ");
							form.lblSerQuot1=new InlineLabel("Total Amount : ");
						}
						Console.log(form.lblSerQuot.getText()+" "+form.lblSerQuot1.getText());
						Console.log(totalAmount+" bbb "+totalRecords);
						
						form.lblSerQuot.setVisible(true);
						form.lblSerQuot1.setVisible(true);
						
						form.lblSerQuot.setText("Total Quotation : "+totalRecords);
						form.lblSerQuot1.setText("Total Amount : "+Math.round(totalAmount));
						
						
						Console.log(form.lblSerQuot.getText()+" "+form.lblSerQuot1.getText());
						
						loadDataForSerQuot(form.globalSerQuotList);
					}else if(tabName.equals("Contract")){
						
						for(SuperModel model:result){
							Contract obj=(Contract) model;
							form.globalContractList.add(obj);
							totalAmount=totalAmount+obj.getNetpayable();
						}
							
						if(form.lblContract==null&&form.lblContract1==null){
							form.lblContract=new InlineLabel("Total Contract : ");
							form.lblContract1=new InlineLabel("Total Amount : ");
						}
						
						Console.log(form.lblContract.getText()+" "+form.lblContract1.getText());
						Console.log(totalAmount+" ccc "+totalRecords);
						
						form.lblContract.setVisible(true);
						form.lblContract1.setVisible(true);
						
						form.lblContract.setText("Total Contract : "+totalRecords);
						form.lblContract1.setText("Total Amount : "+Math.round(totalAmount));
						
						Console.log(form.lblContract.getText()+" "+form.lblContract1.getText());
						
						loadDataForContract(form.globalContractList);
					}else if(tabName.equals("Bill")){
						for(SuperModel model:result){
							BillingDocument obj=(BillingDocument) model;
							form.globalBillingDocList.add(obj);
							totalAmount=totalAmount+obj.getGrandTotalAmount();
						}
						form.setDataToBillListGrid(form.globalBillingDocList);
					}else if(tabName.equals("Invoice")){
						for(SuperModel model:result){
							Invoice obj=(Invoice) model;
							if(obj.getTypeOfOrder().equals("Service Order")) { //26-09-2022 to remove vendor invoice details from list
								form.globalInvoiceList.add(obj);
								totalAmount=totalAmount+obj.getNetPayable();
							}
						}
						form.setDataToInvoiceListGrid(form.globalInvoiceList);
					}else if(tabName.equals("Payment")){
						for(SuperModel model:result){
							CustomerPayment obj=(CustomerPayment) model;
							if(obj.getTypeOfOrder().equals("Service Order")) { //26-09-2022 to remove vendor payment details from list
								form.globalPaymentList.add(obj);
								totalAmount=totalAmount+obj.getPaymentReceived();
							}
						}
						Console.log("form.globalPaymentList size="+form.globalPaymentList.size());
						form.setDataToPaymentListGrid(form.globalPaymentList);
					}else if(tabName.equals("Sales Lead")){
						for(SuperModel model:result){
							Lead obj=(Lead) model;
							if(obj.getLeadScreenType()!=null) {
								if(obj.getLeadScreenType().equals("Sales")) { //26-09-2022 to remove service lead from list
									form.globalSalesLeadList.add(obj);
									totalAmount=totalAmount+obj.getNetpayable();
								}
							}
//							else { 
//							form.globalSerLeadList.add(obj);
//							totalAmount=totalAmount+obj.getNetpayable();
//						}
						}
						form.setDataToSalesLeadLG(form.globalSalesLeadList);
					}else if(tabName.equals("Sales Quotation")){
						for(SuperModel model:result){
							SalesQuotation obj=(SalesQuotation) model;
							form.globalSalesQuotList.add(obj);
							totalAmount=totalAmount+obj.getNetpayable();
						}
						Console.log("form.globalSalesQuotList size="+form.globalSalesQuotList.size());
						form.setDataToSalesQuotLG(form.globalSalesQuotList);
					}else if(tabName.equals("Sales Order")){
						
						Console.log("In getdata before adding data to globalSalesOrderList. result size is"+result.size());
						for(SuperModel model:result){
							SalesOrder obj=(SalesOrder) model;
							form.globalSalesOrderList.add(obj);
							totalAmount=totalAmount+obj.getNetpayable();
						}
						form.setDataToSalesOrderListGrid(form.globalSalesOrderList);
					}else if(tabName.equals("Delivery Note")){
						for(SuperModel model:result){
							DeliveryNote obj=(DeliveryNote) model;
							form.globalDeliveryNoteList.add(obj);
							totalAmount=totalAmount+obj.getNetpayable();
						}
						form.setDataToDeliveryNoteListGrid(form.globalDeliveryNoteList);
					}
					
				}
			}
		});
	}

	protected void loadDataForContract(List<Contract> serContractList) {
		form.setDataToContractListGrid(serContractList);
	}

	protected void loadDataForSerQuot(List<Quotation> serQuotList) {
		form.setDataToSerQuotLG(serQuotList);
	}

	protected void loadDataForSerLead(List<Lead> serLeadList) {
		form.setDataToSerLeadLG(serLeadList);
	}
	
	protected void loadDataForSalesLead(List<Lead> serLeadList) {
		form.setDataToSalesLeadLG(serLeadList);
	}
	
	protected void loadDataForSalesQuot(List<SalesQuotation> serLeadList) {
		form.setDataToSalesQuotLG(serLeadList);
	}
	
	protected void loadDataForSalesOrder(List<SalesOrder> serLeadList) {
		form.setDataToSalesOrderListGrid(serLeadList);
	}
	
	protected void loadDataForDeliveryNote(List<DeliveryNote> serLeadList) {
		form.setDataToDeliveryNoteListGrid(serLeadList);
	}

	public static SummaryForm initalize(){
		LoginPresenter.currentModule="Home!";
		LoginPresenter.currentDocumentName="Home!";//Summary Screen
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Home!", Screen.SUMMARYSCREEN);//Summary Screen
		SummaryForm form = new SummaryForm();
		SummaryPresenter presenter = new SummaryPresenter(form,new DummyEntityOnlyForScreen());
		AppMemory.getAppMemory().stickPnel(form);
		
		return form;
	}
	

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
	}

	@Override
	public void reactOnPrint() {
	}

	@Override
	public void reactOnEmail() {
	}

	@Override
	protected void makeNewModel() {
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		Console.log("Summary click event!!");
		
		try{
			reactOnPopupNavigation(event);
		}catch(Exception e){
			Console.log("Summary Presenter on click error"+e.toString());
		}
		
		if(event.getSource()==form.newCustBtn){
			newcustomerPopup.showPopUp();
		}
		if(event.getSource()==newcustomerPopup.getLblOk()){
			if(validateCustomer()){
				saveNewCustomer();
			}
		}
		if(event.getSource()==newcustomerPopup.getLblCancel()){
			newcustomerPopup.hidePopUp();
		}
		
		if(event.getSource()==form.btnSearch){
			form.defaultLoadingFlag=false;
			form.globalSerLeadList=new ArrayList<Lead>();
			form.globalSerQuotList=new ArrayList<Quotation>();
			form.globalContractList=new ArrayList<Contract>();
			
			form.globalBillingDocList=new ArrayList<BillingDocument>();
			form.globalInvoiceList=new ArrayList<Invoice>();
			form.globalPaymentList=new ArrayList<CustomerPayment>();
			
			form.globalSalesLeadList=new ArrayList<Lead>();
			form.globalSalesQuotList=new ArrayList<SalesQuotation>();
			form.globalSalesOrderList=new ArrayList<SalesOrder>();
			form.globalDeliveryNoteList=new ArrayList<DeliveryNote>();
			
			Console.log("Tab INDEX :: SER "+serTabSelIndex+" /// ACC "+accTabSelIndex+" /// SAL "+salTabSelIndex);
			if(serTabSelIndex==0){
				getData("Service Lead");
			}else if(serTabSelIndex==1){
				getData("Service Quotation");
			}else if(serTabSelIndex==2){
				getData("Contract");
			}else if(accTabSelIndex==0){
				getData("Bill");
			}else if(accTabSelIndex==1){
				getData("Invoice");
			}else if(accTabSelIndex==2){
				getData("Payment");
			}else if(salTabSelIndex==0){
				getData("Sales Lead");
			}else if(salTabSelIndex==1){
				getData("Sales Quotation");
			}else if(salTabSelIndex==2){
				getData("Sales Order");
			}else if(salTabSelIndex==3){
				getData("Delivery Note");
			}
		}
		if(event.getSource()==form.btnClear){
			
//			form.personInfo.getId().setValue("");
//			form.personInfo.getName().setValue("");
//			form.personInfo.getTbpocName().setValue("");
//			form.personInfo.getPhone().setValue("");
//			form.personInfo.setPersonInfoHashMap(null);
			form.dateComparator.getFromDate().setValue(null);
			form.dateComparator.getToDate().setValue(null);
			form.lbStaus.setSelectedIndex(0);
			form.olbBranch.setSelectedIndex(0);
			form.olbSalesPerson.setSelectedIndex(0);
		}
		
		if(event.getSource()==form.btnDownloadSerLead){
			reactOnDownload("Service Lead");
		}
		if(event.getSource()==form.btnDownloadSerQuot){
			reactOnDownload("Service Quotation");	
		}
		if(event.getSource()==form.btnDownloadContract){
			reactOnDownload("Contract");
		}
		
		if(event.getSource()==form.btnDownloadBill){
			reactOnDownload("Bill");
		}
		if(event.getSource()==form.btnDownloadInvoice){
			reactOnDownload("Invoice");
		}
		if(event.getSource()==form.btnDownloadPayment){
			reactOnDownload("Payment");
		}
		
		if(event.getSource()==form.btnNewSerLead){
			reactOnNew("Service Lead");
		}
		if(event.getSource()==form.btnNewSerQuot){
			reactOnNew("Service Quotation");
		}
		if(event.getSource()==form.btnNewContract){
			reactOnNew("Contract");
		}
		
		if(event.getSource()==form.btnNewSalesLead){
			reactOnNew("Sales Lead");
		}
		if(event.getSource()==form.btnNewSalesQuot){
			reactOnNew("Sales Quotation");
		}
		if(event.getSource()==form.btnNewSalesOrder){
			reactOnNew("Sales Order");
		}
		if(event.getSource()==form.btnNewDeliveryNote){
			reactOnNew("Delivery Note");
		}
		
		if(event.getSource()==form.btnDownloadSalesLead){
			reactOnDownload("Sales Lead");
		}
		if(event.getSource()==form.btnDownloadSalesQuot){
			reactOnDownload("Sales Quotation");	
		}
		if(event.getSource()==form.btnDownloadSalesOrder){
			reactOnDownload("Sales Order");
		}
		if(event.getSource()==form.btnDownloadDeliveryNote){
			reactOnDownload("Delivery Note");
		}
		
		if (event.getSource() == form.btnMergeBill) {
			Console.log("Merge Button clicked!!!");
			genralInvoicePopup=new GeneralViewDocumentPopup();
			salesLeadPopup=new GeneralViewDocumentPopup();
			billingPopUp=new GeneralViewDocumentPopup();
			billingPopUp.btnClose.addClickHandler(this);
			
			if(form.dgBill.getSelection()!=null&&form.dgBill.getSelection().length!=0){
				if(validateAllSelectedBillingDocument()){
					BillingDocument billDoc=initializeToNewBillingDetails();
					billingPopUp.setBilling("Multiple Bill", billDoc,getAllSelectedRecords());
				}
			}else{
				SC.say("No Billing Document selected. Please select atleast one Billing Document!");
			}
		}
		
		
		if (event.getSource() == commSmrtPopup.getBtnOk()) {
			
			List<InteractionType> list = commSmrtPopup.getCommunicationLogTable().getDataprovider().getList();
			ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
			interactionlist.addAll(list);

			boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);

			if (checkNewInteraction == false) {
				SC.say("Please add new interaction details");
			} else {
				final Date followUpDate = interactionlist.get(interactionlist.size() - 1).getInteractionDueDate();
//				final String status=interactionlist.get(interactionlist.size() - 1).getInteractionStatus();
				final String salesPerson=	interactionlist.get(interactionlist.size() - 1).getInteractionPersonResponsible();	
				communicationService.saveCommunicationLog(interactionlist,new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						SC.say("Unexpected Error");
					}
					@Override
					public void onSuccess(Void result) {
						commSmrtPopup.hidePopup();
						SC.say("Data Save Successfully");
						
						
						Console.log("Follow Up Save : Sales "+salTabSelIndex+" || Service "+serTabSelIndex+" || Account "+accTabSelIndex);
						
						if(serTabSelIndex==0&&leadObj!=null){
							leadObj.setFollowUpDate(followUpDate);
//							leadObj.setStatus(status);
							leadObj.setEmployee(salesPerson);
							
//							selectedRecord.setAttribute("status", status);
							selectedRecord.setAttribute("salesPerson", salesPerson);
							selectedRecord.setAttribute("followupDate", AppUtility.parseDate(followUpDate));
							
							saveLead(leadObj);
							
							form.dgServiceLead.getDataSource().updateData(selectedRecord);
							form.dgServiceLead.redraw();
						}else if(serTabSelIndex==1&&quotationObj!=null){
							quotationObj.setFollowUpDate(followUpDate);
//							quotationObj.setStatus(status);
							quotationObj.setEmployee(salesPerson);
							
//							selectedRecord.setAttribute("status", status);
							selectedRecord.setAttribute("salesPerson", salesPerson);
							selectedRecord.setAttribute("followupDate", AppUtility.parseDate(followUpDate));
							saveQuotation(quotationObj);
							
							form.dgServiceQuotation.getDataSource().updateData(selectedRecord);
							form.dgServiceQuotation.redraw();
						}else if(accTabSelIndex==2&&custPayObj!=null){
							custPayObj.setFollowUpDate(followUpDate);
//							quotationObj.setStatus(status);
							custPayObj.setEmployee(salesPerson);
							
//							selectedRecord.setAttribute("status", status);
							selectedRecord.setAttribute("followupDate", AppUtility.parseDate(followUpDate));
							savePayment(custPayObj);
							
							form.dgPayment.getDataSource().updateData(selectedRecord);
							form.dgPayment.redraw();
						}else if(salTabSelIndex==0&&salesLeadObj!=null){
							salesLeadObj.setFollowUpDate(followUpDate);
//							salesLeadObj.setStatus(status);
							salesLeadObj.setEmployee(salesPerson);
							
//							selectedRecord.setAttribute("status", status);
							selectedRecord.setAttribute("salesPerson", salesPerson);
							selectedRecord.setAttribute("followupDate", AppUtility.parseDate(followUpDate));
							
							saveLead(salesLeadObj);
							
							form.dgSalesLead.getDataSource().updateData(selectedRecord);
							form.dgSalesLead.redraw();
						}else if(salTabSelIndex==1&&salesQuotObj!=null){
							Console.log("Sales Quotation : Followup");
							salesQuotObj.setFollowUpDate(followUpDate);
							salesQuotObj.setEmployee(salesPerson);
							selectedRecord.setAttribute("salesPerson", salesPerson);
							selectedRecord.setAttribute("followupDate", AppUtility.parseDate(followUpDate));
							saveSalesQuotation(salesQuotObj);
							
							form.dgSalesQuotation.getDataSource().updateData(selectedRecord);
							form.dgSalesQuotation.redraw();
						}
					}
					
				});
			}

		}
		if (event.getSource() == commSmrtPopup.getBtnCancel()) {
			commSmrtPopup.hidePopup();
			
			leadObj=null;
			quotationObj=null;
			contractObj=null;
			
			salesLeadObj=null;
			salesQuotObj=null;
			salesOrderObj=null;
			deliveryNoteObj=null;
			
			selectedRecord=null;
			position=0;
		}
		if (event.getSource() == commSmrtPopup.getBtnAdd()) {
			
			String remark = commSmrtPopup.getRemark().getValue();
			Date dueDate = commSmrtPopup.getDueDate().getValue();
			String interactiongGroup = null;
			
			if (commSmrtPopup.getOblinteractionGroup().getSelectedIndex() != 0){
				interactiongGroup = commSmrtPopup.getOblinteractionGroup().getValue(commSmrtPopup.getOblinteractionGroup().getSelectedIndex());
			}
			boolean validationFlag = AppUtility.validateCommunicationlogSmartGwt(remark, dueDate);

			List<InteractionType> list = commSmrtPopup.getCommunicationLogTable().getDataprovider().getList();
		
			Comparator<InteractionType> comp = new Comparator<InteractionType>() {
				@Override
				public int compare(InteractionType e1, InteractionType e2) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return -1;
					} else {
						return 1;
					}
				}
			};
			Collections.sort(list, comp);

			if (validationFlag) {
				InteractionType communicationLog = null;
				if(serTabSelIndex==0&&leadObj!=null){
					communicationLog = AppUtility.getCommunicationLog(AppConstants.SERVICEMODULE,
							AppConstants.LEAD, leadObj.getCount(),leadObj.getEmployee(), remark, dueDate,
							leadObj.getPersonInfo(), null, interactiongGroup,leadObj.getBranch(),"");
				}else if(serTabSelIndex==1&&quotationObj!=null){
					communicationLog = AppUtility.getCommunicationLog(AppConstants.SERVICEMODULE,
							AppConstants.QUOTATION, quotationObj.getCount(),quotationObj.getEmployee(), remark, dueDate,
							quotationObj.getCinfo(), null, interactiongGroup,quotationObj.getBranch(),"");
				}else if(accTabSelIndex==2&&custPayObj!=null){
					communicationLog = AppUtility.getCommunicationLog(AppConstants.ACCOUNTMODULE,
							AppConstants.PAYMENTDETAILS, custPayObj.getCount(),custPayObj.getEmployee(), remark, dueDate,
							custPayObj.getPersonInfo(), null, interactiongGroup,custPayObj.getBranch(),"");
				}else if(salTabSelIndex==0&&salesLeadObj!=null){
					communicationLog = AppUtility.getCommunicationLog(AppConstants.SALESMODULE,
							AppConstants.LEAD, salesLeadObj.getCount(),salesLeadObj.getEmployee(), remark, dueDate,
							salesLeadObj.getPersonInfo(), null, interactiongGroup,salesLeadObj.getBranch(),"");
				}else if(salTabSelIndex==1&&salesQuotObj!=null){
					communicationLog = AppUtility.getCommunicationLog(AppConstants.SALESMODULE,
							AppConstants.QUOTATION, salesQuotObj.getCount(),salesQuotObj.getEmployee(), remark, dueDate,
							salesQuotObj.getCinfo(), null, interactiongGroup,salesQuotObj.getBranch(),"");
				}
				
				list.add(communicationLog);
				commSmrtPopup.getCommunicationLogTable().getDataprovider().setList(list);
				commSmrtPopup.getRemark().setValue("");
				commSmrtPopup.getDueDate().setValue(null);
				commSmrtPopup.getOblinteractionGroup().setSelectedIndex(0);
			}
//			}
		}
		if(event.getSource()==genralInvoicePopup.btnClose){
			try{
			Console.log("On CLICK OF CLOSE...");
			if(genralInvoicePopup!=null){
				Console.log("in if(genralInvoicePopup!=null)");
				if(genralInvoicePopup.getPresenter()!=null){
					
					if(genralInvoicePopup.getPresenter().getModel()==null||genralInvoicePopup.getPresenter().getModel().getCount()==0){
						Console.log("List Form");
						viewedScreenName="";
						AppMemory.getAppMemory().currentScreen = Screen.SUMMARYSCREEN;
						LoginPresenter.currentModule="Home!";
						LoginPresenter.currentDocumentName="Home!";//Summary Screen
						AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Home!", Screen.SUMMARYSCREEN);//7-10-2022						
						form.setToViewState();
						form.toggleAppHeaderBarMenu();//7-10-2022
						Console.log("end of on close in list form");
						return;
					}
					Console.log("presenter="+genralInvoicePopup.getPresenter().getModel().toString());
					
					if(genralInvoicePopup.getPresenter().getModel() instanceof Lead){
						Console.log("On CLICK OF save LEAD...");
						
						if(leadObj==null){
							Console.log("NEW LEAD ...");
							leadObj=(Lead) genralInvoicePopup.getPresenter().getModel();
							if(leadObj!=null&&leadObj.getCount()!=0){
								addDataToGridAndList(leadObj);
							}
						}else{
							Console.log("EXISTING LEAD ...");
							updateDataToGridAndList(leadObj);
						}
						leadObj=null;
					}
					
						if(genralInvoicePopup.getPresenter().getModel() instanceof Contract){
							Console.log("On CLICK OF save CONTRACT...");
							
							if(contractObj==null){
								Console.log("NEW CONTRACT ...");
								contractObj=(Contract) genralInvoicePopup.getPresenter().getModel();
								if(contractObj!=null&&contractObj.getCount()!=0){
									addDataToGridAndList(contractObj);
								}								
							}else{
								Console.log("EXISTING CONTRACT ...");
								updateDataToGridAndList(contractObj);
							}															
							contractObj=null;							
							selectedRecord=null;
							position=0;
							viewedScreenName="";
							LoginPresenter.currentModule="Home!";
							LoginPresenter.currentDocumentName="Home!";//Summary Screen
							AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Home!", Screen.SUMMARYSCREEN);							
							AppMemory.getAppMemory().currentScreen = Screen.SUMMARYSCREEN;
							form.setToViewState();
							form.toggleAppHeaderBarMenu();
							return; 
						}
										
					if(genralInvoicePopup.getPresenter().getModel() instanceof Quotation){
						Console.log("On CLICK OF save QUOTATION...");
						if(quotationObj==null){
							Console.log("NEW QUOTATION ...");
							quotationObj=(Quotation) genralInvoicePopup.getPresenter().getModel();
							if(quotationObj!=null&&quotationObj.getCount()!=0){
								addDataToGridAndList(quotationObj);
							}
						}else{
							Console.log("EXISTING QUOTATION ...");
							updateDataToGridAndList(quotationObj);
						}
						quotationObj=null;
					}
					if(genralInvoicePopup.getPresenter().getModel() instanceof BillingDocument){
						Console.log("On CLICK OF save BillingDocument...");
						if(billObj==null){
							Console.log("NEW BillingDocument ...");
							billObj=(BillingDocument) genralInvoicePopup.getPresenter().getModel();
							if(billObj!=null&&billObj.getCount()!=0){
								addDataToGridAndList(billObj);
							}
						}else{
							Console.log("EXISTING BillingDocument ...");
							updateDataToGridAndList(billObj);
						}
						billObj=null;
					}
					if(genralInvoicePopup.getPresenter().getModel() instanceof Invoice){
						Console.log("On CLICK OF save Invoice...");
						if(invoiceObj==null){
							Console.log("NEW Invoice ...");
							invoiceObj=(Invoice) genralInvoicePopup.getPresenter().getModel();
							if(invoiceObj!=null&&invoiceObj.getCount()!=0){
								addDataToGridAndList(invoiceObj);
							}
						}else{
							Console.log("EXISTING Invoice ...");
							updateDataToGridAndList(invoiceObj);
						}
						invoiceObj=null;
					}
					if(genralInvoicePopup.getPresenter().getModel() instanceof CustomerPayment){
						Console.log("On CLICK OF save CustomerPayment...");
						if(custPayObj==null){
							Console.log("NEW CustomerPayment ...");
							custPayObj=(CustomerPayment) genralInvoicePopup.getPresenter().getModel();
							if(custPayObj!=null&&custPayObj.getCount()!=0){
								addDataToGridAndList(custPayObj);
							}
						}else{
							Console.log("EXISTING CustomerPayment ...");
							updateDataToGridAndList(custPayObj);
						}
						custPayObj=null;
					}
					
					if(genralInvoicePopup.getPresenter().getModel() instanceof SalesOrder){
						Console.log("On CLICK OF save salesOrderObj...");
						if(salesOrderObj==null){
							Console.log("NEW salesOrderObj ...");
							salesOrderObj=(SalesOrder) genralInvoicePopup.getPresenter().getModel();
							if(salesOrderObj!=null&&salesOrderObj.getCount()!=0){
								addDataToGridAndList(salesOrderObj);
							}
						}else{
							Console.log("EXISTING salesOrderObj ...");
							updateDataToGridAndList(salesOrderObj);
						}
						salesOrderObj=null;
						viewedScreenName="";
						AppMemory.getAppMemory().currentScreen = Screen.SUMMARYSCREEN;
						return; 
					}
					
					if(genralInvoicePopup.getPresenter().getModel() instanceof SalesQuotation){
						Console.log("On CLICK OF save SalesQuotation...");
						if(salesQuotObj==null){
							Console.log("NEW SalesQuotation ...");
							salesQuotObj=(SalesQuotation) genralInvoicePopup.getPresenter().getModel();
							if(salesQuotObj!=null&&salesQuotObj.getCount()!=0){
								addDataToGridAndList(salesQuotObj);
							}
						}else{
							Console.log("EXISTING SalesQuotation ...");
							updateDataToGridAndList(salesQuotObj);
						}
						salesQuotObj=null;
					}
					
				}
			}
			
			viewedScreenName="";
			AppMemory.getAppMemory().currentScreen = Screen.SUMMARYSCREEN;
			Console.log("end of on close");
			}
			catch(Exception e){
				e.printStackTrace();
				Console.log(e.getMessage());
			}
		}
		
		if(event.getSource()==billingPopUp.btnClose){
			Console.log("On CLICK OF Billing CLOSE...");
			if(billingPopUp!=null){
				if(billingPopUp.getPresenter()!=null){
					if(billingPopUp.getPresenter().getModel() instanceof BillingDocument){
						
						BillingDocument billDoc=(BillingDocument) billingPopUp.getPresenter().getModel();
						
						if(billDoc!=null){
							if(billDoc.getStatus().equals(BillingDocument.BILLINGINVOICED)){
								for(ListGridRecord record:form.dgBill.getSelection()){
									record.setAttribute("status",BillingDocument.BILLINGINVOICED);
									record.setAttribute("invoiceId", billDoc.getInvoiceCount());
									form.dgBill.getDataSource().updateData(record);
									
									for(BillingDocument bill:form.globalBillingDocList){
										if(bill.getCount()==Integer.parseInt(record.getAttribute("billingId").trim())){
											bill.setStatus(BillingDocument.BILLINGINVOICED);
											bill.setInvoiceCount(billDoc.getInvoiceCount());
										}
									}
								}
								form.dgBill.redraw();
								reteriveDataFromDB(billDoc.getInvoiceCount(), "Invoice", true);
							}
						}
					}
				}
			}
			viewedScreenName="";
			AppMemory.getAppMemory().currentScreen = Screen.SUMMARYSCREEN;
		}
		
		if(event.getSource()==salesLeadPopup.btnClose){
			Console.log("On CLICK OF salesLeadPopup CLOSE...");
			reactOnCloseBtnOfSalesLead();
			viewedScreenName="";
			AppMemory.getAppMemory().currentScreen = Screen.SUMMARYSCREEN;
		}
		
		selectedRecord=null;
		position=0;
		
		if(event.getSource() == otpPopup.getBtnOne()){
			Console.log("otp popup ok button clicked");
			Console.log("otp value="+otpPopup.getOtp().getValue());
			if(otpPopup.getOtp().getValue()!=null){
				if(generatedOTP==otpPopup.getOtp().getValue()){
					runRegularContractDownloadProgram();
					otppopupPanel.hide();
				}else {
					form.showDialogMessage("Invalid OTP");	
					smsService.checkConfigAndSendWrongOTPAttemptMessage(model.getCompanyId(), LoginPresenter.loggedInUser, generatedOTP, "Contract", new  AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.showDialogMessage("Failed");
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							Console.log("InvalidOTPDownloadNotification result="+result);
						}
					} );
				
				}
			}else
				form.showDialogMessage("Enter OTP");
		}
		if(event.getSource() == otpPopup.getBtnTwo()){
			otppopupPanel.hide();
		}
	}

	

	private void addDataToGridAndList(SuperModel model) {
		form.toggleAppHeaderBarMenu();
		if(model instanceof Lead){
			if(form.globalSerLeadList==null){
				form.globalSerLeadList=new ArrayList<Lead>();
			}
			form.globalSerLeadList.add(leadObj);
			form.dgServiceLead.getDataSource().addData(form.addDateToLeadDG(leadObj));
			form.dgServiceLead.redraw();
		}else if(model instanceof Contract){
			if(form.globalContractList==null){
				form.globalContractList=new ArrayList<Contract>();
			}
			form.globalContractList.add(contractObj);
			form.dgContract.getDataSource().addData(form.addDateToContractDG(contractObj));
			form.dgContract.redraw();
		}
		else if(model instanceof Quotation){
			if(form.globalSerQuotList==null){
				form.globalSerQuotList=new ArrayList<Quotation>();
			}
			form.globalSerQuotList.add(quotationObj);
			form.dgServiceQuotation.getDataSource().addData(form.addDateToSerQuotDG(quotationObj));
			form.dgServiceQuotation.redraw();
		}else if(model instanceof BillingDocument){
			if(form.globalBillingDocList==null){
				form.globalBillingDocList=new ArrayList<BillingDocument>();
			}
			form.globalBillingDocList.add(billObj);
			form.dgBill.getDataSource().addData(form.addDateToBillingDG(billObj,null));
			form.dgBill.redraw();
		}else if(model instanceof Invoice){
			if(form.globalInvoiceList==null){
				form.globalInvoiceList=new ArrayList<Invoice>();
			}
			form.globalInvoiceList.add(invoiceObj);
			form.dgInvoice.getDataSource().addData(form.addDateToInvoiceDG(invoiceObj));
			form.dgInvoice.redraw();
		}else if(model instanceof CustomerPayment){
			if(form.globalPaymentList==null){
				form.globalPaymentList=new ArrayList<CustomerPayment>();
			}
			form.globalPaymentList.add(custPayObj);
			form.dgPayment.getDataSource().addData(form.addDateToPaymentDG(custPayObj));
			form.dgPayment.redraw();
		}else if(model instanceof SalesOrder){
			if(form.globalSalesOrderList==null){
				form.globalSalesOrderList=new ArrayList<SalesOrder>();
			}
			form.globalSalesOrderList.add(salesOrderObj);
			form.dgSalesOrder.getDataSource().addData(form.addDateToSalesOrderDG(salesOrderObj));
			form.dgSalesOrder.redraw();
		}else if(model instanceof SalesQuotation){
			if(form.globalSalesQuotList==null){
				form.globalSalesQuotList=new ArrayList<SalesQuotation>();
			}
			form.globalSalesQuotList.add(salesQuotObj);
			form.dgSalesQuotation.getDataSource().addData(form.addDateToSalesQuotDG(salesQuotObj));
			form.dgSalesQuotation.redraw();
		}
	}
	
	private void updateDataToGridAndList(SuperModel model) {
		form.toggleAppHeaderBarMenu();
		if(model instanceof Lead){
			if(form.globalSerLeadList==null){
				form.globalSerLeadList=new ArrayList<Lead>();
			}
			
			for(Lead obj:form.globalSerLeadList){
				if(obj.getCount()==leadObj.getCount()){
					form.globalSerLeadList.remove(obj);
					form.globalSerLeadList.add(leadObj);
					
				}
			}
			
			if(selectedRecord!=null){
				form.dgServiceLead.getDataSource().removeData(selectedRecord);
				form.dgServiceLead.getDataSource().addData(form.addDateToLeadDG(leadObj));
				form.dgServiceLead.redraw();
			}
			
			
		}else if(model instanceof Contract){
			Console.log("in updateDataToGridAndList of contract "+contractObj.getCount());
			if(form.globalContractList==null){
				form.globalContractList=new ArrayList<Contract>();
			}
			
			for(Contract obj:form.globalContractList){
				if(obj.getCount()==contractObj.getCount()){
					form.globalContractList.remove(obj);
					form.globalContractList.add(contractObj);
					Console.log("new object is added to global list"+contractObj.getStatus());
					loadDataForContract(form.globalContractList); //added on 19-08-2022
					Console.log("new data loaded");
				}
			}
			if(selectedRecord!=null){
				Console.log("in selectedRecord!=null");
				form.dgContract.getDataSource().removeData(selectedRecord);
				form.dgContract.getDataSource().addData(form.addDateToContractDG(contractObj));
				form.dgContract.redraw();
				Console.log("dgContract redrawed");
			}
		}
		else if(model instanceof Quotation){
			if(form.globalSerQuotList==null){
				form.globalSerQuotList=new ArrayList<Quotation>();
			}
			
			for(Quotation obj:form.globalSerQuotList){
				if(obj.getCount()==quotationObj.getCount()){
					form.globalSerQuotList.remove(obj);
					form.globalSerQuotList.add(quotationObj);
					loadDataForSerQuot(form.globalSerQuotList);//added on 22-08-2022
				}
			}
			
			if(selectedRecord!=null){
				form.dgServiceQuotation.getDataSource().removeData(selectedRecord);
				form.dgServiceQuotation.getDataSource().addData(form.addDateToSerQuotDG(quotationObj));
				form.dgServiceQuotation.redraw();
			}
		}
		
		else if(model instanceof CustomerPayment){
			if(form.globalPaymentList==null){
				form.globalPaymentList=new ArrayList<CustomerPayment>();
			}
			
			for(CustomerPayment obj:form.globalPaymentList){
				if(obj.getCount()==custPayObj.getCount()){
					form.globalPaymentList.remove(obj);
					form.globalPaymentList.add(custPayObj);
					form.setDataToPaymentListGrid(form.globalPaymentList);//added on 22-08-2022
				}
			}
			
			if(selectedRecord!=null){
				form.dgPayment.getDataSource().removeData(selectedRecord);
				form.dgPayment.getDataSource().addData(form.addDateToPaymentDG(custPayObj));
				form.dgPayment.redraw();
			}
		}
		else if(model instanceof BillingDocument){
			if(form.globalBillingDocList==null){
				form.globalBillingDocList=new ArrayList<BillingDocument>();
			}
			
			int i=0;
			for(BillingDocument obj:form.globalBillingDocList){
				if(obj.getCount()==billObj.getCount()){
					Console.log("on CLose .. "+obj.getCount()+" / "+billObj.getCount());
					form.globalBillingDocList.remove(i);
					form.globalBillingDocList.add(billObj);
					form.setDataToBillListGrid(form.globalBillingDocList);//added on 22-08-2022
					break;
				}
				i++;
			}
			
			if(selectedRecord!=null){
				Console.log("on CLose SELECTED RECORD .. "+selectedRecord.getAttribute("billingId").trim());
				form.addDateToBillingDG(billObj,selectedRecord);
				form.dgBill.getDataSource().updateData(selectedRecord);
				form.dgBill.redraw();
			}
		}
		else if(model instanceof Invoice){
			if(form.globalInvoiceList==null){
				form.globalInvoiceList=new ArrayList<Invoice>();
			}
			
			for(Invoice obj:form.globalInvoiceList){
				if(obj.getCount()==invoiceObj.getCount()){
					form.globalInvoiceList.remove(obj);
					form.globalInvoiceList.add(invoiceObj);
					form.setDataToInvoiceListGrid(form.globalInvoiceList);//added on 22-08-2022
				}
			}
			
			if(selectedRecord!=null){
				form.dgInvoice.getDataSource().removeData(selectedRecord);
				form.dgInvoice.getDataSource().addData(form.addDateToInvoiceDG(invoiceObj));
				form.dgInvoice.redraw();
			}
		}
		
		else if(model instanceof SalesOrder){
			if(form.globalSalesOrderList==null){
				form.globalSalesOrderList=new ArrayList<SalesOrder>();
			}
			
			for(SalesOrder obj:form.globalSalesOrderList){
				if(obj.getCount()==invoiceObj.getCount()){
					form.globalSalesOrderList.remove(obj);
					form.globalSalesOrderList.add(salesOrderObj);
					form.setDataToSalesOrderListGrid(form.globalSalesOrderList);//added on 22-08-2022
				}
			}
			
			if(selectedRecord!=null){
				form.dgSalesOrder.getDataSource().removeData(selectedRecord);
				form.dgSalesOrder.getDataSource().addData(form.addDateToSalesOrderDG(salesOrderObj));
				form.dgSalesOrder.redraw();
			}
		}
		
		else if(model instanceof SalesQuotation){
			if(form.globalSalesQuotList==null){
				form.globalSalesQuotList=new ArrayList<SalesQuotation>();
			}
			
			for(SalesQuotation obj:form.globalSalesQuotList){
				if(obj.getCount()==salesQuotObj.getCount()){
					form.globalSalesQuotList.remove(obj);
					form.globalSalesQuotList.add(salesQuotObj);
					form.setDataToSalesQuotLG(form.globalSalesQuotList);//added on 22-08-2022
					
				}
			}
			
			if(selectedRecord!=null){
				form.dgSalesQuotation.getDataSource().removeData(selectedRecord);
				form.dgSalesQuotation.getDataSource().addData(form.addDateToSalesQuotDG(salesQuotObj));
				form.dgSalesQuotation.redraw();
			}
		}
	}

	private void reactOnNew(final String tabName) {
		Console.log("reactOnNew "+tabName );
		genralInvoicePopup=new GeneralViewDocumentPopup();
		billingPopUp=new GeneralViewDocumentPopup();
		salesLeadPopup=new GeneralViewDocumentPopup();
		
		leadObj=null;
		quotationObj=null;
		contractObj=null;
		
		salesLeadObj=null;
		salesQuotObj=null;
		salesOrderObj=null;
		deliveryNoteObj=null;
		
		//By Ashwini Patil
		workOrderObj=null;
		purchaseOrderObj=null;
		deviceObj=null;
		
		
		
		if(tabName.equals("Service Lead")){
			AppMemory.getAppMemory().currentScreen = Screen.LEAD;
			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(AppConstants.SERVICELEAD,null);
		}else if(tabName.equals("Service Quotation")){
			AppMemory.getAppMemory().currentScreen = Screen.QUOTATION;
			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(AppConstants.QUOTATION,null);
		}else if(tabName.equals("Contract")){
			AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(AppConstants.CONTRACT,null);
		}else if(tabName.equals("Sales Lead")){
			AppMemory.getAppMemory().currentScreen = Screen.SALESLEAD;
			salesLeadPopup = new GeneralViewDocumentPopup();
			salesLeadPopup.btnClose.addClickHandler(this);
			salesLeadPopup.setModel(tabName,null);
		}else if(tabName.equals("Sales Quotation")){
			AppMemory.getAppMemory().currentScreen = Screen.SALESQUOTATION;
			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(tabName,null);
		}else if(tabName.equals("Sales Order")){
			AppMemory.getAppMemory().currentScreen = Screen.SALESORDER;
			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(tabName,null);
		}else if(tabName.equals("Delivery Note")){
			AppMemory.getAppMemory().currentScreen = Screen.DELIVERYNOTE;
			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(tabName,null);
		}else if(tabName.equals("Work Order")){
			AppMemory.getAppMemory().currentScreen = Screen.WORKORDER;
			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(AppConstants.WORKORDER,null);
		}else if(tabName.equals("Purchase Order")){
			AppMemory.getAppMemory().currentScreen = Screen.PURCHASEORDER;
			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(AppConstants.PURCHASEORDER,null);
		}else if(tabName.equals("Device")){
			AppMemory.getAppMemory().currentScreen = Screen.DEVICE;
			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel("Device",null);			
		}
				
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu(tabName);
			}
		};
		timer.schedule(1100);
	}
	
	private void reactOnView(final String tabName) {
		Console.log("reactOnView "+tabName);
		Console.log("hideNavigationFlag= "+hideNavigationFlag);
		viewedScreenName=tabName;
		
		billingPopUp=new GeneralViewDocumentPopup();
		genralInvoicePopup = new GeneralViewDocumentPopup();
//		if(isRestrictedFlag) {
//			genralInvoicePopup.applyRestrictionOnPopup=true;
//			Console.log("reactOnView applyRestrictionOnPopup set to true");
//		}
		salesLeadPopup=new GeneralViewDocumentPopup();
		if(tabName.equals("Service Lead")){
			AppMemory.getAppMemory().currentScreen = Screen.LEAD;
//			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag);
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(AppConstants.SERVICELEAD,leadObj);			
		}else if(tabName.equals("Service Quotation")){
			AppMemory.getAppMemory().currentScreen = Screen.QUOTATION;
//			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag);
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(AppConstants.QUOTATION,quotationObj);
		}else if(tabName.equals("Contract")){
			AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
//			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag);
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(AppConstants.CONTRACT,contractObj);
		}else if(tabName.equals("Invoice")){
			AppMemory.getAppMemory().currentScreen = Screen.INVOICEDETAILS; //Screen.INVOICE; changed on 19-08-2022
			genralInvoicePopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag); //Ashwini Patil changed arguments
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(AppConstants.PROCESSCONFIGINV,invoiceObj);
		}else if(tabName.equals("Bill")){
			AppMemory.getAppMemory().currentScreen = Screen.BILLINGDETAILS;
			genralInvoicePopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//Ashwini Patil changed arguments
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(AppConstants.BILLINGDETAILS,billObj);
		}else if(tabName.equals("Payment")){
			AppMemory.getAppMemory().currentScreen = Screen.PAYMENTDETAILS;
			genralInvoicePopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//Ashwini Patil changed arguments
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(AppConstants.PAYMENT,custPayObj);
			Console.log("Ashwini setmodel called for paymentDetailsPresenter from summary presenter");
			
		}else if(tabName.equals("Sales Lead")){
			AppMemory.getAppMemory().currentScreen = Screen.SALESLEAD;
//			salesLeadPopup = new GeneralViewDocumentPopup();	
			salesLeadPopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag);
			salesLeadPopup.btnClose.addClickHandler(this);
			salesLeadPopup.setModel(tabName,salesLeadObj);
		}else if(tabName.equals("Sales Quotation")){
			AppMemory.getAppMemory().currentScreen = Screen.SALESQUOTATION;
//			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag);
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(tabName,salesQuotObj);
		}else if(tabName.equals("Sales Order")){
			AppMemory.getAppMemory().currentScreen = Screen.SALESORDER;
//			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag);
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(tabName,salesOrderObj);
		}else if(tabName.equals("Delivery Note")){
			AppMemory.getAppMemory().currentScreen = Screen.DELIVERYNOTE;
//			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag);
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.setModel(tabName,deliveryNoteObj);
		}
//		else if(tabName.equals("Service")||tabName.equals("Customer Service List")||tabName.equals(AppConstants.CUSTOMERSERVICELIST)){
//			AppMemory.getAppMemory().currentScreen = Screen.SERVICE;
//			genralInvoicePopup = new GeneralViewDocumentPopup();
//			genralInvoicePopup.btnClose.addClickHandler(this);
//			genralInvoicePopup.setModel(tabName,serviceObj);
//		}		
		
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu(tabName);
			}
		};
		timer.schedule(1100);
		
	}
	
	private void savePayment(final CustomerPayment custPayObj) {
		service.save(custPayObj, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				for(CustomerPayment obj:form.globalPaymentList){
					if(obj.getCount()==custPayObj.getCount()){
						obj.setFollowUpDate(custPayObj.getFollowUpDate());
						obj.setStatus(custPayObj.getStatus());
						obj.setEmployee(custPayObj.getEmployee());
						break;
					}
				}
			}
		});
	}

	private void saveSalesQuotation(final SalesQuotation salesQuotObj) {
		// TODO Auto-generated method stub
		service.save(salesQuotObj, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				for(SalesQuotation obj:form.globalSalesQuotList){
					if(obj.getCount()==salesQuotObj.getCount()){
						obj.setFollowUpDate(salesQuotObj.getFollowUpDate());
						obj.setStatus(salesQuotObj.getStatus());
						obj.setEmployee(salesQuotObj.getEmployee());
						break;
					}
				}
			}
		});
	}
	
	protected void saveQuotation(final Quotation quotationObj) {
		service.save(quotationObj, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				for(Quotation obj:form.globalSerQuotList){
					if(obj.getCount()==quotationObj.getCount()){
						obj.setFollowUpDate(quotationObj.getFollowUpDate());
						obj.setStatus(quotationObj.getStatus());
						obj.setEmployee(quotationObj.getEmployee());
						break;
					}
				}
			}
		});
		
	}

	private void saveLead(final Lead leadObj) {
		service.save(leadObj, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				for(Lead obj:form.globalSerLeadList){
					if(obj.getCount()==leadObj.getCount()){
						obj.setFollowUpDate(leadObj.getFollowUpDate());
						obj.setStatus(leadObj.getStatus());
						obj.setEmployee(leadObj.getEmployee());
						break;
					}
				}
			}
		});
	}
	
	@Override
	public void onCellClick(CellClickEvent event) {
		Console.log("on cell click");
		selectedRecord=new ListGridRecord();
		position=0;
		if(event.getSource()==form.dgServiceLead){
			ListGridRecord record = event.getRecord();
			int colNum = event.getColNum();
			ListGridField field = form.dgServiceLead.getField(colNum);
			String fieldName = form.dgServiceLead.getFieldName(colNum);
			String fieldTitle = field.getTitle();
			position=event.getRowNum();
			Console.log("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Lead Id :: "+ record.getAttribute("leadId")+" Selected Row "+position);
			leadObj = getLeadDetails((Integer.parseInt(record.getAttribute("leadId").trim())));
			if (fieldName.equals("followup")) {
				hideNavigationFlag=false;
				selectedRecord = record;
				getLogDetails("Service Lead");
			}else if(fieldName.equals("leadId")){
				hideNavigationFlag=false;
				selectedRecord = record;
				reactOnView("Service Lead");
			}else if(fieldName.equals("orderId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("orderId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					if(orderId!=0){
						contractObj=getContractDetails(orderId);
					}
					
					if(contractObj!=null){
						Console.log("calling reactOnView");
						reactOnView("Contract");
					}else{
						Console.log("calling reteriveDataFromDB");
						reteriveDataFromDB(orderId,"Contract",false);
					}
				}
			}
		}
		if(event.getSource()==form.dgServiceQuotation){
			ListGridRecord record = event.getRecord();
			int colNum = event.getColNum();
			ListGridField field = form.dgServiceQuotation.getField(colNum);
			String fieldName = form.dgServiceQuotation.getFieldName(colNum);
			String fieldTitle = field.getTitle();
			position=event.getRowNum();
			Console.log("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Quotation Id :: "+ record.getAttribute("quotationId")+" Selected Row "+position);
			quotationObj = getQuotationDetails((Integer.parseInt(record.getAttribute("quotationId").trim())));
			if (fieldName.equals("followup")) {
				hideNavigationFlag=false;
				selectedRecord = record;
				getLogDetails("Service Quotation");
			}else if(fieldName.equals("email")){
				hideNavigationFlag=false;
				reactOnEmail("Service Quotation");
			}else if(fieldName.equals("print")){
				hideNavigationFlag=false;
				reactOnPrint("Service Quotation");
			}else if(fieldName.equals("leadId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("leadId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					if(orderId!=0){
						leadObj=getLeadDetails(orderId);
					}
					if(leadObj!=null){
						reactOnView("Service Lead");
					}else{
						reteriveDataFromDB(orderId,"Service Lead",false);
					}
				}
			}else if(fieldName.equals("orderId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("orderId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					if(orderId!=0){
						contractObj=getContractDetails(orderId);
					}
					if(contractObj!=null){
						reactOnView("Contract");
					}else{
						reteriveDataFromDB(orderId,"Contract",false);
					}
				}
			}else if(fieldName.equals("quotationId")){
				hideNavigationFlag=false;
				selectedRecord = record;
				reactOnView("Service Quotation");
			}
		}
		
		if(event.getSource()==form.dgContract){
			ListGridRecord record = event.getRecord();
			int colNum = event.getColNum();
			ListGridField field = form.dgContract.getField(colNum);
			String fieldName = form.dgContract.getFieldName(colNum);
			String fieldTitle = field.getTitle();
			position=event.getRowNum();
			Console.log("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Contract Id :: "+ record.getAttribute("orderId")+" Selected Row "+position);
			
			contractObj = getContractDetails((Integer.parseInt(record.getAttribute("orderId").trim())));
			
			
			if(fieldName.equals("email")){
				hideNavigationFlag=false;
				reactOnEmail("Contract");
			}else if(fieldName.equals("print")){
				hideNavigationFlag=false;
				reactOnPrint("Contract");
			}else if(fieldName.equals("leadId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("leadId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					if(orderId!=0){
						leadObj=getLeadDetails(orderId);
					}
					if(leadObj!=null){
						reactOnView("Service Lead");
					}else{
						reteriveDataFromDB(orderId,"Service Lead",false);
					}
			}
			}else if(fieldName.equals("orderId")){
				hideNavigationFlag=false;
				selectedRecord = record;
				reactOnView("Contract");
			}else if(fieldName.equals("quotationId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("quotationId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					if(orderId!=0){
						quotationObj=getQuotationDetails(orderId);
					}
					if(quotationObj!=null){
						reactOnView("Service Quotation");
					}else{
						reteriveDataFromDB(orderId,"Service Quotation",false);
					}
				}
			}
		}
		
		if(event.getSource()==form.dgBill){
			ListGridRecord record = event.getRecord();
			int colNum = event.getColNum();
			ListGridField field = form.dgBill.getField(colNum);
			String fieldName = form.dgBill.getFieldName(colNum);
			String fieldTitle = field.getTitle();
			position=event.getRowNum();
			Console.log("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Bill Id :: "+ record.getAttribute("billingId")+" Selected Row "+position);
			
			if(record.getAttribute(fieldName)==null||record.getAttribute(fieldName).equals("null")){
				selectedRecord=null;
				position=0;
				billObj=null;
				Console.log("Return on checked!!!");
				return;
			}
			
			billObj = getBillDetails((Integer.parseInt(record.getAttribute("billingId").trim())));
			
			Console.log("Billing id - "+billObj.getCount());
			if(fieldName.equals("billingId")){
				hideNavigationFlag=false;
				selectedRecord = record;
				reactOnView("Bill");
			}else if(fieldName.equals("invoiceId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("invoiceId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					if(orderId!=0){
						invoiceObj=getInvoiceDetails(orderId);
					}
					if(invoiceObj!=null){
						reactOnView("Invoice");
					}else{
						reteriveDataFromDB(orderId,"Invoice",false);
					}
				}
			}else if(fieldName.equals("orderId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("orderId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					if(orderId!=0){
						contractObj=getContractDetails(orderId);
					}
					if(contractObj!=null){
						reactOnView("Contract");
					}else{
						reteriveDataFromDB(orderId,"Contract",false);
					}
				}
			}
		}
		
		if(event.getSource()==form.dgInvoice){
			ListGridRecord record = event.getRecord();
			int colNum = event.getColNum();
			ListGridField field = form.dgInvoice.getField(colNum);
			String fieldName = form.dgInvoice.getFieldName(colNum);
			String fieldTitle = field.getTitle();
			position=event.getRowNum();
			Console.log("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Invoice Id :: "+ record.getAttribute("invoiceId")+" Selected Row "+position);
			
			invoiceObj = getInvoiceDetails((Integer.parseInt(record.getAttribute("invoiceId").trim())));
			
			
			if(fieldName.equals("email")){
				hideNavigationFlag=false;
				reactOnEmail("Invoice");
			}else if(fieldName.equals("print")){
				hideNavigationFlag=false;
				reactOnPrint("Invoice");
			}else if(fieldName.equals("invoiceId")){
				hideNavigationFlag=false;
				selectedRecord = record;
				reactOnView("Invoice");
			}else if(fieldName.equals("orderId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("orderId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					if(orderId!=0){
						contractObj=getContractDetails(orderId);
					}
					if(contractObj!=null){
						reactOnView("Contract");
					}else{
						reteriveDataFromDB(orderId,"Contract",false);
					}
				}
			}
		}
		
		if(event.getSource()==form.dgPayment){
			ListGridRecord record = event.getRecord();
			int colNum = event.getColNum();
			ListGridField field = form.dgPayment.getField(colNum);
			String fieldName = form.dgPayment.getFieldName(colNum);
			String fieldTitle = field.getTitle();
			position=event.getRowNum();
			Console.log("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Payment Id :: "+ record.getAttribute("paymentId")+" Selected Row "+position);
			
			custPayObj = getPaymentDetails((Integer.parseInt(record.getAttribute("paymentId").trim())));
			
			
			if(fieldName.equals("email")){
				hideNavigationFlag=false;
				reactOnEmail("Payment");
			}else if(fieldName.equals("print")){
				hideNavigationFlag=false;
				reactOnPrint("Payment");
			}else if (fieldName.equals("followup")) {
				hideNavigationFlag=false;
				selectedRecord = record;
				getLogDetails("Payment");
			}else if(fieldName.equals("paymentId")){
				hideNavigationFlag=false;
				selectedRecord = record;
				reactOnView("Payment");
			}else if(fieldName.equals("invoiceId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("invoiceId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					if(orderId!=0){
						invoiceObj=getInvoiceDetails(orderId);
					}
					if(invoiceObj!=null){
						reactOnView("Invoice");
					}else{
						reteriveDataFromDB(orderId,"Invoice",false);
					}
				}
			}
		}
		
		
		/*****
		 * 
		 * 
		 * 
		 * 
		 * 
		 */
		
		if(event.getSource()==form.dgSalesLead){
			ListGridRecord record = event.getRecord();
			int colNum = event.getColNum();
			ListGridField field = form.dgSalesLead.getField(colNum);
			String fieldName = form.dgSalesLead.getFieldName(colNum);
			String fieldTitle = field.getTitle();
			position=event.getRowNum();
			Console.log("dgSalesLead Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Sales Lead Id :: "+ record.getAttribute("leadId")+" Selected Row "+position);
			
			salesLeadObj = getSalesLeadDetails((Integer.parseInt(record.getAttribute("leadId").trim())));
			
//			if(fieldName.equals("email")){
//				reactOnEmail("Sales Lead");
//			}else if(fieldName.equals("print")){
//				reactOnPrint("Sales Lead");
//			}else 
			if (fieldName.equals("followup")) {
				hideNavigationFlag=false;
				selectedRecord = record;
				getLogDetails("Sales Lead");
			}else if(fieldName.equals("leadId")){
				hideNavigationFlag=false;
				selectedRecord = record;
				reactOnView("Sales Lead");
			}
			if(fieldName.equals("orderId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("orderId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					if(orderId!=0){
						salesOrderObj=getSalesOrderDetails(orderId);
					}
					if(salesOrderObj!=null){
						reactOnView("Sales Order");
					}else{
						reteriveDataFromDB(orderId,"Sales Order",false);
					}
				}
			}
		}
		
		if(event.getSource()==form.dgSalesQuotation){
			ListGridRecord record = event.getRecord();
			int colNum = event.getColNum();
			ListGridField field = form.dgSalesQuotation.getField(colNum);
			String fieldName = form.dgSalesQuotation.getFieldName(colNum);
			String fieldTitle = field.getTitle();
			position=event.getRowNum();
			Console.log("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Sales Quotation Id :: "+ record.getAttribute("quotationId")+" Selected Row "+position);
			
			salesQuotObj = getSalesQuotationDetails((Integer.parseInt(record.getAttribute("quotationId").trim())));
			
			if(fieldName.equals("email")){
				hideNavigationFlag=false;
				reactOnEmail("Sales Quotation");
			}else if(fieldName.equals("print")){
				hideNavigationFlag=false;
				reactOnPrint("Sales Quotation");
			}else if (fieldName.equals("followup")) {
				hideNavigationFlag=false;
				selectedRecord = record;
				getLogDetails("Sales Quotation");
			}else if(fieldName.equals("quotationId")){
				hideNavigationFlag=false;
				selectedRecord = record;
				reactOnView("Sales Quotation");
			}else if(fieldName.equals("orderId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("orderId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					Console.log("step1");
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					Console.log("step2");
					if(orderId!=0){
						salesOrderObj=getSalesOrderDetails(orderId);
					}
					if(salesOrderObj!=null){
						Console.log("step3");
						reactOnView("Sales Order");
					}else{
						Console.log("step4");
						reteriveDataFromDB(orderId,"Sales Order",false);
					}
				}
			}else if(fieldName.equals("leadId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("leadId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					if(orderId!=0){
						salesLeadObj=getSalesLeadDetails(orderId);
					}
					if(salesLeadObj!=null){
						reactOnView("Sales Lead");
					}else{
						reteriveDataFromDB(orderId,"Sales Lead",false);
					}
				}
			}
		}
		
		if(event.getSource()==form.dgSalesOrder){
			ListGridRecord record = event.getRecord();
			int colNum = event.getColNum();
			ListGridField field = form.dgSalesOrder.getField(colNum);
			String fieldName = form.dgSalesOrder.getFieldName(colNum);
			String fieldTitle = field.getTitle();
			position=event.getRowNum();
			Console.log("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Sales Order Id :: "+ record.getAttribute("orderId")+" Selected Row "+position);
			
			salesOrderObj = getSalesOrderDetails((Integer.parseInt(record.getAttribute("orderId").trim())));
			
			if(fieldName.equals("email")){
				hideNavigationFlag=false;
				reactOnEmail("Sales Order");
			}else if(fieldName.equals("print")){
				hideNavigationFlag=false;
				reactOnPrint("Sales Order");
			}else if(fieldName.equals("orderId")){
				hideNavigationFlag=false;
				selectedRecord = record;
				reactOnView("Sales Order");
			}else if(fieldName.equals("quotationId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("quotationId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					if(orderId!=0){
						salesQuotObj=getSalesQuotationDetails(orderId);
					}
					if(salesQuotObj!=null){
						reactOnView("Sales Quotation");
					}else{
						reteriveDataFromDB(orderId,"Sales Quotation",false);
					}
				}
			}else if(fieldName.equals("leadId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("leadId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					if(orderId!=0){
						salesLeadObj=getSalesLeadDetails(orderId);
					}
					if(salesLeadObj!=null){
						reactOnView("Sales Lead");
					}else{
						reteriveDataFromDB(orderId,"Sales Lead",false);
					}
				}
			}
		}
		
		if(event.getSource()==form.dgDeliveryNote){
			ListGridRecord record = event.getRecord();
			int colNum = event.getColNum();
			ListGridField field = form.dgDeliveryNote.getField(colNum);
			String fieldName = form.dgDeliveryNote.getFieldName(colNum);
			String fieldTitle = field.getTitle();
			position=event.getRowNum();
			Console.log("Clicked " + fieldTitle + ":"+ record.getAttribute(fieldName) + "  Delivery Note Id :: "+ record.getAttribute("deliveryNoteId")+" Selected Row "+position);
			
			deliveryNoteObj = getDeliveryNoteDetails((Integer.parseInt(record.getAttribute("deliveryNoteId").trim())));
			
			if(fieldName.equals("email")){
				hideNavigationFlag=false;
				reactOnEmail("Delivery Note");
			}else if(fieldName.equals("print")){
				hideNavigationFlag=false;
				reactOnPrint("Delivery Note");
			}else if(fieldName.equals("deliveryNoteId")){
				hideNavigationFlag=false;
				selectedRecord = record;
				reactOnView("Delivery Note");
			}else if(fieldName.equals("orderId")){
				hideNavigationFlag=true;
				String orderIdStr=record.getAttribute("orderId");
				Console.log("Id : "+orderIdStr);
				int orderId=0;
				if(!orderIdStr.equals("")){
					try{
						orderId=Integer.parseInt(orderIdStr);
					}catch(Exception e){
						
					}
					if(orderId!=0){
						salesOrderObj=getSalesOrderDetails(orderId);
					}
					if(salesOrderObj!=null){
						reactOnView("Sales Order");
					}else{
						reteriveDataFromDB(orderId,"Sales Order",false);
					}
				}
			}
		}
	}
	
	private void reactOnPrint(String tabName) {
		String documentName="";
		SuperModel model=null;
		if(tabName.equals("Service Quotation")){
			documentName="Quotation";
			model=quotationObj;
		}else if(tabName.equals("Contract")){
			documentName=tabName;
			model=contractObj;
		}else if(tabName.equals("Invoice")){
			documentName=tabName;
			model=invoiceObj;
		}else if(tabName.equals("Payment")){
			documentName=tabName;
			model=custPayObj;
		}else if(tabName.equals("Sales Quotation")){
			documentName=tabName;
			model=salesQuotObj;
		}else if(tabName.equals("Sales Order")){
			documentName=tabName;
			model=salesOrderObj;
		}else if(tabName.equals("Delivery Note")){
			documentName=tabName;
			model=deliveryNoteObj;
		}
		
		commonServ.getPDFURL(documentName, model, model.getCount(),UserConfiguration.getCompanyId(), LoginPresenter.company, GWT.getModuleBaseURL(),null, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				
			}

			@Override
			public void onSuccess(String result) {
				if(result!=null&&!result.equals("")){
					Window.open(result, "test", "enabled");
				}
			}
		});
	}

	private void reteriveDataFromDB(int count, final String tabName,final boolean storeInListFlag) {
		Console.log("reteriveDataFromDB: "+count+"/"+tabName);
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(count);
		filtervec.add(temp);
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		
		if(tabName.equals("Service Lead")){
			querry.setQuerryObject(new Lead());
		}else if(tabName.equals("Service Quotation")){
			querry.setQuerryObject(new Quotation());
		}else if(tabName.equals("Contract")){
			querry.setQuerryObject(new Contract());
		}else if(tabName.equals("Bill")){
			querry.setQuerryObject(new BillingDocument());
		}else if(tabName.equals("Invoice")){
			querry.setQuerryObject(new Invoice());
		}else if(tabName.equals("Payment")){
			querry.setQuerryObject(new CustomerPayment());
		}else if(tabName.equals("Sales Lead")){
			querry.setQuerryObject(new Lead());
		}else if(tabName.equals("Sales Quotation")){
			querry.setQuerryObject(new SalesQuotation());
		}else if(tabName.equals("Sales Order")){
			querry.setQuerryObject(new SalesOrder());
		}else if(tabName.equals("Delivery Note")){
			querry.setQuerryObject(new DeliveryNote());
		}
		
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
				Console.log("onSuccess: storeInListFlag="+storeInListFlag);
				Console.log("result.size()="+result.size());
				if(result==null||result.size()==0){
					return;
				}
				if(tabName.equals("Service Lead")){
					leadObj=(Lead) result.get(0);
				}else if(tabName.equals("Service Quotation")){
					quotationObj=(Quotation) result.get(0);
				}else if(tabName.equals("Contract")){
					contractObj=(Contract) result.get(0);
				}else if(tabName.equals("Bill")){
					billObj=(BillingDocument) result.get(0);
				}else if(tabName.equals("Invoice")){
					if(storeInListFlag){
						if(form.globalInvoiceList==null){
							form.globalInvoiceList=new ArrayList<Invoice>();
						}
						form.globalInvoiceList.add((Invoice) result.get(0));
					}else{
						invoiceObj=(Invoice) result.get(0);
					}
				}else if(tabName.equals("Payment")){
					custPayObj=(CustomerPayment) result.get(0);
				}else if(tabName.equals("Sales Lead")){
					salesLeadObj=(Lead) result.get(0);
				}else if(tabName.equals("Sales Quotation")){
					salesQuotObj=(SalesQuotation) result.get(0);
				}else if(tabName.equals("Sales Order")){
					Console.log("salesorderobj creating");
					salesOrderObj=(SalesOrder) result.get(0);
					Console.log("salesorderobj created");
				}else if(tabName.equals("Delivery Note")){
					deliveryNoteObj=(DeliveryNote) result.get(0);
				}
				if(!storeInListFlag){
					Console.log("storeInListFlag false condition");
					reactOnView(tabName);
				}
			}
		});
	}

	private void reactOnEmail(String tabName) {
		emailpopup.showPopUp();
		if(tabName.equals("Service Quotation")){
			setEmailPopUpData(quotationObj);
		}else if(tabName.equals("Contract")){
			setEmailPopUpData(contractObj);
		}else if(tabName.equals("Invoice")){
			setEmailPopUpData(invoiceObj);
		}else if(tabName.equals("Payment")){
			setEmailPopUpData(custPayObj);
		}else if(tabName.equals("Sales Quotation")){
			setEmailPopUpData(salesQuotObj);
		}else if(tabName.equals("Sales Order")){
			setEmailPopUpData(salesOrderObj);
		}else if(tabName.equals("Delivery Note")){
			setEmailPopUpData(deliveryNoteObj);
		}
		quotationObj=null;
		contractObj=null;
		invoiceObj=null;
		custPayObj=null;
		
		salesQuotObj=null;
		salesOrderObj=null;
		deliveryNoteObj=null;
		
		
	}
	
	private void setEmailPopUpData(SuperModel model) {
		form.showWaitSymbol();
		String customerEmail = "";
		String branchName = "";
		String fromEmailId = "";
		String customerName = "";
		int customerId = 0;
		Screen screenName = null;
		String moduleName="";
		
		if(model instanceof Contract){
			branchName=contractObj.getBranch();
			fromEmailId=AppUtility.getFromEmailAddress(branchName,contractObj.getEmployee());
			customerEmail = AppUtility.getCustomerEmailid(contractObj.getCinfo().getCount());
			customerName = contractObj.getCinfo().getFullName();
			customerId=contractObj.getCinfo().getCount();
			screenName=Screen.CONTRACT;
			moduleName="Service";
		}else if(model instanceof Quotation){
			branchName=quotationObj.getBranch();
			fromEmailId=AppUtility.getFromEmailAddress(branchName,quotationObj.getEmployee());
			customerEmail = AppUtility.getCustomerEmailid(quotationObj.getCinfo().getCount());
			customerName = quotationObj.getCinfo().getFullName();
			customerId=quotationObj.getCinfo().getCount();
			screenName=Screen.QUOTATION;
			moduleName="Service";
		}else if(model instanceof Invoice){
			branchName=invoiceObj.getBranch();
			fromEmailId=AppUtility.getFromEmailAddress(branchName,invoiceObj.getEmployee());
			customerEmail = AppUtility.getCustomerEmailid(invoiceObj.getPersonInfo().getCount());
			customerName = invoiceObj.getPersonInfo().getFullName();
			customerId=invoiceObj.getPersonInfo().getCount();
			screenName=Screen.INVOICEDETAILS;//8-09-2022
			moduleName="Accounts";
		}else if(model instanceof CustomerPayment){
			branchName=custPayObj.getBranch();
			fromEmailId=AppUtility.getFromEmailAddress(branchName,custPayObj.getEmployee());
			customerEmail = AppUtility.getCustomerEmailid(custPayObj.getPersonInfo().getCount());
			customerName = custPayObj.getPersonInfo().getFullName();
			customerId=custPayObj.getPersonInfo().getCount();
			screenName=Screen.PAYMENTDETAILS;
			moduleName="Accounts";
		} else if(model instanceof SalesOrder){
			branchName=salesOrderObj.getBranch();
			fromEmailId=AppUtility.getFromEmailAddress(branchName,salesOrderObj.getEmployee());
			customerEmail = AppUtility.getCustomerEmailid(salesOrderObj.getCinfo().getCount());
			customerName = salesOrderObj.getCinfo().getFullName();
			customerId=salesOrderObj.getCinfo().getCount();
			screenName=Screen.SALESORDER;
			moduleName="Sales";
		} else if(model instanceof SalesQuotation){
			branchName=salesQuotObj.getBranch();
			fromEmailId=AppUtility.getFromEmailAddress(branchName,salesQuotObj.getEmployee());
			customerEmail = AppUtility.getCustomerEmailid(salesQuotObj.getCinfo().getCount());
			customerName = salesQuotObj.getCinfo().getFullName();
			customerId=salesQuotObj.getCinfo().getCount();
			screenName=Screen.SALESQUOTATION;
			moduleName="Sales";
		} else if(model instanceof DeliveryNote){
			branchName=deliveryNoteObj.getBranch();
			fromEmailId=AppUtility.getFromEmailAddress(branchName,deliveryNoteObj.getEmployee());
			customerEmail = AppUtility.getCustomerEmailid(deliveryNoteObj.getCinfo().getCount());
			customerName = deliveryNoteObj.getCinfo().getFullName();
			customerId=deliveryNoteObj.getCinfo().getCount();
			screenName=Screen.DELIVERYNOTE;
			moduleName="Sales";
		}  
		
		if(!customerName.equals("") && !customerEmail.equals("")){
			label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",customerId,customerName,customerEmail,null);
		}
		else{
	
			MyQuerry querry = AppUtility.getcustomerQuerry(customerId);
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel smodel : result){
						Customer custEntity  = (Customer) smodel;
						if(custEntity.getEmail()!=null){
							label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",custEntity.getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
						}else{
							label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",custEntity.getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
						}
						break;
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
				}
			});
		}
		
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,moduleName,screenName);
		emailpopup.taFromEmailId.setValue(fromEmailId);
		emailpopup.smodel = model;
		form.hideWaitSymbol();
		
	}

	private void getLogDetails(String tabName) {
		MyQuerry querry = null;
		
		if(tabName.equals("Service Lead")){
			if(leadObj==null){
				return;
			}
			querry=AppUtility.communicationLogQuerry(leadObj.getCount(),leadObj.getCompanyId(), AppConstants.SERVICEMODULE,AppConstants.LEAD);
		}else if(tabName.equals("Service Quotation")){
			if(quotationObj==null){
				return;
			}
			querry=AppUtility.communicationLogQuerry(quotationObj.getCount(),quotationObj.getCompanyId(), AppConstants.SERVICEMODULE,AppConstants.QUOTATION);
		}else if(tabName.equals("Payment")){
			if(custPayObj==null){
				return;
			}
			querry=AppUtility.communicationLogQuerry(custPayObj.getCount(),custPayObj.getCompanyId(), AppConstants.ACCOUNTMODULE,AppConstants.PAYMENTDETAILS);
		}else if(tabName.equals("Sales Lead")){
			if(salesLeadObj==null){
				return;
			}
			querry=AppUtility.communicationLogQuerry(salesLeadObj.getCount(),salesLeadObj.getCompanyId(), AppConstants.SALESMODULE,AppConstants.LEAD);
		}else if(tabName.equals("Sales Quotation")){
			if(salesQuotObj==null){
				return;
			}
			querry=AppUtility.communicationLogQuerry(salesQuotObj.getCount(),salesQuotObj.getCompanyId(), AppConstants.SALESMODULE,AppConstants.QUOTATION);
		}
		
		service.getSearchResult(querry,	new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<InteractionType> list = new ArrayList<InteractionType>();
				for (SuperModel model : result) {
					InteractionType interactionType = (InteractionType) model;
					list.add(interactionType);
				}
				Comparator<InteractionType> comp = new Comparator<InteractionType>() {
					@Override
					public int compare(InteractionType e1,InteractionType e2) {
						if (e1.getCount() == e2.getCount()) {
							return 0;
						}
						if (e1.getCount() > e2.getCount()) {
							return -1;
						} else {
							return 1;
						}
					}
				};
				Collections.sort(list, comp);
				commSmrtPopup.getRemark().setValue("");
				commSmrtPopup.getDueDate().setValue(null);
				commSmrtPopup.getOblinteractionGroup().setSelectedIndex(0);
				commSmrtPopup.getCommunicationLogTable().getDataprovider().setList(list);
				commSmrtPopup.showPopup();
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}
	
	private DeliveryNote getDeliveryNoteDetails(int leadId) {
		for(DeliveryNote obj:form.globalDeliveryNoteList){
			if(obj.getCount()==leadId){
				return obj;
			}
		}
		return null;
	}
	private SalesOrder getSalesOrderDetails(int leadId) {
		Console.log("step2.1 form.globalSalesOrderList size="+form.globalSalesOrderList.size());
		for(SalesOrder obj:form.globalSalesOrderList){
			Console.log("step2.2");
			if(obj.getCount()==leadId){
				Console.log("step2.3");
				return obj;
			}
		}
		return null;
	}
	private SalesQuotation getSalesQuotationDetails(int leadId) {
		for(SalesQuotation obj:form.globalSalesQuotList){
			if(obj.getCount()==leadId){
				return obj;
			}
		}
		return null;
	}
	private Lead getSalesLeadDetails(int leadId) {
		for(Lead obj:form.globalSalesLeadList){
			if(obj.getCount()==leadId){
				return obj;
			}
		}
		return null;
	}

	private Lead getLeadDetails(int leadId) {
		for(Lead obj:form.globalSerLeadList){
			if(obj.getCount()==leadId){
				return obj;
			}
		}
		return null;
	}
	
	private Quotation getQuotationDetails(int quotationId) {
		for(Quotation obj:form.globalSerQuotList){
			if(obj.getCount()==quotationId){
				return obj;
			}
		}
		return null;
	}
	
	private Contract getContractDetails(int contractId) {
		for(Contract obj:form.globalContractList){
			if(obj.getCount()==contractId){
				return obj;
			}
		}
		return null;
	}
	
	private BillingDocument getBillDetails(int contractId) {
		for(BillingDocument obj:form.globalBillingDocList){
			if(obj.getCount()==contractId){
				return obj;
			}
		}
		return null;
	}
	
	private Invoice getInvoiceDetails(int contractId) {
		for(Invoice obj:form.globalInvoiceList){
			if(obj.getCount()==contractId){
				return obj;
			}
		}
		return null;
	}
	
	private CustomerPayment getPaymentDetails(int contractId) {
		for(CustomerPayment obj:form.globalPaymentList){
			if(obj.getCount()==contractId){
				return obj;
			}
		}
		return null;
	}

	private void reactOnDownload(String tabName) {
		if(tabName.equals("Service Lead")){
			if(form.globalSerLeadList==null||form.globalSerLeadList.size()==0){
				return;
			}
			ArrayList<Lead> custarray=new ArrayList<Lead>(form.globalSerLeadList);
			csvservice.setleadslist(custarray, new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}
				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+3;
					Window.open(url, "test", "enabled");
				}
			});
		}else if(tabName.equals("Service Quotation")){
			if(form.globalSerQuotList==null||form.globalSerQuotList.size()==0){
				return;
			}
			ArrayList<Quotation> custarray=new ArrayList<Quotation>(form.globalSerQuotList);
			csvservice.setquotationlist(custarray, new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}

				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+2;
					Window.open(url, "test", "enabled");
				}
			});
		}else if(tabName.equals("Contract")){
			if(form.globalContractList==null||form.globalContractList.size()==0){
				return;
			}
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "RestrictContractsDownloadWithOTP")){
				generatedOTP=(int) Math.floor(100000 + Math.random() * 900000);
				Console.log("generatedOTP="+generatedOTP);
				smsService.checkConfigAndSendContractDownloadOTPMessage(model.getCompanyId(), LoginPresenter.loggedInUser, generatedOTP, new  AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Failed");
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						Console.log("in success");
						if(result!=null&&result.equalsIgnoreCase("success")) {						
							otpPopup.clearOtp();
							otppopupPanel = new PopupPanel(true);
							otppopupPanel.add(otpPopup);
							otppopupPanel.show();
							otppopupPanel.center();
						}else {
							form.showDialogMessage(result);
						}
							
					}
				} );
			
			}else
				runRegularContractDownloadProgram();
			
		}else if(tabName.equals("Bill")){
			if(form.globalBillingDocList==null||form.globalBillingDocList.size()==0){
				return;
			}
			ArrayList<BillingDocument> conbillingarray=new ArrayList<BillingDocument>(form.globalBillingDocList);
			csvservice.setconbillinglist(conbillingarray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}

				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+14;
					Window.open(url, "test", "enabled");
				}
			});
			
		}else if(tabName.equals("Invoice")){
			if(form.globalInvoiceList==null||form.globalInvoiceList.size()==0){
				return;
			}
			ArrayList<Invoice> invoicingarray=new ArrayList<Invoice>(form.globalInvoiceList);
			csvservice.setinvoicelist(invoicingarray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
					
				}

				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+15;
					Window.open(url, "test", "enabled");
				}
			});
			
		}else if(tabName.equals("Payment")){
			if(form.globalPaymentList==null||form.globalPaymentList.size()==0){
				return;
			}
			ArrayList<CustomerPayment> payarray=new ArrayList<CustomerPayment>(form.globalPaymentList);
			csvservice.setpaymentlist(payarray, new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+16;
					Window.open(url, "test", "enabled");
				}
			});
		}else if(tabName.equals("Sales Lead")){
			if(form.globalSalesLeadList==null||form.globalSalesLeadList.size()==0){
				return;
			}
			ArrayList<Lead> payarray=new ArrayList<Lead>(form.globalSalesLeadList);
			csvservice.setleadslist(payarray, new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+3;
					Window.open(url, "test", "enabled");
				}
			});
		}else if(tabName.equals("Sales Quotation")){
			if(form.globalSalesQuotList==null||form.globalSalesQuotList.size()==0){
				return;
			}
			ArrayList<SalesQuotation> payarray=new ArrayList<SalesQuotation>(form.globalSalesQuotList);
			csvservice.setsalesquotationlist(payarray, new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+60;
					Window.open(url, "test", "enabled");
				}
			});
		}else if(tabName.equals("Sales Order")){
			if(form.globalSalesOrderList==null||form.globalSalesOrderList.size()==0){
				return;
			}
			ArrayList<SalesOrder> payarray=new ArrayList<SalesOrder>(form.globalSalesOrderList);
			csvservice.setSalesOrderList(payarray, new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+61;
					Window.open(url, "test", "enabled");
				}
			});
		}else if(tabName.equals("Delivery Note")){
			if(form.globalDeliveryNoteList==null||form.globalDeliveryNoteList.size()==0){
				return;
			}
			ArrayList<DeliveryNote> payarray=new ArrayList<DeliveryNote>(form.globalDeliveryNoteList);
			csvservice.setdeliverynotelist(payarray, new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				@Override
				public void onSuccess(Void result) {
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+62;
					Window.open(url, "test", "enabled");
				}
			});
		}
	}

	public boolean validateCustomer() {
		if(newcustomerPopup.getTbFullName().getValue().equals("")){
			form.showDialogMessage("Please add Customer Fullname..!");
			return false;
		}
		if(newcustomerPopup.getOlbbBranch().getSelectedIndex()==0){
			form.showDialogMessage("Branch is mandetory..!");
			return false;
		}
		if(newcustomerPopup.getTbCellNo().getValue().equals("") && newcustomerPopup.getEtbEmail().getValue().equals("")){
			form.showDialogMessage("Please Fill either customer cell or customer email..!");
			return false;
		}
		if(newcustomerPopup.getAddressComp().getValue()==null){
			form.showDialogMessage("Please add address..!");
			return false;
		}
		if(!newcustomerPopup.getTbFullName().getValue().equals("") || !newcustomerPopup.getTbCompanyName().getValue().equals("")  ){
			if(newcustomerPopup.getAddressComp().getAdressline1().getValue().equals("") || newcustomerPopup.getAddressComp().getCountry().getSelectedIndex()==0 || newcustomerPopup.getAddressComp().getState().getSelectedIndex()==0 || newcustomerPopup.getAddressComp().getCity().getSelectedIndex()==0 ){
				form.showDialogMessage("Please add new customer complete address!");
				return false;
			}
		}
		if(newcustomerPopup.getTbGstNumber().getValue().trim()!=null && !newcustomerPopup.getTbGstNumber().getValue().trim().equals("")){
			if(newcustomerPopup.getTbGstNumber().getValue().trim().length()>15 || newcustomerPopup.getTbGstNumber().getValue().trim().length()<15){
				form.showDialogMessage("GSTIN number must be 15 characters!");
				return false;
			}
		}
		return true;
	}
	
	private void saveNewCustomer() {
		final Customer cust=new Customer();
		
		if(!newcustomerPopup.getTbFullName().getValue().trim().equals("")){
			cust.setFullname(newcustomerPopup.getTbFullName().getValue().toUpperCase().trim());
		}
		if(!newcustomerPopup.getTbCompanyName().getValue().trim().equals("")){
			cust.setCompanyName(newcustomerPopup.getTbCompanyName().getValue().toUpperCase().trim());
			cust.setCompany(true);
		}
		if(!newcustomerPopup.getTbCellNo().getValue().equals("")){
			cust.setCellNumber1(Long.parseLong(newcustomerPopup.getTbCellNo().getValue().trim()));
		}
		if(!newcustomerPopup.getEtbEmail().getValue().equals("")){
			cust.setEmail(newcustomerPopup.getEtbEmail().getValue().trim());
		}
		if(newcustomerPopup.getOlbbBranch().getSelectedIndex()!=0){
			cust.setBranch(newcustomerPopup.getOlbbBranch().getValue(newcustomerPopup.getOlbbBranch().getSelectedIndex()).trim());
		}
		boolean addressFlag=false;
		if(newcustomerPopup.getAddressComp().getValue()!=null){
			cust.setAdress(newcustomerPopup.getAddressComp().getValue());
			cust.setSecondaryAdress(newcustomerPopup.getAddressComp().getValue());
			addressFlag=true;
		}
		if(newcustomerPopup.getTbGstNumber().getValue()!=null&&!newcustomerPopup.getTbGstNumber().getValue().equals("")){
			ArticleType articalType=new ArticleType();
			articalType.setDocumentName("Invoice");
			articalType.setArticleTypeName("GSTIN");
			articalType.setArticleTypeValue(newcustomerPopup.getTbGstNumber().getValue());
			articalType.setArticlePrint("Yes");
			cust.getArticleTypeDetails().add(articalType);
		}
		if(addressFlag){
			cust.setNewCustomerFlag(false);
		}else{
			cust.setNewCustomerFlag(true);
		}
		form.showWaitSymbol();
		service.save(cust,new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("An unexpected error occurred");
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				newcustomerPopup.hidePopUp();
				cust.setCount(result.count);
				setCustomerDetails(cust);
				form.hideWaitSymbol();
			}
		});
	}
	
	public void setCustomerDetails(Customer entity){
		try {
			PersonInfo info=new PersonInfo();
			if(entity.isCompany()==true){
				info.setCount(entity.getCount());
				info.setFullName(entity.getCompanyName());
				info.setCellNumber(entity.getCellNumber1());
				info.setPocName(entity.getFullname());
			}else{
				info.setCount(entity.getCount());
				info.setFullName(entity.getFullname());
				info.setCellNumber(entity.getCellNumber1());
				info.setPocName(entity.getFullname());
			}
			PersonInfoComposite.globalCustomerArray.add(info);
//			setNewCustomerDetails(info);
			form.showDialogMessage("Customer Saved Successfully!");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	public void setNewCustomerDetails(PersonInfo pInfo){
		try {
			form.personInfo.getId().setValue(pInfo.getCount()+"");
			form.personInfo.getName().setValue(pInfo.getFullName());
			form.personInfo.getTbpocName().setValue(pInfo.getPocName());
			form.personInfo.getPhone().setValue(pInfo.getCellNumber()+"");
			form.personInfo.setPersonInfoHashMap(pInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private boolean validateAllSelectedBillingDocument() {
		String msg = "";
		boolean temp = true;
		List<BillingDocument> billingdoc = getAllSelectedRecords();
		if (billingdoc.size() == 0) {
			SC.say("No Billing Document selected. Please select atleast one Billing Document!");
			return false;
		}

		boolean valCustomer = validateCustomer(billingdoc);
		boolean valAccountType = validateAccountType(billingdoc);
		boolean valApproverName = validateApproverName(billingdoc);
		boolean valStatus = validateStatus(billingdoc);
		
		if (valCustomer == false) {
			msg = msg+ "Customer of all selected Billing Document should be same "+ "\n";
			temp = false;
		}

		if (valAccountType == false) {
			msg = msg+ "Account Type of all selected Billing Document should be same "+ "\n";
			temp = false;
		}


		boolean branchValidationFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument","EnableSetBranchAsServicingBranch");
		diffrentBranchflag = false;

		
//		boolean valBranch = validateContractBranch(billingdoc,branchValidationFlag);
		
		if (valStatus == false) {
			msg = msg+ "Status of all selected Billing Document should be Approved/Created"+ "\n";
			temp = false;
		}
		if (temp == false) {
			SC.say(msg);
			return false;
		}

//		if (!diffrentBranchflag) { 
//			if (!valBranch) {
//				return false;
//			}
//		}

		return true;
	}
	
	private List<BillingDocument> getAllSelectedRecords(){
		 List<BillingDocument> lisbillingtable=form.globalBillingDocList;
		 ArrayList<BillingDocument> selectedlist=new ArrayList<BillingDocument>();
		 for(int i=0;i<form.dgBill.getSelection().length;i++){
			 int billId=Integer.parseInt(form.dgBill.getSelection()[i].getAttribute("billingId").trim());
			 for(BillingDocument doc:lisbillingtable){
				 if(billId==doc.getCount()){
					 BillingDocument obj=doc.MyClone();
					 selectedlist.add(obj);
				 }
			 }
		 }
		 return selectedlist;
	 }
	
	private boolean validateApproverName(List<BillingDocument> lis) {
		String baseApproverName = "";
		if (lis.get(0).getApproverName() != null) {
			baseApproverName = lis.get(0).getApproverName();
			for (int i = 0; i < lis.size(); i++) {
				if (!baseApproverName.equals(lis.get(i).getApproverName()))
					return false;
			}
		}
		return true;
	}
	 
	 private boolean validateStatus(List<BillingDocument> lis){
		 String baseStatus="Approved";
		 for(int i=0;i<lis.size();i++){
			if(!baseStatus.equals(lis.get(i).getStatus())&&!lis.get(i).getStatus().equals("Created"))
				return false;
		 }
	 	return true;
	 }
	 
	 private List<SalesOrderProductLineItem> fillBillingProductTableOnClick(){
		 List<BillingDocument> listsel=getAllSelectedRecords();
		 ArrayList<SalesOrderProductLineItem> arrBilingDoc=new ArrayList<SalesOrderProductLineItem>();
		 int productSrNo=0;
		 for(int i=0;i<listsel.size();i++){
			 for(SalesOrderProductLineItem item:listsel.get(i).getSalesOrderProducts()){
				 productSrNo++;
				 item.setProductSrNumber(productSrNo);
				 arrBilingDoc.add(item);
			 }
		 }
		 return arrBilingDoc;
	 }
	 
	 private List<ContractCharges> fillBillingOtherChargesTableOnClick(){
		 List<BillingDocument> listsel=getAllSelectedRecords();
		 ArrayList<ContractCharges> arrBilingDoc=new ArrayList<ContractCharges>();
		 ArrayList<ContractCharges> arrBilingCombineDoc=new ArrayList<ContractCharges>();
		 for(int i=0;i<listsel.size();i++){
			 if(listsel.get(i).getBillingOtherCharges().size()!=0){
				arrBilingDoc.addAll(listsel.get(i).getBillingOtherCharges());
			 }
		 }
		 for(int k=0;k<arrBilingDoc.size();k++){
			System.out.println("ORDER ID "+arrBilingDoc.get(k).getOrderId()+" VIEW TAX "+arrBilingDoc.get(k).getTaxChargeName());
		 }
		 System.out.println();
		 return arrBilingDoc;
	 } 
	 
	 public boolean isMultipleContractBilling(){
		System.out.println("INSIDE CHECKING MULTIPLE CONTRACT BILLING......");
		List<BillingDocumentDetails> billtablelis = fillBillingDocumentTableOnClick();
		
		int contractId=billtablelis.get(0).getOrderId();
		for(int i=0;i<billtablelis.size();i++){
			if(billtablelis.get(i).getOrderId()!=contractId){
				return true;
			}
		}
		return false;
	 }
	 	
	 private boolean validateCustomer(List<BillingDocument> lis){
		 System.out.println("Inside Customer Vallidation !!!");
		 int baseCustomerId=0;
		 if(lis.get(0).getPersonInfo().getCount()!=0){
			 baseCustomerId=lis.get(0).getPersonInfo().getCount();
			 System.out.println("Customer Count :: "+baseCustomerId);
			 for(int i=0;i<lis.size();i++){
				System.out.println("Loop Customer Count :: "+lis.get(i).getPersonInfo().getCount());
				if(baseCustomerId!=lis.get(i).getPersonInfo().getCount()){
					System.out.println("Inside CUST COUNT Not Matched !!!");
					return false;
				}
			 }
		 }
		 return true;
	 }
	 
	 private boolean validateAccountType(List<BillingDocument> lis){
		 String baseAccountType="";
		 if(lis.get(0).getAccountType()!=null){
			 baseAccountType=lis.get(0).getAccountType();
			 for(int i=0;i<lis.size();i++){
				if(baseAccountType!=lis.get(i).getAccountType())
					return false;
			 }
		 }
	 	return true;
	 }
	 
	
	private boolean validateContractBranch(List<BillingDocument> lis,boolean branchValidationFlag) {
		String branch = "";
		if (lis.get(0).getContractCount() != null) {
			branch = lis.get(0).getBranch();
			for (int i = 0; i < lis.size(); i++) {
				if (lis.get(i).getBranch().trim() != "") {
					if (!branch.trim().equalsIgnoreCase(lis.get(i).getBranch().trim())) {
						if (branchValidationFlag) {
							diffrentBranchflag = true;
						}
						if (!branchValidationFlag)
							form.showDialogMessage("All Billing Document Branch Should be same.");
						return false;
					}
				} else {
					if (branchValidationFlag){
						diffrentBranchflag = true;
					}
					if (!branchValidationFlag)
						form.showDialogMessage("All Billing Document Branch Should not be blank.");
					return false;
				}
			}
		}
		return true;
	}
	 
	 private List<BillingDocumentDetails> fillBillingDocumentTableOnClick(){
		 List<BillingDocument> listsel=getAllSelectedRecords();
		 ArrayList<BillingDocumentDetails> arrBilingDoc=new ArrayList<BillingDocumentDetails>();
		 for(int i=0;i<listsel.size();i++){
			 BillingDocumentDetails billingdoc=new BillingDocumentDetails();
			 billingdoc.setOrderId(listsel.get(i).getContractCount());
			 billingdoc.setBillId(listsel.get(i).getCount());
			 billingdoc.setBillingDate(listsel.get(i).getBillingDate());
			 billingdoc.setBillingCategory(listsel.get(i).getBillingCategory());
			 billingdoc.setBillingGroup(listsel.get(i).getBillingGroup());
			 billingdoc.setBillingType(listsel.get(i).getBillingType());
			 billingdoc.setBillAmount(listsel.get(i).getTotalBillingAmount());
			 billingdoc.setBillstatus(listsel.get(i).getStatus());
			 arrBilingDoc.add(billingdoc);
		 }
		 return arrBilingDoc;
	 }
	 
	 private BillingDocument initializeToNewBillingDetails() {
		 
		 List<BillingDocument> listsel=getAllSelectedRecords();
		 
		 String subBillType = listsel.get(0).getSubBillType().trim();
		 for(BillingDocument doc : listsel){
			 if(doc.getSubBillType() != null && !doc.getSubBillType().equals("")){
				 if(Invoice.checkBillSubType(doc.getSubBillType())){
					 subBillType = AppConstants.CONSOLIDATEBILL;
					 break;
				 }
			 }
		 }
		 
		 List<BillingDocumentDetails> billingTableData=this.fillBillingDocumentTableOnClick();
		 final ArrayList<BillingDocumentDetails> multipleBillDocs=new ArrayList<BillingDocumentDetails>();
		 multipleBillDocs.addAll(billingTableData);
		 
		 // use for merging products of multiple contract or orders 
		 List<SalesOrderProductLineItem> billingProductTableData=this.fillBillingProductTableOnClick();
		 final ArrayList<SalesOrderProductLineItem> multipleBillProdDocs=new ArrayList<SalesOrderProductLineItem>();
		 multipleBillProdDocs.addAll(billingProductTableData);

		 List<OtherCharges> otherChrgList=fillBillingOtherChargesTable();
		 ArrayList<OtherCharges> othChrgsList=new ArrayList<OtherCharges>(otherChrgList);
		 double sumOfOtherChargesAmt=0;
		 for(OtherCharges obj:othChrgsList){
			 sumOfOtherChargesAmt=sumOfOtherChargesAmt+obj.getAmount();
		 }
		 
		 double sumOfTotalAmount=0;
		 double sumOfDiscountAmt=0;
		 double sumOfRoundOffAmt=0;
		 HashSet<Integer> serivceId = new HashSet<Integer>();
		 for(BillingDocument entity:listsel){
			for(SalesOrderProductLineItem obj:entity.getSalesOrderProducts()){
				 if(obj.getBasePaymentAmount()!=0){
					 sumOfTotalAmount=sumOfTotalAmount+obj.getBasePaymentAmount();
				 }else{
					 sumOfTotalAmount=sumOfTotalAmount+obj.getBaseBillingAmount(); 
				 }
			}
			sumOfDiscountAmt=sumOfDiscountAmt+entity.getDiscountAmt();
			sumOfRoundOffAmt=sumOfRoundOffAmt+entity.getRoundOffAmt();
			 
			if(entity.getRateContractServiceId()!=0){
				serivceId.add(entity.getRateContractServiceId());
			}else if(entity.getServiceId().size()>0){
				serivceId.addAll(entity.getServiceId());
			}
		 }
		
		 
		 // Calculate billing amount using selected billing documents
		 final double calBillingAmount=this.calculateTotalBillingAmount(billingTableData);
		 final String typeOrder=listsel.get(0).getTypeOfOrder().trim();
		 // Get Order Count
		 final int contractid=listsel.get(0).getContractCount();
		 final int billingid=listsel.get(0).getCount();
		
//		 BillingDocument billingEntity=listsel.get(0);
		 BillingDocument billingEntity=new BillingDocument();
		 billingEntity=listsel.get(0).MyClone();
		 Console.log("Get contract count in -- " + billingEntity.getContractCount());
		 billingEntity.setTotalAmount(sumOfTotalAmount);
		 billingEntity.setDiscountAmt(sumOfDiscountAmt);
		 billingEntity.setFinalTotalAmt(sumOfTotalAmount-sumOfDiscountAmt);
		 billingEntity.setOtherCharges(othChrgsList);
		 billingEntity.setTotalOtherCharges(sumOfOtherChargesAmt);
		 billingEntity.setRoundOffAmt(sumOfRoundOffAmt);
		 billingEntity.setSalesOrderProducts(multipleBillProdDocs);
		 billingEntity.setArrayBillingDocument(multipleBillDocs);
		
//		 form.setToViewState();
		 final List<BillingDocument> selectedlist=getAllSelectedRecords();
		 	
	 	String cncProjectName = "";
	 	if(selectedlist.get(0).getProjectName()!=null){
	 		cncProjectName = selectedlist.get(0).getProjectName();
	 	}
	 	String cncContractNumber = "";
	 	if(selectedlist.get(0).getCncContractNumber()!=null){
	 		cncContractNumber = selectedlist.get(0).getCncContractNumber();
	 	}
	 	
		billingEntity.setProjectName(cncProjectName);
		billingEntity.setCncContractNumber(cncContractNumber);

		int creditperiod =  selectedlist.get(0).getCreditPeriod();
		billingEntity.setCreditPeriod(creditperiod);
		
	 	Collections.sort(selectedlist, new Comparator<BillingDocument>() {
			@Override
			public int compare(BillingDocument o1, BillingDocument o2) {
				Integer count1= o1.getCount();
				Integer count2 = o2.getCount();
				return count1.compareTo(count2);
			}
		});
		Date billingperiodTodate= null;	
		Date billingperiodFromdate= null;	
		boolean multiContractFlag=false;
		for(int i=0;i<selectedlist.size();i++){
			if(i==0){
				billingperiodFromdate=selectedlist.get(i).getBillingPeroidFromDate();
			}
			if(i==selectedlist.size()-1){
				billingperiodTodate = selectedlist.get(i).getBillingPeroidToDate();
			}
			
			if(selectedlist.get(i).getProjectName()!=null && !selectedlist.get(i).getProjectName().equals(cncProjectName)){
				billingEntity.setProjectName("");
			}
			if(selectedlist.get(i).getCncContractNumber()!=null && !selectedlist.get(i).getCncContractNumber().equals(cncContractNumber)){
				billingEntity.setCncContractNumber("");
			}
			if(selectedlist.get(i).getCreditPeriod() != creditperiod){
				billingEntity.setCreditPeriod(0);
			}
		}
		
		List<ContractCharges> blankList=new ArrayList<ContractCharges>();
		billingEntity.setBillingOtherCharges(blankList);
		
		if(isMultipleContractBilling()){
			billingEntity.setTotalSalesAmount(0);
			multiContractFlag=true;
		}
		if(billingperiodFromdate!=null)
			billingEntity.setBillingPeroidFromDate(billingperiodFromdate);
		if(billingperiodTodate!=null)
			billingEntity.setBillingPeroidToDate(billingperiodTodate);
		billingEntity.setStatus(BillingDocument.APPROVED);
		
		
		if(multiContractFlag){
			Date Todate= selectedlist.get(0).getBillingPeroidToDate(), fromDate = selectedlist.get(0).getBillingPeroidFromDate();
			for(int i=0;i<selectedlist.size();i++){
				if(fromDate.before(selectedlist.get(i).getBillingPeroidFromDate())){
					fromDate = selectedlist.get(i).getBillingPeroidFromDate();
				}
				if((selectedlist.get(i).getBillingPeroidToDate()).after(Todate) ){
					Todate = selectedlist.get(i).getBillingPeroidToDate();
				}
			}
			
//			form.getDbbillingperiodFromDate().setValue(fromDate);
//			form.getDbbillingperiodToDate().setValue(Todate);
			 
			Date contractTodate= selectedlist.get(0).getBillingPeroidToDate(), contractfromDate = selectedlist.get(0).getContractStartDate();
			 
			for(int i=0;i<selectedlist.size();i++){
				if(selectedlist.get(i).getContractStartDate()!=null && fromDate.before(selectedlist.get(i).getContractStartDate())){
					contractfromDate = selectedlist.get(i).getContractStartDate();
				}
				if(selectedlist.get(i).getContractEndDate()!=null && (selectedlist.get(i).getContractEndDate()).after(Todate) ){
					contractTodate = selectedlist.get(i).getContractEndDate();
				}
			}
			
			billingEntity.setRemark("This Invoice is Created against multiple contract of same customer As dated on :" +AppUtility.parseDate(new Date()));
			billingEntity.setContractStartDate(contractfromDate);
			billingEntity.setContractEndDate(contractTodate);
			billingEntity.setConStartDur(contractfromDate);
			billingEntity.setConEndDur(contractTodate);
			
			billingEntity.setBillingPeroidFromDate(fromDate);
			billingEntity.setBillingPeroidToDate(Todate);
		}

		boolean valApproverName = validateApproverName(listsel);
		
		if(!valApproverName){
			billingEntity.setApproverName(LoginPresenter.loggedInUser);
		}
		
//		if(diffrentBranchflag){
//			billingEntity.setBranch("");
//		}
		
		if(serivceId.size()>0){
			billingEntity.getServiceId().clear();
			billingEntity.getServiceId().addAll(serivceId);
		}
		
		billingEntity.setSubBillType(subBillType);
		if(subBillType.equalsIgnoreCase(AppConstants.CONSOLIDATEBILL)){
			int count = 0;
			if(billingEntity.getRefNumber() != null && !billingEntity.getRefNumber().equalsIgnoreCase("")){
				try{
				count = Integer.parseInt(billingEntity.getRefNumber());
				}catch(Exception e){
					count = 0;
				}
			} 
			billingEntity.setContractCount(count);
		}
		
		
		double managementFeePer=0;
		for(BillingDocument bill:selectedlist){
			managementFeePer+=bill.getManagementFeesPercent();
		}
		if(managementFeePer!=0){
			managementFeePer=managementFeePer/selectedlist.size();
		}
		billingEntity.setManagementFeesPercent(managementFeePer);
		
		billingEntity.setTypeOfOrder(typeOrder);
		

		
//		Timer t = new Timer() {
//			@Override
//			public void run() {
//				Console.log("Get contract count in set view-- " + billingEntity.getContractCount());
//				form.updateView(billingEntity);
//				form.getDototalbillamt().setValue(calBillingAmount);
//				BillingDetailsPresenter presenter=(BillingDetailsPresenter) form.getPresenter();
//			    presenter.form.setMenus();
//			    presenter.multipleContractBillingInvoiceFlag=true;
//				form.getBillingDocumentTable().getDataprovider().getList().addAll(multipleBillDocs);
//				System.out.println("BILLING DOCUMNET TABLE......! "+form.getBillingDocumentTable().getDataprovider().getList().size());
//				form.getSalesProductTable().getDataprovider().setList(multipleBillProdDocs);
//				System.out.println("BILLING PRODUCT TABLE......! "+form.getSalesProductTable().getDataprovider().getList().size());
//				presenter.reactOnProductsChange();
//				BillingDetailsForm.flag=false;
//				BillingDetailsForm.flag1=true;
//				
//				form.setToViewState();
//				form.getSalesProductTable().getTable().redraw();
//				form.getSalesProductTable().setEnable(false);
//			}
//		};
//		t.schedule(2000);
		
		return billingEntity;
	 }
	 
	 private List<OtherCharges> fillBillingOtherChargesTable() {
		 List<BillingDocument> listsel=getAllSelectedRecords();
		 ArrayList<OtherCharges> arrBilingOtherCharges=new ArrayList<OtherCharges>();
		
		 for(BillingDocument object:listsel){
			 if(object.getOtherCharges()!=null){
				 for(OtherCharges oc:object.getOtherCharges()){
					oc.setOrderId(object.getContractCount());
					oc.setBillingId(object.getCount());
				 }
				 arrBilingOtherCharges.addAll(object.getOtherCharges()); 
			 }
		 }
		 return arrBilingOtherCharges;
	}
	 
	 private double calculateTotalBillingAmount(List<BillingDocumentDetails> liscalbilling){
		 System.out.println("In Calculate Total Billing Amount");
		 System.out.println("Size Here CTBA"+liscalbilling.size());
		 double billingAmt=0;
		 for(int i=0;i<liscalbilling.size();i++){
			 billingAmt=billingAmt+liscalbilling.get(i).getBillAmount();
		 }
		 System.out.println("Calculated Form Bill AMt"+billingAmt);
		 return billingAmt;
	 }
	 
	public void reactOnCloseBtnOfSalesLead(){
		if(salesLeadPopup!=null){
			if(salesLeadPopup.getPresenter()!=null){
				if(salesLeadPopup.getPresenter().getModel() instanceof Lead){
					Console.log("On CLICK OF save LEAD...");
					if(salesLeadObj==null){
						Console.log("NEW salesLeadObj ...");
						salesLeadObj=(Lead) salesLeadPopup.getPresenter().getModel();
						if(salesLeadObj!=null&&salesLeadObj.getCount()!=0){
							if(form.globalSalesLeadList==null){
								form.globalSalesLeadList=new ArrayList<Lead>();
							}
							form.globalSalesLeadList.add(salesLeadObj);
							form.dgSalesLead.getDataSource().addData(form.addDateToSalesLeadDG(salesLeadObj));
							form.dgSalesLead.redraw();
						}
					}else{
						Console.log("EXISTING salesLeadObj ...");
						if(form.globalSalesLeadList==null){
							form.globalSalesLeadList=new ArrayList<Lead>();
						}
						for(Lead obj:form.globalSalesLeadList){
							if(obj.getCount()==salesLeadObj.getCount()){
								form.globalSalesLeadList.remove(obj);
								form.globalSalesLeadList.add(salesLeadObj);
							}
						}
						if(selectedRecord!=null){
							form.dgSalesLead.getDataSource().removeData(selectedRecord);
							form.dgSalesLead.getDataSource().addData(form.addDateToSalesLeadDG(salesLeadObj));
							form.dgSalesLead.redraw();
						}
					}
					salesLeadObj=null;
				}
			}
		}
		
	}
	
	public void reactOnCloseBtnOfServiceLead(){
		
	}
	

	
	public void applyHandlerOnProcessBarMenu(String tabName){
		Console.log("applyHandlerOnProcessBarMenu "+tabName);
		viewedScreenName=tabName;
		if(tabName.equals("Sales Lead")){
			LeadForm view=(LeadForm) salesLeadPopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
		else if(tabName.equals("Purchase Order")){
			PurchaseOrderForm view=(PurchaseOrderForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
		else if(tabName.equals("Delivery Note")||tabName.equals("View Delivery Note")){
			DeliveryNoteForm view=(DeliveryNoteForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
		else if(tabName.equals("Service Lead")){
			LeadForm view=(LeadForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
		else if(tabName.equals("Service Quotation")){
			QuotationForm view=(QuotationForm) genralInvoicePopup.getPresenter().getView();
			
			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
		else if(tabName.equals("Service Order")||tabName.equals("AMC")){
			ContractForm view=(ContractForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
		
		else if(tabName.equals("Quick Service Order")){
		QuickContractForm view=(QuickContractForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
		else if(tabName.equals("Service Contract")||tabName.equals("Contract")){
			ContractForm view=(ContractForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
		else if(tabName.equals("Quick Service Contract")){
			QuickContractForm view=(QuickContractForm) genralInvoicePopup.getPresenter().getView();
			Console.log("applyHandler- QuickContractForm view created");
			if (view.getProcessLevelBar() != null) {
				Console.log("applyHandler- inside if");
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
			Console.log("applyHandler- after if");
		}
		else if(tabName.equals("Contract")){
			ContractForm view=(ContractForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
		else if(tabName.equals("Customer Service List")){
			ServiceForm view=(ServiceForm) genralInvoicePopup.getPresenter().getView();
			
			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
					Console.log("in for"+lbl[k]);
				}
			}
		}
		else if(tabName.equals("View Bill")||tabName.equals("Bill")){
			BillingDetailsForm view=(BillingDetailsForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}else if(tabName.equals("Device")){
			DeviceForm view=(DeviceForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}else if(tabName.equals("Work Order")||tabName.equals("Create Work Order")){
			WorkOrderForm view=(WorkOrderForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
		else if(tabName.equals("Contract Renew")){
			ContractForm view=(ContractForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
		else if(tabName.equals("GRN")){
			GRNForm view=(GRNForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
		else if(tabName.equals("View Invoice")||tabName.equals("Invoice")||tabName.equals("View Invoice Payment")){
			InvoiceDetailsForm view=(InvoiceDetailsForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
		else if(tabName.equals("View Payment")||tabName.equals("Payment")){
			PaymentDetailsForm view=(PaymentDetailsForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
		else if(tabName.equals("Sales Order")){
			SalesOrderForm view=(SalesOrderForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}else if(tabName.equals("Sales Quotation")){
			SalesQuotationForm view=(SalesQuotationForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}else if(tabName.equals("Project")){
			ProjectForm view=(ProjectForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
	}
	
	
	private void reactOnPopupNavigation(ClickEvent event) {
		Console.log("SummaryPresenter-reactOnPopupNavigation");
		String text="";
		
		try{
			InlineLabel label=(InlineLabel) event.getSource();
			text=label.getText().trim();
			Console.log("Summary "+text);
		
		
		if(text.equals("")){
			return;
		}
		Console.log("salTabSelIndex "+salTabSelIndex);
		Console.log("serTabSelIndex "+serTabSelIndex);
		Console.log("accTabSelIndex "+accTabSelIndex);
		Console.log("viewedScreenName "+viewedScreenName);
		
		if(salTabSelIndex!=-1){
			if(salTabSelIndex==0){
				if(!viewedScreenName.equals("")&&viewedScreenName.equals("Sales Lead")){
					Lead lead=(Lead) salesLeadPopup.getPresenter().getModel();
					reactOnCloseBtnOfSalesLead();
//					salesLeadPopup.viewDocumentPanel.hide();
					if(text.equals("Create Quotation")){	
						salesLeadPopup.viewDocumentPanel.hide();
						Console.log("Summary LEAD POPUP : Create Quotation clicked!!");
						reactOnCreateSalesQuotationFromSalesLead(lead);
						return;
					}else if(text.equals("Create Order")){
						salesLeadPopup.viewDocumentPanel.hide();
						Console.log("Summary LEAD POPUP : Create Order clicked!!");
						reactOnCreateSalesOrderFromSalesLead(lead);
						return;
					}else if(text.equals("Quick Order")){
						salesLeadPopup.viewDocumentPanel.hide();
						Console.log("Summary LEAD POPUP : Quick Order clicked!!");
						reactOnCreateQuickSalesOrderFromSalesLead(lead);
						return;
					}else if(text.equals("New")){
						salesLeadPopup.viewDocumentPanel.hide();
						Console.log("Summary LEAD POPUP : New clicked!!");
						reactOnNew("Sales Lead");
						return;
					}
					
				}
				else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Sales Order")){
					SalesOrder salesOrder=(SalesOrder) genralInvoicePopup.getPresenter().getModel();
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("Create AMC")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : Create AMC!!");
						reactOnCreateAMCFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("View Delivery Note")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : View Delivery Note!!");
						reactOnViewDeliveryNoteFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : New clicked!!");
						reactOnNew("Sales Order");
						return;
					}else if(text.equals("Create Work Order")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : Create Work Order!!");
						reactOnCreateWorkOrderFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("View AMC")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : View AMC!!");
						reactOnViewAMCFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("Create PO")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : Create PO!!");
						reactOnCreatePOFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("View PO")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : View PO!!");
						reactOnViewPOFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("View Bill")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : View Bill!!");
						reactOnViewBillFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("Copy Order")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : Copy Order!!");
						reactOnCopyOrderFromSalesOrder(salesOrder);
						return;
					}
					
				}
				else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Contract")){

					Contract contrct=(Contract)genralInvoicePopup.getPresenter().getModel();				
//					genralInvoicePopup.viewDocumentPanel.hide();		
					Console.log("text="+text);
					if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Contract POPUP in reacttopopup : New clicked!!");
						reactOnNew("Contract");
						return;
					}
					else if(text.equals("View Services")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Contract POPUP in reacttopopup : View Services!!");
						reactOnViewServicesFromServiceContract(contrct);
						return;
					}
					else if(text.equals("View Bill")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Contract POPUP in reacttopopup : View Bill!!");
						reactOnViewBillFromServiceContract(contrct);
						return;
					}
					else if(text.equals("Create Work Order")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Contract POPUP in reacttopopup : Create work order!!");
						reactOnCreateWorkOrderFromServiceContract(contrct);
						return;
					}
					else if(text.equals("Contract Renew")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Contract POPUP in reacttopopup : Contract Renew!!");
						reactOnContractRenewFromServiceContract(contrct);
						return;
					}
					
				}else {
					documentIDClickOnAnotherTab(text);
				}
				
				
			}else if(salTabSelIndex==1){
				if(!viewedScreenName.equals("")&&viewedScreenName.equals("Sales Quotation")){
					SalesQuotation salesQuotation=(SalesQuotation) genralInvoicePopup.getPresenter().getModel();
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("Create Sales Order")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Quotation POPUP : Create Sales Order!!");
						reactOnCreateSalesOrderFromSalesQuotation(salesQuotation);
						return;
					}else if(text.equals("Revised Quotation")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Quotation POPUP : Revised Quotation clicked!!");
						reactOnRevisedQuotationFromSalesQuotation(salesQuotation);
						return;
					}	else if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Quotation POPUP : New clicked!!");
						reactOnNew("Sales Quotation");
						return;
					}														
				}else {
					documentIDClickOnAnotherTab(text);
				}
			}else if(salTabSelIndex==2){
				if(!viewedScreenName.equals("")&&viewedScreenName.equals("Sales Order")){
					SalesOrder salesOrder=(SalesOrder) genralInvoicePopup.getPresenter().getModel();
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("Create Work Order")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : Create Work Order!!");
						reactOnCreateWorkOrderFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("Create AMC")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : Create AMC!!");
						reactOnCreateAMCFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("View AMC")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : View AMC!!");
						reactOnViewAMCFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("Create PO")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : Create PO!!");
						reactOnCreatePOFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("View PO")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : View PO!!");
						reactOnViewPOFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("View Bill")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : View Bill!!");
						reactOnViewBillFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("View Delivery Note")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : View Delivery Note!!");
						reactOnViewDeliveryNoteFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("Copy Order")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : Copy Order!!");
						reactOnCopyOrderFromSalesOrder(salesOrder);
						return;
					}
					else if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : New clicked!!");
						reactOnNew("Sales Order");
						return;
					}
				}
				else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Work Order")){
					//WorkOrder workOrder=(WorkOrder) genralInvoicePopup.getPresenter().getModel();
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("SalesOrder Work Order POPUP : New Clicked!!");
						reactOnNew("Work Order");
						return;
					}
				}
				else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Contract")){
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("SalesOrder Create AMC POPUP : New Clicked!!");
						reactOnNew("Contract");
						return;
					}
				}
				else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Purchase Order")){
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("SalesOrder Create PO POPUP : New Clicked!!");
						reactOnNew("Purchase Order");
						return;
					}
//					AppConstants.VIEWSALESORDER,AppConstants.VIEWGRN
					
				}
				else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Bill")){
					BillingDocument BillingDocument=(BillingDocument) genralInvoicePopup.getPresenter().getModel();
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("View Order")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("SalesOrder Bill POPUP : View Order clicked!!");
						reactOnViewOrderFromBill(BillingDocument);
						return;
					}
					if(text.equals("View Service")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("SalesOrder Bill POPUP : View Service clicked!!");
						reactOnViewServiceFromBill(BillingDocument);
						return;
					}
				}
				else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Delivery Note")){
					DeliveryNote deliveryNote=(DeliveryNote) genralInvoicePopup.getPresenter().getModel();
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("View Sales Order")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("SalesOrder DeliveryNote POPUP : View Sales Order clicked!!");
						reactOnViewSalesOrderFromDeliveryNote(deliveryNote);
						return;
					}
					if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("SalesOrder DeliveryNote POPUP : New clicked!!");
						reactOnNew("Delivery Note");
						return;
					}
				}else {
					documentIDClickOnAnotherTab(text);
				}
				
			}else if(salTabSelIndex==3){
				if(!viewedScreenName.equals("")&&viewedScreenName.equals("Delivery Note")){
					DeliveryNote deliveryNote=(DeliveryNote) genralInvoicePopup.getPresenter().getModel();
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("View Sales Order")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("SalesOrder DeliveryNote POPUP : View Sales Order clicked!!");
						reactOnViewSalesOrderFromDeliveryNote(deliveryNote);
						return;
					}
					if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("SalesOrder DeliveryNote POPUP : New clicked!!");
						reactOnNew("Delivery Note");
						return;
					}
				}
				else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Sales Order")){
					SalesOrder salesOrder=(SalesOrder) genralInvoicePopup.getPresenter().getModel();
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("Create Work Order")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : Create Work Order!!");
						reactOnCreateWorkOrderFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("Create AMC")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : Create AMC!!");
						reactOnCreateAMCFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("View AMC")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : View AMC!!");
						reactOnViewAMCFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("Create PO")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : Create PO!!");
						reactOnCreatePOFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("View PO")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : View PO!!");
						reactOnViewPOFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("View Bill")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : View Bill!!");
						reactOnViewBillFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("View Delivery Note")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : View Delivery Note!!");
						reactOnViewDeliveryNoteFromSalesOrder(salesOrder);
						return;
					}else if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Sales Order POPUP : New clicked!!");
						reactOnNew("Sales Order");
						return;
					}				
				}else {
					documentIDClickOnAnotherTab(text);
				}
				
			}
		}
		else if(serTabSelIndex!=-1){
			if(serTabSelIndex==0){
				if(!viewedScreenName.equals("")&&viewedScreenName.equals("Service Lead")){
					Lead lead=(Lead) genralInvoicePopup.getPresenter().getModel();
					reactOnCloseBtnOfServiceLead();
//					genralInvoicePopup.viewDocumentPanel.hide();
					Console.log("text="+text);
					if(text.equals("Create Quotation")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Lead POPUP : Create Quotation clicked!!");
						reactOnCreateServiceQuotationFromServiceLead(lead);			
						return;
					}else if(text.equals("Create Order")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Lead POPUP : Create Order clicked!!");
						reactOnCreateServiceOrderFromServiceLead(lead);
						return;
					}else if(text.equals("Quick Order")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Lead POPUP : Quick Order clicked!!");
						reactOnCreateQuickServiceOrderFromServiceLead(lead);
						return;
					}else if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Lead POPUP : New clicked!!");
						reactOnNew("Service Lead");
						return;
					}else if(text.equals(AppConstants.VIEWORDER)) {
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Lead POPUP : View Order clicked!!");
						reactOnViewOrderFromServiceLead(lead);
						return;
					}
				}else {
					documentIDClickOnAnotherTab(text);
				}
				
				//need to write handlers if contract is opened from lead
			}
			else if(serTabSelIndex==1){
				Console.log("serTabSelIndex=1  Service Quotation tab selected");
				if(!viewedScreenName.equals("")&&viewedScreenName.equals("Service Quotation")){					
					Quotation quot=(Quotation)genralInvoicePopup.getPresenter().getModel();				
//					genralInvoicePopup.viewDocumentPanel.hide();
					Console.log("text="+text);
					if(text.equals("Create Contract")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Quotation POPUP : Create Contract clicked!!");
						reactOnCreateContractFromServiceQuotation(quot);
						return;
					}
					else if(text.equals("Create Quick Contract")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Quotation POPUP : Create Quick Contract clicked!!");
						reactOnCreateQuickContractFromServiceQuotation(quot);
						return;
					}
					else if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Quotation POPUP : New clicked!!");
						reactOnNew("Service Quotation");
						return;
					}
					else if(text.equals(AppConstants.VIEWLEAD)){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Quotation POPUP : View Lead clicked!!");
						reactOnViewLeadFromServiceQuotation(quot);
						return;
					}
					//"Revised Quotation"
				}else {
					documentIDClickOnAnotherTab(text);
				}
			}
			else if(serTabSelIndex==2){
				Console.log("serTabSelIndex=2 Service Contract tab selected");
				if(!viewedScreenName.equals("")&&viewedScreenName.equals("Contract")){
					Contract contrct=(Contract)genralInvoicePopup.getPresenter().getModel();				
//					genralInvoicePopup.viewDocumentPanel.hide();		
					Console.log("text="+text);
					if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Contract POPUP in reacttopopup : New clicked!!");
						reactOnNew("Contract");
						return;
					}
					else if(text.equals("View Services")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Contract POPUP in reacttopopup : View Services!!");
						reactOnViewServicesFromServiceContract(contrct);
						return;
					}
					else if(text.equals("View Bill")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Contract POPUP in reacttopopup : View Bill!!");
						reactOnViewBillFromServiceContract(contrct);
						return;
					}
					else if(text.equals("Create Work Order")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Contract POPUP in reacttopopup : Create work order!!");
						reactOnCreateWorkOrderFromServiceContract(contrct);
						return;
					}
					else if(text.equals("Contract Renew")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Contract POPUP in reacttopopup : Contract Renew!!");
						reactOnContractRenewFromServiceContract(contrct);
						return;
					}
					else if(text.equals(AppConstants.VIEWLEAD)){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Contract POPUP in reacttopopup : View Lead!!");
						reactOnViewLeadFromServiceContract(contrct);
						return;
					}
					else if(text.equals(AppConstants.VIEWQUOTATION)){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Contract POPUP in reacttopopup : View Quotation!!");
						reactOnViewQuotationFromServiceContract(contrct);
						return;
					}
					else if(text.equals("Copy")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service Contract POPUP in reacttopopup : Copy!!");
						reactOnCopyContract(contrct);
						return;
					}
					
				}
				else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Customer Service List")){
					Console.log("in if viewedScreenName.equals(Customer Service List)");	
					Service service=null;
					ServiceForm form=(ServiceForm) genralInvoicePopup.getPresenter().getView();
					List<Service> serviceList=form.getSuperTable().getValue();
					for(Service ser:serviceList)
					 {
						 if(ser.getRecordSelect()==true){
							 service=ser;
						 }
					 }
							
					Console.log("after service obj creation"+service.toString());
//					genralInvoicePopup.viewDocumentPanel.hide();	
					Console.log("text="+text);
					if(text.equals("View Document")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service POPUP in reacttopopup : View Document clicked!!");
						reactOnViewDocumentFromCustomerServiceList(service);
						return;
					}
					else if(text.equals("Contract")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Service POPUP in reacttopopup : Contract clicked!!");
						reactOnContractFromCustomerServiceList(service);
						return;
					}
				}
				else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Work Order")){
					WorkOrder workOrder=(WorkOrder) genralInvoicePopup.getPresenter().getModel();
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Contract Work Order POPUP : New Clicked!!");
						reactOnNew("Work Order");
						return;
					}
				}
				else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Device")){
					Console.log("In Device if text="+text);
					Service service=(Service) genralInvoicePopup.getPresenter().getModel();
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("New")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Contract Device POPUP : New Clicked!!");
						reactOnNew("Device");
						return;
					}
					else if(text.equals("View Bill")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Contract Device POPUP : View Bill Clicked!!");
						reactOnViewBillFromDevice(service);
						return;	
					}					
					else if(text.equals("Manage Customer Project")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Contract Device POPUP : Manage Customer Project Clicked!!");
						reactOnManageCustomerProjectFromDevice(service);
						return;	
					}
					else if(text.equals("View Order")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Contract Device POPUP : View Order Clicked!!");
						reactOnViewOrderFromDevice(service);
						return;	
					}
					
				}else {
					documentIDClickOnAnotherTab(text);
				}
			}
			
		}
		if(accTabSelIndex!=-1){
			if(accTabSelIndex==0){
				Console.log("in accounts bill tab and viewedscreenname="+viewedScreenName);
				if(!viewedScreenName.equals("")&&viewedScreenName.equals("Bill")){
					
					Console.log("in viewedScreenName.equals(Bill) if");
					BillingDocument billdoc=(BillingDocument)genralInvoicePopup.getPresenter().getModel();				
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("View GRN")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts POPUP : View GRN clicked!!");
						reactOnViewGRNFromAccountsBill(billdoc);
						return;
					}
					else if(text.equals("View Invoice")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts POPUP : View Invoice clicked!!");						
						reactOnViewInvoiceFromAccountsBill(billdoc);
						return;
					}
					else if(text.equals("View Order")){//2-09-2022
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts POPUP : View Order clicked!!");
						reactOnViewOrderFromAccountsBill(billdoc);
						return;
					}
					else if(text.equals("View Payment")){//2-09-2022
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts POPUP : View Payment clicked!!");
						reactOnViewPaymentFromAccountsBill(billdoc);
						return;
					}
					else if(text.equals("View Service")){//21-09-2022
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts POPUP : View Service clicked!!");
						reactOnViewServiceFromAccountsBill(billdoc);
						return;
					}
					
					
				}
				else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Invoice")){
					Console.log("in viewedScreenName.equals(View Invoice) if");
					Invoice invoice=(Invoice)genralInvoicePopup.getPresenter().getModel();	
//					genralInvoicePopup.viewDocumentPanel.hide();					
					if(text.equals("View Bill")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts-Bill-Invoice POPUP : View Bill clicked!!");
						reactOnViewBillFromAccountsBillInvoice(invoice);
						return;
					}else if(text.equals("View Payment")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts-Bill-Invoice POPUP : View Payment clicked!!");
						reactOnViewPaymentFromAccountsInvoice(invoice);
						return;
					}
					else if(text.equals("View Order")){//2-09-2022
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts-Bill-Invoice POPUP : View Order clicked!!");
						reactOnViewOrderFromAccountsInvoice(invoice);
						return;
					}
						
				}else {
					documentIDClickOnAnotherTab(text);
				}
				
			}else if(accTabSelIndex==1){

				Console.log("in accounts Invoice tab and viewedscreenname="+viewedScreenName);
				if(!viewedScreenName.equals("")&&viewedScreenName.equals("Invoice")){
					Console.log("in viewedScreenName.equals(Invoice) if");
					Invoice invoice=(Invoice)genralInvoicePopup.getPresenter().getModel();				
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("View Payment")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts-Invoice POPUP : View Payment clicked!!");
						reactOnViewPaymentFromAccountsInvoice(invoice);
						return;
					}else if(text.equals("View Bill")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts-Invoice POPUP : View Bill clicked!!");
						//using same method
						reactOnViewBillFromAccountsBillInvoice(invoice);
						return;						
					}
					else if(text.equals("View Order")){//2-09-2022
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts-Invoice POPUP : View Order clicked!!");
						reactOnViewOrderFromAccountsInvoice(invoice);
						return;
					}else if(text.equals(AppConstants.VIEWTAXINVOICE)) {
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts-Bill-Invoice POPUP : View Tax Invoice clicked!!");
						reactOnViewTaxInvoiceFromAccountsInvoice(invoice);
						return;
					}
				}
				else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Payment")){
						Console.log("in viewedScreenName.equals(View Payment) if");
						CustomerPayment customerPayment=(CustomerPayment)genralInvoicePopup.getPresenter().getModel();				
//						genralInvoicePopup.viewDocumentPanel.hide();
						if(text.equals("View Invoice")){
							genralInvoicePopup.viewDocumentPanel.hide();
							Console.log("Summary Accounts-Invoice-Payment POPUP : View Invoice clicked!!");
							reactOnViewInvoiceFromAccountsPayment(customerPayment);
							return;
						}
						else if(text.equals("View Order")){//2-09-2022
							genralInvoicePopup.viewDocumentPanel.hide();
							Console.log("Summary Accounts-Invoice-Payment POPUP : View Order clicked!!");
							reactOnViewOrderFromAccountsPayment(customerPayment);
							return;
						}
						else if(text.equals("View Bill")){//2-09-2022
							genralInvoicePopup.viewDocumentPanel.hide();
							Console.log("Summary Accounts-Invoice-Payment POPUP : View Bill clicked!!");
							reactOnViewBillFromAccountsPayment(customerPayment);
							return;
						}
						
									
				}else {
						documentIDClickOnAnotherTab(text);
				}									
			}else if(accTabSelIndex==2){
				Console.log("in accounts Payment tab and viewedscreenname="+viewedScreenName);
				if(!viewedScreenName.equals("")&&viewedScreenName.equals("Payment")){
					Console.log("in viewedScreenName.equals(Payment) if");
					CustomerPayment customerPayment=(CustomerPayment)genralInvoicePopup.getPresenter().getModel();				
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("View Invoice")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts-Payment POPUP : View Invoice clicked!!");
						reactOnViewInvoiceFromAccountsPayment(customerPayment);
						return;
					}	
					else if(text.equals("View Order")){//2-09-2022
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts-Invoice-Payment POPUP : View Order clicked!!");
						reactOnViewOrderFromAccountsPayment(customerPayment);
						return;
					}
					else if(text.equals("View Bill")){//2-09-2022
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts-Invoice-Payment POPUP : View Bill clicked!!");
						reactOnViewBillFromAccountsPayment(customerPayment);
						return;
					}
				}	
				else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Invoice")){
					Console.log("in viewedScreenName.equals(Invoice) if");
					Invoice invoice=(Invoice)genralInvoicePopup.getPresenter().getModel();				
//					genralInvoicePopup.viewDocumentPanel.hide();
					if(text.equals("View Bill")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts-Payment-Invoice POPUP : View Bill clicked!!");
						//using same method
						reactOnViewBillFromAccountsBillInvoice(invoice);
						return;
					}	
					else if(text.equals("View Payment")){
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts-Payment-Invoice POPUP : View Payment clicked!!");
						reactOnViewPaymentFromAccountsInvoice(invoice);
						return;
					}	
					else if(text.equals("View Order")){//2-09-2022
						genralInvoicePopup.viewDocumentPanel.hide();
						Console.log("Summary Accounts-Invoice POPUP : View Order clicked!!");
						reactOnViewOrderFromAccountsInvoice(invoice);
						return;
					}
				}else {
					documentIDClickOnAnotherTab(text);
				}
			}
		}
		}catch(Exception e){
			e.printStackTrace();
			Console.log(e.getMessage());
		}
		
		
		
	}

	private void reactOnCreateQuickSalesOrderFromSalesLead(Lead lead) {
		viewedScreenName="Quick Sales Order";
		genralInvoicePopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Quick Sales Order", lead, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Quick Sales Order");
			}
		};
		timer.schedule(1100);	
		
	}

	private void reactOnCreateSalesOrderFromSalesLead(Lead lead) {
		viewedScreenName="Sales Order";
		genralInvoicePopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Sales Order", lead, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Sales Order");
			}
		};
		timer.schedule(3200);		
	}

	private void reactOnCreateSalesQuotationFromSalesLead(Lead lead) {
		viewedScreenName="Sales Quotation";
		genralInvoicePopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Sales Quotation", lead, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Sales Quotation");
			}
		};
		timer.schedule(1100);
	}
	
	//Ashwini Patil
	private void reactOnCreateServiceQuotationFromServiceLead(Lead lead){
		viewedScreenName="Service Quotation";
		genralInvoicePopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		Console.log("in reactOnCreateServiceQuotationFromServiceLead");
		genralInvoicePopup.navigateScreen("Service Quotation", lead, null,true);
		serTabSelIndex=1;
		form.serviceTabPanel.selectTab(1);
		Console.log("before timer");
		Timer timer = new Timer() {
			@Override
			public void run() {
				Console.log("inside timer-applyHandlerOnProcessBarMenu(Service Quotation)");
				applyHandlerOnProcessBarMenu("Service Quotation");
			}
		};
		timer.schedule(3100);
	}
	
	private void reactOnCreateServiceOrderFromServiceLead(Lead lead){
		viewedScreenName="Contract";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Service Order", lead, null,true);
		serTabSelIndex=2;
		form.serviceTabPanel.selectTab(2);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Contract");
			}
		};
		timer.schedule(3100);	
	}
	private void reactOnCreateQuickServiceOrderFromServiceLead(Lead lead)
	{
		viewedScreenName="Quick Service Order";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Quick Service Order", lead, null,true);
		serTabSelIndex=2;
		form.serviceTabPanel.selectTab(2);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Quick Service Order");
			}
		};
		timer.schedule(3100);	
		
	}
	private void reactOnCreateWorkOrderFromSalesOrder(SalesOrder salesOrder)
	{
		viewedScreenName="Work Order";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Create Work Order From SalesOrder", salesOrder, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Work Order");
			}
		};
		timer.schedule(3100);			
	}
	private void reactOnCreateAMCFromSalesOrder(SalesOrder salesOrder)
	{
		viewedScreenName="Contract";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Create AMC", salesOrder, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Contract");
			}
		};
		timer.schedule(3200);			
	}
	private void reactOnViewAMCFromSalesOrder(SalesOrder salesOrder)
	{
		viewedScreenName="Contract";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View AMC", salesOrder, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Contract");
			}
		};
		timer.schedule(3100);			
	}
	private void reactOnCreatePOFromSalesOrder(SalesOrder salesOrder)
	{
		viewedScreenName="Purchase Order";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Create Purchase Order", salesOrder, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Purchase Order");
			}
		};
		timer.schedule(3100);			
	}
	private void reactOnViewPOFromSalesOrder(SalesOrder salesOrder)
	{
		viewedScreenName="Purchase Order";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View Purchase Order", salesOrder, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Purchase Order");
			}
		};
		timer.schedule(3100);			
	}
	private void reactOnViewBillFromSalesOrder(SalesOrder salesOrder)
	{
		viewedScreenName="Bill";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View Sales Bill", salesOrder, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Bill");
			}
		};
		timer.schedule(3100);			
	}
	private void reactOnViewDeliveryNoteFromSalesOrder(SalesOrder salesOrder)
	{
		viewedScreenName="Delivery Note";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View Delivery Note", salesOrder, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Delivery Note");
			}
		};
		timer.schedule(3200);			
	}
	
	private void reactOnCreateContractFromServiceQuotation(Quotation quot){
		viewedScreenName="Contract";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Service Contract", quot, null,true);
		serTabSelIndex=2;
		form.serviceTabPanel.selectTab(2);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Contract");
			}
		};
		timer.schedule(3100);
	}
	private void reactOnCreateQuickContractFromServiceQuotation(Quotation quot){
		viewedScreenName="Quick Service Contract";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Quick Service Contract", quot, null,true);
		serTabSelIndex=2;
		form.serviceTabPanel.selectTab(2);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Quick Service Contract");
			}
		};
		timer.schedule(3100);
	}
	private void reactOnViewServicesFromServiceContract(Contract contrct){
		viewedScreenName="Contract";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);//uncommented on 7-10-2022
		genralInvoicePopup.navigateScreen("Customer Service List", contrct, null,true);

		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Customer Service List");
			}
		};
		timer.schedule(3000);
	}
	private void reactOnViewBillFromServiceContract(Contract contrct){
		viewedScreenName="Bill";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View Bill", contrct, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Bill");
			}
		};
		timer.schedule(1100);
	}
	
	private void reactOnCreateWorkOrderFromServiceContract(Contract contrct){
		viewedScreenName="Work Order";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Create Work Order", contrct, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Work Order");
			}
		};
		timer.schedule(1100);
	}
	private void reactOnContractRenewFromServiceContract(Contract contrct){
		viewedScreenName="Contract";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Contract Renew", contrct, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Contract");
			}
		};
		timer.schedule(10500);
	}
	
	
	private void reactOnViewGRNFromAccountsBill(BillingDocument billdoc){
		viewedScreenName="GRN";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View GRN", billdoc, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("GRN");
			}
		};
		timer.schedule(1100);
	}
	
	private void reactOnViewInvoiceFromAccountsBill(BillingDocument billdoc){
		viewedScreenName="Invoice";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View Invoice", billdoc, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Invoice");
			}
		};
		timer.schedule(5100);
	}
	
	private void reactOnViewBillFromAccountsBillInvoice(Invoice invoice){
		viewedScreenName="Bill";
		genralInvoicePopup = new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View Invoice Bill", invoice, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Bill");
			}
		};
		timer.schedule(1100);
	}
	private void reactOnViewPaymentFromAccountsInvoice(Invoice invoice){
		viewedScreenName="Payment";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View Payment", invoice, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Payment");
			}
		};
		timer.schedule(1100);
	}
	
	private void reactOnViewInvoiceFromAccountsPayment(CustomerPayment customerPayment){
		viewedScreenName="Invoice";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View Payment Invoice", customerPayment, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Invoice");
			}
		};
		timer.schedule(1200);
	}
	
	private void reactOnViewOrderFromBill(BillingDocument billingDocument){
		viewedScreenName="Sales Order";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View Order", billingDocument, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Sales Order");
			}
		};
		timer.schedule(1200);
	}
	private void reactOnViewServiceFromBill(BillingDocument billingDocument){
		viewedScreenName="Device";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View Service", billingDocument, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Device");
			}
		};
		timer.schedule(1200);
	}
	
	private void reactOnViewSalesOrderFromDeliveryNote(DeliveryNote deliveryNote){
		viewedScreenName="Sales Order";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View Sales Order", deliveryNote, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Sales Order");
			}
		};
		timer.schedule(3500);
	}
	
	private void reactOnCopyOrderFromSalesOrder(SalesOrder salesOrder){
		viewedScreenName="Sales Order";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Copy Order", salesOrder, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Sales Order");
			}
		};
		timer.schedule(3200);
	}
	
	private void reactOnCreateSalesOrderFromSalesQuotation(SalesQuotation salesQuotation){
		viewedScreenName="Sales Order";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Create Sales Order", salesQuotation, null,true);
		Console.log("after navigateScreen in reactOnCreateSalesOrderFromSalesQuotation");
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Sales Order");
			}
		};
		timer.schedule(5200);
	}
	
	private void reactOnRevisedQuotationFromSalesQuotation(SalesQuotation salesQuotation){
		viewedScreenName="Sales Quotation";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Revised Quotation", salesQuotation, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Sales Quotation");
			}
		};
		timer.schedule(3200);
	}
	
	private void reactOnViewDocumentFromCustomerServiceList(Service service){
		viewedScreenName="Customer Service List";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View Document", service, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Device");
			}
		};
		timer.schedule(3200);
	}
	
	private void reactOnContractFromCustomerServiceList(Service service){
		viewedScreenName="Customer Service List";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Contract", service, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Contract");
			}
		};
		timer.schedule(7200);
	}
	
	
	private void reactOnViewBillFromDevice(Service service){
		viewedScreenName="Device";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View Bill From Device", service, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Bill");
			}
		};
		timer.schedule(3200);
	}
	
	private void reactOnManageCustomerProjectFromDevice(Service service){
		Console.log("Inside reactOnManageCustomerProjectFromDevice");
		viewedScreenName="Device";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("Manage Customer Project", service, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Project");
			}
		};
		timer.schedule(3200);
	}
	
	//1-09-2022
	private void reactOnViewOrderFromServiceLead(Lead lead) {
		Console.log("Inside reactOnViewOrderFromServiceLead");
		if(String.valueOf(lead.getContractCount())==null||String.valueOf(lead.getContractCount()).equals("")) {
			form.showDialogMessage("No order document found.");
			return;
		}
		viewedScreenName="Contract";
		Console.log("viewedScreenName set to="+viewedScreenName);
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("ViewOrderFromServiceLead", lead, null,true);
		Console.log("viewedScreenName after navigateScreen="+viewedScreenName);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Contract");
			}
		};
		timer.schedule(7200);
	}
	private void reactOnViewLeadFromServiceQuotation(Quotation quot){
		Console.log("ashwini Inside reactOnViewLeadFromServiceQuotation");
		if(String.valueOf(quot.getLeadCount())==null||String.valueOf(quot.getLeadCount()).equals("")) {
			form.showDialogMessage("No Lead document found !");
			return;
		}
		viewedScreenName="Service Lead";
		Console.log("ashiwini viewedScreenName set to="+viewedScreenName);		
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("ViewLeadFromServiceQuotation", quot, null,true);
		Console.log("ashiwini viewedScreenName after navigateScreen="+viewedScreenName);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Service Lead");
			}
		};
		timer.schedule(6000);
	}
	private void reactOnViewLeadFromServiceContract(Contract con){
		Console.log("Inside reactOnViewLeadFromServiceContract");
		if(String.valueOf(con.getLeadCount())==null||String.valueOf(con.getLeadCount()).equals("")) {
			form.showDialogMessage("No Lead document found.");
			return;
		}
		viewedScreenName="Service Lead";
		Console.log("viewedScreenName set to="+viewedScreenName);
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("ViewLeadFromServiceContract", con, null,true);
		Console.log("viewedScreenName after navigateScreen="+viewedScreenName);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Service Lead");
			}
		};
		timer.schedule(6000);
	}
	private void reactOnViewQuotationFromServiceContract(Contract con){
		Console.log("Inside reactOnViewQuotationFromServiceContract");
		if(String.valueOf(con.getQuotationCount())==null||String.valueOf(con.getQuotationCount()).equals("")) {
			form.showDialogMessage("No Quotation document found.");
			return;
		}
		viewedScreenName="Service Quotation";
		Console.log("viewedScreenName set to="+viewedScreenName);
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("ViewQuotationFromServiceContract", con, null,true);
		Console.log("viewedScreenName after navigateScreen="+viewedScreenName);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Service Quotation");
			}
		};
		timer.schedule(6000);
	}
	
	private void reactOnViewOrderFromAccountsBill(BillingDocument billdoc){//02-09-2022
		Console.log("Inside reactOnViewOrderFromAccountsBill");		
		viewedScreenName="Contract";
		Console.log("viewedScreenName set to="+viewedScreenName);
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("ViewOrderFromAccountsBill", billdoc, null,true);
		Console.log("viewedScreenName after navigateScreen="+viewedScreenName);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Contract");
			}
		};
		timer.schedule(7200);
	}
	
	private void reactOnViewPaymentFromAccountsBill(BillingDocument billdoc){//02-09-2022
		Console.log("Inside reactOnViewPaymentFromAccountsBill");	
		//Ashwini Patil Date:8-06-2023 to restrict access to payment as per user authorization			

		if(form.tabNames.contains("Payment Details")||form.tabNames.contains("Payment List")||form.tabNames.contains("PAYMENTDETAILS")||form.tabNames.contains("PAYMENTLIST")) {
			
			viewedScreenName="Payment";
			Console.log("viewedScreenName set to="+viewedScreenName);
			genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
			genralInvoicePopup.btnClose.addClickHandler(this);
			genralInvoicePopup.navigateScreen("ViewPaymentFromAccountsBill", billdoc, null,true);
			Console.log("viewedScreenName after navigateScreen="+viewedScreenName);
			Timer timer = new Timer() {
				@Override
				public void run() {
					applyHandlerOnProcessBarMenu("Payment");
				}
			};
			timer.schedule(1200);			
		}
		else {
			//already message is coming from billingdocumentpresenter
//			form.showDialogMessage("You are not authorized to view this screen!");
		}
		
	}
	
	private void reactOnViewOrderFromAccountsInvoice(Invoice invoice){//02-09-2022
		Console.log("Inside reactOnViewOrderFromAccountsInvoice");		
		viewedScreenName="Contract";
		Console.log("viewedScreenName set to="+viewedScreenName);
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("ViewOrderFromAccountsInvoice", invoice, null,true);
		Console.log("viewedScreenName after navigateScreen="+viewedScreenName);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Contract");
			}
		};
		timer.schedule(7200);
	}
	private void reactOnViewOrderFromAccountsPayment(CustomerPayment custpay){//02-09-2022
		Console.log("Inside reactOnViewOrderFromAccountsPayment");		
		viewedScreenName="Contract";
		Console.log("viewedScreenName set to="+viewedScreenName);
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("ViewOrderFromAccountsPayment", custpay, null,true);
		Console.log("viewedScreenName after navigateScreen="+viewedScreenName);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Contract");
			}
		};
		timer.schedule(7200);
	}
	
	private void reactOnViewBillFromAccountsPayment(CustomerPayment custpay){//02-09-2022
		Console.log("Inside reactOnViewBillFromAccountsPayment");		
		viewedScreenName="Bill";
		Console.log("viewedScreenName set to="+viewedScreenName);
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("ViewBillFromAccountsPayment", custpay, null,true);
		Console.log("viewedScreenName after navigateScreen="+viewedScreenName);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Bill");
			}
		};
		timer.schedule(1200);
	}
	
	private void reactOnViewOrderFromDevice(Service service){//02-09-2022
		Console.log("Inside reactOnViewOrderFromAccountsPayment");		
		viewedScreenName="Contract";
		Console.log("viewedScreenName set to="+viewedScreenName);
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("ViewOrderFromDevice", service, null,true);
		Console.log("viewedScreenName after navigateScreen="+viewedScreenName);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Contract");
			}
		};
		timer.schedule(7200);
	}
	
	
	private void reactOnViewServiceFromAccountsBill(BillingDocument billdoc){//21-09-2022
		Console.log("Inside reactOnViewOrderFromAccountsPayment");		
		viewedScreenName="Service";
		Console.log("viewedScreenName set to="+viewedScreenName);
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("ViewServiceFromAccountsBill", billdoc, null,true);
		Console.log("viewedScreenName after navigateScreen="+viewedScreenName);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Customer Service List");
			}
		};
		timer.schedule(1500);
	}
	
	private void reactOnCopyContract(Contract con){//02-09-2022
		Console.log("Inside reactOnCopyContract");		
		viewedScreenName="Contract";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("CopyContract", con, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Contract");
			}
		};
		timer.schedule(5000);
	}
	
	
	private void reactOnViewTaxInvoiceFromAccountsInvoice(Invoice invoice){//18-10-2022
		Console.log("Inside reactOnCopyContract");		
		viewedScreenName="Invoice";
		genralInvoicePopup =  new GeneralViewDocumentPopup(false,true,hideNavigationFlag);//19-10-2022
		genralInvoicePopup.btnClose.addClickHandler(this);
		genralInvoicePopup.navigateScreen("View Tax Invoice", invoice, null,true);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("Invoice");
			}
		};
		timer.schedule(5200);
	}
	
	private void documentIDClickOnAnotherTab(String text) {
		Console.log("viewedScreenName in documentIDClickOnAnotherTab="+viewedScreenName);
		if(!viewedScreenName.equals("")&&viewedScreenName.equals("Contract")){
			Console.log("order id clicked from another screen"+text);
			Contract contrct=(Contract)genralInvoicePopup.getPresenter().getModel();				
//			genralInvoicePopup.viewDocumentPanel.hide();		
			Console.log("text="+text);
			if(text.equals("New")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("common Summary Service Contract POPUP in reacttopopup : New clicked!!");
				reactOnNew("Contract");
				return;
			}
			else if(text.equals("View Services")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("common Summary Service Contract POPUP in reacttopopup : View Services!!");
				reactOnViewServicesFromServiceContract(contrct);
				return;
			}
			else if(text.equals("View Bill")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("common Summary Service Contract POPUP in reacttopopup : View Bill!!");
				reactOnViewBillFromServiceContract(contrct);
				return;
			}
			else if(text.equals("Create Work Order")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("common Summary Service Contract POPUP in reacttopopup : Create work order!!");
				reactOnCreateWorkOrderFromServiceContract(contrct);
				return;
			}
			else if(text.equals("Contract Renew")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("common Summary Service Contract POPUP in reacttopopup : Contract Renew!!");
				reactOnContractRenewFromServiceContract(contrct);
				return;
			}
			else if(text.equals(AppConstants.VIEWLEAD)){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Service Contract POPUP in reacttopopup : View Lead!!");
				reactOnViewLeadFromServiceContract(contrct);
				return;
			}
			else if(text.equals(AppConstants.VIEWQUOTATION)){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Service Contract POPUP in reacttopopup : View Quotation!!");
				reactOnViewQuotationFromServiceContract(contrct);
				return;
			}
			else if(text.equals("Copy")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Service Contract POPUP in reacttopopup : Copy!!");
				reactOnCopyContract(contrct);
				return;
			}
			
		}
		else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Bill")){
			Console.log("in viewedScreenName.equals(Bill) if");
			BillingDocument billdoc=(BillingDocument)genralInvoicePopup.getPresenter().getModel();				
//			genralInvoicePopup.viewDocumentPanel.hide();
			if(text.equals("View GRN")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Accounts POPUP : View GRN clicked!!");
				reactOnViewGRNFromAccountsBill(billdoc);
				return;
			}
			else if(text.equals("View Invoice")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Accounts POPUP : View Invoice clicked!!");
				reactOnViewInvoiceFromAccountsBill(billdoc);
				return;
			}
			else if(text.equals("View Order")){//2-09-2022
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Accounts POPUP : View Order clicked!!");
				reactOnViewOrderFromAccountsBill(billdoc);
				return;
			}
			else if(text.equals("View Payment")){//2-09-2022
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Accounts POPUP : View Payment clicked!!");
				reactOnViewPaymentFromAccountsBill(billdoc);
				return;
			}
			else if(text.equals("View Service")){//21-09-2022
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Accounts POPUP : View Service clicked!!");
				reactOnViewServiceFromAccountsBill(billdoc);
				return;
			}
			
		}
		else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Invoice")){
			Console.log("Invoice id clicked from another screen"+text);
			Invoice invoice=(Invoice)genralInvoicePopup.getPresenter().getModel();				
//			genralInvoicePopup.viewDocumentPanel.hide();
			if(text.equals("View Payment")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("common Summary Accounts-Invoice POPUP : View Payment clicked!!");
				reactOnViewPaymentFromAccountsInvoice(invoice);
				return;
			}else if(text.equals("View Bill")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("common Summary Accounts-Invoice POPUP : View Payment clicked!!");
				//using same method
				reactOnViewBillFromAccountsBillInvoice(invoice);
				return;				
			}
			else if(text.equals("View Order")){//2-09-2022
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Accounts-Bill-Invoice POPUP : View Order clicked!!");
				reactOnViewOrderFromAccountsInvoice(invoice);
				return;
			}else if(text.equals(AppConstants.VIEWTAXINVOICE)) {
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Accounts-Bill-Invoice POPUP : View Tax Invoice clicked!!");
				reactOnViewTaxInvoiceFromAccountsInvoice(invoice);
				return;
			}
			
		}
		else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Payment")){
			Console.log("in viewedScreenName.equals(Payment) if");
			CustomerPayment customerPayment=(CustomerPayment)genralInvoicePopup.getPresenter().getModel();				
//			genralInvoicePopup.viewDocumentPanel.hide();
			if(text.equals("View Invoice")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Accounts-Payment POPUP : View Invoice clicked!!");
				reactOnViewInvoiceFromAccountsPayment(customerPayment);
				return;
			}	
			else if(text.equals("View Order")){//2-09-2022
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Accounts-Invoice-Payment POPUP : View Order clicked!!");
				reactOnViewOrderFromAccountsPayment(customerPayment);
				return;
			}
			else if(text.equals("View Bill")){//2-09-2022
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Accounts-Invoice-Payment POPUP : View Bill clicked!!");
				reactOnViewBillFromAccountsPayment(customerPayment);
				return;
			}
		}	
		else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Sales Lead")){
			Console.log("Sales Lead id clicked from another screen"+text);
			
			Lead lead=(Lead) salesLeadPopup.getPresenter().getModel();
			reactOnCloseBtnOfSalesLead();
//			salesLeadPopup.viewDocumentPanel.hide();
			if(text.equals("Create Quotation")){	
				salesLeadPopup.viewDocumentPanel.hide();
				Console.log("Summary LEAD POPUP : Create Quotation clicked!!");
				reactOnCreateSalesQuotationFromSalesLead(lead);
				return;
			}else if(text.equals("Create Order")){
				salesLeadPopup.viewDocumentPanel.hide();
				Console.log("Summary LEAD POPUP : Create Order clicked!!");
				reactOnCreateSalesOrderFromSalesLead(lead);
				return;
			}else if(text.equals("Quick Order")){
				salesLeadPopup.viewDocumentPanel.hide();
				Console.log("Summary LEAD POPUP : Quick Order clicked!!");
				reactOnCreateQuickSalesOrderFromSalesLead(lead);
				return;
			}else if(text.equals("New")){
				salesLeadPopup.viewDocumentPanel.hide();
				Console.log("Summary LEAD POPUP : New clicked!!");
				reactOnNew("Sales Lead");
				return;
			}
			
		}
		else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Service Lead")){
			Console.log("Service Lead id clicked from another screen"+text);
			
			Lead lead=(Lead) genralInvoicePopup.getPresenter().getModel();
			reactOnCloseBtnOfServiceLead();
//			genralInvoicePopup.viewDocumentPanel.hide();
			Console.log("text="+text);
			if(text.equals("Create Quotation")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Service Lead POPUP : Create Quotation clicked!!");
				reactOnCreateServiceQuotationFromServiceLead(lead);			
				return;
			}else if(text.equals("Create Order")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Service Lead POPUP : Create Order clicked!!");
				reactOnCreateServiceOrderFromServiceLead(lead);
				return;
			}else if(text.equals("Quick Order")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Service Lead POPUP : Quick Order clicked!!");
				reactOnCreateQuickServiceOrderFromServiceLead(lead);
				return;
			}else if(text.equals("New")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Service Lead POPUP : New clicked!!");
				reactOnNew("Service Lead");
				return;
			}else if(text.equals(AppConstants.VIEWORDER)) {
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Service Lead POPUP : View Order clicked!!");
				reactOnViewOrderFromServiceLead(lead);
				return;
			}
		}
		else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Sales Quotation")){
			Console.log("Sales Quotation id clicked from another screen"+text);
			
			SalesQuotation salesQuotation=(SalesQuotation) genralInvoicePopup.getPresenter().getModel();
//			genralInvoicePopup.viewDocumentPanel.hide();
			if(text.equals("Create Sales Order")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Sales Quotation POPUP : Create Sales Order!!");
				reactOnCreateSalesOrderFromSalesQuotation(salesQuotation);
				return;
			}else if(text.equals("Revised Quotation")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Sales Quotation POPUP : Revised Quotation clicked!!");
				reactOnRevisedQuotationFromSalesQuotation(salesQuotation);
				return;
			}	else if(text.equals("New")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Sales Quotation POPUP : New clicked!!");
				reactOnNew("Sales Quotation");
				return;
			}														
		}
		else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Service Quotation")){					
			Console.log("Service Quotation id clicked from another screen"+text);
			
			Quotation quot=(Quotation)genralInvoicePopup.getPresenter().getModel();				
//			genralInvoicePopup.viewDocumentPanel.hide();
			Console.log("text="+text);
			if(text.equals("Create Contract")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Service Quotation POPUP : Create Contract clicked!!");
				reactOnCreateContractFromServiceQuotation(quot);
				return;
			}
			else if(text.equals("Create Quick Contract")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Service Quotation POPUP : Create Quick Contract clicked!!");
				reactOnCreateQuickContractFromServiceQuotation(quot);
				return;
			}
			else if(text.equals("New")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Service Quotation POPUP : New clicked!!");
				reactOnNew("Service Quotation");
				return;
			}else if(text.equals(AppConstants.VIEWLEAD)){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Summary Service Quotation POPUP : View Lead clicked!!");
				reactOnViewLeadFromServiceQuotation(quot);
				return;
			}
		
	}else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Customer Service List")){
		Console.log("in if viewedScreenName.equals(Customer Service List)");	
		Service service=null;
		ServiceForm form=(ServiceForm) genralInvoicePopup.getPresenter().getView();
		List<Service> serviceList=form.getSuperTable().getValue();
		for(Service ser:serviceList)
		 {
			 if(ser.getRecordSelect()==true){
				 service=ser;
			 }
		 }
				
		Console.log("after service obj creation"+service.toString());
//		genralInvoicePopup.viewDocumentPanel.hide();	
		Console.log("text="+text);
		if(text.equals("View Document")){
			genralInvoicePopup.viewDocumentPanel.hide();
			Console.log("Summary Service POPUP in reacttopopup : View Document clicked!!");
			reactOnViewDocumentFromCustomerServiceList(service);
			return;
		}
		else if(text.equals("Contract")){
			genralInvoicePopup.viewDocumentPanel.hide();
			Console.log("Summary Service POPUP in reacttopopup : Contract clicked!!");
			reactOnContractFromCustomerServiceList(service);
			return;
		}
	}
	else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Work Order")){
		WorkOrder workOrder=(WorkOrder) genralInvoicePopup.getPresenter().getModel();
//		genralInvoicePopup.viewDocumentPanel.hide();
		if(text.equals("New")){
			genralInvoicePopup.viewDocumentPanel.hide();
			Console.log("Contract Work Order POPUP : New Clicked!!");
			reactOnNew("Work Order");
			return;
		}
	}
	else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Device")){
		Console.log("In Device if text="+text);
		Service service=(Service) genralInvoicePopup.getPresenter().getModel();
//		genralInvoicePopup.viewDocumentPanel.hide();
		if(text.equals("New")){
			genralInvoicePopup.viewDocumentPanel.hide();
			Console.log("Contract Device POPUP : New Clicked!!");
			reactOnNew("Device");
			return;
		}
		else if(text.equals("View Bill")){
			genralInvoicePopup.viewDocumentPanel.hide();
			Console.log("Contract Device POPUP : View Bill Clicked!!");
			reactOnViewBillFromDevice(service);
			return;	
		}					
		else if(text.equals("Manage Customer Project")){
			genralInvoicePopup.viewDocumentPanel.hide();
			Console.log("Contract Device POPUP : Manage Customer Project Clicked!!");
			reactOnManageCustomerProjectFromDevice(service);
			return;	
		}
		else if(text.equals("View Order")){
			genralInvoicePopup.viewDocumentPanel.hide();
			Console.log("Contract Device POPUP : View Order Clicked!!");
			reactOnViewOrderFromDevice(service);
			return;	
		}
		
	}
	else if(!viewedScreenName.equals("")&&viewedScreenName.equals("Sales Order")){
			SalesOrder salesOrder=(SalesOrder) genralInvoicePopup.getPresenter().getModel();
//			genralInvoicePopup.viewDocumentPanel.hide();
			if(text.equals("Create Work Order")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Sales Order POPUP : Create Work Order!!");
				reactOnCreateWorkOrderFromSalesOrder(salesOrder);
				return;
			}else if(text.equals("Create AMC")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Sales Order POPUP : Create AMC!!");
				reactOnCreateAMCFromSalesOrder(salesOrder);
				return;
			}else if(text.equals("View AMC")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Sales Order POPUP : View AMC!!");
				reactOnViewAMCFromSalesOrder(salesOrder);
				return;
			}else if(text.equals("Create PO")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Sales Order POPUP : Create PO!!");
				reactOnCreatePOFromSalesOrder(salesOrder);
				return;
			}else if(text.equals("View PO")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Sales Order POPUP : View PO!!");
				reactOnViewPOFromSalesOrder(salesOrder);
				return;
			}else if(text.equals("View Bill")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Sales Order POPUP : View Bill!!");
				reactOnViewBillFromSalesOrder(salesOrder);
				return;
			}else if(text.equals("View Delivery Note")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Sales Order POPUP : View Delivery Note!!");
				reactOnViewDeliveryNoteFromSalesOrder(salesOrder);
				return;
			}else if(text.equals("Copy Order")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Sales Order POPUP : Copy Order!!");
				reactOnCopyOrderFromSalesOrder(salesOrder);
				return;
			}
			else if(text.equals("New")){
				genralInvoicePopup.viewDocumentPanel.hide();
				Console.log("Sales Order POPUP : New clicked!!");
				reactOnNew("Sales Order");
				return;
			}
		}
	
	}
	private void runRegularContractDownloadProgram() {
		// TODO Auto-generated method stub
		ArrayList<Contract> custarray = new ArrayList<Contract>(form.globalContractList);
		csvservice.setcontractlist(custarray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractReport")){
					final String url = gwt + "csvservlet" + "?type=" + 127;
					Window.open(url, "test", "enabled");
				}else{
					final String url = gwt + "csvservlet" + "?type=" + 6;
					Window.open(url, "test", "enabled");
				}
			}
		});
	}
}

