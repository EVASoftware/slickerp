package com.slicktechnologies.client.views.authorization;

import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.shared.common.role.UserRole;

// TODO: Auto-generated Javadoc
/**
 * Authorization Presenter
 */
public class AuthorizationPresenter extends FormTableScreenPresenter<UserRole>   {

	//Token to set the concrete form
	/** The form. */
	UserAuthorizationForm form;
	
	/**
	 * Instantiates a new authorization presenter.
	 *
	 * @param view the view
	 * @param model the model
	 */
	public AuthorizationPresenter (FormTableScreen<UserRole> view,
			UserRole model) {
		super(view, model);
		form=(UserAuthorizationForm) view;
		form.getSupertable().connectToLocal();
		form.getTable().connectToLocal();
		form.getTable().getDataprovider().setList(model.getAuthorization());
		
		form.retriveTable(getUserQuery());
		form.setPresenter(this);
		
	}

	/**
	 * Method template to set the processBar events.
	 *
	 * @param e the e
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals(""))
  reactTo();

		
	}
	
	

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnPrint()
	 */
	@Override
	public void reactOnPrint() 
	{
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactOnEmail()
	 */
	@Override
	public void reactOnEmail() 
	{
		
	}

	/**
	 * Method template to set new model.
	 */
	@Override
	protected void makeNewModel() {
		
		model=new UserRole();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	/**
	 * Gets the user query.
	 *
	 * @return the user query
	 */
	public MyQuerry getUserQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new UserRole());
		return quer;
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#setModel(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	public void setModel(UserRole entity)
	{
		model=entity;
	}
	
	/**
	 * Initalize.
	 */
	@SuppressWarnings("unused")
	public static void initalize()
	{
		AuthorizationPresenterTable gentableScreen=GWT.create( AuthorizationPresenterTable.class);
			
		UserAuthorizationForm  form=new  UserAuthorizationForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
			  //// BranchPresenterTable gentableSearch=GWT.create( BranchPresenterTable.class);
			  ////   BranchPresenterSearch.staticSuperTable=gentableSearch;
			  ////     BranchPresenterSearch searchpopup=GWT.create( BranchPresenterSearch.class);
			  ////         form.setSearchpopupscreen(searchpopup);
			
			AuthorizationPresenter  presenter=new  AuthorizationPresenter  (form,new UserRole());
			AppMemory.getAppMemory().stickPnel(form);
			
			
		 
	}
	
	
		/**
		 * The Class AuthorizationPresenterTable.
		 */
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.role.UserRole")
		 public static class  AuthorizationPresenterTable extends SuperTable<UserRole> implements GeneratedVariableRefrence{

			/* (non-Javadoc)
			 * @see com.slicktechnologies.client.utility.GeneratedVariableRefrence#getVarRef(java.lang.String)
			 */
			@Override
			public Object getVarRef(String varName) {
				
				return null;
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#createTable()
			 */
			@Override
			public void createTable() {
								
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#initializekeyprovider()
			 */
			@Override
			protected void initializekeyprovider() {
				
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#addFieldUpdater()
			 */
			@Override
			public void addFieldUpdater() {
				
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.SuperTable#setEnable(boolean)
			 */
			@Override
			public void setEnable(boolean state) {
				
				
			}

			/* (non-Javadoc)
			 * @see com.simplesoftwares.client.library.appstructure.ViewContainer#applyStyle()
			 */
			@Override
			public void applyStyle() {
				
				
			}} ;
			
			/**
			 * The Class AuthorizationPresenterSearch.
			 */
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.role.UserRole")
			 public static class  AuthorizationPresenterSearch extends SearchPopUpScreen<  UserRole>{

				/* (non-Javadoc)
				 * @see com.simplesoftwares.client.library.appstructure.SearchPopUpScreen#getQuerry()
				 */
				@Override
				public MyQuerry getQuerry() {
					
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
	  /**
  	 * React to.
  	 */
  	private void reactTo()
  {
}


	  /* (non-Javadoc)
  	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#onClick(com.google.gwt.event.dom.client.ClickEvent)
  	 */
  	@Override
	   public void onClick(ClickEvent event) {
		  super.onClick(event);
//	      InlineLabel label= (InlineLabel) event.getSource();
//	      if(label.getText().equals("Selenium Test"))
//		    reactOnSilleniumTest();
		  
	  };
	
//	/* (non-Javadoc)
//	 * @see com.testclasses.client.TestImplementer#reactOnSilleniumTest()
//	 */
//	@Override
//
//	public void reactOnSilleniumTest() {
//		//model=BranchTestClass.populateBranch();
//		form.updateView(model);
//		System.out.println("ahay---");
//	}

}
