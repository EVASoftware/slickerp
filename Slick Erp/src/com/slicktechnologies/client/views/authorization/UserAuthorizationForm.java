package com.slicktechnologies.client.views.authorization;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.role.UserRole;

// TODO: Auto-generated Javadoc
/**
 * UserAuthorizationForm.View For User Authorization
 */
public class UserAuthorizationForm extends FormTableScreen<UserRole> implements ClickHandler
{

	/** The tb role. */
	TextBox tbRole;
	
	/** The tadescription. */
	TextArea tadescription;
	
	/** The status. */
	CheckBox status;
	
	/** The olb category. */
	//ObjectListBox<Config> olbCategory;
	
	/** The table. */
	AuthorizationTable table;
	
	/**
	 * Instantiates a new user authorization form.
	 *
	 * @param table the table
	 * @param mode the mode
	 * @param captionmode the captionmode
	 */
	public UserAuthorizationForm  (SuperTable<UserRole> table, int mode,boolean captionmode)
	{
		super(table, mode, captionmode);
		createGui();
		
	}
	
	public UserAuthorizationForm  (SuperTable<UserRole> table, int mode, boolean captionmode, boolean popupflag)
	{
		super(table, mode, captionmode,popupflag);
		createGui();
	}
	
	/**
	 * Initalize widget.
	 */
	private void initalizeWidget()
	{
		tbRole=new TextBox();
		tadescription=new TextArea();
		status=new CheckBox();
		status.setValue(true); 
		//olbCategory=new ObjectListBox<Config>();
	//	AppUtility.MakeLiveConfig(olbCategory, Screen.ROLECATEGORY);
		table=new AuthorizationTable();
	
		
	}

	/*
	 * Method template to create the formtable screen
	 */
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#createScreen()
	 */
	@Override
	public void createScreen() {
		
		//Token to initialize the processlevel menus.
	    initalizeWidget();
		
		
		
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////
	    
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
	fbuilder = new FormFieldBuilder();
	fbuilder = new FormFieldBuilder("* Role",tbRole);
	FormField ftbRole=fbuilder.setMandatory(true).setColSpan(0).setMandatoryMsg("Role is Mandatory").build();
	
	fbuilder = new FormFieldBuilder("Description",tadescription);
	FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
	
	//fbuilder = new FormFieldBuilder("* Category",olbCategory);
	//FormField folbCategory= fbuilder.setMandatory(true).setMandatoryMsg("Category is Mandatory").setRowSpan(0).setColSpan(0).build();
	
	fbuilder=new FormFieldBuilder("Status",status);
	FormField fcheckBoxStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
	
	fbuilder = new FormFieldBuilder();
	FormField fgroupingCustomerInformation=fbuilder.setlabel("Screen Authorization").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	
	fbuilder=new FormFieldBuilder("",table.getTable());
	FormField ftable=fbuilder.setColSpan(2).build();
	
		
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		
		
		FormField[][] formfield = {   
				{ftbRole,fcheckBoxStatus},
				{ftaDescription},
				{fgroupingCustomerInformation},
				{ftable}
  };

		
        this.fields=formfield;		
   }

	/**
	 * method template to update the model with token entity name.
	 *
	 * @param model the model
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(UserRole model) 
	{
		
		if(tbRole.getValue()!=null)
			model.setRoleName(tbRole.getValue());
		if(tadescription.getValue()!=null)
			model.setRoleDescription(tadescription.getValue());
		//if(olbCategory.getValue()!=null)
			//model.setRoleCategory(olbCategory.getValue());
		if(status.getValue()!=null)
			model.setRoleStatus(status.getValue());
		if(table.getValue()!=null)
			model.setAuthorization(table.getValue());

		presenter.setModel(model);

		
	}

	/**
	 * method template to update the view with token entity name.
	 *
	 * @param view the view
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void updateView(UserRole view) 
	{
		if(view.getRoleName()!=null)
			tbRole.setValue(view.getRoleName());
		if(view.getRoleDescription()!=null)
			tadescription.setValue(view.getRoleDescription());
		//if(view.getRoleCategory()!=null)
			//olbCategory.setValue(view.getRoleCategory());
		if(view.getRoleStatus()!=null)
			status.setValue(view.getRoleStatus());
		if(view.getAuthorization()!=null)
			table.setValue(view.getAuthorization());
		presenter.setModel(view);
	}

	/**
	 * Toggles the app header bar menus as per screen state.
	 */
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			System.out.println("isPopUpAppMenubar() "+isPopUpAppMenubar());
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			System.out.println("menus =="+menus);
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Sillenium Test"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
	}
	
	/**
	 * sets the id textbox with the passed count value.
	 *
	 * @param count the new count
	 */
	@Override
	public void setCount(int count)
	{
	
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) 
	{
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#clear()
	 */
	@Override
	public void clear() {
		super.clear();
		status.setValue(true);
	}

	/**
	 * Gets the tb role.
	 *
	 * @return the tb role
	 */
	public TextBox getTbRole() {
		return tbRole;
	}

	/**
	 * Sets the tb role.
	 *
	 * @param tbRole the new tb role
	 */
	public void setTbRole(TextBox tbRole) {
		this.tbRole = tbRole;
	}

	/**
	 * Gets the tadescription.
	 *
	 * @return the tadescription
	 */
	public TextArea getTadescription() {
		return tadescription;
	}

	/**
	 * Sets the tadescription.
	 *
	 * @param tadescription the new tadescription
	 */
	public void setTadescription(TextArea tadescription) {
		this.tadescription = tadescription;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public CheckBox getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(CheckBox status) {
		this.status = status;
	}

	/**
	 * Gets the olb category.
	 *
	 * @return the olb category
	 */
	

	/**
	 * Sets the olb category.
	 *
	 * @param olbCategory the new olb category
	 */
	

	/**
	 * Gets the table.
	 *
	 * @return the table
	 */
	public AuthorizationTable getTable() {
		return table;
	}

	/**
	 * Sets the table.
	 *
	 * @param table the new table
	 */
	public void setTable(AuthorizationTable table) {
		this.table = table;
	}

	
	
}
