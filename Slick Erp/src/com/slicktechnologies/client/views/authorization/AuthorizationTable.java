package com.slicktechnologies.client.views.authorization;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.views.authorization.AuthorizationPresenter.AuthorizationPresenterTable;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.role.UserRole;

public class AuthorizationTable extends SuperTable<ScreenAuthorization> {
	
	TextColumn<ScreenAuthorization>screenColumn;
	Column<ScreenAuthorization,Boolean>download;
	Column<ScreenAuthorization,Boolean>view;
	Column<ScreenAuthorization,Boolean>create;
	Column<ScreenAuthorization,Boolean>edit;
	Column<ScreenAuthorization,Boolean>approve;
	
	

	@Override
	public void createTable()
	{
		addColumngetScreens();
		addColumnisView();
		addColumnisCreate();
		//addColumnisEdit();
		//addColumnisApprove();
		addColumnDownload();
		//addFieldUpdater();
		this.table.setHeight("500px");
		
	}
	
	
	private void addColumnDownload() {
		CheckboxCell cb=new CheckboxCell();
		download=new Column<ScreenAuthorization, Boolean>(cb) {

			@Override
			public Boolean getValue(ScreenAuthorization object) {
				return object.isCreate();
			}
		};
		table.addColumn(download,"Download");	
		
	}


	protected void addColumngetScreens()
	{
		screenColumn=new TextColumn<ScreenAuthorization>()
				{
			@Override
			public String getValue(ScreenAuthorization object)
			{
				if( object.getScreens()==null)
					return "N.A";
				else return object.getScreens().toString();
			}
				};
				table.addColumn(screenColumn,"Screen");
				table.setColumnWidth(screenColumn, 200,Unit.PX);
	}
	
	protected void addColumnisCreate()
	{
		CheckboxCell cb=new CheckboxCell();
		create=new Column<ScreenAuthorization, Boolean>(cb) {

			@Override
			public Boolean getValue(ScreenAuthorization object) {
				return object.isCreate();
			}
		};
		table.addColumn(create,"Create/Edit");	
	}
	
	protected void addColumnisView()
	{
		CheckboxCell cb=new CheckboxCell();
		view=new Column<ScreenAuthorization, Boolean>(cb) {

			@Override
			public Boolean getValue(ScreenAuthorization object) {
				return object.isView();
			}
		};
		table.addColumn(view,"View");		
	}
	
	protected void addColumnisEdit()
	{
		CheckboxCell cb=new CheckboxCell();
		edit=new Column<ScreenAuthorization, Boolean>(cb) {

			@Override
			public Boolean getValue(ScreenAuthorization object) {
				return object.isCreate();
			}
		};
		table.addColumn(edit,"Create/Edit");	
	}
	
	protected void addColumnisApprove()
	{
		CheckboxCell cb=new CheckboxCell();
		approve=new Column<ScreenAuthorization, Boolean>(cb) {

			@Override
			public Boolean getValue(ScreenAuthorization object) {
				return object.isApprove();
			}
		};
		table.addColumn(approve,"Approve");
				
	}
	
	
	
	/**********************************************************************************************/
	
	protected void setFieldUpdaterOnCreate()
	{
		create.setFieldUpdater(new FieldUpdater<ScreenAuthorization, Boolean>() {
						@Override
			public void update(int index, ScreenAuthorization object, Boolean value) {
				object.setCreate(value);
				getDataprovider().getList();
				table.redrawRow(index);
			
			}
		});
	}
	
	
	protected void setFieldUpdaterOnView()
	{
		view.setFieldUpdater(new FieldUpdater<ScreenAuthorization, Boolean>() {
			
			@Override
			public void update(int index, ScreenAuthorization object, Boolean value) {
				object.setView(value);
				getDataprovider().getList();
				table.redrawRow(index);
			
			}
		});
	}
	
	
	protected void setFieldUpdaterOnDownload()
	{
		download.setFieldUpdater(new FieldUpdater<ScreenAuthorization, Boolean>() {
			
			@Override
			public void update(int index, ScreenAuthorization object, Boolean value) {
				object.setDownload(value);
				getDataprovider().getList();
				table.redrawRow(index);
			
			}
		});
	}


	

	@Override
	protected void initializekeyprovider() 
	{
		keyProvider= new ProvidesKey<ScreenAuthorization>()
				{
			@Override
			public Object getKey(ScreenAuthorization item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return 10;
			}
				};
		
	}

	@Override
	public void addFieldUpdater() 
	{
	//	setFieldUpdaterOnCreate();
		//setFieldUpdaterOnView();
		//setFieldUpdaterOnDownload();

		
	}

	@Override
	public void setEnable(boolean state)
	{
	
		
	}

	@Override
	public void applyStyle()
	{
		
		
	}
	
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("screenColumn"))
			return this.screenColumn;
		if(varName.equals("approve"))
			return this.approve;
		if(varName.equals("create"))
			return this.create;
		if(varName.equals("view"))
			return this.view;
		if(varName.equals("edit"))
			return this.edit;
		
		return null ;
	}

}
