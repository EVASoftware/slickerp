package com.slicktechnologies.client.views.settings.customerslogin;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.views.settings.user.UserForm;
import com.slicktechnologies.client.views.settings.user.UserPresenter;
import com.slicktechnologies.client.views.settings.user.UserPresenter.UserPresenterTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.User;

public class CustomerUserPresenter extends FormTableScreenPresenter<CustomerUser> implements SelectionHandler<Suggestion>, ValueChangeHandler<String> {

	CustomerUserForm form;
	
	public CustomerUserPresenter(FormTableScreen<CustomerUser> view,CustomerUser model) {
		super(view, model);
		form=(CustomerUserForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getCustomerUserQuery());
		form.pic.getId().addSelectionHandler(this);
		form.pic.getName().addSelectionHandler(this);
		form.pic.getPhone().addSelectionHandler(this);
		form.tbUserId.addValueChangeHandler(this);
		//  added by anil for fort password email 
		form.tbEmail.addValueChangeHandler(this);
		
		form.setPresenter(this);
	}
	
	public MyQuerry getCustomerUserQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new CustomerUser());
		return quer;
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
	}

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}

	@Override
	protected void makeNewModel() {
		model=new CustomerUser();
		
	}
	
	
	public static CustomerUserForm initalize() {
		CustomerUserPresenterTable gentableScreen = new CustomerUserPresenterTable();

		CustomerUserForm form = new CustomerUserForm(gentableScreen,FormTableScreen.UPPER_MODE, true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();

		CustomerUserPresenter presenter = new CustomerUserPresenter(form, new CustomerUser());
		AppMemory.getAppMemory().stickPnel(form);
		
		return form;

	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if (event.getSource().equals(form.pic.getId())||event.getSource().equals(form.pic.getName())||event.getSource().equals(form.pic.getPhone())) {
		
			
			
			int contractId=Integer.parseInt(form.pic.getId().getValue());
			MyQuerry querry = new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setLongValue(c.getCompanyId());
			temp.setQuerryString("companyId");
			filtervec.add(temp);
			
			
			temp=new Filter();
			temp.setIntValue(contractId);
			temp.setQuerryString("count");
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			form.showWaitSymbol();
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				 
	            public void onSuccess(ArrayList<SuperModel> result)
	            {
	            	form.hideWaitSymbol();
	            	System.out.println(" result size :: "+result.size());
	            	if(result.size()!=0)
	            	{
	            		Customer entity=(Customer) result.get(0);
	            		form.branch=entity.getBranch();
//                       old code commented by anil 	            		
//	            		form.tbUserId.setValue(form.pic.getId().getValue()+"");
	            		
//                      new code commented by anil 
	            		form.tbEmail.setValue(entity.getEmail());
	            		checkEmailInUser(form.tbEmail.getValue());
	            	}
	            }

	            public void onFailure(Throwable throwable)
	            {
	            	form.showDialogMessage("An unexpected error occurred!");
	            	form.hideWaitSymbol();
	            	throwable.printStackTrace();
	            }

	        });
			
			
		}		
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
//		ScreeenState scr=AppMemory.getAppMemory().currentState;
//		
//		if(scr.equals(ScreeenState.NEW))
//		{
			if(event.getSource().equals(form.tbUserId)){
				if(!form.tbUserId.getValue().equals("")){
					validateUserName();
				}
			}
//		}
		
		if(event.getSource().equals(form.tbEmail))
		{
			if(!form.tbEmail.getValue().equals(""))
			{
				checkEmailInUser(form.tbEmail.getValue().trim());
			}
		}
		
	}

	private void validateUserName() {
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setLongValue(c.getCompanyId());
		temp.setQuerryString("companyId");
		filtervec.add(temp);
		
		
		temp=new Filter();
		temp.setStringValue(form.tbUserId.getValue());
		temp.setQuerryString("userName");
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new User());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			 
            public void onSuccess(ArrayList<SuperModel> result)
            {
            	form.hideWaitSymbol();
            	System.out.println(" result size :: "+result.size());
            	if(result.size()!=0)
            	{
            		User entity=(User) result.get(0);
            		form.showDialogMessage("User id is not available!");
					form.tbUserId.setValue("");
            	}
            }

            public void onFailure(Throwable throwable)
            {
            	form.showDialogMessage("An unexpected error occurred!");
            	form.hideWaitSymbol();
            	throwable.printStackTrace();
            }

        });
	}


	/**
	 * This method checks whether passed/entered email is available or not
	 * it checks email from User 
	 * Date : 17-09-2016 By Anil
	 * Release 30 Sept 2016
	 * I/P : Email Id
	 * 
	 */
	
	private void checkEmailInUser(String email) {
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setLongValue(c.getCompanyId());
		temp.setQuerryString("companyId");
		filtervec.add(temp);
		
		
		temp=new Filter();
		temp.setStringValue(email);
		temp.setQuerryString("email");
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new User());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			 
            public void onSuccess(ArrayList<SuperModel> result)
            {
            	form.hideWaitSymbol();
            	System.out.println(" result size :: "+result.size());
            	if(result.size()!=0)
            	{
            		User entity=(User) result.get(0);
            		form.showDialogMessage("Email id is already used!");
					form.tbEmail.setValue("");
            	}
            }

            public void onFailure(Throwable throwable)
            {
            	form.showDialogMessage("An unexpected error occurred!");
            	form.hideWaitSymbol();
            	throwable.printStackTrace();
            }

        });
	};

	
}
