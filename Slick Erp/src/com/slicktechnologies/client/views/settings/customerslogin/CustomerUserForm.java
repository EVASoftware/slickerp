package com.slicktechnologies.client.views.settings.customerslogin;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.UserRole;

public class CustomerUserForm extends FormTableScreen<CustomerUser> {
	CheckBox cbStatus;
	TextBox tbUserId;
	PasswordTextBox ptbPassword,ptbReTypePassword;
	CustomerUser custUserObj;
	
//	ObjectListBox<Customer> lbCustomerName;
	PersonInfoComposite pic;
	public String branch;
	

	/**
	 * This email box stores user email address
	 * Date 17-09-2016 By Anil
	 * Release : 30 Sept 2016
	 */
	EmailTextBox tbEmail;
	
	/**
	 * End
	 */
	
	
	public CustomerUserForm(SuperTable<CustomerUser> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
	}
	
	private void initalizeWidget()
	{


		cbStatus=new CheckBox();
		cbStatus.setValue(true);  
		tbUserId=new TextBox();

		ptbPassword=new PasswordTextBox();

		ptbReTypePassword=new PasswordTextBox();

//		lbCustomerName=new ObjectListBox<Customer>();
//		MyQuerry qu= new MyQuerry();
//		filter.setQuerryString("roleStatus");
//		filter.setBooleanvalue(true);
//		qu.getFilters().add(filter);
//		qu.setQuerryObject(new Customer());
//		lbCustomerName.MakeLive(qu);
		
		
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.setQuerryObject(new Customer());
		pic = new PersonInfoComposite(querry, false);
		
		tbEmail=new EmailTextBox();

	}
	

	@Override
	public void createScreen() {
		initalizeWidget();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingUserInformation = fbuilder
				.setlabel("User Information").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",pic);
		FormField pic= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
//		fbuilder = new FormFieldBuilder("* Customer Name", lbCustomerName);
//		FormField flbEmployeeName = fbuilder.setMandatory(true)
//				.setMandatoryMsg("Customer Name is Mandatory").setRowSpan(0)
//				.setColSpan(0).build();
		

		fbuilder = new FormFieldBuilder("Status", cbStatus);
		FormField fcbStatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* User Id", tbUserId);
		FormField ftbUserId = fbuilder.setMandatory(true)
				.setMandatoryMsg("User Id is Mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Password", ptbPassword);
		FormField fptbPassword = fbuilder.setMandatory(true)
				.setMandatoryMsg("Password is Mandatory").setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Re Type Password", ptbReTypePassword);
		FormField fptbReTypePassword = fbuilder.setMandatory(true)
				.setMandatoryMsg("Re Type Password is Mandatory").setRowSpan(0)
				.setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Email",tbEmail);
		FormField ftbEmail = fbuilder.setMandatory(true).setMandatoryMsg("Email is Mandatory!").setRowSpan(0).setColSpan(0).build();

		FormField[][] formfield = {
				{ fgroupingUserInformation },
				{pic},
				{ ftbUserId,fptbPassword, fptbReTypePassword, fcbStatus},
				{ ftbEmail}

		};

		this.fields = formfield;
	}

	@Override
	public void updateModel(CustomerUser model) {
//		if(lbCustomerName.getValue()!=null)
//			model.setCustomerName(lbCustomerName.getValue());
		if(cbStatus.getValue()!=null)
			model.setStatus(cbStatus.getValue());
		if(tbUserId.getValue()!=null)
			model.setUserName(tbUserId.getValue());
		if(ptbPassword.getValue()!=null)
			model.setPassword(ptbPassword.getValue());
		if(pic.getValue()!=null){
			model.setCinfo(pic.getValue());
			model.setCustomerName(pic.getName().getValue());
			model.setBranch(branch);
		}
		
//		if(this.lbCustomerName.getValue()!=null)
//		{
//			Customer employee=lbCustomerName.getSelectedItem();
//			String branch=employee.getBranch();
//			model.setBranch(branch);
//			
////			Customer cust=pic.
//		}
		
		if(tbEmail.getValue()!=null){
			model.setEmail(tbEmail.getValue());
		}
		
		custUserObj=model;
		presenter.setModel(model);
	}

	@Override
	public void updateView(CustomerUser view) {
		
		custUserObj=view;
		
//		if(view.getCustomerName()!=null)
//			lbCustomerName.setValue(view.getCustomerName());
		if(view.getStatus()!=null)
			cbStatus.setValue(view.getStatus());
		if(view.getUserName()!=null)
			tbUserId.setValue(view.getUserName());
		if(view.getPassword()!=null)
		{
			ptbPassword.setValue(view.getPassword());
			ptbReTypePassword.setValue(view.getPassword());

		}
		
		if(view.getCinfo()!=null)
			pic.setValue(view.getCinfo());
		
		if(view.getEmail()!=null){
			tbEmail.setValue(view.getEmail());
		}
		presenter.setModel(view);
	}

	@Override
	public boolean validate() {
		boolean result=super.validate();
		boolean presresult=true;

		String pass=this.ptbPassword.getText().trim();
		String retypepass=ptbReTypePassword.getText().trim();

		if((pass.equals("")==false&&retypepass.equals("")==false)&&pass.equals(retypepass)==false)
		{
			ptbReTypePassword.getElement().getStyle().setBorderColor("#dd4b39");
			//ptbPassword.getElement().getStyle().setBorderColor("#dd4b39");
			presresult=false;
		}
		if(presresult==false)
			showDialogMessage("Password Mismatch!");



		return result&&presresult;
	}
	

	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CUSTOMERUSER,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void clear() {

		super.clear();
		cbStatus.setValue(true); 
	}
	
	@Override
	public void setToViewState() {
		super.setToViewState();

		SuperModel model=new CustomerUser();
		model=custUserObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.CUSTOMERUSER, custUserObj.getCount(), null,null,null, false, model, null);
	
	}
	
	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(custUserObj!=null){
			SuperModel model=new CustomerUser();
			model=custUserObj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.CUSTOMERUSER, custUserObj.getCount(), null,null,null, false, model, null);
		
		}
	}
}
