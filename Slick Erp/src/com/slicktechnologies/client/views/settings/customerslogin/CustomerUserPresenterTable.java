package com.slicktechnologies.client.views.settings.customerslogin;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;

public class CustomerUserPresenterTable extends SuperTable<CustomerUser>{

	TextColumn<CustomerUser>getCustId;
	TextColumn<CustomerUser>getCustName;
	TextColumn<CustomerUser>getCustUserId;
	TextColumn<CustomerUser>getCustBranch;
	TextColumn<CustomerUser>getCustStatus;
	/**
	 * anil Added this email column in table 
	 */
	TextColumn<CustomerUser>getCustEmailColumn;
	
	@Override
	public void createTable() {
		getCustId();
		getCustName();
		getCustUserId();
		getCustBranch();
		getCustEmailColumn();
		getCustStatus();
	}

	
	private void getCustEmailColumn() {
		getCustEmailColumn=new TextColumn<CustomerUser>() {
			@Override
			public String getValue(CustomerUser object) {
				
				if(object.getEmail()!=null){
					return object.getEmail() + "";
				}
				return "";
			}
		};
		table.addColumn(getCustEmailColumn, "Email");
		table.setColumnWidth(getCustEmailColumn,180,Unit.PX);
		getCustEmailColumn.setSortable(true);
	}
	
	private void getCustId() {
		getCustId=new TextColumn<CustomerUser>() {
			@Override
			public String getValue(CustomerUser object) {
				return object.getCount() + "";
			}
		};
		table.addColumn(getCustId, "Id");
		table.setColumnWidth(getCustId,120,Unit.PX);
		getCustId.setSortable(true);
	}

	private void getCustName() {
		getCustName=new TextColumn<CustomerUser>() {
			@Override
			public String getValue(CustomerUser object) {
				return object.getCustomerName() + "";
			}
		};
		table.addColumn(getCustName, "Customer");
		table.setColumnWidth(getCustName,120,Unit.PX);
		getCustName.setSortable(true);
		
	}

	private void getCustUserId() {
		getCustUserId=new TextColumn<CustomerUser>() {
			@Override
			public String getValue(CustomerUser object) {
				return object.getUserName() + "";
			}
		};
		table.addColumn(getCustUserId, "User Id");
		table.setColumnWidth(getCustUserId,120,Unit.PX);
		getCustUserId.setSortable(true);
		
	}

	private void getCustBranch() {
		getCustBranch=new TextColumn<CustomerUser>() {
			@Override
			public String getValue(CustomerUser object) {
				return object.getBranch() + "";
			}
		};
		table.addColumn(getCustBranch, "Branch");
		table.setColumnWidth(getCustBranch,120,Unit.PX);
		getCustBranch.setSortable(true);
		
	}

	private void getCustStatus() {
		getCustStatus=new TextColumn<CustomerUser>() {
			@Override
			public String getValue(CustomerUser object) {
				if(object.getStatus()){
					return "Active";
				}else{
					return "In Active";
				}
			}
		};
		table.addColumn(getCustStatus, "Status");
		table.setColumnWidth(getCustStatus,120,Unit.PX);
		getCustStatus.setSortable(true);
		
	}
	
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetCustomerName();
		addSortinggetUserId();
		addSortinggetBranch();
		addSortinggetStatus();
		addSortinggetEmail();
	}
	
	private void addSortinggetEmail() {
		List<CustomerUser> list = getDataprovider().getList();
		columnSort = new ListHandler<CustomerUser>(list);
		columnSort.setComparator(getCustEmailColumn, new Comparator<CustomerUser>() {
			@Override
			public int compare(CustomerUser e1, CustomerUser e2) {
				if (e1 != null && e2 != null) {
					if (e1.getEmail() != null && e2.getEmail() != null) {
						return e1.getEmail().compareTo(e2.getEmail());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}
	
	private void addSortinggetStatus() {
		List<CustomerUser> list = getDataprovider().getList();
		columnSort = new ListHandler<CustomerUser>(list);
		columnSort.setComparator(getCustStatus, new Comparator<CustomerUser>() {
			@Override
			public int compare(CustomerUser e1, CustomerUser e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null && e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortinggetUserId() {
		List<CustomerUser> list = getDataprovider().getList();
		columnSort = new ListHandler<CustomerUser>(list);
		columnSort.setComparator(getCustUserId, new Comparator<CustomerUser>() {
			@Override
			public int compare(CustomerUser e1, CustomerUser e2) {
				if (e1 != null && e2 != null) {
					if (e1.getUserName() != null && e2.getUserName() != null) {
						return e1.getUserName().compareTo(e2.getUserName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortinggetCustomerName() {
		List<CustomerUser> list = getDataprovider().getList();
		columnSort = new ListHandler<CustomerUser>(list);
		columnSort.setComparator(getCustName, new Comparator<CustomerUser>() {
			@Override
			public int compare(CustomerUser e1, CustomerUser e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCustomerName() != null && e2.getCustomerName() != null) {
						return e1.getCustomerName().compareTo(e2.getCustomerName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	protected void addSortinggetCount()
	{
		List<CustomerUser> list = getDataprovider().getList();
		columnSort = new ListHandler<CustomerUser>(list);
		columnSort.setComparator(getCustId, new Comparator<CustomerUser>() {
			@Override
			public int compare(CustomerUser e1, CustomerUser e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	protected void addSortinggetBranch()
	{
		List<CustomerUser> list = getDataprovider().getList();
		columnSort = new ListHandler<CustomerUser>(list);
		columnSort.setComparator(getCustBranch, new Comparator<CustomerUser>() {
			@Override
			public int compare(CustomerUser e1, CustomerUser e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranch() != null && e2.getBranch() != null) {
						return e1.getBranch().compareTo(e2.getBranch());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
