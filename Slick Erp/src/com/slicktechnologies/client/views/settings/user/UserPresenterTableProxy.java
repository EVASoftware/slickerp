package com.slicktechnologies.client.views.settings.user;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.helperlayer.User;

public class UserPresenterTableProxy extends SuperTable<User> {

	TextColumn<User> getUserIdCol;
	TextColumn<User> getUserEmpIdCol;
	TextColumn<User> getUserEmpCol;
	TextColumn<User> getUserUserIdCol;
	TextColumn<User> getUserBranchCol;
	TextColumn<User> getUserEmailCol;
	TextColumn<User> getUserUserRoleCol;
	TextColumn<User> getUserStatusCol;
	
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		getUserIdCol();
		getUserEmpIdCol();
		getUserEmpCol();
		getUserUserIdCol();
		getUserEmailCol();
		getUserBranchCol();
		getUserUserRoleCol();
		getUserStatusCol();
		
	}

	
	
	private void getUserIdCol() {
		// TODO Auto-generated method stub
		getUserIdCol=new TextColumn<User>() {
			@Override
			public String getValue(User object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getUserIdCol,"Id");
		table.setColumnWidth(getUserIdCol,90, Unit.PX);
		getUserIdCol.setSortable(true);
	}

	private void getUserEmpIdCol() {
		// TODO Auto-generated method stub
		getUserEmpIdCol=new TextColumn<User>() {
			@Override
			public String getValue(User object) {
				if(object.getEmpCount()!=null)
					return object.getEmpCount()+"";
				else
					return "";
			}
		};
		table.addColumn(getUserEmpIdCol,"Id");
		table.setColumnWidth(getUserEmpIdCol,90, Unit.PX);

	}



	private void getUserEmpCol() {
		// TODO Auto-generated method stub
		getUserEmpCol=new TextColumn<User>() {
			@Override
			public String getValue(User object) {
				if(object.getEmployeeName()!=null){
					return object.getEmployeeName();
				}
				return "";
			}
		};
		table.addColumn(getUserEmpCol,"Employee");
		table.setColumnWidth(getUserEmpCol,90, Unit.PX);
		getUserEmpCol.setSortable(true);
	}



	private void getUserUserIdCol() {
		// TODO Auto-generated method stub
		getUserUserIdCol=new TextColumn<User>() {
			@Override
			public String getValue(User object) {
				if(object.getUserName()!=null){
					return object.getUserName();
				}
				return "";
			}
		};
		table.addColumn(getUserUserIdCol,"User Id");
		table.setColumnWidth(getUserUserIdCol,90, Unit.PX);
		getUserUserIdCol.setSortable(true);
	}



	private void getUserEmailCol() {
		// TODO Auto-generated method stub
		getUserEmailCol=new TextColumn<User>() {
			@Override
			public String getValue(User object) {
				if(object.getEmail()!=null){
					return object.getEmail();
				}
				return "";
			}
		};
		table.addColumn(getUserEmailCol,"Email");
		table.setColumnWidth(getUserEmailCol,110, Unit.PX);
		getUserEmailCol.setSortable(true);
	}



	private void getUserBranchCol() {
		// TODO Auto-generated method stub
		getUserBranchCol=new TextColumn<User>() {
			@Override
			public String getValue(User object) {
				if(object.getBranch()!=null){
					return object.getBranch();
				}
				return "";
			}
		};
		table.addColumn(getUserBranchCol,"Branch");
		table.setColumnWidth(getUserBranchCol,110, Unit.PX);
		getUserBranchCol.setSortable(true);
		
	}



	private void getUserUserRoleCol() {
		// TODO Auto-generated method stub
		getUserUserRoleCol=new TextColumn<User>() {
			@Override
			public String getValue(User object) {
				if(object.getRole()!=null&&object.getRoleName()!=null){
					return object.getRoleName();
				}
				return "";
			}
		};
		table.addColumn(getUserUserRoleCol,"User Role");
		table.setColumnWidth(getUserUserRoleCol,90, Unit.PX);
		getUserUserRoleCol.setSortable(true);
	}



	private void getUserStatusCol() {
		// TODO Auto-generated method stub
		getUserStatusCol=new TextColumn<User>() {
			@Override
			public String getValue(User object) {
				if(object.getStatus()==true){
					return "Active";
				}
				return "Inactive";
			}
		};
		table.addColumn(getUserStatusCol,"Status");
		table.setColumnWidth(getUserStatusCol,110, Unit.PX);
		getUserStatusCol.setSortable(true);
	}



	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		addSortingOnUserIdCol();
		addSortingOnUserEmpCol();
		addSortingOnUserUserIdCol();
		addSortingOnUserEmailCol();
		addSortingOnUserBranchCol();
		addSortingOnUserUserRoleCol();
		addSortingOnUserStatusCol();
	}



	private void addSortingOnUserIdCol() {
		List<User> list=getDataprovider().getList();
		columnSort=new ListHandler<User>(list);
		columnSort.setComparator(getUserIdCol, new Comparator<User>()
		{
			@Override
			public int compare(User e1,User e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}



	private void addSortingOnUserEmpCol() {
		List<User> list=getDataprovider().getList();
		columnSort=new ListHandler<User>(list);
		columnSort.setComparator(getUserEmpCol, new Comparator<User>()
		{
			@Override
			public int compare(User e1,User e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeeName()!=null && e2.getEmployeeName()!=null){
						return e1.getEmployeeName().compareTo(e2.getEmployeeName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnUserUserIdCol() {
		List<User> list=getDataprovider().getList();
		columnSort=new ListHandler<User>(list);
		columnSort.setComparator(getUserUserIdCol, new Comparator<User>()
		{
			@Override
			public int compare(User e1,User e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getUserName()!=null && e2.getUserName()!=null){
						return e1.getUserName().compareTo(e2.getUserName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnUserEmailCol() {
		List<User> list=getDataprovider().getList();
		columnSort=new ListHandler<User>(list);
		columnSort.setComparator(getUserEmailCol, new Comparator<User>()
		{
			@Override
			public int compare(User e1,User e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmail()!=null && e2.getEmail()!=null){
						return e1.getEmail().compareTo(e2.getEmail());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnUserBranchCol() {
		List<User> list=getDataprovider().getList();
		columnSort=new ListHandler<User>(list);
		columnSort.setComparator(getUserBranchCol, new Comparator<User>()
		{
			@Override
			public int compare(User e1,User e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnUserUserRoleCol() {
		List<User> list=getDataprovider().getList();
		columnSort=new ListHandler<User>(list);
		columnSort.setComparator(getUserUserRoleCol, new Comparator<User>()
		{
			@Override
			public int compare(User e1,User e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getRoleName()!=null && e2.getRoleName()!=null){
						return e1.getRoleName().compareTo(e2.getRoleName());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	private void addSortingOnUserStatusCol() {
		List<User> list=getDataprovider().getList();
		columnSort=new ListHandler<User>(list);
		columnSort.setComparator(getUserStatusCol, new Comparator<User>()
		{
			@Override
			public int compare(User e1,User e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}



	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
