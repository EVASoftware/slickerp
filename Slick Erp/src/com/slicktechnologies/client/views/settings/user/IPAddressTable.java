package com.slicktechnologies.client.views.settings.user;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;

public class IPAddressTable extends SuperTable<String>{

	TextColumn<String> ipaddressColumn;
	Column<String, String> deleteColumn;
	
	
	public IPAddressTable()
	    {
	    	super();
	    }

	@Override
	public void createTable() {

		createColumnIPAddress();
		createColumnDelete();
		 table.setWidth("100%");
        createFieldUpdaterdeleteColumn();
		
	}

	private void createFieldUpdaterdeleteColumn() {

		deleteColumn.setFieldUpdater(new FieldUpdater<String, String>() {
			
			@Override
			public void update(int index, String object, String value) {

				 getDataprovider().getList().remove(index);
	                table.redrawRow(index);
			}
		});
	}

	private void createColumnDelete() {
		 ButtonCell btnCell = new ButtonCell();
		deleteColumn=new Column<String, String>(btnCell) {
			
			@Override
			public String getValue(String object) {
				return "Delete";
			}
		};
		
        table.addColumn(deleteColumn, "Delete");

	}

	private void createColumnIPAddress() {
		ipaddressColumn = new TextColumn<String>() {
			
			@Override
			public String getValue(String object) {
				return object;
			}
		};
		 table.addColumn(ipaddressColumn, "IP Address");
		 ipaddressColumn.setSortable(true);
	}
	
	
	
	
	@Override
	public void setEnable(boolean state)
	{
        for(int i=table.getColumnCount()-1;i>-1;i--)
    	  table.removeColumn(i); 
          if(state==true)
          {
        	  createColumnIPAddress();
        	  createColumnDelete();
      		createFieldUpdaterdeleteColumn();
          }
          
          else
          {
        	  createColumnIPAddress();
        	  createColumnDelete();
      	
          }
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
}
