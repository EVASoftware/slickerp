package com.slicktechnologies.client.views.settings.user;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.*;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.*;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.simplesoftwares.client.library.appstructure.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.UserRole;
import com.google.gwt.event.dom.client.*;
/**
 * FormTablescreen template.
 */
public class UserForm extends FormTableScreen<User>  implements ClickHandler {

	//Token to add the varialble declarations
	ObjectListBox<Config> lbAccessLevel;
	ObjectListBox<Branch>lblBranch;
	CheckBox cbStatus;
	TextBox tbUserId;
	PasswordTextBox ptbPassword,ptbReTypePassword;
	ObjectListBox<Employee> lbEmployeeName;
	ObjectListBox<UserRole> olbRole;

	TextBox tbIPAddress,tbloginIPAddress,tbCallerID;
	Button btnAdd;
	IPAddressTable ipAddressTable;

	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;

	/**
	 * This email box stores user email address
	 * Date 17-09-2016 By Anil
	 * Release : 30 Sept 2016
	 */
		EmailTextBox tbEmail;
	
	/**
	 * End
	 */
	
	User userobj;

	public UserForm  (SuperTable<User> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();

	}

	public UserForm  (SuperTable<User> table, int mode,
			boolean captionmode, boolean popupFlag) {
		super(table, mode, captionmode, popupFlag);
		createGui();

	}
	
	private void initalizeWidget()
	{
		Console.log("Inside user form initialize4444444444444444");
		lblBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(lblBranch);

		lbAccessLevel=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(lbAccessLevel,Screen.ACESSLEVEL);

		cbStatus=new CheckBox();
		cbStatus.setValue(true);  
		tbUserId=new TextBox();

		ptbPassword=new PasswordTextBox();

		ptbReTypePassword=new PasswordTextBox();

		lbEmployeeName=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(lbEmployeeName);

		
		tbIPAddress = new TextBox();
		btnAdd = new Button("Add");
		btnAdd.addClickHandler(this);
		
		ipAddressTable = new IPAddressTable();
		ipAddressTable.connectToLocal();
		
		Console.log("Inside user form initialize 555555555555555555");
		olbRole=new ObjectListBox<UserRole>();
		MyQuerry qu= new MyQuerry();
//		Filter filter=new Filter();
//		filter.setQuerryString("roleStatus");
//		filter.setBooleanvalue(true);
//		qu.getFilters().add(filter);
		qu.setQuerryObject(new UserRole());
		olbRole.MakeLive(qu);
		Console.log("Inside user form initialize 666666666666666666");
		tbEmail=new EmailTextBox();
		
		tbloginIPAddress =new TextBox();
		tbCallerID=new TextBox();

	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();



		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingUserInformation=fbuilder.setlabel("User Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("* Employee Name",lbEmployeeName);
		FormField flbEmployeeName= fbuilder.setMandatory(true).setMandatoryMsg("Employee Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Branch",lblBranch);
		FormField flblBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* User Name",tbUserId);
		FormField ftbUserId= fbuilder.setMandatory(true).setMandatoryMsg("User Id is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Password",ptbPassword);
		FormField fptbPassword= fbuilder.setMandatory(true).setMandatoryMsg("Password is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Confirm Password",ptbReTypePassword);
		FormField fptbReTypePassword= fbuilder.setMandatory(true).setMandatoryMsg("Re Type Password is Mandatory").setRowSpan(0).setColSpan(0).build();

		FormField ipInfo=fbuilder.setlabel("IP Address Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("* User Role",olbRole);
		FormField folbRole= fbuilder.setMandatory(true).setMandatoryMsg("Role is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder(" IP Address",tbIPAddress);
		FormField ftbIPAddress = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnAdd);
		FormField fbtnAdd = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",ipAddressTable.getTable());
		FormField fipAddressTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Email Id",tbEmail);
		FormField ftbEmail = fbuilder.setMandatory(false).setMandatoryMsg("Email is Mandatory!").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Last Login IP Address",tbloginIPAddress);
		FormField ftbloginIPAddress = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Caller ID",tbCallerID);
		FormField ftbCallerID = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingUserInformation},
				{flbEmployeeName,ftbUserId,fcbStatus},
				{folbRole,fptbPassword,fptbReTypePassword},
				{ftbEmail,ftbloginIPAddress,ftbCallerID},
				{ipInfo},
				{ftbIPAddress,fbtnAdd},
				{fipAddressTable}


		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(User model) 
	{
		//Console.log("in updatemodel of user");
		if(lbEmployeeName.getValue()!=null){
			model.setEmployeeName(lbEmployeeName.getValue());
			Employee e=lbEmployeeName.getSelectedItem();
			model.setEmpCount(e.getCount());
		//	Console.log("in updatemodel empid="+e.getCount());
		}
		
		
		
		/*if(lblBranch.getValue()!=null)
  model.setBranch(lblBranch.getValue());*/
		if(lbAccessLevel.getValue()!=null)
			model.setAccessLevel(lbAccessLevel.getValue());
		if(cbStatus.getValue()!=null)
			model.setStatus(cbStatus.getValue());
		if(tbUserId.getValue()!=null)
			model.setUserName(tbUserId.getValue());
		if(ptbPassword.getValue()!=null)
			model.setPassword(ptbPassword.getValue());
		if(this.lbEmployeeName.getValue()!=null)
		{
			Employee employee=lbEmployeeName.getSelectedItem();
			String branch=employee.getBranchName();
			model.setBranch(branch);
		}
		if(olbRole.getValue()!=null)
		{
			model.setRole(olbRole.getSelectedItem());
		}

//		System.out.println("RRRRRRRR"+ipAddressTable.getDataprovider().getList().size());
//		model.setIpAddress(ipAddressTable.getDataprovider().getList());
		
		model.setIpAddress(ipAddressTable.getValue());
		
		if(tbEmail.getValue()!=null){
			model.setEmail(tbEmail.getValue());
		}
		
		if(tbloginIPAddress.getValue()!=null){
			model.setLoginIPAddress(tbloginIPAddress.getValue());
		}
		
		if(tbCallerID.getValue()!=null){
			model.setCallerId(tbCallerID.getValue());
			LoginPresenter.loggedInUserCallerId=tbCallerID.getValue();
		}else
			model.setCallerId("");
		
		manageGlobalList(model);
		
		userobj=model;
		presenter.setModel(model);


	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(User view) 
	{

		userobj=view;
		
	
		
		if(view.getEmployeeName()!=null)
			lbEmployeeName.setValue(view.getEmployeeName());
		/*if(view.getBranch()!=null)
  		lblBranch.setValue(view.getBranch());*/
		if(view.getAccessLevel()!=null)
			lbAccessLevel.setValue(view.getAccessLevel());
		if(view.getStatus()!=null)
			cbStatus.setValue(view.getStatus());
		if(view.getUserName()!=null)
			tbUserId.setValue(view.getUserName());
		if(view.getPassword()!=null)
		{
			ptbPassword.setValue(view.getPassword());
			ptbReTypePassword.setValue(view.getPassword());

		}
		if(view.getRole()!=null)
		{
			olbRole.setValue(view.getRole().toString());
		}
		ipAddressTable.setValue(view.getIpAddress());
		
		if(view.getEmail()!=null){
			tbEmail.setValue(view.getEmail());
		}
		if(view.getLoginIPAddress()!=null){
			tbloginIPAddress.setValue(view.getLoginIPAddress());
		}
		if(view.getCallerId()!=null)
			tbCallerID.setValue(view.getCallerId());
		
		presenter.setModel(view);
	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			/**
			 * @author Vijay Date - 24-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION)|| text.equals("Download"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			/**
			 * @author Vijay Date - 24-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			/**
			 * @author Vijay Date - 24-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION)|| text.equals("Download"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.USER,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{

	}


	@Override
	public void onClick(ClickEvent event) {

		if(event.getSource().equals(btnAdd)){
			System.out.println("11111");
			if(!tbIPAddress.getValue().equals("")){
				System.out.println("2222222");
				if(validateIP()){
					System.out.println("333333333");
					ipAddressTable.getDataprovider().getList().add(tbIPAddress.getValue());
					tbIPAddress.setValue("");
					
				}
			}else{
				showDialogMessage("Please Add IP Address");
			}
			
			
		}
	}
	
	private boolean validateIP() {
		
		if(ipAddressTable.getDataprovider().getList().size()!=0){
			for(int i=0;i<ipAddressTable.getDataprovider().getList().size();i++){
				if(ipAddressTable.getDataprovider().getList().get(i).equals(tbIPAddress.getValue())){
					
					showDialogMessage("Can not duplicate IP Address!");
					return false;
				}
			}
		}
		
		return true;
	}

	/**
	 * Extra validation which matches the typed pass word from retyped passwords
	 */
	@Override
	public boolean validate()
	{
		boolean result=super.validate();
		boolean presresult=true;

		String pass=this.ptbPassword.getText().trim();
		String retypepass=ptbReTypePassword.getText().trim();

		if((pass.equals("")==false&&retypepass.equals("")==false)&&pass.equals(retypepass)==false)
		{
			ptbReTypePassword.getElement().getStyle().setBorderColor("#dd4b39");
			//ptbPassword.getElement().getStyle().setBorderColor("#dd4b39");
			presresult=false;
		}
		if(presresult==false)
			showDialogMessage("Password Mismatch!");



		return result&&presresult;

	}


	@Override
	public void clear() {

		super.clear();
		cbStatus.setValue(true); 
	}


	@Override
	public void setToViewState() {
		super.setToViewState();

		SuperModel model=new User();
		model=userobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.USER, userobj.getCount(), null,null,null, false, model, null);
	
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(userobj!=null){
			SuperModel model=new User();
			model=userobj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.USER, userobj.getCount(), null,null,null, false, model, null);
		
		}
	}


	public void manageGlobalList(User userModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalUserEntitylist.add(userModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalUserEntitylist.size();i++)
			{
				if(LoginPresenter.globalUserEntitylist.get(i).getId().equals(userModel.getId()))
				{
					LoginPresenter.globalUserEntitylist.add(userModel);
					LoginPresenter.globalUserEntitylist.remove(i);
				}
			}
		}
		
	}

}
