package com.slicktechnologies.client.views.settings.user;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.slicktechnologies.client.utils.Console;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;

import java.util.Vector;

import javax.jws.soap.SOAPBinding.Use;

import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.slicktechnologies.shared.common.paymentlayer.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
/**
 *  FormTableScreen presenter template
 */
public class UserPresenter extends FormTableScreenPresenter<User> implements ChangeHandler,ValueChangeHandler<String> {

	//Token to set the concrete form
	UserForm form;
	CsvServiceAsync async=GWT.create(CsvService.class);
	
	public UserPresenter (FormTableScreen<User> view,
			User model) {
		super(view, model);
		form=(UserForm) view;
		
		Console.log("inside Presenter Const");
		form.getSupertable().connectToLocal();
		form.retriveTable(getUserQuery());
		form.setPresenter(this);
		
		/**
		 * Date 17-09-2016 By Anil
		 * Release 30 Sept 2016
		 */
		form.lbEmployeeName.addChangeHandler(this);
		form.tbUserId.addValueChangeHandler(this);
		form.tbEmail.addValueChangeHandler(this);
		/**
		 * End
		 */
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals(""))
  reactTo();

		
	}
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new User();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getUserQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new User());
		return quer;
	}
	
	public void setModel(User entity)
	{
		model=entity;
	}
	
	public static UserForm initalize()
	{
			Console.log("Inside user form initialize");
//			 UserPresenterTable gentableScreen=GWT.create(UserPresenterTable.class);
			UserPresenterTableProxy gentableScreen=new UserPresenterTableProxy(); 
			UserForm  form=new  UserForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
			gentableScreen.setView(form);
			gentableScreen.applySelectionModle();
			
//			try{
////			   UserPresenterTable gentableSearch=GWT.create(UserPresenterTable.class);
//			   Console.log("Inside user form initialize 000000");
//			   TextColumn<User>salesPerson=(TextColumn<User>) gentableScreen.getTable().getColumn(0);
//			   Console.log("Inside user form initialize 01010101010101010");
//			   gentableScreen.getTable().setColumnWidth(salesPerson,60,Unit.PX);
//			}
//			catch(Exception e){
//				  Console.log("111111111111"+e.getStackTrace());
//			}
			    // UserPresenterSearch.staticSuperTable=gentableSearch;
			     //  UserPresenterSearch searchpopup=GWT.create( UserPresenterSearch.class);
			       //    form.setSearchpopupscreen(searchpopup);
			
			Console.log("Inside user form initialize 2222222");
			UserPresenter  presenter=new  UserPresenter  (form,new User());
			AppMemory.getAppMemory().stickPnel(form);
			Console.log("Inside user form initialize 333333333");
			return form;
			
			
		 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.helperlayer.User")
		 public static class  UserPresenterTable extends SuperTable<User> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.helperlayer.User")
			 public static class  UserPresenterSearch extends SearchPopUpScreen<  User>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
	  private void reactTo()
  {
}

	@Override
	public void onChange(ChangeEvent event) {
		
		if(event.getSource().equals(form.lbEmployeeName)){
			if(form.lbEmployeeName.getSelectedIndex()!=0){
				Employee employee=form.lbEmployeeName.getSelectedItem();
				if(employee.getEmail()!=null){
					form.tbEmail.setValue(employee.getEmail());
					checkEmailInCustomerUser(form.tbEmail.getValue());
				}
			}else{
				form.tbEmail.setValue("");
			}
		}
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		
		if(event.getSource().equals(form.tbUserId)){
			validateUserName();
		}
		if(event.getSource().equals(form.tbEmail)){
			checkEmailInCustomerUser(form.tbEmail.getValue());
		}
	}
	

	/**
	 * This method checks whether passed/entered email is available or not
	 * it checks email from CustomerUser 
	 * Date : 17-09-2016 By Anil
	 * Release 30 Sept 2016
	 * I/P : Email Id
	 * 
	 */
	
	private void checkEmailInCustomerUser(String email) {
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setLongValue(c.getCompanyId());
		temp.setQuerryString("companyId");
		filtervec.add(temp);
		
		
		temp=new Filter();
		temp.setStringValue(email);
		temp.setQuerryString("email");
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerUser());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			 
            public void onSuccess(ArrayList<SuperModel> result)
            {
            	form.hideWaitSymbol();
            	System.out.println(" result size :: "+result.size());
            	if(result.size()!=0)
            	{
            		CustomerUser entity=(CustomerUser) result.get(0);
            		form.showDialogMessage("Email id is already used!");
					form.tbEmail.setValue("");
            	}
            }

            public void onFailure(Throwable throwable)
            {
            	form.showDialogMessage("An unexpected error occurred!");
            	form.hideWaitSymbol();
            	throwable.printStackTrace();
            }

        });
	}
	
	
	/**
	 * This method checks whether entered user id is available or not
	 * it checks user id from CustomerUser 
	 * Date : 17-09-2016 By Anil
	 * Release 30 Sept 2016
	 * I/P : user Id
	 * 
	 */
	
	
	private void validateUserName() {
		
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setLongValue(c.getCompanyId());
		temp.setQuerryString("companyId");
		filtervec.add(temp);
		
		
		temp=new Filter();
		temp.setStringValue(form.tbUserId.getValue());
		temp.setQuerryString("userName");
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerUser());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			 
            public void onSuccess(ArrayList<SuperModel> result)
            {
            	form.hideWaitSymbol();
            	System.out.println(" result size :: "+result.size());
            	if(result.size()!=0)
            	{
            		CustomerUser entity=(CustomerUser) result.get(0);
            		form.showDialogMessage("User id is not available!");
					form.tbUserId.setValue("");
            	}
            }

            public void onFailure(Throwable throwable)
            {
            	form.showDialogMessage("An unexpected error occurred!");
            	form.hideWaitSymbol();
            	throwable.printStackTrace();
            }

        });
	}
	
	@Override
	public void reactOnDownload() {
		
		/**
		 * Date 2-03-2017 added by vijay
		 * for User Download
		 */
		
		ArrayList<User> userarray=new ArrayList<User>();
		List<User> list=form.getSupertable().getListDataProvider().getList();
		
		userarray.addAll(list); 
		
		async.setUserList(userarray, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+121;
				Window.open(url, "test", "enabled");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
			}
		});
		
		/**
		 * ends here
		 */
		
	}

}
