package com.slicktechnologies.client.views.settings.employee;

import java.util.Vector;

import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.settings.employee.EmployeePresenter.EmployeePresenterSearch;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.sun.corba.se.impl.ior.OldPOAObjectKeyTemplate;

public class EmployeePresenterSearchProxy extends EmployeePresenterSearch {
	
	public EmployeeInfoComposite personInfo;
	public ObjectListBox<Branch> olbBranch;
	public ObjectListBox<Department> olbDepartMent;
	public ObjectListBox<Config> olbEmploymentType;
	public ObjectListBox<Config> olbDesignation;
	public ObjectListBox<Country> olbCountry;
	public ObjectListBox<Config> olbRole;
	public TextBox idCardNumber;
	public TextBox accessCardNumber;


	public ObjectListBox<Employee> olbReportsTo;
	public ListBox lbStatus;
	public DateComparator dateComparator;
	public ObjectListBox<ConfigCategory> olbConfigCustomerCategory;
	public ObjectListBox<Config>olbCustomerGroup;
	public ObjectListBox<Type> olbConfigCustomerType;

	public ObjectListBox<Config> olbConfigCustomerLevel;
	public ObjectListBox<Config> olbConfigCustomerPriority;

       /**Date 4-4-2019 added by Amol ***/
	public ObjectListBox<HrProject>olbProjectName;
	
	/**Date 17-5-2019by Amol Added City AND Locality**/
	public ObjectListBox<City>olbCity;
	public ObjectListBox<Locality>olbLocality;
	EmployeeForm empForm;
	
	/**
	 * @author Anil , Date : 25-05-2019
	 */
	ListBox lbFnfStatus;
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("olbBranch"))
			return this.olbBranch;
		if(varName.equals("olbSalesPerson"))
			return this.olbReportsTo;
		if(varName.equals("lbStatus"))
			return this.lbStatus;
		if(varName.equals("dateComparator"))
			return this.dateComparator;
		if(varName.equals("olbConfigCustomerCategory"))
			return this.olbConfigCustomerCategory;
		if(varName.equals("olbConfigCustomerLevel"))
			return this.olbConfigCustomerLevel;
		if(varName.equals("olbConfigCustomerPriority"))
			return this.olbConfigCustomerPriority;
		if(varName.equals("olbConfigCustomerType"))
			return this.olbConfigCustomerType;
		if(varName.equals("personInfo"))
			return this.personInfo;
		return null ;
	}
	public EmployeePresenterSearchProxy()
	{
		super();
		createGui();
	}
	public void initWidget()
	{
		/***Date 24-5-2019 by AMOL change the method to load the all Employees ***/
		personInfo=AppUtility.InitialiseEmployeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
		olbDepartMent=new ObjectListBox<Department>();
		AppUtility.makeSalesPersonListBoxDepartment(olbDepartMent);
		olbEmploymentType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbEmploymentType,Screen.EMPLOYEETYPE);
		olbDesignation=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbDesignation,Screen.EMPLOYEEDESIGNATION);
		olbRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbRole,Screen.EMPLOYEEROLE);
		olbReportsTo= new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbReportsTo);
		idCardNumber=new TextBox();
		accessCardNumber=new TextBox();
		
		olbCountry=new ObjectListBox<Country>();
		olbCountry.MakeLive(new MyQuerry(new Vector<Filter>(),new Country()));
		lbStatus= new ListBox();
		lbStatus.addItem("--Select--");
		lbStatus.addItem("Active");
		lbStatus.addItem("Inactive");
		
		olbProjectName=new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(olbProjectName);
		
		olbCity=new ObjectListBox<City>();
		City.makeOjbectListBoxLive(olbCity);
		
		olbLocality=new ObjectListBox<Locality>();
		Locality.MakeObjectListBoxLive(olbLocality);
		
			lbFnfStatus= new ListBox();
		lbFnfStatus.addItem("--Select--");
		lbFnfStatus.addItem("Initiated");
		lbFnfStatus.addItem("Completed");
		
		dateComparator= new DateComparator("fnfDate",new Employee());
	}
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Department",olbDepartMent);
		FormField folbdepartment= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Employment Type",olbEmploymentType);
		FormField folbtype= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Employee Designation",olbDesignation);
		FormField folbdesignation= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Employee Role",olbRole);
		FormField folbrole= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Base Country",olbCountry);
		FormField folbCountry= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();


		builder = new FormFieldBuilder("Reports To",olbReportsTo);
		FormField folbSalesPerson= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("ID Card No.",idCardNumber);
		FormField fidCard= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Access Card No.",accessCardNumber);
		FormField facesscard= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Status",lbStatus);
		FormField fstatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

       builder=new FormFieldBuilder("Project Name",olbProjectName);
       FormField folbProjectName= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
       
       builder=new FormFieldBuilder("City",olbCity);
       FormField folbCity= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
       
       builder=new FormFieldBuilder("Locality",olbLocality);
       FormField folbLocality= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
       
       	builder = new FormFieldBuilder("FNF Status",lbFnfStatus);
		FormField flbFnfStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("From FNF Date",dateComparator.getFromDate());
		FormField fStartDate= builder.setMandatory(false).setRowSpan(1).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To FNF Date",dateComparator.getToDate());
		FormField fEndDate= builder.setMandatory(false).setRowSpan(1).setColSpan(0).build();

		this.fields=new FormField[][]{
				{fpersonInfo},
				{folbCountry,folbBranch,folbdepartment,folbtype,folbdesignation},
				{folbrole,folbSalesPerson,fidCard,facesscard,fstatus},
				{folbProjectName,folbCity,folbLocality,flbFnfStatus},
				{fStartDate,fEndDate}
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;


		if(olbDepartMent.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(olbDepartMent.getValue().trim());
			temp.setQuerryString("departMent");
			filtervec.add(temp);
		}
		
		if(lbStatus.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setQuerryString("status");
			
			String selVal=lbStatus.getItemText(lbStatus.getSelectedIndex());
			if(selVal.equals("Active"))
			{
				temp.setBooleanvalue(true);
			}
			
			else
			{
				temp.setBooleanvalue(false);
			}
			
			filtervec.add(temp);
		}

		if(olbCountry.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(olbCountry.getValue().trim());
			temp.setQuerryString("country");
			filtervec.add(temp);
		}

		if(olbEmploymentType.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(olbEmploymentType.getValue().trim());
			temp.setQuerryString("employeeType");
			filtervec.add(temp);
		}

		if(olbRole.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbRole.getValue().trim());
			temp.setQuerryString("roleName");
			filtervec.add(temp);
		}
		if(olbDesignation.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbDesignation.getValue().trim());
			temp.setQuerryString("designation");
			filtervec.add(temp);
		}

		if(LoginPresenter.branchRestrictionFlag==true)
		{
			temp=new Filter();
			temp.setStringValue(olbBranch.getValue(olbBranch.getSelectedIndex()).trim());
			temp.setQuerryString("branchName");
			filtervec.add(temp);
		}
		else
		{
		if(olbBranch.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbBranch.getValue().trim());
			temp.setQuerryString("branchName");
			filtervec.add(temp);
		}
		}
		if(olbReportsTo.getSelectedIndex()!=0)
		{
			temp=new Filter();
			temp.setStringValue(olbReportsTo.getValue().trim());
			temp.setQuerryString("reportsTo");
			filtervec.add(temp);
		}
		if(accessCardNumber.getText().trim().equals("")==false){
			temp=new Filter();temp.setStringValue(accessCardNumber.getText().trim());
			temp.setQuerryString("accessCardNo");
			filtervec.add(temp);
		}

		if(idCardNumber.getText().trim().equals("")==false){
			temp=new Filter();temp.setStringValue(idCardNumber.getText().trim());
			temp.setQuerryString("idCardNo");
			filtervec.add(temp);
		}



		{


			if(!personInfo.getId().getValue().equals(""))
			{  
				temp=new Filter();
				temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
				temp.setQuerryString("count");
				filtervec.add(temp);
			}

			if(!(personInfo.getName().getValue().equals("")))
			{
				temp=new Filter();
				temp.setStringValue(personInfo.getName().getValue());
				temp.setQuerryString("fullname");
				filtervec.add(temp);
			}
			if(!personInfo.getPhone().getValue().equals(""))
			{
				temp=new Filter();
				temp.setLongValue(personInfo.getCellNumber());
				temp.setQuerryString("contacts.cellNo1");
				filtervec.add(temp);
			}		}
		  if(olbProjectName.getValue()!=null){
				temp=new Filter();
				temp.setStringValue(olbProjectName.getValue());
				temp.setQuerryString("projectName");
				filtervec.add(temp);
		 }
		  
		  if(olbCity.getSelectedIndex() != 0){
			 temp=new Filter();
			 temp.setStringValue(olbCity.getValue(olbCity.getSelectedIndex()));
			 temp.setQuerryString("contacts.address.city");
				filtervec.add(temp);
		  }
		  if(olbLocality.getSelectedIndex() != 0){
				 temp=new Filter();
				 temp.setStringValue(olbLocality.getValue(olbLocality.getSelectedIndex()));
				 temp.setQuerryString("contacts.address.locality");
					filtervec.add(temp);
			  }
		  	if(lbFnfStatus.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(lbFnfStatus.getValue(lbFnfStatus.getSelectedIndex()));
			temp.setQuerryString("fnfStatus");
			filtervec.add(temp);
		}
		  
		  	if(dateComparator.getValue()!=null){
				filtervec.addAll(dateComparator.getValue());
			}
			
		  	
		  	
		  	
		  	
		  	
		  
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Employee());
		return querry;
	}
	
	
	@Override
	public boolean validate() {
		if(LoginPresenter.branchRestrictionFlag)
		{
		if(olbBranch.getSelectedIndex()==0)
		{
			showDialogMessage("Select Branch");
			return false;
		}
		}

		//Ashwini Patil Date:27-09-2023 
		boolean filterAppliedFlag=false;
//		!personInfo.getId().getValue().equals("")
		if(!personInfo.getId().getValue().equals("")
				||olbCountry.getSelectedIndex()!=0||olbBranch.getSelectedIndex()!=0||olbDepartMent.getSelectedIndex()!=0||olbEmploymentType.getSelectedIndex()!=0||olbDesignation.getSelectedIndex()!=0
				||olbRole.getSelectedIndex()!=0||olbReportsTo.getSelectedIndex()!=0||(idCardNumber.getValue()!=null&&!idCardNumber.getValue().equals(""))||
				(accessCardNumber.getValue()!=null&&!accessCardNumber.getValue().equals(""))||lbStatus.getSelectedIndex()!=0
				||olbProjectName.getSelectedIndex()!=0||olbCity.getSelectedIndex()!=0||olbLocality.getSelectedIndex()!=0||lbFnfStatus.getSelectedIndex()!=0
				||(dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)
				){
			filterAppliedFlag=true;
		}
		
		if(!filterAppliedFlag){
			showDialogMessage("Please apply at least one filter!");
			return false;
		}
				
		
		return super.validate();
	}
}
