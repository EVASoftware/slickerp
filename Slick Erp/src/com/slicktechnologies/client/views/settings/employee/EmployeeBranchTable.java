package com.slicktechnologies.client.views.settings.employee;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.SalesLineItem;

public class EmployeeBranchTable extends SuperTable<EmployeeBranch>{
	
	Column<EmployeeBranch, String> deleteColumn;
	TextColumn<EmployeeBranch> srNo, branchName;
	
	
	@Override
	public void createTable() {
		
//		createColumnlnSrNoColumn();
		createColumnlnBranchNameColumn();
		createColumndeleteColumn();
		addFieldUpdater();
//		table.addColumn(srNo, "SrNo");
		
		
	}
	
	
	private void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<EmployeeBranch, String>(btnCell) {
			@Override
			public String getValue(EmployeeBranch object) {
				return "Delete";
			}
		};
		
		table.addColumn(deleteColumn, "Remove");		
	}


	private void createColumnlnBranchNameColumn() {
		branchName = new TextColumn<EmployeeBranch>() {
			@Override
			public String getValue(EmployeeBranch object) {
				return object.getBranchName();
			}
		};	
		
		table.addColumn(branchName, "Branch");
	}


//	private void createColumnlnSrNoColumn() {
//		
//		srNo = new TextColumn<EmployeeBranch>() {
//			@Override
//			public String getValue(EmployeeBranch object) {
//				return object.getSrNo()+"";
//			}
//		};
//		
//	}
	@Override
	protected void initializekeyprovider() {
		
	}
	@Override
	public void addFieldUpdater() {
		System.out.println("in side field updator");
		createFieldUpdaterdeleteColumn();
	}
	
	protected void createFieldUpdaterdeleteColumn()
	{
		System.out.println("in side field updator method");
		deleteColumn.setFieldUpdater(new FieldUpdater<EmployeeBranch,String>()
				{
			@Override
			public void update(int index,EmployeeBranch object,String value)
			{
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}

	@Override
	public void setEnable(boolean state) {

		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);

		if (state) {
			createColumnlnBranchNameColumn();
			createColumndeleteColumn();
			addFieldUpdater();
		} else {
			createColumnlnBranchNameColumn();
		}

	}
	@Override
	public void applyStyle() {
		
	}
}
 