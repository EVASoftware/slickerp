package com.slicktechnologies.client.views.settings.employee;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.humanresource.allocation.ctcallocation.CtcAllocationService;
import com.slicktechnologies.client.views.humanresource.allocation.ctcallocation.CtcAllocationServiceAsync;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.client.views.popups.GeneralComposite;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;

public class EmployeeCtcAllocationPopup extends PopupScreen {

	GeneralComposite generalComposite;
	DateBox dbApplicableFormDate;
	DateBox dbApplicableToDate;
	DateBox dbEffectiveFromDate;
	InlineLabel lblNew = new InlineLabel("Create Ctc Template");
	
	int employeeId=0;
	boolean ctcAllocationStatus=false;
	
	CtcAllocationServiceAsync ctcAllocationService = GWT.create(CtcAllocationService.class);
	
	public EmployeeCtcAllocationPopup() {
		// TODO Auto-generated constructor stub
		super();
		
		createGui();
	}
	
	
	public void initializeWidget(){
		dbApplicableFormDate=new DateBoxWithYearSelector();
		dbApplicableToDate=new DateBoxWithYearSelector();
		dbEffectiveFromDate=new DateBoxWithYearSelector();
		
		intializeCtcTemplate();
		
//		if(employeeId!=0){
//			checkEmpCtcAllocationStatus();
//		}
		
	}
	
	public void intializeCtcTemplate(){
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CtcAllocation", "LoadHrTemplate")){
			ArrayList<String> categoryList=new ArrayList<String>();
			categoryList.add("HR");
			categoryList.add("Hr");
			categoryList.add("hr");
			categoryList.add("");
			
			filter = new Filter();
			filter.setQuerryString("category IN");
			filter.setList(categoryList);
			filtervec.add(filter);
		}
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CTCTemplate());
		generalComposite = new GeneralComposite("", querry);
	}
	
	
	public void checkEmpCtcAllocationStatus() {
		// TODO Auto-generated method stub
		
		GenricServiceAsync async = GWT.create(GenricService.class);
		
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("empid");
		filter.setIntValue(employeeId);
		filtervec.add(filter);
		
		querry.setQuerryObject(new CTC());
		querry.setFilters(filtervec);
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				if(result!=null&&result.size()!=0){
					ctcAllocationStatus=true;
					showDialogMessage("CTC is already allocated to employee.");
					hidePopUp();
				}else{
					ctcAllocationStatus=false;
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
			}
		});
		
		
		
	}
	CTCTemplate ctcTemplate=null;
	public void allocateCtcToEmployee() {
		// TODO Auto-generated method stub
		System.out.println("CTC COMP VAL : "+generalComposite.name.getValue());
		if(generalComposite.name.getValue()!=null){
			System.out.println("TEMPLATE NM : "+generalComposite.getModel()+" "+generalComposite.getModel().getCompanyId());
			ctcTemplate=(CTCTemplate) generalComposite.getModel();
			System.out.println("TEMPLATE NM : "+ctcTemplate.getCtcTemplateName()+" "+ctcTemplate.getCompanyId());
		}else{
			showDialogMessage("Please select CTC Template.");
			return;
		}
		showWaitSymbol();
		
		
		ArrayList<Filter> filtervec=new ArrayList<Filter>();
		Filter temp=null;
		temp=new Filter();
		temp.setQuerryString("companyId");
		if(UserConfiguration.getCompanyId()!=null)
			temp.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setIntValue(employeeId);
		temp.setQuerryString("empCount");
		filtervec.add(temp);
		
		
		ctcAllocationService.getAllocationStatus(filtervec,new AsyncCallback<ArrayList<AllocationResult>>() {
			@Override
			public void onFailure(Throwable caught) {
				hideWaitSymbol();
			}
			@Override
			public void onSuccess(ArrayList<AllocationResult> result) {
				System.out.println("TEMPLATE NM 11 : "+ctcTemplate.getCtcTemplateName()+" "+ctcTemplate.getCompanyId());
				ctcAllocationService.allocateCtc(result,ctcTemplate,dbApplicableFormDate.getValue(),dbApplicableToDate.getValue(),dbEffectiveFromDate.getValue(),LoginPresenter.loggedInUser,null,null,new AsyncCallback<ArrayList<AllocationResult>>() {
					@Override
					public void onFailure(Throwable caught) {
						hideWaitSymbol();
						showDialogMessage("Failed to execute CTC Allocation Process !");
					}
					@Override
					public void onSuccess(ArrayList<AllocationResult> result) {
						hideWaitSymbol();
						showDialogMessage("CTC Allocation Process Successfully executed !");
						hidePopUp();
					}
				});
				
			}
		});
		
		
		
		
	}

	public void clear(){
		dbApplicableFormDate.setValue(null);
		dbApplicableToDate.setValue(null);
		dbEffectiveFromDate.setValue(null);
//		generalComposite.name.setValue(null);
		generalComposite.name.setText("");
		
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPopup=fbuilder.setlabel("Allocate CTC").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", generalComposite);
		FormField fgeneralComposite= fbuilder.setMandatory(true).setMandatoryMsg("CTC Template is mandatory!").setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("From Date", dbApplicableFormDate);
		FormField fdbApplicableFormDate= fbuilder.setMandatory(true).setMandatoryMsg("From date is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date", dbApplicableToDate);
		FormField fdbApplicableToDate= fbuilder.setMandatory(true).setMandatoryMsg("To date is mandatory!").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Effective From Date", dbEffectiveFromDate);
		FormField fdbEffectiveFromDate= fbuilder.setMandatory(true).setMandatoryMsg("Effective from date is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = {  
				{fgroupingPopup},
				{fgeneralComposite} ,
				{fdbApplicableFormDate ,fdbApplicableToDate, fdbEffectiveFromDate},
			
		};
		this.fields=formfield;
	}


	public GeneralComposite getGeneralComposite() {
		return generalComposite;
	}


	public void setGeneralComposite(GeneralComposite generalComposite) {
		this.generalComposite = generalComposite;
	}


	public DateBox getDbApplicableFormDate() {
		return dbApplicableFormDate;
	}


	public void setDbApplicableFormDate(DateBox dbApplicableFormDate) {
		this.dbApplicableFormDate = dbApplicableFormDate;
	}


	public DateBox getDbApplicableToDate() {
		return dbApplicableToDate;
	}


	public void setDbApplicableToDate(DateBox dbApplicableToDate) {
		this.dbApplicableToDate = dbApplicableToDate;
	}


	public DateBox getDbEffectiveFromDate() {
		return dbEffectiveFromDate;
	}


	public void setDbEffectiveFromDate(DateBox dbEffectiveFromDate) {
		this.dbEffectiveFromDate = dbEffectiveFromDate;
	}


	public InlineLabel getLblNew() {
		return lblNew;
	}


	public void setLblNew(InlineLabel lblNew) {
		this.lblNew = lblNew;
	}


	public int getEmployeeId() {
		return employeeId;
	}


	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	
	

}
