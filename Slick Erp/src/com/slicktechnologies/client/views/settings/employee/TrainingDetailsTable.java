package com.slicktechnologies.client.views.settings.employee;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.personlayer.TrainingDetails;

public class TrainingDetailsTable extends SuperTable<TrainingDetails> {
	
	TextColumn<TrainingDetails> viewTrainingNameCol,viewTrainingDateCol,viewTrainingDescription;
	Column<TrainingDetails,String> getDeleteCol,getTrainingNameCol,getTrainingDescription,getTrainingNameColTextbox;
	Column<TrainingDetails,Date> getTrainingDateCol;
	
	ArrayList<String> trainingList=new ArrayList<String>();
	
	@Override
	public void createTable() {
//		getTrainingDetails();
//		getTrainingNameCol();
		getTrainingNameColTextbox();
		getTrainingDescriptionCol();
		getTrainingDateCol();
		getDeleteCol();
		
//		viewTrainingNameCol();
//		viewTrainingDateCol();
	}

	private void getTrainingDetails() {
		trainingList=new ArrayList<String>();
		trainingList.add("--SELECT--");
		
		for(Config config:LoginPresenter.globalConfig){
			if(config.isStatus()==true&&config.getType()==112){
				trainingList.add(config.getName());
			}
		}
		
	}

	private void getTrainingNameCol() {
		SelectionCell cell=new SelectionCell(trainingList);
		getTrainingNameCol = new Column<TrainingDetails, String>(cell) {
			@Override
			public String getValue(TrainingDetails object) {
				if(object.getTrainingName()!=null){
					return object.getTrainingName();
				}else{
					return "";
				}
			}
		};
		table.addColumn(getTrainingNameCol,"Training Type");
		
		getTrainingNameCol.setFieldUpdater(new FieldUpdater<TrainingDetails, String>() {
			@Override
			public void update(int index, TrainingDetails object,String value) {
				// TODO Auto-generated method stub
				if(value!=null&&!value.equals("--SELECT--")){
					object.setTrainingName(value);
					table.redraw();
				}else {
					object.setTrainingName("");
					table.redraw();
				}
			}
		});
	}

	
	private void getTrainingNameColTextbox() {
		EditTextCell cell=new EditTextCell();
		getTrainingNameColTextbox = new Column<TrainingDetails, String>(cell) {
			@Override
			public String getValue(TrainingDetails object) {
				if(object.getTrainingName()!=null){
					return object.getTrainingName();
				}else{
					return "";
				}
			}
		};
		table.addColumn(getTrainingNameColTextbox,"Training Type");
		
		getTrainingNameColTextbox.setFieldUpdater(new FieldUpdater<TrainingDetails, String>() {
			@Override
			public void update(int index, TrainingDetails object,String value) {
				// TODO Auto-generated method stub
				if(value!=null){
					object.setTrainingName(value);
					table.redraw();
				}
			}
		});
	}

	
	private void getTrainingDescriptionCol() {

		EditTextCell editCell=new EditTextCell();
		getTrainingDescription=new Column<TrainingDetails,String>(editCell)
		{
			@Override
			public String getValue(TrainingDetails object)
			{
				if(object.getTrainingDescription()!=null){
					return object.getTrainingDescription();
				}else{
					return "";
				}
			}
		};
		table.addColumn(getTrainingDescription,"Training Description");
		
		getTrainingDescription.setFieldUpdater(new FieldUpdater<TrainingDetails, String>() {
			@Override
			public void update(int index, TrainingDetails object,String value) {
				// TODO Auto-generated method stub
				if(value!=null){
					object.setTrainingDescription(value);
					table.redraw();
				}
			}
		});
	}
	private void getTrainingDateCol() {
		// TODO Auto-generated method stub
//		DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MMM");
//		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		getTrainingDateCol = new Column<TrainingDetails, Date>(date) {
			@Override
			public Date getValue(TrainingDetails object) {
				if(object.getTrainingDate()!=null){
					return object.getTrainingDate();
				}else{
					object.setTrainingDate(new Date());
					return object.getTrainingDate();
				}
			}
		};
		table.addColumn(getTrainingDateCol,"Training Date");
		
		getTrainingDateCol.setFieldUpdater(new FieldUpdater<TrainingDetails, Date>() {
			@Override
			public void update(int index, TrainingDetails object,Date value) {
				// TODO Auto-generated method stub
				if(value!=null){
					object.setTrainingDate(value);
					table.redraw();
				}
			}
		});
	}

	private void getDeleteCol() {
		// TODO Auto-generated method stub
		ButtonCell btnCell=new ButtonCell();
		getDeleteCol=new Column<TrainingDetails, String>(btnCell) {
			@Override
			public String getValue(TrainingDetails object) {
				return "Delete";
			}
		};
		table.addColumn(getDeleteCol,"");
		
		getDeleteCol.setFieldUpdater(new FieldUpdater<TrainingDetails, String>() {
			@Override
			public void update(int index, TrainingDetails object, String value) {
				// TODO Auto-generated method stub
				getDataprovider().getList().remove(index);
				table.redraw();
			}
		});
	}

	private void viewTrainingNameCol() {
		// TODO Auto-generated method stub
		viewTrainingNameCol = new TextColumn<TrainingDetails>() {
			@Override
			public String getValue(TrainingDetails object) {
				if(object.getTrainingName()!=null){
					return object.getTrainingName();
				}else{
					return "";
				}
			}
		};
		table.addColumn(viewTrainingNameCol, "Training Type");
	}
	
	
	private void viewTrainingDescriptionCol() {
		// TODO Auto-generated method stub
		viewTrainingDescription = new TextColumn<TrainingDetails>() {
			@Override
			public String getValue(TrainingDetails object) {
				if(object.getTrainingDescription()!=null){
					return object.getTrainingDescription();
				}else{
					return "";
				}
			}
		};
		table.addColumn(viewTrainingDescription, "Training Description");
	}
	

	private void viewTrainingDateCol() {
		viewTrainingDateCol = new TextColumn<TrainingDetails>() {
			@Override
			public String getValue(TrainingDetails object) {
				// TODO Auto-generated method stub
				String date;
				if(object.getTrainingDate()!=null)
					date=AppUtility.parseDate(object.getTrainingDate());
				else
					date="";
				
				return date;
			}
		};
		table.addColumn(viewTrainingDateCol, "Training Date");
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true) {
//			getTrainingDetails();
//			getTrainingNameCol();
			getTrainingNameColTextbox();
			getTrainingDescriptionCol();
			getTrainingDateCol();
			getDeleteCol();
		}else {
			viewTrainingNameCol();
			viewTrainingDescriptionCol();
			viewTrainingDateCol();
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
