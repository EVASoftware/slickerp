package com.slicktechnologies.client.views.settings.employee;

import java.util.Date;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeFamilyDeatails;

public class NominationDetailTable extends SuperTable<EmployeeFamilyDeatails> {

	TextColumn<EmployeeFamilyDeatails> getColumnNominatedFor;
	Column<EmployeeFamilyDeatails,String> getColumnEmpRel;
	Column<EmployeeFamilyDeatails,String> getColumnEmpfname;
	Column<EmployeeFamilyDeatails,String> getColumnEmpDob;
	TextColumn<EmployeeFamilyDeatails> getColumnEmpAge;
	Column<EmployeeFamilyDeatails,String> getColumnNomineeShare;
	TextColumn<EmployeeFamilyDeatails> getColumnFamily;
	Column<EmployeeFamilyDeatails,String> getColumnAddress;
	
	Column<EmployeeFamilyDeatails,String> getColumnGuardianName;
	Column<EmployeeFamilyDeatails,String> getColumnGuardianAddress;
	Column<EmployeeFamilyDeatails,String> deleteColumn;

	@Override
	public void createTable() {
		getColumnNominatedFor();
		getColumnEmplRel();
		getColumnEmplfname();
		getColumnEmpDob();
		getColumnEmpAge();
		getColumnNomineeShare();
		getColumnFamily();
		getColumnAddress();
		getColumnGuardianName();
		getColumnGuardianAddress();
		
		createColumndeleteColumn();
		addFieldUpdater();

	}

	private void getColumnNominatedFor() {
		// TODO Auto-generated method stub
		getColumnNominatedFor = new TextColumn<EmployeeFamilyDeatails>() {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getNominatedFor() != null)
					return object.getNominatedFor();
				else
					return "";
			}
		};
		table.addColumn(getColumnNominatedFor, "Nominated For");
		getColumnNominatedFor.setSortable(true);
		table.setColumnWidth(getColumnNominatedFor, 90,Unit.PX);
	}

	private void getColumnAddress() {
		EditTextCell cell=new EditTextCell();
		getColumnAddress = new Column<EmployeeFamilyDeatails,String>(cell) {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getAddress() != null)
					return object.getAddress();
				else
					return "";
			}
		};
		table.addColumn(getColumnAddress, "#Address");
		getColumnAddress.setSortable(true);
		table.setColumnWidth(getColumnAddress, 120,Unit.PX);
		
		getColumnAddress.setFieldUpdater(new FieldUpdater<EmployeeFamilyDeatails, String>() {
			@Override
			public void update(int index, EmployeeFamilyDeatails object,String value) {
				// TODO Auto-generated method stub
				object.setAddress(value);
				table.redraw();
			}
		});

	}

	private void getColumnGuardianName() {
		EditTextCell cell=new EditTextCell();
		getColumnGuardianName = new Column<EmployeeFamilyDeatails,String>(cell) {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getGuardianName() != null)
					return object.getGuardianName();
				else
					return "";
			}
			
			@Override
			public void render(Context context, EmployeeFamilyDeatails object,SafeHtmlBuilder sb) {
				if(object.getDob()!=null&&getAgeAsOnDate(object.getDob())<18){
					super.render(context, object, sb);
				}
			}
		};
		table.addColumn(getColumnGuardianName, "#Guardian Name");
		getColumnGuardianName.setSortable(true);
		table.setColumnWidth(getColumnGuardianName, 100,Unit.PX);
		
		
		
		getColumnGuardianName.setFieldUpdater(new FieldUpdater<EmployeeFamilyDeatails, String>() {
			@Override
			public void update(int index, EmployeeFamilyDeatails object,String value) {
				// TODO Auto-generated method stub
				object.setGuardianName(value);
				table.redraw();
			}
		});
	}

	private void getColumnGuardianAddress() {
		EditTextCell cell=new EditTextCell();
		getColumnGuardianAddress = new Column<EmployeeFamilyDeatails,String>(cell) {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getGuardianAddress() != null)
					return object.getGuardianAddress();
				else
					return "";
			}
			@Override
			public void render(Context context, EmployeeFamilyDeatails object,SafeHtmlBuilder sb) {
				if(object.getDob()!=null&&getAgeAsOnDate(object.getDob())<18){
					super.render(context, object, sb);
				}
			}
		};
		table.addColumn(getColumnGuardianAddress, "#Guardian Address");
		getColumnGuardianAddress.setSortable(true);
		table.setColumnWidth(getColumnGuardianAddress, 120,Unit.PX);
		
		getColumnGuardianAddress.setFieldUpdater(new FieldUpdater<EmployeeFamilyDeatails, String>() {
			@Override
			public void update(int index, EmployeeFamilyDeatails object,String value) {
				// TODO Auto-generated method stub
				object.setGuardianAddress(value);
				table.redraw();
			}
		});
	}

	private void getColumnEmplRel() {
		EditTextCell cell=new EditTextCell();
		getColumnEmpRel = new Column<EmployeeFamilyDeatails,String>(cell) {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getFamilyRelation() != null)
					return object.getFamilyRelation();
				else
					return "";
			}
		};
		table.addColumn(getColumnEmpRel, "#Relation");
		getColumnEmpRel.setSortable(true);
		table.setColumnWidth(getColumnEmpRel, 100,Unit.PX);
		
		getColumnEmpRel.setFieldUpdater(new FieldUpdater<EmployeeFamilyDeatails, String>() {
			@Override
			public void update(int index, EmployeeFamilyDeatails object,String value) {
				// TODO Auto-generated method stub
				object.setFamilyRelation(value);
				table.redraw();
			}
		});
	}

	private void getColumnEmplfname() {
		EditTextCell cell=new EditTextCell();
		getColumnEmpfname = new Column<EmployeeFamilyDeatails,String>(cell) {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
							
				if (object.getFullName() != null)
					return object.getFullName();
				else
					return "";
			}
		};
		table.addColumn(getColumnEmpfname, "#Name");
		getColumnEmpfname.setSortable(true);
		table.setColumnWidth(getColumnEmpfname, 100,Unit.PX);
		
		getColumnEmpfname.setFieldUpdater(new FieldUpdater<EmployeeFamilyDeatails, String>() {
			@Override
			public void update(int index, EmployeeFamilyDeatails object,String value) {
				// TODO Auto-generated method stub
				object.setFullName(value);
				table.redraw();
			}
		});
	}

	private void getColumnFamily() {

		getColumnFamily = new TextColumn<EmployeeFamilyDeatails>() {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.isNonFamilyMember()==false)
					return "Yes";
				else
					return "No";
			}
		};
		table.addColumn(getColumnFamily, "Is Family");
		getColumnFamily.setSortable(true);
		table.setColumnWidth(getColumnFamily, 80,Unit.PX);

	}

	private void getColumnNomineeShare() {
		EditTextCell cell=new EditTextCell();
		getColumnNomineeShare = new Column<EmployeeFamilyDeatails,String>(cell) {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getNomineeShare() != 0)
					return object.getNomineeShare()+"";
				else
					return "";
			}
		};
		table.addColumn(getColumnNomineeShare, "#Share(%)");
		getColumnNomineeShare.setSortable(true);
		table.setColumnWidth(getColumnNomineeShare, 80,Unit.PX);
		
		getColumnNomineeShare.setFieldUpdater(new FieldUpdater<EmployeeFamilyDeatails, String>() {
			@Override
			public void update(int index, EmployeeFamilyDeatails object,String value) {
				// TODO Auto-generated method stub
				double share=0;
				if(!value.equals("")){
					try{
						share=Double.parseDouble(value.trim());
					}catch(NumberFormatException e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Please add numeric value only.");
					}
				}
				object.setNomineeShare(share);
				table.redraw();
			}
		});
	}

	private void getColumnEmpAge() {
		getColumnEmpAge = new TextColumn<EmployeeFamilyDeatails>() {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getDob() != null)
					return getAgeAsOnDate(object.getDob())+"";
				else
					return "";
			}
		};
		table.addColumn(getColumnEmpAge, "Age");
		getColumnEmpAge.setSortable(true);
		table.setColumnWidth(getColumnEmpAge, 80,Unit.PX);
	}

	private void getColumnEmpDob() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
		EditTextCell cell=new EditTextCell();
		getColumnEmpDob = new Column<EmployeeFamilyDeatails,String>(cell) {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				if (object.getDob() != null)
					return fmt.format(object.getDob()) + "";
				else
					return "";
			}
		};
		table.addColumn(getColumnEmpDob, "#DOB(dd- MM-yyyy)");
		getColumnEmpDob.setSortable(true);
		table.setColumnWidth(getColumnEmpDob, 120,Unit.PX);
		
		getColumnEmpDob.setFieldUpdater(new FieldUpdater<EmployeeFamilyDeatails, String>() {
			@Override
			public void update(int index, EmployeeFamilyDeatails object,String value) {
				// TODO Auto-generated method stub
				Date date=null;
				try{
					date=fmt.parse(value);
				}catch(Exception e){
					GWTCAlert alert=new GWTCAlert();
					alert.alert("Please add date in given format(dd-MM-yyyy)only");
				}
				object.setDob(date);
				table.redraw();
				
			}
		});
	}

	protected void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<EmployeeFamilyDeatails, String>(btnCell) {
			@Override
			public String getValue(EmployeeFamilyDeatails object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn, 90,Unit.PX);
	}

	protected void createFieldUpdaterdeleteColumn() {
		deleteColumn
				.setFieldUpdater(new FieldUpdater<EmployeeFamilyDeatails, String>() {
					@Override
					public void update(int index,
							EmployeeFamilyDeatails object, String value) {
						getDataprovider().getList().remove(object);
						table.redrawRow(index);
					}
				});
	}

	@Override
	protected void initializekeyprovider() {

	}

	@Override
	public void addFieldUpdater() {
		if (deleteColumn != null)
			createFieldUpdaterdeleteColumn();
	}

	@Override
	public void setEnable(boolean state) {
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);

		if (state == true) {
			getColumnEmplRel();
			getColumnEmplfname();
			getColumnFamily();
			getColumnNomineeShare();
			getColumnEmpAge();
			getColumnEmpDob();
			getColumnGuardianName();
			getColumnGuardianAddress();
			getColumnAddress();
			createColumndeleteColumn();

			addFieldUpdater();
		}

		else {
			getColumnEmplRel();
			getColumnEmplfname();
			getColumnFamily();
			getColumnNomineeShare();
			getColumnEmpAge();
			getColumnEmpDob();
			getColumnGuardianName();
			getColumnGuardianAddress();
			getColumnAddress();

		}
	}

	@Override
	public void applyStyle() {

	}
	
	public int getAgeAsOnDate(Date dob){
        Date now = new Date();
        long timeBetween = now.getTime() - dob.getTime();
        double yearsBetween = timeBetween / 3.15576e+10;
        int age = (int) Math.floor(yearsBetween);
        return age;
	}

}
