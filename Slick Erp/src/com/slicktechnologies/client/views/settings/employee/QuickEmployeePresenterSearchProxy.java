package com.slicktechnologies.client.views.settings.employee;

import java.util.Vector;

import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.client.views.settings.employee.EmployeePresenter.EmployeePresenterSearch;
import com.slicktechnologies.client.views.settings.employee.QuickEmployeePresenter.QuickEmployeePresenterSearch;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class QuickEmployeePresenterSearchProxy extends QuickEmployeePresenterSearch {

	public EmployeeInfoComposite personInfo;
//	TextBox lbEmployeeName;
	ObjectListBox<Branch>lblBranch;
	ObjectListBox<Config> olbRole;
	
	QuickEmployeeForm empForm;
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("lblBranch"))
			return this.lblBranch;
//		if(varName.equals("lbEmployeeName"))
//			return this.lbEmployeeName;
		if(varName.equals("olbRole"))
			return this.olbRole;
		if(varName.equals("personInfo"))
			return this.personInfo;
		
		return null ;
	}
	
	
	public QuickEmployeePresenterSearchProxy()
	{
		super();
		createGui();
	}
	
	public void initWidget()
	{
		
//		lbEmployeeName=new TextBox();
		
		personInfo=AppUtility.InitialiseEmployeeInfoComposite(new EmployeeInfo(),false);
		personInfo.getEmployeeId1().getHeaderLabel().setText("Employee ID");
		personInfo.getEmployeeName1().getHeaderLabel().setText("Employee Name");
		personInfo.getEmployeeCell().getHeaderLabel().setText("Employee Cell");
		
		lblBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(lblBranch);
		
		
		olbRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbRole,Screen.EMPLOYEEROLE);// here to check screen
		
		
	}
	
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
//		builder = new FormFieldBuilder("Employee Name",lbEmployeeName);
//		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(0).build();
		
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(4).build();
		
		builder = new FormFieldBuilder("Branch",lblBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Employee Role",olbRole);
		FormField folbrole= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		
		

		this.fields=new FormField[][]{
				{fpersonInfo},
				{folbBranch,folbrole},
				};
	}
	
	
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;


//		 if(lbEmployeeName.getValue()!=null && !lbEmployeeName.getValue().equals("")){
//				temp=new Filter();
//				temp.setStringValue(lbEmployeeName.getValue());
//				temp.setQuerryString("employeeName");
//				filtervec.add(temp);
//		 }
		
		if(!personInfo.getId().getValue().equals(""))
		{  
			temp=new Filter();
			temp.setIntValue(Integer.parseInt(personInfo.getId().getValue()));
			temp.setQuerryString("count");
			filtervec.add(temp);
		}

		if(!(personInfo.getName().getValue().equals("")))
		{
			temp=new Filter();
			temp.setStringValue(personInfo.getName().getValue());
			temp.setQuerryString("fullname");
			filtervec.add(temp);
		}
		if(!personInfo.getPhone().getValue().equals(""))
		{
			temp=new Filter();
			temp.setLongValue(personInfo.getCellNumber());
			temp.setQuerryString("contacts.cellNo1");
			filtervec.add(temp);
		}
		
		if(olbRole.getSelectedIndex()!=0)
		{
			temp=new Filter();temp.setStringValue(olbRole.getValue().trim());
			temp.setQuerryString("roleName");
			filtervec.add(temp);
		}
		
		if(lblBranch.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(lblBranch.getValue().trim());
			temp.setQuerryString("branchName");
			filtervec.add(temp);
		}
		
		  
		 
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Employee());
		return querry;
	}
	
	
	
	
	
	
	
}
