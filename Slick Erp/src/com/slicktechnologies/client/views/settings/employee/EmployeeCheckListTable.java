package com.slicktechnologies.client.views.settings.employee;

import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;

public class EmployeeCheckListTable extends SuperTable<EmployeeAssetBean>{

	TextColumn<EmployeeAssetBean> assetNameColumn;
	TextColumn<EmployeeAssetBean> assetPriceColumn;	
	Column<EmployeeAssetBean,String> editAssetPriceColumn;	
	Column<EmployeeAssetBean, Boolean> checkColumn;
	Header<Boolean> selectAllHeader;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addCheckBoxCell();
		createAssetNameColumn();
//		createAssetPriceColumn();	
		editAssetPriceColumn();
	}

	private void editAssetPriceColumn() {
		// TODO Auto-generated method stub
		editAssetPriceColumn = new Column<EmployeeAssetBean,String>(new EditTextCell()) {
			@Override
			public String getValue(EmployeeAssetBean object) {			
				return object.getPrice()+"";
			}
		};
		table.addColumn(editAssetPriceColumn, "#Price");
		editAssetPriceColumn.setFieldUpdater(new FieldUpdater<EmployeeAssetBean, String>() {

			@Override
			public void update(int index, EmployeeAssetBean object, String value) {
				// TODO Auto-generated method stub
				try {
					double price=Double.parseDouble(value.trim());
					object.setPrice(price);
				}catch(Exception e){
					GWTCAlert alert=new GWTCAlert();
					alert.alert("Please enter numeric value.");
					object.setPrice(object.getPrice());
				}
				table.redrawRow(index);
			}
		} );
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	
	private void addCheckBoxCell() {
		
		checkColumn=new Column<EmployeeAssetBean, Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue(EmployeeAssetBean object) {
				return object.isRecordSelect();
			}
		};
		
		checkColumn.setFieldUpdater(new FieldUpdater<EmployeeAssetBean, Boolean>() {
			@Override
			public void update(int index, EmployeeAssetBean object, Boolean value) {
				System.out.println("Check Column ....");
				object.setRecordSelect(value);
				table.redrawRow(index);
				selectAllHeader.getValue();
				table.redrawHeaders();
			
			}
		});
		
		
		selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue() {
				if(getDataprovider().getList().size()!=0){
					
				}
				return false;
			}
		};
		
		
		selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
			@Override
			public void update(Boolean value) {
				List<EmployeeAssetBean> list=getDataprovider().getList();
				for(EmployeeAssetBean object:list){
					object.setRecordSelect(value);
				}
				getDataprovider().setList(list);
				table.redraw();
				addColumnSorting();
			}
		});
		table.addColumn(checkColumn,selectAllHeader);

	
}
	protected void createAssetNameColumn()
	{
	
		assetNameColumn=new TextColumn<EmployeeAssetBean>()
				{
			@Override
			public String getValue(EmployeeAssetBean object)
			{
				return object.getAssetName();
			}
				};
				table.addColumn(assetNameColumn,"Name");
				//table.setColumnWidth(assetNameColumn, 100,Unit.PX);
	}
	protected void createAssetPriceColumn(){
		assetPriceColumn = new TextColumn<EmployeeAssetBean>() {
			@Override
			public String getValue(EmployeeAssetBean object) {			
				return (object.getPrice() * object.getQty())+"";
			}
		};
		table.addColumn(assetPriceColumn, "Price");
		//table.setColumnWidth(assetPriceColumn, 100, Unit.PX);
	}
}
