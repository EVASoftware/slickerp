package com.slicktechnologies.client.views.settings.employee;
import java.util.Date;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.TextInputCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.helperlayer.EducationalInfo;
import com.slicktechnologies.shared.common.helperlayer.PreviousCompanyHistory;


public class PreviousWorkExperienceTable extends SuperTable<PreviousCompanyHistory>  
{
	Column<PreviousCompanyHistory,String> deleteColumn;
	Column<PreviousCompanyHistory,String> companyColumn,designationColumn,salaryColumn,pfNumber,schemeCirtificateNumber,pensionPaymentOrderNumber;
	Column<PreviousCompanyHistory,String> fromDateColumn1,toDateColumn1;
//	Column<PreviousCompanyHistory,Date>fromDateColumn,toDateColumn;
	TextColumn<PreviousCompanyHistory> viewcompanyColumn,viewDesignationColumn,viewSalaryColumn,
	viewfromDate,viewToDate,viewPfNumber,viewSchemeCirtificateNumber,viewpensionPaymentOrderNumber;

	TextColumn<PreviousCompanyHistory> viewReasonOfLeaving;
	Column<PreviousCompanyHistory,String>reasonOfLeavingColumn;

	public PreviousWorkExperienceTable()
	{
		super();
		table.setWidth("100%");
		table.setHeight("300px");
	}
	public PreviousWorkExperienceTable(UiScreen<PreviousCompanyHistory> view)
	{
		super(view);	
	}

	public void createTable()
	{
		
		createColumnlnCompany();
		createColumnlnDesignation();
		createColumnlnSalary();
		createColumnlnFromDate();
		createColumnlnToDate();
		/**@author Amol Date 7-4-2019 **/
		createColumnPfNumber();
		createSchemeCirtificateNumber();
		createReasonOfLeaving();
		createPensionPaymentOrder();
		
		
		
		
		createColumndeleteColumn();
		createViewCompanyColumn();
		createViewDesignationColumn();
		createViewSalaryColumn();
		createViewfromDate();
		createViewToDate();
		createViewColumnPfNumber();
		createViewColumnCirtificateNumber();
		createViewColumnReasonOfLeaving();
		createViewColumnPensionPaymentOrder();
		table.addColumn(companyColumn,"Company");
    	table.addColumn(designationColumn,"Designation");
    	table.addColumn(salaryColumn,"Salary");
    	/**@author Amol Date 4-7-2019 add new pf ,**/
    	
    	table.addColumn(pfNumber,"PF Number");
    	table.addColumn(schemeCirtificateNumber,"Scheme Certificate No.");
    	table.addColumn(reasonOfLeavingColumn,"Reason Of Leaving");
    	table.addColumn(pensionPaymentOrderNumber,"Pension Payment Order(PPO) No.");
    	
    	
    	
    	
    	
    	
//    	table.addColumn(fromDateColumn,"From");
//    	table.addColumn(toDateColumn,"To");
    	
    	table.addColumn(fromDateColumn1,"From");
    	table.addColumn(toDateColumn1,"To");
    	
    	table.addColumn(deleteColumn,"Remove");
    	table.setLoadingIndicator(null);
    	
    	addFieldUpdater();
    	table.setLoadingIndicator(null);
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);	
		
	}
	
	
	
	
	
	
	
	private void createViewColumnReasonOfLeaving() {
		
		viewReasonOfLeaving=new TextColumn<PreviousCompanyHistory>()
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getReasonOfLeaving();
			}
				};
			
	}
	private void createReasonOfLeaving() {
		TextInputCell editCell=new TextInputCell();
		reasonOfLeavingColumn=new Column<PreviousCompanyHistory,String>(editCell)
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getReasonOfLeaving();
			}
				};
				table.setColumnWidth(reasonOfLeavingColumn, 180,Unit.PX);		
	}
	private void createViewColumnPensionPaymentOrder() {
		
		viewpensionPaymentOrderNumber=new TextColumn<PreviousCompanyHistory>()
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getPensionPaymentOrderNumber();
			}
				};
			
	}
	
	private void createViewColumnCirtificateNumber() {
		
		viewSchemeCirtificateNumber=new TextColumn<PreviousCompanyHistory>()
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getSchemeCirtificateNumber();
			}
				};
			
	}
	private void createViewColumnPfNumber() {
		
		viewPfNumber=new TextColumn<PreviousCompanyHistory>()
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getPfNumber();
			}
				};
			
	}
	protected void createViewCompanyColumn()
	{
		
		viewcompanyColumn=new TextColumn<PreviousCompanyHistory>()
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getCompany();
			}
				};
			
	}
	
	
	protected void createViewDesignationColumn()
	{
		
		viewDesignationColumn=new TextColumn<PreviousCompanyHistory>()
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getDesignation();
			}
				};
				
	}
	
	protected void createViewSalaryColumn()
	{
		
		viewSalaryColumn=new TextColumn<PreviousCompanyHistory>()
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getSalary()+"";
			}
				};
				
	}
	
	protected void createViewfromDate()
	{
		
		viewfromDate=new TextColumn<PreviousCompanyHistory>()
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
//				return AppUtility.parseDate(object.getFromDate());
				return object.getFromDate();
			}
				};
				
	}
	
	
	protected void createViewToDate()
	{
		
		viewToDate=new TextColumn<PreviousCompanyHistory>()
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
//				return AppUtility.parseDate(object.getToDate());
				return object.getToDate();
			}
				};
				
	}
	
	
	
			
	
	
	
	//************************************************************************************************************//	
	
	
	
	protected void createColumnlnCompany()
	{
		TextInputCell editCell=new TextInputCell();
		companyColumn=new Column<PreviousCompanyHistory,String>(editCell)
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getCompany();
			}
				};
				
	}
	
	protected void createColumnlnDesignation()
	{
		TextInputCell editCell=new TextInputCell();
		designationColumn=new Column<PreviousCompanyHistory,String>(editCell)
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getDesignation();
			}
				};
				
	}
	
	
	protected void createColumnlnSalary()
	{
		TextInputCell editCell=new TextInputCell();
		salaryColumn=new Column<PreviousCompanyHistory,String>(editCell)
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getSalary()+"";
			}
				};
				
	}
	
	protected void createColumnlnFromDate()
	{
//		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
//		DatePickerCell date= new DatePickerCell(fmt);
//		
//		fromDateColumn=new Column<PreviousCompanyHistory,Date>(date)
//				{
//			@Override
//			public Date getValue(PreviousCompanyHistory object)
//			{
//				return object.getFromDate();
//			}
//				};
		
		TextInputCell editCell=new TextInputCell();
		fromDateColumn1=new Column<PreviousCompanyHistory,String>(editCell)
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getFromDate()+"";
			}
				};
				
	}
	
	
	protected void createColumnlnToDate()
	{
//		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
//		DatePickerCell date= new DatePickerCell(fmt);
//		
//		toDateColumn=new Column<PreviousCompanyHistory,Date>(date)
//				{
//			@Override
//			public Date getValue(PreviousCompanyHistory object)
//			{
//				return object.getToDate();
//			}
//				};
		TextInputCell editCell=new TextInputCell();
		toDateColumn1=new Column<PreviousCompanyHistory,String>(editCell)
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getToDate()+"";
			}
				};
				
	}
	
	
	
	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<PreviousCompanyHistory,String>(btnCell)
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return  "Delete" ;
			}
				};
				
	}
	
	

	
	protected void createSchemeCirtificateNumber() {
		TextInputCell editCell=new TextInputCell();
		schemeCirtificateNumber=new Column<PreviousCompanyHistory,String>(editCell)
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getSchemeCirtificateNumber();
			}
				};
				
	}
	
	protected void createPensionPaymentOrder() {
		TextInputCell editCell=new TextInputCell();
		pensionPaymentOrderNumber=new Column<PreviousCompanyHistory,String>(editCell)
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getPensionPaymentOrderNumber();
			}
				};
				
	}
	
	
	protected void createColumnPfNumber() {
		TextInputCell editCell=new TextInputCell();
		pfNumber=new Column<PreviousCompanyHistory,String>(editCell)
				{
			@Override
			public String getValue(PreviousCompanyHistory object)
			{
				return object.getPfNumber();
			}
				};
				
	}
	
	
	

//************************************************************************************************************//

	@Override 
	public void addFieldUpdater() 
	{
		createFieldUpdaterCompany();
		createFieldUpdaterDesignation();
		createFieldUpdaterSalary();
		createFieldUpdaterPfNumber();
		createFieldUpdaterReasonOfLeaving();
		createFieldUpdaterPensionPaymentNumber();
		createFieldUpdaterSchemeCirtificateNumber();
		createFieldUpdaterdeleteColumn();
		createFieldUpdaterFromDate();
		createFieldUpdaterToDate();
	}

	private void createFieldUpdaterReasonOfLeaving() {
		reasonOfLeavingColumn.setFieldUpdater(new FieldUpdater<PreviousCompanyHistory,String>()
				{
			@Override
			public void update(int index,PreviousCompanyHistory object,String value)
			{
				if(value!=null)
				{
					if(value.trim().equals("")==false)
						object.setReasonOfLeaving(value);
				}
				table.setColumnWidth(reasonOfLeavingColumn, 180,Unit.PX);
				table.redrawRow(index);
			}
				});
	}
	
	
	private void createFieldUpdaterSchemeCirtificateNumber() {
		schemeCirtificateNumber.setFieldUpdater(new FieldUpdater<PreviousCompanyHistory,String>()
				{
			@Override
			public void update(int index,PreviousCompanyHistory object,String value)
			{
				if(value!=null)
				{
					if(value.trim().equals("")==false)
						object.setSchemeCirtificateNumber(value);
				}

				table.redrawRow(index);
			}
				});
	}
	
	
	private void createFieldUpdaterPensionPaymentNumber() {
		pensionPaymentOrderNumber.setFieldUpdater(new FieldUpdater<PreviousCompanyHistory,String>()
				{
			@Override
			public void update(int index,PreviousCompanyHistory object,String value)
			{
				if(value!=null)
				{
					if(value.trim().equals("")==false)
						object.setPensionPaymentOrderNumber(value);
				}

				table.redrawRow(index);
			}
				});
	}
	
	
	private void createFieldUpdaterPfNumber() {
		pfNumber.setFieldUpdater(new FieldUpdater<PreviousCompanyHistory,String>()
				{
			@Override
			public void update(int index,PreviousCompanyHistory object,String value)
			{
				if(value!=null)
				{
					if(value.trim().equals("")==false)
						object.setPfNumber(value);
				}

				table.redrawRow(index);
			}
				});
	}
	
	protected void createFieldUpdaterCompany()
	{
		companyColumn.setFieldUpdater(new FieldUpdater<PreviousCompanyHistory,String>()
				{
			@Override
			public void update(int index,PreviousCompanyHistory object,String value)
			{
				if(value!=null)
				{
					if(value.trim().equals("")==false)
						object.setCompany(value);
				}

				table.redrawRow(index);
			}
				});
	}
	
	
	protected void createFieldUpdaterDesignation()
	{
		designationColumn.setFieldUpdater(new FieldUpdater<PreviousCompanyHistory,String>()
				{
			@Override
			public void update(int index,PreviousCompanyHistory object,String value)
			{
				if(value!=null)
				{
					if(value.trim().equals("")==false)
						object.setDesignation(value);
				}

				table.redrawRow(index);
			}
				});
	}
	
	
	protected void createFieldUpdaterSalary()
	{
		salaryColumn.setFieldUpdater(new FieldUpdater<PreviousCompanyHistory,String>()
				{
			@Override
			public void update(int index,PreviousCompanyHistory object,String value)
			{
				if(value!=null)
				{
					if(value.trim().equals("")==false)
					{
						try{
							Double salary=Double.parseDouble(value.trim());
							object.setSalary(salary);
							
						}
						catch(Exception e)
						{
							
						}
					}
				}

				table.redrawRow(index);
			}
				});
	}
	
	protected void createFieldUpdaterFromDate()
	{
//		fromDateColumn.setFieldUpdater(new FieldUpdater<PreviousCompanyHistory,Date>()
//				{
//			@Override
//			public void update(int index,PreviousCompanyHistory object,Date date)
//			{
//				if(date!=null)
//					object.setFromDate(date);
//
//				table.redrawRow(index);
//			}
//			});
		
		fromDateColumn1.setFieldUpdater(new FieldUpdater<PreviousCompanyHistory,String>()
				{
			@Override
			public void update(int index,PreviousCompanyHistory object,String date)
			{
				if(date!=null)
					object.setFromDate(date);

				table.redrawRow(index);
			}
			});
	}
	
	
	protected void createFieldUpdaterToDate()
	{
//		toDateColumn.setFieldUpdater(new FieldUpdater<PreviousCompanyHistory,Date>()
//				{
//			@Override
//			public void update(int index,PreviousCompanyHistory object,Date date)
//			{
//				if(date!=null)
//					object.setToDate(date);
//
//				table.redrawRow(index);
//			}
//			});
		
		toDateColumn1.setFieldUpdater(new FieldUpdater<PreviousCompanyHistory,String>()
				{
			@Override
			public void update(int index,PreviousCompanyHistory object,String date)
			{
				if(date!=null)
					object.setToDate(date);

				table.redrawRow(index);
			}
			});
	}
	
	


	


	
	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<PreviousCompanyHistory,String>()
				{
			@Override
			public void update(int index,PreviousCompanyHistory object,String value)
			{
				getDataprovider().getList().remove(object);

				table.redrawRow(index);
			}
				});
	}


	public void addColumnSorting(){

		

	}

	
	@Override
	public void setEnable(boolean state)
	{
          
		
        for(int i=table.getColumnCount()-1;i>-1;i--)
    	  table.removeColumn(i); 
        	  
        System.out.println("Called Set Enabeled With State"+state);
          if(state==true)
          {
        	table.addColumn(companyColumn,"Company");
        	table.addColumn(designationColumn,"Designation");
        	table.addColumn(salaryColumn,"Salary");
        	table.addColumn(pfNumber,"PF Number");
        	table.addColumn(schemeCirtificateNumber,"Scheme Certificate No.");
        	table.addColumn(reasonOfLeavingColumn,"Reason Of Leaving");
        	table.addColumn(pensionPaymentOrderNumber,"Pension Payment Order(PPO) No.");
        	
//        	table.addColumn(fromDateColumn,"From");
//        	table.addColumn(toDateColumn,"To");
        	table.addColumn(fromDateColumn1,"From");
        	table.addColumn(toDateColumn1,"To");
        	table.addColumn(deleteColumn,"Remove");
        	addFieldUpdater();
        	
      	}
          
          
          else
          {
        	  
        	table.addColumn(viewcompanyColumn,"Company");
          	table.addColumn(viewDesignationColumn,"Designation");
          	table.addColumn(viewSalaryColumn,"Salary");
          	
          	table.addColumn(viewPfNumber,"PF Number");
        	table.addColumn(viewSchemeCirtificateNumber,"Scheme Certificate No.");
        	table.addColumn(viewReasonOfLeaving,"Reason Of Leaving");
        	table.addColumn(viewpensionPaymentOrderNumber,"Pension Payment Order(PPO) No.");
          	
          	
          	table.addColumn(viewfromDate,"From");
        	table.addColumn(viewToDate,"To");
          	
        	
      	
          }
	}

	@Override
	public void applyStyle()
	{

	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<PreviousCompanyHistory>()
				{
			@Override
			public Object getKey(PreviousCompanyHistory item)
			{
				return new Date().getTime();
			}
				};
	}

}
