package com.slicktechnologies.client.views.settings.employee;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.views.humanresource.ctctemplate.CTCTemplateForm;
import com.slicktechnologies.client.views.humanresource.ctctemplate.CTCTemplatePresenter;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;

public class CtcTemplatePopupScreen extends PopupScreen {
	CTCTemplateForm form;
	CTCTemplatePresenter ctcTempPresenter;
	
	public CtcTemplatePopupScreen() {
		// TODO Auto-generated constructor stub
		super();
		getPopup().getElement().addClassName("setCtcTempPopSize");
		createGui();
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
//		super.
		System.out.println("CTC TEMP POP UP CLICK");
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		form=new CTCTemplateForm();
		ctcTempPresenter=new CTCTemplatePresenter(form, new CTCTemplate());
		form.content.setSize("970px", "470px");
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fbuilder = new FormFieldBuilder("",form.content);
		FormField fpop= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField[][] formfield = {  
				{fpop},
		};
		this.fields=formfield;
	}
	
	
	public  void saveCtcTemplate(){
		if(ctcTempPresenter!=null){
			ctcTempPresenter.reactOnSave();
		}
	}
	
	

}
