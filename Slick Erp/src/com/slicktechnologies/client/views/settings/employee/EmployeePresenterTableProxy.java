package com.slicktechnologies.client.views.settings.employee;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import javax.management.relation.RoleNotFoundException;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.client.views.settings.employee.EmployeePresenter.EmployeePresenterTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;

public class EmployeePresenterTableProxy extends EmployeePresenterTable {
	TextColumn<Employee> getCountColumn;
	TextColumn<Employee> getFullnameColumn;
	TextColumn<Employee> getCellNumber1Column;
	TextColumn<Employee> getBranchNameColumn;
	TextColumn<Employee> getDepartmentColumn;
	TextColumn<Employee> getDesignationColumn;
	TextColumn<Employee> getEmployeeTypeColumn;
	TextColumn<Employee> isStatusColumn;
	TextColumn<Employee> roleColumn;
	TextColumn<Employee> reportstoColumn;
	TextColumn<Employee> idCardColumn;
	TextColumn<Employee> accessCardColumn;
	TextColumn<Employee> bloodGroup;
	
	TextColumn<Employee> country;
	TextColumn<Employee> HusbandName;
	ProcessConfiguration processConfig;
	Employee emp;

	public Object getVarRef(String varName)
	{
		if(varName.equals("isStatusColumn"))
			return this.isStatusColumn;
		if(varName.equals("getBranchNameColumn"))
			return this.getBranchNameColumn;
		if(varName.equals("getDesignationColumn"))
			return this.getDesignationColumn;
		if(varName.equals("getCellNumber1Column"))
			return this.getCellNumber1Column;
		if(varName.equals("getFullnameColumn"))
			return this.getFullnameColumn;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		return null ;
	}
	public EmployeePresenterTableProxy()
	{
		super();
	}
	@Override public void createTable() {
		
		
		addColumngetIdCountry();
		addColumngetCount();
		addColumngetFullname();
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "OnlyForOrion")){
			addColumnHusbandName();
		}
		
		
		addColumngetCellNumber1();
		addRoleColumn();
		addReportstoColumn();
		addColumngetBranchName();
		addColumngetDesignation();
		addColumngetDepartment();
		addColumngetEmploymentType();
		addColumngetAcessCard();
		addColumngetIdCard();
		addColumnBloodGroup();
		addColumnisStatus();
		
		
		
		table.setColumnWidth(getCellNumber1Column,"130px");
		table.setColumnWidth(roleColumn,"130px");
		table.setColumnWidth(getBranchNameColumn,"130px");
		table.setColumnWidth(getDepartmentColumn,"130px");
		table.setColumnWidth(getEmployeeTypeColumn,"130px");
		table.setColumnWidth(isStatusColumn,"130px");
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Employee>()
				{
			@Override
			public Object getKey(Employee item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetFullname();
		addSortinggetCellNumber1();
		addSortinggetrolename();
		addSortinggetReportsTo();
		addSortinggetBranchName();
		addSortinggetDesignation();
		addSortingisStatus();
		addSortingDepartment();
		addSortingEmploymentType();
		addSortingAcessCard();
		
		addSortingIdCard();
		addSortingCountry();
		addSortingBloodGroup();
		addSortingHusbandName();
	}
	
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetCount()
	{
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"Id");
				getCountColumn.setSortable(true);
				table.setColumnWidth(getCountColumn, "100px");
	}
	
	protected void addRoleColumn()
	{
		roleColumn=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				return object.getRoleName();
			}
				};
				table.addColumn(roleColumn,"Role");
				roleColumn.setSortable(true);
	}
	
	protected void addReportstoColumn()
	{
		reportstoColumn=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				if(object.getReportsTo()!=null)
					return object.getReportsTo();
				else
					return "N.A";
			}
				};
				table.addColumn(reportstoColumn,"Reports To");
				table.setColumnWidth(reportstoColumn,"120px");
				reportstoColumn.setSortable(true);
	}
	
	protected void addSortinggetrolename()
	{
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(roleColumn, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getRoleName()!=null && e2.getRoleName()!=null){
						return e1.getRoleName().compareTo(e2.getRoleName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetReportsTo()
	{
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(reportstoColumn, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getReportsTo()!=null && e2.getReportsTo()!=null){
						return e1.getReportsTo().compareTo(e2.getReportsTo());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addSortinggetFullname()
	{
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(getFullnameColumn, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getFullname()!=null && e2.getFullname()!=null){
						return e1.getFullname().compareTo(e2.getFullname());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetFullname()
	{
		getFullnameColumn=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				return object.getFullname()+"";
			}
				};
				table.addColumn(getFullnameColumn,"Name");
				getFullnameColumn.setSortable(true);
				table.setColumnWidth(getFullnameColumn, "120px");
	}
	protected void addSortinggetCellNumber1()
	{
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(getCellNumber1Column, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCellNumber1()== e2.getCellNumber1()){
						return 0;}
					if(e1.getCellNumber1()> e2.getCellNumber1()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCellNumber1()
	{
		getCellNumber1Column=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				return object.getCellNumber1()+"";
			}
				};
				table.addColumn(getCellNumber1Column,"Phone");
				getCellNumber1Column.setSortable(true);
	}
	protected void addSortinggetBranchName()
	{
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(getBranchNameColumn, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranchName()!=null && e2.getBranchName()!=null){
						return e1.getBranchName().compareTo(e2.getBranchName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBranchName()
	{
		getBranchNameColumn=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				return object.getBranchName()+"";
			}
				};
				table.addColumn(getBranchNameColumn,"Branch");
				getBranchNameColumn.setSortable(true);
	}
	protected void addSortinggetDesignation()
	{
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(getDesignationColumn, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDesignation()!=null && e2.getDesignation()!=null){
						return e1.getDesignation().compareTo(e2.getDesignation());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetDesignation()
	{
		getDesignationColumn=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				return object.getDesignation()+"";
			}
				};
				table.addColumn(getDesignationColumn,"Designation");
				table.setColumnWidth(getDesignationColumn,"110px");
				getDesignationColumn.setSortable(true);
	}
	protected void addSortingisStatus()
	{
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(isStatusColumn, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.isStatus()== e2.isStatus()){
						return 0;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumnisStatus()
	{
		isStatusColumn=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				if( object.isStatus()==true)
					return "Active";
				else 
					return "In Active";
			}
				};
				table.addColumn(isStatusColumn,"Status");
				isStatusColumn.setSortable(true);
	}

	protected void addSortingDepartment()
	{
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(getDepartmentColumn, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDepartMent()!=null && e1.getDepartMent()!=null){
						return e1.getDepartMent().compareTo(e2.getDepartMent());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetDepartment()
	{
		getDepartmentColumn=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				return object.getDepartMent()+"";
			}
				};
				table.addColumn(getDepartmentColumn,"Department");
				getDepartmentColumn.setSortable(true);
	}



	protected void addSortingEmploymentType()
	{
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(getEmployeeTypeColumn, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeeType()!=null && e2.getEmployeeType()!=null){
						return e1.getEmployeeType().compareTo(e2.getEmployeeType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmploymentType()
	{
		getEmployeeTypeColumn=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				return object.getEmployeeType()+"";
			}
				};
				table.addColumn(getEmployeeTypeColumn,"Employment Type");
				table.setColumnWidth(getEmployeeTypeColumn,"130px");
				getEmployeeTypeColumn.setSortable(true);
	}
	
	
	
	protected void addSortingAcessCard()
	{
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(accessCardColumn, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getAccessCardNo()!=null && e2.getAccessCardNo()!=null){
						return e1.getAccessCardNo().compareTo(e2.getAccessCardNo());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetAcessCard()
	{
		accessCardColumn=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				return object.getAccessCardNo()+"";
			}
				};
				table.addColumn(accessCardColumn,"Access Card");
				table.setColumnWidth(accessCardColumn,"130px");
				accessCardColumn.setSortable(true);
	}
	
	
	
	
	
	protected void addSortingIdCard()
	{
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(idCardColumn, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getIdCardNo()!=null && e2.getIdCardNo()!=null){
						return e1.getIdCardNo().compareTo(e2.getIdCardNo());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetIdCard()
	{
		idCardColumn=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				return object.getIdCardNo();
			}
				};
				table.addColumn(idCardColumn,"ID Card");
				table.setColumnWidth(idCardColumn,"130px");
				idCardColumn.setSortable(true);
	}
	
	
	protected void addSortingCountry()
	{
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(country, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCountry()!=null && e2.getCountry()!=null){
						return e1.getCountry().compareTo(e2.getCountry());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetIdCountry()
	{
		country=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				return object.getCountry();
			}
				};
				table.addColumn(country,"Country");
				table.setColumnWidth(country,"90px");
				country.setSortable(true);
	}
	
	
	protected void addColumnBloodGroup()
	{
		bloodGroup=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				return object.getBloodGroup();
			}
				};
				table.addColumn(bloodGroup,"Blood Group");
				table.setColumnWidth(bloodGroup,"100px");
				bloodGroup.setSortable(true);
	}
	
	
	
	
	
	
	protected void addSortingBloodGroup()
	{
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(bloodGroup, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBloodGroup()!=null && e2.getBloodGroup()!=null){
						return e1.getBloodGroup().compareTo(e2.getBloodGroup());
						}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnHusbandName() {
		// TODO Auto-generated method stub
		HusbandName=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				return object.getHusbandName();
			}
				};
				table.addColumn(HusbandName,"Father/Husband Name");
				table.setColumnWidth(HusbandName,"100px");
				HusbandName.setSortable(true);
		
            }
	private void addSortingHusbandName() {
		// TODO Auto-generated method stub
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(HusbandName, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getHusbandName()!=null && e2.getHusbandName()!=null){
						return e1.getHusbandName().compareTo(e2.getHusbandName());
						}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
		
		
		
		
		
	}
	
	
	
	
}
