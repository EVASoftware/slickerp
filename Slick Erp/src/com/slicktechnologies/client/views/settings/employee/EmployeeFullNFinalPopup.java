package com.slicktechnologies.client.views.settings.employee;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.views.contract.CancelContractTable;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;

public class EmployeeFullNFinalPopup extends PopupScreen  {

	CheckBox chIsResigned;
	DateBox dtResiggnationDate;
	DateBox dtLastDateAtOrganization;
	DoubleBox dbNoticePeriod;
	DoubleBox dbShortFall;
	DoubleBox dbWaiveOff;
	Button btProcessPayRoll;
	ObjectListBox<CheckListType> olbchecklist;
	EmployeeCheckListTable employeeCheckListTable;
	InlineLabel lblUpdate = new InlineLabel("Submit");
	DateBox dbPayrollMonth;
	
	/**
	 * @author Anil , Date : 17-04-2019
	 * Added print button
	 */
	InlineLabel lblPrint = new InlineLabel("FnF Print");
	
	/**
	 * @author Anil ,Date : 23-04-2019
	 */
	DateBox dbFnFDate;
	
	public EmployeeFullNFinalPopup(){
		super();
		employeeCheckListTable.connectToLocal();
		getPopup().setWidth("800px");
		//getPopup().setHeight("500px");
		employeeCheckListTable.getTable().setHeight("360px");
	//	lblUpdate.addClickHandler(this);	
		createGui();
	}
	
	private void initializeWidget() {
		chIsResigned = new CheckBox();
		dtResiggnationDate = new DateBoxWithYearSelector();
		dtLastDateAtOrganization = new DateBoxWithYearSelector();
		dbNoticePeriod = new DoubleBox();
		dbShortFall = new DoubleBox();
		dbWaiveOff = new DoubleBox();
		btProcessPayRoll = new Button("Process PayRoll");
		chIsResigned.setValue(true);
		employeeCheckListTable = new EmployeeCheckListTable();
		olbchecklist=new ObjectListBox<CheckListType>();
		CheckListType.initializeTypeName(olbchecklist);
//		dbPayrollMonth = new DateBox();
		dbPayrollMonth = new DateBoxWithYearSelector();

//		dbFnFDate=new DateBoxWithYearSelector();
		dbFnFDate = new DateBoxWithYearSelector();

	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
		initializeWidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPopup=fbuilder.setlabel("Employee F&F Popup").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		chIsResigned = new CheckBox();
		dtResiggnationDate = new DateBoxWithYearSelector();
		dtLastDateAtOrganization = new DateBoxWithYearSelector();
		dbNoticePeriod = new DoubleBox();
		dbShortFall = new DoubleBox();
		dbWaiveOff = new DoubleBox();  
		btProcessPayRoll = new Button("Process PayRoll");
		
		fbuilder = new FormFieldBuilder("Is Resigned", chIsResigned);
		FormField fchIsResigned= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Resignation Date", dtResiggnationDate);
		FormField fdtResiggnationDate= fbuilder.setMandatory(true).setMandatoryMsg("Resignation Date is mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("* Last date At Organization", dtLastDateAtOrganization);
		FormField fdtLastDateAtOrganization= fbuilder.setMandatory(true).setMandatoryMsg("Last date At Organization is mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Notice Period", dbNoticePeriod);
		FormField fdbNoticePeriod= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("ShortFall", dbShortFall);
		FormField fdbShortFall= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Waive-Off", dbWaiveOff);
		FormField fdbWaiveOff= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fempty= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", btProcessPayRoll);
		FormField fbtProcessPayRoll= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", employeeCheckListTable.getTable());
		FormField femployeeCheckListTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("CheckList Type", olbchecklist);
		FormField folbchecklist= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("* Month", dbPayrollMonth);
		FormField fdbPayrollMonth= fbuilder.setMandatory(true).setMandatoryMsg("Month is mandatory.").setRowSpan(0).setColSpan(0).build();
		setFormat(null);
		
		fbuilder = new FormFieldBuilder("FnF Date", dbFnFDate);
		FormField fdbFnFDate= fbuilder.setMandatory(true).setMandatoryMsg("FnF date is mandatory.").setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = {  
				{fgroupingPopup},
			//	{fchIsResigned ,fdtResiggnationDate ,fdtLastDateAtOrganization},
				{fdbFnFDate},
				{fdbPayrollMonth ,fdbNoticePeriod , fdbShortFall,fdbWaiveOff},
			//	{fempty ,fbtProcessPayRoll}
				{femployeeCheckListTable},
				//{fbillingAddressComposite}
			
		};
		this.fields=formfield;
	}

	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public CheckBox getChIsResigned() {
		return chIsResigned;
	}

	public void setChIsResigned(CheckBox chIsResigned) {
		this.chIsResigned = chIsResigned;
	}

	public DateBox getDtResiggnationDate() {
		return dtResiggnationDate;
	}

	public void setDtResiggnationDate(DateBox dtResiggnationDate) {
		this.dtResiggnationDate = dtResiggnationDate;
	}

	public DateBox getDtLastDateAtOrganization() {
		return dtLastDateAtOrganization;
	}

	public void setDtLastDateAtOrganization(DateBox dtLastDateAtOrganization) {
		this.dtLastDateAtOrganization = dtLastDateAtOrganization;
	}

	public DoubleBox getDbNoticePeriod() {
		return dbNoticePeriod;
	}

	public void setDbNoticePeriod(DoubleBox dbNoticePeriod) {
		this.dbNoticePeriod = dbNoticePeriod;
	}

	public DoubleBox getDbShortFall() {
		return dbShortFall;
	}

	public void setDbShortFall(DoubleBox dbShortFall) {
		this.dbShortFall = dbShortFall;
	}

	public DoubleBox getDbWaiveOff() {
		return dbWaiveOff;
	}

	public void setDbWaiveOff(DoubleBox dbWaiveOff) {
		this.dbWaiveOff = dbWaiveOff;
	}

	public ObjectListBox<CheckListType> getOlbchecklist() {
		return olbchecklist;
	}

	public void setOlbchecklist(ObjectListBox<CheckListType> olbchecklist) {
		this.olbchecklist = olbchecklist;
	}

	public EmployeeCheckListTable getEmployeeCheckListTable() {
		return employeeCheckListTable;
	}

	public void setEmployeeCheckListTable(
			EmployeeCheckListTable employeeCheckListTable) {
		this.employeeCheckListTable = employeeCheckListTable;
	}

//	@Override
//	public void onChange(ChangeEvent event) {
//		// TODO Auto-generated method stub
//		if(event.getSource().equals(this.))
//	}

	/**
	 * @author Anil
	 * @since 30-04-2020
	 * @param lastDateOfWorking - as per discussion with rahul the fnf month should be as per the last date of working
	 */
	public void setFormat(Date lastDateOfWorking)
	{
		DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MMM");
		DateBox.DefaultFormat defformat=new DateBox.DefaultFormat(format);
		dbPayrollMonth.setFormat(defformat);
//		dbPayrollMonth.setValue(new Date());
		if(lastDateOfWorking!=null) {
			dbPayrollMonth.setValue(lastDateOfWorking);
		}else {
			dbPayrollMonth.setValue(new Date());
		}
	}

	public DateBox getDbFnFDate() {
		return dbFnFDate;
	}

	public void setDbFnFDate(DateBox dbFnFDate) {
		this.dbFnFDate = dbFnFDate;
	}
	
	

}
