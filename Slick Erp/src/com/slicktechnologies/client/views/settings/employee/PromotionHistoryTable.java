package com.slicktechnologies.client.views.settings.employee;

import java.util.ArrayList;
import java.util.Date;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextInputCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.PromotionDetails;

public class PromotionHistoryTable extends SuperTable<PromotionDetails>{

	TextColumn<PromotionDetails> getColPrevDesignation,getColPrevSal,getColCurrDesg,getColCurrSal,getColPromotedBy,getColDateOfPromotion;
	Column<PromotionDetails, String> editableCurrDesg,editableCurrSal,editablePromotedBy,deleteCol;
	Column<PromotionDetails,Date> editableDateOfPromotion;
	
	public PromotionHistoryTable() {
		super();
	}
	
	@Override
	public void createTable() {
		getColPrevDesignation();
		getColPrevSal();
		
		editableCurrDesg();
		editableCurrSal();
		editableDateOfPromotion();
		editablePromotedBy();
		deleteCol();
		
		
//		getColCurrDesg();
//		getColCurrSal();
//		getColDateOfPromotion();
//		getColPromotedBy();
	}

	private void getColCurrDesg() {
		// TODO Auto-generated method stub
		getColCurrDesg=new TextColumn<PromotionDetails>() {
			@Override
			public String getValue(PromotionDetails object) {
				if(object.getCurrentDesignation()!=null){
					return object.getCurrentDesignation();
				}
				return "";
			}
		};
		table.addColumn(getColCurrDesg,"Current Designation");
	}

	private void getColCurrSal() {
		// TODO Auto-generated method stub
		getColCurrSal=new TextColumn<PromotionDetails>() {
			@Override
			public String getValue(PromotionDetails object) {
				if(object.getCurrentSalary()!=0){
					return object.getCurrentSalary()+"";
				}
				return "";
			}
		};
		table.addColumn(getColCurrSal,"Current Salary");
	}

	private void getColDateOfPromotion() {
		// TODO Auto-generated method stub
		getColDateOfPromotion=new TextColumn<PromotionDetails>() {
			@Override
			public String getValue(PromotionDetails object) {
				if(object.getDateOfPromotion()!=null){
					return AppUtility.parseDate(object.getDateOfPromotion());
				}
				return "";
			}
		};
		table.addColumn(getColDateOfPromotion,"Date of Promotion");
	}

	private void getColPromotedBy() {
		getColPromotedBy=new TextColumn<PromotionDetails>() {
			@Override
			public String getValue(PromotionDetails object) {
				if(object.getPromotedBy()!=null){
					return object.getPromotedBy();
				}
				return "";
			}
		};
		table.addColumn(getColPromotedBy,"Promoted By");
	}

	private void getColPrevDesignation() {
		getColPrevDesignation=new TextColumn<PromotionDetails>() {
			@Override
			public String getValue(PromotionDetails object) {
				if(object.getPreviousDesignation()!=null){
					return object.getPreviousDesignation();
				}
				return "";
			}
		};
		table.addColumn(getColPrevDesignation,"Previous Designation");
	}

	private void getColPrevSal() {
		getColPrevSal=new TextColumn<PromotionDetails>() {
			@Override
			public String getValue(PromotionDetails object) {
				if(object.getPreviousSalary()!=0){
					return object.getPreviousSalary()+"";
				}
				return "";
			}
		};
		table.addColumn(getColPrevSal,"Previous Salary");
		
	}

	private void editableCurrDesg() {
		SelectionCell cell=new SelectionCell(getDesignationList());
		editableCurrDesg = new Column<PromotionDetails, String>(cell) {
			@Override
			public String getValue(PromotionDetails object) {
				if(object.getCurrentDesignation()!=null){
					return object.getCurrentDesignation();
				}else{
					return "";
				}
			}
		};
		table.addColumn(editableCurrDesg,"#Current Designation");
		
		editableCurrDesg.setFieldUpdater(new FieldUpdater<PromotionDetails, String>() {
			@Override
			public void update(int index, PromotionDetails object,String value) {
				if(value!=null&&!value.equals("--SELECT--")){
					object.setCurrentDesignation(value);
					table.redraw();
				}
			}
		});
	}

	private void editableCurrSal() {
		TextInputCell editCell = new TextInputCell();
		editableCurrSal = new Column<PromotionDetails, String>(editCell) {
			@Override
			public String getValue(PromotionDetails object) {
				if(object.getCurrentSalary()!=0){
					return object.getCurrentSalary()+"";
				}else{
					return "";
				}
			}
		};
		table.addColumn(editableCurrSal,"#Current Salary");
		
		editableCurrSal.setFieldUpdater(new FieldUpdater<PromotionDetails, String>() {
			@Override
			public void update(int index, PromotionDetails object,String value) {
				// TODO Auto-generated method stub
				if(value!=null){
					try{
						double salary=Double.parseDouble(value.trim());
						object.setCurrentSalary(salary);
					}catch(Exception e){
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Please add only numeric value.");
					}
					
					table.redraw();
				}
			}
		});
	}

	private void editableDateOfPromotion() {
		DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		editableDateOfPromotion = new Column<PromotionDetails, Date>(date) {
			@Override
			public Date getValue(PromotionDetails object) {
				if(object.getDateOfPromotion()!=null){
					return object.getDateOfPromotion();
				}else{
					object.setDateOfPromotion(new Date());
					return object.getDateOfPromotion();
				}
			}
		};
		table.addColumn(editableDateOfPromotion,"#Date of Promotion");
		
		editableDateOfPromotion.setFieldUpdater(new FieldUpdater<PromotionDetails, Date>() {
			@Override
			public void update(int index, PromotionDetails object,Date value) {
				if(value!=null){
					object.setDateOfPromotion(value);
					table.redraw();
				}
			}
		});
	}

	private void editablePromotedBy() {
		// TODO Auto-generated method stub
		SelectionCell cell=new SelectionCell(getPromotedByList());
		editablePromotedBy = new Column<PromotionDetails, String>(cell) {
			@Override
			public String getValue(PromotionDetails object) {
				if(object.getPromotedBy()!=null){
					return object.getPromotedBy();
				}else{
					return "";
				}
			}
		};
		table.addColumn(editablePromotedBy,"#Promoted By");
		
		editablePromotedBy.setFieldUpdater(new FieldUpdater<PromotionDetails, String>() {
			@Override
			public void update(int index, PromotionDetails object,String value) {
				if(value!=null&&!value.equals("--SELECT--")){
					object.setPromotedBy(value);
					table.redraw();
				}
			}
		});
	}

	private void deleteCol() {
		// TODO Auto-generated method stub
		ButtonCell btnCell=new ButtonCell();
		deleteCol=new Column<PromotionDetails, String>(btnCell) {
			@Override
			public String getValue(PromotionDetails object) {
				return "Delete";
			}
		};
		table.addColumn(deleteCol,"");
		
		deleteCol.setFieldUpdater(new FieldUpdater<PromotionDetails, String>() {
			@Override
			public void update(int index, PromotionDetails object, String value) {
				// TODO Auto-generated method stub
				getDataprovider().getList().remove(index);
				table.redraw();
			}
		});
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true) {
			getColPrevDesignation();
			getColPrevSal();
			editableCurrDesg();
			editableCurrSal();
			editableDateOfPromotion();
			editablePromotedBy();
			deleteCol();
		}else {
			getColPrevDesignation();
			getColPrevSal();
			getColCurrDesg();
			getColCurrSal();
			getColDateOfPromotion();
			getColPromotedBy();
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	public ArrayList<String> getDesignationList(){
		ArrayList<String> designationList=new ArrayList<String>();
		designationList.add("--SELECT--");
		for(Config obj:LoginPresenter.globalConfig){
			if(obj.isStatus()==true&&obj.getType()==13){
				designationList.add(obj.getName());
			}
		}
		return designationList;
	}
	
	public ArrayList<String> getPromotedByList(){
		ArrayList<String> promotedByList=new ArrayList<String>();
		promotedByList.add("--SELECT--");
		for(Employee obj:LoginPresenter.globalEmployee){
			if(obj.isStatus()==true){
				promotedByList.add(obj.getFullName());
			}
		}
		return promotedByList;
	}

}
