package com.slicktechnologies.client.views.settings.employee;

import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ListBox;
import com.itextpdf.text.io.GetBufferedRandomAccessSource;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeFamilyDeatails;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class NominationPopup extends PopupScreen {
	ListBox lbNominatedFor;
	ObjectListBox<EmployeeFamilyDeatails> oblFamilyDetails;
	CheckBox cbNonFamilyMember;
	Button btnAdd;
	NominationDetailTable nominationTable;
	
	Employee model;
	GenricServiceAsync async = GWT.create(GenricService.class);
	public NominationPopup() {
		super();
		nominationTable.connectToLocal();
		nominationTable.getTable().setHeight("250px");
		cbNonFamilyMember.setValue(false);
//		getPopup().setWidth("830px !important");
//		getLblOk().addClickHandler(this);
//		getLblCancel().addClickHandler(this);
//		content.getElement().getStyle().setWidth(830, Unit.PX);
		getPopup().getElement().addClassName("setWidth");
		createGui();
	}
	
	private void initializeWidget(){
		lbNominatedFor=new ListBox();
		lbNominatedFor.addItem("--SELECT--");
		lbNominatedFor.addItem("EPF");
		lbNominatedFor.addItem("EPS");
		
		oblFamilyDetails=new ObjectListBox<EmployeeFamilyDeatails>();
		
		nominationTable=new NominationDetailTable();
		btnAdd=new Button("Add");
		btnAdd.addClickHandler(this);
		
		cbNonFamilyMember=new CheckBox();
		cbNonFamilyMember.setValue(false);
		
	}
	
	public void initializeNominationPopup(Employee model,ArrayList<EmployeeFamilyDeatails> empFam){
//	public void initializeNominationPopup(Employee model){
		ArrayList<EmployeeFamilyDeatails> empFamList=new ArrayList<EmployeeFamilyDeatails>();
		if(empFam!=null){
			empFamList.addAll(empFam);
		}
		
		this.model=model;
		nominationTable.connectToLocal();
		if(empFamList!=null&&empFamList.size()!=0){
			nominationTable.setValue(empFamList);
		}
		nominationTable.getTable().redraw();
		final ArrayList<EmployeeFamilyDeatails> empFamDetList=new ArrayList<EmployeeFamilyDeatails>();
		
		MyQuerry query=new MyQuerry();
		Filter temp=null;
		Vector<Filter> vecList=new Vector<Filter>();
		
		temp=new Filter();
		temp.setQuerryString("empInfo.empCount");
		temp.setIntValue(model.getCount());
		vecList.add(temp);
		
		query.setFilters(vecList);
		query.setQuerryObject(new EmployeeAdditionalDetails());
		
		async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("EMP FAM SIZE : "+result);
				for(SuperModel model:result){
					EmployeeAdditionalDetails obj=(EmployeeAdditionalDetails) model;
					if(obj.getEmpFamDetList()!=null&&obj.getEmpFamDetList().size()!=0){
						empFamDetList.addAll(obj.getEmpFamDetList());
					}
				}
				
				oblFamilyDetails.setListItems(empFamDetList);
				
			}
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==btnAdd){
			if(validateMemberAddition()){
				if(oblFamilyDetails.getSelectedIndex()!=0){
					EmployeeFamilyDeatails obj=new EmployeeFamilyDeatails();
					obj=obj.getFamilyDetails(oblFamilyDetails.getSelectedItem());
					for(EmployeeFamilyDeatails fam:nominationTable.getValue()){
						if(fam.getNominatedFor().equals(lbNominatedFor.getValue(lbNominatedFor.getSelectedIndex()))&&fam.toString().equals(oblFamilyDetails.getValue())){
							showDialogMessage("Member already added.");
							return;
						}
					}
					obj.setNominatedFor(lbNominatedFor.getValue(lbNominatedFor.getSelectedIndex()));
					nominationTable.getDataprovider().getList().add(obj);
				}else if(cbNonFamilyMember.getValue().equals(true)){
					EmployeeFamilyDeatails fam=new EmployeeFamilyDeatails();
					fam.setNominatedFor(lbNominatedFor.getValue(lbNominatedFor.getSelectedIndex()));
					fam.setNonFamilyMember(true);
					nominationTable.getDataprovider().getList().add(fam);
				}
			}
		}
		
		if(event.getSource()==getLblOk()){
			System.out.println("Inside on Click...");
			if(validModel()){
				model.setNominationList(nominationTable.getValue());
				async.save(model, new AsyncCallback<ReturnFromServer>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(ReturnFromServer result) {
						// TODO Auto-generated method stub
						showDialogMessage("Nominee details updated successfully.");
						getPopup().hide();
					}
				});
			}
		}
		if(event.getSource()==getLblCancel()){
			getPopup().hide();
		}
	}

	private boolean validModel() {
		// TODO Auto-generated method stub
		String msg="";
		
		for(EmployeeFamilyDeatails obj:nominationTable.getValue()){
			if(obj.getDob()!=null){
				if(getAgeAsOnDate(obj.getDob())<18&&(obj.getGuardianName().equals("")||obj.getGuardianAddress().equals(""))){
					msg=msg+"Please add guardian details for "+obj.toString()+"\n";
				}
			}else{
				msg=msg+"Please add DOB for "+obj.toString()+"\n";
			}
			
		}
		
		for(int i=1;i<lbNominatedFor.getItemCount();i++){
			String nominatedFor=lbNominatedFor.getItemText(i);
			double share=0;
			for(EmployeeFamilyDeatails obj:nominationTable.getValue()){
				if(obj.getNominatedFor().equals(nominatedFor)){
					share=share+obj.getNomineeShare();
				}
			}
			if(share>100){
				msg=msg+"Nomination share for "+nominatedFor+" is more than 100%."+"\n";
			}
		}
		if(!msg.equals("")){
			showDialogMessage(msg);
			return false;
		}
		return true;
	}

	private boolean validateMemberAddition() {
		// TODO Auto-generated method stub
		String msg="";
		if(lbNominatedFor.getSelectedIndex()==0){
			msg=msg+"Please select nominated for."+"\n";
		}
		if(oblFamilyDetails.getSelectedIndex()==0&&cbNonFamilyMember.getValue().equals(false)){
			msg=msg+"Please select family member or tick non family member checkbox."+"\n";
		}
		if(oblFamilyDetails.getSelectedIndex()!=0&&cbNonFamilyMember.getValue().equals(true)){
			msg=msg+"Either select family member or tick non family member checkbox.";
		}
		if(!msg.equals("")){
			showDialogMessage(msg);
			return false;
		}
		
		return true;
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPopup=fbuilder.setlabel("Nomination Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", nominationTable.getTable());
		FormField fnominationTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Nominated For", lbNominatedFor);
		FormField flbNominatedFor= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Family Member", oblFamilyDetails);
		FormField foblFamilyDetails= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", btnAdd);
		FormField fbtnAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Non Family Member", cbNonFamilyMember);
		FormField fcbNonFamilyMember= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = {  
				{fgroupingPopup},
				{flbNominatedFor ,foblFamilyDetails ,fcbNonFamilyMember, fbtnAdd},
				{fnominationTable}
			
		};
		this.fields=formfield;
	}
	
	public int getAgeAsOnDate(Date dob){
        Date now = new Date();
        long timeBetween = now.getTime() - dob.getTime();
        double yearsBetween = timeBetween / 3.15576e+10;
        int age = (int) Math.floor(yearsBetween);
        return age;
	}
}
