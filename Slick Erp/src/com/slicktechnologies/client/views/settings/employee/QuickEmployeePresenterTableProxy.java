package com.slicktechnologies.client.views.settings.employee;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.slicktechnologies.client.views.settings.employee.EmployeePresenter.EmployeePresenterTable;
import com.slicktechnologies.client.views.settings.employee.QuickEmployeePresenter.QuickEmployeePresenterTable;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.UserRole;

public class QuickEmployeePresenterTableProxy extends QuickEmployeePresenterTable {

	
	TextColumn<Employee> getCountColumn;
	TextColumn<Employee> getEmpNameCol;
	TextColumn<Employee> getEmpBranchCol;
	TextColumn<Employee> getUserIdCol;
	TextColumn<Employee> getEmpEmailCol;
	TextColumn<Employee> customerPhone;
	TextColumn<Employee> getEmpRoleCol;
	TextColumn<Employee> getEmpStatusCol;
	
	
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("getEmpStatusCol"))
			return this.getEmpStatusCol;
		if(varName.equals("getEmpBranchCol"))
			return this.getEmpBranchCol;
		if(varName.equals("getUserIdCol"))
			return this.getUserIdCol;
		if(varName.equals("getEmpEmailCol"))
			return this.getEmpEmailCol;
		if(varName.equals("customerPhone"))
			return this.customerPhone;
		if(varName.equals("getEmpRoleCol"))
			return this.getEmpRoleCol;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		return null ;
	}
	
	public QuickEmployeePresenterTableProxy()
	{
		super();
	}
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		getCountColumn();
		getEmpNameCol();
		getEmpBranchCol();
//		getUserIdCol();
		getEmpEmailCol();
//		getEmpRoleCol();
		getEmpStatusCol();
		getPhoneno();
		
		
		
	}
	

	private void getPhoneno() {
		// TODO Auto-generated method stub
		customerPhone=new TextColumn<Employee>(){

			@Override
			public String getValue(Employee object) {
				
				return object.getCellNumber1() + "";
			}
			
		};
		table.addColumn(customerPhone, "Phone");
		table.setColumnWidth(customerPhone, 110, Unit.PX);
		customerPhone.setSortable(true);
		
	}

	private void getEmpStatusCol() {
		// TODO Auto-generated method stub
		getEmpStatusCol=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				if( object.isStatus()==true)
					return "Active";
				else 
					return "In Active";
			}
				};
		table.addColumn(getEmpStatusCol,"Status");
		table.setColumnWidth(getEmpStatusCol,110, Unit.PX);
		getEmpStatusCol.setSortable(true);
		
	}



	private void getEmpRoleCol() {
		// TODO Auto-generated method stub
		getEmpRoleCol=new TextColumn<Employee>() {
			@Override
			public String getValue(Employee object) {
				if(object.getRole()!=null && object.getUserRoleName()!=null){
					return object.getUserRoleName();
				}
				return "";
			}
		};
		table.addColumn(getEmpRoleCol,"User Role");
		table.setColumnWidth(getEmpRoleCol,90, Unit.PX);
		getEmpRoleCol.setSortable(true);
		
		
	}
	

	

	private void getEmpEmailCol() {
		// TODO Auto-generated method stub
		getEmpEmailCol=new TextColumn<Employee>() {
			@Override
			public String getValue(Employee object) {
				if(object.getEmail()!=null){
					return object.getEmail();
				}
				return "";
			}
		};
		table.addColumn(getEmpEmailCol,"Email");
		table.setColumnWidth(getEmpEmailCol,110, Unit.PX);
		getEmpEmailCol.setSortable(true);
		
		
	}



	private void getUserIdCol() {
		// TODO Auto-generated method stub
		getUserIdCol=new TextColumn<Employee>() {
			@Override
			public String getValue(Employee object) {
				if(object.getUserName()!=null){
					return object.getUserName();
				}
				return "";
			}
		};
		table.addColumn(getUserIdCol,"User Id");
		table.setColumnWidth(getUserIdCol,90, Unit.PX);
		getUserIdCol.setSortable(true);
		
	}




	private void getEmpBranchCol() {
		// TODO Auto-generated method stub
		getEmpBranchCol=new TextColumn<Employee>() {
			@Override
			public String getValue(Employee object) {
				if(object.getBranchName()!=null){
					return object.getBranchName();
				}
				return "";
			}
		};
		table.addColumn(getEmpBranchCol,"Branch");
		table.setColumnWidth(getEmpBranchCol,110, Unit.PX);
		getEmpBranchCol.setSortable(true);
		
	}




	private void getEmpNameCol() {
		// TODO Auto-generated method stub
		getEmpNameCol=new TextColumn<Employee>() {
			@Override
			public String getValue(Employee object) {
				if(object.getEmployeeName()!=null){
					return object.getEmployeeName();
				}
				return "";
			}
		};
		table.addColumn(getEmpNameCol,"Employee Name");
		table.setColumnWidth(getEmpNameCol,110, Unit.PX);
		getEmpNameCol.setSortable(true);
		
		
	}

	private void getCountColumn()
	{
		getCountColumn=new TextColumn<Employee>()
				{
			@Override
			public String getValue(Employee object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"Id");
				getCountColumn.setSortable(true);
				table.setColumnWidth(getCountColumn, "100px");
	}
	
	
	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		addSortingOnCountColumn();
		addSortingOnEmpNameCol();
		addSortingOnEmpBranchCol();
		addSortingOnEmpEmailCol();
		addSortingOnEmpRoleCol();
		addSortingOnEmpStatusCol();
		
	}


  
	private void addSortingOnCountColumn() {
		// TODO Auto-generated method stub
		
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingOnEmpNameCol() {
		// TODO Auto-generated method stub
		
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(getEmpNameCol, new Comparator<Employee>()
			{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployeeName()!=null && e2.getEmployeeName()!=null){
						return e1.getEmployeeName().compareTo(e2.getEmployeeName());}
				}
				else{
					return 0;}
				
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingOnEmpBranchCol() {
		// TODO Auto-generated method stub
		
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(getEmpBranchCol, new Comparator<Employee>()
			{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranchName()!=null && e2.getBranchName()!=null){
						return e1.getBranchName().compareTo(e2.getBranchName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingOnEmpEmailCol() {
		// TODO Auto-generated method stub
		
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(getEmpEmailCol, new Comparator<Employee>()
			{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmail()!=null && e2.getEmail()!=null){
						return e1.getEmail().compareTo(e2.getEmail());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingOnEmpRoleCol() {
		// TODO Auto-generated method stub
		
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(getEmpRoleCol, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getUserRoleName()!=null && e2.getUserRoleName()!=null){
						return e1.getUserRoleName().compareTo(e2.getUserRoleName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingOnEmpStatusCol() {
		// TODO Auto-generated method stub
		List<Employee> list=getDataprovider().getList();
		columnSort=new ListHandler<Employee>(list);
		columnSort.setComparator(getEmpStatusCol, new Comparator<Employee>()
				{
			@Override
			public int compare(Employee e1,Employee e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.isStatus()!=null && e2.isStatus()!=null){
						return e1.isStatus().compareTo(e2.isStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
		
		
	}

	
	
	

}
