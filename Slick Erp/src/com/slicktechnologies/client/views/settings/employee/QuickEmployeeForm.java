package com.slicktechnologies.client.views.settings.employee;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.UserRole;

public class QuickEmployeeForm extends FormScreen<Employee>  implements ClickHandler, ValueChangeHandler<Long>, ChangeHandler{

	//ObjectListBox<Employee> lbEmployeeName;
	
	TextBox lbEmployeeName;
	ObjectListBox<Branch>lblBranch;
	TextBox tbUserId;
	CheckBox cbStatus;
	PasswordTextBox ptbPassword,ptbReTypePassword;
	//ObjectListBox<Config> olbRole;
	ObjectListBox<UserRole> olbRole;
	PhoneNumberBox pnbCellNo1;
	EmailTextBox tbEmail;
	
	Employee employeeobj;
	boolean validusername=true;
	
	
	User userEntity;
	
	public QuickEmployeeForm  () {
		super(FormStyle.DEFAULT);
		createGui();
	}
	
	
	public QuickEmployeeForm  (boolean poupFlag) {
		super(poupFlag);
		createGui();
	}


	private void initalizeWidget()
	{
		Console.log("Inside user form initialize 1");
		lblBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(lblBranch);

		cbStatus=new CheckBox();
		cbStatus.setValue(true);  
		tbUserId=new TextBox();

		ptbPassword=new PasswordTextBox();

		ptbReTypePassword=new PasswordTextBox();

		lbEmployeeName=new TextBox();
	//	AppUtility.makeSalesPersonListBoxLive(lbEmployeeName);

	
		Console.log("Inside user form initialize 2");
//		olbRole=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbRole,Screen.EMPLOYEEROLE);
		
		olbRole=new ObjectListBox<UserRole>();
		MyQuerry qu= new MyQuerry();
		qu.setQuerryObject(new UserRole());
		olbRole.MakeLive(qu);
		
		Console.log("Inside user form initialize 3");
		pnbCellNo1=new PhoneNumberBox(); 
		tbEmail=new EmailTextBox();

	}

		
	@Override
	public void createScreen() {
    /*******Token to initialize the processlevel menus.**********/
		initalizeWidget();
		
		this.processlevelBarNames = new String[] { "New" };
		
    /*****************Form Field Initialization*****************/

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingUserInformation=fbuilder.setlabel("Employee Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("* Employee Name",lbEmployeeName);
		FormField flbEmployeeName= fbuilder.setMandatory(true).setMandatoryMsg("Employee Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Branch",lblBranch);
		FormField flblBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* User ID",tbUserId);
		FormField ftbUserId= fbuilder.setMandatory(true).setMandatoryMsg("User Id is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Password",ptbPassword);
		FormField fptbPassword= fbuilder.setMandatory(true).setMandatoryMsg("Password is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Confirm Password",ptbReTypePassword);
		FormField fptbReTypePassword= fbuilder.setMandatory(true).setMandatoryMsg("Re Type Password is Mandatory").setRowSpan(0).setColSpan(0).build();

		
//		fbuilder = new FormFieldBuilder("User Role",olbRole);
//		FormField folbRole= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* User Role",olbRole);
		FormField folbRole= fbuilder.setMandatory(true).setMandatoryMsg("Role is Mandatory").setRowSpan(0).setColSpan(0).build();


		fbuilder = new FormFieldBuilder("Mobile Number",pnbCellNo1);
		FormField ftbMobileNumber = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Email Id",tbEmail);
		FormField ftbEmail = fbuilder.setMandatory(false).setMandatoryMsg("Email is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmpInformation=fbuilder.setlabel("User Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		/**************************************************End*********************************************************************/

		/*********************************************Lay Out Making Code**********************************************************/


		FormField[][] formfield = {  
//				{fgroupingUserInformation},
//				{flbEmployeeName,flblBranch,ftbUserId,fptbPassword,fptbReTypePassword},
//				{folbRole,ftbMobileNumber,ftbEmail,fcbStatus},
				
				{fgroupingUserInformation},
				{flbEmployeeName,flblBranch,ftbMobileNumber,ftbEmail},
				{fcbStatus},
				{fgroupingEmpInformation},
				{folbRole,ftbUserId,fptbPassword,fptbReTypePassword},
		};


		this.fields=formfield;		
	}
	
	
	


	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Employee  model) 
	{

		if(lbEmployeeName.getValue()!=null){
			model.setEmployeeName(lbEmployeeName.getValue());
			model.setFullname(lbEmployeeName.getValue());
		}
		if(lblBranch.getValue()!=null)
			model.setBranchName(lblBranch.getValue());
		if(cbStatus.getValue()!=null)
			model.setStatus(cbStatus.getValue());
		if(tbUserId.getValue()!=null)
			model.setUserName(tbUserId.getValue());
		if(ptbPassword.getValue()!=null)
			model.setPassword(ptbPassword.getValue());
		if(this.lbEmployeeName.getValue()!=null){
				model.setFullname(lbEmployeeName.getValue().trim());
			}	
		
		if(pnbCellNo1.getValue()!=null){
			model.setCellNumber1(pnbCellNo1.getValue());
		}
		if(olbRole.getSelectedIndex()!=0){
			model.setRole(olbRole.getSelectedItem());
		}
		if(olbRole.getValue()!=null)
		{
			model.setRoleName(olbRole.getValue());
		}
		
		
		
		if(tbEmail.getValue()!=null){
			model.setEmail(tbEmail.getValue());
		}
		
		manageEmployeeDropDownl(model);
		
		employeeobj=model;
		presenter.setModel(model);


	}

	
	


	@Override
	public void updateView(Employee view) 
	{

		employeeobj=view;
		
	
		String employeeName = "";
		if(view.getEmployeeName()!=null && !view.getEmployeeName().equals("")){
			lbEmployeeName.setValue(view.getEmployeeName());
			employeeName = view.getEmployeeName();
		}
		else{
			lbEmployeeName.setValue(view.getFullname());
			employeeName = view.getFullname();
		}
		if(view.getBranchName()!=null)
  		lblBranch.setValue(view.getBranchName());
		
		if(view.isStatus()!=null)
			cbStatus.setValue(view.isStatus());
		if(view.getUserName()!=null)
			tbUserId.setValue(view.getUserName());
		if(view.getPassword()!=null)
		{
			ptbPassword.setValue(view.getPassword());
			ptbReTypePassword.setValue(view.getPassword());

		}
		if(view.getCellNumber1()!=null){
			pnbCellNo1.setValue(view.getCellNumber1());
		}
		if(view.getRole()!=null)
		{
			olbRole.setValue(view.getRole().toString());
		}
		
		
		if(view.getEmail()!=null){
			tbEmail.setValue(view.getEmail());
		}
		System.out.println("view.getUserName()"+view.getUserName());
		System.out.println("view.getPassword()"+view.getPassword());

		if(view.getUserName()==null && view.getPassword()==null){
			getdetailsFromUserEntity(employeeName, true);
		}
		else{
			getdetailsFromUserEntity(employeeName, false);
		}
		
		presenter.setModel(view);
	}
	
	
	public void getdetailsFromUserEntity(String fullname, final boolean flag) {
		final GenricServiceAsync async = GWT.create(GenricService.class);
		
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("employeeName");
		filter.setStringValue(fullname);
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new User());
		final boolean flag2 = flag;
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				Console.log("user entity size"+result.size());
					for(SuperModel model : result){
						User  userentity = (User) model;
						userEntity = userentity;
						if(flag2){
							olbRole.setValue(userentity.getRole().getRoleName());
							tbUserId.setValue(userentity.getUserName());
							ptbPassword.setValue(userentity.getPassword());
							ptbReTypePassword.setValue(userentity.getPassword());
						}

						
					}

			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}


	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 27-011-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date -  27-011-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			
			/**
			 * @author Vijay Date -  27-011-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.QUICKEMPLOYEESCREEN,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{

	}

	/**
	 * Extra validation which matches the typed pass word from retyped passwords
	 */
	@Override
	public boolean validate()
	{
		boolean result=super.validate();
		boolean presresult=true;
		

		String pass=this.ptbPassword.getText().trim();
		String retypepass=ptbReTypePassword.getText().trim();

		if((pass.equals("")==false&&retypepass.equals("")==false)&&pass.equals(retypepass)==false)
		{
			ptbReTypePassword.getElement().getStyle().setBorderColor("#dd4b39");
			//ptbPassword.getElement().getStyle().setBorderColor("#dd4b39");
			presresult=false;
		}
		if(presresult==false)
			showDialogMessage("Password Mismatch!");

		
		

		return result&&presresult;

	}
	
	


	@Override
	public void clear() {

		super.clear();
		cbStatus.setValue(true); 
	}


	@Override
	public void setToViewState() {
		super.setToViewState();

		SuperModel model=new User();
		model=employeeobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.QUICKEMPLOYEESCREEN, employeeobj.getCount(), null,null,null, false, model, null);
	
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(employeeobj!=null){
			SuperModel model=new User();
			model=employeeobj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.QUICKEMPLOYEESCREEN, employeeobj.getCount(), null,null,null, false, model, null);
		
		}
	}





	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onValueChange(ValueChangeEvent<Long> event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}


	public SuperTable<User> getSupertable() {
		// TODO Auto-generated method stub
		return null;
	}


	public void retriveTable(MyQuerry userQuery) {
		// TODO Auto-generated method stub
		
	}


	public void applyHandler(QuickEmployeePresenter quickEmployeePresenter) {
		// TODO Auto-generated method stub
		
	}


	public Object getAddRowapprover() {
		// TODO Auto-generated method stub
		return null;
	}


//	public void applyHandler(QuickEmployeePresenter quickEmployeePresenter) {
//		// TODO Auto-generated method stub
//		
//	}
	
	/**
	 * @author Vijay Date :- 30-12-2021
	 * Des :- when we create new employee i have updated it the same to global list show in dropdown immediately 
	 */
	private void manageEmployeeDropDownl(Employee empModel) {
			ScreeenState scrState = AppMemory.getAppMemory().currentState;

			if (scrState.equals(ScreeenState.NEW)) {
				LoginPresenter.globalEmployee.add(empModel);
			}
			if (scrState.equals(ScreeenState.EDIT)) {
				for (int i = 0; i < LoginPresenter.globalEmployee.size(); i++) {
					if (LoginPresenter.globalEmployee.get(i).getId().equals(empModel.getId())) {
						LoginPresenter.globalEmployee.add(empModel);
						LoginPresenter.globalEmployee.remove(i);
					}
				}
			}

			if (scrState.equals(ScreeenState.NEW)) {
				EmployeeInfo empinfo = getEmployeInfo(empModel);
				LoginPresenter.globalEmployeeInfo.add(empinfo);
			}
			if (scrState.equals(ScreeenState.EDIT)) {
				for (int i = 0; i < LoginPresenter.globalEmployeeInfo.size(); i++) {
					if (LoginPresenter.globalEmployeeInfo.get(i).getId()!=null && LoginPresenter.globalEmployeeInfo.get(i).getId().equals(empModel.getId())) {
						LoginPresenter.globalEmployeeInfo.add(getEmployeInfo(empModel));
						LoginPresenter.globalEmployeeInfo.remove(i);
					}
				}
			}

	}
	
	private EmployeeInfo getEmployeInfo(Employee empModel) {

		EmployeeInfo empinfo=new EmployeeInfo();
		empinfo.setCount(empModel.getCount());
		empinfo.setBranch(empModel.getBranchName());
		empinfo.setCellNumber(empModel.getCellNumber1());
		empinfo.setDepartment(empModel.getDepartMent());
		empinfo.setDesignation(empModel.getDesignation());
		empinfo.setEmpCount(empModel.getCount());
		empinfo.setEmployeerole(empModel.getRoleName());
		empinfo.setEmployeeType(empModel.getEmployeeType());
		empinfo.setFullName(empModel.getFullname());
		empinfo.setId(empModel.getId());
		empinfo.setCountry(empModel.getCountry());
		empinfo.setCompanyId(empModel.getCompanyId());
		empinfo.setCaste(empModel.getCaste());
		/**
		 * Date : 04-05-2018 BY ANIL
		 */
		if (empModel.getGender() != null) {
			empinfo.setGender(empModel.getGender());
		} else {
			empinfo.setGender("");
		}
		
		/**
		 * Date : 29-05-2018 BY ANIL
		 */
		if (empModel.getNumberRange() != null) {
			empinfo.setNumberRangeAsEmpTyp(empModel.getNumberRange());
		} else {
			empinfo.setNumberRangeAsEmpTyp("");
		}
		
		/**
		 * Date : 20-07-2018 BY ANIL
		 */
		if(empModel.getProjectName()!=null){
			empinfo.setProjectName(empModel.getProjectName());
		}
		
		empinfo.setStatus(empModel.isStatus());
		
		/**
		 * Date : 25-07-2018 By  ANIL
		 */
		if(empModel.getDob()!=null){
			empinfo.setDateOfBirth(empModel.getDob());
		}
		
		if(empModel.getJoinedAt()!=null){
			empinfo.setDateOfJoining(empModel.getJoinedAt());
		}
		
		if(empModel.getLastWorkingDate()!=null){
			empinfo.setLastWorkingDate(empModel.getLastWorkingDate());
		}else{
			empinfo.setLastWorkingDate(null);
		}
		/**
		 * Date : 14-11-2018 BY ANIL
		 */
		empinfo.setDisabled(empModel.isDisabled());
		empinfo.setDisablityPercentage(empModel.getDisablityPercentage());
		

		/**
		 * Date : 21-12-2018 By ANIL
		 */
		empinfo.setAbsconded(empModel.isAbsconded());
		empinfo.setTerminated(empModel.isTerminated());
		empinfo.setResigned(empModel.isResigned());
		empinfo.setRealeaseForPayment(empModel.isRealeaseForPayment());
		/**
		 * End
		 */
		
		/**
		 * @author Anil ,Date : 10-04-2019
		 */
		if(empModel.getEmpGrp()!=null){
			empinfo.setEmpGrp(empModel.getEmpGrp());
		}
		
		/**
		 * @author Anil , Date : 23-09-2019
		 */
		if(empModel.getShiftName()!=null){
			empinfo.setShiftName(empModel.getShiftName());
		}
		
		empinfo.setReliever(empinfo.isReliever());
		return empinfo;
	
	}

}
