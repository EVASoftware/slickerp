package com.slicktechnologies.client.views.settings.employee;
import java.util.Date;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.TextInputCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.helperlayer.EducationalInfo;


public class EducationalQualificationTable extends SuperTable<EducationalInfo>  
{
	Column<EducationalInfo, String> deleteColumn;
	Column<EducationalInfo, String> schoolColumn, qulifactionColumn,
			levelColumn;
//	Column<EducationalInfo, Date> dateYearOfPassing;
	Column<EducationalInfo, String> dateYearOfPassing;
	TextColumn<EducationalInfo> viewschoolColumn, viewqualificationColumn,
			viewLevelColumn, viewdColumn;

	public EducationalQualificationTable() {
		super();
	}

	public EducationalQualificationTable(UiScreen<EducationalInfo> view) {
		super(view);
	}

	public void createTable() {

		createColumnlnSchoolColumn();
		createColumnlnQualificationColumn();
		createColumnlnLevel();
		createColumndateColumn();
		createColumndeleteColumn();
		createViewSchoolColumn();
		createViewQualificationColumn();
		createViewLevelColumn();
		createViewColumndateColumn();
		table.addColumn(schoolColumn, "School/University");
		table.addColumn(qulifactionColumn, "Qualification");
		table.addColumn(levelColumn, "Level");
		table.addColumn(dateYearOfPassing, "Year of Passing");
		table.addColumn(deleteColumn, "Remove");
		addFieldUpdater();
		table.setLoadingIndicator(null);
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);

	}

	protected void createViewSchoolColumn() {

		viewschoolColumn = new TextColumn<EducationalInfo>() {
			@Override
			public String getValue(EducationalInfo object) {
				return object.getSchoolName();
			}
		};

	}

	protected void createViewQualificationColumn() {

		viewqualificationColumn = new TextColumn<EducationalInfo>() {
			@Override
			public String getValue(EducationalInfo object) {
				return object.getDegreeObtained();
			}
		};

	}

	protected void createViewLevelColumn() {

		viewLevelColumn = new TextColumn<EducationalInfo>() {
			@Override
			public String getValue(EducationalInfo object) {
				return object.getPercentage();
			}
		};

	}

	protected void createViewColumndateColumn() {
		viewdColumn = new TextColumn<EducationalInfo>() {
			@Override
			public String getValue(EducationalInfo object) {
				System.out.println("Year of passing"+ object.getYearOfPassing());
//				try {
					DateTimeFormat format = DateTimeFormat.getFormat("yyyy");
//					String date = format.format(object.getYearOfPassing());
					String date = object.getYearOfPassing();
					return date;
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				return "";
			}

		};

	}

	// ************************************************************************************************************//

	protected void createColumnlnSchoolColumn() {
		TextInputCell editCell = new TextInputCell();
		schoolColumn = new Column<EducationalInfo, String>(editCell) {
			@Override
			public String getValue(EducationalInfo object) {
				return object.getSchoolName();
			}
		};

	}

	protected void createColumnlnQualificationColumn() {
		TextInputCell editCell = new TextInputCell();
		qulifactionColumn = new Column<EducationalInfo, String>(editCell) {
			@Override
			public String getValue(EducationalInfo object) {
				return object.getDegreeObtained();
			}
		};

	}

	protected void createColumnlnLevel() {
		TextInputCell editCell = new TextInputCell();
		levelColumn = new Column<EducationalInfo, String>(editCell) {
			@Override
			public String getValue(EducationalInfo object) {
				return object.getPercentage();
			}
		};

	}

	protected void createColumndateColumn() {
//		DateTimeFormat fmt = DateTimeFormat.getFormat("yyyy");
//		DatePickerCell date = new DatePickerCell(fmt);
//		dateYearOfPassing = new Column<EducationalInfo, Date>(date) {
//			@Override
//			public Date getValue(EducationalInfo object) {
//				if (object.getYearOfPassing() == null)
//					return new Date();
//				return object.getYearOfPassing();
//			}
//
//		};
//		DateTimeFormat fmt = DateTimeFormat.getFormat("yyyy");
//		DatePickerCell date = new DatePickerCell(fmt);
		TextInputCell editCell = new TextInputCell();
		dateYearOfPassing = new Column<EducationalInfo, String>(editCell) {
			@Override
			public String getValue(EducationalInfo object) {
//				if (object.getYearOfPassing() == null)
//					return new Date()+"";
				return object.getYearOfPassing()+"";
			}

		};

	}

	protected void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<EducationalInfo, String>(btnCell) {
			@Override
			public String getValue(EducationalInfo object) {
				return "Delete";
			}
		};

	}

	// ************************************************************************************************************//

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterSchool();
		createFieldUpdaterQualification();
		createFieldUpdaterLevel();
		createFieldUpdaterYearOfPassingColumn();
		createFieldUpdaterdeleteColumn();
	}

	protected void createFieldUpdaterSchool() {
		schoolColumn
				.setFieldUpdater(new FieldUpdater<EducationalInfo, String>() {
					@Override
					public void update(int index, EducationalInfo object,
							String value) {
						if (value != null) {
							if (value.trim().equals("") == false)
								object.setSchoolName(value);
						}

						table.redrawRow(index);
					}
				});
	}

	protected void createFieldUpdaterQualification() {
		qulifactionColumn
				.setFieldUpdater(new FieldUpdater<EducationalInfo, String>() {
					@Override
					public void update(int index, EducationalInfo object,
							String value) {
						if (value != null) {
							if (value.trim().equals("") == false)
								object.setDegreeObtained(value);
						}

						table.redrawRow(index);
					}
				});
	}

	protected void createFieldUpdaterLevel() {
		levelColumn
				.setFieldUpdater(new FieldUpdater<EducationalInfo, String>() {
					@Override
					public void update(int index, EducationalInfo object,
							String value) {
						if (value != null) {
							if (value.trim().equals("") == false)
								object.setPercentage(value);
						}

						table.redrawRow(index);
					}
				});
	}

	protected void createFieldUpdaterYearOfPassingColumn() {
//		dateYearOfPassing
//				.setFieldUpdater(new FieldUpdater<EducationalInfo, Date>() {
//					@Override
//					public void update(int index, EducationalInfo object,
//							Date value) {
//						if (value != null) {
//							object.setYearOfPassing(value);
//							System.out.println("Year of passing"
//									+ object.getYearOfPassing());
//						}
//						table.redrawRow(index);
//					}
//				});
		
		dateYearOfPassing.setFieldUpdater(new FieldUpdater<EducationalInfo, String>() {
			@Override
			public void update(int index, EducationalInfo object,
					String value) {
				if (value != null) {
					object.setYearOfPassing(value);
					System.out.println("Year of passing"
							+ object.getYearOfPassing());
				}
				table.redrawRow(index);
			}
		});
	}

	protected void createFieldUpdaterdeleteColumn() {
		deleteColumn
				.setFieldUpdater(new FieldUpdater<EducationalInfo, String>() {
					@Override
					public void update(int index, EducationalInfo object,
							String value) {
						getDataprovider().getList().remove(object);

						table.redrawRow(index);
					}
				});
	}

	public void addColumnSorting() {

	}

	@Override
	public void setEnable(boolean state) {

		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);

		if (state == true) {
			table.addColumn(schoolColumn, "School/University");
			table.addColumn(qulifactionColumn, "Qualification");
			table.addColumn(levelColumn, "Level");
			table.addColumn(dateYearOfPassing, "Year of Passing");
			table.addColumn(deleteColumn, "Remove");
			table.setLoadingIndicator(null);
			addFieldUpdater();
		}

		else {

			table.addColumn(viewschoolColumn, "School/University");
			table.addColumn(viewqualificationColumn, "Qualification");
			table.addColumn(viewLevelColumn, "Level");
			table.addColumn(viewdColumn, "Year of Passing");

		}
	}

	@Override
	public void applyStyle() {

	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<EducationalInfo>() {
			@Override
			public Object getKey(EducationalInfo item) {
				return new Date().getTime();
			}
		};
	}}
