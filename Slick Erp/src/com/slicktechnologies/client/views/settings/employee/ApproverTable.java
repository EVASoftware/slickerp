package com.slicktechnologies.client.views.settings.employee;

import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;

public class ApproverTable extends DataGrid<String>
{
	TextColumn<String>employeeColumn;
	Column<String,String>delete;
	ListDataProvider<String>listDataProvider;
	FlowPanel panel;
	public ApproverTable()
	{
		listDataProvider=new ListDataProvider<String>();
		listDataProvider.addDataDisplay(this);
		employeeColumn=new TextColumn<String>() {

			@Override
			public String getValue(String object) {
				return object;
			}
		};
		addColumn(employeeColumn,"Approver Name");
		
		ButtonCell cell=new ButtonCell();
		delete=new Column<String, String>(cell) {

			@Override
			public String getValue(String object) {
				// TODO Auto-generated method stub
				return "Delete";
			}
		};
		delete.setFieldUpdater(new FieldUpdater<String, String>() {
			
			@Override
			public void update(int index, String object, String value) {
				
				
			}
		});
		addColumn(delete,"Remove");
		setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		
		setWidth("100%");
		setHeight("100%");
		panel=new FlowPanel();
		panel.add(this);
		
		setHeight("150px");
		setRowCount(20);
		setLoadingIndicator(null);
	
		
	    //getElement().getStyle().setBackgroundColor("RED");
		//System.out.println("Ajj");
	}
	
	public List<String>getValue()
	{
		return listDataProvider.getList();
	}
	
	public void clear()
	{
		if(listDataProvider!=null)
		{
			listDataProvider.removeDataDisplay(this);
			listDataProvider=new ListDataProvider<String>();
			listDataProvider.addDataDisplay(this);
			
		}
		
	}

}
