package com.slicktechnologies.client.views.settings.employee;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
public class EmployeeBranch extends SuperModel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8663561052377894916L;
	int srNo;
	String BranchName;
	
	
	//    getters and setters 
	
	public int getSrNo() {
		return srNo;
	}
	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	public String getBranchName() {
		return BranchName;
	}
	public void setBranchName(String branchName) {
		BranchName = branchName;
	}
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	

}
