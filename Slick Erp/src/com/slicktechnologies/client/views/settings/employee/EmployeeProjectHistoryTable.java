package com.slicktechnologies.client.views.settings.employee;

import java.util.ArrayList;
import java.util.Date;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextInputCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.personlayer.EmployeeProjectHistory;

public class EmployeeProjectHistoryTable  extends SuperTable<EmployeeProjectHistory>{

	TextColumn<EmployeeProjectHistory> viewProjectName,viewProjectStartDate,viewProjectEndDate,viewProjectCodeCol;
	Column<EmployeeProjectHistory,String> getDeleteCol,getProjectNameCol,getProjectCodeCol;
	Column<EmployeeProjectHistory,Date> getProjectStartDateCol,getProjectEndDateCol;
	
	ArrayList<HrProject> projectList=new ArrayList<HrProject>();
	ArrayList<String> list=new ArrayList<String>();
	
	
	/**
	 * @author Anil
	 * @since 02-09-2020
	 * EnableSiteLocation
	 */
	TextColumn<EmployeeProjectHistory> getSiteLocationCol;
	boolean siteLocation=false;
	
	public EmployeeProjectHistoryTable() {
		super();
		
		siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "EnableSiteLocation");
	}
	
	
	public EmployeeProjectHistoryTable(UiScreen<EmployeeProjectHistory> mv) {
		super(mv);
		
		siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "EnableSiteLocation");
	}


	@Override
	public void createTable() {
		
		getProjectName();
		
		
//		getProjectCodeCol();
//		getProjectNameCol();
//		getProjectStartDateCol();
//		getProjectEndDateCol();
//		getDeleteCol();
		
		// TODO Auto-generated method stub
//		addProjectCodeColumn();
//		addProjectNameColumn();
//		addProjectStartDateColumn();
//		addProjectEndDateColumn();
	}
	
	private void getSiteLocationCol() {
		getSiteLocationCol = new TextColumn<EmployeeProjectHistory>() {
			@Override
			public String getValue(EmployeeProjectHistory object) {
				if(object.getSiteLocation()!=null){
					return object.getSiteLocation();
				}else{
					return "";
				}
			}
		};
		table.addColumn(getSiteLocationCol,"Site Location");
	}

	private void getProjectName() {
		final GWTCGlassPanel glassPanel=new GWTCGlassPanel();
		MyQuerry querry =new MyQuerry();
		Filter temp=null;
		temp=new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		querry.setQuerryObject(new HrProject());
		querry.getFilters().add(temp);
		glassPanel.show();
		generivservice.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				glassPanel.hide();
				projectList=new ArrayList<HrProject>();
				list=new ArrayList<String>();
				list.add("--SELECT--");
				if(result.size()!=0){
					for(SuperModel model:result){
						HrProject entity=(HrProject) model;
						projectList.add(entity);
						list.add(entity.getProjectName());
					}
				}
				getProjectNameCol();
				if(!siteLocation){
					getProjectCodeCol();
				}
				if(siteLocation){
					getSiteLocationCol();
				}
				getProjectStartDateCol();
				if(!siteLocation){
					getProjectEndDateCol();
				}
				getDeleteCol();
			}
			@Override
			public void onFailure(Throwable caught) {
				glassPanel.hide();
				getProjectNameCol();
				if(!siteLocation){
					getProjectCodeCol();
				}
				if(siteLocation){
					getSiteLocationCol();
				}
				getProjectStartDateCol();
				if(!siteLocation){
					getProjectEndDateCol();
				}
				getDeleteCol();
			}
			
			
		});
		
		
		
	}


	private void getProjectCodeCol() {
		// TODO Auto-generated method stub
		TextInputCell editCell = new TextInputCell();
		getProjectCodeCol = new Column<EmployeeProjectHistory, String>(editCell) {
			@Override
			public String getValue(EmployeeProjectHistory object) {
				if(object.getProjectCode()!=null){
					return object.getProjectCode();
				}else{
					return "";
				}
			}
		};
		table.addColumn(getProjectCodeCol,"Project Code");
		
		getProjectCodeCol.setFieldUpdater(new FieldUpdater<EmployeeProjectHistory, String>() {
			@Override
			public void update(int index, EmployeeProjectHistory object,String value) {
				// TODO Auto-generated method stub
				if(value!=null){
					object.setProjectCode(value);
					table.redraw();
				}
			}
		});
	}


	private void getProjectNameCol() {
		// TODO Auto-generated method stub
//		TextInputCell cell = new TextInputCell();
		SelectionCell cell=new SelectionCell(list);
		getProjectNameCol = new Column<EmployeeProjectHistory, String>(cell) {
			@Override
			public String getValue(EmployeeProjectHistory object) {
				if(object.getProjectName()!=null){
					return object.getProjectName();
				}else{
					return "";
				}
			}
		};
		table.addColumn(getProjectNameCol,"Project Name");
		
		getProjectNameCol.setFieldUpdater(new FieldUpdater<EmployeeProjectHistory, String>() {
			@Override
			public void update(int index, EmployeeProjectHistory object,String value) {
				// TODO Auto-generated method stub
				if(value!=null&&!value.equals("--SELECT--")){
					
					HrProject project=getProject(value);
					object.setProjectName(value);
					if(project!=null){
						if(project.getProjectCode()!=null){
							object.setProjectCode(project.getProjectCode());
						}else{
							object.setProjectCode("");
						}
						if(project.getFromDate()!=null){
							object.setProjectStartDate(project.getFromDate());
						}
						if(project.getToDate()!=null){
							object.setProjectEndDate(project.getToDate());
						}
					}
					table.redraw();
				}
			}

			
		});

	}

	private HrProject getProject(String value) {
		for(HrProject project:projectList){
			if(value.equals(project.getProjectName())){
				return project;
			}
		}
		return null;
	}
	private void getProjectStartDateCol() {
		// TODO Auto-generated method stub
//		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		getProjectStartDateCol = new Column<EmployeeProjectHistory, Date>(date) {
			@Override
			public Date getValue(EmployeeProjectHistory object) {
				if(object.getProjectStartDate()!=null){
					return object.getProjectStartDate();
				}else{
					object.setProjectStartDate(new Date());
					return object.getProjectStartDate();
				}
			}
		};
		table.addColumn(getProjectStartDateCol,"Start Date");
		
		getProjectStartDateCol.setFieldUpdater(new FieldUpdater<EmployeeProjectHistory, Date>() {
			@Override
			public void update(int index, EmployeeProjectHistory object,Date value) {
				// TODO Auto-generated method stub
				if(value!=null){
					object.setProjectStartDate(value);
					table.redraw();
				}
			}
		});
	}


	private void getProjectEndDateCol() {
		// TODO Auto-generated method stub
//		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		getProjectEndDateCol = new Column<EmployeeProjectHistory, Date>(date) {
			@Override
			public Date getValue(EmployeeProjectHistory object) {
				if(object.getProjectEndDate()!=null){
					return object.getProjectEndDate();
				}else{
					object.setProjectEndDate(new Date());
					return object.getProjectEndDate();
				}
			}
		};
		table.addColumn(getProjectEndDateCol,"End Date");
		
		getProjectEndDateCol.setFieldUpdater(new FieldUpdater<EmployeeProjectHistory, Date>() {
			@Override
			public void update(int index, EmployeeProjectHistory object,Date value) {
				// TODO Auto-generated method stub
				if(value!=null){
					object.setProjectEndDate(value);
					table.redraw();
				}
			}
		});
	}


	private void getDeleteCol() {
		// TODO Auto-generated method stub
		ButtonCell btnCell=new ButtonCell();
		getDeleteCol=new Column<EmployeeProjectHistory, String>(btnCell) {
			@Override
			public String getValue(EmployeeProjectHistory object) {
				return "Delete";
			}
		};
		table.addColumn(getDeleteCol,"");
		
		getDeleteCol.setFieldUpdater(new FieldUpdater<EmployeeProjectHistory, String>() {
			@Override
			public void update(int index, EmployeeProjectHistory object, String value) {
				// TODO Auto-generated method stub
				getDataprovider().getList().remove(index);
				table.redraw();
			}
		});
		
	}


	private void addProjectCodeColumn() {
		// TODO Auto-generated method stub
		viewProjectCodeCol = new TextColumn<EmployeeProjectHistory>() {
			@Override
			public String getValue(EmployeeProjectHistory object) {
				if(object.getProjectCode()!=null){
					return object.getProjectCode();
				}else{
					return "";
				}
			}
		};
		table.addColumn(viewProjectCodeCol, "Project Code");
//		table.setColumnWidth(viewProjectCodeCol, 120, Unit.PX);
	}


	private void addProjectEndDateColumn() {
		// TODO Auto-generated method stub
		viewProjectEndDate = new TextColumn<EmployeeProjectHistory>() {

			@Override
			public String getValue(EmployeeProjectHistory object) {
				// TODO Auto-generated method stub
				String date;
				if(object.getProjectEndDate()!=null)
					date=AppUtility.parseDate(object.getProjectEndDate());
				else
					date="";
				
				return date;
			}
		};

		table.addColumn(viewProjectEndDate, "End Date");
//		table.setColumnWidth(viewProjectEndDate, 120, Unit.PX);	
	}


	private void addProjectStartDateColumn() {
		// TODO Auto-generated method stub
		viewProjectStartDate = new TextColumn<EmployeeProjectHistory>() {

			@Override
			public String getValue(EmployeeProjectHistory object) {
				// TODO Auto-generated method stub
				String date;
				if(object.getProjectStartDate()!=null)
					date=AppUtility.parseDate(object.getProjectStartDate());
				else
					date="";
				
				return date;
			}
		};

		table.addColumn(viewProjectStartDate, "Start Date");
//		table.setColumnWidth(viewProjectStartDate, 120, Unit.PX);	
	}


	private void addProjectNameColumn() {
		// TODO Auto-generated method stub
		viewProjectName = new TextColumn<EmployeeProjectHistory>() {

			@Override
			public String getValue(EmployeeProjectHistory object) {
				// TODO Auto-generated method stub
				return object.getProjectName();
			}
		};

		table.addColumn(viewProjectName, "Project Name");
//		table.setColumnWidth(viewProjectName, 120, Unit.PX);	
	}


	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true) {
			getProjectNameCol();
			if(!siteLocation){
				getProjectCodeCol();
			}
			if(siteLocation){
				getSiteLocationCol();
			}
			getProjectStartDateCol();
			if(!siteLocation){
				getProjectEndDateCol();
			}
			getDeleteCol();
		}else {
			addProjectNameColumn();
			if(!siteLocation){
				addProjectCodeColumn();
			}
			if(siteLocation){
				getSiteLocationCol();
			}
			addProjectStartDateColumn();
			if(!siteLocation){
				addProjectEndDateColumn();
			}
		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
}
