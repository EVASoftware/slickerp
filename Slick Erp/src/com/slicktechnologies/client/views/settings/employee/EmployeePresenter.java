package com.slicktechnologies.client.views.settings.employee;
import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeCheckListType;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;
import com.slicktechnologies.shared.common.shippingpackingprocess.StepsDetails;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.approvalutility.ApprovableScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.CustomerNameChangeService;
import com.slicktechnologies.client.services.CustomerNameChangeServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.RateContractServicesBillingService;
import com.slicktechnologies.client.services.RateContractServicesBillingServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.approval.ApprovalService;
import com.slicktechnologies.client.views.approval.ApprovalServiceAsync;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.humanresource.allocation.payslipallocation.PaySlipAllocationService;
import com.slicktechnologies.client.views.humanresource.allocation.payslipallocation.PaySlipAllocationServiceAsync;
import com.slicktechnologies.client.views.humanresource.employeeadditionaldetails.EmployeeAdditionalDetailsForm;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
/**
 *  FormTableScreen presenter template
 */
public class EmployeePresenter extends FormScreenPresenter<Employee> implements ValueChangeHandler<Date> ,ClickHandler ,ChangeHandler{

	//Token to set the concrete form
	EmployeeForm form;
	ApprovalServiceAsync approvalService=GWT.create(ApprovalService.class);
	CsvServiceAsync service=GWT.create(CsvService.class);
	final GenricServiceAsync async = GWT.create(GenricService.class);
	final RateContractServicesBillingServiceAsync rateasync = GWT.create(RateContractServicesBillingService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	/** The service to initate the approvel process*/
	
	public final GenricServiceAsync genservice=GWT.create(GenricService.class);
	/** date 24.7.2018 added by komal for employee full and final popup **/
	EmployeeFullNFinalPopup employeeFullNFinalPopup = new EmployeeFullNFinalPopup();
	PaySlipAllocationServiceAsync payslipAllocationService = GWT.create(PaySlipAllocationService.class);
	
	/**
	 * Date : 01-12-2018 BY ANIL
	 */
	FromAndToDateBoxPopup datePopup=new FromAndToDateBoxPopup();
	PopupPanel panel=new PopupPanel();
	/**
	 * @author Anil,Date : 28-01-2019
	 * 
	 */
	NominationPopup nominationPopup=new NominationPopup();
	CustomerNameChangeServiceAsync customernamechangeasync = GWT.create(CustomerNameChangeService.class);

	/**
	 * @author Anil , Date : 04-05-2019
	 * added employee ctc allcation popup
	 * raised by sonu for charlene hospitality
	 */
//	EmployeeCtcAllocationPopup empCtcAllocationPopup=new EmployeeCtcAllocationPopup();
	EmployeeCtcAllocationPopup empCtcAllocationPopup;
	CtcTemplatePopupScreen ctcTemplatePopup=null;
	
	/**
	 * Updated By: Viraj
	 * Date: 04-06-2019
	 * Description: To save additional employee details on the employee form itself
	 */
	GeneralViewDocumentPopup gvPopup = new GeneralViewDocumentPopup();
	EmployeeAdditionalDetails entity = new EmployeeAdditionalDetails();
	EmployeeInfo empInfo = new EmployeeInfo();
	/** Ends **/
	
	List<Employee> searchedEmpList;//Ashwini Patil Date:16-06-2023
	public EmployeePresenter (FormScreen<Employee> view,
			Employee model) {
		super(view, model);
		form=(EmployeeForm) view;
		form.applyHandler(this);
		form.getSearchpopupscreen().content.setSize("100%","100%");
		
//		form.getTbFirstName().addValueChangeHandler(this);
//		form.getTbMiddleName().addValueChangeHandler(this);
//		form.getTbLastName().addValueChangeHandler(this);
		
		/*
		 * above old code commented by vijay to change employee first middle and last to fullName and bellow code added
		 */
//		form.getTbfullName().addValueChangeHandler(this);
//		form.ibAadharNumber.addValueChangeHandler(this);
		form.getTbfullName().addChangeHandler(this);
		form.ibAadharNumber.addChangeHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.EMPLOYEE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		/** date 03.8.2018 added by komal for employee full and final popup **/
		employeeFullNFinalPopup.getLblOk().addClickHandler(this);
		employeeFullNFinalPopup.getLblCancel().addClickHandler(this);
		form.olbEmploymentType.addChangeHandler(this);
		employeeFullNFinalPopup.getDbWaiveOff().addChangeHandler(this);
		employeeFullNFinalPopup.lblUpdate.addClickHandler(this);
		form.dbnoticePeriod.addChangeHandler(this);
		form.dbResignationDate.addValueChangeHandler(this);
		
		datePopup.getBtnOne().addClickHandler(this);
		datePopup.getBtnTwo().addClickHandler(this);
		
//		nominationPopup.getLblOk().addClickHandler(this);
//		nominationPopup.getLblCancel().addClickHandler(this);
		
		employeeFullNFinalPopup.lblPrint.addClickHandler(this);
		gvPopup.btnClose.addClickHandler(this);
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {

		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			this.initalize();
		}
		
		if(lbl.getText().contains("Request For Approval"))
		{
			form.showWaitSymbol();
			/**
			 * Updated By Anil Date : 14-10-2016
			 * Release : 30 Sept 2016 
			 * Project : PURCHASE MODIFICATION(NBHC)
			 */
			String documentCreatedBy="";
			if(model.getCreatedBy()!=null){
				documentCreatedBy=model.getCreatedBy();
			}
			
			ApproverFactory appFactory=new ApproverFactory();
//			Approvals approval=appFactory.getApprovals(LoginPresenter.myUserEntity.getEmployeeName(),model.getApproverName()
//					, model.getBranchName(), model.getCount(),documentCreatedBy);
//			
			/**
			 *  nidhi
			 *  7-10-2017
			 *  method change for display name and id
			 */
			Approvals approval=appFactory.getApprovals(LoginPresenter.myUserEntity.getEmployeeName(),model.getApproverName()
					, model.getBranchName(), model.getCount(),documentCreatedBy,model.getCount()+"",model.getFullname());
			
			saveApproval(approval,ConcreteBusinessProcess.REQUESTED,"Request Sent!");
			reactOnApprovalEmail(approval);
			
			
		}
		
		if(lbl.getText().contains("Cancel Approval Request"))
		{
			form.showWaitSymbol(); 
	  		ApproverFactory approverfactory=new ApproverFactory();
			  
			  Filter filter=new Filter();
			  filter.setStringValue(approverfactory.getBuisnessProcessType());
			  filter.setQuerryString("businessprocesstype");
			  
			  Filter filterOnid=new Filter();
			  filterOnid.setIntValue(model.getCount());
			  filterOnid.setQuerryString("businessprocessId");
			  
			  Filter filterOnStatus=new Filter();
			  filterOnStatus.setStringValue(Approvals.PENDING);
			  filterOnStatus.setQuerryString("status");
			  
			  Vector<Filter> vecfilter=new Vector<Filter>();
			  vecfilter.add(filter);
			  vecfilter.add(filterOnid);
			  vecfilter.add(filterOnStatus);
			  MyQuerry querry=new MyQuerry(vecfilter, new Approvals());
			  final ArrayList<SuperModel> array = new ArrayList<SuperModel>();
			  async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

				@Override
				public void onFailure(Throwable caught) {
					caught.printStackTrace();
					
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					array.addAll(result);
					Approvals approval=(Approvals) array.get(0);
					approval.setStatus(Approvals.CANCELLED);
					 
					 saveApproval(approval, ConcreteBusinessProcess.CREATED, "Approval Request Cancelled!");
				}
			});
		}
		
		/**
		 * Date 15-5-2018
		 * By Jayshree
		 */
		if(lbl.getText().contains(AppConstants.PRINTIDCARDINPORTRATEVIEW)){
			final String url = GWT.getModuleBaseURL() + "employee" + "?Id="+model.getId()+"&"+"type="+"EmployeeIDCard"+"&"+"subtype="+"EmployeeIDCardPortrate";
			Window.open(url, "test", "enable");
		}
		
		if(lbl.getText().contains(AppConstants.PRINTIDCARDINLANDSCAPEVIEW)){
			final String url = GWT.getModuleBaseURL() + "employee" + "?Id="+model.getId()+"&"+"type="+"EmployeeIDCard"+"&"+"subtype="+"EmployeeIDCardLandscape";
			Window.open(url, "test", "enable");
		}
		/** date 24.7.2018 added by komal for employee full and final popup **/
		if(lbl.getText().equalsIgnoreCase(AppConstants.FULLANDFINAL)){	
			Console.log("STEP 1");
			if(validateFnFDetails()){
				Console.log("STEP 2");
			employeeFullNFinalPopup.showPopUp();		
//			employeeFullNFinalPopup.getEmployeeCheckListTable().getDataprovider().getList().clear();
			employeeFullNFinalPopup.getEmployeeCheckListTable().connectToLocal();
			employeeFullNFinalPopup.lblUpdate.getElement().setId("addbutton");
			employeeFullNFinalPopup.getHorizontal().add(employeeFullNFinalPopup.lblUpdate);
			/**
			 * @author Anil ,Date : 17-04-2019
			 * Added print button on FnF popup
			 */
			employeeFullNFinalPopup.lblPrint.getElement().setId("addbutton");
			employeeFullNFinalPopup.getHorizontal().add(employeeFullNFinalPopup.lblPrint);
			
			employeeFullNFinalPopup.getDbNoticePeriod().setValue(model.getNoticePeriod());
			Console.log("notice period :" + model.getNoticePeriod());
//			if(model.getShortfall()!=null){
//				employeeFullNFinalPopup.getDbShortFall().setValue(model.getShortfall());
//			}else{
			Console.log("STEP 3");
			Date resignationDate = null , lastWorkingDate = null; 
			if(model.getResignationDate()!=null){
				resignationDate = model.getResignationDate();
			}
			if(model.getLastWorkingDate()!=null){
				lastWorkingDate = model.getLastWorkingDate();
			}
			if(model.getFnfMonth()!=null){
				employeeFullNFinalPopup.dbPayrollMonth.setValue(model.getFnfMonth());
			}else{
				/**
				 * @author Anil
				 * @since 30-04-2020
				 * Setting last set of working as fnf month				 *
				 */
				employeeFullNFinalPopup.setFormat(model.getLastWorkingDate());
			}
			double waiveOff = 0;
			if(model.getWaivedOff()!=null){
				employeeFullNFinalPopup.getDbWaiveOff().setValue(model.getWaivedOff());
				waiveOff = employeeFullNFinalPopup.getDbWaiveOff().getValue();
			}
			/**
			 * @author Anil
			 * @since 22-05-2020
			 */
			if(resignationDate != null && lastWorkingDate != null&&model.getFnfStatus().equals("Initiated")){
				String days = model.getNoticePeriod() - CalendarUtil.getDaysBetween(resignationDate ,lastWorkingDate) - 1+"";
				/**
				 * @author Anil
				 * @since 20-05-2020
				 * if shortfall is going negative then we will set zero value raised by Rahul Tiwari
				 */
				double diff=Double.parseDouble(days) - waiveOff;
				if(diff<0){
					diff=0;
				}
				employeeFullNFinalPopup.getDbShortFall().setValue(diff);
			}else{
				/**
				 * @author Anil
				 * @since 22-05-2020
				 */
				if(model.getShortfall()!=null){
					employeeFullNFinalPopup.getDbShortFall().setValue(model.getShortfall());
				}
			}
//			}
			/**
			 * @author Anil
			 * @since 30-04-2020
			 * checked null condition
			 */
			if(model.getCheckList()!=null&&model.getCheckList().size()>0){
				Console.log("STEP 4");
				employeeFullNFinalPopup.getEmployeeCheckListTable().getDataprovider().setList(model.getCheckList());
			}else{
				Console.log("STEP 5");
				setCheckList();
			}
			
			if(model.getFnfDate()!=null){
				employeeFullNFinalPopup.getDbFnFDate().setValue(model.getFnfDate());
			}
					
		}
		}
		
		if(lbl.getText().contains(AppConstants.EMPLOYMENTCARD)){
			reactOnEmploymentCardPrint();
		}
		
		if(lbl.getText().contains(AppConstants.FORM3A)){
			panel=new PopupPanel(true);
			panel.add(datePopup);
			panel.center();
			panel.show();
		}
		
		if(lbl.getText().contains("Nomination Details")){
			nominationPopup.showPopUp();
			nominationPopup.cbNonFamilyMember.setValue(false);
			nominationPopup.getPopup().setWidth("830px!important");
			nominationPopup.initializeNominationPopup(model,model.getNominationList());
		}
		
		if(lbl.getText().contains("Change Employee Name")){
			reactOnEmpployeeNameChange();
		}
		
		if(lbl.getText().contains("Allocate CTC")){
			empCtcAllocationPopup=new EmployeeCtcAllocationPopup();
			empCtcAllocationPopup.lblNew.getElement().setId("addbutton");
			empCtcAllocationPopup.getHorizontal().add(empCtcAllocationPopup.lblNew);
			addingHandler();
			empCtcAllocationPopup.setEmployeeId(model.getCount());
			empCtcAllocationPopup.checkEmpCtcAllocationStatus();
			empCtcAllocationPopup.showPopUp();		
			
		}
		
		/**
		 * Updated By: Viraj
		 * Date: 04-06-2019
		 * Description: To add employee page on a popup for saving additional employee details
		 */
		if(lbl.getText().contains("Employee Additional Details")) {
			searchedEmpList=new ArrayList<Employee>();
			searchedEmpList=view.getSearchpopupscreen().getSupertable().getListDataProvider().getList();
			//Console.log("searchedEmpList size="+searchedEmpList.size());
			
			reactOnEmployeeAddDetails();
		}
		/** Ends **/
		
	}
	
	/**
	 * Updated By: Viraj
	 * Date: 04-06-2019
	 * Description: To add employee page on a popup for saving additional employee details
	 */
	private void reactOnEmployeeAddDetails() {
		Console.log("inside reactOnEmployeeAddDetails");
		
		MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("empInfo.empCount");
		filter.setIntValue(model.getCount());
		temp.add(filter);
			
		querry.setFilters(temp);
		querry.setQuerryObject(new EmployeeAdditionalDetails());
		form.showWaitSymbol();
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size() != 0) {
					for(SuperModel model:result){
						entity = (EmployeeAdditionalDetails)model;
					}
				} else {
					Console.log("empInfo: "+model.getCount() +" "+ model.getCellNumber1() +" "+ model.getFullname() +" "+ model.getBranchName() 
									+" "+ model.getDesignation() +" "+ model.getRoleName() +" "+ model.getEmployeeType() +" "+ model.getDepartMent());
					empInfo.setEmpCount(model.getCount());
					empInfo.setCellNumber(model.getCellNumber1());
					empInfo.setFullName(model.getFullname());
					empInfo.setBranch(model.getBranchName());
					empInfo.setDesignation(model.getDesignation());
					empInfo.setEmployeerole(model.getRoleName());
					empInfo.setEmployeeType(model.getEmployeeType());
					empInfo.setDepartment(model.getDepartMent());
					
					entity.setEmpInfo(empInfo);
				}
				
				final EmployeeAdditionalDetailsForm form = new EmployeeAdditionalDetailsForm();

				form.content.getElement().getStyle().setHeight(570, Unit.PX);
				form.content.getElement().getStyle().setWidth(950, Unit.PX);

				AppMemory.getAppMemory().currentScreen = Screen.EMPLOYEEADDITIONALDETAILS;
//				gvPopup = new GeneralViewDocumentPopup();
				
				gvPopup.setModel(AppConstants.EMPLOYEEADDITIONALDETAILS, entity);
			}
			
		});

	}
	/** Ends **/
	
	private void reactOnEmpployeeNameChange() {
		System.out.println("in the method");
		System.out.println("name --- "+model.getFullname());
		
		form.showWaitSymbol();
		customernamechangeasync.SaveEmployeeChangeInAllDocs(model, new AsyncCallback<ArrayList<Integer>>() {
			
			@Override
			public void onSuccess(ArrayList<Integer> result) {
				System.out.println("in on success ");
				if(result.contains(25))
					form.showDialogMessage("Employee  Name & Cell Number Updated Successfully");
				form.hideWaitSymbol();
				
				
				if(result.contains(21))
					Console.log("Payslip updated");
				if(result.contains(22))
					Console.log("CTC  updated");
				if(result.contains(23))
					Console.log("Leave Balance updated");
				if(result.contains(24))
					Console.log("User updated");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("falied");
				form.showDialogMessage("Unexpected Error");
				form.hideWaitSymbol();
			}
		});
		
	}

	private void saveApproval(Approvals approval, final String requested,	final String string) {
		
		genservice.save(approval, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onSuccess(ReturnFromServer result) 
			{
//				form.setStatus(requested);
//				SuperModel model=(SuperModel)currentScreen;
				
				model.setApproveStatus(requested);
				genservice.save(model, new AsyncCallback<ReturnFromServer>() {
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
					@Override
					public void onSuccess(ReturnFromServer result) {
//						ApprovableScreen ap=(ApprovableScreen) currentScreen;
//						ap.getstatustextbox().setValue(requested);
						form.setToViewState();
						form.approvedStatus.setValue(model.getApproveStatus());
						form.showDialogMessage(string);
						form.setMenuAsPerStatus();
						form.hideWaitSymbol();
						//Special for Form Table Screen
//						if(ap instanceof FormTableScreen)
//						{
//							FormTableScreen<?>screen=(FormTableScreen<?>) ap;
//							((FormTableScreen) ap).getSupertable().getDataprovider().refresh();
//						}
					}
				});
			}
			
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("Request Failed! Try Again!");
				form.hideWaitSymbol();
			}
		});
	}

	@Override
	public void reactOnPrint() {
		
		System.out.println("in side react on print ");
		final String url = GWT.getModuleBaseURL() + "employee" + "?Id="+model.getId()+"&"+"type="+"EmployeeForm"+"&"+"subtype="+"EmployeeFormPortrate";
//		final String url = GWT.getModuleBaseURL() + "employee" + "?Id="+model.getId();
		Window.open(url, "test", "enable");
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}
	
	
	
	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		if(event.getSource().equals(form.dbResignationDate)){
			if(form.dbResignationDate.getValue()!=null && !(form.dbResignationDate.getValue().equals(""))){
				double noticePeriod = 0;
				int period = 0;
				if(form.dbnoticePeriod.getValue()!=null){
					noticePeriod = form.dbnoticePeriod.getValue();
					period = (int) noticePeriod;
				}
				Date date = form.dbResignationDate.getValue();
				CalendarUtil.addDaysToDate(date, period - 1);
				form.dbLastWorkingDay.setValue(date);
			}
		}
	//	ScreeenState scr = AppMemory.getAppMemory().currentState;

//		if (scr.equals(ScreeenState.NEW) || scr.equals(ScreeenState.EDIT)) {
//			
//			if (event.getSource().equals(form.getTbfullName())) {
//				validateEmployeeName();
//			}
//			
//			if(event.getSource().equals(form.ibAadharNumber)){
//				if(isValidAadharNumber()){
//					validateEmployeeName();
//				}
//			}
			
			// below code commented by vijay 
//			if (event.getSource().equals(form.getTbFirstName())) {
//				validateEmployeeName();
//			}
//			if (event.getSource().equals(form.getTbMiddleName())) {
//				validateEmployeeName();
//			}
//			if (event.getSource().equals(form.getTbLastName())) {
//				validateEmployeeName();
//			}
			//  ends here 
			
//			if (event.getSource().equals(form.getEtbEmail())) {
//				validateEmployeeEmail();
//			}

//		}
	}
	
	
	private boolean isValidAadharNumber() {
		// TODO Auto-generated method stub
		ArrayList<Employee> employeeList = LoginPresenter.globalEmployee;
		if(form.ibAadharNumber.getValue()!=null&&!form.ibAadharNumber.getValue().equals("")){
			long aadharNumber=0;
			try{
				aadharNumber=Long.parseLong(form.ibAadharNumber.getValue().trim());
			}catch(Exception e){
				
			}
			int empId=0;
			if(!form.getEmployeeId().getValue().equals("")){
				try {
					empId = Integer.parseInt(form.getEmployeeId().getValue());
				} catch (Exception e) {
				}
			}
			if(aadharNumber!=0){
				if(empId!=0){
					for(int i=0;i<employeeList.size();i++){
						if(aadharNumber==employeeList.get(i).getAadharNumber()){
							if(empId!=0&&empId!=employeeList.get(i).getCount()){
								/**
								 * @author Anil,Date:03-01-2019
								 * If aadhar number is matching with any inactive employee 
								 * then only show alert message but allow user to save data.
								 * For Orion BY Sonu
								 */
								if(employeeList.get(i).isStatus()==false){
									form.showDialogMessage("Aadhar number already exists for Inactive employee "+employeeList.get(i).getFullName()+"/"+employeeList.get(i).getCount());
									return true;
								}else{
									form.showDialogMessage("Aadhar number already exists.");
									form.ibAadharNumber.setValue("");
									return false;
								}
							}
						}
					}
				}else{
					for(int i=0;i<employeeList.size();i++){
						if(aadharNumber==employeeList.get(i).getAadharNumber()){
							/**
							 * @author Anil,Date:03-01-2019
							 * If aadhar number is matching with any inactive employee 
							 * then only show alert message but allow user to save data.
							 * For Orion BY Sonu
							 */
							if(employeeList.get(i).isStatus()==false){
								form.showDialogMessage("Aadhar number already exists for Inactive employee "+employeeList.get(i).getFullName()+"/"+employeeList.get(i).getCount());
								return true;
							}else{
								form.showDialogMessage("Aadhar number already exists.");
								form.ibAadharNumber.setValue("");
								return false;
							}
						}
					}
				}
				
			}else{
				return true;
			}
			
		}else{
			return true;
		}
		return false;
	}

	private void validateEmployeeName() 
	{
		ArrayList<Employee> employeeList = LoginPresenter.globalEmployee;
		System.out.println("Employee list size ==="+employeeList.size());
		
		if (!form.getTbfullName().getValue().equals("")){
			
			String empname = form.getTbfullName().getValue().trim();
			int empId=0;
			if(!form.getEmployeeId().getValue().equals("")){
				try {
					empId = Integer.parseInt(form.getEmployeeId().getValue());
				} catch (Exception e) {
					form.showDialogMessage("Employee id must be numeric.");
					return;
				}
			}
			
			long aadharCardNumber=0;
			if(form.ibAadharNumber.getValue()!=null&&!form.ibAadharNumber.getValue().equals("")){
				try {
					aadharCardNumber=Long.parseLong(form.ibAadharNumber.getValue());
				} catch (Exception e) {
					form.showDialogMessage("Aadhar number must be numeric value.");
					return;
				}
				
			}
			
			for(int i=0;i<employeeList.size();i++){
				/**
				 * @author Anil,Date:03-01-2019
				 * If aadhar number is matching with any inactive employee 
				 * then only show alert message but allow user to save data.
				 * For Orion BY Sonu
				 */
				if(employeeList.get(i).getFullname().equalsIgnoreCase(empname)
						&&aadharCardNumber==employeeList.get(i).getAadharNumber()&&empId!=0&&employeeList.get(i).isStatus()==true){
					if(empId!=employeeList.get(i).getCount()){
						form.showDialogMessage("Employee Already Exists");
						form.getTbfullName().setValue("");
					}
				}else if(employeeList.get(i).getFullname().equalsIgnoreCase(empname)&&aadharCardNumber==employeeList.get(i).getAadharNumber()&&employeeList.get(i).isStatus()==true){
					form.showDialogMessage("Employee Already Exists");
					form.getTbfullName().setValue("");
				}
			}
		}
	}
	

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new Employee();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getEmployeeQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new Employee());
		return quer;
	}
	
	public void setModel(Employee entity)
	{
		model=entity;
	}
	
	public static EmployeeForm initalize()
	{
			// EmployeePresenterTable gentableScreen=GWT.create( EmployeePresenterTable.class);
			
			EmployeeForm  form=new  EmployeeForm();
			//
			

			   EmployeePresenterTable gentableSearch=new EmployeePresenterTableProxy();
			    gentableSearch.setView(form);
				gentableSearch.applySelectionModle();
			    EmployeePresenterSearch.staticSuperTable=gentableSearch;
			    EmployeePresenterSearch searchpopup=new EmployeePresenterSearchProxy();
			    form.setSearchpopupscreen(searchpopup);
			    form.addClickEventOnActionAndNavigationMenus();//Ashwini Patil Date:15-06-2023
			 EmployeePresenter  presenter=new  EmployeePresenter  (form,new Employee());
			AppMemory.getAppMemory().stickPnel(form);
			return form;
			
			
		 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.personlayer.Employee")
		 public static class  EmployeePresenterTable extends SuperTable<Employee> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.personlayer.Employee")

			 public static class  EmployeePresenterSearch extends SearchPopUpScreen<Employee>{

				@Override
				public MyQuerry getQuerry() 
				{
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
	 

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		super.onClick(event);
		Console.log("EMPLOYEE ONCLICK");
		if(event.getSource() ==(this.form.getAddRowapprover()))
				{
					ObjectListBox<Employee>approvers=form.approvers;
					if(approvers.getSelectedIndex()!=0)
					{
						String approverName=approvers.getValue();
						form.approverTable.listDataProvider.getList().add(approverName);
					}
				}
		
		if(event.getSource() ==(this.form.getAddRowInPreviousWork()))
			{
				PreviousCompanyHistory history=new PreviousCompanyHistory();
				history.setCompany("");
				history.setDesignation("");
				history.setSalary(0.0);
				this.form.previousWorkExperience.getListDataProvider().getList().add(history);
			}
		
		if(event.getSource() ==(this.form.getAddRowInQualification()))
			{
			EducationalInfo info = new EducationalInfo();
			info.setSchoolName("");
			info.setDegreeObtained("");
			info.setPercentage("");
			this.form.educationalQualification.getDataprovider().getList().add(info);
			}
		/** date 24.7.2018 added by komal for employee full and final popup **/
		if(event.getSource() == employeeFullNFinalPopup.getLblOk()){
			boolean flag = true;
			if(employeeFullNFinalPopup.dbPayrollMonth.getValue()!=null){
				/**
				 * @author Anil
				 * @since 30-04-2020
				 * commented validation 
				 */
//				flag = validateMonth(employeeFullNFinalPopup.dbPayrollMonth.getValue());
			}else{
				form.showDialogMessage("Please enter month.");
				return;
			}
			if(flag){
				model.setFnfMonth(employeeFullNFinalPopup.dbPayrollMonth.getValue());
			if(employeeFullNFinalPopup.getDbShortFall().getValue()!=null){
				model.setShortfall(employeeFullNFinalPopup.getDbShortFall().getValue());
			}
			if(employeeFullNFinalPopup.getDbWaiveOff().getValue()!=null){
				model.setWaivedOff(employeeFullNFinalPopup.getDbWaiveOff().getValue());
			}
			if(employeeFullNFinalPopup.getEmployeeCheckListTable().getDataprovider().getList().size()>0){
				model.setCheckList(employeeFullNFinalPopup.getEmployeeCheckListTable().getDataprovider().getList());
			}
			
			if(employeeFullNFinalPopup.getDbFnFDate().getValue()!=null){
				model.setFnfDate(employeeFullNFinalPopup.getDbFnFDate().getValue());
				form.dbFnFDate.setValue(model.getFnfDate());
			}
			if(!model.isRealeaseForPayment()){
				/**
				 * @author Anil
				 * @since 22-05-2020
				 */
				if(model.getFnfStatus().equals("Initiated")){
					model.setFnfStatus("Inprocess");
				}
				genservice.save(model, new AsyncCallback<ReturnFromServer>() {
	
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Console.log("error :" + caught);
					}
	
					@Override
					public void onSuccess(ReturnFromServer result) {
						// TODO Auto-generated method stub
						employeeFullNFinalPopup.hidePopUp();
						/**
						 * @author Anil
						 * @since 22-05-2020
						 */
						form.tbFnfStatus.setValue(model.getFnfStatus());
						if(form.employeeobj!=null){
							form.employeeobj=model;
						}
						form.showDialogMessage("Data save successfully.");
						
					}
				});
			}else{
				form.showDialogMessage("Full and final settlement is already completed.");
			}
		  }
		}
		if(event.getSource() == employeeFullNFinalPopup.getLblCancel()){
			employeeFullNFinalPopup.hidePopUp();
		}
		if(event.getSource().equals(employeeFullNFinalPopup.lblUpdate)){
			/**
			 * @author Anil ,  Date : 23-04-2019
			 */
			if(employeeFullNFinalPopup.dbFnFDate.getValue()!=null){
				model.setFnfDate(employeeFullNFinalPopup.dbFnFDate.getValue());
			}else{
				form.showDialogMessage("Please select FnF date.");
				return;
			}
			
			boolean flag = true;
			if(employeeFullNFinalPopup.dbPayrollMonth.getValue()!=null){
				/**
				 * @author Anil
				 * @since 30-04-2020
				 * commented validation 
				 */
//				flag = validateMonth(employeeFullNFinalPopup.dbPayrollMonth.getValue());
			}else{
				form.showDialogMessage("Please enter month.");
				return;
			}
			if(flag){
				model.setFnfMonth(employeeFullNFinalPopup.dbPayrollMonth.getValue());
			if(employeeFullNFinalPopup.getDbShortFall().getValue()!=null){
				model.setShortfall(employeeFullNFinalPopup.getDbShortFall().getValue());
			}
			if(employeeFullNFinalPopup.getDbWaiveOff().getValue()!=null){
				model.setWaivedOff(employeeFullNFinalPopup.getDbWaiveOff().getValue());
			}
			if(employeeFullNFinalPopup.getEmployeeCheckListTable().getDataprovider().getList().size()>0){
				model.setCheckList(employeeFullNFinalPopup.getEmployeeCheckListTable().getDataprovider().getList());
			}
			model.setFnfStatus("Completed");
			if(!model.isRealeaseForPayment()){
			payslipAllocationService.saveArrearsData(model, employeeFullNFinalPopup.getDbNoticePeriod().getValue(), employeeFullNFinalPopup.getDbShortFall().getValue(),
					  model.getResignationDate(), model.getLastWorkingDate(),new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							Console.log("error : " + caught);
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							if(result.equalsIgnoreCase("success")){
								model.setRealeaseForPayment(true);
								form.tbFnfStatus.setValue(model.getFnfStatus());
								form.showDialogMessage("Data submitted successfully.");
							}else{
								form.showDialogMessage(result);
							}
							employeeFullNFinalPopup.hidePopUp();
						}
				
					});
		}else{
			form.showDialogMessage("Full and final settlement is already completed.");
		}
	  }
	}
		
		if(event.getSource() == datePopup.getBtnOne()){
			if(datePopup.fromDate.getValue()!=null&&datePopup.toDate.getValue()!=null){
				DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy");
				final String url1=GWT.getModuleBaseURL()+ "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"FORM3A"+"&"+"subtype="+model.getCount()+"&"+"fromDate="+format.format(datePopup.getFromDate().getValue())+"&"+"toDate="+format.format(datePopup.getToDate().getValue());
				Window.open(url1, "test", "enabled");
			}else{
				form.showDialogMessage("Please select from and to date.");
			}
		}
		
		if(event.getSource() == datePopup.getBtnTwo()){
			panel.hide();
		}
		
//		if(event.getSource() ==nominationPopup.getLblOk()){
//			
//		}
//		if(event.getSource() ==nominationPopup.getLblCancel()){
//			nominationPopup.hidePopUp();
//		}
		
		if(event.getSource().equals(employeeFullNFinalPopup.lblPrint)){
			final String url1=GWT.getModuleBaseURL()+ "complianceform"+"?Id="+UserConfiguration.getCompanyId()+"&"+"type="+"FnF"+"&"+"subtype="+model.getCount();
			Window.open(url1, "test", "enabled");
		}
		
		if(empCtcAllocationPopup!=null&&event.getSource().equals(empCtcAllocationPopup.getLblOk())){
			if(empCtcAllocationPopup.validate()){
				if(empCtcAllocationPopup.ctcAllocationStatus){
					form.showDialogMessage("CTC is already allocated to employee.");
					return;
				}
				empCtcAllocationPopup.allocateCtcToEmployee();
			}
		}
		if(empCtcAllocationPopup!=null&&event.getSource().equals(empCtcAllocationPopup.getLblCancel())){
			empCtcAllocationPopup.hidePopUp();		
		}
		if(empCtcAllocationPopup!=null&&event.getSource().equals(empCtcAllocationPopup.lblNew)){
			empCtcAllocationPopup.hidePopUp();	
			ctcTemplatePopup=new CtcTemplatePopupScreen();
			ctcTemplatePopup.getLblOk().addClickHandler(this);
			ctcTemplatePopup.getLblCancel().addClickHandler(this);
			ctcTemplatePopup.showPopUp();
		}
		
		if(ctcTemplatePopup!=null&&event.getSource().equals(ctcTemplatePopup.getLblOk())){
//			ctcTemplatePopup.saveCtcTemplate();
//			ctcTemplatePopup.hidePopUp();
			saveCtcTemplate();
		}
		if(ctcTemplatePopup!=null&&event.getSource().equals(ctcTemplatePopup.getLblCancel())){
			ctcTemplatePopup.hidePopUp();
			setEventHandeling();
		}
		
		//Ashwini Patil Date:15-06-2023
		if(event.getSource()==gvPopup.btnClose){

			
			final EmployeeForm form = new EmployeeForm();
			EmployeePresenterTable gentable = new EmployeePresenterTableProxy();
			gentable.setView(form);
			gentable.applySelectionModle();
			EmployeePresenterSearch.staticSuperTable = gentable;
			EmployeePresenterSearch searchpopup = new EmployeePresenterSearchProxy();
			searchpopup.getSupertable().getListDataProvider().setList(searchedEmpList);
			Console.log("list set to searchpopup size="+searchedEmpList.size());
			form.setSearchpopupscreen(searchpopup);
			form.addClickEventOnActionAndNavigationMenus(); 
			EmployeePresenter presenter = new EmployeePresenter(form,new Employee());
			form.setToNewState();
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Human Resource/Employee",Screen.EMPLOYEE);
			AppMemory.getAppMemory().stickPnel(form);
			
//			------------------------
		
				
				Timer time = new Timer() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						form.updateView(model);
			        	form.setToViewState();
					}
				};
				time.schedule(2000);

		}
	}
	
	public void saveCtcTemplate(){
		ctcTemplatePopup.ctcTempPresenter.validateAndUpdateModel();
		SuperModel model1=ctcTemplatePopup.ctcTempPresenter.getModel();
		async.save(model1, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void onSuccess(ReturnFromServer result) {
				ctcTemplatePopup.hidePopUp();
				
				System.out.println("INSIDE TIMER SHOW EMP CTC ALLOCATION");
				empCtcAllocationPopup=new EmployeeCtcAllocationPopup();
				empCtcAllocationPopup.lblNew.getElement().setId("addbutton");
				empCtcAllocationPopup.getHorizontal().add(empCtcAllocationPopup.lblNew);
				empCtcAllocationPopup.setEmployeeId(model.getCount());
				empCtcAllocationPopup.checkEmpCtcAllocationStatus();
				empCtcAllocationPopup.showPopUp();		
				
				form.showDialogMessage("Data saved successfully.");
				setEventHandeling();
				
//				empCtcAllocationPopup.getLblOk().addClickHandler(this);
//				empCtcAllocationPopup.getLblCancel().addClickHandler(this);
//				empCtcAllocationPopup.lblNew.addClickHandler(this);
				
				addingHandler();
			}
		});
	}
	
	public void addingHandler(){
		empCtcAllocationPopup.getLblOk().addClickHandler(this);
		empCtcAllocationPopup.getLblCancel().addClickHandler(this);
		empCtcAllocationPopup.lblNew.addClickHandler(this);
	}
	
	@Override
	public void reactOnDownload() {
		
		
			ArrayList<Employee> emparray=new ArrayList<Employee>();
			List<Employee> list=(List<Employee>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
			
			emparray.addAll(list);
			
			service.setEmployeelist(emparray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				
				@Override
				public void onSuccess(Void result) {
		
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+40;
					Window.open(url, "test", "enabled");
					
//					final String url=gwt + "CreateXLXSServlet"+"?type="+18;
//					Window.open(url, "test", "enabled");
				}
			});
			
//			
//			/**
//			 * @author Anil , Date : 29-06-2019
//			 * if employee approval process is active then in employee download file also show approval status and approver name
//			 */
//			boolean employeeApprovalProcess=false;
//			boolean isOnlyForOrion=false;
//			
//			boolean siteLocation=false;
////			if (employees.get(0).getCompanyId() != null){
////				ProcessConfiguration processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", employees.get(0).getCompanyId()).filter("processName", "Employee").filter("configStatus", true).first().now();
////				System.out.println("PROCESS CONFIG");
////				if(processConfig!=null){
////					System.out.println("PROCESS CONFIG NOT NULL");
////					for(int k=0;k<processConfig.getProcessList().size();k++)
////					{
////						if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("OnlyForOrion")&&processConfig.getProcessList().get(k).isStatus()==true){
////							isOnlyForOrion = true;
////						}
////						if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("EmployeeApprovalProcess")&&processConfig.getProcessList().get(k).isStatus()==true){
////							employeeApprovalProcess = true;
////						}
////						
////						if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("EnableSiteLocation")&&processConfig.getProcessList().get(k).isStatus()==true){
////							siteLocation = true;
////						}
////					}
////				}
////				
////			}
//			
//			isOnlyForOrion = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "OnlyForOrion");
//			employeeApprovalProcess = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "EmployeeApprovalProcess");
//			siteLocation = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "EnableSiteLocation");
//
//			ArrayList<String> employeeDatalist = new ArrayList<String>();
//
////			employeeDatalist = getEmployeeDetails();
////			
////			ArrayList<String> list = new ArrayList<String>();
//			employeeDatalist.add("");
//			employeeDatalist.add("");
//			employeeDatalist.add("FORM XIII - Employee Details");
//			for(int i=0;i<64;i++) {
//				employeeDatalist.add("");
//			}
//			
//			if(isOnlyForOrion==true||employeeApprovalProcess) {
//				employeeDatalist.add("");
//			}
//			if(employeeApprovalProcess) {
//				employeeDatalist.add("");
//				employeeDatalist.add("");
//			}
//			if(siteLocation) {
//				employeeDatalist.add("");
//			}
//			
//			employeeDatalist.add("COUNTRY");
//			employeeDatalist.add("ID");
//			employeeDatalist.add("NAME");
//
//			
//			if(isOnlyForOrion==true||employeeApprovalProcess) {
//				employeeDatalist.add("FATHER/HUSBAND NAME");
//			}
//			employeeDatalist.add("PHONE");
//			employeeDatalist.add("ROLE");
//			employeeDatalist.add("REPORTS TO");
//			employeeDatalist.add("BRANCH");
//			employeeDatalist.add("DESIGNATION");
//			employeeDatalist.add("DEPARTMENT");
//			employeeDatalist.add("TYPE");
//			employeeDatalist.add("ACCESS CARD NO.");
//			employeeDatalist.add("ID CARD NO.");
//			employeeDatalist.add("BANK NAME");
//			employeeDatalist.add("BRANCH NAME");
//			employeeDatalist.add("IFSC CODE");
//			employeeDatalist.add("BANK ACCOUNT NUMBER");
//			employeeDatalist.add("PREVIOUS ESIC");
//			employeeDatalist.add("ESIC NUMBER");
//			employeeDatalist.add("PAN CARD NUMBER");
//			/**
//			 * Date : 11-09-2018 By ANIL
//			 */
//			employeeDatalist.add("PREVIOUS PF");
//			employeeDatalist.add("PF NUMBER");
//			employeeDatalist.add("AADHAR NUMBER");
//			employeeDatalist.add("GROSS SALARY");
//			employeeDatalist.add("BLOOD GROUP");
//			employeeDatalist.add("DATE OF BIRTH");
//			employeeDatalist.add("DATE OF JOINING");
//			/**
//			 * Updated by: Viraj
//			 * Date: 23-02-2019
//			 * Description: To show f&f details
//			 */
//			employeeDatalist.add("NOTICE PERIOD");
//			employeeDatalist.add("DATE OF RESIGNATION");
//			employeeDatalist.add("LAST DATE OF WORKING");
//			employeeDatalist.add("ABSCOND DATE");
//			employeeDatalist.add("DATE OF TERMINATION");
//			employeeDatalist.add("UAN NO.");
//			employeeDatalist.add("GENDER");
//			employeeDatalist.add("MARITAL STATUS");
//			employeeDatalist.add("NUMBER RANGE");
//			employeeDatalist.add("REFERENCE EMPLOYEE");
//			employeeDatalist.add("PROJECT");
//
//			if(siteLocation){
//				employeeDatalist.add("SITE LOCATION");
//			}
//			employeeDatalist.add("LANDLINE NO.");
//			employeeDatalist.add("CELL NO. 1");
//			employeeDatalist.add("CELL NO. 2");
//			employeeDatalist.add("EMAIL");
//			employeeDatalist.add("PRIMARY ADDRESS LINE 1");
//			employeeDatalist.add("ADDRESS LINE 2");
//			employeeDatalist.add("LANDMARK");
//			employeeDatalist.add("LOCALITY");
//			employeeDatalist.add("CITY");
//			employeeDatalist.add("STATE");
//			employeeDatalist.add("COUNTRY");
//			employeeDatalist.add("PIN NO.");
//			/**
//			 * Date : 18-09-2018 By Anil
//			 */
//			employeeDatalist.add("SECONDARY ADDRESS LINE 1");
//			employeeDatalist.add("ADDRESS LINE 2");
//			employeeDatalist.add("LANDMARK");
//			employeeDatalist.add("LOCALITY");
//			employeeDatalist.add("CITY");
//			employeeDatalist.add("STATE");
//			employeeDatalist.add("COUNTRY");
//			employeeDatalist.add("PIN NO.");
//			employeeDatalist.add("STATUS");
//
//			/**Date 17-5-2019 by Amol added a Employee Type Column**/
//			employeeDatalist.add("EMPLOYEE GROUP");
//
//			/**
//			 * @author Anil , Date : 29-06-2019
//			 * if employee approval process is active then in employee download file also show approval status and approver name
//			 */
//			if(employeeApprovalProcess){
//				employeeDatalist.add("Approval Status");
//				employeeDatalist.add("Approver");
//			}
//			/**
//			 * @author Anil , Date : 12-07-2019
//			 * showing finger registered status
//			 * raised by Baljinder for sasha
//			 */
//			employeeDatalist.add("FINGER REGISTERED(YES/NO)");
//
//			/**
//			 * @author Anil , Date : 18-07-2019
//			 * showing created by , creation date,update by and updat date
//			 * raised by Baljinder for sasha
//			 */
//			employeeDatalist.add("CREATED BY");
//			employeeDatalist.add("CREATION DATE");
//			employeeDatalist.add("UPDATE BY");
//			employeeDatalist.add("UPDATE DATE");
//			employeeDatalist.add("FNF STATUS");
//			employeeDatalist.add("FNF DATE");
//			employeeDatalist.add("CASTE");
//
//			int columnCount = 67;
//			if(isOnlyForOrion==true) {
//				columnCount++;
//			}
//			if(employeeApprovalProcess) {
//				columnCount = columnCount +3;
//			}
//			if(siteLocation==true) {
//				columnCount++;
//			}
//			
//			for(Employee emp:emparray)
//			{
//				employeeDatalist.add(emp.getCountry()+"");
//				employeeDatalist.add(emp.getCount()+"");
//				employeeDatalist.add(emp.getFullName());
//				
//				if(isOnlyForOrion==true||employeeApprovalProcess==true){ 
//					employeeDatalist.add(emp.getHusbandName());
//				}
//
//				if(emp.getCellNumber1()!=null) {
//					employeeDatalist.add(emp.getCellNumber1()+"");
//				}
//				else {
//					employeeDatalist.add("N.A");
//				}
//
//				
//				if(emp.getRoleName()!=null)
//					employeeDatalist.add(emp.getRoleName());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getReportsTo()!=null)
//					employeeDatalist.add(emp.getReportsTo());
//				else
//					employeeDatalist.add("N.A");
//
//				if(emp.getBranchName()!=null)
//					employeeDatalist.add(emp.getBranchName());
//				else
//					employeeDatalist.add("N.A");
//			
//				if(emp.getDesignation()!=null)
//					employeeDatalist.add(emp.getDesignation());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getDepartMent()!=null)
//					employeeDatalist.add(emp.getDepartMent());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getEmployeeType()!=null)
//					employeeDatalist.add(emp.getEmployeeType());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getAccessCardNo()!=null)
//					employeeDatalist.add(emp.getAccessCardNo());
//				else
//					employeeDatalist.add("N.A");
//
//				if(emp.getIdCardNo()!=null)
//					employeeDatalist.add(emp.getIdCardNo());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getEmployeeBankName()!=null)
//					employeeDatalist.add(emp.getEmployeeBankName());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getBankBranch()!=null)
//					employeeDatalist.add(emp.getBankBranch());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getIfscCode()!=null)
//					employeeDatalist.add(emp.getIfscCode());
//				else
//					employeeDatalist.add("N.A");
//				/**
//				 * @author Anil,Date : 18-02-2019
//				 * Issue : 0 was not printed in downloaded file
//				 * For orion raised by Rahul tiwari
//				 */
//				if(emp.getEmployeeBankAccountNo()!=null)
//					employeeDatalist.add(emp.getEmployeeBankAccountNo());
//				else
//					employeeDatalist.add("N.A");
//				/**
//				 * Date : 11-09-2018 By ANIL
//				 */
//				if(emp.getEmployeeESICcode()!=null&&!emp.getEmployeeESICcode().equals("")){
//					if(emp.isPreviousEsicNumber()==true){
//						employeeDatalist.add("YES");
//					}else{
//						employeeDatalist.add("NO");
//					}
//					employeeDatalist.add(emp.getEmployeeESICcode());
//				}else{
//					employeeDatalist.add("N.A");
//					employeeDatalist.add("N.A");
//				}
//				/**
//				 * End
//				 */
//				
//				if(emp.getEmployeePanNo()!=null)
//					employeeDatalist.add(emp.getEmployeePanNo());
//				else
//					employeeDatalist.add("N.A");
//				/**
//				 * Date : 11-09-2018 By ANIL
//				 * 
//				 */
//				
//				if(emp.getPPFNaumber()!=null&&!emp.getPPFNaumber().equals("")){
//					if(emp.isPreviousPfNumber()==true){
//						employeeDatalist.add("YES");
//					}else{
//						employeeDatalist.add("NO");
//					}
//					employeeDatalist.add(emp.getPPFNaumber());
//				}else{
//					employeeDatalist.add("N.A");
//					employeeDatalist.add("N.A");
//				}
//				/**
//				 * Date : 25-08-2018 By ANIL
//				 */
//				if(emp.getAadharNumber()!=0)
//					employeeDatalist.add(emp.getAadharNumber()+"");
//				else
//					employeeDatalist.add("");
//				
//				if(emp.getSalaryAmt()!=0)
//					employeeDatalist.add(emp.getSalaryAmt()+"");
//				else
//					employeeDatalist.add("");
//				/**
//				 * End
//				 */
//				
//				if(emp.getBloodGroup()!=null)
//					employeeDatalist.add(emp.getBloodGroup());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getDob()!=null)
//					employeeDatalist.add(AppUtility.parseDate(emp.getDob(),"dd-MM-yyyy"));
//				else
//					employeeDatalist.add("N.A");
//
//				if(emp.getJoinedAt()!=null)
//					employeeDatalist.add(AppUtility.parseDate(emp.getJoinedAt(),"dd-MM-yyyy"));
//				else
//					employeeDatalist.add("N.A");
//				/**
//				 * Updated By: Viraj
//				 * Date: 23-02-2019
//				 * Description: To show f&f details
//				 */
//				if(emp.getNoticePeriod() != 0) {
//					employeeDatalist.add(emp.getNoticePeriod()+"");
//				} else {
//					employeeDatalist.add("");
//				}
//				if(emp.getResignationDate() != null) {
//					employeeDatalist.add(AppUtility.parseDate(emp.getResignationDate(),"dd-MM-yyyy"));
//				} else {
//					employeeDatalist.add("");
//				}
//				if(emp.getLastWorkingDate() != null) {
//					employeeDatalist.add(AppUtility.parseDate(emp.getLastWorkingDate(),"dd-MM-yyyy"));
//				} else {
//					employeeDatalist.add("");
//				}
//				if(emp.getAbscondDate() != null) {
//					employeeDatalist.add(AppUtility.parseDate(emp.getAbscondDate(),"dd-MM-yyyy"));
//				} else {
//					employeeDatalist.add("");
//				}
//				if(emp.getTerminationDate() != null) {
//					employeeDatalist.add(AppUtility.parseDate(emp.getTerminationDate(),"dd-MM-yyyy"));
//				} else {
//					employeeDatalist.add("");
//				}
//				/** Ends **/
//				
//				if(emp.getUANno()!=null)
//					employeeDatalist.add(emp.getUANno());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getGender()!=null)
//					employeeDatalist.add(emp.getGender());
//				else
//					employeeDatalist.add("N.A");
//
//				if(emp.getMaritalStatus()!=null)
//					employeeDatalist.add(emp.getMaritalStatus());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getNumberRange()!=null)
//					employeeDatalist.add(emp.getNumberRange());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getRefEmployee()!=null)
//					employeeDatalist.add(emp.getRefEmployee());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getProjectName()!=null)
//					employeeDatalist.add(emp.getProjectName());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(siteLocation){
//					if(emp.getSiteLocation()!=null)
//						employeeDatalist.add(emp.getSiteLocation());
//					else
//						employeeDatalist.add("N.A");
//				}
//				
//				if(emp.getLandline()!=null)
//					employeeDatalist.add(emp.getLandline()+"");
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getCellNumber1()!=null)
//					employeeDatalist.add(emp.getCellNumber1()+"");
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getCellNumber2()!=null)
//					employeeDatalist.add(emp.getCellNumber2()+"");
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getEmail()!=null)
//					employeeDatalist.add(emp.getEmail());
//				else
//					employeeDatalist.add("N.A");
//				if(emp.getAddress().getAddrLine1()!=null)
//					employeeDatalist.add(emp.getAddress().getAddrLine1());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getAddress().getAddrLine2()!=null)
//					employeeDatalist.add(emp.getAddress().getAddrLine2());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getAddress().getLandmark()!=null)
//					employeeDatalist.add(emp.getAddress().getLandmark());
//				else
//					employeeDatalist.add("N.A");
//
//				if(emp.getAddress().getLocality()!=null)
//					employeeDatalist.add(emp.getAddress().getLocality());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getAddress().getCity()!=null)
//					employeeDatalist.add(emp.getAddress().getCity());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getAddress().getState()!=null)
//					employeeDatalist.add(emp.getAddress().getState());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getAddress().getCountry()!=null)
//					employeeDatalist.add(emp.getAddress().getCountry());
//				else
//					employeeDatalist.add("N.A");
//				
//				employeeDatalist.add(emp.getAddress().getPin()+"");
//				/**
//				 * Date : 18-09-2018 BY ANIL
//				 */
//				if(emp.getSecondaryAddress()!=null&&emp.getSecondaryAddress().getAddrLine1()!=null)
//					employeeDatalist.add(emp.getSecondaryAddress().getAddrLine1());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getSecondaryAddress()!=null&&emp.getSecondaryAddress().getAddrLine2()!=null)
//					employeeDatalist.add(emp.getSecondaryAddress().getAddrLine2());
//				else
//					employeeDatalist.add("N.A");
//
//				if(emp.getSecondaryAddress()!=null&&emp.getSecondaryAddress().getLandmark()!=null)
//					employeeDatalist.add(emp.getSecondaryAddress().getLandmark());
//				else
//					employeeDatalist.add("N.A");
//
//				if(emp.getSecondaryAddress()!=null&&emp.getSecondaryAddress().getLocality()!=null)
//					employeeDatalist.add(emp.getSecondaryAddress().getLocality());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getSecondaryAddress()!=null&&emp.getSecondaryAddress().getCity()!=null)
//					employeeDatalist.add(emp.getSecondaryAddress().getCity());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getSecondaryAddress()!=null&&emp.getSecondaryAddress().getState()!=null)
//					employeeDatalist.add(emp.getSecondaryAddress().getState());
//				else
//					employeeDatalist.add("N.A");
//				
//				if(emp.getSecondaryAddress()!=null&&emp.getSecondaryAddress().getCountry()!=null)
//					employeeDatalist.add(emp.getSecondaryAddress().getCountry());
//				else
//					employeeDatalist.add("N.A");
//				
//				employeeDatalist.add(emp.getSecondaryAddress().getPin()+"");
//
//				
//				if(emp.isStatus()==true)
//					employeeDatalist.add("Active");
//				else
//					employeeDatalist.add("InActive");
//				
//				/**Date 17-5-2019 by Amol**/
//				if(emp.getEmpGrp()!=null){
//					employeeDatalist.add(emp.getEmpGrp());
//				}else{
//					employeeDatalist.add("N.A");
//				}
//				
//				if(employeeApprovalProcess){
//					if(emp.getApproveStatus()!=null){
//						employeeDatalist.add(emp.getApproveStatus());
//					}else{
//						employeeDatalist.add("");
//					}
//					if(emp.getApproverName()!=null){
//						employeeDatalist.add(emp.getApproverName());
//					}else{
//						employeeDatalist.add("");
//					}
//				}
//				
//				/**
//				 * @author Anil , Date : 12-07-2019
//				 */
//				if(emp.isFingerDataPresent()){
//					employeeDatalist.add("YES");
//				}else{
//					employeeDatalist.add("NO");
//				}
//				
//				if(emp.getCreatedBy()!=null){
//					employeeDatalist.add(emp.getCreatedBy());
//				}else{
//					employeeDatalist.add("");
//				}
//				if(emp.getCreationDate()!=null){
//					employeeDatalist.add(AppUtility.parseDate(emp.getCreationDate(),"dd-MMM-yyyy"));
//				}else{
//					employeeDatalist.add("");
//				}
//				
//				if(emp.getUpdatedBy()!=null){
//					employeeDatalist.add(emp.getUpdatedBy());
//				}else{
//					employeeDatalist.add("");
//				}
//				
//				if(emp.getUpdateDate()!=null){
//					employeeDatalist.add(AppUtility.parseDate(emp.getUpdateDate(),"dd-MMM-yyyy"));
//				}else{
//					employeeDatalist.add("");
//				}
//				if(emp.getFnfStatus()!=null){
//					employeeDatalist.add(emp.getFnfStatus());
//				}else{
//					employeeDatalist.add("N.A");
//				}
//				if(emp.getFnfDate()!=null){
//					employeeDatalist.add(AppUtility.parseDate(emp.getFnfDate(),"dd-MM-yyyy"));
//				}else{
//					employeeDatalist.add("N.A");
//				}
//				
//				if(emp.getCaste()!=null){
//					employeeDatalist.add(emp.getCaste());
//				}else{
//					employeeDatalist.add("N.A");
//				}
//				
//			}
//		
//			service.setExcelData(employeeDatalist, columnCount, AppConstants.EMPLOYEE, new AsyncCallback<String>() {
//				
//				@Override
//				public void onSuccess(String result) {
//					// TODO Auto-generated method stub
//					
//					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//					final String url=gwt + "CreateXLXSServlet"+"?type="+18;
//					Window.open(url, "test", "enabled");
//				}
//				
//				@Override
//				public void onFailure(Throwable caught) {
//					// TODO Auto-generated method stub
//					
//				}
//			});
	}


	public void reactOnApprovalEmail(Approvals approvalEntity)
	{
		emailService.initiateApprovalEmail(approvalEntity,new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Unable To Send Email");
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(Void result) {
//				Window.alert("Email Sent Sucessfully !");
				
			}
		});
	}
	private boolean validateFnFDetails(){
		if(model.getNoticePeriod() == 0){
			form.showDialogMessage("Please enter notice period.");
			return false;
		}
		if(model.getResignationDate()==null){
			form.showDialogMessage("Please enter resignation date.");
			return false;
		}
		if(model.getLastWorkingDate()==null){
			form.showDialogMessage("Please enter last date at organization.");
			return false;
		}
			return true;
		}
	/** date 3.8.2018 added by komal for employee checklist **/
	private void setCheckList(){
		try {
			if(model.getCheckListType()!=null){
				Console.log("STEP 6");
				CheckListType type = model.getCheckListType();
				List<StepsDetails> stepList = type.getStepsInfo();
				ArrayList<EmployeeAssetBean> list = new ArrayList<EmployeeAssetBean>();
				for (StepsDetails s : stepList) {
					EmployeeAssetBean bean = new EmployeeAssetBean();
					bean.setAssetName(s.getStepDetailsName());
					list.add(bean);
				}
				employeeFullNFinalPopup.getEmployeeCheckListTable().getDataprovider().getList().addAll(list);
			}
		}catch(Exception e) {
			
		}
		Console.log("STEP 7");
		MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("empId");
		filter.setIntValue(model.getCount());
		temp.add(filter);
			
		querry.setFilters(temp);
		querry.setQuerryObject(new EmployeeAsset());
		form.showWaitSymbol();
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				Console.log("STEP 7 "+result.size());
				if(result.size()==0){
					//form.showDialogMessage("No records found.");
					return;
				}
				if(result.size()>0){
					Console.log("STEP 8 "+result.size());
					EmployeeAsset employeeAsset=(EmployeeAsset) result.get(0);
					ArrayList<EmployeeAssetBean> list = employeeAsset.getEmpAssetList();
					employeeFullNFinalPopup.getEmployeeCheckListTable().getDataprovider().getList().addAll(list);
				}
			}
		});
	}
	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if (event.getSource().equals(form.olbEmploymentType)) {
			MyQuerry querry = new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter = new Filter();
			filter.setQuerryString("employeeType");
			filter.setStringValue(form.olbEmploymentType.getValue());
			temp.add(filter);
			querry.setFilters(temp);
			querry.setQuerryObject(new EmployeeCheckListType());
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					Console.log("result : "+ result.size());
					if(result.size()>0){
						for(SuperModel s : result){
							EmployeeCheckListType type = (EmployeeCheckListType) s;
							model.setCheckListType(type.getCheckListType());
							Console.log("list type : "+ type.getCheckListType().getCheckListName());
						}
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					Console.log("error :" + caught);
				}
			});

		}
		
		if(event.getSource().equals(employeeFullNFinalPopup.getDbWaiveOff())){
			double shortFall = 0;
			if(employeeFullNFinalPopup.getDbShortFall().getValue()!=null){
				shortFall = employeeFullNFinalPopup.getDbShortFall().getValue();
				double diff = shortFall - employeeFullNFinalPopup.getDbWaiveOff().getValue();
				if(diff >= 0)
					employeeFullNFinalPopup.getDbShortFall().setValue(diff);
			}else{
				form.showDialogMessage("Please enter resignation date and last working date first.");
			}
			
		}
		if(event.getSource().equals(form.dbnoticePeriod)){
			if(form.dbResignationDate.getValue()!=null && !(form.dbResignationDate.getValue().equals(""))){
				double noticePeriod = 0;
				int period = 0;
				if(form.dbnoticePeriod.getValue()!=null){
					noticePeriod = form.dbnoticePeriod.getValue();
					period = (int) noticePeriod;
				}
				Date date = form.dbResignationDate.getValue();
				CalendarUtil.addDaysToDate(date, period-1);
				form.dbLastWorkingDay.setValue(date);
			}
		}
		if (AppMemory.getAppMemory().currentState.equals(ScreeenState.NEW) || AppMemory.getAppMemory().currentState.equals(ScreeenState.EDIT)) {
			
			if (event.getSource().equals(form.getTbfullName())) {
				validateEmployeeName();
			}
			
			if(event.getSource().equals(form.ibAadharNumber)){
				if(isValidAadharNumber()){
					validateEmployeeName();
				}
			}
		}
}
	private boolean validateMonth(Date date){
		DateTimeFormat format=DateTimeFormat.getFormat("M");
		DateTimeFormat yearFormat=DateTimeFormat.getFormat("yyyy");
		int month = Integer.parseInt(format.format(date));
		int currentMonth = Integer.parseInt(format.format(new Date()));
		int year = Integer.parseInt(yearFormat.format(date));
		int currentYear = Integer.parseInt(yearFormat.format(new Date()));
		Console.log("current :" + currentMonth +" " + "entered month :"+ month);
		if(month >= currentMonth && currentYear == year){
			return true;
		}else if(month <= currentMonth && currentYear!=year){
			return true;
		}else{
			form.showDialogMessage("Month should be greater than equal to current month.");
			return false;
		}
		
	}
	
	/**
	 * @author Anil 
	 * Date : 30-10-2018
	 * Printing employment card
	 * 
	 */
	public void reactOnEmploymentCardPrint() {
		final String url = GWT.getModuleBaseURL() + "employee" + "?Id="+model.getId()+"&"+"type="+"employmentCard"+"&"+"subtype="+"employmentCard";
		Window.open(url, "test", "enable");
	}
}

