package com.slicktechnologies.client.views.settings.employee;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.cnc.StaffingDetails;
import com.slicktechnologies.shared.common.personlayer.EmployeeCTCTemplate;

public class EmployeeCTCTemplateTable extends SuperTable<EmployeeCTCTemplate> {

	TextColumn<EmployeeCTCTemplate> viewCTCName, viewCTCStartDate,
			viewCTCEndDate, viewDeleteButton, viewCTCAmt;
	Column<EmployeeCTCTemplate, String> deleteButton;
	
	/**
	 * Date :14-09-2018 By ANIL
	 */
	boolean isAdmin=false;
	
	public EmployeeCTCTemplateTable() {
		// TODO Auto-generated constructor stub
		super();
		table.setHeight("300px");
	}

	public EmployeeCTCTemplateTable(boolean isAdmin) {
		super(isAdmin);
		this.isAdmin=isAdmin;
		createTable(isAdmin);
		
		// TODO Auto-generated constructor stub
	}
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addCTCName();
		addCTCAmount();
		addCTCStartDate();
		addCTCEndDate();
		addDelete();
		setFieldUpdaterOnDelete();
	}

	private void createTable(boolean isAdmin2) {
		// TODO Auto-generated method stub
		addCTCName();
		addCTCAmount();
		addCTCStartDate();
		addCTCEndDate();
		if(isAdmin2){
			addDelete();
			setFieldUpdaterOnDelete();
		}
	}

	private void addDelete() {
		// TODO Auto-generated method stub
		ButtonCell btnCell = new ButtonCell();
		deleteButton = new Column<EmployeeCTCTemplate, String>(btnCell) {

			@Override
			public String getValue(EmployeeCTCTemplate object) {

				return "Delete";
			}
		};
		table.addColumn(deleteButton, "");
		table.setColumnWidth(deleteButton, 50, Unit.PX);
	}

	private void addCTCEndDate() {
		// TODO Auto-generated method stub
		viewCTCEndDate = new TextColumn<EmployeeCTCTemplate>() {

			@Override
			public String getValue(EmployeeCTCTemplate object) {
				// TODO Auto-generated method stub
				String date;
				if (object.getEndDate() != null)
					date = AppUtility.parseDate(object.getEndDate());
				else
					date = "";

				return date;
			}
		};

		table.addColumn(viewCTCEndDate, "End Date");
		table.setColumnWidth(viewCTCEndDate, 120, Unit.PX);
	}

	private void addCTCStartDate() {
		// TODO Auto-generated method stub
		viewCTCStartDate = new TextColumn<EmployeeCTCTemplate>() {

			@Override
			public String getValue(EmployeeCTCTemplate object) {
				// TODO Auto-generated method stub
				String date;
				if (object.getEndDate() != null)
					date = AppUtility.parseDate(object.getStartDate());
				else
					date = "";

				return date;
			}
		};

		table.addColumn(viewCTCStartDate, "Start Date");
		table.setColumnWidth(viewCTCStartDate, 120, Unit.PX);
	}

	private void addCTCName() {
		// TODO Auto-generated method stub
		viewCTCName = new TextColumn<EmployeeCTCTemplate>() {

			@Override
			public String getValue(EmployeeCTCTemplate object) {
				// TODO Auto-generated method stub
				return object.getCtcTemplateName() + "";
			}
		};

		table.addColumn(viewCTCName, "Component Name");
		table.setColumnWidth(viewCTCName, 120, Unit.PX);
	}

	public EmployeeCTCTemplateTable(UiScreen<EmployeeCTCTemplate> mv) {
		super(mv);
		// TODO Auto-generated constructor stub
	}

	

	private void setFieldUpdaterOnDelete() {
		// TODO Auto-generated method stub
		deleteButton.setFieldUpdater(new FieldUpdater<EmployeeCTCTemplate, String>() {

				@Override
				public void update(int index, EmployeeCTCTemplate object,String value) {
					getDataprovider().getList().remove(index);
					table.redraw();
				}
			});
	}

	private void addCTCAmount() {
		// TODO Auto-generated method stub
		viewCTCAmt = new TextColumn<EmployeeCTCTemplate>() {

			@Override
			public String getValue(EmployeeCTCTemplate object) {
				// TODO Auto-generated method stub
				return object.getAmount() + "";
			}
		};

		table.addColumn(viewCTCAmt, "Amount");
		table.setColumnWidth(viewCTCAmt, 120, Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();
	}

	private void addViewColumn() {
		// TODO Auto-generated method stub
		addCTCName();
		addCTCAmount();
		addCTCStartDate();
		addCTCEndDate();
		addViewDeleteButton();
	}

	private void addViewDeleteButton() {
		// TODO Auto-generated method stub
		viewDeleteButton = new TextColumn<EmployeeCTCTemplate>() {

			@Override
			public String getValue(EmployeeCTCTemplate object) {
				// TODO Auto-generated method stub
				return "";
			}
		};
		table.addColumn(viewDeleteButton, "");
		table.setColumnWidth(viewDeleteButton, 120, Unit.PX);
	}

	private void addeditColumn() {
		// TODO Auto-generated method stub
		addCTCName();
		addCTCAmount();
		addCTCStartDate();
		addCTCEndDate();
//		if(isAdmin){
		addDelete();
		setFieldUpdaterOnDelete();
//		}
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub

	}

}
