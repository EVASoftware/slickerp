package com.slicktechnologies.client.views.settings.employee;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.SocialInfoComposites;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.PassportComosite;
import com.slicktechnologies.client.views.composites.articletypecomposite.ArticleTypeComposite;
import com.slicktechnologies.client.views.history.HistoryPopup;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.EmployeeCTCTemplate;
import com.slicktechnologies.shared.common.personlayer.EmployeeProjectHistory;
import com.slicktechnologies.shared.common.personlayer.PromotionDetails;
import com.slicktechnologies.shared.common.personlayer.TrainingDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
/**
 * FormTablescreen template.
 */
public class EmployeeForm extends FormScreen<Employee>  implements ClickHandler, ValueChangeHandler<Long>, ChangeHandler {

	//Token to add the varialble declarations
	DateBox dbDateOfBirth,dbJoiningDate;
	ObjectListBox<Branch> lbBranch;
	EmailTextBox etbEmail;
	ObjectListBox<Config> lbDesignation,lbRole,olbEmploymentType;
	ObjectListBox<Department>olbDepartMent;
	AddressComposite addressComposite;
	AddressComposite secondoryAddressComposite;
	
	CheckBox cbStatus;
	SocialInfoComposites socialInfoComposite;

	UploadComposite uploadComposite;//1
	PhoneNumberBox pnbLandlinePhone,pnbCellPhoneNo2;
	MyLongBox pnbCellPhoneNo1;
	ObjectListBox<Employee>reportsTo,approvers;
	ApproverTable approverTable;
	EducationalQualificationTable educationalQualification;
	PreviousWorkExperienceTable previousWorkExperience;
	EmployeeProjectHistoryTable employeeProjectHistoryTable;
	PassportComosite passPortInformation;
	TextBox idCardNumber,accessCardNo,ppfNumber,ibbankAccountNo,ibESICcode,ibPanNo,tbbankName,tbUANno ;
	Button addRowInQualification;
	Button addRowInPreviousWork;
	Button addRowapprover;
	TextBox employeeId;
	ArticleTypeComposite tbArticleTypeInfoComposite;
	
	EmployeeBranchTable empBranchTable;
	
	TextBox bankbranchName,iifcCode;
	
	//*************anil **********881
	UploadComposite uploadAddressDoc;//2
	UploadComposite uploadEducationalDoc;//3
	UploadComposite uploadExperienceDoc;//4
	

	ObjectListBox<Branch> branchLis;
	Button addBranchButton;
	ObjectListBox<Country> olbcountry;
	TextBox bloodGroup;
	
	/**
	 * rohan added this for NBHC employee approve process
	 * date : 20/10/2016
	 */
	DoubleBox salary;
	boolean empApprovalConfigFlag = false;
	ObjectListBox<Employee> approverName;
	TextBox approvedStatus;
	DoubleBox monthlyWorkingHrs;
	
	/**
	 * 
	 */
	Employee employeeobj;
	
	//DoubleBox dbSalary;


	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;


	/* Date :- 12 sep 2016
	 * Release :- 30 sep 2016
	 * above old code commented by vijay to change employee first middle and last to fullName and bellow code added
	 */
//	TextBox tbFirstName,tbMiddleName,tbLastName;
	TextBox tbfullName;
	
	TextBox tbHusbandName;		// Added by Viraj Date 11-10-2018 for orion
	
	/**
	 * Date : 04-05-2018 By ANIL
	 * adding static gender list,which used at payroll for calculating PT 
	 */
	ListBox lbGender;
	
	/**
	 * Date : 14-05-2018 By ANIL
	 * Added number range process for Sasha ERP
	 */
	ObjectListBox<Config> olbcNumberRange;
	
	/**
	 * Date : 04-05-2018 By ANIL
	 * adding static gender list,which used at payroll for calculating PT 
	 */
	ListBox lbMaritialStatus;
	
	ObjectListBox<Employee> olbRefEmployee;
	
	TextBox tbFirstName,tbMiddleName,tbLastName;
	/**
	 * Rahul added on 16 July 2018
	 * CTCComponents drop down
	 * Only exclude from payroll will be loaded
	 */
	ObjectListBox<CtcComponent> olbCTCComponents;
	DoubleBox dbCTCAmtValue;
	DateBox dbFromDateForCTC,dbToDateForCTC;
	Button btAddCtcValue;
	EmployeeCTCTemplateTable empCTCTemplateTable;
	/**
	 * Ends
	 */
	
	
	/**
	 * Date : 20-07-2018 BY ANIL
	 */
	Button btnCopyPrimaryAdd;
	Button btnCopySecondaryAdd;
	
	ObjectListBox<HrProject> project;
	
	/**
	 * Date : 25-07-2018 By  ANIL
	 */
	DateBox dbLastWorkingDay;
	/** date 18.7.2018 added by komal to store employee resignation information **/
	CheckBox cbResigned;
	DateBox dbResignationDate;
	//DateBox dbLastDateAtOrganization;
	DoubleBox dbnoticePeriod;
	CheckBox cbAbsconded;
	DateBox dbAbscondDate;
	CheckBox cbTerminated;
	DateBox dbTerminationDate;
	CheckBox cbReleaseForPayment;
	/** date 11.8.2018 added by komal for one time deduction flag and amount **/
	CheckBox cbOneTimeDeduction;
	DoubleBox dbOneTimeDedectionAmount;
	
	/**
	 * Date : 13-08-2018 BY ANIL
	 */
	Button btnAddProject;
	Button btnAddTrainingDetails;
	TrainingDetailsTable trainingTbl;
	ObjectListBox<Config> olbTrainingType;
	TextBox tbTrainingDescription;
	DateBox trainingDate;
	
	/**
	 * Date : 14-08-2018 By ANIL
	 */
	ObjectListBox<Shift> oblShiftName;	
	ObjectListBox<Employee> oblInterviewedBy;
	ObjectListBox<Employee> oblSalAuthBy;
	
	TextBox tbNomineeName;
	TextBox tbNomineeRelation;
	IntegerBox ibNomineeAge;
	TextBox tbNomineeAddress;
	
	TextBox tbHappayCardUserId;
	TextBox tbHappayCardUserName;
	TextBox tbHappayCardWalletNumber;
	
	CheckBox cbIsFingerRegistered;
	TextBox tbFingerRegisteredBy;
	DateBox dbFingerRegisteredDate;
	
	/**
	 * Date : 20-08-2018 By ANIL
	 * As discussed with nitin sir ,added aadhar number field which is also used for validation
	 */
	TextBox ibAadharNumber;
	
	/**
	 * Date : 11-09-2018 By  ANIL
	 */
	CheckBox cbPreviousPfNumber;
	CheckBox cbPreviousEsicNumber;
	
	/**
	 * Date : 18-09-2018 BY ANIL
	 */
	
	CheckBox cbSkilled;
	CheckBox cbSemiSkilled;
	CheckBox cbUnskilled;
	TextBox tbLanguageSpoken;
	
	/**
	 * Date : 11-10-2018 BY  ANIL
	 */
	UploadComposite uploadEmpSignature;//5
	TextBox tbAgeAsOnDate;
	
	HorizontalPanel panel;
	
	/**
	 * Date : 19-10-2018 BY ANIL
	 */
	PromotionHistoryTable promotionTbl;
	Button btnAddPromotion;
	/**
	 * Date : 24-10-2018 By ANIL
	 * Added date of retirement
	 */
	DateBox dbDateOfRetirement;
	
	/**
	 * Date : 10-11-2018 By ANIL
	 * Added option to upload compliance forms and other documents
	 */
	UploadComposite uploadComposite1;//6
	UploadComposite uploadComposite2;//7
	
	/**
	 * Date : 14-11-2018 By ANIL
	 */
	CheckBox cbIsDisabled;
	DoubleBox dbDisabilityPercentage;
	UploadComposite uploadDisablityDoc;
	TextArea taDisabilityRemark;
	
	/**
	 * @author Anil ,Date : 07-01-2019
	 * Adding scheme certificate number and pension payment order number for printing 
	 * on Form 11 PF declaration Form 
	 * By Nitin Sir
	 */
	TextBox tbSchemeCertificateNumber;
	TextBox tbPensionPaymentNumber;
	
	/**
	 * @author Anil ,Date: 01-03-2018
	 * On field attendance-for outstation employee
	 */
	CheckBox cbOutstationEmp;
	
	/**
	 * @author Anil ,Date : 10-04-2019
	 */
	ListBox lbEmpGrp;
	
	/**
	 * @author Anil , Date : 29-04-2019
	 * Added reason for leaving
	 */
	TextBox tbReasonForLeaving;
	
	/**
	 * @author Anil , Date :24-05-2019
	 * FnF status
	 */
	TextBox tbFnfStatus;
	
	/**
	 * @author Anil , Date : 18-07-2019
	 * Adding created by ,creation date,updated by and update date
	 */
	TextBox tbCreatedBy;
	TextBox tbUpdatedBy;
	
	DateBox dbCreationDate;
	DateBox dbUpdationDate;
	
	/**Date 20-8-2019 by Amol added a fnfdate**/
	DateBox dbFnFDate;
	/** Date 1-10-2019 by Amol**/
	ObjectListBox<Config> olbCaste;
	
	/**
	 * @author Anil , Date : 05-10-2019
	 */
	CheckBox cbIsReliever;
	
	
	UploadComposite policeVerfDocUplCom;
	DateBox dbExpiryDate;
	
	/**
	 * @author Anil
	 * @since 02-09-2020
	 * Enable site location
	 */
	ObjectListBox<CustomerBranchDetails> oblSiteLocationList;
	boolean siteLocation;
	
	DoubleBox incentivePercent;//Ashwini Patil Date:23-03-2023
	ObjectListBox<Overtime> oblOtList;//Ashwini Patil Date:25-08-2023
	ObjectListBox<Calendar> olbcalender;//Ashwini Patil Date:25-08-2023
	ObjectListBox<LeaveGroup> olbleavegroup;//Ashwini Patil Date:25-08-2023
	Label seperatorAddr1,seperatorAddr2,seperatorProjectHistory,seperatorPassport,seperatorSocial,noteCTCAllocation,seperatorHappyCard;
	
	
	public EmployeeForm  () {
		super();//FormStyle.DEFAULT //Ashwini Patil Date:24-08-2023
		createGui();
		tbArticleTypeInfoComposite.setForm(this);
		tbAgeAsOnDate.setEnabled(false);
		cbIsDisabled.setValue(false);
		dbDisabilityPercentage.setEnabled(false);
		uploadDisablityDoc.setEnable(false);
		cbReleaseForPayment.setEnabled(false);
		
		/**
		 * @author Anil,Date : 07-02-2019
		 * disabled the joining date for sasha erp
		 * @author Anil , Date : 09-07-2019
		 * Disabling joining date for admin too
		 */
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","DisableJoiningDate")
//				&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","DisableJoiningDate")){
			dbJoiningDate.setEnabled(false);
		}
		
		/**
		 * @author Anil ,Date : 01-03-2019
		 * Making finger registration fields read only
		 */
		cbIsFingerRegistered.setEnabled(false);
		tbFingerRegisteredBy.setEnabled(false);
		dbFingerRegisteredDate.setEnabled(false);
		cbOutstationEmp.setValue(false);
		
		tbFnfStatus.setEnabled(false);
		dbFnFDate.setEnabled(false);
		tbCreatedBy.setEnabled(false);
		tbUpdatedBy.setEnabled(false);
		dbCreationDate.setEnabled(false);
		dbUpdationDate.setEnabled(false);
		
		cbIsReliever.setValue(false);
		
		siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","EnableSiteLocation");
		
		
	}

	private void initalizeWidget()
	{
		idCardNumber=new TextBox();
		accessCardNo=new TextBox();
		addRowInPreviousWork=new Button("Add");
		addRowInQualification=new Button("Add");

		passPortInformation=new PassportComosite();
		dbDateOfBirth=new DateBoxWithYearSelector();

		dbJoiningDate=new DateBoxWithYearSelector();
		
		/**
		 * @author Anil,Date : 07-02-2019
		 * disabled the joining date for sasha erp
		 * @author Anil , Date : 09-07-2019
		 * Disabling joining date for admin too
		 */
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","DisableJoiningDate")
//				&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","DisableJoiningDate")){
			dbJoiningDate.setValue(new Date());
			dbJoiningDate.setEnabled(false);
		}

		lbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(lbBranch);

		etbEmail=new EmailTextBox();

		lbDesignation=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(lbDesignation,Screen.EMPLOYEEDESIGNATION);

		lbRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(lbRole,Screen.EMPLOYEEROLE);

		addressComposite=new AddressComposite();
		secondoryAddressComposite=new AddressComposite();
		//**************anil **********
	    uploadAddressDoc=new UploadComposite();
		uploadEducationalDoc=new UploadComposite();
		uploadExperienceDoc=new UploadComposite();
		socialInfoComposite=new SocialInfoComposites();

		
		
//		tbFirstName=new TextBox();
//
//		tbMiddleName=new TextBox();
//
//		tbLastName=new TextBox();

		//above old code commented by vijay to change employee first middle and last to fullName and bellow code added
		
		tbfullName = new TextBox();
		
		tbHusbandName = new TextBox();		// Added by Viraj Date 11-10-2018 for Orion
		
		uploadComposite=new UploadComposite();

		pnbLandlinePhone=new PhoneNumberBox();

		pnbCellPhoneNo1=new MyLongBox();
		approvers=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(approvers);
		AppUtility.makeApproverListBoxLive(approvers,"Employee");
		
		pnbCellPhoneNo2=new PhoneNumberBox();
		olbEmploymentType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbEmploymentType,Screen.EMPLOYEETYPE);
		olbDepartMent=new ObjectListBox<Department>();  
		reportsTo=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(reportsTo);
		this.educationalQualification=new EducationalQualificationTable();
		this.previousWorkExperience=new PreviousWorkExperienceTable();
		this.employeeProjectHistoryTable=new EmployeeProjectHistoryTable();
		this.approverTable=new ApproverTable();
		makeDepartMentListBoxLive();
		addRowapprover=new Button("Add");
		employeeId=new TextBox();
		employeeId.setEnabled(false);

		ibbankAccountNo=new TextBox();
		ibESICcode=new TextBox();
		ibPanNo=new TextBox();
		tbbankName=new TextBox();
		tbUANno=new TextBox();
		iifcCode=new TextBox();
		
	    this.bankbranchName=new TextBox();
	    tbArticleTypeInfoComposite = new ArticleTypeComposite();
		olbcountry=new ObjectListBox<Country>();
		olbcountry.MakeLive(new MyQuerry(new Vector<Filter>(), new Country()));
		
		ppfNumber=new TextBox();
		bloodGroup=new TextBox();
		
		
		
		empBranchTable = new EmployeeBranchTable();
		branchLis=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(branchLis);
		
		addBranchButton=new Button("Add");
		
		
		/**
		 * rohan added this for NBHC employee approve process
		 * date : 20/10/2016
		 */

		approverName = new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(approverName,"Employee");
		
		approvedStatus =new TextBox();
		approvedStatus.setEnabled(false);
		approvedStatus.setValue(Contract.CREATED);
		
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","EmployeeApprovalProcess"))
		{
			empApprovalConfigFlag =true;
			cbStatus=new CheckBox();
			cbStatus.setValue(false); 
			cbStatus.setEnabled(false); 
		}
		else
		{
			cbStatus=new CheckBox();
			cbStatus.setValue(true); 
		}
		
		
		
		salary = new DoubleBox();
		monthlyWorkingHrs= new DoubleBox();
		/**
		 * ends here 
		 */
		
		lbGender=new ListBox();
		lbGender.addItem("--SELECT--");
		lbGender.addItem("Male");
		lbGender.addItem("Female");
		lbGender.addItem("Other");
		/**
		 * Date18-2-2019 widow,widower added by amol for gratuity
		 */
		lbMaritialStatus=new ListBox();
		lbMaritialStatus.addItem("--SELECT--");
		lbMaritialStatus.addItem("Married");
		lbMaritialStatus.addItem("Unmarried");
		lbMaritialStatus.addItem("widow");
		lbMaritialStatus.addItem("widower");
//		lbMaritialStatus.addItem("Other");
		
		olbRefEmployee=new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbRefEmployee);
		
		
		
		olbcNumberRange = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange, Screen.NUMBERRANGE);
		/**
		 * Rahul added this on 16 July 2108
		 */
		olbCTCComponents=new ObjectListBox<CtcComponent>();
		MyQuerry qu= new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("excludeInPayRoll");
		filter.setBooleanvalue(true);
		qu.getFilters().add(filter);
		qu.setQuerryObject(new CtcComponent());
		olbCTCComponents.MakeLive(qu);
		
		
		dbFromDateForCTC=new DateBoxWithYearSelector();
		dbToDateForCTC=new DateBoxWithYearSelector();
		
		dbCTCAmtValue=new DoubleBox();
		
		btAddCtcValue=new Button("Add");
		

		this.empCTCTemplateTable=new EmployeeCTCTemplateTable();
		/**
		 * Ends
		 */
		
		pnbCellPhoneNo1.addValueChangeHandler(this);
		
		
		btnCopyPrimaryAdd=new Button("Copy Primary Address");
		btnCopySecondaryAdd=new Button("Copy Secondary Address");
		
		btnCopyPrimaryAdd.addClickHandler(this);
		btnCopySecondaryAdd.addClickHandler(this);
		
		project = new ObjectListBox<HrProject>();
		HrProject.MakeProjectListBoxLive(project);
		
		dbLastWorkingDay=new DateBoxWithYearSelector();
		/** date 18.7.2018 added by komal to store employee resignation information **/
		cbResigned = new CheckBox();
		cbResigned.setEnabled(false);
		dbResignationDate = new DateBoxWithYearSelector();
		//dbLastDateAtOrganization = new DateBoxWithYearSelector();
		dbnoticePeriod =  new DoubleBox();
		cbAbsconded =  new CheckBox();
		cbAbsconded.setEnabled(false);
		dbAbscondDate = new DateBoxWithYearSelector();
		cbTerminated =  new CheckBox();
		cbTerminated.setEnabled(false);
		dbTerminationDate = new DateBoxWithYearSelector();
		cbReleaseForPayment=  new CheckBox();
		cbReleaseForPayment.setEnabled(false);
		/** date 11.8.2018 added by komal for one time deduction flag and amount **/
		cbOneTimeDeduction = new CheckBox();
		cbOneTimeDeduction.setValue(false);
		dbOneTimeDedectionAmount = new DoubleBox();
		
		
		btnAddProject=new Button("Add");
		btnAddProject.addClickHandler(this);
		
		btnAddTrainingDetails=new Button("Add");
		btnAddTrainingDetails.addClickHandler(this);
		
		trainingTbl=new TrainingDetailsTable();
		olbTrainingType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbTrainingType, Screen.TRAINING);
		tbTrainingDescription=new TextBox();
		trainingDate=new DateBoxWithYearSelector();
		Date date=new Date();
		trainingDate.setValue(date);
        
		oblShiftName=new ObjectListBox<Shift>();
		MyQuerry querry1=new MyQuerry();
//		Filter temp1=new Filter();
//		temp1.setQuerryString("");
//		temp1.setBooleanvalue(true);
		querry1.setQuerryObject(new Shift());
//		querry1.getFilters().add(temp1);
		oblShiftName.MakeLive(querry1);
		
		
		
		oblInterviewedBy=new ObjectListBox<Employee>();
		MyQuerry querry2=new MyQuerry();
		Filter temp1=new Filter();
		temp1.setQuerryString("status");
		temp1.setBooleanvalue(true);
		querry2.setQuerryObject(new Employee());
		querry2.getFilters().add(temp1);
		oblInterviewedBy.MakeLive(querry2);
		
		oblSalAuthBy=new ObjectListBox<Employee>();
		oblSalAuthBy.MakeLive(querry2);
		  
		tbNomineeName=new TextBox();                    
		tbNomineeRelation=new TextBox();                
		ibNomineeAge=new IntegerBox();                  
		tbNomineeAddress=new TextBox();                 
		        
		tbHappayCardUserId=new TextBox();               
		tbHappayCardUserName=new TextBox();             
		tbHappayCardWalletNumber=new TextBox();         
		        
		cbIsFingerRegistered=new CheckBox(); 
		cbIsFingerRegistered.setValue(false);
		tbFingerRegisteredBy=new TextBox();             
		dbFingerRegisteredDate=new DateBoxWithYearSelector();   
		
		ibAadharNumber=new TextBox();
		
		cbPreviousPfNumber=new CheckBox();
		cbPreviousPfNumber.setValue(false);
		
		cbPreviousEsicNumber=new CheckBox();
		cbPreviousEsicNumber.setValue(false);
		
		cbSkilled=new CheckBox();
		cbSkilled.setValue(false);
		
		cbSemiSkilled=new CheckBox();
		cbSemiSkilled.setValue(false);
		
		cbUnskilled=new CheckBox();
		cbUnskilled.setValue(false);
		
		tbLanguageSpoken=new TextBox();
		
		uploadEmpSignature=new UploadComposite();
		tbAgeAsOnDate=new TextBox();
		tbAgeAsOnDate.setEnabled(false);
		panel=new HorizontalPanel();
		
		promotionTbl=new PromotionHistoryTable();
		btnAddPromotion=new Button("Add");
		btnAddPromotion.addClickHandler(this);
		
		dbDateOfRetirement=new DateBoxWithYearSelector();
		
		uploadComposite1=new UploadComposite();
		uploadComposite2=new UploadComposite();
		
		cbIsDisabled=new CheckBox();
		cbIsDisabled.setValue(false);
		cbIsDisabled.addClickHandler(this);
		dbDisabilityPercentage=new DoubleBox();
		dbDisabilityPercentage.setEnabled(false);
		uploadDisablityDoc=new UploadComposite();
		uploadDisablityDoc.setEnable(false);
		taDisabilityRemark=new TextArea();
		
		tbSchemeCertificateNumber=new TextBox();
		tbPensionPaymentNumber=new TextBox();
		
		
		
		cbIsFingerRegistered.setEnabled(false);
		tbFingerRegisteredBy.setEnabled(false);
		dbFingerRegisteredDate.setEnabled(false);
		cbOutstationEmp=new CheckBox();
		cbOutstationEmp.setValue(false);
		
		/**
		 * Employee group is used to calculate lwf
		 * lwf is applicable to direct(blue collar) employee
		 */
		lbEmpGrp=new ListBox();
		lbEmpGrp.addItem("--SELECT--");
		lbEmpGrp.addItem("Direct");
		lbEmpGrp.addItem("Indirect");
		
		tbReasonForLeaving=new TextBox();
		
		tbFnfStatus=new TextBox();
		tbFnfStatus.setEnabled(false);
		
		
		tbCreatedBy=new TextBox();
		tbUpdatedBy=new TextBox();
//		dbCreationDate=new DateBox();
		dbCreationDate = new DateBoxWithYearSelector();

//		dbUpdationDate=new DateBox();
		dbUpdationDate = new DateBoxWithYearSelector();

		dbFnFDate=new DateBoxWithYearSelector();

		dbFnFDate.setEnabled(false);
		
		olbCaste=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCaste,Screen.CASTE);
		
		cbIsReliever=new CheckBox();
		cbIsReliever.setValue(false);
		
		policeVerfDocUplCom=new UploadComposite();
		dbExpiryDate=new DateBoxWithYearSelector();
		
		siteLocation=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","EnableSiteLocation");
		oblSiteLocationList=new ObjectListBox<CustomerBranchDetails>();
		oblSiteLocationList.addItem("--SELECT--");
		project.addChangeHandler(this);
		incentivePercent=new DoubleBox();
		
		oblOtList=new ObjectListBox<Overtime>();
		filter=new Filter();
		filter.setBooleanvalue(true);
		filter.setQuerryString("status");
		querry1=new MyQuerry();
		querry1.getFilters().add(filter);
		querry1.setQuerryObject(new Overtime());
		oblOtList.MakeLive(querry1);
		
		olbcalender = new ObjectListBox<Calendar>();
		Calendar.makeObjectListBoxLive(olbcalender);		
		
		olbleavegroup = new ObjectListBox<LeaveGroup>();
		olbleavegroup.MakeLive(new MyQuerry(new Vector<Filter>(),new LeaveGroup()));
		
		seperatorAddr1=            new Label("_________________________________________________________________________________Primary Address________________________________________________________________________");
		seperatorAddr2=            new Label("_________________________________________________________________________________Secondary Address______________________________________________________________________");
		seperatorProjectHistory=   new Label("_________________________________________________________________________________Employee Project History_______________________________________________________________");
		seperatorPassport=         new Label("_________________________________________________________________________________Passport Information___________________________________________________________________");
		seperatorSocial=           new Label("_________________________________________________________________________________Social Information_____________________________________________________________________");
		noteCTCAllocation=         new Label("Note: This is applicable only for 'Travelling Allowance' component defined as exclusive. It gets printed on Salary register internal report. It does not have any impact on payroll.");
		seperatorHappyCard=        new Label("_________________________________________________________________________________Happy Card Information__________________________________________________________________");
			
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();
		/**
		 * @author Anil,Date : 28-01-2019
		 * Added Nomination tab for storing nomination details(Form 2)
		 * @author Anil,Date : 04-05-2019
		 * Added allocate CTC button
		 * raised by Sonu for charlene hospitality
		 */
		if(empApprovalConfigFlag){
			this.processlevelBarNames=new String[]{"New","Request For Approval","Cancel Approval Request" ,AppConstants.PRINTIDCARDINPORTRATEVIEW,AppConstants.PRINTIDCARDINLANDSCAPEVIEW,AppConstants.FULLANDFINAL,AppConstants.EMPLOYMENTCARD,AppConstants.FORM3A,"Nomination Details","Allocate CTC","Change Employee Name"
					,"Employee Additional Details"};
		}
		else
		{
			this.processlevelBarNames=new String[]{"New",AppConstants.PRINTIDCARDINPORTRATEVIEW,AppConstants.PRINTIDCARDINLANDSCAPEVIEW,AppConstants.FULLANDFINAL,AppConstants.EMPLOYMENTCARD,AppConstants.FORM3A,"Nomination Details","Allocate CTC","Change Employee Name","Employee Additional Details"};
		}
		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		String mainScreenLabel="Employee Information";
		if(employeeobj!=null&&employeeobj.getCount()!=0){
			mainScreenLabel=employeeobj.getCount()+" "+"/"+" "+employeeobj.getApproveStatus()+" "+"/"+" "+AppUtility.parseDate(employeeobj.getCreationDate());
		}
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).setKeyField(true).build();
		
//		fbuilder = new FormFieldBuilder();		
//		//old label Employee Information
//		FormField fgroupingEmployeeInformation=fbuilder.setlabel("Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
//		fgroupingEmployeeInformation.getWidget().setTitle("testtitle");
		//		fbuilder = new FormFieldBuilder("* First Name",tbFirstName);
//		FormField ftbFirstName= fbuilder.setMandatory(true).setMandatoryMsg("First Name is Mandatory !").setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Middle Name",tbMiddleName);
//		FormField ftbMiddleName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("* Last Name",tbLastName);
		
		// above old code commented by vijay to change employee first middle and last to fullName and bellow code added
		fbuilder = new FormFieldBuilder("* Full Name",tbfullName);
		FormField ftbfullName= fbuilder.setMandatory(true).setMandatoryMsg("Full Name is Mandatory !").setRowSpan(0).setColSpan(0).build();
		
		/** Added by Viraj Date 11-10-2018 for Orion **/
		FormField ftbHusbandName = null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "ONLYFORORION")
				||AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "EmployeeApprovalProcess"))
		{
			fbuilder = new FormFieldBuilder("Father/Husband Name",tbHusbandName);
			ftbHusbandName= fbuilder.setMandatory(false).setMandatoryMsg("Father/Husband Name is Mandatory !").setRowSpan(0).setColSpan(0).build();
		}
		/** Ends **/
		FormField ftbLastName= fbuilder.setMandatory(true).setMandatoryMsg("Last Name is Mandatory !").setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 04-07-2019 by Vijay 
		 * Des :- NBHC CCPM Cell No Non Mandatory with process config
		 */
		FormField fdbDateOfBirth=null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "EnableEmployeeDateOfBirthNonMandatory")){
			fbuilder = new FormFieldBuilder("Date Of Birth",dbDateOfBirth);
			fdbDateOfBirth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		else{
			fbuilder = new FormFieldBuilder("* Date Of Birth",dbDateOfBirth);
			fdbDateOfBirth= fbuilder.setMandatory(true).setMandatoryMsg("D.O.B is mandatory !").setRowSpan(0).setColSpan(0).build();
						
		}
		/**
		 * ends here
		 */
		
		fbuilder = new FormFieldBuilder("* Branch",lbBranch);
		FormField flbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory !").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Designation",lbDesignation);
		FormField flbDesignation= fbuilder.setMandatory(false).setMandatoryMsg("Designation is Mandatory !").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Role",lbRole);
		FormField flbRole= fbuilder.setMandatory(false).setMandatoryMsg("Role is Mandatory !").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Joining Date",dbJoiningDate);
		FormField fdbJoiningDate= fbuilder.setMandatory(true).setMandatoryMsg("Joining Date is Mandatory !").setRowSpan(0).setColSpan(0).build();

		FormField folbreportsTo = null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "MakeReportToMandatory")){
			fbuilder = new FormFieldBuilder("* Reports To",reportsTo);
			folbreportsTo= fbuilder.setMandatory(true).setRowSpan(0).setMandatoryMsg("Employee Report To is Mandatory !").setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder(" Reports To",reportsTo);
			folbreportsTo= fbuilder.setMandatory(false).setRowSpan(0).setMandatoryMsg("Employee Reporter is Mandatory !").setColSpan(0).build();
		}
		fbuilder = new FormFieldBuilder();
		FormField fgroupingArticleInformation=fbuilder.setlabel("Article Info").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",tbArticleTypeInfoComposite);
        FormField ftbTaxTypeInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		

		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fcbStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmployeeContactInformation=fbuilder.setlabel("Contact Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Landline Phone",pnbLandlinePhone);
		FormField fpnbLandlinePhone= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 * Date 04-07-2019 by Vijay 
		 * Des :- NBHC CCPM Cell No Non Mandatory with process config
		 */
		FormField fpnbCellPhoneNo1=null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","EnableEmployeeCellNoNonMandatory")){
			fbuilder = new FormFieldBuilder("Cell Phone No 1",pnbCellPhoneNo1);
			fpnbCellPhoneNo1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		else{
			fbuilder = new FormFieldBuilder("* Cell Phone No 1",pnbCellPhoneNo1);
			fpnbCellPhoneNo1= fbuilder.setMandatory(true).setMandatoryMsg("Cell Phone No 1 is Mandatory !").setRowSpan(0).setColSpan(0).build();
				
		}
		/**
		 * ends here
		 */
		fbuilder = new FormFieldBuilder("Cell Phone No 2",pnbCellPhoneNo2);
		FormField fpnbCellPhoneNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		FormField fetbEmail=null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","EMAILNOTMANDATORY")){
			fbuilder = new FormFieldBuilder("Email",etbEmail);
			fetbEmail= fbuilder.setMandatory(false).setMandatoryMsg("Email is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("* Email",etbEmail);
			fetbEmail= fbuilder.setMandatory(true).setMandatoryMsg("Email is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}
		//old label Employee Primary Address. since we are merging primary and seconadary address in one tab.
		FormField fgroupingEmployeeAddress1=fbuilder.setlabel("Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",addressComposite);
		FormField faddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		FormField fgroupingEmployeeAddress2=fbuilder.setlabel("Secondary Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",secondoryAddressComposite);
		FormField fsecondoryAddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSocialInformation=fbuilder.setlabel("Social Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",socialInfoComposite);
		FormField fsocialInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingUploadPhoto=fbuilder.setlabel("Upload Photo").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		/**
		 * @author Anil @since 23-02-2021
		 */
//		fbuilder = new FormFieldBuilder("Upload Employee Photo(Size : 600x200 px)",uploadComposite);
		fbuilder = new FormFieldBuilder("Upload Employee Photo(Size : 516x500 px)",uploadComposite);
		FormField fuploadComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		//old label Educational Details
		FormField fgroupingEducationalQualifiaction=fbuilder.setlabel("Qualification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",educationalQualification.getTable());
		FormField feducationalQualification= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		//old label Experience Details 
		FormField fgroupingPreviousWorkInformation=fbuilder.setlabel("Experience").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",previousWorkExperience.getTable());
		FormField fpreviousWork= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField fgroupingApprover=fbuilder.setlabel("Leave Approver").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",approverTable);
		FormField fapproverTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField fgroupingpassport=fbuilder.setlabel("Passport Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",passPortInformation);
		FormField fpassPortComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Id Card Number",idCardNumber);
		FormField fidCardNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Access Card Number",accessCardNo);
		FormField faccesscard= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",addRowInPreviousWork);
		FormField faddrowprevious= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",addRowInQualification);
		FormField faddrowinqual= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Leave Approvers",approvers);
		FormField fapprovers= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",addRowapprover);
		FormField faddRowApprover= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Department",olbDepartMent);
		FormField folbDepartment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Employee Type",olbEmploymentType);
		FormField folbemploymentType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Employment ID",employeeId);
		FormField folbemployeeId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		

		fbuilder = new FormFieldBuilder();
		//old label Employee Payroll Information
		FormField fgroupingEmployeePayrollInformation=fbuilder.setlabel("Payroll").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	
        /**
         *  Date : 24-03-2021 Added By Priynaka
         *  Des :Make country Non mandatory As per requiremnet of Nitin Sir.
         */
		fbuilder = new FormFieldBuilder("Base Country",olbcountry);
		FormField folbcountry= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		

		
		fbuilder = new FormFieldBuilder("Bank Account No",ibbankAccountNo);
		FormField fibbankAccountNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("ESIC No",ibESICcode);
		FormField fibESICcode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("PAN No",ibPanNo);
		FormField fibPanNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Bank Name",tbbankName);
		FormField ftbbankName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("UAN NO",tbUANno);
		FormField ftbUANno= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Bank Branch",bankbranchName);
		FormField ftbranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("IFSC ",this.iifcCode);
		FormField ftIIFC= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("PF Number ",this.ppfNumber);
		FormField ftppfNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Blood Group",this.bloodGroup);
		FormField fbloodGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		//***************anil ***********8
		/**
		 * Updated By: Viraj
		 * Date: 23-02-2019
		 * Description: To make address document mandatory
		 */
		FormField fuploadaddressDoc;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "MakeAddressDocumentMandatory")){
			fbuilder = new FormFieldBuilder("* Address Document (To upload more than one file, upload zip file)",uploadAddressDoc);
			fuploadaddressDoc= fbuilder.setMandatory(true).setMandatoryMsg("Address Document is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Address Document (To upload more than one file, upload zip file)",uploadAddressDoc);
			fuploadaddressDoc= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/** Ends **/
		
		fbuilder = new FormFieldBuilder("Educational Document",uploadEducationalDoc);
		FormField fuploadEducationalDoc= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Experience Document",uploadExperienceDoc);
		FormField fuploadExperienceDoc= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("",empBranchTable.getTable());
		FormField fempBranchTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		//old label Employee Project History
		FormField fgroupingEmployeeProjectHistory=fbuilder.setlabel("Project History").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",employeeProjectHistoryTable.getTable());
		FormField femployeeProjectHistoryTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		//old label Employee Branch Informantion
		FormField fgroupingBranchInformation=fbuilder.setlabel("Branch").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Select Branch",branchLis);
		FormField fbranchLis= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",addBranchButton);
		FormField faddBranchButton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * rohan added this for NBHC Employee approved process
		 */
		
		fbuilder = new FormFieldBuilder("Approval Status",approvedStatus);
		FormField fapprovedStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fapprovName = null;
		if(empApprovalConfigFlag){
			fbuilder = new FormFieldBuilder("* Approver Name",approverName);
			fapprovName= fbuilder.setMandatory(true).setMandatoryMsg("Approver is Mandatory").setRowSpan(0).setColSpan(0).build();
		}
		
		
		/**
		 * ends here 
		 */
		

		/**
		 * Date: 08-02-2017 by Anil
		 * Salary field is mandatory for only NBHC CCPM and its enabled from Process Configuration
		 */
		FormField fsalary=null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","OnlyForNBHC")){
			fbuilder = new FormFieldBuilder("* Salary(in Rs)",salary);
			fsalary= fbuilder.setMandatory(true).setMandatoryMsg("This is mandatory").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Salary(in Rs)",salary);
			fsalary= fbuilder.setMandatory(false).setMandatoryMsg("This is mandatory").setRowSpan(0).setColSpan(0).build();
		}
		
		fbuilder = new FormFieldBuilder("Monthly Working Hrs",monthlyWorkingHrs);
		FormField monthlyWorkHrs= fbuilder.setMandatory(false).setMandatoryMsg("This is mandatory").setRowSpan(0).setColSpan(0).build();
		/**
		 * End
		 */
		
		fbuilder = new FormFieldBuilder("* Gender",lbGender);
		FormField flbGender= fbuilder.setMandatory(true).setMandatoryMsg("Gender is mandatory").setRowSpan(0).setColSpan(0).build();
		
		FormField folbcNumberRange;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "MakeNumberRangeMandatory")){
			fbuilder = new FormFieldBuilder("* Number Range",olbcNumberRange);
			folbcNumberRange= fbuilder.setMandatory(true).setMandatoryMsg("Number Range is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Number Range",olbcNumberRange);
			folbcNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		fbuilder = new FormFieldBuilder("Marital Status",lbMaritialStatus);
		FormField flbMaritialStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Reference Employee",olbRefEmployee);
		FormField folbRefEmployee= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Rahul Verma added this on 16 July 2018
		 */
		FormField fgroupingCTCAllocation=fbuilder.setlabel("CTC ALLOCATION").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("CTC Component",olbCTCComponents);
		FormField folbCTCComponents= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Start Date",dbFromDateForCTC);
		FormField fdbFromDateForCTC= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("End Date",dbToDateForCTC);
		FormField fdbToDateForCTC= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btAddCtcValue);
		FormField fbtAddCtcValue= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",empCTCTemplateTable.getTable());
		FormField fempCTCTemplateTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Amount",dbCTCAmtValue);
		FormField fdbCTCAmtValue= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/**
		 * Ends
		 */
		
		
		fbuilder = new FormFieldBuilder("",btnCopyPrimaryAdd);
		FormField fbtnCopyPrimaryAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnCopySecondaryAdd);
		FormField fbtnCopySecondaryAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Project", project);
		FormField fproject = fbuilder.setMandatory(false).setMandatoryMsg("Project is mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Last Date Of Working", dbLastWorkingDay);
		FormField fdbLastWorkingDay = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 18.7.2018 added by komal to store employee resignation information **/
		fbuilder = new FormFieldBuilder("Resigned", cbResigned);
		FormField fcbResigned= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Date of Resignation",dbResignationDate);
		FormField fdbResignationDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("Last Date at Organization",dbLastDateAtOrganization);
//		FormField fdbLastDateAtOrganization= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Notice Period",dbnoticePeriod);
		FormField fdbnoticePeriod= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Terminated", cbTerminated);
		FormField fcbTerminated= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Date of Termination",dbTerminationDate);
		FormField fdbTerminationDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Absconded", cbAbsconded);
		FormField fcbAbsconded= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Abscond Date",dbAbscondDate);
		FormField fdbAbscondDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Release Payment",cbReleaseForPayment);
		FormField fcbRealeaseForPayment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** date 10.8.2018 added by komal to seperate full and final dates **/
		//old label Full and Final Information
		FormField fgroupingFullandFinal=fbuilder.setlabel("Full and Final").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		/**
		 * Ends
		 */
		/** date 11.8.2018 added by komal for one time deduction flag and amount **/
		fbuilder = new FormFieldBuilder("One Time Deduction",cbOneTimeDeduction);
		FormField fcbOneTimeDeduction= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("One Time Deduction Amount",dbOneTimeDedectionAmount);
		FormField fdbOneTimeDedectionAmount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * end komal
		 */
		
		fbuilder = new FormFieldBuilder("", btnAddProject);
		FormField fbtnAddProject = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		fbuilder = new FormFieldBuilder();
		//old label Employee Training Details
		FormField fgroupingTrainingDetails=fbuilder.setlabel("Trainings").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",trainingTbl.getTable());
		FormField ftrainingTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", btnAddTrainingDetails);
		FormField fbtnAddTrainingDetails = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Training Type", olbTrainingType);
		FormField folbTrainingType = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Training Description", tbTrainingDescription);
		FormField ftbTrainingDescription = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Training Date", trainingDate);
		FormField ftrainingDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingHappayCard=fbuilder.setlabel("Happay Card Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingNomineeDetails=fbuilder.setlabel("Nominee Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder(); //old label Finger Registration Details
		FormField fgroupingFingerRegistrationDetails=fbuilder.setlabel("Finger Print").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Interviewed By", oblInterviewedBy);
		FormField foblInterviewedBy = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Salary Authorized By", oblSalAuthBy);
		FormField foblSalAuthBy = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Shift", oblShiftName);
		FormField foblShiftName = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Nominee Name", tbNomineeName);
		FormField ftbNomineeName = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Nominee Age", ibNomineeAge);
		FormField fibNomineeAge = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Nominee Relation", tbNomineeRelation);
		FormField ftbNomineeRelation = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Nominee Address", tbNomineeAddress);
		FormField ftbNomineeAddress = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Happay Card User Id", tbHappayCardUserId);
		FormField ftbHappayCardUserId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Happay Card User Name", tbHappayCardUserName);
		FormField ftbHappayCardUserName = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Happay Card Wallet Number", tbHappayCardWalletNumber);
		FormField ftbHappayCardWalletNumber = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Finger Registered", cbIsFingerRegistered);
		FormField fcbIsFingerRegistered = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Finger Registered By", tbFingerRegisteredBy);
		FormField ftbFingerRegisteredBy = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Finger Registration Date", dbFingerRegisteredDate);
		FormField fdbFingerRegisteredDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Updated By: Viraj
		 * Date: 23-02-2019
		 * Description: To make adhar number mandatory
		 */
		FormField fibAadharNumber;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "MakeAadharNumberMandatory")){
			fbuilder = new FormFieldBuilder("* Aadhar Number",ibAadharNumber);
			fibAadharNumber= fbuilder.setMandatory(true).setMandatoryMsg("Aadhar Number is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Aadhar Number",ibAadharNumber);
			fibAadharNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/** Ends **/
		
		fbuilder = new FormFieldBuilder("Previous PF Number",cbPreviousPfNumber);
		FormField fcbPreviousPfNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Previous ESIC Number",cbPreviousEsicNumber);
		FormField fcbPreviousEsicNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Skilled",cbSkilled);
		FormField fcbSkilled= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Semi Skilled",cbSemiSkilled);
		FormField fcbSemiSkilled= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Unskilled",cbUnskilled);
		FormField fcbUnskilled= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Language Spoken",tbLanguageSpoken);
		FormField ftbLanguageSpoken= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Employee Signature",uploadEmpSignature);
		FormField fuploadEmpSignature= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		/**
		 * Date : 12-10-2018 By ANIL
		 */
		fbuilder = new FormFieldBuilder("Age as on date",tbAgeAsOnDate);
		FormField ftbAgeAsOnDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(); //old label Promotion History
		FormField fgroupingpromotion=fbuilder.setlabel("Promotions").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",promotionTbl.getTable());
		FormField fpromotionTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", btnAddPromotion);
		FormField fbtnAddPromotion = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Date of Retirement", dbDateOfRetirement);
		FormField fdbDateOfRetirement = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupinguploaddocuments=fbuilder.setlabel("Upload Documents").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Upload Document 1",uploadComposite1);
		FormField fuploadComposite1= fbuilder.setMandatory(false).setRowSpan(2).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Upload Document 2",uploadComposite2);
		FormField fuploadComposite2= fbuilder.setMandatory(false).setRowSpan(2).setColSpan(1).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingdisabilitydetails=fbuilder.setlabel("Disability Info").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Disabled",cbIsDisabled);
		FormField fcbIsDisabled= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Disability Percentage",dbDisabilityPercentage);
		FormField fdbDisabilityPercentage= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Upload Disability Document",uploadDisablityDoc);
		FormField fuploadDisablityDoc= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Additional Details",taDisabilityRemark);
		FormField ftaDisabilityRemark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Scheme Certificate No.",tbSchemeCertificateNumber);
		FormField ftbSchemeCertificateNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Pension Payment Order(PPO) No.",tbPensionPaymentNumber);
		FormField ftbPensionPaymentNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("On field attendance",cbOutstationEmp);
		FormField fcbOutstationEmp= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * @author Anil,Date : 10-04-2019
		 * Adding employee grp ,it contains direct and indirect static value
		 * if process configuration is  active then it is  mandatory and used for LWF calculation.
		 * LWF is only applicable for direct employee
		 */
		FormField flbEmpGrp=null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "LwfAsPerEmployeeGroup")){
			fbuilder = new FormFieldBuilder("* Employee Group",lbEmpGrp);
			flbEmpGrp= fbuilder.setMandatory(true).setMandatoryMsg("Employee group is mandatory.").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Employee Group",lbEmpGrp);
			flbEmpGrp= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/**
		 * 
		 */
		
		fbuilder = new FormFieldBuilder("Reason for leaving",tbReasonForLeaving);
		FormField ftbReasonForLeaving= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("FNF Status",tbFnfStatus);
		FormField ftbFnfStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Created By",tbCreatedBy);
		FormField ftbCreatedBy= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Creation Date",dbCreationDate);
		FormField fdbCreationDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Update By",tbUpdatedBy);
		FormField ftbUpdatedBy= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Update Date",dbUpdationDate);
		FormField fdbUpdationDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("FnF Date", dbFnFDate);
		FormField fdbFnFDate= fbuilder.setMandatory(false).setMandatoryMsg("FnF date is mandatory.").setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Caste", olbCaste);
		FormField folbCast= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Reliever", cbIsReliever);
		FormField fcbIsReliever= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fpoliceVerfDocUplCom;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "ONLYFORORION") || AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","EmployeeApprovalProcess")){
			fbuilder = new FormFieldBuilder("Police verfication document", policeVerfDocUplCom);
			fpoliceVerfDocUplCom= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		}else{
			fbuilder = new FormFieldBuilder("Police verfication document", policeVerfDocUplCom);
			fpoliceVerfDocUplCom= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		}
		
		fbuilder = new FormFieldBuilder("Expiry Date", dbExpiryDate);
		FormField fdbExpiryDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Site Location", oblSiteLocationList);
		FormField foblSiteLocationList= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setVisible(siteLocation).build();
		
		
		fbuilder = new FormFieldBuilder("Sales Incentive %", incentivePercent);
		FormField fIncentivePercent= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		//old label Other Information
		FormField fgroupingOtherInfo=fbuilder.setlabel("Other Info").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingProjectInfo=fbuilder.setlabel("Project").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("Overtime", oblOtList);
		FormField foblOtList = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Calendar", olbcalender);
		FormField folbcalender = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Leave Group", olbleavegroup);
		FormField folbleavegroup = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPassportSocial=fbuilder.setlabel("Passport and Social Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		
		fbuilder = new FormFieldBuilder("", seperatorAddr1);
		FormField fseperatorAddr1 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", seperatorAddr2);
		FormField fseperatorAddr2 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", seperatorProjectHistory);
		FormField fseperatorProjectHistory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", seperatorPassport);
		FormField fseperatorPassport = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("", seperatorSocial);
		FormField fseperatorSocial = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("", seperatorHappyCard);
		FormField fseperatorHappyCard = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("", noteCTCAllocation);
		FormField fnoteCTCAllocation = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingTimeline=fbuilder.setlabel("Timeline").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		/** Added by Viraj Date 11-10-2018 for Orion **/
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "ONLYFORORION"))
		{
//			FormField[][] formfield = {   {fgroupingEmployeeInformation},
//					{folbemployeeId,folbcountry,folbDepartment,folbemploymentType},
////					{ftbFirstName,ftbMiddleName,ftbLastName,fdbDateOfBirth},
////					{flbBranch,flbDesignation,flbRole,fdbJoiningDate},
////					{folbreportsTo,faccesscard,fidCardNumber,fbloodGroup},
////					{fcbStatus},
//					
//					//vijay
//					{ftbfullName,fdbDateOfBirth,flbBranch,flbDesignation}, 
//					{ftbHusbandName,flbRole,fdbJoiningDate,folbreportsTo},
//					{faccesscard,fidCardNumber,fbloodGroup,fsalary},
//					{monthlyWorkHrs,flbGender,folbcNumberRange,flbMaritialStatus},
//					{flbEmpGrp,foblShiftName,foblInterviewedBy,foblSalAuthBy},
//					{fcbSkilled,fcbSemiSkilled,fcbUnskilled,ftbLanguageSpoken},
//					{folbRefEmployee,fcbIsReliever,fcbOneTimeDeduction , fdbOneTimeDedectionAmount},
//					{fpoliceVerfDocUplCom,fdbExpiryDate,fcbStatus},
//					{fproject,foblSiteLocationList,fIncentivePercent},
//					
//					{fgroupingFullandFinal},/** date 2.8.2018 added by komal to store employee resignation information **/
//					{fdbnoticePeriod ,fcbResigned , fdbResignationDate,fdbLastWorkingDay},	/** date 18.7.2018 added by komal to store employee resignation information **/
//					{fcbAbsconded , fdbAbscondDate , fcbTerminated , fdbTerminationDate},/** date 2.8.2018 added by komal to store employee abscond and terminate information **/
//					{ftbReasonForLeaving,fcbRealeaseForPayment,ftbFnfStatus,fdbFnFDate},
//					//{fgroupingApprover},
//					//{fapprovers,faddRowApprover},
//					//{fapproverTable},
//					
//					{fgroupingEmployeePayrollInformation},
//					{ftbbankName,ftbranch,ftIIFC,fibbankAccountNo},
//					{fcbPreviousEsicNumber,fibESICcode,fcbPreviousPfNumber,ftppfNumber},
//					{fibAadharNumber,fibPanNo,ftbUANno},
//					
//					{fgroupingPreviousWorkInformation},
//					{faddrowprevious},
//					{fpreviousWork},
//					{fuploadExperienceDoc},
//					
//					{fgroupingEducationalQualifiaction},
//					{faddrowinqual},
//					{feducationalQualification},
//					{fuploadEducationalDoc},
//					
//					{fgroupingEmployeeContactInformation},
//					{fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2,fetbEmail},
//					{fuploadaddressDoc},
//					
//					{fgroupingEmployeeAddress1},
//					{fbtnCopySecondaryAdd},
//					{faddressComposite},
//					
//					{fgroupingEmployeeAddress2},
//					{fbtnCopyPrimaryAdd},
//					{fsecondoryAddressComposite},
//					
//					{fgroupingpassport},
//					{fpassPortComposite},
//					
//					{fgroupingSocialInformation},
//					{fsocialInfoComposite},
//					
//					{fgroupingBranchInformation},	
//					{fbranchLis,faddBranchButton},
//					{fempBranchTable},
//					
//					{fgroupingUploadPhoto},
//					{fuploadComposite},
//					
//					{fgroupingArticleInformation},
//					{ftbTaxTypeInfoComposite},
//					
//					{fgroupingEmployeeProjectHistory},
//					{femployeeProjectHistoryTable},
//					
//					{fgroupingCTCAllocation},
//					{folbCTCComponents,fdbCTCAmtValue,fdbFromDateForCTC,fdbToDateForCTC},
//					{fbtAddCtcValue},
//					{fempCTCTemplateTable},
//					
//					{fgroupingNomineeDetails},
//					{ftbNomineeName,fibNomineeAge,ftbNomineeRelation,ftbNomineeAddress},
//					
//					{fgroupingHappayCard},
//					{ftbHappayCardUserId,ftbHappayCardUserName,ftbHappayCardWalletNumber},
//					
//					{fgroupingFingerRegistrationDetails},
//					/***Date 29-7-2019 Commented "Finger Registered by" and "Finger Registered Date" 
//					 * raised by sasha ***/
////					{fcbIsFingerRegistered,ftbFingerRegisteredBy,fdbFingerRegisteredDate,fcbOutstationEmp},
//					{fcbIsFingerRegistered,fcbOutstationEmp},
//					{ftbSchemeCertificateNumber,ftbPensionPaymentNumber},
//					{ftbCreatedBy,fdbCreationDate,ftbUpdatedBy,fdbUpdationDate}
//		};			
					
					FormField[][] formfield = {   
							
							{fgroupingEmployeeInformation},
							{folbemployeeId,ftbfullName,flbBranch,fdbJoiningDate,fdbDateOfBirth},
							{flbGender,fpnbCellPhoneNo1,fetbEmail,folbemploymentType,flbEmpGrp},
							{folbDepartment,flbRole,flbDesignation,folbreportsTo,fcbIsReliever},
							{fcbStatus,flbMaritialStatus,ftbHusbandName,folbcNumberRange,fbloodGroup},
							{fuploadComposite},							
							
							//Other info
							{fgroupingOtherInfo},
							{folbcountry,faccesscard,fidCardNumber,fcbOneTimeDeduction,fdbOneTimeDedectionAmount},
							{fsalary,monthlyWorkHrs,foblSalAuthBy,fIncentivePercent,foblInterviewedBy},
							{fcbSkilled,fcbSemiSkilled,fcbUnskilled,fdbDateOfRetirement,folbRefEmployee},
							{ftbLanguageSpoken,folbCast,ftbHappayCardUserId,ftbHappayCardUserName,ftbHappayCardWalletNumber},
							{fpoliceVerfDocUplCom,fdbExpiryDate,fuploadComposite1,fuploadComposite2},	
							{fpnbLandlinePhone,fpnbCellPhoneNo2,fuploadaddressDoc},
							{fseperatorPassport},
							{fpassPortComposite},		
							{fseperatorSocial},
							{fsocialInfoComposite},
							
							
							//Address
							{fgroupingEmployeeAddress1},
							{fseperatorAddr1},
							{fbtnCopySecondaryAdd},
							{faddressComposite},
							{fseperatorAddr2},
							{fbtnCopyPrimaryAdd},
							{fsecondoryAddressComposite},
							
							//Article Info
							{fgroupingArticleInformation},
							{ftbTaxTypeInfoComposite},
							
							//Branch info
							{fgroupingBranchInformation},	
							{fbranchLis,faddBranchButton},
							{fempBranchTable},
							
							//Nominee info						
							{fgroupingNomineeDetails},
							{ftbNomineeName,fibNomineeAge,ftbNomineeRelation,ftbNomineeAddress},
							
							//payroll info
							{fgroupingEmployeePayrollInformation},
							{ftbbankName,ftbranch,ftIIFC,fibbankAccountNo},
							{fcbPreviousEsicNumber,fibESICcode,fcbPreviousPfNumber,ftppfNumber},
							{fibAadharNumber,fibPanNo,ftbUANno},
							
							{fproject,foblSiteLocationList,foblShiftName,foblOtList},
							{folbcalender,folbleavegroup},
							{fseperatorProjectHistory},
							{femployeeProjectHistoryTable},
							
														
							{fgroupingFullandFinal},/** date 2.8.2018 added by komal to store employee resignation information **/
							{fdbnoticePeriod ,fcbResigned , fdbResignationDate,fdbLastWorkingDay},	/** date 18.7.2018 added by komal to store employee resignation information **/
							{fcbAbsconded , fdbAbscondDate , fcbTerminated , fdbTerminationDate},/** date 2.8.2018 added by komal to store employee abscond and terminate information **/
							{ftbReasonForLeaving,fcbRealeaseForPayment,ftbFnfStatus,fdbFnFDate},
							
														
							{fgroupingPreviousWorkInformation},
							{faddrowprevious},
							{fpreviousWork},
							{fuploadExperienceDoc},
							
							{fgroupingEducationalQualifiaction},
							{faddrowinqual},
							{feducationalQualification},
							{fuploadEducationalDoc},
							
							//training info
							{fgroupingTrainingDetails},
							{folbTrainingType,ftbTrainingDescription,ftrainingDate,fbtnAddTrainingDetails},
							{ftrainingTbl},
							
							{fgroupingFingerRegistrationDetails},
							/***Date 29-7-2019 Commented "Finger Registered by" and "Finger Registered Date" 
							 * raised by sasha ***/
//							{fcbIsFingerRegistered,ftbFingerRegisteredBy,fdbFingerRegisteredDate,fcbOutstationEmp},
							{fcbIsFingerRegistered,fcbOutstationEmp},
							{ftbSchemeCertificateNumber,ftbPensionPaymentNumber},		
					
					
							{fgroupingCTCAllocation},
							{fnoteCTCAllocation},
							{folbCTCComponents,fdbCTCAmtValue,fdbFromDateForCTC,fdbToDateForCTC},
							{fbtAddCtcValue},
							{fempCTCTemplateTable},
							
							{fgroupingTimeline},
							{ftbCreatedBy,fdbCreationDate,ftbUpdatedBy,fdbUpdationDate},
					
					
		};this.fields=formfield;
		}
		else if(empApprovalConfigFlag){
			
//		FormField[][] formfield = {   
//				
//				{fgroupingEmployeeInformation},
//				{folbemployeeId,folbcountry,folbDepartment,folbemploymentType},
////				{ftbFirstName,ftbMiddleName,ftbLastName,fdbDateOfBirth},
////				{flbBranch,flbDesignation,flbRole,fdbJoiningDate},
////				{folbreportsTo,faccesscard,fidCardNumber,fbloodGroup},
////				{fcbStatus},
//				
//				//vijay    
//				{ftbfullName,flbBranch,flbDesignation,ftbHusbandName},
//				{flbRole,fdbJoiningDate,folbreportsTo,faccesscard},
//				{fidCardNumber,fbloodGroup,fapprovName,fapprovedStatus},
//				{flbGender,fsalary,monthlyWorkHrs,fcbStatus},
//				{folbcNumberRange,flbEmpGrp,foblShiftName,flbMaritialStatus},
//				{folbRefEmployee,foblInterviewedBy,foblSalAuthBy,ftbLanguageSpoken},
//				{fcbSkilled,fcbSemiSkilled,fcbUnskilled,folbCast},
//				{fcbOneTimeDeduction,fdbOneTimeDedectionAmount,fdbDateOfBirth,ftbAgeAsOnDate},	/** date 11.8.2018 added by komal for one time deduction flag and amount **/
//				{fdbDateOfRetirement,fcbIsReliever,fproject,foblSiteLocationList},
//				{fpoliceVerfDocUplCom,fdbExpiryDate,fIncentivePercent},
//				
//				{fgroupingFullandFinal},/** date 2.8.2018 added by komal to store employee resignation information **/
//				{fdbnoticePeriod ,fcbResigned , fdbResignationDate,fdbLastWorkingDay},	/** date 18.7.2018 added by komal to store employee resignation information **/
//				{fcbAbsconded , fdbAbscondDate , fcbTerminated , fdbTerminationDate},/** date 2.8.2018 added by komal to store employee abscond and terminate information **/
//				{ftbReasonForLeaving,fcbRealeaseForPayment,ftbFnfStatus,fdbFnFDate},
//				
//				
//				
//				//{fgroupingApprover},
//				//{fapprovers,faddRowApprover},
//				//{fapproverTable},
//				{fgroupingEmployeePayrollInformation},
//				{ftbbankName,ftbranch,ftIIFC,fibbankAccountNo},
//				
//				{fcbPreviousEsicNumber,fibESICcode,fcbPreviousPfNumber,ftppfNumber},
//				{fibAadharNumber,fibPanNo,ftbUANno},
//				
//				{fgroupingPreviousWorkInformation},
//				{faddrowprevious},
//				{fpreviousWork},
//				{fuploadExperienceDoc},
//				
//				{fgroupingEducationalQualifiaction},
//				{faddrowinqual},
//				{feducationalQualification},
//				{fuploadEducationalDoc},
//				
//				{fgroupingEmployeeContactInformation},
//				{fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2,fetbEmail},
//				{fuploadaddressDoc},
//				
//				{fgroupingEmployeeAddress1},
//				{fbtnCopySecondaryAdd},
//				{faddressComposite},
//				
//				{fgroupingEmployeeAddress2},
//				{fbtnCopyPrimaryAdd},
//				{fsecondoryAddressComposite},
//				
//				{fgroupingpassport},
//				{fpassPortComposite},
//				
//				{fgroupingSocialInformation},
//				{fsocialInfoComposite},
//				
//				{fgroupingBranchInformation},	
//				{fbranchLis,faddBranchButton},
//				{fempBranchTable},
//				
//				{fgroupingUploadPhoto},
//				{fuploadComposite},
//				
//				{fgroupingArticleInformation},
//				{ftbTaxTypeInfoComposite},
//				
//				
//				
////				{fgroupingEmployeeProjectHistory},
////				{femployeeProjectHistoryTable},
//				{fgroupingEmployeeProjectHistory},
//				{fbtnAddProject},
//				{femployeeProjectHistoryTable},
//				
//				{fgroupingTrainingDetails},
//				{fbtnAddTrainingDetails},
//				{ftrainingTbl},
//				
//				{fgroupingCTCAllocation},
//				{folbCTCComponents,fdbCTCAmtValue,fdbFromDateForCTC,fdbToDateForCTC},
//				{fbtAddCtcValue},
//				{fempCTCTemplateTable},
//				
//				{fgroupingNomineeDetails},
//				{ftbNomineeName,fibNomineeAge,ftbNomineeRelation,ftbNomineeAddress},
//				
//				{fgroupingHappayCard},
//				{ftbHappayCardUserId,ftbHappayCardUserName,ftbHappayCardWalletNumber},
//				
//				{fgroupingFingerRegistrationDetails},
//				/***Date 29-7-2019 Commented "Finger Registered by" and "Finger Registered Date" 
//				 * raised by sasha ***/
////				{fcbIsFingerRegistered,ftbFingerRegisteredBy,fdbFingerRegisteredDate,fcbOutstationEmp},
//				{fcbIsFingerRegistered,fcbOutstationEmp},
//				{fuploadEmpSignature},
//				
//				{fgroupingpromotion},
//				{fbtnAddPromotion},
//				{fpromotionTbl},
//				
//				{fgroupinguploaddocuments},
//				{fuploadComposite1,fuploadComposite2},
//				
//				{fgroupingdisabilitydetails},
//				{fcbIsDisabled,fdbDisabilityPercentage,fuploadDisablityDoc},
//				{ftaDisabilityRemark},
//				{ftbSchemeCertificateNumber,ftbPensionPaymentNumber},
//				{ftbCreatedBy,fdbCreationDate,ftbUpdatedBy,fdbUpdationDate}
//				
//		};this.fields=formfield;
		
		
			FormField[][] formfield = {  
					{fgroupingEmployeeInformation},
					{folbemployeeId,ftbfullName,flbBranch,fdbJoiningDate,fdbDateOfBirth},
					{flbGender,ftbHusbandName,fapprovName,fapprovedStatus,ftbAgeAsOnDate},
					{fpnbCellPhoneNo1,fetbEmail,folbemploymentType,flbEmpGrp,fcbIsReliever},
					{folbDepartment,flbRole,flbDesignation,folbreportsTo,folbcNumberRange},
					{fcbStatus,flbMaritialStatus,fbloodGroup,fuploadComposite},
					
					
					//Other info
					{fgroupingOtherInfo},
					{folbcountry,faccesscard,fidCardNumber,fcbOneTimeDeduction,fdbOneTimeDedectionAmount},
					{fsalary,monthlyWorkHrs,foblSalAuthBy,fIncentivePercent,foblInterviewedBy},
					{fcbSkilled,fcbSemiSkilled,fcbUnskilled,fdbDateOfRetirement,folbRefEmployee},
					{ftbLanguageSpoken,folbCast,ftbHappayCardUserId,ftbHappayCardUserName,ftbHappayCardWalletNumber},
					{fpoliceVerfDocUplCom,fdbExpiryDate,fuploadComposite1,fuploadComposite2},	
					{fpnbLandlinePhone,fpnbCellPhoneNo2,fuploadaddressDoc},
					{fseperatorPassport},
					{fpassPortComposite},		
					{fseperatorSocial},
					{fsocialInfoComposite},
				
					
					//Address
					{fgroupingEmployeeAddress1},
					{fseperatorAddr1},
					{fbtnCopySecondaryAdd},
					{faddressComposite},
					{fseperatorAddr2},
					{fbtnCopyPrimaryAdd},
					{fsecondoryAddressComposite},
					
					//Article info
					{fgroupingArticleInformation},
					{ftbTaxTypeInfoComposite},
					
					//Branch info
					{fgroupingBranchInformation},	
					{fbranchLis,faddBranchButton},
					{fempBranchTable},
					
					//Nominee Info
					{fgroupingNomineeDetails},
					{ftbNomineeName,fibNomineeAge,ftbNomineeRelation,ftbNomineeAddress},
					
					//payroll info
					{fgroupingEmployeePayrollInformation},
					{ftbbankName,ftbranch,ftIIFC,fibbankAccountNo},
					{fcbPreviousEsicNumber,fibESICcode,fcbPreviousPfNumber,ftppfNumber},
					{fibAadharNumber,fibPanNo,ftbUANno},
					
					{fproject,foblSiteLocationList,foblShiftName,foblOtList},
					{folbcalender,folbleavegroup},
					{fseperatorProjectHistory},
					{fbtnAddProject},
					{femployeeProjectHistoryTable},
					
					{fgroupingFullandFinal},/** date 2.8.2018 added by komal to store employee resignation information **/
					{fdbnoticePeriod ,fcbResigned , fdbResignationDate,fdbLastWorkingDay},	/** date 18.7.2018 added by komal to store employee resignation information **/
					{fcbAbsconded , fdbAbscondDate , fcbTerminated , fdbTerminationDate},/** date 2.8.2018 added by komal to store employee abscond and terminate information **/
					{ftbReasonForLeaving,fcbRealeaseForPayment,ftbFnfStatus,fdbFnFDate},
					
					//experience info
					{fgroupingPreviousWorkInformation},
					{faddrowprevious},
					{fpreviousWork},
					{fuploadExperienceDoc},
					
					//education info
					{fgroupingEducationalQualifiaction},
					{faddrowinqual},
					{feducationalQualification},
					{fuploadEducationalDoc},
					
					//training info
					{fgroupingTrainingDetails},
					{folbTrainingType,ftbTrainingDescription,ftrainingDate,fbtnAddTrainingDetails},
					{ftrainingTbl},
					
					{fgroupingFingerRegistrationDetails},					
					/***Date 29-7-2019 Commented "Finger Registered by" and "Finger Registered Date" 
					 * raised by sasha ***/
//					{fcbIsFingerRegistered,ftbFingerRegisteredBy,fdbFingerRegisteredDate,fcbOutstationEmp},
					{fcbIsFingerRegistered,fcbOutstationEmp},
					{fuploadEmpSignature},
					
					{fgroupingpromotion},
					{fbtnAddPromotion},
					{fpromotionTbl},
					
					{fgroupingCTCAllocation},
					{fnoteCTCAllocation},
					{folbCTCComponents,fdbCTCAmtValue,fdbFromDateForCTC,fdbToDateForCTC},
					{fbtAddCtcValue},
					{fempCTCTemplateTable},
					
					{fgroupingdisabilitydetails},
					{fcbIsDisabled,fdbDisabilityPercentage,fuploadDisablityDoc},
					{ftaDisabilityRemark},
					{ftbSchemeCertificateNumber,ftbPensionPaymentNumber},
					
					{fgroupingTimeline},
					{ftbCreatedBy,fdbCreationDate,ftbUpdatedBy,fdbUpdationDate},
					
			};this.fields=formfield;
		
		}else
		{
//FormField[][] formfield = {   
//					
//					{fgroupingEmployeeInformation},
//					{folbemployeeId,folbcountry,folbDepartment,folbemploymentType},
////					{ftbFirstName,ftbMiddleName,ftbLastName,fdbDateOfBirth},
////					{flbBranch,flbDesignation,flbRole,fdbJoiningDate},
////					{folbreportsTo,faccesscard,fidCardNumber,fbloodGroup},
////					{fcbStatus},
//					
//					//vijay      
//					{ftbfullName,flbBranch,flbDesignation,folbRefEmployee},
//					{flbRole,fdbJoiningDate,folbreportsTo,faccesscard},
//					{fidCardNumber,fbloodGroup,fsalary,monthlyWorkHrs},
//					{flbGender,folbcNumberRange,flbMaritialStatus,fcbStatus},
//					{flbEmpGrp,foblShiftName,foblInterviewedBy,foblSalAuthBy},
//					{fcbSkilled,fcbSemiSkilled,fcbUnskilled,ftbLanguageSpoken},
//					{fcbOneTimeDeduction , fdbOneTimeDedectionAmount,fdbDateOfBirth,ftbAgeAsOnDate},	/** date 11.8.2018 added by komal for one time deduction flag and amount **/
//					{fdbDateOfRetirement,fcbIsReliever,fproject,foblSiteLocationList},
//					{fpoliceVerfDocUplCom,fdbExpiryDate,folbCast,fIncentivePercent},
//					
//					{fgroupingFullandFinal},/** date 2.8.2018 added by komal to store employee resignation information **/
//					{fdbnoticePeriod ,fcbResigned , fdbResignationDate,fdbLastWorkingDay},	/** date 18.7.2018 added by komal to store employee resignation information **/
//					{fcbAbsconded , fdbAbscondDate , fcbTerminated , fdbTerminationDate},/** date 2.8.2018 added by komal to store employee abscond and terminate information **/
//					{ftbReasonForLeaving,fcbRealeaseForPayment,ftbFnfStatus,fdbFnFDate},
//					//{fgroupingApprover},
//					//{fapprovers,faddRowApprover},
//					//{fapproverTable},
//					{fgroupingEmployeePayrollInformation},
//					{ftbbankName,ftbranch,ftIIFC,fibbankAccountNo},
//					
////					{fibESICcode,fibPanNo,ftppfNumber,ftbUANno},
////					{fibAadharNumber},
//					{fcbPreviousEsicNumber,fibESICcode,fcbPreviousPfNumber,ftppfNumber},
//					{fibAadharNumber,fibPanNo,ftbUANno},
//					
//					{fgroupingPreviousWorkInformation},
//					{faddrowprevious},
//					{fpreviousWork},
//					{fuploadExperienceDoc},
			
//					{fgroupingEducationalQualifiaction},
//					{faddrowinqual},
//					{feducationalQualification},
//					{fuploadEducationalDoc},
			
//					{fgroupingEmployeeContactInformation},
//					{fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2,fetbEmail},
//					{fuploadaddressDoc},
//					
//					{fgroupingEmployeeAddress1},
//					{fbtnCopySecondaryAdd},
//					{faddressComposite},
			
//					{fgroupingEmployeeAddress2},
//					{fbtnCopyPrimaryAdd},
//					{fsecondoryAddressComposite},
//					
//					{fgroupingpassport},
//					{fpassPortComposite},
			
//					{fgroupingSocialInformation},
//					{fsocialInfoComposite},
			
//					{fgroupingBranchInformation},	
//					{fbranchLis,faddBranchButton},
//					{fempBranchTable},
			
//					{fgroupingUploadPhoto},
//					{fuploadComposite},
			
//					{fgroupingArticleInformation},
//					{ftbTaxTypeInfoComposite},
			
//					{fgroupingEmployeeProjectHistory},
//					{femployeeProjectHistoryTable},
			
//					{fgroupingCTCAllocation},
//					{folbCTCComponents,fdbCTCAmtValue,fdbFromDateForCTC,fdbToDateForCTC},
//					{fbtAddCtcValue},
//					{fempCTCTemplateTable},
			
//					{fgroupingNomineeDetails},
//					{ftbNomineeName,fibNomineeAge,ftbNomineeRelation,ftbNomineeAddress},
//					
//					{fgroupingHappayCard},
//					{ftbHappayCardUserId,ftbHappayCardUserName,ftbHappayCardWalletNumber},
//					
//					{fgroupingFingerRegistrationDetails},
//					
//					
//					/***Date 29-7-2019 Commented "Finger Registered by" and "Finger Registered Date" 
//					 * raised by sasha ***/
////					{fcbIsFingerRegistered,ftbFingerRegisteredBy,fdbFingerRegisteredDate,fcbOutstationEmp},
//					{fcbIsFingerRegistered,fcbOutstationEmp},
//					
//					
//					{fuploadEmpSignature},
//					
//					{fgroupingpromotion},
//					{fbtnAddPromotion},
//					{fpromotionTbl},
			
//					{fgroupinguploaddocuments},
//					{fuploadComposite1,fuploadComposite2},
//					
//					{fgroupingdisabilitydetails},
//					{fcbIsDisabled,fdbDisabilityPercentage,fuploadDisablityDoc},
//					{ftaDisabilityRemark},
//					{ftbSchemeCertificateNumber,ftbPensionPaymentNumber},
//					{ftbCreatedBy,fdbCreationDate,ftbUpdatedBy,fdbUpdationDate}
//					
//					
//		};this.fields=formfield;
			
			
			
		//New 
FormField[][] formfield = {   
//					{fgroupingEmployeeInformation},
//					{ftbfullName,flbBranch,fdbJoiningDate,fdbDateOfBirth,ftbAgeAsOnDate},
//					{flbGender,fpnbCellPhoneNo1,fetbEmail,folbemploymentType,flbEmpGrp},
//					{folbDepartment,flbRole,flbDesignation,folbreportsTo,fcbIsReliever},
//					{fcbStatus,flbMaritialStatus,folbcNumberRange,fbloodGroup,fuploadComposite},
		
		
					{fgroupingEmployeeInformation},
					{folbemployeeId,ftbfullName,flbBranch,fdbJoiningDate,fdbDateOfBirth},
					{flbGender,fpnbCellPhoneNo1,fetbEmail,folbemploymentType,ftbAgeAsOnDate},
					{flbEmpGrp,folbDepartment,flbRole,flbDesignation,folbreportsTo,},
					{fcbStatus,fcbIsReliever,flbMaritialStatus,folbcNumberRange,fbloodGroup},
					{fuploadComposite},
		
										
					//Address
					{fgroupingEmployeeAddress1},
					{fuploadaddressDoc},
					{fseperatorAddr1},
					{fbtnCopySecondaryAdd},
					{faddressComposite},
					{fseperatorAddr2},
//					{fgroupingEmployeeAddress2},
					{fbtnCopyPrimaryAdd},
					{fsecondoryAddressComposite},
					
					
					//Article info
					{fgroupingArticleInformation},
					{ftbTaxTypeInfoComposite},
					
					//branch info
					{fgroupingBranchInformation},	
					{fbranchLis,faddBranchButton},
					{fempBranchTable},
					
					//Other info
					{fgroupingOtherInfo},
					{folbcountry,faccesscard,fidCardNumber,fcbOneTimeDeduction,fdbOneTimeDedectionAmount},
					{fsalary,monthlyWorkHrs,foblSalAuthBy,fIncentivePercent,foblInterviewedBy},
					{fcbSkilled,fcbSemiSkilled,fcbUnskilled,fdbDateOfRetirement,folbRefEmployee},
					{ftbLanguageSpoken,folbCast,fpnbLandlinePhone,fpnbCellPhoneNo2},
					{fpoliceVerfDocUplCom,fdbExpiryDate,fuploadComposite1,fuploadComposite2},	
					{fseperatorHappyCard},
					{ftbHappayCardUserId,ftbHappayCardUserName,ftbHappayCardWalletNumber},
					{fseperatorPassport},
					{fpassPortComposite},		
					{fseperatorSocial},
					{fsocialInfoComposite},
					
					//Nominee info
					{fgroupingNomineeDetails},
					{ftbNomineeName,fibNomineeAge,ftbNomineeRelation,ftbNomineeAddress},
					
					//payroll info
					{fgroupingEmployeePayrollInformation},
					{ftbbankName,ftbranch,ftIIFC,fibbankAccountNo},
					{fcbPreviousEsicNumber,fibESICcode,fcbPreviousPfNumber,ftppfNumber},
					{fibAadharNumber,fibPanNo,ftbUANno},
					
					{fproject,foblSiteLocationList,foblShiftName,foblOtList},
					{folbcalender,folbleavegroup},
					{fseperatorProjectHistory},
					{femployeeProjectHistoryTable},
					
					
					//full and final
					{fgroupingFullandFinal},/** date 2.8.2018 added by komal to store employee resignation information **/
					{fdbnoticePeriod ,fcbResigned , fdbResignationDate,fdbLastWorkingDay},	/** date 18.7.2018 added by komal to store employee resignation information **/
					{fcbAbsconded , fdbAbscondDate , fcbTerminated , fdbTerminationDate},/** date 2.8.2018 added by komal to store employee abscond and terminate information **/
					{ftbReasonForLeaving,fcbRealeaseForPayment,ftbFnfStatus,fdbFnFDate},
					
					//experience info
					{fgroupingPreviousWorkInformation},
					{faddrowprevious},
					{fpreviousWork},
					{fuploadExperienceDoc},
					
					//education info
					{fgroupingEducationalQualifiaction},
					{faddrowinqual},
					{feducationalQualification},
					{fuploadEducationalDoc},
					
					//training info
					{fgroupingTrainingDetails},
					{folbTrainingType,ftbTrainingDescription,ftrainingDate,fbtnAddTrainingDetails},
					{ftrainingTbl},
										
					{fgroupingFingerRegistrationDetails},
					{fcbIsFingerRegistered,fcbOutstationEmp,fuploadEmpSignature},					
					
					{fgroupingpromotion},
					{fbtnAddPromotion},
					{fpromotionTbl},
					
					{fgroupingCTCAllocation},
					{fnoteCTCAllocation},
					{folbCTCComponents,fdbCTCAmtValue,fdbFromDateForCTC,fdbToDateForCTC},
					{fbtAddCtcValue},
					{fempCTCTemplateTable},
					
					{fgroupingdisabilitydetails},
					{fcbIsDisabled,fdbDisabilityPercentage,fuploadDisablityDoc},
					{ftaDisabilityRemark},
					{ftbSchemeCertificateNumber,ftbPensionPaymentNumber},
										
					{fgroupingTimeline},
					{ftbCreatedBy,fdbCreationDate,ftbUpdatedBy,fdbUpdationDate},
		};this.fields=formfield;
			
			
		}

				
	}
		

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Employee model) 
	{
		/**
		 * @author Anil
		 * @since 22-05-2020
		 */
		if(tbFnfStatus.getValue().equals("Inprocess")){
			Console.log("FNF status 5");
			if(employeeobj!=null){
				Console.log("FNF status 6 ");
				Console.log("NP "+dbnoticePeriod.getValue()+" NPE "+employeeobj.getNoticePeriod());
				Console.log("REG DT "+dbResignationDate.getValue()+" REG DT E "+employeeobj.getResignationDate());
				Console.log("LWD DT "+dbLastWorkingDay.getValue()+" LWD DT E "+employeeobj.getLastWorkingDate());
				Console.log("FNF status 6 ");
				if(!dbnoticePeriod.getValue().equals(employeeobj.getNoticePeriod())){
					model.setFnfStatus("Initiated");
					tbFnfStatus.setValue(model.getFnfStatus());
					Console.log("FNF status 7");
				}else if(dbResignationDate.getValue()!=null&&!dbResignationDate.getValue().equals(employeeobj.getResignationDate())){
					model.setFnfStatus("Initiated");
					tbFnfStatus.setValue(model.getFnfStatus());
					Console.log("FNF status 7a");
				}else if(dbLastWorkingDay.getValue()!=null&&!dbLastWorkingDay.getValue().equals(employeeobj.getLastWorkingDate())){
					model.setFnfStatus("Initiated");
					tbFnfStatus.setValue(model.getFnfStatus());
					Console.log("FNF status 7b");
				}else{
					model.setFnfStatus(tbFnfStatus.getValue());
					Console.log("FNF status 8");
				}
			}else{
				Console.log("FNF status 4");
				model.setFnfStatus(tbFnfStatus.getValue());
			}
		}else{
			Console.log("FNF status 3");
			model.setFnfStatus(tbFnfStatus.getValue());
		}

//		if(tbFirstName.getValue()!=null)
//			model.setFirstName(tbFirstName.getValue());
//		if(tbMiddleName.getValue()!=null)
//			model.setMiddleName(tbMiddleName.getValue());
//		if(tbLastName.getValue()!=null)
//			model.setLastName(tbLastName.getValue());
//
//		model.setFullname();
		
		
		/* 
		 * above old code commented by vijay to change employee first middle and last to fullName and bellow code added
		 */
		if(tbfullName.getValue()!=null){
			model.setFullname(tbfullName.getValue().trim());
		}	
		/** Added By Viraj Date 11-10-2018 **/
		if(tbHusbandName.getValue()!= null)
		{
			model.setHusbandName(tbHusbandName.getValue().trim());
		}
		/** Ends **/
		if(dbDateOfBirth.getValue()!=null)
			model.setDob(dbDateOfBirth.getValue());
		
		if(lbBranch.getValue(lbBranch.getSelectedIndex())!=null)
			model.setBranchName(lbBranch.getValue(lbBranch.getSelectedIndex()));
		
		if(olbDepartMent.getValue()!=null)
			model.setDepartMent(olbDepartMent.getValue());
		if(lbDesignation.getValue()!=null)
			model.setDesignation(lbDesignation.getValue());
		if(lbRole.getValue()!=null)
			model.setRoleName(lbRole.getValue());
		if(dbJoiningDate.getValue()!=null)
			model.setJoinedAt(dbJoiningDate.getValue());
		
		if(reportsTo.getValue()!=null)
			model.setReportsTo(reportsTo.getValue());
		
		if(cbStatus.getValue()!=null)
			model.setStatus(cbStatus.getValue());
		if(pnbLandlinePhone.getValue()!=null)
			model.setLandline(pnbLandlinePhone.getValue());
		if(pnbCellPhoneNo1.getValue()!=null)
			model.setCellNumber1(pnbCellPhoneNo1.getValue());
		if(pnbCellPhoneNo2.getValue()!=null)
			model.setCellNumber2(pnbCellPhoneNo2.getValue());
		if(etbEmail.getValue()!=null)
			model.setEmail(etbEmail.getValue());
		if(addressComposite.getValue()!=null)
			model.setAddress(addressComposite.getValue());
		
		if(secondoryAddressComposite.getValue()!=null)
			model.setSecondaryAddress(secondoryAddressComposite.getValue());
		
			model.setPhoto(uploadComposite.getValue());
		if(socialInfoComposite.getValue()!=null)
			model.setSocialInfo(socialInfoComposite.getValue());
		if(educationalQualification.getValue()!=null)
			model.setEducationalInfo(educationalQualification.getValue());
		//if(approverTable.listDataProvider.getList().size()!=0)
			//model.setApprovers(approverTable.listDataProvider.getList());
		if(previousWorkExperience.getValue()!=null)
			model.setPreviousCompanyHistory(previousWorkExperience.getValue());
		
		if(empBranchTable.getValue()!=null)
			model.setEmpBranchList(empBranchTable.getValue());
		
		
		if(passPortInformation.getValue()!=null)
			model.setPassPortInformation(passPortInformation.getValue());
		if(idCardNumber.getValue()!=null)
			model.setIdCardNo(idCardNumber.getValue());
		if(accessCardNo.getValue() != null)
			model.setAccessCardNo(accessCardNo.getValue());

		if(olbDepartMent.getValue()!=null)
			model.setDepartMent(olbDepartMent.getValue());
		if(olbEmploymentType.getValue()!=null)
			model.setEmployeeType(olbEmploymentType.getValue());

		if(olbEmploymentType.getValue()!=null)
			model.setEmployeeType(olbEmploymentType.getValue());


		if(tbbankName.getValue()!=null)
			model.setEmployeeBankName(tbbankName.getValue());
		if(ibbankAccountNo.getValue()!=null)
		     model.setEmployeeBankAccountNo(ibbankAccountNo.getValue());
		
		if(tbUANno.getValue()!=null)
			model.setUANno(tbUANno.getValue());
		
			
		if(ibESICcode.getValue()!=null)
		   model.setEmployeeESICcode(ibESICcode.getValue());
		if(ibPanNo.getValue()!=null)
		  model.setEmployeePanNo(ibPanNo.getValue());
		
		

		if(olbcountry.getValue()!=null)
			model.setCountry(olbcountry.getValue());
		
		if(this.bankbranchName.getValue()!=null)
			model.setBankBranch(bankbranchName.getValue());
		
		
		if(this.iifcCode.getValue()!=null)
			model.setIfscCode(iifcCode.getValue());
		
		if(this.ppfNumber.getText()!=null)
			model.setPPFNumber(this.ppfNumber.getValue());
		
		if(this.bloodGroup.getValue()!=null)
			model.setBloodGroup(this.bloodGroup.getValue());
		if(tbArticleTypeInfoComposite.getValue()!=null)
			model.setArticleTypeDetails(tbArticleTypeInfoComposite.getValue());
		
		
		//***********anil ******
		if(uploadAddressDoc.getValue()!=null){
			System.out.println("UM ADDRESS DOC");
			model.setUploadAddDet(uploadAddressDoc.getValue());
		}else{
			model.setUploadAddDet(null);
		}
		if(uploadEducationalDoc.getValue()!=null){
			System.out.println("UM EDU DOC");
			model.setUploadEduDet(uploadEducationalDoc.getValue());
		}else{
			model.setUploadEduDet(null);
		}
		if(uploadExperienceDoc.getValue()!=null){
			System.out.println("UM EXP DOC");
			model.setUploadExpDet(uploadExperienceDoc.getValue());
		}else{
			model.setUploadExpDet(null);
		}
		
		
		if(approverName.getSelectedIndex()!=0)
		{
			model.setApproverName(approverName.getValue(approverName.getSelectedIndex()));
		}
		
		if(approvedStatus.getValue().trim()!=null)
		{
			model.setApproveStatus(approvedStatus.getValue().trim());
		}
		
		if(salary.getValue()!=null)
		{
			model.setSalaryAmt(salary.getValue());
		}
		
		if(monthlyWorkingHrs.getValue()!=null)
		{
			model.setMonthlyWorkingHrs(monthlyWorkingHrs.getValue());
		}
		
		
		if(lbGender.getSelectedIndex()!=0){
			model.setGender(lbGender.getValue(lbGender.getSelectedIndex()));
		}else{
			model.setGender("");
		}
		
		if(olbcNumberRange.getValue()!=null){
			model.setNumberRange(olbcNumberRange.getValue(olbcNumberRange.getSelectedIndex()));
		}
		
		if (this.employeeProjectHistoryTable.getValue().size() != 0) {
			model.setEmployeeProjectHistoryList(employeeProjectHistoryTable.getValue());
		}
		if(this.lbMaritialStatus.getSelectedIndex()!=0){
			model.setMaritalStatus(lbMaritialStatus.getValue(lbMaritialStatus.getSelectedIndex()));
		}
		
		if(this.olbRefEmployee.getSelectedIndex()!=0){
			model.setRefEmployee(olbRefEmployee.getValue());
		}
		
		if(this.empCTCTemplateTable.getValue().size()!=0){
			model.setEmployeeCTCList(empCTCTemplateTable.getValue());
		}
		
		if(project.getValue()!=null){
			model.setProjectName(project.getValue());
		}else{
			model.setProjectName("");
		}
		
		if(dbLastWorkingDay.getValue()!=null){
			model.setLastWorkingDate(dbLastWorkingDay.getValue());
		}else{
			model.setLastWorkingDate(null);
		}
		/** date 18.7.2018 added by komal to store employee resignation information **/
		model.setResigned(cbResigned.getValue());
		if(dbResignationDate.getValue()!=null){
			model.setResignationDate(dbResignationDate.getValue());
		}else{
			model.setResignationDate(null);
		}
		if(dbnoticePeriod.getValue()!=null){
			model.setNoticePeriod(dbnoticePeriod.getValue());
		}else{
			model.setNoticePeriod(0);
		}
		model.setAbsconded(cbAbsconded.getValue());
		if(dbAbscondDate.getValue()!=null){
			model.setAbscondDate(dbAbscondDate.getValue());
		}else{
			model.setAbscondDate(null);
		}
		model.setTerminated(cbTerminated.getValue());
		if(dbTerminationDate.getValue()!=null){
			model.setTerminationDate(dbTerminationDate.getValue());
		}else{
			model.setTerminationDate(null);
		}
		model.setRealeaseForPayment(cbReleaseForPayment.getValue());
		/** date 11.8.2018 added by komal for one time deduction flag and amount **/
		model.setOneTimeDeduction(cbOneTimeDeduction.getValue());
		if(dbOneTimeDedectionAmount.getValue()!=null){
			model.setOneTimeDeductionAmount(dbOneTimeDedectionAmount.getValue());
		}
		
		if(trainingTbl.getValue()!=null){
			model.setTrainingInfoList(trainingTbl.getValue());
		}
		if(oblShiftName.getValue()!=null){
			model.setShiftName(oblShiftName.getValue());
			Shift shift=oblShiftName.getSelectedItem();
			model.setShiftFromTime(shift.getFromTime()+"");
			model.setShiftToTime(shift.getToTime()+"");
			
		}else{
			model.setShiftName("");
			model.setShiftFromTime("");
			model.setShiftToTime("");
		}
		if(oblInterviewedBy.getValue()!=null){
			model.setInterviewedBy(oblInterviewedBy.getValue());
		}else{
			model.setInterviewedBy("");
		}
		if(oblSalAuthBy.getValue()!=null){
			model.setSalaryAuthorizedBy(oblSalAuthBy.getValue());
		}else{
			model.setSalaryAuthorizedBy("");
		}
		
		if(tbNomineeName.getValue()!=null){
			model.setNomineeName(tbNomineeName.getValue());
		}else{
			model.setNomineeName("");
		}
		if(tbNomineeRelation.getValue()!=null){
			model.setNomineeRelation(tbNomineeRelation.getValue());
		}else{
			model.setNomineeRelation("");
		}
		if(tbNomineeAddress.getValue()!=null){
			model.setNomineeAddress(tbNomineeAddress.getValue());
		}else{
			model.setNomineeAddress("");
		}
		
		if(ibNomineeAge.getValue()!=null){
			model.setNomineeAge(ibNomineeAge.getValue());
		}else{
			model.setNomineeAge(0);
		}
		
		if(tbHappayCardUserId.getValue()!=null){
			model.setHappayCardUserId(tbHappayCardUserId.getValue());
		}else{
			model.setHappayCardUserId("");
		}
		if(tbHappayCardUserName.getValue()!=null){
			model.setHappayCardUserName(tbHappayCardUserName.getValue());
		}else{
			model.setHappayCardUserName("");
		}
		if(tbHappayCardWalletNumber.getValue()!=null){
			model.setHappayCardWalletNumber(tbHappayCardWalletNumber.getValue());
		}else{
			model.setHappayCardWalletNumber("");
		}
		
		model.setFingerDataPresent(cbIsFingerRegistered.getValue());
		if(tbFingerRegisteredBy.getValue()!=null){
			model.setFingerRegisteredBy(tbFingerRegisteredBy.getValue());
		}else{
			model.setFingerRegisteredBy("");
		}
		if(dbFingerRegisteredDate.getValue()!=null){
			model.setFingerRegisterationDate(dbFingerRegisteredDate.getValue());
		}
		
		if(ibAadharNumber.getValue()!=null&&!ibAadharNumber.getValue().equals("")){
			model.setAadharNumber(Long.parseLong(ibAadharNumber.getValue()));
		}else{
			model.setAadharNumber(0);
		}
		
		model.setPreviousPfNumber(cbPreviousPfNumber.getValue());
		model.setPreviousEsicNumber(cbPreviousEsicNumber.getValue());
		
		model.setSkilled(cbSkilled.getValue());
		model.setSemiSkilled(cbSemiSkilled.getValue());
		model.setUnskilled(cbUnskilled.getValue());
		
		if(tbLanguageSpoken.getValue()!=null){
			model.setLanuageSpoken(tbLanguageSpoken.getValue());
		}
		
		if(uploadEmpSignature.getValue()!=null){
			System.out.println("UM EMP SIGN");
			model.setUploadSign(uploadEmpSignature.getValue());
		}else{
			model.setUploadSign(null);
		}
		
		if(promotionTbl.getValue()!=null){
			model.setPromotionHistoryList(promotionTbl.getValue());
			PromotionDetails promotion=null;
			if(promotionTbl.getValue().size()!=0){
				if(promotionTbl.getValue().size()==1){
					promotion=promotionTbl.getValue().get(0);
				}else{
					int size=promotionTbl.getValue().size();
					promotion=promotionTbl.getValue().get(size-1);
				}
				if(promotion!=null){
					if(!lbDesignation.getValue().equals(promotion.getCurrentDesignation())
							&&salary.getValue()!=null&&salary.getValue()!=promotion.getCurrentSalary()){
						lbDesignation.setValue(promotion.getCurrentDesignation());
						model.setDesignation(promotion.getCurrentDesignation());
						salary.setValue(promotion.getCurrentSalary());
						model.setSalaryAmt(promotion.getCurrentSalary());
					}
				}
			}
		}
		
		if(dbDateOfRetirement.getValue()!=null){
			model.setDateOfRetirement(dbDateOfRetirement.getValue());
		}else{
			model.setDateOfRetirement(null);
		}
		
		if(uploadComposite1.getValue()!=null){
			System.out.println("UM UPLOAD1");
			model.setDocumentUpload1(uploadComposite1.getValue());
		}else{
			model.setDocumentUpload1(null);
		}
		if(uploadComposite2.getValue()!=null){
			System.out.println("UM UPLOAD1");
			model.setDocumentUpload2(uploadComposite2.getValue());
		}else{
			model.setDocumentUpload2(null);
		}
		
		model.setDisabled(cbIsDisabled.getValue());
		if(dbDisabilityPercentage.getValue()!=null){
			model.setDisablityPercentage(dbDisabilityPercentage.getValue());
		}else{
			model.setDisablityPercentage(0);
		}
		if(uploadDisablityDoc.getValue()!=null){
			model.setUploadDisabilityDoc(uploadDisablityDoc.getValue());
		}else{
			model.setUploadDisabilityDoc(null);
		}
		if(taDisabilityRemark.getValue()!=null){
			model.setDisabiltyRemark(taDisabilityRemark.getValue());
		}
		
		/**
		 * Date : 13-12-2018 By ANIl
		 */
		if(LoginPresenter.loggedInUser!=null){
			model.setUpdatedBy(LoginPresenter.loggedInUser);
		}
		
		if(tbSchemeCertificateNumber.getValue()!=null){
			model.setSchemeCertificateNum(tbSchemeCertificateNumber.getValue());
		}
		if(tbPensionPaymentNumber.getValue()!=null){
			model.setPensionPaymentNum(tbPensionPaymentNumber.getValue());
		}
		
		if(cbOutstationEmp.getValue()!=null){
			model.setOutstationEmployee(cbOutstationEmp.getValue());
		}
		
		if(lbEmpGrp.getSelectedIndex()!=0){
			model.setEmpGrp(lbEmpGrp.getValue(lbEmpGrp.getSelectedIndex()));
		}else{
			model.setEmpGrp("");
		}
		
		if(tbReasonForLeaving.getValue()!=null){
			model.setReasonForLeaving(tbReasonForLeaving.getValue());
		}
		
		if(tbFnfStatus.getValue()==null||tbFnfStatus.getValue().equals("")){
			model.setFnfStatus(getFnfStatus(model));
			tbFnfStatus.setValue(model.getFnfStatus());
		}else{
			model.setFnfStatus(tbFnfStatus.getValue());
		}
		
		if(dbFnFDate.getValue()!=null){
			model.setFnfDate(model.getFnfDate());
			dbFnFDate.setValue(model.getFnfDate());
		}
		
		if(employeeobj!=null){
			model.setUpdatePfNum(updatePfAndEsicNumberOnPayslip());
		}
		
		if(olbCaste.getValue()!=null){
			model.setCaste(olbCaste.getValue());
		}
		model.setReliever(cbIsReliever.getValue());
			
//		if(policeVerfDocUplCom.getValue()!=null){
			model.setPoliceVerfDoc(policeVerfDocUplCom.getValue());
//		}
//		if(dbExpiryDate.getValue()!=null){
			model.setExpiryDate(dbExpiryDate.getValue());
//		}else{
//			model.setExpiryDate(null);
//		}
			
		if(oblSiteLocationList.getValue()!=null){
			model.setSiteLocation(oblSiteLocationList.getValue());
		}else{
			model.setSiteLocation(null);
		}
		
		if(incentivePercent.getValue()!=null)
			model.setIncentivePercent(incentivePercent.getValue());
		
		if(oblOtList.getValue()!=null)
			model.setOvertimeName(oblOtList.getValue());
		else
			model.setOvertimeName("");
		
		if(olbcalender.getValue()!=null)
			model.setCalendar(olbcalender.getValue());
		else
			model.setCalendar("");
		
		if(olbleavegroup.getValue()!=null)
			model.setLeaveGroup(olbleavegroup.getValue());
		else
			model.setLeaveGroup("");
		
		
		
		/**
		 * @author Anil @since 31-03-2021
		 * commented this code now maintaing employee data in global list from common method of save
		 */
		manageEmployeeDropDown(model);
		
		employeeobj=model;
		
		presenter.setModel(model);


	}

	private String getFnfStatus(Employee model) {
		// TODO Auto-generated method stub
		String fnfStatus="";
		if(model.getFnfStatus()==null||model.getFnfStatus().equals("")){
			if(cbResigned.getValue()==true
					||cbAbsconded.getValue()==true
					||cbTerminated.getValue()==true){
				fnfStatus="Initiated";
			}
		}else{
			fnfStatus=model.getFnfStatus();
		}
		return fnfStatus;
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Employee view) 
	{

		employeeobj=view;
		
		employeeId.setValue(view.getCount()+"");
//		if(view.getFirstName()!=null)
//			tbFirstName.setValue(view.getFirstName());
//		if(view.getMiddleName()!=null)
//			tbMiddleName.setValue(view.getMiddleName());
//		if(view.getLastName()!=null)
//			tbLastName.setValue(view.getLastName());
		
		if(view.getFullName()!=null){
			tbfullName.setValue(view.getFullName());
		}
		/** Added by Viraj Date 11-10-2018 **/
		if(view.getHusbandName()!= null)
		{
			tbHusbandName.setValue(view.getHusbandName());
		}
		/** Ends **/
		if(view.getDob()!=null){
			dbDateOfBirth.setValue(view.getDob());
			tbAgeAsOnDate.setValue(calculateAge(dbDateOfBirth.getValue())+"");
		}
		if(view.getBranchName()!=null)
			lbBranch.setValue(view.getBranchName());
		if(view.getDepartMent()!=null)
			olbDepartMent.setValue(view.getDepartMent());
		if(view.getDesignation()!=null)
			lbDesignation.setValue(view.getDesignation());
		if(view.getRoleName()!=null)
			lbRole.setValue(view.getRoleName());
		if(view.getJoinedAt()!=null)
			dbJoiningDate.setValue(view.getJoinedAt());
		if(view.getCountry()!=null)
			olbcountry.setValue(view.getCountry());
		
		if(view.isStatus()!=null)
			cbStatus.setValue(view.isStatus());
		if(view.getLandline()!=null)
			pnbLandlinePhone.setValue(view.getLandline());
		if(view.getCellNumber1()!=null)
			pnbCellPhoneNo1.setValue(view.getCellNumber1());
		if(view.getCellNumber2()!=null)
			pnbCellPhoneNo2.setValue(view.getCellNumber2());
		if(view.getEmail()!=null)
			etbEmail.setValue(view.getEmail());
		if(view.getAddress()!=null)
			addressComposite.setValue(view.getAddress());
		
		if(view.getSecondaryAddress()!=null)
			secondoryAddressComposite.setValue(view.getSecondaryAddress());
		
		if(view.getPhoto()!=null){
			System.out.println("PHOTO NOT NULL..");
			uploadComposite.setValue(view.getPhoto());
		}
		if(view.getSocialInfo()!=null)
			socialInfoComposite.setValue(view.getSocialInfo());
		//if(view.getApprovers()!=null)
			//approverTable.listDataProvider.getList().addAll(view.getApprovers());
		if(view.getEducationalInfo()!=null)
			educationalQualification.setValue(view.getEducationalInfo());
		
		if(view.getEmpBranchList()!=null)
			empBranchTable.setValue(view.getEmpBranchList());
		
		if(view.getPreviousCompanyHistory()!=null)
			previousWorkExperience.setValue(view.getPreviousCompanyHistory());
		if(view.getPassPortInformation()!=null)
			passPortInformation.setValue(view.getPassPortInformation());
		if(view.getIdCardNo()!=null)
			idCardNumber.setValue(view.getIdCardNo());
		if(accessCardNo.getValue() != null)
			accessCardNo.setValue(view.getAccessCardNo());

		if(view.getDepartMent()!=null)
			olbDepartMent.setValue(view.getDepartMent());
		if(view.getEmployeeType()!=null)
			olbEmploymentType.setValue(view.getEmployeeType());
		

		if(view.getEmployeeType()!=null)
			olbEmploymentType.setValue(view.getEmployeeType());

		if(view.getEmployeeBankName()!=null)
			tbbankName.setValue(view.getEmployeeBankName());
		if(view.getEmployeePanNo()!=null)
			ibPanNo.setValue(view.getEmployeePanNo());
		if(view.getEmployeeESICcode()!=null)
		    ibESICcode.setValue(view.getEmployeeESICcode());
		if(view.getEmployeeBankAccountNo()!=null)
		     ibbankAccountNo.setValue(view.getEmployeeBankAccountNo());
		
		if(view.getUANno()!=null)
			tbUANno.setValue(view.getUANno());
			
		if(view.getReportsTo()!=null)
			reportsTo.setValue(view.getReportsTo());
		
		if(view.getIfscCode()!=null)
			iifcCode.setValue(view.getIfscCode());
		
	
		
		if(view.getPPFNaumber()!=null)
			ppfNumber.setValue(view.getPPFNaumber());
		
		if(view.getBankBranch()!=null)
			bankbranchName.setValue(view.getBankBranch());
		
		if(view.getBloodGroup()!=null)
			bloodGroup.setValue(view.getBloodGroup());
		if(view.getArticleTypeDetails()!=null)
			tbArticleTypeInfoComposite.setValue(view.getArticleTypeDetails());
		
		//*********anil *******
		if(view.getUploadAddDet()!=null){
			uploadAddressDoc.setValue(view.getUploadAddDet());
			System.out.println("UPLOAD ADDRESS");
		}
		if(view.getUploadEduDet()!=null){
			uploadEducationalDoc.setValue(view.getUploadEduDet());
			System.out.println("UPLOAD EDUCATIONAL");
		}
		if(view.getUploadExpDet()!=null){
			uploadExperienceDoc.setValue(view.getUploadExpDet());
			System.out.println("UPLOAD EXPERIENCE");
		}

		
		if(view.getApproverName()!=null)
		{
			approverName.setValue(view.getApproverName());
		}
		
		if(view.getApproveStatus()!= null)
		{
			approvedStatus.setValue(view.getApproveStatus());
		}
		
		if(view.getSalaryAmt()!=0)
		{
			salary.setValue(view.getSalaryAmt());
		}
		
		if(view.getMonthlyWorkingHrs()!=0)
		{
			monthlyWorkingHrs.setValue(view.getMonthlyWorkingHrs());
		}
		
		if(view.getGender()!=null){
			for(int i=0;i<lbGender.getItemCount();i++){
				if(lbGender.getItemText(i).equals(view.getGender())){
					lbGender.setSelectedIndex(i);
					break;
				}
			}
		}
		
		if(view.getNumberRange()!=null)
			olbcNumberRange.setValue(view.getNumberRange());
		if(view.getEmployeeProjectHistoryList()!=null){
			if(view.getEmployeeProjectHistoryList().size()!=0)
				this.employeeProjectHistoryTable.setValue(view.getEmployeeProjectHistoryList());
		}
		
		if(view.getMaritalStatus()!=null){
			for(int i=0;i<lbMaritialStatus.getItemCount();i++){
				if(lbMaritialStatus.getItemText(i).equals(view.getMaritalStatus())){
					lbMaritialStatus.setSelectedIndex(i);
					break;
				}
			}
		}
		
		if(view.getRefEmployee()!=null){
			olbRefEmployee.setValue(view.getRefEmployee());
		}
		
		if(view.getEmployeeCTCList()!=null){
			this.empCTCTemplateTable.setValue(view.getEmployeeCTCList());
		}
		
		if(view.getProjectName()!=null){
			project.setValue(view.getProjectName());
		}
		
		if(view.getLastWorkingDate()!=null){
			dbLastWorkingDay.setValue(view.getLastWorkingDate());
		}
		/** date 18.7.2018 added by komal to store employee resignation information **/
		cbResigned.setValue(view.isResigned());
		if(view.getResignationDate()!=null){
			dbResignationDate.setValue(view.getResignationDate());
		}
		dbnoticePeriod.setValue(view.getNoticePeriod());
		cbAbsconded.setValue(view.isAbsconded());
		cbTerminated.setValue(view.isTerminated());
		if(view.getTerminationDate()!=null){
			dbTerminationDate.setValue(view.getTerminationDate());
		}
		if(view.getAbscondDate()!=null){
			dbAbscondDate.setValue(view.getAbscondDate());
		}
		cbReleaseForPayment.setValue(view.isRealeaseForPayment());
		/** date 11.8.2018 added by komal for one time deduction flag and amount **/
		cbOneTimeDeduction.setValue(view.isOneTimeDeduction());
		dbOneTimeDedectionAmount.setValue(view.getOneTimeDeductionAmount());
		
		if(view.getTrainingInfoList()!=null){
			trainingTbl.setValue(view.getTrainingInfoList());
		}
		
		if(view.getShiftName()!=null){
			oblShiftName.setValue(view.getShiftName());
		}
		
		if(view.getInterviewedBy()!=null){
			oblInterviewedBy.setValue(view.getInterviewedBy());
		}
		
		if(view.getSalaryAuthorizedBy()!=null){
			oblSalAuthBy.setValue(view.getSalaryAuthorizedBy());
		}
		
		if(view.getNomineeName()!=null){
			tbNomineeName.setValue(view.getNomineeName());
		}
		if(view.getNomineeRelation()!=null){
			tbNomineeRelation.setValue(view.getNomineeRelation());
		}
		if(view.getNomineeAddress()!=null){
			tbNomineeAddress.setValue(view.getNomineeAddress());
		}
		if(view.getNomineeAge()!=0){
			ibNomineeAge.setValue(view.getNomineeAge());
		}
		
//		if(view.getSalaryAuthorizedBy()!=null){
			cbIsFingerRegistered.setValue(view.isFingerDataPresent());
//		}
		if(view.getFingerRegisteredBy()!=null){
			tbFingerRegisteredBy.setValue(view.getFingerRegisteredBy());
		}
		if(view.getFingerRegisterationDate()!=null){
			dbFingerRegisteredDate.setValue(view.getFingerRegisterationDate());
		}
		if(view.getHappayCardUserId()!=null){
			tbHappayCardUserId.setValue(view.getHappayCardUserId());
		}
		if(view.getHappayCardUserName()!=null){
			tbHappayCardUserName.setValue(view.getHappayCardUserName());
		}
		if(view.getHappayCardWalletNumber()!=null){
			tbHappayCardWalletNumber.setValue(view.getHappayCardWalletNumber());
		}
		
		if(view.getAadharNumber()!=0){
			ibAadharNumber.setValue(view.getAadharNumber()+"");
		}
		
		cbPreviousPfNumber.setValue(view.isPreviousPfNumber());
		cbPreviousEsicNumber.setValue(view.isPreviousEsicNumber());
		
		cbSkilled.setValue(view.isSkilled());
		cbSemiSkilled.setValue(view.isSemiSkilled());
		cbUnskilled.setValue(view.isUnskilled());
		
		if(view.getLanuageSpoken()!=null){
			tbLanguageSpoken.setValue(view.getLanuageSpoken());
		}
		
		if(view.getUploadSign()!=null){
			uploadEmpSignature.setValue(view.getUploadSign());
			System.out.println("UPLOAD SIGN");
		}
		
		if(view.getPromotionHistoryList()!=null){
			promotionTbl.setValue(view.getPromotionHistoryList());
		}
		if(view.getDateOfRetirement()!=null){
			dbDateOfRetirement.setValue(view.getDateOfRetirement());
		}
		if(view.getDocumentUpload1()!=null){
			uploadComposite1.setValue(view.getDocumentUpload1());
			System.out.println("UPLOAD 1");
		}
		if(view.getDocumentUpload2()!=null){
			uploadComposite2.setValue(view.getDocumentUpload2());
			System.out.println("UPLOAD 2");
		}
		
		cbIsDisabled.setValue(view.isDisabled());
		if(view.getDisablityPercentage()!=0){
			dbDisabilityPercentage.setValue(view.getDisablityPercentage());
		}
		if(view.getUploadDisabilityDoc()!=null){
			uploadDisablityDoc.setValue(view.getUploadDisabilityDoc());
		}
		
		if(view.getDisabiltyRemark()!=null){
			taDisabilityRemark.setValue(view.getDisabiltyRemark());
		}
		
		if(view.getSchemeCertificateNum()!=null){
			tbSchemeCertificateNumber.setValue(view.getSchemeCertificateNum());
		}
		if(view.getPensionPaymentNum()!=null){
			tbPensionPaymentNumber.setValue(view.getPensionPaymentNum());
		}
		
		cbOutstationEmp.setValue(view.isOutstationEmployee());
		
		if(view.getEmpGrp()!=null){
			for(int i=0;i<lbEmpGrp.getItemCount();i++){
				if(lbEmpGrp.getItemText(i).equals(view.getEmpGrp())){
					lbEmpGrp.setSelectedIndex(i);
					break;
				}
			}
		}
		
		if(view.getReasonForLeaving()!=null){
			tbReasonForLeaving.setValue(view.getReasonForLeaving());
		}
		
		if(view.getFnfStatus()!=null){
			tbFnfStatus.setValue(view.getFnfStatus());
		}
		
		if(view.getFnfDate()!=null){
			dbFnFDate.setValue(view.getFnfDate());
		}
		
		if(view.getCreatedBy()!=null){
			tbCreatedBy.setValue(view.getCreatedBy());
		}
		if(view.getCreationDate()!=null){
			dbCreationDate.setValue(view.getCreationDate());
		}
		if(view.getUpdatedBy()!=null){
			tbUpdatedBy.setValue(view.getUpdatedBy());
		}
		if(view.getUpdateDate()!=null){
			dbUpdationDate.setValue(view.getUpdateDate());
		}
		
		if(view.getCaste()!=null){
			olbCaste.setValue(view.getCaste());
		}
		
		cbIsReliever.setValue(view.isReliever());
		
		if(view.getPoliceVerfDoc()!=null){
			policeVerfDocUplCom.setValue(view.getPoliceVerfDoc());
		}
		
		if(view.getExpiryDate()!=null){
			dbExpiryDate.setValue(view.getExpiryDate());
		}
		
		if(view.getProjectName()!=null&&!view.getProjectName().equals("")){
			loadAndMapSiteLocation(view.getSiteLocation());
		}
		if(view.getIncentivePercent()>0)
			incentivePercent.setValue(view.getIncentivePercent());
		
		if(view.getOvertimeName()!=null&&!view.getOvertimeName().equals(""))
			oblOtList.setValue(view.getOvertimeName());
		
		if(view.getCalendar()!=null&&!view.getCalendar().equals(""))
			olbcalender.setValue(view.getCalendar());
		
		if(view.getLeaveGroup()!=null&&!view.getLeaveGroup().equals(""))
			olbleavegroup.setValue(view.getLeaveGroup());
		
		
		if(presenter!=null){
			presenter.setModel(view);
		}

	}

	private void loadAndMapSiteLocation(final String siteLocation) {
		HrProject hrProject=project.getSelectedItem();
		
		int custId=0;
		String state=null;
		if(hrProject!=null&&hrProject.getPersoninfo()!=null){
			custId=hrProject.getPersoninfo().getCount();
		}
		if(hrProject!=null&&hrProject.getState()!=null){
			state=hrProject.getState();
		}
		
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(custId);
		filtervec.add(filter);
		
		if(state!=null&&!state.equals("")){
			filter = new Filter();
//			filter.setQuerryString("billingAddress.state");
			filter.setQuerryString("contact.address.state");
			filter.setStringValue(state);
			filtervec.add(filter);
		}
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		query.setFilters(filtervec);
		
		query.setQuerryObject(new CustomerBranchDetails());
		showWaitSymbol();
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				hideWaitSymbol();
				oblSiteLocationList.clear();
				oblSiteLocationList.addItem("--SELECT--");
				List<CustomerBranchDetails> custBranchList=new ArrayList<CustomerBranchDetails>();
				for (SuperModel model : result) {
					CustomerBranchDetails location=(CustomerBranchDetails) model;
					custBranchList.add(location);
				}
				
				oblSiteLocationList.setListItems(custBranchList);
				if(siteLocation!=null){
					oblSiteLocationList.setValue(siteLocation);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}
		});
	}

	public void setMenuAsPerStatus()
	{
		this.toggleAppHeaderBarMenu();
		this.toggleProcessLevelMenu();
		
	}
	
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Print")||text.equals("Search")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.EMPLOYEE,LoginPresenter.currentModule.trim());
	}

	
	/**
	 * rohan added this code for managing process level buttons 
	 */
	
	public void toggleProcessLevelMenu()
	{
		Employee entity=(Employee) presenter.getModel();
		String status=entity.getApproveStatus();
		System.out.println("status"+status);
		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();

			if(status.equalsIgnoreCase(Contract.CREATED))
			{
				if(text.equals("Request For Approval"))
					label.setVisible(true);
				if(text.equals("Cancel Approval Request"))
					label.setVisible(false);
				
				if(text.equals("New"))
					label.setVisible(true);
				if(text.equals("Change Employee Name")&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
					label.setVisible(true);
				}
				
			}
			else if(status.equalsIgnoreCase(Contract.REQUESTED))
			{
				if(text.equals("Request For Approval"))
					label.setVisible(false);
				
				if(text.equals("Cancel Approval Request"))
					label.setVisible(true);
				
				if(text.equals("New"))
					label.setVisible(true);
				if(text.equals("Change Employee Name")&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
					label.setVisible(true);
				}
			}
			else if(status.equalsIgnoreCase(Contract.APPROVED))
			{
				if(text.equals("Request For Approval"))
					label.setVisible(false);
				
				if(text.equals("New"))
					label.setVisible(true);
				
				if(text.equals("Cancel Approval Request"))
					label.setVisible(false);
				if(text.equals("Change Employee Name")&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
					label.setVisible(true);
				}
				
			}
		}
	}
	
	/**
	 * ends here 
	 */
	
	
	
	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
      this.employeeId.setValue(count+"");
	}


	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource()==cbIsDisabled){
			if(cbIsDisabled.getValue()==true){
				dbDisabilityPercentage.setEnabled(true);
				uploadDisablityDoc.setEnable(true);
			}else{
				dbDisabilityPercentage.setEnabled(false);
				uploadDisablityDoc.setEnable(false);
			}
		}
		
		if(event.getSource()==btnAddProject){
			EmployeeProjectHistory empProjHis=new EmployeeProjectHistory();
			employeeProjectHistoryTable.getDataprovider().getList().add(empProjHis);
		}
		
		if(event.getSource()==btnAddTrainingDetails){
			TrainingDetails training=new TrainingDetails();
			if(tbTrainingDescription.getValue()!=null)
				training.setTrainingDescription(tbTrainingDescription.getValue());
			if(olbTrainingType.getValue()!=null)
				training.setTrainingName(olbTrainingType.getValue());
			if(trainingDate.getValue()!=null)
				training.setTrainingDate(trainingDate.getValue());			
			
			trainingTbl.getDataprovider().getList().add(training);
		}
		
		if(event.getSource()==addBranchButton)
		{
			
			if(validateMethod(this.branchLis.getValue(this.branchLis.getSelectedIndex()))==false)
			{
			int count =0;
			if(empBranchTable.getDataprovider().getList().size()!=0)
			{
				int srno =getSrNumber();
				if(srno >0)
				{
					count = srno;
					EmployeeBranch empBranch = new EmployeeBranch();
					empBranch.setBranchName(this.branchLis.getValue(this.branchLis.getSelectedIndex()));
					empBranch.setSrNo(count+1);
					empBranchTable.getDataprovider().getList().add(empBranch);
					this.branchLis.setSelectedIndex(0);
				}
			}
			else{
				EmployeeBranch empBranch = new EmployeeBranch();
				empBranch.setBranchName(this.branchLis.getValue(this.branchLis.getSelectedIndex()));
				empBranch.setSrNo(count+1);
				empBranchTable.getDataprovider().getList().add(empBranch);
				this.branchLis.setSelectedIndex(0);
			}
		}
			else
			{
				this.showDialogMessage("Branch already exists");
			}
		}
		
		if(event.getSource()==btAddCtcValue){
			if(olbCTCComponents.getSelectedIndex()!=0){
				if(dbFromDateForCTC.getValue()!=null){
					if(dbToDateForCTC.getValue()!=null){
						if(dbCTCAmtValue.getValue()!=0){
							EmployeeCTCTemplate empTable=new EmployeeCTCTemplate(olbCTCComponents.getValue(olbCTCComponents.getSelectedIndex()),dbFromDateForCTC.getValue(),dbToDateForCTC.getValue(),dbCTCAmtValue.getValue());
							this.empCTCTemplateTable.getDataprovider().getList().add(empTable);
						}else{
							showDialogMessage("Please enter CTC Amount");
						}
					}else{
						showDialogMessage("Please enter End Date");
					}
				}else{
					showDialogMessage("Please enter Start Date");
				}
			}else{
				showDialogMessage("Please select CTC");
			}
		}
		
		if(event.getSource()==btnCopyPrimaryAdd){
			if(addressComposite.getValue()!=null){
				secondoryAddressComposite.setValue(addressComposite.getValue());
			}
		}
		
		if(event.getSource()==btnCopySecondaryAdd){
			if(secondoryAddressComposite.getValue()!=null){
				addressComposite.setValue(secondoryAddressComposite.getValue());
			}
		}
		
		if(event.getSource()==btnAddPromotion){
			PromotionDetails promotion=new PromotionDetails();
			String msg="";
			if(lbDesignation.getSelectedIndex()==0){
				msg=msg+"Please select previous designation."+"\n";
			}
			if(salary.getValue()==null||salary.getValue()==0){
				msg=msg+"Please add previous salary.";
			}
			if(!msg.equals("")){
				showDialogMessage(msg);
				return;
			}
			promotion.setPreviousDesignation(lbDesignation.getValue());
			promotion.setPreviousSalary(salary.getValue());
			promotionTbl.getDataprovider().getList().add(promotion);
		}
	}
	
	private boolean validateMethod(String branchName) {
		
		List<EmployeeBranch> branchdetails=this.empBranchTable.getDataprovider().getList();
		for(int i=0;i<branchdetails.size();i++)
		{
			System.out.println("salesl"+branchdetails.get(i).getBranchName());
			if(branchdetails.get(i).getBranchName().trim().equals(branchName)){
				return true;
			}
		}
		System.out.println("Out");
		return false;
		
	}

	private int getSrNumber() {
		
		List<EmployeeBranch> branchTableList = empBranchTable.getDataprovider().getList();
		Comparator<EmployeeBranch> compareBranch = new Comparator<EmployeeBranch>() {
			
			@Override
			public int compare(EmployeeBranch o1, EmployeeBranch o2) {
				// TODO Auto-generated method stub
				return Integer.compare(o1.getSrNo(),o2.getSrNo());
			}
		};
		Collections.sort(branchTableList,compareBranch);
		return branchTableList.get(branchTableList.size()-1).getSrNo();
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new Employee();
		model=employeeobj;
		if(HistoryPopup.historyObj!=null){
			AppUtility.addDocumentToHistoryTable(HistoryPopup.historyObj.getModuleName(),AppConstants.EMPLOYEE, employeeobj.getCount(), employeeobj.getCount(),employeeobj.getFullName(),employeeobj.getCellNumber1(), false, model, null);
		}else{
		AppUtility.addDocumentToHistoryTable(LoginPresenter.currentModule,AppConstants.EMPLOYEE, employeeobj.getCount(), employeeobj.getCount(),employeeobj.getFullName(),employeeobj.getCellNumber1(), false, model, null);
		}
		
		setMenuAsPerStatus();
		
	}
	

	@Override
	public void clear() {

		super.clear();
		cbStatus.setValue(true); 
		this.approverTable.clear();
		educationalQualification.clear();
		previousWorkExperience.clear();
		cbIsDisabled.setValue(false);
		dbDisabilityPercentage.setEnabled(false);
		uploadDisablityDoc.setEnable(false);
		
		cbOutstationEmp.setValue(false);
		cbIsReliever.setValue(false);
		employeeobj=null;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		System.out.println("INSIDE VALIDATION");
		boolean superRes= super.validate();
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "BackDatedJoiningDateNotAllowed")){
			DateTimeFormat format= DateTimeFormat.getFormat("dd-MM-yyyy");
			
			boolean isJoinigDateUpdated=true;
			if(employeeobj!=null){
				isJoinigDateUpdated=false;
				if(!format.parse(format.format(employeeobj.getJoinedAt())).equals(format.parse(format.format(dbJoiningDate.getValue())))){
					isJoinigDateUpdated=true;
				}
//				&&!format.parse(format.format(dbJoiningDate.getValue())).equals(format.parse(format.format(new Date())))
				if(isJoinigDateUpdated&&format.parse(format.format(dbJoiningDate.getValue())).before(format.parse(format.format(new Date())))){
					showDialogMessage("Joining date can not be backdated !");
					return false;
				}
			}else{
				if(isJoinigDateUpdated&&format.parse(format.format(dbJoiningDate.getValue())).before(format.parse(format.format(new Date())))){
					showDialogMessage("Joining date can not be backdated !");
					return false;
				}
			}
		}
		
		
		boolean passportvalidation=passPortInformation.validate();;
//		if(superRes==false&&passportvalidation==true)
//		{
//			return false;
//		}
//		
		if(passportvalidation==false){
			showDialogMessage("Expiry Date of Passpost cannot be less than Issue Date !");
			return false;
		}
		
		
		if(this.dbDateOfBirth.getValue()!=null){
			int date=calculateAge(dbDateOfBirth.getValue());
			if(date>=18){
//				return true;
			}else
			{
				showDialogMessage("Age is less than 18!");
				return false;
			}
		}
		
		/**
		 * Date : 15-05-2018 By ANIL
		 * Validated employee cell no 1
		 * for sasha erp
		 */
		/*** Date 10-08-2019 by Vijay if condition added if process cofig active then below validation not required ***/
		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","EnableEmployeeCellNoNonMandatory")){
			int cellCurrLen = pnbCellPhoneNo1.getValue().toString().length();
			if(AppUtility.validateFieldRange(cellCurrLen, 10, 11, "Cell No. 1 can not be less than 10 digit !", "Cell No. 1 can not be more than 11 digit !")==false){
				return false;
			}
		}

		
		
		/**
		 * Date : 11-10-2018 BY ANIL
		 * validating employee's branch as per the branch selected in project
		 */
	
		if(project.getSelectedIndex()!=0){
			System.out.println("INSIDE PROJECT CHECK");
			HrProject hrProject=project.getSelectedItem();
			System.out.println("BRANCH: "+lbBranch.getValue()+" : "+hrProject.getBranch());
			if(hrProject.getBranch()!=null&&!hrProject.getBranch().equals("")){
				System.out.println("1");
				if(lbBranch.getValue().equals(hrProject.getBranch())){
					System.out.println("2");
//					return true;
				}else{
					System.out.println("3");
					showDialogMessage("Employee's and selected project branch should be same.");
					return false;
				}
			}
		}
		
		if(cbIsDisabled.getValue()==true&&dbDisabilityPercentage.getValue()==null){
			showDialogMessage("Please add disabilty percentage.");
			return false;
		}
		
		/**
		 * Updated By: Viraj
		 * Date: 26-02-2019
		 * Description: To make address document mandatory
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "MakeAddressDocumentMandatory")){
			if(this.uploadAddressDoc.getValue() == null) {
				showDialogMessage("Please upload Address document.");
				return false;
			}	
		}
		/** Ends **/
		
		if(olbcalender.getValue()!=null&&olbleavegroup.getValue()==null){
			showDialogMessage("Please select calendar and leave group");
			return false;
		}
		
		if(olbcalender.getValue()==null&&olbleavegroup.getValue()!=null){
			showDialogMessage("Please select calendar and leave group");
			return false;
		}
		
		boolean overtimeNonMandatoryFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("HrProject", AppConstants.PC_HRPROJECTOVERTIMENONMANDATORY);
		boolean isProjectSelected=false;
		boolean isShiftSelected=false;
		boolean isOTSelected=false;
		if(project.getValue()!=null)
			isProjectSelected=true;
		if(oblShiftName.getValue()!=null)
			isShiftSelected=true;
		if(oblOtList.getValue()!=null)
			isOTSelected=true;
		
		if(overtimeNonMandatoryFlag){
    		if(isProjectSelected&&!isShiftSelected){
    			showDialogMessage("Please select project and shift");
    			return false;
    		}
    		if(!isProjectSelected&&isShiftSelected){
    			showDialogMessage("Please select project and shift.");
    			return false;
    		}
    		
    	}else{
    		if(isProjectSelected){
    			if(!isShiftSelected||!isOTSelected){
    				showDialogMessage("Please select shift and overtime along with project.");
        			return false;
    			}
    		}
    		
    	}
		
		
		return superRes;

	}
	
	
	
	

	public static int calculateAge(Date dob)
	{
		Date today = new Date();
		Integer currentYear = new Integer(DateTimeFormat.getFormat("yyyy").format(today));
		Integer currentMonth = new Integer(DateTimeFormat.getFormat("M").format(today));
		Integer currentDay = new Integer(DateTimeFormat.getFormat("d").format(today));

		Integer dobYear = new Integer(DateTimeFormat.getFormat("yyyy").format(dob));
		Integer dobMonth = new Integer(DateTimeFormat.getFormat("M").format(dob));
		Integer dobDay = new Integer(DateTimeFormat.getFormat("d").format(dob));

		int age = currentYear - dobYear;

		if((dobMonth > currentMonth) || (currentMonth == dobMonth && dobDay > currentDay))
			age--;

		return age;
	}

	private void makeDepartMentListBoxLive()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Department());
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		
		/** date 17/10/2017 komal added below line of code for department dropdown **/
		querry.getFilters().add(filter);
		olbDepartMent.MakeLive(querry);

	}
       
	public void applyHandler(ClickHandler handler)
	{
		addRowapprover.addClickHandler(handler);
		addRowInPreviousWork.addClickHandler(handler);
		addRowInQualification.addClickHandler(handler);
		addBranchButton.addClickHandler(this);
		btAddCtcValue.addClickHandler(this);
	}

	public Button getAddRowInQualification() {
		return addRowInQualification;
	}

	public void setAddRowInQualification(Button addRowInQualification) {
		this.addRowInQualification = addRowInQualification;
	}

	public Button getAddRowInPreviousWork() {
		return addRowInPreviousWork;
	}

	public void setAddRowInPreviousWork(Button addRowInPreviousWork) {
		this.addRowInPreviousWork = addRowInPreviousWork;
	}

	public Button getAddRowapprover() {
		return addRowapprover;
	}

	public void setAddRowapprover(Button addRowapprover) {
		this.addRowapprover = addRowapprover;
	}
	
//	public TextBox getTbFirstName() {
//		return tbFirstName;
//	}
//
//	public void setTbFirstName(TextBox tbFirstName) {
//		this.tbFirstName = tbFirstName;
//	}
//
//	public TextBox getTbMiddleName() {
//		return tbMiddleName;
//	}
//
//	public void setTbMiddleName(TextBox tbMiddleName) {
//		this.tbMiddleName = tbMiddleName;
//	}
//
//	public TextBox getTbLastName() {
//		return tbLastName;
//	}
//
//	public void setTbLastName(TextBox tbLastName) {
//		this.tbLastName = tbLastName;
//	}

	public ObjectListBox<Employee>getApprover()
	{
		return approvers;
	}
	
	@Override
	public void setEnable(boolean state)
	{
		super.setEnable(state);
		previousWorkExperience.setEnable(state);
		educationalQualification.setEnable(state);
		employeeId.setEnabled(false);
		approvedStatus.setEnabled(false);
		empBranchTable.setEnable(state);
		
		/**
		 * @author Anil,Date : 07-02-2019
		 * disabled the joining date for sasha erp
		 * @author Anil , Date : 09-07-2019
		 * Disabling joining date for admin too
		 */
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","DisableJoiningDate")
//				&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","DisableJoiningDate")){
			dbJoiningDate.setEnabled(false);
		}
		
		if(employeeId.getValue()!=null&&!employeeId.getValue().equals("")){
			this.olbcNumberRange.setEnabled(false);
		}
		this.empCTCTemplateTable.setEnable(state);
		this.employeeProjectHistoryTable.setEnable(state);
		this.trainingTbl.setEnable(state);
		this.promotionTbl.setEnable(state);
		
		if(cbIsDisabled.getValue()==true){
			dbDisabilityPercentage.setEnabled(state);
			uploadDisablityDoc.setEnable(state);
		}else{
			dbDisabilityPercentage.setEnabled(false);
			uploadDisablityDoc.setEnable(false);
		}
		
		/**
		 * @author Anil,Date : 07-02-2019
		 * disabled the joining date for sasha erp
		 * @author Anil , Date : 09-07-2019
		 * Disabling joining date for admin too
		 */
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","DisableJoiningDate")
//				&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","DisableJoiningDate")){
			dbJoiningDate.setEnabled(false);
		}
		cbIsFingerRegistered.setEnabled(false);
		tbFingerRegisteredBy.setEnabled(false);
		dbFingerRegisteredDate.setEnabled(false);
		
		/**
		 * @author Anil , Date : 22-03-2019
		 * made employee name disable after it approval/or it saved once
		 */
		if(cbStatus.getValue()==true&&!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "EnableEmployeeName")){
			tbfullName.setEnabled(false);
		}
		
		tbFnfStatus.setEnabled(false);
		dbFnFDate.setEnabled(false);
		tbCreatedBy.setEnabled(false);
		dbCreationDate.setEnabled(false);
		tbUpdatedBy.setEnabled(false);
		dbUpdationDate.setEnabled(false);
		
	}
	
	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		//updated by viraj date 21-02-2019 to disable project field
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee","makeProjectDisable")){
			this.project.setEnabled(false);
		}
		setMenuAsPerStatus();
	}
	
	public void manageEmployeeDropDown(Employee empModel){
		ScreeenState scrState = AppMemory.getAppMemory().currentState;

		if (scrState.equals(ScreeenState.NEW)) {
			LoginPresenter.globalEmployee.add(empModel);
		}
		if (scrState.equals(ScreeenState.EDIT)) {
			for (int i = 0; i < LoginPresenter.globalEmployee.size(); i++) {
				if (LoginPresenter.globalEmployee.get(i).getId().equals(empModel.getId())) {
					LoginPresenter.globalEmployee.add(empModel);
					LoginPresenter.globalEmployee.remove(i);
				}
			}
		}

		if (scrState.equals(ScreeenState.NEW)) {
			EmployeeInfo empinfo = getEmployeInfo(empModel);
			LoginPresenter.globalEmployeeInfo.add(empinfo);
		}
		if (scrState.equals(ScreeenState.EDIT)) {
			for (int i = 0; i < LoginPresenter.globalEmployeeInfo.size(); i++) {
				if (LoginPresenter.globalEmployeeInfo.get(i).getId()!=null && LoginPresenter.globalEmployeeInfo.get(i).getId().equals(empModel.getId())) {
					LoginPresenter.globalEmployeeInfo.add(getEmployeInfo(empModel));
					LoginPresenter.globalEmployeeInfo.remove(i);
				}
			}
		}

	}

	

	public ArticleTypeComposite getTbArticleTypeInfoComposite() {
		return tbArticleTypeInfoComposite;
	}

	public void setTbArticleTypeInfoComposite(
			ArticleTypeComposite tbArticleTypeInfoComposite) {
		this.tbArticleTypeInfoComposite = tbArticleTypeInfoComposite;
	}

	public TextBox getTbfullName() {
		return tbfullName;
	}

	public void setTbfullName(TextBox tbfullName) {
		this.tbfullName = tbfullName;
	}
	
	

	public TextBox getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(TextBox employeeId) {
		this.employeeId = employeeId;
	}

	@Override
	public void onValueChange(ValueChangeEvent<Long> event) {
		if (event.getSource() == pnbCellPhoneNo1) {
			int cellCurrLen = pnbCellPhoneNo1.getValue().toString().length();
			AppUtility.validateFieldRange(cellCurrLen, 10, 11, "Cell No. 1 can not be less than 10 digit !", "Cell No. 1 can not be more than 11 digit !");
		
		}
	}
	
	//************************all  anil code ***********************
	
	
	
//	public String fillFullName()
//	{
//		String fullname="";
//		if(this.getTbMiddleName().getValue().trim().equals("")){
//			fullname=this.getTbFirstName().getValue().trim()+" "+this.getTbLastName().getValue().trim();
//		}
//		else{
//		   fullname=this.getTbFirstName().getValue().trim()+" "+this.getTbMiddleName().getValue().trim()+" "+this.getTbLastName().getValue().trim();
//		}
//		System.out.println("Full Name Of Emp "+fullname);
//		return fullname;
//	}
	
	
	/**
	 * 
	 * Date : 12-10-2018 BY ANIL
	 * Method to calculate date  as on current date
	 * 365*24*60*60*1000
	 */
	public int getAgeAsOnDate(Date dob){
        Date now = new Date();
        long timeBetween = now.getTime() - dob.getTime();
        double yearsBetween = timeBetween / 3.15576e+10;
        int age = (int) Math.floor(yearsBetween);
        return age;
	}
	
	/**
	 * @author Anil , Date : 13-09-2019
	 * this method checks whether to update pf and esic number on payslip
	 * suggested by Nitin Sir for Riseon
	 */
	public boolean updatePfAndEsicNumberOnPayslip(){
		if(employeeobj!=null){
			//PF
			if(employeeobj.getPPFNaumber()!=null&&!employeeobj.getPPFNaumber().equals("")){
				if(ppfNumber.getValue()!=null&&!ppfNumber.getValue().equals("")){
					if(!ppfNumber.getValue().equals(employeeobj.getPPFNaumber())){
						return true;
					}
				}else{
					return true;
				}
			}else{
				if(ppfNumber.getValue()!=null&&!ppfNumber.getValue().equals("")){
					return true;
				}
			}
			//ESIC
			if(employeeobj.getEmployeeESICcode()!=null&&!employeeobj.getEmployeeESICcode().equals("")){
				if(ibESICcode.getValue()!=null&&!ibESICcode.getValue().equals("")){
					if(!ibESICcode.getValue().equals(employeeobj.getEmployeeESICcode())){
						return true;
					}
				}else{
					return true;
				}
			}else{
				if(ibESICcode.getValue()!=null&&!ibESICcode.getValue().equals("")){
					return true;
				}
			}
			//UAN
			if(employeeobj.getUANno()!=null&&!employeeobj.getUANno().equals("")){
				if(tbUANno.getValue()!=null&&!tbUANno.getValue().equals("")){
					if(!tbUANno.getValue().equals(employeeobj.getUANno())){
						return true;
					}
				}else{
					return true;
				}
			}else{
				if(tbUANno.getValue()!=null&&!tbUANno.getValue().equals("")){
					return true;
				}
			}
			//AADHAR
			if(employeeobj.getAadharNumber()!=0){
				if(ibAadharNumber.getValue()!=null&&!ibAadharNumber.getValue().equals("")){
					if(!ibAadharNumber.getValue().equals(employeeobj.getAadharNumber())){
						return true;
					}
				}else{
					return true;
				}
			}else{
				if(ibAadharNumber.getValue()!=null&&!ibAadharNumber.getValue().equals("")){
					return true;
				}
			}
			//PAN
			if(employeeobj.getEmployeePanNo()!=null&&!employeeobj.getEmployeePanNo().equals("")){
				if(ibPanNo.getValue()!=null&&!ibPanNo.getValue().equals("")){
					if(!ibPanNo.getValue().equals(employeeobj.getEmployeePanNo())){
						return true;
					}
				}else{
					return true;
				}
			}else{
				if(ibPanNo.getValue()!=null&&!ibPanNo.getValue().equals("")){
					return true;
				}
			}
		}
		
		return false;
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(project)){
			if(siteLocation){
				if(project.getSelectedIndex()!=0){
					loadAndMapSiteLocation(null);
				}else{
					oblSiteLocationList.clear();
					oblSiteLocationList.addItem("--SELECT--");
				}
			}
		}
	}
	
	
	private EmployeeInfo getEmployeInfo(Employee empModel) {

		EmployeeInfo empinfo=new EmployeeInfo();
		empinfo.setCount(empModel.getCount());
		empinfo.setBranch(empModel.getBranchName());
		empinfo.setCellNumber(empModel.getCellNumber1());
		empinfo.setDepartment(empModel.getDepartMent());
		empinfo.setDesignation(empModel.getDesignation());
		empinfo.setEmpCount(empModel.getCount());
		empinfo.setEmployeerole(empModel.getRoleName());
		empinfo.setEmployeeType(empModel.getEmployeeType());
		empinfo.setFullName(empModel.getFullname());
		empinfo.setId(empModel.getId());
		empinfo.setCountry(empModel.getCountry());
		empinfo.setCompanyId(empModel.getCompanyId());
		empinfo.setCaste(empModel.getCaste());
		/**
		 * Date : 04-05-2018 BY ANIL
		 */
		if (empModel.getGender() != null) {
			empinfo.setGender(empModel.getGender());
		} else {
			empinfo.setGender("");
		}
		
		/**
		 * Date : 29-05-2018 BY ANIL
		 */
		if (empModel.getNumberRange() != null) {
			empinfo.setNumberRangeAsEmpTyp(empModel.getNumberRange());
		} else {
			empinfo.setNumberRangeAsEmpTyp("");
		}
		
		/**
		 * Date : 20-07-2018 BY ANIL
		 */
		if(empModel.getProjectName()!=null){
			empinfo.setProjectName(empModel.getProjectName());
		}
		
		empinfo.setStatus(empModel.isStatus());
		
		/**
		 * Date : 25-07-2018 By  ANIL
		 */
		if(empModel.getDob()!=null){
			empinfo.setDateOfBirth(empModel.getDob());
		}
		
		if(empModel.getJoinedAt()!=null){
			empinfo.setDateOfJoining(empModel.getJoinedAt());
		}
		
		if(empModel.getLastWorkingDate()!=null){
			empinfo.setLastWorkingDate(empModel.getLastWorkingDate());
		}else{
			empinfo.setLastWorkingDate(null);
		}
		/**
		 * Date : 14-11-2018 BY ANIL
		 */
		empinfo.setDisabled(empModel.isDisabled());
		empinfo.setDisablityPercentage(empModel.getDisablityPercentage());
		

		/**
		 * Date : 21-12-2018 By ANIL
		 */
		empinfo.setAbsconded(empModel.isAbsconded());
		empinfo.setTerminated(empModel.isTerminated());
		empinfo.setResigned(empModel.isResigned());
		empinfo.setRealeaseForPayment(empModel.isRealeaseForPayment());
		/**
		 * End
		 */
		
		/**
		 * @author Anil ,Date : 10-04-2019
		 */
		if(empModel.getEmpGrp()!=null){
			empinfo.setEmpGrp(empModel.getEmpGrp());
		}
		
		/**
		 * @author Anil , Date : 23-09-2019
		 */
		if(empModel.getShiftName()!=null){
			empinfo.setShiftName(empModel.getShiftName());
		}
		
		empinfo.setReliever(empinfo.isReliever());
		return empinfo;
	
	}
	
}
