// QuickEmployeePresenter
package com.slicktechnologies.client.views.settings.employee;
import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeCheckListType;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;
import com.slicktechnologies.shared.common.shippingpackingprocess.StepsDetails;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.client.approvalutility.ApprovableScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.CustomerNameChangeService;
import com.slicktechnologies.client.services.CustomerNameChangeServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.RateContractServicesBillingService;
import com.slicktechnologies.client.services.RateContractServicesBillingServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.approval.ApprovalService;
import com.slicktechnologies.client.views.approval.ApprovalServiceAsync;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.humanresource.allocation.payslipallocation.PaySlipAllocationService;
import com.slicktechnologies.client.views.humanresource.allocation.payslipallocation.PaySlipAllocationServiceAsync;
import com.slicktechnologies.client.views.humanresource.employeeadditionaldetails.EmployeeAdditionalDetailsForm;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
/**
 *  FormTableScreen presenter template
 */
public class QuickEmployeePresenter extends FormScreenPresenter<Employee> implements ValueChangeHandler<Date> ,ClickHandler ,ChangeHandler{

	QuickEmployeeForm form;
	ApprovalServiceAsync approvalService=GWT.create(ApprovalService.class);
	CsvServiceAsync service=GWT.create(CsvService.class);
	final GenricServiceAsync async = GWT.create(GenricService.class);
	final RateContractServicesBillingServiceAsync rateasync = GWT.create(RateContractServicesBillingService.class);
	EmailServiceAsync emailService=GWT.create(EmailService.class);

	
	public final GenricServiceAsync genservice=GWT.create(GenricService.class);
	
	FromAndToDateBoxPopup datePopup=new FromAndToDateBoxPopup();
	PopupPanel panel=new PopupPanel();
	GeneralViewDocumentPopup gvPopup = null;
	EmployeeAdditionalDetails entity = new EmployeeAdditionalDetails();
	EmployeeInfo empInfo = new EmployeeInfo();
	
	
	public QuickEmployeePresenter (FormScreen<Employee> view,
			Employee model) {
		super(view, model);
		form=(QuickEmployeeForm) view;
		form.applyHandler(this);
		
		if(form.getSearchpopupscreen()!=null)
		form.getSearchpopupscreen().content.setSize("100%","100%");
		
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.QUICKEMPLOYEESCREEN,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
		datePopup.getBtnOne().addClickHandler(this);
		datePopup.getBtnTwo().addClickHandler(this);
		}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {

		InlineLabel lbl= (InlineLabel) e.getSource();
		if(lbl.getText().contains("New"))
		{
			form.setToNewState();
			QuickEmployeePresenter.initalize();
			
		}
	
	}
	@Override
	public void reactOnPrint() {
		
	
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}
	
	

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new Employee();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getEmployeeQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new Employee());
		return quer;
	}
	
	public void setModel(Employee entity)
	{
		model=entity;
	}
	
	public static QuickEmployeeForm initalize()
	{
			
		QuickEmployeeForm  form=new  QuickEmployeeForm();
			
			

				QuickEmployeePresenterTable gentableSearch=new QuickEmployeePresenterTableProxy();
				gentableSearch.setView(form);
				gentableSearch.applySelectionModle();
				QuickEmployeePresenterSearch.staticSuperTable=gentableSearch;
			    QuickEmployeePresenterSearchProxy searchpopup=new QuickEmployeePresenterSearchProxy();
			    form.setSearchpopupscreen(searchpopup);
			
			    QuickEmployeePresenter  presenter=new  QuickEmployeePresenter  (form,new Employee());
			    AppMemory.getAppMemory().stickPnel(form);
			    return form;
			
			
		 
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.personlayer.Employee")
		 public static class  QuickEmployeePresenterTable extends SuperTable<Employee> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.personlayer.Employee")

			 public static class  QuickEmployeePresenterSearch extends SearchPopUpScreen<Employee>{

				@Override
				public MyQuerry getQuerry() 
				{
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
	 

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
	}
		
	@Override
	public void reactOnDownload() {
		
		
			ArrayList<Employee> custarray=new ArrayList<Employee>();
			List<Employee> list=(List<Employee>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
			
			custarray.addAll(list);
			
			service.setEmployeelist(custarray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				
				@Override
				public void onSuccess(Void result) {
		
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+40;
					Window.open(url, "test", "enabled");
				}
			});
	}


	public void reactOnApprovalEmail(Approvals approvalEntity)
	{
		emailService.initiateApprovalEmail(approvalEntity,new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Unable To Send Email");
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(Void result) {
//				Window.alert("Email Sent Sucessfully !");
				
			}
		});
	}

	
	
	
	@Override
	public void reactOnSave() {
//		super.reactOnSave();
		
		final GenricServiceAsync service=GWT.create(GenricService.class);
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("userName");
		filter.setStringValue(form.tbUserId.getValue());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new User());
		service.getSearchResult(querry,	new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() > 0) {
					boolean duplicateUserNameflag = true;
					for(SuperModel model1 : result){
						User user = (User) model1;
						if(form.userEntity!=null && form.userEntity.getCount()==user.getCount()){
							duplicateUserNameflag = false;
						}

					}
					
					if(duplicateUserNameflag){
						form.showDialogMessage("This User id already exist in system!");
						return;
					}
				}	
				
				
				final MyQuerry querry2=new MyQuerry();
				Vector<Filter> temp2=new Vector<Filter>();
				Filter filter2=null;
				
				filter2=new Filter();
				filter2.setQuerryString("fullname");
				filter2.setStringValue(form.lbEmployeeName.getValue());
				temp2.add(filter2);
				
				querry2.setFilters(temp2);
				querry2.setQuerryObject(new Employee());
				
				service.getSearchResult(querry2, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						if (result.size() > 0) {
							boolean duplicateEmployeeNameflag = true;
							for(SuperModel model1 : result){
								Employee employee = (Employee) model1;
								if(model.getId()!=null && model.getId().equals(employee.getId())){
									duplicateEmployeeNameflag = false;
								}

							}
							if(duplicateEmployeeNameflag){
								form.showDialogMessage("Employee name already exist in system!");
								return;
							}
							
						}	
						
						if(validateAndUpdateModel())
						{
							view.showWaitSymbol();
							service.save(model,new AsyncCallback<ReturnFromServer>() {
								
								@Override
								public void onSuccess(ReturnFromServer result) {
									
									if(model.getId()==null){
									    QuickEmployeePresenterSearchProxy searchpopup=new QuickEmployeePresenterSearchProxy();
									}
									//Async run sucessfully , hide the wait symbol
									view.hideWaitSymbol();
									// set the count returned from server to this model
									model.setCount(result.count);
								   //set the id from server to this model
									model.setId(result.id);
									view.setToViewState();
									//Set count on view
									view.setCount(result.count);
									
									view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
							
									form.getdetailsFromUserEntity(form.lbEmployeeName.getValue(), false);
									
								}
								
								@Override
								public void onFailure(Throwable caught) {
									view.hideWaitSymbol();
									view.showDialogMessage("Data Save Unsuccessful! Try again.");
									caught.printStackTrace();
									
								}
							});
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
				
				
			}
		});
	}

	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

}

