package com.slicktechnologies.client.views.settings.company;
import com.google.gwt.event.dom.client.ClickEvent;

import java.util.*;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appstructure.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formscreen.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.google.gwt.core.shared.GWT;
import com.slicktechnologies.client.services.CompanyRegistrationService;
import com.slicktechnologies.client.services.CompanyRegistrationServiceAsync;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.popups.ConfigurationPopUp;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.shared.common.paymentlayer.*;
import com.google.gwt.event.dom.client.*;
/**
 * FormScreen presenter template.
 */
public class CompanyPresenter extends FormScreenPresenter<Company>{

	//Token to set the concrete FormScreen class name
	CompanyForm form;
	GenricServiceAsync async =GWT.create(GenricService.class);
	CompanyRegistrationServiceAsync companyAsync = GWT.create(CompanyRegistrationService.class);
	
	ConfigurationPopUp configpopup = new ConfigurationPopUp();
	
	public CompanyPresenter  (FormScreen<Company> view, Company model) {
		super(view, model);
		form=(CompanyForm) view;
		form.showWaitSymbol();
		Timer timer=new Timer() {
				
				@Override
				public void run() {
					retriveCompany();
			    
			             
				}
			};
			 timer.schedule(5000); 
		
		}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model=new Company();
	}
	
	
	
	
	public static void initalize()
	{
		CompanyForm  form=new  CompanyForm();
		
		
		//// CompanyPresenterTable gentable=GWT.create( CompanyPresenterTable.class);
		////  gentable.setView(form);
		////  gentable.applySelectionModle();
		//// CompanyPresenterSearch.staticSuperTable=gentable;
		//// CompanyPresenterSearch searchpopup=GWT.create( CompanyPresenterSearch.class);
		////form.setSearchpopupscreen(searchpopup);
		
		
		 CompanyPresenter  presenter=new CompanyPresenter(form,new Company());
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessunitlayer.Company")
	 public static  class CompanyPresenterSearch extends SearchPopUpScreen<Company>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};
		
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessunitlayer.Company")
		 public static class CompanyPresenterTable extends SuperTable<Company> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
	
	
		  
		  protected void retriveCompany()
			{
			  MyQuerry myQuer= new MyQuerry();
			  myQuer.setQuerryObject(new Company());
			  
			  async.getSearchResult(myQuer,new AsyncCallback<ArrayList<SuperModel>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					model=(Company) result.get(0);
					form.updateView(model);
					form.setToViewState();
					
					
				}
			});
			}



		@Override
		public void reactToProcessBarEvents(ClickEvent event) {
			
			System.out.println("process bar buttons");
			InlineLabel label=(InlineLabel) event.getSource();
			String text=label.getText().trim();
			
			if(text.equals("Create Configs")){
				Console.log("Inside create configs");
				boolean conf = Window.confirm("Do you really want to create configs ?");
				if (conf == true) {
				reactToCreateConfigs();
				}
			}
			
			/**
			 *  Date : 10/02/2021 By Priyanka.
			 *  Des : Default Configuration Requirement.
			 */
			if(text.equals("Create Configuration")){
				reactonCreateConfiguration();
			}
			
			if(text.equals("Create User Role")){
				createuserAuthorizationRoles();
				
			}
			
		}



		

		private void createuserAuthorizationRoles() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void reactOnPrint() {
			// TODO Auto-generated method stub
			
		}



		@Override
		public void reactOnEmail() {
			// TODO Auto-generated method stub
			
		}
		
		
		private void reactToCreateConfigs() {
			form.showWaitSymbol();
			companyAsync.createConfigsData(model.getCompanyId(), new AsyncCallback<ArrayList<String>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<String> result) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage(result.get(0));
				}
			});
			
		}
	
		
		private void reactonCreateConfiguration() {
			// TODO Auto-generated method stub
			configpopup.showPopUp();
			/**
			 * @author Anil @since 25-11-2021
			 * creating updated list which copies data from other reference link
			 */
//			ArrayList<ScreenName> screenNamelist = getScreenNamelist();
			ArrayList<ScreenName> screenNamelist = getListOfScreenName();
			configpopup.getScreenNameTable().getDataprovider().setList(screenNamelist);
			configpopup.tbRefernceLinkUrl.setText("my.evaerp505515.appspot.com"); //Ashwini Patil Date:14-07-2022
		}

		private ArrayList<ScreenName> getScreenNamelist() {
			ArrayList<ScreenName> screennamelsit = new ArrayList<ScreenName>();
			ArrayList<String> list = getConfigurationlist();
			for(String str : list){
				ScreenName screenname = new ScreenName();
				screenname.setScreenName(str);
				screennamelsit.add(screenname);
			}
			return screennamelsit;
		}

		private ArrayList<String> getConfigurationlist() {
			ArrayList<String> documentArray = new ArrayList<String>();
			
			documentArray.add("Customer"); 
			documentArray.add("Lead");
			documentArray.add("Quotation");
			documentArray.add("Contract");
			documentArray.add("Customer Service");
			documentArray.add("MMN");
			documentArray.add("Product Configuration");
			documentArray.add("Process Configuration");

			return documentArray;
		}
		
		/**
		 * @author Anil @since 25-11-2021
		 * Adding the list of entity that should be copied from given reference link
		 * Raised by Nitin Sir
		 */
		public ArrayList<String> getListOfConfiguration() {
			ArrayList<String> documentArray = new ArrayList<String>();
			
//			documentArray.add("Module Name"); 
//			documentArray.add("Document Name");
//			documentArray.add("Screen Name");
			documentArray.add("ConfigCategory");
			documentArray.add("Type");
			documentArray.add("Config");
			documentArray.add("ServiceProduct");
			documentArray.add("ItemProduct");
			documentArray.add("Screen Menu Configuration");
			documentArray.add("SMS Template");
			
			
			documentArray.add("Email template");
//			documentArray.add("Authorization");
			documentArray.add("User Authorization");
//			documentArray.add("Process Type");
			documentArray.add("Process Name");
			documentArray.add("Process Configurations");
			documentArray.add("Number Generation");
//			documentArray.add("Cron Job");
//			documentArray.add("Cron Job Reminder Setting");
			documentArray.add("Country");
			
			documentArray.add("State");
			documentArray.add("City");
			documentArray.add("Product Category");
			documentArray.add("Role Definition");
			documentArray.add("Communication Configuration");
			documentArray.add("Warehouse/Storage Location/Storage Bin - Central Warehouse");
			documentArray.add("Tax Details");
			documentArray.add("Shift");
			documentArray.add("Leave Type");
			documentArray.add("Leave Group");
			documentArray.add("Overtime");
			
			documentArray.add("CTC component");
			documentArray.add("Deduction");
			documentArray.add("Tax Slab Configuration");
			documentArray.add("Investment Configuration");
			documentArray.add("Paid Leave");
			documentArray.add("Bonus");
			documentArray.add("PT");
			documentArray.add("PF");
			documentArray.add("LWF");
			documentArray.add("ESIC");
			documentArray.add("Cron Job");//Ashwini Patil Date:5-08-2022
			documentArray.add("Material Movement Type");//Ashwini Patil Date:13-08-2022
			
			

			return documentArray;
		}
		
		private ArrayList<ScreenName> getListOfScreenName() {
			ArrayList<ScreenName> screennamelsit = new ArrayList<ScreenName>();
			ArrayList<String> list = getListOfConfiguration();
			for(String str : list){
				ScreenName screenname = new ScreenName();
				screenname.setScreenName(str);
				screennamelsit.add(screenname);
			}
			return screennamelsit;
		}
}
