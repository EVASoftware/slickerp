package com.slicktechnologies.client.views.settings.company;
import java.util.ArrayList;
import java.util.List;



import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.SocialInfoComposites;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.articletypecomposite.ArticleTypeComposite;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.popups.CompanyIntroductionPopUp;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.BranchUsernamePassword;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.FeedbackUrlAsPerCustomerCategory;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;


public class CompanyForm extends FormScreen<Company>    implements ClickHandler, ChangeHandler {

	//Token to add the varialble declarations
		EmailTextBox etbEmail,etbPointOfContactEmail;
		ObjectListBox<Config> lbCompanyType;
	  AddressComposite addressComposite;
	  SocialInfoComposites socialInfoComposite;
	  
	  TextBox companyURL;
	  /**
	   * Date 31-3-2018
	   * By jayshree
	   * Des.to add the statecode and country code
	   */
	  TextBox tbstatecode,tbcountrycode;
	  //End by jayshree
	  UploadComposite uploadIdCardLogo,uploadIdCardHeader,uploadIdCardFooter;
	  TextBox tbCompanyName,tbWebsite,tbPointOfContactName,tbServiceTaxNo,tbVatTaxNo,tbPANCard,tbLicenseNo,tbSignatoryText/*Added By Rahul Verma*/,tbcompanyGSTTypeText/*Added BY Rahul Verma*/;
	  UploadComposite uploadHeader,uploadFooter,uploadComposite,uploadDigitalSign/*Digital Sign added by rahul verma*/;
	  PhoneNumberBox pnbLandlinePhone,pnbCellPhoneNo1,pnbCellPhoneNo2,pnbFaxNo,pnbPointOfContactLandline,pnbPointOfContactCell;
	  DateBox dblicenseEndDate,dblicenseStartDate;
	  ArticleTypeComposite tbArticleTypeInfoComposite;
	  
	   /**8-12-2018 added pfgroupno and pfoffice textbox  by amol **/
	   TextBox tbPfGroupno,tbPfoffice;
	   
	   
       /**15-12-2018 date of coverage add by amol**/
	   DateBox dbdateofcoverage;
	   
	  //***************rohan added dates here for finitial year for v care
	  ListBox StdateMonth,EndDateMonth,StdateDate,endDateDate;
	  ListBox companyGSTType;/*Added By Rahul Verma*/
	  //   rohan added this button for process configs creating in production 
//	  Button createConfigs;
	  /** Added by Viraj Date 20-10-2018 for Ordient Ventures to Add Company Profile and Terms And Condition **/
	  UploadComposite companyProfile,termsAndCondition;
	  
	  /**
	   * Updated By: Viraj
	   * Date: 14-01-2019
	   * Description: To add company description 
	   */
	  TextArea description;
	  /** Ends **/
	  
	  /**
	   * Updated By: Viraj
	   * Date: 23-03-2019
	   * Description: To add franchise Information
	   */
	  TextBox brandUrl,masterUrl;
	  ListBox lbFranchiseType;
	  /** Ends **/
	  
	/**
	 * @author Anil , Date : 10-04-2020
	 * This field stores display name for from email id 
	 * it is used during email communication
	 */
	 TextBox tbDisplayNameForCompanyEmail;
	    /**
	   * Updated By: Amol
	   * Date:1-8-2020
	   * Description: To add Invoice Prefix raised by Ashwini Bhagwat
	   */
	  TextBox tbInvoicePrefix;
	  /**
	   * @author Vijay Chougule Date :- 08-09-2020
	   * Des :- while caling need paymentGateway API we need Merchant id.
	   */
	  TextBox tbmerchantId;
	  
	/**
	 * @author Anil @since 09-03-2021
	 * As per the nitin sir instruction we have to store feedback url on company master
	 */
	  
	  ObjectListBox<ConfigCategory> olbCustomerCategory;
	  TextBox tbFeedbackUrl;
	  Button addFeedbackBtn;
	  FeedbackUrlTable feedbackTbl;
	  
	  
	  /**
	   * @author Anil @since 18-02-2021
	   * adding upload composite to capture the back side of id card
	   * for Alkosh raised by Rahul & Nitin
	   */
	  UploadComposite uploadIdCardBackSide;
	  
	  /**
	   * @author Anil @since 30-04-2021
	   * Capturing scheduling Url
	   */
	  TextBox tbSchedulingUrl;
	  IntegerBox ibServiceDuration;
	  
	  TextBox tbwebsiteserviceTermsAndCondition;
	  ObjectListBox<Config> olbCurrency;
	  
	  TextBox paymentGatewayId;
	  PasswordTextBox paymentGatewayKey;
	  
	/**Added by sheetal:05-01-2022
	  Des: Adding field Telephony and Digital Payment on company screen for Exotel Integration Voice Calling / SMS / Number masking**/
	  public Label noteForTelephony;
	 
	  TextBox tbAccountSid;
	  TextBox tbAPIKey;
	  PasswordTextBox tbAPIToken;
	  TextBox tbCallerID;
	  TextBox tbApiUrl;
	  /**end**/
	  
	  TextArea taWhatsAppAPIUrl;
	  TextBox tbWhatsAppIntanceId,tbAccessToken;
	  CheckBox cbWhatsAppAPIStatus;
	  IntegerBox ibMobileNoPrefix;
	  
	  
	  TextBox tbCustomerId, tbUserName, tbApiId,tbApiSecret;
	  PasswordTextBox  tbPassword,tbBranchPassword;
	  BranchUsernamePasswordTable branchUsernamePasswordTable;
	  ObjectListBox<Branch> olbBranch;
	  TextBox tbBranchUsername;
	  Button addBranchUsernamePassword, btnIntroduction;
	  
	  CompanyIntroductionPopUp intoductionpopup = new CompanyIntroductionPopUp();
	  GenricServiceAsync async =  GWT.create(GenricService.class);
	  Company companyObj=null;//Ashwini Patil Date:24-04-2023 after clicking save button company introduction was getting deleted
	  ObjectListBox<String> einvoiceVendor;
	  ObjectListBox<String> eInvoiceEnvironment;
	  
	  TextBox tbZohoOrgID,tbZohoCode,tbZohoClientId,tbZohoClientSecret,tbZohoSurveyUrl;
	  
	  IntegerBox ibServiceCompletionDeadline; //Ashwini Patil Date:29-08-2024 Orion  do not want to allow last month service completion after 10th of every month.
	  
	  IntegerBox ibInvoiceEditDeadline;//Ashwini Patil Date:8-10-2024 Ultra do not want restrict invoice editing/cancel/reset only to current month for non admin users
	  
	public  CompanyForm() {
		super();
		createGui();
		tbArticleTypeInfoComposite.setForm(this);
//		createConfigs.addClickHandler(this);
		intoductionpopup.getLblOk().addClickHandler(this);
		intoductionpopup.getLblCancel().addClickHandler(this);
	}
	
	public  CompanyForm(boolean popuflag) {
		super(popuflag);
		createGui();
		tbArticleTypeInfoComposite.setForm(this);
		intoductionpopup.getLblOk().addClickHandler(this);
		intoductionpopup.getLblCancel().addClickHandler(this);
	}

	public CompanyForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
//		createConfigs.addClickHandler(this);
		
		
	}

	
	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget()
	{
		etbEmail=new EmailTextBox();
		etbPointOfContactEmail=new EmailTextBox();
		lbCompanyType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(lbCompanyType,Screen.COMPANYTYPE);
		addressComposite=new AddressComposite();
		socialInfoComposite=new SocialInfoComposites();
		tbCompanyName=new TextBox();
		tbWebsite=new TextBox();
		tbPointOfContactName=new TextBox();
		tbServiceTaxNo=new TextBox();
		tbVatTaxNo=new TextBox();
		tbPANCard=new TextBox();
		tbLicenseNo=new TextBox();
		uploadComposite=new UploadComposite();
		
		//*******************rohan changes here for sending header and footer in email *****
		uploadHeader = new UploadComposite();
		uploadFooter = new UploadComposite();
		
		uploadIdCardLogo= new UploadComposite(); 
		uploadIdCardHeader = new UploadComposite();
		uploadIdCardFooter = new UploadComposite();
		
		companyURL = new TextBox();
		//***************changes ends here ****************************
		
		/** Added by Viraj Date 20-10-2018 for Ordient Ventures to Add Company Profile and Terms And Condition **/
		companyProfile = new UploadComposite();
		termsAndCondition = new UploadComposite();
		description = new TextArea();				//Added by:Viraj Date:14-01-2019
		/** Ends **/
		
		pnbLandlinePhone=new PhoneNumberBox();
		pnbCellPhoneNo1=new PhoneNumberBox();
		pnbCellPhoneNo2=new PhoneNumberBox();
		pnbFaxNo=new PhoneNumberBox();
		pnbPointOfContactLandline=new PhoneNumberBox();
		pnbPointOfContactCell=new PhoneNumberBox();
		dblicenseEndDate=new DateBoxWithYearSelector();
		DateTimeFormat format1=DateTimeFormat.getFormat("dd-MM-yyyy");
		DateBox.DefaultFormat defformat1=new DateBox.DefaultFormat(format1);
		dblicenseEndDate.setFormat(defformat1);
		dblicenseEndDate.setEnabled(false);
		
		dblicenseStartDate=new DateBoxWithYearSelector();
		DateTimeFormat format2=DateTimeFormat.getFormat("dd-MM-yyyy");
		DateBox.DefaultFormat defformat2=new DateBox.DefaultFormat(format2);
		dblicenseStartDate.setFormat(defformat2);
		dblicenseStartDate.setEnabled(false);
		
		ibServiceCompletionDeadline=new IntegerBox();
		ibServiceCompletionDeadline.setTitle("You can add day of month here. It should be in range 1 to 31. If you put 10 here then after 10th of every month, system will not allow you to mark complete previous month services");
		//************rohan code for year *****
		StdateMonth = new ListBox();
		EndDateMonth= new ListBox();
		StdateDate = new ListBox();
		endDateDate = new ListBox();
		
		tbArticleTypeInfoComposite = new ArticleTypeComposite();
		
		endDateDate = new ListBox();
		endDateDate.insertItem("--Select--", 0);
		endDateDate.insertItem("1", 1);
		endDateDate.insertItem("2", 2);
		endDateDate.insertItem("3", 3);
		endDateDate.insertItem("4", 4);
		endDateDate.insertItem("5",5);
		endDateDate.insertItem("6", 6);
		endDateDate.insertItem("7", 7);
		endDateDate.insertItem("8", 8);
		endDateDate.insertItem("9", 9);
		endDateDate.insertItem("10", 10);
		endDateDate.insertItem("11", 11);
		endDateDate.insertItem("12", 12);
		endDateDate.insertItem("13", 13);
		endDateDate.insertItem("14", 14);
		endDateDate.insertItem("15", 15);
		endDateDate.insertItem("16", 16);
		endDateDate.insertItem("17", 17);
		endDateDate.insertItem("18", 18);
		endDateDate.insertItem("19", 19);
		endDateDate.insertItem("20", 20);
		endDateDate.insertItem("21", 21);
		endDateDate.insertItem("22", 22);
		endDateDate.insertItem("23", 23);
		endDateDate.insertItem("24", 24);
		endDateDate.insertItem("25", 25);
		endDateDate.insertItem("26", 26);
		endDateDate.insertItem("27", 27);
		endDateDate.insertItem("28", 28);
		endDateDate.insertItem("29", 29);
		endDateDate.insertItem("30", 30);
		endDateDate.insertItem("31", 31);
		
		
		StdateDate = new ListBox();
		StdateDate.insertItem("--Select--", 0);
		StdateDate.insertItem("1", 1);
		StdateDate.insertItem("2", 2);
		StdateDate.insertItem("3", 3);
		StdateDate.insertItem("4", 4);
		StdateDate.insertItem("5",5);
		StdateDate.insertItem("6", 6);
		StdateDate.insertItem("7", 7);
		StdateDate.insertItem("8", 8);
		StdateDate.insertItem("9", 9);
		StdateDate.insertItem("10", 10);
		StdateDate.insertItem("11", 11);
		StdateDate.insertItem("12", 12);
		StdateDate.insertItem("13", 13);
		StdateDate.insertItem("14", 14);
		StdateDate.insertItem("15", 15);
		StdateDate.insertItem("16", 16);
		StdateDate.insertItem("17", 17);
		StdateDate.insertItem("18", 18);
		StdateDate.insertItem("19", 19);
		StdateDate.insertItem("20", 20);
		StdateDate.insertItem("21", 21);
		StdateDate.insertItem("22", 22);
		StdateDate.insertItem("23", 23);
		StdateDate.insertItem("24", 24);
		StdateDate.insertItem("25", 25);
		StdateDate.insertItem("26", 26);
		StdateDate.insertItem("27", 27);
		StdateDate.insertItem("28", 28);
		StdateDate.insertItem("29", 29);
		StdateDate.insertItem("30", 30);
		StdateDate.insertItem("31", 31);
		
		
		StdateMonth = new ListBox();
		StdateMonth.insertItem("--Select--", 0);
		StdateMonth.insertItem("JAN", 1);
		StdateMonth.insertItem("FEB", 2);
		StdateMonth.insertItem("MAR", 3);
		StdateMonth.insertItem("APR", 4);
		StdateMonth.insertItem("MAY",5);
		StdateMonth.insertItem("JUN", 6);
		StdateMonth.insertItem("JUL", 7);
		StdateMonth.insertItem("AUG", 8);
		StdateMonth.insertItem("SEP", 9);
		StdateMonth.insertItem("OCT", 10);
		StdateMonth.insertItem("NOV", 11);
		StdateMonth.insertItem("DEC", 12);
		
		
		EndDateMonth = new ListBox();
		EndDateMonth.insertItem("--Select--", 0);
		EndDateMonth.insertItem("JAN", 1);
		EndDateMonth.insertItem("FEB", 2);
		EndDateMonth.insertItem("MAR", 3);
		EndDateMonth.insertItem("APR", 4);
		EndDateMonth.insertItem("MAY",5);
		EndDateMonth.insertItem("JUN", 6);
		EndDateMonth.insertItem("JUL", 7);
		EndDateMonth.insertItem("AUG", 8);
		EndDateMonth.insertItem("SEP", 9);
		EndDateMonth.insertItem("OCT", 10);
		EndDateMonth.insertItem("NOV", 11);
		EndDateMonth.insertItem("DEC", 12);
		
		
//		createConfigs =new Button("Create Configs");
		
		/**
		 * Date : 09 Sept 2017
		 * Created by Rahul Verma
		 */
		uploadDigitalSign=new UploadComposite();
		
		/**
		 * Rahul Verma Added this on 14 Sept 2017
		 * 
		 */
		companyGSTType=new ListBox();
		companyGSTType.insertItem("--Select--", 0);
		companyGSTType.insertItem("GST Not Applicable", 1);//Company not under gst
		companyGSTType.insertItem("Composite GST", 2);//Composite Gst i.e from 20L to 75L
		companyGSTType.insertItem("GST Applicable", 3);//Under GST
		
		tbSignatoryText=new TextBox();
		tbcompanyGSTTypeText=new TextBox();
		
		/**8-12-2018 added by amol**/
		tbPfGroupno=new TextBox();
		tbPfoffice=new TextBox();
		
		/**15-12-2018 added by amol**/
		dbdateofcoverage = new DateBoxWithYearSelector();
		
		
		/**
		   * Date 31-3-2018
		   * By jayshree
		   * Des.to add the statecode and country code
		   */
		   tbstatecode=new TextBox();
		   tbcountrycode=new TextBox();
		  //End by jayshree
		/**
		 * Updated By: Viraj 
		 * Date: 24-03-2019
		 * Description: To add franchise Information   
		 */
		   brandUrl = new TextBox();
		   masterUrl = new TextBox();
		   
		   lbFranchiseType = new ListBox();
		   lbFranchiseType.insertItem("--Select--", 0);
		   lbFranchiseType.insertItem("BRAND", 1);
		   lbFranchiseType.insertItem("MASTER FRANCHISE", 2);
		   lbFranchiseType.insertItem("FRANCHISE", 3);
		   
		   lbFranchiseType.addChangeHandler(this);
		   brandUrl.setEnabled(false);
		   masterUrl.setEnabled(false);
		 /** Ends **/  
		   
		   tbDisplayNameForCompanyEmail=new TextBox();
           tbInvoicePrefix=new TextBox();
           
           tbmerchantId = new TextBox();
           
           olbCustomerCategory= new ObjectListBox<ConfigCategory>();
   		   AppUtility.MakeLiveCategoryConfig(olbCustomerCategory, Screen.CUSTOMERCATEGORY);
           
   		   tbFeedbackUrl=new TextBox();
           addFeedbackBtn=new Button("Add");
           addFeedbackBtn.addClickHandler(this);
           
           feedbackTbl=new FeedbackUrlTable();
           
           uploadIdCardBackSide= new UploadComposite(); 
           
           tbSchedulingUrl=new TextBox();
           ibServiceDuration=new IntegerBox();
           
           tbwebsiteserviceTermsAndCondition = new TextBox();
           
           olbCurrency=new ObjectListBox<Config>();
           AppUtility.MakeLiveConfig(olbCurrency,Screen.CURRENCY);
           
           paymentGatewayId = new TextBox();
           paymentGatewayKey = new PasswordTextBox();
           
           /**Added by sheetal:05-01-2022**/
           noteForTelephony = new Label();
           noteForTelephony.setText("Enter the following details to connect telephony software in the system. Following details are provided by the telephone software provider. These are there typically under settings. If you are not able to find contact your service provider.");
           tbAccountSid = new TextBox();
           tbAPIKey = new TextBox();
           tbAPIToken = new PasswordTextBox();
           tbCallerID = new TextBox();
           tbApiUrl = new TextBox();
           /**end**/
           
           taWhatsAppAPIUrl = new TextArea();
           tbWhatsAppIntanceId = new TextBox();
           cbWhatsAppAPIStatus = new CheckBox();
           ibMobileNoPrefix = new IntegerBox();
           tbAccessToken = new TextBox();
           
           tbCustomerId=new TextBox();
           tbCustomerId.setTitle("This is required if vendor is MyGSTCafe");
           tbUserName=new TextBox();
           tbUserName.setTitle("Enter username registered in NIC portal for GSTIN number of company. You can put NA if branchAsCompany is active in settings.");
           tbPassword=new PasswordTextBox();
           tbPassword.setTitle("Enter password of entered username. You can put NA if branchAsCompany is active in settings.");
           tbApiId=new TextBox();
           tbApiId.setTitle("Enter APIid/GspAppid received from E-invoice vendor");
           tbApiSecret=new TextBox();
           tbApiSecret.setTitle("Enter APISecret/GspAppSecret received from E-invoice vendor");
           branchUsernamePasswordTable=new BranchUsernamePasswordTable();
           olbBranch=new ObjectListBox<Branch>();
   		   AppUtility.makeBranchListBoxLive(olbBranch);
   		   tbBranchUsername=new TextBox();
   		   tbBranchUsername.setTitle("Enter username registered in NIC portal for GSTIN number of selected branch");
   		   tbBranchPassword=new PasswordTextBox();
   		   tbBranchPassword.setTitle("Enter password of entered username for branch.");
  		  
   		   addBranchUsernamePassword=new Button("Add");
   		   addBranchUsernamePassword.addClickHandler(this);
   		   
   		   btnIntroduction = new Button("Introduction");
   		   btnIntroduction.addClickHandler(this);
   		   
   		   einvoiceVendor=new ObjectListBox<String>(); 	
   		   einvoiceVendor.addItem(AppConstants.MYGSTCAFE);
   		   einvoiceVendor.addItem(AppConstants.TAXILLA);
   		   
   		   
   		   eInvoiceEnvironment=new ObjectListBox<String>(); 	
   		   eInvoiceEnvironment.addItem("--Select--");
   		   eInvoiceEnvironment.addItem("Staging");
   		   eInvoiceEnvironment.addItem("Live");
   		   eInvoiceEnvironment.setTitle("Select Staging for einvoice testing or select Live to generate live e-invoices from system");
	
   		   tbZohoOrgID=new TextBox();
   		   tbZohoCode=new TextBox();
   		   tbZohoClientId=new TextBox();
   		   tbZohoClientSecret=new TextBox();
   		   tbZohoSurveyUrl=new TextBox();
   		   tbZohoSurveyUrl.setTitle("Add zoho survey form url here. This will be used to generate complain QR code from customer branch screen");
	
   		   ibInvoiceEditDeadline=new IntegerBox();
   		   ibInvoiceEditDeadline.setTitle("You can add day of month here. It should be in range 1 to 31. If you put 10 here then after 10th of every month, system will not allow non admin users to edit/cancel/reset previous month invoices");
		
	
	}
	
	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen() {
		initalizeWidget();
		/**
		 *  Date : 10/02/2021 By Priyanka.
		 *  Des : Add process bar button for Default Configuration.
		 */
		this.processlevelBarNames=new String[]{/*"Create Configs",*/"Create Configuration"};
		
		//Token to initialize the processlevel menus.
		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCompanyInformation=fbuilder.setlabel("Company Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Name",tbCompanyName);// added by priyanka simplification comp screen.Company Name
		FormField ftbCompanyName= fbuilder.setMandatory(true).setMandatoryMsg("Company Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Type",lbCompanyType);//// added by priyanka simplification comp screen.Company Type
		FormField flbCompanyType= fbuilder.setMandatory(true).setMandatoryMsg("Company Type is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Website",tbWebsite);
		FormField ftbWebsite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Email",etbEmail);
		FormField fetbEmail= fbuilder.setMandatory(true).setMandatoryMsg("Email is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("E-mail Display Name",tbDisplayNameForCompanyEmail);// Ashwini Patil changed Email to E-mail as if label contains Email word it checks for email validation
		FormField ftbEmailDisplayName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/**
		   * Date 31-3-2018
		   * By jayshree
		   * Des.to add the statecode and country code
		   */
		fbuilder = new FormFieldBuilder("State Code",tbstatecode);
		FormField ftbstatecode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Country Code",tbcountrycode);
		FormField ftbcountrycode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		//End By jayshree
		
		fbuilder = new FormFieldBuilder("Landline Phone",pnbLandlinePhone);
		FormField fpnbLandlinePhone= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("*Mobile 1",pnbCellPhoneNo1); // //Cell Phone No 
		FormField fpnbCellPhoneNo1= fbuilder.setMandatory(true).setMandatoryMsg("Mobile 1 is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Mobile 2",pnbCellPhoneNo2);
		FormField fpnbCellPhoneNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Fax No",pnbFaxNo);
		FormField fpnbFaxNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPointOfContactInformation=fbuilder.setlabel("Point Of Contact Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* PoC Name",tbPointOfContactName);//added by Priyanka
		FormField ftbPointOfContactName= fbuilder.setMandatory(true).setMandatoryMsg("Point Of Contact Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("PoC Landline",pnbPointOfContactLandline);//added by Priyanka
		FormField fpnbPointOfContactLandline= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* PoC Cell",pnbPointOfContactCell);//added by Priyanka
		FormField fpnbPointOfContactCell= fbuilder.setMandatory(true).setMandatoryMsg("Point Of Contact Cell is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("PoC Email",etbPointOfContactEmail);
		FormField fetbPointOfContactEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		//old label Tax Details
		FormField fgroupingTaxInformation=fbuilder.setlabel("Article Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Service Tax Nr",tbServiceTaxNo);
		FormField ftbServiceTaxNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("VAT Registration Nr",tbVatTaxNo);
		FormField ftbVatTaxNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("PAN Card",tbPANCard);
		FormField ftbPANCard= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("License Nr",tbLicenseNo);
		FormField ftbLicenseNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSocialInformation=fbuilder.setlabel("Social Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",socialInfoComposite);
		FormField fsocialInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAddressInformation=fbuilder.setlabel("Address Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",addressComposite);
		FormField faddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingUploadLogo=fbuilder.setlabel("Upload Logo").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("(Size : 475x513 px)",uploadComposite);
		FormField fuploadComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		/**
		 *  Added By : Priyanka Date : 09-02-2021
		 *  Des : Custmization of ID card PDF requirement by Sasha raised by Rahul Tiwari.
		 */
		fbuilder = new FormFieldBuilder("(Logo : ID-Card)",uploadIdCardLogo); 
		FormField fuploadCompositeOne= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		//***************rohan added here for sending header and footer in email **************
		FormField fgroupingUploadHF=fbuilder.setlabel("Upload Header and Footer").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Header(Size : 2400x722 px)",uploadHeader);
		FormField fuploadHeader= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Footer(Size : 3340x365 px)",uploadFooter);
		FormField fuploadFooter= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		/**
		 *  Added By : Priyanka Date : 09-02-2021
		 *  Des : Custmization of ID card PDF requirement by Sasha raised by Rahul Tiwari.
		 */
		fbuilder = new FormFieldBuilder("(Header : ID-Card(2400x722 px))",uploadIdCardHeader);
		FormField fuploadHeaderOne= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("(Footer : ID-Card(3340x365 px))",uploadIdCardFooter);
		FormField fuploadFooterOne= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();

		/** Added by Priyanka Date 25-06-2021 Remove process configuration for company screen simplifiaction suggest by Nitin Sir**/		
//		/** Added by Viraj Date 31-10-2018 for Ordient Ventures to Add Company Profile and Terms And Condition **/
		FormField fucCompanyProfile = null;
		FormField fucTermsAndCondition = null;
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Company","OnlyForOrdientVentures"))
//		{
			fbuilder = new FormFieldBuilder("Company Profile",companyProfile);
			fucCompanyProfile= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Terms & Conditions",termsAndCondition);
			fucTermsAndCondition= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		//}
		/** Ends **/
		
		fbuilder = new FormFieldBuilder("* App / ERP-CRM URL",companyURL);//Ashwini Patil Date:13-07-2022
		FormField fcompanyURL= fbuilder.setMandatory(true).setMandatoryMsg("Company URL is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Start Date",StdateDate);
		FormField fyearStdateMonth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("End Date",endDateDate);
		FormField fyearEndDateMonth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Start Month",StdateMonth);
		FormField fyearStdate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("End Month",EndDateMonth);
		FormField fyearEndDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		//*******************************changes ends here ***********************************
		
		fbuilder = new FormFieldBuilder("License Start Date",dblicenseStartDate);
		FormField fdblicenseStartDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("License End Date",dblicenseEndDate);
		FormField fdblicenseEndDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",tbArticleTypeInfoComposite);
        FormField ftbTaxTypeInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
        
        FormField fgroupingUploadDigital=fbuilder.setlabel("Upload Digital Signature").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        fbuilder = new FormFieldBuilder("(Size : 613x500 px)",uploadDigitalSign);
		FormField fuploadDigitalSign= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
        
		fbuilder = new FormFieldBuilder("Signatory Text",tbSignatoryText);
		FormField ftbSignatoryText= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		
		FormField fGSTInfo=fbuilder.setlabel("GST Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        fbuilder = new FormFieldBuilder("GST Type",companyGSTType);
		FormField fcompanyGSTType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("GST Number",tbcompanyGSTTypeText);//added by Priyanka 
		FormField ftbcompanyGSTTypeText= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
	
		
		                           /**8-12-2018 added by amol "pf information grouping"**/
		FormField fPFINFO=fbuilder.setlabel("PF Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("PF Group Number",tbPfGroupno);
		FormField ftbPfGroupno= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
			
		fbuilder = new FormFieldBuilder("PF Office",tbPfoffice);
		FormField ftbPfoffice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
			
		
		/**15-12-2018 added by amol for dateofcoverage datebox**/
		fbuilder= new FormFieldBuilder("Date Of Coverage",dbdateofcoverage);
		FormField fdbdateofcoverage= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		 /**
		   * Updated By: Viraj
		   * Date: 14-01-2019
		   * Description: To add company description 
		   */
		fbuilder= new FormFieldBuilder("Description",description);
		FormField fdescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		/** Ends **/
//        fbuilder = new FormFieldBuilder("",createConfigs);
//        FormField fcreateConfigs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 * Updated By: Viraj 
		 * Date: 24-03-2019
		 * Description: To add franchise Information   
		 */
		FormField flbFranchiseType = null;
		FormField fFranchiseBrandUrl = null;
		FormField fmasterUrl = null;
		FormField fgroupingFranchiseInfo = null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Company","FranchiseInformation")) {
			fgroupingFranchiseInfo = fbuilder.setlabel("Franchise Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder("Franchise Type",lbFranchiseType);
			flbFranchiseType = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Brand URL",brandUrl);
			fFranchiseBrandUrl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Master URL",masterUrl);
			fmasterUrl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
        /** Ends **/
		
		fbuilder = new FormFieldBuilder("Invocie Prefix",tbInvoicePrefix);
		FormField ftbInvoicePrefix= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Merchant Id",tbmerchantId);
		FormField ftbmerchantId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer Category",olbCustomerCategory);
		FormField ftbFeedbackUrl1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Feedback URL ",tbFeedbackUrl);
		FormField ftbFeedbackUrl2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",addFeedbackBtn);
		FormField faddFeedbackBtn= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",feedbackTbl.getTable());
		FormField ffeedbackTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		// Added By Priyanka : Simplification Of Company Screen suggest by Nitin Sir
		
		FormField fgroupFiscalyear=fbuilder.setlabel("Fiscal Year").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		//Ashwini Patil Date:05-05-2022 changing label from Service Feedback to Customer Service Setup
//		FormField frefandgengrp=fbuilder.setlabel("Service Feedback").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField frefandgengrp=fbuilder.setlabel("Customer Service Setup").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("(Backside : ID-Card(2400x722 px))",uploadIdCardBackSide);
		FormField fuploadIdCardBackSide= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Scheduling URL",tbSchedulingUrl);
		FormField ftbSchedulingUrl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Duration",ibServiceDuration);
		FormField fibServiceDuration= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Completion Deadline",ibServiceCompletionDeadline);
		FormField fibServiceCompletionDeadlinen= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Website Service Terms & Condition Link",tbwebsiteserviceTermsAndCondition);
		FormField ftbwebsiteserviceTermsAndCondition= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Currency",olbCurrency);
		FormField folbCurrency= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Payment Gateway Id",paymentGatewayId);
		FormField fpaymentGatewayId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Payment Gateway Key",paymentGatewayKey);
		FormField fpaymentGatewayKey= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**Added by Sheetal:05-01-2022**/
		
		FormField fgroupingTelephony=fbuilder.setlabel("Telephony").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",noteForTelephony);
		FormField fnoteForTelephony= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Account SID",tbAccountSid);
		FormField ftbAccountSid= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("API Key",tbAPIKey);
		FormField ftbAPIKey= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("API Token (Password)",tbAPIToken);
		FormField ftbAPIToken= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Caller ID",tbCallerID);
		FormField ftbCallerID= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("API URL",tbApiUrl);
		FormField ftbApiUrl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		FormField fgroupingDigitalPayment=fbuilder.setlabel("Digital Payment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		/**end**/
		
		FormField fWhatsAppIntegration=fbuilder.setlabel("WhatsApp Integration").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("WhatsApp API URL",taWhatsAppAPIUrl);
		FormField ftaWhatsAppAPIUrl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("WhatsApp Instance Id",tbWhatsAppIntanceId);
		FormField ftbWhatsAppIntanceId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbWhatsAppAPIStatus);
		FormField fcbWhatsAppAPIStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Mobile No Prefix",ibMobileNoPrefix);
		FormField fibMobileNoPrefix= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Access Token",tbAccessToken);
		FormField ftbAccessToken= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fEInvoice=fbuilder.setlabel("E-Invoice").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Select eInvoice Vendor",einvoiceVendor);
		FormField feinvoiceVendor= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Select Environment",eInvoiceEnvironment);
		FormField feInvoiceEnvironment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer Id",tbCustomerId);
		FormField ftbCustomerId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("API Id",tbApiId);
		FormField ftbApiId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("API Secret",tbApiSecret);
		FormField ftbApiSecret= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Username",tbUserName);
		FormField ftbUserName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Password",tbPassword);
		FormField ftbPassword= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Branch",olbBranch);
		FormField folbBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Username for Branch",tbBranchUsername);
		FormField ftbBranchUsername= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Password for Branch",tbBranchPassword);
		FormField ftbBranchPassword= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();		
		
		fbuilder = new FormFieldBuilder("",addBranchUsernamePassword);
		FormField faddBranchUsernamePassword= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();		
		
		fbuilder = new FormFieldBuilder("",branchUsernamePasswordTable.getTable());
		FormField fbranchUsernamePasswordTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",btnIntroduction);
		FormField fbtnIntroduction= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fZohoIntegration=fbuilder.setlabel("Zoho Integration").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("Orgranization ID",tbZohoOrgID);
		FormField ftbZohoOrgID= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Refresh Token",tbZohoCode);
		FormField ftbZohoCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Client ID",tbZohoClientId);
		FormField ftbZohoClientId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Client Secret",tbZohoClientSecret);
		FormField ftbZohoClientSecret= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Complain Survey Form Url",tbZohoSurveyUrl);
		FormField ftbZohoSurveyUrl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
				
		fbuilder = new FormFieldBuilder("Invoice Edit Deadline",ibInvoiceEditDeadline);
		FormField fibInvoiceEditDeadline= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		/** Added by Viraj Date 31-10-2018 for Ordient Ventures to Add Company Profile and Terms And Condition **/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Company","OnlyForOrdientVentures")) {
			
			FormField[][] formfield = {   
//				  {fgroupingCompanyInformation},
//				  {ftbCompanyName,flbCompanyType,ftbWebsite,fetbEmail},
//				  {ftbstatecode,fpnbLandlinePhone,ftbcountrycode,fpnbCellPhoneNo1},//add by Jayshree
//				  {fpnbCellPhoneNo2,fpnbFaxNo,fcompanyURL,ftbSignatoryText},
//				  {fyearStdateMonth,fyearStdate,fyearEndDateMonth,fyearEndDate},
//				  {ftbEmailDisplayName,ftbmerchantId},
//			//	  {fdblicenseStartDate,fdblicenseEndDate},
//				  {fgroupingPointOfContactInformation},
//				  {ftbPointOfContactName,fpnbPointOfContactLandline,fpnbPointOfContactCell,fetbPointOfContactEmail},
//				  {fgroupingTaxInformation},
//			//	  {ftbServiceTaxNo,ftbVatTaxNo,ftbPANCard,ftbLicenseNo},
//				  {ftbTaxTypeInfoComposite},
//				  {fgroupingSocialInformation},
//				  {fsocialInfoComposite},
//				  {fgroupingAddressInformation},
//				  {faddressComposite},
//				  /**
//				   *  Added By : Priyanka Date : 09-02-2021
//				   *  Des : Custmization of ID card PDF requirement by Sasha raised by Rahul Tiwari.
//				   */
//				  {fgroupingUploadLogo},
//				  {fuploadComposite,fuploadCompositeOne},
//				  
//				  {fgroupingUploadHF},
//				  {fuploadHeader,fuploadFooter},
//				  {fuploadHeaderOne,fuploadFooterOne},
//				  {fuploadIdCardBackSide},
//				  /**
//				   *  End
//				   */
//				  
//				  {fgroupingUploadDigital},
//				  {fuploadDigitalSign,fucCompanyProfile},
//				  {fucTermsAndCondition},
//				  {fGSTInfo},
//				  {fcompanyGSTType,ftbcompanyGSTTypeText},
//				  {fdescription},
//				  {frefandgengrp},
//				  {ftbFeedbackUrl1,ftbFeedbackUrl2,faddFeedbackBtn},
//				  {ffeedbackTbl},
//				  {ftbSchedulingUrl,fibServiceDuration}
//			  };
//			
//					this.fields=formfield;
			
			
		
			//Compnay Information
			  {fgroupingCompanyInformation},
			  {ftbCompanyName,flbCompanyType,fcompanyURL,fpnbCellPhoneNo1},//ftbWebsite
			  {fetbEmail,ftbEmailDisplayName,ftbPointOfContactName,fpnbPointOfContactCell},
			  {fetbPointOfContactEmail,fpnbPointOfContactLandline,fpnbCellPhoneNo2,fpnbLandlinePhone},
			  {ftbstatecode,ftbcountrycode,fucCompanyProfile,fbtnIntroduction},
			  {fibInvoiceEditDeadline},
			  
			// Address
			  {fgroupingAddressInformation},
			  {faddressComposite},
			  
			// Tax deatails
			  {fgroupingTaxInformation},
			  {ftbTaxTypeInfoComposite},
			  {ftbInvoicePrefix},	  

			  //GST
			  {fGSTInfo},
			  {fcompanyGSTType,ftbcompanyGSTTypeText},
			  {fdescription},
//			  {fPFINFO},
//			  {ftbPfGroupno,ftbPfoffice,fdbdateofcoverage},
			 
			  //Fiscal Year
			  {fgroupFiscalyear},
			  {fyearStdateMonth,fyearStdate,fyearEndDateMonth,fyearEndDate},
			  //upload logo
			  {fgroupingUploadLogo},
			  {fuploadComposite,fuploadCompositeOne},
			  //Upload Header and Footer
			  {fgroupingUploadHF},
			  {fuploadHeader,fuploadFooter},
			  {fuploadHeaderOne,fuploadFooterOne},
			  {fuploadIdCardBackSide},
			  //Upload Digital Signature
			  {fgroupingUploadDigital},
			  {fuploadDigitalSign},
//			  {fucCompanyProfile,fucTermsAndCondition},
			  {fucTermsAndCondition},/**@Sheetal:22-03-2022,Moving company profile button to company information tab**/
			  //service feedback
			  {frefandgengrp},
			  {ftbFeedbackUrl1,ftbFeedbackUrl2,faddFeedbackBtn},
			  {ffeedbackTbl},
//			  {ftbSchedulingUrl,fibServiceDuration,ftbwebsiteserviceTermsAndCondition},
			  {ftbSchedulingUrl,fibServiceDuration},/**@Sheetal:22-03-2022,Moving terms and conditions link to digital payment tab**/
			  {fibServiceCompletionDeadlinen},
			  //socail information
			  {fgroupingSocialInformation},
			  {fsocialInfoComposite,ftbWebsite},
			  {folbCurrency},
			//Telephony
			  {fgroupingTelephony},
			  {fnoteForTelephony},
			  {ftbAccountSid,ftbAPIKey,ftbAPIToken,ftbCallerID},
			  {ftbApiUrl},
			  //Digital Payment
			  {fgroupingDigitalPayment},
			  {ftbmerchantId,fpaymentGatewayId,fpaymentGatewayKey},
			  {ftbwebsiteserviceTermsAndCondition},
			  
			  //WhatsApp Integration
			  {fWhatsAppIntegration},
			  {ftaWhatsAppAPIUrl},
			  {fibMobileNoPrefix,ftbWhatsAppIntanceId,ftbAccessToken,fcbWhatsAppAPIStatus},
			  
			  //E-Invoice
				{fEInvoice},
				{feinvoiceVendor,feInvoiceEnvironment},
				{ftbCustomerId,ftbApiId,ftbApiSecret,ftbUserName,ftbPassword},
				{folbBranch,ftbBranchUsername,ftbBranchPassword,faddBranchUsernamePassword},
				{fbranchUsernamePasswordTable},
			  	
			
				//Zoho Integration
				{fZohoIntegration},
				{ftbZohoOrgID,ftbZohoCode},
				{ftbZohoClientId,ftbZohoClientSecret},
				{ftbZohoSurveyUrl}
				
			};
			 
			
			this.fields=formfield;
						
		
		}
		/** Ends **/
		/**
		 * Updated By: Viraj 
		 * Date: 24-03-2019
		 * Description: To add franchise Information   
		 */
		else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Company","FranchiseInformation")) {
			FormField[][] formfield = { 
//					//Compnay Information
//					  {fgroupingCompanyInformation},
//					  {ftbCompanyName,flbCompanyType,fcompanyURL,fpnbCellPhoneNo1},//ftbWebsite
//					  {fetbEmail,ftbEmailDisplayName,ftbPointOfContactName,fpnbPointOfContactCell},
//					  {fetbPointOfContactEmail,fpnbPointOfContactLandline,fpnbCellPhoneNo2,fpnbLandlinePhone},
//					  {ftbstatecode,ftbcountrycode},
//					// Address
//					  {fgroupingAddressInformation},
//					  {faddressComposite},
//					  
//					// Tax deatails
//					  {fgroupingTaxInformation},
//					  {ftbTaxTypeInfoComposite},
//					  {fgroupingSocialInformation},
//					  {fsocialInfoComposite},
//					
//					  {fcompanyGSTType,ftbcompanyGSTTypeText},
//					  
//					//
//					  
//					  {fpnbFaxNo,ftbSignatoryText},
//					  {fyearStdateMonth,fyearStdate,fyearEndDateMonth,fyearEndDate},
//					  {ftbmerchantId},
//				//	  {fdblicenseStartDate,fdblicenseEndDate},
//					  {fgroupingFranchiseInfo},
//					  {flbFranchiseType,fFranchiseBrandUrl,fmasterUrl},
//				//	  {fgroupingPointOfContactInformation},
//					  
//					  
//					  
//					  {fgroupingUploadLogo},
//					  {fuploadComposite,fuploadCompositeOne},
//					  
//					  {fgroupingUploadHF},
//					  {fuploadHeader,fuploadFooter},
//					  {fuploadHeaderOne,fuploadFooterOne},
//					  {fuploadIdCardBackSide},
//					  
//					  {fgroupingUploadDigital},
//					  {fuploadDigitalSign},
//					  
//					  {fPFINFO},
//					  {ftbPfGroupno,ftbPfoffice,fdbdateofcoverage},
//					  {fdescription},
//					  {frefandgengrp},
//					  {ftbFeedbackUrl1,ftbFeedbackUrl2,faddFeedbackBtn},
//					  {ffeedbackTbl},
//					  {ftbSchedulingUrl,fibServiceDuration}
//					  };
//					
//							this.fields=formfield;
					
					//Compnay Information
					  {fgroupingCompanyInformation},
					  {ftbCompanyName,flbCompanyType,fcompanyURL,fpnbCellPhoneNo1},
					  {fetbEmail,ftbEmailDisplayName,ftbPointOfContactName,fpnbPointOfContactCell},
					  {fetbPointOfContactEmail,fpnbPointOfContactLandline,fpnbCellPhoneNo2,fpnbLandlinePhone},
					  {ftbstatecode,ftbcountrycode,fucCompanyProfile,fbtnIntroduction},
					  {fibInvoiceEditDeadline},
					  
					// Address
					  {fgroupingAddressInformation},
					  {faddressComposite},
					  
					// Tax deatails
					  {fgroupingTaxInformation},
					  {ftbTaxTypeInfoComposite},
					  {ftbInvoicePrefix},	 
					  //GST
					  {fGSTInfo},
					  {fcompanyGSTType,ftbcompanyGSTTypeText},
					  {fPFINFO},
					  {ftbPfGroupno,ftbPfoffice,fdbdateofcoverage},
					  {fdescription},
					  //Fiscal Year
					  {fgroupFiscalyear},
					  {fyearStdateMonth,fyearStdate,fyearEndDateMonth,fyearEndDate},
					  //upload logo
					  {fgroupingUploadLogo},
					  {fuploadComposite,fuploadCompositeOne},
					  //Upload Header and Footer
					  {fgroupingUploadHF},
					  {fuploadHeader,fuploadFooter},
					  {fuploadHeaderOne,fuploadFooterOne},
					  {fuploadIdCardBackSide},
					  //Upload Digital Signature
					  {fgroupingUploadDigital},
					  {fuploadDigitalSign},
//					  {fucCompanyProfile,fucTermsAndCondition},
					  {fucTermsAndCondition},/**@Sheetal:22-03-2022, Moving company profile button to company information tab**/
					  //service feedback
					  {frefandgengrp},
					  {ftbFeedbackUrl1,ftbFeedbackUrl2,faddFeedbackBtn},
					  {ffeedbackTbl},
//					  {ftbSchedulingUrl,fibServiceDuration,ftbwebsiteserviceTermsAndCondition},
					  {ftbSchedulingUrl,fibServiceDuration},/**@Sheetal:22-03-2022,Moving terms and conditions link to digital payment tab**/
					  {fibServiceCompletionDeadlinen},
					  //socail information
					  {fgroupingSocialInformation},
					  {fsocialInfoComposite,ftbWebsite},
					  {folbCurrency},
					//Telephony
					  {fgroupingTelephony},
					  {fnoteForTelephony},
					  {ftbAccountSid,ftbAPIKey,ftbAPIToken,ftbCallerID},
					  {ftbApiUrl},
					  //Digital Payment
					  {fgroupingDigitalPayment},
					  {ftbmerchantId,fpaymentGatewayId,fpaymentGatewayKey},
					  {ftbwebsiteserviceTermsAndCondition},
					  //WhatsApp Integration
					  {fWhatsAppIntegration},
					  {ftaWhatsAppAPIUrl},
					  {fibMobileNoPrefix,ftbWhatsAppIntanceId,ftbAccessToken,fcbWhatsAppAPIStatus},
					  
					  //E-Invoice
					  {fEInvoice},
					  {feinvoiceVendor,feInvoiceEnvironment},
					  {ftbCustomerId,ftbApiId,ftbApiSecret,ftbUserName,ftbPassword},
					  {folbBranch,ftbBranchUsername,ftbBranchPassword,faddBranchUsernamePassword},
					  {fbranchUsernamePasswordTable},
					  
			
					//Zoho Integration
					{fZohoIntegration},
					{ftbZohoOrgID,ftbZohoCode},
					{ftbZohoClientId,ftbZohoClientSecret},
					{ftbZohoSurveyUrl}
					};
					
					this.fields=formfield;
		}
		/** Ends **/
		
		else if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","PC_PRINTINVOICENUMBERPREFIX")){	
			FormField[][] formfield = {  
//					  {ftbCompanyName,flbCompanyType,ftbWebsite,fetbEmail},
//					  {ftbstatecode,fpnbLandlinePhone,ftbcountrycode,fpnbCellPhoneNo1},//add by Jayshree
//					  {fpnbCellPhoneNo2,fpnbFaxNo,fcompanyURL,ftbSignatoryText},
//					  {fyearStdateMonth,fyearStdate,fyearEndDateMonth,fyearEndDate},
//					  {ftbInvoicePrefix,ftbmerchantId},
//				//	  {fdblicenseStartDate,fdblicenseEndDate},
//					  {fgroupingPointOfContactInformation},
//					  {ftbPointOfContactName,fpnbPointOfContactLandline,fpnbPointOfContactCell,fetbPointOfContactEmail},
//					  {fgroupingTaxInformation},
//				//	  {ftbServiceTaxNo,ftbVatTaxNo,ftbPANCard,ftbLicenseNo},
//					  {ftbTaxTypeInfoComposite},
//					  {fgroupingSocialInformation},
//					  {fsocialInfoComposite},
//					  {fgroupingAddressInformation},
//					  {faddressComposite},
//					  
//					  {fgroupingUploadLogo},
//					  {fuploadComposite,fuploadCompositeOne},
//					  
//					  {fgroupingUploadHF},
//					  {fuploadHeader,fuploadFooter},
//					  {fuploadHeaderOne,fuploadFooterOne},
//					  {fuploadIdCardBackSide},
//					  
//					  {fgroupingUploadDigital},
//					  {fuploadDigitalSign},
//					  {fGSTInfo},
//					  {fcompanyGSTType,ftbcompanyGSTTypeText},
//					  {fPFINFO},
//					  {ftbPfGroupno,ftbPfoffice,fdbdateofcoverage},
//					  {fdescription},
//					  {frefandgengrp},
//					  {ftbFeedbackUrl1,ftbFeedbackUrl2,faddFeedbackBtn},
//					  {ffeedbackTbl},
//					  {ftbSchedulingUrl,fibServiceDuration}
//					  };
//					
//							this.fields=formfield;
					//Compnay Information
					  {fgroupingCompanyInformation},
					  {ftbCompanyName,flbCompanyType,fcompanyURL,fpnbCellPhoneNo1},//ftbWebsite
					  {fetbEmail,ftbEmailDisplayName,ftbPointOfContactName,fpnbPointOfContactCell},
					  {fetbPointOfContactEmail,fpnbPointOfContactLandline,fpnbCellPhoneNo2,fpnbLandlinePhone},
					  {ftbstatecode,ftbcountrycode,fucCompanyProfile,fbtnIntroduction},
					  {fibInvoiceEditDeadline},
					  
					// Address
					  {fgroupingAddressInformation},
					  {faddressComposite},
					  {ftbInvoicePrefix},	 
					  
					// Tax deatails
					  {fgroupingTaxInformation},
					  {ftbTaxTypeInfoComposite},
					  //GST
					  {fGSTInfo},
					  {fcompanyGSTType,ftbcompanyGSTTypeText},
					  {fPFINFO},
					  {ftbPfGroupno,ftbPfoffice,fdbdateofcoverage},
					  {fdescription},
					  //Fiscal Year
					  {fgroupFiscalyear},
					  {fyearStdateMonth,fyearStdate,fyearEndDateMonth,fyearEndDate},
					  //upload logo
					  {fgroupingUploadLogo},
					  {fuploadComposite,fuploadCompositeOne},
					  //Upload Header and Footer
					  {fgroupingUploadHF},
					  {fuploadHeader,fuploadFooter},
					  {fuploadHeaderOne,fuploadFooterOne},
					  {fuploadIdCardBackSide},
					  //Upload Digital Signature
					  {fgroupingUploadDigital},
					  {fuploadDigitalSign},
//					  {fucCompanyProfile,fucTermsAndCondition},
					  {fucTermsAndCondition},/**@Sheetal:22-03-2022, Moving company profile button to company information tab**/
					  //service feedback
					  {frefandgengrp},
					  {ftbFeedbackUrl1,ftbFeedbackUrl2,faddFeedbackBtn},
					  {ffeedbackTbl},
//				  {ftbSchedulingUrl,fibServiceDuration,ftbwebsiteserviceTermsAndCondition},
					  {ftbSchedulingUrl,fibServiceDuration},/**@Sheetal:22-03-2022,Moving terms and conditions link to digital payment tab**/
					  {fibServiceCompletionDeadlinen},
					  //socail information
					  {fgroupingSocialInformation},
					  {fsocialInfoComposite,ftbWebsite},
					  {folbCurrency},
					//Telephony
					  {fgroupingTelephony},
					  {fnoteForTelephony},
					  {ftbAccountSid,ftbAPIKey,ftbAPIToken,ftbCallerID},
					  {ftbApiUrl},
					  //Digital Payment
					  {fgroupingDigitalPayment},
					  {ftbmerchantId,fpaymentGatewayId,fpaymentGatewayKey},
					  {ftbwebsiteserviceTermsAndCondition},
					  //WhatsApp Integration
					  {fWhatsAppIntegration},
					  {ftaWhatsAppAPIUrl},
					  {fibMobileNoPrefix,ftbWhatsAppIntanceId,ftbAccessToken,fcbWhatsAppAPIStatus},
					  
					  //E-Invoice
						{fEInvoice},
						{feinvoiceVendor,feInvoiceEnvironment},
						{ftbCustomerId,ftbApiId,ftbApiSecret,ftbUserName,ftbPassword},
						{folbBranch,ftbBranchUsername,ftbBranchPassword,faddBranchUsernamePassword},
						{fbranchUsernamePasswordTable},
						
						//Zoho Integration
						{fZohoIntegration},
						{ftbZohoOrgID,ftbZohoCode},
						{ftbZohoClientId,ftbZohoClientSecret},
						{ftbZohoSurveyUrl}
					  };
					
					this.fields=formfield;
				}
		
		
		
		else {	
			FormField[][] formfield = { 
//			  {fgroupingCompanyInformation},
//			  {ftbCompanyName,flbCompanyType,ftbWebsite,fetbEmail},
//			  {ftbstatecode,fpnbLandlinePhone,ftbcountrycode,fpnbCellPhoneNo1},//add by Jayshree
//			  {fpnbCellPhoneNo2,fpnbFaxNo,fcompanyURL,ftbSignatoryText},
//			  {fyearStdateMonth,fyearStdate,fyearEndDateMonth,fyearEndDate},
//			  {ftbEmailDisplayName,ftbmerchantId},
//		//	  {fdblicenseStartDate,fdblicenseEndDate},
//			  {fgroupingPointOfContactInformation},
//			  {ftbPointOfContactName,fpnbPointOfContactLandline,fpnbPointOfContactCell,fetbPointOfContactEmail},
//			  {fgroupingTaxInformation},
//		//	  {ftbServiceTaxNo,ftbVatTaxNo,ftbPANCard,ftbLicenseNo},
//			  {ftbTaxTypeInfoComposite},
//			  {fgroupingSocialInformation},
//			  {fsocialInfoComposite},
//			  {fgroupingAddressInformation},
//			  {faddressComposite},
//			  
//			  {fgroupingUploadLogo},
//			  {fuploadComposite,fuploadCompositeOne},
//			  
//			  {fgroupingUploadHF},
//			  {fuploadHeader,fuploadFooter},
//			  {fuploadHeaderOne,fuploadFooterOne},
//			  {fuploadIdCardBackSide},
//			  
//			  {fgroupingUploadDigital},
//			  {fuploadDigitalSign},
//			  {fGSTInfo},
//			  {fcompanyGSTType,ftbcompanyGSTTypeText},
//			  {fPFINFO},
//			  {ftbPfGroupno,ftbPfoffice,fdbdateofcoverage},
//			  {fdescription},
//			  {frefandgengrp},
//			  {ftbFeedbackUrl1,ftbFeedbackUrl2,faddFeedbackBtn},
//			  {ffeedbackTbl},
//			  {ftbSchedulingUrl,fibServiceDuration}
//			  };
//			
//					this.fields=formfield;
					
					//Compnay Information
					  {fgroupingCompanyInformation},
					  {ftbCompanyName,flbCompanyType,fcompanyURL,fpnbCellPhoneNo1},//ftbWebsite
					  {fetbEmail,ftbEmailDisplayName,ftbPointOfContactName,fpnbPointOfContactCell},
					  {fetbPointOfContactEmail,fpnbPointOfContactLandline,fpnbCellPhoneNo2,fpnbLandlinePhone},
					  {ftbstatecode,ftbcountrycode,fucCompanyProfile, fbtnIntroduction},
					  {fibInvoiceEditDeadline},
		
					// Address
					  {fgroupingAddressInformation},
					  {faddressComposite},
					  
					// Tax deatails
					  {fgroupingTaxInformation},
					  {ftbTaxTypeInfoComposite},
					  {ftbInvoicePrefix},	 
					  //GST
					  {fGSTInfo},
					  {fcompanyGSTType,ftbcompanyGSTTypeText},
					  {fPFINFO},
					  {ftbPfGroupno,ftbPfoffice,fdbdateofcoverage},
					  {fdescription},
					  //Fiscal Year
					  {fgroupFiscalyear},
					  {fyearStdateMonth,fyearStdate,fyearEndDateMonth,fyearEndDate},
					  //upload logo
					  {fgroupingUploadLogo},
					  {fuploadComposite,fuploadCompositeOne},
					  //upload header and footer
					  {fgroupingUploadHF},
					  {fuploadHeader,fuploadFooter},
					  {fuploadHeaderOne,fuploadFooterOne},
					  {fuploadIdCardBackSide},
					  //Upload Digital Signature
					  {fgroupingUploadDigital},
					  {fuploadDigitalSign},
//					  {fucCompanyProfile,fucTermsAndCondition},
					  {fucTermsAndCondition},/**@Sheetal:22-03-2022, Moving company profile button to company information tab**/
					  //service feedback
					  {frefandgengrp},
					  {ftbFeedbackUrl1,ftbFeedbackUrl2,faddFeedbackBtn},
					  {ffeedbackTbl},
//					  {ftbSchedulingUrl,fibServiceDuration,ftbwebsiteserviceTermsAndCondition},
					  {ftbSchedulingUrl,fibServiceDuration},/**@Sheetal:22-03-2022,Moving terms and conditions link to digital payment tab**/
					  {fibServiceCompletionDeadlinen},
					  //socail information
					  {fgroupingSocialInformation},
					  {fsocialInfoComposite,ftbWebsite},
					  {folbCurrency},
					  //Telephony
					  {fgroupingTelephony},
					  {fnoteForTelephony},
					  {ftbAccountSid,ftbAPIKey,ftbAPIToken,ftbCallerID},
					  {ftbApiUrl},
					  //Digital Payment
					  {fgroupingDigitalPayment},
					  {ftbmerchantId,fpaymentGatewayId,fpaymentGatewayKey},
					  {ftbwebsiteserviceTermsAndCondition},
					  //WhatsApp Integration
					  {fWhatsAppIntegration},
					  {ftaWhatsAppAPIUrl},
					  {fibMobileNoPrefix,ftbWhatsAppIntanceId,ftbAccessToken,fcbWhatsAppAPIStatus},
					  
					  //E-Invoice
						{fEInvoice},
						{feinvoiceVendor,feInvoiceEnvironment},
						{ftbCustomerId,ftbApiId,ftbApiSecret,ftbUserName,ftbPassword},
						{folbBranch,ftbBranchUsername,ftbBranchPassword,faddBranchUsernamePassword},
						{fbranchUsernamePasswordTable},
						
						//Zoho Integration
						{fZohoIntegration},
						{ftbZohoOrgID,ftbZohoCode},
						{ftbZohoClientId,ftbZohoClientSecret},
						{ftbZohoSurveyUrl}
					  };
					
					this.fields=formfield;
		}
	   }

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Company model) 
	{
		Console.log("in updateModel(Company model)");
		if(tbCompanyName.getValue()!=null)
		  model.setBusinessUnitName(tbCompanyName.getValue());
		
		
		if(companyURL.getValue()!=null){
			/**
			 * @author Ashwini
			 * @since 17-1-2022
			 * To save and display url in correct format as per requirements for the android app.
			 */
			if(companyURL.getValue().equalsIgnoreCase(getUrlFromwindow()))
				model.setCompanyURL(companyURL.getValue());
			else
				model.setCompanyURL(getUrlFromwindow());
				companyURL.setValue(getUrlFromwindow());
				Console.log("companyURL="+companyURL.getValue());		  
		}
		if(lbCompanyType.getValue()!=null)
		  model.setCompanyType(lbCompanyType.getValue());
		if(tbWebsite.getValue()!=null)
		  model.setWebsite(tbWebsite.getValue());
		if(etbEmail.getValue()!=null)
		  model.setEmail(etbEmail.getValue());
		
		if(pnbLandlinePhone.getValue()!=null){
		  model.setLandline(pnbLandlinePhone.getValue());
		}
		else{
			model.setLandline(0l);
		}
		if(pnbCellPhoneNo1.getValue()!=null)
		  model.setCellNumber1(pnbCellPhoneNo1.getValue());
		/**
		 * Date 27/12/2017 By Jayshree 
		 * To check the null condition
		 */
		if(pnbCellPhoneNo2.getValue()!=null)
		{
		  model.setCellNumber2(pnbCellPhoneNo2.getValue());
		}
		else{
			model.setCellNumber2(0l);
		}
		if(pnbFaxNo.getValue()!=null)
		  model.setFaxNumber(pnbFaxNo.getValue());
		if(tbPointOfContactName.getValue()!=null)
		  model.setPocName(tbPointOfContactName.getValue());
		if(pnbPointOfContactLandline.getValue()!=null)
		  model.setPocLandline(pnbPointOfContactLandline.getValue());
		if(pnbPointOfContactCell.getValue()!=null)
		  model.setPocCell(pnbPointOfContactCell.getValue());
		if(etbPointOfContactEmail.getValue()!=null)
		  model.setPocEmail(etbPointOfContactEmail.getValue());
//		if(tbServiceTaxNo.getValue()!=null)
//		  model.setServiceTaxNo(tbServiceTaxNo.getValue());
//		if(tbVatTaxNo.getValue()!=null)
//		  model.setVatTaxNo(tbVatTaxNo.getValue());
//		if(tbPANCard.getValue()!=null)
//		  model.setPanCard(tbPANCard.getValue());
//		if(tbLicenseNo.getValue()!=null)
//		  model.setLicense(tbLicenseNo.getValue());
		if(socialInfoComposite.getValue()!=null)
		  model.setSocialInfo(socialInfoComposite.getValue());
		if(addressComposite.getValue()!=null)
		  model.setAddress(addressComposite.getValue());
		
		if(tbArticleTypeInfoComposite.getValue()!=null)
			model.setArticleTypeDetails(tbArticleTypeInfoComposite.getValue());

		model.setLogo(uploadComposite.getValue());  
		
		model.setIdCardLogo(uploadIdCardLogo.getValue());
		
  
  
  //******************rohan added this ********************88
		if(StdateDate.getSelectedIndex()!=0)
		{
		model.setStartdate(StdateDate.getValue(StdateDate.getSelectedIndex()));
		}
		
		if(StdateMonth.getSelectedIndex()!=0)
		{
		model.setStartMonth(StdateMonth.getValue(StdateMonth.getSelectedIndex()));
		}
		
		if(endDateDate.getSelectedIndex()!=0)
		{
		model.setEnddate(endDateDate.getValue(endDateDate.getSelectedIndex()));
		}
	
		if(EndDateMonth.getSelectedIndex()!=0)
		{
		model.setEndMonth(EndDateMonth.getValue(EndDateMonth.getSelectedIndex()));
		}
  
		if(tbSignatoryText.getValue()!=null){
			model.setSignatoryText(tbSignatoryText.getValue());
		}
		
		if(companyGSTType.getSelectedIndex()!=0){
			model.setCompanyGSTType(companyGSTType.getValue(companyGSTType.getSelectedIndex()));
		}
		
		if(tbcompanyGSTTypeText.getValue()!=null){
			model.setCompanyGSTTypeText(tbcompanyGSTTypeText.getValue());
		}
	
		model.setUploadHeader(uploadHeader.getValue());
		model.setUploadFooter(uploadFooter.getValue());
		
		model.setIdCarduploadHeader(uploadIdCardHeader.getValue());  
		model.setIdCarduploadFooter(uploadIdCardFooter.getValue());
		
		/** Added by Viraj Date 31-10-2018 for Ordient Ventures to Add Company Profile and Terms And Condition **/
		model.setCompanyProfile(companyProfile.getValue());
		model.setTermsAndCondition(termsAndCondition.getValue());
		/** Ends **/
		model.setUploadDigitalSign(uploadDigitalSign.getValue());
		
		
		/**
		   * Date 31-3-2018
		   * By jayshree
		   * Des.to add the statecode and country code
		   */
		
		if(tbstatecode.getValue()!=null){
			model.setStateCode(tbstatecode.getValue());
		}
		
		if(tbcountrycode.getValue()!=null){
			model.setCountryCode(tbcountrycode.getValue());
		}
		
		//End by jayshree
		
		/**8-12-2018 added by amol**/
		if(tbPfGroupno.getValue()!=null){
			model.setPfGroupNum(tbPfGroupno.getValue());
		}
		
		if(tbPfoffice.getValue()!=null){
			model.setPfOfficeAt(tbPfoffice.getValue());
		}
		
		
		if(dbdateofcoverage.getValue()!=null){
			model.setDateofcoverage(dbdateofcoverage.getValue());
		}
		/**
		 *Updated By: Viraj
		 *Date: 14-01-2019
		 *Description: To save description added in company
		 */
		if(description.getValue() != null) {
			model.setDescription(description.getValue());
		}
		/** Ends **/
		/**
		 * Updated By: Viraj 
		 * Date: 24-03-2019
		 * Description: To add franchise Information   
		 */
		if(lbFranchiseType.getSelectedIndex() != 0) {
			model.setFranchiseType(lbFranchiseType.getValue(lbFranchiseType.getSelectedIndex()));
		}
		if(brandUrl.getValue() != null) {
			model.setBrandUrl(brandUrl.getValue());
		}
		if(masterUrl.getValue() != null) {
			model.setMasterUrl(masterUrl.getValue());
		}
		/** Ends **/
		if(tbDisplayNameForCompanyEmail.getValue()!=null){
			model.setDisplayNameForCompanyEmailId(tbDisplayNameForCompanyEmail.getValue());
		}
		if(tbInvoicePrefix.getValue() != null) {
			model.setInvoicePrefix(tbInvoicePrefix.getValue());
		}
		
		if(tbmerchantId.getValue()!=null){
			model.setMerchantId(tbmerchantId.getValue());
		}
		
		if(feedbackTbl.getValue()!=null){
			model.setFeedbackUrlList(feedbackTbl.getValue());
		}

		model.setIdCardBackSideLogo(uploadIdCardBackSide.getValue());
		
		model.setSchedulingUrl(tbSchedulingUrl.getValue());
		
		if(ibServiceDuration.getValue()!=null){
			model.setMinimumDuration(ibServiceDuration.getValue());
		}else{
			model.setMinimumDuration(0);
		}
		
		if(tbwebsiteserviceTermsAndCondition.getValue()!=null){
			model.setWebsiteServiceTermsAndConditions(tbwebsiteserviceTermsAndCondition.getValue());
		}
		if(olbCurrency.getSelectedIndex()!=0){
			model.setCurrency(olbCurrency.getValue());
		}
		
		if(paymentGatewayId.getValue()!=null){
			model.setPaymentGatewayId(paymentGatewayId.getValue());
		}
		if(paymentGatewayKey.getValue()!=null){
			model.setPaymentGatewayKey(paymentGatewayKey.getValue());
		}
		
		/**Added by sheetal:05-01-2022**/
		if(tbAccountSid.getValue()!=null){
			model.setAccountSid(tbAccountSid.getValue());
		}
		if(tbAPIKey.getValue()!=null){
			model.setApiKey(tbAPIKey.getValue());
		}
		if(tbAPIToken.getValue()!=null){
			model.setApiToken(tbAPIToken.getValue());
		}
		if(tbCallerID.getValue()!=null){
			model.setCallerId(tbCallerID.getValue());
		}
		if(tbApiUrl.getValue()!=null){
			model.setApiUrl(tbApiUrl.getValue());
			LoginPresenter.company.setApiUrl(tbApiUrl.getValue());
		}
		/**end**/
		
		if(taWhatsAppAPIUrl.getValue()!=null){
			model.setWhatsAppApiUrl(taWhatsAppAPIUrl.getValue());
		}
		if(tbWhatsAppIntanceId.getValue()!=null){
			model.setWhatsAppInstaceId(tbWhatsAppIntanceId.getValue());
		}
		
		model.setWhatsAppApiStatus(cbWhatsAppAPIStatus.getValue());
		if(ibMobileNoPrefix.getValue()!=null){
			model.setMobileNoPrefix(ibMobileNoPrefix.getValue());
		}
		
		if(tbAccessToken.getValue()!=null) {
			model.setAccessToken(tbAccessToken.getValue());
		}
		
		if(tbCustomerId.getValue()!=null) {
			model.setCustomerId(tbCustomerId.getValue());
		}
		if(tbUserName.getValue()!=null) {
			model.setUserName(tbUserName.getValue());
		}
		if(tbPassword.getValue()!=null) {
			model.setPassword(tbPassword.getValue());
		}
		if(tbApiId.getValue()!=null) {
			model.setApiId(tbApiId.getValue());
		}
		if(tbApiSecret.getValue()!=null) {
			model.setApiSecret(tbApiSecret.getValue());
		}
		if(branchUsernamePasswordTable.getValue()!=null) {
			model.setBranchUsernamePasswordList(branchUsernamePasswordTable.getValue());
		}

		if(companyObj!=null&&companyObj.getCompanyIntroduction()!=null)
			model.setCompanyIntroduction(companyObj.getCompanyIntroduction());
		
		
		model.setEinvoicevendor(einvoiceVendor.getSelectedItemText());
		
		if(eInvoiceEnvironment.getSelectedIndex()!=0)
			model.seteInvoiceEnvironment(eInvoiceEnvironment.getSelectedItemText());
		else
			model.seteInvoiceEnvironment("");
		
		if(tbZohoOrgID.getValue()!=null)
			model.setZohoOrganisationID(tbZohoOrgID.getValue());
		else
			model.setZohoOrganisationID("");
		
		if(tbZohoCode.getValue()!=null)
			model.setZohoPermissionsCode(tbZohoCode.getValue());
		else
			model.setZohoPermissionsCode("");
		
		if(tbZohoClientId.getValue()!=null)
			model.setZohoClientID(tbZohoClientId.getValue());
		else
			model.setZohoClientID("");
		
		if(tbZohoClientSecret.getValue()!=null)
			model.setZohoClientSecret(tbZohoClientSecret.getValue());
		else
			model.setZohoClientSecret("");
		
		if(tbZohoSurveyUrl.getValue()!=null)
			model.setZohoSurveyUrl(tbZohoSurveyUrl.getValue());
		else
			model.setZohoSurveyUrl("");
		
		if(ibServiceCompletionDeadline.getValue()!=null)
			model.setServiceCompletionDeadline(ibServiceCompletionDeadline.getValue());
		else
			model.setServiceCompletionDeadline(0);
		
		if(ibInvoiceEditDeadline.getValue()!=null)
			model.setInvoiceEditDeadline(ibInvoiceEditDeadline.getValue());
		else
			model.setInvoiceEditDeadline(0);
	}
	
	/**
	 * @author Ashwini
	 * @since 17-1-2022
	 * To fetch url from browser and format it as per requirements for the android app.
	 */
	private String getUrlFromwindow() {
		// TODO Auto-generated method stub
				String url=com.google.gwt.user.client.Window.Location.getHref();
				//String url="https://My.evadev006.appspot.com";
				String urlWithoutHttp="";
				String urlWithoutDomain="";
				String finalurl="";
				
				String[] urlarray=url.split("://");
				System.out.println("arrlength="+urlarray.length);
				if(urlarray.length>1)
					 urlWithoutHttp=urlarray[1];
				else
					urlWithoutHttp=url;
				System.out.println("urlWithoutHttp="+urlWithoutHttp);
				urlarray=urlWithoutHttp.split(".appspot.com");
				System.out.println("arrlength="+urlarray.length);
				if(urlarray.length>0)
					urlWithoutDomain=urlarray[0];
				System.out.println("urlWithoutDomain="+urlWithoutDomain);
				finalurl=urlWithoutDomain.replace(".","-dot-");
				finalurl=finalurl+".appspot.com";
				System.out.println("finalurl="+finalurl);
		return finalurl;
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Company view) 
	{
		companyObj=view;
		/**
		 * Date 20-04-2018 By vijay for orion pest locality load  
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
			setAddressDropDown();
		}
		/**
		 * ends here
		 */
		
		if(view.getBusinessUnitName()!=null)
		  tbCompanyName.setValue(view.getBusinessUnitName());
		if(view.getCompanyType()!=null)
		  lbCompanyType.setValue(view.getCompanyType());
		if(view.getWebsite()!=null)
		  tbWebsite.setValue(view.getWebsite());
		if(view.getEmail()!=null)
		  etbEmail.setValue(view.getEmail());
		if(view.getLandline()!=null)
		  pnbLandlinePhone.setValue(view.getLandline());
		if(view.getCellNumber1()!=null)
		  pnbCellPhoneNo1.setValue(view.getCellNumber1());
		if(view.getCellNumber2()!=null)
		  pnbCellPhoneNo2.setValue(view.getCellNumber2());
		if(view.getFaxNumber()!=null)
		  pnbFaxNo.setValue(view.getFaxNumber());
		if(view.getPocName()!=null)
		  tbPointOfContactName.setValue(view.getPocName());
		if(view.getPocLandline()!=null)
		  pnbPointOfContactLandline.setValue(view.getPocLandline());
		if(view.getPocCell()!=null)
		  pnbPointOfContactCell.setValue(view.getPocCell());
		if(view.getPocEmail()!=null)
		  etbPointOfContactEmail.setValue(view.getPocEmail());
		if(view.getServiceTaxNo()!=null)
		  tbServiceTaxNo.setValue(view.getServiceTaxNo());
		if(view.getVatTaxNo()!=null)
		  tbVatTaxNo.setValue(view.getVatTaxNo());
		if(view.getPanCard()!=null)
		  tbPANCard.setValue(view.getPanCard());
		if(view.getLicense()!=null)
		  tbLicenseNo.setValue(view.getLicense());
		if(view.getSocialInfo()!=null)
		  socialInfoComposite.setValue(view.getSocialInfo());
		if(view.getAddress()!=null)
		  addressComposite.setValue(view.getAddress());
		if(view.getLogo()!=null)
		  uploadComposite.setValue(view.getLogo());
		
		if(view.getIdCardLogo()!=null)
			uploadIdCardLogo.setValue(view.getIdCardLogo());   
		
		if(view.getCompanyURL()!=null){
			companyURL.setValue(view.getCompanyURL());
			 Console.log("companyURL="+view.getCompanyURL());
		}
		
		if(view.getUploadHeader()!=null)
			  uploadHeader.setValue(view.getUploadHeader());
		
		if(view.getUploadFooter()!=null)
			  uploadFooter.setValue(view.getUploadFooter());
		
		if(view.getIdCarduploadHeader()!=null)
			uploadIdCardHeader.setValue(view.getIdCarduploadHeader());
		
		if(view.getIdCarduploadFooter()!=null)
			uploadIdCardFooter.setValue(view.getIdCarduploadFooter());
		
		/** Added by Viraj Date 31-10-2018 for Ordient Ventures to Add Company Profile and Terms And Condition **/
		if(view.getCompanyProfile()!= null)
			companyProfile.setValue(view.getCompanyProfile());
		
		if(view.getTermsAndCondition()!= null)
			termsAndCondition.setValue(view.getTermsAndCondition());
		/** Ends **/
		
		if(view.getArticleTypeDetails()!=null)
			tbArticleTypeInfoComposite.setValue(view.getArticleTypeDetails());
		
		
		if(view.getStartdate()!=null)
			  StdateDate.setTitle(view.getStartdate());
		
		if(view.getUploadDigitalSign()!=null)
			uploadDigitalSign.setValue(view.getUploadDigitalSign());
		
		//***********************rohan changes *************
			
		if(!view.getStartdate().equals(""))
		{
			int count = StdateDate.getItemCount();
			String item=view.getStartdate();
			for(int i=0;i<count;i++)
			{
				if(item.trim().equals(StdateDate.getItemText(i).trim()))
				{
					StdateDate.setSelectedIndex(i);
					break;
				}
			}
		}
		
		
		
		if(!view.getStartMonth().equals(""))
		{
			int count = StdateMonth.getItemCount();
			String item=view.getStartMonth();
			for(int i=0;i<count;i++)
			{
				if(item.trim().equals(StdateMonth.getItemText(i).trim()))
				{
					StdateMonth.setSelectedIndex(i);
					break;
				}
			}
		}
		
		
		if(!view.getEnddate().equals(""))
		{
			int count = endDateDate.getItemCount();
			String item=view.getEnddate();
			for(int i=0;i<count;i++)
			{
				if(item.trim().equals(endDateDate.getItemText(i).trim()))
				{
					endDateDate.setSelectedIndex(i);
					break;
				}
			}
		}
		
		if(!view.getEndMonth().equals(""))
		{
			int count = EndDateMonth.getItemCount();
			String item=view.getEndMonth();
			for(int i=0;i<count;i++)
			{
				if(item.trim().equals(EndDateMonth.getItemText(i).trim()))
				{
					EndDateMonth.setSelectedIndex(i);
					break;
				}
			}
		}
		
		
		if(!view.getSignatoryText().equals("")){
			tbSignatoryText.setValue(view.getSignatoryText());
		}
		
		if(!view.getCompanyGSTType().equals("")){
			int count = companyGSTType.getItemCount();
			String item=view.getCompanyGSTType().trim();
			for (int i = 0; i < count; i++) {
				if(item.trim().equals(companyGSTType.getItemText(i).trim())){
					companyGSTType.setSelectedIndex(i);
					break;
				}
			}
			
		}
		
		if(view.getCompanyGSTTypeText()!=null){
			tbcompanyGSTTypeText.setValue(view.getCompanyGSTTypeText().trim());
		}
		
		
		/**
		   * Date 31-3-2018
		   * By jayshree
		   * Des.to add the statecode and country code
		   */
		
		if(view.getStateCode()!=null){
			tbstatecode.setValue(view.getStateCode().trim());
		}
		
		if(view.getCountryCode()!=null){
			tbcountrycode.setValue(view.getCountryCode().trim());
		}
		
		//End By Jayshree
		
		/**8-12-2018 added by amol **/
		if(view.getPfGroupNum()!=null){
			tbPfGroupno.setValue(view.getPfGroupNum().trim());
		}
		
		if(view.getPfOfficeAt()!=null){
			tbPfoffice.setValue(view.getPfOfficeAt().trim());
		}
		
		
		/**15-12-2018 added by amol***/
		
		if(view.getDateofcoverage()!=null){
			dbdateofcoverage.setValue(view.getDateofcoverage());
		}
		/**
		 *Updated By: Viraj
		 *Date: 14-01-2019
		 *Description: To save description added in company
		 */
		if(view.getDescription() != null) {
			description.setValue(view.getDescription());
		}
		/** Ends **/
		
		/**
		 * Updated By: Viraj 
		 * Date: 24-03-2019
		 * Description: To add franchise Information   
		 */
		if(view.getFranchiseType() != null) {
			int count = lbFranchiseType.getItemCount();
			String item=view.getFranchiseType().trim();
			for (int i = 0; i < count; i++) {
				if(item.trim().equals(lbFranchiseType.getItemText(i).trim())){
					lbFranchiseType.setSelectedIndex(i);
					break;
				}
			}
		}
		
		if(view.getBrandUrl() != null) {
			brandUrl.setValue(view.getBrandUrl());
		}
		
		if(view.getMasterUrl() != null) {
			masterUrl.setValue(view.getMasterUrl());
		}
		/** Ends **/
		
		if(view.getDisplayNameForCompanyEmailId()!=null){
			tbDisplayNameForCompanyEmail.setValue(view.getDisplayNameForCompanyEmailId());
		}
        if(view.getInvoicePrefix() != null) {
			tbInvoicePrefix.setValue(view.getInvoicePrefix());
		}

        if(view.getMerchantId()!=null){
        	tbmerchantId.setValue(view.getMerchantId());
        }
        
//        if(view.getFeedbackUrl1()!=null){
//        	tbFeedbackUrl1.setValue(view.getFeedbackUrl1());
//        }
//        if(view.getFeedbackUrl2()!=null){
//        	tbFeedbackUrl2.setValue(view.getFeedbackUrl2());
//        }
        
        if(view.getFeedbackUrlList()!=null){
        	feedbackTbl.setValue(view.getFeedbackUrlList());
        }
        
        if(view.getIdCardBackSideLogo()!=null){
        	uploadIdCardBackSide.setValue(view.getIdCardBackSideLogo());
        }
        
        if(view.getSchedulingUrl()!=null){
        	tbSchedulingUrl.setValue(view.getSchedulingUrl());
        }
        
        ibServiceDuration.setValue(view.getMinimumDuration());
        
        if(view.getCurrency()!=null){
        	olbCurrency.setValue(view.getCurrency());
        }
        
        if(view.getPaymentGatewayId()!=null){
        	paymentGatewayId.setValue(view.getPaymentGatewayId());
        }
        if(view.getPaymentGatewayKey()!=null){
        	paymentGatewayKey.setValue(view.getPaymentGatewayKey());
        }
        if(view.getWebsiteServiceTermsAndConditions()!=null){
        	tbwebsiteserviceTermsAndCondition.setValue(view.getWebsiteServiceTermsAndConditions());
        }
        
        /**Added by sheetal:05-01-2022**/
        if(view.getAccountSid()!=null){
        	tbAccountSid.setValue(view.getAccountSid());
        }
        if(view.getApiKey()!=null){
        	tbAPIKey.setValue(view.getApiKey());
        }
        if(view.getApiToken()!=null){
        	tbAPIToken.setValue(view.getApiToken());
        }
        if(view.getCallerId()!=null){
        	tbCallerID.setValue(view.getCallerId());
        }
        if(view.getApiUrl()!=null){
        	tbApiUrl.setValue(view.getApiUrl());
        }
        
        if(view.getWhatsAppApiUrl()!=null){
        	taWhatsAppAPIUrl.setValue(view.getWhatsAppApiUrl());
        }
        if(view.getWhatsAppInstaceId()!=null){
        	tbWhatsAppIntanceId.setValue(view.getWhatsAppInstaceId());
        }
        
        cbWhatsAppAPIStatus.setValue(view.isWhatsAppApiStatus());
        if(view.getMobileNoPrefix()!=0)
        ibMobileNoPrefix.setValue(view.getMobileNoPrefix());
        
        if(view.getAccessToken()!=null) {
        	tbAccessToken.setValue(view.getAccessToken());
        }
        
        if(view.getCustomerId()!=null) {
        	tbCustomerId.setValue(view.getCustomerId());
        }
        if(view.getUserName()!=null) {
        	tbUserName.setValue(view.getUserName());
        }
        if(view.getPassword()!=null) {
        	tbPassword.setValue(view.getPassword());
        }
        if(view.getApiId()!=null) {
        	tbApiId.setValue(view.getApiId());
        }
        if(view.getApiSecret()!=null) {
        	tbApiSecret.setValue(view.getApiSecret());
        }
        if(view.getBranchUsernamePasswordList()!=null) {
        	Console.log("view.getBranchUsernamePasswordList()!=null");
        	branchUsernamePasswordTable.setValue(view.getBranchUsernamePasswordList());
        	Console.log("BranchUsernamePasswordList set to view");
        }
        if(view.getEinvoicevendor()!=null) {
        	einvoiceVendor.setValue(view.getEinvoicevendor());
        }
        if(view.geteInvoiceEnvironment()!=null) {
        	eInvoiceEnvironment.setValue(view.geteInvoiceEnvironment());
        }
        if(view.getZohoOrganisationID()!=null) {
        	tbZohoOrgID.setValue(view.getZohoOrganisationID());
        }
        if(view.getZohoPermissionsCode()!=null) {
        	tbZohoCode.setValue(view.getZohoPermissionsCode());
        }
        if(view.getZohoClientID()!=null) {
        	tbZohoClientId.setValue(view.getZohoClientID());
        }
        if(view.getZohoClientSecret()!=null) {
        	tbZohoClientSecret.setValue(view.getZohoClientSecret());
        }
        if(view.getZohoSurveyUrl()!=null) {
        	tbZohoSurveyUrl.setValue(view.getZohoSurveyUrl());
        }
        if(view.getServiceCompletionDeadline()!=null)
        	ibServiceCompletionDeadline.setValue(view.getServiceCompletionDeadline());
        
        if(view.getInvoiceEditDeadline()!=null)
        	ibInvoiceEditDeadline.setValue(view.getInvoiceEditDeadline());
        
	}
	
	
	/**
	 * Date 20-04-2018 By vijay
	 * here i am refreshing address composite data if global list size greater than drop down list size
	 * because we are adding locality and city from an entity in view state to global list if doest not exist in global list 
	 */
	private void setAddressDropDown() {
		if(LoginPresenter.globalLocality.size()>addressComposite.locality.getItems().size()	){
			addressComposite.locality.setListItems(LoginPresenter.globalLocality);
		}
	}
	/**
	 * ends here 
	 */
	
	
	// Hand written code shift in presenter
	
	
	
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			System.out.println("New mode");
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
				 System.out.println("popup menu");
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				 System.out.println("normal flow");
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.COMPANY,LoginPresenter.currentModule.trim());
	}
	
	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
	
	}

	
	@Override
	public void onClick(ClickEvent event) {
//		if(event.getSource().equals(createConfigs))
//		{
//			System.out.println("in side on click");
//			
//			
//		}	
		
		if(event.getSource()==addFeedbackBtn){
			
			String msg="";
			if(olbCustomerCategory.getSelectedIndex()==0){
				msg="Please select customer category!\n";
				
			}
			if(tbFeedbackUrl.getValue()==null||tbFeedbackUrl.getValue().equals("")){
				msg=msg+"Please add feedback URL!";
			}
			if(!msg.equals("")){
				showDialogMessage(msg);
				return;
			}
			
			FeedbackUrlAsPerCustomerCategory feed=new FeedbackUrlAsPerCustomerCategory();
			feed.setCustomerCategory(olbCustomerCategory.getValue());
			feed.setFeedbackUrl(tbFeedbackUrl.getValue());
			
			if(feedbackTbl.getValue()!=null&&feedbackTbl.getValue().size()!=0){
				for(FeedbackUrlAsPerCustomerCategory obj:feedbackTbl.getValue()){
					if(obj.getCustomerCategory().equals(olbCustomerCategory.getValue())){
						showDialogMessage("Customer category is already added!");
						return;
					}
				}
			}
			
			feedbackTbl.getDataprovider().getList().add(feed);
		}
		
		if(event.getSource()==addBranchUsernamePassword){
			
			String msg="";
			if(olbBranch.getSelectedIndex()==0){
				showDialogMessage("Please select branch");
				return;				
			}
			if(tbBranchUsername.getText()==null||tbBranchUsername.getText().equals(""))
			{
				showDialogMessage("Please enter Username");
				return;
			}
			if(tbBranchPassword.getText()==null||tbBranchPassword.getText().equals(""))
			{
				showDialogMessage("Please enter Password");
				return;
			}
			
			
			BranchUsernamePassword entry=new BranchUsernamePassword();
			entry.setBranchName(olbBranch.getValue());
			entry.setUsername(tbBranchUsername.getText());
			entry.setPassword(tbBranchPassword.getText());
			
			if(branchUsernamePasswordTable.getValue()!=null&&branchUsernamePasswordTable.getValue().size()!=0){
				for(BranchUsernamePassword obj:branchUsernamePasswordTable.getValue()){
					if(obj.getBranchName().equals(olbBranch.getValue())){
						showDialogMessage("Branch already added!");
						return;
					}
				}
			}
			
			branchUsernamePasswordTable.getDataprovider().getList().add(entry);
		}
		
		if(event.getSource()==btnIntroduction){
			
			reactonOnIntroductionButton();
			
		}
		if(event.getSource()==intoductionpopup.getLblOk()){
			reactonIntroductionOkbutton();
		}
		if(event.getSource()==intoductionpopup.getLblCancel()){
			intoductionpopup.hidePopUp();
		}
	}
	
	
	private void reactonIntroductionOkbutton() {
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Company());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
			
				for(SuperModel model : result){
					Company company = (Company) model;
					if(intoductionpopup.getTaIntroduction().getValue()!=null && !intoductionpopup.getTaIntroduction().getValue().equals("")){
						company.setCompanyIntroduction(intoductionpopup.getTaIntroduction().getValue());
						companyObj.setCompanyIntroduction(intoductionpopup.getTaIntroduction().getValue());
						
					}
					else{
						company.setCompanyIntroduction("");
						companyObj.setCompanyIntroduction("");
					}
					
					async.save(company, new AsyncCallback<ReturnFromServer>() {
						
						@Override
						public void onSuccess(ReturnFromServer result) {
							// TODO Auto-generated method stub
							intoductionpopup.showDialogMessage("Updated sucessfully");
							intoductionpopup.hidePopUp();
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
					});
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}

	private void reactonOnIntroductionButton() {

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Company());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
			
				for(SuperModel model : result){
					Company company = (Company) model;
					intoductionpopup.showPopUp();
					if(company.getCompanyIntroduction()!=null)
					intoductionpopup.getTaIntroduction().setValue(company.getCompanyIntroduction());
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		processLevelBar.setVisibleFalse(false);
		/**
		 * Updated By: Viraj 
		 * Date: 24-03-2019
		 * Description: To add franchise Information   
		 */
		brandUrl.setEnabled(false);
		masterUrl.setEnabled(false);
		if(lbFranchiseType.getSelectedIndex() == 3) {
			brandUrl.setEnabled(true);
			masterUrl.setEnabled(true);
		}
		if(lbFranchiseType.getSelectedIndex() == 1) {
			brandUrl.setEnabled(false);
			masterUrl.setEnabled(false);
		}
		if(lbFranchiseType.getSelectedIndex() == 2) {
			brandUrl.setEnabled(true);
		}
		/** Ends **/
	}

	public ArticleTypeComposite getTbArticleTypeInfoComposite() {
		return tbArticleTypeInfoComposite;
	}

	public void setTbArticleTypeInfoComposite(
			ArticleTypeComposite tbArticleTypeInfoComposite) {
		this.tbArticleTypeInfoComposite = tbArticleTypeInfoComposite;
	}

	/**
	 * Updated By: Viraj 
	 * Date: 24-03-2019
	 * Description: To add franchise Information   
	 */
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(lbFranchiseType)) {
			if(lbFranchiseType.getSelectedIndex() == 3) {
				brandUrl.setEnabled(true);
				masterUrl.setEnabled(true);
			}
			
			if(lbFranchiseType.getSelectedIndex() == 2) {
				brandUrl.setEnabled(true);
				masterUrl.setEnabled(false);
			}
			
			if(lbFranchiseType.getSelectedIndex() == 1) {
				brandUrl.setEnabled(false);
				masterUrl.setEnabled(false);
			}
		}
		
	}
	/** Ends **/
	
	

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		
		feedbackTbl.getTable().redraw();
		branchUsernamePasswordTable.getTable().redraw();
		tbArticleTypeInfoComposite.getArticletypetable().getTable().redraw();
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
		feedbackTbl.setEnable(state);
		branchUsernamePasswordTable.setEnable(state);
	}

	@Override
	public boolean validate() {
		boolean flag = super.validate();
		
		if(cbWhatsAppAPIStatus.getValue()) {
			if(ibMobileNoPrefix.getValue()==null || ibMobileNoPrefix.getValue()==0) {
				showDialogMessage("Please add mobile no prefix in the whats app integration tab");
				return false;
			}
		}
		
		if(!tbApiId.getValue().equals("") || !tbApiSecret.getValue().equals("") || !tbUserName.getValue().equals("")
				|| !tbPassword.getValue().equals("")){
			
			if(tbApiId.getValue().equals("")){
				showDialogMessage("API id is mandaroy for einvoice");
				return false;
			}
			if(tbApiSecret.getValue().equals("")){
				showDialogMessage("API Secret is mandaroy for einvoice");
				return false;
			}
			if(tbUserName.getValue().equals("")){
				showDialogMessage("Username is mandaroy for einvoice");
				return false;
			}
			if(tbPassword.getValue().equals("")){
				showDialogMessage("Password is mandaroy for einvoice");
				return false;
			}
		}
		
		if(ibServiceCompletionDeadline.getValue()!=null&&(ibServiceCompletionDeadline.getValue()<0||ibServiceCompletionDeadline.getValue()>31)){
			showDialogMessage("Enter value between 1 to 31 in Service Completion Deadline");
			return false;
		}
		if(ibInvoiceEditDeadline.getValue()!=null&&(ibInvoiceEditDeadline.getValue()<0||ibInvoiceEditDeadline.getValue()>31)){
			showDialogMessage("Enter value between 1 to 31 in Invoice Edit Deadline");
			return false;
		}
		
		return flag && true;
	}
	
	
}
