package com.slicktechnologies.client.views.settings.company;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.businessunitlayer.FeedbackUrlAsPerCustomerCategory;
import com.slicktechnologies.shared.common.personlayer.EmployeeCTCTemplate;

public class FeedbackUrlTable extends SuperTable<FeedbackUrlAsPerCustomerCategory>{
	TextColumn<FeedbackUrlAsPerCustomerCategory> categoryCol;
	TextColumn<FeedbackUrlAsPerCustomerCategory> feedbackUrlCol;
	
	
	Column<FeedbackUrlAsPerCustomerCategory,String> deleteCol;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addCategoryCol();
		feedbackUrlCol();
		deleteCol();
	}

	private void deleteCol() {
		// TODO Auto-generated method stub
		ButtonCell btnCell = new ButtonCell();
		deleteCol = new Column<FeedbackUrlAsPerCustomerCategory, String>(btnCell) {
			@Override
			public String getValue(FeedbackUrlAsPerCustomerCategory object) {

				return "Delete";
			}
		};
		table.addColumn(deleteCol, "");
//		table.setColumnWidth(deleteCol, 50, Unit.PX);
		
		deleteCol.setFieldUpdater(new FieldUpdater<FeedbackUrlAsPerCustomerCategory, String>() {
			@Override
			public void update(int index, FeedbackUrlAsPerCustomerCategory object,String value) {
				getDataprovider().getList().remove(index);
				table.redraw();
			}
		});
	}

	private void addCategoryCol() {
		// TODO Auto-generated method stub
		categoryCol = new TextColumn<FeedbackUrlAsPerCustomerCategory>() {
			@Override
			public String getValue(FeedbackUrlAsPerCustomerCategory object) {
				return object.getCustomerCategory().trim();
			}
		};
		table.addColumn(categoryCol, "Category");
//		table.setColumnWidth(categoryCol, 200, Unit.PX);
//		categoryCol.setSortable(true);
	}

	private void feedbackUrlCol() {
		// TODO Auto-generated method stub
		feedbackUrlCol = new TextColumn<FeedbackUrlAsPerCustomerCategory>() {
			@Override
			public String getValue(FeedbackUrlAsPerCustomerCategory object) {
				return object.getFeedbackUrl().trim();
			}
		};
		table.addColumn(feedbackUrlCol, "Feedback URL");
//		table.setColumnWidth(feedbackUrlCol, 200, Unit.PX);
//		feedbackUrlCol.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();
	}

	private void addeditColumn() {
		// TODO Auto-generated method stub
		addCategoryCol();
		feedbackUrlCol();
		deleteCol();
	}

	private void addViewColumn() {
		// TODO Auto-generated method stub
		addCategoryCol();
		feedbackUrlCol();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
