package com.slicktechnologies.client.views.settings.company;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessunitlayer.BranchUsernamePassword;
import com.slicktechnologies.shared.common.businessunitlayer.FeedbackUrlAsPerCustomerCategory;

public class BranchUsernamePasswordTable extends SuperTable<BranchUsernamePassword> {

	
	TextColumn<BranchUsernamePassword> branchname;
	TextColumn<BranchUsernamePassword> username;
	TextColumn<BranchUsernamePassword> password;
	
	Column<BranchUsernamePassword,String> deleteCol;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addBranchNameColumn();
		addUsernameColumn();
		addPasswordColumn();
		addDeleteColumn();
	}
	private void addBranchNameColumn() {
		// TODO Auto-generated method stub
		branchname = new TextColumn<BranchUsernamePassword>() {
			@Override
			public String getValue(BranchUsernamePassword object) {
				return object.getBranchName().trim();
			}
		};
		table.addColumn(branchname, "Branch");
	}
	private void addUsernameColumn() {
		// TODO Auto-generated method stub
		username = new TextColumn<BranchUsernamePassword>() {
			@Override
			public String getValue(BranchUsernamePassword object) {
				return object.getUsername().trim();
			}
		};
		table.addColumn(username, "Username");
	}
	private void addPasswordColumn() {
		// TODO Auto-generated method stub
		password = new TextColumn<BranchUsernamePassword>() {
			@Override
			public String getValue(BranchUsernamePassword object) {
				int len=object.getPassword().length();
				String passwordChars="";
				for(int i=0;i<len;i++)
					passwordChars+="*";
				return passwordChars;//object.getPassword().trim();
			}
		};
		table.addColumn(password, "Password");
	}
	private void addDeleteColumn() {
		// TODO Auto-generated method stub
		ButtonCell btnCell = new ButtonCell();
		deleteCol = new Column<BranchUsernamePassword, String>(btnCell) {
			@Override
			public String getValue(BranchUsernamePassword object) {

				return "Delete";
			}
		};
		table.addColumn(deleteCol, "");
//		table.setColumnWidth(deleteCol, 50, Unit.PX);
		
		deleteCol.setFieldUpdater(new FieldUpdater<BranchUsernamePassword, String>() {
			@Override
			public void update(int index, BranchUsernamePassword object,String value) {
				getDataprovider().getList().remove(index);
				table.redraw();
			}
		});
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();
		
	}
	
	private void addeditColumn() {
		// TODO Auto-generated method stub
		addBranchNameColumn();
		addUsernameColumn();
		addPasswordColumn();
		addDeleteColumn();
	}

	private void addViewColumn() {
		// TODO Auto-generated method stub
		addBranchNameColumn();
		addUsernameColumn();
		addPasswordColumn();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
