package com.slicktechnologies.client.views.settings.roledefinition;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;

public class RoleTable extends SuperTable<String> {
	TextColumn<String> getRoleNameCol;
	Column<String,String> getDeleteButtonCol;

	@Override
	public void createTable() {
		getRoleNameCol();
		getDeleteButtonCol();
		createFieldUpdaterdeleteColumn();
	}

	
	private void getRoleNameCol() {
		getRoleNameCol = new TextColumn<String>() {
			@Override
			public String getValue(String object) {
				return object;
			}
		};	
		table.addColumn(getRoleNameCol, "Role Name");
	}


	private void getDeleteButtonCol() {
		ButtonCell btnCell = new ButtonCell();
		getDeleteButtonCol = new Column<String, String>(btnCell) {
			@Override
			public String getValue(String object) {
				return "Delete";
			}
		};
		table.addColumn(getDeleteButtonCol, "Delete");	
	}


	protected void createFieldUpdaterdeleteColumn(){
		getDeleteButtonCol.setFieldUpdater(new FieldUpdater<String, String>() {
			@Override
			public void update(int index, String object, String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);
		if (state) {
			getRoleNameCol();
			getDeleteButtonCol();
			createFieldUpdaterdeleteColumn();
		} else {
			getRoleNameCol();
		}
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
