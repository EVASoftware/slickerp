package com.slicktechnologies.client.views.settings.roledefinition;

import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.RoleDefinition;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class RoleDefinitionForm extends FormTableScreen<RoleDefinition> implements ClickHandler, ChangeHandler{
	
	CheckBox cbStatus;
	ObjectListBox<Type> olbDocumentName;
	ObjectListBox<ConfigCategory> olbModuleName;
	ListBox lbRoleType;
	ObjectListBox<Config> olbRole;
	
	RoleTable roleTable;
	Button btnAdd;
	
	RoleDefinition roleDefinitionObject;
	
	private void initializeWidget(){
		
		cbStatus=new CheckBox();
		cbStatus.setValue(true);
		
		olbModuleName = new ObjectListBox<ConfigCategory>();
		olbDocumentName = new ObjectListBox<Type>();
		CategoryTypeFactory.initiateOneManyFunctionality(this.olbModuleName,this.olbDocumentName);
		AppUtility.makeTypeListBoxLive(this.olbDocumentName, Screen.MODULENAME);
		
		olbModuleName = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbModuleName, Screen.MODULENAME);
		olbModuleName.addChangeHandler(this);
		olbDocumentName = new ObjectListBox<Type>();		
		AppUtility.makeTypeListBoxLive(olbDocumentName, Screen.DOCUMENTNAME);
		
		lbRoleType=new ListBox();
		lbRoleType.addItem("--SELECT--");
		lbRoleType.addItem("Sales Person");
		lbRoleType.addItem("Purchase Engineer");
		lbRoleType.addItem("Assign To 1");
		lbRoleType.addItem("Assign To 2");
		lbRoleType.addItem("Requested By");
		lbRoleType.addItem("Issued By");
		lbRoleType.addItem("Person Responsible");
		lbRoleType.addItem("Service Engineer");
		lbRoleType.addItem("Technician");
		lbRoleType.addItem("Assigned To");
		/**
		 * Date : 11-12-2018 By ANIL
		 */
		lbRoleType.addItem("Authorised By");
	/*** Date 26-03-2019 by Vijay for NBHC CCPM only ****/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
			lbRoleType.addItem("Zonal Head");
		}
		lbRoleType.addItem("Operator");
		lbRoleType.addItem("Manager");
		lbRoleType.addItem("Admin");
		
		lbRoleType.addItem("Account Manager");
		
		olbRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbRole,Screen.EMPLOYEEROLE);
		
		roleTable =new RoleTable();
		btnAdd=new Button("Add");
		btnAdd.addClickHandler(this);
	}

	public RoleDefinitionForm(SuperTable<RoleDefinition> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		cbStatus.setValue(true);
		table.connectToLocal();
	}

	@Override
	public void createScreen() {
		initializeWidget();
		this.processlevelBarNames=new String[]{"New"};
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingRoleDef=fbuilder.setlabel("Role Definition").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Module Name",olbModuleName);
		FormField folbModuleName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Document Name",olbDocumentName);
		FormField folbDocumentName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Role Type",lbRoleType);
		FormField flbRoleType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Role",olbRole);
		FormField folbRole= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnAdd);
		FormField fbtnAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingRoleDetails=fbuilder.setlabel("Role Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",roleTable.getTable());
		FormField froleDefinitionTbl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		
		
		FormField[][] formfield = {   
			{fgroupingRoleDef},
			{folbModuleName,folbDocumentName,flbRoleType,fcbStatus},
			{fgroupingRoleDetails},
			{folbRole,fbtnAdd},
			{froleDefinitionTbl},
		};
		this.fields=formfield;
	}

	@Override
	public void updateModel(RoleDefinition model) {
		if(olbModuleName.getSelectedIndex()!=0){
			model.setModuleName(olbModuleName.getValue());
		}
		if(olbDocumentName.getSelectedIndex()!=0){
			model.setDocumentName(olbDocumentName.getValue(olbDocumentName.getSelectedIndex()));
		}
		if(lbRoleType.getSelectedIndex()!=0){
			model.setRoleType(lbRoleType.getValue(lbRoleType.getSelectedIndex()));
		}
		model.setStatus(cbStatus.getValue());
		
		if(roleTable.getValue()!=null){
			model.setRoleList(roleTable.getValue());
		}
		manageRoleDefinition(model);
		roleDefinitionObject=model;
		presenter.setModel(model);
	}

	@Override
	public void updateView(RoleDefinition view) {
		roleDefinitionObject=view;
		if(view.getModuleName()!=null){
			olbModuleName.setValue(view.getModuleName());
		}
		if(view.getDocumentName()!=null){
			ConfigCategory cat=olbModuleName.getSelectedItem();
			if(cat!=null){
				AppUtility.makeLiveTypeDropDown(olbDocumentName, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
			}
			olbDocumentName.setValue(view.getDocumentName());
		}
		if(view.getRoleType()!=null){
			int count=lbRoleType.getItemCount();
			for(int i=0;i<count;i++){
				String roleName=lbRoleType.getItemText(i);
				if(view.getRoleType().equals(roleName)){
					lbRoleType.setSelectedIndex(i);
					break;
				}
			}
		}
		cbStatus.setValue(view.isStatus());
		roleTable.setValue(view.getRoleList());
		presenter.setModel(view);
		
	}

	
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Download"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Download"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains("Download"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.ROLEDEFINITION,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void clear() {
		super.clear();
		roleTable.clear();
		cbStatus.setValue(true);
	}
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		roleTable.setEnable(state);
	}
	@Override
	public boolean validate() {
		boolean superValidate=super.validate();
		List<String> roleList=roleTable.getDataprovider().getList();
		if(roleList.size()==0){
			showDialogMessage("Role table should not be empty!");
			return false;
		}
		return superValidate;
	}

	@Override
	public void setToNewState() {
		super.setToNewState();
		processLevelBar.setVisibleFalse(true);
		if(roleDefinitionObject!=null){
			SuperModel model=new RoleDefinition();
			model=roleDefinitionObject;
			AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.ROLEDEFINITION, roleDefinitionObject.getCount(), null,null,null, false, model, null);
		}
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		SuperModel model=new MultilevelApproval();
		model=roleDefinitionObject;
		AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.ROLEDEFINITION, roleDefinitionObject.getCount(), null,null,null, false, model, null);
	
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		this.olbModuleName.setEnabled(false);
		this.olbDocumentName.setEnabled(false);
		this.lbRoleType.setEnabled(false);
	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource().equals(btnAdd)){
			if(olbRole.getSelectedIndex()!=0){
				List<String> roleList=roleTable.getDataprovider().getList();
				if(roleList.size()!=0){
					boolean duplicateFlag=false;
					for(String role:roleList){
						if(role.trim().equals(olbRole.getValue().trim())){
							duplicateFlag=true;
							break;
						}
					}
					if(!duplicateFlag){
						roleTable.getDataprovider().getList().add(olbRole.getValue());
					}
				}else{
					roleTable.getDataprovider().getList().add(olbRole.getValue());
				}
			}else{
				showDialogMessage("Please Select Role!");
			}
		}
	}
	
	public void manageRoleDefinition(RoleDefinition roleDifinitionModel){
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		if(scrState.equals(ScreeenState.NEW)){
			LoginPresenter.globalRoleDefinition.add(roleDifinitionModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalRoleDefinition.size();i++){
				if(LoginPresenter.globalRoleDefinition.get(i).getId().equals(roleDifinitionModel.getId())){
					LoginPresenter.globalRoleDefinition.add(roleDifinitionModel);
					LoginPresenter.globalRoleDefinition.remove(i);
				}
			}
		}
	}

	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(olbModuleName)){
			if(olbModuleName.getSelectedIndex()!=0){
				ConfigCategory cat=olbModuleName.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(olbDocumentName, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
	}
	
	
	
}
