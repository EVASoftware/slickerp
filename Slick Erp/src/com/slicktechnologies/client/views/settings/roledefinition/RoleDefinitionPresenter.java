package com.slicktechnologies.client.views.settings.roledefinition;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.inventory.warehouse.WareHouseForm;
import com.slicktechnologies.client.views.inventory.warehouse.WareHousePresenter;
import com.slicktechnologies.client.views.inventory.warehouse.WareHouseTable;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.RoleDefinition;

public class RoleDefinitionPresenter extends FormTableScreenPresenter<RoleDefinition> {

	RoleDefinitionForm form;
	public RoleDefinitionPresenter(FormTableScreen<RoleDefinition> view,RoleDefinition model) {
		super(view, model);
		form = (RoleDefinitionForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getRoleDefinitionQuery());
		form.setPresenter(this);

		boolean isDownload = AuthorizationHelper.getDownloadAuthorization(Screen.ROLEDEFINITION, LoginPresenter.currentModule.trim());
		if (isDownload == false) {
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	public MyQuerry getRoleDefinitionQuery() {
		MyQuerry quer = new MyQuerry(new Vector<Filter>(), new RoleDefinition());
		return quer;
	}
	
	public static RoleDefinitionForm initialize() {
		RoleDefinitionTable gentableScreen = new RoleDefinitionTable();
		RoleDefinitionForm form = new RoleDefinitionForm(gentableScreen,FormTableScreen.UPPER_MODE, true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		RoleDefinitionPresenter presenter = new RoleDefinitionPresenter(form,new RoleDefinition());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		if (text.equals("New")) {
			form.setToNewState();
			initialize();
		}
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		model = new RoleDefinition();
	}
	@Override
	public void reactOnDownload() {
		CsvServiceAsync async = GWT.create(CsvService.class);
		ArrayList<RoleDefinition> roleArray = new ArrayList<RoleDefinition>();
		List<RoleDefinition> list = form.getSupertable().getDataprovider().getList();
		roleArray.addAll(list);
		async.setRoleDefinitionList(roleArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 119;
				Window.open(url, "test", "enabled");
			}
		});
	}
	
	

}
