package com.slicktechnologies.client.views.settings.roledefinition;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.RoleDefinition;
import com.slicktechnologies.shared.common.inventory.WareHouse;

public class RoleDefinitionTable extends SuperTable<RoleDefinition> {

	TextColumn<RoleDefinition> getRoleDefIdCol;
	TextColumn<RoleDefinition> getRoleDefModuleNameCol;
	TextColumn<RoleDefinition> getRoleDefDocumentNameCol;
	TextColumn<RoleDefinition> getRoleDefRoleTypeCol;
	TextColumn<RoleDefinition> getRoleDefStatusCol;
	
	@Override
	public void createTable() {
		getRoleDefIdCol();
		getRoleDefModuleNameCol();
		getRoleDefDocumentNameCol();
		getRoleDefRoleTypeCol();
		getRoleDefStatusCol();
	}

	private void getRoleDefIdCol() {
		getRoleDefIdCol=new TextColumn<RoleDefinition>() {
			@Override
			public String getValue(RoleDefinition object) {
				if(object.getCount()!=0){
					return object.getCount()+"";
				}
				return "";
			}
		};
		table.addColumn(getRoleDefIdCol,"ID");
		getRoleDefIdCol.setSortable(true);
		table.setColumnWidth(getRoleDefIdCol, 100, Unit.PX);
	}

	private void getRoleDefModuleNameCol() {
		getRoleDefModuleNameCol=new TextColumn<RoleDefinition>() {
			@Override
			public String getValue(RoleDefinition object) {
				if(object.getModuleName()!=null){
					return object.getModuleName();
				}
				return "";
			}
		};
		table.addColumn(getRoleDefModuleNameCol,"Module Name");
		getRoleDefModuleNameCol.setSortable(true);
		table.setColumnWidth(getRoleDefModuleNameCol, 100, Unit.PX);
	}

	private void getRoleDefDocumentNameCol() {
		getRoleDefDocumentNameCol=new TextColumn<RoleDefinition>() {
			@Override
			public String getValue(RoleDefinition object) {
				if(object.getDocumentName()!=null){
					return object.getDocumentName();
				}
				return "";
			}
		};
		table.addColumn(getRoleDefDocumentNameCol,"Document Name");
		getRoleDefDocumentNameCol.setSortable(true);
		table.setColumnWidth(getRoleDefDocumentNameCol, 100, Unit.PX);
		
	}

	private void getRoleDefRoleTypeCol() {
		getRoleDefRoleTypeCol=new TextColumn<RoleDefinition>() {
			@Override
			public String getValue(RoleDefinition object) {
				if(object.getRoleType()!=null){
					return object.getRoleType();
				}
				return "";
			}
		};
		table.addColumn(getRoleDefRoleTypeCol,"Role Type");
		getRoleDefRoleTypeCol.setSortable(true);
		table.setColumnWidth(getRoleDefRoleTypeCol, 100, Unit.PX);
		
	}

	private void getRoleDefStatusCol() {
		getRoleDefStatusCol=new TextColumn<RoleDefinition>() {
			@Override
			public String getValue(RoleDefinition object) {
				if(object.isStatus()){
					return "Active";
				}else{
					return "In Active";
				}
			}
		};
		table.addColumn(getRoleDefStatusCol,"Status");
		getRoleDefStatusCol.setSortable(true);
		table.setColumnWidth(getRoleDefStatusCol, 100, Unit.PX);
		
	}
	
	

	@Override
	public void addColumnSorting() {
//		super.addColumnSorting();
		addSortingOnIdColumn();
		addSortingOnModuleColumn();
		addSortingOnDocumnetColumn();
		addSortingOnRoleTypeColumn();
		addSortingOnStatusColumn();
	}

	private void addSortingOnIdColumn() {
		List<RoleDefinition> list = getDataprovider().getList();
		columnSort = new ListHandler<RoleDefinition>(list);
		columnSort.setComparator(getRoleDefIdCol,new Comparator<RoleDefinition>() {
			@Override
			public int compare(RoleDefinition e1, RoleDefinition e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingOnModuleColumn() {
		List<RoleDefinition> list=getDataprovider().getList();
		columnSort=new ListHandler<RoleDefinition>(list);
		columnSort.setComparator(getRoleDefModuleNameCol, new Comparator<RoleDefinition>()
		{
			@Override
			public int compare(RoleDefinition e1, RoleDefinition e2) {
				if (e1 != null && e2 != null) {
					if (e1.getModuleName() != null&& e2.getModuleName() != null) {
						return e1.getModuleName().compareTo(e2.getModuleName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingOnDocumnetColumn() {
		List<RoleDefinition> list=getDataprovider().getList();
		columnSort=new ListHandler<RoleDefinition>(list);
		columnSort.setComparator(getRoleDefDocumentNameCol, new Comparator<RoleDefinition>()
		{
			@Override
			public int compare(RoleDefinition e1, RoleDefinition e2) {
				if (e1 != null && e2 != null) {
					if (e1.getDocumentName() != null&& e2.getDocumentName() != null) {
						return e1.getDocumentName().compareTo(e2.getDocumentName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingOnRoleTypeColumn() {
		List<RoleDefinition> list=getDataprovider().getList();
		columnSort=new ListHandler<RoleDefinition>(list);
		columnSort.setComparator(getRoleDefRoleTypeCol, new Comparator<RoleDefinition>()
		{
			@Override
			public int compare(RoleDefinition e1, RoleDefinition e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRoleType() != null&& e2.getRoleType() != null) {
						return e1.getRoleType().compareTo(e2.getRoleType());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingOnStatusColumn() {
		List<RoleDefinition> list = getDataprovider().getList();
		columnSort = new ListHandler<RoleDefinition>(list);
		columnSort.setComparator(getRoleDefStatusCol, new Comparator<RoleDefinition>() {
			@Override
			public int compare(RoleDefinition e1, RoleDefinition e2) {
				if (e1 != null && e2 != null) {
					if (e1.isStatus() == e2.isStatus()) {
						return 0;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
