package com.slicktechnologies.client.views.settings.branch;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.articletypecomposite.ArticleTypeComposite;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.googlecode.gwtTableToExcel.client.TableToExcelClient;
import com.googlecode.gwtTableToExcel.client.TableToExcelClientBuilder;
/**
 * FormTablescreen template.
 */
public class BranchForm extends FormTableScreen<Branch>  implements ClickHandler {

	//Token to add the varialble declarations
	EmailTextBox etbEmail,etbPointOfContactEmail;
	AddressComposite addressComposite;
	CheckBox checkBoxStatus;
	TextBox tbBranchName,tbPointOfContactName;
	//ObjectListBox<LeaveCalendar>calendar;
	PhoneNumberBox pnbLandlinePhone,pnbCellPhoneNo1,pnbCellPhoneNo2,pnbFaxNo,pnbPointOfContactLandline,pnbPointOfContactCell;


	//Token to add the concrete presenter class name
	//protected <PresenterClassName> presenter;

	Branch branchobj;
	/**
	 * Added by Rahul Verma
	 * Date: 2 Sept 2017
	 * 
	 */
	TextBox tbGSTINNumber;
	
	/**
	 * Date 16/12/2017
	 * By Jayshree
	 * Des.to add the BranchCode Changes are Done
	 */
		TextBox tbBranchCode;
		/**
		 *  nidhi
		 *  5-04-2018
		 *  for branch as a company
		 */
		 UploadComposite uploadComposite,uploadHeader,uploadFooter,uploadDigitalSign;
		ObjectListBox<CompanyPayment> objPaymentMode ;
		 TextBox tbSignatoryText;
		
		 ArrayList<CompanyPayment> paymentModeList = new ArrayList<CompanyPayment>();
		 /**
		  *  end
		  * @param table
		  * @param mode
		  * @param captionmode
		  */
		  /** date 13.4.2018 added by komal for pestmorterm **/
		TextBox tbReferenceNumber;
		
		/**
		 * Date : 02-10-2018 BY ANIL
		 */
		TextBox tbEsicCode;
		TextBox tbPtCode;
		TextBox tbLwfCode;
		
		/**
		 * Updated By:Viraj 
		 * Date: 12-01-2019
		 * Description: To add fumigation Info 
		 */
		TextBox tbShortName;
		TextBox tbRegNoMBR;
		TextBox tbRegNoALP;
		/** Ends **/
		/** DATE 4.5.2019 added by komal for refernce number 2 (profit center for NBHC )**/
		TextBox tbReferenceNumber2;
	/**
	 * @author Anil , Date : 29-11-2019 Capturing paid days for payroll,
	 *         currently per day salary calculated on the basis of month days
	 */

	DoubleBox dbPaidDaysInMonth;
	CheckBox cbExceptionForMonth;
	DoubleBox dbPaidDaysForExceptionalMonth;
	ListBox lbMonth;
	TextBox tbExtraDayCompName;
	
	/**
	 * @author Abhinav
	 * @since 06/12/2019
	 * For Bitco by Rahul Tiwari, We Added a "Correspondence Name" field in branch Master
	 */
	TextBox tbCorrespondenceName;
	
	/**
	 * @author Anil
	 * @since 02-05-2020
	 * adding calendar and shift for premium 
	 */
	
	ObjectListBox<Calendar> olbcalender;
	ObjectListBox<Shift> oblShiftName;
      	/**
	   * Updated By: Amol
	   * Date:1-8-2020
	   * Description: To add Invoice Prefix raised by Ashwini Bhagwat
	   */
	  TextBox tbInvoicePrefix;	
	  
	  TextBox tbPanNumber;
	  
	  /**
	   * @author Anil @since 01-10-2021
	   * Adding invoice title for non gst invoices and print bank details flag
	   * Raised by Rahul Tiwari and Nitin Sir
	   */
	  TextBox tbInvoiceTitle;
	  CheckBox cbPrintBankDetails;
	  
	  
	  ArticleTypeComposite tbArticleTypeInfoComposite;
	  
	  CheckBox cbEinvoiceApplicable;
	  
	  public ObjectListBox<Config> olbcNumberRange;

	  
	public BranchForm  (SuperTable<Branch> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
		tbArticleTypeInfoComposite.setForm(this);
	}
	public BranchForm  (SuperTable<Branch> table, int mode,boolean captionmode, boolean popupflag) {
		super(table, mode, captionmode,popupflag);
		createGui();
		tbArticleTypeInfoComposite.setForm(this);

	}

	private void initalizeWidget()
	{

		etbEmail=new EmailTextBox();

		etbPointOfContactEmail=new EmailTextBox();

		addressComposite=new AddressComposite();

		checkBoxStatus=new CheckBox();
		checkBoxStatus.setValue(true); 
		tbBranchName=new TextBox();

		tbPointOfContactName=new TextBox();

		pnbLandlinePhone=new PhoneNumberBox();

		pnbCellPhoneNo1=new PhoneNumberBox();

		pnbCellPhoneNo2=new PhoneNumberBox();

		pnbFaxNo=new PhoneNumberBox();

		pnbPointOfContactLandline=new PhoneNumberBox();

		pnbPointOfContactCell=new PhoneNumberBox();
		/*calendar=new ObjectListBox<LeaveCalendar>();
		LeaveCalendar.makeObjectListBoxLive(calendar);*/

		/*
		 * Added by Rahul Verma
		 */
		tbGSTINNumber=new TextBox();
		

		/**
		 * Date 16/12/2017
		 * By Jayshree
		 * Des.to add the BranchCode Changes are Done
		 */
		tbBranchCode=new TextBox();
		/**
		 *  nidhi
		 *  5-04-2018
		 *  for branch as company
		 */
		uploadComposite=new UploadComposite();
		
		uploadHeader = new UploadComposite();
		uploadFooter = new UploadComposite();
		uploadDigitalSign=new UploadComposite();
		
		tbSignatoryText=new TextBox();
		objPaymentMode = new ObjectListBox<CompanyPayment>();
		objPaymentMode.addItem("--SELECT--");
		loadPaymentMode();
		/**
		 * end
		 */
		 /** date 13.4.2018 added by komal for pestmorterm **/
		 tbReferenceNumber = new TextBox();
		 
		 tbEsicCode=new TextBox();
		 tbLwfCode=new TextBox();
		 tbPtCode=new TextBox();
		 
		 /**
		  * Updated By: Viraj
		  * Date: 12-01-2019
		  * Description: To Add fumigation Info
		  */
		 tbShortName = new TextBox();
		 tbRegNoMBR = new TextBox();
		 tbRegNoALP = new TextBox();
		 /** Ends **/
		 /** DATE 4.5.2019 added by komal for refernce number 2 (profit center for NBHC )**/
		 tbReferenceNumber2 = new TextBox();
		 
		 
		dbPaidDaysInMonth=new DoubleBox();
		cbExceptionForMonth=new CheckBox();
		cbExceptionForMonth.setValue(false);
		cbExceptionForMonth.addClickHandler(this);
		dbPaidDaysForExceptionalMonth=new DoubleBox();
		dbPaidDaysForExceptionalMonth.setEnabled(false);

		lbMonth = new ListBox();
		lbMonth.addItem("--SELECT--");
		lbMonth.addItem("JAN");
		lbMonth.addItem("FEB");
		lbMonth.addItem("MAR");
		lbMonth.addItem("APR");
		lbMonth.addItem("MAY");
		lbMonth.addItem("JUN");
		lbMonth.addItem("JUL");
		lbMonth.addItem("AUG");
		lbMonth.addItem("SEP");
		lbMonth.addItem("OCT");
		lbMonth.addItem("NOV");
		lbMonth.addItem("DEC");
		lbMonth.setEnabled(false);
			
		tbExtraDayCompName=new TextBox();
		tbCorrespondenceName=new TextBox();
		 
		olbcalender = new ObjectListBox<Calendar>();
		Calendar.makeObjectListBoxLive(olbcalender);
		
		oblShiftName=new ObjectListBox<Shift>();
		MyQuerry querry1=new MyQuerry();
		querry1.setQuerryObject(new Shift());
		oblShiftName.MakeLive(querry1);
		  tbInvoicePrefix=new TextBox();
		
		tbPanNumber = new TextBox();
		
		tbInvoiceTitle=new TextBox();
		cbPrintBankDetails=new CheckBox();
		cbPrintBankDetails.setValue(false);

		tbArticleTypeInfoComposite = new ArticleTypeComposite();

		cbEinvoiceApplicable = new CheckBox();
		
		olbcNumberRange = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange, Screen.NUMBERRANGE);
		olbcNumberRange.setTitle("If you select number range at branch level then that will get automatically selected after selecting branch on contract screen");
		
		
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.

	    initalizeWidget();
		
		this.processlevelBarNames=new String[]{"New"};
		

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCompanyInformation=fbuilder.setlabel("Branch Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Branch Name",tbBranchName);
		FormField ftbBranchName= fbuilder.setMandatory(true).setMandatoryMsg("Branch Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Branch Email",etbEmail);
		FormField fetbEmail= fbuilder.setMandatory(true).setMandatoryMsg("Email is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",checkBoxStatus);
		FormField fcheckBoxStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Landline Phone",pnbLandlinePhone);
		FormField fpnbLandlinePhone= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Cell Phone No 1",pnbCellPhoneNo1);
		FormField fpnbCellPhoneNo1= fbuilder.setMandatory(true).setMandatoryMsg("Cell Number 1 is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Cell Phone No 2",pnbCellPhoneNo2);
		FormField fpnbCellPhoneNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Fax No",pnbFaxNo);
		FormField fpnbFaxNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPointOfContactInformation=fbuilder.setlabel("Point Of Contact Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Point Of Contact Name",tbPointOfContactName);
		FormField ftbPointOfContactName= fbuilder.setMandatory(true).setMandatoryMsg("Point Of Contact Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Point Of Contact Landline",pnbPointOfContactLandline);
		FormField fpnbPointOfContactLandline= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Point Of Contact Cell",pnbPointOfContactCell);
		FormField fpnbPointOfContactCell= fbuilder.setMandatory(true).setMandatoryMsg("Point Of Contact Cell is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Point Of Contact Email",etbPointOfContactEmail);
		FormField fetbPointOfContactEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingAddressInformation=fbuilder.setlabel("Address Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",addressComposite);
		FormField faddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
       /*
		fbuilder = new FormFieldBuilder("Calendar",calendar);
		FormField fcalendar= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        */

		fbuilder = new FormFieldBuilder("Branch GSTIN",tbGSTINNumber);
		FormField ftbGSTINNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 16/12/2017
		 * By Jayshree
		 * Des.to add the BranchCode Changes are Done
		 */
		
		fbuilder = new FormFieldBuilder("Branch Code",tbBranchCode);
		FormField ftbBranchCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		/**
		 * nidhi
		 * 5-04-2018
		 * for branch as company
		 */
		fbuilder = new FormFieldBuilder();
		FormField fgroupingUploadLogo=fbuilder.setlabel("Upload Logo (Size : 475x513 px)").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",uploadComposite);
		FormField fuploadComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		//***************rohan added here for sending header and footer in email **************
		FormField fgroupingUploadHF=fbuilder.setlabel("Upload Header and Footer").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Header(Size : 2400x722 px)",uploadHeader);
		FormField fuploadHeader= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Footer(Size : 3340x365 px)",uploadFooter);
		FormField fuploadFooter= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
	    FormField fgroupingUploadDigital=fbuilder.setlabel("Upload Digital Signature (Size : 613x500 px)").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
        fbuilder = new FormFieldBuilder("",uploadDigitalSign);
		FormField fuploadDigitalSign= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
        
		fbuilder = new FormFieldBuilder("Signatory Text",tbSignatoryText);
		FormField ftbSignatoryText= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		fbuilder = new FormFieldBuilder("Payment Mode",objPaymentMode);
		FormField fobjPaymentMode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		/**
		 *  end
		 */
		/** date 13/04/2018 added by komal for pestmorterm **/
		FormField ftbReferenceNumber =  null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "MakeRefNumberAsCostCenter")){
			fbuilder = new FormFieldBuilder("* Cost Center",tbReferenceNumber);
			ftbReferenceNumber= fbuilder.setMandatory(true).setMandatoryMsg("Cost Center is Mandatory.").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Reference Number",tbReferenceNumber);
			ftbReferenceNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		
		
		fbuilder = new FormFieldBuilder("ESIC Code No.",tbEsicCode);
		FormField ftbEsicCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("PT Code No.",tbPtCode);
		FormField ftbPtCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("LWF Code No.",tbLwfCode);
		FormField ftbLwfCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 * Updated By: Viraj
		 * Date: 12-01-2019
		 * Description: To add Fumigation info
		 */
		FormField fgroupingFumigationInfo=fbuilder.setlabel("Fumigation Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Short Name",tbShortName);
		FormField ftbShortName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Registration number(MBR)",tbRegNoMBR);
		FormField ftbRegNoMBR= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Registration number(ALP)",tbRegNoALP);
		FormField ftbRegNoALP= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/** Ends **/
		/** DATE 4.5.2019 added by komal for refernce number 2 (profit center for NBHC )**/
		FormField ftbReferenceNumber2 =  null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "MakeRefNumber2AsProfitCenter")){
			fbuilder = new FormFieldBuilder("* Profit Center",tbReferenceNumber2);
			ftbReferenceNumber2= fbuilder.setMandatory(true).setMandatoryMsg("Profit Center is Mandatory.").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Reference Number 2",tbReferenceNumber2);
			ftbReferenceNumber2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
			
		boolean flagForBranch = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany");
		

		fbuilder = new FormFieldBuilder("Invoice Prefix",tbInvoicePrefix);
		FormField ftbInvoicePrefix= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		
		/**
		 * Updated By: Viraj
		 * Date: 12-01-2019
		 * Description: To add fumigation info
		 */
		boolean flagForFumigation = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "FumigationDetails");
		
		
		/**
		 * 
		 */
		FormField fgroupingPaidDaysInMonth=fbuilder.setlabel("HR Paid Days Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Paid days in month",dbPaidDaysInMonth);
		FormField fdbPaidDaysInMonth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Is Exception For Specific Month",cbExceptionForMonth);
		FormField fcbExceptionForMonth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Specific Month",lbMonth);
		FormField flbMonth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Paid days in specific monnth",dbPaidDaysForExceptionalMonth);
		FormField fdbPaidDaysForExceptionalMonth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Component name for extra day",tbExtraDayCompName);
		FormField ftbExtraDayCompName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Correspondence Name",tbCorrespondenceName);
		FormField ftbCorrespondenceName= fbuilder.setMandatory(false).setMandatoryMsg("Correspondence Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Calender Name", olbcalender);
		FormField fcalendername = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Shift", oblShiftName);
		FormField foblShiftName = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("PAN Number", tbPanNumber);
		FormField ftbPanNumber = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Invoice Title", tbInvoiceTitle);
		FormField ftbInvoiceTitle = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Print bank details on invoice", cbPrintBankDetails);
		FormField fcbPrintBankDetails = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingArticleInformation=fbuilder.setlabel("Article Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("",tbArticleTypeInfoComposite);
        FormField ftbArticleTypeInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
        
        fbuilder = new FormFieldBuilder("Einvoice Applicable",cbEinvoiceApplicable);
        FormField fcbEinvoiceApplicable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        
        fbuilder = new FormFieldBuilder("Number Range",olbcNumberRange);
        FormField folbcNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
        
		if(flagForFumigation) {
			FormField[][] formfield = {   {fgroupingCompanyInformation},
					  {ftbBranchName,ftbBranchCode,ftbGSTINNumber,ftbCorrespondenceName},
					  {fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2,fpnbFaxNo},
					  {fetbEmail , ftbReferenceNumber , ftbReferenceNumber2,fcheckBoxStatus},
					  {ftbInvoiceTitle,fcbPrintBankDetails,fcbEinvoiceApplicable,folbcNumberRange},
					  {fgroupingPointOfContactInformation},
					  {ftbPointOfContactName,fpnbPointOfContactLandline,fpnbPointOfContactCell,fetbPointOfContactEmail},
					  {fgroupingFumigationInfo},
					  {ftbShortName,ftbRegNoMBR,ftbRegNoALP},
					  {fgroupingAddressInformation},
					  {faddressComposite},
					  {ftbEsicCode,ftbPtCode,ftbLwfCode,ftbPanNumber},
					  
					  {fgroupingPaidDaysInMonth},
					  {fdbPaidDaysInMonth,fcbExceptionForMonth,flbMonth,fdbPaidDaysForExceptionalMonth},
					  {ftbExtraDayCompName,fcalendername,foblShiftName},
					  {fgroupingArticleInformation},
					  {ftbArticleTypeInfoComposite}
					};
			
			this.fields=formfield;
		}
		/** Ends **/
			else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","PC_PRINTINVOICENUMBERPREFIX")){
				FormField[][] formfield = {   {fgroupingCompanyInformation},
						  {ftbBranchName,ftbBranchCode,ftbGSTINNumber,ftbCorrespondenceName},
						  {fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2,fpnbFaxNo},
						  {fetbEmail,ftbSignatoryText,ftbReferenceNumber,ftbReferenceNumber2},
						  {ftbInvoicePrefix,ftbInvoiceTitle,fcbPrintBankDetails,fcheckBoxStatus},
						  {fcbEinvoiceApplicable,folbcNumberRange},
						  {fgroupingPointOfContactInformation},
						  {ftbPointOfContactName,fpnbPointOfContactLandline,fpnbPointOfContactCell,fetbPointOfContactEmail},
						  {fgroupingAddressInformation},
						  {faddressComposite},
						  {fobjPaymentMode},
						  {fgroupingUploadLogo},
						  {fuploadComposite},
						  {fgroupingUploadHF},
						  {fuploadHeader,fuploadFooter},
						  {fgroupingUploadDigital},
						  {fuploadDigitalSign},
						  {ftbEsicCode,ftbPtCode,ftbLwfCode,ftbPanNumber},
						  
						  {fgroupingPaidDaysInMonth},
						  {fdbPaidDaysInMonth,fcbExceptionForMonth,flbMonth,fdbPaidDaysForExceptionalMonth},
						  {ftbExtraDayCompName,fcalendername,foblShiftName},
						  {fgroupingArticleInformation},
						  {ftbArticleTypeInfoComposite}
						};



			this.fields=formfield;
			}
		
		
		
		
		else if(!flagForBranch){
			FormField[][] formfield = {   {fgroupingCompanyInformation},
					  {ftbBranchName,ftbBranchCode,ftbGSTINNumber,ftbCorrespondenceName},
					  {fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2,fpnbFaxNo},
					  {fetbEmail , ftbReferenceNumber ,ftbReferenceNumber2,fcheckBoxStatus},/** reference number added by komal **/
					  {ftbInvoiceTitle,fcbPrintBankDetails,fcbEinvoiceApplicable,folbcNumberRange},
					  {fgroupingPointOfContactInformation},
					  {ftbPointOfContactName,fpnbPointOfContactLandline,fpnbPointOfContactCell,fetbPointOfContactEmail},
					  {fgroupingAddressInformation},
					  {faddressComposite},
					  {ftbEsicCode,ftbPtCode,ftbLwfCode,ftbPanNumber},
					  
					  {fgroupingPaidDaysInMonth},
					  {fdbPaidDaysInMonth,fcbExceptionForMonth,flbMonth,fdbPaidDaysForExceptionalMonth},
					  {ftbExtraDayCompName,fcalendername,foblShiftName},
					  {fgroupingArticleInformation},
					  {ftbArticleTypeInfoComposite}
					};

		this.fields=formfield;
		}else{
			FormField[][] formfield = {   {fgroupingCompanyInformation},
					  {ftbBranchName,ftbBranchCode,ftbGSTINNumber,ftbCorrespondenceName},
					  {fpnbLandlinePhone,fpnbCellPhoneNo1,fpnbCellPhoneNo2,fpnbFaxNo},
					  {fetbEmail,ftbSignatoryText,ftbReferenceNumber,ftbReferenceNumber2},
					  {ftbInvoiceTitle,fcbPrintBankDetails,fcheckBoxStatus,fcbEinvoiceApplicable},
					  {folbcNumberRange},
					  {fgroupingPointOfContactInformation},
					  {ftbPointOfContactName,fpnbPointOfContactLandline,fpnbPointOfContactCell,fetbPointOfContactEmail},
					  {fgroupingAddressInformation},
					  {faddressComposite},
					  {fobjPaymentMode},
					  {fgroupingUploadLogo},
					  {fuploadComposite},
					  {fgroupingUploadHF},
					  {fuploadHeader,fuploadFooter},
					  {fgroupingUploadDigital},
					  {fuploadDigitalSign},
					  {ftbEsicCode,ftbPtCode,ftbLwfCode,ftbPanNumber},
					  
					  {fgroupingPaidDaysInMonth},
					  {fdbPaidDaysInMonth,fcbExceptionForMonth,flbMonth,fdbPaidDaysForExceptionalMonth},
					  {ftbExtraDayCompName,fcalendername,foblShiftName},
					  {fgroupingArticleInformation},
					  {ftbArticleTypeInfoComposite}
					};



		this.fields=formfield;
		}
		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Branch model) 
	{

		if(tbBranchName.getValue()!=null)
			model.setBusinessUnitName(tbBranchName.getValue().trim());
		if(etbEmail.getValue()!=null)
			model.setEmail(etbEmail.getValue());
		if(checkBoxStatus.getValue()!=null)
			model.setstatus(checkBoxStatus.getValue());
		if(pnbLandlinePhone.getValue()!=null)
			model.setLandline(pnbLandlinePhone.getValue());
		if(pnbCellPhoneNo1.getValue()!=null)
			model.setCellNumber1(pnbCellPhoneNo1.getValue());
		if(pnbCellPhoneNo2.getValue()!=null)
			model.setCellNumber2(pnbCellPhoneNo2.getValue());
		if(pnbFaxNo.getValue()!=null)
			model.setFaxNumber(pnbFaxNo.getValue());
		if(tbPointOfContactName.getValue()!=null)
			model.setPocName(tbPointOfContactName.getValue());
		if(pnbPointOfContactLandline.getValue()!=null)
			model.setPocLandline(pnbPointOfContactLandline.getValue());
		if(pnbPointOfContactCell.getValue()!=null)
			model.setPocCell(pnbPointOfContactCell.getValue());
		if(etbPointOfContactEmail.getValue()!=null)
			model.setPocEmail(etbPointOfContactEmail.getValue());
		if(addressComposite.getValue()!=null)
			model.setAddress(addressComposite.getValue());

		if(tbGSTINNumber.getValue()!=null){
			model.setGSTINNumber(tbGSTINNumber.getValue());
		}
		
		/**
		 * Date 16/12/2017
		 * By Jayshree
		 * Des.to add the BranchCode Changes are Done
		 */
		if(tbBranchCode.getValue()!=null){
			model.setBranchCode(tbBranchCode.getValue());
		}
		/*model.setCalendar(calendar.getSelectedItem());
		if(calendar.getValue()!=null)
			model.setCalendarName(calendar.getValue());
		else
			model.setCalendarName("");*/
		
		if(tbEsicCode.getValue()!=null){
			model.setEsicCode(tbEsicCode.getValue());
		}
		if(tbPtCode.getValue()!=null){
			model.setPtCode(tbPtCode.getValue());
		}
		if(tbLwfCode.getValue()!=null){
			model.setLwfCode(tbLwfCode.getValue());
		}
		
		
		manageBranchDropDown(model);
		
		branchobj=model;

		/**
		 *  nidhi
		 *  5-04-2018
		 *  for branch as company
		 */
		model.setUploadHeader(uploadHeader.getValue());
		model.setUploadFooter(uploadFooter.getValue());

		model.setUploadDigitalSign(uploadDigitalSign.getValue());
		model.setLogo(uploadComposite.getValue());
		model.setSignatoryText(tbSignatoryText.getValue());
		
		if(objPaymentMode.getSelectedIndex()!=0){
			model.setPaymentMode(objPaymentMode.getValue(objPaymentMode.getSelectedIndex()));
		}
		else {
			model.setPaymentMode("");
		}
		/**
		 *  end
		 */
		/** date 13/04/2018 added by komal for pestmorterm **/
		if(tbReferenceNumber.getValue()!=null){
			model.setReferenceNumber(tbReferenceNumber.getValue());
		}
		
		/**
		 * Update By: Viraj
		 * Date: 12-01-2019
		 * Description: To save fumigation info
		 */
		if(tbShortName.getValue() != null) {
			model.setShortName(tbShortName.getValue());
		}
		
		if(tbRegNoMBR.getValue() != null) {
			model.setRegNoMBR(tbRegNoMBR.getValue());
		}
		
		if(tbRegNoALP.getValue() != null) {
			model.setRegNoALP(tbRegNoALP.getValue());
		}
		/** Ends **/
		/** DATE 4.5.2019 added by komal for refernce number 2 (profit center for NBHC )**/
		if(tbReferenceNumber2.getValue() != null){
			model.setReferenceNumber2(tbReferenceNumber2.getValue());
		}
		
		if(dbPaidDaysInMonth.getValue()!=null){
			model.setPaidDaysInMonth(dbPaidDaysInMonth.getValue());
		}else{
			model.setPaidDaysInMonth(0);
		}
		model.setExceptionForMonth(cbExceptionForMonth.getValue());
		if(lbMonth.getSelectedIndex()!=0){
			model.setSpecificMonth(lbMonth.getValue(lbMonth.getSelectedIndex()));
		}else{
			model.setSpecificMonth("");
		}
		if(dbPaidDaysForExceptionalMonth.getValue()!=null){
			model.setPaidDaysForExceptionalMonth(dbPaidDaysForExceptionalMonth.getValue());
		}else{
			model.setPaidDaysForExceptionalMonth(0);
		}
		
		if(tbExtraDayCompName.getValue()!=null){
			model.setExtraDayCompName(tbExtraDayCompName.getValue());
		}
		
		if(tbCorrespondenceName.getValue()!=null)
			model.setCorrespondenceName(tbCorrespondenceName.getValue().trim());
		
		if(olbcalender.getSelectedIndex()!=0) {
			model.setCalendarName(olbcalender.getValue());
			model.setCalendar(olbcalender.getSelectedItem());
		}else {
			model.setCalendarName("");
			model.setCalendar(null);
		}
		
		if(oblShiftName.getSelectedIndex()!=0) {
			model.setShiftName(oblShiftName.getValue());
			model.setShift(oblShiftName.getSelectedItem());
		}else {
			model.setShiftName("");
			model.setShift(null);
		}
			if(tbInvoicePrefix.getValue() != null) {
			model.setInvoicePrefix(tbInvoicePrefix.getValue());
		}
			
		if(tbPanNumber.getValue()!=null){
			model.setPANNumber(tbPanNumber.getValue());
		}
		
		model.setInvoiceTitle(tbInvoiceTitle.getValue());
		model.setPrintBankDetails(cbPrintBankDetails.getValue());
			
		if(tbArticleTypeInfoComposite.getValue()!=null)
			model.setArticleTypeDetails(tbArticleTypeInfoComposite.getValue());

		model.setEinvoiceApplicable(cbEinvoiceApplicable.getValue());
		
		if(olbcNumberRange.getValue()!=null)
			model.setNumberRange(olbcNumberRange.getValue());
		else
			model.setNumberRange("");
		
		presenter.setModel(model);


	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Branch view) 
	{

		branchobj=view;
		/**
		 * Date 20-04-2018 By vijay for orion pest locality load  
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
			setAddressDropDown();
		}
		/**
		 * ends here
		 */
		
		if(view.getBusinessUnitName()!=null)
			tbBranchName.setValue(view.getBusinessUnitName());
		if(view.getEmail()!=null)
			etbEmail.setValue(view.getEmail());
		if(view.getstatus()!=null)
			checkBoxStatus.setValue(view.getstatus());
		if(view.getLandline()!=null)
			pnbLandlinePhone.setValue(view.getLandline());
		if(view.getCellNumber1()!=null)
			pnbCellPhoneNo1.setValue(view.getCellNumber1());
		if(view.getCellNumber2()!=null)
			pnbCellPhoneNo2.setValue(view.getCellNumber2());
		if(view.getFaxNumber()!=null)
			pnbFaxNo.setValue(view.getFaxNumber());
		if(view.getPocName()!=null)
			tbPointOfContactName.setValue(view.getPocName());
		if(view.getPocLandline()!=null)
			pnbPointOfContactLandline.setValue(view.getPocLandline());
		if(view.getPocCell()!=null)
			pnbPointOfContactCell.setValue(view.getPocCell());
		if(view.getPocEmail()!=null)
			etbPointOfContactEmail.setValue(view.getPocEmail());
		if(view.getAddress()!=null)
			addressComposite.setValue(view.getAddress());
		
		if(view.getGSTINNumber()!=null){
			tbGSTINNumber.setValue(view.getGSTINNumber());
		}
		

		if(view.getBranchCode()!=null){
			tbBranchCode.setValue(view.getBranchCode());
		}
		
		/**
		 *  nidhi
		 *  5-04-2018
		 *  for branch as company
		 */
			if(view.getLogo()!=null)
			  uploadComposite.setValue(view.getLogo());
			
			
			if(view.getUploadHeader()!=null)
				  uploadHeader.setValue(view.getUploadHeader());
			
			if(view.getUploadFooter()!=null)
				  uploadFooter.setValue(view.getUploadFooter());
			
			if(view.getUploadDigitalSign()!=null)
				uploadDigitalSign.setValue(view.getUploadDigitalSign());
			
			tbSignatoryText.setValue(view.getSignatoryText());
			
			if(view.getPaymentMode()!=null){
				objPaymentMode.setValue(view.getPaymentMode());
			}
		/**
		 *  end
		 */
			/** date 13/04/2018 added by komal for pestmorterm **/
			if(view.getReferenceNumber()!=null){
				tbReferenceNumber.setValue(view.getReferenceNumber());
			}

		/*if(view.getCalendarName()!=null)
			calendar.setValue(view.getCalendarName());*/
			
			if(view.getEsicCode()!=null){
				tbEsicCode.setValue(view.getEsicCode());
			}
			
			if(view.getPtCode()!=null){
				tbPtCode.setValue(view.getPtCode());
			}
			
			if(view.getLwfCode()!=null){
				tbLwfCode.setValue(view.getLwfCode());
			}	
			/**
			 * Update By: Viraj
			 * Date: 12-01-2019
			 * Description: To save fumigation info
			 */
			if(view.getShortName() != null) {
				tbShortName.setValue(view.getShortName());
			}
			
			if(view.getRegNoMBR() != null) {
				tbRegNoMBR.setValue(view.getRegNoMBR());
			}
			
			if(view.getRegNoALP() !=  null) {
				tbRegNoALP.setValue(view.getRegNoALP());
			}
			/** Ends **/
			/** DATE 4.5.2019 added by komal for refernce number 2 (profit center for NBHC )**/
			if(view.getReferenceNumber2() != null){
				tbReferenceNumber2.setValue(view.getReferenceNumber2());
			}
			
			if(view.getPaidDaysInMonth()!=0){
				dbPaidDaysInMonth.setValue(view.getPaidDaysInMonth());
			}
			cbExceptionForMonth.setValue(view.isExceptionForMonth());
			if(view.getSpecificMonth()!=null){
				for(int i=1;i<lbMonth.getItemCount();i++){
					if(lbMonth.getItemText(i).equals(view.getSpecificMonth())){
						lbMonth.setSelectedIndex(i);
						break;
					}
				}
			}
			if(view.getPaidDaysForExceptionalMonth()!=0){
				dbPaidDaysForExceptionalMonth.setValue(view.getPaidDaysForExceptionalMonth());
			}
			
			if(view.getExtraDayCompName()!=null){
				tbExtraDayCompName.setValue(view.getExtraDayCompName());
			}
			if(view.getCorrespondenceName()!=null)
				tbCorrespondenceName.setValue(view.getCorrespondenceName());
			
			if(view.getCalendarName()!=null) {
				olbcalender.setValue(view.getCalendarName());
			}
			
			if(view.getShiftName()!=null) {
				oblShiftName.setValue(view.getShiftName());
			}
			if(view.getInvoicePrefix() != null) {
				tbInvoicePrefix.setValue(view.getInvoicePrefix());
			}

			if(view.getPANNumber()!=null){
				tbPanNumber.setValue(view.getPANNumber());
			}
			
			if(view.getInvoiceTitle()!=null){
				tbInvoiceTitle.setValue(view.getInvoiceTitle());
			}
			
			cbPrintBankDetails.setValue(view.isPrintBankDetails());
			
			if(view.getArticleTypeDetails()!=null)
				tbArticleTypeInfoComposite.setValue(view.getArticleTypeDetails());
			
			cbEinvoiceApplicable.setValue(view.isEinvoiceApplicable());
			
			if(view.getNumberRange()!=null&&!view.getNumberRange().equals(""))
				olbcNumberRange.setValue(view.getNumberRange());
		presenter.setModel(view);



	}

	
	/**
	 * Date 20-04-2018 By vijay
	 * here i am refreshing address composite data if global list size greater than drop down list size
	 * because we are adding locality and city from an entity in view state to global list if doest not exist in global list 
	 */
	private void setAddressDropDown() {
		if(LoginPresenter.globalLocality.size()>addressComposite.locality.getItems().size()	){
			addressComposite.locality.setListItems(LoginPresenter.globalLocality);
		}
	}
	/**
	 * ends here 
	 */
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
				 System.out.println("popup menu bar"+menus.toString());
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				 System.out.println("normal flow"+menus.toString());
			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Sillenium Test")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Vijay Date - 21-04-2021
			 * Des :- updated code to manage popup and regular screen app level menus
			 */
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.BRANCH,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{

	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==cbExceptionForMonth){
			if(cbExceptionForMonth.getValue()==true){
				lbMonth.setEnabled(true);
				dbPaidDaysForExceptionalMonth.setEnabled(true);
			}else{
				lbMonth.setSelectedIndex(0);
				lbMonth.setEnabled(false);
				dbPaidDaysForExceptionalMonth.setValue(null);
				dbPaidDaysForExceptionalMonth.setEnabled(false);
			}
		}
	}

	
	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new Branch();
		model=branchobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.BILLINGDETAILS, branchobj.getCount(), null,null,null, false, model, null);
	
		
	}
	
	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(branchobj!=null){

			SuperModel model=new Branch();
			model=branchobj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.BILLINGDETAILS, branchobj.getCount(), null,null,null, false, model, null);
		
		}
	}
	
	
	@Override
	public void clear() {
		super.clear();
		checkBoxStatus.setValue(true);
		cbExceptionForMonth.setValue(false);
		dbPaidDaysForExceptionalMonth.setEnabled(false);
		lbMonth.setEnabled(false);
		cbPrintBankDetails.setValue(false);
	}
	
	
	public void manageBranchDropDown(Branch branchModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalBranch.add(branchModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalBranch.size();i++)
			{
				if(LoginPresenter.globalBranch.get(i).getId().equals(branchModel.getId()))
				{
					LoginPresenter.globalBranch.add(branchModel);
					LoginPresenter.globalBranch.remove(i);
				}
			}
		}
		
		/**
		 * Date 20-11-2020 added by Priyanka 
		 * Des - when a new branch is created it is not reflecting automatically in the customer screen branch selection dropdown.
		 */
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.customerScreenBranchList.add(branchModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.customerScreenBranchList.size();i++)
			{
				if(LoginPresenter.customerScreenBranchList.get(i).getId().equals(branchModel.getId()))
				{
					LoginPresenter.customerScreenBranchList.add(branchModel);
					LoginPresenter.customerScreenBranchList.remove(i);
				}
			}
		}
		
	}
	
	/**
	 *  End Here
	 */

	void loadPaymentMode(){
		try {
			 GenricServiceAsync async =  GWT.create(GenricService.class);

			MyQuerry querry = new MyQuerry();
			
			
			Company c = new Company();
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			
			
			querry.setQuerryObject(new CompanyPayment());

			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub

					System.out.println(" in side date load size"+result.size());
					Console.log("get result size of payment mode --" + result.size());
					objPaymentMode.clear();
					objPaymentMode.addItem("--SELECT--");
					paymentModeList = new ArrayList<CompanyPayment>();
					if (result.size() == 0) {

					} else {
						for (SuperModel model : result) {
							CompanyPayment payList = (CompanyPayment) model;
							paymentModeList.add(payList);
						}
						
						if (paymentModeList.size() != 0) {
							objPaymentMode.setListItems(paymentModeList);
						}
						
						for(int i=1;i<objPaymentMode.getItemCount();i++){
							String paymentDet = objPaymentMode.getItemText(i);
							Console.log("SERVICE DETAILS : "+paymentDet);
						}	
					}
				
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
		if(cbExceptionForMonth.getValue()==true){
			lbMonth.setEnabled(true);
			dbPaidDaysForExceptionalMonth.setEnabled(true);
		}else{
			lbMonth.setEnabled(false);
			dbPaidDaysForExceptionalMonth.setEnabled(false);
		}
		
		tbBranchName.setEnabled(false);
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
//		return super.validate();
		boolean superVal=super.validate();
		
		if(cbExceptionForMonth.getValue()==true){
			String msg="";
			if(lbMonth.getSelectedIndex()==0){
				msg=msg+"Please select specific month. \n";
			}
			if(dbPaidDaysForExceptionalMonth.getValue()==null||dbPaidDaysForExceptionalMonth.getValue()==0){
				msg=msg+"Please add paid days for specific month.";
			}
			
			if(!msg.equals("")){
				showDialogMessage(msg);
				return false;
			}
		}
		
		return superVal;
		
	}
	
	

}
