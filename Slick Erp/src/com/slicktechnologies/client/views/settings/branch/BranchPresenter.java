package com.slicktechnologies.client.views.settings.branch;
import com.google.gwt.event.dom.client.ClickEvent;

import java.util.Vector;

import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.client.library.appstructure.formtablescreen.*;
import com.slicktechnologies.shared.*;
import com.slicktechnologies.shared.common.*;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.productlayer.*;

import java.util.Vector;

import com.simplesoftwares.client.library.appstructure.search.*;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.google.gwt.core.shared.GWT;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.slicktechnologies.shared.common.paymentlayer.*;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
//import com.testclasses.client.BranchTestClass;
//import com.testclasses.client.TestImplementer;
import com.google.gwt.event.dom.client.*;
/**
 *  FormTableScreen presenter template
 */
public class BranchPresenter extends FormTableScreenPresenter<Branch>  {

	//Token to set the concrete form
	BranchForm form;
	
	public BranchPresenter (FormTableScreen<Branch> view, Branch model) {
		super(view, model);
		form=(BranchForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getBranchQuery());
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.BRANCH,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("New"))
		{
			reactOnNew();
		}	
	 

		
	}
	
	

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method template to set new model
	 */
	@Override
	protected void makeNewModel() {
		
		model=new Branch();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getBranchQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new Branch());
		return quer;
	}
	
	public void setModel(Branch entity)
	{
		model=entity;
	}
	
	public static BranchForm initalize()
	{
		BranchPresenterTableProxy gentableScreen=new BranchPresenterTableProxy();
// 		BranchPresenterTable gentableScreen=GWT.create( BranchPresenterTable.class);
		BranchForm  form=new  BranchForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		
		  //// BranchPresenterTable gentableSearch=GWT.create( BranchPresenterTable.class);
		  ////   BranchPresenterSearch.staticSuperTable=gentableSearch;
		  ////     BranchPresenterSearch searchpopup=GWT.create( BranchPresenterSearch.class);
		  ////         form.setSearchpopupscreen(searchpopup);
		
		 BranchPresenter  presenter=new  BranchPresenter  (form,new Branch());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	
		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessunitlayer.Branch")
		 public static class  BranchPresenterTable extends SuperTable<Branch> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}} ;
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessunitlayer.Branch")
			 public static class  BranchPresenterSearch extends SearchPopUpScreen<  Branch>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return true;
				}};
				
	  private void reactTo()
	  {
	  }
	  
	  private void reactOnNew()
	  {
		  form.setToNewState();
		  this.initalize();
	  }


	  @Override
	   public void onClick(ClickEvent event) {
		  super.onClick(event);
	      InlineLabel label= (InlineLabel) event.getSource();
//	      if(label.getText().equals("Selenium Test"))
//		    reactOnSilleniumTest();
		  
	  };
//	@Override
//
//	public void reactOnSilleniumTest() {
//		model=BranchTestClass.populateBranch();
//		form.updateView(model);
//		System.out.println("ahay---");
//	}

	

}
